﻿using System.Collections.Generic;
using System.Web.Optimization;

namespace PayMammoth_v6Deploy.App_Start
{
    public class BundleConfig : CS.MvcCmsGeneral_CS_v6.App_Start.BundleConfig
    {
        protected override void updateLayoutCoreStyleBundle(List<string> filePathsRegex)
        {

            filePathsRegex.Remove("~/Content/css/compiled/main-mobile.css");
            filePathsRegex.Remove("~/Content/css/compiled/main-tablet.css");
            filePathsRegex.Remove("~/Content/css/compiled/main-desktop.css");
            filePathsRegex.Remove("~/Content/css/compiled/main-large-desktop.css");
            filePathsRegex.Add("~/Content/css/compiled/main.css");

            base.updateLayoutCoreStyleBundle(filePathsRegex);
        }

        protected override void updateLayoutMainStyleBundle(System.Collections.Generic.List<string> filePathsRegex)
        {
            
            base.updateLayoutMainStyleBundle(filePathsRegex);
        }
    }
}