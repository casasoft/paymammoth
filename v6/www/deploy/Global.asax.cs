﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CS.General_CS_v6.Util;
using Microsoft.WindowsAzure.Diagnostics;
using NLog;
using PayMammoth_v6.Framework.Application;
using PayMammoth_v6.Modules.Services.TestData;
using PayMammoth_v6Deploy.App_Start;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.Notifications;
using PayMammoth_v6.Connector.Services;
using Raven.Client.Linq;
using LogLevel = NLog.LogLevel;

namespace PayMammoth_v6Deploy
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : MVCApplicationPayMammoth_v6
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        public MvcApplication()
        {
            
            Trace.WriteLine("MvcApplication - Constructor()");
        }


     

        protected override void onApplicationStart()
        {
            
         

            _log.LogToLoggerAndTrace(LogLevel.Info, "global.asax.onApplicationStart() [START]");
            
            var app = new AppInstancePayMammoth_v6();
          //  app.CasaSoftProjectAssemblies.Add(typeof(MvcApplication).Assembly); 
            app.OnApplicationStart();
            _log.LogToLoggerAndTrace(LogLevel.Info, "global.asax.onApplicationStart() [FINISH]");


            InversionUtil.Get<INotificationsHandlerService>()
                .OnNotificationMessageRecevied += MvcApplication_OnPayMammothNotificationMessageRecevied;
            
            

        }

        void MvcApplication_OnPayMammothNotificationMessageRecevied(NotificationMessage msg)
        {
            if (msg.MessageType == EnumsPayMammothConnector.NotificationMessageType.ImmediatePaymentSuccess)
            {
                var testPaymentsDataService = InversionUtil.Get<ITestDataPaymentsService>();
                testPaymentsDataService.OnTestPaymentPaidSuccessfully(msg);
            }
            else
            {
                int k = 5;
            }
        }
        
        /// <summary>
        /// This is only use to confirm the test payments
        /// </summary>
        /// <param name="msg"></param>
        void PayMammothNotificationHandler_OnPaidSuccessfully(NotificationMessage msg)
        {
            
        }

        protected override void onApplicationEnd()
        {
        }

        private static bool _firstRequestDone = false;

        protected override void onBeginRequest()
        {
            if (!_firstRequestDone)
            {
                Constants.PayMammoth_ServerUrl = CS.General_CS_v6.Util.PageUtil.GetApplicationBaseUrl().ToString();
                _log.Debug("Constants.PayMammoth_ServerUrl changed to: {0}", Constants.PayMammoth_ServerUrl);
            }
            _firstRequestDone = true;


        }

        protected override void onEndRequest()
        {
        }
    }
}