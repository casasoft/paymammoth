﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v6.Presentation.Services.ContentTexts;
using BusinessLogic_CS_v6.Presentation.Services.Context;
using BusinessLogic_CS_v6.Presentation.Services.Cultures;
using BusinessLogic_CS_v6.Presentation.Services.Sections;
using CS.General_CS_v6.Modules.Pages;
using CS.General_CS_v6.Modules.Urls;
using CS.MvcGeneral_CS_v6.Code.Attributes;
using PayMammoth_v6.Presentation.Code.Mvc;
using PayMammoth_v6.Presentation.Code.ViewData;
using PayMammoth_v6.Presentation.Models.Home;
using PayMammoth_v6.Presentation.Models.PayPal;
using PayMammoth_v6.Presentation.Services.Payments.PayPal;

namespace PayMammoth_v6Deploy.Controllers
{
    [InitializeAllViewDataFilters]
    [CommonViewContextDataSettingsFilter(AllowAllEmpty = true)]
    public partial class PayPalController : CsControllerPayMammoth_v6
    {
        private readonly ICultureDataPresentationService _cultureModelService;
        private readonly ICommonViewContextDataService _commonViewContextDataService;
        private readonly ISectionDataPresentationService _sectionModelService;
        private readonly IPageService _pageService;
        private readonly IContentTextPresentationService _contentTextModelService;
        private readonly IPaypalDataModelService _paypalDataModelService;
        //
        // GET: /PaymentTransactium/

        public PayPalController(ICultureDataPresentationService cultureModelService,
            ICommonViewContextDataService commonViewContextDataService,
            ISectionDataPresentationService sectionModelService,
            IPageService pageService,
            IContentTextPresentationService contentTextModelService,
            IPaypalDataModelService paypalDataModelService)
        {
            _paypalDataModelService = paypalDataModelService;
            _contentTextModelService = contentTextModelService;
            _pageService = pageService;
            _sectionModelService = sectionModelService;
            _commonViewContextDataService = commonViewContextDataService;
            _cultureModelService = cultureModelService;
        }

        [CommonViewContextDataSettingsFilter(
            AllowEmptyCanonicalUrl = true,
            AllowEmptyOpenGraphTitle = true,
            AllowEmptySection = true,
            DoNotPerformPermanentRedirectionIfPageUrlIsNotSameAsCanonicalUrl = true)]
        public virtual ActionResult PaymentConfirmation()
        {

            var culture = _cultureModelService.GetCultureFromCurrentRouteOrDefaultCultureAndStoreInContext();
            var commonViewContextData = _commonViewContextDataService.GetCommonViewContextData();
            bool isConfirmingRequest =
                _pageService.GetVariableFromRoutingQuerystringOrForm<bool>(
                    PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_Confirm);
            if (isConfirmingRequest)
            {
                var layoutData =
                    _pageService.GetContextObject<LayoutViewData>(
                        PayMammoth_v6.Constants.PayMammoth_v6Constants.PayMammoth_v6LayoutViewDataKey);

                string identifier = layoutData.PaymentRequestDataModel.PaymentRequestId;
                string token = _pageService.GetVariableFromRoutingQuerystringOrForm<string>(
                    PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_Token);
                string payerId = _pageService.GetVariableFromRoutingQuerystringOrForm<string>(
                    PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_PayerId);


                var confirmResult = _paypalDataModelService.ConfirmPaypalRequestFromPresentation(CurrentSession,
                    culture, identifier, token, payerId);
                if (confirmResult.Status == ConfirmPaypalRequestStatus.Success)
                {
                    return new RedirectResult(confirmResult.Result.RedirectUrl);
                }
                else
                {
                    //Show error
                    commonViewContextData.NotificationMessage = new _NotificationMessageModel()
                    {
                        MessageContentTextModel =
                            _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                                CurrentSession,
                                culture,
                                confirmResult.Status),
                        MessageType = NotificationMessageType.Error
                    };
                }
            }


            string url = _pageService.GetCurrentPageUrl();
            UrlHandler urlHandler = new UrlHandler(url);
            urlHandler.QueryString[PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_Confirm] =
                CS.General_CS_v6.Util.ConversionUtil.ConvertBasicDataTypeToString(true);
            string confirmUrl = urlHandler.GetUrl();
            var section = _sectionModelService.GetSectionDataModelByIdentifierOrCreateNew(
                CurrentSession,
                culture,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Payment_PaypalConfirmation);
            commonViewContextData.Page.CurrentSection = section;

            PaypalConfirmationModel model = new PaypalConfirmationModel()
            {
                ConfirmButtonTextModel =
                    _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                        CurrentSession,
                        culture,
                        PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.PayPalConfirmation_ConfirmButton),
                ConfirmTextModel = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    CurrentSession,
                    culture,
                    PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.PayPalConfirmation_DescriptionHtml),
                TitleContentTextModel =
                    _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                        CurrentSession,
                        culture,
                        PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.PayPalConfirmation_Title),
                ConfirmUrl = confirmUrl
            };
            return View(MVCPayMammoth_v6.PayPal.Views.Confirmation, model);

        }
    }
}
