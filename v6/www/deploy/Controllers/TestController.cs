﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic_CS_v5.Presentation.Services.HierarchyData;
using CS.General_CS_v5;
using CS.General_CS_v5.Modules.Pages;
using PayMammoth_v2.Presentation.Code.Mvc;
using PayMammoth_v2.Presentation.Models.Home;
using PayMammoth_v2.Presentation.Services.Test;

namespace PayMammoth_v2Deploy.Controllers
{
    public partial class TestController : CsControllerPayMammoth_v2
    {
        private readonly IHierarchyDataPresentationService _hierarchyDataPresentationService;
        private readonly IMyTestService _testService;
        private IPageService _pageService;


        public TestController(
            IHierarchyDataPresentationService hierarchyDataPresentationService, 
            IMyTestService testService,
            IPageService pageService)
        {
            _pageService = pageService;
            _testService = testService;
            _hierarchyDataPresentationService = hierarchyDataPresentationService;
        }


        //
        // GET: /Test/

        public virtual ActionResult Index()
        {

           // BusinessLogic_CS_v5.Presentation.Code.ContextData.CommonViewContextDataOld.Instance.OpenGraphData.DoNotThrowErrorIfCurrentSectionIsEmptyInLocalhost = true;

            int total = _testService.AddValues(10, 50);
            TestIndexModel model = new TestIndexModel();
            model.PresentationService = _hierarchyDataPresentationService;
            model.Total = total;
            return View(model);
        }


        //
        // GET: /Test/

        public virtual ActionResult Search()
        {
            string keywords = _pageService.GetVariableFromRoutingQuerystringOrForm<string>("keywords");
            var sortBy = _pageService.GetVariableFromRoutingQuerystringOrForm<Enums.EnumsSortBy>("sortBy");

            var results = _testService.SearchItems(keywords, sortBy);
            TestSearchModel model = new TestSearchModel();
            model.Results = results;
            return View(model);
        }
    }
}
