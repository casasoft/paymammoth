﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Presentation.Services.ContentTexts;
using BusinessLogic_CS_v6.Presentation.Services.Context;
using BusinessLogic_CS_v6.Presentation.Services.Cultures;
using BusinessLogic_CS_v6.Presentation.Services.NotificationMessages;
using BusinessLogic_CS_v6.Presentation.Services.Sections;
using CS.General_CS_v6.Modules.Pages;
using CS.MvcGeneral_CS_v6.Code.Attributes;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.HelperClasses;
using PayMammoth_v6.Presentation.Code.Mvc;
using PayMammoth_v6.Presentation.Code.ViewData;
using PayMammoth_v6.Presentation.Models.Home;
using PayMammoth_v6.Presentation.Models.Transactium;
using PayMammoth_v6.Presentation.Services.Payments;

namespace PayMammoth_v6Deploy.Controllers
{
    [InitializeAllViewDataFilters]
    [CommonViewContextDataSettingsFilter(AllowAllEmpty = true)]
    public partial class TransactiumController : CsControllerPayMammoth_v6
    {
        private readonly ICultureDataPresentationService _cultureModelService;
        private readonly ICommonViewContextDataService _commonViewContextDataService;
        private readonly ISectionDataPresentationService _sectionModelService;
        private readonly IPageService _pageService;
        private readonly IContentTextPresentationService _contentTextModelService;
        private readonly IRavenDbService _ravenDbService;
        private readonly ITransactiumService _transactiumService;
        private readonly INotificationMessagesPresentationService _notificationMessagesModelService;
        private readonly IPaymentUrlModelService _paymentUrlModelService;
        //
        // GET: /PaymentTransactium/

        public TransactiumController(
            ICultureDataPresentationService cultureModelService,
            ICommonViewContextDataService commonViewContextDataService,
            ISectionDataPresentationService sectionModelService,
            IPageService pageService,
            IContentTextPresentationService contentTextModelService,
            ITransactiumService transactiumService,
            INotificationMessagesPresentationService notificationMessagesModelService,
            IPaymentUrlModelService paymentUrlModelService)
        {
            _paymentUrlModelService = paymentUrlModelService;
            _notificationMessagesModelService = notificationMessagesModelService;
            _transactiumService = transactiumService;
            _contentTextModelService = contentTextModelService;
            _pageService = pageService;
            _sectionModelService = sectionModelService;
            _commonViewContextDataService = commonViewContextDataService;
            _cultureModelService = cultureModelService;
        }

        [CommonViewContextDataSettingsFilter(AllowEmptyCanonicalUrl = true, AllowEmptyOpenGraphTitle = true, AllowEmptySection = true,
            DoNotPerformPermanentRedirectionIfPageUrlIsNotSameAsCanonicalUrl = true)]
        public virtual ActionResult Payment()
        {
            var culture = _cultureModelService.GetCultureFromCurrentRouteOrDefaultCultureAndStoreInContext();
            var commonViewContextData = _commonViewContextDataService.GetCommonViewContextData();
            string redirectUrl = _pageService.GetVariableFromRoutingQuerystringOrForm<string>(PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_Transactium_RedirectUrl);

            var section = _sectionModelService.GetSectionDataModelByIdentifierOrCreateNew(
                CurrentSession,
                culture,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Payment_Transactium);
            commonViewContextData.Page.CurrentSection = section;

            TransactiumPaymentModel model = new TransactiumPaymentModel()
            {
                RedirectUrl = redirectUrl,
                TitleContentTextModel = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    CurrentSession,
                    culture,
                    PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Transactium_PaymentDetailsTitle)
            };
            return View(
                MVCPayMammoth_v6.Transactium.Views.Payment,
                model);
        }

        [CommonViewContextDataSettingsFilter(AllowEmptyCanonicalUrl = true, AllowEmptyOpenGraphTitle = true,
            AllowEmptySection = true,
            AllowEmptyCmsItem = true,
            DoNotPerformPermanentRedirectionIfPageUrlIsNotSameAsCanonicalUrl = true)]
        public virtual ActionResult StatusHandler()

        {
            string transactionId =
                _pageService.GetVariableFromRoutingQuerystringOrForm<string>(
                    PayMammoth_v6.Connector.Constants.PARAM_TransactionId);


            string hpsId = _pageService.GetVariableFromRoutingQuerystringOrForm<string>(ConstantsTransactium.HPSID);
            TransactiumStatusHandlerModel model = new TransactiumStatusHandlerModel();
            var layoutData =
                _pageService.GetContextObject<LayoutViewData>(
                    PayMammoth_v6.Constants.PayMammoth_v6Constants.PayMammoth_v6LayoutViewDataKey);
            var result = _transactiumService.CheckTransactiumPayment(
                new ReferenceLink<PaymentRequestTransactionData>(transactionId),hpsId);

            if (result.Result == CheckPaymentResponse.CheckPaymentStatusType.Success)
            {
                if (string.IsNullOrWhiteSpace(result.SuccessRedirectUrl))
                {
                    throw new InvalidOperationException("SuccessRedirectUrl should be filled in");
                }

                model.RedirectUrl = result.SuccessRedirectUrl;
            }
            else
            {

                var culture = _cultureModelService.GetCultureFromCurrentRouteOrDefaultCultureAndStoreInContext();

                string paymentUrl = _paymentUrlModelService.GetPaymentSelectionPage(
                    CurrentSession,
                    culture,
                    layoutData.PaymentRequestDataModel.PaymentRequestId);

                string paymentSelectionUrlWithMessage = _notificationMessagesModelService
                    .AppendNotificationMessageToUrlFromIdentifier(
                        CurrentSession,
                        culture,
                        paymentUrl,
                        result.Result);

                model.RedirectUrl = paymentSelectionUrlWithMessage;
            }
            return View(
                MVCPayMammoth_v6.Transactium.Views.StatusHandler,
                model);
        }
    }
}