﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic_CS_v5.Framework.Caching;
using BusinessLogic_CS_v6.Framework.Caching;
using PayMammoth_v6.Presentation.Code.Mvc;
using PayMammoth_v6.Presentation.Models._Shared.PartialViews.Test;

namespace PayMammoth_v2Deploy.Controllers.PartialViews
{
    public partial class PartialViewsTestController : CsControllerPayMammoth_v2
    {
        //
        // GET: /PartialViewsTest/

        [CsOutputCache(Duration = 20)]
        public virtual ActionResult _TestDonutCache(string name)
        {
            _TestDonutCacheModel model = new _TestDonutCacheModel();
            return PartialView(MVCPayMammoth_v2._Shared.Views.PartialViews.Test._TestDonutCache, model );
        }

    }
}
