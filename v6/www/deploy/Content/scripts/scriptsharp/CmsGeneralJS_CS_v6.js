/*! CmsGeneralJS_CS_v6.js 1.0.0.0
 * 
 */

"use strict";

  var $global = this;
 
  // CmsGeneralJS_CS_v6.Enums.CollectionListingViewType

  var CmsGeneralJS_CS_v6$Enums$CollectionListingViewType = {
    listingView: 0, 
    treeView: 10
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.IListingDataGridItem

  function CmsGeneralJS_CS_v6$Controllers$Listing$IListingDataGridItem() { }


  // CmsGeneralJS_CS_v6.Constants.DataAttributes

  function CmsGeneralJS_CS_v6$Constants$DataAttributes() {
  }
  var CmsGeneralJS_CS_v6$Constants$DataAttributes$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.Unobtrusive.UnobtrusiveReferenceLinkSearchButton

  function CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton(options) {
    this._options = options;
    this._init();
  }
  CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton.updateSelectedValueOfCurrentSearchButton = function(selectedValue, selectedText) {
    var currentButton = window.top['CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton'].currentClickedSearchButton;
    if (currentButton != null) {
      currentButton.updateSelectedValueInHiddenFieldAndCloseDialog(selectedValue, selectedText);
    }
  }
  var CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton$ = {
    _init: function() {
      var jHiddenFieldId = this._options.jButton.attr('data-cms-reference-link-search-button');
      this._jHiddenField = $('#' + jHiddenFieldId);
      this._jText = $(this._options.jButton.attr('data-cms-reference-link-search-text'));
      this._options.jButton.on('click', ss.bind('_onClickButton', this));
    },
    _onClickButton: function(e) {
      this._openSearch();
      e.preventDefault();
    },
    _openSearch: function() {
      var searchUrl = this._options.jButton.attr('href');
      this._searchDialog = CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openDialog(searchUrl, 'Search', 'reference-link-search-item-dialog', true);
      CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton.currentClickedSearchButton = this;
      this._searchDialog.on('dialogclose', ss.bind('_onCloseDialog', this));
    },
    _onCloseDialog: function(e) {
      if (CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton.currentClickedSearchButton === this) {
        CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton.currentClickedSearchButton = null;
      }
    },
    get_options: function() {
      return this._options;
    },
    updateSelectedValueInHiddenFieldAndCloseDialog: function(value, selectedText) {
      this._jHiddenField.val(value);
      this._jText.text(selectedText);
      if (this._searchDialog != null) {
        this._searchDialog.dialog('destroy');
        this._searchDialog = null;
      }
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Unobtrusive.UnobtrusiveDialogController

  function CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController(options) {
    this._options = options;
    this._init();
  }
  CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openDialog = function(url, title, cssClass, modal) {
    var options = {};
    var iFrame = $(ss.format("<div class='{0}'><iframe src='{1}'></iframe></div>", cssClass, url));
    options.width = 'auto';
    options.height = 'auto';
    options.title = title;
    options.modal = modal;
    iFrame.dialog(options);
    return iFrame;
  }
  CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.closeCurrentOpenedDialog = function() {
    if (CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openedDialog != null) {
      CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openedDialog.close();
    }
  }
  var CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController$ = {
    _init: function() {
      this._options.jButton.on('click', ss.bind('_onClickButton', this));
    },
    _onClickButton: function(e) {
      var url = this._options.jButton.attr('href');
      var title = this._options.jButton.attr('data-cms-dialog-title') || 'Edit Field Meta Data';
      this._iFrame = CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openDialog(url, title, 'general-dialog-iframe-container', true);
      CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openedDialog = this;
      e.preventDefault();
    },
    get_options: function() {
      return this._options;
    },
    close: function() {
      this._iFrame.dialog('close');
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Unobtrusive.UnobtrusiveController

  function CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveController() {
    $(ss.bind('_init', this));
  }
  var CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveController$ = {
    _init: function() {
      this._dialogsController = new CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogsController();
      this._referenceLinkSearchButtonsController = new CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButtonsController();
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingDataGridSaveChangesItem

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridSaveChangesItem() {
    this.rowIndex = 0;
    this.changes = [];
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridSaveChangesItem$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingDataGridSaveChangesItemProperty

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridSaveChangesItemProperty() {
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridSaveChangesItemProperty$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingDataGridAddItemController

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridAddItemController(options) {
    this._options = options;
    this._init();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridAddItemController$ = {
    _initXhr: function() {
      this._xhrAdd = new GeneralJS_CS_v6.MyXmlHttpRequest();
      this._xhrAdd.add_onRequestComplete(ss.bind('_onAddRequestComplete', this));
    },
    _onAddRequestComplete: function(sender, data, status, error) {
      if (status === GeneralJS_CS_v6.XmlHttpRequestStatus.success) {
        var dataObj = data;
        if (dataObj.success) {
          this._options.listingCoreController.get_gridController().get_ajaxDataController().addRow(dataObj.newItem);
          this._options.listingCoreController.get_gridController().get_ajaxDataController().hideLoading();
        }
        else {
          this._options.listingCoreController.get_gridController().get_ajaxDataController().showError();
          alert(dataObj.errorMsg);
        }
      }
    },
    _initControls: function() {
      this._jAddItemIdField = $(this._options.listingCoreController.get_options().addFieldIdSelector);
      this._jBtnAdd = $(this._options.listingCoreController.get_options().addButtonSelector);
      this._jBtnAdd.on('click', ss.bind('_onClickButton', this));
    },
    _submitAddRequest: function() {
      var itemId = this._jAddItemIdField.val();
      if (!ss.whitespace(itemId)) {
        var data = {};
        data['add'] = itemId;
        this._options.listingCoreController.get_gridController().get_ajaxDataController().showLoading();
        this._xhrAdd.post(this._options.listingCoreController.get_options().addAjaxUrl, data, false);
      }
      else {
        alert('Please choose item to add');
      }
    },
    _onClickButton: function(e) {
      this._submitAddRequest();
      e.preventDefault();
    },
    _init: function() {
      this._initControls();
      this._initXhr();
    },
    get_options: function() {
      return this._options;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingDataGrid

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGrid(options) {
    this._saveChanges = [];
    this._options = options;
    this._init();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGrid$ = {
    _initDataGrid: function() {
      this._jGrid = $('#' + this._options.listingCoreController.get_options().containerId + ' .listing-data-grid');
      this._data = [];
      (this.get_data())['length'] = this._options.listingCoreController.get_options().totalResults;
      var gridOptions = {};
      gridOptions.forceFitColumns = true;
      gridOptions.enableCellNavigation = true;
      gridOptions.syncColumnCellResize = true;
      gridOptions.multiColumnSort = true;
      gridOptions.enableAddRow = true;
      var isEditable = !this._options.listingCoreController.get_options().isSearchingForExternalReferenceLinkData;
      gridOptions.editable = isEditable;
      gridOptions.autoEdit = false;
      gridOptions.asyncEditorLoading = false;
      var colOpts = [];
      var checkboxSelector = this._initCheckboxSelection(colOpts);
      for (var i = 0; i < this._options.listingCoreController.get_options().columns.length; i++) {
        var colOptions = this._options.listingCoreController.get_options().columns[i];
        var colOptionsColumnMetaData = colOptions.column;
        colOptionsColumnMetaData.columnOptions.cssClass = colOptionsColumnMetaData.columnOptions.cssClass || '';
        var isColumnEditable = isEditable && this._options.listingCoreController.get_options().columns[i].column.columnOptions.editor != null && !ss.whitespace(this._options.listingCoreController.get_options().saveChangesAjaxUrl);
        if (isColumnEditable) {
          this._options.listingCoreController.get_options().columns[i].column.columnOptions.editor = eval(this._options.listingCoreController.get_options().columns[i].column.columnOptions.editor.toString());
          this._options.listingCoreController.get_options().columns[i].column.columnOptions.cssClass += ' data-grid-cell-editable';
        }
        else {
          this._options.listingCoreController.get_options().columns[i].column.columnOptions.cssClass += ' data-grid-cell-not-editable';
        }
        this._initializeColumnTreeViewFormatterIfListingIsTreeView(i, colOptions);
        colOpts.push(this._options.listingCoreController.get_options().columns[i].column.columnOptions);
        this._options.listingCoreController.get_options().columns[i].column.columnOptions.toolTip = this._options.listingCoreController.get_options().columns[i].column.columnOptions.name;
      }
      this._initEditLinkColumn(colOpts);
      this._grid = new Slick.Grid(this._jGrid, this.get_data(), colOpts, gridOptions);
      var cellSelectionModel = new Slick.CellSelectionModel();
      this.get_grid().setSelectionModel(cellSelectionModel);
      this._grid.onCellChange.subscribe(ss.bind('_onCellDataChange', this));
      if (!isEditable) {
        this._grid.onDblClick.subscribe(ss.bind('_onDoubleClickRow', this));
      }
      var rowSelectionModelOptions = {};
      var rowSelectionModel = new Slick.RowSelectionModel(rowSelectionModelOptions);
      this.get_grid().setSelectionModel(rowSelectionModel);
      this.get_grid().registerPlugin(checkboxSelector);
      this._grid.onAddNewRow.subscribe(ss.bind('_onAddNewRowHandler', this));
    },
    _onAddNewRowHandler: function(e, args) {
      new CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridListingAddNewItem(this, (args)['item']);
    },
    registerDataChange: function(rowIndex, propertyName, newValue) {
      var id = this._data[rowIndex].Id;
      if (!ss.isValue(id)) {
        id = 'newItem-' + rowIndex;
        this._data[rowIndex].Id = id;
      }
      var saveChangesItem = null;
      for (var i = 0; i < this._saveChanges.length; i++) {
        if (this._saveChanges[i].itemId === id) {
          saveChangesItem = this._saveChanges[i];
          break;
        }
      }
      if (saveChangesItem == null) {
        saveChangesItem = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridSaveChangesItem();
        saveChangesItem.itemId = id;
        saveChangesItem.rowIndex = rowIndex;
        this._saveChanges[this._saveChanges.length] = saveChangesItem;
      }
      var propChanges = null;
      for (var i = 0; i < saveChangesItem.changes.length; i++) {
        if (saveChangesItem.changes[i].propertyName === propertyName) {
          propChanges = saveChangesItem.changes[i];
          break;
        }
      }
      if (propChanges == null) {
        propChanges = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridSaveChangesItemProperty();
        propChanges.propertyName = propertyName;
        saveChangesItem.changes[saveChangesItem.changes.length] = propChanges;
      }
      propChanges.value = newValue;
    },
    _onCellDataChange: function(e, args) {
      var dArgs = args;
      var rowIndex = dArgs['row'];
      var cellIndex = dArgs['cell'];
      var col = this._grid.getColumns()[cellIndex];
      var propName = col.field;
      var item = dArgs['item'];
      var value = item[propName];
      this.registerDataChange(rowIndex, propName, value);
    },
    _onClickRow: function(e, args) {
      if (!this._options.listingCoreController.get_options().isSearchingForExternalReferenceLinkData && (e.ctrlKey || e.which === 2)) {
        var args2 = args;
        this._openWindow(args2.row, true);
      }
    },
    _selectRowForExternalReferenceLinkSearch: function(rowIndex) {
      var item = this._data[rowIndex];
      var itemId = item.Id;
      CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton.updateSelectedValueOfCurrentSearchButton(itemId, item.Title);
    },
    _getItemTitleFromRowIndex: function(rowIndex) {
      var item = this._data[rowIndex];
      if (item != null) {
        return item.Title;
      }
      return null;
    },
    _getEditLinkUrlFromRowIndex: function(rowIndex) {
      var item = this._data[rowIndex];
      if (item != null) {
        var editItemUrl = this._options.listingCoreController.get_options().editItemUrlTemplate;
        editItemUrl = decodeURIComponent(editItemUrl);
        var url = ss.replaceString(editItemUrl, this._options.listingCoreController.get_options().editItemIdTag, item.Id);
        return url;
      }
      return null;
    },
    _openWindow: function(rowIndex, popup) {
      var editItemUrl = this._getEditLinkUrlFromRowIndex(rowIndex);
      if (editItemUrl != null) {
        if (popup) {
          window.open(editItemUrl);
        }
        else {
          window.location.replace(editItemUrl);
        }
      }
    },
    _onDoubleClickRow: function(e, args) {
      var rowIndex = args.row;
      if (this._options.listingCoreController.get_options().isSearchingForExternalReferenceLinkData) {
        this._selectRowForExternalReferenceLinkSearch(rowIndex);
      }
    },
    _initEditLinkColumn: function(colOpts) {
      var col = {};
      col.maxWidth = 50;
      col.cssClass = 'data-grid-edit';
      col.sortable = false;
      col.formatter = ss.bind('_editLinkFormatter', this);
      colOpts.push(col);
      return col;
    },
    _editLinkFormatter: function(row, cell, value, columndef, datacontext) {
      var url = this._getEditLinkUrlFromRowIndex(row);
      var title = this._getItemTitleFromRowIndex(row);
      return ss.format("<a href='{0}' title='Edit {1}'>Edit &raquo;</a>", url, title);
    },
    _initCheckboxSelection: function(colOpts) {
      var opts = {};
      opts.cssClass = 'data-grid-checkbox';
      var checkboxSelector = new Slick.CheckboxSelectColumn(opts);
      colOpts.push(checkboxSelector.getColumnDefinition());
      return checkboxSelector;
    },
    _initAjaxDataController: function() {
      var opts = {};
      opts.grid = this.get_grid();
      opts.data = this.get_data();
      opts.ajaxDataUrl = this._options.listingCoreController.get_options().dataAjaxUrl;
      var defaultSortColumns = [];
      for (var i = 0; i < this._options.listingCoreController.get_options().columns.length; i++) {
        var col = this._options.listingCoreController.get_options().columns[i];
        var sort = {};
        sort.id = col.column.columnOptions.id;
        if (col.column.defaultSort != null) {
          sort.sortAsc = col.column.defaultSort.sortAsc;
          sort.sortPriority = col.column.defaultSort.sortPriority;
        }
        defaultSortColumns.push(sort);
      }
      defaultSortColumns.sort(ss.bind('_sortDefaultSortColumnsByPriority', this));
      opts.defaultSortColumns = defaultSortColumns;
      this._ajaxDataController = new GeneralJS_CS_v6.SlickGridAjaxDataController(opts);
      if (this._options.listingCoreController.get_options().listingViewType === 10) {
        this._ajaxDataController.add_onDataLoaded(ss.bind('_onTreeViewLoadedFirstTimeStopAjaxLoader', this));
      }
    },
    _onTreeViewLoadedFirstTimeStopAjaxLoader: function(ajaxController, sender, data, status, error) {
      this._ajaxDataController.stopAjaxLoading();
    },
    _initializeColumnTreeViewFormatterIfListingIsTreeView: function(i, col) {
      if (!i && this._options.listingCoreController.get_options().listingViewType === 10) {
        col.column.columnOptions.formatter = ss.bind('_treeViewFormatter', this);
      }
    },
    _treeViewFormatter: function(row, cell, value, columnDef, dataContext) {
      var sValue = (value == null) ? null : value.toString();
      var dataContextObject = ss.safeCast(dataContext, Object);
      if (sValue != null) {
        sValue = sValue.replace(new RegExp('&', 'g'), '&amp;');
        sValue = sValue.replace(new RegExp('<', 'g'), '&lt;');
        sValue = sValue.replace(new RegExp('>', 'g'), '&gt;');
      }
      var amtIndent = (dataContextObject[this._options.listingCoreController.get_options().listingTreeViewIndentPropertyName] == null) ? 0 : dataContextObject[this._options.listingCoreController.get_options().listingTreeViewIndentPropertyName];
      if (amtIndent > 0) {
        var spacers = "<div class='listing-view-tree-spacers'>";
        for (var i = 0; i < amtIndent; i++) {
          spacers += "<span class='listing-view-tree-spacer'>&nbsp;</span>";
        }
        spacers += '</div>';
        sValue = ss.format("{0}<span class='listing-view-tree-text'>{1}</span>", spacers, sValue);
      }
      return sValue;
    },
    _sortDefaultSortColumnsByPriority: function(x, y) {
      if (x.sortPriority === y.sortPriority) {
        return 0;
      }
      else if (x.sortPriority < y.sortPriority) {
        return -1;
      }
      else {
        return 1;
      }
    },
    _init: function() {
      this._initDataGrid();
      this._initAjaxDataController();
    },
    get_options: function() {
      return this._options;
    },
    get_grid: function() {
      return this._grid;
    },
    get_data: function() {
      return this._data;
    },
    get_ajaxDataController: function() {
      return this._ajaxDataController;
    },
    get_saveChanges: function() {
      return this._saveChanges;
    },
    clearDataForCurrentViewport: function() {
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingDataGridListingAddNewItem

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridListingAddNewItem(grid, item) {
    this._xhr = null;
    this._rowIndex = 0;
    this._item = item;
    this._grid = grid;
    this._addItemAndSubmitRequest();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridListingAddNewItem$ = {
    _submitRequest: function(initialData) {
      this._xhr = new GeneralJS_CS_v6.MyXmlHttpRequest();
      this._xhr.add_onRequestComplete(ss.bind('_onRequestComplete', this));
      var data = {};
      data['initialData'] = GeneralJS_CS_v6.JsonUtil.stringify(initialData);
      this._xhr.post(this._grid.get_options().listingCoreController.get_options().addItemAjaxUrl, data);
    },
    _onRequestComplete: function(sender, data, status, error) {
      var errorMsg = 'Error encountered adding item.  Please try again later - if problem persists, kindly contact CasaSoft - www.casasoft.com.mt';
      if (status === GeneralJS_CS_v6.XmlHttpRequestStatus.success) {
        var requestStatus = data;
        if (requestStatus.success) {
          errorMsg = null;
          this._grid.get_data()[this._rowIndex].Id = requestStatus.id;
          this._grid.get_grid().invalidateRow(this._rowIndex);
          this._grid.get_grid().render();
        }
        else {
          errorMsg = requestStatus.errorMsg;
        }
      }
      if (!ss.whitespace(errorMsg)) {
        errorMsg += '\r\n Please refresh page to make sure you are viewing correct data.';
        alert(errorMsg);
      }
    },
    _addItemAndSubmitRequest: function() {
      var listingDataGridItem = this._item;
      var properyName = null;
      var value = null;
      var $dict1 = this._item;
      for (var $key2 in $dict1) {
        var itemKey = { key: $key2, value: $dict1[$key2] };
        properyName = itemKey.key;
        value = itemKey.value;
        break;
      }
      this._rowIndex = this._grid.get_options().listingCoreController.get_gridController().get_data().length;
      this._grid.get_grid().invalidateRow(this._rowIndex);
      this._grid.get_options().listingCoreController.get_gridController().get_data().push(listingDataGridItem);
      this._grid.get_grid().updateRowCount();
      this._grid.get_grid().render();
      var prop = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridSaveChangesItemProperty();
      prop.propertyName = properyName;
      prop.value = value;
      this._submitRequest(prop);
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingCoreController

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingCoreController(options) {
    this._options = options;
    this._init();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingCoreController$ = {
    _initGridController: function() {
      this._jContainer = $('#' + this._options.containerId);
      var opts = {};
      opts.listingCoreController = this;
      this._gridController = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGrid(opts);
    },
    _initBottomBar: function() {
      var jBottomBar = this._jContainer.find('.listing-bottom-bar-container');
      var opts = {};
      opts.grid = this.get_gridController();
      this._bottomBar = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBar(jBottomBar, opts);
    },
    _initAddItemController: function() {
      if (!ss.whitespace(this._options.addFieldIdSelector)) {
        var opts = {};
        opts.listingCoreController = this;
        this._addItemController = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridAddItemController(opts);
      }
    },
    _init: function() {
      this._initGridController();
      this._initBottomBar();
      this._initAddItemController();
    },
    get_options: function() {
      return this._options;
    },
    get_jContainer: function() {
      return this._jContainer;
    },
    get_gridController: function() {
      return this._gridController;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingBottomBarButton

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton(jButton, options) {
    this._jButton = jButton;
    this._options = options;
    this._init();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton$ = {
    _initHandlers: function() {
      this._jButton.on('click', ss.bind('onClick', this));
    },
    onClick: function(e) {
    },
    _init: function() {
      this._initHandlers();
    },
    get_options: function() {
      return this._options;
    }
  };


  // CmsGeneralJS_CS_v6.GridDataRetriever

  function CmsGeneralJS_CS_v6$GridDataRetriever(cols) {
    this._cols = 0;
    this._cols = cols;
  }
  var CmsGeneralJS_CS_v6$GridDataRetriever$ = {
    getItem: function(rowIndex) {
      var row = {};
      for (var i = 0; i < this._cols; i++) {
        row['col' + i] = 'Row ' + rowIndex + ' : ' + i;
      }
      return row;
    },
    getItemMetadata: function(rowIndex) {
      var metaData = {};
      if (rowIndex % 2 === 1) {
        var dict = {};
        metaData.cssClasses = 'row-mark';
        metaData.columns = {};
        metaData.columns[0] = {};
        metaData.columns[0].colspan = '*';
      }
      return metaData;
    },
    getLength: function() {
      return 938;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingBottomBar

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBar(container, options) {
    this._options = options;
    this._init();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBar$ = {
    _initDelete: function() {
      var jBtnDelete = this._options.grid.get_options().listingCoreController.get_jContainer().find('.bottom-menu-item-delete');
      var opts = {};
      opts.grid = this._options.grid;
      this._btnDelete = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarDeleteButton(jBtnDelete, opts);
    },
    _initSearch: function() {
      var jBtnSearch = this._options.grid.get_options().listingCoreController.get_jContainer().find('.bottom-menu-item-search');
      var opts = {};
      opts.grid = this._options.grid;
      this._btnSearch = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSearchButton(jBtnSearch, opts);
    },
    _initSave: function() {
      var jBtn = this._options.grid.get_options().listingCoreController.get_jContainer().find('.bottom-menu-item-save');
      var opts = {};
      opts.grid = this._options.grid;
      this._btnSaveChanges = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSaveButton(jBtn, opts);
    },
    _init: function() {
      this._initDelete();
      this._initSearch();
      this._initSave();
    },
    get_options: function() {
      return this._options;
    },
    get_btnDelete: function() {
      return this._btnDelete;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.InlineEditing.InlineEditingUpdateResult

  function CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingUpdateResult() {
    this.Success = false;
  }
  var CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingUpdateResult$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.CmsMenu.CmsMenuBar

  function CmsGeneralJS_CS_v6$Controllers$CmsMenu$CmsMenuBar() {
    this._init();
  }
  CmsGeneralJS_CS_v6$Controllers$CmsMenu$CmsMenuBar.get_instance = function() {
    if (CmsGeneralJS_CS_v6$Controllers$CmsMenu$CmsMenuBar._instance == null) {
      CmsGeneralJS_CS_v6$Controllers$CmsMenu$CmsMenuBar._instance = new CmsGeneralJS_CS_v6$Controllers$CmsMenu$CmsMenuBar();
    }
    return CmsGeneralJS_CS_v6$Controllers$CmsMenu$CmsMenuBar._instance;
  }
  var CmsGeneralJS_CS_v6$Controllers$CmsMenu$CmsMenuBar$ = {
    get_jBar: function() {
      return this._jBar;
    },
    _initBar: function() {
      this._jBar = $('.cms-menu-bar');
      if (!this._jBar.length) {
        this._jBar = $("<div class='cms-menu-bar'></div>");
        $('body').prepend(this.get_jBar());
      }
      $('html').addClass('cms-menu-bar-present');
    },
    _init: function() {
      this._initBar();
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.InlineEditing.InlineEditingController

  function CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingController(options) {
    this._isEditingTexts = false;
    this._options = options;
    this._init();
  }
  var CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingController$ = {
    _initOriginalValueForEditableItems: function() {
      var jNoOriginalContentAttribute = this._jEditableItems.filter(':not([' + 'data-inline-editing-original' + '])');
      for (var i = 0; i < jNoOriginalContentAttribute.length; i++) {
        var jItem = jNoOriginalContentAttribute.eq(i);
        jItem.attr('data-inline-editing-original', jItem.html());
      }
    },
    _initItems: function() {
      this._jEditableItems = $('[data-inline-editing-id]');
      this._jEditableItems.on('focus', ss.bind('_onFocusEditableItem', this));
      this._initOriginalValueForEditableItems();
      this._stopEditingTexts(false);
    },
    _onFocusEditableItem: function(e) {
      var jItem = $(e.currentTarget);
      var hasFocused = jItem.data('wasFocused');
      if (!hasFocused) {
        var origValue = jItem.attr('data-inline-editing-original');
        if (!ss.whitespace(origValue)) {
          jItem.html(origValue);
        }
        jItem.data('wasFocused', true);
      }
      jItem.attr('data-inline-editing-dirty', 'true');
    },
    _onChangeContent: function(e) {
      var jItem = $(e.currentTarget);
      var originalValue = jItem.attr('data-inline-editing-original');
      var newValue = jItem.html();
      jItem.removeAttr('data-inline-editing-dirty');
      if (!!ss.compareStrings(newValue, originalValue, false)) {
        jItem.attr('data-inline-editing-dirty', 'true');
      }
    },
    _initXhr: function() {
      this._xhrInlineEditing = new GeneralJS_CS_v6.MyXmlHttpRequest();
      this._xhrInlineEditing.add_onRequestComplete(ss.bind('_onTextsSaved', this));
    },
    _startInlineEditingIfVariableIsPresent: function() {
      if (this._isInlineEditingTextsEnabledVariablePresentInQuerystring()) {
        this._toggleEditing();
      }
    },
    _init: function() {
      this._initXhr();
      this._initButton();
      this._initItems();
      this._startInlineEditingIfVariableIsPresent();
    },
    _initButton: function() {
      this._jButton = $("<button class='cms-inline-editing'>Edit Texts</button>");
      CmsGeneralJS_CS_v6$Controllers$CmsMenu$CmsMenuBar.get_instance().get_jBar().append(this._jButton);
      this._jButton.on('click', ss.bind('_onClickEditTexts', this));
    },
    _destroyAllInlineCkEditors: function() {
      if (this._activeInlineCkEditors != null) {
        for (var i = 0; i < this._activeInlineCkEditors.length; i++) {
          this._activeInlineCkEditors[i].setReadOnly(true);
        }
      }
    },
    _enableAllInlineCkEditors: function() {
      for (var i = 0; i < this._activeInlineCkEditors.length; i++) {
        this._activeInlineCkEditors[i].setReadOnly(false);
      }
    },
    _disableAllLinksOnceInEditMode: function() {
      var jLinks = $('a');
      jLinks.on('click', ss.bind('_onClickLink', this));
    },
    _isInlineEditingTextsEnabledVariablePresentInQuerystring: function() {
      if (window.location != null) {
        var url = window.location.toString();
        if (url != null) {
          if (url.indexOf(ss.format('{0}=1', 'inlineEditingEnabled')) !== -1) {
            return true;
          }
        }
      }
      return false;
    },
    _startEditingTexts: function() {
      if (this._isInlineEditingTextsEnabledVariablePresentInQuerystring()) {
        this._isEditingTexts = true;
        if (this._activeInlineCkEditors == null) {
          var jItems = this._jEditableItems.not('span,a,button').filter('[data-inline-editing-html]');
          this._activeInlineCkEditors = [];
          for (var i = 0; i < jItems.length; i++) {
            var ckEditor = CKEDITOR.inline(jItems[i]);;
            this._activeInlineCkEditors.push(ckEditor);
          }
          this._disableAllLinksOnceInEditMode();
        }
        else {
          this._enableAllInlineCkEditors();
        }
        this._jEditableItems.attr('contenteditable', 'true');
        this._jButton.text('Stop Editing Texts');
        this._jEditableItems.addClass('inline-editing-on');
      }
      else {
        var url = window.location.toString();
        if (url.indexOf('?') === -1) {
          url += '?';
        }
        else {
          url += '&';
        }
        url += ss.format('{0}=1', 'inlineEditingEnabled');
        window.location.replace(url);
      }
    },
    _onClickLink: function(e) {
      if (this._isEditingTexts) {
        e.preventDefault();
      }
    },
    _stopEditingTexts: function(doSubmitUpdatedTexts) {
      this._jEditableItems.removeClass('inline-editing-on');
      this._isEditingTexts = false;
      this._jButton.text('Start Editing Texts');
      this._destroyAllInlineCkEditors();
      this._jEditableItems.removeAttr('contenteditable');
      if (doSubmitUpdatedTexts) {
        this._submitUpdatedTexts();
      }
    },
    _toggleEditing: function() {
      if (this._isEditingTexts) {
        this._stopEditingTexts(true);
      }
      else {
        this._startEditingTexts();
      }
    },
    _onClickEditTexts: function(e) {
      this._toggleEditing();
    },
    _submitUpdatedTexts: function() {
      this._jEditableItems.removeClass('inline-editing-error');
      GeneralJS_CS_v6.Console.log('Submit updated texts');
      var jDirtyItems = this._jEditableItems.filter('[' + 'data-inline-editing-dirty' + ']');
      var changedItems = [];
      GeneralJS_CS_v6.Console.log('jDirty Items: ' + jDirtyItems.length);
      for (var i = 0; i < jDirtyItems.length; i++) {
        var currItem = jDirtyItems.eq(i);
        var origValue = currItem.attr('data-inline-editing-original');
        var newValue = currItem.html();
        currItem.attr('data-inline-editing-original', newValue);
        if (!!ss.compareStrings(origValue, newValue, false)) {
          changedItems.push(currItem);
        }
      }
      jDirtyItems.removeAttr('data-inline-editing-dirty');
      if (changedItems != null && changedItems.length > 0) {
        var items = [];
        for (var i = 0; i < changedItems.length; i++) {
          var jItem = changedItems[i];
          var dataItem = new CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingData();
          dataItem.id = jItem.attr('data-inline-editing-id');
          dataItem.objectType = jItem.attr('data-inline-editing-type');
          dataItem.propertyName = jItem.attr('data-inline-editing-property');
          dataItem.value = jItem.html();
          items.push(dataItem);
        }
        var data = {};
        data['data'] = GeneralJS_CS_v6.JsonUtil.stringify(items);
        this._xhrInlineEditing.post(this._options.ajaxInlineEditingUrl, data);
      }
    },
    _onTextsSaved: function(sender, data, status, error) {
      var errMsg = '';
      if (status === GeneralJS_CS_v6.XmlHttpRequestStatus.success) {
        var results = data;
        if (results != null) {
          for (var i = 0; i < results.length; i++) {
            var result = results[i];
            if (!result.Result.Success) {
              errMsg += '\r- ' + result.Result.ErrorMessage;
              var id = result.Item.id;
              var jItem = this._jEditableItems.filter("[data-inline-editing-id='" + id + "']");
              jItem.addClass('inline-editing-error');
            }
          }
          if (!ss.whitespace(errMsg)) {
            errMsg = 'An error has been encountered updating the following items.  Please try again and if problem persists, contact developers.\r\r' + errMsg;
          }
        }
      }
      else {
        errMsg = 'Error encountered performing inline editing.  Please try again later and if problem persists, contact developers.';
      }
      if (!ss.whitespace(errMsg)) {
        alert(errMsg);
      }
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.InlineEditing.InlineEditingControllerParameters

  function CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingControllerParameters() {
  }
  var CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingControllerParameters$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.InlineEditing.InlineEditingData

  function CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingData() {
  }
  var CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingData$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.InlineEditing.InlineEditItemsResult

  function CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditItemsResult() {
  }
  var CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditItemsResult$ = {

  };


  // CmsGeneralJS_CS_v6.Class1

  function CmsGeneralJS_CS_v6$Class1() {
    var jGrid = $('.test-slick-grid');
    var cols = [];
    for (var i = 0; i < 5; i++) {
      var col = {};
      col.name = 'Column ' + i;
      col.field = 'col' + i;
      cols.push(col);
    }
    var data = new CmsGeneralJS_CS_v6$GridDataRetriever(cols.length);
    var opts = {};
    opts.enableCellNavigation = false;
    opts.enableColumnReorder = false;
    this._grid = new Slick.Grid(jGrid, data, cols, opts);
    this._grid.onViewportChanged.subscribe(ss.bind('_onViewportChanged', this));
    var ajaxControllerOpts = {};
    ajaxControllerOpts.ajaxDataUrl = '/en/ajax/load-data/SectionData/';
    ajaxControllerOpts.grid = this._grid;
    var ajaxController = new GeneralJS_CS_v6.SlickGridAjaxDataController(ajaxControllerOpts);
  }
  var CmsGeneralJS_CS_v6$Class1$ = {
    _onViewportChanged: function(e, args) {
      var vp = this._grid.getViewport();
      GeneralJS_CS_v6.Console.log(vp.top + ' // ' + vp.bottom);
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.CmsAjaxRequestSaveChangesStatus

  function CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestSaveChangesStatus() {
    this.success = false;
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestSaveChangesStatus$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.CmsAjaxRequestStatus

  function CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus() {
    this.success = false;
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingBottomBarReorderButton

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarReorderButton(jButton, options) {
    CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton.call(this, jButton, options);
    this._init$1();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarReorderButton$ = {
    _init$1: function() {
    },
    get_options: function() {
      return this._options;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.CmsAjaxAddRequestStatus

  function CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddRequestStatus() {
    CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus.call(this);
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddRequestStatus$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingBottomBarDeleteButton

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarDeleteButton(jButton, options) {
    this._xhrDelete$1 = new GeneralJS_CS_v6.MyXmlHttpRequest();
    this._lastDeleteIndices$1 = null;
    CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton.call(this, jButton, options);
    this._init$1();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarDeleteButton$ = {
    _initHandlers$1: function() {
    },
    _onWindowKeyDown$1: function(e) {
      if (e.which === GeneralJS_CS_v6.KeyCode.deleteKey) {
        this._deleteSelected$1();
      }
    },
    _deleteSelected$1: function() {
      var rowIndices = this._options.grid.get_grid().getSelectedRows();
      if (rowIndices.length > 0) {
        if (confirm('Are you sure you want to delete the selected items?')) {
          rowIndices.sort();
          var rowIds = [];
          for (var i = 0; i < rowIndices.length; i++) {
            var rowIndex = rowIndices[i];
            if (!!ss.isValue(this._options.grid.get_data()[rowIndex])) {
              var itemId = this._options.grid.get_data()[rowIndex].Id;
              rowIds.push(itemId);
            }
            else {
              rowIndices.splice(i, 1);
              i--;
            }
          }
          if (this._xhrDelete$1 == null) {
            this._xhrDelete$1 = new GeneralJS_CS_v6.MyXmlHttpRequest();
            this._xhrDelete$1.add_onRequestComplete(ss.bind('_onDeleteComplete$1', this));
          }
          else {
            this._xhrDelete$1.abort(false);
          }
          var data = {};
          data['ids'] = rowIds.join(',');
          this._xhrDelete$1.post(this._options.grid.get_options().listingCoreController.get_options().deleteAjaxUrl, data);
          this._options.grid.get_ajaxDataController().deleteRows(rowIndices);
        }
      }
      else {
        alert('Please select items to delete by clicking on the items or the checkboxes on the left hand side');
      }
    },
    _deleteRows$1: function(rowIndices) {
      for (var i = 0; i < rowIndices.length; i++) {
        var indexToDelete = rowIndices[i] - i;
        this._options.grid.get_data().splice(indexToDelete, 1);
      }
    },
    _onDeleteComplete$1: function(sender, data, status, error) {
      var errorMsg = 'Error encountered deleting items.  Please try again later - if problem persists, kindly contact CasaSoft - www.casasoft.com.mt';
      if (status === GeneralJS_CS_v6.XmlHttpRequestStatus.success) {
        var requestStatus = data;
        if (requestStatus.success) {
          errorMsg = null;
        }
        else {
          errorMsg = requestStatus.errorMsg;
        }
      }
      if (!ss.whitespace(errorMsg)) {
        alert(errorMsg);
      }
    },
    onClick: function(e) {
      this._deleteSelected$1();
      e.preventDefault();
    },
    _init$1: function() {
      this._initHandlers$1();
    },
    get_options: function() {
      return this._options;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingBottomBarSaveButton

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSaveButton(jButton, options) {
    this._xhr$1 = null;
    CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton.call(this, jButton, options);
    this._init$1();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSaveButton$ = {
    _initHandlers$1: function() {
    },
    _clearIdsOfELementsWhichAreBeingAdded$1: function() {
      for (var i = 0; i < this._options.grid.get_saveChanges().length; i++) {
        var saveChangesItem = this._options.grid.get_saveChanges()[i];
        if (ss.startsWith(saveChangesItem.itemId, 'newItem-')) {
          saveChangesItem.itemId = null;
        }
      }
    },
    _submitSaveChanges$1: function() {
      if (this._xhr$1 != null) {
        alert('Your previous save request is still not finished.  Please wait bit more for saving to be complete.  If this problem persists, please contact CasaSoft.');
      }
      else {
        var anyChanges = this._options.grid.get_saveChanges().length > 0;
        if (anyChanges) {
          if (confirm('Are you sure you want to save the changes performed?')) {
            this._clearIdsOfELementsWhichAreBeingAdded$1();
            var saveJson = GeneralJS_CS_v6.JsonUtil.stringify(this._options.grid.get_saveChanges());
            if (this._xhr$1 == null) {
              this._xhr$1 = new GeneralJS_CS_v6.MyXmlHttpRequest();
              this._xhr$1.add_onRequestComplete(ss.bind('_onSaveComplete$1', this));
            }
            else {
              this._xhr$1.abort(false);
            }
            var data = {};
            data['changes'] = saveJson;
            this._xhr$1.post(this._options.grid.get_options().listingCoreController.get_options().saveChangesAjaxUrl, data);
            this._lastSavedChangesCopy$1 = GeneralJS_CS_v6.ListUtil.cloneList(this._options.grid.get_saveChanges());
            this._options.grid.get_saveChanges().length = 0;
          }
        }
        else {
          alert('No changes have been made so far.  ' + '\n\nPlease make changes above by double clicking on editable fields and make sure to press ENTER once complete.  ' + '\n\nEditable fields are shown with a light blue background.');
        }
      }
    },
    _updateIdsOfLastElementsInDataGridFromSaveChangesResult$1: function(requestStatus) {
      if (requestStatus.updatedItems != null) {
        for (var i = 0; i < requestStatus.updatedItems.length; i++) {
          this._options.grid.get_data()[this._lastSavedChangesCopy$1[i].rowIndex].Id = requestStatus.updatedItems[i].Id;
        }
      }
    },
    _onSaveComplete$1: function(sender, data, status, error) {
      var errorMsg = 'Error encountered saving items.  Please try again later - if problem persists, kindly contact CasaSoft - www.casasoft.com.mt';
      if (status === GeneralJS_CS_v6.XmlHttpRequestStatus.success) {
        var requestStatus = data;
        if (requestStatus.success) {
          this._updateIdsOfLastElementsInDataGridFromSaveChangesResult$1(requestStatus);
          errorMsg = null;
        }
        else {
          errorMsg = requestStatus.errorMsg;
        }
      }
      if (!ss.whitespace(errorMsg)) {
        errorMsg += '\r\n Please refresh page to make sure you are viewing correct data.';
        alert(errorMsg);
      }
      this._xhr$1 = null;
      this._lastSavedChangesCopy$1 = null;
    },
    onClick: function(e) {
      this._submitSaveChanges$1();
      e.preventDefault();
    },
    _init$1: function() {
      this._initHandlers$1();
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.CmsAjaxAddItemRequestStatus

  function CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddItemRequestStatus() {
    CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus.call(this);
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddItemRequestStatus$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingBottomBarSearchButton

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSearchButton(jButton, options) {
    this._xhrDelete$1 = new GeneralJS_CS_v6.MyXmlHttpRequest();
    this._lastDeleteIndices$1 = null;
    CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton.call(this, jButton, options);
    this._init$1();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSearchButton$ = {
    _initHandlers$1: function() {
    },
    _openSearchForm$1: function() {
      var options = {};
      var iFrame = $(ss.format("<div class='search-dialog-iframe-container'><iframe src='{0}'></iframe></div>", this._options.grid.get_options().listingCoreController.get_options().searchAjaxUrl));
      options.width = 'auto';
      options.height = 'auto';
      options.title = 'Search';
      options.modal = true;
      this._jDialog$1 = iFrame.dialog(options);
    },
    onClick: function(e) {
      this._openSearchForm$1();
      ss.base(this, 'onClick').call(this, e);
    },
    _init$1: function() {
      this._initHandlers$1();
    },
    get_options: function() {
      return this._options;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Unobtrusive.UnobtrusiveDialogsController

  function CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogsController() {
    GeneralJS_CS_v6.UnobtrusiveBaseController.call(this, '[data-cms-dialog]');
  }
  var CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogsController$ = {
    initializeItem: function(item) {
      var opts = {};
      opts.jButton = item;
      new CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController(opts);
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Unobtrusive.UnobtrusiveReferenceLinkSearchButtonsController

  function CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButtonsController() {
    GeneralJS_CS_v6.UnobtrusiveBaseController.call(this, '[' + 'data-cms-reference-link-search-button' + ']');
  }
  var CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButtonsController$ = {
    initializeItem: function(item) {
      var opts = {};
      opts.jButton = item;
      new CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton(opts);
    }
  };


  var $exports = ss.module('CmsGeneralJS_CS_v6', null,
    {
      CollectionListingViewType: CmsGeneralJS_CS_v6$Enums$CollectionListingViewType,
      IListingDataGridItem: [ CmsGeneralJS_CS_v6$Controllers$Listing$IListingDataGridItem ],
      DataAttributes: [ CmsGeneralJS_CS_v6$Constants$DataAttributes, CmsGeneralJS_CS_v6$Constants$DataAttributes$, null ],
      UnobtrusiveReferenceLinkSearchButton: [ CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton, CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton$, null ],
      UnobtrusiveDialogController: [ CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController, CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController$, null ],
      UnobtrusiveController: [ CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveController, CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveController$, null ],
      ListingDataGridSaveChangesItem: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridSaveChangesItem, CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridSaveChangesItem$, null ],
      ListingDataGridSaveChangesItemProperty: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridSaveChangesItemProperty, CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridSaveChangesItemProperty$, null ],
      ListingDataGridAddItemController: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridAddItemController, CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridAddItemController$, null ],
      ListingDataGrid: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGrid, CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGrid$, null ],
      ListingDataGridListingAddNewItem: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridListingAddNewItem, CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridListingAddNewItem$, null ],
      ListingCoreController: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingCoreController, CmsGeneralJS_CS_v6$Controllers$Listing$ListingCoreController$, null ],
      ListingBottomBarButton: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton$, null ],
      GridDataRetriever: [ CmsGeneralJS_CS_v6$GridDataRetriever, CmsGeneralJS_CS_v6$GridDataRetriever$, null, Object ],
      ListingBottomBar: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBar, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBar$, null ],
      InlineEditingUpdateResult: [ CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingUpdateResult, CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingUpdateResult$, null ],
      CmsMenuBar: [ CmsGeneralJS_CS_v6$Controllers$CmsMenu$CmsMenuBar, CmsGeneralJS_CS_v6$Controllers$CmsMenu$CmsMenuBar$, null ],
      InlineEditingController: [ CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingController, CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingController$, null ],
      InlineEditingControllerParameters: [ CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingControllerParameters, CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingControllerParameters$, null ],
      InlineEditingData: [ CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingData, CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditingData$, null ],
      InlineEditItemsResult: [ CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditItemsResult, CmsGeneralJS_CS_v6$Controllers$InlineEditing$InlineEditItemsResult$, null ],
      Class1: [ CmsGeneralJS_CS_v6$Class1, CmsGeneralJS_CS_v6$Class1$, null ],
      CmsAjaxRequestSaveChangesStatus: [ CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestSaveChangesStatus, CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestSaveChangesStatus$, null ],
      CmsAjaxRequestStatus: [ CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus, CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus$, null ],
      ListingBottomBarReorderButton: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarReorderButton, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarReorderButton$, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton ],
      CmsAjaxAddRequestStatus: [ CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddRequestStatus, CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddRequestStatus$, CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus ],
      ListingBottomBarDeleteButton: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarDeleteButton, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarDeleteButton$, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton ],
      ListingBottomBarSaveButton: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSaveButton, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSaveButton$, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton ],
      CmsAjaxAddItemRequestStatus: [ CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddItemRequestStatus, CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddItemRequestStatus$, CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus ],
      ListingBottomBarSearchButton: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSearchButton, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSearchButton$, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton ],
      UnobtrusiveDialogsController: [ CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogsController, CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogsController$, GeneralJS_CS_v6.UnobtrusiveBaseController ],
      UnobtrusiveReferenceLinkSearchButtonsController: [ CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButtonsController, CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButtonsController$, GeneralJS_CS_v6.UnobtrusiveBaseController ]
    });

  CmsGeneralJS_CS_v6$Constants$DataAttributes.dialog = 'data-cms-dialog';
  CmsGeneralJS_CS_v6$Constants$DataAttributes.dialogTitle = 'data-cms-dialog-title';
  CmsGeneralJS_CS_v6$Constants$DataAttributes.referenceLinkSearchButton = 'data-cms-reference-link-search-button';
  CmsGeneralJS_CS_v6$Constants$DataAttributes.referenceLinkSearchTitle = 'data-cms-reference-link-search-title';
  CmsGeneralJS_CS_v6$Constants$DataAttributes.referenceLinkSearchTextElementSelector = 'data-cms-reference-link-search-text';
  CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGrid.addItemIdPrefix = 'newItem-';


  $global.CmsGeneralJS_CS_v6 = $exports;
