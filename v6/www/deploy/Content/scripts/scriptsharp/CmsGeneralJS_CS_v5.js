/*! CmsGeneralJS_CS_v6.js 1.0.0.0
 * 
 */

"use strict";

  var $global = this;
 
  // CmsGeneralJS_CS_v6.Controllers.Listing.IListingDataGridItem

  function CmsGeneralJS_CS_v6$Controllers$Listing$IListingDataGridItem() { }


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingDataGridAddItemController

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridAddItemController(options) {
    this._options = options;
    this._init();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridAddItemController$ = {
    _initXhr: function() {
      this._xhrAdd = new GeneralJS_CS_v6.MyXmlHttpRequest();
      this._xhrAdd.add_onRequestComplete(ss.bind('_onAddRequestComplete', this));
    },
    _onAddRequestComplete: function(sender, data, status, error) {
      if (status === GeneralJS_CS_v6.XmlHttpRequestStatus.success) {
        var dataObj = data;
        if (dataObj.success) {
          this._options.listingCoreController.get_gridController().get_ajaxDataController().addRow(dataObj.newItem);
          this._options.listingCoreController.get_gridController().get_ajaxDataController().hideLoading();
        }
        else {
          this._options.listingCoreController.get_gridController().get_ajaxDataController().showError();
          alert(dataObj.errorMsg);
        }
      }
    },
    _initControls: function() {
      this._jAddItemIdField = $(this._options.listingCoreController.get_options().addFieldIdSelector);
      this._jBtnAdd = $(this._options.listingCoreController.get_options().addButtonSelector);
      this._jBtnAdd.on('click', ss.bind('_onClickButton', this));
    },
    _submitAddRequest: function() {
      var itemId = this._jAddItemIdField.val();
      if (!ss.whitespace(itemId)) {
        var data = {};
        data['add'] = itemId;
        this._options.listingCoreController.get_gridController().get_ajaxDataController().showLoading();
        this._xhrAdd.post(this._options.listingCoreController.get_options().addAjaxUrl, data, false);
      }
      else {
        alert('Please choose item to add');
      }
    },
    _onClickButton: function(e) {
      this._submitAddRequest();
      e.preventDefault();
    },
    _init: function() {
      this._initControls();
      this._initXhr();
    },
    get_options: function() {
      return this._options;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Unobtrusive.UnobtrusiveController

  function CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveController() {
    $(ss.bind('_init', this));
  }
  var CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveController$ = {
    _init: function() {
      this._dialogsController = new CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogsController();
      this._referenceLinkSearchButtonsController = new CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButtonsController();
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingCoreController

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingCoreController(options) {
    this._options = options;
    this._init();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingCoreController$ = {
    _initGridController: function() {
      this._jContainer = $('#' + this._options.containerId);
      var opts = {};
      opts.listingCoreController = this;
      this._gridController = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGrid(opts);
    },
    _initBottomBar: function() {
      var jBottomBar = this._jContainer.find('.listing-bottom-bar-container');
      var opts = {};
      opts.grid = this.get_gridController();
      this._bottomBar = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBar(jBottomBar, opts);
    },
    _initAddItemController: function() {
      if (!ss.whitespace(this._options.addFieldIdSelector)) {
        var opts = {};
        opts.listingCoreController = this;
        this._addItemController = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridAddItemController(opts);
      }
    },
    _init: function() {
      this._initGridController();
      this._initBottomBar();
      this._initAddItemController();
    },
    get_options: function() {
      return this._options;
    },
    get_jContainer: function() {
      return this._jContainer;
    },
    get_gridController: function() {
      return this._gridController;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingDataGrid

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGrid(options) {
    this._options = options;
    this._init();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGrid$ = {
    _initDataGrid: function() {
      this._jGrid = $('#' + this._options.listingCoreController.get_options().containerId + ' .listing-data-grid');
      this._data = [];
      (this.get_data())['length'] = this._options.listingCoreController.get_options().totalResults;
      var gridOptions = {};
      gridOptions.forceFitColumns = true;
      gridOptions.enableCellNavigation = true;
      gridOptions.syncColumnCellResize = true;
      gridOptions.multiColumnSort = true;
      var colOpts = [];
      var checkboxSelector = this._initCheckboxSelection(colOpts);
      for (var i = 0; i < this._options.listingCoreController.get_options().columns.length; i++) {
        colOpts.push(this._options.listingCoreController.get_options().columns[i].column.columnOptions);
      }
      this._grid = new Slick.Grid(this._jGrid, this.get_data(), colOpts, gridOptions);
      this._grid.onClick.subscribe(ss.bind('_onClickRow', this));
      this._grid.onDblClick.subscribe(ss.bind('_onDoubleClickRow', this));
      var rowSelectionModelOptions = {};
      var rowSelectionModel = new Slick.RowSelectionModel(rowSelectionModelOptions);
      this.get_grid().setSelectionModel(rowSelectionModel);
      this.get_grid().registerPlugin(checkboxSelector);
    },
    _onClickRow: function(e, args) {
      if (!this._options.listingCoreController.get_options().isSearchingForExternalReferenceLinkData && (e.ctrlKey || e.which === 2)) {
        var args2 = args;
        this._openWindow(args2.row, true);
      }
    },
    _selectRowForExternalReferenceLinkSearch: function(rowIndex) {
      var item = this._data[rowIndex];
      var itemId = item.Id;
      CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton.updateSelectedValueOfCurrentSearchButton(itemId, item.Title);
    },
    _openWindow: function(rowIndex, popup) {
      var item = this._data[rowIndex];
      if (item != null) {
        var editItemUrl = this._options.listingCoreController.get_options().editItemUrlTemplate;
        editItemUrl = decodeURIComponent(editItemUrl);
        var url = ss.replaceString(editItemUrl, this._options.listingCoreController.get_options().editItemIdTag, item.Id);
        if (popup) {
          window.open(url);
        }
        else {
          window.location.replace(url);
        }
      }
    },
    _onDoubleClickRow: function(e, args) {
      var rowIndex = args.row;
      if (this._options.listingCoreController.get_options().isSearchingForExternalReferenceLinkData) {
        this._selectRowForExternalReferenceLinkSearch(rowIndex);
      }
      else {
        this._openWindow(rowIndex, false);
      }
    },
    _initCheckboxSelection: function(colOpts) {
      var opts = {};
      opts.cssClass = 'data-grid-checkbox';
      var checkboxSelector = new Slick.CheckboxSelectColumn(opts);
      colOpts.push(checkboxSelector.getColumnDefinition());
      return checkboxSelector;
    },
    _initAjaxDataController: function() {
      var opts = {};
      opts.grid = this.get_grid();
      opts.data = this.get_data();
      opts.ajaxDataUrl = this._options.listingCoreController.get_options().dataAjaxUrl;
      var defaultSortColumns = [];
      for (var i = 0; i < this._options.listingCoreController.get_options().columns.length; i++) {
        var col = this._options.listingCoreController.get_options().columns[i];
        var sort = {};
        sort.id = col.column.columnOptions.id;
        if (col.column.defaultSort != null) {
          sort.sortAsc = col.column.defaultSort.sortAsc;
          sort.sortPriority = col.column.defaultSort.sortPriority;
        }
        defaultSortColumns.push(sort);
      }
      defaultSortColumns.sort(ss.bind('_sortDefaultSortColumnsByPriority', this));
      opts.defaultSortColumns = defaultSortColumns;
      this._ajaxDataController = new GeneralJS_CS_v6.SlickGridAjaxDataController(opts);
    },
    _sortDefaultSortColumnsByPriority: function(x, y) {
      if (x.sortPriority === y.sortPriority) {
        return 0;
      }
      else if (x.sortPriority < y.sortPriority) {
        return -1;
      }
      else {
        return 1;
      }
    },
    _init: function() {
      this._initDataGrid();
      this._initAjaxDataController();
    },
    get_options: function() {
      return this._options;
    },
    get_grid: function() {
      return this._grid;
    },
    get_data: function() {
      return this._data;
    },
    get_ajaxDataController: function() {
      return this._ajaxDataController;
    },
    clearDataForCurrentViewport: function() {
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Unobtrusive.UnobtrusiveReferenceLinkSearchButton

  function CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton(options) {
    this._options = options;
    this._init();
  }
  CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton.updateSelectedValueOfCurrentSearchButton = function(selectedValue, selectedText) {
    var currentButton = window.top['CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton'].currentClickedSearchButton;
    if (currentButton != null) {
      currentButton.updateSelectedValueInHiddenFieldAndCloseDialog(selectedValue, selectedText);
    }
  }
  var CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton$ = {
    _init: function() {
      var jHiddenFieldId = this._options.jButton.attr('data-cms-reference-link-search-button');
      this._jHiddenField = $('#' + jHiddenFieldId);
      this._jText = $(this._options.jButton.attr('data-cms-reference-link-search-text'));
      this._options.jButton.on('click', ss.bind('_onClickButton', this));
    },
    _onClickButton: function(e) {
      this._openSearch();
      e.preventDefault();
    },
    _openSearch: function() {
      var searchUrl = this._options.jButton.attr('href');
      this._searchDialog = CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openDialog(searchUrl, 'Search', 'reference-link-search-item-dialog', true);
      CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton.currentClickedSearchButton = this;
      this._searchDialog.on('dialogclose', ss.bind('_onCloseDialog', this));
    },
    _onCloseDialog: function(e) {
      if (CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton.currentClickedSearchButton === this) {
        CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton.currentClickedSearchButton = null;
      }
    },
    get_options: function() {
      return this._options;
    },
    updateSelectedValueInHiddenFieldAndCloseDialog: function(value, selectedText) {
      this._jHiddenField.val(value);
      this._jText.text(selectedText);
      if (this._searchDialog != null) {
        this._searchDialog.dialog('destroy');
        this._searchDialog = null;
      }
    }
  };


  // CmsGeneralJS_CS_v6.GridDataRetriever

  function CmsGeneralJS_CS_v6$GridDataRetriever(cols) {
    this._cols = 0;
    this._cols = cols;
  }
  var CmsGeneralJS_CS_v6$GridDataRetriever$ = {
    getItem: function(rowIndex) {
      var row = {};
      for (var i = 0; i < this._cols; i++) {
        row['col' + i] = 'Row ' + rowIndex + ' : ' + i;
      }
      return row;
    },
    getItemMetadata: function(rowIndex) {
      var metaData = {};
      if (rowIndex % 2 === 1) {
        var dict = {};
        metaData.cssClasses = 'row-mark';
        metaData.columns = {};
        metaData.columns[0] = {};
        metaData.columns[0].colspan = '*';
      }
      return metaData;
    },
    getLength: function() {
      return 938;
    }
  };


  // CmsGeneralJS_CS_v6.Class1

  function CmsGeneralJS_CS_v6$Class1() {
    var jGrid = $('.test-slick-grid');
    var cols = [];
    for (var i = 0; i < 5; i++) {
      var col = {};
      col.name = 'Column ' + i;
      col.field = 'col' + i;
      cols.push(col);
    }
    var data = new CmsGeneralJS_CS_v6$GridDataRetriever(cols.length);
    var opts = {};
    opts.enableCellNavigation = false;
    opts.enableColumnReorder = false;
    this._grid = new Slick.Grid(jGrid, data, cols, opts);
    this._grid.onViewportChanged.subscribe(ss.bind('_onViewportChanged', this));
    var ajaxControllerOpts = {};
    ajaxControllerOpts.ajaxDataUrl = '/en/ajax/load-data/SectionData/';
    ajaxControllerOpts.grid = this._grid;
    var ajaxController = new GeneralJS_CS_v6.SlickGridAjaxDataController(ajaxControllerOpts);
  }
  var CmsGeneralJS_CS_v6$Class1$ = {
    _onViewportChanged: function(e, args) {
      var vp = this._grid.getViewport();
      GeneralJS_CS_v6.Console.log(vp.top + ' // ' + vp.bottom);
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Unobtrusive.UnobtrusiveDialogController

  function CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController(options) {
    this._options = options;
    this._init();
  }
  CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openDialog = function(url, title, cssClass, modal) {
    var options = {};
    var iFrame = $(ss.format("<div class='{0}'><iframe src='{1}'></iframe></div>", cssClass, url));
    options.width = 'auto';
    options.height = 'auto';
    options.title = title;
    options.modal = modal;
    iFrame.dialog(options);
    return iFrame;
  }
  CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.closeCurrentOpenedDialog = function() {
    if (CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openedDialog != null) {
      CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openedDialog.close();
    }
  }
  var CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController$ = {
    _init: function() {
      this._options.jButton.on('click', ss.bind('_onClickButton', this));
    },
    _onClickButton: function(e) {
      var url = this._options.jButton.attr('href');
      var title = this._options.jButton.attr('data-cms-dialog-title') || 'Edit Field Meta Data';
      this._iFrame = CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openDialog(url, title, 'general-dialog-iframe-container', true);
      CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController.openedDialog = this;
      e.preventDefault();
    },
    get_options: function() {
      return this._options;
    },
    close: function() {
      this._iFrame.dialog('close');
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingBottomBar

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBar(container, options) {
    this._options = options;
    this._init();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBar$ = {
    _initDelete: function() {
      var jBtnDelete = this._options.grid.get_options().listingCoreController.get_jContainer().find('.bottom-menu-item-delete');
      var opts = {};
      opts.grid = this._options.grid;
      this._btnDelete = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarDeleteButton(jBtnDelete, opts);
    },
    _initSearch: function() {
      var jBtnSearch = this._options.grid.get_options().listingCoreController.get_jContainer().find('.bottom-menu-item-search');
      var opts = {};
      opts.grid = this._options.grid;
      this._btnSearch = new CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSearchButton(jBtnSearch, opts);
    },
    _init: function() {
      this._initDelete();
      this._initSearch();
    },
    get_options: function() {
      return this._options;
    },
    get_btnDelete: function() {
      return this._btnDelete;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.CmsAjaxRequestStatus

  function CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus() {
    this.success = false;
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus$ = {

  };


  // CmsGeneralJS_CS_v6.Constants.DataAttributes

  function CmsGeneralJS_CS_v6$Constants$DataAttributes() {
  }
  var CmsGeneralJS_CS_v6$Constants$DataAttributes$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingBottomBarButton

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton(jButton, options) {
    this._jButton = jButton;
    this._options = options;
    this._init();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton$ = {
    _initHandlers: function() {
      this._jButton.on('click', ss.bind('onClick', this));
    },
    onClick: function(e) {
    },
    _init: function() {
      this._initHandlers();
    },
    get_options: function() {
      return this._options;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.CmsAjaxAddRequestStatus

  function CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddRequestStatus() {
    CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus.call(this);
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddRequestStatus$ = {

  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingBottomBarDeleteButton

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarDeleteButton(jButton, options) {
    this._xhrDelete$1 = new GeneralJS_CS_v6.MyXmlHttpRequest();
    this._lastDeleteIndices$1 = null;
    CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton.call(this, jButton, options);
    this._init$1();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarDeleteButton$ = {
    _initHandlers$1: function() {
    },
    _onWindowKeyDown$1: function(e) {
      if (e.which === GeneralJS_CS_v6.KeyCode.deleteKey) {
        this._deleteSelected$1();
      }
    },
    _deleteSelected$1: function() {
      var rowIndices = this._options.grid.get_grid().getSelectedRows();
      if (rowIndices.length > 0) {
        if (confirm('Are you sure you want to delete the selected items?')) {
          rowIndices.sort();
          var rowIds = [];
          for (var i = 0; i < rowIndices.length; i++) {
            var rowIndex = rowIndices[i];
            if (!!ss.isValue(this._options.grid.get_data()[rowIndex])) {
              var itemId = this._options.grid.get_data()[rowIndex].Id;
              rowIds.push(itemId);
            }
            else {
              rowIndices.splice(i, 1);
              i--;
            }
          }
          if (this._xhrDelete$1 == null) {
            this._xhrDelete$1 = new GeneralJS_CS_v6.MyXmlHttpRequest();
            this._xhrDelete$1.add_onRequestComplete(ss.bind('_onDeleteComplete$1', this));
          }
          else {
            this._xhrDelete$1.abort(false);
          }
          var data = {};
          data['ids'] = rowIds.join(',');
          this._xhrDelete$1.post(this._options.grid.get_options().listingCoreController.get_options().deleteAjaxUrl, data);
          this._options.grid.get_ajaxDataController().deleteRows(rowIndices);
        }
      }
      else {
        alert('Please select items to delete by clicking on the items or the checkboxes on the left hand side');
      }
    },
    _deleteRows$1: function(rowIndices) {
      for (var i = 0; i < rowIndices.length; i++) {
        var indexToDelete = rowIndices[i] - i;
        this._options.grid.get_data().splice(indexToDelete, 1);
      }
    },
    _onDeleteComplete$1: function(sender, data, status, error) {
      var errorMsg = 'Error encountered deleting items.  Please try again later - if problem persists, kindly contact CasaSoft - www.casasoft.com.mt';
      if (status === GeneralJS_CS_v6.XmlHttpRequestStatus.success) {
        var requestStatus = data;
        if (requestStatus.success) {
          errorMsg = null;
        }
        else {
          errorMsg = requestStatus.errorMsg;
        }
      }
      if (!ss.whitespace(errorMsg)) {
        alert(errorMsg);
      }
    },
    onClick: function(e) {
      this._deleteSelected$1();
      ss.base(this, 'onClick').call(this, e);
    },
    _init$1: function() {
      this._initHandlers$1();
    },
    get_options: function() {
      return this._options;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingBottomBarSearchButton

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSearchButton(jButton, options) {
    this._xhrDelete$1 = new GeneralJS_CS_v6.MyXmlHttpRequest();
    this._lastDeleteIndices$1 = null;
    CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton.call(this, jButton, options);
    this._init$1();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSearchButton$ = {
    _initHandlers$1: function() {
    },
    _openSearchForm$1: function() {
      var options = {};
      var iFrame = $(ss.format("<div class='search-dialog-iframe-container'><iframe src='{0}'></iframe></div>", this._options.grid.get_options().listingCoreController.get_options().searchAjaxUrl));
      options.width = 'auto';
      options.height = 'auto';
      options.title = 'Search';
      options.modal = true;
      this._jDialog$1 = iFrame.dialog(options);
    },
    onClick: function(e) {
      this._openSearchForm$1();
      ss.base(this, 'onClick').call(this, e);
    },
    _init$1: function() {
      this._initHandlers$1();
    },
    get_options: function() {
      return this._options;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Listing.ListingBottomBarReorderButton

  function CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarReorderButton(jButton, options) {
    CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton.call(this, jButton, options);
    this._init$1();
  }
  var CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarReorderButton$ = {
    _init$1: function() {
    },
    get_options: function() {
      return this._options;
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Unobtrusive.UnobtrusiveDialogsController

  function CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogsController() {
    GeneralJS_CS_v6.UnobtrusiveBaseController.call(this, '[data-cms-dialog]');
  }
  var CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogsController$ = {
    initializeItem: function(item) {
      var opts = {};
      opts.jButton = item;
      new CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController(opts);
    }
  };


  // CmsGeneralJS_CS_v6.Controllers.Unobtrusive.UnobtrusiveReferenceLinkSearchButtonsController

  function CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButtonsController() {
    GeneralJS_CS_v6.UnobtrusiveBaseController.call(this, '[' + 'data-cms-reference-link-search-button' + ']');
  }
  var CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButtonsController$ = {
    initializeItem: function(item) {
      var opts = {};
      opts.jButton = item;
      new CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton(opts);
    }
  };


  var $exports = ss.module('CmsGeneralJS_CS_v6', null,
    {
      IListingDataGridItem: [ CmsGeneralJS_CS_v6$Controllers$Listing$IListingDataGridItem ],
      ListingDataGridAddItemController: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridAddItemController, CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGridAddItemController$, null ],
      UnobtrusiveController: [ CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveController, CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveController$, null ],
      ListingCoreController: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingCoreController, CmsGeneralJS_CS_v6$Controllers$Listing$ListingCoreController$, null ],
      ListingDataGrid: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGrid, CmsGeneralJS_CS_v6$Controllers$Listing$ListingDataGrid$, null ],
      UnobtrusiveReferenceLinkSearchButton: [ CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton, CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButton$, null ],
      GridDataRetriever: [ CmsGeneralJS_CS_v6$GridDataRetriever, CmsGeneralJS_CS_v6$GridDataRetriever$, null, Object ],
      Class1: [ CmsGeneralJS_CS_v6$Class1, CmsGeneralJS_CS_v6$Class1$, null ],
      UnobtrusiveDialogController: [ CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController, CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogController$, null ],
      ListingBottomBar: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBar, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBar$, null ],
      CmsAjaxRequestStatus: [ CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus, CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus$, null ],
      DataAttributes: [ CmsGeneralJS_CS_v6$Constants$DataAttributes, CmsGeneralJS_CS_v6$Constants$DataAttributes$, null ],
      ListingBottomBarButton: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton$, null ],
      CmsAjaxAddRequestStatus: [ CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddRequestStatus, CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxAddRequestStatus$, CmsGeneralJS_CS_v6$Controllers$Listing$CmsAjaxRequestStatus ],
      ListingBottomBarDeleteButton: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarDeleteButton, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarDeleteButton$, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton ],
      ListingBottomBarSearchButton: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSearchButton, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarSearchButton$, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton ],
      ListingBottomBarReorderButton: [ CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarReorderButton, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarReorderButton$, CmsGeneralJS_CS_v6$Controllers$Listing$ListingBottomBarButton ],
      UnobtrusiveDialogsController: [ CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogsController, CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveDialogsController$, GeneralJS_CS_v6.UnobtrusiveBaseController ],
      UnobtrusiveReferenceLinkSearchButtonsController: [ CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButtonsController, CmsGeneralJS_CS_v6$Controllers$Unobtrusive$UnobtrusiveReferenceLinkSearchButtonsController$, GeneralJS_CS_v6.UnobtrusiveBaseController ]
    });

  CmsGeneralJS_CS_v6$Constants$DataAttributes.dialog = 'data-cms-dialog';
  CmsGeneralJS_CS_v6$Constants$DataAttributes.dialogTitle = 'data-cms-dialog-title';
  CmsGeneralJS_CS_v6$Constants$DataAttributes.referenceLinkSearchButton = 'data-cms-reference-link-search-button';
  CmsGeneralJS_CS_v6$Constants$DataAttributes.referenceLinkSearchTitle = 'data-cms-reference-link-search-title';
  CmsGeneralJS_CS_v6$Constants$DataAttributes.referenceLinkSearchTextElementSelector = 'data-cms-reference-link-search-text';


  $global.CmsGeneralJS_CS_v6 = $exports;
