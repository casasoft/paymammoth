/*! GeneralJS_CS_v6.js 1.0.0.0
 * 
 */

"use strict";

  var $global = this;
 
  // GeneralJS_CS_v6.Wrappers.jQueryPlugins.QTip.QTipStyle

  var GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTipStyle = {
    defaultStyle: 1, 
    plain: 2, 
    light: 4, 
    dark: 8, 
    red: 16, 
    green: 32, 
    blue: 64, 
    shadow: 128, 
    rounded: 256, 
    youTube: 512, 
    jTools: 1024, 
    clueTip: 2048, 
    tipped: 4096, 
    tipsy: 8192
  };


  // GeneralJS_CS_v6.Enums.XmlHttpRequestStatus

  var GeneralJS_CS_v6$Enums$XmlHttpRequestStatus = {
    success: 0, 
    notModified: 1, 
    error: 2, 
    timeout: 3, 
    abort: 4, 
    parserError: 5
  };


  // GeneralJS_CS_v6.Enums.JQueryXmlHttpRequestDataType

  var GeneralJS_CS_v6$Enums$JQueryXmlHttpRequestDataType = {
    xml: 0, 
    json: 1, 
    script: 2, 
    html: 3
  };


  // GeneralJS_CS_v6.Enums.KeyCode

  var GeneralJS_CS_v6$Enums$KeyCode = {
    backSpace: 8, 
    tab: 9, 
    enter: 13, 
    shift: 16, 
    ctrl: 17, 
    alt: 18, 
    pauseBreak: 19, 
    capsLock: 20, 
    escape: 27, 
    pageUp: 33, 
    pageDown: 34, 
    end: 35, 
    home: 36, 
    leftArrow: 37, 
    upArrow: 38, 
    rightArrow: 39, 
    downArrow: 40, 
    insert: 45, 
    deleteKey: 46, 
    digit_0: 48, 
    digit_1: 49, 
    digit_2: 50, 
    digit_3: 51, 
    digit_4: 52, 
    digit_5: 53, 
    digit_6: 54, 
    digit_7: 55, 
    digit_8: 56, 
    digit_9: 57, 
    a: 65, 
    b: 66, 
    c: 67, 
    d: 68, 
    e: 69, 
    f: 70, 
    g: 71, 
    h: 72, 
    i: 73, 
    j: 74, 
    k: 75, 
    l: 76, 
    m: 77, 
    n: 78, 
    o: 79, 
    p: 80, 
    q: 81, 
    r: 82, 
    s: 83, 
    t: 84, 
    u: 85, 
    v: 86, 
    w: 87, 
    x: 88, 
    y: 89, 
    z: 90, 
    leftWindowKey: 91, 
    rightWindowKey: 92, 
    selectKey: 93, 
    numpad_0: 96, 
    numpad_1: 97, 
    numpad_2: 98, 
    numpad_3: 99, 
    numpad_4: 100, 
    numpad_5: 101, 
    numpad_6: 102, 
    numpad_7: 103, 
    numpad_8: 104, 
    numpad_9: 105, 
    multiply: 106, 
    add: 107, 
    subtract: 109, 
    decimalPoint: 110, 
    divide: 111, 
    f1: 112, 
    f2: 113, 
    f3: 114, 
    f4: 115, 
    f5: 116, 
    f6: 117, 
    f7: 118, 
    f8: 119, 
    f9: 120, 
    f10: 121, 
    f11: 122, 
    f12: 123, 
    numLock: 144, 
    scrollLock: 145, 
    semiColon: 186, 
    equalSign: 187, 
    comma: 188, 
    dash: 189, 
    period: 190, 
    forwardSlash: 191, 
    graveAccent: 192, 
    openBracket: 219, 
    backSlash: 220, 
    closeBraket: 221, 
    singleQuote: 222
  };


  // GeneralJS_CS_v6.Enums.JQueryEventType

  var GeneralJS_CS_v6$Enums$JQueryEventType = {
    blur: 0, 
    focus: 1, 
    load: 2, 
    resize: 3, 
    scroll: 4, 
    unload: 5, 
    beforeUnload: 6, 
    click: 7, 
    doubleClick: 8, 
    mouseDown: 9, 
    mouseUp: 10, 
    mouseMove: 11, 
    mouseOver: 12, 
    mouseOut: 13, 
    mouseEnter: 14, 
    mouseLeave: 15, 
    change: 16, 
    select: 17, 
    submit: 18, 
    keyDown: 19, 
    keyPress: 20, 
    keyUp: 21, 
    error: 22, 
    ready: 23
  };


  // GeneralJS_CS_v6.Enums.JQueryPseudoSelector

  var GeneralJS_CS_v6$Enums$JQueryPseudoSelector = {
    animated: 0, 
    button: 1, 
    checkbox: 2, 
    checked: 3, 
    disabled: 4, 
    empty: 5, 
    enabled: 6, 
    even: 7, 
    file: 8, 
    firstChild: 9, 
    first: 10, 
    focus: 11, 
    header: 12, 
    hidden: 13, 
    image: 14, 
    input: 15, 
    lastChild: 16, 
    last: 17, 
    odd: 18, 
    parent: 19, 
    password: 20, 
    radio: 21, 
    reset: 22, 
    selected: 23, 
    submit: 24, 
    text: 25, 
    visible: 26
  };


  // GeneralJS_CS_v6.Enums.JQueryPosition

  var GeneralJS_CS_v6$Enums$JQueryPosition = {
    center: 0, 
    top: 1, 
    left: 2, 
    right: 3, 
    bottom: 4, 
    none: 5
  };


  // GeneralJS_CS_v6.Enums.JQueryUiEffect

  var GeneralJS_CS_v6$Enums$JQueryUiEffect = {
    blind: 0, 
    bounce: 1, 
    clip: 2, 
    drop: 3, 
    explode: 4, 
    fold: 5, 
    highlight: 6, 
    puff: 7, 
    pulsate: 8, 
    scale: 9, 
    shake: 10, 
    size: 11, 
    slide: 12, 
    transfer: 13, 
    fade: 14, 
    none: 9999
  };


  // GeneralJS_CS_v6.Enums.JQueryEasing

  var GeneralJS_CS_v6$Enums$JQueryEasing = {
    linear: 0, 
    swing: 10
  };


  // GeneralJS_CS_v6.Interfaces.IDestroyable

  function GeneralJS_CS_v6$Interfaces$IDestroyable() { }


  // GeneralJS_CS_v6.Console

  function GeneralJS_CS_v6$Console() {
  }
  GeneralJS_CS_v6$Console.log = function(obj) {
    console.log(obj);;
  }
  var GeneralJS_CS_v6$Console$ = {

  };


  // GeneralJS_CS_v6.Util.DateUtil

  function GeneralJS_CS_v6$Util$DateUtil() {
  }
  GeneralJS_CS_v6$Util$DateUtil.formatTime = function(date, includeSeconds) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    var sSeconds = seconds.toString();
    if (hours < 10) {
      sHours = '0' + hours;
    }
    if (minutes < 10) {
      sMinutes = '0' + minutes;
    }
    if (seconds < 10) {
      sSeconds = '0' + seconds;
    }
    var time = sHours + ':' + sMinutes;
    if (includeSeconds) {
      time += ':' + sSeconds;
    }
    return time;
  }
  GeneralJS_CS_v6$Util$DateUtil.dateDiffMilliseconds = function(dateFrom, dateTo) {
    var dFrom = (dateFrom < dateTo) ? dateFrom : dateTo;
    var dTo = (dateFrom > dateTo) ? dateFrom : dateTo;
    return dTo - dFrom;
  }
  GeneralJS_CS_v6$Util$DateUtil.dateDiffSeconds = function(dateFrom, dateTo) {
    return GeneralJS_CS_v6$Util$DateUtil.dateDiffMilliseconds(dateFrom, dateTo) / 1000;
  }
  GeneralJS_CS_v6$Util$DateUtil.dateDiffMinutes = function(dateFrom, dateTo) {
    return GeneralJS_CS_v6$Util$DateUtil.dateDiffSeconds(dateFrom, dateTo) / 60;
  }
  GeneralJS_CS_v6$Util$DateUtil.addMinutes = function(date, minutes) {
    return new Date(date.getTime() + ss.truncate((minutes * 60 * 1000)));
  }
  var GeneralJS_CS_v6$Util$DateUtil$ = {

  };


  // GeneralJS_CS_v6.Util.DomUtil

  function GeneralJS_CS_v6$Util$DomUtil() {
  }
  GeneralJS_CS_v6$Util$DomUtil.getWindowElement = function() {
    return $(window.self);
  }
  GeneralJS_CS_v6$Util$DomUtil.getOuterHtml = function(element) {
    return $('<p>').append(element.eq(0).clone()).html();
  }
  GeneralJS_CS_v6$Util$DomUtil.hasAttribute = function(item, attribute) {
    var attrValue = item.attr(attribute);
    return !(attrValue === undefined) && attrValue !== false;
  }
  var GeneralJS_CS_v6$Util$DomUtil$ = {

  };


  // GeneralJS_CS_v6.Util.FunctionUtil

  function GeneralJS_CS_v6$Util$FunctionUtil() {
  }
  GeneralJS_CS_v6$Util$FunctionUtil.deBounceFunction = function(f, delay) {
    return GeneralJS_CS_v6$Util$FunctionUtil._deBounce(f, delay);
  }
  GeneralJS_CS_v6$Util$FunctionUtil._deBounce = function(f, delay) {
    var timer = null;
    var fn = arguments[0];
    var delay = arguments[1];
    
      return function () {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      fn.apply(context, args);
    }, delay);
  };;
    return null;
  }
  GeneralJS_CS_v6$Util$FunctionUtil.deBounceAction = function(f, delay) {
    return GeneralJS_CS_v6$Util$FunctionUtil._deBounce(f, delay);
  }
  var GeneralJS_CS_v6$Util$FunctionUtil$ = {

  };


  // GeneralJS_CS_v6.Util.HandlebarsUtil

  function GeneralJS_CS_v6$Util$HandlebarsUtil() {
  }
  GeneralJS_CS_v6$Util$HandlebarsUtil.compileTemplate = function(templateHtml) {
    return Handlebars.compile(templateHtml);
  }
  var GeneralJS_CS_v6$Util$HandlebarsUtil$ = {

  };


  // GeneralJS_CS_v6.Util.JsonUtil

  function GeneralJS_CS_v6$Util$JsonUtil() {
  }
  GeneralJS_CS_v6$Util$JsonUtil.stringify = function(item) {
    return JSON.stringify(item);
  }
  GeneralJS_CS_v6$Util$JsonUtil.parseAspNetDates = function(item) {
    if (item != null) {
      var dict = item;
      for (var $key1 in dict) {
        var entry = { key: $key1, value: dict[$key1] };
        if (ss.canCast(entry.value, String)) {
          var strValue = (entry.value != null) ? entry.value.toString() : null;
          if (strValue != null && ss.startsWith(strValue, '/Date(') && ss.endsWith(strValue, '/')) {
            var ticksValue = parseInt(strValue.substring(6, strValue.length - 1));
            var dt = new Date(ticksValue);
            dict[entry.key] = dt;
          }
        }
        else {
          GeneralJS_CS_v6$Util$JsonUtil.parseAspNetDates(entry.value);
        }
      }
    }
  }
  var GeneralJS_CS_v6$Util$JsonUtil$ = {

  };


  // GeneralJS_CS_v6.Util.WindowUtil

  function GeneralJS_CS_v6$Util$WindowUtil() {
  }
  GeneralJS_CS_v6$Util$WindowUtil.get_window = function() {
    return window;
  }
  GeneralJS_CS_v6$Util$WindowUtil.getViewportWidth = function() {
    return $(GeneralJS_CS_v6$Util$WindowUtil.get_window()).width();
  }
  GeneralJS_CS_v6$Util$WindowUtil.getViewportHeight = function() {
    return $(GeneralJS_CS_v6$Util$WindowUtil.get_window()).height();
  }
  var GeneralJS_CS_v6$Util$WindowUtil$ = {

  };


  // GeneralJS_CS_v6.Util.ObjectUtil

  function GeneralJS_CS_v6$Util$ObjectUtil() {
  }
  GeneralJS_CS_v6$Util$ObjectUtil.deleteProperty = function(o, property) {
    delete o[property];
  }
  GeneralJS_CS_v6$Util$ObjectUtil.clearObject = function(obj) {
    var d = obj;
    var $enum1 = ss.enumerate(ss.keys(d));
    while ($enum1.moveNext()) {
      var key = $enum1.current;
      GeneralJS_CS_v6$Util$ObjectUtil.deleteProperty(obj, key);
    }
  }
  var GeneralJS_CS_v6$Util$ObjectUtil$ = {

  };


  // GeneralJS_CS_v6.Util.TypeUtil

  function GeneralJS_CS_v6$Util$TypeUtil() {
  }
  GeneralJS_CS_v6$Util$TypeUtil.instanceOf = function(obj, type) {
    return obj instanceof type;
  }
  GeneralJS_CS_v6$Util$TypeUtil.instanceOfJQuery = function(obj) {
    return obj instanceof $;
  }
  var GeneralJS_CS_v6$Util$TypeUtil$ = {

  };


  // GeneralJS_CS_v6.Unobtrusive.UnobtrusiveController

  function GeneralJS_CS_v6$Unobtrusive$UnobtrusiveController() {
  }
  GeneralJS_CS_v6$Unobtrusive$UnobtrusiveController.get_instance = function() {
    if (GeneralJS_CS_v6$Unobtrusive$UnobtrusiveController._instance == null) {
      GeneralJS_CS_v6$Unobtrusive$UnobtrusiveController._instance = new GeneralJS_CS_v6$Unobtrusive$UnobtrusiveController();
    }
    return GeneralJS_CS_v6$Unobtrusive$UnobtrusiveController._instance;
  }
  var GeneralJS_CS_v6$Unobtrusive$UnobtrusiveController$ = {
    _init: function() {
      this._forms = new GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveForms();
      this._helpMessages = new GeneralJS_CS_v6$Unobtrusive$HelpMessages$HelpMessageControllers();
    },
    initialize: function() {
      this._init();
    }
  };


  // GeneralJS_CS_v6.Wrappers.Elements.MyElementEventHandlers

  function GeneralJS_CS_v6$Wrappers$Elements$MyElementEventHandlers(element) {
    this._element = element;
    this._handlersMap = {};
  }
  var GeneralJS_CS_v6$Wrappers$Elements$MyElementEventHandlers$ = {
    _eventHandler: function(eventObj) {
      var eventType = eventObj.data['eventType'];
      this._element._triggerEvent(eventType, eventObj);
    },
    _addBindEvent: function(eventType) {
      var eventName = GeneralJS_CS_v6$Enums$JQueryEnums.eventTypeToString(eventType);
      if (!ss.keyExists(this._handlersMap, eventName)) {
        var eventData = {};
        eventData['eventType'] = eventType;
        this._element.get_jQueryObject().bind(eventName, eventData, ss.bind('_eventHandler', this));
        this._handlersMap[eventName] = 0;
      }
      var count = this._handlersMap[eventName];
      count++;
      this._handlersMap[eventName] = count;
    },
    _unBindEvent: function(eventType) {
      var eventName = GeneralJS_CS_v6$Enums$JQueryEnums.eventTypeToString(eventType);
      if (ss.keyExists(this._handlersMap, eventName)) {
        var count = this._handlersMap[eventName];
        count--;
        this._handlersMap[eventName] = count;
        if (!count) {
          delete this._handlersMap[eventName];
          this._element.get_jQueryObject().unbind(eventName, ss.bind('_eventHandler', this));
        }
      }
    },
    addBindEvent: function(eventType) {
      this._addBindEvent(eventType);
    },
    removeBindEvent: function(eventType) {
      this._unBindEvent(eventType);
    }
  };


  // GeneralJS_CS_v6.Wrappers.jQueryPlugins.QTip.QTipOptions

  function GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTipOptions() {
    this.styles = (1 | 128);
    this.stayVisibleOnMouseOver = false;
    this.stayVisibleOnMouseOverDelayMs = 200;
  }
  var GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTipOptions$ = {

  };


  // GeneralJS_CS_v6.Util.ListUtil

  function GeneralJS_CS_v6$Util$ListUtil() {
  }
  GeneralJS_CS_v6$Util$ListUtil.cloneList = function(array) {
    return array.slice();
  }
  GeneralJS_CS_v6$Util$ListUtil.clearList = function(a, clearLength) {
    var d = a;
    var $enum1 = ss.enumerate(ss.keys(d));
    while ($enum1.moveNext()) {
      var key = $enum1.current;
      GeneralJS_CS_v6$Util$ObjectUtil.deleteProperty(a, key);
    }
    if (clearLength) {
      d['length'] = 0;
    }
  }
  GeneralJS_CS_v6$Util$ListUtil.addRange = function(a, listOfItems) {
    var array = a;
    var aListOfItems = listOfItems;
    for (var i = 0; i < aListOfItems.length; i++) {
      array.push(aListOfItems[i]);
    }
  }
  var GeneralJS_CS_v6$Util$ListUtil$ = {

  };


  // GeneralJS_CS_v6.Unobtrusive.UnobtrusiveBaseController

  function GeneralJS_CS_v6$Unobtrusive$UnobtrusiveBaseController(jQuerySelector) {
    this._jQuerySelector = jQuerySelector;
    this._init();
  }
  var GeneralJS_CS_v6$Unobtrusive$UnobtrusiveBaseController$ = {
    _init: function() {
      var jItems = $(this._jQuerySelector);
      for (var i = 0; i < jItems.length; i++) {
        var jItem = jItems.eq(i);
        this.initializeItem(jItem);
      }
    }
  };


  // GeneralJS_CS_v6.Unobtrusive.Fields.UnobtrusiveForm

  function GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveForm(jForm) {
    this._jForm = jForm;
    this._init();
  }
  var GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveForm$ = {
    _initFields: function() {
      var jFields = this._jForm.find('[' + 'data-csval-field' + ']');
      this._fields = [];
      var validationOptions = {};
      validationOptions.rules = {};
      validationOptions.messages = {};
      for (var i = 0; i < jFields.length; i++) {
        var field = new GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveField(jFields.eq(i));
        this._fields[this._fields.length] = field;
        var fieldMessages = field.getMessagesObject();
        var fieldRules = field.getRulesObject();
        validationOptions.rules[field.get_name()] = fieldRules;
        validationOptions.messages[field.get_name()] = fieldMessages;
      }
      validationOptions.highlight = ss.bind('_onHighlightError', this);
      validationOptions.success = ss.bind('_onSuccess', this);
      validationOptions.unhighlight = ss.bind('_onUnHighlightError', this);
      validationOptions.errorPlacement = ss.bind('_onErrorPlacement', this);
      this._jForm.validate(validationOptions);
    },
    _onSuccess: function(label) {
      label.css('background', 'red');
    },
    _onErrorPlacement: function(label, error) {
      var elementSelector = '#' + label.attr('for');
      var element = $(elementSelector);
      var field = this._getFieldByElement(element.get(0));
      field.attachErrorMessageViaQTip(label, !ss.emptyString(label.text()));
    },
    _getFieldByElement: function(element) {
      for (var i = 0; i < this._fields.length; i++) {
        var field = this._fields[i];
        if (field.get_field().get_jQueryObject().get(0) === element) {
          return field;
        }
      }
      return null;
    },
    _updateFieldValidationIcon: function(element, success) {
      var field = this._getFieldByElement(element);
      if (field != null) {
        field.updateValidationIcon(success);
      }
    },
    _onHighlightError: function(element, errorClass, validClass) {
      this._updateFieldValidationIcon(element, false);
    },
    _onUnHighlightError: function(element, errorClass, validClass) {
      this._updateFieldValidationIcon(element, true);
    },
    _init: function() {
      this._initFields();
    }
  };


  // GeneralJS_CS_v6.Unobtrusive.HelpMessages.HelpMessageController

  function GeneralJS_CS_v6$Unobtrusive$HelpMessages$HelpMessageController(element) {
    this._element = new GeneralJS_CS_v6$Wrappers$Elements$MyElement(element);
    this._init();
  }
  var GeneralJS_CS_v6$Unobtrusive$HelpMessages$HelpMessageController$ = {
    _parseMessage: function() {
      var msg = this._element.get_jQueryObject().attr('data-help-msg');
      var helpMessageIconSelector = this._element.get_jQueryObject().attr('data-help-msg-element');
      var jHelpMessageIcon = $(helpMessageIconSelector);
      if (!ss.emptyString(msg) && jHelpMessageIcon.length > 0) {
        var opts = new GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTipOptions();
        opts.options = {};
        opts.contentHtml = msg;
        opts.stayVisibleOnMouseOver = true;
        this._qTip = new GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTip(jHelpMessageIcon, opts);
      }
    },
    _init: function() {
      this._parseMessage();
    }
  };


  // GeneralJS_CS_v6.Unobtrusive.Fields.UnobtrusiveForms

  function GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveForms() {
    this._initialize();
  }
  var GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveForms$ = {
    _initValidatorDefaults: function() {
      var opts = {};
      opts.onkeyup = false;
      $.validator.setDefaults(opts);
    },
    _initialize: function() {
      this._initValidatorDefaults();
      this._forms = [];
      var jForms = $('form');
      for (var i = 0; i < jForms.length; i++) {
        var jForm = jForms.eq(i);
        var form = new GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveForm(jForm);
        this._forms[this._forms.length] = form;
      }
    }
  };


  // GeneralJS_CS_v6.Unobtrusive.Fields.UnobtrusiveField

  function GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveField(jField) {
    this._showingErrorMessageForFirstTime = false;
    this._field = new GeneralJS_CS_v6$Wrappers$Elements$MyElement(jField);
    this._init();
  }
  var GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveField$ = {
    _getErrorMessage: function(jQueryValidationMethod, attributePrefix) {
      var attribute = attributePrefix + '-' + 'errmsg';
      var msg = this.get_field().get_jQueryObject().attr(attribute);
      return msg;
    },
    _addErrorMessage: function(messages, jQueryValidationMethod, attributePrefix) {
      var msg = this._getErrorMessage(jQueryValidationMethod, attributePrefix);
      if (!ss.emptyString(msg)) {
        messages[jQueryValidationMethod] = msg;
      }
    },
    _initNumericRule: function(rules) {
      var isNumeric = GeneralJS_CS_v6$Util$DomUtil.hasAttribute(this.get_field().get_jQueryObject(), 'data-csval-numeric');
      if (isNumeric) {
        rules['number'] = true;
      }
    },
    _initNumericMinRule: function(rules) {
      var min = parseFloat(this.get_field().get_jQueryObject().attr('data-csval-min-value'));
      if (!isNaN(min)) {
        rules['min'] = min;
      }
    },
    _initNumericMaxRule: function(rules) {
      var max = parseFloat(this.get_field().get_jQueryObject().attr('data-csval-max-value'));
      if (!isNaN(max)) {
        rules['max'] = max;
      }
    },
    _initNumericRangeRule: function(rules) {
      if (GeneralJS_CS_v6$Util$DomUtil.hasAttribute(this.get_field().get_jQueryObject(), 'data-csval-range-value')) {
        var values = this.get_field().get_jQueryObject().attr('data-csval-range-value').split(',');
        if (values.length > 1) {
          var min = parseFloat(values[0]);
          var max = parseFloat(values[1]);
          if (!isNaN(min) && !isNaN(max)) {
            var range = [];
            range.push(min);
            range.push(max);
            rules['range'] = range;
          }
        }
      }
    },
    _initStringLengthMinRule: function(rules) {
      var min = parseInt(this.get_field().get_jQueryObject().attr('data-csval-min-length'));
      if (!isNaN(min)) {
        rules['minlength'] = min;
      }
    },
    _initStringLengthMaxRule: function(rules) {
      var max = parseInt(this.get_field().get_jQueryObject().attr('data-csval-max-length'));
      if (!isNaN(max)) {
        rules['minlength'] = max;
      }
    },
    _initDateRule: function(rules) {
      var isDate = GeneralJS_CS_v6$Util$DomUtil.hasAttribute(this.get_field().get_jQueryObject(), 'data-csval-date');
      if (isDate) {
        rules['date'] = true;
      }
    },
    _initRegexPatternRule: function(rules) {
      var regexPattern = this.get_field().get_jQueryObject().attr('data-csval-regex-pattern');
      if (!ss.whitespace(regexPattern)) {
        var re = new RegExp(regexPattern);
        rules['pattern'] = re;
      }
    },
    _initStringLengthRangeRule: function(rules) {
      if (GeneralJS_CS_v6$Util$DomUtil.hasAttribute(this.get_field().get_jQueryObject(), 'data-csval-range-length')) {
        var values = this.get_field().get_jQueryObject().attr('data-csval-range-length').split(',');
        if (values.length > 1) {
          var min = parseInt(values[0]);
          var max = parseInt(values[1]);
          if (!isNaN(min) && !isNaN(max)) {
            var range = [];
            range.push(min);
            range.push(max);
            rules['rangelength'] = range;
          }
        }
      }
    },
    _initNumericIntegersOnlyRule: function(rules) {
      var isIntegersOnly = GeneralJS_CS_v6$Util$DomUtil.hasAttribute(this.get_field().get_jQueryObject(), 'data-csval-integers-only');
      if (isIntegersOnly) {
        rules['digits'] = true;
      }
    },
    _initRemoteRule: function(rules) {
      var remoteUrl = this.get_field().get_jQueryObject().attr('data-csval-remote');
      if (!ss.whitespace(remoteUrl)) {
        var ajaxOpts = {};
        ajaxOpts.type = 'post';
        ajaxOpts.url = remoteUrl;
        var data = {};
        data['value'] = ss.bind('val', this.get_field().get_jQueryObject());
        ajaxOpts.data = data;
        rules['remote'] = ajaxOpts;
      }
    },
    _initEmailRule: function(rules) {
      var isEmail = GeneralJS_CS_v6$Util$DomUtil.hasAttribute(this.get_field().get_jQueryObject(), 'data-csval-email');
      if (isEmail) {
        rules['email'] = true;
      }
    },
    _initUrlRule: function(rules) {
      var isUrl = GeneralJS_CS_v6$Util$DomUtil.hasAttribute(this.get_field().get_jQueryObject(), 'data-csval-url');
      if (isUrl) {
        rules['url'] = true;
      }
    },
    _initEqualTo: function(rules) {
      var equalToIdentifiers = this.get_field().get_jQueryObject().attr('data-csval-equal-to');
      if (!ss.whitespace(equalToIdentifiers)) {
        var selector = this._parseIdentifiersToJQuerySelector(equalToIdentifiers);
        rules['equalTo'] = selector;
      }
    },
    _initRequiredRule: function(rules) {
      var isRequired = GeneralJS_CS_v6$Util$DomUtil.hasAttribute(this.get_field().get_jQueryObject(), 'data-csval-required');
      if (isRequired) {
        rules['required'] = true;
      }
    },
    _initRequiredDependency: function(rules) {
      var requiredDependency = this.get_field().get_jQueryObject().attr('data-csval-required-dependency');
      if (!ss.whitespace(requiredDependency)) {
        var parsedSelector = this._parseIdentifiersToJQuerySelector(requiredDependency);
        rules['required'] = parsedSelector;
      }
    },
    _parseIdentifiersToJQuerySelector: function(identifiers) {
      var fields = identifiers.split(',');
      var selector = '';
      for (var i = 0; i < fields.length; i++) {
        if (!ss.emptyString(selector)) {
          selector += ',';
        }
        selector += this._parseIdentifierToJQuerySelector(fields[i].trim());
      }
      return selector;
    },
    _parseIdentifierToJQuerySelector: function(identifier) {
      var colonIndex = identifier.indexOf(':');
      var pseudoSelector = '';
      if (colonIndex !== -1) {
        pseudoSelector = identifier.substr(colonIndex);
        identifier = identifier.substr(0, colonIndex);
      }
      var fieldSelector = '[' + 'data-csval-identifier' + '=' + identifier + ']' + pseudoSelector;
      return fieldSelector;
    },
    _addErrorMessages: function(messages) {
      this._addErrorMessage(messages, 'digits', 'data-csval-integers-only');
      this._addErrorMessage(messages, 'email', 'data-csval-email');
      this._addErrorMessage(messages, 'equalTo', 'data-csval-equal-to');
      this._addErrorMessage(messages, 'max', 'data-csval-max-value');
      this._addErrorMessage(messages, 'min', 'data-csval-min-value');
      this._addErrorMessage(messages, 'maxlength', 'data-csval-max-length');
      this._addErrorMessage(messages, 'minlength', 'data-csval-min-length');
      this._addErrorMessage(messages, 'range', 'data-csval-range-value');
      this._addErrorMessage(messages, 'rangelength', 'data-csval-range-length');
      this._addErrorMessage(messages, 'required', 'data-csval-required');
      this._addErrorMessage(messages, 'url', 'data-csval-url');
      this._addErrorMessage(messages, 'pattern', 'data-csval-regex-pattern');
      this._addErrorMessage(messages, 'date', 'data-csval-date');
    },
    _addValidationRules: function(rules) {
      this._initRequiredRule(rules);
      this._initRequiredDependency(rules);
      this._initEmailRule(rules);
      this._initUrlRule(rules);
      this._initEqualTo(rules);
      this._initNumericRule(rules);
      this._initNumericIntegersOnlyRule(rules);
      this._initNumericMinRule(rules);
      this._initNumericMaxRule(rules);
      this._initNumericRangeRule(rules);
      this._initStringLengthMinRule(rules);
      this._initStringLengthMaxRule(rules);
      this._initStringLengthRangeRule(rules);
      this._initRegexPatternRule(rules);
      this._initRemoteRule(rules);
      this._initDateRule(rules);
    },
    _initValidationIcon: function() {
      var validationIconElementSelector = this.get_field().get_jQueryObject().attr('data-csval-validation-element');
      if (!ss.whitespace(validationIconElementSelector)) {
        var jValidationIcon = $(validationIconElementSelector);
        if (jValidationIcon.length > 0) {
          this._validationIcon = new GeneralJS_CS_v6$Wrappers$Elements$MyElement(validationIconElementSelector);
        }
      }
    },
    _init: function() {
      this._initValidationIcon();
    },
    get_name: function() {
      return this.get_field().get_jQueryObject().attr('name');
    },
    get_field: function() {
      return this._field;
    },
    getMessagesObject: function() {
      var messages = {};
      this._addErrorMessages(messages);
      return messages;
    },
    getRulesObject: function() {
      var rules = {};
      this._addValidationRules(rules);
      return rules;
    },
    updateValidationIcon: function(success) {
      if (this._validationIcon != null) {
        this._validationIcon.get_jQueryObject().removeClass('validation-icon-success');
        this._validationIcon.get_jQueryObject().removeClass('validation-icon-error');
        if (success) {
          this._validationIcon.get_jQueryObject().addClass('validation-icon-success');
        }
        else {
          this._validationIcon.get_jQueryObject().addClass('validation-icon-error');
        }
      }
    },
    _hideFirstTimeErrorMessageIfFieldNotFocused: function() {
      if (!this._field.isFocused()) {
        this._hideErrorMessage();
      }
      this._showingErrorMessageForFirstTime = false;
    },
    _showFirstTimeErrorMessage: function() {
      this._showingErrorMessageForFirstTime = true;
      this._showErrorMessage();
      var showSecDelay = 3000;
      setTimeout(ss.bind('_hideFirstTimeErrorMessageIfFieldNotFocused', this), showSecDelay);
    },
    _hideErrorMessage: function() {
      if (this._qTipErrorMessage != null) {
        this._qTipErrorMessage.enable();
        if (this._qTipErrorMessage.isVisible() && !this._showingErrorMessageForFirstTime) {
          this._qTipErrorMessage.hide();
        }
      }
    },
    _showErrorMessage: function() {
      if (this._qTipErrorMessage != null) {
        this._qTipErrorMessage.disable();
        if (!this._qTipErrorMessage.isVisible()) {
          this._qTipErrorMessage.show();
        }
      }
    },
    attachErrorMessageViaQTip: function(label, attachOrDetach) {
      var errorMessage = label.text();
      if (attachOrDetach) {
        if (errorMessage !== this._qTipErrorMessageText) {
          if (this._qTipErrorMessage != null) {
            this._qTipErrorMessage.destroy();
          }
          this._qTipErrorMessageText = errorMessage;
          var qTipOpts = new GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTipOptions();
          qTipOpts.options = {};
          qTipOpts.options.style = {};
          qTipOpts.options.style.classes = 'validation-icon-error-tooltip qtip-bootstrap';
          qTipOpts.stayVisibleOnMouseOver = true;
          qTipOpts.contentHtml = GeneralJS_CS_v6$Util$DomUtil.getOuterHtml(label);
          var jTooltipElement = null;
          if (this._validationIcon != null) {
            jTooltipElement = this._validationIcon.get_jQueryObject();
          }
          else {
            jTooltipElement = this._field.get_jQueryObject();
          }
          this._qTipErrorMessage = new GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTip(jTooltipElement, qTipOpts);
          this._field.add_onFocus(ss.bind('_field_OnFocus', this));
          this._field.add_onBlur(ss.bind('_field_OnBlur', this));
          this._showFirstTimeErrorMessage();
        }
      }
      else {
        if (this._qTipErrorMessage != null) {
          this._qTipErrorMessage.destroy();
        }
        this._field.remove_onFocus(ss.bind('_field_OnFocus', this));
        this._field.remove_onBlur(ss.bind('_field_OnBlur', this));
        this._qTipErrorMessageText = null;
      }
    },
    _field_OnBlur: function(sender, eventObj) {
      this._hideErrorMessage();
    },
    _field_OnFocus: function(sender, eventObj) {
      this._showErrorMessage();
    }
  };


  // GeneralJS_CS_v6.UI.JQuery.Plugins.SlickGrid.SlickGridAjaxDataController

  function GeneralJS_CS_v6$UI$JQuery$Plugins$SlickGrid$SlickGridAjaxDataController(options) {
    this._error = false;
    this._loadingShowing = false;
    this._stoppedLoading = false;
    this._delayLoadRequestHandler = 0;
    this._options = options;
    this._init();
  }
  var GeneralJS_CS_v6$UI$JQuery$Plugins$SlickGrid$SlickGridAjaxDataController$ = {
    add_onDataLoaded: function(value) {
      this.__onDataLoaded = ss.bindAdd(this.__onDataLoaded, value);
    },
    remove_onDataLoaded: function(value) {
      this.__onDataLoaded = ss.bindSub(this.__onDataLoaded, value);
    },
    _initLoadingMask: function() {
      this._jLoadingIconLoading = $("<div class='slick-grid-loading-icon' />");
      this._jLoadingIconError = $("<div class='slick-grid-loading-icon-error' />");
      this._jLoadingMask = $("<div class='slick-grid-loading-mask'>\r\n                                                <div class='slick-grid-loading-back'></div>\r\n                                              </div>");
      this._jLoadingMask.append(this._jLoadingIconLoading);
      this._jLoadingMask.append(this._jLoadingIconError);
      var jCanvas = $(this._options.grid.getCanvasNode());
      jCanvas.parent().parent().append(this._jLoadingMask);
      this._jLoadingIconError.fadeOut(0);
      this._updateLoadingMaskWidth();
      $(GeneralJS_CS_v6$Util$WindowUtil.get_window()).bind('resize', ss.bind('_onWindowResize', this));
    },
    _onWindowResize: function(e) {
      this._updateLoadingMaskWidth();
    },
    _updateLoadingMaskWidth: function() {
      var width = GeneralJS_CS_v6$Util$WindowUtil.getViewportWidth() - 16;
      this._jLoadingMask.width(width);
    },
    _initHandlers: function() {
      this._options.grid.onViewportChanged.subscribe(ss.bind('_onViewportChanged', this));
      this._options.grid.onSort.subscribe(ss.bind('_onSortData', this));
    },
    _onSortData: function(e, args) {
      this._lastSortColsData = [];
      this._options.grid.setSelectedRows([]);
      for (var i = 0; i < args.sortCols.length; i++) {
        var colData = args.sortCols[i];
        var sortCol = {};
        sortCol.id = colData.sortCol.id;
        sortCol.sortAsc = colData.sortAsc;
        this._lastSortColsData.push(sortCol);
      }
      this._options.grid.scrollRowToTop(0);
      GeneralJS_CS_v6$Util$ListUtil.clearList(this._options.data, false);
      this._loadDataOfCurrentViewportRange(false);
    },
    _initXhr: function() {
      this._xhr = new GeneralJS_CS_v6$Wrappers$Xhr$MyXmlHttpRequest();
      this._xhr.timeoutMs = 60000;
      this._xhr.add_onRequestComplete(ss.bind('_onDataLoaded', this));
    },
    _minimizeLoadingRange: function(from, to) {
      var normalizedFrom = -1;
      var normalizedTo = -1;
      for (var i = from; i < to; i++) {
        if (!ss.isValue(this._options.data[i])) {
          normalizedFrom = i;
          break;
        }
      }
      for (var i = to - 1; i >= 0; i--) {
        if (!ss.isValue(this._options.data[i])) {
          normalizedTo = i + 1;
          break;
        }
      }
      if (normalizedFrom !== -1 && normalizedTo !== -1) {
        var normalizedRange = [];
        normalizedRange.push(normalizedFrom);
        normalizedRange.push(normalizedTo);
        return normalizedRange;
      }
      else {
        return null;
      }
    },
    _updateData: function(response) {
      var rowIndices = [];
      for (var i = 0; i < response.data.length; i++) {
        var rowIndex = i + response.from;
        this._options.data[rowIndex] = response.data[i];
        rowIndices.push(rowIndex);
      }
      this._options.grid.invalidateRows(rowIndices);
      this._options.grid.render();
    },
    _onDataLoaded: function(sender, data, status, error) {
      if (!status) {
        var response = data;
        this._updateData(response);
        this.hideLoading();
      }
      else {
        this.showError();
      }
      if (this.__onDataLoaded != null) {
        this.__onDataLoaded(this, sender, data, status, error);
      }
    },
    hideLoading: function() {
      if (this._loadingShowing) {
        this._jLoadingMask.fadeOut(200);
        this._loadingShowing = false;
      }
    },
    showError: function() {
      if (!this._error) {
        this._jLoadingIconError.fadeIn(200);
        this._jLoadingIconLoading.fadeOut(200);
        this._error = true;
      }
    },
    showLoading: function() {
      if (!this._loadingShowing) {
        this._jLoadingMask.fadeIn(200);
        this._loadingShowing = true;
      }
    },
    _loadLastRequestedData: function() {
      var from = this._lastLoadRequestRange[0];
      var to = this._lastLoadRequestRange[1];
      this._lastLoadRequestRange = null;
      this._delayLoadRequestHandler = -1;
      var sortJson = null;
      if (this._lastSortColsData != null) {
        sortJson = GeneralJS_CS_v6$Util$JsonUtil.stringify(this._lastSortColsData);
      }
      if (this._error) {
        this._jLoadingIconError.fadeOut(200);
        this._jLoadingIconLoading.fadeIn(200);
        this._error = false;
      }
      var data = {};
      data['from'] = from;
      data['to'] = to;
      data['sort'] = sortJson;
      this._xhr.post(this._options.ajaxDataUrl, data, false);
    },
    _loadDataOfCurrentViewportRange: function(delayLoad) {
      if (!this._stoppedLoading) {
        var vp = this._options.grid.getViewport();
        var startRowIndex = vp.top;
        var endRowIndex = vp.bottom;
        var minimizedRange = this._minimizeLoadingRange(startRowIndex, endRowIndex);
        if (minimizedRange != null) {
          if (this._delayLoadRequestHandler !== -1) {
            clearTimeout(this._delayLoadRequestHandler);
          }
          this._lastLoadRequestRange = minimizedRange;
          this.showLoading();
          var delayMs = (delayLoad) ? 400 : 0;
          this._delayLoadRequestHandler = setTimeout(ss.bind('_loadLastRequestedData', this), delayMs);
        }
        else {
        }
      }
    },
    refreshCurrentViewportData: function() {
      this._loadDataOfCurrentViewportRange(false);
    },
    _onViewportChanged: function(e, args) {
      this._loadDataOfCurrentViewportRange(true);
    },
    _loadInitialData: function() {
      this._loadDataOfCurrentViewportRange(false);
    },
    _initSort: function() {
      this._lastSortColsData = this._options.defaultSortColumns;
    },
    _init: function() {
      this._initSort();
      this._initLoadingMask();
      this._initHandlers();
      this._initXhr();
      this._loadInitialData();
    },
    addRow: function(newItem) {
      this.clearDataOfCurrentViewport();
      this._options.data.length = this._options.data.length + 1;
      this._options.grid.scrollRowToTop(this._options.data.length);
      this.refreshCurrentViewportData();
      this._options.grid.updateRowCount();
    },
    deleteRows: function(rowIndices) {
      this._options.grid.setSelectedRows([]);
      this.clearDataOfCurrentViewport();
      this._options.data.length = this._options.data.length - rowIndices.length;
      this._options.grid.updateRowCount();
      this.refreshCurrentViewportData();
    },
    clearDataOfCurrentViewport: function() {
      var vp = this._options.grid.getViewport();
      var startRowIndex = vp.top;
      var endRowIndex = Math.min(this._options.data.length, vp.bottom);
      for (var i = startRowIndex; i < endRowIndex; i++) {
        this._options.data[i] = null;
      }
    },
    stopAjaxLoading: function() {
      this._stoppedLoading = true;
    }
  };


  // GeneralJS_CS_v6.UI.Controls.FileUpload.DeleteFileItemResponse

  function GeneralJS_CS_v6$UI$Controls$FileUpload$DeleteFileItemResponse() {
  }
  var GeneralJS_CS_v6$UI$Controls$FileUpload$DeleteFileItemResponse$ = {

  };


  // GeneralJS_CS_v6.UI.Controls.FileUpload.DeleteFileItemController

  function GeneralJS_CS_v6$UI$Controls$FileUpload$DeleteFileItemController(options) {
    this._options = options;
    this._init();
  }
  var GeneralJS_CS_v6$UI$Controls$FileUpload$DeleteFileItemController$ = {
    _initControls: function() {
      this._jBtnDelete = $(this._options.btnDeleteSelector);
      this._jElement = $(this._options.contentElementSelector);
      this._jBtnDelete.on('click', ss.bind('_onClickDelete', this));
    },
    _submitDelete: function() {
      this._jElement.addClass('delete-file-item-confirmation');
      if (confirm(this._options.confirmationMessage)) {
        this._xhrDelete = new GeneralJS_CS_v6$Wrappers$Xhr$MyXmlHttpRequest();
        this._xhrDelete.add_onRequestComplete(ss.bind('_xhrDelete_OnRequestComplete', this));
        this._xhrDelete.post(this._options.deleteAjaxUrl, null);
        this._jElement.slideUp(250);
      }
      this._jElement.removeClass('delete-file-item-confirmation');
    },
    _xhrDelete_OnRequestComplete: function(sender, data, status, error) {
      var errorMessage = null;
      if (!status) {
        var response = data;
        errorMessage = response.errorMessage;
      }
      else {
        errorMessage = 'Error encountered deleting file.  Please refresh page and try again later.  If problem persists, contact administrators.';
      }
      if (!ss.whitespace(errorMessage)) {
        alert(errorMessage);
      }
    },
    _onClickDelete: function(e) {
      this._submitDelete();
      e.preventDefault();
    },
    _init: function() {
      this._initControls();
    },
    get_options: function() {
      return this._options;
    }
  };


  // GeneralJS_CS_v6.UI.Common.CommonControllers

  function GeneralJS_CS_v6$UI$Common$CommonControllers(options) {
    this._options = options;
    this._init();
  }
  var GeneralJS_CS_v6$UI$Common$CommonControllers$ = {
    _initInlineEditing: function() {
    },
    _init: function() {
      this._initInlineEditing();
    },
    get_options: function() {
      return this._options;
    }
  };


  // GeneralJS_CS_v6.Interfaces.Impl.Destroyable

  function GeneralJS_CS_v6$Interfaces$Impl$Destroyable() {
  }
  var GeneralJS_CS_v6$Interfaces$Impl$Destroyable$ = {
    destroy: function() {
    }
  };


  // GeneralJS_CS_v6.Enums.GenericEnums

  function GeneralJS_CS_v6$Enums$GenericEnums() {
  }
  GeneralJS_CS_v6$Enums$GenericEnums.convertJQueryXmlHttpRequestDataTypeToString = function(dataType) {
    switch (dataType) {
      case 3:
        return 'html';
      case 1:
        return 'json';
      case 2:
        return 'script';
      case 0:
        return 'xml';
    }
    return null;
  }
  GeneralJS_CS_v6$Enums$GenericEnums.convertTextToXmlHttpRequestStatus = function(text) {
    if (!ss.compareStrings(text, 'abort', true)) {
      return 4;
    }
    if (!ss.compareStrings(text, 'error', true)) {
      return 2;
    }
    if (!ss.compareStrings(text, 'notmodified', true)) {
      return 1;
    }
    if (!ss.compareStrings(text, 'parsererror', true)) {
      return 5;
    }
    if (!ss.compareStrings(text, 'success', true)) {
      return 0;
    }
    if (!ss.compareStrings(text, 'timeout', true)) {
      return 3;
    }
    throw new Error('Invalid text status ' + text);
  }
  var GeneralJS_CS_v6$Enums$GenericEnums$ = {

  };


  // GeneralJS_CS_v6.Enums.JQueryEnums

  function GeneralJS_CS_v6$Enums$JQueryEnums() {
  }
  GeneralJS_CS_v6$Enums$JQueryEnums.easingToString = function(easing) {
    switch (easing) {
      case 0:
        return 'linear';
      case 10:
        return 'swing';
    }
    throw new Error("Invalid easing value '" + easing + "'");
  }
  GeneralJS_CS_v6$Enums$JQueryEnums.eventTypeToString = function(type) {
    switch (type) {
      case 6:
        return 'beforeunload';
      case 0:
        return 'blur';
      case 16:
        return 'change';
      case 7:
        return 'click';
      case 8:
        return 'dblclick';
      case 22:
        return 'error';
      case 1:
        return 'focus';
      case 19:
        return 'keydown';
      case 20:
        return 'keypress';
      case 21:
        return 'keyup';
      case 2:
        return 'load';
      case 9:
        return 'mousedown';
      case 14:
        return 'mouseenter';
      case 15:
        return 'mouseleave';
      case 11:
        return 'mousemove';
      case 13:
        return 'mouseout';
      case 12:
        return 'mouseover';
      case 10:
        return 'mouseup';
      case 23:
        return 'ready';
      case 3:
        return 'resize';
      case 4:
        return 'scroll';
      case 17:
        return 'select';
      case 18:
        return 'submit';
      case 5:
        return 'unload';
    }
    return null;
  }
  GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString = function(effect) {
    switch (effect) {
      case 0:
        return 'blind';
      case 1:
        return 'bounce';
      case 2:
        return 'clip';
      case 3:
        return 'drop';
      case 4:
        return 'explode';
      case 5:
        return 'fold';
      case 6:
        return 'highlight';
      case 7:
        return 'puff';
      case 8:
        return 'pulsate';
      case 9:
        return 'scale';
      case 10:
        return 'shake';
      case 11:
        return 'size';
      case 12:
        return 'slide';
      case 13:
        return 'transfer';
      case 14:
        return 'fade';
    }
    return null;
  }
  GeneralJS_CS_v6$Enums$JQueryEnums.jQueryPositionToString = function(pos) {
    switch (pos) {
      case 4:
        return 'bottom';
      case 0:
        return 'center';
      case 2:
        return 'left';
      case 3:
        return 'right';
      case 1:
        return 'top';
    }
    return null;
  }
  GeneralJS_CS_v6$Enums$JQueryEnums.stringToJQueryUIEffect = function(effect) {
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(0)) {
      return 0;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(1)) {
      return 1;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(2)) {
      return 2;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(3)) {
      return 3;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(4)) {
      return 4;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(5)) {
      return 5;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(6)) {
      return 6;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(7)) {
      return 7;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(8)) {
      return 8;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(9)) {
      return 9;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(10)) {
      return 10;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(11)) {
      return 11;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(12)) {
      return 12;
    }
    if (effect === GeneralJS_CS_v6$Enums$JQueryEnums.jQueryUIEffectToString(13)) {
      return 13;
    }
    return 9999;
  }
  GeneralJS_CS_v6$Enums$JQueryEnums.jQueryPseudoSelectorsToString = function(selector) {
    switch (selector) {
      case 0:
        return ':animated';
      case 1:
        return ':button';
      case 2:
        return ':checkbox';
      case 3:
        return ':checked';
      case 4:
        return ':disabled';
      case 5:
        return ':empty';
      case 6:
        return ':enabled';
      case 7:
        return ':even';
      case 8:
        return ':file';
      case 10:
        return ':first';
      case 9:
        return ':first-child';
      case 11:
        return ':focus';
      case 12:
        return ':header';
      case 13:
        return ':hidden';
      case 14:
        return ':image';
      case 15:
        return ':input';
      case 17:
        return ':last';
      case 16:
        return ':last-child';
      case 18:
        return ':odd';
      case 19:
        return ':parent';
      case 20:
        return ':password';
      case 21:
        return ':radio';
      case 22:
        return ':reset';
      case 23:
        return ':selected';
      case 24:
        return ':submit';
      case 25:
        return ':text';
      case 26:
        return ':visible';
    }
    throw new Error("Pseudo selector not defined '" + selector + "'");
  }
  var GeneralJS_CS_v6$Enums$JQueryEnums$ = {

  };


  // GeneralJS_CS_v6.Constants.FieldAttributes

  function GeneralJS_CS_v6$Constants$FieldAttributes() {
  }
  var GeneralJS_CS_v6$Constants$FieldAttributes$ = {

  };


  // GeneralJS_CS_v6.Class1

  function GeneralJS_CS_v6$Class1() {
    var i = 3;
  }
  var GeneralJS_CS_v6$Class1$ = {

  };


  // GeneralJS_CS_v6.Unobtrusive.HelpMessages.HelpMessageControllers

  function GeneralJS_CS_v6$Unobtrusive$HelpMessages$HelpMessageControllers() {
    this._init();
  }
  var GeneralJS_CS_v6$Unobtrusive$HelpMessages$HelpMessageControllers$ = {
    _initHelpMessages: function() {
      var jElements = $('[' + 'data-help-msg' + ']');
      this._controllers = [];
      for (var i = 0; i < jElements.length; i++) {
        var jElement = jElements.eq(i);
        var helpMessageController = new GeneralJS_CS_v6$Unobtrusive$HelpMessages$HelpMessageController(jElement);
        this._controllers[this._controllers.length] = helpMessageController;
      }
    },
    _init: function() {
      this._initHelpMessages();
    }
  };


  // GeneralJS_CS_v6.Wrappers.Xhr.MyXmlHttpRequest

  function GeneralJS_CS_v6$Wrappers$Xhr$MyXmlHttpRequest() {
    this.dataType = 1;
    this.cache = true;
    this.timeoutMs = 0;
    this._omitErrors = false;
  }
  var GeneralJS_CS_v6$Wrappers$Xhr$MyXmlHttpRequest$ = {
    add_onRequestError: function(value) {
      this.__onRequestError = ss.bindAdd(this.__onRequestError, value);
    },
    remove_onRequestError: function(value) {
      this.__onRequestError = ss.bindSub(this.__onRequestError, value);
    },
    add_onRequestSuccess: function(value) {
      this.__onRequestSuccess = ss.bindAdd(this.__onRequestSuccess, value);
    },
    remove_onRequestSuccess: function(value) {
      this.__onRequestSuccess = ss.bindSub(this.__onRequestSuccess, value);
    },
    add_onRequestComplete: function(value) {
      this.__onRequestComplete = ss.bindAdd(this.__onRequestComplete, value);
    },
    remove_onRequestComplete: function(value) {
      this.__onRequestComplete = ss.bindSub(this.__onRequestComplete, value);
    },
    _submitAjaxRequest: function(opts, triggerErrorIfRequestStillLoading) {
      opts.cache = this.cache;
      if (this._xhr != null) {
        if (!triggerErrorIfRequestStillLoading) {
          this._omitErrors = true;
        }
        this._xhr.abort();
        this._xhr = null;
        this._omitErrors = false;
      }
      this._xhr = $.ajax(opts);
    },
    _onRequestError: function(request, textStatus, error) {
      if (!this._omitErrors) {
        var status = GeneralJS_CS_v6$Enums$GenericEnums.convertTextToXmlHttpRequestStatus(textStatus);
        if (this.__onRequestError != null) {
          this.__onRequestError(this, status, error);
        }
        if (this.__onRequestComplete != null) {
          this.__onRequestComplete(this, null, status, error);
        }
      }
    },
    _getAjaxOptions: function(getOrPost, data, url) {
      var opts = {};
      opts.url = url;
      opts.data = data;
      opts.type = (getOrPost) ? 'GET' : 'POST';
      opts.success = ss.bind('_onRequestSuccess', this);
      opts.error = ss.bind('_onRequestError', this);
      if (ss.isValue(this.timeoutMs)) {
        opts.timeout = this.timeoutMs;
      }
      opts.dataType = GeneralJS_CS_v6$Enums$GenericEnums.convertJQueryXmlHttpRequestDataTypeToString(this.dataType);
      return opts;
    },
    _onRequestSuccess: function(data, textStatus, request) {
      var status = GeneralJS_CS_v6$Enums$GenericEnums.convertTextToXmlHttpRequestStatus(textStatus);
      GeneralJS_CS_v6$Util$JsonUtil.parseAspNetDates(data);
      if (this.__onRequestSuccess != null) {
        this.__onRequestSuccess(this, data, status);
      }
      if (this.__onRequestComplete != null) {
        this.__onRequestComplete(this, data, status, null);
      }
    },
    get: function(url, triggerErrorIfRequestStillLoading) {
      var opts = this._getAjaxOptions(true, null, url);
      this._submitAjaxRequest(opts, triggerErrorIfRequestStillLoading);
    },
    post: function(url, data, triggerErrorIfRequestStillLoading) {
      if (!ss.isValue(triggerErrorIfRequestStillLoading)) {
        triggerErrorIfRequestStillLoading = false;
      }
      var opts = this._getAjaxOptions(false, data, url);
      this._submitAjaxRequest(opts, triggerErrorIfRequestStillLoading);
    },
    abort: function(triggerErrorIfRequestStillLoading) {
      if (this._xhr != null) {
        if (!triggerErrorIfRequestStillLoading) {
          this._omitErrors = true;
        }
        this._xhr.abort();
        this._omitErrors = false;
      }
    }
  };


  // GeneralJS_CS_v6.Wrappers.Elements.MyElement

  function GeneralJS_CS_v6$Wrappers$Elements$MyElement(jQueryElement) {
    GeneralJS_CS_v6$Interfaces$Impl$Destroyable.call(this);
    if (ss.canCast(jQueryElement, String)) {
      this._jQueryObject$1 = $(jQueryElement);
    }
    else if (GeneralJS_CS_v6$Util$TypeUtil.instanceOfJQuery(jQueryElement)) {
      this._jQueryObject$1 = jQueryElement;
    }
    else {
      this._jQueryObject$1 = $(jQueryElement);
    }
    if (!this._jQueryObject$1.length) {
      throw new Error("Object with element '" + jQueryElement + "' not found");
    }
    this._init$1();
  }
  var GeneralJS_CS_v6$Wrappers$Elements$MyElement$ = {
    _init$1: function() {
      this._eventHandlers$1 = new GeneralJS_CS_v6$Wrappers$Elements$MyElementEventHandlers(this);
    },
    destroy: function() {
      this.get_jQueryObject().remove();
      ss.base(this, 'destroy').call(this);
    },
    get_jQueryObject: function() {
      return this._jQueryObject$1;
    },
    add__onBlur$1: function(value) {
      this.___onBlur$1 = ss.bindAdd(this.___onBlur$1, value);
    },
    remove__onBlur$1: function(value) {
      this.___onBlur$1 = ss.bindSub(this.___onBlur$1, value);
    },
    add_onBlur: function(value) {
      this._eventHandlers$1.addBindEvent(0);
      this.add__onBlur$1(value);
    },
    remove_onBlur: function(value) {
      this._eventHandlers$1.removeBindEvent(0);
      this.remove__onBlur$1(value);
    },
    add__onFocus$1: function(value) {
      this.___onFocus$1 = ss.bindAdd(this.___onFocus$1, value);
    },
    remove__onFocus$1: function(value) {
      this.___onFocus$1 = ss.bindSub(this.___onFocus$1, value);
    },
    add_onFocus: function(value) {
      this._eventHandlers$1.addBindEvent(1);
      this.add__onFocus$1(value);
    },
    remove_onFocus: function(value) {
      this._eventHandlers$1.removeBindEvent(1);
      this.remove__onFocus$1(value);
    },
    add__onLoad$1: function(value) {
      this.___onLoad$1 = ss.bindAdd(this.___onLoad$1, value);
    },
    remove__onLoad$1: function(value) {
      this.___onLoad$1 = ss.bindSub(this.___onLoad$1, value);
    },
    add_onLoad: function(value) {
      this._eventHandlers$1.addBindEvent(2);
      this.add__onLoad$1(value);
    },
    remove_onLoad: function(value) {
      this._eventHandlers$1.removeBindEvent(2);
      this.remove__onLoad$1(value);
    },
    add__onResize$1: function(value) {
      this.___onResize$1 = ss.bindAdd(this.___onResize$1, value);
    },
    remove__onResize$1: function(value) {
      this.___onResize$1 = ss.bindSub(this.___onResize$1, value);
    },
    add_onResize: function(value) {
      this._eventHandlers$1.addBindEvent(3);
      this.add__onResize$1(value);
    },
    remove_onResize: function(value) {
      this._eventHandlers$1.removeBindEvent(3);
      this.remove__onResize$1(value);
    },
    add__onScroll$1: function(value) {
      this.___onScroll$1 = ss.bindAdd(this.___onScroll$1, value);
    },
    remove__onScroll$1: function(value) {
      this.___onScroll$1 = ss.bindSub(this.___onScroll$1, value);
    },
    add_onScroll: function(value) {
      this._eventHandlers$1.addBindEvent(4);
      this.add__onScroll$1(value);
    },
    remove_onScroll: function(value) {
      this._eventHandlers$1.removeBindEvent(4);
      this.remove__onScroll$1(value);
    },
    add__onUnload$1: function(value) {
      this.___onUnload$1 = ss.bindAdd(this.___onUnload$1, value);
    },
    remove__onUnload$1: function(value) {
      this.___onUnload$1 = ss.bindSub(this.___onUnload$1, value);
    },
    add_onUnload: function(value) {
      this._eventHandlers$1.addBindEvent(5);
      this.add__onUnload$1(value);
    },
    remove_onUnload: function(value) {
      this._eventHandlers$1.removeBindEvent(5);
      this.remove__onUnload$1(value);
    },
    add__onBeforeUnload$1: function(value) {
      this.___onBeforeUnload$1 = ss.bindAdd(this.___onBeforeUnload$1, value);
    },
    remove__onBeforeUnload$1: function(value) {
      this.___onBeforeUnload$1 = ss.bindSub(this.___onBeforeUnload$1, value);
    },
    add_onBeforeUnload: function(value) {
      this._eventHandlers$1.addBindEvent(6);
      this.add__onBeforeUnload$1(value);
    },
    remove_onBeforeUnload: function(value) {
      this._eventHandlers$1.removeBindEvent(6);
      this.remove__onBeforeUnload$1(value);
    },
    add__onClick$1: function(value) {
      this.___onClick$1 = ss.bindAdd(this.___onClick$1, value);
    },
    remove__onClick$1: function(value) {
      this.___onClick$1 = ss.bindSub(this.___onClick$1, value);
    },
    add_onClick: function(value) {
      this._eventHandlers$1.addBindEvent(7);
      this.add__onClick$1(value);
    },
    remove_onClick: function(value) {
      this._eventHandlers$1.removeBindEvent(7);
      this.remove__onClick$1(value);
    },
    add__onDoubleClick$1: function(value) {
      this.___onDoubleClick$1 = ss.bindAdd(this.___onDoubleClick$1, value);
    },
    remove__onDoubleClick$1: function(value) {
      this.___onDoubleClick$1 = ss.bindSub(this.___onDoubleClick$1, value);
    },
    add_onDoubleClick: function(value) {
      this._eventHandlers$1.addBindEvent(8);
      this.add__onDoubleClick$1(value);
    },
    remove_onDoubleClick: function(value) {
      this._eventHandlers$1.removeBindEvent(8);
      this.remove__onDoubleClick$1(value);
    },
    add__onMouseDown$1: function(value) {
      this.___onMouseDown$1 = ss.bindAdd(this.___onMouseDown$1, value);
    },
    remove__onMouseDown$1: function(value) {
      this.___onMouseDown$1 = ss.bindSub(this.___onMouseDown$1, value);
    },
    add_onMouseDown: function(value) {
      this._eventHandlers$1.addBindEvent(9);
      this.add__onMouseDown$1(value);
    },
    remove_onMouseDown: function(value) {
      this._eventHandlers$1.removeBindEvent(9);
      this.remove__onMouseDown$1(value);
    },
    add__onMouseUp$1: function(value) {
      this.___onMouseUp$1 = ss.bindAdd(this.___onMouseUp$1, value);
    },
    remove__onMouseUp$1: function(value) {
      this.___onMouseUp$1 = ss.bindSub(this.___onMouseUp$1, value);
    },
    add_onMouseUp: function(value) {
      this._eventHandlers$1.addBindEvent(10);
      this.add__onMouseUp$1(value);
    },
    remove_onMouseUp: function(value) {
      this._eventHandlers$1.removeBindEvent(10);
      this.remove__onMouseUp$1(value);
    },
    add__onMouseMove$1: function(value) {
      this.___onMouseMove$1 = ss.bindAdd(this.___onMouseMove$1, value);
    },
    remove__onMouseMove$1: function(value) {
      this.___onMouseMove$1 = ss.bindSub(this.___onMouseMove$1, value);
    },
    add_onMouseMove: function(value) {
      this._eventHandlers$1.addBindEvent(11);
      this.add__onMouseMove$1(value);
    },
    remove_onMouseMove: function(value) {
      this._eventHandlers$1.removeBindEvent(11);
      this.remove__onMouseMove$1(value);
    },
    add__onMouseOver$1: function(value) {
      this.___onMouseOver$1 = ss.bindAdd(this.___onMouseOver$1, value);
    },
    remove__onMouseOver$1: function(value) {
      this.___onMouseOver$1 = ss.bindSub(this.___onMouseOver$1, value);
    },
    add_onMouseOver: function(value) {
      this._eventHandlers$1.addBindEvent(12);
      this.add__onMouseOver$1(value);
    },
    remove_onMouseOver: function(value) {
      this._eventHandlers$1.removeBindEvent(12);
      this.remove__onMouseOver$1(value);
    },
    add__onMouseOut$1: function(value) {
      this.___onMouseOut$1 = ss.bindAdd(this.___onMouseOut$1, value);
    },
    remove__onMouseOut$1: function(value) {
      this.___onMouseOut$1 = ss.bindSub(this.___onMouseOut$1, value);
    },
    add_onMouseOut: function(value) {
      this._eventHandlers$1.addBindEvent(13);
      this.add__onMouseOut$1(value);
    },
    remove_onMouseOut: function(value) {
      this._eventHandlers$1.removeBindEvent(13);
      this.remove__onMouseOut$1(value);
    },
    add__onMouseEnter$1: function(value) {
      this.___onMouseEnter$1 = ss.bindAdd(this.___onMouseEnter$1, value);
    },
    remove__onMouseEnter$1: function(value) {
      this.___onMouseEnter$1 = ss.bindSub(this.___onMouseEnter$1, value);
    },
    add_onMouseEnter: function(value) {
      this._eventHandlers$1.addBindEvent(14);
      this.add__onMouseEnter$1(value);
    },
    remove_onMouseEnter: function(value) {
      this._eventHandlers$1.removeBindEvent(14);
      this.remove__onMouseEnter$1(value);
    },
    add__onMouseLeave$1: function(value) {
      this.___onMouseLeave$1 = ss.bindAdd(this.___onMouseLeave$1, value);
    },
    remove__onMouseLeave$1: function(value) {
      this.___onMouseLeave$1 = ss.bindSub(this.___onMouseLeave$1, value);
    },
    add_onMouseLeave: function(value) {
      this._eventHandlers$1.addBindEvent(15);
      this.add__onMouseLeave$1(value);
    },
    remove_onMouseLeave: function(value) {
      this._eventHandlers$1.removeBindEvent(15);
      this.remove__onMouseLeave$1(value);
    },
    add__onChange$1: function(value) {
      this.___onChange$1 = ss.bindAdd(this.___onChange$1, value);
    },
    remove__onChange$1: function(value) {
      this.___onChange$1 = ss.bindSub(this.___onChange$1, value);
    },
    add_onChange: function(value) {
      this._eventHandlers$1.addBindEvent(16);
      this.add__onChange$1(value);
    },
    remove_onChange: function(value) {
      this._eventHandlers$1.removeBindEvent(16);
      this.remove__onChange$1(value);
    },
    add__onSelect$1: function(value) {
      this.___onSelect$1 = ss.bindAdd(this.___onSelect$1, value);
    },
    remove__onSelect$1: function(value) {
      this.___onSelect$1 = ss.bindSub(this.___onSelect$1, value);
    },
    add_onSelect: function(value) {
      this._eventHandlers$1.addBindEvent(17);
      this.add__onSelect$1(value);
    },
    remove_onSelect: function(value) {
      this._eventHandlers$1.removeBindEvent(17);
      this.remove__onSelect$1(value);
    },
    add__onSubmit$1: function(value) {
      this.___onSubmit$1 = ss.bindAdd(this.___onSubmit$1, value);
    },
    remove__onSubmit$1: function(value) {
      this.___onSubmit$1 = ss.bindSub(this.___onSubmit$1, value);
    },
    add_onSubmit: function(value) {
      this._eventHandlers$1.addBindEvent(18);
      this.add__onSubmit$1(value);
    },
    remove_onSubmit: function(value) {
      this._eventHandlers$1.removeBindEvent(18);
      this.remove__onSubmit$1(value);
    },
    add__onKeyDown$1: function(value) {
      this.___onKeyDown$1 = ss.bindAdd(this.___onKeyDown$1, value);
    },
    remove__onKeyDown$1: function(value) {
      this.___onKeyDown$1 = ss.bindSub(this.___onKeyDown$1, value);
    },
    add_onKeyDown: function(value) {
      this._eventHandlers$1.addBindEvent(19);
      this.add__onKeyDown$1(value);
    },
    remove_onKeyDown: function(value) {
      this._eventHandlers$1.removeBindEvent(19);
      this.remove__onKeyDown$1(value);
    },
    add__onKeyPress$1: function(value) {
      this.___onKeyPress$1 = ss.bindAdd(this.___onKeyPress$1, value);
    },
    remove__onKeyPress$1: function(value) {
      this.___onKeyPress$1 = ss.bindSub(this.___onKeyPress$1, value);
    },
    add_onKeyPress: function(value) {
      this._eventHandlers$1.addBindEvent(20);
      this.add__onKeyPress$1(value);
    },
    remove_onKeyPress: function(value) {
      this._eventHandlers$1.removeBindEvent(20);
      this.remove__onKeyPress$1(value);
    },
    add__onKeyUp$1: function(value) {
      this.___onKeyUp$1 = ss.bindAdd(this.___onKeyUp$1, value);
    },
    remove__onKeyUp$1: function(value) {
      this.___onKeyUp$1 = ss.bindSub(this.___onKeyUp$1, value);
    },
    add_onKeyUp: function(value) {
      this._eventHandlers$1.addBindEvent(21);
      this.add__onKeyUp$1(value);
    },
    remove_onKeyUp: function(value) {
      this._eventHandlers$1.removeBindEvent(21);
      this.remove__onKeyUp$1(value);
    },
    add__onError$1: function(value) {
      this.___onError$1 = ss.bindAdd(this.___onError$1, value);
    },
    remove__onError$1: function(value) {
      this.___onError$1 = ss.bindSub(this.___onError$1, value);
    },
    add_onError: function(value) {
      this._eventHandlers$1.addBindEvent(22);
      this.add__onError$1(value);
    },
    remove_onError: function(value) {
      this._eventHandlers$1.removeBindEvent(22);
      this.remove__onError$1(value);
    },
    add__onReady$1: function(value) {
      this.___onReady$1 = ss.bindAdd(this.___onReady$1, value);
    },
    remove__onReady$1: function(value) {
      this.___onReady$1 = ss.bindSub(this.___onReady$1, value);
    },
    add_onReady: function(value) {
      this._eventHandlers$1.addBindEvent(23);
      this.add__onReady$1(value);
    },
    remove_onReady: function(value) {
      this._eventHandlers$1.removeBindEvent(23);
      this.remove__onReady$1(value);
    },
    _triggerEvent: function(eventType, eventObj) {
      var eventHandler = null;
      switch (eventType) {
        case 6:
          eventHandler = this.___onBeforeUnload$1;
          break;
        case 0:
          eventHandler = this.___onBlur$1;
          break;
        case 16:
          eventHandler = this.___onChange$1;
          break;
        case 7:
          eventHandler = this.___onClick$1;
          break;
        case 8:
          eventHandler = this.___onDoubleClick$1;
          break;
        case 22:
          eventHandler = this.___onError$1;
          break;
        case 1:
          eventHandler = this.___onFocus$1;
          break;
        case 19:
          eventHandler = this.___onKeyDown$1;
          break;
        case 20:
          eventHandler = this.___onKeyPress$1;
          break;
        case 21:
          eventHandler = this.___onKeyUp$1;
          break;
        case 2:
          eventHandler = this.___onLoad$1;
          break;
        case 9:
          eventHandler = this.___onMouseDown$1;
          break;
        case 14:
          eventHandler = this.___onMouseEnter$1;
          break;
        case 15:
          eventHandler = this.___onMouseLeave$1;
          break;
        case 11:
          eventHandler = this.___onMouseMove$1;
          break;
        case 13:
          eventHandler = this.___onMouseOut$1;
          break;
        case 12:
          eventHandler = this.___onMouseOver$1;
          break;
        case 10:
          eventHandler = this.___onMouseUp$1;
          break;
        case 23:
          eventHandler = this.___onReady$1;
          break;
        case 3:
          eventHandler = this.___onResize$1;
          break;
        case 4:
          eventHandler = this.___onScroll$1;
          break;
        case 17:
          eventHandler = this.___onSelect$1;
          break;
        case 18:
          eventHandler = this.___onSubmit$1;
          break;
        case 5:
          eventHandler = this.___onUnload$1;
          break;
        default:
          throw new Error("Event with type '" + eventType + "' not handled");
      }
      if (eventHandler != null) {
        eventHandler(this, eventObj);
      }
    },
    isVisible: function() {
      return this.is(26);
    },
    isFocused: function() {
      return this.is(11);
    },
    is: function(psuedoSelector) {
      var selector = GeneralJS_CS_v6$Enums$JQueryEnums.jQueryPseudoSelectorsToString(psuedoSelector);
      return this.get_jQueryObject().is(selector);
    }
  };


  // GeneralJS_CS_v6.Wrappers.jQueryPlugins.QTip.QTip

  function GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTip(element, options) {
    GeneralJS_CS_v6$Interfaces$Impl$Destroyable.call(this);
    this._element$1 = element;
    this._options$1 = options;
    this._init$1();
  }
  GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTip.hideAllQTips = function() {
    var qTips = $('.qtip');
    for (var i = 0; i < qTips.length; i++) {
      (qTips.eq(i)).qtip('hide');
    }
  }
  var GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTip$ = {
    _initOptions$1: function() {
      if (this._options$1 == null) {
        this._options$1 = new GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTipOptions();
      }
      if (this._options$1.options == null) {
        this._options$1.options = {};
      }
    },
    _init$1: function() {
      this._initOptions$1();
      this._initTooltip$1();
    },
    _getStyle$1: function(style, styleValue, cssSuffix) {
      if ((styleValue & style) === style) {
        return 'ui-tooltip' + ((ss.emptyString(cssSuffix)) ? '' : '-' + cssSuffix) + ' ';
      }
      else {
        return '';
      }
    },
    _getStyles$1: function() {
      var css = '';
      css += this._getStyle$1(1, this._options$1.styles, '');
      css += this._getStyle$1(2, this._options$1.styles, 'plain');
      css += this._getStyle$1(4, this._options$1.styles, 'light');
      css += this._getStyle$1(8, this._options$1.styles, 'dark');
      css += this._getStyle$1(16, this._options$1.styles, 'red');
      css += this._getStyle$1(32, this._options$1.styles, 'green');
      css += this._getStyle$1(64, this._options$1.styles, 'blue');
      css += this._getStyle$1(128, this._options$1.styles, 'shadow');
      css += this._getStyle$1(256, this._options$1.styles, 'rounded');
      css += this._getStyle$1(512, this._options$1.styles, 'youtube');
      css += this._getStyle$1(1024, this._options$1.styles, 'jtools');
      css += this._getStyle$1(2048, this._options$1.styles, 'cluetip');
      css += this._getStyle$1(4096, this._options$1.styles, 'tipped');
      css += this._getStyle$1(8192, this._options$1.styles, 'tipsy');
      return css;
    },
    _initTooltip$1: function() {
      var opts = this._options$1.options || {};
      opts.content = {};
      opts.content.text = (ss.emptyString(this._options$1.contentHtml)) ? ' ' : this._options$1.contentHtml;
      if (!ss.emptyString(this._options$1.contentTitle)) {
        opts.content.title = {};
        opts.content.title.text = this._options$1.contentTitle;
      }
      opts.position = opts.position || {};
      if (ss.emptyString(opts.position.my)) {
        opts.position.my = 'left center';
      }
      if (ss.emptyString(opts.position.at)) {
        opts.position.at = 'right center';
      }
      opts.position.viewport = GeneralJS_CS_v6$Util$DomUtil.getWindowElement();
      opts.events = {};
      opts.events.render = ss.bind('_onRenderTooltip$1', this);
      opts.style = opts.style || {};
      opts.style.classes += ' ' + this._getStyles$1();
      if (this._options$1.stayVisibleOnMouseOver) {
        opts.hide = opts.hide || {};
        opts.hide.fixed = true;
        opts.hide.delay = this._options$1.stayVisibleOnMouseOverDelayMs;
      }
      this._element$1.qtip(opts);
    },
    _onRenderTooltip$1: function(e, api) {
      this._qTipElement$1 = new GeneralJS_CS_v6$Wrappers$Elements$MyElement(e.currentTarget);
    },
    _getOption$1: function(name) {
      return this._element$1.qtip('option', name);
    },
    _setOption$1: function(name, value) {
      this._element$1.qtip('option', name, value);
    },
    get_contentText: function() {
      return this._getOption$1('content.text');
    },
    set_contentText: function(value) {
      this._setOption$1('content.text', value);
      return value;
    },
    get_contentTitle: function() {
      return this._getOption$1('content.title.text');
    },
    set_contentTitle: function(value) {
      this._setOption$1('content.title.text', value);
      return value;
    },
    toggle: function(show) {
      this._element$1.qtip('toggle', show);
    },
    show: function() {
      this._element$1.qtip('show');
    },
    hide: function() {
      this._element$1.qtip('hide');
    },
    disable: function() {
      this._element$1.qtip('disable', true);
    },
    enable: function() {
      this._element$1.qtip('enable', true);
    },
    reposition: function() {
      this._element$1.qtip('reposition');
    },
    focus: function() {
      this._element$1.qtip('focus');
    },
    blur: function() {
      this._element$1.qtip('blur');
    },
    get_id: function() {
      return this._getOption$1('id');
    },
    set_id: function(value) {
      this._setOption$1('id', value);
      return value;
    },
    get_rendered: function() {
      return this._getOption$1('rendered');
    },
    destroy: function() {
      this._element$1.qtip('destroy');
      ss.base(this, 'destroy').call(this);
    },
    isVisible: function() {
      return this._qTipElement$1 != null && this._qTipElement$1.isVisible();
    }
  };


  var $exports = ss.module('GeneralJS_CS_v6', null,
    {
      QTipStyle: GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTipStyle,
      XmlHttpRequestStatus: GeneralJS_CS_v6$Enums$XmlHttpRequestStatus,
      JQueryXmlHttpRequestDataType: GeneralJS_CS_v6$Enums$JQueryXmlHttpRequestDataType,
      KeyCode: GeneralJS_CS_v6$Enums$KeyCode,
      JQueryEventType: GeneralJS_CS_v6$Enums$JQueryEventType,
      JQueryPseudoSelector: GeneralJS_CS_v6$Enums$JQueryPseudoSelector,
      JQueryPosition: GeneralJS_CS_v6$Enums$JQueryPosition,
      JQueryUiEffect: GeneralJS_CS_v6$Enums$JQueryUiEffect,
      JQueryEasing: GeneralJS_CS_v6$Enums$JQueryEasing,
      IDestroyable: [ GeneralJS_CS_v6$Interfaces$IDestroyable ],
      Console: [ GeneralJS_CS_v6$Console, GeneralJS_CS_v6$Console$, null ],
      DateUtil: [ GeneralJS_CS_v6$Util$DateUtil, GeneralJS_CS_v6$Util$DateUtil$, null ],
      DomUtil: [ GeneralJS_CS_v6$Util$DomUtil, GeneralJS_CS_v6$Util$DomUtil$, null ],
      FunctionUtil: [ GeneralJS_CS_v6$Util$FunctionUtil, GeneralJS_CS_v6$Util$FunctionUtil$, null ],
      HandlebarsUtil: [ GeneralJS_CS_v6$Util$HandlebarsUtil, GeneralJS_CS_v6$Util$HandlebarsUtil$, null ],
      JsonUtil: [ GeneralJS_CS_v6$Util$JsonUtil, GeneralJS_CS_v6$Util$JsonUtil$, null ],
      WindowUtil: [ GeneralJS_CS_v6$Util$WindowUtil, GeneralJS_CS_v6$Util$WindowUtil$, null ],
      ObjectUtil: [ GeneralJS_CS_v6$Util$ObjectUtil, GeneralJS_CS_v6$Util$ObjectUtil$, null ],
      TypeUtil: [ GeneralJS_CS_v6$Util$TypeUtil, GeneralJS_CS_v6$Util$TypeUtil$, null ],
      UnobtrusiveController: [ GeneralJS_CS_v6$Unobtrusive$UnobtrusiveController, GeneralJS_CS_v6$Unobtrusive$UnobtrusiveController$, null ],
      MyElementEventHandlers: [ GeneralJS_CS_v6$Wrappers$Elements$MyElementEventHandlers, GeneralJS_CS_v6$Wrappers$Elements$MyElementEventHandlers$, null ],
      QTipOptions: [ GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTipOptions, GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTipOptions$, null ],
      ListUtil: [ GeneralJS_CS_v6$Util$ListUtil, GeneralJS_CS_v6$Util$ListUtil$, null ],
      UnobtrusiveBaseController: [ GeneralJS_CS_v6$Unobtrusive$UnobtrusiveBaseController, GeneralJS_CS_v6$Unobtrusive$UnobtrusiveBaseController$, null ],
      UnobtrusiveForm: [ GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveForm, GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveForm$, null ],
      HelpMessageController: [ GeneralJS_CS_v6$Unobtrusive$HelpMessages$HelpMessageController, GeneralJS_CS_v6$Unobtrusive$HelpMessages$HelpMessageController$, null ],
      UnobtrusiveForms: [ GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveForms, GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveForms$, null ],
      UnobtrusiveField: [ GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveField, GeneralJS_CS_v6$Unobtrusive$Fields$UnobtrusiveField$, null ],
      SlickGridAjaxDataController: [ GeneralJS_CS_v6$UI$JQuery$Plugins$SlickGrid$SlickGridAjaxDataController, GeneralJS_CS_v6$UI$JQuery$Plugins$SlickGrid$SlickGridAjaxDataController$, null ],
      DeleteFileItemResponse: [ GeneralJS_CS_v6$UI$Controls$FileUpload$DeleteFileItemResponse, GeneralJS_CS_v6$UI$Controls$FileUpload$DeleteFileItemResponse$, null ],
      DeleteFileItemController: [ GeneralJS_CS_v6$UI$Controls$FileUpload$DeleteFileItemController, GeneralJS_CS_v6$UI$Controls$FileUpload$DeleteFileItemController$, null ],
      CommonControllers: [ GeneralJS_CS_v6$UI$Common$CommonControllers, GeneralJS_CS_v6$UI$Common$CommonControllers$, null ],
      Destroyable: [ GeneralJS_CS_v6$Interfaces$Impl$Destroyable, GeneralJS_CS_v6$Interfaces$Impl$Destroyable$, null, GeneralJS_CS_v6$Interfaces$IDestroyable ],
      GenericEnums: [ GeneralJS_CS_v6$Enums$GenericEnums, GeneralJS_CS_v6$Enums$GenericEnums$, null ],
      JQueryEnums: [ GeneralJS_CS_v6$Enums$JQueryEnums, GeneralJS_CS_v6$Enums$JQueryEnums$, null ],
      FieldAttributes: [ GeneralJS_CS_v6$Constants$FieldAttributes, GeneralJS_CS_v6$Constants$FieldAttributes$, null ],
      Class1: [ GeneralJS_CS_v6$Class1, GeneralJS_CS_v6$Class1$, null ],
      HelpMessageControllers: [ GeneralJS_CS_v6$Unobtrusive$HelpMessages$HelpMessageControllers, GeneralJS_CS_v6$Unobtrusive$HelpMessages$HelpMessageControllers$, null ],
      MyXmlHttpRequest: [ GeneralJS_CS_v6$Wrappers$Xhr$MyXmlHttpRequest, GeneralJS_CS_v6$Wrappers$Xhr$MyXmlHttpRequest$, null ],
      MyElement: [ GeneralJS_CS_v6$Wrappers$Elements$MyElement, GeneralJS_CS_v6$Wrappers$Elements$MyElement$, GeneralJS_CS_v6$Interfaces$Impl$Destroyable ],
      QTip: [ GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTip, GeneralJS_CS_v6$Wrappers$jQueryPlugins$QTip$QTip$, GeneralJS_CS_v6$Interfaces$Impl$Destroyable ]
    });

  GeneralJS_CS_v6$Constants$FieldAttributes.helpMessage = 'data-help-msg';
  GeneralJS_CS_v6$Constants$FieldAttributes.helpMessageElement = 'data-help-msg-element';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationField = 'data-csval-field';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationElement = 'data-csval-validation-element';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationRequired = 'data-csval-required';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationRemote = 'data-csval-remote';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationIdentifier = 'data-csval-identifier';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationEmailAddress = 'data-csval-email';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationNumeric = 'data-csval-numeric';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationEqualTo = 'data-csval-equal-to';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationUrl = 'data-csval-url';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationMinLength = 'data-csval-min-length';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationDate = 'data-csval-date';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationRangeLength = 'data-csval-range-length';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationMaxLength = 'data-csval-max-length';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationMinValue = 'data-csval-min-value';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationRangeValue = 'data-csval-range-value';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationRegexPattern = 'data-csval-regex-pattern';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationMaxValue = 'data-csval-max-value';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationPassword = 'data-csval-password';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationIntegersOnly = 'data-csval-integers-only';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationErrorMessageSuffix = 'errmsg';
  GeneralJS_CS_v6$Constants$FieldAttributes.validationRequiredDependency = 'data-csval-required-dependency';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodRequired = 'required';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodMinLength = 'minlength';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodMaxLength = 'maxlength';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodRangeLength = 'rangelength';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodEqualTo = 'equalTo';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodCreditCard = 'creditcard';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodRemote = 'remote';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodMin = 'min';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodMax = 'max';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodRange = 'range';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodNumber = 'number';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodDate = 'date';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodDigits = 'digits';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodEmail = 'email';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodUrl = 'url';
  GeneralJS_CS_v6$Constants$FieldAttributes.jQueryValidationMethodRegexPattern = 'pattern';


  $global.GeneralJS_CS_v6 = $exports;
