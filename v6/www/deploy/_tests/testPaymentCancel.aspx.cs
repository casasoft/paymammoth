﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth_v6.Util;

namespace PayMammoth_v6Deploy._tests
{
    public partial class testPaymentFailure : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        
       {
            HashSet<string> testSet = new HashSet<string>();
            testSet.Add("karl");
            testSet.Add("mark");
            testSet.Add("andre");
            testSet.Add("karl");
            testSet.Add("mark");
            testSet.Add("andre");
            testSet.Add("karl");
            testSet.Add("mark");
            testSet.Add("andre");
            testSet.Add("karl");
            testSet.Add("mark");
            testSet.Add("andre");
            testSet.Add("karl");
            testSet.Add("mark");
            testSet.Add("andre");
            testSet.Add("karl");
            testSet.Add("mark");
            testSet.Add("andre");
            TestAspectsUtil.test(testSet);
        }
    }
}