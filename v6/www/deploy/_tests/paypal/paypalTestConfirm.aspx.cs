﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using CS.General_CS_v6.Util;
using NLog;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1;
using Raven.Client.Linq;

namespace PayMammoth_v6Deploy._tests.paypal
{
    public partial class paypalTestConfirm : System.Web.UI.Page
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {

            




            _log.Info("paypalTestConfirm [Start]");

            btnConfirm.Click += btnConfirm_Click;
            int k = 5;
        }

        void btnConfirm_Click(object sender, EventArgs e)
        {
            string identifier = Request.QueryString["identifier"];
            string tokenId = Request.QueryString["token"];
            string payer = Request.QueryString["PayerID"];

            var paypalManager = InversionUtil.Get<IPayPalManager>();
            var result = paypalManager.ConfirmPayment(new ReferenceLink<PaymentRequestData>(identifier),
                tokenId, payer);
            lblStatus.Text = string.Format("Confirm complete! Result: {0}", result);
        }
    }
}