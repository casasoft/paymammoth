﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using CS.General_CS_v6;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v6Deploy._tests.paypal
{
    public partial class testNewPayment : System.Web.UI.Page
    {
        private IRavenDbService _ravenDbService;
        private IDataObjectFactory _dataObjectFactory;
        private IPaymentRequestDataService _paymentRequestDataService;

        private WebsiteAccountData createWebsiteAccount(IDocumentSession session)
        {
            WebsiteAccountData testWebsite = _dataObjectFactory.CreateNewDataObject<WebsiteAccountData>();
            testWebsite.WebsiteName = "TestWebsiteAccount-" + DateTime.Now.Ticks;
            testWebsite.PaymentsInformation.PayPal.Enabled = true;
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.MerchantEmail = "paypal.cs.test.business.en@dispostable.com";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.MerchantId = "SAN5FMKHMTM9C";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.Password = "1389117990";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.Signature = "AxRMHSZhKz2kBtwBMbnqHM0tDC7XA8hhARUerqhHUKWfOaz0b1MN80Zd";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.Username = "paypal.cs.test.business.en_api1.dispostable.com";
            testWebsite.PaymentsInformation.PayPal.UseLiveEnvironment = false;
            session.Store(testWebsite);
            return testWebsite;

        }
     
        

        protected void Page_Load(object sender, EventArgs e)
        {
            _dataObjectFactory = InversionUtil.Get<IDataObjectFactory>();
            _ravenDbService = InversionUtil.Get<IRavenDbService>();
            _paymentRequestDataService = InversionUtil.Get<IPaymentRequestDataService>();
            var session = _ravenDbService.CreateNewSession();

            var websiteAccount = createWebsiteAccount(session);

            session.SaveChanges();
            session.Dispose();
            session = _ravenDbService.CreateNewSession();

        }
    }
}