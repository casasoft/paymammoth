﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testPaymentSuccess.aspx.cs" Inherits="PayMammoth_v6Deploy._tests.testPaymentSuccess" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Test Payment Success - PayMammoth</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        This page check the status of a particular test payment, if it was paid.  Since this is done on a background thread, refresh this page to get the latest status.
    </div>
        <hr/>
        Status: <br/>
        <asp:TextBox ID="txtStatus" runat="server" TextMode="MultiLine" Height="300" Columns="80"></asp:TextBox>
    </form>
</body>
</html>
