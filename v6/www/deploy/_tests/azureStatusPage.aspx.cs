﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMammoth_v2Deploy._tests
{
    public partial class azureStatusPage : System.Web.UI.Page
    {

        public static long GetDirectorySize(string path)
        {
            // 1.
            // Get array of all file names.
            string[] a = Directory.GetFiles(path, "*.*");

            // 2.
            // Calculate total bytes of all files in a loop.
            long b = 0;
            foreach (string name in a)
            {
                // 3.
                // Use FileInfo to get length of each file.
                FileInfo info = new FileInfo(name);
                b += info.Length;
            }
            // 4.
            // Return total size
            return b;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("RoleRoot: " + Environment.GetEnvironmentVariable("RoleRoot") + " <br />");
            sb.AppendLine("RdRoleRoot: " + Environment.GetEnvironmentVariable("RdRoleRoot") + " <br />");
            sb.AppendLine("RdRoleId: " + Environment.GetEnvironmentVariable("RdRoleId") + " <br />");
            sb.AppendLine("RoleName: " + Environment.GetEnvironmentVariable("RoleName") + " <br />");
            sb.AppendLine("RoleInstanceID: " + Environment.GetEnvironmentVariable("RoleInstanceID") + " <br />");
            sb.AppendLine("RoleDeploymentID: " + Environment.GetEnvironmentVariable("RoleDeploymentID") + " <br />");
            sb.AppendLine("WEBSITE_SITE_NAME: " + Environment.GetEnvironmentVariable("WEBSITE_SITE_NAME") + " <br />");
            sb.AppendLine("MachineName: " + Environment.MachineName + " <br />");
            sb.AppendLine("OSVersion: " + Environment.OSVersion + " <br />");
            sb.AppendLine("=======================" + " <br />");
            sb.AppendLine("App_Data Size: " + GetDirectorySize(Server.MapPath("/App_Data/")) + " bytes");
            divText.InnerHtml = sb.ToString();
        }
    }
}