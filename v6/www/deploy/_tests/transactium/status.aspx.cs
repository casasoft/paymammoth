﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.HelperClasses;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6Deploy._tests.transactium
{
    public partial class status : System.Web.UI.Page
    {

        public string TransactionId
        {
            get
            {
                return this.Request.QueryString[Constants.PARAM_IDENTIFIER];
            }
        }
        public string HpsId
        {
            get
            {
                return this.Request.QueryString[ConstantsTransactium.HPSID];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var transactiumService= InversionUtil.Get<ITransactiumService>();
            var result = transactiumService.CheckTransactiumPayment(new ReferenceLink<PaymentRequestTransactionData>(TransactionId), HpsId);

            spanStatus.InnerText = result.Result.ToString();

            
        }
    }
}