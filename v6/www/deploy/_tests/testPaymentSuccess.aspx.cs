﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Modules.Data.TestData;
using Raven.Client;

namespace PayMammoth_v6Deploy._tests
{
    public partial class testPaymentSuccess : System.Web.UI.Page
    {
        private IRavenDbService _ravenDbService;
        private IDocumentSession _session;

        
        public testPaymentSuccess()
        {
            _ravenDbService = InversionUtil.Get<IRavenDbService>();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _session = _ravenDbService.CreateNewSession();

            var testPaymentId = Request.QueryString["TestPaymentId"];
            TestPaymentData testPayment = null;
            if (!string.IsNullOrWhiteSpace(testPaymentId))
            {
                var refLink = new ReferenceLink<TestPaymentData>(testPaymentId);
                testPayment = _session.Load<TestPaymentData>(refLink);

            }
            if (testPayment != null)
            {
                if (testPayment.Status == TestPaymentData.TestPaymentStatus.Pending)
                {
                    System.Threading.Thread.Sleep(2000);
                }
                txtStatus.Text =
                    string.Format(
@"
Status: {0}
Title: {1}
MarkedAsPaidOn: {2}

StatusLog:
{3}
", testPayment.Status, testPayment.Title, testPayment.MarkedAsPaidOn, testPayment.StatusLog);


            }
            else
            {
                txtStatus.Text = string.Format("TestPayment with id '{0}' could not be found", testPaymentId);
            }


            _session.Dispose();

        }
    }
}