﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMammoth_v2Deploy._tests.cacheTEsts
{
    public partial class testCaching : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpRuntime.Cache.Remove("karl");
            HttpRuntime.Cache.Remove(null);
            object data = null;
            HttpRuntime.Cache.Add("karl", data, null, System.Web.Caching.Cache.NoAbsoluteExpiration,
                System.Web.Caching.Cache.NoSlidingExpiration,
                 CacheItemPriority.Normal, null);


        }
    }
}