﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testPaymentCancel.aspx.cs" Inherits="PayMammoth_v6Deploy._tests.testPaymentFailure" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Test Payment Failure</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    This page signifies that it was cancelled. This should only happen normally IF the user presses cancel.
    </div>
    </form>
</body>
</html>
