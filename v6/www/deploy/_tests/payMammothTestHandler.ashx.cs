﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.Services;

namespace PayMammoth_v6Deploy._tests
{
    /// <summary>
    /// This is a test handler which would be implemented on the client website, e.g JK Villas.
    /// </summary>
    public class payMammothTestHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {

            var notificationsHandlerService = InversionUtil.Get<INotificationsHandlerService>();

            string response = Constants.RESPONSE_ERROR;

            string data = CS.General_CS_v6.Util.StreamUtil.ReadStreamAsStringToEnd(context.Request.InputStream);
            response = notificationsHandlerService.HandleNotificationResponseData(data);
            context.Response.Clear();
            context.Response.Write(response);
            context.Response.End();
            

        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}