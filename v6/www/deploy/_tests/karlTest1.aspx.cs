﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Fields;
using CS.General_CS_v6;
using CS.General_CS_v6.Util;

namespace PayMammoth_v6Deploy._tests
{
    public partial class karlTest1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var ravenDbService = InversionUtil.Get<BusinessLogic_CS_v6.Modules.Services.RavenDb.IRavenDbService>();
            var session = ravenDbService.CreateNewSession();
            var paymentUrlModelService = InversionUtil.Get<PayMammoth_v6.Presentation.Services.Payments.IPaymentUrlModelService>();
            CultureModel cultureModel = new CultureModel();
            cultureModel.CountryDataModel = new Iso3166_CountryDataModel() { Identifier = "MLT", TwoLetterCode = "MT", Title = new FieldModel<string>() { Value = "Malta" } };
            cultureModel.CurrencyDataModel = new Iso4217_CurrencyDataModel() { Identifier = "EUR", Symbol = "E", Title = new FieldModel<string>() { Value = "Euro" } };


            //var selectionUrl = paymentUrlModelService.GetPaymentSelectionPage(session, cultureModel, "PaymentRequestData/74a2346266a54621889f05ee54f22ead");

            //Response.Redirect(selectionUrl);

            session.Dispose();
            int k = 5;
        }
    }
}