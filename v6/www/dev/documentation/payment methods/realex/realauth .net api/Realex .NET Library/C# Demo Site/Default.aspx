<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Dovetail.Realex.DemoSite.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Realex .Net Payment Processing Examples</title>
    <style type="text/css">
        #main
        {
            border-style: solid;
            border-color: #FF9933;
            width: 550px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="main">
        <h1>
            Realex Integration Examples</h1>
        <p>
            This application demonstates methods for integrating Microsoft .Net applications
            with the Realex Realauth Service. The Realauth service works under two different
            scenarios: <b>Redirect</b> and <b>Remote</b>.</p>
        <h2>
            Redirect</h2>
        <p>
            The <b>Redirect</b> method is suitable for merchants that do not have their own
            secure server. Using this method the customer will be redirected to Realex's secure
            server to enter their credit card details.
            <br />
            <a href="Redirect/Redirect.aspx">Click here for the Redirect example</a></p>
        <h2>
            Remote</h2>
        <p>
            The <b>Remote</b> method affords the merchant greater control of the transaction
            process but requires that they maintain their own secure server. Using this method,
            the customers’ card details will be taken by the merchant and passed to Realex by
            XML messages.</p>
        <p>
            <b>Remote</b> can be used in two ways: using <b>3-D Secure</b> or not. This application
            provides examples of both.</p>
        <p>
            The first example demonstrates the use of <b>Remote</b> without 3-D Secure. It demonstrates
            the full range of operations including payment authorisation, processing rebates,
            voiding transactions and deferred settlement.
            <br />
            <a href="Remote/Remote.aspx">Click here for the normal <b>Remote</b> example</a>
        </p>
        <p>
            3-D Secure is only applicable to payment authorisations, so this example only demonstrates
            a normal payment authorisation.
            <br />
            <a href="MPI/MpiRequest.aspx">Click here for the <b>3-D Secure</b> example</a></p>
    </div>
    </form>
</body>
</html>
