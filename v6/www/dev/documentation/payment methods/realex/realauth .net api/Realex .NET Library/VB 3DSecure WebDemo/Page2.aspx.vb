﻿Imports Dovetail.Realex.RealAuth.Mpi

Partial Public Class Page2
    Inherits RealexMpiResponsePageBase

    Protected Overrides ReadOnly Property SharedSecret() As String
        Get
            Return "XXXXX"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' calling "TryAuthorise" starts the MPI process which decides
        ' which actions to take on the results which are returned from the
        ' Realex servers and the clients "Risk" value (i.e, the maximum value
        ' the client is willing to risk being charged back when an MPI transaction
        ' is not available

        Dim mpiResult As MpiResult = TryAuthorise()
        Select Case mpiResult
            Case mpiResult.Authorised
                ' the users credit card has been authorised for the amount
                ' specified in the initial request, log the details of the
                ' transaction to database
                Exit Select
            Case mpiResult.Rejected
                ' based on the response from the ACS, an MPI transaction is not possible
                ' and therefore no liability shift is possible. The library has determined that
                ' the value of the transaction is greater than the level of liability the merchant will accept
                Exit Select
            Case mpiResult.CardDeclined
                ' the system attempted to authorise the credit card but it was declined
                ' for the amount requested
                Exit Select
            Case mpiResult.[Error]
                ' an error has occured from which the system cannot recover
                Exit Select
        End Select
        Response.Write("Result: " & mpiResult.ToString & "<br>")
        Response.Write("Scenario: " & MpiScenario & "<br>")
        Response.Write("Result code: " & Processor.Response.ResultCode & "<br>")
        Response.Write("Result message: " & Processor.Response.ResponseMessage & "<br>")
        Response.Write("Time now: " & DateTime.Now.ToString("d/MM/yyyy HH:mm:ss") & "<br>")

    End Sub

End Class