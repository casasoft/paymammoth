﻿Imports Dovetail.Realex.RealAuth.Mpi

Partial Public Class Page1
    Inherits RealexMpiRequestPageBase

    Public Overrides ReadOnly Property Body() As HtmlGenericControl
        Get
            Return xbody
        End Get
    End Property

    Public Overrides ReadOnly Property Head() As HtmlGenericControl
        Get
            Return xhead
        End Get
    End Property

    Public Overrides ReadOnly Property ResponseUrl() As String
        Get
            Return "http://localhost:21296/Page2.aspx"
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Processor.Request.MerchantId = "XXXXX"
        Processor.Request.SharedSecret = "XXXXX"
        Processor.Request.AccountName = "XXXXX"

        ' in a real application the following would be populated by the user through the user interface
        Processor.Request.CardHolderName = "John Smith"
        Processor.Request.CardNumber = "4012001037141112"
        Processor.Request.CardExpDate = "0519"  ' may 2019
        Processor.Request.CardType = CardType.Visa
        Processor.Request.Amount = 100   ' in cent, not euro
        Processor.Request.Currency = Currency.EUR

        ' in a real application the order id would typically come frome the Orders table of a db
        MyBase.Processor.Request.OrderId = DateTime.Now.ToString("yyyyMMddhhmmssffff")

        Dim mpiResult As MpiResult = TryAuthorise()
        Select Case mpiResult
            Case mpiResult.Authorised
                ' the users credit card has been authorised.  An MPI transaction was not possible, probably because the 
                ' card holder was not enrolled.
                Exit Select
            Case mpiResult.ProceedToAccessControlServer
                ' the system has received data from Realex indicating that an MPI transaction is possible and provided 
                ' enough information to transfer the next step of the transaction to the card's issuing bank's
                ' Access Control Server (ACS).  As a developer you don't need to do anything here: the base class will 
                ' handle everything for you, and your Response page will be called later where you can handle the final result.
                Exit Select
            Case mpiResult.Rejected
                ' based on the response from the ACS no liability shift was possible.  The library has determined that
                ' the value of the transaction is greater than MaximumLiabilityAccepted so it has not been processed
                Exit Select
            Case mpiResult.CardDeclined
                ' the system attempted to authorise the credit card but it was declined
                Exit Select
            Case mpiResult.[Error]
                ' an error has occured
                Exit Select
        End Select

        Response.Write("Result: " & mpiResult.ToString & "<br>")
        Response.Write("Scenario: " & MpiScenario & "<br>")
        Response.Write("Result code: " & Processor.Response.ResultCode & "<br>")
        Response.Write("Result message: " & Processor.Response.ResponseMessage & "<br>")
        Response.Write("Time now: " & DateTime.Now.ToString("d/MM/yyyy HH:mm:ss") & "<br>")
    End Sub

End Class