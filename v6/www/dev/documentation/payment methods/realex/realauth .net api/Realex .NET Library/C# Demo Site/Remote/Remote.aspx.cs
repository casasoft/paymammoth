﻿using System;
using System.Web.Configuration;
using Dovetail.Realex.Realauth;
using System.Web.UI;

namespace Dovetail.Realex.DemoSite.Remote
{
    public partial class Remote : Page
    {
        #region Main Realex request-processing methods.  These are the heart of the sample code.

        /// <summary>
        /// This method demonstrate basic usage of the .Net library
        /// </summary>
        private void ProcessAuthRequest()
        {
            // Create a quote processor object
            RealexProcessor p = new RealexProcessor();

            // populate the request
            RealexRequest r = p.Request;
            r.RequestType = RequestType.Auth;
            r.MerchantId = _merchantID;             // supplied by Realex
            r.SharedSecret = _sharedSecret;         // supplied by Realex
            r.AccountName = _accountName;           // supplied by Realex
            r.OrderId = NewOrderID();               // any unique value.  Usually the Primary Key from your Orders table.  For this sample we just make a unique string.
            r.Amount = decimal.ToInt16(decimal.Parse(txtAmount.Text) * 100);    // amount must be provided in CENT if using Euro, so multiply the supplied value by 100.
            r.Currency = Currency.EUR;              // currency
            r.CardHolderName = txtCardholder.Text;  // the cardholders name
            r.CardNumber = txtNumber.Text;          // the cardnumber
            r.CardExpDate = txtExpiryDate.Text;     // expiry date in MMYY format.  e.g. May 2012 is "0512"
            r.CardType = (CardType)Enum.Parse(typeof(CardType), ddlCardType.SelectedValue, true); // this is an example of how to parse a string value into the CardType enum.  To hardcode this just use "CardType.Visa" for example.
            r.CardVerificationNumber = txtCVV.Text; // three digit verification number
            r.Autosettle = chkAutosettle.Checked;   // whether to automatically send the transaction to the bank for settlement in the next batch (tonight)

            // issue the request.  This call is synchronous.
            p.SendRequest();

            // display the response.  Based on the ResponseCode you may update a customer's order as paid etc. "00" indicates a successful payment.
            RealexResponse rp = p.Response;
            lblResult.Text = rp.ResultCode;
            lblMessage.Text = rp.ResponseMessage;
            lblAuthCode.Text = rp.AuthCode;
            lblPasRef.Text = rp.PasRef;
            lblOrderID.Text = r.OrderId;
        }

        private void ProcessManualRequest()
        {
            // Create a quote processor object
            RealexProcessor rp = new RealexProcessor();

            // populate the request
            RealexRequest r = rp.Request;
            r.MerchantId = _merchantID;
            r.SharedSecret = _sharedSecret;
            r.AccountName = _accountName;
            r.OrderId = NewOrderID();
            r.Amount = decimal.ToInt16(decimal.Parse(txtAmount.Text) * 100);
            r.Currency = Currency.EUR;
            r.CardHolderName = txtCardholder.Text;
            r.CardNumber = txtNumber.Text;
            r.CardExpDate = txtExpiryDate.Text;
            r.CardType = (CardType)Enum.Parse(typeof(CardType), ddlCardType.SelectedValue, true);
            r.CardVerificationNumber = txtCVV.Text;
            r.Autosettle = true;
            r.Authcode = txtAuthcode.Text;  // In a Manual Request, this is supplied by the bank's acquisition centre

            // issue the request
            rp.SendRequest();

            // display the response
            RealexResponse response = rp.Response;
            lblResult.Text = response.ResultCode;
            lblMessage.Text = response.ResponseMessage;
            lblAuthCode.Text = response.AuthCode;
            lblOrderID.Text = r.OrderId;
        }

        private void ProcessOfflineRequest()
        {
            // Create a quote processor object
            RealexProcessor p = new RealexProcessor();
            RealexRequest r = p.Request;
            r.RequestType = RequestType.Offline;
            r.MerchantId = _merchantID;
            r.SharedSecret = _sharedSecret;
            r.AccountName = _accountName;
            r.OrderId = txtOrderID.Text; // for the offline request we don't need a new OrderID, just reuse the one submitted.
            r.Pasref = txtPasref.Text;
            r.Authcode = txtAuthcode.Text;
            r.Comment1 = "This is an Offline request for Order ID " + txtOrderID.Text;
            p.SendRequest();

            // display the response
            RealexResponse rp = p.Response;
            lblResult.Text = rp.ResultCode;
            lblMessage.Text = rp.ResponseMessage;
            lblAuthCode.Text = rp.AuthCode;
            lblPasRef.Text = rp.PasRef;
            lblOrderID.Text = r.OrderId;
        }

        private void ProcessRebateRequest()
        {
            RealexProcessor p = new RealexProcessor();
            RealexRequest r = p.Request;
            r.RequestType = RequestType.Rebate;

            r.MerchantId = _merchantID;
            r.SharedSecret = _sharedSecret;
            r.AccountName = _accountName;
            r.Amount = decimal.ToInt16(decimal.Parse(txtAmount.Text) * 100);
            r.OrderId = txtOrderID.Text;
            r.Authcode = txtAuthcode.Text;
            r.Pasref = txtPasref.Text;
            r.Autosettle = true;
            r.RefundPassword = _rebatePassword;
            r.Comment1 = "Test rebate";

            p.SendRequest();

            // display the response
            RealexResponse rp = p.Response;
            lblResult.Text = rp.ResultCode;
            lblMessage.Text = rp.ResponseMessage;
            lblAuthCode.Text = rp.AuthCode;
            lblPasRef.Text = rp.PasRef;
            lblOrderID.Text = r.OrderId;

        }

        private void ProcessVoidRequest()
        {
            RealexProcessor p = new RealexProcessor();
            RealexRequest r = p.Request;
            r.RequestType = RequestType.Void;

            r.MerchantId = _merchantID;
            r.SharedSecret = _sharedSecret;
            r.AccountName = _accountName;
            r.OrderId = txtOrderID.Text;
            r.Authcode = txtAuthcode.Text;
            r.Pasref = txtPasref.Text;
            r.Comment1 = "Voiding Order " + txtOrderID.Text;

            p.SendRequest();

            // display the response
            RealexResponse rp = p.Response;
            lblResult.Text = rp.ResultCode;
            lblMessage.Text = rp.ResponseMessage;
            lblAuthCode.Text = rp.AuthCode;
            lblPasRef.Text = rp.PasRef;
            lblOrderID.Text = r.OrderId;

        }

        private void ProcessRealscoreRequest()
        {
            // Create a quote processor object
            RealexProcessor p = new RealexProcessor();

            // populate the request
            RealexRequest r = p.Request;
            r.RequestType = RequestType.Realscore;
            r.MerchantId = _merchantID;
            r.SharedSecret = _sharedSecret;
            r.AccountName = _accountName;
            r.OrderId = NewOrderID();
            r.Amount = decimal.ToInt16(decimal.Parse(txtAmount.Text) * 100);
            r.Currency = Currency.EUR;
            r.CardHolderName = txtCardholder.Text;
            r.CardNumber = txtNumber.Text;
            r.CardExpDate = txtExpiryDate.Text;
            r.CardType = (CardType)Enum.Parse(typeof(CardType), ddlCardType.SelectedValue, true);
            r.CardVerificationNumber = txtCVV.Text;
            r.Autosettle = true;
            r.Comment1 = "Testing Realscore request";

            // issue the request
            p.SendRequest();

            // display the response
            RealexResponse rp = p.Response;
            lblResult.Text = rp.ResultCode;
            lblMessage.Text = rp.ResponseMessage;
            lblAuthCode.Text = rp.AuthCode;
            lblPasRef.Text = rp.PasRef;
            lblOrderID.Text = r.OrderId;

        }

        private void ProcessSettleRequest()
        {
            // Create a quote processor object
            RealexProcessor p = new RealexProcessor();

            // populate the request
            RealexRequest r = p.Request;
            r.RequestType = RequestType.Settle;
            r.MerchantId = _merchantID;
            r.SharedSecret = _sharedSecret;
            r.AccountName = _accountName;
            r.OrderId = txtOrderID.Text;
            r.Pasref = txtPasref.Text;
            r.Authcode = txtAuthcode.Text;
            r.Comment1 = "Settling order " + txtOrderID;

            // issue the request
            p.SendRequest();

            // display the response
            RealexResponse rp = p.Response;
            lblResult.Text = rp.ResultCode;
            lblMessage.Text = rp.ResponseMessage;
            lblAuthCode.Text = rp.AuthCode;
            lblPasRef.Text = rp.PasRef;
            lblOrderID.Text = r.OrderId;

        }

        private static string NewOrderID()
        {
            /* Order ID must be unique across all your Realex accounts.  Using the time to the second plus 
             * a random number will pretty much guarantee this for testing purposes.
             * In the real world you will probably want to use something more meaningful such 
             * as the Primary Key column from your Orders table.
             * */

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            Random r = new Random();
            int rand = r.Next(1, 1000);
            return timestamp + rand;
        }

        #endregion

        #region Form event handlers and presentation methods

        protected void Page_Load(object sender, EventArgs e)
        {
            // default to Auth requests
            if (!IsPostBack)
            {
                rblRequestType.SelectedValue = "Auth";
                SetVisibleControls(RequestType.Auth);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ResetResponsePopulatedTextboxes();
        }

        protected void rblRequestType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // When the user changes the request type we'll show/hide controls as appropiate
            RequestType rt = (RequestType)Enum.Parse(typeof(RequestType), rblRequestType.SelectedValue, true);
            SetVisibleControls(rt);
        }

        protected void btnSendRequest_Click(object sender, EventArgs e)
        {
            // Call the appropiate method depending on request type submitted

            RequestType rt = (RequestType)Enum.Parse(typeof(RequestType), rblRequestType.SelectedValue, true);
            switch (rt)
            {
                case RequestType.Auth:
                    ProcessAuthRequest();
                    break;
                case RequestType.Offline:
                    ProcessOfflineRequest();
                    break;
                case RequestType.Manual:
                    ProcessManualRequest();
                    break;
                case RequestType.Rebate:
                    ProcessRebateRequest();
                    break;
                case RequestType.Void:
                    ProcessVoidRequest();
                    break;
                case RequestType.Realscore:
                    ProcessRealscoreRequest();
                    break;
                case RequestType.Settle:
                    ProcessSettleRequest();
                    break;
                default:
                    break;
            }

            lblTimestamp.Text = DateTime.Now.ToString("d/MM/yyyy HH:mm:ss");

        }

        private void SetVisibleControls(RequestType requestType)
        {
            // first we'll just hide everything
            pnlAmount.Visible = false;
            pnlCardDetails.Visible = false;
            pnlAuthCode.Visible = false;
            pnlOrderID.Visible = false;
            pnlPasRef.Visible = false;
            pnlAutosettle.Visible = false;

            pnlRequestInfoAuth.Visible = false;
            pnlRequestInfoManual.Visible = false;
            pnlRequestInfoOffline.Visible = false;
            pnlRequestInfoVoid.Visible = false;
            pnlRequestInfoSettle.Visible = false;
            pnlRequestInfoRealscore.Visible = false;
            pnlRequestInfoRebate.Visible = false;

            // now just show the controls we need for the current Request Type
            switch (requestType)
            {
                case RequestType.Auth:
                    pnlAmount.Visible = true;
                    pnlCardDetails.Visible = true;
                    pnlRequestInfoAuth.Visible = true;
                    pnlAutosettle.Visible = true;
                    break;
                case RequestType.Manual:
                    pnlAmount.Visible = true;
                    pnlCardDetails.Visible = true;
                    pnlAuthCode.Visible = true;
                    pnlRequestInfoManual.Visible = true;
                    pnlAutosettle.Visible = true;
                    break;
                case RequestType.Offline:
                    pnlPasRef.Visible = true;
                    pnlOrderID.Visible = true;
                    pnlAuthCode.Visible = true;
                    pnlRequestInfoOffline.Visible = true;
                    break;
                case RequestType.Rebate:
                    pnlAmount.Visible = true;
                    pnlOrderID.Visible = true;
                    pnlPasRef.Visible = true;
                    pnlAuthCode.Visible = true;
                    pnlRequestInfoRebate.Visible = true;
                    pnlAutosettle.Visible = true;
                    break;
                case RequestType.Void:
                    pnlPasRef.Visible = true;
                    pnlOrderID.Visible = true;
                    pnlAuthCode.Visible = true;
                    pnlRequestInfoVoid.Visible = true;
                    break;
                case RequestType.Realscore:
                    pnlAmount.Visible = true;
                    pnlCardDetails.Visible = true;
                    pnlRequestInfoRealscore.Visible = true;
                    break;
                case RequestType.Settle:
                    pnlPasRef.Visible = true;
                    pnlOrderID.Visible = true;
                    pnlAuthCode.Visible = true;
                    pnlRequestInfoSettle.Visible = true;
                    break;
                default:
                    // unrecognised request type
                    throw new ArgumentOutOfRangeException("requestType");
            }
        }

        private void ResetResponsePopulatedTextboxes()
        {
            /* it makes things clearer for the user if we clear the boxes for AuthCode, PasRef and OrderID between transactions.
             * On the otherhand we'll leave the other boxes populated (such as Cardnumber and Cardholder) so that the user can 
             * submit multiple transactions without having to repopulate the fields each time.
             */
            txtAuthcode.Text = txtOrderID.Text = txtPasref.Text = string.Empty;
        }


        #endregion


        #region member variables for config settings

        // get the Realex settings from the web config file
        readonly string _merchantID = WebConfigurationManager.AppSettings["RealexMerchantID"];
        readonly string _sharedSecret = WebConfigurationManager.AppSettings["RealexSharedSecret"];
        readonly string _rebatePassword = WebConfigurationManager.AppSettings["RealexRebatePassword"];
        readonly string _accountName = WebConfigurationManager.AppSettings["RealexAccountName"];

        #endregion


        //DELETE_FROM_HERE
        // this section of code will be deleted before shipping to Realex
        #region Trevor's shortcuts for testing

        protected void btnSuccess_Click(object sender, EventArgs e)
        {
            rblRequestType.SelectedValue = "Auth";
            txtAmount.Text = "123.45";
            txtCardholder.Text = "Somebody A Tester";
            txtNumber.Text = "4263971921001307";
            txtCVV.Text = "123";
            txtExpiryDate.Text = "0510";
        }

        protected void btnDeclined_Click(object sender, EventArgs e)
        {
            rblRequestType.SelectedValue = "Auth";
            txtAmount.Text = "123.45";
            txtCardholder.Text = "Somebody A Tester";
            txtNumber.Text = "4000126842489127";
            txtCVV.Text = "123";
            txtExpiryDate.Text = "0510";
        }

        protected void btnReferral_Click(object sender, EventArgs e)
        {
            rblRequestType.SelectedValue = "Auth";
            txtAmount.Text = "123.45";
            txtCardholder.Text = "Somebody A Tester";
            txtNumber.Text = "4000136842489878";
            txtCVV.Text = "123";
            txtExpiryDate.Text = "0510";
        }

        protected void btnCommsError_Click(object sender, EventArgs e)
        {
            rblRequestType.SelectedValue = "Auth";
            txtAmount.Text = "123.45";
            txtCardholder.Text = "Somebody A Tester";
            txtNumber.Text = "4009837983422344";
            txtCVV.Text = "123";
            txtExpiryDate.Text = "0510";
        }

        protected void btnExpired_Click(object sender, EventArgs e)
        {
            rblRequestType.SelectedValue = "Auth";
            txtAmount.Text = "123.45";
            txtCardholder.Text = "Somebody A Tester";
            txtNumber.Text = "4009837983422344";
            txtCVV.Text = "123";
            txtExpiryDate.Text = "0508";
        }

        #endregion
        //DELETE_TO_HERE
    }
}