﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DateAndTime;
using CS.General_CS_v5;
using CS.General_CS_v5.Modules.Web;
using General_Tests_CS_v5.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using PayMammoth_v2.Connector.Notifications;
using PayMammoth_v2.Modules.Data.Notifications;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Services.Notifications;
using Raven.Client;
using Should;
using SpecsFor;
using SpecsFor.ShouldExtensions;
using General_Tests_CS_v5.TestUtil;
using BusinessLogic_CS_v5.Extensions;
using CS.General_CS_v5.Extensions;


namespace PayMammoth_v2_Tests.NormalTests.Logic.Modules.Services.Notifications
{
    public class NotificationsSendingServiceSpecs
    {
        [TestFixture]
        public class SendNotificationMessageSpecs
        {
            [TestFixture]
            public class given_such_data : MySpecsFor<NotificationsSendingService>
            {
                protected NotificationMessageData _notificationMessage;
                protected Mock<IDocumentSession> _mockSession;
                protected PaymentRequestData _request;
                protected DateTimeOffset _currentDateTime;


                protected override void Given()
                {
                    _notificationMessage = new NotificationMessageData();
                    _notificationMessage.Id = "Message/1";

                    _request = new PaymentRequestData();
                    _request.Id = "Request/1";

                    _mockSession = new Mock<IDocumentSession>();
                    _mockSession.SetupLoad(_notificationMessage);
                    _mockSession.SetupLoad(_request);

                    _notificationMessage.LinkedPaymentRequestId = _request;

                    _notificationMessage.NotificationSendingInformation.Add(new NotificationSendingInstanceInfo()
                    { DateTime = new DateTimeOffset(2012,1,1,1,1,1,new TimeSpan())});
                    _currentDateTime = new DateTimeOffset(2014, 1, 2, 0, 0, 0, new TimeSpan());
                    GetMockFor<ICurrentDateTimeRetrieverService>().Setup(x => x.GetCurrentDateTime()).Returns(_currentDateTime);

                    base.Given();
                }


            }

            [TestFixture]
            public class given_notification_msg_without_notification_url : given_such_data
            {


                protected override void Given()
                {
                    base.Given();
                    _notificationMessage.SendToUrl = "";
                    
                }

                protected override void When()
                {
                    SUT.SendNotificationMessage(_notificationMessage, _mockSession.Object);
                    base.When();
                }

                [Test]
                public void then_no_notification_must_be_sent()
                {
                    GetMockFor<IHttpRequestSenderService>().Verify(x=>x.SendHttpRequest(
                        It.IsAny<string>(),
                        It.IsAny<Enums.HttpMethod>(),
                        It.IsAny<WebHeaderCollection>(),
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<int>(),
                        It.IsAny<X509Certificate>(),
                        It.IsAny<string>()),Times.Never());


                }
                [Test]
                public void then_notification_sending_instance_should_be_created_with_correct_status()
                {
                    _notificationMessage.NotificationSendingInformation.Count.ShouldEqual(2);
                    var createdNotificationSendInfo = _notificationMessage.NotificationSendingInformation.Last(x => x.DateTime > new DateTimeOffset(2013, 1, 1, 0, 0, 0, new TimeSpan()));
                    createdNotificationSendInfo.Status.ShouldEqual(NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.NoSendToUrl);
                    
                }
            }

            [TestFixture]
            public class given_notification_msg_with_notification_url : given_such_data
            {
                protected string _msgAsJson;
                protected Expression<Func<IHttpRequestSenderService, IHttpWebResponse>> _httpRequestSignature;
                protected Mock<IHttpWebResponse> _mockHttpResponse;


                protected override void Given()
                {
                    base.Given();
                    _notificationMessage.SendToUrl = "http://www.karlcassar.com/testnotify.ashx";

                    _notificationMessage.Identifier = "msg-identifier";
                    _notificationMessage.Message = " test message!";
                    _notificationMessage.NotificationType = PayMammoth_v2.Connector.Enums.NotificationMessageType.RecurringProfileActivated; 
                    _notificationMessage.LinkedPaymentRequestId = _request;
                    _notificationMessage.RetryCount = 5;
                    

                    
                    NotificationMessage msg = new NotificationMessage();
                    msg.DateTimeSent = _currentDateTime;
                    msg.Identifier = _notificationMessage.Identifier;
                    msg.Message = _notificationMessage.Message;
                    msg.MessageType = _notificationMessage.NotificationType;
                    msg.NotificationId = _notificationMessage.Id;
                    msg.PaymentRequestId = _notificationMessage.LinkedPaymentRequestId;
                    msg.RetryCount = _notificationMessage.RetryCount;

                    _msgAsJson = CS.General_CS_v5.Util.JsonUtil.Serialize(msg);

                    _httpRequestSignature = (x => x.SendHttpRequest(
                        _notificationMessage.SendToUrl,
                        Enums.HttpMethod.POST,
                        It.IsAny<WebHeaderCollection>(),
                        _msgAsJson,
                        It.IsAny<string>(),
                        It.IsAny<int>(),
                        It.IsAny<X509Certificate>(),
                        It.IsAny<string>()));

                    _mockHttpResponse = new Mock<IHttpWebResponse>();

                    _mockHttpResponse.SetupAllProperties();
                    GetMockFor<IHttpRequestSenderService>().Setup(_httpRequestSignature).Returns(_mockHttpResponse.Object);
                    

                }

            }

            [TestFixture]
            public class given_post_is_acknowledged : given_notification_msg_with_notification_url
            {
                

                protected override void Given()
                {
                    base.Given();

                    _mockHttpResponse.Setup(x => x.StatusCode).Returns(HttpStatusCode.OK);
                    _mockHttpResponse.Setup(x => x.GetResponseAsString()).Returns(PayMammoth_v2.Connector.Constants.RESPONSE_OK);


                }

                protected override void When()
                {
                    SUT.SendNotificationMessage(_notificationMessage, _mockSession.Object);
                    base.When();
                }

                [Test]
                public void then_http_post_should_be_sent_with_proper_data()
                {

                    GetMockFor<IHttpRequestSenderService>().Verify(_httpRequestSignature, Times.Once());



                }
                [Test]
                public void then_http_response_is_closed()
                {
                    _mockHttpResponse.Verify(x => x.Close(), Times.Once());

                }
                [Test]
                public void then_notification_should_be_marked_as_acknowledged()
                {
                    _notificationMessage.Status.ShouldEqual(PayMammoth_v2.Connector.Enums.NotificationMessageStatus.Success);

                    _notificationMessage.NotificationSendingInformation.Count.ShouldEqual(2);
                    var createdNotificationSendInfo = _notificationMessage.NotificationSendingInformation.Last(x => x.DateTime > new DateTimeOffset(2013, 1, 1, 0, 0, 0, new TimeSpan()));
                    createdNotificationSendInfo.Status.ShouldEqual(NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.Success);

                }
                [Test]
                public void then_retry_count_should_NOT_be_incremented()
                {

                    GetMockFor<INotificationsRetryService>()
                        .Verify(x => x.IncrementRetryCountOrMarkAsFailedIfMaximumReached(_notificationMessage, _mockSession.Object), Times.Never());


                }
            }
            [TestFixture]
            public class given_post_returns_invalid_data : given_notification_msg_with_notification_url
            {
               
                

                protected override void Given()
                {
                    base.Given();
                    
                    _mockHttpResponse.Setup(x => x.StatusCode).Returns(HttpStatusCode.OK);
                    _mockHttpResponse.Setup(x => x.GetResponseAsString()).Returns("invalid-data");
                    

                }

                protected override void When()
                {
                    SUT.SendNotificationMessage(_notificationMessage, _mockSession.Object);
                    base.When();
                }


                [Test]
                public void then_notification_should_NOT_be_marked_as_acknowledged()
                {
                    _notificationMessage.Status.ShouldEqual(
                        PayMammoth_v2.Connector.Enums.NotificationMessageStatus.Pending);

                    _notificationMessage.NotificationSendingInformation.Count.ShouldEqual(2);
                    var createdNotificationSendInfo =
                        _notificationMessage.NotificationSendingInformation.Last(
                            x => x.DateTime > new DateTimeOffset(2013, 1, 1, 0, 0, 0, new TimeSpan()));
                    createdNotificationSendInfo.Status.ShouldEqual(
                        NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.IncorrectResponse);


                }
                [Test]
                public void then_retry_count_should_be_incremented()
                {
                    
                    GetMockFor<INotificationsRetryService>()
                        .Verify(x => x.IncrementRetryCountOrMarkAsFailedIfMaximumReached(_notificationMessage, _mockSession.Object), Times.Once());
                   

                }
            }

            [TestFixture]
            public class given_http_post_raises_an_exception : given_notification_msg_with_notification_url
            {
                private string _testErrorGenerated;


                protected override void Given()
                {
                    base.Given();
                    _testErrorGenerated = "Test error generated";
                    GetMockFor<IHttpRequestSenderService>().Setup(_httpRequestSignature).Returns(() =>
                    {
                        
                        throw new WebException(_testErrorGenerated, null, WebExceptionStatus.Timeout, null);
                        return null;
                    });


                }

                protected override void When()
                {
                    SUT.SendNotificationMessage(_notificationMessage, _mockSession.Object);
                    base.When();
                }


                [Test]
                public void then_notification_should_NOT_be_marked_as_acknowledged()
                {
                    _notificationMessage.Status.ShouldEqual(
                        PayMammoth_v2.Connector.Enums.NotificationMessageStatus.Pending);

                    _notificationMessage.NotificationSendingInformation.Count.ShouldEqual(2);
                    
                    var createdNotificationSendInfo =
                        _notificationMessage.NotificationSendingInformation.Last(
                            x => x.DateTime > new DateTimeOffset(2013, 1, 1, 0, 0, 0, new TimeSpan()));
                    
                    createdNotificationSendInfo.Status.ShouldEqual(
                        NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.ErrorOccurred);

                    createdNotificationSendInfo.Message.ShouldContain(WebExceptionStatus.Timeout.ToString());
                    createdNotificationSendInfo.Message.ShouldContain(_testErrorGenerated);


                }
                [Test]
                public void then_retry_count_should_be_incremented()
                {

                    GetMockFor<INotificationsRetryService>()
                        .Verify(x => x.IncrementRetryCountOrMarkAsFailedIfMaximumReached(_notificationMessage, _mockSession.Object), Times.Once());


                }
            }
            //  continue below, with the ones not having the [N] not being done yet.  implenention not done at all.
            /* 
load the notification msg
make sure it is not yet acknowledged
if notify url is empty, 

create a notification msg to send (json)
set the identifier, notification msg type, message and requestId, retryCount
                         
send as POST to notify url
add log to payment request
if (acknowledged), mark as acknowloeged
--------

given notification msg already marked as acknowl.edged
	log warning
	notification sending info is added, with correct result.
given notify url is empty,
	log warning
	notification sending info is added, with correct result.
given notification msg is not marked as acknowledged
	then http post should be sent with proper JSON object and values
	given post is acknowledged
		then notification is marked as acknowledged
		then notification send info status should mark as success
[N]	given post is not acknowledged
		then notification is NOT marked as acknowledged
		then retry count should be incremented (method call)
[N]	given no http resonse
		then notification send info status should mark as No Response
[N]	given invalid http response
		then notification send info status should mark as Incorrect Response
[N]	notification sending info is added, with correct result.
		
		
	
*/		
	
        }
    }
}
