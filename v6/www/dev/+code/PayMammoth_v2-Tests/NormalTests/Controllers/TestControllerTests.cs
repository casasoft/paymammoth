﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CS.General_CS_v5;
using CS.General_CS_v5.Modules.Pages;
using PayMammoth_v2.Presentation.Models.Home;
using PayMammoth_v2.Presentation.Services.Test;
using PayMammoth_v2Deploy.Controllers;
using General_Tests_CS_v5.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using Moq.Language.Flow;

namespace PayMammoth_v2_Tests.NormalTests.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;

    [TestFixture]
    public class TestControllerTestsSpecs
    {

        [TestFixture]
        public class IndexSpecs : MySpecsFor<TestController>
        {


            public class given_action_is_called : MySpecsForControllerAction<TestController, TestSearchModel>
            {
                private string _keywords;
                private Enums.EnumsSortBy _sortBy;

                public override ActionResult ExecuteAction()
                {
                    return (ActionResult)SUT.Search();
                }
                
                protected override void Given()
                {
                    _keywords = "my test keywords";
                    _sortBy = Enums.EnumsSortBy.NameDescending;

                    GetMockFor<IPageService>().Setup(x => x.GetVariableFromRoutingQuerystringOrForm<string>("keywords", false)).Returns(_keywords);
                    GetMockFor<IPageService>().Setup(x => x.GetVariableFromRoutingQuerystringOrForm<CS.General_CS_v5.Enums.EnumsSortBy>("sortBy", false)).Returns(_sortBy);




                    //nothing
                    base.Given();
                }

                protected override void When()
                {

                    base.When();
                }

                [Test]
                public void then_search_method_should_be_passed_correct_parameters()
                {
                    GetMockFor<IMyTestService>().Verify(x => x.SearchItems(_keywords, _sortBy), Times.Once());
                }




                public override string GetViewNameThroughT4Mvc()
                {
                    return MVCPayMammoth_v2.Test.Views.Search;
                }
            }

            public class given_TestController_Indx_action_is_called : MySpecsForControllerAction<TestController, TestIndexModel>
            {
                public override ActionResult ExecuteAction()
                {
                    return (ActionResult)SUT.Index();
                }
                
                protected override void Given()
                {
                    //nothing
                    base.Given();
                }

                protected override void When()
                {
                    //TestController's action Indx is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_something_should_happen()
                {
                }




                public override string GetViewNameThroughT4Mvc()
                {
                    return MVCPayMammoth_v2.Home.Views.Index;
                }
            }


            


            

            
        }



            


       
    }

}
