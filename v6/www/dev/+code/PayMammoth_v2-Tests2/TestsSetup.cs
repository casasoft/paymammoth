﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.RavenDb;
using General_Tests_CS_v6.BackendTests.SlowTests.BusinessLogic.Framework.RavenDb;
using NUnit.Framework;
using PayMammoth_v2.Framework.Application;

namespace PayMammoth_v2_Tests
{
    [SetUpFixture]
    public class TestsSetup
    {
      

        public class AppInstancePaymammothv2Tests : AppInstancePayMammoth_v2
        {
            protected override void initialCustomComponentsWithInversionOfControl()
            {
                base.initialCustomComponentsWithInversionOfControl();
               // InversionUtil.Register<RavenDbManager, TestRavenDbManager>();
            }
        }

        [SetUp]
        public void Setup()
        {
           // RavenDbManager._RavenDbManagerToInstantiate = typeof(TestRavenDbManager);
           // CS.General_CS_v6.Util.OtherUtil.SetIsInUnitTestingEnvironment();
           // var app = new AppInstancePaymammothv2Tests();
         
           //// app.AddProjectAssembly(typeof(General_Tests_CS_v6.TestsSetup).Assembly);
           // app.OnApplicationStart();

        }
        [TearDown]
        public void Teardown()
        {

        }
    }
}
