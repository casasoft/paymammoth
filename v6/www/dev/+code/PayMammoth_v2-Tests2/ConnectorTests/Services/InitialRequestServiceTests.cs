﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CS.General_CS_v6;
using CS.General_CS_v6.Modules.Web;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Connector.Services;
using Raven.Client.Linq;

namespace PayMammoth_v2_Tests2.ConnectorTests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v6.TestUtil;
    using BusinessLogic_CS_v6.Extensions;
    using CS.General_CS_v6.Extensions;


    [TestFixture]
    public class InitialRequestServiceSpecs
    {
        public class class_under_test : MySpecsFor<PayMammothConnectorInitialRequestService>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time
            [TestFixture]
            public class CreatePaymentRequestOnPayMammothSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {


                    protected override void Given()
                    {


                        base.Given();
                    }

                    [TestFixture]
                    public class given_the_request_is_successful : given_such_data
                    {
                        private string _websiteAccountCode;
                        private string _secretWord;
                        private InitialRequestInfo _initialRequest;
                        private InitialRequestResponseMessage _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _websiteAccountCode = "Website";
                            _secretWord = "123456";
                            _initialRequest = new InitialRequestInfo();
                            _initialRequest.OrderLinkOnClientWebsite = "http://www.testwebsite.com";

                            string url = PayMammoth_v2.Connector.Constants.PayMammoth_ConfirmMsgUrl;

                            
                            var mockResponse = new Mock<IHttpWebResponse>();
                            mockResponse.Setup(x=>x.StatusCode).Returns(HttpStatusCode.OK);
                            

                            InitialRequestResponseMessage initialRequestResponseMessage = new InitialRequestResponseMessage();
                            initialRequestResponseMessage.RequestId = "Request/5";
                            initialRequestResponseMessage.Status = PayMammoth_v2.Connector.EnumsPayMammothConnector.InitialRequestResponseStatus.Success;

                            string initialRequestResponseMessageJson = CS.General_CS_v6.Util.JsonUtil.Serialize(initialRequestResponseMessage);

                            mockResponse.Setup(x=>x.GetResponseAsString()).Returns(initialRequestResponseMessageJson);

                            GetMockFor<IHttpRequestSenderService>().Setup(x=>x.SendHttpRequest(url,
                                 Enums.HttpMethod.POST, 
                                 It.IsAny<WebHeaderCollection>(),
                                 It.IsAny<string>(),
                                 null,60000,null,"UserAgent")).Returns(mockResponse.Object);




                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.CreatePaymentRequestOnPayMammoth(_websiteAccountCode, _secretWord, _initialRequest);
                        }

                        [Test]
                        public void then_result_should_be_as_sent()
                        {
                            _result.RequestId.ShouldEqual("Request/5");
                            _result.Status.ShouldEqual(PayMammoth_v2.Connector.EnumsPayMammothConnector.InitialRequestResponseStatus.Success);
                        }
                    }
                    [TestFixture]
                    public class given_the_request_fails : given_such_data
                    {
                        private string _websiteAccountCode;
                        private string _secretWord;
                        private InitialRequestInfo _initialRequest;
                        private InitialRequestResponseMessage _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _websiteAccountCode = "Website";
                            _secretWord = "123456";
                            _initialRequest = new InitialRequestInfo();
                            _initialRequest.OrderLinkOnClientWebsite = "http://www.testwebsite.com";

                            string url = PayMammoth_v2.Connector.Constants.PayMammoth_InitialRequestUrl;


                            var mockResponse = new Mock<IHttpWebResponse>();
                            mockResponse.Setup(x => x.StatusCode).Returns(HttpStatusCode.Forbidden);


                            
                            
                            //mockResponse.Setup(x => x.GetResponseAsString()).Returns(null);

                            GetMockFor<IHttpRequestSenderService>().Setup(x => x.SendHttpRequest(url,
                                 Enums.HttpMethod.POST,
                                 It.IsAny<WebHeaderCollection>(),
                                 It.IsAny<string>(),
                                 null, 60000, null, "UserAgent")).Returns(mockResponse.Object);




                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.CreatePaymentRequestOnPayMammoth(_websiteAccountCode, _secretWord, _initialRequest);
                        }

                        [Test]
                        public void then_result_should_be_as_sent()
                        {
                            _result.Status.ShouldEqual(PayMammoth_v2.Connector.EnumsPayMammothConnector.InitialRequestResponseStatus.ErrorWhenSendingMessage);
                        }
                    }

                }
            }
        }


    }
}
