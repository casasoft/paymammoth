﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Layouts;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using CS.General_CS_v6.Classes.HelperClasses;
using CS.General_CS_v6.Modules.Pages;
using PayMammoth_v2.Connector;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Presentation.Code.ViewData;
using PayMammoth_v2.Presentation.Models.Home;
using PayMammoth_v2.Presentation.Models.PayPal;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments.PayPal;
using PayMammoth_v2.Presentation.Services.Payments;
using PayMammoth_v2.Presentation.Services.Payments.PayPal;
using PayMammoth_v2.Presentation.Services.Transactions;
using PayMammoth_v2Deploy.Controllers;

namespace PayMammoth_v2_Tests2.FrontendTests.NormalTests.Controllers
{
    using Moq;
    using NUnit.Framework;
    using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v6.TestUtil;
    using BusinessLogic_CS_v6.Extensions;
    using CS.General_CS_v6.Extensions;
    using System.Web.Mvc;
    using BusinessLogic_CS_v6.Presentation.Code.ContextData;
    using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Sections;
    using BusinessLogic_CS_v6.Presentation.Services.Context;
    using BusinessLogic_CS_v6.Presentation.Services.Sections;

    [TestFixture]
    public class PayPalControllerSpecs
    {

  
        public abstract class PaymentPaypalConfirmationSpecs :
            MySpecsForControllerActionWithoutModelAndView<PayPalController>
        {
            private CommonViewContextData _commonViewContextData;
            private SectionDataModel _sectionModel;
            private string _url;
            private LayoutViewData _layoutViewData;
            private string _token;
            private string _payerId;

            public override ActionResult ExecuteAction()
            {
                return SUT.PaymentConfirmation();
            }



            protected override void Given()
            {
                //nothing
                base.Given();

                //Common Given setup

                _commonViewContextData = new CommonViewContextData();
                GetMockFor<ICommonViewContextDataService>()
                    .Setup(x => x.GetCommonViewContextData())
                    .Returns(_commonViewContextData);

                _sectionModel = new SectionDataModel();
                GetMockFor<ISectionDataPresentationService>()
                    .Setup(x => x.GetSectionDataModelByIdentifierOrCreateNew(_mockedSessionObject,
                        _cultureDataModel,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Payment_PaypalConfirmation))
                    .Returns(_sectionModel);

                _url = "/url/";
                GetMockFor<IPageService>().Setup(x => x.GetCurrentPageUrl()).Returns(_url);

                _layoutViewData = new LayoutViewData()
                {
                    PaymentRequestDataModel = new PaymentRequestDataModel()
                    {
                        PaymentRequestId = "RequestABC"
                    }
                };
                GetMockFor<IPageService>()
                    .Setup(
                        x =>
                            x.GetContextObject<LayoutViewData>(
                                PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey,
                                true))
                    .Returns(_layoutViewData);

                _token = "tokenA";
                GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetVariableFromRoutingQuerystringOrForm<string>(
                                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Token,
                                    false))
                        .Returns(_token);

                _payerId = "PayerID123";
                GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetVariableFromRoutingQuerystringOrForm<string>(
                                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_PayerId,
                                    false))
                        .Returns(_payerId);

            }

            protected override void When()
            {
                //Home's action PaymentSelection is called automatically through the When() method of this base class

                base.When();
            }


            public class given_PaymentPaypalConfirmation_is_called : PaymentPaypalConfirmationSpecs
            {

                protected override void Given()
                {
                    //nothing
                    base.Given();

                    //Common Given setup



                    var layoutViewData = new LayoutViewData()
                    {
                        PaymentRequestDataModel = new PaymentRequestDataModel()
                    };
                    GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetContextObject<LayoutViewData>(
                                    PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey,
                                    true))
                        .Returns(layoutViewData);


                    _mockedContentTexts.MockItems(
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PayPalConfirmation_Title,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PayPalConfirmation_DescriptionHtml,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PayPalConfirmation_ConfirmButton);

                   
                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_common_view_context_section_should_be_set()
                {
                    _commonViewContextData.Page.CurrentSection.ShouldEqual(_sectionModel);
                }
                [Test]
                public void then_view_is_correct()
                {
                    ((ViewResult)_actionResult).ViewName.ShouldEqual(MVCPayMammoth_v2.PayPal.Views.Confirmation);
                }
                [Test]
                public void then_confirm_url_should_be_fine()
                {
                    var model = (PaypalConfirmationModel)((ViewResult)_actionResult).Model;
                    model.ConfirmUrl.ShouldContain(string.Format("{0}?{1}={2}",
                        _url,
                        PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Confirm,
                        CS.General_CS_v6.Util.ConversionUtil.ConvertBasicDataTypeToString(true)));
                }

                [Test]
                public void then_content_texts_should_be_correct()
                {
                    var model = (PaypalConfirmationModel)((ViewResult)_actionResult).Model;
                    model.ConfirmButtonTextModel.ShouldEqual(_mockedContentTexts.Values[PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PayPalConfirmation_ConfirmButton]);
                    model.ConfirmTextModel.ShouldEqual(_mockedContentTexts.Values[PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PayPalConfirmation_DescriptionHtml]);
                    model.TitleContentTextModel.ShouldEqual(_mockedContentTexts.Values[PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PayPalConfirmation_Title]);
                }

            }
            public class given_PaymentPaypalConfirmation_is_called_and_confirming_and_result_success : PaymentPaypalConfirmationSpecs
            {
                private StatusResult<ConfirmPaypalRequestStatus, PayPalConfirmationResultDataModel> _confirmResult;

                protected override void Given()
                {
                    //nothing
                    base.Given();

                    //Common Given setup




                    GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetVariableFromRoutingQuerystringOrForm<bool>(
                                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Confirm,
                                    false))
                        .Returns(true);

                    _confirmResult = new StatusResult<ConfirmPaypalRequestStatus, PayPalConfirmationResultDataModel>()
                    {
                        Status = ConfirmPaypalRequestStatus.Success,
                        Result = new PayPalConfirmationResultDataModel() {
                            RedirectUrl = "http://www.yahoo.com"
                        }
                    };
                    GetMockFor<IPaypalDataModelService>().Setup(x => x.ConfirmPaypalRequestFromPresentation(
                        _mockedSessionObject, _cultureDataModel, 
                        _layoutViewData.PaymentRequestDataModel.PaymentRequestId, 
                        _token, _payerId)).Returns(_confirmResult);

                    
                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

             

                [Test]
                public void then_redirect_result()
                {
                    ((RedirectResult)_actionResult).Url.ShouldEqual(_confirmResult.Result.RedirectUrl);
                }


            }
            public class given_PaymentPaypalConfirmation_is_called_and_confirming_and_result_not_success : PaymentPaypalConfirmationSpecs
            {
                private StatusResult<ConfirmPaypalRequestStatus, PayPalConfirmationResultDataModel> _confirmResult;

                protected override void Given()
                {
                    //nothing
                    base.Given();

                    //Common Given setup



                    GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetVariableFromRoutingQuerystringOrForm<bool>(
                                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Confirm,
                                    false))
                        .Returns(true);

                    _confirmResult = new StatusResult<ConfirmPaypalRequestStatus, PayPalConfirmationResultDataModel>()
                    {
                        Status = ConfirmPaypalRequestStatus.Error
                    };
                    GetMockFor<IPaypalDataModelService>().Setup(x => x.ConfirmPaypalRequestFromPresentation(
                        _mockedSessionObject,
                        _cultureDataModel,
                        _layoutViewData.PaymentRequestDataModel.PaymentRequestId,
                        _token,
                        _payerId)).Returns(_confirmResult);

                    _mockedContentTexts.MockItems(_confirmResult.Status);


                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_common_view_context_section_should_be_set()
                {
                    _commonViewContextData.Page.CurrentSection.ShouldEqual(_sectionModel);
                }

                [Test]
                public void then_notification_message_should_be_error()
                {
                    _commonViewContextData.NotificationMessage.MessageContentTextModel.ShouldEqual(
                        _mockedContentTexts.Values[_confirmResult.Status]);
                    _commonViewContextData.NotificationMessage.MessageType.ShouldEqual(NotificationMessageType.Error);
                }


            }

        }

      
    }
}
