﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Fields;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Layouts;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using CS.General_CS_v6.Classes.HelperClasses;
using CS.General_CS_v6.Modules.Pages;
using PayMammoth_v2.Connector;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Services.FakePayments;
using PayMammoth_v2.Presentation.Code.ViewData;
using PayMammoth_v2.Presentation.Models.Home;
using PayMammoth_v2.Presentation.Models.PayPal;
using PayMammoth_v2.Presentation.Models.Transactium;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments.PayPal;
using PayMammoth_v2.Presentation.Services.Payments;
using PayMammoth_v2.Presentation.Services.Payments.PayPal;
using PayMammoth_v2.Presentation.Services.Transactions;
using PayMammoth_v2Deploy.Controllers;

namespace PayMammoth_v2_Tests2.FrontendTests.NormalTests.Controllers
{
    using Moq;
    using NUnit.Framework;
    using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v6.TestUtil;
    using BusinessLogic_CS_v6.Extensions;
    using CS.General_CS_v6.Extensions;
    using System.Web.Mvc;
    using BusinessLogic_CS_v6.Presentation.Code.ContextData;
    using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Sections;
    using BusinessLogic_CS_v6.Presentation.Services.Context;
    using BusinessLogic_CS_v6.Presentation.Services.Sections;

    [TestFixture]
    public class HomeControllerSpecs
    {

        public abstract class PaymentSelectionSpecs :
            MySpecsForControllerAction<HomeController, HomePaymentSelectionModel>
        {
            private CommonViewContextData _commonViewContextData;
            private SectionDataModel _sectionModel;

            public override ActionResult ExecuteAction()
            {
                return SUT.PaymentSelection();
            }

            public override string GetViewNameThroughT4Mvc()
            {
                return MVCPayMammoth_v2.Home.Views.PaymentSelection;
            }

            protected override void Given()
            {
                //nothing
                base.Given();

                //Common Given setup

                _commonViewContextData = new CommonViewContextData();
                GetMockFor<ICommonViewContextDataService>()
                    .Setup(x => x.GetCommonViewContextData())
                    .Returns(_commonViewContextData);

                _sectionModel = new SectionDataModel();
                GetMockFor<ISectionDataPresentationService>()
                    .Setup(x => x.GetSectionDataModelByIdentifierOrCreateNew(_mockedSessionObject,
                        _cultureDataModel,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Selection)).Returns(_sectionModel);



            }

            protected override void When()
            {
                //Home's action PaymentSelection is called automatically through the When() method of this base class

                base.When();
            }


            public class given_PaymentSelection_is_called : PaymentSelectionSpecs
            {

                private HomePaymentSelectionModel _homePaymentSelectionModel;

                protected override void Given()
                {
                    //nothing
                    base.Given();

                    //Common Given setup

                    _homePaymentSelectionModel = new HomePaymentSelectionModel();
                    

                    var layoutViewData = new LayoutViewData()
                    {
                        PaymentRequestDataModel = new PaymentRequestDataModel()
                    };
                    GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetContextObject<LayoutViewData>(
                                    PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey,
                                    true))
                        .Returns(layoutViewData);


                    GetMockFor<IHomePaymentSelectionModelService>()
                        .Setup(x => x.CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestId(
                            _mockedSessionObject,
                            _cultureDataModel,
                            layoutViewData.PaymentRequestDataModel)).Returns(_homePaymentSelectionModel);
                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_common_view_context_section_should_be_set()
                {
                    _commonViewContextData.Page.CurrentSection.ShouldEqual(_sectionModel);
                }

                [Test]
                public void then_model_should_be_correct()
                {
                    Model.ShouldEqual(_homePaymentSelectionModel);
                }

            }

        }


        public abstract class PaymentSelectionHandlerSpecs :
            MySpecsForControllerActionWithoutModelAndView<HomeController>
        {

            public override ActionResult ExecuteAction()
            {
                return SUT.PaymentSelectionHandler();
            }

            protected override void Given()
            {
                //nothing
                base.Given();


               
                

            }

            protected override void When()
            {
                //Home's action PaymentSelection is called automatically through the When() method of this base class

                base.When();
            }


            public class given_PaymentSelectionHandler_is_called_and_transaction_success_and_no_fake_payment_ids : PaymentSelectionHandlerSpecs
            {
                private EnumsPayMammothConnector.PaymentMethodType _paymentMethodType;
                private LayoutViewData _layoutViewData;
                private TransactionResultDataModel _transactionResultDataModel;


                protected override void Given()
                {
                    //nothing
                    base.Given();

                    _layoutViewData = new LayoutViewData()
                    {
                        PaymentRequestDataModel = new PaymentRequestDataModel()
                        {
                            PaymentRequestId = "PaymentRequestData/MyRequest"
                        }
                    };
                    GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetContextObject<LayoutViewData>(
                                    PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey,
                                    true))
                        .Returns(_layoutViewData);


                    _paymentMethodType = EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Transactium;
                    GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetVariableFromRoutingQuerystringOrForm<EnumsPayMammothConnector.PaymentMethodType?>(
                                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_PaymentMethod,
                                    false))
                        .Returns(_paymentMethodType);

                    _transactionResultDataModel = new TransactionResultDataModel()
                    {
                        Status = PayMammoth_v2Enums.RedirectionResultStatus.Success,
                        RedirectUrl = "http://www.google.com"
                    };
                    GetMockFor<IPaymentRequestDataModelService>()
                        .Setup(x => x.GenerateTransactionResultFromPaymentMethod(
                            _mockedSessionObject,
                            _cultureDataModel,
                            _paymentMethodType,
                            _layoutViewData.PaymentRequestDataModel.PaymentRequestId))
                        .Returns(_transactionResultDataModel);
                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_view_and_model_should_be_correct()
                {
                    var view = (ViewResult)_actionResult;
                    view.ViewName.ShouldEqual(MVCPayMammoth_v2.Home.Views.PaymentSelectionHandler);
                    var model = (HomePaymentSelectionHandlerModel)view.Model;
                    model.RedirectUrl.ShouldEqual(_transactionResultDataModel.RedirectUrl);
                }


            }
            public class given_PaymentSelectionHandler_is_called_and_transaction_success_with_fake_payment_ids : PaymentSelectionHandlerSpecs
            {
                private EnumsPayMammothConnector.PaymentMethodType _paymentMethodType;
                private LayoutViewData _layoutViewData;
                private TransactionResultDataModel _transactionResultDataModel;
                private StatusResult<MakeFakePaymentService.MakeFakePaymentResult, MakeFakePaymentResponse> _fakePaymentResult;


                protected override void Given()
                {
                    //nothing
                    base.Given();

                    _layoutViewData = new LayoutViewData()
                    {
                        PaymentRequestDataModel = new PaymentRequestDataModel()
                        {
                            PaymentRequestId = "PaymentRequestData/MyRequest",
                            FakePaymentKey = "FAKE"
                        }
                    };
                    GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetContextObject<LayoutViewData>(
                                    PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey,
                                    true))
                        .Returns(_layoutViewData);


                    _paymentMethodType = EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Transactium;
                    GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetVariableFromRoutingQuerystringOrForm<EnumsPayMammothConnector.PaymentMethodType?>(
                                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_PaymentMethod,
                                    false))
                        .Returns(_paymentMethodType);

                    _transactionResultDataModel = new TransactionResultDataModel()
                    {
                        Status = PayMammoth_v2Enums.RedirectionResultStatus.Success,
                        RedirectUrl = "http://www.google.com"
                    };
                    _fakePaymentResult = new StatusResult<MakeFakePaymentService.MakeFakePaymentResult, MakeFakePaymentResponse>
                        ()
                    {
                        Result = new MakeFakePaymentResponse()
                        {
                            RedirectUrl = "http://www.fake.com"

                        },
                        Status = MakeFakePaymentService.MakeFakePaymentResult.Success
                    };
                    GetMockFor<IMakeFakePaymentService>()
                        .Setup(
                            x =>
                                x.MakeFakePayment(
                                    It.Is<ReferenceLink<PaymentRequestData>>(
                                        refLink =>
                                            refLink.GetLinkId() ==
                                            _layoutViewData.PaymentRequestDataModel.PaymentRequestId),
                                    _paymentMethodType,
                                    _layoutViewData.PaymentRequestDataModel.FakePaymentKey,
                                    _mockedSessionObject))
                        .Returns(_fakePaymentResult);

                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_view_and_model_should_be_correct()
                {
                    var view = (ViewResult)_actionResult;
                    view.ViewName.ShouldEqual(MVCPayMammoth_v2.Home.Views.PaymentSelectionHandler);
                    var model = (HomePaymentSelectionHandlerModel)view.Model;
                    model.RedirectUrl.ShouldEqual(_fakePaymentResult.Result.RedirectUrl);
                }


            }

        }

        public abstract class ExpiredSpecs :
            MySpecsForControllerAction<HomeController, HomeExpiredModel>
        {
            private SectionDataModel _sectionDataModel;
            private string _cancelUrl;

            public override ActionResult ExecuteAction()
            {
                return SUT.Expired();
            }
            public override string GetViewNameThroughT4Mvc()
            {
                return MVCPayMammoth_v2.Home.Views.Expired;
            }
            protected override void Given()
            {
                //nothing
                base.Given();


                _sectionDataModel = new SectionDataModel()
                {
                    TitleFieldModel =  new FieldModel<string>()
                };
                GetMockFor<ISectionDataPresentationService>().Setup(x => x.GetSectionDataModelByIdentifierOrCreateNew(
                    _mockedSessionObject,
                    _cultureDataModel,
                    PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Expired)).Returns(_sectionDataModel);


                _cancelUrl = "http://cancel-url/";
                GetMockFor<IPageService>().Setup(x => x.GetVariableFromRoutingQuerystringOrForm<string>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_CancelUrl, false)).Returns(_cancelUrl);



                _mockedContentTexts.MockItemWithReplacementModel(PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.Payment_Expired, _cancelUrl);
            }

            protected override void When()
            {
                //Home's action PaymentSelection is called automatically through the When() method of this base class

                base.When();
            }


            public class given_page_is_called : ExpiredSpecs
            {
                

                protected override void Given()
                {
                    //nothing
                    base.Given();

                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_model_details_should_be_correct()
                {
                    Model.ExpiredContentText.ShouldEqual(_mockedContentTexts.Values[PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.Payment_Expired]);
                    Model.Title.ShouldEqual(_sectionDataModel.TitleFieldModel);
                }


            }


           
        }
        public abstract class CancelPaymentSpecs :
          MySpecsForControllerActionWithoutModelAndView<HomeController>
        {
            private LayoutViewData _layoutViewData;

            public override ActionResult ExecuteAction()
            {
                return SUT.CancelPayment();
            }

            protected override void Given()
            {
                //nothing
                base.Given();
                _layoutViewData = new LayoutViewData()
                {
                    PaymentRequestDataModel = new PaymentRequestDataModel()
                    {
                        PaymentRequestId = "requestID"
                    }
                };
                GetMockFor<IPageService>().Setup(x => x.GetContextObject<LayoutViewData>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey,
                    true)).Returns(_layoutViewData);




            }

            protected override void When()
            {
                //Home's action PaymentSelection is called automatically through the When() method of this base class

                base.When();
            }


            public class given_cancel_is_success : CancelPaymentSpecs
            {
                private StatusResult<CancelPaymentStatus, string> _cancelStatusResult;


                protected override void Given()
                {
                    //nothing
                    base.Given();

                    _cancelStatusResult = new StatusResult<CancelPaymentStatus, string>()
                    {
                        Status = CancelPaymentStatus.Success,
                        Result = "http://www.test.com"
                    };
                    GetMockFor<IPaymentRequestDataModelService>().Setup(x => x.CancelPaymentForPaymentRequestId(_mockedSessionObject,
                        _cultureDataModel, _layoutViewData.PaymentRequestDataModel.PaymentRequestId)).Returns(_cancelStatusResult);

                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_result_should_redirect_to_cancel_url()
                {
                    ((RedirectResult)_actionResult).Url.ShouldEqual(_cancelStatusResult.Result);
                }


            }

            public class given_cancel_is_error : CancelPaymentSpecs
            {
                private StatusResult<CancelPaymentStatus, string> _cancelStatusResult;
                private string _errorMessage;


                protected override void Given()
                {
                    //nothing
                    base.Given();

                    _cancelStatusResult = new StatusResult<CancelPaymentStatus, string>()
                    {
                        Status = CancelPaymentStatus.Error
                    };
                    GetMockFor<IPaymentRequestDataModelService>().Setup(x => x.CancelPaymentForPaymentRequestId(_mockedSessionObject,
                       _cultureDataModel, _layoutViewData.PaymentRequestDataModel.PaymentRequestId)).Returns(_cancelStatusResult);


                    _errorMessage = "Error";
                    _mockedContentTexts.MockItem(_cancelStatusResult.Status,_errorMessage);
                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_result_should_be_content_result_with_error_message()
                {
                    ((ContentResult)_actionResult).Content.ShouldEqual(_errorMessage);
                }


            }

        }

    }
}
