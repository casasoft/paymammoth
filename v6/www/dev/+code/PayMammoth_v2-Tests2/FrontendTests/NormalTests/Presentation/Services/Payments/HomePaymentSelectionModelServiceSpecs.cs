﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using CS.General_CS_v6.Classes.HelperClasses;
using CS.General_CS_v6.Modules.Pages;
using General_Tests_CS_v6.TestUtil;
using PayMammoth_v2.Presentation.Models.Home;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using Should;

namespace PayMammoth_v2_Tests2.FrontendTests.NormalTests.Presentation.Services.Payments
{

    using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
    using NUnit.Framework;
    using PayMammoth_v2.Presentation.Services.Payments;

    [TestFixture]
    public abstract class CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestIdSpecs : MySpecsFor<HomePaymentSelectionModelService>
    {
        protected override void Given()
        {
            //Common data setup between test cases for same method

            base.Given();
        }

        protected override void When()
        {
            base.When();
        }


        public class given_valid_request_id : CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestIdSpecs
        {
            private List<PaymentMethodModel> _paymentMethodModels;
            private PaymentRequestDataModel _paymentRequestDataModel;
            private HomePaymentSelectionModel _result;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup


                
                _paymentRequestDataModel = new PaymentRequestDataModel()
                {
                    PaymentMethodsAvailable = new List<PaymentMethodDataModel>()
                    {
                        new PaymentMethodDataModel(),
                        new PaymentMethodDataModel(),
                        new PaymentMethodDataModel()
                    },
                    PaymentRequestId = "1234",
                    FakePaymentKey = "FAKE"
                };
                _paymentMethodModels = _paymentRequestDataModel.PaymentMethodsAvailable.ConvertAll(
                   x =>
                   {
                       var paymentMethodModel = new PaymentMethodModel();
                       GetMockFor<IPaymentMethodModelService>().Setup(y => y.ConvertPaymentRequestDataModelToModel(
                           _mockedSessionObject,
                           _cultureDataModel,
                           x, _paymentRequestDataModel.PaymentRequestId, _paymentRequestDataModel.FakePaymentKey)).Returns(paymentMethodModel);
                       return paymentMethodModel;
                   });
            }

            protected override void When()
            {
                base.When();
                _result = SUT.CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestId(
                    _mockedSessionObject,
                    _cultureDataModel, _paymentRequestDataModel);
            }

            [Test]
            public void then_payment_methods_should_be_correct()
            {
                _result.PaymentMethods.ShouldContainAllAndNothingElseFromList(_paymentMethodModels);



            }
        }
        public class given_fake_payment_is_enabled : CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestIdSpecs
        {
            private List<PaymentMethodModel> _paymentMethodModels;
            private PaymentRequestDataModel _paymentRequestDataModel;
            private HomePaymentSelectionModel _result;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup



                _paymentRequestDataModel = new PaymentRequestDataModel()
                {
                    PaymentMethodsAvailable = new List<PaymentMethodDataModel>()
                    {
                        new PaymentMethodDataModel(),
                        new PaymentMethodDataModel(),
                        new PaymentMethodDataModel()
                    },
                    PaymentRequestId = "1234",
                    FakePaymentKey = "FAKE",
                    FakePaymentsEnabled = true
                };

                _mockedContentTexts.MockItem(PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.Messages_FakePaymentEnabled);
               
            }

            protected override void When()
            {
                base.When();
                _result = SUT.CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestId(
                    _mockedSessionObject,
                    _cultureDataModel, _paymentRequestDataModel);
            }

            
        }
       
    }
}
