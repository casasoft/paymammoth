﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using NUnit.Framework;
using PayMammoth_v2.Connector;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.InitialRequests;
using PayMammoth_v2.Modules.Services.InitialRequests.Helpers;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v2_Tests2.NormalTests.Logic.Modules.Services.InitialRequests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v6.TestUtil;
    using BusinessLogic_CS_v6.Extensions;
    using CS.General_CS_v6.Extensions;


    [TestFixture]
    public class InitialRequestRawMessageParserServiceSpecs
    {
        public class class_under_test : MySpecsFor<InitialRequestRawMessageParserService>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time
        }

        [TestFixture]
        public class ParseRawMessageSpecs
        {
            [TestFixture]
            public class given_such_data : class_under_test
            {


                protected override void Given()
                {


                    base.Given();
                }

                [TestFixture]
                public class given_could_not_parse_string : given_such_data
                {
                    private string _rawData;
                    private InitialRequestResponseMessage _result;
                    private InitialRequestEncryptedData _initialRequestEncryptedData;


                    protected override void Given()
                    {
                        base.Given();
                        //add any code after base.Given!
                        _rawData = "[INVALID]";
                        _initialRequestEncryptedData = null;
                        GetMockFor<IInitialRequestService>().Setup(x => x.ParseRawStringToInitialRequest(_rawData)).Returns(_initialRequestEncryptedData);


                    }

                    protected override void When()
                    {
                        base.When();
                        _result = SUT.ParseRawMessage(_rawData);
                    }

                    [Test]
                    public void then_result_should_be_invalidData()
                    {
                        _result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.InvalidData);
                    }
                }

                [TestFixture]
                public class given_could_parse_string_ok : given_such_data
                {
                    protected InitialRequestEncryptedData _initialRequestEncryptedData;
                    protected string _rawData;
                    protected Mock<IDocumentSession> _mockSession;
                    protected InitialRequestDecryptResult _decryptResult;


                    protected override void Given()
                    {
                        base.Given();
                        //add any code after base.Given!

                        _rawData = "[Ok]";
                        _initialRequestEncryptedData = new InitialRequestEncryptedData();

                        _mockSession = new Mock<IDocumentSession>();


                        GetMockFor<IInitialRequestService>().Setup(x => x.ParseRawStringToInitialRequest(_rawData)).Returns(_initialRequestEncryptedData);


                    }

                    [TestFixture]
                    public class given_could_not_decrypt_initial_request : given_could_parse_string_ok
                    {
                        private InitialRequestResponseMessage _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!


                            _decryptResult = new InitialRequestDecryptResult();
                            _decryptResult.Status = EnumsPayMammothConnector.InitialRequestResponseStatus.CouldNotDecrypt;

                            GetMockFor<IInitialRequestService>().Setup(x => x.DecryptInitialRequest(_mockSession.Object, _initialRequestEncryptedData))
                                .Returns(_decryptResult);


                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ParseRawMessage(_rawData, _mockSession.Object);

                        }

                        [Test]
                        public void then_result_should_be_could_not_decrypt()
                        {
                            _result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.CouldNotDecrypt);


                        }
                    }
                    [TestFixture]
                    public class given_could_decrypt_initial_request : given_could_parse_string_ok
                    {
                        

                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!


                            _decryptResult = new InitialRequestDecryptResult();
                            _decryptResult.InitialRequest = new InitialRequestInfo();
                            _decryptResult.Status = EnumsPayMammothConnector.InitialRequestResponseStatus.Success;
                            _decryptResult.WebsiteAccountId = new ReferenceLink<WebsiteAccountData>("WebsiteAccount/5");
                            GetMockFor<IInitialRequestService>().Setup(x => x.DecryptInitialRequest(_mockSession.Object, _initialRequestEncryptedData))
                                .Returns(_decryptResult);


                        }

                        [TestFixture]
                        public class given_generate_payment_request_did_not_succeed : given_could_decrypt_initial_request
                        {
                            private PaymentRequestDataService.GeneratePaymentRequestResult _generatePaymentRequestResult;
                            private InitialRequestResponseMessage _result;


                            protected override void Given()
                            {
                                base.Given();
                                //add any code after base.Given!

                                _generatePaymentRequestResult = new PaymentRequestDataService.GeneratePaymentRequestResult();
                                _generatePaymentRequestResult.Status = EnumsPayMammothConnector.InitialRequestResponseStatus.IpAddressNotValid; 
                                GetMockFor<IPaymentRequestDataService>().Setup(x => x.GeneratePaymentRequestFromInitialRequest(_decryptResult.WebsiteAccountId,
                                    _decryptResult.InitialRequest,
                                    null)).Returns(_generatePaymentRequestResult);


                            }

                            protected override void When()
                            {
                                base.When();


                                _result = SUT.ParseRawMessage(_rawData, _mockSession.Object);
                            }

                            [Test]
                            public void then_result_is_outputted()
                            {
                                _result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.IpAddressNotValid);


                            }
                        }
                        [TestFixture]
                        public class given_generate_payment_request_succeeded : given_could_decrypt_initial_request
                        {
                            private PaymentRequestDataService.GeneratePaymentRequestResult _generatePaymentRequestResult;
                            private InitialRequestResponseMessage _result;


                            protected override void Given()
                            {
                                base.Given();
                                //add any code after base.Given!

                                _generatePaymentRequestResult = new PaymentRequestDataService.GeneratePaymentRequestResult();
                                _generatePaymentRequestResult.Status = EnumsPayMammothConnector.InitialRequestResponseStatus.Success;
                                _generatePaymentRequestResult.GeneratedRequest = new PaymentRequestData();
                                _generatePaymentRequestResult.GeneratedRequest.Id = "Request/5";
                                GetMockFor<IPaymentRequestDataService>().Setup(x => x.GeneratePaymentRequestFromInitialRequest(_decryptResult.WebsiteAccountId,
                                    _decryptResult.InitialRequest,
                                    null)).Returns(_generatePaymentRequestResult);


                            }

                            protected override void When()
                            {
                                base.When();


                                _result = SUT.ParseRawMessage(_rawData, _mockSession.Object);
                            }

                            [Test]
                            public void then_result_is_outputted()
                            {
                                _result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.Success);


                            }
                            [Test]
                            public void then_request_id_is_filled()
                            {
                                _result.RequestId.ShouldEqual(_generatePaymentRequestResult.GeneratedRequest.Id);

                            }
                        }
                    }
                 

                  
                }

            }
        }
    }
}
