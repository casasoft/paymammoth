﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using PayMammoth_v2.Connector;
using PayMammoth_v2.Connector.Notifications;
using PayMammoth_v2.Modules.Data.Notifications;
using PayMammoth_v2.Modules.Services.Notifications;

namespace PayMammoth_v2_Tests.NormalTests.Logic.Modules.Services.Notifications
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v6.TestUtil;
    using BusinessLogic_CS_v6.Extensions;
    using CS.General_CS_v6.Extensions;


    [TestFixture]
    public class NotificationParseRawConfirmationResponseServiceSpecs
    {

        [TestFixture]
        public class ParseRawConfirmationResponseSpecs
        {
            [TestFixture]
            public class given_such_data : MySpecsFor<NotificationParseRawConfirmationResponseService>
            {


                protected override void Given()
                {


                    base.Given();
                }


            }

            [TestFixture]
            public class given_invalid_form_data : given_such_data
            {
                private string _formData;
                private EnumsPayMammothConnector.NotificationConfirmationStatus _result;


                protected override void Given()
                {
                    base.Given();
                    _formData = "invalidFormData";
                }

                protected override void When()
                {
                    _result = SUT.ParseRawConfirmationResponse(_formData);
                    base.When();
                }

                [Test]
                public void then_result_should_be_invalid()
                {
                    _result.ShouldEqual(EnumsPayMammothConnector.NotificationConfirmationStatus.InvalidFormData);


                }
            }

            [TestFixture]
            public class given_valid_form_data : given_such_data
            {
                protected string _formData;
                protected NotificationConfirmMessage _msg;


                protected override void Given()
                {
                    base.Given();
                    _msg = new NotificationConfirmMessage();
                    _msg.NotificationId = "Notification/67";
                    _msg.MessageType = EnumsPayMammothConnector.NotificationMessageType.RecurringPaymentSkipped;
                    _formData = CS.General_CS_v6.Util.JsonUtil.Serialize(_msg);


                    
                }

                [TestFixture]
                public class given_confirmation_is_not_ok : given_valid_form_data
                {
                    private EnumsPayMammothConnector.NotificationConfirmationStatus _result;


                    protected override void Given()
                    {
                        base.Given();
                        GetMockFor<INotificationsConfirmationService>()
                            .Setup(x => x.ConfirmNotification(new ReferenceLink<NotificationMessageData>(_msg.NotificationId),
                            _msg.MessageType)).Returns(false);
                    }

                    protected override void When()
                    {
                        base.When();
                        _result = SUT.ParseRawConfirmationResponse(_formData);
                    }

                    [Test]
                    public void then_result_should_be_not_confirmed()
                    {
                        _result.ShouldEqual(EnumsPayMammothConnector.NotificationConfirmationStatus.NotificationCouldNotBeConfirmed);
                    }
                }
                [TestFixture]
                public class given_confirmation_is_ok : given_valid_form_data
                {
                    private EnumsPayMammothConnector.NotificationConfirmationStatus _result;


                    protected override void Given()
                    {
                        base.Given();
                        GetMockFor<INotificationsConfirmationService>()
                            .Setup(x => x.ConfirmNotification(new ReferenceLink<NotificationMessageData>(_msg.NotificationId),
                            _msg.MessageType)).Returns(true);
                    }

                    protected override void When()
                    {
                        base.When();
                        _result = SUT.ParseRawConfirmationResponse(_formData);
                    }

                    [Test]
                    public void then_result_should_be_not_confirmed()
                    {
                        _result.ShouldEqual(EnumsPayMammothConnector.NotificationConfirmationStatus.OK);
                    }
                }

              
            }
        }
    }

}
