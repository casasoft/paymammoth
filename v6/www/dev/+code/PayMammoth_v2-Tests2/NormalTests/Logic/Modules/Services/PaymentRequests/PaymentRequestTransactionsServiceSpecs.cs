﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.Payments.Helpers;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2.Modules.Services.RecurringProfiles;
using Raven.Client;

namespace PayMammoth_v2_Tests.NormalTests.Logic.Modules.Services.PaymentRequests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v6.TestUtil;
    using BusinessLogic_CS_v6.Extensions;
    using CS.General_CS_v6.Extensions;
    

    [TestFixture]
    public class PaymentRequestTransactionsServiceSpecs
    {

        [TestFixture]
        public class MarkTransactionAsPaidSpecs : MySpecsFor<PaymentRequestTransactionDataService>
        {

/*
 * given this is called
	then last activity date should be updated
	then finished date should be updated
	then transaction should be marked as successful,
	then markRequestAsSuccessful should be called
	given this requires a recurring profile
		then create recurring profile is called
	given this does NOT require a recurring profile
		then create recurring profile is NOT called.
	
 * 
 * CONTINUE HERE.  Add tests such that this cannot be called on requests/transactions that are already marked as paid.
 */ 
            //cont here

            public class given_such_data : MySpecsFor<PaymentRequestTransactionDataService>
            {
                protected string _authorisationCode;
                protected bool _requiresManualIntervention;
                protected ReferenceLink<PaymentRequestTransactionData> _transactionId;
                protected Mock<IDocumentSession> _mockSession;
                protected PaymentRequestTransactionData _transaction;
                protected DateTimeOffset _currentDateTime;
                protected PaymentRequestData _request;


                protected override void Given()
                {
                    _authorisationCode = "test-auth";
                    _requiresManualIntervention = false;
                    _transactionId = new ReferenceLink<PaymentRequestTransactionData>("transaction/1");
                    _mockSession = new Mock<IDocumentSession>();
                    _transaction = new PaymentRequestTransactionData();


                    _mockSession.Setup(x => x.Load<PaymentRequestTransactionData>(_transactionId)).Returns(_transaction);

                    _currentDateTime = new DateTimeOffset(2013, 12, 31, 12, 6, 0, 0, new TimeSpan(0));
                    GetMockFor<ICurrentDateTimeRetrieverService>().Setup(x => x.GetCurrentDateTime()).Returns(_currentDateTime);

                    _request = new PaymentRequestData();
                    _request.Id = "request/1";


                    _transaction.PaymentRequestId = _request;


                    _mockSession.Setup(x => x.Load<PaymentRequestData>(_request.Id)).Returns(_request);

                    base.Given();
                }

               
            }


            public class given_this_is_called : given_such_data
            {
              

                protected override void Given()
                {
                  

                    base.Given();
                }

                protected override void When()
                {

                    SUT.MarkTransactionAsPaid(_transaction, _requiresManualIntervention, _authorisationCode, _mockSession.Object);
                    base.When();
                }

                [Test]
                public void then_details_are_updated()
                {
                    _transaction.DateLastActivity.ShouldEqual(_currentDateTime);
                    _transaction.DateFinished.ShouldEqual(_currentDateTime);
                    _transaction.IsSuccessful.ShouldBeTrue();
                    _transaction.AuthCode.ShouldEqual(_authorisationCode);
                    

                }
              
                [Test]

                public void then_mark_request_as_successful_is_called()
                {
                    GetMockFor<IPaymentRequestDataMarkerService>().Verify(x=>x.MarkRequestAsPaidSuccessfulAndSendNotification(_transaction.PaymentRequestId,
                        _transaction,_mockSession.Object),Times.Once());
                }
            }

            public class and_transaction_already_marked_as_paid : given_such_data
            {
                protected override void Given()
                {
                    base.Given();

                    _transaction.IsSuccessful = true;

                }

                protected override void When()
                {

                    Assert.Throws<InvalidOperationException>(()=> SUT.MarkTransactionAsPaid(_transaction, _requiresManualIntervention, _authorisationCode, _mockSession.Object));
                    
                }

                [Test]
                public void then_error_should_be_thrown()
                {

                }
            }
            public class and_request_already_marked_as_paid : given_such_data
            {
                protected override void Given()
                {
                    base.Given();

                    _request.PaymentStatus.Status = PaymentStatusInfo.PaymentStatus.Paid;

                }

                protected override void When()
                {

                    Assert.Throws<InvalidOperationException>(() => SUT.MarkTransactionAsPaid(_transaction, _requiresManualIntervention, _authorisationCode, _mockSession.Object));

                }

                [Test]
                public void then_error_should_be_thrown()
                {

                }
            }
            public class given_recurring_profile_is_not_required : given_such_data
            {


                protected override void Given()
                {
                    base.Given();
                    _request.RequestDetails.RecurringProfile.Required = false;
                    

                }

                protected override void When()
                {
                    SUT.MarkTransactionAsPaid(_transaction, _requiresManualIntervention, _authorisationCode, _mockSession.Object);
                    
                    base.When();
                }

                [Test]
                public void then_create_recurring_profile_should_not_be_called()
                {

                    GetMockFor<IRecurringProfilesService>().Verify(x=>x.CreateRecurringProfileIfRequired(It.IsAny<ReferenceLink<PaymentRequestData>>(),
                        It.IsAny<IDocumentSession>()),Times.Never());

                }
            }
            public class given_recurring_profile_is_required : given_such_data
            {


                protected override void Given()
                {
                    base.Given();
                    _request.RequestDetails.RecurringProfile.Required = true;


                }

                protected override void When()
                {
                    SUT.MarkTransactionAsPaid(_transaction, _requiresManualIntervention, _authorisationCode, _mockSession.Object);
                    
                    base.When();
                }

                [Test]
                public void then_create_recurring_profile_should_be_called()
                {

                    GetMockFor<IRecurringProfilesService>().Verify(x => x.CreateRecurringProfileIfRequired(
                        _transaction.PaymentRequestId,
                        _mockSession.Object),Times.Once());

                }
            }

        }
    }

}
