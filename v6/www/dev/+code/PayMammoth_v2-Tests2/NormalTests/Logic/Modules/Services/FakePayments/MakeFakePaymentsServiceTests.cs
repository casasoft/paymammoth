﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using CS.General_CS_v6.Classes.HelperClasses;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using PayMammoth_v2.Connector;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.FakePayments;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v2_Tests2.NormalTests.Logic.Modules.Services.FakePayments
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v6.TestUtil;
    using BusinessLogic_CS_v6.Extensions;
    using CS.General_CS_v6.Extensions;


    [TestFixture]
    public class MakeFakePaymentsServiceSpecs
    {
        public class class_under_test : MySpecsFor<MakeFakePaymentService>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time
            [TestFixture]
            public class MakeFakePaymentsSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {
                    private PaymentRequestData _paymentRequestData;
                    private WebsiteAccountData _websiteAccountData;
                    private EnumsPayMammothConnector.PaymentMethodType _paymentMethodToUse;


                    protected override void Given()
                    {
                        _paymentRequestData = new PaymentRequestData();
                        _paymentRequestData.Id = "Request/1";
                        _paymentRequestData.RequestDetails.ReturnUrls.SuccessUrl = "http://karl-test.com/success";
                        _websiteAccountData = new WebsiteAccountData();
                        _websiteAccountData.Id = "WebsiteAccount/Test123";
                        _paymentRequestData.LinkedWebsiteAccountId = new ReferenceLink<WebsiteAccountData>(_websiteAccountData.Id);
                        _websiteAccountData.FakePayments.FakePaymentKey = "FakeTest";

                        _paymentMethodToUse = EnumsPayMammothConnector.PaymentMethodType.PayPal_ExpressCheckout; ;

                        base.Given();
                    }

                    [TestFixture]
                    public class given_fake_payment_cannot_be_done : given_such_data
                    {
                        private StatusResult<MakeFakePaymentService.MakeFakePaymentResult, MakeFakePaymentResponse> _result;


                        protected override void Given()
                        {
                            
                            base.Given();
                            //add any code after base.Given!
                            base.setupSessionToLoadObject(_mockedSession, _websiteAccountData);
                            base.setupSessionToLoadObject(_mockedSession, _paymentRequestData);
                            
                            GetMockFor<ICheckIfFakePaymentEnabledAndKeyIsCorrectService>().Setup(
                                x => x.CheckIfFakePaymentEnabledAndKeyIsCorrect(_websiteAccountData, _websiteAccountData.FakePayments.FakePaymentKey))
                                .Returns(false);


                        }

                        protected override void When()
                        {
                            base.When();
                            _result = SUT.MakeFakePayment(_paymentRequestData,_paymentMethodToUse,_websiteAccountData.FakePayments.FakePaymentKey,
                                _mockedSession.Object);
                        }

                        [Test]
                        public void then_result_should_be_fake_payments_cannot_be_done()
                        {
                            _result.Status.ShouldEqual(MakeFakePaymentService.MakeFakePaymentResult.FakePaymentsNotEnabled);


                        }
                    }
                    [TestFixture]
                    public class given_fake_payment_can_be_done : given_such_data
                    {
                        private PaymentRequestTransactionData _fakePaymentRequestTransactionData;
                        private StatusResult<MakeFakePaymentService.MakeFakePaymentResult, MakeFakePaymentResponse> _result;


                        protected override void Given()
                        {

                            base.Given();
                            //add any code after base.Given!

                            GetMockFor<ICheckIfFakePaymentEnabledAndKeyIsCorrectService>().Setup(
                                x => x.CheckIfFakePaymentEnabledAndKeyIsCorrect(_websiteAccountData, _websiteAccountData.FakePayments.FakePaymentKey))
                                .Returns(true);

                            _fakePaymentRequestTransactionData = new PaymentRequestTransactionData();
                            _fakePaymentRequestTransactionData.Id = "Transaction/1";
                            GetMockFor<IPaymentRequestTransactionDataService>()
                                .Setup(x => x.CreateNewTransaction(
                                    It.Is<ReferenceLink<PaymentRequestData>>(refLink => (string)refLink == _paymentRequestData.Id),
                                    _paymentMethodToUse,_mockedSessionObject)).Returns(_fakePaymentRequestTransactionData);

                            base.setupSessionToLoadObject(_mockedSession, _websiteAccountData);
                            base.setupSessionToLoadObject(_mockedSession, _fakePaymentRequestTransactionData);
                            base.setupSessionToLoadObject(_mockedSession, _paymentRequestData);
                            

                        }

                        protected override void When()
                        {
                            base.When();
                            _result = SUT.MakeFakePayment(_paymentRequestData,
                                _paymentMethodToUse,
                                _websiteAccountData.FakePayments.FakePaymentKey,
                                _mockedSession.Object);
                        }

                        [Test]
                        public void then_result_should_be_success()
                        {
                            _result.Status.ShouldEqual(MakeFakePaymentService.MakeFakePaymentResult.Success);


                        }
                        [Test]
                        public void then_result_should_return_success_url_of_request()
                        {
                            _result.Result.RedirectUrl.ShouldEqual(_paymentRequestData.RequestDetails.ReturnUrls.SuccessUrl);


                        }
                        [Test]
                        public void then_transaction_details_are_ok()
                        {
                            _fakePaymentRequestTransactionData.IsFakePayment.ShouldBeTrue();
                            _fakePaymentRequestTransactionData.FakePaymentKeyUsed.ShouldEqual(_websiteAccountData.FakePayments.FakePaymentKey);

                            
                        }

                        [Test]
                        public void then_transaction_should_be_marked_as_paid()
                        {
                            GetMockFor<IPaymentRequestTransactionDataService>().Verify(x => x.MarkTransactionAsPaid(_fakePaymentRequestTransactionData,
                                false, It.IsAny<string>(), _mockedSession.Object), Times.Once());


                        }
                    }
                }
            }
        }


    }
}
