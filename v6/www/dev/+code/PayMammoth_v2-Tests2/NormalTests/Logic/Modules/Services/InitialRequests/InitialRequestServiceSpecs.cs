﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using PayMammoth_v2.Connector;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.InitialRequests;
using PayMammoth_v2.Modules.Services.InitialRequests.Helpers;
using Raven.Client;
using Raven.Client.Linq;

namespace PayMammoth_v2_Tests.NormalTests.Logic.Modules.Services.InitialRequests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using BusinessLogic_CS_v6.Extensions;
    using CS.General_CS_v6.Extensions;


    [TestFixture]
    public class InitialRequestServiceSpecs
    {
        [TestFixture]
        public class DecryptInitialRequestSpecs : MySpecsFor<InitialRequestService>
        {
            public class given_account_does_not_exist : MySpecsFor<InitialRequestService>
            {
                private InitialRequestEncryptedData _data;
                private InitialRequestDecryptResult _result;
                private Mock<IDocumentSession> _mockSession;


                protected override void Given()
                {
                    _mockSession = new Mock<IDocumentSession>();
                    
                    _data = new InitialRequestEncryptedData();
                    _data.WebsiteAccountCode = "TEST";
                    _data.Data = "123456";
                    base.Given();
                }

                protected override void When()
                {
                    _result = SUT.DecryptInitialRequest(_mockSession.Object, _data);
                    base.When();
                }

                [Test]
                public void then_appropriate_result_should_be_returned()
                {
                    _result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.WebsiteAccountDoesNotExist);
                    _result.InitialRequest.ShouldBeNull();

                }
            }

            public class given_account_exists : MySpecsFor<InitialRequestService>
            {
                protected InitialRequestEncryptedData _data;
                private InitialRequestDecryptResult _result;
                protected Mock<IDocumentSession> _mockSession;
                

                protected override void Given()
                {
                    _mockSession = new Mock<IDocumentSession>();
                    _data = new InitialRequestEncryptedData();
                    _data.WebsiteAccountCode = "ValidAccount";
                    _data.Data = "123456";
                    base.Given();
                }

                protected override void When()
                {
                    base.When();
                }

               
            }

            public class and_data_cannot_be_decrypted : given_account_exists
            {

                protected WebsiteAccountData _websiteAccount;
                private InitialRequestDecryptResult _result;

                protected override void Given()
                {
                    base.Given();
                    _websiteAccount = new WebsiteAccountData();
                    _websiteAccount.SecretWord = "SecretWord";
                    _mockSession.Setup(x => x.Load<WebsiteAccountData>(_data.WebsiteAccountCode)).Returns(_websiteAccount);
                   
                    
                }

                protected override void When()
                {
                    _result = SUT.DecryptInitialRequest(_mockSession.Object, _data);

                    base.When();
                }

                [Test]
                public void then_appropriate_result_should_be_returned()
                {
                    _result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.CouldNotDecrypt);
                    _result.InitialRequest.ShouldBeNull();

                }
            }
            public class and_data_can_be_decrypted_but_not_a_valid_data_object : given_account_exists
            {

                protected WebsiteAccountData _websiteAccount;
                private InitialRequestDecryptResult _result;

                protected override void Given()
                {
                    base.Given();
                    _websiteAccount = new WebsiteAccountData();
                    _websiteAccount.SecretWord = "SecretWord";
                    _mockSession.Setup(x => x.Load<WebsiteAccountData>(_data.WebsiteAccountCode)).Returns(_websiteAccount);

                    _data.Data = CS.General_CS_v6.Util.CryptographyUtil.Encrypt("invalidData", _websiteAccount.SecretWord);
                }

                protected override void When()
                {
                    _result = SUT.DecryptInitialRequest(_mockSession.Object, _data);

                    base.When();
                }

                [Test]
                public void then_appropriate_result_should_be_returned()
                {
                    _result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.InvalidData);
                    _result.InitialRequest.ShouldBeNull();

                }
            }
            public class and_data_can_be_decrypted_and_valid : given_account_exists
            {

                protected WebsiteAccountData _websiteAccount;
                private InitialRequestDecryptResult _result;

                protected override void Given()
                {
                    base.Given();
                    _websiteAccount = new WebsiteAccountData();
                    _websiteAccount.Id = "WebsiteAccount/1";

                    _websiteAccount.SecretWord = "SecretWord";
                    _mockSession.Setup(x => x.Load<WebsiteAccountData>(_data.WebsiteAccountCode)).Returns(_websiteAccount);

                    _data.Data = CS.General_CS_v6.Util.CryptographyUtil.Encrypt(
@"
{
    ""Pricing"" : {
        ""HandlingAmount"":7.95
    }
    ,
    ""Details"":{
        ""ClientReference"":""Ref1""
    },
     ""ItemDetails"": 
    [
        {
            ""Title"":""Item1"",
            ""Quantity"":5
        },
        {
            ""Title"":""Item2"",
            ""Quantity"":7
        }
    ]
}
                "
                        , _websiteAccount.SecretWord);
                }

                protected override void When()
                {
                    _result = SUT.DecryptInitialRequest(_mockSession.Object, _data);

                    base.When();
                }

                [Test]
                public void then_appropriate_result_should_be_returned()
                {
                    _result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.Success);
                    _result.InitialRequest.Pricing.HandlingAmount.ShouldEqual((decimal)7.95);
                    _result.InitialRequest.Details.ClientReference.ShouldEqual("Ref1");
                    _result.InitialRequest.ItemDetails.Count.ShouldEqual(2);
                    _result.InitialRequest.ItemDetails[0].Title.ShouldEqual("Item1");
                    _result.InitialRequest.ItemDetails[0].Quantity.ShouldEqual(5);
                    _result.InitialRequest.ItemDetails[1].Title.ShouldEqual("Item2");
                    _result.InitialRequest.ItemDetails[1].Quantity.ShouldEqual(7);
                    _result.WebsiteAccountId.ToString().ShouldEqual(_websiteAccount.Id);



                }
            }
        }

        [TestFixture]
        public class ParseRawStringToInitialRequestSpecs : MySpecsFor<InitialRequestService>
        {
            public class given_invalid_string : MySpecsFor<InitialRequestService>
            {
                private string _rawData;
                private InitialRequestEncryptedData _result;


                protected override void Given()
                {
                    _rawData = "fsadfsdas";
                    base.Given();
                }

                protected override void When()
                {
                    _result = SUT.ParseRawStringToInitialRequest(_rawData);
                    base.When();
                }

                [Test]
                public void then_return_should_be_null()
                {
                    _result.ShouldEqual(null);
                   

                }
            }

            public class given_valid_string : MySpecsFor<InitialRequestService>
            {
                private string _rawData;
                private InitialRequestEncryptedData _result;


                protected override void Given()
                {
                    _rawData = @"{
    ""WebsiteAccountCode"":""Account"",
    ""Data"":""ABCDEF123456789""
}";
                    base.Given();
                }

                protected override void When()
                {
                    _result = SUT.ParseRawStringToInitialRequest(_rawData);

                    base.When();
                }

                [Test]
                public void then_result_should_be_parsed_successfully()
                {
                    _result.Data.ShouldEqual("ABCDEF123456789");
                    _result.WebsiteAccountCode.ShouldEqual("Account");


                }
            }

        }
    }

}
