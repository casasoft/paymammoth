﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using PayMammoth_v2.Presentation.Services.Test;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;

namespace PayMammoth_v2_Tests.NormalTests.Presentation.Services.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;

    [TestFixture]
    public class MyTestServiceSpecs
    {

        [TestFixture]
        public class AddSpecs : MySpecsFor<MyTestService>
        { 
            
            
            /*
             * Test Data

             Num 1 = 40
             Num 2 = 28
             
             */
            public class given_the_above_test_data : MySpecsFor<MyTestService>
            {
                private int _num1;
                private int _num2;
                private int _result;


                protected override void Given()
                {
                    _num1 = 40;
                    _num2 = 28;


                    base.Given();
                }

                protected override void When()
                {
                    _result = SUT.AddValues(_num1, _num2);
                    base.When();
                }

                [Test]
                public void then_result_should_be_68()
                {
                    _result.ShouldEqual(68);
                }
            }

        }
    }

    
}
