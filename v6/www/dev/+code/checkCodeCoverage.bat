del .\coverage-report /Q
md .\coverage-report
OpenCover.Console.exe -register:user -targetdir:PayMammoth_v2-Test\bin\Debug "-target:..\..\..\..\..\..\Resources\Components\nUnit\NUnit-2.6.2\NUnit-2.6.2\bin\nunit-console-x86.exe" "-targetargs:PayMammoth_v2-Test.dll" -filter:"+[BusinessLogic_CS_v5]* +[PayMammoth_v2]* +[General_CS_v5]* +[MvcGeneral_CS_v5]*" -output:.\coverage-report\opencover-report.xml

ReportGenerator -reports:coverage-report\opencover-report.xml -targetdir:coverage-report\ -reportTypes:Html
.\coverage-report\index.htm