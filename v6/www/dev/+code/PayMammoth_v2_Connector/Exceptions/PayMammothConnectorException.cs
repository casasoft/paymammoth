﻿using System;

namespace PayMammoth_v6.Connector.Exceptions
{
    public class PayMammothConnectorException : Exception
    {
        public PayMammothConnectorException(string msg, Exception innerEx) : base(msg,innerEx)
        {

        }
    }
}
