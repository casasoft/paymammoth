﻿namespace PayMammoth_v6.Connector.HelperClasses
{
    public class SetupRecurringProfileResult
    {
        public EnumsPayMammothConnector.RecurringProfileCreateStatusMsg Status { get; set; }
        public string Message { get; set; }
    }
}
