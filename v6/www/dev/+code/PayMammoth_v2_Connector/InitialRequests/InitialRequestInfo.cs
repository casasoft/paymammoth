﻿using System.Collections.Generic;

namespace PayMammoth_v6.Connector.InitialRequests
{
    public class InitialRequestInfo
    {
        public InitialRequestInfo()
        {
            this.Details = new PaymentRequestDetails();
            this.Pricing = new PaymentRequestPricingDetails();
            this.ReturnUrls = new PaymentRequestReturnUrlsInfo();
            this.RecurringProfile = new PaymentRequestRecurringProfileInfo();
            this.ItemDetails = new List<PaymentRequestItemLine>();
        }

        private PaymentRequestDetails _m_PaymentRequestDetails;

        public PaymentRequestDetails Details
        {
            get
            {
                if (_m_PaymentRequestDetails == null)
                {
                    _m_PaymentRequestDetails = new PaymentRequestDetails();
                }
                return _m_PaymentRequestDetails;
            }
            set { _m_PaymentRequestDetails = value; }
        }

        private PaymentRequestPricingDetails _m_PaymentRequestPricingDetails;

        public PaymentRequestPricingDetails Pricing
        {
            get
            {
                if (_m_PaymentRequestPricingDetails == null)
                {
                    _m_PaymentRequestPricingDetails = new PaymentRequestPricingDetails();
                }
                return _m_PaymentRequestPricingDetails;
            }
            set { _m_PaymentRequestPricingDetails = value; }
        }
       
        public EnumsPayMammothConnector.SupportedLanguage Language { get; set; }
        public string OrderLinkOnClientWebsite { get; set; }
        
        /// <summary>
        /// Required - At least one item.
        /// </summary>
        public List<PaymentRequestItemLine> ItemDetails { get; set; }


        private PaymentRequestRecurringProfileInfo _m_PaymentRequestRecurringProfileInfo;

        public PaymentRequestRecurringProfileInfo RecurringProfile
        {
            get
            {
                if (_m_PaymentRequestRecurringProfileInfo == null)
                {
                    _m_PaymentRequestRecurringProfileInfo = new PaymentRequestRecurringProfileInfo();
                }
                return _m_PaymentRequestRecurringProfileInfo;
            }
            set { _m_PaymentRequestRecurringProfileInfo = value; }
        }

        private PaymentRequestReturnUrlsInfo _m_PaymentRequestReturnUrlsInfo;

        /// <summary>
        /// Required.
        /// </summary>
        public PaymentRequestReturnUrlsInfo ReturnUrls
        {
            get
            {
                if (_m_PaymentRequestReturnUrlsInfo == null)
                {
                    _m_PaymentRequestReturnUrlsInfo = new PaymentRequestReturnUrlsInfo();
                }
                return _m_PaymentRequestReturnUrlsInfo;
            }
            set { _m_PaymentRequestReturnUrlsInfo = value; }
        }

        public string NotificationUrl { get; set; }


        private PaymentRequestExpirationInfo _m_PaymentRequestExpirationInfo;

        public PaymentRequestExpirationInfo ExpirationInfo
        {
            get
            {
                if (_m_PaymentRequestExpirationInfo == null)
                {
                    _m_PaymentRequestExpirationInfo = new PaymentRequestExpirationInfo();
                }
                return _m_PaymentRequestExpirationInfo;
            }
            set { _m_PaymentRequestExpirationInfo = value; }
        }

    }
}
