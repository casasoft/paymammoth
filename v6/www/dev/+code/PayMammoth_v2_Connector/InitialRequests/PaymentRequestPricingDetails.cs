﻿namespace PayMammoth_v6.Connector.InitialRequests
{
    public class PaymentRequestPricingDetails
    {
        public decimal ShippingAmount { get; set; }
        public decimal HandlingAmount { get; set; }

        /// <summary>
        /// Required.  Must be one from the ISO4217 standard
        /// </summary>
        public string CurrencyCode3Letter { get; set; }
    }
}
