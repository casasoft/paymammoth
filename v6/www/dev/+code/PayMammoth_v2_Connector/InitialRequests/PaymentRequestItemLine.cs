﻿namespace PayMammoth_v6.Connector.InitialRequests
{
    public class PaymentRequestItemLine 
    {
        /// <summary>
        /// Required
        /// </summary>
        public string Title { get; set; }
        public string Description { get; set; }
        /// <summary>
        /// Required. Cannot be 0 or less.
        /// </summary>
        public decimal UnitPrice { get; set; }
        /// <summary>
        /// Required.  Cannot be 0 or less
        /// </summary>
        public int Quantity { get; set; }
        public decimal TaxAmountPerUnit { get; set; }

    }
}
