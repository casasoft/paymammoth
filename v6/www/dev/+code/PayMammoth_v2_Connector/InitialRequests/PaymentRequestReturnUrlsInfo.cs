﻿namespace PayMammoth_v6.Connector.InitialRequests
{
    public class PaymentRequestReturnUrlsInfo
    {
        /// <summary>
        /// Required.  The URL for the user to be redirected to, if they choose to cancel payment.  Note that there is no 'FailureUrl' because
        /// failure to do payment are constantly redirected to payMammoth, not the main website.
        /// </summary>
        public string CancelUrl { get; set; }
        /// <summary>
        /// Required.  The URL for the user to be redirected to, upon success.
        /// </summary>
        public string SuccessUrl { get; set; }

    }
}
