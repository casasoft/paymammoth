﻿namespace PayMammoth_v6.Connector.InitialRequests
{
    public class PaymentRequestRecurringProfileInfo
    {
        public bool Required { get; set; }
        public int IntervalFrequency { get; set; }
        public Connector.EnumsPayMammothConnector.RecurringBillingPeriod IntervalType { get; set; }
        public int MaxFailedAttempts { get; set; }
        public int? TotalBillingCycles { get; set; }
    }
}
