﻿namespace PayMammoth_v6.Connector.InitialRequests
{
    public class InitialRequestEncryptedData
    {
        /// <summary>
        /// This should be equal to primary key of website account.
        /// </summary>
        public string WebsiteAccountCode { get; set; }
        /// <summary>
        /// This should be encrypted
        /// </summary>
        public string Data { get; set; }
    }
}
