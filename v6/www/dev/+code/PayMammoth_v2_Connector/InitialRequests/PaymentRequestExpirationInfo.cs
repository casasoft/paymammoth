﻿using System;

namespace PayMammoth_v6.Connector.InitialRequests
{
    public class PaymentRequestExpirationInfo
    {
        /// <summary>
        /// This means that if this is set, the payment request will expire if this time is elapsed, and a message is sent back
        /// to the client website that the request is expired
        /// </summary>
        public DateTimeOffset? ExpiresAt { get; set; }
    }
}
