﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PayMammoth_v6.Connector.InitialRequests
{
    public class InitialRequestResponseMessage
    {   
        /// <summary>
        /// This is the PayMammoth request Id as generated
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// This is the url to redirect to on paymammoth, for payment.  This is only filled if Status = Success
        /// </summary>
        public string PayMammothUrlToRedirectTo { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public EnumsPayMammothConnector.InitialRequestResponseStatus Status { get; set; }
    }
}
