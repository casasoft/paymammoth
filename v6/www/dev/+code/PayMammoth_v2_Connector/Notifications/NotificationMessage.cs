﻿using System;
using System.Text;

namespace PayMammoth_v6.Connector.Notifications
{
    public class NotificationMessage
    {
        public EnumsPayMammothConnector.NotificationMessageType MessageType { get; set; }
        /// <summary>
        /// The PayMammoth payment request id (GUID)
        /// </summary>
        public string PaymentRequestId { get; set; }
        /// <summary>
        /// The identifier related to this notification message.  This is some information originally passed from the client website
        /// to identify the transaction. Below are the values for different message types:
        /// 
        /// - CreateImmediatePayment: InitialRequestInfo.RequestDetails.Details.OrderReference
        /// </summary>
        public string Identifier { get; set; }
        public string Message { get; set; }
        /// <summary>
        /// The retry count of this message, i.e how many times this notification message has been tried. 
        /// </summary>
        public int RetryCount { get; set; }
        public DateTimeOffset DateTimeSent { get; set; }
        /// <summary>
        /// The PayMammoth unique notification. GUID 
        /// </summary>
        public string NotificationId { get; set; }
        public override string ToString()
        {
            return string.Format("{0} [Identifier: {1} | NotificationId: {2}]", this.MessageType, this.Identifier, this.NotificationId);
        }


        public string GetNotificationMessageAsString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("MessageType: " + this.MessageType);
            sb.AppendLine("PaymentRequestId: " + this.PaymentRequestId);
            sb.AppendLine("Identifier: " + this.Identifier);
            sb.AppendLine("RetryCount: " + this.RetryCount);
            sb.AppendLine("DateTimeSent: " + this.DateTimeSent);
            sb.AppendLine("NotificationId: " + this.NotificationId);
            sb.AppendLine("Message: " + this.Message);
            return sb.ToString();
            

        }

    }
}
