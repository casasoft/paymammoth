﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PayMammoth_v6.Connector.Notifications
{
    public class NotificationConfirmMessage
    {
        public string NotificationId { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public EnumsPayMammothConnector.NotificationMessageType MessageType { get; set; }

    }
}
