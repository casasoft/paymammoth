﻿using System;
using System.Net;
using NLog;
using PayMammoth_v6.Connector.Notifications;
using PayMammoth_v6.Connector.Services.HttpRequests;
using PayMammoth_v6.Connector.Util;

namespace PayMammoth_v6.Connector.Services
{
    
    public interface IConfirmNotificationMessageOnPayMammothService
    {
        bool ConfirmNotificationMessageOnPayMammoth(string formData);
    }

   
    public class ConfirmNotificationMessageOnPayMammothService : IConfirmNotificationMessageOnPayMammothService
    {

        private static readonly ConfirmNotificationMessageOnPayMammothService _instance = new ConfirmNotificationMessageOnPayMammothService(HttpRequestSenderService.Instance);

        public static ConfirmNotificationMessageOnPayMammothService Instance
        {
            get { return _instance; }
        }

        private static readonly Logger _log = LogManager.GetCurrentClassLogger();

        private readonly IHttpRequestSenderService _httpRequestSenderService;
        //private static readonly ConfirmNotificationMessageOnPayMammothService _instance = new ConfirmNotificationMessageOnPayMammothService();
        

        //public static ConfirmNotificationMessageOnPayMammothService Instance
        //{
        //    get { return _instance; }
        //}

        public ConfirmNotificationMessageOnPayMammothService(IHttpRequestSenderService httpRequestSenderService)
        {
            _httpRequestSenderService = httpRequestSenderService;
        }

        /// <summary>
        /// This will confirm back on PayMammoth, the notification message data sent by to the client.
        /// </summary>
        /// <param name="formData"></param>
        /// <returns></returns>
        public bool ConfirmNotificationMessageOnPayMammoth(string formData)
        {
            //todo: [For: Backend | 2014/03/26] unit-test NotificationsService.ConfirmNotificationMessageOnPayMammoth (WrittenBy: Backend)

            bool confirmed = false;
            NotificationMessage msg = JsonUtil.Deserialise<NotificationMessage>(formData, throwErrorIfCannotDeserialize: false);
            if (msg != null)
            {
                try
                {
                    NotificationConfirmMessage confirmMessage = new NotificationConfirmMessage();
                    confirmMessage.MessageType = msg.MessageType;
                    confirmMessage.NotificationId = msg.NotificationId;
                    string confirmMessageJson = JsonUtil.Serialize(confirmMessage);

                    var webResponse = _httpRequestSenderService.SendHttpRequest(Constants.PayMammoth_ConfirmMsgUrl,
                               EnumsPayMammothConnector.HttpMethod.Post, null,
                               confirmMessageJson, null, 60 * 1000, null, null);
                    var statusCode = webResponse.StatusCode;
                    if (statusCode == HttpStatusCode.OK)
                    {
                        var responseString = webResponse.GetResponseAsString();
                        if (responseString == Constants.RESPONSE_OK)
                        {
                            confirmed = true;
                        }
                        else
                        {
                            _log.Warn("Notification message could not be confirmed. Result: {0} | FormData: {1}", responseString,confirmMessageJson);
                        }
                    }
                    else
                    {
                        _log.Warn("Notification message could not be confirmed. StatusCode: {0}",statusCode);
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorException("Error occurred in ConfirmNotificationMessageOnPayMammoth", ex);
                    confirmed = false;
                }

            }
            else
            {
                _log.Trace("Notification message coult not be parsed. FormData: '{0}'", formData);
                
            }
            return confirmed;
        }



    }
}
