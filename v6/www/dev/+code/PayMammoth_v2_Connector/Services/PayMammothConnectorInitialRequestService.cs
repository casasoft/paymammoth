﻿using System;
using System.Net;
using NLog;
using PayMammoth_v6.Connector.Exceptions;
using PayMammoth_v6.Connector.InitialRequests;
using PayMammoth_v6.Connector.Services.HttpRequests;
using PayMammoth_v6.Connector.Util;

namespace PayMammoth_v6.Connector.Services
{
    public interface IPayMammothConnectorInitialRequestService
    {
        InitialRequestResponseMessage CreatePaymentRequestOnPayMammoth(
            string websiteAccountCode, 
            string secretWord,
            InitialRequestInfo initialRequestInfo);
    }


    public class PayMammothConnectorInitialRequestService : IPayMammothConnectorInitialRequestService
    {
        private static readonly PayMammothConnectorInitialRequestService _instance = new PayMammothConnectorInitialRequestService(HttpRequestSenderService.Instance);

        public static PayMammothConnectorInitialRequestService Instance
        {
            get { return _instance; }
        }

        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        //private static readonly InitialRequestService _instance = new InitialRequestService(HttpRequestSenderService.Instance);
        private readonly IHttpRequestSenderService _httpRequestSenderService;

        //public static InitialRequestService Instance
        //{
        //    get { return _instance; }
        //}

        public PayMammothConnectorInitialRequestService(
            IHttpRequestSenderService httpRequestSenderService)
        {
            _httpRequestSenderService = httpRequestSenderService;
        }

        public InitialRequestResponseMessage CreatePaymentRequestOnPayMammoth(
            string websiteAccountCode, 
            string secretWord,
            InitialRequestInfo initialRequestInfo)
        {

            if (string.IsNullOrWhiteSpace(websiteAccountCode)) throw new InvalidOperationException("websiteAccountCode cannot be empty or null");
            if (string.IsNullOrWhiteSpace(secretWord)) throw new InvalidOperationException("secretWord cannot be empty or null");
            //------------
            InitialRequestResponseMessage result = new InitialRequestResponseMessage();
            result.Status = EnumsPayMammothConnector.InitialRequestResponseStatus.CouldNotConnect;

            InitialRequestEncryptedData encryptedData = new InitialRequestEncryptedData();
            encryptedData.WebsiteAccountCode = websiteAccountCode;

            string initialRequestAsJson = JsonUtil.Serialize(initialRequestInfo);
            string encryptedInitialRequestJson = EncryptionUtil.Encrypt(initialRequestAsJson,secretWord);

            encryptedData.Data = encryptedInitialRequestJson;

            string encryptedJson = JsonUtil.Serialize(encryptedData);
            
            try
            {
                var defaultHeaders = _httpRequestSenderService.GetDefaultHeaderCollectionForPost();

                
                

                var response = _httpRequestSenderService.SendHttpRequest(Constants.PayMammoth_InitialRequestUrl,
                     EnumsPayMammothConnector.HttpMethod.Post, 
                     
                     defaultHeaders, encryptedJson);
                _log.Debug("Response received. Status: {0}", response.StatusCode);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string responseResultJson = response.GetResponseAsString();
                    var responseMsg = JsonUtil.Deserialise<InitialRequestResponseMessage>(responseResultJson);
                    result = responseMsg;
                }
                else
                {
                    _log.Warn("Response status code not OK. Status: {0}", response.StatusCode);
                    result.Status = EnumsPayMammothConnector.InitialRequestResponseStatus.ErrorWhenSendingMessage; 
                }
            }
            catch (Exception ex)
            {
                throw new PayMammothConnectorException(
                    string.Format("Cound not send request to paymammoth - PayMammothUrl: {0}", Constants.PayMammoth_InitialRequestUrl)
                    , ex);
                
            }
            return result;
        }

    }
}
