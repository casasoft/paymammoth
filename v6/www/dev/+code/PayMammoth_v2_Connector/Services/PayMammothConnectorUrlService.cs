﻿using System;
using System.Web;

namespace PayMammoth_v6.Connector.Services
{
    public interface IPayMammothConnectorUrlService
    {
        [Obsolete("Such information is now being passed in the InitialRequestResponseMessage.PayMammothUrlToRedirectTo")]
        string GetPayMammothUrlForRequest(string payMammothRequestId, EnumsPayMammothConnector.SupportedLanguage language);
        
    }

    
    public class PayMammothConnectorUrlService : IPayMammothConnectorUrlService
    {
        private static readonly PayMammothConnectorUrlService _instance = new PayMammothConnectorUrlService();

        public static PayMammothConnectorUrlService Instance
        {
            get { return _instance; }
        }
       
        //private static readonly PayMammothConnectorConnectorUrlService _instance = new PayMammothConnectorConnectorUrlService();

        //public static PayMammothConnectorConnectorUrlService Instance
        //{
        //    get { return _instance; }
        //}

        /// <summary>
        /// The template for the PayMammoth Url.  {0} is the base url. {1} is the language code (2letter). {2} is the requestID
        /// </summary>
        public static string PayMammothUrlTemplate = "{0}{1}/payment/?" + PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER + "={2}";

        
        public string GetPayMammothUrlForRequest(string payMammothRequestId, EnumsPayMammothConnector.SupportedLanguage language)
        {
            
            //todo: [For: Karl | 2014/05/05] make this use the supported language (WrittenBy: Karl)        			
            return string.Format(PayMammothUrlTemplate, 
                                            Constants.PayMammoth_ServerUrl, 
                                            HttpUtility.UrlEncode("en"), 
                                            HttpUtility.UrlEncode(payMammothRequestId));
        }
       
    }
}
