﻿using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace PayMammoth_v6.Connector.Services.HttpRequests
{
    public interface IHttpRequestSenderService
    {
        IHttpWebResponse SendHttpRequest(string url, EnumsPayMammothConnector.HttpMethod method,
            WebHeaderCollection headers, string postData, string contentType = null, int timeoutInMs = 60000, X509Certificate x509 = null,
            string userAgent = "UserAgent");


        WebHeaderCollection GetDefaultHeaderCollectionForPost();

    }

    public class HttpRequestSenderService : IHttpRequestSenderService
    {
        private static readonly HttpRequestSenderService _instance = new HttpRequestSenderService();

        public static HttpRequestSenderService Instance
        {
            get { return _instance; }
        }
        public HttpRequestSenderService()
        {

        }


        /// <summary>
        /// Submits an HTTP request. It is important that once done, you close the HttpWebResponse!  Uses UTF8 encoding
        /// </summary>
        /// <param name="url">url to send to</param>
        /// <param name="method">method, one of 'POST' or 'GET'</param>
        /// <param name="headers">a collection of headers</param>
        /// <param name="postData">post data</param>
        /// <param name="contentType">content type.  If POST and null, defaults to 'application/x-www-form-urlencoded'</param>
        /// <param name="timeoutInMs">if </param>
        /// <param name="x509">The response headers</param>
        /// <param name="userAgent">The response headers</param>
        /// <exception cref="WebException">If a WebException is thrown, use the Response and Status properties of the exception to determine the response from the server.</exception>
        /// <returns>the response</returns>
        public IHttpWebResponse SendHttpRequest(string url, EnumsPayMammothConnector.HttpMethod method,
            WebHeaderCollection headers, string postData, string contentType = null, int timeoutInMs = 60000, X509Certificate x509 = null,
            string userAgent = "UserAgent")
        {
            postData = postData ?? "";



            if (!string.IsNullOrEmpty(postData) && method == EnumsPayMammothConnector.HttpMethod.Get)
                throw new InvalidOperationException("Cannot send a GET request and include POST data");

            if (contentType == null && headers != null && !string.IsNullOrEmpty(headers["content-type"]))
                contentType = headers["content-type"];
            if (contentType == null && method == EnumsPayMammothConnector.HttpMethod.Post)
                contentType = "application/x-www-form-urlencoded";
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);
            objRequest.AllowWriteStreamBuffering = true;
            

            objRequest.Timeout = timeoutInMs;
            
            
            objRequest.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
            objRequest.ContentType = contentType;
            objRequest.Method = method.ToString();

            //var enc = System.Text.Encoding.UTF8;

            //int totalByteCount = enc.GetByteCount(postData);


            if (null != x509)
            {
                objRequest.ClientCertificates.Add(x509);
            }
            if (method == EnumsPayMammothConnector.HttpMethod.Post)
            {

                var bytes = Encoding.UTF8.GetBytes(postData);
                var requestStream = objRequest.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

            }
            
            //objRequest.ContentLength = postData.Length;
            HttpWebResponse response = (HttpWebResponse)objRequest.GetResponse();
            
            var result = new IHttpWebResponseImpl(response);
            return result;


        }


        public WebHeaderCollection GetDefaultHeaderCollectionForPost()
        {
            WebHeaderCollection headers = new WebHeaderCollection();

            headers.Add("Cache-Control", "no-cache");
            //client.Headers.Add("Accept-Encoding", "gzip, deflate");
            headers.Add("Accept-Language", "en-us");
            headers.Add("Content-Type", "application/x-www-form-urlencoded");
            return headers;
        }
    }
}
