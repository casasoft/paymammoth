﻿using System.IO;
using System.Net;

namespace PayMammoth_v6.Connector.Services.HttpRequests
{
    public class IHttpWebResponseImpl: IHttpWebResponse
    {
        private readonly HttpWebResponse _response;
        public IHttpWebResponseImpl(HttpWebResponse webResponse)
        {
            
            this._response = webResponse;
        }

        public HttpWebResponse HttpWebResponse
        {
            get
            {
                
                return _response;
                
            }
            
        }


        public HttpStatusCode StatusCode
        {
            get { return _response.StatusCode; }
        }

        public string GetResponseAsString()
        {
            var responseStream = _response.GetResponseStream();
            
            StreamReader sr = new StreamReader(responseStream);
            var result = sr.ReadToEnd();
            return result;
        }


        public void Close()
        {
            _response.Close();
            
            
        }
        public void Dispose()
        {
            _response.Dispose();


        }


        public Stream GetResponseStream()
        {
            return _response.GetResponseStream();
        }
    }

    public interface IHttpWebResponse
    {
        void Dispose();
        void Close();
         HttpWebResponse HttpWebResponse { get; }
        HttpStatusCode StatusCode { get; }
        string GetResponseAsString();

        Stream GetResponseStream();
    }
}
