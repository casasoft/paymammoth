﻿using NLog;
using PayMammoth_v6.Connector.Notifications;
using PayMammoth_v6.Connector.Util;

namespace PayMammoth_v6.Connector.Services
{
    public interface INotificationsHandlerService
    {
        string HandleNotificationResponseData(string data);
        event NotificationsHandlerService.OnNotificationMessageReceviedDelegate OnNotificationMessageRecevied;
    }

 
    public class NotificationsHandlerService : INotificationsHandlerService
    {
        private static readonly NotificationsHandlerService _instance = new NotificationsHandlerService(ConfirmNotificationMessageOnPayMammothService.Instance);

        public static NotificationsHandlerService Instance
        {
            get { return _instance; }
        }
        public NotificationsHandlerService(IConfirmNotificationMessageOnPayMammothService confirmNotificationMessageOnPayMammothService)
        {
            _confirmNotificationMessageOnPayMammothService = confirmNotificationMessageOnPayMammothService;
        }

        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IConfirmNotificationMessageOnPayMammothService _confirmNotificationMessageOnPayMammothService;

        public delegate void OnNotificationMessageReceviedDelegate(NotificationMessage msg);

        /// <summary>
        /// This event is called when a notification is successfully received and confirmed back with PayMammoth
        /// </summary>
        public event OnNotificationMessageReceviedDelegate OnNotificationMessageRecevied;

        public string HandleNotificationResponseData(string data)
        {
            string response = Constants.RESPONSE_ERROR;


            bool confirmed = _confirmNotificationMessageOnPayMammothService.ConfirmNotificationMessageOnPayMammoth(data);
            if (confirmed && OnNotificationMessageRecevied != null)
            {
                NotificationMessage msg = JsonUtil.Deserialise<NotificationMessage>(data,
                    throwErrorIfCannotDeserialize: false);

                response = Constants.RESPONSE_OK;
                ThreadingUtil.CallMethodAsAsyncTask(() => OnNotificationMessageRecevied(msg));
            }
            return response;
        }
    }
}
