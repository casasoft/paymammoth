﻿namespace PayMammoth_v6.Connector
{
    public static partial class Constants
    {
        public const int k = 5;
        public const string PARAM_DATA= "data";
        public const string RESPONSE_OK = "OK";
        public const string RESPONSE_ERROR = "ERROR";
        
        public const string PARAM_IDENTIFIER = "identifier";
        public const string PARAM_TransactionId = "transactionId";

        /// <summary>
        /// This must always end in a slash, and include http://
        /// </summary>
        public static string PayMammoth_ServerUrl = "http://www.v2.paymammoth.com/";

        public static string PayMammoth_InitialRequestUrl
        {

            get
            {
                //todo: [For: Backend | 2014/04/03] These URLs need to be updated (WrittenBy: Karl)        			
                return PayMammoth_ServerUrl + "_tests/initialrequest.ashx";

            }
        }
        public static string PayMammoth_ConfirmMsgUrl
        {

            get
            {
                //todo: [For: Backend | 2014/04/03] These URLs need to be updated (WrittenBy: Karl)        			
                return PayMammoth_ServerUrl + "_tests/confirmmsg.ashx";

            }
        }

        //public static string HttpPost(string URI, string formData)
        //{
        //    System.Net.WebRequest req = System.Net.WebRequest.Create(URI);
        //    //req.Proxy = new System.Net.WebProxy(ProxyString, true);
        //    //Add these, as we're doing a POST
        //    req.ContentType = "application/x-www-form-urlencoded";
        //    req.Method = "POST";
        //    //We need to count how many bytes we're sending. Post'ed Faked Forms should be name=value&
        //    byte[] bytes = System.Text.Encoding.ASCII.GetBytes(formData);
        //    req.ContentLength = bytes.Length;
        //    System.IO.Stream os = req.GetRequestStream();
        //    os.Write(bytes, 0, bytes.Length); //Push it out there
        //    os.Close();
        //    System.Net.WebResponse resp = req.GetResponse();
        //    if (resp == null) return null;
        //    System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
        //    return sr.ReadToEnd().Trim();
        //}
        //public static void TestSendRequest5()
        //{
        //    string url = "http://localhost/_tests/initialrequest.ashx";
        //    string result = PayMammoth_v6.Connector.Constants.HttpPost(url, "testData");
        //}
    
    }
}
