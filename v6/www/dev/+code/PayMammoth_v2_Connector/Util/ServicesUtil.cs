﻿using System;
using System.Collections.Generic;

namespace PayMammoth_v6.Connector.Util
{
    public static class ServicesUtil
    {
        private static List<Type> _getAllServiceTypesToRegisterWithIoc_Result = null;
        public static List<Type> GetAllServiceTypesToRegisterWithIoc()
        {
            if (_getAllServiceTypesToRegisterWithIoc_Result == null)
            {
                _getAllServiceTypesToRegisterWithIoc_Result = new List<Type>();
                _getAllServiceTypesToRegisterWithIoc_Result.Add(typeof(Services.ConfirmNotificationMessageOnPayMammothService));
                _getAllServiceTypesToRegisterWithIoc_Result.Add(typeof(Services.NotificationsHandlerService));
                _getAllServiceTypesToRegisterWithIoc_Result.Add(typeof(Services.PayMammothConnectorUrlService));
                _getAllServiceTypesToRegisterWithIoc_Result.Add(typeof(Services.PayMammothConnectorInitialRequestService));
                _getAllServiceTypesToRegisterWithIoc_Result.Add(typeof(Services.HttpRequests.HttpRequestSenderService));
            }

            return _getAllServiceTypesToRegisterWithIoc_Result;
        }
    }
}
