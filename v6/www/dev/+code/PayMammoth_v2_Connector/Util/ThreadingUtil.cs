﻿using System;
using System.Threading.Tasks;
using NLog;

namespace PayMammoth_v6.Connector.Util
{
    public static class ThreadingUtil
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        public static Task CallMethodAsAsyncTask(Action action, bool dontLaunchOnSeperateThread = false)
        {

            Task task = null;
            //methodToCall();
            //return null;
            if (!dontLaunchOnSeperateThread)
            {
                task = Task.Run(action);

            }
            else
            {
                action();
            }
            return task;
        }

    }
}
