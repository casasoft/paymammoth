﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Services.Cultures;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6;
using CS.General_CS_v6.Util;
using General_Tests_CS_v6.TestUtil;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.TestData;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;
using Raven.Client;
using Should;
using PayMammoth_v6.Presentation.Services.Payments;

namespace PayMammoth_v6_TestsIntegration.IntegrationTests.PaymentMethods.Generic
{
    [TestFixture]
    public class PaymentSelectionTests
    {

        
        
        /*
Integration Test - PayPal
----------------

1. generates a test payment, and stores its equivalent TestPaymentData.
2. redirects to PayPal
3. user pays on paypal, and then is redirected to the confirm page on PayMammoth
4. user accepts confirm page, confirmation sent to PayPal
5. PayPal redirects user to success page, and sends IPN message in the background
6. PayMammoth receives IPN, and marks the payment as successful.  
7. PayMammoth sends a notification msg, which is received by PayMammoth itself.
8. Notification is confirmed, and TestData is marked as paid
9. Integration test is waiting until TestData is marked as paid.
         * 
         */

        

        public class TestPaymentsData
        {
            public TestPaymentData TestPayment { get; set; }
            public PaymentRequestData PaymentRequest { get; set; }


        }

        private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestDataService _paymentRequestDataService;
        private readonly IPayPalManager _payPalManager;
        private readonly IPaymentUrlModelService _paymentUrlModelService;
        private readonly ICultureDataPresentationService _cultureModelService;

        public PaymentSelectionTests()
        {
            _ravenDbService = InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestDataService = InversionUtil.Get<IPaymentRequestDataService>();
            _payPalManager = InversionUtil.Get<IPayPalManager>();
            _paymentUrlModelService = InversionUtil.Get<IPaymentUrlModelService>();
            _cultureModelService = InversionUtil.Get<ICultureDataPresentationService>();
        }

    

        private WebsiteAccountData getTestWebsiteAccount(IDocumentSession session)
        {
            var websiteAccountGetByIdParams = new GetByIdParams<WebsiteAccountData>(Constants.TestWebsiteAccountId);
            websiteAccountGetByIdParams.ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No;
            return session.GetById(websiteAccountGetByIdParams);


            //WebsiteAccountData testWebsite = _dataObjectFactory.CreateNewDataObject<WebsiteAccountData>();
            //testWebsite.WebsiteName = "TestWebsiteAccount-" + DateTime.Now.Ticks;
            //testWebsite.PaymentsInformation.PayPal.Enabled = true;
            //testWebsite.PaymentsInformation.PayPal.SandboxAccount.MerchantEmail = "paypal.cs.test.business.en@dispostable.com";
            //testWebsite.PaymentsInformation.PayPal.SandboxAccount.MerchantId = "SAN5FMKHMTM9C";
            //testWebsite.PaymentsInformation.PayPal.SandboxAccount.Password = "1389117990";
            //testWebsite.PaymentsInformation.PayPal.SandboxAccount.Signature = "AxRMHSZhKz2kBtwBMbnqHM0tDC7XA8hhARUerqhHUKWfOaz0b1MN80Zd";
            //testWebsite.PaymentsInformation.PayPal.SandboxAccount.Username = "paypal.cs.test.business.en_api1.dispostable.com";
            //testWebsite.PaymentsInformation.PayPal.UseLiveEnvironment = false;
            //session.Store(testWebsite);
//            return testWebsite;

        }
      



        [Test]
        public void Start()
        {

           var testPayment = IntegrationUtil.CreateTestPaymentInfo();
           

            var initialRequest = IntegrationUtil.CreateTestInitialRequestAndSendToPayMammoth(testPayment.GetRavenDbIdOnlyFromItem());

            CultureModel cultureModel = _cultureModelService.GetCultureModelFromIdentifier("en");

            //var selectionUrl = _paymentUrlModelService.GetPaymentSelectionPage(session, cultureModel, request.Id);
            var selectionUrl = CS.General_CS_v6.Util.PageUtil.GetApplicationBaseUrl() + "en/payment/?identifier=" + HttpUtility.UrlEncode(initialRequest.RequestId);


            visitPage(selectionUrl);


            string testPaymentId = testPayment.Id;

            IntegrationUtil.WaitUntilTestPaymentIsMarkedAsPaid(testPayment.Id);
            System.Threading.Thread.Sleep(300 * 1000);
           
        }

        private void visitPage(string url)
        {

            IWebDriver webDriver = new FirefoxDriver();
            webDriver.Navigate().GoToUrl(url);
        }

    }
}
