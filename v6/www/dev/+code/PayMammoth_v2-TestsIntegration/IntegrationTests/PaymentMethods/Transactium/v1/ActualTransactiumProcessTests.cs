﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Services.Cultures;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Util;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1;
using PayMammoth_v6.Presentation.Services.Payments;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.Services;

namespace PayMammoth_v6_TestsIntegration.IntegrationTests.PaymentMethods.Transactium.v1
{
    [TestFixture]
    public class ActualTransactiumProcessTests
    {
         private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestDataService _paymentRequestDataService;
        private readonly IPayPalManager _payPalManager;
        private readonly IPaymentUrlModelService _paymentUrlModelService;
        private readonly ICultureDataPresentationService _cultureModelService;
        private readonly IPayMammothConnectorUrlService _payMammothConnectorUrlService;

        public ActualTransactiumProcessTests()
        {
            _ravenDbService = InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestDataService = InversionUtil.Get<IPaymentRequestDataService>();
            _payPalManager = InversionUtil.Get<IPayPalManager>();
            _paymentUrlModelService = InversionUtil.Get<IPaymentUrlModelService>();
            _cultureModelService = InversionUtil.Get<ICultureDataPresentationService>();
            _payMammothConnectorUrlService = InversionUtil.Get<IPayMammothConnectorUrlService>();
        }

        [Test]
        public void StartActualTransactiumProcessIntegrationTest()
        {

            var testPayment = IntegrationUtil.CreateTestPaymentInfo();


            var initialRequest = IntegrationUtil.CreateTestInitialRequestAndSendToPayMammoth(testPayment.GetRavenDbIdOnlyFromItem());


            automateTransactium(initialRequest.RequestId);
            

            string testPaymentId = testPayment.Id;

            IntegrationUtil.WaitUntilTestPaymentIsMarkedAsPaid(testPayment.Id);
            System.Threading.Thread.Sleep(60 * 1000);

        }

        private void automateTransactium(string requestId)
        {
            var selectionUrl = _payMammothConnectorUrlService.GetPayMammothUrlForRequest(requestId, EnumsPayMammothConnector.SupportedLanguage.English);


            IWebDriver webDriver = new FirefoxDriver();
            webDriver.Navigate().GoToUrl(selectionUrl);

            

            int k = 5;

        
        }

    }
}
