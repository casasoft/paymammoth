﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using CS.General_CS_v6;
using CS.General_CS_v6.Util;
using General_Tests_CS_v6.TestUtil;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.TestData;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1;
using PayMammoth_v6.PaymentsIntegration.Transactium;
using PayMammoth_v6_TestsIntegration.IntegrationTests.PaymentMethods.PayPal.v1;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;
using Raven.Client;
using Should;

namespace PayMammoth_v6_TestsIntegration.IntegrationTests.PaymentMethods.Transactium.v1
{
    public class TransactiumPaymentTest
    {
        

        

        public class TestData
        {
            public TestPaymentData TestPayment { get; set; }
            public PaymentRequestData PaymentRequest { get; set; }


        }

        private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestDataService _paymentRequestDataService;
        private readonly IGenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService _generateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService;
        private readonly IPayPalManager _payPalManager;

        public TransactiumPaymentTest()
        {
            _ravenDbService = InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestDataService = InversionUtil.Get<IPaymentRequestDataService>();
            _payPalManager = InversionUtil.Get<IPayPalManager>();
            _generateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService = InversionUtil.Get<IGenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService>();
            
        }

    

        private WebsiteAccountData createWebsiteAccount(IDocumentSession session)
        {
            WebsiteAccountData testWebsite = _dataObjectFactory.CreateNewDataObject<WebsiteAccountData>();
            testWebsite.WebsiteName = "TestWebsiteAccount-" + DateTime.Now.Ticks;
            testWebsite.PaymentsInformation.Transactium.Enabled = true;
            var info = testWebsite.PaymentsInformation.Transactium;
            info.UseLiveEnvironment = false;
            info.StagingAccount.Username = "DiscountPages";
            info.StagingAccount.Password = "Te210_ewpMAs";
            info.LiveAccount.Username = "DiscountPages";
            info.LiveAccount.Password = "7fcnt4RDo(7c8f)";
            info.ProfileTag = "DiscountPages";
            
            session.Store(testWebsite);
            return testWebsite;

        }
        private StartExpressCheckoutIntegrationTest.TestPaymentsData createTestPaymentRequest(WebsiteAccountData websiteAccount, IDocumentSession session)
        {
            StartExpressCheckoutIntegrationTest.TestPaymentsData testData = new StartExpressCheckoutIntegrationTest.TestPaymentsData();


            TestPaymentData testingPayment = new TestPaymentData();
            


            InitialRequestInfo initialRequest = new InitialRequestInfo();
            //{
            //    initialRequest.Details.ClientContactDetails.Address1 = "20, Cor Jesu";
            //    initialRequest.Details.ClientContactDetails.Address2 = "Triq il-Kittenija";
            //    initialRequest.Details.ClientContactDetails.ClientReference = "Test-Client";
            //    initialRequest.Details.ClientContactDetails.Country3LetterCode =
            //        CS.General_CS_v6.Enums.CountryIso3166To3LetterCode(Enums.CountryIso3166.Malta);
            //    initialRequest.Details.ClientContactDetails.Email = "karlcassar@gmail.com";
            //}
            initialRequest.Pricing.CurrencyCode3Letter = "EUR";
            {
                var paymentRequestItemLine = new PaymentRequestItemLine();
                initialRequest.ItemDetails.Add(paymentRequestItemLine);
                paymentRequestItemLine.Quantity = 2;
                paymentRequestItemLine.UnitPrice = 1;
                paymentRequestItemLine.Title = "Test Item";
            }
            {
                var paymentRequestItemLine = new PaymentRequestItemLine();
                initialRequest.ItemDetails.Add(paymentRequestItemLine);
                paymentRequestItemLine.Quantity = 1;
                paymentRequestItemLine.UnitPrice = 2;
                paymentRequestItemLine.Title = "Test Item2" ;
            }
            testingPayment.Total = 3;

            testingPayment.Title = string.Format("Test payment generated on [{0}]", DateTime.Now.ToString(("dd/MM/yyyy HH:mm:ss")));
            session.Store(testingPayment);
            session.SaveChanges();


            testData.TestPayment = testingPayment;

            initialRequest.Details.OrderReference = testingPayment.Id;
            initialRequest.NotificationUrl = PayMammoth_v6.Constants.TestPayMammothLocalDomain + PayMammoth_v6.Constants.TestNotificationHandler;
            initialRequest.ReturnUrls.SuccessUrl = PayMammoth_v6.Constants.TestPayMammothLocalDomain + "_tests/success.aspx";
            initialRequest.ReturnUrls.CancelUrl = PayMammoth_v6.Constants.TestPayMammothLocalDomain + "_tests/failure.aspx";
            var newPaymentRequest = _paymentRequestDataService.GeneratePaymentRequestFromInitialRequest(websiteAccount, initialRequest, null);
            newPaymentRequest.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.Success);
            testData.PaymentRequest = newPaymentRequest.GeneratedRequest;
            return testData;

        }


        [Test]
        public void Start()
        {
            var session = _ravenDbService.CreateNewSession();

            var websiteAccount = createWebsiteAccount(session);
            session.SaveChanges();

            var testData = createTestPaymentRequest(websiteAccount, session);
            var request = testData.PaymentRequest;
            session.SaveChanges();

            request.ShouldNotBeNull();
            request.LinkedWebsiteAccountId.ShouldNotBeNull();
            

            
            session.Dispose();
            session = _ravenDbService.CreateNewSession();

            var generateResult = _generateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService
                .GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructions(EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Transactium, 
                request,null);
            

            //Assertions
            
            generateResult.Result.ShouldEqual(PayMammoth_v6Enums.RedirectionResultStatus.Success);
            generateResult.UrlToRedirectTo.ShouldNotBeNullOrWhitespace();
            
            visitPage(generateResult);

            CS.General_CS_v6.Util.ThreadUtil.Sleep(300 * 1000);
           
        }

        private void visitPage(TransactionGeneratorResult result)
        {

            string url = TransactiumUrls.GetPaymentPageUrl(null, result.UrlToRedirectTo);

            IWebDriver webDriver = new FirefoxDriver();
            webDriver.Navigate().GoToUrl(url);
        }

    }
}
