﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6;
using CS.General_CS_v6.Util;
using General_Tests_CS_v6.TestUtil;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.TestData;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1;
using PayMammoth_v6_TestsIntegration.IntegrationTests.PaymentMethods.PayPal.v1;
using PayMammoth_v6.Connector;
using Raven.Client;
using Should;

namespace PayMammoth_v6_TestsIntegration.IntegrationTests.InitialRequests
{
     [TestFixture]
    public class InitialRequestIntegrationTest
    {
       
        

        public class TestData
        {
            public TestPaymentData TestPayment { get; set; }
            public PaymentRequestData PaymentRequest { get; set; }


        }

        private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestDataService _paymentRequestDataService;
        
        public InitialRequestIntegrationTest()
        {
            _ravenDbService = InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestDataService = InversionUtil.Get<IPaymentRequestDataService>();
           
        }

    

        //private WebsiteAccountData createWebsiteAccount(IDocumentSession session)
        //{
        //    WebsiteAccountData testWebsite = _dataObjectFactory.CreateNewDataObject<WebsiteAccountData>();
        //    testWebsite.WebsiteName = "TestWebsiteAccount-" + DateTime.Now.Ticks;
        //    //testWebsite.AccountCode = "TestWebsite";
        //    testWebsite.SecretWord = "123456";

        //    session.Store(testWebsite);
        //    return testWebsite;

        //}


        

         [Test]
        public void Start()
        {
          //  PayMammoth_v6.Connector.Constants.TestSendRequest();


            var result = IntegrationUtil.CreateTestInitialRequestAndSendToPayMammoth();

            result.ShouldNotBeNull();
            result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.Success);
            result.RequestId.ShouldNotBeEmpty();
            

           
        }


    }
}
