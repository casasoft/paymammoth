﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Util;
using NUnit.Framework;
using PayMammoth_v6.Modules.Data.TestData;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v6_TestsIntegration.IntegrationTests.PaymentProcess
{
    /// <summary>
    /// This test tests out the full interaction with PayMammoth.  This needs to have PayMammoth listening locally.
    /// This will first create an initial request via the test website account.  Then, you need to MANUALLY mark it as paid on PayMammoth.
    /// It will wait indefinitely until transaction is marked as paid
    /// </summary>
    [TestFixture]
    public class FullClientPaymammothInteractionIncludingNotificationsTests
    {

         private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestTransactionDataService _paymentRequestTransactionDataService;

        public FullClientPaymammothInteractionIncludingNotificationsTests()
        {
            _ravenDbService = InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestTransactionDataService = InversionUtil.Get<IPaymentRequestTransactionDataService>();
        }
        

        [Test]
        public void StartTest()
        {
            var testPayment = IntegrationUtil.CreateTestPaymentInfo();
           
            var initialRequest = IntegrationUtil.CreateTestInitialRequestAndSendToPayMammoth(testPayment.GetRavenDbIdOnlyFromItem());

            string requestId = initialRequest.RequestId;
            System.Diagnostics.Trace.WriteLine("RequestID: " + requestId);

            IntegrationUtil.WaitUntilTestPaymentIsMarkedAsPaid(testPayment.Id);

        }
    }
}
