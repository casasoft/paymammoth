﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Modules.Data.TestData;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;
using PayMammoth_v6.Connector.Services;

namespace PayMammoth_v6_TestsIntegration.IntegrationTests
{
    public static class IntegrationUtil
    {
        private static readonly IRavenDbService _ravenDbService;
        private static ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;

        static IntegrationUtil()
        {
            _ravenDbService = InversionUtil.Get<IRavenDbService>();
            _currentDateTimeRetrieverService = InversionUtil.Get<ICurrentDateTimeRetrieverService>();
        }

        /// <summary>
        /// This creates a test payment, which is a record of this test payment.  This is used to track if the test payment was paid, etc
        /// 
        /// </summary>
        /// <returns></returns>
        public static TestPaymentData CreateTestPaymentInfo()
        {
            var session = _ravenDbService.CreateNewSession();

            TestPaymentData testPayment = new TestPaymentData();
            testPayment.DateCreated = _currentDateTimeRetrieverService.GetCurrentDateTime();
            testPayment.Title = "Test payment generated on [" + testPayment.DateCreated + "]" ;
            session.Store(testPayment);
            session.SaveChanges();
            session.Dispose();
            return testPayment;
        }
        /// <summary>
        /// This will create an initial request to PayMammoth, linked with the TestAccount defined in Constants.TestWebsiteAccountId and SecretWord.
        /// </summary>
        /// <param name="orderReference"></param>
        /// <returns></returns>
        public static InitialRequestResponseMessage CreateTestInitialRequestAndSendToPayMammoth(string orderReference = null)
        {
            if (orderReference == null)
            {
                orderReference = "Karl-Test1";
            }
            InitialRequestInfo initialRequest = new InitialRequestInfo();
            initialRequest.Details.ClientContactDetails.Address1 = "Triq il-Kittenija";
            initialRequest.Details.ClientContactDetails.Country3LetterCode = "MLT";
            initialRequest.Details.ClientContactDetails.Email = "karl@casasoft.com.mt";
            initialRequest.Details.ClientContactDetails.Name = "Karl";
            initialRequest.Details.ClientContactDetails.LastName = "Cassar";
            initialRequest.Details.ClientReference = "Karl-Test1";
            initialRequest.Details.Description = "Test item 1";
            initialRequest.Details.OrderReference = orderReference;
            initialRequest.Details.Title = "Test Payment";
            decimal unitPrice = (decimal) CS.General_CS_v6.Util.RandomUtil.GetInt(10, 300) / 100m;
            initialRequest.ItemDetails.Add(new PaymentRequestItemLine()
            {
                Description = "Line 1",
                Quantity = 1,
                TaxAmountPerUnit = 0.18m,
                Title = "Item 1",
                UnitPrice = unitPrice
            });
            initialRequest.Language = EnumsPayMammothConnector.SupportedLanguage.English; ;
            initialRequest.NotificationUrl = PayMammoth_v6.Constants.TestPayMammothLocalDomain + PayMammoth_v6.Constants.TestNotificationHandler;
            initialRequest.Pricing.CurrencyCode3Letter = "EUR";
            initialRequest.Pricing.HandlingAmount = 0.5m;
            initialRequest.Pricing.ShippingAmount = 0.75m;
            initialRequest.ReturnUrls.CancelUrl = PayMammoth_v6.Connector.Constants.PayMammoth_ServerUrl + PayMammoth_v6.Constants.TestPaymentsConstants.TestPaymentCancelUrl;
            initialRequest.ReturnUrls.SuccessUrl = string.Format("{0}{1}?{2}={3}",
                CS.General_CS_v6.Util.FileUtil.EnsurePathEndsWithSlash(PayMammoth_v6.Connector.Constants.PayMammoth_ServerUrl),
             CS.General_CS_v6.Util.FileUtil.EnsurePathDoesNotStartWithSlash(PayMammoth_v6.Constants.TestPaymentsConstants.TestPaymentSuccessUrl),
             PayMammoth_v6.Constants.TestPaymentsConstants.QuerystringVar_TestPaymentId,
             orderReference);
            
            initialRequest.OrderLinkOnClientWebsite = initialRequest.ReturnUrls.SuccessUrl;
            
            //PayMammoth_v6.Connector.Constants.PayMammoth_InitialRequestUrl = Constants.PaymentClientBaseUrl + Constants.InitialRequestHandler;
            //PayMammoth_v6.Connector.Constants.PayMammoth_InitialRequestUrl = "http://localhost/" + Constants.InitialRequestHandler;
            //PayMammoth_v6.Connector.Constants.PayMammoth_InitialRequestUrl = "http://localhost/_tests/initialrequest.ashx";
            //PayMammoth_v6.Connector.Constants.TestSendRequest();
            var result = InversionUtil.Get<IPayMammothConnectorInitialRequestService>().CreatePaymentRequestOnPayMammoth(
                RavenDbUtil.GetIdOnlyFromFullId(Constants.TestWebsiteAccountId),
                Constants.TestWebsiteAccount_SecretWord,
                initialRequest);

            return result;


        }

        public static void WaitUntilTestPaymentIsMarkedAsPaid(string testPaymentId)
        {
            
            bool isPaid = false;
            do
            {
                var session = _ravenDbService.CreateNewSession();
                var checkTestPayment = session.GetById<TestPaymentData>(new GetByIdParams<TestPaymentData>(testPaymentId)
                {
                    ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No
                });

                isPaid = (checkTestPayment.Status == TestPaymentData.TestPaymentStatus.Paid);


                session.Dispose();
                if (!isPaid)
                {
                    System.Threading.Thread.Sleep(30 * 1000);
                }
            } while (!isPaid);

            int k = 5;
            //System.Threading.Thread.Sleep(30 * 1000); //one last time

        }
    }
}
