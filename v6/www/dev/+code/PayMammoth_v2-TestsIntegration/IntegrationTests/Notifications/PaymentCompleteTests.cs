﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5;
using NUnit.Framework;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.TestData;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2_TestsIntegration.IntegrationTests.PaymentMethods.PayPal.v1;
using Raven.Client;
using Should;

namespace PayMammoth_v2_TestsIntegration.IntegrationTests.Notifications
{
    [TestFixture]
    public class PaymentCompleteTests
    {
        private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestTransactionsService _paymentRequestTransactionsService;

        public PaymentCompleteTests()
        {
            _ravenDbService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = BusinessLogic_CS_v5.Util.InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestTransactionsService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPaymentRequestTransactionsService>();
        }


        private WebsiteAccountData createWebsiteAccount(IDocumentSession session)
        {
            WebsiteAccountData testWebsite = _dataObjectFactory.CreateNewDataObject<WebsiteAccountData>();
            testWebsite.WebsiteName = "TestWebsiteAccount-PaymentCompleteTests-" + DateTime.Now.Ticks;
            testWebsite.Notifications.ClientWebsiteResponseUrl = "http://office.casasoft.com.mt/test-notify.ashx";
            session.Store(testWebsite);
            return testWebsite;

        }
        private StartExpressCheckoutIntegrationTest.TestPaymentsData createTestPaymentRequest(WebsiteAccountData websiteAccount, IDocumentSession session)
        {
            StartExpressCheckoutIntegrationTest.TestPaymentsData testPaymentsData = new StartExpressCheckoutIntegrationTest.TestPaymentsData();

            TestPaymentData testPayment = new TestPaymentData();
            testPayment.Title = "Test payment in PaymentCompleteTests";
            session.Store(testPayment);
            
            testPaymentsData.TestPayment = testPayment;

            PaymentRequestData requestData = _dataObjectFactory.CreateNewDataObject<PaymentRequestData>();
            requestData.RequestDetails = new InitialRequestInfo();
            requestData.RequestDetails.Details.OrderReference = testPayment.GetRavenDbIdOnlyFromItem();
            requestData.RequestDetails.NotificationUrl = PayMammoth_v2.Constants.TestPayMammothLocalDomain + Constants.TestNotificationHandler;
            requestData.WebsiteAccountId = websiteAccount;
            testPaymentsData.PaymentRequest = requestData;
            session.Store(requestData);
            return testPaymentsData;;

        }
        private PaymentRequestTransactionData createTestPaymentRequestTransaction(PaymentRequestData request, IDocumentSession session)
        {
            PaymentRequestTransactionData transaction = _dataObjectFactory.CreateNewDataObject<PaymentRequestTransactionData>();
            transaction.PaymentRequestId = request;
            session.Store(transaction);
            return transaction;

        }
        [Test]
        public void StartTest()
        {
            var session = _ravenDbService.CreateNewSession();
            var websiteAccount =createWebsiteAccount(session);
            var testData = createTestPaymentRequest(websiteAccount, session);
            var transaction = createTestPaymentRequestTransaction(testData.PaymentRequest, session);
            session.SaveChanges();
            session.Dispose();
            //-------/


            {
                session = _ravenDbService.CreateNewSession();
                transaction =
                   session.GetById<PaymentRequestTransactionData>(
                       new GetByIdParams<PaymentRequestTransactionData>(transaction.Id)
                       {
                           ThrowErrorIfNotAlreadyLoadedInSession = false
                       });
                _paymentRequestTransactionsService.MarkTransactionAsPaid(transaction, false, "TEST-AUTH", session);
                session.SaveChanges();
                session.Dispose();
            }
            {
                session = _ravenDbService.CreateNewSession();
                transaction =
                    session.GetById<PaymentRequestTransactionData>(
                        new GetByIdParams<PaymentRequestTransactionData>(transaction.Id)
                        {
                            ThrowErrorIfNotAlreadyLoadedInSession = false
                        });
                transaction.IsSuccessful.ShouldBeTrue();
                session.Dispose();
            }
            waitUntilTestPaymentIsMarkedAsPaid(testData);




        }

        private void waitUntilTestPaymentIsMarkedAsPaid(StartExpressCheckoutIntegrationTest.TestPaymentsData testData)
        {
            IDocumentSession session;
            string testPaymentId = testData.TestPayment.Id;
            bool isPaid = false;
            do
            {
                session = _ravenDbService.CreateNewSession();
                var checkTestPayment = session.GetById<TestPaymentData>(new GetByIdParams<TestPaymentData>(testPaymentId)
                {
                    ThrowErrorIfNotAlreadyLoadedInSession = false
                });

                isPaid = checkTestPayment.MarkedAsPaid;


                session.Dispose();
                if (!isPaid)
                {
                    System.Threading.Thread.Sleep(30*1000);
                }
            } while (!isPaid);

            int k = 5;
            System.Threading.Thread.Sleep(30 * 1000); //one last time

        }
    }
}
