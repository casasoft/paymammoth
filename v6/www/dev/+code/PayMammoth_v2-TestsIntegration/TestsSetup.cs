﻿using CS.General_CS_v6.Util;
using NUnit.Framework;
using PayMammoth_v6.Framework.Application;

namespace PayMammoth_v6_TestsIntegration
{
    [SetUpFixture]
    public class TestsSetup
    {

        public class AppInstancePaymammothv2TestsIntegration : AppInstancePayMammoth_v6
        {
            //protected override void initialCustomComponentsWithInversionOfControl()
            //{
            //    base.initialCustomComponentsWithInversionOfControl();
            //}
        }

        [SetUp]
        public void Setup()
        {
            OtherUtil.SetIsInIntegrationTestingEnvironment();
            //CS.General_CS_v6.Util.OtherUtil.SetIsInUnitTestingEnvironment();

            var app = new AppInstancePaymammothv2TestsIntegration();
            //app.AddProjectAssembly(typeof(General_Tests_CS_v6.TestsSetup).Assembly);
            //app.AddProjectAssembly(typeof(TestsSetup).Assembly);
            app.OnApplicationStart();


            PayMammoth_v6.Connector.Constants.PayMammoth_ServerUrl = PayMammoth_v6.Constants.TestPayMammothLocalDomain;
            //PayMammoth_v6.Connector.Constants.PayMammoth_ServerUrl = PayMammoth_v6.Constants.TestPayMammothAzureDomain;

        }

        [TearDown]
        public void Teardown()
        {

        }
    }
}
