﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6_TestsIntegration
{
    public static class Constants
    {
        //public static string PaymentClientBaseUrl = "http://office.casasoft.com.mt/";
        public static string TestWebsiteAccountId = "WebsiteAccountData/Test";
        public static string TestWebsiteAccount_SecretWord = "TestSecret";

        //public static string Transactium_TestPaymentPage = "/_tests/transactium/transactiumpayment.aspx";
        public static string Transactium_StatusPage = "/_tests/transactium/status.aspx";

        public static string InitialRequestHandler = "_tests/initialrequest.ashx";
        //public static string ConfirmMsgHandler = "_tests/confirmmsg.ashx";


    }
}
