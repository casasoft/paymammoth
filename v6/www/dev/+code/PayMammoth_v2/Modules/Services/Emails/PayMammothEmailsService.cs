﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.EnumsBL;

using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Payments;

namespace PayMammoth_v6.Modules.Services.Emails
{
    public interface IPayMammothEmailsService
    {
        void SendEmailsAboutSuccessfulPayment(ReferenceLink<PaymentRequestTransactionData> transactionId); 
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PayMammothEmailsService : IPayMammothEmailsService
    {
        public PayMammothEmailsService()
        {

        }

        [LogAspect(GenericEnums.NlogLogLevel.Trace)]
        public void SendEmailsAboutSuccessfulPayment(ReferenceLink<PaymentRequestTransactionData> transactionId)
        {
            //todo: [For: Karl | 2014/01/02] implement PayMammothEmailsService.SendEmailsAboutSuccessfulPayment (WrittenBy: Karl)
            //throw new NotImplementedException();
        }
    }
}
