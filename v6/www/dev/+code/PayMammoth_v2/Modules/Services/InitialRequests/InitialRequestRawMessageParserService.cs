﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Modules.Services.Settings;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Modules.Pages;
using PayMammoth_v6.Connector.Services;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;
using Raven.Client;
using Raven.Client.Linq;

namespace PayMammoth_v6.Modules.Services.InitialRequests
{
    using BusinessLogic_CS_v6.Aspects;
    using BusinessLogic_CS_v6.Aspects.NullAspects;
    using CS.General_CS_v6.Modules.InversionOfControl;

    public interface IInitialRequestRawMessageParserService
    {
        /// <summary>
        /// This parses the raw contents from the client website
        /// </summary>
        /// <param name="formContents"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        InitialRequestResponseMessage ParseRawMessage(string formContents, IDocumentSession session = null);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class InitialRequestRawMessageParserService : IInitialRequestRawMessageParserService
    {
        private readonly IInitialRequestService _initialRequestService;
        private readonly IPaymentRequestDataService _paymentRequestDataService;
        private readonly IPayMammothConnectorUrlService _payMammothConnectorUrlService;
        
        private readonly IPageService _pageService;

        public InitialRequestRawMessageParserService(IInitialRequestService initialRequestService,
            IPaymentRequestDataService paymentRequestDataService,
            IPayMammothConnectorUrlService payMammothConnectorUrlService,
            IPageService pageService)
        {
            _pageService = pageService;
            _payMammothConnectorUrlService = payMammothConnectorUrlService;
            _paymentRequestDataService = paymentRequestDataService;
            _initialRequestService = initialRequestService;
        }

        [RavenTaskAspect]
        public InitialRequestResponseMessage ParseRawMessage(string formContents, IDocumentSession session = null)
        {
            //System.Web.HttpContext.Current.Request.
            InitialRequestResponseMessage result = new InitialRequestResponseMessage();
            result.Status = EnumsPayMammothConnector.InitialRequestResponseStatus.InvalidData;
            var rawResult=  _initialRequestService.ParseRawStringToInitialRequest(formContents);
            if (rawResult != null)
            {
                var decryptedResult = _initialRequestService.DecryptInitialRequest(session, rawResult);
                if (decryptedResult.Status == EnumsPayMammothConnector.InitialRequestResponseStatus.Success)
                {
                    var generatedResult = _paymentRequestDataService.GeneratePaymentRequestFromInitialRequest(decryptedResult.WebsiteAccountId,
                        decryptedResult.InitialRequest);
                    result.Status = generatedResult.Status;
                    if (generatedResult.Status == EnumsPayMammothConnector.InitialRequestResponseStatus.Success)
                    {
                        result.RequestId = generatedResult.GeneratedRequest.GetRavenDbIdOnlyFromItem();

                        string baseUrl = _pageService.GetApplicationBaseUrl();
                        string twoLetterLanguageCode = EnumsPayMammothConnector.ConvertSupportedLanguageToTwoLetterCode(decryptedResult.InitialRequest.Language);

                        result.PayMammothUrlToRedirectTo =
                            string.Format(PayMammothConnectorUrlService.PayMammothUrlTemplate,
                            baseUrl,
                            twoLetterLanguageCode,
                            result.RequestId);
                    }

                }
                else
                {
                    result.Status = decryptedResult.Status;
                }
            }
            return result;
            
        }
    }
}
