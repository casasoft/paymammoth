﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;


namespace PayMammoth_v6.Modules.Services.InitialRequests.Helpers
{
    public class InitialRequestDecryptResult
    {
        public ReferenceLink<WebsiteAccountData> WebsiteAccountId { get; set; }
        public InitialRequestInfo InitialRequest { get; set; }
        public EnumsPayMammothConnector.InitialRequestResponseStatus Status { get; set; }
    }
}
