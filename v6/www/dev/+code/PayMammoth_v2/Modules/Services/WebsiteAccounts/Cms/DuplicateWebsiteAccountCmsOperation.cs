﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.Authorization;
using BusinessLogic_CS_v6.Framework.Cms;
using BusinessLogic_CS_v6.Framework.Cms.CustomOperations;
using BusinessLogic_CS_v6.Modules.Data.Members;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;
using Raven.Abstractions.Data;

namespace PayMammoth_v6.Modules.Services.WebsiteAccounts.Cms
{
    [AccessRequiredDefaults(GenericEnums.AccessTypes.General)]
    [CmsOperationDefaults]
    public class DuplicateWebsiteAccountCmsOperation : CmsSpecificOperation<WebsiteAccountData>
    {
       
     

        protected override CmsOperationResult ExecuteOperation(WebsiteAccountData item, List<CmsFieldDataValue> fieldInputValues)
        {
            var result = new CmsOperationResult();

            WebsiteAccountData duplicated = item;
            duplicated.Id = RavenDbUtil.GetDocumentIdForType<WebsiteAccountData>(RandomUtil.GetGuid(removeDashes: true));

            var ravenService = InversionUtil.Get<IRavenDbService>();
            var session = ravenService.CreateNewSession();
            session.Store(duplicated);
            session.SaveChanges();
            session.Dispose();
            

            result.NotificationMessage = "Website account cloned successfully - Id: " + duplicated.Id;
            result.ResultType = NotificationMessageType.Success;
            return result;

        }

       
    }
    
}
