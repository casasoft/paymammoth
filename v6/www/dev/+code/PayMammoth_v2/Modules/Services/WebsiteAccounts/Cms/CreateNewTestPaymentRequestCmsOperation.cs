﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.Authorization;
using BusinessLogic_CS_v6.Framework.Cms;
using BusinessLogic_CS_v6.Framework.Cms.CustomOperations;
using BusinessLogic_CS_v6.Modules.Data.Members;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;
using Raven.Abstractions.Data;

namespace PayMammoth_v6.Modules.Services.WebsiteAccounts.Cms
{
    [AccessRequiredDefaults(GenericEnums.AccessTypes.General)]
    [CmsOperationDefaults]
    public class CreateNewTestPaymentRequestCmsOperation : CmsSpecificOperation<WebsiteAccountData>
    {
        const string Identifier_Total = "Total";
        
        public override CmsOperationFormData GetFormDataToShowWithOperation()
        {
            var form = new CmsOperationFormData();
            form.FormTitle = "Create new test item";
            {
                var fieldTotal = new CmsOperationFormFieldData();
                fieldTotal.FieldType = typeof(double);
                fieldTotal.Title = Identifier_Total;
                

                form.Fields.Add(fieldTotal);
                
            }
            return form;
        }

        protected override CmsOperationResult ExecuteOperation(WebsiteAccountData item, List<CmsFieldDataValue> fieldInputValues)
        {
            var fieldTotal = fieldInputValues.FirstOrDefault(x => x.FieldData.Identifier == Identifier_Total);
            double total = (double)fieldTotal.FormValue;
            

            //----
            var ravenService = InversionUtil.Get<IRavenDbService>();
            var dataFactory = InversionUtil.Get<IDataObjectFactory>();

            var result = new CmsOperationResult();
            var session = ravenService.CreateNewSession();
            PaymentRequestData paymentRequestData = dataFactory.CreateNewDataObject<PaymentRequestData>();
            paymentRequestData.RequestDetails.Details.ClientContactDetails.Address1 = "20, Rainbow Street";
            paymentRequestData.RequestDetails.Details.ClientContactDetails.Address2 = "Triq in-Natura";
            paymentRequestData.RequestDetails.Details.ClientContactDetails.Country3LetterCode = "MLT";
            paymentRequestData.RequestDetails.Details.ClientContactDetails.Email = "test.paymammoth@dispostable.com";
            paymentRequestData.RequestDetails.Details.ClientContactDetails.IpAddress = "196.154.20.30";
            paymentRequestData.RequestDetails.Details.ClientContactDetails.LastName = "Cassar";
            paymentRequestData.RequestDetails.Details.ClientContactDetails.Locality= "Hamrun";
            paymentRequestData.RequestDetails.Details.ClientContactDetails.Mobile = "79123123";
            paymentRequestData.RequestDetails.Details.ClientContactDetails.Name= "Joseph";
            paymentRequestData.RequestDetails.Details.ClientContactDetails.PostCode = "HMR1607";
            paymentRequestData.RequestDetails.Details.ClientContactDetails.State= "N/A";
            paymentRequestData.RequestDetails.Details.ClientContactDetails.Telephone= "21665588";
            paymentRequestData.RequestDetails.Details.ClientReference = "TestClientReference";
            paymentRequestData.RequestDetails.Details.Description = "";
            paymentRequestData.RequestDetails.Details.OrderReference = "OrderRef-" + DateTime.Now.Ticks;
            paymentRequestData.RequestDetails.Details.Title= "Test Payment";
            {
                paymentRequestData.RequestDetails.ItemDetails = new List<PaymentRequestItemLine>();
                PaymentRequestItemLine line1 = new PaymentRequestItemLine();
                paymentRequestData.RequestDetails.ItemDetails.Add(line1);
                line1.Quantity = 1;
                line1.Title = "Test Item #1";
                line1.UnitPrice = (decimal)total / (decimal)1.18;
                line1.TaxAmountPerUnit = (decimal)total - line1.UnitPrice;
                
            }
            paymentRequestData.RequestDetails.Language = EnumsPayMammothConnector.SupportedLanguage.English;
            paymentRequestData.RequestDetails.OrderLinkOnClientWebsite = "http://office.casasoft.com.mt/order-link";
            paymentRequestData.RequestDetails.Pricing.CurrencyCode3Letter = "EUR";
            paymentRequestData.RequestDetails.Pricing.HandlingAmount = 0;
            paymentRequestData.RequestDetails.Pricing.ShippingAmount = 0;
            paymentRequestData.RequestDetails.RecurringProfile.Required = false;
            paymentRequestData.RequestDetails.ReturnUrls.CancelUrl = "http://office.casasoft.com.mt/failure";
            paymentRequestData.RequestDetails.ReturnUrls.SuccessUrl = "http://office.casasoft.com.mt/success";
            paymentRequestData.RequestDetails.NotificationUrl = CS.General_CS_v6.Util.PageUtil.GetApplicationBaseUrl() + PayMammoth_v6.Constants.TestNotificationHandler;
            paymentRequestData.LinkedWebsiteAccountId = item;

            paymentRequestData.PaymentStatus.Status = PaymentStatusInfo.PaymentStatus.Pending;
            paymentRequestData.RequestDateTime = DateTime.Now;
            paymentRequestData.RequestServerIp = CS.General_CS_v6.Util.PageUtil.GetUserIP();

            session.Store(paymentRequestData);
            session.SaveChanges();
            session.Dispose();

            result.NotificationMessage = "Payment Request created successfully - Id: " + paymentRequestData.GetRavenDbIdOnlyFromItem();
            result.ResultType = NotificationMessageType.Success;
            return result;

        }

       
    }
    
}
