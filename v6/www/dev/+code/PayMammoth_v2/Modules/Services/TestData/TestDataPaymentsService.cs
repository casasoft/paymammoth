﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.TestData;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.Notifications;

namespace PayMammoth_v6.Modules.Services.TestData
{
    public interface ITestDataPaymentsService
    {
        void OnTestPaymentPaidSuccessfully(NotificationMessage msg);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class TestDataPaymentsService : ITestDataPaymentsService
    {
        private readonly IRavenDbService _ravenDbService;
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;

        public TestDataPaymentsService(IRavenDbService ravenDbService, ICurrentDateTimeRetrieverService currentDateTimeRetrieverService)
        {
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
            _ravenDbService = ravenDbService;
        }

        public void OnTestPaymentPaidSuccessfully(NotificationMessage msg)
        {
            if (msg.MessageType == EnumsPayMammothConnector.NotificationMessageType.ImmediatePaymentSuccess)
            {
                var session =  _ravenDbService.CreateNewSession();
                string testPaymentId = RavenDbUtil.GetDocumentIdForType<TestPaymentData>(msg.Identifier);
                var testPayment = session.GetById(new GetByIdParams<TestPaymentData>(testPaymentId) { ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No });
                testPayment.Status = TestPaymentData.TestPaymentStatus.Paid;
                testPayment.MarkedAsPaidOn = _currentDateTimeRetrieverService.GetCurrentDateTime();
                session.Store(testPayment);
                session.SaveChanges();

            }

            
        }
    }
}
