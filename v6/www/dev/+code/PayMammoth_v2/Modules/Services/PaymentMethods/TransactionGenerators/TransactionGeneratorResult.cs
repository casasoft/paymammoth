﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v6.Modules.Data.Payments;

namespace PayMammoth_v6.Modules.Services.PaymentMethods.TransactionGenerators
{
    public class TransactionGeneratorResult
    {
        

        public PayMammoth_v6.Enums.PayMammoth_v6Enums.RedirectionResultStatus Result { get; set; }
        public PaymentRequestTransactionData GeneratedTransaction { get; set; }
        public string UrlToRedirectTo { get; set; }
        public CS.General_CS_v6.Enums.HrefTarget RedirectionType { get; set; }
        

    }
}
