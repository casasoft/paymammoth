﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Modules.Services.PaymentMethods
{
    public interface IPaymentMethodsService
    {
        IPaymentMethodTransactionGeneratorService GetTransactionGeneratorServiceForPaymentMethod(EnumsPayMammothConnector.PaymentMethodType paymentMethod);
        List<EnumsPayMammothConnector.PaymentMethodType> GetAllPaymentMethodTypesAvailableToWebsiteAccount(WebsiteAccountData websiteAccount);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentMethodsService : IPaymentMethodsService
    {
        private readonly IPayPalTransactionGeneratorService _payPalTransactionGeneratorService;
        private readonly ITransactiumTransactionGeneratorService _transactiumTransactionGeneratorService;

        public PaymentMethodsService(IPayPalTransactionGeneratorService payPalTransactionGeneratorService,
            ITransactiumTransactionGeneratorService transactiumTransactionGeneratorService)
        {
            _transactiumTransactionGeneratorService = transactiumTransactionGeneratorService;
            _payPalTransactionGeneratorService = payPalTransactionGeneratorService;
        }

        public IPaymentMethodTransactionGeneratorService GetTransactionGeneratorServiceForPaymentMethod(EnumsPayMammothConnector.PaymentMethodType paymentMethod)
        {
            //todo: [For: Karl | 2013/12/31] GetTransactionGeneratorServiceForPaymentMethod - Implement this once they have been properly implemented (WrittenBy: Karl)        			
            switch (paymentMethod)
            {
                case EnumsPayMammothConnector.PaymentMethodType.PayPal_ExpressCheckout:
                    return _payPalTransactionGeneratorService;
                case  EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Transactium:
                    return _transactiumTransactionGeneratorService;
            }
            throw new InvalidOperationException(string.Format("TransactionGeneratorService not implemented for payment method '{0}'", paymentMethod));
            
        }


        public List<EnumsPayMammothConnector.PaymentMethodType> GetAllPaymentMethodTypesAvailableToWebsiteAccount(WebsiteAccountData websiteAccount)
        {
            List<EnumsPayMammothConnector.PaymentMethodType> result = new List<EnumsPayMammothConnector.PaymentMethodType>();
            var paymentsInfo = websiteAccount.PaymentsInformation;
            if (paymentsInfo.Apco.Enabled)
            {
                result.Add(EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Apco); 
            }

            if (paymentsInfo.Cheque.Enabled)
            {
                result.Add(EnumsPayMammothConnector.PaymentMethodType.Cheque);
            }

            if (paymentsInfo.PayPal.Enabled)
            {
                result.Add(EnumsPayMammothConnector.PaymentMethodType.PayPal_ExpressCheckout);
            }

            if (paymentsInfo.Realex.Enabled)
            {
                result.Add(EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Realex);
            }

            if (paymentsInfo.Transactium.Enabled)
            {
                result.Add(EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Transactium);
            }
            return result;
        }
    }
}
