﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Notifications;
using PayMammoth_v6.Connector;
using Raven.Client;

namespace PayMammoth_v6.Modules.Services.Notifications
{
    public interface INotificationsRetryService
    {
        void IncrementRetryCountAndRepushOnAzureQueueOrMarkAsFailedIfMaximumReached(NotificationMessageData notification); 
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class NotificationsRetryService : INotificationsRetryService
    {
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;
        private readonly INotificationsAzureService _notificationsAzureService;

        public NotificationsRetryService(ICurrentDateTimeRetrieverService currentDateTimeRetrieverService,
            INotificationsAzureService notificationsAzureService)
        {
            _notificationsAzureService = notificationsAzureService;
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
        }

        public void IncrementRetryCountAndRepushOnAzureQueueOrMarkAsFailedIfMaximumReached(NotificationMessageData notification)
        {

            if (notification.Status != EnumsPayMammothConnector.NotificationMessageStatus.Pending)
            {
                throw new InvalidOperationException(
                    "Notification retry count can only be incremented if it is pending. Id: " + notification.Id);
            }
            //---------
            if (notification.RetryCount < -1)
            {
                notification.RetryCount = -1;
            }
            notification.RetryCount++;



            var timeIntervalList = PayMammoth_v6.Constants.GetPaymentNotificationsRetryCounts();
            if (notification.RetryCount >= timeIntervalList.Count)
            {
                notification.Status = EnumsPayMammothConnector.NotificationMessageStatus.Failed;
            }
            else
            {
                var intervalInSeconds = timeIntervalList[notification.RetryCount];
                var currentDate = _currentDateTimeRetrieverService.GetCurrentDateTime();
                notification.NextRetryOn = currentDate.AddSeconds(intervalInSeconds);
                _notificationsAzureService.PushNotificationOnAzureQueue(notification, notification.NextRetryOn);

            }

        }
    }
}
