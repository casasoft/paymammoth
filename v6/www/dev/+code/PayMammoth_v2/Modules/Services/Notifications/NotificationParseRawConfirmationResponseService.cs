﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Notifications;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.Notifications;

namespace PayMammoth_v6.Modules.Services.Notifications
{
    public interface INotificationParseRawConfirmationResponseService
    {
        EnumsPayMammothConnector.NotificationConfirmationStatus ParseRawConfirmationResponse(string formData);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class NotificationParseRawConfirmationResponseService : INotificationParseRawConfirmationResponseService
    {
        private readonly INotificationsConfirmationService _notificationsConfirmationService;

        public NotificationParseRawConfirmationResponseService(INotificationsConfirmationService notificationsConfirmationService)
        {
            _notificationsConfirmationService = notificationsConfirmationService;
        }

        public EnumsPayMammothConnector.NotificationConfirmationStatus ParseRawConfirmationResponse(string formData)
        {
            EnumsPayMammothConnector.NotificationConfirmationStatus result = 
                EnumsPayMammothConnector.NotificationConfirmationStatus.InvalidFormData;
            if (!string.IsNullOrWhiteSpace(formData))
            {
                NotificationConfirmMessage msg = CS.General_CS_v6.Util.JsonUtil.Deserialise<NotificationConfirmMessage>(formData, throwErrorIfCannotDeserialize:false);
                if (msg != null && !string.IsNullOrWhiteSpace(msg.NotificationId))
                {
                    result = EnumsPayMammothConnector.NotificationConfirmationStatus.NotificationCouldNotBeConfirmed;
                    var confirmResult = _notificationsConfirmationService.ConfirmNotification(
                                                    new ReferenceLink<NotificationMessageData>(msg.NotificationId),
                                                     msg.MessageType);
                    if (confirmResult)
                    {
                        result = EnumsPayMammothConnector.NotificationConfirmationStatus.OK;
                    }
                   
                }

            }
            return result;
        }
     }
}
