﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services.Settings;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Notifications;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Repositories.Notifications;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.Connector;
using Raven.Abstractions.Data;
using Raven.Client;

namespace PayMammoth_v6.Modules.Services.Notifications
{
    public interface INotificationMessageDataService
    {
        NotificationMessageData CreateNotificationMessageDataForPaymentRequestDataWithNotificationMessageType(
            ReferenceLink<PaymentRequestData> paymentRequestId,
            EnumsPayMammothConnector.NotificationMessageType notificationMessageType,
            IDocumentSession documentSession = null);
        void CheckAndSendPendingNotificationMessageDatas(
            IDocumentSession documentSession);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class NotificationMessageDataService : INotificationMessageDataService
    {
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestNotificationsUtilService _paymentRequestNotificationsUtilService;
        private readonly ISettingsService _settingsService;
        private readonly INotificationMessageDataRepository _notificationMessageDataRepository;
        private readonly INotificationMessageDataSendingService _notificationMessageDataSendingService;
        private readonly IRavenDbService _ravenDbService;

        public NotificationMessageDataService(
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService,
            IDataObjectFactory dataObjectFactory,
            IPaymentRequestNotificationsUtilService paymentRequestNotificationsUtilService,
            ISettingsService settingsService,
            INotificationMessageDataRepository notificationMessageDataRepository,
            INotificationMessageDataSendingService notificationMessageDataSendingService,
            IRavenDbService ravenDbService)
        {
            _ravenDbService = ravenDbService;
            _notificationMessageDataSendingService = notificationMessageDataSendingService;
            _notificationMessageDataRepository = notificationMessageDataRepository;
            _settingsService = settingsService;
            _paymentRequestNotificationsUtilService = paymentRequestNotificationsUtilService;
            _dataObjectFactory = dataObjectFactory;
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
        }

        [RavenTaskAspect]
        public NotificationMessageData CreateNotificationMessageDataForPaymentRequestDataWithNotificationMessageType(
            ReferenceLink<PaymentRequestData> paymentRequestId,
            EnumsPayMammothConnector.NotificationMessageType notificationMessageType,
            IDocumentSession documentSession = null)
        {
            var paymentRequestData = _ravenDbService.GetById(
                documentSession,
                paymentRequestId,
                throwErrorIfNotAlreadyLoadedInSession: ThrowErrorIfNotAlreadyLoadedInSession.No, 
                loadOnlyAvailableToFrontend: LoadOnlyAvailableToFrontend.Yes);
            if (paymentRequestData == null)
                throw new InvalidOperationException(
                    String.Format(
                        "Can't create NotificationMessageData for a PaymentRequestData [{0}] that does not exist!",
                        paymentRequestId.GetLinkId()));

            var notificationMessage = _dataObjectFactory.CreateNewDataObject<NotificationMessageData>();
            notificationMessage.Identifier = paymentRequestData.RequestDetails.Details.OrderReference;
            notificationMessage.CreatedOn = _currentDateTimeRetrieverService.GetCurrentDateTime();
            notificationMessage.LinkedPaymentRequestId = paymentRequestData;
            notificationMessage.NotificationType = notificationMessageType;
            notificationMessage.SendToUrl = _paymentRequestNotificationsUtilService.GetNotificationUrlForPaymentRequest(
                documentSession,
                paymentRequestData);
            notificationMessage.Status = EnumsPayMammothConnector.NotificationMessageStatus.Pending;
            documentSession.Store(notificationMessage);
            return notificationMessage;
        }

        public void CheckAndSendPendingNotificationMessageDatas(
            IDocumentSession documentSession)
        {
            var pgSize = _settingsService.GetSettingValue<int>(PayMammoth_v6Enums.PayMammothSettings.Notifications_BatchSizeToSend);

            var notificationsToBeSent = _notificationMessageDataRepository.GetAllNotificationsYetToBeSent(
                documentSession,
                pgSize);

            foreach (var notification in notificationsToBeSent)
            {
                _notificationMessageDataSendingService.SendNotificationMessageData(notification);
            }
        }
    }
}