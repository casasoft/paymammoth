﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using NLog;
using PayMammoth_v6.Modules.Data.Notifications;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.Modules.Services.WebsiteAccounts;
using PayMammoth_v6.Connector;
using Raven.Client;

namespace PayMammoth_v6.Modules.Services.Notifications
{
    public enum PushToAzureStatus
    {
        Pushed,
        Error
    }
    public interface INotificationMessageDataCreatorService
    {
        void CreateNotificationMessageForSuccessfulPaymentAndSend(
            IDocumentSession documentSession,
            ReferenceLink<PaymentRequestTransactionData> paymentRequestTransactionId);

        void CreateNotificationMessageDataForImmediatePaymentExpiredAndPushToAzureQueue(
            IDocumentSession documentSession,
            ReferenceLink<PaymentRequestData> paymentRequestId);

        void CreateNotificationMessageDataForImmediatePaymentCancelledAndPushToAzureQueue(
            IDocumentSession documentSession,
            ReferenceLink<PaymentRequestData> paymentRequestId);
    }


    [IocComponent]
    [EnsureNonNullAspect]
    public class NotificationMessageDataCreatorService : INotificationMessageDataCreatorService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();

        private readonly IPaymentRequestNotificationsUtilService _paymentRequestNotificationsUtilService;
        private readonly INotificationMessageDataService _notificationMessageDataService;
        private readonly INotificationsAzureService _notificationsAzureService;
        private IRavenDbService _ravenDbService;


        public NotificationMessageDataCreatorService(
            IPaymentRequestNotificationsUtilService paymentRequestNotificationsUtilService,
            INotificationMessageDataService notificationMessageDataService,
            INotificationsAzureService notificationsAzureService,
            IRavenDbService ravenDbService)
        {
            _ravenDbService = ravenDbService;

            _notificationsAzureService = notificationsAzureService;
            _notificationMessageDataService = notificationMessageDataService;
            _paymentRequestNotificationsUtilService = paymentRequestNotificationsUtilService;
        }

        public void CreateNotificationMessageDataForImmediatePaymentExpiredAndPushToAzureQueue(
            IDocumentSession documentSession,
            ReferenceLink<PaymentRequestData> paymentRequestId)
        {
            var pushToAzureStatus = CreateNotificationMessageDataForNotificationMessageTypeAndPushToAzureQueue(
                documentSession,
                paymentRequestId,
                EnumsPayMammothConnector.NotificationMessageType.ImmediatePaymentExpired);
        }

        public void CreateNotificationMessageDataForImmediatePaymentCancelledAndPushToAzureQueue(
            IDocumentSession documentSession,
            ReferenceLink<PaymentRequestData> paymentRequestId)
        {
            var pushToAzureStatus = CreateNotificationMessageDataForNotificationMessageTypeAndPushToAzureQueue(
                documentSession,
                paymentRequestId,
                EnumsPayMammothConnector.NotificationMessageType.ImmediatePaymentCancelled);
        }

        public PushToAzureStatus CreateNotificationMessageDataForNotificationMessageTypeAndPushToAzureQueue(
            IDocumentSession documentSession,
            ReferenceLink<PaymentRequestData> paymentRequestId,
            EnumsPayMammothConnector.NotificationMessageType notificationMessageType)
        {
            var status = PushToAzureStatus.Error;


            var paymentRequestGetByIdParams = new GetByIdParams<PaymentRequestData>(
                paymentRequestId,
                 ThrowErrorIfNotAlreadyLoadedInSession.No,
                 LoadOnlyAvailableToFrontend.Yes);
            paymentRequestGetByIdParams.AddInclude(x => x.LinkedWebsiteAccountId);

            var paymentRequestData=    _ravenDbService.GetById(
                                            documentSession,paymentRequestGetByIdParams);

            if (paymentRequestData == null)throw new InvalidOperationException(String.Format("Cannot Create PaymentRequestExpiration NotificationMessageData for PaymentRequestData [{0}] that does not exist!",paymentRequestId.GetLinkId()));

            WebsiteAccountData websiteAccountData = _ravenDbService.GetById(
                documentSession,
                paymentRequestData.LinkedWebsiteAccountId,
                throwErrorIfNotAlreadyLoadedInSession: ThrowErrorIfNotAlreadyLoadedInSession.Yes,
                loadOnlyAvailableToFrontend: LoadOnlyAvailableToFrontend.Yes);

            if (websiteAccountData == null)throw new InvalidOperationException(String.Format("Failed to load WebsiteAccountData [{0}] from PaymentRequestData [{1}]",paymentRequestData.LinkedWebsiteAccountId,paymentRequestId));

            string notificationUrl = _paymentRequestNotificationsUtilService.GetNotificationUrlForPaymentRequest(
                documentSession,
                paymentRequestData);

            if (!String.IsNullOrWhiteSpace(notificationUrl))
            {
                var notificationMsg = _notificationMessageDataService.CreateNotificationMessageDataForPaymentRequestDataWithNotificationMessageType(
                    paymentRequestId,
                    notificationMessageType);

                _notificationsAzureService.PushNotificationOnAzureQueue(
                    notificationMsg,
                    scheduleTimeToSendUtc: null);

                status = PushToAzureStatus.Pushed;
            }
            else
            {
                _log.Warn("Could not create notification message, because no ClientWebsiteResponseUrl is set for account '{0}'",websiteAccountData.Id);
            }
            return status;
        }

        public void CreateNotificationMessageForSuccessfulPaymentAndSend(
            IDocumentSession documentSession,
            ReferenceLink<PaymentRequestTransactionData> paymentRequestTransactionId)
        {
            //todo: [For: Backend | 2014/04/03] re-execute unit tests.  This has been updated, related to NotificationUrl (WrittenBy: Karl)  
            //todo: [For: Karl | 2014/05/15] update unit-tests to reflect pushing notifications on azure queue (WrittenBy: Karl)        				
            var getTransactionByIdParams = new GetByIdParams<PaymentRequestTransactionData>(paymentRequestTransactionId);
            getTransactionByIdParams.AddInclude(x => x.PaymentRequestId);
            var transaction = documentSession.GetById(getTransactionByIdParams);

            var requestGetByIdParams = new GetByIdParams<PaymentRequestData>(transaction.PaymentRequestId);
            requestGetByIdParams.AddInclude(x => x.LinkedWebsiteAccountId);
            var request = documentSession.GetById(requestGetByIdParams);
            var websiteAccount = documentSession.GetById(new GetByIdParams<WebsiteAccountData>(request.LinkedWebsiteAccountId));

            string notificationUrl = _paymentRequestNotificationsUtilService.GetNotificationUrlForPaymentRequest(
                documentSession,
                request);

            //-----------
            if (!String.IsNullOrWhiteSpace(notificationUrl))
            {
                var notificationMsg = _notificationMessageDataService.CreateNotificationMessageDataForPaymentRequestDataWithNotificationMessageType(
                    request,
                    EnumsPayMammothConnector.NotificationMessageType.ImmediatePaymentSuccess);

                //this is done, because the actual notification message is sent on a seperate thread and hence must be saved to database
                //update this to actually push to Azure Queue

                _notificationsAzureService.PushNotificationOnAzureQueue(
                    notificationMsg,
                    scheduleTimeToSendUtc: null);
            }
            else
            {
                _log.Warn(
                    "Could not create notification message, because no ClientWebsiteResponseUrl is set for account '{0}'",
                    websiteAccount.Id);
            }
        }
    }
}