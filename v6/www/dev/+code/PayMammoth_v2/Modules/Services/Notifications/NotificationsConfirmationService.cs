﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Presentation.Services.Fields;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Notifications;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Modules.Services.Notifications
{
    public interface INotificationsConfirmationService
    {
        bool ConfirmNotification(ReferenceLink<NotificationMessageData> notificationId, EnumsPayMammothConnector.NotificationMessageType messageType);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class NotificationsConfirmationService : INotificationsConfirmationService
    {
        private readonly IRavenDbService _ravenDbService;

        public NotificationsConfirmationService(IRavenDbService ravenDbService)
        {
            _ravenDbService = ravenDbService;
        }

        public bool ConfirmNotification(ReferenceLink<NotificationMessageData> notificationId, EnumsPayMammothConnector.NotificationMessageType messageType)
        {
            //todo: [For: Backend | 2014/04/03] implement unit-tests for NotificationsConfirmationService.ConfirmNotification (WrittenBy: Backend)

            NotificationMessageData msg;
            var session = _ravenDbService.CreateNewSession();
            var notification = session.GetById<NotificationMessageData>(new GetByIdParams<NotificationMessageData>(notificationId) 
                    { ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No , ThrowErrorIfDoesNotExist = ThrowErrorIfDoesNotExist.No});
            bool confirmed = false;
            if (notification != null && notification.NotificationType == messageType)
            {
                confirmed = true;
            }
            return confirmed;

            
            
        }
    }
}
