﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using Microsoft.ServiceBus.Messaging;
using PayMammoth_v6.Modules.Data.Notifications;

namespace PayMammoth_v6.Modules.Services.Notifications
{
    using BusinessLogic_CS_v6.Aspects;
    using BusinessLogic_CS_v6.Aspects.NullAspects;
    using CS.General_CS_v6.Modules.InversionOfControl;

    public interface INotificationsAzureService
    {
        /// <summary>
        /// Queues a notification msg on azure.  
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="scheduleTimeToSendUtc">Optional. If left null, it is sent immediately. If specified, it is sent on that time.</param>
        /// <returns></returns>
        BrokeredMessage PushNotificationOnAzureQueue(NotificationMessageData msg, DateTimeOffset? scheduleTimeToSendUtc);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class NotificationsAzureService : INotificationsAzureService
    {
        private readonly INotificationsAzureQueueManager _notificationsAzureQueueManager;

        public NotificationsAzureService(INotificationsAzureQueueManager notificationsAzureQueueManager)
        {
            _notificationsAzureQueueManager = notificationsAzureQueueManager;
        }

        public BrokeredMessage PushNotificationOnAzureQueue(NotificationMessageData msg, DateTimeOffset? scheduleTimeToSendUtc)
        {
            //todo: [For: Karl | 2014/05/14] add unit-tests NotificationsAzureService.PushNotificationOnAzureQueue (WrittenBy: Karl)
            QueueNotificationMessage queueMsg = new QueueNotificationMessage();
            queueMsg.NotificationId = new ReferenceLink<NotificationMessageData>(msg.Id);
            var message = new BrokeredMessage(queueMsg);
            if (scheduleTimeToSendUtc.HasValue)
            {
                message.ScheduledEnqueueTimeUtc = scheduleTimeToSendUtc.Value.UtcDateTime;
            }
            _notificationsAzureQueueManager.QueueClient.Send(message);
            // Submit the order
            return message;

        }
    }
}
