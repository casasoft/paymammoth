﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services.Settings;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Repositories.Payments;
using Raven.Client;
using Raven.Client.Linq;

namespace PayMammoth_v6.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestExpirerService
    {
        void CheckForBatchOfExpiredPaymentRequestsAndMarkedThemAsExpired(
          IDocumentSession documentSession);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentRequestExpirerService : IPaymentRequestExpirerService
    {
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;
        private readonly IPaymentRequestDataMarkerService _paymentRequestDataMarkerService;
        private readonly IPaymentRequestRepository _paymentRequestRepository;

        public PaymentRequestExpirerService(
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService,
            IPaymentRequestDataMarkerService paymentRequestDataMarkerService,
            IPaymentRequestRepository paymentRequestRepository)
        {
            _paymentRequestRepository = paymentRequestRepository;
            _paymentRequestDataMarkerService = paymentRequestDataMarkerService;
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
        }

        public void CheckForBatchOfExpiredPaymentRequestsAndMarkedThemAsExpired(
            IDocumentSession documentSession)
        {
            //PAYMFIVE-63

            var expiredRequests = _paymentRequestRepository.GetAllPendingPaymentRequestsWhichAreExpired(documentSession);

            foreach (var expiredRequest in expiredRequests)
            {
                _paymentRequestDataMarkerService.MarkPaymentRequestAsExpiredAndSendNotification(expiredRequest);
            }

            //_ravenDbService.GetAllDatabaseObjectsForDatabaseTypeWithPaging<PaymentRequestData>(
            //    documentSession,
            //    0,
            //    9999,
            //    LoadOnlyAvailableToFrontend.Yes), 
            //      q => q.Where(x => x.PaymentStatus.Status == PaymentStatusInfo.PaymentStatus.Pending &&
            //          (x.RequestDetails.ExpirationInfo.ExpiresAt != null && x.RequestDetails.ExpirationInfo.ExpiresAt > currentDateTime)),
            //      item =>  _paymentRequestDataMarkerService.MarkPaymentRequestAsExpiredAndSendNotification(item));
            //}
        }
    }
}
