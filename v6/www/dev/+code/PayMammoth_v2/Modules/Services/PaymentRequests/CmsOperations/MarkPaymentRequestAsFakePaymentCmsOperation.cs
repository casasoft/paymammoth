﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.Authorization;
using BusinessLogic_CS_v6.Framework.Cms;
using BusinessLogic_CS_v6.Framework.Cms.CustomOperations;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.FakePayments;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Modules.Services.PaymentRequests.CmsOperations
{
    [AccessRequiredDefaults(GenericEnums.AccessTypes.General)]
    [CmsOperationDefaults(Title = "Mark As Fake Payment")]
    public class MarkPaymentRequestAsFakePaymentCmsOperation : CmsSpecificOperation<PaymentRequestData>
    {
        private readonly IRavenDbService _ravenDbService;


        public MarkPaymentRequestAsFakePaymentCmsOperation()
        {
            _ravenDbService = InversionUtil.Get<IRavenDbService>();
        }

        protected override CmsOperationResult ExecuteOperation(PaymentRequestData item, List<CmsFieldDataValue> fieldInputValues)
        {

            //todo: [For: Karl | 2014/05/15] update this to be able to take a form, which you can tick the payment method and/or fake (WrittenBy: Karl)        			

            var session = _ravenDbService.CreateNewSession();
            
            var websiteAccountGetByIdParams = new GetByIdParams<WebsiteAccountData>(item.LinkedWebsiteAccountId);
            websiteAccountGetByIdParams.ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No;

            
            var websiteAccount = session.GetById(websiteAccountGetByIdParams);

            session.Dispose();
            


            InversionUtil.Get<IMakeFakePaymentService>().MakeFakePayment(item, EnumsPayMammothConnector.PaymentMethodType.BankTransfer,
                websiteAccount.FakePayments.FakePaymentKey);
            var result = new CmsOperationResult();
            
            result.NotificationMessage = "Request marked as paid (fake)!";
            result.ResultType = NotificationMessageType.Success; 
            return result;
        }
    }
}
