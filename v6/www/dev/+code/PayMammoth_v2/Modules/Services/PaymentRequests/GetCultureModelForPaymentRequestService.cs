﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Modules.Services.Cultures;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Services.Cultures;
using PayMammoth_v6.Modules.Data.Payments;

namespace PayMammoth_v6.Modules.Services.PaymentRequests
{
    using BusinessLogic_CS_v6.Aspects;
    using BusinessLogic_CS_v6.Aspects.NullAspects;
    using CS.General_CS_v6.Modules.InversionOfControl;

    public interface IGetCultureModelForPaymentRequestService
    {
        CultureModel GetCultureModelForPaymentRequest(PaymentRequestData paymentRequest);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class GetCultureModelForPaymentRequestService : IGetCultureModelForPaymentRequestService
    {
        private readonly ICultureDataPresentationService _cultureModelService;

        public GetCultureModelForPaymentRequestService(ICultureDataPresentationService cultureModelService)
        {
            _cultureModelService = cultureModelService;
        }

        public CultureModel GetCultureModelForPaymentRequest(PaymentRequestData paymentRequest)
        {
            //todo: [For: Karl | 2014/04/29] implement GetCultureModelForPaymentRequestService.GetCultureModelForPaymentRequestService (WrittenBy: Karl)
            return _cultureModelService.GetCultureModelFromIdentifier("en");
        }
    }
}