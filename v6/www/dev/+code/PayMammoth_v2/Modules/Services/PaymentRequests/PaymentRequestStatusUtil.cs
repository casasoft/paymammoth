﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;

namespace PayMammoth_v6.Modules.Services.PaymentRequests
{
    
  
   
    [LogAspect]
    [EnsureNonNullAspect]
    public static class PaymentRequestStatusUtil 
    {
      
        public static bool CheckIfRequestIsStillPending(PaymentRequestData request)
        {
            //todo: [For: Karl | 2014/06/20] add unit-tests for  CheckIfRequestIsStillPending(WrittenBy: Karl)        			
            return (request.PaymentStatus.Status == PaymentStatusInfo.PaymentStatus.Pending);
            
        }
    }
}
