﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Modules.Services._Shared.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Extensions;
using CS.General_CS_v6.Modules.InversionOfControl;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;
using Raven.Client;


namespace PayMammoth_v6.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestValidatorService
    {
        EnumsPayMammothConnector.InitialRequestResponseStatus ValidateInitialRequest(InitialRequestInfo initialRequestInfo);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentRequestValidatorService : IPaymentRequestValidatorService
    {
        private readonly IIso4217_CurrencyDataCache _iso4217CurrencyDataCache;

        public PaymentRequestValidatorService(
            IIso4217_CurrencyDataCache iso4217CurrencyDataCache)
        {
            _iso4217CurrencyDataCache = iso4217CurrencyDataCache;
        }

        public EnumsPayMammothConnector.InitialRequestResponseStatus ValidateInitialRequest(InitialRequestInfo initialRequestInfo)
        {


            initialRequestInfo.MustNotBeNullable("InitialRequestInfo is required");
            //defensive checks done

            EnumsPayMammothConnector.InitialRequestResponseStatus result = EnumsPayMammothConnector.InitialRequestResponseStatus.Success;
            if (initialRequestInfo.ReturnUrls == null)
            {
                result = EnumsPayMammothConnector.InitialRequestResponseStatus.ReturnUrlSuccessIsRequired;
            }
            else if (initialRequestInfo.ReturnUrls != null &&
                     string.IsNullOrWhiteSpace(initialRequestInfo.ReturnUrls.SuccessUrl))
            {
                result = EnumsPayMammothConnector.InitialRequestResponseStatus.ReturnUrlSuccessIsRequired;
            }
            else if (initialRequestInfo.ReturnUrls != null &&
                     string.IsNullOrWhiteSpace(initialRequestInfo.ReturnUrls.CancelUrl))
            {
                result = EnumsPayMammothConnector.InitialRequestResponseStatus.ReturnUrlFailureIsRequired;
            }
            else if (initialRequestInfo.NotificationUrl.IsNullOrWhiteSpace())
            {
                result = EnumsPayMammothConnector.InitialRequestResponseStatus.NotificationUrlIsRequred;
            }
            else if (initialRequestInfo.ItemDetails == null || initialRequestInfo.ItemDetails.Count == 0)
            {
                result = EnumsPayMammothConnector.InitialRequestResponseStatus.PleaseSpecifyAtLeastOneItem;
            }
            
            else if (string.IsNullOrWhiteSpace(initialRequestInfo.Pricing.CurrencyCode3Letter))
            {
                result = EnumsPayMammothConnector.InitialRequestResponseStatus.CurrencyCodeNotFilledIn;
            }
            if (result == EnumsPayMammothConnector.InitialRequestResponseStatus.Success &&
                !string.IsNullOrWhiteSpace(initialRequestInfo.Pricing.CurrencyCode3Letter))
            {
                var currency = _iso4217CurrencyDataCache.GetCurrencyByCurrencyCode(initialRequestInfo.Pricing.CurrencyCode3Letter);
                if (currency == null)
                {
                    result = EnumsPayMammothConnector.InitialRequestResponseStatus.CurrencyCodeInvalid;
                }
            }

            if (result == EnumsPayMammothConnector.InitialRequestResponseStatus.Success && initialRequestInfo.ItemDetails != null)
            {
                foreach (var item in initialRequestInfo.ItemDetails)
                {
                    if (item.UnitPrice <= 0)
                    {
                        result = EnumsPayMammothConnector.InitialRequestResponseStatus.ItemPriceMustNeverBeZeroOrLess;
                        
                    }
                    else if (item.Quantity <= 0)
                    {
                        result = EnumsPayMammothConnector.InitialRequestResponseStatus.ItemQuantityMustBeOneOrGreater;

                    }
                    else if (string.IsNullOrWhiteSpace(item.Title))
                    {
                        result = EnumsPayMammothConnector.InitialRequestResponseStatus.ItemTitleMustBeFilledIn;

                    }
                    if (result != EnumsPayMammothConnector.InitialRequestResponseStatus.Success)
                    {
                        break;
                    }
                }
            }

            return result;

        }
    }
}
