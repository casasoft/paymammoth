﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Modules.Data.Payments;
using PayPal.PayPalAPIInterfaceService.Model;
using Raven.Client;

namespace PayMammoth_v2.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestMarkerService
    {

    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentRequestMarkerService : IPaymentRequestMarkerService
    {
        private readonly IPaymentRequestService _paymentRequestService;

        public PaymentRequestMarkerService(
            IPaymentRequestService paymentRequestService)
        {
            _paymentRequestService = paymentRequestService;
        }

        [RavenTaskAspect]
        public void MarkPaymentRequestAsExpired(
            ReferenceLink<PaymentRequestData> paymentRequestId,
            IDocumentSession documentSession = null)
        {
            var paymentRequestData = _paymentRequestService.GetById(
                documentSession,
                paymentRequestId,
                throwErrorIfNotAlreadyLoadedInSession: false,
                loadItemOnlyIfAvailableToFrontend: true);

            if (paymentRequestData == null) throw new InvalidOperationException(String.Format("Cannot mark PaymentRequest [{0}]. It does not seem to exist", paymentRequestId.GetLinkId()));


            if (paymentRequestData.PaymentStatus.Paid == true)throw new InvalidOperationException(String.Format("Cannot mark PaymentRequest [{0}]. It does not seem to exist", paymentRequestId.GetLinkId()));
        }
    }
}
