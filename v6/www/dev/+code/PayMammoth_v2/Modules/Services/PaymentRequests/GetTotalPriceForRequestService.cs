﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v6.Modules.Data.Payments;

namespace PayMammoth_v6.Modules.Services.PaymentRequests
{
    using BusinessLogic_CS_v6.Aspects;
    using BusinessLogic_CS_v6.Aspects.NullAspects;
    using CS.General_CS_v6.Modules.InversionOfControl;

    public interface IGetTotalPriceForRequestService
    {
        decimal GetTotalPrice(PaymentRequestData request, bool includeTaxes, bool includeShippingAndHandling);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class GetTotalPriceForRequestService : IGetTotalPriceForRequestService
    {
        public GetTotalPriceForRequestService()
        {

        }
        public decimal GetTotalPrice(PaymentRequestData request, bool includeTaxes,
            bool includeShippingAndHandling)
        {
            //todo: [For: Karl | 2014/01/06] add unit tests for PaymentRequestService.GetTotalPrice (WrittenBy: Karl)
            decimal total = 0;
            foreach (var item in request.RequestDetails.ItemDetails)
            {
                decimal totalPriceExcTax = item.Quantity * item.UnitPrice;
                decimal totalTax = item.Quantity * item.TaxAmountPerUnit;
                total += totalPriceExcTax;
                if (includeTaxes)
                {
                    total += totalTax;
                }
            }
            if (includeShippingAndHandling)
            {
                total += request.RequestDetails.Pricing.HandlingAmount;
                total += request.RequestDetails.Pricing.ShippingAmount;
            }
            return total;

            

        }
    }
}
