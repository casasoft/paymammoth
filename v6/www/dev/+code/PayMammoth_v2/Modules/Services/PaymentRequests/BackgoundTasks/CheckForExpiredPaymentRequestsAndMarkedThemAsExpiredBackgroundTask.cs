﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.Tasks;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Modules.Services.PaymentRequests;

namespace PayMammoth_v6.Presentation.Services.Payments.BackgoundTasks
{
    public class CheckForExpiredPaymentRequestsAndMarkedThemAsExpiredBackgroundTask : BaseRecurringBackgroundTask
    {
        public CheckForExpiredPaymentRequestsAndMarkedThemAsExpiredBackgroundTask()
            : base()
        {
        }

        public override Enum SettingForRecurringTaskInterval
        {
            get { return Enums.PayMammoth_v6Enums.BackgroundTasksSettings.CheckForExpiredPaymentRequestsAndMarkedThemAsExpiredTask_RecurringIntervalInSeconds; }
        }

        protected override void recurringTask()
        {
            var documentSession = RavenDbUtil.DocumentStore.OpenSession();
            InversionUtil.Get<IPaymentRequestExpirerService>().CheckForBatchOfExpiredPaymentRequestsAndMarkedThemAsExpired(documentSession);
            documentSession.Dispose();
        }
    }
}
