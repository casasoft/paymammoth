﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;

namespace PayMammoth_v6.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestStatusCheckService
    {
        bool CheckIfRequestIsPaid(PaymentRequestData request);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentRequestStatusCheckService : IPaymentRequestStatusCheckService
    {
        public PaymentRequestStatusCheckService()
        {

        }

        public bool CheckIfRequestIsPaid(PaymentRequestData request)
        {
            return request.PaymentStatus.Status == PaymentStatusInfo.PaymentStatus.Paid;
        }
    }
}
