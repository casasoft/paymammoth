﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Framework.RavenDb.Services;
using BusinessLogic_CS_v6.Modules.Importer.Helpers;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services.Settings;

using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Extensions;
using CS.General_CS_v6.Modules.InversionOfControl;
using CS.General_CS_v6.Modules.IPAddress;
using NLog;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Repositories.Payments;
using PayMammoth_v6.Modules.Services.Emails;
using PayMammoth_v6.Modules.Services.InitialRequests;
using PayMammoth_v6.Modules.Services.Logging;
using PayMammoth_v6.Modules.Services.Notifications;
using PayMammoth_v6.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;
using Raven.Client;
using PayMammoth_v6.Modules.Services.PaymentMethods;
using Raven.Client.Connection.Profiling;
using Raven.Client.Extensions;
using Raven.Client.Linq;

namespace PayMammoth_v6.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestDataService 
    {
        PaymentRequestDataService.GeneratePaymentRequestResult GeneratePaymentRequestFromInitialRequest(
            ReferenceLink<WebsiteAccountData> websiteAccountId,
            InitialRequestInfo initialRequestInfo,
            IDocumentSession documentSession = null);

        bool WaitUntilRequestIsMarkedAsPaid(ReferenceLink<PaymentRequestData> paymentRequestId);

        List<PaymentMethodData> GetAllPaymentMethodsAvailableForPayment(
            ReferenceLink<PaymentRequestData> requestId,
            IDocumentSession documentSession);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentRequestDataService : IPaymentRequestDataService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();

        public class GeneratePaymentRequestResult
        {
            public EnumsPayMammothConnector.InitialRequestResponseStatus Status { get; set; }

            public PaymentRequestData GeneratedRequest { get; set; }
        }

        private readonly IPaymentRequestValidatorService _paymentRequestValidatorService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IInitialRequestService _initialRequestService;
        private readonly IIPAddressRetrieverService _ipAddressRetrieverService;
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;
        private readonly IPayMammothLogService _payMammothLogService;
        private readonly IRavenDbService _ravenDbService;
        private readonly ISettingsService _settingsService;
        private readonly IPaymentRequestStatusCheckService _paymentRequestStatusCheckService;
        private readonly IPaymentMethodsRepository _paymentMethodsRepository;
        private readonly IPaymentMethodsService _paymentMethodsService;

        public PaymentRequestDataService(
            IDataObjectFactory dataObjectFactory,
            IPaymentRequestValidatorService paymentRequestValidatorService,
            IInitialRequestService initialRequestService,
            IIPAddressRetrieverService ipAddressRetrieverService,
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService,
            IPayMammothLogService payMammothLogService,
            IRavenDbService ravenDbService,
            ISettingsService settingsService,
            IPaymentRequestStatusCheckService paymentRequestStatusCheckService,
            IPaymentMethodsRepository paymentMethodsRepository,
            IPaymentMethodsService paymentMethodsService
        

            ) 
           
        {
            _paymentMethodsService = paymentMethodsService;
            _paymentMethodsRepository = paymentMethodsRepository;
            _paymentRequestStatusCheckService = paymentRequestStatusCheckService;
            _settingsService = settingsService;
            _ravenDbService = ravenDbService;
            _payMammothLogService = payMammothLogService;
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
            _ipAddressRetrieverService = ipAddressRetrieverService;
            _initialRequestService = initialRequestService;
            _dataObjectFactory = dataObjectFactory;
            _paymentRequestValidatorService = paymentRequestValidatorService;
        }

        [RavenTaskAspect]
        [LogAspect]
        public GeneratePaymentRequestResult GeneratePaymentRequestFromInitialRequest(
            ReferenceLink<WebsiteAccountData> websiteAccountId,
            InitialRequestInfo initialRequestInfo,
            IDocumentSession documentSession = null)
        {
            //todo: [For: Karl | 2014/01/07] add test case such that website account is copied successfully to payment request (WrittenBy: YourName)        			

            GeneratePaymentRequestResult result = new GeneratePaymentRequestResult();
            result.Status = _paymentRequestValidatorService.ValidateInitialRequest(initialRequestInfo);
            if (result.Status == EnumsPayMammothConnector.InitialRequestResponseStatus.Success)
            {
                var websiteAccount = documentSession.GetById(new GetByIdParams<WebsiteAccountData>(websiteAccountId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});

                var newRequest = _dataObjectFactory.CreateNewDataObject<PaymentRequestData>();
                newRequest.LinkedWebsiteAccountId = websiteAccount;
                _initialRequestService.CopyDetailsFromInitialRequestToPaymentRequest(
                    initialRequestInfo,
                    newRequest);
                newRequest.RequestServerIp = _ipAddressRetrieverService.GetIPAddress();
                newRequest.RequestDateTime = _currentDateTimeRetrieverService.GetCurrentDateTime();
                _payMammothLogService.AddLogEntry(
                    newRequest.StatusLog,
                    "Generated payment request");
                result.GeneratedRequest = newRequest;
                documentSession.Store(newRequest);
            }
            return result;
        }


        [LogAspect(GenericEnums.NlogLogLevel.Trace)]
        public bool WaitUntilRequestIsMarkedAsPaid(ReferenceLink<PaymentRequestData> paymentRequestId)
        {
            //todo: [For: Karl | 2014/01/07] add unit tests for PaymentRequestService.WaitUntilRequestIsMarkedAsPaid (WrittenBy: Karl)
            bool markedAsPaid = false;
            int totalWait = 0;
            int waitInterval = 2000;

            int timeoutInMilleSec = _settingsService.GetSettingValue<int>(PayMammoth_v6.Enums.PayMammoth_v6Enums.PayMammothSettings.Other_Timeouts_MarkPaid);
            _log.Trace(
                "Timeout to wait until request is marked as paid: {0}",
                timeoutInMilleSec);
            do
            {
                var session = _ravenDbService.CreateNewSession();
                var request = session.GetById(new GetByIdParams<PaymentRequestData>(paymentRequestId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});
                session.Dispose();
                bool isPaid = _paymentRequestStatusCheckService.CheckIfRequestIsPaid(request);
                if (isPaid)
                {
                    markedAsPaid = true;
                    _log.Debug("Request marked as paid");
                }
                else
                {
                    _log.Trace(
                        "Request still not yet paid.  Waiting {0}ms",
                        waitInterval);

                    System.Threading.Thread.Sleep(waitInterval);
                    totalWait += waitInterval;
                }
            }
            while (!markedAsPaid && totalWait <= timeoutInMilleSec);
            if (!markedAsPaid)
            {
                _log.Warn(
                    "Request {0} - called WaitUntilRequestIsMarkedAsPaid but was not marked as paid",
                    paymentRequestId);
            }

            return markedAsPaid;
        }

        public List<PaymentMethodData> GetAllPaymentMethodsAvailableForPayment(
            ReferenceLink<PaymentRequestData> requestId,
            IDocumentSession documentSession)
        {

            var requestParams = new GetByIdParams<PaymentRequestData>(requestId);
            requestParams.ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No;
            requestParams.AddInclude(x => x.LinkedWebsiteAccountId);

            var request = documentSession.GetById(requestParams);

            var websiteAccountParams = new GetByIdParams<WebsiteAccountData>(request.LinkedWebsiteAccountId);
            var websiteAccount = documentSession.GetById(websiteAccountParams);

            var paymentMethodTypesAvailableToWebsiteAccount = _paymentMethodsService.GetAllPaymentMethodTypesAvailableToWebsiteAccount(websiteAccount);

            var allPaymentMethods = _paymentMethodsRepository.GetAllPaymentMethods(documentSession);

            allPaymentMethods.RemoveAll(x => !paymentMethodTypesAvailableToWebsiteAccount.Contains(x.PaymentMethod));

            return allPaymentMethods;
        }

      
    }
}