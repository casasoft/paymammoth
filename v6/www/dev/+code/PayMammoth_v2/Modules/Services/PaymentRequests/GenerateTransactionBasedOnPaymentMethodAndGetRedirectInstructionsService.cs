﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Services.PaymentMethods;
using PayMammoth_v6.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v6.Connector;
using Raven.Client;

namespace PayMammoth_v6.Modules.Services.PaymentRequests
{
    public interface IGenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService
    {
        TransactionGeneratorResult GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructions(
            EnumsPayMammothConnector.PaymentMethodType paymentMethod,
            ReferenceLink<PaymentRequestData> paymentRequestId,
            IDocumentSession documentSession = null);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService : IGenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService
    {
        private readonly IPaymentMethodsService _paymentMethodsService;

        public GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService(
            IPaymentMethodsService paymentMethodsService)
        {
            _paymentMethodsService = paymentMethodsService;
        }

        [LogAspect]
        [RavenTaskAspect]
        public TransactionGeneratorResult GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructions(
            EnumsPayMammothConnector.PaymentMethodType paymentMethod,
            ReferenceLink<PaymentRequestData> paymentRequestId,
            IDocumentSession documentSession = null)
        {
            var transactionGenerator = _paymentMethodsService.GetTransactionGeneratorServiceForPaymentMethod(paymentMethod);
            if (transactionGenerator == null)
                throw new InvalidOperationException(
                    string.Format(
                        "TransactionGenerator not implemented for '{0}'",
                        paymentMethod));
            var result = transactionGenerator.GenerateTransaction(
                paymentRequestId,
                documentSession);
            return result;
        }
    }
}
