﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.Authorization;
using BusinessLogic_CS_v6.Framework.Cms;
using BusinessLogic_CS_v6.Framework.Cms.CustomOperations;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.Services;

namespace PayMammoth_v6.Modules.Services.PaymentRequests.CmsOperations
{
    [AccessRequiredDefaults(GenericEnums.AccessTypes.General)]
    [CmsOperationDefaults(Title = "Go to frontend for this request")]
    public class GoToFrontendForPaymentRequestCmsOperation : CmsSpecificOperation<PaymentRequestData>
    {
        private readonly IRavenDbService _ravenDbService;
        private readonly IPayMammothConnectorUrlService _payMammothConnectorUrlService;


        public GoToFrontendForPaymentRequestCmsOperation()
        {
            _ravenDbService = InversionUtil.Get<IRavenDbService>();
            _payMammothConnectorUrlService = InversionUtil.Get<IPayMammothConnectorUrlService>();
        }

        protected override CmsOperationResult ExecuteOperation(PaymentRequestData item, List<CmsFieldDataValue> fieldInputValues)
        {

            
            var url= _payMammothConnectorUrlService.GetPayMammothUrlForRequest(item.GetRavenDbIdOnlyFromItem(), EnumsPayMammothConnector.SupportedLanguage.English);
            System.Web.HttpContext.Current.Response.Redirect(url);

         
            var result = new CmsOperationResult();

            result.NotificationMessage = "Redirecting...";
            result.ResultType= NotificationMessageType.Success;
            return result;
        }
    }
    
}
