﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Framework.RavenDb.Services;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Extensions;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Services.RecurringProfiles;
using PayMammoth_v6.Connector;
using Raven.Client;

namespace PayMammoth_v6.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestTransactionDataService 
    {

        void MarkTransactionAsPendingActionByBusinessOwner(
           PaymentRequestTransactionData transaction,
           IDocumentSession session);

        void MarkTransactionAsPaid(
            PaymentRequestTransactionData transaction,
            bool requiresManualIntervention,
            string authorisationCode,
            IDocumentSession session);

        PaymentRequestTransactionData CreateNewTransaction(
            ReferenceLink<PaymentRequestData> requestId,
            EnumsPayMammothConnector.PaymentMethodType paymentMethodType,
            IDocumentSession session);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentRequestTransactionDataService : IPaymentRequestTransactionDataService
    {
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;
        private readonly IRecurringProfilesService _recurringProfilesService;
        
        private readonly IPaymentRequestDataMarkerService _markRequestAsSuccessfulService;
        
        private readonly IDataObjectFactory _dataObjectFactory;

        public PaymentRequestTransactionDataService(
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService,
            IRecurringProfilesService recurringProfilesService,
            IPaymentRequestDataMarkerService markRequestAsSuccessfulService,
            IDataObjectFactory dataObjectFactory)
        {
            _dataObjectFactory = dataObjectFactory;
            _markRequestAsSuccessfulService = markRequestAsSuccessfulService;
            _recurringProfilesService = recurringProfilesService;
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
        }

        [LogAspect]
        public void MarkTransactionAsPaid(
            PaymentRequestTransactionData transaction,
            bool requiresManualIntervention,
            string authorisationCode,
            IDocumentSession session)
        {
            //defensive checks
            //var getByIdParams = new GetByIdParams<PaymentRequestTransactionData>(transactionId);
            //getByIdParams.AddInclude(x => x.PaymentRequestId);
            //getByIdParams.ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No;
            //var transaction = session.GetById(getByIdParams);
            //if (transaction == null) throw new InvalidOperationException("Cannot call MarkTransactionAsPaid on a transaction that does not exist - Id: " + transactionId);
            if (transaction.IsSuccessful) throw new InvalidOperationException("Cannot call MarkTransactionAsPaid on a transaction that is already successful - Id: " + transaction);
            var request = session.GetById(new GetByIdParams<PaymentRequestData>(transaction.PaymentRequestId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});
            ;
            if (request == null)
                throw new InvalidOperationException(
                    string.Format(
                        "Cannot call MarkTransactionAsPaid on a transaction whose request does not exist.  TransactionId: {0} | RequestId: {1}",
                        transaction,
                        transaction.PaymentRequestId));
            if (PaymentRequestStatusUtil.CheckIfRequestIsStillPending(request).IsFalse())
                throw new InvalidOperationException(
                    string.Format(
                        "Cannot call MarkTransactionAsPaid on a transaction whose request status is not Pending.  TransactionId: {0} | RequestId: {1}",
                        transaction,
                        transaction.PaymentRequestId));

            //------------

            transaction.DateFinished = transaction.DateLastActivity = _currentDateTimeRetrieverService.GetCurrentDateTime();
            transaction.IsSuccessful = true;
            transaction.AuthCode = authorisationCode;
            transaction.RequiresManualIntervention = requiresManualIntervention;

            if (request.RequestDetails.RecurringProfile.Required)
            {
                _recurringProfilesService.CreateRecurringProfileIfRequired(
                    request,
                    session);
            }

            _markRequestAsSuccessfulService.MarkRequestAsPaidSuccessfulAndSendNotification(
                request,
                transaction,
                session);
        }

        public PaymentRequestTransactionData CreateNewTransaction(
            ReferenceLink<PaymentRequestData> requestId,
            EnumsPayMammothConnector.PaymentMethodType paymentMethodType,
            IDocumentSession session)
        {
            //todo: [For: Karl | 2014/01/07] create unit tests PaymentRequestTransactionsService.CreateNewTransaction (WrittenBy: Karl)
            PaymentRequestTransactionData transaction = _dataObjectFactory.CreateNewDataObject<PaymentRequestTransactionData>();
            transaction.PaymentRequestId = requestId;
            transaction.PaymentMethod = paymentMethodType;

            session.Store(transaction);
            return transaction;
        }

        public void MarkTransactionAsPendingActionByBusinessOwner(PaymentRequestTransactionData transaction, IDocumentSession session)
        {
            //PAYMFIVE-51
            //yet to implement, not critical
        }
    }
}