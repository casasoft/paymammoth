﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Modules.Repositories.Payments
{
    public interface IPaymentTransactionRepository
    {
        PaymentRequestTransactionData GetLatestTransactionForPaymentRequest(ReferenceLink<PaymentRequestData> requestId,
            EnumsPayMammothConnector.PaymentMethodType paymentMethodType); 
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentTransactionRepository : IPaymentTransactionRepository
    {
        public PaymentTransactionRepository()
        {

        }

        public PaymentRequestTransactionData GetLatestTransactionForPaymentRequest(ReferenceLink<PaymentRequestData> requestId,
            EnumsPayMammothConnector.PaymentMethodType paymentMethodType)
        {
            
            //todo: [For: Karl | 2014/01/07] implement PaymentTransactionRepository.GetLatestTransactionForPaymentRequest (WrittenBy: Karl)
            throw new NotImplementedException();
        }
    }
}
