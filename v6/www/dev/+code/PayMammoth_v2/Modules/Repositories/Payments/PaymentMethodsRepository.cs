﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Modules.Services.Repositories;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Payments;
using Raven.Client;

namespace PayMammoth_v6.Modules.Repositories.Payments
{
    public interface IPaymentMethodsRepository
    {
        List<PaymentMethodData> GetAllPaymentMethods(IDocumentSession documentSession);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentMethodsRepository : IPaymentMethodsRepository
    {
        private readonly IRepositoryService _repositoryService;
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;

        public PaymentMethodsRepository(
            IRepositoryService repositoryService,
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService)
        {
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
            _repositoryService = repositoryService;
        }

        public List<PaymentMethodData> GetAllPaymentMethods(IDocumentSession documentSession)
        {
            //todo: [For: Karl | 2014/01/14] create unit tests PaymentMethodsRepository.GetAllPaymentMethods (WrittenBy: Karl)
            var q = documentSession.Query<PaymentMethodData>(typeof(PaymentMethodDataIndex).Name);
            RavenDbQueryUtil.AddConditionsToQueryToLoadOnlyAvailableForFrontend(
                _currentDateTimeRetrieverService,
                q);
            var list = q.ToList();
            return list;
        }
    }
}