﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Util;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using Raven.Client;
using Raven.Client.Linq;

namespace PayMammoth_v6.Modules.Repositories.Payments
{
    using BusinessLogic_CS_v6.Aspects;
    using BusinessLogic_CS_v6.Aspects.NullAspects;
    using CS.General_CS_v6.Modules.InversionOfControl;

    public interface IPaymentRequestRepository
    {
        List<PaymentRequestData> GetAllPendingPaymentRequestsWhichAreExpired(IDocumentSession session);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentRequestRepository : IPaymentRequestRepository
    {
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;

        public PaymentRequestRepository(ICurrentDateTimeRetrieverService currentDateTimeRetrieverService)
        {
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
        }

        #region IPaymentRequestRepository Members

        public List<PaymentRequestData> GetAllPendingPaymentRequestsWhichAreExpired(IDocumentSession session)
        {
            //PAYMFIVE-66
            IRavenQueryable<PaymentRequestData> q = BusinessLogic_CS_v6.Util.RavenDbQueryUtil
                                                                   .CreateFrontendQuery<PaymentRequestData, PaymentRequestDataIndex>(session,
                                                                           _currentDateTimeRetrieverService,
                                                                           AutoSortByPriority.No);

            var currentDateTime = _currentDateTimeRetrieverService.GetCurrentDateTime();
            q = q.Where(x => x.PaymentStatus.Status == PaymentStatusInfo.PaymentStatus.Pending &&
                             (x.RequestDetails.ExpirationInfo.ExpiresAt != null &&
                              x.RequestDetails.ExpirationInfo.ExpiresAt < currentDateTime));
            var result = q.Take(99999).ToList();
            return result;
        }

        #endregion
    }
}
