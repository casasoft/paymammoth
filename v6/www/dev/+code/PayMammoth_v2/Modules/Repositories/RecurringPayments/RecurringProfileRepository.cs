﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.RecurringProfiles;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Modules.Repositories.RecurringPayments
{
    public interface IRecurringProfileRepository
    {
        RecurringProfileData GetRecurringProfileByGatewayIdentifier(string gatewayIdentifier, EnumsPayMammothConnector.PaymentMethodType gateway);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class RecurringProfileRepository : IRecurringProfileRepository
    {
        public RecurringProfileRepository()
        {
            
        }
    
        public RecurringProfileData GetRecurringProfileByGatewayIdentifier(string gatewayIdentifier, EnumsPayMammothConnector.PaymentMethodType gateway)
        {
            //todo: [For: Karl | 2014/01/07] implement RecurringPaymentsRepository.GetRecurringProfileByGatewayIdentifier (WrittenBy: Karl)
 	        //  var q = GetQuery();
            //q.Where(item => item.PaymentGateway == gateway && item.ProfileIdentifierOnGateway == gatewayIdentifier);
            //q.Take(1);
            //return FindItem(q);
            return null;
        }
    }
}
