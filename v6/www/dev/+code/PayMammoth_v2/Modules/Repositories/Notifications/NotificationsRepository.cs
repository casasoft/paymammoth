﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Notifications;
using PayMammoth_v6.Connector;
using Raven.Client;
using Raven.Client.Linq;

namespace PayMammoth_v6.Modules.Repositories.Notifications
{
    public interface INotificationMessageDataRepository
    {
        List<NotificationMessageData> GetAllNotificationsYetToBeSent(
            IDocumentSession documentSession,
            int pgSize);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class NotificationMessageDataRepository : INotificationMessageDataRepository
    {
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;

        public NotificationMessageDataRepository(
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService)
        {
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
        }

        public List<NotificationMessageData> GetAllNotificationsYetToBeSent(
            IDocumentSession documentSession,
            int pgSize)
        {
            var currentDate = _currentDateTimeRetrieverService.GetCurrentDateTime();
            var query = documentSession.Query<NotificationMessageData, NotificationMessageDataIndex>();
            query = query.Where(x => x.NextRetryOn <= currentDate && x.Status == EnumsPayMammothConnector.NotificationMessageStatus.Pending);
            var list = query.Take(pgSize).ToList();
            return list;
        }
    }
}