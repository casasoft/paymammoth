﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.Cms.DefaultMetadataValues;
using BusinessLogic_CS_v6.Framework.DbObjects.Collections;
using BusinessLogic_CS_v6.Framework.DbObjects.Objects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Framework.RavenDb.Indexes;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Notifications;

namespace PayMammoth_v6.Modules.Data.RecurringProfiles
{


    public class RecurringProfilePaymentTransactionData : BaseObject
    {
        public RecurringProfilePaymentTransactionData()
        {

        }
        public ReferenceLink<NotificationMessageData> LinkedNotificationMsgId { get; set; }
        public ReferenceLink<RecurringProfileData> ProfileId { get; set; }
        public string OtherData { get; set; }
        public string PaymentGatewayReference { get; set; }
        public string ResponseContent { get; set; }
        public PayMammoth_v6Enums.RecurringProfileTransactionStatus Status { get; set; }
    }


    [CmsSectionDefaults(ShowInMainMenu = true)]
    public class RecurringProfilePaymentTransactionIndex : DocumentIndexCreationTask<RecurringProfilePaymentTransactionData>
    {
        public RecurringProfilePaymentTransactionIndex()
        {

        }

        public override string IndexName
        {
            get
            {
                return typeof (RecurringProfilePaymentTransactionIndex).Name;
            }
        }

        protected override void createIndex()
        {
            // index definition goes here


            this.Map = (items => items.Select(item => new
            {
                //index fields go here
                CommonMetaData_CreatedOn = item.CommonMetaData.CreatedOn,
                CommonMetaData_Published = item.CommonMetaData.Published,
                CommonMetaData_PublishedOn = item.CommonMetaData.PublishedOn,
                CommonMetaData_PublishedUntil = item.CommonMetaData.PublishedUntil,
                CommonMetaData_Deleted = item.CommonMetaData.Deleted,
                CommonMetaData_SortPriority = item.CommonMetaData.SortPriority,
                CommonMetaData_IsTemporary = item.CommonMetaData.IsTemporary,
                CommonMetaData_LastEditedOn = item.CommonMetaData.LastEditedOn


            }));

        }

    }
   
}
