﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DbObjects.Collections;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Modules.Data.RecurringProfiles
{
    public class RecurringProfilePayment : SubCollectionItem
    {
        public int FailureCount { get; set; }
        public string Identifier { get; set; }
        public bool InitiatedByPayMammoth { get; set; }
        public DateTimeOffset? NextRetryOn { get; set; }
        public DateTimeOffset PaymentDueOn { get; set; }
        public EnumsPayMammothConnector.RecurringProfilePaymentStatus Status { get; set; }
        

    }
}
