﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.Cms.DefaultMetadataValues;
using BusinessLogic_CS_v6.Framework.DbObjects.Collections;
using BusinessLogic_CS_v6.Framework.DbObjects.Objects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Framework.RavenDb.Indexes;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Data._Shared;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Modules.Data.RecurringProfiles
{

    public class RecurringProfileData : BaseObject
    {
        public RecurringProfileData()
        {
            this.Payments = new SubCollection<RecurringProfilePayment>();
         
        }

        public DateTimeOffset? ActivatedOn { get; set; }
        public int BillingCyclesCompleted { get; set; }
        public ReferenceLink<PaymentRequestData> CreatedByPaymentRequestId { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public EnumsPayMammothConnector.RecurringProfileCreateStatusMsg CreationStatus { get; set; }
        public DateTimeOffset? ExpiredOn { get; set; }
        public string Identifier { get; set; }
        public int MaximumFailedAttempts { get; set; }
        public string NotificationUrl { get; set; }

        public EnumsPayMammothConnector.PaymentMethodType PaymentGatewayUsed { get; set;}

        public SubCollection<RecurringProfilePayment> Payments { get; set; }
        

        public string ProfileIdentifierOnPaymentGateway { get; set; }
        public int RecurringIntervalFrequency { get; set; }
        public EnumsPayMammothConnector.RecurringBillingPeriod RecurringIntervalType { get; set; }

        public int? TotalBillingCycles { get; set; }

        public ReferenceLink<WebsiteAccountData> WebsiteAccountId { get; set; }

        public PayMammoth_v6Enums.RecurringProfileStatus Status { get; set; }

        private PayMammothLog _m_PayMammothLog;

        public PayMammothLog PayMammothLog
        {
            get
            {
                if (_m_PayMammothLog == null)
                {
                    _m_PayMammothLog = new PayMammothLog();
                }
                return _m_PayMammothLog;
            }
            set { _m_PayMammothLog = value; }
        }


    }


    [CmsSectionDefaults(ShowInMainMenu = true)]
    public class RecurringProfileDataIndex : DocumentIndexCreationTask<RecurringProfileData>
    {
        public RecurringProfileDataIndex()
        {

        }




        public override string IndexName
        {
            get
            {
                return typeof (RecurringProfileDataIndex).Name;
            }
        }

        protected override void createIndex()
        {
            // index definition goes here


            this.Map = (items => items.Select(item => new
            {
                //index fields go here
                CommonMetaData_CreatedOn = item.CommonMetaData.CreatedOn,
                CommonMetaData_Published = item.CommonMetaData.Published,
                CommonMetaData_PublishedOn = item.CommonMetaData.PublishedOn,
                CommonMetaData_PublishedUntil = item.CommonMetaData.PublishedUntil,
                CommonMetaData_Deleted = item.CommonMetaData.Deleted,
                CommonMetaData_SortPriority = item.CommonMetaData.SortPriority,
                CommonMetaData_IsTemporary = item.CommonMetaData.IsTemporary,
                CommonMetaData_LastEditedOn = item.CommonMetaData.LastEditedOn


            }));

        }

    }
}
