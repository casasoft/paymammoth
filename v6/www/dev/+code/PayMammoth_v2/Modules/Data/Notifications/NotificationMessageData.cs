﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.Cms.DefaultMetadataValues;
using BusinessLogic_CS_v6.Framework.DbObjects.Collections;
using BusinessLogic_CS_v6.Framework.DbObjects.Objects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Framework.RavenDb.Indexes;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data._Shared;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Modules.Data.Notifications
{

    public class NotificationMessageData : BaseObject
    {
        public NotificationMessageData()
        {
            //this.StatusLog = new PayMammothLog();
            
        }
        [CmsFieldDefaults(ShowInListing = true, Searchable = true)]

        public DateTimeOffset CreatedOn { get; set; }
        [CmsFieldDefaults(ShowInListing = true, Searchable = true)]
        public EnumsPayMammothConnector.NotificationMessageStatus Status { get; set; }
        /// <summary>
        /// Check comment of NotificationMessage.Identifier
        /// </summary>
        [CmsFieldDefaults(ShowInListing = true, Searchable = true)]
        public string Identifier { get; set; }
        [CmsFieldDefaults(ShowInListing = true, Searchable = true)]
        public ReferenceLink<PaymentRequestData> LinkedPaymentRequestId { get; set; }
        [CmsFieldDefaults(ShowInListing = true, Searchable = true)]
        public string Message { get; set; }
        [CmsFieldDefaults(ShowInListing = true, Searchable = true)]
        public DateTimeOffset NextRetryOn { get; set; }

        public DateTimeOffset? SentSuccessfullyOn { get; set; }

        [CmsFieldDefaults(ShowInListing = true, Searchable = true)]
        public EnumsPayMammothConnector.NotificationMessageType NotificationType { get; set; }
        public int RetryCount { get; set; }
        public string SendToUrl { get; set; }
        


        private SubCollection<NotificationSendingInstanceInfo> _m_NotificationSendingInformation;

        public SubCollection<NotificationSendingInstanceInfo> NotificationSendingInformation
        {
            get
            {
                if (_m_NotificationSendingInformation == null)
                {
                    _m_NotificationSendingInformation = new SubCollection<NotificationSendingInstanceInfo>();
                }
                return _m_NotificationSendingInformation;
            }
            set { _m_NotificationSendingInformation = value; }
        }
       

        //public PayMammothLog StatusLog { get; set; }

    }


    [CmsSectionDefaults(ShowInMainMenu = true)]
    public class NotificationMessageDataIndex : DocumentIndexCreationTask<NotificationMessageData>
    {
        public NotificationMessageDataIndex()
        {

        }

        public override string IndexName
        {
            get
            {
                return typeof (NotificationMessageDataIndex).Name;
            }
        }

        protected override void createIndex()
        {
            // index definition goes here


            this.Map = (items => items.Select(item => new
            {
                item.NextRetryOn,
                item.Status,

                //index fields go here
                CommonMetaData_CreatedOn = item.CommonMetaData.CreatedOn,
                CommonMetaData_Published = item.CommonMetaData.Published,
                CommonMetaData_PublishedOn = item.CommonMetaData.PublishedOn,
                CommonMetaData_PublishedUntil = item.CommonMetaData.PublishedUntil,
                CommonMetaData_Deleted = item.CommonMetaData.Deleted,
                CommonMetaData_SortPriority = item.CommonMetaData.SortPriority,
                CommonMetaData_IsTemporary = item.CommonMetaData.IsTemporary,
                CommonMetaData_LastEditedOn = item.CommonMetaData.LastEditedOn


            }));

        }

    }

}
