﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.Cms.DefaultMetadataValues;
using BusinessLogic_CS_v6.Framework.DbObjects.Objects;
using BusinessLogic_CS_v6.Framework.RavenDb.Indexes;

namespace PayMammoth_v6.Modules.Data.TestData
{

    /// <summary>
    /// These are used to store information about actual Test payments done on PayMammoth
    /// </summary>
    public class TestPaymentData : BaseObject
    {
        public TestPaymentData()
        {
            this.Status = TestPaymentStatus.Pending;
        }

        public enum TestPaymentStatus
        {
            Paid,
            Pending
        }

        public DateTimeOffset DateCreated { get; set; }
        

        public decimal Total { get; set; }
        public string Title { get; set; }
        public TestPaymentStatus Status { get; set; }
        public DateTimeOffset? MarkedAsPaidOn { get; set; }

        public string StatusLog { get; set; }


    }


    [CmsSectionDefaults(ShowInMainMenu = true)]
    public class TestPaymentDataIndex : DocumentIndexCreationTask<TestPaymentData>
    {
        public TestPaymentDataIndex()
        {

        }

        public override string IndexName
        {
            get
            {
                return typeof (TestPaymentDataIndex).Name;
            }
        }

        protected override void createIndex()
        {
            // index definition goes here


            this.Map = (items => items.Select(item => new
            {
                //index fields go here
                CommonMetaData_CreatedOn = item.CommonMetaData.CreatedOn,
                CommonMetaData_Published = item.CommonMetaData.Published,
                CommonMetaData_PublishedOn = item.CommonMetaData.PublishedOn,
                CommonMetaData_PublishedUntil = item.CommonMetaData.PublishedUntil,
                CommonMetaData_Deleted = item.CommonMetaData.Deleted,
                CommonMetaData_SortPriority = item.CommonMetaData.SortPriority,
                CommonMetaData_IsTemporary = item.CommonMetaData.IsTemporary,
                CommonMetaData_LastEditedOn = item.CommonMetaData.LastEditedOn


            }));

        }

    }
}
