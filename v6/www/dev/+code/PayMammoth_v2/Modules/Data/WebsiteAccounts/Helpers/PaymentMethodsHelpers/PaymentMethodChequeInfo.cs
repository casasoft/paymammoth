﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6.Modules.Data.WebsiteAccounts.Helpers.PaymentMethodsHelpers
{
    [Serializable]
    public class PaymentMethodChequeInfo
    {
        public PaymentMethodChequeAddressInfo Address { get; set; }
        public bool Enabled { get; set; }
        public string PayableTo { get; set; }

    }
}
