﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6.Modules.Data.WebsiteAccounts.Helpers
{
    [Serializable]
    public class WebsiteAccountContactDetailsInfo
    {
        public string ContactEmail { get; set; }
        public string ContactName { get; set; }
    }
}
