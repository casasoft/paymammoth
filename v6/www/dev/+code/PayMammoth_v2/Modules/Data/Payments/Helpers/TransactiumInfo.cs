﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.Wrappers;

namespace PayMammoth_v6.Modules.Data.Payments.Helpers
{
    public class TransactiumInfo
    {
        public TransactiumInfo()
        {
            this.PaymentStatuses = new List<MyHpsPaymentInfo>();
        }

        public class MyHpsPaymentInfo
        {
           
            public string Title { get; set; }
            public MyHPSPayment PaymentInfo { get; set; }

            public MyHpsPaymentInfo(string title, MyHPSPayment payment)
            {
                this.Title = title;
                this.PaymentInfo = payment;
            }
        }

        public string HpsId { get; set; }
        public List<MyHpsPaymentInfo> PaymentStatuses { get; set; }

    }
}
