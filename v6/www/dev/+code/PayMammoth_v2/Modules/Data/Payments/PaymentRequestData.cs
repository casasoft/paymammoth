﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.Cms.DefaultMetadataValues;
using BusinessLogic_CS_v6.Framework.DbObjects.Collections;
using BusinessLogic_CS_v6.Framework.DbObjects.Objects;
using BusinessLogic_CS_v6.Framework.DbObjects.Objects.PrimaryKeys;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Framework.RavenDb.Indexes;
using BusinessLogic_CS_v6.Util;
using PayMammoth_v6.Modules.Data.Notifications;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Data._Shared;
using PayMammoth_v6.Connector.InitialRequests;

namespace PayMammoth_v6.Modules.Data.Payments
{
    [CmsSectionDefaults(ShowInMainMenu = true)]
    
    public class PaymentRequestData : BaseObject, IPrimaryKeyAsGuid
    {
        

        public PaymentRequestData()
        {
            SetIdPrependedWithDocumentType(CS.General_CS_v6.Util.RandomUtil.GetGuid(removeDashes: true));
            
            this.PaymentStatus = new PaymentStatusInfo();
            this.StatusLog = new PayMammothLog();

        }

        private InitialRequestInfo _m_InitialRequestInfo;

        public InitialRequestInfo RequestDetails
        {
            get
            {
                if (_m_InitialRequestInfo == null)
                {
                    _m_InitialRequestInfo = new InitialRequestInfo();
                }
                return _m_InitialRequestInfo;
            }
            set { _m_InitialRequestInfo = value; }
        }


        private PaymentStatusInfo _m_PaymentStatusInfo;

        public PaymentStatusInfo PaymentStatus
        {
            get
            {
                if (_m_PaymentStatusInfo == null)
                {
                    _m_PaymentStatusInfo = new PaymentStatusInfo();
                }
                return _m_PaymentStatusInfo;
            }
            set { _m_PaymentStatusInfo = value; }
        }
       


        /// <summary>
        /// This is the IP from where the request came.  This is usually the server IP, and not the user's client IP address.
        /// </summary>
        public string RequestServerIp { get; set; }

        [CmsFieldDefaults(ShowInListing = true,ListingSortPriority = 100, 
            DefaultSortable = BusinessLogic_CS_v6.EnumsBL.CmsEnums_BL.CmsFieldDefaultSortable.DefaultSortableDescending)]
        public DateTimeOffset RequestDateTime { get; set; }
        public bool RequiresManualIntervention { get; set; }
        public ReferenceLink<WebsiteAccountData> LinkedWebsiteAccountId { get; set; }

        private PayMammothLog _m_StatusLog;

        public PayMammothLog StatusLog
        {
            get
            {
                if (_m_StatusLog == null)
                {
                    _m_StatusLog = new PayMammothLog();
                }
                return _m_StatusLog;
            }
            set { _m_StatusLog = value; }
        }



    }


    public class PaymentRequestDataIndex : DocumentIndexCreationTask<PaymentRequestData>
    {
        public PaymentRequestDataIndex()
        {

        }

        public override string IndexName
        {
            get
            {
                return typeof (PaymentRequestDataIndex).Name;
            }
        }

        protected override void createIndex()
        {
            Map = items => from item in items
                           select new 
                           {
                               CommonMetaData_CreatedOn = item.CommonMetaData.CreatedOn,
                               CommonMetaData_Published = item.CommonMetaData.Published,
                               CommonMetaData_PublishedOn = item.CommonMetaData.PublishedOn,
                               CommonMetaData_PublishedUntil = item.CommonMetaData.PublishedUntil,
                               CommonMetaData_Deleted = item.CommonMetaData.Deleted,
                               CommonMetaData_SortPriority = item.CommonMetaData.SortPriority,
                               CommonMetaData_IsTemporary = item.CommonMetaData.IsTemporary,
                               CommonMetaData_LastEditedOn = item.CommonMetaData.LastEditedOn,

                               PaymentStatus_Status = item.PaymentStatus.Status,
                               RequestDetails_ExpirationInfo_ExpiresAt = item.RequestDetails.ExpirationInfo.ExpiresAt,
                           };
        }

    }
}
