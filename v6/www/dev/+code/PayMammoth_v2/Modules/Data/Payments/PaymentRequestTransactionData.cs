﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.Cms.DefaultMetadataValues;
using BusinessLogic_CS_v6.Framework.DbObjects.Objects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Framework.RavenDb.Indexes;
using BusinessLogic_CS_v6.Util;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Data._Shared;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Modules.Data.Payments
{

    public class PaymentRequestTransactionData : BaseObject
    {
        public PaymentRequestTransactionData()
        {
           // this.Id = RavenDbUtil.GetDocumentIdForType<PaymentRequestTransactionData>(CS.General_CS_v6.Util.RandomUtil.GetGuid(removeDashes: true));
        }

        public string AuthCode { get; set; }

        public string Comments { get; set; }
     
        public DateTimeOffset DateStart { get; set; }
        public DateTimeOffset DateLastActivity { get; set; }
        public DateTimeOffset DateFinished { get; set; }
        public string IpAddress { get; set; }
        public EnumsPayMammothConnector.PaymentMethodType PaymentMethod { get; set; }
        public string PaymentParameters { get; set; }
        public string PaymentMethodVersion { get; set; }
        public string ExternalReference { get; set; }
        public bool RequiresManualIntervention { get; set; }
        public bool IsSuccessful { get; set; }
        public bool IsFakePayment { get; set; }
        public string FakePaymentKeyUsed { get; set; }

        public ReferenceLink<PaymentRequestData> PaymentRequestId { get; set; }


        //private PayMammothLog _m_TransactionLog;

        ////todo: [For: Karl | 2014/01/08] Update log to be a seperate class, so that it can be saved without overwriting other documents (WrittenBy: Karl)        			
        //public PayMammothLog TransactionLog
        //{
        //    get
        //    {
        //        if (_m_TransactionLog == null)
        //        {
        //            _m_TransactionLog = new PayMammothLog();
        //        }
        //        return _m_TransactionLog;
        //    }
        //    set { _m_TransactionLog = value; }
        //}

        private PaymentGatewaySpecificInfo _m_PaymentGatewaySpecificInfo;

        public PaymentGatewaySpecificInfo PaymentGatewaySpecificInfo
        {
            get
            {
                if (_m_PaymentGatewaySpecificInfo == null)
                {
                    _m_PaymentGatewaySpecificInfo = new PaymentGatewaySpecificInfo();
                }
                return _m_PaymentGatewaySpecificInfo;
            }
            set { _m_PaymentGatewaySpecificInfo = value; }
        }
        

    }


    [CmsSectionDefaults(ShowInMainMenu = true)]
    public class PaymentRequestTransactionDataIndex : DocumentIndexCreationTask<PaymentRequestTransactionData>
    {
        public PaymentRequestTransactionDataIndex()
        {

        }

        public override string IndexName
        {
            get
            {
                return typeof (PaymentRequestTransactionDataIndex).Name;
            }
        }

        protected override void createIndex()
        {
            // index definition goes here


            this.Map = (items => items.Select(item => new
            {
                //index fields go here
                CommonMetaData_CreatedOn = item.CommonMetaData.CreatedOn,
                CommonMetaData_Published = item.CommonMetaData.Published,
                CommonMetaData_PublishedOn = item.CommonMetaData.PublishedOn,
                CommonMetaData_PublishedUntil = item.CommonMetaData.PublishedUntil,
                CommonMetaData_Deleted = item.CommonMetaData.Deleted,
                CommonMetaData_SortPriority = item.CommonMetaData.SortPriority,
                CommonMetaData_IsTemporary = item.CommonMetaData.IsTemporary,
                CommonMetaData_LastEditedOn = item.CommonMetaData.LastEditedOn


            }));

        }

    }

}
