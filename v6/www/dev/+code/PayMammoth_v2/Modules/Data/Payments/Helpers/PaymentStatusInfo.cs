﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.Cms.DefaultMetadataValues;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Modules.Data.Payments.Helpers
{
    public class PaymentStatusInfo
    {

        public PaymentStatusInfo()
        {
            this.Status = PaymentStatus.Pending;
        }

        public enum PaymentStatus
        {
            Pending, Paid, Expired, Cancelled
        }

        [CmsFieldDefaults(ShowInListing = true, ListingSortPriority = 200,
               DefaultSortable = CmsEnums_BL.CmsFieldDefaultSortable.DefaultSortableDescending)]
        public PaymentStatus Status { get; set; }

        [CmsFieldDefaults(ShowInListing = true, ListingSortPriority = 300,
            DefaultSortable = CmsEnums_BL.CmsFieldDefaultSortable.DefaultSortableDescending)]
        public DateTimeOffset? PaidOn { get; set; }
        [CmsFieldDefaults(ShowInListing = true, ListingSortPriority = 300,
            DefaultSortable = CmsEnums_BL.CmsFieldDefaultSortable.DefaultSortableDescending)]
        public DateTimeOffset? ExpiredOn { get; set; }
        [CmsFieldDefaults(ShowInListing = true, ListingSortPriority = 300,
            DefaultSortable = CmsEnums_BL.CmsFieldDefaultSortable.DefaultSortableDescending)]
        public DateTimeOffset? CancelledOn { get; set; }

        public EnumsPayMammothConnector.PaymentMethodType PaymentMethodUsed { get; set; }
        public ReferenceLink<PaymentRequestTransactionData> SuccessfulTransactionId { get; set; }

    }
}
