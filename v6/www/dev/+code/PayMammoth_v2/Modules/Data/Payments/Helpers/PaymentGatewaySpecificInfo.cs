﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6.Modules.Data.Payments.Helpers
{
    public class PaymentGatewaySpecificInfo
    {
        public PayPalTransactionInfo PayPalTransactionInfo { get; set; }
        public TransactiumInfo TransactiumInfo { get; set; }
    }
}
