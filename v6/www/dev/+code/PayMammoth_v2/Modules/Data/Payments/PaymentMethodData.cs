﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.Cms.DefaultMetadataValues;
using BusinessLogic_CS_v6.Framework.DbObjects.Objects;
using BusinessLogic_CS_v6.Framework.MediaItems;
using BusinessLogic_CS_v6.Framework.MediaItems.Attributes;
using BusinessLogic_CS_v6.Framework.Multilingual;
using BusinessLogic_CS_v6.Framework.RavenDb.Indexes;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Modules.Data.Payments
{
     [CmsSectionDefaults(ShowInMainMenu = true)]
    
    public class PaymentMethodData : BaseObject
    {
        public PaymentMethodData()
        {

        }

        private MultilingualValue<string> _Title = new MultilingualValue<string>();

         [CmsFieldDefaults(ShowInListing = true, Searchable = true)]

        public MultilingualValue<string> Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private MultilingualValue<string> _Description = new MultilingualValue<string>();

        public MultilingualValue<string> Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [CmsFieldDefaults(ShowInListing = true, Searchable = true)]
        public EnumsPayMammothConnector.PaymentMethodType PaymentMethod { get; set; }


        #region PaymentMethodData.Icon (Media Item)


        public enum PaymentMethodDataIconImageSize
        {
            

            [SpecificSizeDefaults(Width = 110, Height = 70,
                CropIdentifier = CS.General_CS_v6.Enums.ImageCropIdentifier.NoCrop,
                ImageFormat = CS.General_CS_v6.Enums.ImageType.PNG)]
            Normal

        }

        public class PaymentMethodDataIcon : MediaItem<PaymentMethodDataIconImageSize>
        {
            public PaymentMethodDataIcon() : base()
            {
                var p = this.Parameters;

                p.Identifier = "PaymentMethodData.Icon";
                p.UploadFolder = "/PaymentMethodData/Icon/";
                    // "/" means relative to the base folder, which by default is /uploads/.  Must end with a slash

                p.ImageDefaults.MaximumWidth = 2048;
                p.ImageDefaults.MaximumHeight = 2048;

            }
        }

        public PaymentMethodDataIcon Icon { get; set; }

        #endregion


    }


   public class PaymentMethodDataIndex : DocumentIndexCreationTask<PaymentMethodData>
    {
        public PaymentMethodDataIndex()
        {

        }

        public override string IndexName
        {
            get
            {
                return typeof (PaymentMethodDataIndex).Name;
            }
        }

        protected override void createIndex()
        {
            // index definition goes here


            this.Map = (items => items.Select(item => new
            {
                item.Title,
                item.PaymentMethod,
                //index fields go here
                CommonMetaData_CreatedOn = item.CommonMetaData.CreatedOn,
                CommonMetaData_Published = item.CommonMetaData.Published,
                CommonMetaData_PublishedOn = item.CommonMetaData.PublishedOn,
                CommonMetaData_PublishedUntil = item.CommonMetaData.PublishedUntil,
                CommonMetaData_Deleted = item.CommonMetaData.Deleted,
                CommonMetaData_SortPriority = item.CommonMetaData.SortPriority,
                CommonMetaData_IsTemporary = item.CommonMetaData.IsTemporary,
                CommonMetaData_LastEditedOn = item.CommonMetaData.LastEditedOn


            }));

        }

    }
}
