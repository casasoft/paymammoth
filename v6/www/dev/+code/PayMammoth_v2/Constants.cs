﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6
{
    public static class Constants
    {

        public static class TestPaymentsConstants
        {
            public const string QuerystringVar_TestPaymentId = "TestPaymentId";
            public const string TestPaymentSuccessUrl = "/_tests/testPaymentSuccess.aspx";
            public const string TestPaymentCancelUrl = "/_tests/testPaymentCancel.aspx";

        }

        public static class PayMammoth_v6Constants
        {
            public const string QuerystringVar_CancelUrl = "cancelUrl";
            public const string QuerystringVar_Transactium_RedirectUrl = "RedirectURL";
            public const string QuerystringVar_PaymentMethod = "paymentMethod";
            public const string PayMammoth_v6LayoutViewDataKey = "PayMammoth_v6LayoutViewData";
            public const string QuerystringVar_Confirm = "confirm";
            public const string QuerystringVar_Token = "token";
            public const string QuerystringVar_PayerId = "PayerID";
            
            public const string QuerystringVar_FakePaymentId = "fakePaymentKey";
        }

        public static string TestPayMammothLocalDomain = "http://office.casasoft.com.mt/";
        public static string TestPayMammothAzureDomain = "http://paymammoth.cloudapp.net/";
        public static string TestNotificationHandler = "_tests/paymammothtesthandler.ashx";

        private static List<int> _retryCounts = new List<int>();

        private static void initRetryCounts()
        {
            var list = _retryCounts;
            list.Add(5);
            list.Add(5);
            list.Add(5);
            list.Add(15);
            list.Add(30);
            list.Add(60);
            list.Add(120);
            list.Add(200);
            list.Add(300);
            list.Add(300);
            list.Add(300);
            list.Add(300);
            list.Add(600);
            list.Add(600);
            list.Add(600);
            list.Add(600);
            list.Add(600);
            list.Add(1200);
            list.Add(1200);
            list.Add(1200);
            list.Add(1200);
            list.Add(1200);
            list.Add(2400);
            list.Add(2400);
            list.Add(2400);
            list.Add(4800);
            list.Add(4800);
            list.Add(4800);
            list.Add(9600);
            list.Add(9600);
            list.Add(9600);
            list.Add(19200);
            list.Add(19200);
            list.Add(38400);
            list.Add(38400);
        }

        static Constants()
        {
            initRetryCounts();
            
        }

        /// <summary>
        /// Returns the list of interval try counts in seconds
        /// </summary>
        /// <returns></returns>
        public static List<int> GetPaymentNotificationsRetryCounts()
        {
            return _retryCounts;


        }
        public const string PARAM_SUCCESS = "success";
    }
}
