﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6.PaymentsIntegration.Transactium.Exceptions
{
    public class TransactiumValidationException : Exception
    {
        public TransactiumValidationException(string msg) : base(msg)
        {

        }
    }
}
