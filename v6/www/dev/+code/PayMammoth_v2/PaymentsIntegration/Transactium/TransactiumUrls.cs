﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.General_CS_v6.Modules.Urls;
using CS.General_CS_v6.Util;

namespace PayMammoth_v6.PaymentsIntegration.Transactium
{
    public class TransactiumUrls
    {



        //public static string PaymentStatusUrl { get { return PageUtil.GetApplicationBaseUrl() + "payment/transactium/v1/gatewayResult.aspx"; } }
        //public static string PaymentPage { get { return PageUtil.GetApplicationBaseUrl() + "payment/transactium/v1/"; } }
        //public static string RedirectIFrameUrl { get { return PageUtil.GetApplicationBaseUrl() + "payment/transactium/v1/redirectIFrame.aspx"; } }
        public static string PaymentStatusUrl { get { return PageUtil.GetApplicationBaseUrl() + "_tests/transactium/status.aspx"; } }
        public static string PaymentPage { get { return PageUtil.GetApplicationBaseUrl() + "/_tests/transactium/transactiumpayment.aspx"; } }
        public static string RedirectIFrameUrl { get { return PageUtil.GetApplicationBaseUrl() + "_tests/transactium/status.aspx"; } }

        public static string GetSuccessUrl(string transactionId)
        {
            UrlClass url = new UrlClass(RedirectIFrameUrl);
            url[PayMammoth_v6.Connector.Constants.PARAM_TransactionId] = transactionId;
            url[Constants.PARAM_SUCCESS] = "Y";
            return url.ToString();
        }
        public static string GetFailureUrl(string transactionId)
        {
            UrlClass url = new UrlClass(RedirectIFrameUrl);
            url[PayMammoth_v6.Connector.Constants.PARAM_TransactionId] = transactionId;
            url[Constants.PARAM_SUCCESS] = "N";
            return url.ToString();
        }

        public static string GetPaymentStatusUrl(string transactionId)
        {
            UrlClass url = new UrlClass(PaymentStatusUrl);
            url[PayMammoth_v6.Connector.Constants.PARAM_TransactionId] = transactionId;
            return url.ToString();
        }

        public static string GetPaymentPageUrl(string transactionId, string urlToRedirectTo)
        {
            UrlClass url = new UrlClass(PaymentPage);
            url[PayMammoth_v6.Connector.Constants.PARAM_TransactionId] = transactionId;
            url["RedirectURL"] = urlToRedirectTo;
            return url.ToString();

        }


    }
}
