﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.General_CS_v6.Extensions;
using PayMammoth_v6.PaymentsIntegration.Transactium.Exceptions;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.Wrappers;

namespace PayMammoth_v6.PaymentsIntegration.Transactium.v1.Services
{
    using BusinessLogic_CS_v6.Aspects;
    using BusinessLogic_CS_v6.Aspects.NullAspects;
    using CS.General_CS_v6.Modules.InversionOfControl;

    public interface ITransactiumValidationService
    {
        void ValidateTransactiumRequest(MyHPSCreatePaymentRequest transactiumRequest);

    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class TransactiumValidationService : ITransactiumValidationService
    {
        public TransactiumValidationService()
        {

        }

        public void ValidateTransactiumRequest(MyHPSCreatePaymentRequest transactiumRequest)
        {
            StringBuilder sbErrors = new StringBuilder();

            if (transactiumRequest.SuccessURL.IsNotNullOrWhitespace() &&
                !Uri.IsWellFormedUriString(transactiumRequest.SuccessURL, UriKind.Absolute))
            {
                sbErrors.AppendLine("SuccessUrl needs to be a valid full URL (not relative");
            }

            if (transactiumRequest.FailURL.IsNotNullOrWhitespace() &&
                !Uri.IsWellFormedUriString(transactiumRequest.FailURL, UriKind.Absolute))
            {
                sbErrors.AppendLine("FailURL needs to be a valid full URL (not relative");
            }
            if (sbErrors.Length > 0)
            {
                throw new TransactiumValidationException(sbErrors.ToString());
            }
            
        }
    }

}
