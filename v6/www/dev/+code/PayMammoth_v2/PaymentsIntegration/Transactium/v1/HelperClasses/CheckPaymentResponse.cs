﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6.PaymentsIntegration.Transactium.v1.HelperClasses
{
    public class CheckPaymentResponse
    {
        public enum CheckPaymentStatusType
        {
            Success,
            Error,
            Cancelled,
            InvalidCardDetails
        }

        public CheckPaymentStatusType Result { get; set; }
        public string SuccessRedirectUrl { get; set; }

        //public EnumsTransactium.CHECK_PAYMENT_RESPONSE Status { get; set; }
       // public PaymentStatus Status { get; set; }
      
        public CheckPaymentResponse()
        {
            
        }
    }
}
