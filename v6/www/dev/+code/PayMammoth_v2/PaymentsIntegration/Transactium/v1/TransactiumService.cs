﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using NLog;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.Logging;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.HelperClasses;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.Services;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.Wrappers;
using Raven.Client;

namespace PayMammoth_v6.PaymentsIntegration.Transactium.v1
{
    public interface ITransactiumService
    {
        CheckPaymentResponse CheckTransactiumPayment(
            ReferenceLink<PaymentRequestTransactionData> transactionId,
            string hpsId);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class TransactiumService : ITransactiumService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IPayMammothLogService _payMammothLogService;
        private readonly IGetGatewayClientForWebsiteAccountService _getGatewayClientForWebsiteAccountService;
        private readonly IPaymentRequestTransactionDataService _paymentRequestTransactionDataService;

        public TransactiumService(
            IPayMammothLogService payMammothLogService,
            IGetGatewayClientForWebsiteAccountService getGatewayClientForWebsiteAccountService,
            IPaymentRequestTransactionDataService paymentRequestTransactionDataService)
        {
            _paymentRequestTransactionDataService = paymentRequestTransactionDataService;
            _getGatewayClientForWebsiteAccountService = getGatewayClientForWebsiteAccountService;
            _payMammothLogService = payMammothLogService;
        }

        [RavenTaskAspect_v2]
        public CheckPaymentResponse CheckTransactiumPayment(
            ReferenceLink<PaymentRequestTransactionData> transactionId,
            string hpsId)
        {
            //PAYMFIVE-55

            var session = RavenTaskAspect_v2.GetCurrentSession();

            CheckPaymentResponse result = new CheckPaymentResponse();
            result.Result = CheckPaymentResponse.CheckPaymentStatusType.Error;
            
            GetByIdParams<PaymentRequestTransactionData> getParams = new GetByIdParams<PaymentRequestTransactionData>(transactionId);
            getParams.ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No;
            getParams.AddInclude(x => x.PaymentRequestId);
            var transaction = session.GetById(getParams);
            var request = session.GetById(new GetByIdParams<PaymentRequestData>(transaction.PaymentRequestId));
            var websiteAccount = session.GetById(new GetByIdParams<WebsiteAccountData>(request.LinkedWebsiteAccountId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});
            //##########

            if (!transaction.IsSuccessful)
            {
                var gateway = _getGatewayClientForWebsiteAccountService.GetGatewayClientForWebsiteAccount(websiteAccount);
                var paymentStatus = gateway.CheckPaymentStatus(hpsId);

                if (paymentStatus != null)
                {
                    //transaction.AddDetailsFromTransactiumPayment(payment, true);
                    //transaction.MarkAsNotTemporary(false);
                    transaction.PaymentGatewaySpecificInfo.TransactiumInfo.PaymentStatuses.Add(
                        new TransactiumInfo.MyHpsPaymentInfo(
                            "CheckPayment",
                            paymentStatus));

                    if (paymentStatus.Status == EnumsTransactium.HPStatus.Completed && paymentStatus.ProceedWithPurchase)
                    {
                        _payMammothLogService.AddLogEntry(
                            request,
                            "[Transactium] Success.",
                            _log,
                            GenericEnums.NlogLogLevel.Info,
                            relatedTransaction:transaction);
                        //sucess
                        string authCode = paymentStatus.GetAuthCode();
                        result.Result = CheckPaymentResponse.CheckPaymentStatusType.Success;
                        result.SuccessRedirectUrl = request.RequestDetails.ReturnUrls.SuccessUrl;
                        _paymentRequestTransactionDataService.MarkTransactionAsPaid(
                                            transaction,
                                            requiresManualIntervention: false,
                                            authorisationCode: authCode,
                                            session: session);
                    }
                    else
                    {
                        if (paymentStatus.Status == EnumsTransactium.HPStatus.Cancelled)
                        {
                            result.Result = CheckPaymentResponse.CheckPaymentStatusType.Cancelled;
                            _payMammothLogService.AddLogEntry(
                                                                request,
                                                                "[Transactium] Cancelled",
                                                                _log,
                                                                GenericEnums.NlogLogLevel.Info,
                            relatedTransaction: transaction);
                        }
                        else
                        {
                            result.Result = CheckPaymentResponse.CheckPaymentStatusType.InvalidCardDetails;

                            logInvalidPayment(paymentStatus,transaction,request);
                        }
                    }
                }
                else
                {
                    _payMammothLogService.AddLogEntry(
                                                        request,
                                                        "Could not find payment",
                                                        _log,
                                                        GenericEnums.NlogLogLevel.Error,
                                                            relatedTransaction: transaction);
                }
            }
            else
            {
                _payMammothLogService.AddLogEntry(
                                                    request,
                                                    "Transaction already marked as paid",
                                                    _log,
                                                    GenericEnums.NlogLogLevel.Info,
                                                            relatedTransaction: transaction);
            }
            return result;
        }

        private void logInvalidPayment(
            MyHPSPayment paymentStatus,
            PaymentRequestTransactionData transaction,
            PaymentRequestData request)
        {
            //check that HPS10 is catered well for it.

            if (_log.IsWarnEnabled)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Could not process card. Details below");
                sb.AppendLine();
                sb.AppendLine("Status: " + paymentStatus.Status);
                sb.AppendLine("Proceed with Purchase: " + paymentStatus.ProceedWithPurchase);
                sb.AppendLine();
                for (int i = 0; i < paymentStatus.Transactions.Count; i++)
                {
                    var t = paymentStatus.Transactions[i];
                    sb.AppendLine("Transaction Details #" + (i + 1));
                    sb.AppendLine("------------------");
                    sb.AppendLine(": " + t.FSBlockReason);
                    sb.AppendLine(": " + t.HostMessage);
                    sb.AppendLine(": " + t.HostCode);
                    sb.AppendLine(": " + t.HostAuthorisationCode);
                    sb.AppendLine(": " + t.TransactionResultStatus);
                    sb.AppendLine(": " + t.CVV2CheckResponse);
                    sb.AppendLine(": " + t.CVV2Supplied);
                    sb.AppendLine(": " + t.CardIdentifier);
                    sb.AppendLine(": " + t.CardNumber);
                    sb.AppendLine();
                }
                _payMammothLogService.AddLogEntry(request,"Could not process payment\r\n\r\n" + sb.ToString(), _log,
                                            GenericEnums.NlogLogLevel.Warn, relatedTransaction: transaction);
            }
        }
    }
}