﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Modules.Data._Shared.BaseObjectWithIdentifiers;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.PaymentsIntegration.Transactium.v1
{
    public class ConstantsTransactium
    {
        public const string HPSID = "HPSID";
        //public static string GetHpsIdFromQuerystring()
        //{
        //    return PageUtil.GetVariableFromQuerystring(HPSID);
        //}
        public static string GetLanguageCode(EnumsPayMammothConnector.SupportedLanguage language,
            string languageCountry)
        {
            string s = "en-GB";
            switch (language)
            {
                case EnumsPayMammothConnector.SupportedLanguage.English:
                    {
                        if (languageCountry == Iso3166_CountryData.UNITEDSTATES_3_LETTER_CODE)
                            s = "en-US";
                        else
                            s = "en-GB";
                        break;

                    }
                //case CS.General_CS_v6.Enums.LanguageCode_Iso639.Spanish: s = "es-ES"; break;
                //case CS.General_CS_v6.Enums.LanguageCode_Iso639.Italian: s = "it-IT"; break;
                //case CS.General_CS_v6.Enums.LanguageCode_Iso639.Maltese: s = "mt-MT"; break;
                //case CS.General_CS_v6.Enums.LanguageCode_Iso639.Portuguese: s = "pt-BR"; break;
                //case CS.General_CS_v6.Enums.LanguageCode_Iso639.French: s = "fr-FR"; break;
                //case CS.General_CS_v6.Enums.LanguageCode_Iso639.German: s = "de-DE"; break;
                default:
                    s = "en-GB"; break;

            }
            return s;
        }
    }
}
