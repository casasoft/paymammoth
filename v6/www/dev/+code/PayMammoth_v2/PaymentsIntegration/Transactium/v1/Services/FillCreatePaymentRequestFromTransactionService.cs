﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Modules.Services._Shared.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Util;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.Wrappers;
using PayMammoth_v6.Presentation.Services.Payments;
using Raven.Client;

namespace PayMammoth_v6.PaymentsIntegration.Transactium.v1.Services
{
    using BusinessLogic_CS_v6.Aspects;
    using BusinessLogic_CS_v6.Aspects.NullAspects;
    using CS.General_CS_v6.Modules.InversionOfControl;

    public interface IFillCreatePaymentRequestFromTransactionService
    {
        MyHPSCreatePaymentRequest FillTransactiumCreatePaymentRequestFromTransaction(WebsiteAccountData websiteAccount, 
            PaymentRequestTransactionData transaction,
            PaymentRequestData request, IDocumentSession session);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class FillCreatePaymentRequestFromTransactionService : IFillCreatePaymentRequestFromTransactionService
    {
        
        private readonly IGetTotalPriceForRequestService _getTotalPriceForRequestService;
        private readonly IPaymentUrlModelService _paymentUrlModelService;
        private readonly IGetCultureModelForPaymentRequestService _getCultureModelForPaymentRequestService;
        private readonly IIso3166_CountryDataCache _iso3166CountryDataCache;
        private readonly ITransactiumValidationService _transactiumValidationService;

        public FillCreatePaymentRequestFromTransactionService(
            IGetTotalPriceForRequestService getTotalPriceForRequestService,
            IPaymentUrlModelService paymentUrlModelService,
            IGetCultureModelForPaymentRequestService getCultureModelForPaymentRequestService,
            IIso3166_CountryDataCache iso3166CountryDataCache,
            ITransactiumValidationService transactiumValidationService
            )
        {
            _transactiumValidationService = transactiumValidationService;
            _iso3166CountryDataCache = iso3166CountryDataCache;
            _getCultureModelForPaymentRequestService = getCultureModelForPaymentRequestService;
            _paymentUrlModelService = paymentUrlModelService;
            _getTotalPriceForRequestService = getTotalPriceForRequestService;
        }

        public MyHPSCreatePaymentRequest FillTransactiumCreatePaymentRequestFromTransaction(WebsiteAccountData websiteAccount,
            PaymentRequestTransactionData transaction, PaymentRequestData request, IDocumentSession session)
        {
            //todo: [For: Backend | 2014/03/28] implement unit-test (WrittenBy: Karl)        			

            MyHPSCreatePaymentRequest transactiumRequest = new MyHPSCreatePaymentRequest();
            decimal total = _getTotalPriceForRequestService.GetTotalPrice(request, true, true);
            int totalInCents = (int)Math.Round(total * 100);
            transactiumRequest.Amount = totalInCents;
            transactiumRequest.Currency = EnumsTransactium.GetCurrencyCodeFromString(request.RequestDetails.Pricing.CurrencyCode3Letter);
            transactiumRequest.PaymentType = EnumsTransactium.HPSPaymentType.Sale;

            var transactionIdOnly = BusinessLogic_CS_v6.Util.RavenDbUtil.GetRavenDbIdOnlyFromFullId(transaction.Id);
            var requestIdOnly = BusinessLogic_CS_v6.Util.RavenDbUtil.GetRavenDbIdOnlyFromFullId(request.Id);

            transactiumRequest.HPSProfileTag = websiteAccount.PaymentsInformation.Transactium.ProfileTag; //payment profile
            transactiumRequest.MaxTimeLimit = 0; //max time limit
            transactiumRequest.OrderReference = transactionIdOnly; //order reference
            transactiumRequest.ClientReference = null; //client reference

            
          //  transactiumRequest.ClientIPRestriction = CS.General_v3.Util.PageUtil.GetUserIP(); //customer IP


            var country = _iso3166CountryDataCache.GetCountryByCountryCode(request.RequestDetails.Details.ClientContactDetails.Country3LetterCode);
            EnumsTransactium.CountryCodes? transactiumCountryCode = null;
            if (country != null)
            {
                transactiumCountryCode = EnumsTransactium.GetCountryCodeFromISOAsTwoLetterCode(country);
            }

            transactiumRequest.ClientBillingCountry = transactiumCountryCode.GetValueOrDefault(EnumsTransactium.CountryCodes.NotSet);
            transactiumRequest.ClientEmail = request.RequestDetails.Details.ClientContactDetails.Email; //customer email address

            var cultureModel = _getCultureModelForPaymentRequestService.GetCultureModelForPaymentRequest(request);

            //string successUrl = TransactiumUrls.GetSuccessUrl(transactionIdOnly);
            //string failureUrl = TransactiumUrls.GetFailureUrl(transactionIdOnly);
            //string failureUrl = _paymentUrlModelService.GetPaymentSelectionPage(session, cultureModel, request.Id);
            //string paymentUrl = TransactiumUrls.GetPaymentStatusUrl(transactionIdOnly);

            var statusUrl = _paymentUrlModelService.GetTransactiumStatusPageUrl(session, cultureModel, requestIdOnly, transactionIdOnly); //URL to redirect in case of success

            transactiumRequest.SuccessURL = statusUrl;
            transactiumRequest.FailURL = statusUrl;

            //transactiumRequest.FailURL = failureUrl; //URL to redirect in case of error
            

            transactiumRequest.LanguageCode = ConstantsTransactium.GetLanguageCode(request.RequestDetails.Language,
                request.RequestDetails.Details.ClientContactDetails.Country3LetterCode);

            _transactiumValidationService.ValidateTransactiumRequest(transactiumRequest);

            return transactiumRequest;

            
        }
    }
}
