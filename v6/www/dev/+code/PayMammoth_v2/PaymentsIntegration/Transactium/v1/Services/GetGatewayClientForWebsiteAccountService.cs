﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.Wrappers;

namespace PayMammoth_v6.PaymentsIntegration.Transactium.v1.Services
{
    using BusinessLogic_CS_v6.Aspects;
    using BusinessLogic_CS_v6.Aspects.NullAspects;
    using CS.General_CS_v6.Modules.InversionOfControl;

    public interface IGetGatewayClientForWebsiteAccountService
    {
        GatewayClient GetGatewayClientForWebsiteAccount(WebsiteAccountData websiteAccount);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class GetGatewayClientForWebsiteAccountService : IGetGatewayClientForWebsiteAccountService
    {
        public GetGatewayClientForWebsiteAccountService()
        {

        }

        public GatewayClient GetGatewayClientForWebsiteAccount(WebsiteAccountData websiteAccount)
        {
            //todo: [For: Backend | 2014/03/28] create unit tests for this (WrittenBy: YourName)        			

            var transactiumInfo = websiteAccount.PaymentsInformation.Transactium;
            GatewayClient client = new GatewayClient(!transactiumInfo.UseLiveEnvironment,
               transactiumInfo.LiveAccount.Username,
               transactiumInfo.LiveAccount.Password,
               transactiumInfo.StagingAccount.Username,
               transactiumInfo.StagingAccount.Password);
            return client;
            
        }
    }
}
