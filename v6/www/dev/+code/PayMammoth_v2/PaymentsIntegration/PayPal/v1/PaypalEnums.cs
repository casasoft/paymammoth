﻿using System;
using System.ComponentModel;
using BusinessLogic_CS_v6.Modules.Data._Shared.BaseObjectWithIdentifiers;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1
{
    public static class PayPalEnums
    {
        public enum CONFIRM_RESULT
        {
            OK,
            Error

        }

        public enum ERROR_CODE
        {

        }
        public enum CHECKOUT_STATUS
        {
            PaymentActionNotInitiated,
            PaymentActionFailed,
            PaymentActionInProgress,
            PaymentCompleted
        }

        public enum BILLING_TYPES
        {
            RecurringPayments
        }

        public enum RECURRING_AUTO_BILL_OUTSTANDING_AMOUNT
        {
            /// <summary>
            /// Payment gateway does not automatically bill the outstanding balance.
            /// </summary>
            NoAutoBill,
            /// <summary>
            /// Payment gateway automatically bills the outstanding balance.
            /// </summary>
            AddToNextBilling
        }

        public enum RECURRING_ITEMCATEGORY
        {
            Digital,
            Physical
        }

        public enum RECURRING_PAYPAL_SETUP_FAILED_INITIAL_AMOUNT_ACTION
        {
            /// <summary>
            /// If the initial payment amount fails, PayPal adds the failed payment amount to the outstanding balance for this recurring payment profile.
            /// </summary>
            ContinueOnFailure,
            /// <summary>
            /// PayPal creates the recurring payment profile, but places it into a pending status until the initial payment completes. If the initial payment clears, PayPal notifies you by IPN that the pending profile has been activated. If the payment fails, PayPal notifies you by IPN that the pending profile has been canceled.
            /// </summary>
            CancelOnFailure
        }

        public enum PAYPAL_PAYER_STATUS
        {
            Verified,
            Unverified
        }

        public enum CARD_TYPES
        {
            Visa,
            MasterCard,
            Discover,
            Amex,
            /// <summary>
            /// For Maestro you must set CURRENCYCODE to GBP and in addition, you must specify either STARTDATE or ISSUENUMBER.
            /// </summary>
            Maestro
        }

        public enum PAYMENT_STATUS
        {
            None,
            [Description("Canceled-Reversal")]
            Cancelled_Reversal,
            Completed,
            Denied,
            Expired,
            Failed,
            [Description("In-Progress")]
            In_Progress,
            [Description("Partially-Refunded")]
            Partially_Refunded,
            Pending,
            Refunded,
            Reversed,
            Processed,
            Voided
        }
        public static bool CheckIfPaymentStatusIsSuccess(PAYMENT_STATUS status)
        {
            if (status == PAYMENT_STATUS.Completed || status == PAYMENT_STATUS.Pending)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool CheckIfPaymentStatusRequiresManualIntervention(PAYMENT_STATUS status)
        {
            if (status == PAYMENT_STATUS.Pending)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum PENDING_REASON
        {
            None,
            Address,
            Authorization,
            ECheck,
            [Description("intl")]
            International,
            /// <summary>
            /// You do not have a balance in the currency sent, and you do not have your Payment Receiving Preferences set to automatically convert and accept this payment. You must manually accept or deny this payment.
            /// </summary>
            [Description("multi-currency")]
            MultiCurrency,
            Order,
            PaymentReview,
            Unilateral,
            Verify,
            Other
        }
        public enum ACKNOWLEDGEMENT
        {
            [Description("success")]
            Success,
            [Description("successwithwarning")]
            SuccessWithWarning,
            [Description("failure")]
            Failure,
            [Description("failurewithwarning")]
            FailureWithWarning

        }
        /*



        public static ACKNOWLEDGEMENT AcknowledgementFromPaypalCode(string code)
        {
            if (code == null) code = "";
            code = code.ToLower();
            switch (code)
            {
                case "success": return ACKNOWLEDGEMENT.Success;
                case "successwithwarning": return ACKNOWLEDGEMENT.SuccessWithWarning;
                case "failure": return ACKNOWLEDGEMENT.Failure;
                case "failurewithwarning": return ACKNOWLEDGEMENT.FailureWithWarning;
            }
            return ACKNOWLEDGEMENT.Failure;
        }*/
        public enum ADDRESS_STATUS
        {
            [Description("Confirmed")]
            Confirmed,
            [Description("Unconfirmed")]
            UnConfirmed,
            [Description("None")]
            None
        }

        public enum SOLUTION_TYPE
        {
            [Description("Sole")]
            PaypalAccountOptional,
            [Description("Mark")]
            PaypalAccountRequired
        }

        public enum CHANNEL_TYPE
        {
            [Description("Merchant")]
            Merchant,
            [Description("eBayItem")]

            EBayItem
        }
       

        public enum LANDING_PAGE_TYPE
        {
            [Description("Billing")]
            Billing,
            [Description("Login")]
            Login
        }

        public enum LANGUAGE_PAYPAL
        {
            [Description("AU")]
            Australian,
            [Description("AT")]
            Austrian,
            [Description("BE")]
            Belgium,
            [Description("BR")]
            Brazilian,
            [Description("CA")]
            Canadian,
            [Description("CH")]
            Swiss,
            [Description("CN")]
            Chinese,
            [Description("DE")]
            German,
            [Description("ES")]
            Spanish,
            [Description("GB")]
            EnglishUK,
            [Description("FR")]
            French,
            [Description("IT")]
            Italian,
            [Description("NL")]
            Dutch,
            [Description("PL")]
            Polish,
            [Description("PT")]
            Portuguese,
            [Description("RU")]
            Russian,

            [Description("US")]
            EnglishUS,
            [Description("da_DK")]
            DanishDenmark,

            [Description("he_IL")]
            Hebrew,

            [Description("id_ID")]
            Indonesian,

            [Description("jp_JP")]
            Japanese,

            [Description("no_NO")]
            Norwegian,

            [Description("pt_BR")]
            BrazilianPortuguese,

            [Description("ru_RU")]
            RussianLithuaniaLatviaUkraine,

            [Description("sv_SE")]
            Swedish,

            [Description("th_TH")]
            Thai,

            [Description("tr_TR")]
            Turkish,

            [Description("zh_CN")]
            SimplifiedChineseChina,

            [Description("zh_HK")]
            TraditionalChineseHongKong,
            [Description("zh_TW")]
            TraditionalChineseTaiWan,
            

        }

        public enum PAYMENT_ACTION
        {
            [Description("Sale")]
            Sale,
            [Description("Authorization")]
            Authorization,
            [Description("Order")]
            Order
        }

        public static LANGUAGE_PAYPAL LanguageFromISO(EnumsPayMammothConnector.SupportedLanguage language, Iso3166_CountryData countryLanguage)
        {
            LANGUAGE_PAYPAL? result = null;
            if (countryLanguage != null)
            {
                switch(countryLanguage.Identifier)
                {
                    case Iso3166_CountryData.BELGIUM_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Belgium;break;
                    case Iso3166_CountryData.AUSTRALIA_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Australian; break;
                    case Iso3166_CountryData.CANADA_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Canadian; break;
                    case Iso3166_CountryData.BRAZIL_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Brazilian; break;
                    case Iso3166_CountryData.SWITZERLAND_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Swiss; break;
                    case Iso3166_CountryData.CHINA_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Chinese; break;

                    case Iso3166_CountryData.GERMANY_3_LETTER_CODE: result = LANGUAGE_PAYPAL.German; break;
                    case Iso3166_CountryData.SPAIN_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Spanish; break;
                    case Iso3166_CountryData.UNITEDKINGDOM_3_LETTER_CODE: result = LANGUAGE_PAYPAL.EnglishUK; break;
                    case Iso3166_CountryData.FRANCE_3_LETTER_CODE: result = LANGUAGE_PAYPAL.French; break;
                    case Iso3166_CountryData.ITALY_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Italian; break;
                    case Iso3166_CountryData.NETHERLANDS_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Dutch; break;
                    case Iso3166_CountryData.POLAND_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Polish; break;
                    case Iso3166_CountryData.PORTUGAL_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Portuguese; break;
                    case Iso3166_CountryData.RUSSIANFEDERATION_3_LETTER_CODE: result = LANGUAGE_PAYPAL.Russian; break;
                    case Iso3166_CountryData.UNITEDSTATES_3_LETTER_CODE: result = LANGUAGE_PAYPAL.EnglishUS; break;

                    case Iso3166_CountryData.HONGKONG_3_LETTER_CODE: result = LANGUAGE_PAYPAL.TraditionalChineseHongKong; break;
                    case Iso3166_CountryData.TAIWAN_3_LETTER_CODE: result = LANGUAGE_PAYPAL.TraditionalChineseTaiWan; break;

                    case Iso3166_CountryData.LATVIA_3_LETTER_CODE:
                    case Iso3166_CountryData.LITHUANIA_3_LETTER_CODE:
                    case Iso3166_CountryData.UKRAINE_3_LETTER_CODE: result = LANGUAGE_PAYPAL.RussianLithuaniaLatviaUkraine; break;

                }
            }
            if (!result.HasValue)
            {
                result = LANGUAGE_PAYPAL.EnglishUK; //default language
                switch (language)
                {
                    case EnumsPayMammothConnector.SupportedLanguage.English: result = LANGUAGE_PAYPAL.EnglishUK; break;
                    
                        
                        


                }
                
            }
            return result.Value;
            
        }

        public enum TRANSACTION_TYPE
        {
            [Description("adjustment")]
            Adjustment,

            [Description("cart")]
            Cart,
            [Description("express-checkout")]
            ExpressCheckout,
            [Description("masspay")]
            MassPay,
            [Description("mp_signup")]
            CreatedABillingAgreement,
            [Description("merch_pmt")]
            MonthlySubscriptionPaidForWebsitePaymentsPro,
            [Description("new_case")]
            NewDisputeFiled,
            /// <summary>
            /// For each successful payment, you receive an IPN with txn_type set to recurring_payment.
            /// </summary>
            [Description("recurring_payment")]
            RecurringPaymentReceived,
            /// <summary>
            /// hen the profile has "expired" (e.g., there are no more payments left on the profile, and the amount of time since the last payment plus the billing period has elapsed), you will get another 
            /// IPN with txn_type set to recurring_payment_expired.  This is intended to be a "reminder" to you that the buyer's subscription is up, and to deactivate their service. 
            /// </summary>
            [Description("recurring_payment_profile_cancel")]
            RecurringPaymentCancelled,
            [Description("recurring_payment_expired")]
            RecurringPaymentExpired,
            [Description("recurring_payment_profile_created")]
            RecurringPaymentProfileCreated,
            /// <summary>
            /// When you receive an IPN with txn_type set to recurring_payment_skipped, this means that, for some reason, 
            /// PayPal was not able to process the recurring payment.  This does not necessarily mean that the buyer's credit card 
            /// (or other funding source) was declined, but rather, it indicates that some other error occurred that prevented us from 
            /// processing the payment.  Because there are multiple reasons why this could happen, PayPal will make three attempts to
            ///  charge the buyer -- once after three days, and again five days after that. 
            ///  If the 3-day reattempt fails you will receive another IPN with txn_type set to recurring_payment_skipped.  
            /// </summary>
            [Description("recurring_payment_skipped")]
            RecurringPaymentSkipped,

            /// <summary>
            /// For each unsuccessful payment, you receive an IPN with txn_type set to recurring_payment_failed. The outstanding_balance field will have the amount currently outstanding.
            /// Note this is received after PayPal has tried multiple times. First you will receive up to 3 'Skipped' messages, then you get the 'Failed' msg.
            /// </summary>
            [Description("recurring_payment_failed")]
            RecurringPaymentFailed,
            /// <summary>
            /// When the maximum number of failed payments is reached (as specified in the MAXFAILEDPAYMENTS parameter in your CreateRecurringPaymentsProfile call), you receive an IPN with txn_type 
            /// set to recurring_payment_suspended_due_to_max_failed_payment. This is the only IPN you receive (e.g., if MAXFAILEDPAYMENTS was set to 1, you only receive this IPN on the failed payment; 
            /// you do not receive another one with txn_type of recurring_payment_failed).
            /// </summary>
            [Description("recurring_payment_suspended_due_to_max_failed_payment")]
            RecurringPaymentSuspendedDueToMaxFailedPayment,

            [Description("send_money")]
            SendMoney,
            [Description("subscr_cancel")]
            SubscriptionCancelled,
            [Description("subscr_eot")]
            SubscriptionExpired,
            [Description("subscr_failed")]
            SubscriptionSignupFailed,
            [Description("subscr_modify")]
            SubscriptionModified,
            [Description("subscr_payment")]
            SubscriptionPaymentReceived,
            [Description("subscr_signup")]
            SubscriptionStarted,
            [Description("virtual_terminal")]
            PaymentReceivedFromVirtualTerminal,
            [Description("web_accept")]
            PaymentReceivedFromBuyNow_Donation_AuctionSmartLogos,
            CouldNotParse

        }

        public static TRANSACTION_TYPE GetTransactionTypeFromPayPalCode(string code, bool throwErrorIfCannotMatch)
        {
            var result = CS.General_CS_v6.Util.EnumUtil.ConvertStringToEnum<PayPalEnums.TRANSACTION_TYPE>(code, PayPalEnums.TRANSACTION_TYPE.CouldNotParse);
            if (result == TRANSACTION_TYPE.CouldNotParse && throwErrorIfCannotMatch)
            {
                throw new InvalidOperationException(string.Format("Could not match TRANSACTION_TYPE to code [{0}]", code));
            }
            return result;
        }

        /*
        public static TRANSACTION_TYPE TransactionTypeFromPayPalCode(string code)
        {
            if (code == null) code = "";
            code = code.ToLower();
            switch (code)
            {
                case "cart":
                    return TRANSACTION_TYPE.Cart;
                case "express-checkout": return TRANSACTION_TYPE.ExpressCheckout;
            }
            return TRANSACTION_TYPE.ExpressCheckout;
        }*/
        public enum PAYMENT_TYPE
        {
            [Description("none")]
            None,
            [Description("echeck")]
            eCheck,
            [Description("instant")]
            Instant

        }
        /*
        public static PAYMENT_TYPE PaymentTypeFromPayPalCode(string code)
        {
            if (code == null) code = "";
            code = code.ToLower();
            switch (code)
            {
                case "none": return PAYMENT_TYPE.None;
                case "echeck": return PAYMENT_TYPE.eCheck;
                case "instant": return PAYMENT_TYPE.Instant;
            }
            return PAYMENT_TYPE.None;
        }*/
        
        public enum RECURRING_AGREEMENT_STATUS
        {
            /// <summary>
            /// If PayPal successfully creates the profile, the profile has an ActiveProfile status.
            /// </summary>
            ActiveProfile,
            /// <summary>
            ///  If a non-recurring initial payment fails and you set FAILEDINITAMTACTION to CancelOnFailure in the CreateRecurringPaymentsProfile request, 
            /// PayPal creates the profile with a status of PendingProfile. The profile remains in this status until the initial payment either completes successfully or fails.
            /// </summary>
            PendingProfile,
            /// <summary>
            /// A profile has a status of ExpiredProfile when PayPal completes the total billing cycles for the optional trial and the regular payment periods.
            /// </summary>
            ExpiredProfile,
            SuspendedProfile,
            CancelledProfile
        }
    }
}
