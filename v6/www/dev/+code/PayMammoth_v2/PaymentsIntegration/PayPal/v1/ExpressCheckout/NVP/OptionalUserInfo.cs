﻿using PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class OptionalUserInfo : BasePayPalObject
    {
       


            

        /// <summary>
        /// The gift message entered by the buyer on the PayPal Review page.
        /// </summary>
        [PayPalFieldInfo(FieldName = "GiftMessage")]
        public string GiftMessage { get; set; }

        /// <summary>
        /// Returns true if the buyer requested a gift receipt on the PayPal Review page and
        ///false if the buyer did not.
        /// </summary>
        [PayPalFieldInfo(FieldName = "GiftReceiptEnable")]
        public bool? GiftReceiptEnabled { get; set; }

        /// <summary>
        /// Return the gift wrap name only if the gift option on the PayPal Review page is selected by the buyer.
        /// </summary>
        [PayPalFieldInfo(FieldName = "GiftWrapName")]
        public string GiftWrapName { get; set; }

        [PayPalFieldInfo(FieldName = "GiftWrapAmount")]
        public double? GiftWrapAmount { get; set; }

        [PayPalFieldInfo(FieldName = "BuyerMarketingEmail")]
        public string EmailSpecifiedInOptInField { get; set; }

        [PayPalFieldInfo(FieldName = "SurveyQuestion")]
        public string SurveyQuestion { get; set; }

        [PayPalFieldInfo(FieldName = "SurveyChoiceSelected")]
        public string SurveyChoiceSelectedByUser { get; set; }

        [PayPalFieldInfo(FieldName = "PayerID")]
        public string ClientPayerID { get; set; }

        

    }
        
}
