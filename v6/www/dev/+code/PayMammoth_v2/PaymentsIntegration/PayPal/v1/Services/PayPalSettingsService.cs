﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses;
using Raven.Client;
using BusinessLogic_CS_v6.Modules.Services.Settings;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1
{
    public interface IPayPalSettingsService
    {
        IPayPalSettings GetPaypalSettings(
            ReferenceLink<WebsiteAccountData> websiteAccountId,
            IDocumentSession documentSession);

        IPayPalCredentials GetCurrentSettings(
            ReferenceLink<WebsiteAccountData> websiteAccountId,
            IDocumentSession session);

        IPayPalCredentials GetCurrentSettings(IPayPalSettings settings);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PayPalSettingsService : IPayPalSettingsService
    {
        private readonly ISettingsService _settingsService;

        public PayPalSettingsService(
            ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        public IPayPalSettings GetPaypalSettings(
            ReferenceLink<WebsiteAccountData> websiteAccountId,
            IDocumentSession documentSession)
        {
            var websiteAccount = documentSession.GetById(new GetByIdParams<WebsiteAccountData>(websiteAccountId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});

            var paypal = websiteAccount.PaymentsInformation.PayPal;

            var account = paypal.LiveAccount;

            PayPalSettingsImpl settings = new PayPalSettingsImpl();
            settings.LiveCredentials.Username = account.Username;
            settings.LiveCredentials.Password = account.Password;
            settings.LiveCredentials.Signature = account.Signature;
            settings.LiveCredentials.NvpServerUrl = _settingsService.GetSettingValue<string>(PayMammoth_v6Enums.PayMammothSettings.PayPal_v1_Live_NvpServerUrl);
            settings.LiveCredentials.ApiServerUrl = _settingsService.GetSettingValue<string>(PayMammoth_v6Enums.PayMammothSettings.PayPal_v1_Live_ApiServerUrl);
            settings.LiveCredentials.MerchantEmail = account.MerchantEmail;
            settings.LiveCredentials.MerchantId = account.MerchantId;

            account = paypal.SandboxAccount;
            settings.SandboxCredentials.Username = account.Username;
            settings.SandboxCredentials.Password = account.Password;
            settings.SandboxCredentials.Signature = account.Signature;
            settings.SandboxCredentials.NvpServerUrl = _settingsService.GetSettingValue<string>(PayMammoth_v6Enums.PayMammothSettings.PayPal_v1_Sandbox_NvpServerUrl);
            settings.SandboxCredentials.ApiServerUrl = _settingsService.GetSettingValue<string>(PayMammoth_v6Enums.PayMammothSettings.PayPal_v1_Sandbox_ApiServerUrl);
            settings.SandboxCredentials.MerchantEmail = account.MerchantEmail;
            settings.SandboxCredentials.MerchantId = account.MerchantId;

            settings.UseLiveEnvironment = paypal.UseLiveEnvironment;
            return settings;
        }

        public IPayPalCredentials GetCurrentSettings(IPayPalSettings settings)
        {
            if (settings.UseLiveEnvironment)
                return settings.LiveCredentials;
            else
                return settings.SandboxCredentials;
        }

        public IPayPalCredentials GetCurrentSettings(
            ReferenceLink<WebsiteAccountData> websiteAccountId,
            IDocumentSession session)
        {
            var settings = GetPaypalSettings(
                websiteAccountId,
                session);
            return GetCurrentSettings(settings);
        }
    }
}