﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.CompilerServices;
using BusinessLogic_CS_v6.Modules.Data._Shared.BaseObjectWithIdentifiers;
using BusinessLogic_CS_v6.Modules.Services._Shared.BaseIsoEnumDatas;
using CS.General_CS_v6.Modules.Reflection;
using CS.General_CS_v6.Util;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses;
using PayPal.PayPalAPIInterfaceService.Model;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.Util
{
    public static class GeneralUtil
    {
        public static string GetSandboxOrLiveEnvironmentMode(bool useLiveEnvironment)
        {
            if (useLiveEnvironment)
                return "live";
            else
            {
                return "sandbox";
            }
        }

        public static void FillSellerDetailsFromPayPalSettings(SellerDetailsType sellerDetails,
            IPayPalSettings payPalSettings)
        {
            var credentialsToUse = payPalSettings.SandboxCredentials;
            if (payPalSettings.UseLiveEnvironment)
            {
                credentialsToUse = payPalSettings.LiveCredentials;
            }

            sellerDetails.PayPalAccountID = credentialsToUse.MerchantEmail;
            
        }

        public static string ConvertErrorTypeToString(ErrorType paypalError)
        {


            string s = string.Format("API Error | Code: {0} | Message: {1} | SeverityCode: {2}", paypalError.ErrorCode, paypalError.LongMessage, paypalError.SeverityCode);
            return s;
        }

        public static BasicAmountType GetAmountTypeFromPrice(decimal price, string currencyCode)
        {
            var currency = GetPayPalCurrencyFromCode(currencyCode);
            var priceStr = ConvertPriceValueToString(price);
            return new BasicAmountType(currency, priceStr);
        }

        public static CurrencyCodeType? GetPayPalCurrencyFromCode(string code)
        {
            var currencyCode = code;
            CurrencyCodeType currencyCodeType;
            if (CurrencyCodeType.TryParse(currencyCode, true, out currencyCodeType))
            {
                return currencyCodeType;
            }
            else
            {
                return null;
            }
        }
        public static CountryCodeType? GetPayPalCountryFromCode(string countryCode)
        {
            var country = InversionUtil.Get<IIso3166_CountryDataCache>().GetCountryByCountryCode(countryCode);
            return GetPayPalCountryFromCode(country);
        }
        public static CountryCodeType? GetPayPalCountryFromCode(Iso3166_CountryData countryCode)
        {
            //if (countryCode == null) throw new InvalidOperationException("Country code must be filled in");
            string twoLetterCode = null;
            if (countryCode != null)
            {
                twoLetterCode = countryCode.TwoLetterCode;
            }
            CountryCodeType country;
            if (CountryCodeType.TryParse(twoLetterCode, true, out country))
            {
                return country;
            }
            else
            {
                return null;
            }
        }


        public static string ConvertPriceValueToString(decimal price)
        {
            return price.ToString("0.00");
        }

        public static string ConvertObjectToPayPal(object o)
        {
            string s = null;
            if (o != null)
            {
                if (o is double)
                {
                    double d = (double)o;
                    s = d.ToString("0.00").Replace(",", ".");
                }
                else if (o is bool)
                {
                    bool b = (bool)o;
                    if (b)
                        s = "1";
                    else
                        s = "0";
                }
                else
                {
                    s = o.ToString();
                }
            }
            return s;
        }
        public static void AddItemToNVForPaypal(NameValueCollection nv, string title, object o)
        {
            string s = ConvertObjectToPayPal(o);
            
            if (!string.IsNullOrEmpty(s))
            {
                nv[title] = s;
            }
        }
        public static void AddPaypalFieldsToNameValueColl(NameValueCollection nv, object obj, string keyPrepend, string keyAppend)
        {
            List<PropertyAttributeInfo> properties = ReflectionUtil.GetAllPropertiesWithAttributes(obj.GetType(), typeof(PayPalFieldInfoAttribute));
            for (int i = 0; i < properties.Count; i++)
            {
                if (i == 32)
                {
                    int k = 5;
                }
                var pInfo = properties[i];
                PayPalFieldInfoAttribute fieldInfo = pInfo.Attributes.Cast<PayPalFieldInfoAttribute>().FirstOrDefault();
                object value = pInfo.Property.GetValue(obj, null);
                fieldInfo.AddToNameValueCollection(pInfo.Property, value, keyPrepend, keyAppend, nv);
            }
        }

        public static void FillObjectFromPaypalNameValueColl(NameValueCollection nv, object obj, string keyPrepend, string keyAppend)
        {
            var properties =ReflectionUtil.GetAllPropertiesWithAttributes(obj.GetType(), typeof(PayPalFieldInfoAttribute));
            foreach (var pInfo in properties)
            {
                PayPalFieldInfoAttribute fieldInfo = pInfo.Attributes.Cast<PayPalFieldInfoAttribute>().FirstOrDefault();
                object val = fieldInfo.GetValueFromNameValueCollection(pInfo.Property, keyPrepend, keyAppend, nv);
                if (pInfo.Property.CanWrite)
                {
                    pInfo.Property.SetValue(obj, val, null);
                }
                
            }
        }
        public static object ConvertPaypalStringToDataType(string s, Type t)
        {
            object val = null;
            if (!string.IsNullOrEmpty(s))
            {
                
                if (t.IsAssignableFrom(typeof(double)) || t.IsAssignableFrom(typeof(int)))
                {
                    s = s.Trim();
                    double d = Convert.ToDouble(s);
                    val = d;
                    if (t.IsAssignableFrom(typeof(int)))
                        val = (int)d;
                }
                
                else if (t.IsAssignableFrom(typeof(bool)))
                {
                    s = s.Trim();
                    bool b = ConversionUtil.TextToBoolNullable(s).GetValueOrDefault();
                    val = b;
                }
                else if (t.IsAssignableFrom(typeof(string)))
                {
                    val = s;

                }
                

                else
                    throw new InvalidOperationException("Invalid type!");
            }
            return val;
        }

    }
}
