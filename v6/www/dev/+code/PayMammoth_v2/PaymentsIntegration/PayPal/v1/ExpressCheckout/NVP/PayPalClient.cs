﻿using System.Collections.Specialized;
using CS.General_CS_v6.Util;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    /// <summary>
    /// Summary description for PayPal
    /// </summary>
    public class PayPalClient
    {
        private readonly IPayPalSettingsService _paypalSettingsService;


        public IPayPalSettings Settings { get; private set; }

        public PayPalClient(IPayPalSettings settings)
        {
            settings.MustNotBeNullable("Settings must be filled in");
            this.Settings = settings;
            init();
            _paypalSettingsService = InversionUtil.Get<IPayPalSettingsService>();
        }
        
        private void init()
        {
            
        }

       

       
        public void AppendAuthParametersToNV(NameValueCollection nv)
        {
            var cred = _paypalSettingsService.GetCurrentSettings(this.Settings);
            nv[PayPalConstants.PARAM_USER] = cred.Username;
            nv[PayPalConstants.PARAM_PASSWORD] = cred.Password;
            nv[PayPalConstants.PARAM_SIGNATURE] = cred.Signature;
            nv[PayPalConstants.PARAM_VERSION] = PayPalConstants.APIVersion;
        }
        
        public SetExpressCheckoutPayPalResponse SetExpressCheckout(SetExpressCheckoutRequest req)
        {
            
            return req.GetResponse();

        }
        public DoExpressCheckoutResponse DoExpressCheckout(DoExpressCheckoutRequest req)
        {
            return req.GetResponse();
        }
        public GetExpressCheckoutResponse GetExpressCheckout(GetExpressCheckoutRequest req)
        {
            return req.GetResponse();
        }



    }
}
