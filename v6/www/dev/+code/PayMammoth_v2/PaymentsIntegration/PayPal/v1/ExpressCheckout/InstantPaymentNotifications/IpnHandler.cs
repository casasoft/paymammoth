﻿using System.Text;
using System.Web;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using CS.General_CS_v6.Util;
using NLog;
using PayMammoth_v6.Modules.Data.Payments;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public class IpnHandler : IHttpHandler
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IIpnParserService _ipnParserService;

        //private readonly IRavenDbService _ravenDbService;
        public IpnHandler()
        {
            _ipnParserService = InversionUtil.Get<IIpnParserService>();
        }


        //protected virtual void parseSuccessfulIpnMessage(IpnResponse response)
        //{
        //    _log.Debug(string.Format("Received successful Ipn message - Transaction Type: {0} | Paypal Transaction Id: {1} | Ref: {2}",
        //        response.IpnMessage.TransactionType, response.IpnMessage.PayPalTransactionID, response.IpnMessage.Reference));

        //    var t = _request.CurrentTransaction;
        //    var msg = response.IpnMessage;
        //    var messageParser = getIpnMessageParserBasedOnResponseType(msg.TransactionType);
        //    if (messageParser != null)
        //    {
        //        messageParser.HandleIpnMessage(_request, response);
        //    }
            

        //}

      
        //protected virtual void TransactionInvalid(IpnResponse response)
        //{
        //    IPaymentRequestTransaction t = _request.CurrentTransaction;

        //    t.AddMessage("Invalid Transaction", CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
        //    using (var nhTransaction = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
        //    {
        //        var qs = t.GetParametersAsQueryString();

        //        addIpnResponseToTransaction(qs, response);
        //        t.SetPaymentParameters(qs);
        //        nhTransaction.Commit();

        //    }
        //}

        private string getFormContents(HttpContext context)
        {
            Encoding encoding = Encoding.UTF8;
            byte[] formContentBy = context.Request.BinaryRead(context.Request.ContentLength);
            string formContents = encoding.GetString(formContentBy);

            return formContents;
        }


        private static readonly object _testLock = new object();

        

        public void ProcessRequest(HttpContext context)
        {
            //todo: [For: Karl | 2014/03/28] remove this lock, only for testing! (WrittenBy: Karl)        			
            lock (_testLock)
            {
                string formContents = getFormContents(context);
                //retest this out
                

                _log.Debug(string.Format("IPN Handler. Form: {0}   || QueryString: {1}",
                    formContents,
                    context.Request.QueryString.ToString()));


                if (CS.General_CS_v6.Util.OtherUtil.IsLocalTestingMachine)
                {
                    //System.Threading.Thread.Sleep(5000);
                }


                bool ok = false;
                string paymentRequestId =
                    CS.General_CS_v6.Util.PageUtil.GetVariableFromQuerystring<string>(
                        PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER);
                if (!string.IsNullOrWhiteSpace(paymentRequestId))
                {

                    string qsContents = context.Request.QueryString.ToString();

                    ok = _ipnParserService.ParseIpnMessage(new ReferenceLink<PaymentRequestData>(paymentRequestId),
                                                    formContents,
                                                    qsContents,
                                                    null);

                }

                if (ok)
                {
                    context.Response.Write(PayPalConstants.IPN_OK);
                }
                else
                {
                    context.Response.Write(PayPalConstants.IPN_FAIL);
                }

                context.Response.StatusCode = 200;
                context.Response.Flush();
                context.Response.Close();
                context.Response.End();
            }
            // context.Response.End();
            //}
            //else
            //{
            //    checkIfRecurringPayment(formContents);
            //}
            
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}
