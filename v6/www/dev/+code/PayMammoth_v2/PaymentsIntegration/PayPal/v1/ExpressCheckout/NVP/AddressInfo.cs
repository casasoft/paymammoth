﻿using BusinessLogic_CS_v6.Modules.Data._Shared.BaseObjectWithIdentifiers;
using BusinessLogic_CS_v6.Modules.Services._Shared.BaseIsoEnumDatas;
using CS.General_CS_v6.Util;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class AddressInfo : BasePayPalObject
    {
        [PayPalFieldInfo(FieldName = "ShipToName", MaxLength = 32)]
        public string CustomerName
        {
            get { return CS.General_CS_v6.Util.TextUtil.AppendStrings(" ", this._clientFirstName, _clientMiddleName, _clientLastName); }
            set 
            {
                _clientFirstName = value;
                _clientLastName = null;
                _clientMiddleName = null;
            }
        }
        [PayPalFieldInfo(FieldName = "ShipToStreet", MaxLength = 100)]
        public string Street1 { get; set; }
        [PayPalFieldInfo(FieldName = "ShipToStreet2", MaxLength = 100)]
        public string Street2
        {
            get { return CS.General_CS_v6.Util.TextUtil.AppendStrings(", ", _street2, _street3); }
            set
            {
                _street2 = value;
                _street3 = null;
            }

        }
        [PayPalFieldInfo(FieldName = "ShipToCity", MaxLength = 40)]
        public string City { get; set; }
        [PayPalFieldInfo(FieldName = "ShipToState", MaxLength = 40)]
        public string State { get; set; }
        [PayPalFieldInfo(FieldName = "ShipToZIP", MaxLength = 20)]
        public string ZIPCode { get; set; }

        [PayPalFieldInfo(FieldName = "ShipToCountryCode", MaxLength = 2)]
        private string _CountryCode_Str { get; set; }
        public Iso3166_CountryData CountryCode
        {
            get
            {
                return InversionUtil.Get<IIso3166_CountryDataCache>().GetCountryByTwoLetterCode(_CountryCode_Str);
            }
            set
            {
                _CountryCode_Str = value.TwoLetterCode;

            }
        }


        [PayPalFieldInfo(FieldName = "ShipToPhoneNum", MaxLength = 32)]
        public string ClientPhoneNumber { get; set; }


        private string _clientFirstName;
        private string _clientMiddleName;
        private string _clientLastName;

        private string _street2;
        private string _street3;

       


        #region IClientDetails Members


        public string ClientReference { get; set; }

        #endregion
    }
        
}
