﻿using System;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services._Shared.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using NLog;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.RecurringProfiles;
using PayMammoth_v6.Modules.Services.Logging;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.Interfaces;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.HelperClasses;
using Raven.Client;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1
{
    [IocComponent]
    public class PayPalRecurringProfileManager : IPayPalRecurringProfileManager
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;

        private readonly IPayPalManager _payPalManager;
        private readonly IPayMammothLogService _payMammothLogService;
        private readonly IGetTotalPriceForRequestService _getTotalPriceForRequestService;
        private readonly IIso3166_CountryDataCache _iso3166CountryDataCache;
        private readonly IIso4217_CurrencyDataCache _iso4217CurrencyDataCache;

        public PayPalRecurringProfileManager(
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService,
            IPayPalManager payPalManager,
            IPayMammothLogService payMammothLogService,
            IGetTotalPriceForRequestService getTotalPriceForRequestService,
            IIso3166_CountryDataCache iso3166CountryDataCache,
            IIso4217_CurrencyDataCache iso4217CurrencyDataCache)
        {
            _iso4217CurrencyDataCache = iso4217CurrencyDataCache;
            _iso3166CountryDataCache = iso3166CountryDataCache;
            _getTotalPriceForRequestService = getTotalPriceForRequestService;
            _payMammothLogService = payMammothLogService;
            _payPalManager = payPalManager;

            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
        }

        //private static readonly PayPalRecurringProfileManager _instance = new PayPalRecurringProfileManager();

        //public static PayPalRecurringProfileManager Instance
        //{
        //    get { return _instance; }
        //}

        private CreateRecurringPaymentsProfileRequest createRecurringPaymentsProfileRequest(
            PayPalClient paypalClient,
            ReferenceLink<RecurringProfileData> recurringProfileDataId,
            ReferenceLink<PaymentRequestTransactionData> paymentRequestTransactionId,
            IDocumentSession session)
        {
            var recurringProfile = session.GetById(new GetByIdParams<RecurringProfileData>(recurringProfileDataId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});
            session.Include<PaymentRequestTransactionData>(x => x.PaymentRequestId);

            var transactionGetByIdParams = new GetByIdParams<PaymentRequestTransactionData>(paymentRequestTransactionId);
            transactionGetByIdParams.ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No;
            transactionGetByIdParams.AddInclude(x => x.PaymentRequestId);

            var transaction = session.GetById(transactionGetByIdParams);
            var paymentRequestDetails = session.GetById(new GetByIdParams<PaymentRequestData>(transaction.PaymentRequestId));
            //---------

            CreateRecurringPaymentsProfileRequest recurringRequest = new CreateRecurringPaymentsProfileRequest(
                paypalClient,
                transaction.ExternalReference);
            recurringRequest.ProfileStartDate = _currentDateTimeRetrieverService.GetCurrentDateTime();
            recurringRequest.Description = paymentRequestDetails.RequestDetails.Details.Description;
            recurringRequest.BillingPeriod = paymentRequestDetails.RequestDetails.RecurringProfile.IntervalType;
            recurringRequest.BillingFrequency = paymentRequestDetails.RequestDetails.RecurringProfile.IntervalFrequency;
            recurringRequest.TotalBillingCycles = paymentRequestDetails.RequestDetails.RecurringProfile.TotalBillingCycles.GetValueOrDefault();
            recurringRequest.MaximumFailedPayments = paymentRequestDetails.RequestDetails.RecurringProfile.MaxFailedAttempts;
            recurringRequest.MerchantProfileReference = recurringProfile.Identifier;

            recurringRequest.PayerFirstName = paymentRequestDetails.RequestDetails.Details.ClientContactDetails.Name;
            recurringRequest.PayerLastName = paymentRequestDetails.RequestDetails.Details.ClientContactDetails.LastName;
            recurringRequest.PayerMiddleName = paymentRequestDetails.RequestDetails.Details.ClientContactDetails.MiddleName;
            recurringRequest.BuyerStreet = paymentRequestDetails.RequestDetails.Details.ClientContactDetails.Address1;
            recurringRequest.BuyerStreet2 = paymentRequestDetails.RequestDetails.Details.ClientContactDetails.Address2;
            recurringRequest.BuyerZip = paymentRequestDetails.RequestDetails.Details.ClientContactDetails.PostCode;
            recurringRequest.BuyersCountryCode = _iso3166CountryDataCache.GetCountryByCountryCode(paymentRequestDetails.RequestDetails.Details.ClientContactDetails.Country3LetterCode);

            decimal totalExclTax = _getTotalPriceForRequestService.GetTotalPrice(
                paymentRequestDetails,
                includeTaxes: false,
                includeShippingAndHandling: true);
            decimal totalTax = _getTotalPriceForRequestService.GetTotalPrice(
                paymentRequestDetails,
                includeTaxes: true,
                includeShippingAndHandling: true) - totalExclTax;

            recurringRequest.Amount = totalExclTax;
            recurringRequest.TaxAmount = totalTax;
            recurringRequest.CurrencyCode = _iso4217CurrencyDataCache.GetCurrencyByCurrencyCode(paymentRequestDetails.RequestDetails.Pricing.CurrencyCode3Letter);
            recurringRequest.Email = paymentRequestDetails.RequestDetails.Details.ClientContactDetails.Email;
            return recurringRequest;
        }

        public SetupRecurringProfileResult SetupRecurringProfile(
            ReferenceLink<RecurringProfileData> recurringProfileDataId,
            ReferenceLink<PaymentRequestTransactionData> transactionId,
            IDocumentSession session)
        {
            var transaction = session.GetById(new GetByIdParams<PaymentRequestTransactionData>(transactionId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});
            session.Include<PaymentRequestTransactionData>(x => x.PaymentRequestId);
            var request = session.GetById(new GetByIdParams<PaymentRequestData>(transaction.PaymentRequestId));
            var profile = session.GetById(new GetByIdParams<RecurringProfileData>(recurringProfileDataId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});
            //----

            SetupRecurringProfileResult result = new SetupRecurringProfileResult();

            profile.Status = PayMammoth_v6Enums.RecurringProfileStatus.CouldNotCreate;

            if (request != null)
            {
                if (transaction != null && (request.RequestDetails.RecurringProfile.Required))
                {
                    PayPalClient payPalClient = _payPalManager.GetPayPalClient(
                        request.LinkedWebsiteAccountId,
                        session);
                    if (payPalClient != null)
                    {
                        CreateRecurringPaymentsProfileRequest recurringRequest = createRecurringPaymentsProfileRequest(
                            payPalClient,
                            profile,
                            transaction,
                            session);
                        CreateRecurringPaymentsProfileResponse response = recurringRequest.GetResponse();
                        checkResponseAndSave(
                            response,
                            transaction,
                            profile,
                            result,
                            session: null);
                    }
                    else
                    {
                        result.Status = EnumsPayMammothConnector.RecurringProfileCreateStatusMsg.InternalError;
                        _log.Error(
                            "PayPalClient should never be null when this method is called!",
                            new NullReferenceException());
                    }
                }
                else
                {
                    result.Status = EnumsPayMammothConnector.RecurringProfileCreateStatusMsg.InternalError;
                    throw new InvalidOperationException("This method should never be called if recurring payments were not enabled in the transaction!");
                }
            }
            else
            {
                result.Status = EnumsPayMammothConnector.RecurringProfileCreateStatusMsg.InternalError;
                throw new InvalidOperationException("PaymentRequestTransaction should never be null when this method is called!");
            }

            return result;
        }

        private const string invalidCountryPaypalErrorMessage = "This transaction cannot be processed. The country code in the shipping address must match the buyer's country of residence.";

        private void updateProfileIdentifierAndStatusForRecurringProfile(
            ReferenceLink<RecurringProfileData> profileId,
            EnumsPayMammothConnector.RecurringProfileCreateStatusMsg creationStatus,
            string profileIdOnGateway,
            IDocumentSession session)
        {
            var profile = session.GetById(new GetByIdParams<RecurringProfileData>(profileId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});
            //---

            profile.CreationStatus = creationStatus;
            profile.ProfileIdentifierOnPaymentGateway = profileIdOnGateway;
            session.Store(profile);
        }

        [RavenTaskAspect]
        private void checkResponseAndSave(
            CreateRecurringPaymentsProfileResponse response,
            ReferenceLink<PaymentRequestTransactionData> transactionId,
            ReferenceLink<RecurringProfileData> profileId,
            SetupRecurringProfileResult result,
            IDocumentSession session)
        {
            var profile = session.GetById(new GetByIdParams<RecurringProfileData>(profileId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});

            result.Status = EnumsPayMammothConnector.RecurringProfileCreateStatusMsg.InternalError;

            if (!response.IsGatewayFailure)
            {
                if (response.ProfileStatus == PayPalEnums.RECURRING_AGREEMENT_STATUS.ActiveProfile)
                {
                    result.Status = EnumsPayMammothConnector.RecurringProfileCreateStatusMsg.Success;
                    ;

                    updateProfileIdentifierAndStatusForRecurringProfile(
                        profileId,
                        result.Status,
                        response.ProfileId,
                        session);

                    //createRecurringProfileInDb(response, paymentRequestTransaction);
                }
                else
                {
                    _payMammothLogService.AddLogEntry(
                        profile.PayMammothLog,
                        string.Format(
                            "UserProfileError occurred while creating recurring profile. Response: {0}",
                            response.ResponseStatusInfo.GetAsString()),
                        _log,
                        GenericEnums.NlogLogLevel.Warn);
                }
            }
            else
            {
                if (response.ErrorMessage.ToLower() == invalidCountryPaypalErrorMessage.ToLower())
                {
                    result.Status = EnumsPayMammothConnector.RecurringProfileCreateStatusMsg.BuyerCountryDoesNotMatchShippingCountry;
                    _payMammothLogService.AddLogEntry(
                        profile.PayMammothLog,
                        string.Format("An error occured with the gateway due to the buyer's country not matching the shipping country. Original error message: " + response.ErrorMessage),
                        _log,
                        GenericEnums.NlogLogLevel.Warn);
                }
                else
                {
                    result.Status = EnumsPayMammothConnector.RecurringProfileCreateStatusMsg.GatewayError;
                    _payMammothLogService.AddLogEntry(
                        profile.PayMammothLog,
                        string.Format("An error occured with the gateway, this is probably due to incorrect information being supplied. PayPal error message: " + response.ErrorMessage),
                        _log,
                        GenericEnums.NlogLogLevel.Warn);
                }
                result.Message = response.ErrorMessage;
            }
            session.Store(profile);
        }

        //private void createRecurringProfileInDb(CreateRecurringPaymentsProfileResponse response, IPaymentRequestTransaction paymentRequestTransaction, bool autoSave = true)
        //{
        //    using (var t = nHibernateUtil.BeginTransactionBasedOnAutoSaveBool(autoSave))
        //    {
        //        IRecurringProfile profile = Factories.RecurringProfileFactory.CreateNewItem();
        //        profile.PaymentTransaction = paymentRequestTransaction;
        //        profile.ProfileIdentifierOnGateway = response.ProfileId;
        //        profile.PaymentGateway = Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout;
        //        profile.Status = PayMammoth.Enums.RecurringProfileStatus.Pending;
        //        profile.Save();
        //        paymentRequestTransaction.Save();
        //        t.CommitIfNotNull();
        //    }
        //}
    }
}