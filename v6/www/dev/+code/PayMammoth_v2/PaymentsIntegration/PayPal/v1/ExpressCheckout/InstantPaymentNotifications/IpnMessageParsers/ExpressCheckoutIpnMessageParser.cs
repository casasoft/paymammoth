﻿
using System.Runtime.InteropServices;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using CS.General_CS_v6.Modules.Urls;
using NLog;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Repositories.Payments;
using PayMammoth_v6.Modules.Services.Logging;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers
{
	public interface IExpressCheckoutIpnMessageParser:IIpnMessageParser
	{

	}

	[IocComponent]
	[EnsureNonNullAspect]
	public class ExpressCheckoutIpnMessageParser : IExpressCheckoutIpnMessageParser
	{
	   
		private static readonly Logger _log = LogManager.GetCurrentClassLogger();
		
			
		
		private readonly IPaymentRequestTransactionDataService _paymentRequestTransactionDataService;
		private readonly IPayMammothLogService _payMammothLogService;

		public ExpressCheckoutIpnMessageParser(IPaymentRequestTransactionDataService paymentRequestTransactionDataService,
			IPayMammothLogService payMammothLogService)
		{
			_payMammothLogService = payMammothLogService;
			_paymentRequestTransactionDataService = paymentRequestTransactionDataService;
			
		}

		#region IpnMessageParser Members

		//private void addIpnResponseToTransaction(QueryString qs, IpnResponse response)
		//{

		//    qs.Add("Ipn PayPal Response", response.PaypalResponse);



		//}
		[RavenTaskAspect]
		public void HandleIpnMessage(IpnMessage ipnMessage, IDocumentSession session = null)
		{
			string paymentRequestTransactionId = ipnMessage.Reference;

			var transactionGetParams =new GetByIdParams<PaymentRequestTransactionData>(paymentRequestTransactionId) { ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No };
			transactionGetParams.AddInclude(x=>x.PaymentRequestId);
			var transaction = BusinessLogic_CS_v6.Util.RavenDbUtil.GetById(session,transactionGetParams);

			var requestGetParams = new GetByIdParams<PaymentRequestData>(transaction.PaymentRequestId);
			var request = BusinessLogic_CS_v6.Util.RavenDbUtil.GetById(session, requestGetParams);
			

			//############


			_payMammothLogService.AddLogEntry(request, 
				string.Format("IpnMessage received related to transaction | {0}",ipnMessage.MessageBlockRaw),_log, GenericEnums.NlogLogLevel.Info,
				relatedTransaction:transaction);
			//##########
			if (!transaction.IsSuccessful)
			{
				

				if (ipnMessage.PaymentStatus == PayPalEnums.PAYMENT_STATUS.Completed)
				{
					_paymentRequestTransactionDataService.MarkTransactionAsPaid(transaction, false, "", session);
					
				}
				else if (ipnMessage.PaymentStatus == PayPalEnums.PAYMENT_STATUS.Pending)
				{
					// this means that the payment has been 'authorized', but needs to be confirmed by the business owner first
					//check ipnMessage.PendingReason for more info

					_payMammothLogService.AddLogEntry(request,
							string.Format("IpnMessage.Pending recieved for transaction [{0}].  This is because it needs to be approved by business owner first",
							transaction.Id), _log, GenericEnums.NlogLogLevel.Info,
							relatedTransaction: transaction);

					_paymentRequestTransactionDataService.MarkTransactionAsPendingActionByBusinessOwner(transaction, session);
				}

			}
			else
			{
				_payMammothLogService.AddLogEntry(request,
				string.Format("This should never be called when the transaction is already marked as successful. TransactionId: {0}", transaction.Id), _log, GenericEnums.NlogLogLevel.Warn,
				relatedTransaction: transaction);
				
			}
			
			

		}

		#endregion
	}
}
