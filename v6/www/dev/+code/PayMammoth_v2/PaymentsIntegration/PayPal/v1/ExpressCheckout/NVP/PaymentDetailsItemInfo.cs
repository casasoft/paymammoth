﻿using System.Collections.Generic;
using System.Collections.Specialized;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class PaymentDetailsItemInfo
    {
        /// <summary>
        /// Item Name
        /// </summary>
        [PayPalFieldInfo(FieldName="Name", MaxLength=  127)]
        public string Name { get; set; }
        /// <summary>
        /// Item Description
        /// </summary>
        [PayPalFieldInfo(FieldName = "Desc", MaxLength = 127)]
        public string Description { get; set; }
        /// <summary>
        /// Cost of item
        /// </summary>
        [PayPalFieldInfo(FieldName = "Amt", MaxAmount = 10000)]
        public decimal? PriceExcTaxPerUnit { get; set; }

        public decimal? GetTotalPriceExcTax()
        {
            decimal? d = null;
            if (this.PriceExcTaxPerUnit.HasValue)
            {
                int qty = 1;
                if (this.Quantity.HasValue)
                    qty = this.Quantity.Value;
                d = this.PriceExcTaxPerUnit.Value * qty;
            }
            return d;
        }

        /// <summary>
        /// Item Number
        /// </summary>
        [PayPalFieldInfo(FieldName = "Number")]
        public int? ItemNumber { get; set; }
        /// <summary>
        /// Quantity
        /// </summary>
        [PayPalFieldInfo(FieldName = "Qty")]
        public int? Quantity { get; set; }
        /// <summary>
        /// Tax Amount
        /// </summary>
        [PayPalFieldInfo(FieldName="TaxAmt", MaxAmount=  10000)]
        public decimal? TaxAmountPerUnit { get; set; }
        /// <summary>
        /// (Optional) Item weight corresponds to the weight of the item. You can pass this
///data to the shipping carrier as is without having to make an additional database
///query.
///You can
        /// </summary>
        [PayPalFieldInfo(FieldName="ItemWeightValue")]
        public int? ItemWeightValue { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [PayPalFieldInfo(FieldName="ItemWeightUnit")]
        public string ItemWeightUnit { get; set; }
        /// <summary>
        /// Item length corresponds to the length of the item. You can pass this
///data to the shipping carrier as is without having to make an additional database
///query
        /// </summary>
        [PayPalFieldInfo(FieldName="ItemLengthValue", MaxLength=  127)]
        public int? ItemLengthValue { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [PayPalFieldInfo(FieldName="ItemLengthUnit", MaxLength=  127)]
        public string ItemLengthUnit { get; set; }
        /// <summary>
        /// (Optional) Item width corresponds to the width of the item. You can pass this data
///to the shipping carrier as is without having to make an additional database query.
        /// </summary>

        [PayPalFieldInfo(FieldName="ItemWidthValue", MaxLength=  127)]
        public int? ItemWidthValue { get; set; }
        /// <summary>
        /// (Optional) Item width corresponds to the width of the item. You can pass this data
///to the shipping carrier as is without having to make an additional database query.
        /// </summary>
        [PayPalFieldInfo(FieldName="ItemWidthUnit", MaxLength=  127)]
        public string ItemWidthUnit { get; set; }
        /// <summary>
        /// Item height corresponds to the height of the item. You can pass this
///data to the shipping carrier as is without having to make an additional database
///query.
        /// </summary>
        [PayPalFieldInfo(FieldName="ItemHeightValue", MaxLength=  127)]
        public int? ItemHeightValue { get; set; }
        /// <summary>
        /// Item height corresponds to the height of the item. You can pass this
///data to the shipping carrier as is without having to make an additional database
///query.
        /// </summary>
        [PayPalFieldInfo(FieldName="ItemHeightUnit", MaxLength=  127)]
        public string ItemHeightUnit { get; set; }

        /// <summary>
        /// URL for the item
        /// </summary>
        [PayPalFieldInfo(FieldName="ItemUrl")]
        public string ItemUrl { get; set; }

        protected static string getAppendKey(int id)
        {
            string keyAppend = id.ToString();
            return keyAppend;
        }

        public void AppendToNameValue(NameValueCollection nv, string keyPrepend, int paymentDetailsItemID)
        {
            //GeneralUtil.AddItemToNVForPaypal(nv, "L_" + "Name" + id, Name);
            //GeneralUtil.AddItemToNVForPaypal(nv, "L_" + "Amt" + id, Amount);
            //GeneralUtil.AddItemToNVForPaypal(nv, "L_" + "Qty" + id, Quantity);
            //GeneralUtil.AddItemToNVForPaypal(nv, "L_" + "TaxAmt" + id, Tax);
            //string baseStr = "L_PaymentRequest_" + paymentDetailsID + "_";
            string keyAppend = getAppendKey(paymentDetailsItemID);

            Util.GeneralUtil.AddPaypalFieldsToNameValueColl(nv, this, keyPrepend, keyAppend);
/*            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "Name" + paymentDetailsItemID, this.Name);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "Desc" + paymentDetailsItemID, this.Description);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "Amt" + paymentDetailsItemID, this.TotalAmount);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "Number" + paymentDetailsItemID, this.ItemNumber);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "Qty" + paymentDetailsItemID, this.Quantity);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "TaxAmt" + paymentDetailsItemID, this.TaxAmount);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "ItemWeightValue" + paymentDetailsItemID, this.ItemWeightValue);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "ItemWeightUnit" + paymentDetailsItemID, this.ItemWeightUnit);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "ItemHeightValue" + paymentDetailsItemID, this.ItemHeightValue);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "ItemHeightUnit" + paymentDetailsItemID, this.ItemHeightUnit);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "ItemLengthValue" + paymentDetailsItemID, this.ItemLengthValue);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "ItemLengthUnit" + paymentDetailsItemID, this.ItemLengthUnit);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "ItemWidthValue" + paymentDetailsItemID, this.ItemWeightValue);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "ItemWidthUnit" + paymentDetailsItemID, this.ItemWeightUnit);
            GeneralUtil.AddItemToNVForPaypal(nv, baseStr + "ItemUrl" + paymentDetailsItemID, this.ItemUrl);
            */
            
            
        }

        public decimal? GetTotalTaxAmount()
        {
            decimal? d = null;
            if (this.TaxAmountPerUnit.HasValue)
            {
                int qty = 1;
                if (this.Quantity.HasValue)
                    qty = this.Quantity.Value;
                d = this.TaxAmountPerUnit.Value * qty;
            }
            return d;
        }

        public PaymentDetailsItemInfo()
        {

        }
        protected void ParseFromNameValueCollection(NameValueCollection nv, string keyPrepend, int id)
        {
            string keyAppend = getAppendKey(id);
            Util.GeneralUtil.FillObjectFromPaypalNameValueColl(nv, this, keyPrepend, keyAppend);



        }
        protected bool isValid()
        {
            return (this.GetTotalPriceExcTax().GetValueOrDefault() > 0 || !string.IsNullOrEmpty(this.Name));
        }
        
        public static List<PaymentDetailsItemInfo> ParseFromNameValueCollection(NameValueCollection nv, string keyPrepend)
        {
            List<PaymentDetailsItemInfo> list = new List<PaymentDetailsItemInfo>();
            for (int i = 0; i < 50; i++)
            {
                string appendKey = getAppendKey(i);
                PaymentDetailsItemInfo info = new PaymentDetailsItemInfo();
                info.ParseFromNameValueCollection(nv, keyPrepend, i);
                if (info.isValid())
                    list.Add(info);
            }
            return list;
        }

    }
        
}
