﻿using BusinessLogic_CS_v6.Aspects.NullAspects;

using BusinessLogic_CS_v6.Framework.DbObjects.References;
using CS.General_CS_v6.Modules.InversionOfControl;
using NLog;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Framework.Payments;
using PayMammoth_v6.Modules.Data.Payments;
using Raven.Client;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1
{
    public interface IPayPalRedirector : IPaymentRedirector
    {

    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PayPalRedirector : IPayPalRedirector
    {
        public PayPalRedirector()
        {

        }
    

        private static readonly Logger _log = LogManager.GetCurrentClassLogger();			
		
		
			
        #region IPaymentRedirector Members

        public RedirectionResult Redirect(ReferenceLink<PaymentRequestData> paymentRequestId, IDocumentSession session)
        {
            return null;
            //OperationResult result = new OperationResult();
            PayMammoth_v6Enums.RedirectionResultStatus result = PayMammoth_v6Enums.RedirectionResultStatus.Error;

            //todo: [For: Karl | 2014/01/07] check this out -  PayPalRedirector.Redirect (WrittenBy: Karl)

            //var request = session.GetById(paymentRequestId, throwErrorIfNull: true);
            ////-------


            //var transaction = request.StartNewTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout);
            //var response = PayPalManager.Instance.StartExpressCheckout(transaction);

            //if (!response.Success)
            //{
            //    _log.Debug(response.ResponseStatusInfo.GetAsString());
                
            //}
            //else
            //{
            //    result = Enums.RedirectionResult.Success;
                    
            //}


            //return result;


        }

        #endregion
    }
}
