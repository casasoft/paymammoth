﻿using System;
using Raven.Client;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers
{
    public interface IIpnMessageParser
    {
        void HandleIpnMessage(IpnMessage ipnMessage, IDocumentSession session = null);

    }
}
