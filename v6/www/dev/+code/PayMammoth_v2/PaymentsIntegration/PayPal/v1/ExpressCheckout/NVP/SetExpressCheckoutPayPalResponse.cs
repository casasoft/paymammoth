﻿using System.Collections.Specialized;
using CS.General_CS_v6.Modules.Urls;
using CS.General_CS_v6.Modules.Web;
using CS.General_CS_v6.Util;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class SetExpressCheckoutPayPalResponse : BasePayPalResponse
    {
        private PayPalClient _paypal = null;
        private readonly IPayPalSettingsService _paypalSettingService;
        private readonly IHttpRequestSenderService _httpRequestSenderService;

        public SetExpressCheckoutPayPalResponse(PayPalClient paypal)
        {
            _paypal = paypal;

            _paypalSettingService = InversionUtil.Get<IPayPalSettingsService>();
            _httpRequestSenderService = InversionUtil.Get<IHttpRequestSenderService>();

        }
        /// <summary>
        /// A token that uniquely identifiers this response for your user.  If the token was filled in in the request, the same
        /// token is returned
        /// </summary>
        [PayPalFieldInfo]
        public string Token { get; set; }

        public string GetUrlToRedirectTo()
        {
            var nv = new QueryString();
            nv[PayPalConstants.PARAM_TOKEN] = Token;
            nv[PayPalConstants.PARAM_CMD] = PayPalConstants.COMMAND_EXPRESS_CHECKOUT;
            var currentSettings = _paypalSettingService.GetCurrentSettings(this._paypal.Settings);
            string url = currentSettings.ApiServerUrl + "?" + nv.ToString();
            return url;
            
        }

        public void RedirectBrowserToPaypalCheckout()
        {
            var url = GetUrlToRedirectTo();
            PageUtil.RedirectPage(url);
        }
        public override void ParseFromNameValueCollection(NameValueCollection nv)
        {
            base.ParseFromNameValueCollection(nv);
        }
        
    }
        
        
}
