﻿namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses
{
    public class PayPalCredentialsImpl: IPayPalCredentials

    {
        #region IPaypalCredentials Members

        public string Username { get; set; }

        public string Password { get; set; }

        public string Signature { get; set; }

        #endregion

        #region IPaypalCredentials Members


        public string NvpServerUrl { get; set; }

        public string ApiServerUrl { get; set; }

        #endregion

        #region IPaypalCredentials Members


        public string MerchantEmail { get; set; }

        #endregion

        #region IPayPalCredentials Members


        public string MerchantId { get; set; }

        #endregion
    }
}
