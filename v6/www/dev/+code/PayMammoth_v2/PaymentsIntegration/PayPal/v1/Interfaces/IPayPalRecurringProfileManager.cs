﻿using PayMammoth_v6.Framework.Payments;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.Interfaces
{
    public interface IPayPalRecurringProfileManager : IRecurringProfileManager
    {
    }
}
