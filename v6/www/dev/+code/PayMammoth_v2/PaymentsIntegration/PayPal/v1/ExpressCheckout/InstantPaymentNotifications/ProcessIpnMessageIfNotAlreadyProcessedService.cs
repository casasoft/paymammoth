﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using NLog;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Services.Logging;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers;
using Raven.Client;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public interface IProcessIpnMessageIfNotAlreadyProcessedService
    {
        /// <summary>
        /// This processes an IPN message, AFTER it has been verified from PayPal that it is authentic. 
        /// </summary>
        void Process(
            ReferenceLink<PaymentRequestData> paymentRequestId,
            IpnMessage ipnMessage);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class ProcessIpnMessageIfNotAlreadyProcessedService : IProcessIpnMessageIfNotAlreadyProcessedService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IIpnService _ipnService;
        private readonly IGetIpnMessageParserBasedOnTransactionTypeService _getIpnMessageParserBasedOnTransactionTypeService;
        private readonly IPayMammothLogService _payMammothLogService;
        private readonly IRavenDbService _ravenDbService;

        public ProcessIpnMessageIfNotAlreadyProcessedService(
            IIpnService ipnService,
            IGetIpnMessageParserBasedOnTransactionTypeService getIpnMessageParserBasedOnTransactionTypeService,
            IPayMammothLogService payMammothLogService,
            IRavenDbService ravenDbService)
        {
            _ravenDbService = ravenDbService;
            _payMammothLogService = payMammothLogService;
            _getIpnMessageParserBasedOnTransactionTypeService = getIpnMessageParserBasedOnTransactionTypeService;
            _ipnService = ipnService;
        }

        
        public void Process(
            ReferenceLink<PaymentRequestData> paymentRequestId,
            IpnMessage ipnMessage
            )
        {
            var session = _ravenDbService.CreateNewSession();
            var paymentRequest = RavenDbUtil.GetById<PaymentRequestData>(
                session,
                new GetByIdParams<PaymentRequestData>(paymentRequestId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});

            //##########

            bool alreadyVerified = _ipnService.CheckIfIpnMessageAlreadyVerifiedInDatabase(
                ipnMessage.PayPalTransactionID,
                ipnMessage.PaymentStatus,
                session);
            if (!alreadyVerified)
            {
                _payMammothLogService.AddLogEntry(
                    paymentRequest.StatusLog,
                    string.Format(
                        "IpnMessage processed - PayPalTransactionID: {0} | IpnTrackId: {1}",
                        ipnMessage.PayPalTransactionID,
                        ipnMessage.IpnTrackId),
                    _log,
                    GenericEnums.NlogLogLevel.Debug);
              
            }
            else
            {
                _payMammothLogService.AddLogEntry(
                    paymentRequest.StatusLog,
                    string.Format(
                        "Skipping, IpnMessage already processed - PayPalTransactionID: {0} | IpnTrackId: {1}",
                        ipnMessage.PayPalTransactionID,
                        ipnMessage.IpnTrackId),
                    _log,
                    GenericEnums.NlogLogLevel.Debug);
                //do nothing
            }
            session.SaveChanges();
            session.Dispose();

            if (!alreadyVerified)
            {
               
                _ipnService.SaveIpnMessageAsProcessedInDatabase(
                    ipnMessage,
                    null);
                var ipnMsgParser = _getIpnMessageParserBasedOnTransactionTypeService.GetIpnMessageParserBasedOnTransactionType(ipnMessage.TransactionType);
                ipnMsgParser.HandleIpnMessage(
                    ipnMessage,
                    null);
            }
            
        }
    }
}