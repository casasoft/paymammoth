﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.Settings;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.Util;
using Raven.Client;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.Services
{
    public interface IPaypalConfigService
    {
        Dictionary<string, string> GetPayPalConfig(
            ReferenceLink<WebsiteAccountData> websiteAccountId,
            IDocumentSession documentSession);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaypalConfigService : IPaypalConfigService
    {
        private readonly IPayPalSettingsService _payPalSettingsService;
        private readonly ISettingsService _settingsService;

        public PaypalConfigService(
            IPayPalSettingsService payPalSettingsService,
            ISettingsService settingsService)
        {
            _settingsService = settingsService;
            _payPalSettingsService = payPalSettingsService;
        }

        public Dictionary<string, string> GetPayPalConfig(
            ReferenceLink<WebsiteAccountData> websiteAccountId,
            IDocumentSession documentSession)
        {
            //Check out https://github.com/paypal/merchant-sdk-dotnet/tree/master/PayPalMerchantSDK

            //todo: [For: Backend | 2014/03/26] unit-test PaypalConfigService.GetPayPalConfig (WrittenBy: Backend)
            Dictionary<string, string> paypalConfig = new Dictionary<string, string>();

            var payPalSettings = _payPalSettingsService.GetPaypalSettings(
                websiteAccountId,
                documentSession);
            var currentCredentials = _payPalSettingsService.GetCurrentSettings(payPalSettings);

            paypalConfig.Add(
                "mode",
                GeneralUtil.GetSandboxOrLiveEnvironmentMode(payPalSettings.UseLiveEnvironment)); //one of 'sandbox' or 'live'
            paypalConfig.Add(
                "connectionTimeout",
                _settingsService.GetSettingValue<int>(PayMammoth_v6Enums.PayMammothSettings.PayPal_v1_RequestTimeout)
                    .ToString());
            paypalConfig.Add(
                "requestRetries",
                _settingsService.GetSettingValue<int>(PayMammoth_v6Enums.PayMammothSettings.PayPal_v1_RequestRetries)
                    .ToString());

            paypalConfig.Add(
                "account1.apiUsername",
                currentCredentials.Username);
            paypalConfig.Add(
                "account1.apiPassword",
                currentCredentials.Password);
            paypalConfig.Add(
                "account1.apiSignature",
                currentCredentials.Signature);
            return paypalConfig;
        }
    }
}