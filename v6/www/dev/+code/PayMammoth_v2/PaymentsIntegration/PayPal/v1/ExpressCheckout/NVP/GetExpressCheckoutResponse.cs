﻿using System.Collections.Generic;
using System.Collections.Specialized;
using BusinessLogic_CS_v6.Modules.Data._Shared.BaseObjectWithIdentifiers;
using BusinessLogic_CS_v6.Modules.Services._Shared.BaseIsoEnumDatas;
using CS.General_CS_v6.Util;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class GetExpressCheckoutResponse : BasePayPalResponse
    {
        /// <summary>
        /// A token that uniquely identifiers this response for your user.  If the token was filled in in the request, the same
        /// token is returned
        /// </summary>
        [PayPalFieldInfo]
        public string Token { get; set; }
        /// <summary>
        /// A free-form field for your own use, as set by you in the Custom element of
///SetExpressCheckout request.
        /// </summary>
        [PayPalFieldInfo(FieldName="Custom")]
        public string CustomField { get; set; }
        
        /// <summary>
        /// Your own invoice or tracking number, as set by you in the element of the same name
///in SetExpressCheckout request.
        /// </summary>
        [PayPalFieldInfo(FieldName="InvNum")]
        public string InvoiceNo { get; set; }
        
        [PayPalFieldInfo(FieldName="PhoneNum")]
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// A discount or gift certificate offered by PayPal to the buyer. This amount will be
///represented by a negative amount. If the buyer has a negative PayPal account
///balance, PayPal adds the negative balance to the transaction amount, which is
///represented as a positive value.
        /// </summary>
        [PayPalFieldInfo(FieldName = "PayPalAdjustment")]
        public double? PayPalAdjustment { get; set; }
        
        [PayPalFieldInfo(FieldName = "Note")]
        public string Note { get; set; }

        
        [PayPalFieldInfo(FieldName = "CheckoutStatus")]
        protected string _checkoutStatus { get; set; }
        public PayPalEnums.CHECKOUT_STATUS CheckoutStatus
        {
            get
            {
                return CS.General_CS_v6.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.CHECKOUT_STATUS>(_checkoutStatus).GetValueOrDefault();
            }
            
        }


        [PayPalFieldInfo(FieldName = "Email")]
        public string ClientEmail { get; protected set; }

        [PayPalFieldInfo(FieldName = "PayerID")]
        public string ClientPayerID { get; protected set; }

        [PayPalFieldInfo(FieldName="PayerStatus")]
        protected string _clientPayerStatus {get;set;}

        public PayPalEnums.PAYPAL_PAYER_STATUS ClientPayerStatus 
        {
            get
            {
                return CS.General_CS_v6.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.PAYPAL_PAYER_STATUS>(_clientPayerStatus).GetValueOrDefault();
                
            }
        }

        [PayPalFieldInfo(FieldName="CountryCode")]
        protected string _clientCountryCode {get;set;}
        public Iso3166_CountryData  ClientCountryCode
        {
            get
            {
                return InversionUtil.Get<IIso3166_CountryDataCache>().GetCountryByCountryCode(_clientCountryCode);
            }
        }
        
        [PayPalFieldInfo(FieldName="Business")]
        public string ClientBusinessName {get;set;}


        [PayPalFieldInfo(FieldName="Salutation")]
        public string ClientSalutation { get; set; }
        
        [PayPalFieldInfo(FieldName="FirstName")]
        public string ClientFirstName { get; set; }
        
        [PayPalFieldInfo(FieldName="MiddleName")]
        public string ClientMiddleName { get; set; }
        
        [PayPalFieldInfo(FieldName="LastName")]
        public string ClientLastName { get; set; }
        
        [PayPalFieldInfo(FieldName="Suffix")]
        public string ClientSuffix { get; set; }
        
        public List<PaymentDetailsInfo> PaymentDetails {get; private set;}

        /// <summary>
        /// Describes how the options that were presented to the user were determined. Is one
/// of the following values:
/// API - Callback
/// API - Flatrate
        /// </summary>
        [PayPalFieldInfo(FieldName="ShippingCalculationMode")]
        protected string _ShippingCalculationMode {get;set;}

        public OptionalUserInfo OptionalUserInfo {get; private set;}

        public UserSelectedOptionsInfo UserSelectedOptions {get; private set;}

        public override void ParseFromNameValueCollection(NameValueCollection nv)
        {
 	        base.ParseFromNameValueCollection(nv);
            this.PaymentDetails = PaymentDetailsInfo.ParseFromNameValueCollection(nv);
            this.UserSelectedOptions = new UserSelectedOptionsInfo();
            this.UserSelectedOptions.ParseFromNameValueCollection(nv,null,null);
            this.OptionalUserInfo = new OptionalUserInfo();
            this.OptionalUserInfo.ParseFromNameValueCollection(nv,null,null);
            

        }

    }
           
}
