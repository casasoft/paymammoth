﻿using System;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using NLog;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Services.Logging;
using PayMammoth_v6.Modules.Services.PaymentMethods;
using PayMammoth_v6.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.Connector;
using PayPal.PayPalAPIInterfaceService.Model;
using Raven.Client;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1
{
    public interface IPayPalTransactionGeneratorService:IPaymentMethodTransactionGeneratorService
    {

    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PayPalTransactionGeneratorService : IPayPalTransactionGeneratorService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IPayPalManager _payPalManager;
        private readonly IPaymentRequestTransactionDataService _paymentRequestTransactionDataService;
        private readonly IPayMammothLogService _payMammothLogService;

        public PayPalTransactionGeneratorService(IPayPalManager payPalManager, IPaymentRequestTransactionDataService paymentRequestTransactionDataService,
            IPayMammothLogService payMammothLogService)
        {
            _payMammothLogService = payMammothLogService;
            _paymentRequestTransactionDataService = paymentRequestTransactionDataService;
            this._payPalManager = payPalManager;
        }

       
        public TransactionGeneratorResult GenerateTransaction(ReferenceLink<PaymentRequestData> requestId, IDocumentSession session)
        {
            //todo: [For: Karl | 2013/12/31] create unit tests PayPalTransactionGeneratorService.GenerateTransaction (WrittenBy: Karl)
            
            TransactionGeneratorResult result = new TransactionGeneratorResult();
            
            

            var request = session.GetById(new GetByIdParams<PaymentRequestData>(requestId) {ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No});
            //-------


            var transaction = _paymentRequestTransactionDataService.CreateNewTransaction(requestId,
                EnumsPayMammothConnector.PaymentMethodType.PayPal_ExpressCheckout, session);
            var response = _payPalManager.StartExpressCheckout(transaction,session);
            if (response == null || response.PayPalResponse == null)
            {
                throw new InvalidOperationException("An error has occurred in StartExpressCheckout.  'response' or 'response.PayPalResponse' should never be null");
            }

            result.Result = PayMammoth_v6Enums.RedirectionResultStatus.Error;
            if (response.PayPalResponse.Ack == AckCodeType.SUCCESS)
            {
                 result.Result = PayMammoth_v6Enums.RedirectionResultStatus.Success;
                 result.RedirectionType= CS.General_CS_v6.Enums.HrefTarget.Top;
                 result.UrlToRedirectTo = response.UrlToRedirectTo;
                

            }
            else
            {
              _payMammothLogService.AddLogEntry(request, "Error calling 'StartExpressCheckout'",
                  _log, GenericEnums.NlogLogLevel.Warn, relatedTransaction:transaction);
                
            }


            return result;

        }
    }
}
