﻿namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses
{
    public interface IPayPalSettings
    {
        IPayPalCredentials SandboxCredentials { get;  }
        IPayPalCredentials LiveCredentials { get; }
        bool UseLiveEnvironment { get; }
        
        
    }
}
