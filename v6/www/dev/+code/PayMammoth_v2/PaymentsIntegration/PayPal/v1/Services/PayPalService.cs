﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using CS.General_CS_v6.Modules.Urls;
using NLog;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.Logging;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses;
using PayPal.PayPalAPIInterfaceService.Model;
using Raven.Client;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1
{
    public interface IPayPalService
    {
        void UpdateTransactionWithPayPalResponse(
               PaymentRequestTransactionData transaction,
               PaymentRequestData request,
               AbstractResponseType payPalResponse,
               IDocumentSession documentSession);

        string GetUrlToRedirectToAfterSetExpressCheckout(
            SetExpressCheckoutResponseType setExpressCheckoutResponse,
            IPayPalCredentials payPalCredentials);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PayPalService : IPayPalService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IPayMammothLogService _payMammothLogService;

        public PayPalService(IPayMammothLogService payMammothLogService)
        {
            _payMammothLogService = payMammothLogService;
        }

        public string GetUrlToRedirectToAfterSetExpressCheckout(
            SetExpressCheckoutResponseType setExpressCheckoutResponse,
            IPayPalCredentials payPalCredentials)
        {
            //todo: [For: Backend | 2014/03/26] unit-test PayPalService.GetUrlToRedirectToAfterSetExpressCheckout (WrittenBy: Backend)
            var nv = new QueryString();
            nv[PayPalConstants.PARAM_TOKEN] = setExpressCheckoutResponse.Token;
            nv[PayPalConstants.PARAM_CMD] = PayPalConstants.COMMAND_EXPRESS_CHECKOUT;
            //var currentSettings = _paypalSettingService.GetCurrentSettings(this._paypal.Settings);
            string url = payPalCredentials.ApiServerUrl + "?" + nv.ToString();

            return url;
        }

        

        public void UpdateTransactionWithPayPalResponse(
            PaymentRequestTransactionData transaction,
            PaymentRequestData request,
            AbstractResponseType payPalResponse,
            IDocumentSession documentSession)
        {
          
            _payMammothLogService.AddLogEntry(
                request,
                string.Format(
                    "CorrelationID: {0} | Timestamp: {1} | Version: {2}",
                    payPalResponse.CorrelationID,
                    payPalResponse.Timestamp,
                    payPalResponse.Version),
                _log,
                relatedTransaction: transaction
                );
            documentSession.Store(transaction);
        }
    }
}