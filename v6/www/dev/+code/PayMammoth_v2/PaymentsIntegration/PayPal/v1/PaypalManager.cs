﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.Settings;
using BusinessLogic_CS_v6.Modules.Services._Shared.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Modules.InversionOfControl;
using CS.General_CS_v6.Modules.Urls;
using NLog;
using PayMammoth_v6;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.Emails;
using PayMammoth_v6.Modules.Services.Logging;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.HelperClasses;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.Services;
using PayMammoth_v6.Presentation.Services.Payments;
using PayMammoth_v6.Connector.Services;
using PayPal.Authentication;
using PayPal.Exception;
using PayPal.PayPalAPIInterfaceService;
using PayPal.PayPalAPIInterfaceService.Model;
using Raven.Client;
using Raven.Client.Linq;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.Util;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1
{
    public interface IPayPalManager
    {
        PayPalClient GetPayPalClient(
            ReferenceLink<WebsiteAccountData> accountId,
            IDocumentSession session);

        SetExpressCheckoutResult StartExpressCheckout(
            PaymentRequestTransactionData transaction,
            IDocumentSession session);

        PayPalEnums.CONFIRM_RESULT ConfirmPayment(
            ReferenceLink<PaymentRequestData> requestId,
            string token,
            string payerId,
            IDocumentSession session = null);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PayPalManager : IPayPalManager
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();

        private readonly IPayPalSettingsService _payPalSettingsService;

        private readonly ISettingsService _settingsService;
        private readonly IPayPalService _payPalService;
        private readonly IPayMammothLogService _payMammothLogService;
        private readonly IPaypalConfigService _paypalConfigService;
        private readonly IGetTotalPriceForRequestService _getTotalPriceForRequestService;
        private readonly IPaymentUrlModelService _paymentUrlModelService;
        private readonly IGetCultureModelForPaymentRequestService _getCultureModelForPaymentRequestService;
        private readonly IPayMammothConnectorUrlService _payMammothConnectorUrlService;
        private readonly IIso3166_CountryDataCache _iso3166CountryDataCache;

        public PayPalManager(
            IPayPalSettingsService payPalSettingsService,
            ISettingsService settingsService,
            IPayPalService payPalService,
            IPayMammothLogService payMammothLogService,
            IPaypalConfigService paypalConfigService,
            IGetTotalPriceForRequestService getTotalPriceForRequestService,
            IPaymentUrlModelService paymentUrlModelService,
            IGetCultureModelForPaymentRequestService getCultureModelForPaymentRequestService,
            IPayMammothConnectorUrlService payMammothConnectorUrlService,
            IIso3166_CountryDataCache iso3166CountryDataCache)
        {
            _iso3166CountryDataCache = iso3166CountryDataCache;
            _payMammothConnectorUrlService = payMammothConnectorUrlService;
            _getCultureModelForPaymentRequestService = getCultureModelForPaymentRequestService;
            _paymentUrlModelService = paymentUrlModelService;
            _getTotalPriceForRequestService = getTotalPriceForRequestService;
            _paypalConfigService = paypalConfigService;
            _payMammothLogService = payMammothLogService;
            _payPalService = payPalService;
            _settingsService = settingsService;

            _payPalSettingsService = payPalSettingsService;
        }

        public PayPalClient GetPayPalClient(
            ReferenceLink<WebsiteAccountData> accountId,
            IDocumentSession session)
        {
            IPayPalSettings settings = _payPalSettingsService.GetPaypalSettings(
                accountId,
                session);
            return new PayPalClient(settings);
        }

        public IpnClient GetIpnClient(
            ReferenceLink<WebsiteAccountData> accountId,
            IDocumentSession session)
        {
            IPayPalSettings settings = _payPalSettingsService.GetPaypalSettings(
                accountId,
                session);
            return new IpnClient(settings);
        }

        /// <summary>
        /// Starts an express checkout response.  If successful, redirects to paypal page
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        public SetExpressCheckoutResult StartExpressCheckout(
            PaymentRequestTransactionData transaction,
            IDocumentSession session = null)
        {
            //todo: [For: Backend | 2014/03/26] split this out into as unit-testable as possible (WrittenBy: Karl)    
            //todo: [For: Backend | 2014/04/29] Update this unit tests, to test out that the confirm URL is correctly retrieved (WrittenBy: Karl)        				
            SetExpressCheckoutResult result = new SetExpressCheckoutResult();
            var request = session.GetById(new GetByIdParams<PaymentRequestData>(transaction.PaymentRequestId));

            try
            {
                SetExpressCheckoutRequestDetailsType setExpressCheckoutRequestDetails = new SetExpressCheckoutRequestDetailsType();

                
                string requestIdentifier = request.Id;

                var paypalSettings = _payPalSettingsService.GetPaypalSettings(
                    request.LinkedWebsiteAccountId,
                    session);
                var currentCredentials = _payPalSettingsService.GetCurrentSettings(paypalSettings);
                string returnUrl = null;
                var cultureModel = _getCultureModelForPaymentRequestService.GetCultureModelForPaymentRequest(request);

                {
                    string confirmUrl = _paymentUrlModelService.GetPayPalConfirmationPageUrl(
                        session,
                        cultureModel,
                        request.Id);

                    UrlClass redirectUrlClass = new UrlClass(confirmUrl);
                    redirectUrlClass[PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER] = requestIdentifier;
                    returnUrl = redirectUrlClass.ToString();
                }
                string paymentChoicePage = _payMammothConnectorUrlService.GetPayMammothUrlForRequest(
                    requestIdentifier,
                    request.RequestDetails.Language);

                setExpressCheckoutRequestDetails.ReturnURL = returnUrl;
                setExpressCheckoutRequestDetails.CancelURL = paymentChoicePage;

                //############# OLD CODE
                //############# OLD CODE
                //############# OLD CODE
                //############# OLD CODE

//                SetExpressCheckoutRequest expCheckoutRequest = new SetExpressCheckoutRequest(client, returnUrl, paymentChoicePage);

                fillRecurringPaymentsInfoIfPaymentRequestIsRecurring(
                    request,
                    setExpressCheckoutRequestDetails,
                    session);

                setExpressCheckoutRequestDetails.PaymentDetails = new List<PaymentDetailsType>();
                PaymentDetailsType paymentDetails = new PaymentDetailsType();
                setExpressCheckoutRequestDetails.PaymentDetails.Add(paymentDetails);

                var countryCode = _iso3166CountryDataCache.GetCountryByCountryCode(request.RequestDetails.Details.ClientContactDetails.Country3LetterCode);

                var paypalLanguage = PayPalEnums.LanguageFromISO(request.RequestDetails.Language, countryCode);
                
                setExpressCheckoutRequestDetails.LocaleCode = CS.General_CS_v6.Util.EnumUtil.ConvertEnumToString(paypalLanguage);

                string notifyUrl = null;
                {
                    UrlClass urlClass = new UrlClass(PayPalUrls.IpnHandlerUrl);
                    urlClass[PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER] = requestIdentifier;
                    notifyUrl = urlClass.ToString();
                }

                //continue here switching paypal to this type
                paymentDetails.PaymentAction = PaymentActionCodeType.SALE;

                SellerDetailsType sellerDetails = new SellerDetailsType();
                paymentDetails.SellerDetails = sellerDetails;
                sellerDetails.SellerId = currentCredentials.MerchantEmail;

                paymentDetails.PaymentRequestID = transaction.Id;

                paymentDetails.NotifyURL = notifyUrl;
                {
                    paymentDetails.ShipToAddress = new AddressType();
                    paymentDetails.ShipToAddress.CityName = request.RequestDetails.Details.ClientContactDetails.Locality;
                    paymentDetails.ShipToAddress.Phone = request.RequestDetails.Details.ClientContactDetails.Telephone;

                    //paymentInfo.ShipToAddress.ClientReference = request.InitialRequestInfo.Details.ClientReference;

                    paymentDetails.ShipToAddress.Country = GeneralUtil.GetPayPalCountryFromCode(countryCode);

                    paymentDetails.ShipToAddress.Name = request.RequestDetails.Details.ClientContactDetails.GetFullName();
                    paymentDetails.ShipToAddress.StateOrProvince = request.RequestDetails.Details.ClientContactDetails.State;
                    paymentDetails.ShipToAddress.Street1 = request.RequestDetails.Details.ClientContactDetails.Address1;
                    paymentDetails.ShipToAddress.Street2 = request.RequestDetails.Details.ClientContactDetails.Address2;
                    paymentDetails.ShipToAddress.PostalCode = request.RequestDetails.Details.ClientContactDetails.PostCode;
                }

                paymentDetails.OrderDescription = request.RequestDetails.Details.Description;

                paymentDetails.HandlingTotal = GeneralUtil.GetAmountTypeFromPrice(
                    request.RequestDetails.Pricing.HandlingAmount,
                    request.RequestDetails.Pricing.CurrencyCode3Letter);
                //paymentInfo.PaymentRequestID
                paymentDetails.InvoiceID = transaction.Id;
                paymentDetails.ShippingTotal = GeneralUtil.GetAmountTypeFromPrice(
                    request.RequestDetails.Pricing.ShippingAmount,
                    request.RequestDetails.Pricing.CurrencyCode3Letter);

                paymentDetails.PaymentDetailsItem = new List<PaymentDetailsItemType>();

                foreach (var itemLine in request.RequestDetails.ItemDetails)
                {
                    var paymentItemInfo = new PaymentDetailsItemType();
                    paymentDetails.PaymentDetailsItem.Add(paymentItemInfo);

                    paymentItemInfo.Description = itemLine.Description;
                    paymentItemInfo.Amount = GeneralUtil.GetAmountTypeFromPrice(
                        itemLine.UnitPrice,
                        request.RequestDetails.Pricing.CurrencyCode3Letter);
                    paymentItemInfo.Tax = GeneralUtil.GetAmountTypeFromPrice(
                        itemLine.TaxAmountPerUnit,
                        request.RequestDetails.Pricing.CurrencyCode3Letter);
                    paymentItemInfo.Name = itemLine.Title;
                    paymentItemInfo.Quantity = itemLine.Quantity;
                }

                var itemsTotalExcTaxes = _getTotalPriceForRequestService.GetTotalPrice(
                    request,
                    includeTaxes: false,
                    includeShippingAndHandling: false);
                var itemsTotalIncTaxes = _getTotalPriceForRequestService.GetTotalPrice(
                    request,
                    includeTaxes: true,
                    includeShippingAndHandling: false);
                var taxesOnly = itemsTotalIncTaxes - itemsTotalExcTaxes;
                paymentDetails.ItemTotal = GeneralUtil.GetAmountTypeFromPrice(
                    itemsTotalExcTaxes,
                    request.RequestDetails.Pricing.CurrencyCode3Letter);

                paymentDetails.TaxTotal = GeneralUtil.GetAmountTypeFromPrice(
                    taxesOnly,
                    request.RequestDetails.Pricing.CurrencyCode3Letter);

                var orderTotal = _getTotalPriceForRequestService.GetTotalPrice(
                    request,
                    includeTaxes: true,
                    includeShippingAndHandling: true);
                paymentDetails.OrderTotal = GeneralUtil.GetAmountTypeFromPrice(
                    orderTotal,
                    request.RequestDetails.Pricing.CurrencyCode3Letter);

                //var total = paymentInfo.HandlingAmount + paymentInfo.ShippingAmount + paymentInfo.TaxTotal + paymentInfo.ItemsTotalAmountExcTax;
                //var total2 = paymentInfo.TotalCostOfTransaction;

                _payMammothLogService.AddLogEntry(
                    request,
                    
                    "Calling 'SetExpressCheckout'",
                    relatedTransaction:transaction);
                SetExpressCheckoutResponseType setExpressCheckoutResponse = null;
                {
                    var paypalConfig = _paypalConfigService.GetPayPalConfig(
                        request.LinkedWebsiteAccountId,
                        session);

                    var payPalApiInterfaceService = new PayPalAPIInterfaceServiceService(paypalConfig);

                    var setExpressCheckout = new SetExpressCheckoutReq();
                    var setExpressCheckoutRequest = new SetExpressCheckoutRequestType(setExpressCheckoutRequestDetails);
                    setExpressCheckout.SetExpressCheckoutRequest = setExpressCheckoutRequest;

                    setExpressCheckoutResponse = payPalApiInterfaceService.SetExpressCheckout(setExpressCheckout);
                }

                if (setExpressCheckoutResponse != null)
                {
                    transaction.PaymentGatewaySpecificInfo.PayPalTransactionInfo = new PayPalTransactionInfo();
                    var paypalRawResponse = setExpressCheckoutResponse.ToString();
                    transaction.PaymentGatewaySpecificInfo.PayPalTransactionInfo.TokenId = setExpressCheckoutResponse.Token;
                    _payMammothLogService.AddLogEntry(
                        request,
                        "SetExpressCheckout - Response: " + paypalRawResponse,
                        relatedTransaction: transaction);

                    result.PayPalResponse = setExpressCheckoutResponse;

                    if (setExpressCheckoutResponse.Ack == AckCodeType.SUCCESS)
                    {
                        _payMammothLogService.AddLogEntry(
                            request,
                            "SetExpressCheckout: SUCCESS",
                            relatedTransaction: transaction);
                        _payPalService.UpdateTransactionWithPayPalResponse(
                            transaction, request,
                            setExpressCheckoutResponse,
                            session);
                        result.UrlToRedirectTo = _payPalService.GetUrlToRedirectToAfterSetExpressCheckout(
                            setExpressCheckoutResponse,
                            currentCredentials);
                    }
                    else
                    {
                        StringBuilder sbErrors = new StringBuilder();
                        if (setExpressCheckoutResponse.Errors != null)
                        {
                            foreach (ErrorType error in setExpressCheckoutResponse.Errors)
                            {
                                string errorAsString = GeneralUtil.ConvertErrorTypeToString(error);
                                sbErrors.AppendLine(errorAsString);
                            }
                        }
                        _payMammothLogService.AddLogEntry(
                            request,
                            string.Format(
                                "SetExpressCheckout - FAILED. Error: {0}",
                                sbErrors.ToString()),
                            _log,
                            GenericEnums.NlogLogLevel.Error,
                        relatedTransaction: transaction);
                    }
                }
                else
                {
                    _payMammothLogService.AddLogEntry(
                        request,
                        string.Format("SetExpressCheckout - Returned NULL"),
                        relatedTransaction: transaction);
                }
            }
            catch (Exception ex)
            {
                _payMammothLogService.AddLogEntry(
                    request,
                    "Error occurred in StartExpressCheckout",
                    _log,
                    GenericEnums.NlogLogLevel.Error,
                    ex,
                        relatedTransaction: transaction);
                throw;
            }

            return result;
        }

        private decimal calculateRecurringMaxAmountAndIncreasePercentage(
            decimal totalPrice,
            IDocumentSession session)
        {
            var maxAmountPercentage = _settingsService.GetSettingValue<decimal>(
                PayMammoth_v6.Enums.PayMammoth_v6Enums.PayMammothSettings.RecurringPayments_MaxAmountExtraPercentage);
            return totalPrice*maxAmountPercentage;
        }

        internal void fillRecurringPaymentsInfoIfPaymentRequestIsRecurring(
            PaymentRequestData request,
            SetExpressCheckoutRequestDetailsType expCheckoutRequest,
            IDocumentSession session)
        {
            if (request.RequestDetails.RecurringProfile.Required)
            {
                if (_log.IsDebugEnabled) _log.Debug("Setting recurring payments in ExpressCheckoutRequest.");
                BillingAgreementDetailsType billingAgreementDetailsType = new BillingAgreementDetailsType();

                if (expCheckoutRequest.BillingAgreementDetails == null)
                {
                    expCheckoutRequest.BillingAgreementDetails = new List<BillingAgreementDetailsType>();
                }

                expCheckoutRequest.BillingAgreementDetails.Add(billingAgreementDetailsType);

                billingAgreementDetailsType.BillingAgreementDescription = request.RequestDetails.Details.Description;
                billingAgreementDetailsType.BillingType = BillingCodeType.RECURRINGPAYMENTS;

                //expCheckoutRequest.BillingAgreementDetails.Add( .BillingTypes.Add(PayPalEnums.BILLING_TYPES.RecurringPayments.ToString());
                //expCheckoutRequest.BillingAgreementDescriptions.Add(request.InitialRequestInfo.Details.Description);

                decimal totalRequestPrice = _getTotalPriceForRequestService.GetTotalPrice(
                    request,
                    includeTaxes: true,
                    includeShippingAndHandling: true);

                var recurringMaxAmount = calculateRecurringMaxAmountAndIncreasePercentage(
                    totalRequestPrice,
                    session);

                expCheckoutRequest.MaxAmount = GeneralUtil.GetAmountTypeFromPrice(
                    recurringMaxAmount,
                    request.RequestDetails.Pricing.CurrencyCode3Letter);

                if (_log.IsDebugEnabled) _log.Debug("Recurring payments in ExpressCheckoutRequest set successfully.");
            }
            else
            {
                if (_log.IsDebugEnabled) _log.Debug("Recurring payments is not enabled for this transaction.");
            }
        }

        [RavenTaskAspect]
        public PayPalEnums.CONFIRM_RESULT ConfirmPayment(
            ReferenceLink<PaymentRequestData> requestId,
            string token,
            string payerId,
            IDocumentSession session)
        {
            var getByIdParams = new GetByIdParams<PaymentRequestData>(requestId);
            getByIdParams.ThrowErrorIfNotAlreadyLoadedInSession = ThrowErrorIfNotAlreadyLoadedInSession.No;
            getByIdParams.AddInclude(x => x.LinkedWebsiteAccountId);
            var request = session.GetById(getByIdParams);
            var websiteAccount = session.GetById(new GetByIdParams<WebsiteAccountData>(request.LinkedWebsiteAccountId));

            //---------

            PayPalEnums.CONFIRM_RESULT result = PayPalEnums.CONFIRM_RESULT.Error;
            if (_log.IsDebugEnabled) _log.Debug("Confirm Payment - Start");

            var payPalClient = GetPayPalClient(
                request.LinkedWebsiteAccountId,
                session);

            PayPalDoExpressCheckoutConfirmer paypalRequest = new PayPalDoExpressCheckoutConfirmer();

            bool ok = paypalRequest.ConfirmPayment(payPalClient,payerId,token);
            if (ok)
            {
                result = PayPalEnums.CONFIRM_RESULT.OK;
                //2014-03-28 this was removed, as user can have a request 'pending'. 
                //ok = _paymentRequestService.WaitUntilRequestIsMarkedAsPaid(request);
                //if (ok)
                //{

                //}
            }

            if (!ok && paypalRequest.ResponseStatus.ErrorCode == "10002")
            {
                throw new PayPalException("Cannot confirm payment (Error code: 10002) - This is probably due to an invalid NVP/API URL, or username, password, signature etc. Make sure you are using sandbox urls & passwords if you are in testing mode, or live urls & passwords if you in live");
            }

            if (!ok)
            {
                _log.Error(
                    "Confirm Payment - Error occurred [Code: {0}, Short Msg: {1}, Long Msg: {2}]",
                    paypalRequest.ResponseStatus.ErrorCode,
                    paypalRequest.ResponseStatus.ShortMessage,
                    paypalRequest.ResponseStatus.LongMessage);
            }
            else
            {
                _log.Debug("Payment confirmed");
            }

            _log.Debug("Confirm Payment - Finish");
            return result;
        }
    }
}