﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace PaymentGateways.v1.PayPal.ExpressCheckout.NVP
{
    /// <summary>
    /// Summary description for PayPal
    /// </summary>
    public class PayPalClient
    {
        public static PayPalClient Instance
        {
            get; set;
        }



        public const string SERVER_NVP_SANDBOX = "https://api-3t.sandbox.paypal.com/nvp";
        public const string SERVER_NVP_LIVE = "https://api-3t.paypal.com/nvp";
        public const string SERVER_NVP_SANDBOX_CERTIFICATE = "https://api.sandbox.paypal.com/nvp";
        public const string SERVER_NVP_LIVE_CERTIFICATE = "https://api.paypal.com/nvp";
        public const string SERVER_SANDBOX = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        public const string SERVER_LIVE = "https://www.paypal.com/cgi-bin/webscr";
        
        public PayPalClient(bool useSandbox, string userSandbox, string passSandbox, string signatureSandbox, string userLive, string passLive, string passSignature,
            string serverNVP = null, string apiServer = null)
        {
            if (serverNVP == null)
            {
                if (useSandbox)
                {
                    serverNVP = SERVER_NVP_SANDBOX;
                    apiServer = SERVER_SANDBOX;
                }
                else
                {

                    serverNVP = SERVER_NVP_LIVE;
                    apiServer = SERVER_LIVE;

                }
            }

            init(serverNVP, apiServer, useSandbox, userSandbox, passSandbox, signatureSandbox, userLive, passLive, passSignature);
        }
        
        private void init(string serverNVP, string server, bool useSandbox, string userSandbox, string passSandbox, string signatureSandbox, string userLive, string passLive, string signatureLive)
        {
            this.NVPServer = serverNVP;
            
            this.Server = server;
            if (useSandbox)
            {
                this.Pass = passSandbox;
                this.User = userSandbox;
                this.Signature = signatureSandbox;
            }
            else
            {
                this.Pass = passLive;
                this.User = userLive;
                this.Signature = signatureLive;
            }
        }
        
        public const string APIVersion = "65.0";
        
        public void AppendAuthParametersToNV(NameValueCollection nv)
        {
            nv["user"] = User;
            nv["pwd"] = Pass;
            nv["signature"] = Signature;
            nv["version"] = APIVersion;
        }
        public string User { get; set; }
        public string Pass { get; set; }
        public string Signature { get; set; }
        public string NVPServer { get; set; }
        public string Server { get; set; }
        
        public SetExpressCheckoutResponse SetExpressCheckout(SetExpressCheckoutRequest req)
        {
            
            return req.GetResponse();

        }
        public DoExpressCheckoutResponse DoExpressCheckout(DoExpressCheckoutRequest req)
        {
            return req.GetResponse();
        }
        public GetExpressCheckoutResponse GetExpressCheckout(GetExpressCheckoutRequest req)
        {
            return req.GetResponse();
        }



    }
}
