﻿using BusinessLogic_CS_v6.Aspects;
using CS.General_CS_v6.Util;

namespace PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    [LogAspect]
    public class PayPalDoExpressCheckoutConfirmer
    {
        //public string PayerID { get; private set; }
        //public string Token { get; private set; }

     
        /// <summary>
        /// Creates the object
        /// </summary>
        /// <param name="paypalClient">PayPal client to load from.  If null, loads from the default instance</param>
        /// <param name="loadFromQueryString">Whether to load from query string if id and token are none</param>
        /// <param name="payerID">Payer ID</param>
        /// <param name="token">Token</param>
        public PayPalDoExpressCheckoutConfirmer()
        {
          
            
           
        }

        public ResponseStatusInfo ResponseStatus { get; private set; }
        
        
        /// <summary>
        /// Confirms the payment authorization.  HOWEVER, this does not mean that the payment has really been effected
        /// </summary>
        /// <returns></returns>
        public bool ConfirmPayment(PayPalClient paypalClient, string payerId, string token)
        {
            
            bool ok = false;
            PayPalClient client = paypalClient;
            GetExpressCheckoutRequest req = new GetExpressCheckoutRequest(client, token);
            GetExpressCheckoutResponse GECresp = req.GetResponse();

            if (GECresp.Success)
            {
                DoExpressCheckoutRequest DECReq = new DoExpressCheckoutRequest(paypalClient, GECresp);
                DoExpressCheckoutResponse DECResp = DECReq.GetResponse();
                this.ResponseStatus = DECResp.ResponseStatusInfo;
                if (DECResp.Success)
                {
                    ok = true;
                }
                
            }
            else
            {
                this.ResponseStatus = GECresp.ResponseStatusInfo;

            }
            return ok;

        }
    }
}
