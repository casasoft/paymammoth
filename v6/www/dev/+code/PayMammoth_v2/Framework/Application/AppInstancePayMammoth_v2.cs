﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Modules.Services.ContentTexts;
using BusinessLogic_CS_v6.Modules.Services.Emails.EmailTemplates;
using BusinessLogic_CS_v6.Modules.Services.Sections;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Modules.Services.Notifications;
using SimpleInjector;
using SimpleInjector.Extensions;

namespace PayMammoth_v6.Framework.Application
{
    public class AppInstancePayMammoth_v6 : CS.MvcCmsGeneral_CS_v6.Code.Framework.Application.AppInstanceMvcCmsGeneral
    {
        public new static AppInstancePayMammoth_v6 Instance
        {
            get { return (AppInstancePayMammoth_v6)CS.General_CS_v6.Modules.Application.AppInstanceGeneral.Instance; }
            
        }
        
        //protected override void addAssemblies()
        //{
        //    //this.ProjectAssemblies.Add(typeof(PayMammoth_v6.Framework.Application.AppInstancePayMammoth_v6).Assembly);
        //    //this.ProjectAssemblies.Add(typeof(PayMammoth_v6.Connector.Services.PayMammothConnectorInitialRequestService).Assembly);
        //    //base.addAssemblies();
        //}
        
       
        protected override void onApplicationStart()
        {
            base.onApplicationStart();
         
        }

        protected override void onPostApplicationStart()
        {
            base.onPostApplicationStart();
            //now it is being automatically initailsied
            //InversionUtil.Get<INotificationsAzureQueueManager>().Initialise();

        }

        protected override void initialiseInversionOfControl()
        {
            base.initialiseInversionOfControl();
        }

        protected override void initialCustomComponentsWithInversionOfControl(Container container)
        {
            base.initialCustomComponentsWithInversionOfControl(container);

            CS.General_CS_v6.Util.InversionUtil.RegisterAllTypesInAssemblyWhichEndWithTheWordService(typeof(PayMammoth_v6.Connector.Util.JsonUtil).Assembly);
        }

        protected override void checkAndCreateData()
        {
            base.checkAndCreateData();
            ContentTextService.Instance.CreateAllContentTextsDefinedbyEnum(typeof(PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText));
            EmailTemplateService.Instance.CreateAllEmailTemplatesForEnum(typeof(PayMammoth_v6.Enums.PayMammoth_v6Enums.EmailTemplate));

        }

        //todo: [For: Mark | 2014/07/17] This was commented out as it couldn't be overriden anymore (WrittenBy: Karl)        			
        //public override List<Type> GetAllRouteEnumTypes()
        //{
        //    var types = base.GetAllRouteEnumTypes();
        //    types.Add(typeof(PayMammoth_v6.Enums.PayMammoth_v6Enums.Section));
        //    return types;
        //}
        static AppInstancePayMammoth_v6()
        {
            //Instance = new AppInstancePayMammoth_v6();
        }

       
        public override string GetDeployApplicationName()
        {
            return "PayMammoth_v6Deploy";
        }
        



    }
}
