﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Modules.Services.ContentTexts;
using BusinessLogic_CS_v5.Modules.Services.Emails.EmailTemplates;
using Microsoft.ApplicationServer.Caching;

namespace PayMammoth_v2.Framework.Application
{
    public class AppInstanceSpecific : CS.MvcGeneral_CS_v5.Code.Framework.Application.AppInstanceMvcGeneralv4
    {
        public new static AppInstanceSpecific Instance
        {
            get { return (AppInstanceSpecific)CS.General_CS_v5.Modules.Application.AppInstanceGeneral.Instance; }
            protected set { CS.General_CS_v5.Modules.Application.AppInstanceGeneral.Instance = value; }
        }
        
        protected override void addAssemblies()
        {
            this.ProjectAssemblies.Add(typeof(PayMammoth_v2.Framework.Application.AppInstanceSpecific).Assembly);
            base.addAssemblies();
        }
        
        public static void Initialise()
        {
            
        }
        protected override void onApplicationStart()
        {
            base.onApplicationStart();
         
        }
      
        protected override void checkAndCreateData()
        {
            base.checkAndCreateData();
            ContentTextService.Instance.CreateAllContentTextsDefinedbyEnum(typeof(PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText));
            EmailTemplateService.Instance.CreateAllEmailTemplatesForEnum(typeof(PayMammoth_v2.Enums.PayMammoth_v2Enums.EmailTemplate));

        }
        static AppInstanceSpecific()
        {
            Instance = new AppInstanceSpecific();
        }


        public override string GetDeployApplicationName()
        {
            return "PayMammoth_v2Deploy";
        }

        protected override void addCmsAssemblies()
        {
            
        }
    }
}
