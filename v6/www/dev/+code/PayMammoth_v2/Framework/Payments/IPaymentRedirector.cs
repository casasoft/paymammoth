﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using Raven.Client;

namespace PayMammoth_v6.Framework.Payments
{
    public interface IPaymentRedirector
    {
        RedirectionResult Redirect(ReferenceLink<PaymentRequestData> paymentRequestId, IDocumentSession session);
    }
}
