﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6.Framework.Payments
{
    public class RedirectionResult
    {
        public string UrlToRedirectTo { get; set; }
        public CS.General_CS_v6.Enums.HrefTarget RedirectionType { get; set; }
        public bool Success { get; set; }

    }
}
