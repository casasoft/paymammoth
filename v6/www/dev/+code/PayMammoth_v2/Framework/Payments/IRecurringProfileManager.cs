﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.RecurringProfiles;
using PayMammoth_v6.Connector.HelperClasses;
using Raven.Client;

namespace PayMammoth_v6.Framework.Payments
{
    public interface IRecurringProfileManager
    {
        SetupRecurringProfileResult SetupRecurringProfile(ReferenceLink<RecurringProfileData> recurringProfileDataId,
            ReferenceLink<PaymentRequestTransactionData> transactionId, IDocumentSession session);
    }
}
