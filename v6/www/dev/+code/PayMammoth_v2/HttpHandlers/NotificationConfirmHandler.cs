﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CS.General_CS_v6.Util;
using PayMammoth_v6.Modules.Services.Notifications;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.HttpHandlers
{
    /// <summary>
    /// This handler is used by PAYMAMMOTH, to confirm back any notifications sent to the client
    /// </summary>
    public class NotificationConfirmHandler : IHttpHandler

    {
        private readonly INotificationParseRawConfirmationResponseService _notificationParseRawConfirmationResponseService;

        public NotificationConfirmHandler()
        {
            _notificationParseRawConfirmationResponseService = InversionUtil.Get<INotificationParseRawConfirmationResponseService>();
        }

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            string response = "ERROR";
            string data = CS.General_CS_v6.Util.StreamUtil.ReadStreamAsStringToEnd(context.Request.InputStream);

            if (!string.IsNullOrWhiteSpace(data))
            {


                var result = _notificationParseRawConfirmationResponseService.ParseRawConfirmationResponse(data);
                if (result == EnumsPayMammothConnector.NotificationConfirmationStatus.OK)
                {
                    response = PayMammoth_v6.Connector.Constants.RESPONSE_OK;
                }
                else
                {
                    response = result.ToString();
                }
                
                
            }

            context.Response.Write(response);
            context.Response.Flush();
            context.Response.End();
        }
        
    }
}
