﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6.Presentation.Services.Transactions
{
    public class TransactionResultDataModel
    {
        public PayMammoth_v6.Enums.PayMammoth_v6Enums.RedirectionResultStatus Status { get; set; }
        public string RedirectUrl { get; set; }
    }
}
