﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Services.Sections;
using CS.General_CS_v6.Modules.InversionOfControl;
using CS.General_CS_v6.Modules.Urls;
using CS.General_CS_v6.Util;
using PayMammoth_v6.KarlTestUI.Models;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using Raven.Client;

namespace PayMammoth_v6.Presentation.Services.Payments
{
    public interface IPaymentMethodModelService
    {
        PaymentMethodModel ConvertPaymentRequestDataModelToModel(
            IDocumentSession documentSession,
            CultureModel culture,
            PaymentMethodDataModel paymentMethodDataModel,
            string paymentRequestId,
            string fakePaymentId);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentMethodModelService : IPaymentMethodModelService
    {
        private readonly ISectionDataPresentationService _sectionModelService;

        public PaymentMethodModelService(ISectionDataPresentationService sectionModelService)
        {
            _sectionModelService = sectionModelService;
        }

        public PaymentMethodModel ConvertPaymentRequestDataModelToModel(
            IDocumentSession documentSession,
            CultureModel culture,
            PaymentMethodDataModel paymentMethodDataModel,
            string paymentRequestId,
            [AllowNull] string fakePaymentId)
        {
            string url = _sectionModelService.GetSectionUrlWithRouteValuesFromIdentifier(
                documentSession,
                culture,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Selection_Handler,
                routeValues: null);
            UrlHandler urlHandler = new UrlHandler(url);
            urlHandler.QueryString[PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_PaymentMethod] =
                ConversionUtil.ConvertBasicDataTypeToString(paymentMethodDataModel.PaymentMethodType);
            if (!string.IsNullOrWhiteSpace(fakePaymentId))
            {
                urlHandler.QueryString[PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_FakePaymentId] = fakePaymentId;
            }
            urlHandler.QueryString[PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER] = paymentRequestId;
            string urlPayment = urlHandler.GetUrl();
            PaymentMethodModel model = new PaymentMethodModel() {PaymentMethodDataModel = paymentMethodDataModel, Href = urlPayment};
            return model;
        }
    }
}