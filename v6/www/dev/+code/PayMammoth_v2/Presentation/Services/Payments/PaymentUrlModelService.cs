﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Services.Sections;
using BusinessLogic_CS_v6.Presentation.Services.Url;
using CS.General_CS_v6.Modules.InversionOfControl;
using CS.General_CS_v6.Modules.Pages;
using CS.General_CS_v6.Modules.Urls;
using Raven.Client;

namespace PayMammoth_v6.Presentation.Services.Payments
{
    public interface IPaymentUrlModelService
    {
        string GetPaymentSelectionPage(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestId);

        string GetPayPalConfirmationPageUrl(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestId);

        
        string GetTransactiumStatusPageUrl(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestId,
            string transactionId
            );

        string GetTransactiumPaymentPage(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestId,
            string redirectUrl);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentUrlModelService : IPaymentUrlModelService
    {
        private readonly ISectionDataPresentationService _sectionDataPresentationService;
        private readonly IUrlHandlerService _urlHandlerService;
        private readonly IPageService _pageService;

        public PaymentUrlModelService(
            ISectionDataPresentationService sectionDataPresentationService,
            IUrlHandlerService urlHandlerService,
            IPageService pageService)
        {
            _pageService = pageService;
            _urlHandlerService = urlHandlerService;
            _sectionDataPresentationService = sectionDataPresentationService;
        }

        private string addRequestAndTransactionIdToUrl(
            string url,
            string requestId,
            string transactionId
            )
        {
            return _urlHandlerService.UpdateUrlQuerystring(url,
                new NameValueCollection()
                {
                    {PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER, requestId},
                    {PayMammoth_v6.Connector.Constants.PARAM_TransactionId, transactionId}
                });

            /*
            UrlHandler urlHandler = new UrlHandler(url);
            urlHandler.QueryString[PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER] = requestId;
            urlHandler.QueryString[PayMammoth_v6.Connector.Constants.PARAM_TransactionId] = transactionId;
            return urlHandler.GetUrl(true);*/
        }

        //public string GetPaymentSelectionPage(
        //    IDocumentSession documentSession,
        //    CultureModel culture,
        //    string requestId)
        //{
        //    string url = _sectionModelService.GetSectionUrlWithRouteValuesFromIdentifier(documentSession, culture,
        //        PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Selection);
        //    url = addRequestIdToUrl(url, requestId);
        //    return url;
        //}
        public string GetTransactiumPaymentPage(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestId,
            string redirectUrl)
        {
            string url = _sectionDataPresentationService.GetSectionUrlWithRouteValuesFromIdentifier(
                documentSession,
                culture,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Payment_Transactium);
            url = addRequestAndTransactionIdToUrl(
                url,
                requestId,
                transactionId:null
                );

            UrlHandler urlHandler = new UrlHandler(url);
            urlHandler.QueryString[PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_Transactium_RedirectUrl] = redirectUrl;
            url = urlHandler.GetUrl();

            return url;
        }

        public string GetPayPalConfirmationPageUrl(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestId)
        {
            string url = _sectionDataPresentationService.GetSectionUrlWithRouteValuesFromIdentifier(
                documentSession,
                culture,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Payment_PaypalConfirmation);
            url = addRequestAndTransactionIdToUrl(
                url,
                requestId,
                transactionId: null);
            return url;
        }

        public string GetPayPalConfirmationPageUrl_OLD(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestId,
            string token,
            string payerId)
        {
            string url = _sectionDataPresentationService.GetSectionUrlWithRouteValuesFromIdentifier(
                documentSession,
                culture,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Payment_PaypalConfirmation);
            url = addRequestAndTransactionIdToUrl(
                url,
                requestId,
                transactionId: null);

            UrlHandler urlHandler = new UrlHandler(url);
            urlHandler.QueryString[PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_Token] = token;
            urlHandler.QueryString[PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_PayerId] = payerId;
            url = urlHandler.GetUrl();
            return url;
        }

        /// <summary>
        /// This must return a FULLY QUALIFIED URL, not relative.
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="culture"></param>
        /// <param name="requestId"></param>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public string GetTransactiumStatusPageUrl(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestId,
            string transactionId)
        {
            string url = _sectionDataPresentationService.GetSectionUrlWithRouteValuesFromIdentifier(
                documentSession,
                culture,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Payment_Transactium_Status);
            url = addRequestAndTransactionIdToUrl(
                url,
                requestId: requestId,
                transactionId:transactionId);
            url = _pageService.ConvertRelativeUrlToAbsoluteUrl(url);
            return url;
        }

        public string GetPaymentSelectionPage(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestId)
        {
            string url = _sectionDataPresentationService.GetSectionUrlWithRouteValuesFromIdentifier(
                documentSession,
                culture,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Selection);
            url = addRequestAndTransactionIdToUrl(
                url,
                requestId,
                transactionId: null);

            return url;

        }
    }
}