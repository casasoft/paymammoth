﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Services.Address;
using BusinessLogic_CS_v6.Presentation.Services.Contact;
using BusinessLogic_CS_v6.Presentation.Services.ContentTexts;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v6.Presentation.Models._Shared.PartialViews.Payments;
using Raven.Client;

namespace PayMammoth_v6.Presentation.Services.Payments
{
    public interface IPaymentDetailsModelService
    {
        _PaymentDetailsModel CreatePaymentDetailsModel(
            IDocumentSession documentSession,
            CultureModel culture,
            PaymentUserDetailsDataModel userDetails);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentDetailsModelService : IPaymentDetailsModelService
    {
        private readonly IContactDetailsPresentationService _contactDetailsModelService;
        private readonly IAddressDetailsPresentationService _addressDetailsModelService;
        private readonly IContentTextPresentationService _contentTextModelService;

        public PaymentDetailsModelService(
            IContactDetailsPresentationService contactDetailsModelService,
            IAddressDetailsPresentationService addressDetailsModelService,
            IContentTextPresentationService contentTextModelService)
        {
            _contentTextModelService = contentTextModelService;
            _addressDetailsModelService = addressDetailsModelService;
            _contactDetailsModelService = contactDetailsModelService;
        }

        public _PaymentDetailsModel CreatePaymentDetailsModel(
            IDocumentSession documentSession,
            CultureModel culture,
            PaymentUserDetailsDataModel userDetails)
        {
            return new _PaymentDetailsModel()
            {
                FullName = _contactDetailsModelService.GetFullNameFromContactDetails(userDetails.ContactDetailsModel),
                AddressAsOneLineHtml = _addressDetailsModelService.GetAddressDetailsModelAsHtml(
                    documentSession,
                    culture,
                    userDetails.ContactDetailsModel.Address),
                TitleContentText = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    documentSession,
                    culture,
                    PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.PaymentDetails_Title),
                YourDetailsContentTextModel = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    documentSession,
                    culture,
                    PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.PaymentDetails_YourDetails),
                UserDetails = userDetails
            };
        }
    }
}