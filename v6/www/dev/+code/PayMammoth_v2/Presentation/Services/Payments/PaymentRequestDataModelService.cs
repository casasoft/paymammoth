﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.ContactDetails;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Fields;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.MediaItems;
using BusinessLogic_CS_v6.Presentation.Services.Bundling;
using BusinessLogic_CS_v6.Util;
using BusinessLogic_CS_v6.Util.Helpers;
using CS.General_CS_v6.Classes.HelperClasses;
using CS.General_CS_v6.Modules.Attributes;
using CS.General_CS_v6.Modules.InversionOfControl;
using Microsoft.SqlServer.Server;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.Modules.Services.WebsiteAccounts;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v6.Presentation.Models._Shared.Data.WebsiteAccounts;
using PayMammoth_v6.Presentation.Services.Models;
using PayMammoth_v6.Presentation.Services.Transactions;
using PayMammoth_v6.Connector;
using PayPal.OpenIdConnect;
using Raven.Client;

namespace PayMammoth_v6.Presentation.Services.Payments
{
    public enum GetPaymentMethodRequestForPaymentRequestIdStatus
    {
        Success,

        [ContentTextDataDefaultValues("Invalid or no request identifier present")]
        ValidRequestIdNotPresent,

        [ContentTextDataDefaultValues(@"
<p>Your request has expired due to inactivity.</p>
<p>Please <a href=''>restart the process</a> and if your have any problems, kindly contact us.</p>")]
        Expired,
        Error
    }

    public enum CancelPaymentStatus
    {
        Success,

        [ContentTextDataDefaultValues("An error was encountered cancelling payment.  Please try again later and if problem persists, contact us.")]
        Error
    }

    public interface IPaymentRequestDataModelService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="cultureModel"></param>
        /// <param name="paymentRequestId"></param>
        /// <param name="fakePaymentKey">The Fake Payment ID if available</param>
        /// <returns></returns>
        StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel> GetPaymentMethodRequestForPaymentRequestId(
            IDocumentSession documentSession,
            CultureModel cultureModel,
            string paymentRequestId,
            string fakePaymentKey);

        TransactionResultDataModel GenerateTransactionResultFromPaymentMethod(
            IDocumentSession documentSession,
            CultureModel cultureModel,
            EnumsPayMammothConnector.PaymentMethodType paymentMethod,
            string paymentRequestId);

        /// <summary>
        /// Cancels a request
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="cultureModel"></param>
        /// <param name="paymentRequestId"></param>
        /// <returns>The URL to redirect to which should be the Cancel URL of the Website</returns>
        StatusResult<CancelPaymentStatus, string> CancelPaymentForPaymentRequestId(
            IDocumentSession documentSession,
            CultureModel cultureModel,
            string paymentRequestId);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentRequestDataModelService : IPaymentRequestDataModelService
    {
        private readonly IFillPaymentRequestDataModelFromPaymentRequestDataService _fillPaymentRequestDataModelFromPaymentRequestDataService;
        private readonly IPaymentRequestDataService _paymentRequestDataService;
        
        private readonly IPaymentRequestDataMarkerService _paymentRequestDataMarkerService;
        private readonly IGenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService _generateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService;
        private readonly IRavenDbService _ravenDbService;

        public PaymentRequestDataModelService(
            IFillPaymentRequestDataModelFromPaymentRequestDataService fillPaymentRequestDataModelFromPaymentRequestDataService,
            IPaymentRequestDataService paymentRequestDataService,
            IRavenDbService ravenDbService,
            IPaymentRequestDataMarkerService paymentRequestDataMarkerService,
            IGenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService generateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService)
        {
            _ravenDbService = ravenDbService;
            _generateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService = generateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService;
            _paymentRequestDataMarkerService = paymentRequestDataMarkerService;
            
            _paymentRequestDataService = paymentRequestDataService;
            _fillPaymentRequestDataModelFromPaymentRequestDataService = fillPaymentRequestDataModelFromPaymentRequestDataService;
        }

        public TransactionResultDataModel GenerateTransactionResultFromPaymentMethod(
            IDocumentSession documentSession,
            CultureModel cultureModel,
            EnumsPayMammothConnector.PaymentMethodType paymentMethod,
            string paymentRequestId)
        {
            var result = _generateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService.GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructions(
                paymentMethod,
                new ReferenceLink<PaymentRequestData>(paymentRequestId));

            TransactionResultDataModel model = new TransactionResultDataModel();
            model.RedirectUrl = result.UrlToRedirectTo;
            model.Status = result.Result;
            return model;
            //model.

            ////todo: [For: Backend | 2014/04/15] https://casasoft.atlassian.net/browse/PAYMFIVE-7 (WrittenBy: Mark)        			
            //if (paymentMethod == Connector.Enums.PaymentMethodType.PaymentGateway_Transactium)
            //{
            //    //Take to transactium page
            //    return new TransactionResultDataModel()
            //    {
            //        Status = PayMammoth_v6Enums.RedirectionResultStatus.Success,
            //        RedirectUrl = "/en/payment/transactium/?identifier=ABC"
            //    };
            //}
            //else
            //{
            //    return new TransactionResultDataModel()
            //    {
            //        Status = PayMammoth_v6Enums.RedirectionResultStatus.Error,
            //        RedirectUrl = "http://www.paypal.com"
            //    };
            //}
        }

        public TransactionResultDataModel GenerateTransactionResultFromPaymentMethod_old(
            IDocumentSession documentSession,
            CultureModel culture,
            EnumsPayMammothConnector.PaymentMethodType paymentMethod,
            string paymentRequestId)
        {
            //todo: [For: Backend | 2014/04/15] https://casasoft.atlassian.net/browse/PAYMFIVE-7 (WrittenBy: Mark)        			
            if (paymentMethod == EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Transactium)
            {
                //Take to transactium page
                return new TransactionResultDataModel() {Status = PayMammoth_v6Enums.RedirectionResultStatus.Success, RedirectUrl = "/en/payment/transactium/?identifier=ABC"};
            }
            else
            {
                return new TransactionResultDataModel() {Status = PayMammoth_v6Enums.RedirectionResultStatus.Error, RedirectUrl = "http://www.paypal.com"};
            }
        }

        public StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel> 
            GetPaymentMethodRequestForPaymentRequestId(
                                                        IDocumentSession documentSession,
                                                        CultureModel cultureModel,
                                                        string paymentRequestId,
                                                        [AllowNull] string fakePaymentKey)
        {
            
            var requestGetByIdParams = new GetByIdParams<PaymentRequestData>(paymentRequestId,
                ThrowErrorIfNotAlreadyLoadedInSession.No,
                LoadOnlyAvailableToFrontend.Yes
                );
            requestGetByIdParams.AddInclude(x => x.LinkedWebsiteAccountId);




            var paymentRequest = _ravenDbService.GetById(documentSession, requestGetByIdParams);

            var result = new StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>();
            result.Status = GetPaymentMethodRequestForPaymentRequestIdStatus.ValidRequestIdNotPresent;
            if (paymentRequest != null)
            {
                if (paymentRequest.PaymentStatus.Status == PaymentStatusInfo.PaymentStatus.Pending)
                {

                    var websiteAccount = _ravenDbService.GetById(
                        documentSession,
                        paymentRequest.LinkedWebsiteAccountId,
                        throwErrorIfNotAlreadyLoadedInSession: ThrowErrorIfNotAlreadyLoadedInSession.Yes,
                        loadOnlyAvailableToFrontend: LoadOnlyAvailableToFrontend.Yes);

                    if (websiteAccount != null)
                    {
                        result.Result = _fillPaymentRequestDataModelFromPaymentRequestDataService
                            .CreatePaymentRequestDataModelFromPaymentRequestData(
                                paymentRequest,
                                websiteAccount,
                                cultureModel,
                                documentSession,
                                fakePaymentKey);

                        result.Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Success;
                    }
                    else
                    {
                        result.Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Error;
                    }
                }
                else
                {
                    switch (paymentRequest.PaymentStatus.Status)
                    {
                        case PaymentStatusInfo.PaymentStatus.Expired: result.Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Expired; break;
                        default: result.Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Error; break;
                    }
                    result.Result.CancelUrl = paymentRequest.RequestDetails.ReturnUrls.CancelUrl;
                    
                }
            }
            return result;
        }

        public StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel> GetPaymentMethodRequestForPaymentRequestId_old(
            IDocumentSession documentSession,
            CultureModel culture,
            string paymentRequestId)
        {
            //todo: [For: Backend | 2014/04/10] https://casasoft.atlassian.net/browse/PAYMFIVE-6 (WrittenBy: Mark)        			
            return new StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>()
            {
                Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Success,
                Result =
                    new PaymentRequestDataModel()
                    {
                        PaymentRequestId = paymentRequestId,
                        WebsiteAccount =
                            new WebsiteAccountDataModel()
                            {
                                CssFile = "/uploads/css/jkvillas.css",
                                Logo =
                                    new MediaItemModel<WebsiteAccountData.WebsiteAccountDataLogoImageSize>()
                                    {
                                        MediaItems =
                                            new Dictionary<WebsiteAccountData.WebsiteAccountDataLogoImageSize, MediaItemDataModel>()
                                            {
                                                {WebsiteAccountData.WebsiteAccountDataLogoImageSize.Normal, new MediaItemDataModel() {ImageUrl = "/Content/images/tmp/logo.png"}}
                                            }
                                    },
                                WebsiteName = "JK Holidays"
                            },
                        PaymentMethodsAvailable =
                            new List<PaymentMethodDataModel>()
                            {
                                new PaymentMethodDataModel()
                                {
                                    Description = new FieldModel<string>() {Value = "Pay using PayPal"},
                                    Title = new FieldModel<string>() {Value = "PayPal"},
                                    Icon =
                                        new MediaItemModel<PaymentMethodData.PaymentMethodDataIconImageSize>()
                                        {
                                            MediaItems =
                                                new Dictionary<PaymentMethodData.PaymentMethodDataIconImageSize, MediaItemDataModel>()
                                                {
                                                    {PaymentMethodData.PaymentMethodDataIconImageSize.Normal, new MediaItemDataModel() {ImageUrl = "/content/images/tmp/paypal.png"}}
                                                }
                                        },
                                    PaymentMethodType = EnumsPayMammothConnector.PaymentMethodType.PayPal_ExpressCheckout
                                },
                                new PaymentMethodDataModel()
                                {
                                    Description = new FieldModel<string>() {Value = "Pay using VISA, MasterCard or BOV Cashlink"},
                                    Title = new FieldModel<string>() {Value = "PayPal"},
                                    Icon =
                                        new MediaItemModel<PaymentMethodData.PaymentMethodDataIconImageSize>()
                                        {
                                            MediaItems =
                                                new Dictionary<PaymentMethodData.PaymentMethodDataIconImageSize, MediaItemDataModel>()
                                                {
                                                    {PaymentMethodData.PaymentMethodDataIconImageSize.Normal, new MediaItemDataModel() {ImageUrl = "/content/images/tmp/credit-cards.png"}}
                                                }
                                        },
                                    PaymentMethodType = EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Transactium
                                }
                            },
                        OrderSummary =
                            new PaymentOrderSummaryDataModel()
                            {
                                DescriptionHtml = "<p>This is a sample order description</p>",
                                FullDetailsUrl = "http://www.google.com",
                                OrderCurrency =
                                    new Iso4217_CurrencyDataModel()
                                    {
                                        Identifier = "GBP",
                                        Symbol = "£",
                                        ExchangeRate = 1.343m
                                    },
                                Title = "Stereo",
                                Items =
                                    new List<PaymentOrderSummaryItemDataModel>()
                                    {
                                        new PaymentOrderSummaryItemDataModel() {Title = "Stereo Full On", DescriptionHtml = "<p>An amazing stereo:</p><ul><li>test 1</li><li>test 2</li></ul>"},
                                        new PaymentOrderSummaryItemDataModel() {Title = "Stereo Full Off", DescriptionHtml = "<p>An <a href='#'>amazing</a> stereo:</p><ul><li>test 34</li></ul>"}
                                    },
                                Total = 510,
                                TotalHandling = 10,
                                TotalNet = 400,
                                TotalTax = 100
                            },
                        UserDetails =
                            new PaymentUserDetailsDataModel()
                            {
                                ContactDetailsModel =
                                    new ContactDetailsWithAddressDataModel()
                                    {
                                        Name = new FieldModel<string>() {Value = "Mark"},
                                        Surname = new FieldModel<string>() {Value = "Cassar"},
                                        EmailAddress = new FieldModel<string>() {Value = "markcassar@outlook.com"},
                                        Fax = new FieldModel<string>() {Value = "+356 21366666"},
                                        Mobile = new FieldModel<string>() {Value = "+356 79046946"},
                                        Telephone = new FieldModel<string>() {Value = "+32546 15614136"},
                                        Website = new FieldModel<string>() {Value = "www.casasoft.com.mt"},
                                        Address =
                                            new AddressDetailsModel()
                                            {
                                                Address1 = new FieldModel<string>() {Value = "20, Cor Jesu"},
                                                Address2 = new FieldModel<string>() {Value = "Triq il-Kittenija"},
                                                CountryDataModel = new Iso3166_CountryDataModel(){Identifier = "MLT", TwoLetterCode = "mt", Title = new FieldModel<string>(){Value = "Malta"}},
                                                Locality = new FieldModel<string>() {Value = "Zurrieq"},
                                                PostCode = new FieldModel<string>() {Value = "ZRQ 4020"}
                                            }
                                    },
                            }
                    }
            };
        }

        public StatusResult<CancelPaymentStatus, string> CancelPaymentForPaymentRequestId(
            IDocumentSession documentSession,
            CultureModel cultureModel,
            string paymentRequestId)
        {
            var statusResult = new StatusResult<CancelPaymentStatus, string>();
            statusResult.Status = CancelPaymentStatus.Error;
            var paymentRequestData = _paymentRequestDataMarkerService.MarkPaymentRequestAsCancelledAndSendNotification(new ReferenceLink<PaymentRequestData>(paymentRequestId));
            statusResult.Status = CancelPaymentStatus.Success;
            statusResult.Result = paymentRequestData.RequestDetails.ReturnUrls.CancelUrl;
            return statusResult;
        }
    }
}