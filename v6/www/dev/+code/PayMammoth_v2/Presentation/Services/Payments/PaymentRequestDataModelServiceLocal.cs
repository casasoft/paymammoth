﻿using System;
using System.Collections.Generic;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.ContactDetails;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Fields;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.MediaItems;
using CS.General_CS_v6.Classes.HelperClasses;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v6.Presentation.Models._Shared.Data.WebsiteAccounts;
using PayMammoth_v6.Presentation.Services.Models;
using PayMammoth_v6.Presentation.Services.Transactions;
using Raven.Client;

namespace PayMammoth_v6.Presentation.Services.Payments
{

    [IocComponent(Priority = -100)]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentRequestDataModelServiceLocal : IPaymentRequestDataModelService
    {
        private readonly IFillPaymentRequestDataModelFromPaymentRequestDataService
            _fillPaymentRequestDataModelFromPaymentRequestDataService;


        public PaymentRequestDataModelServiceLocal(IFillPaymentRequestDataModelFromPaymentRequestDataService 
            fillPaymentRequestDataModelFromPaymentRequestDataService)
        {
            _fillPaymentRequestDataModelFromPaymentRequestDataService =
                fillPaymentRequestDataModelFromPaymentRequestDataService;
        }

        public StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>
            GetPaymentMethodRequestForPaymentRequestId(
            IDocumentSession documentSession,
            CultureModel cultureModel,
            string paymentRequestId,
            [AllowNull] string fakePaymentKey)
        {
            if (!string.IsNullOrWhiteSpace(paymentRequestId))
            {
                var result = new StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>
                    ()
                {
                    Result =
                        new PaymentRequestDataModel()
                        {
                            PaymentRequestId = paymentRequestId,
                            FakePaymentKey = fakePaymentKey,
                            FakePaymentsEnabled = !string.IsNullOrWhiteSpace(fakePaymentKey),
                            PaymentMethodsAvailable =
                                new List<PaymentMethodDataModel>()
                                {
                                    new PaymentMethodDataModel()
                                    {
                                        PaymentMethodType =
                                            EnumsPayMammothConnector.PaymentMethodType.PayPal_ExpressCheckout,
                                        Icon = new MediaItemModel<PaymentMethodData.PaymentMethodDataIconImageSize>(),
                                        Title = new FieldModel<string>() {Value = "PayPal"},
                                        Description = new FieldModel<string>() {Value = "desc here"}
                                    }
                                },
                            OrderSummary =
                                new PaymentOrderSummaryDataModel()
                                {
                                    OrderCurrency = new Iso4217_CurrencyDataModel()
                                    {
                                        Identifier = "EUR",
                                        Title = new FieldModel<string>() {Value = "Euro"},
                                        Symbol = "€",
                                        ExchangeRate = 1.443m
                                    },
                                    Items =
                                        new List<PaymentOrderSummaryItemDataModel>()
                                        {
                                            new PaymentOrderSummaryItemDataModel()
                                            {
                                                Title = "My Item",
                                                DescriptionHtml = "Decription"
                                            }
                                        }
                                },
                            CancelUrl = "http://www.cancel.com",
                            UserDetails =
                                new PaymentUserDetailsDataModel()
                                {
                                    ContactDetailsModel =
                                        new ContactDetailsWithAddressDataModel()
                                        {
                                            Address =
                                                new AddressDetailsModel()
                                                {
                                                    CountryDataModel =
                                                        new Iso3166_CountryDataModel()
                                                        {
                                                            TwoLetterCode = "MT",
                                                            Title = new FieldModel<string>() {Value = "Malta"},
                                                            Identifier = "MT"
                                                        }
                                                }
                                        }
                                },
                            PaymentExpiryTimespan = new TimeSpan(0, 0, 31, 0),
                            WebsiteAccount =
                                new WebsiteAccountDataModel()
                                {
                                    Logo = new MediaItemModel<WebsiteAccountData.WebsiteAccountDataLogoImageSize>()
                                    {
                                        MediaItems =
                                            new Dictionary
                                                <WebsiteAccountData.WebsiteAccountDataLogoImageSize, MediaItemDataModel>
                                                ()
                                            {
                                                {
                                                    WebsiteAccountData.WebsiteAccountDataLogoImageSize.Normal,
                                                    new MediaItemDataModel()
                                                    {
                                                        ImageUrl = "/Content/images/tmp/logo.png"
                                                    }
                                                }
                                            }
                                    }
                                }
                        },
                    Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Success
                };

                if (paymentRequestId == "Expired")
                {
                    result.Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Expired;
                    result.Result.CancelUrl = "http://www.cancel.com";
                }
                return result;
            }
            else
            {
                return new StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>()
                {
                    Status = GetPaymentMethodRequestForPaymentRequestIdStatus.ValidRequestIdNotPresent
                };
            }
        }

        public Transactions.TransactionResultDataModel GenerateTransactionResultFromPaymentMethod(IDocumentSession documentSession, CultureModel cultureModel, Connector.EnumsPayMammothConnector.PaymentMethodType paymentMethod, string paymentRequestId)
        {
            return new TransactionResultDataModel()
            {
                Status = PayMammoth_v6Enums.RedirectionResultStatus.Success,
                RedirectUrl =
                    "https://www.sandbox.paypal.com/cgi-bin/webscr?token=EC-68214744R89530450&cmd=_express-checkout"
            };
        }

        public StatusResult<CancelPaymentStatus, string> CancelPaymentForPaymentRequestId(
            IDocumentSession documentSession,
            CultureModel cultureModel,
            string paymentRequestId)
        {
            var status = new StatusResult<CancelPaymentStatus, string>()
            {
                Status = paymentRequestId == "Error" ? CancelPaymentStatus.Error : CancelPaymentStatus.Success
            };
            if (status.Status == CancelPaymentStatus.Success)
            {
                status.Result = "http://www.cancel.com";
            }
            return status;
        }


        
    }
}