﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Aspects;
using BusinessLogic_CS_v6.Aspects.NullAspects;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Framework.MediaItems.Services;
using BusinessLogic_CS_v6.Modules.Data._Shared.BaseObjectWithIdentifiers;
using BusinessLogic_CS_v6.Modules.Services._Shared.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.ContactDetails;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Locations;
using BusinessLogic_CS_v6.Presentation.Services.Contact;
using BusinessLogic_CS_v6.Presentation.Services.Fields;
using BusinessLogic_CS_v6.Presentation.Services.MediaItems;
using BusinessLogic_CS_v6.Presentation.Services._Shared.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Modules.InversionOfControl;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.FakePayments;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v6.Presentation.Models._Shared.Data.WebsiteAccounts;
using Raven.Client;
using Raven.Client.Document;

namespace PayMammoth_v6.Presentation.Services.Models
{
	public interface IFillPaymentRequestDataModelFromPaymentRequestDataService
	{
		PaymentRequestDataModel CreatePaymentRequestDataModelFromPaymentRequestData(
			PaymentRequestData paymentRequestData,
			WebsiteAccountData websiteAccountData,
			CultureModel CultureModel,
			IDocumentSession documentSession,
			[AllowNull] string fakePaymentKey);
	}

	[IocComponent]
	[LogAspect]
	[EnsureNonNullAspect]
	public class FillPaymentRequestDataModelFromPaymentRequestDataService : IFillPaymentRequestDataModelFromPaymentRequestDataService
	{
		private readonly IGetTotalPriceForRequestService _getTotalPriceForRequestService;
		private readonly IPaymentRequestDataService _paymentRequestDataService;
		private readonly IBaseObjectFieldPresentationService _baseObjectFieldModelService;
		private readonly IMediaItemPresentationService _mediaItemModelService;
		private readonly IMediaItemService _mediaItemService;
		private readonly ICheckIfFakePaymentEnabledAndKeyIsCorrectService _checkIfFakePaymentEnabledAndKeyIsCorrectService;
		private readonly IIso3166_CountryDataPresentationService _iso3166CountryDataPresentationService;
		private readonly IIso4217_CurrencyDataPresentationService _iso4217CurrencyDataPresentationService;
		private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;

		public FillPaymentRequestDataModelFromPaymentRequestDataService(
			IGetTotalPriceForRequestService getTotalPriceForRequestService,
			IPaymentRequestDataService paymentRequestDataService,
			IBaseObjectFieldPresentationService baseObjectFieldModelService,
			IMediaItemPresentationService mediaItemModelService,
			IMediaItemService mediaItemService,
			ICheckIfFakePaymentEnabledAndKeyIsCorrectService checkIfFakePaymentEnabledAndKeyIsCorrectService,
			IIso3166_CountryDataPresentationService iso3166CountryDataPresentationService,
			IIso4217_CurrencyDataPresentationService iso4217CurrencyDataPresentationService,
			ICurrentDateTimeRetrieverService currentDateTimeRetrieverService
			)
		{
			_currentDateTimeRetrieverService = currentDateTimeRetrieverService;
			_iso4217CurrencyDataPresentationService = iso4217CurrencyDataPresentationService;
			_iso3166CountryDataPresentationService = iso3166CountryDataPresentationService;
			_checkIfFakePaymentEnabledAndKeyIsCorrectService = checkIfFakePaymentEnabledAndKeyIsCorrectService;
			_mediaItemService = mediaItemService;
			_mediaItemModelService = mediaItemModelService;
			_baseObjectFieldModelService = baseObjectFieldModelService;
			_paymentRequestDataService = paymentRequestDataService;
			_getTotalPriceForRequestService = getTotalPriceForRequestService;
		}

		public PaymentRequestDataModel CreatePaymentRequestDataModelFromPaymentRequestData(
			PaymentRequestData paymentRequestData,
			WebsiteAccountData websiteAccountData,
			CultureModel CultureModel,
			IDocumentSession documentSession,
			[AllowNull] string fakePaymentKey)
		{
			var model = new PaymentRequestDataModel();
			model.OrderSummary = new PaymentOrderSummaryDataModel();
			model.OrderSummary.DescriptionHtml = paymentRequestData.RequestDetails.Details.Description;
			model.OrderSummary.FullDetailsUrl = paymentRequestData.RequestDetails.OrderLinkOnClientWebsite;

			
			

			fillOrderSummaryItems(
				paymentRequestData,
				model);
			fillOrderSummaryCurrency(
				CultureModel,
				paymentRequestData,
				model);
			model.OrderSummary.Title = paymentRequestData.RequestDetails.Details.Title;

			var totalPriceIncTaxes = _getTotalPriceForRequestService.GetTotalPrice(
				paymentRequestData,
				includeTaxes: true,
				includeShippingAndHandling: true);
			var totalPriceExcTaxes = _getTotalPriceForRequestService.GetTotalPrice(
				paymentRequestData,
				includeTaxes: false,
				includeShippingAndHandling: true);
			var taxesOnly = totalPriceIncTaxes - totalPriceExcTaxes;

			model.OrderSummary.Total = _getTotalPriceForRequestService.GetTotalPrice(
				paymentRequestData,
				true,
				includeShippingAndHandling: true);
			model.OrderSummary.TotalHandling = paymentRequestData.RequestDetails.Pricing.HandlingAmount;
			model.OrderSummary.TotalNet = totalPriceExcTaxes;
			model.OrderSummary.TotalShipping = paymentRequestData.RequestDetails.Pricing.ShippingAmount;
			model.OrderSummary.TotalTax = taxesOnly;

			fillPaymentMethods(
				paymentRequestData,
				documentSession,
				model,
				CultureModel);

			

			model.PaymentRequestId = paymentRequestData.Id;

			fillUserDetails(CultureModel,model,paymentRequestData);

			fillWebsiteAccount(model,paymentRequestData,websiteAccountData);
			//todo: [For: Backend | 2014/09/16] https://casasoft.atlassian.net/browse/PAYMFIVE-71 (WrittenBy: Mark)        			
			//model.PaymentExpiryDate = paymentRequestData.RequestDetails.ExpirationInfo.ExpiresAt;

			model.FakePaymentKey = fakePaymentKey;
			model.FakePaymentsEnabled = _checkIfFakePaymentEnabledAndKeyIsCorrectService.CheckIfFakePaymentEnabledAndKeyIsCorrect(
				websiteAccountData,
				fakePaymentKey);

			if (paymentRequestData.RequestDetails.ExpirationInfo.ExpiresAt.HasValue)
			{
				var nowTime = _currentDateTimeRetrieverService.GetCurrentDateTime();
				model.PaymentExpiryTimespan = paymentRequestData.RequestDetails.ExpirationInfo.ExpiresAt.Value.Subtract(nowTime);
			}


			return model;
		}

		private void fillWebsiteAccount(
			PaymentRequestDataModel model,
			PaymentRequestData paymentRequest,
			WebsiteAccountData websiteAccountData)
		{
			model.WebsiteAccount = new WebsiteAccountDataModel();
			if (websiteAccountData.CustomCssFile != null && !websiteAccountData.CustomCssFile.IsEmpty())
			{
				model.WebsiteAccount.CssFile = _mediaItemService.GetWebPathForOriginalFile(websiteAccountData.CustomCssFile);
			}
			model.WebsiteAccount.Logo = _mediaItemModelService.ConvertMediaItemDataToMediaItemModel(
				websiteAccountData,
				websiteAccountData.Logo,
				x => x.Logo);
			model.WebsiteAccount.WebsiteName = websiteAccountData.WebsiteName;
		}

		private void fillUserDetails(
			CultureModel cultureModel,
			PaymentRequestDataModel model,
			PaymentRequestData paymentRequest)
		{
			model.UserDetails = new PaymentUserDetailsDataModel();
			fillUserDetails_ContactDetails(cultureModel,model,paymentRequest);
		}

		private void fillUserDetails_ContactDetails(
			CultureModel cultureModel,
			PaymentRequestDataModel model,
			PaymentRequestData paymentRequest)
		{
			var contactDetailsModel = new ContactDetailsWithAddressDataModel();
			model.UserDetails.ContactDetailsModel = contactDetailsModel;

			contactDetailsModel.Address = new AddressDetailsModel();
			contactDetailsModel.Address.Address1 = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.Address1,
				paymentRequest.RequestDetails.Details.ClientContactDetails.Address1);

			contactDetailsModel.Address.Address2 = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.Address2,
				paymentRequest.RequestDetails.Details.ClientContactDetails.Address2);

			contactDetailsModel.Address.CountryDataModel = _iso3166CountryDataPresentationService.GetIso3166_CountryDataModelById(
				cultureModel,
				new ReferenceLink<Iso3166_CountryData>(paymentRequest.RequestDetails.Details.ClientContactDetails.Country3LetterCode),
				ThrowErrorIfNotAlreadyLoadedInSession.Yes);

			contactDetailsModel.Address.Locality = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.Locality,
				paymentRequest.RequestDetails.Details.ClientContactDetails.Locality);

			//todo: [For: Karl | 2014/04/29] why exactly is this needed - LocalityDataModel? (WrittenBy: Karl)    
			//Alan - because we have 2 ways to set a locality
			// 1 - LinkedLocalityId
			// 2 - Hardcoded as string Locality;
			// If its 1) - then we need to load LinkedLocalityId into its model.
			contactDetailsModel.Address.LocalityDataModel = new LocalityDataModel();

			contactDetailsModel.Address.PostCode = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.PostCode,
				paymentRequest.RequestDetails.Details.ClientContactDetails.PostCode);

			contactDetailsModel.Address.State = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.State,
				paymentRequest.RequestDetails.Details.ClientContactDetails.State);


			model.UserDetails.ContactDetailsModel.Name = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.Name,
				paymentRequest.RequestDetails.Details.ClientContactDetails.Name);

			model.UserDetails.ContactDetailsModel.Mobile = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.Mobile,
				paymentRequest.RequestDetails.Details.ClientContactDetails.Mobile);

			model.UserDetails.ContactDetailsModel.Surname = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.LastName,
				paymentRequest.RequestDetails.Details.ClientContactDetails.LastName);

			model.UserDetails.ContactDetailsModel.Telephone = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
			   cultureModel,
			   paymentRequest,
			   x => paymentRequest.RequestDetails.Details.ClientContactDetails.Telephone,
			   paymentRequest.RequestDetails.Details.ClientContactDetails.Telephone);

			

		}

		private void fillPaymentMethods(
			PaymentRequestData paymentRequest,
			IDocumentSession session,
			PaymentRequestDataModel model,
			CultureModel cultureModel)
		{
			model.PaymentMethodsAvailable = new List<PaymentMethodDataModel>();
			var availablePaymentMethods = _paymentRequestDataService.GetAllPaymentMethodsAvailableForPayment(
				paymentRequest,
				session);

			foreach (var paymentMethod in availablePaymentMethods)
			{
				var paymentMethodModel = new PaymentMethodDataModel();

				paymentMethodModel.Description = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
					cultureModel,
					paymentMethod,
					x => x.Description,
					paymentMethod.Description[cultureModel]);

				paymentMethodModel.Icon = _mediaItemModelService.ConvertMediaItemDataToMediaItemModel(
					paymentMethod,
					paymentMethod.Icon,
					x => x.Icon);

				paymentMethodModel.PaymentMethodType = paymentMethod.PaymentMethod;

				paymentMethodModel.Title = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
					cultureModel,
					paymentMethod,
					x => x.Title,
					paymentMethod.Title[cultureModel]);

				model.PaymentMethodsAvailable.Add(paymentMethodModel);
			}
		}

		private void fillOrderSummaryCurrency(
			CultureModel CultureModel,
			PaymentRequestData paymentRequest,
			PaymentRequestDataModel model)
		{
			var currency = _iso4217CurrencyDataPresentationService
				.GetCurrencyModelByCurrencyCode(CultureModel, paymentRequest.RequestDetails.Pricing.CurrencyCode3Letter);
			if (currency == null) throw new InvalidOperationException("Currency cannot be null.  Code: '" + paymentRequest.RequestDetails.Pricing.CurrencyCode3Letter + "'");
			model.OrderSummary.OrderCurrency = currency;

			//model.OrderSummary.OrderCurrency = new Iso4217_CurrencyDataModel();
			//model.OrderSummary.OrderCurrency.Identifier = paymentRequest.RequestDetails.Pricing.CurrencyCode3Letter;
			//model.OrderSummary.OrderCurrency.CurrencyCode = currency.Value;
			//model.OrderSummary.OrderCurrency.CurrencySymbol = CS.General_CS_v6.Enums.GetCurrencySymbol(model.OrderSummary.OrderCurrency.CurrencyCode);
		}

		private static void fillOrderSummaryItems(
			PaymentRequestData paymentRequest,
			PaymentRequestDataModel model)
		{
			model.OrderSummary.Items = new List<PaymentOrderSummaryItemDataModel>();
			if (paymentRequest.RequestDetails.ItemDetails != null)
			{
				foreach (var item in paymentRequest.RequestDetails.ItemDetails)
				{
					if (item != null)
					{
						var itemModel = new PaymentOrderSummaryItemDataModel();
						itemModel.DescriptionHtml = item.Description;
						itemModel.Title = item.Title;
						model.OrderSummary.Items.Add(itemModel);
					}
				}
			}
		}
	}
}