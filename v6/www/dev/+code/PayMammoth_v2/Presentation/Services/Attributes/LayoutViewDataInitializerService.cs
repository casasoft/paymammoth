﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Links;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v6.Presentation.Services.ContentTexts;
using BusinessLogic_CS_v6.Presentation.Services.Context;
using BusinessLogic_CS_v6.Presentation.Services.Sections;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Modules.Pages;
using CS.General_CS_v6.Modules.Urls;
using PayMammoth_v6.Presentation.Code.ViewData;
using PayMammoth_v6.Presentation.Services.Payments;
using Raven.Client;

namespace PayMammoth_v6.Presentation.Services.Attributes
{
    using BusinessLogic_CS_v6.Aspects;
    using BusinessLogic_CS_v6.Aspects.NullAspects;
    using CS.General_CS_v6.Modules.InversionOfControl;

    public interface ILayoutViewDataInitializerService
    {
        LayoutViewData InitalizeLayoutViewDataForCurrentRequest(
            IDocumentSession documentSession,
            CultureModel culture,
            ActionExecutingContext context);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class LayoutViewDataInitializerService : ILayoutViewDataInitializerService
    {
        private readonly IPageService _pageService;
        private readonly IPaymentRequestDataModelService _paymentRequestDataModelService;
        private readonly IContentTextPresentationService _contentTextModelService;
        private readonly IPaymentOrderSummaryModelService _paymentOrderSummaryModelService;
        private readonly IPaymentDetailsModelService _paymentDetailsModelService;
        private readonly ISectionDataPresentationService _sectionModelService;
        private readonly ICommonViewContextDataService _commonViewContextDataService;

        public LayoutViewDataInitializerService(
            IPageService pageService,
            IPaymentRequestDataModelService paymentRequestDataModelService,
            IContentTextPresentationService contentTextModelService,
            IPaymentOrderSummaryModelService paymentOrderSummaryModelService,
            IPaymentDetailsModelService paymentDetailsModelService,
            ISectionDataPresentationService sectionModelService,
            ICommonViewContextDataService commonViewContextDataService)
        {
            _commonViewContextDataService = commonViewContextDataService;
            _sectionModelService = sectionModelService;
            _paymentDetailsModelService = paymentDetailsModelService;
            _paymentOrderSummaryModelService = paymentOrderSummaryModelService;
            _contentTextModelService = contentTextModelService;
            _paymentRequestDataModelService = paymentRequestDataModelService;
            _pageService = pageService;
        }

        public LayoutViewData InitalizeLayoutViewDataForCurrentRequest(
            IDocumentSession documentSession,
            CultureModel culture,
            ActionExecutingContext context)
        {


            var requestId =
                _pageService
                    .GetVariableFromRoutingQuerystringOrForm<string>(PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER);

            if (!string.IsNullOrWhiteSpace(requestId))
            {
                string fakePaymentId = _pageService
                    .GetVariableFromRoutingQuerystringOrForm<string>(
                        PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_FakePaymentId);

                var paymentRequestResult = _paymentRequestDataModelService
                    .GetPaymentMethodRequestForPaymentRequestId(
                        documentSession,
                        culture,
                        requestId,
                        fakePaymentId);



                if (paymentRequestResult.Status == GetPaymentMethodRequestForPaymentRequestIdStatus.Success)
                {
                    var data = new LayoutViewData();
                    data.PaymentRequestDataModel = paymentRequestResult.Status ==
                                                   GetPaymentMethodRequestForPaymentRequestIdStatus.Success
                        ? paymentRequestResult.Result
                        : null;
                    var cvcd = _commonViewContextDataService.GetCommonViewContextData();
                    cvcd.Cms.CmsItem = data.PaymentRequestDataModel;
                    data.OrderSummaryModel = _paymentOrderSummaryModelService.CreatePaymentOrderSummaryModel(
                        documentSession,
                        culture,
                        paymentRequestResult.Result.OrderSummary);
                    data.PaymentDetailsModel = _paymentDetailsModelService.CreatePaymentDetailsModel(
                        documentSession,
                        culture,
                        paymentRequestResult.Result.UserDetails);
                    data.CancelAndReturnBackLink = new AnchorModel();
                    data.CancelAndReturnBackLink.TitleContentText = _contentTextModelService
                        .GetContentTextModelForCultureByIdentifierOrCreateNew(
                            documentSession,
                            culture,
                            PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Payment_CancelAndGoBackToWebsite,
                            paymentRequestResult.Result.WebsiteAccount.WebsiteName);
                    data.CancelAndReturnBackLink.Href = _sectionModelService.GetSectionUrlWithRouteValuesFromIdentifier(
                        documentSession,
                        culture,
                        PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_CancelPayment,
                        null,
                        new NameValueCollection()
                        {
                            {
                                PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER, requestId
                            }
                        });

                    if (paymentRequestResult.Result.PaymentExpiryTimespan.HasValue)
                    {
                        //Has expiry
                       
                        data.ExpirationMessage = new _NotificationMessageModel()
                        {
                            MessageContentTextModel = _contentTextModelService
                                .GetContentTextModelForCultureByIdentifierOrCreateNew(
                                    documentSession,
                                    culture,
                                    PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Payment_HasExpiryMinutes,
                                    (int) (paymentRequestResult.Result.PaymentExpiryTimespan.Value.TotalMinutes)),
                            MessageType = NotificationMessageType.Info
                        };

                    }
                    if (paymentRequestResult.Result.FakePaymentsEnabled)
                    {
                        //Fake payments are enabled
                        data.FakePaymentsEnabledMessage = new _NotificationMessageModel()
                        {
                            MessageType = NotificationMessageType.Info,
                            MessageContentTextModel =
                                _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                                    documentSession,
                                    culture,
                                    PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Messages_FakePaymentEnabled)
                        };
                    }
                    return data;
                }
                else if (paymentRequestResult.Status == GetPaymentMethodRequestForPaymentRequestIdStatus.Expired)
                {
                    var expiredUrl = _sectionModelService.GetSectionUrlWithRouteValuesFromIdentifier(
                        documentSession,
                        culture,
                        PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Expired);

                    UrlHandler urlExpired = new UrlHandler(expiredUrl);
                    urlExpired.QueryString[PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_CancelUrl] =
                        paymentRequestResult.Result.CancelUrl;
                    var urlToRedirectTo = urlExpired.GetAbsoluteUrl();
                    context.Result = new RedirectResult(urlToRedirectTo);
                }
                else
                {
                    var contentTextModelService = _contentTextModelService;
                    var contentText =
                        contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                            documentSession,
                            culture,
                            paymentRequestResult.Status);

                    context.Result = new ContentResult()
                    {
                        Content = contentText.ReplacedValue
                    };
                }

            }
            else
            {
                var contentTextModelService = _contentTextModelService;
                var contentText =
                    contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                        documentSession,
                        culture,
                        PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.InvalidRequestIdentifier);

                context.Result = new ContentResult()
                {
                    Content = contentText.ReplacedValue
                };
            }
            return null;
        }
    }


}
