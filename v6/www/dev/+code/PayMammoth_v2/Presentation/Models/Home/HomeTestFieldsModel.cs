using BusinessLogic_CS_v5.Presentation.Data.Fields;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.ContentPage;
using System;
namespace PayMammoth_v2.Presentation.Models.Home
{
    public class HomeTestFieldsModel
    {
        public class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public DateTime DateOfBirth { get; set; }
            public DateTimeOffset? DateOfBirth2 { get; set; }
            public bool WantPromoItems { get; set; }
            public string Html { get; set; }

        }

        public string Name { get; set; }
        public TextFieldModel NameField { get; set; }
        public TextFieldModel HtmlTextField { get; set; }
        public CheckboxFieldModel MarkPromoItemsField { get; set; }



        public string Name2 { get; set; }
        public string Name3 { get; set; }

        public Person Mark { get; set; }

        public TextFieldModel MarkAgeField { get; set; }
        public DateFieldModel DateOfBirthField { get; set; }
        public DateFieldModel DateOfBirthField2 { get; set; }


        public Person Karl { get; set; }

        public HomeTestFieldsModel()
        {
            
            
        }
    }
}