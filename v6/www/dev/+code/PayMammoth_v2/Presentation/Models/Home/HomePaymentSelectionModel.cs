using System.Collections.Generic;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.ContentTexts;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.MailingList;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;

namespace PayMammoth_v6.Presentation.Models.Home
{
    public class HomePaymentSelectionModel
    {
        public ContentTextModel TitleContentTextModel { get; set; }
        public List<PaymentMethodModel> PaymentMethods { get; set; }

        public HomePaymentSelectionModel()
        {
            
            
        }
    }
}