﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.ContentTexts;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;

namespace PayMammoth_v6.Presentation.Models._Shared.PartialViews.Payments
{
    public class _PaymentOrderSummaryModel
    {
        public PaymentOrderSummaryDataModel OrderSummaryDataModel { get; set; }
        public ContentTextModel TitleContentText { get; set; }
        public ContentTextModel TotalNetContentText { get; set; }
        public ContentTextModel TotalTaxContentText { get; set; }
        public ContentTextModel TotalShippingContentText { get; set; }
        public ContentTextModel TotalHandlingContentText { get; set; }
        public ContentTextModel TotalContentText { get; set; }
        public ContentTextModel ViewFullOrderDetailsContentText { get; set; }

        public string TotalNet { get; set; }
        public string TotalTax { get; set; }
        public string TotalShipping { get; set; }
        public string TotalHandling { get; set; }
        public string Total { get; set; }
    }
}
