﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Presentation.Interfaces;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Presentation.Models._Shared.Data.WebsiteAccounts;

namespace PayMammoth_v6.Presentation.Models._Shared.Data.Payments
{
    public class PaymentRequestDataModel : ICmsItem
    {
        /// <summary>
        /// The date when this payment is set to expire if not effected
        /// </summary>
        public DateTimeOffset? PaymentExpiryDate { get; set; }
        public TimeSpan? PaymentExpiryTimespan { get; set; }


        public string PaymentRequestId { get; set; }
        public string FakePaymentKey { get; set; }
        public bool FakePaymentsEnabled { get; set; }

        public WebsiteAccountDataModel WebsiteAccount { get; set; }

        public List<PaymentMethodDataModel> PaymentMethodsAvailable { get; set; }

        public PaymentOrderSummaryDataModel OrderSummary { get; set; }
        public PaymentUserDetailsDataModel UserDetails { get; set; }

        /// <summary>
        /// This is used when the request is expired so as to show to the user the URL to go to
        /// </summary>
        public string CancelUrl { get; set; }

        string ICmsItem.Id
        {
            get { return PaymentRequestId; }
        }

        Type ICmsItem.CollectionType
        {
            get { return typeof(PaymentRequestData); }
        }
    }
}
