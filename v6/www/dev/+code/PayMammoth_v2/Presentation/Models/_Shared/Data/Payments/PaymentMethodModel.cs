﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6.Presentation.Models._Shared.Data.Payments
{
    public class PaymentMethodModel
    {
        public PaymentMethodDataModel PaymentMethodDataModel { get; set; }
        public string Href { get; set; }
    }
}
