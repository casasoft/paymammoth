﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6.Presentation.Models._Shared.Data.Payments
{
    public class PaymentOrderSummaryItemDataModel
    {
        public string Title { get; set; }
        public string DescriptionHtml { get; set; }
    }
}
