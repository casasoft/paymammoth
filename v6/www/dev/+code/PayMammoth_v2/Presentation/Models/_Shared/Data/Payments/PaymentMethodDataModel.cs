﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Fields;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.MediaItems;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Connector;

namespace PayMammoth_v6.Presentation.Models._Shared.Data.Payments
{
    public class PaymentMethodDataModel
    {
        public FieldModel<string> Title { get; set; }

        public FieldModel<string> Description { get; set; }

        public EnumsPayMammothConnector.PaymentMethodType PaymentMethodType { get; set; }

        public MediaItemModel<PaymentMethodData.PaymentMethodDataIconImageSize> Icon { get; set; }
    }
}
