﻿using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.ContentTexts;

namespace PayMammoth_v6.Presentation.Models.Transactium
{
    public class TransactiumPaymentModel
    {
        public ContentTextModel TitleContentTextModel { get; set; }
        public string RedirectUrl { get; set; }


    }
}
