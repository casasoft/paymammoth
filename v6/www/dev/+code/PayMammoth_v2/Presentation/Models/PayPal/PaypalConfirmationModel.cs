﻿using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.ContentTexts;

namespace PayMammoth_v6.Presentation.Models.PayPal
{
    public class PaypalConfirmationModel
    {
        public string ConfirmUrl { get; set; }
        public ContentTextModel TitleContentTextModel { get; set; }
        public ContentTextModel ConfirmTextModel { get; set; }
        public ContentTextModel ConfirmButtonTextModel { get; set; }


    }
}
