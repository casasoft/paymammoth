﻿using System.Web.Mvc;
using System.Web.UI;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Services.CasaSoft;
using BusinessLogic_CS_v6.Presentation.Services.ContentTexts;
using BusinessLogic_CS_v6.Presentation.Services.Cultures;
using BusinessLogic_CS_v6.Presentation.Services.Navigation;
using CS.General_CS_v6.Modules.Pages;
using CS.General_CS_v6.Util;
using CS.MvcGeneral_CS_v6.Code.Attributes;
using PayMammoth_v6.Presentation.Code.ViewData;
using PayMammoth_v6.Presentation.Services.Attributes;
using PayMammoth_v6.Presentation.Services.Payments;
using Raven.Client;

namespace PayMammoth_v6.Presentation.Code.Attributes
{
    public class LayoutViewDataInitializerFilterAttribute : ViewDataAndContextInitializerBaseFilterAttribute

    {
        public LayoutViewDataInitializerFilterAttribute() : 
            base(ActionFilterTriggerType.OnActionExecuting)
        {
            string s = "";
        }

        public override string ViewDataAndContextKey
        {
            get { return PayMammoth_v6.Constants.PayMammoth_v6Constants.PayMammoth_v6LayoutViewDataKey; }
        }

        public override object CreateViewDataObject(
            IDocumentSession documentSession,
            CultureModel CultureModel,
            ControllerContext controllerContext)
        {
            return InversionUtil.Get<ILayoutViewDataInitializerService>()
                .InitalizeLayoutViewDataForCurrentRequest(
                    documentSession,
                    CultureModel,
                    controllerContext as ActionExecutingContext);
        }

    }
}
