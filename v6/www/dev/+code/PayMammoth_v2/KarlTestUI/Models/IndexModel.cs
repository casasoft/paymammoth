﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v6.KarlTestUI.Models
{
    public class IndexModel 
    {
        public LayoutModel Layout { get; set; }

        public List<PaymentMethodModel_Karl> PaymentMethodsAvailable { get; set; }

        public IndexModel()
        {
            this.Layout = new LayoutModel();
        }




    }
}
