﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using General_Tests_CS_v6.TestUtil;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Repositories.Payments;
using Raven.Client.Document;

namespace PayMammoth_v6_Tests.SlowTests.Modules.Repositories.PaymentRequests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    

    [TestFixture]
    public class PaymentRequestRepositorySpecs
    {
        public class class_under_test : SpecsForRavenDb<PaymentRequestRepository,PaymentRequestData>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time
            [TestFixture]
            public class GetAllPendingPaymentRequestsWhichAreExpiredSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {
                    protected override void initialiseIndexes(DocumentStore store)
                    {
                        store.ExecuteIndex(new PaymentRequestDataIndex());
                        base.initialiseIndexes(store);
                    }

                    protected override void Given()
                    {


                        base.Given();
                    }

                    [TestFixture]
                    public class given_query_is_called : given_such_data
                    {
                        private PaymentRequestData _request1;
                        private PaymentRequestData _request2;
                        private PaymentRequestData _request3;
                        private PaymentRequestData _request4;
                        private PaymentRequestData _request5;
                        private PaymentRequestData _request6;
                        private List<PaymentRequestData> _result;
                        private PaymentRequestData _request7;
                        private PaymentRequestData _request8;

                        private PaymentRequestData addRecord(PaymentStatusInfo.PaymentStatus paymentStatus, bool doesExpire, DateTimeOffset? expiresAt)
                        {
                            PaymentRequestData newRequest = new PaymentRequestData();
                            newRequest.PaymentStatus.Status = paymentStatus;
                            if (doesExpire)
                            {
                                newRequest.RequestDetails.ExpirationInfo.ExpiresAt = expiresAt;
                            }
                            else
                            {
                                newRequest.RequestDetails.ExpirationInfo.ExpiresAt = null;
                            }
                            base.currentSession.Store(newRequest);
                            return newRequest;
                        }


                        protected override void Given()
                        {
                            base.Given();

                            var expiredTime = new DateTimeOffset(2014, 8, 14, 16, 25, 0, new TimeSpan());
                            var notExpiredTime = new DateTimeOffset(2014, 8, 14, 16, 35, 0, new TimeSpan());
                            var currentTime = new DateTimeOffset(2014, 8, 14, 16, 30, 0, new TimeSpan());

                            GetMockFor<ICurrentDateTimeRetrieverService>().Setup(x => x.GetCurrentDateTime()).Returns(currentTime);

                            //add any code after base.Given!
                            _request2 = addRecord(PaymentStatusInfo.PaymentStatus.Cancelled, true, expiredTime);
                            _request3 = addRecord(PaymentStatusInfo.PaymentStatus.Pending, true, notExpiredTime);
                            _request4 = addRecord(PaymentStatusInfo.PaymentStatus.Expired, true, notExpiredTime);
                            _request5 = addRecord(PaymentStatusInfo.PaymentStatus.Pending, false, expiredTime);
                            _request1 = addRecord(PaymentStatusInfo.PaymentStatus.Pending, true, expiredTime);
                            _request6 = addRecord(PaymentStatusInfo.PaymentStatus.Paid, false, expiredTime);
                            _request7 = addRecord(PaymentStatusInfo.PaymentStatus.Pending, false, notExpiredTime);
                            _request8 = addRecord(PaymentStatusInfo.PaymentStatus.Cancelled, false, notExpiredTime);

                           

                        }

                        protected override void When()
                        {
                            base.When();


                            _result = SUT.GetAllPendingPaymentRequestsWhichAreExpired(currentSession);
                        }

                        [Test]
                        public void then_correct_results_should_be_returned()
                        {
                            _result.ShouldContainAllAndNothingElseFrom(_request1);


                        }
                    }

                }
            }
        }


    }
}
