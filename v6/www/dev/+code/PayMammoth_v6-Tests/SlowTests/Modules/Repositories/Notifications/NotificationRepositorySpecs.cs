﻿using System;
using System.Collections.Generic;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using General_Tests_CS_v6.TestUtil;
using NUnit.Framework;
using PayMammoth_v6.Modules.Data.Notifications;
using PayMammoth_v6.Modules.Repositories.Notifications;
using PayMammoth_v6.Connector;
using Raven.Client.Document;
using Should;

namespace PayMammoth_v6_Tests.SlowTests.Modules.Repositories.Notifications
{
    [TestFixture]
    public class NotificationsRepositorySpecs
    {
        public class class_under_test : SpecsForRavenDb<NotificationMessageDataRepository,NotificationMessageData>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time
            protected override void initialiseIndexes(DocumentStore store)
            {
                base.initialiseIndexes(store);
                store.ExecuteIndex(new NotificationMessageDataIndex());
            }
        }

        [TestFixture]
        public class GetAllNotificationsYetToBeSentSpecs
        {
            [TestFixture]
            public class given_such_data : class_under_test
            {
                protected NotificationMessageData _msg1;
                protected NotificationMessageData _msg2;
                protected NotificationMessageData _msg7;
                protected NotificationMessageData _msg6;
                protected NotificationMessageData _msg5;
                protected NotificationMessageData _msg4;
                protected NotificationMessageData _msg3;
                private DateTimeOffset _currentDateTime;

                private NotificationMessageData createNotification(DateTimeOffset nextRetryOn,
                  EnumsPayMammothConnector.NotificationMessageStatus status)
                {
                    NotificationMessageData msg = new NotificationMessageData();
                    msg.NextRetryOn = nextRetryOn;
                    msg.Status = status;
                    currentSession.Store(msg);
                    return msg;
                }
                protected override void Given()
                {


                    base.Given();
                    _currentDateTime = new DateTimeOffset(2014,1,17,17,42,40,new TimeSpan(0));

                    GetMockFor<ICurrentDateTimeRetrieverService>().Setup(x => x.GetCurrentDateTime()).Returns(_currentDateTime);
                    _msg1 = createNotification(new DateTimeOffset(2014, 1, 17, 17, 42, 40, new TimeSpan(0)), EnumsPayMammothConnector.NotificationMessageStatus.Pending);
                    _msg2 = createNotification(new DateTimeOffset(2014, 1, 17, 17, 42, 40, new TimeSpan(0)), EnumsPayMammothConnector.NotificationMessageStatus.Failed);
                    _msg3 = createNotification(new DateTimeOffset(2014, 1, 17, 17, 42, 40, new TimeSpan(0)), EnumsPayMammothConnector.NotificationMessageStatus.Success);
                    _msg4 = createNotification(new DateTimeOffset(2014, 1, 17, 17, 41, 00, new TimeSpan(0)), EnumsPayMammothConnector.NotificationMessageStatus.Pending);
                    _msg5 = createNotification(new DateTimeOffset(2014, 1, 17, 17, 45, 00, new TimeSpan(0)), EnumsPayMammothConnector.NotificationMessageStatus.Pending);
                    _msg6 = createNotification(new DateTimeOffset(2014, 1, 17, 17, 41, 00, new TimeSpan(0)), EnumsPayMammothConnector.NotificationMessageStatus.Success);
                    _msg7 = createNotification(new DateTimeOffset(2014, 1, 17, 17, 48, 00, new TimeSpan(0)), EnumsPayMammothConnector.NotificationMessageStatus.Success);

                    currentSession.SaveChanges();
                    disposeCurrentSessionAndCreateNew();
                }


            }

            [TestFixture]
            public class given_data : given_such_data
            {
                private List<NotificationMessageData> _list;


                /*
STATUS 			NEXT RETRY ON      			LOADED
PENDING			17/01/2014 17:42:40			Y
FAILED 			17/01/2014 17:42:40			N
SUCCESS  		17/01/2014 17:42:40			N
PENDING			17/01/2014 17:41:00			Y
PENDING			17/01/2014 17:45:00			N
SUCCESS 		17/01/2014 17:41:00			Y
SUCCESS 		17/01/2014 17:48:00			N
                 */

              


                protected override void Given()
                {
                    base.Given();
                    //add any code after base.Given!

                 


                }

                protected override void When()
                {
                    base.When();

                    _list = SUT.GetAllNotificationsYetToBeSent(currentSession,1000);
                }

                [Test]
                public void then_correct_data_is_returned()
                {

                    _list.ShouldContainAllAndNothingElseFrom(_msg1, _msg4,_msg5);

                }
            }

            [TestFixture]
            public class given_page_size_of_one : given_such_data
            {
                private List<NotificationMessageData> _list;


                /*
STATUS 			NEXT RETRY ON      			LOADED
PENDING			17/01/2014 17:42:40			Y
FAILED 			17/01/2014 17:42:40			N
SUCCESS  		17/01/2014 17:42:40			N
PENDING			17/01/2014 17:41:00			Y
PENDING			17/01/2014 17:45:00			N
SUCCESS 		17/01/2014 17:41:00			Y
SUCCESS 		17/01/2014 17:48:00			N
                 */




                protected override void Given()
                {
                    base.Given();
                    //add any code after base.Given!




                }

                protected override void When()
                {
                    base.When();

                    _list = SUT.GetAllNotificationsYetToBeSent(currentSession,1);
                }

                [Test]
                public void then_only_one_item_is_returend()
                {
                    _list.Count.ShouldEqual(1);
                }
            }
        }

    }

}
