﻿using System.Collections.Generic;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using General_Tests_CS_v6.TestUtil;
using NUnit.Framework;
using PayMammoth_v6.Presentation.Models.Home;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v6.Presentation.Services.Payments;
using Should;

namespace PayMammoth_v6_Tests.FrontendTests.NormalTests.Presentation.Services.Payments
{
    [TestFixture]
    public abstract class CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestIdSpecs : MySpecsFor<HomePaymentSelectionModelService>
    {
        private HomePaymentSelectionModel _result;

        protected override void Given()
        {
            //Common data setup between test cases for same method

            base.Given();


            _mockedContentTexts.MockItem(PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.PaymentDetails_ChoosePaymentMethod);
        }

        protected override void When()
        {
            base.When();
        }

        [Test]
        public void then_content_texts_should_be_correct()
        {
            _result.TitleContentTextModel.ShouldEqual(
                _mockedContentTexts.Values[
                    PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.PaymentDetails_ChoosePaymentMethod]);



        }

        public class given_valid_request_id : CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestIdSpecs
        {
            private List<PaymentMethodModel> _paymentMethodModels;
            private PaymentRequestDataModel _paymentRequestDataModel;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup


                
                _paymentRequestDataModel = new PaymentRequestDataModel()
                {
                    PaymentMethodsAvailable = new List<PaymentMethodDataModel>()
                    {
                        new PaymentMethodDataModel(),
                        new PaymentMethodDataModel(),
                        new PaymentMethodDataModel()
                    },
                    PaymentRequestId = "1234",
                    FakePaymentKey = "FAKE"
                };
                _paymentMethodModels = _paymentRequestDataModel.PaymentMethodsAvailable.ConvertAll(
                   x =>
                   {
                       var paymentMethodModel = new PaymentMethodModel();
                       GetMockFor<IPaymentMethodModelService>().Setup(y => y.ConvertPaymentRequestDataModelToModel(
                           _mockedSessionObject,
                           _CultureModel,
                           x, _paymentRequestDataModel.PaymentRequestId, _paymentRequestDataModel.FakePaymentKey)).Returns(paymentMethodModel);
                       return paymentMethodModel;
                   });
            }

            protected override void When()
            {
                base.When();
                _result = SUT.CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestId(
                    _mockedSessionObject,
                    _CultureModel, _paymentRequestDataModel);
            }

            [Test]
            public void then_payment_methods_should_be_correct()
            {
                _result.PaymentMethods.ShouldContainAllAndNothingElseFromList(_paymentMethodModels);



            }
        }
        public class given_fake_payment_is_enabled : CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestIdSpecs
        {
            private List<PaymentMethodModel> _paymentMethodModels;
            private PaymentRequestDataModel _paymentRequestDataModel;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup



                _paymentRequestDataModel = new PaymentRequestDataModel()
                {
                    PaymentMethodsAvailable = new List<PaymentMethodDataModel>()
                    {
                        new PaymentMethodDataModel(),
                        new PaymentMethodDataModel(),
                        new PaymentMethodDataModel()
                    },
                    PaymentRequestId = "1234",
                    FakePaymentKey = "FAKE",
                    FakePaymentsEnabled = true
                };

                _mockedContentTexts.MockItem(PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Messages_FakePaymentEnabled);
               
            }

            protected override void When()
            {
                base.When();
                _result = SUT.CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestId(
                    _mockedSessionObject,
                    _CultureModel, _paymentRequestDataModel);
            }

            
        }
       
    }
}
