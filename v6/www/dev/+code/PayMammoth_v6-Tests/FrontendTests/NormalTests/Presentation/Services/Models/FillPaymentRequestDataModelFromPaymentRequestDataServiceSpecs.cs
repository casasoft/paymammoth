﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v6.Presentation.Services._Shared.BaseIsoEnumDatas;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v6.Presentation.Services.Models;

namespace PayMammoth_v6_Tests.FrontendTests.NormalTests.Presentation.Services.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
   


    [TestFixture]
    public class FillPaymentRequestDataModelFromPaymentRequestDataServiceSpecs
    {
        public class class_under_test : MySpecsFor<FillPaymentRequestDataModelFromPaymentRequestDataService>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time
            [TestFixture]
            public class CreatePaymentRequestDataModelFromPaymentRequestDataSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {
                    protected WebsiteAccountData _websiteAccountData;
                    protected PaymentRequestData _paymentRequestData;
                    protected CultureModel _CultureModel;
                    protected string _fakePaymentKey;


                    protected override void Given()
                    {


                        _paymentRequestData = new PaymentRequestData();
                        _paymentRequestData.Id = "Request/1";
                        _websiteAccountData = new WebsiteAccountData();
                        _CultureModel = new CultureModel();
                        _fakePaymentKey = "";

                        _paymentRequestData.RequestDetails.Pricing.CurrencyCode3Letter = "EUR";
                            
                        Iso4217_CurrencyDataModel currencyModel = new Iso4217_CurrencyDataModel();

                        GetMockFor<IIso4217_CurrencyDataPresentationService>()
                            .Setup(x=>x.GetCurrencyModelByCurrencyCode(_CultureModel,_paymentRequestData.RequestDetails.Pricing.CurrencyCode3Letter))
                            .Returns(currencyModel);


                        GetMockFor<IPaymentRequestDataService>()
                            .Setup(x => x.GetAllPaymentMethodsAvailableForPayment(_paymentRequestData,
                                _mockedSessionObject))
                                .Returns(new List<PaymentMethodData>());

                        base.Given();
                    }

                    [TestFixture]
                    public class given_it_has_expiry_date : given_such_data
                    {
                        private PaymentRequestDataModel _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            DateTimeOffset currentDate = new DateTimeOffset(2014,9,17,0,0,0,new TimeSpan(0));
                            DateTimeOffset expiryDate = new DateTimeOffset(2014, 9, 17, 1, 30, 00, new TimeSpan(0));

                            _paymentRequestData.RequestDetails.ExpirationInfo.ExpiresAt = expiryDate;

                            GetMockFor<ICurrentDateTimeRetrieverService>().Setup(x => x.GetCurrentDateTime())
                                .Returns(currentDate);

                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.CreatePaymentRequestDataModelFromPaymentRequestData(_paymentRequestData, _websiteAccountData, _CultureModel, _mockedSessionObject, _fakePaymentKey);
                        }

                        [Test]
                        public void then_expiry_timespan_should_be_correct()
                        {
                            _result.PaymentExpiryTimespan.Value.TotalMinutes.ShouldEqual(90d);

                        }
                    }
                    [TestFixture]
                    public class given_it_does_not_have_an_expiry_date : given_such_data
                    {
                        private PaymentRequestDataModel _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            DateTimeOffset currentDate = new DateTimeOffset(2014, 9, 17, 0, 0, 0, new TimeSpan(0));
                            
                            _paymentRequestData.RequestDetails.ExpirationInfo.ExpiresAt = null;

                            GetMockFor<ICurrentDateTimeRetrieverService>().Setup(x => x.GetCurrentDateTime())
                                .Returns(currentDate);

                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.CreatePaymentRequestDataModelFromPaymentRequestData(_paymentRequestData, _websiteAccountData, _CultureModel, _mockedSessionObject, _fakePaymentKey);
                        }

                        [Test]
                        public void then_expiry_timespan_should_be_null()
                        {
                            _result.PaymentExpiryTimespan.ShouldBeNull();

                        }
                    }
                }
            }
        }


    }
}
