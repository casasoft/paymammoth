﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v6.Presentation.Services.Sections;
using BusinessLogic_CS_v6.Presentation.Services.Url;
using CS.General_CS_v6.Modules.Pages;
using PayMammoth_v6.Presentation.Services.Payments;

namespace PayMammoth_v6_Tests.FrontendTests.NormalTests.Presentation.Services.Payments
{
    using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
    using Moq;
    using NUnit.Framework;
    using Should;

    [TestFixture]
    public abstract class PaymentUrlModelServiceSpecs
    {
        [TestFixture]
        public abstract class GetPaymentSelectionPageSpecs : MySpecsFor<PaymentUrlModelService>
        {
            protected override void Given()
            {
                //Common data setup between test cases for same method

                base.Given();
            }

            protected override void When()
            {
                base.When();
            }

            public class given_method_is_called : GetPaymentSelectionPageSpecs
            {
                private string _requestId;
                private string _result;
                private string _paymentSelectionUrl;

                protected override void Given()
                {
                    //Setup test case specific data here as the base will setup common data setup
                    _requestId = "PaymentRequestId";


                    var paymentSelectionSectionUrl = "http://payment-selection/";
                    GetMockFor<ISectionDataPresentationService>()
                        .Setup(x => x.GetSectionUrlWithRouteValuesFromIdentifier(
                            _mockedSessionObject,
                            _CultureModel,
                            PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Selection,
                            null,
                            null,
                            true)).Returns(paymentSelectionSectionUrl);


                    _paymentSelectionUrl = "http://payment-selection/?transid=1";
                    GetMockFor<IUrlHandlerService>().Setup(x => x.UpdateUrlQuerystring(paymentSelectionSectionUrl,
                        It.Is<NameValueCollection>(
                            nvc => nvc[PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER] == _requestId &&
                                   nvc[PayMammoth_v6.Connector.Constants.PARAM_TransactionId] == null)))
                        .Returns(_paymentSelectionUrl);


                    base.Given();
                }

                protected override void When()
                {
                    base.When();

                    _result = SUT.GetPaymentSelectionPage(_mockedSessionObject,
                        _CultureModel,
                        _requestId
                        );
                }

                [Test]
                public void then_url_should_be_correct()
                {
                    _result.ShouldEqual(_paymentSelectionUrl);
                }
            }
        }

        [TestFixture]
        public abstract class GetPayPalConfirmationPageUrlSpecs : MySpecsFor<PaymentUrlModelService>
        {
            protected override void Given()
            {
                //Common data setup between test cases for same method

                base.Given();
            }

            protected override void When()
            {
                base.When();
            }

            public class given_method_is_called : GetPayPalConfirmationPageUrlSpecs
            {
                private string _requestId;
                private string _result;
                private string _url;

                protected override void Given()
                {
                    //Setup test case specific data here as the base will setup common data setup
                    _requestId = "PaymentRequestId";


                    var paypalConfirmationSectionUrl = "http://paypal-confirmation/";
                    GetMockFor<ISectionDataPresentationService>()
                        .Setup(x => x.GetSectionUrlWithRouteValuesFromIdentifier(
                            _mockedSessionObject,
                            _CultureModel,
                            PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Payment_PaypalConfirmation,
                            null,
                            null,
                            true)).Returns(paypalConfirmationSectionUrl);


                    _url = "http://payment-selection/?transid=1";
                    GetMockFor<IUrlHandlerService>().Setup(x => x.UpdateUrlQuerystring(paypalConfirmationSectionUrl,
                        It.Is<NameValueCollection>(
                            nvc => nvc[PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER] == _requestId &&
                                   nvc[PayMammoth_v6.Connector.Constants.PARAM_TransactionId] == null)))
                        .Returns(_url);


                    base.Given();
                }

                protected override void When()
                {
                    base.When();

                    _result = SUT.GetPayPalConfirmationPageUrl(_mockedSessionObject,
                        _CultureModel,
                        _requestId
                        );
                }

                [Test]
                public void then_url_should_be_correct()
                {
                    _result.ShouldEqual(_url);
                }
            }
        }

        [TestFixture]
        public abstract class GetTransactiumStatusPageUrlSpecs : MySpecsFor<PaymentUrlModelService>
        {
            protected override void Given()
            {
                //Common data setup between test cases for same method

                base.Given();
            }

            protected override void When()
            {
                base.When();
            }

            public class given_method_is_called : GetTransactiumStatusPageUrlSpecs
            {
                private string _requestId;
                private string _result;
                private string _transactiumId;
                private string _urlTransactiumStatus;

                protected override void Given()
                {
                    //Setup test case specific data here as the base will setup common data setup
                    _requestId = "PaymentRequestId";


                    var transactiumStatusSectionUrl = "http://transactium-status/";
                    GetMockFor<ISectionDataPresentationService>()
                        .Setup(x => x.GetSectionUrlWithRouteValuesFromIdentifier(
                            _mockedSessionObject,
                            _CultureModel,
                            PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Payment_Transactium_Status,
                            null,
                            null,
                            true)).Returns(transactiumStatusSectionUrl);


                    _transactiumId = "TransactiumId";
                    var url = "http://payment-selection/?transid=1";
                    GetMockFor<IUrlHandlerService>().Setup(x => x.UpdateUrlQuerystring(transactiumStatusSectionUrl,
                        It.Is<NameValueCollection>(
                            nvc => nvc[PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER] == _requestId &&
                                   nvc[PayMammoth_v6.Connector.Constants.PARAM_TransactionId] == _transactiumId)))
                        .Returns(url);



                    _urlTransactiumStatus = "http://transactium-status-page/";
                    GetMockFor<IPageService>().Setup(x => x.ConvertRelativeUrlToAbsoluteUrl(url)).Returns(_urlTransactiumStatus);

                    base.Given();
                }

                protected override void When()
                {
                    base.When();

                    _result = SUT.GetTransactiumStatusPageUrl(_mockedSessionObject,
                        _CultureModel,
                        _requestId,
                        _transactiumId
                        );
                }

                [Test]
                public void then_url_should_be_correct()
                {
                    _result.ShouldEqual(_urlTransactiumStatus);
                }
            }
        }

        [TestFixture]
        public abstract class GetTransactiumPaymentPageSpecs : MySpecsFor<PaymentUrlModelService>
        {
            protected override void Given()
            {
                //Common data setup between test cases for same method

                base.Given();
            }

            protected override void When()
            {
                base.When();
            }

            public class given_method_is_called : GetTransactiumPaymentPageSpecs
            {
                private string _requestId;
                private string _result;
                private string _url;
                private string _transactiumId;

                protected override void Given()
                {
                    //Setup test case specific data here as the base will setup common data setup
                    _requestId = "PaymentRequestId";


                    var transactiumPaymentPageSectionUrl = "http://transactium-payment/";
                    GetMockFor<ISectionDataPresentationService>()
                        .Setup(x => x.GetSectionUrlWithRouteValuesFromIdentifier(
                            _mockedSessionObject,
                            _CultureModel,
                            PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Payment_Transactium_Status,
                            null,
                            null,
                            true)).Returns(transactiumPaymentPageSectionUrl);


                    _transactiumId = "TransactiumId";
                    _url = "http://payment-selection/?transid=1";
                    GetMockFor<IUrlHandlerService>().Setup(x => x.UpdateUrlQuerystring(transactiumPaymentPageSectionUrl,
                        It.Is<NameValueCollection>(
                            nvc => nvc[PayMammoth_v6.Connector.Constants.PARAM_IDENTIFIER] == _requestId &&
                                   nvc[PayMammoth_v6.Connector.Constants.PARAM_TransactionId] == _transactiumId)))
                        .Returns(_url);



                    GetMockFor<IPageService>().Setup(x => x.ConvertRelativeUrlToAbsoluteUrl(_url)).Returns(_url);

                    base.Given();
                }

                protected override void When()
                {
                    base.When();

                    _result = SUT.GetTransactiumStatusPageUrl(_mockedSessionObject,
                        _CultureModel,
                        _requestId,
                        _transactiumId
                        );
                }

                [Test]
                public void then_url_should_be_correct()
                {
                    _result.ShouldEqual(_url);
                }
            }
        }
    }
}
