﻿using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Fields;
using BusinessLogic_CS_v6.Presentation.Services.Sections;
using CS.General_CS_v6.Modules.Urls;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using NUnit.Framework;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v6.Presentation.Services.Payments;
using PayMammoth_v6.Connector;
using Should;

namespace PayMammoth_v6_Tests.FrontendTests.NormalTests.Presentation.Services.Payments
{



    [TestFixture]
    public abstract class ConvertPaymentRequestDataModelToModelSpecs : MySpecsFor<PaymentMethodModelService>
    {
        protected override void Given()
        {
            //Common data setup between test cases for same method

            base.Given();
        }

        protected override void When()
        {
            base.When();
        }


        public class given_method_is_called : ConvertPaymentRequestDataModelToModelSpecs
        {
            private PaymentMethodModel _result;
            private PaymentMethodDataModel _paymentMethodDataModel;
            private string _url;
            private string _paymentRequestId;
            private string _fakePaymentId;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup

                _paymentMethodDataModel = new PaymentMethodDataModel()
                {
                    Title = new FieldModel<string>() {Value = "Test"},
                    PaymentMethodType = EnumsPayMammothConnector.PaymentMethodType.PayPal_ExpressCheckout
                };

                _url = "/payment/method/";
                GetMockFor<ISectionDataPresentationService>().Setup(x => x.GetSectionUrlWithRouteValuesFromIdentifier(
                    _mockedSessionObject,
                    _CultureModel,
                    PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Selection_Handler,
                    null,
                    null, 
                    true)).Returns(_url);
                _paymentRequestId = "1234";


            }

            protected override void When()
            {
                base.When();
                _fakePaymentId = "fake-id";
                _result = SUT.ConvertPaymentRequestDataModelToModel(_mockedSessionObject,
                    _CultureModel, _paymentMethodDataModel, _paymentRequestId, _fakePaymentId);
            }

            [Test]
            public void then_href_must_be_fine()
            {
                UrlHandler url = new UrlHandler(_result.Href);
                url.UriBuilder.Path.ShouldContain(_url);
                url.QueryString[PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_PaymentMethod].ShouldEqual(
                    CS.General_CS_v6.Util.ConversionUtil.ConvertBasicDataTypeToString(_paymentMethodDataModel.PaymentMethodType));
                url.QueryString[PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_FakePaymentId].ShouldEqual(
                   CS.General_CS_v6.Util.ConversionUtil.ConvertBasicDataTypeToString(_fakePaymentId));

                url.QueryString[Constants.PARAM_IDENTIFIER].ShouldEqual(
                    CS.General_CS_v6.Util.ConversionUtil.ConvertBasicDataTypeToString(_paymentRequestId));

                


            }
        }
      
    }
}
