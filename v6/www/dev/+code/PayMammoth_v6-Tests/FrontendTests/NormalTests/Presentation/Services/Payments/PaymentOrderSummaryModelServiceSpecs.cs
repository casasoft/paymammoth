﻿using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.BaseIsoEnumDatas;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using NUnit.Framework;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v6.Presentation.Models._Shared.PartialViews.Payments;
using PayMammoth_v6.Presentation.Services.Payments;
using Should;

namespace PayMammoth_v6_Tests.FrontendTests.NormalTests.Presentation.Services.Payments
{
    [TestFixture]
    public abstract class CreatePaymentOrderSummaryModelSpecs : MySpecsFor<PaymentOrderSummaryModelService>
    {
            private PaymentOrderSummaryDataModel _paymentOrderSummaryDataModel;

        protected override void Given()
        {
            //Common data setup between test cases for same method

            base.Given();

            _paymentOrderSummaryDataModel = new PaymentOrderSummaryDataModel()
            {
                DescriptionHtml = "<p>Test</p>",
                OrderCurrency = new Iso4217_CurrencyDataModel()
                {
                    Identifier = "GBP",
                    Symbol = "£"
                },
                Total = 100,
                TotalHandling = 200,
                TotalNet = 300,
                TotalTax = 500
            };


            _mockedContentTexts.MockItems(
                PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_Title,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_TotalNet,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_TotalTax,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_TotalShipping,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_TotalHandling,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_Total,
                PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_ViewFullOrderDetails);

                /*
                public ContentTextModel TitleContentText { get; set; }
        public ContentTextModel TotalNetContentText { get; set; }
        public ContentTextModel TotalTaxContentText { get; set; }
        public ContentTextModel TotalShippingContentText { get; set; }
        public ContentTextModel TotalHandlingContentText { get; set; }
        public ContentTextModel TotalContentText { get; set; }
        public ContentTextModel ViewFullOrderDetailsContentText { get; set; }*/
        }

        protected override void When()
        {
            base.When();
        }


        public class given_the_following_order_details_with_title : CreatePaymentOrderSummaryModelSpecs
        {
            private _PaymentOrderSummaryModel _result;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup

                _paymentOrderSummaryDataModel.Title = "Test";

            }

            protected override void When()
            {
                base.When();
                _result = SUT.CreatePaymentOrderSummaryModel(
                    _mockedSessionObject,
                    _CultureModel,
                    _paymentOrderSummaryDataModel);
            }

            [Test]
            public void then_title_content_text_should_be_null()
            {
                _result.TitleContentText.ShouldBeNull();

            }
           
        }
        public class given_the_following_order_details_without_title : CreatePaymentOrderSummaryModelSpecs
        {
            private _PaymentOrderSummaryModel _result;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup



            }

            protected override void When()
            {
                base.When();
                _result = SUT.CreatePaymentOrderSummaryModel(
                    _mockedSessionObject,
                    _CultureModel,
                    _paymentOrderSummaryDataModel);
            }

            [Test]
            public void then_totals_should_be_correct_with_currency()
            {
                _result.Total.ShouldEqual(
                    BusinessLogic_CS_v6.Util.NumberUtilBL.FormatCurrency(_paymentOrderSummaryDataModel.Total,
                        _CultureModel,
                        _paymentOrderSummaryDataModel.OrderCurrency));

                _result.TotalHandling.ShouldEqual(
                    BusinessLogic_CS_v6.Util.NumberUtilBL.FormatCurrency(
                        _paymentOrderSummaryDataModel.TotalHandling.GetValueOrDefault(),
                        _CultureModel,
                        _paymentOrderSummaryDataModel.OrderCurrency));

                _result.TotalNet.ShouldEqual(
                    BusinessLogic_CS_v6.Util.NumberUtilBL.FormatCurrency(_paymentOrderSummaryDataModel.TotalNet,
                        _CultureModel,
                        _paymentOrderSummaryDataModel.OrderCurrency));

                _result.TotalShipping.ShouldBeNull();

                _result.TotalTax.ShouldEqual(
                    BusinessLogic_CS_v6.Util.NumberUtilBL.FormatCurrency(
                        _paymentOrderSummaryDataModel.TotalTax.GetValueOrDefault(),
                        _CultureModel,
                        _paymentOrderSummaryDataModel.OrderCurrency));

            }
            [Test]
            public void then_order_summary_should_be_correct()
            {
                _result.OrderSummaryDataModel.ShouldEqual(_paymentOrderSummaryDataModel);
            }
            [Test]
            public void then_content_texts_should_be_correct()
            {
                _result.TitleContentText.ShouldEqual(
                    _mockedContentTexts.Values[PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_Title]);
                _result.TotalContentText.ShouldEqual(
                    _mockedContentTexts.Values[PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_Total]);
                _result.TotalHandlingContentText.ShouldEqual(
                    _mockedContentTexts.Values[
                        PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_TotalHandling]);
                _result.TotalNetContentText.ShouldEqual(
                    _mockedContentTexts.Values[PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_TotalNet]);
                _result.TotalShippingContentText.ShouldEqual(
                    _mockedContentTexts.Values[
                        PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_TotalShipping]);
                _result.TotalTaxContentText.ShouldEqual(
                    _mockedContentTexts.Values[PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_TotalTax]);
                _result.ViewFullOrderDetailsContentText.ShouldEqual(
                    _mockedContentTexts.Values[
                        PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.OrderSummary_ViewFullOrderDetails]);
            }
        }

    }
}
