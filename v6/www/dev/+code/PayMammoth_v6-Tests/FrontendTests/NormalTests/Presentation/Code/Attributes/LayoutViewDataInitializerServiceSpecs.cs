﻿using System;
using System.Collections.Specialized;
using System.Web.Mvc;
using BusinessLogic_CS_v6.Presentation.Code.Mvc;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v6.Presentation.Services.Sections;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Classes.HelperClasses;
using CS.General_CS_v6.Modules.Pages;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using Moq;
using NUnit.Framework;
using PayMammoth_v6.Presentation.Code.ViewData;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v6.Presentation.Models._Shared.Data.WebsiteAccounts;
using PayMammoth_v6.Presentation.Models._Shared.PartialViews.Payments;
using PayMammoth_v6.Presentation.Services.Attributes;
using PayMammoth_v6.Presentation.Services.Payments;
using PayMammoth_v6.Connector;
using Should;

namespace PayMammoth_v6_Tests.FrontendTests.NormalTests.Presentation.Code.Attributes
{
    [TestFixture]
    public abstract class LayoutViewDataInitializerServiceSpecs : MySpecsFor<LayoutViewDataInitializerService>
    {
        private ActionExecutingContext _actionExecutingContext;
        private string _invalidRequestMessage;

        protected override void Given()
        {
            //Common data setup between test cases for same method

            base.Given();
            _actionExecutingContext = new ActionExecutingContext()
            {
                Controller = new CsControllerBL()
            };


            _invalidRequestMessage = "Invalid Request";
            _mockedContentTexts.MockItem(PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.InvalidRequestIdentifier,
                _invalidRequestMessage);


        }

        protected override void When()
        {
            base.When();
        }


        public class given_request_identifier_present_and_request_valid_and_has_expiration_and_fake_payment :
            LayoutViewDataInitializerServiceSpecs
        {
            private string _requestId;

            private StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>
                _statusResult;

            private LayoutViewData _result;
            private _PaymentOrderSummaryModel _paymentOrderSummaryModel;
            private _PaymentDetailsModel _paymentDetailsModel;
            private string _fakepaymentid;
            private string _cancelPaymentUrl;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup
                _requestId = "Request";
                GetMockFor<IPageService>()
                    .Setup(
                        x =>
                            x.GetVariableFromRoutingQuerystringOrForm<string>(
                                Constants.PARAM_IDENTIFIER,
                                false))
                    .Returns(_requestId);

                _fakepaymentid = "fakePaymentId";
                GetMockFor<IPageService>()
                    .Setup(
                        x =>
                            x.GetVariableFromRoutingQuerystringOrForm<string>(
                                PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_FakePaymentId,
                                false))
                    .Returns(_fakepaymentid);

                _statusResult = new StatusResult
                    <GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>()
                {
                    Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Success,
                    Result = new PaymentRequestDataModel()
                    {
                        FakePaymentsEnabled = true,
                        FakePaymentKey = _fakepaymentid,
                        OrderSummary = new PaymentOrderSummaryDataModel(),
                        UserDetails = new PaymentUserDetailsDataModel(),
                        WebsiteAccount = new WebsiteAccountDataModel()
                        {
                            WebsiteName = "My Website"
                        },
                        PaymentExpiryTimespan = new TimeSpan(0, 0, 32, 0)

                    }
                };
                GetMockFor<IPaymentRequestDataModelService>().Setup(x => x.GetPaymentMethodRequestForPaymentRequestId(
                    _mockedSessionObject,
                    _CultureModel,
                    _requestId,
                    _fakepaymentid)).Returns(_statusResult);


                _paymentOrderSummaryModel = new _PaymentOrderSummaryModel();
                GetMockFor<IPaymentOrderSummaryModelService>().Setup(x => x.CreatePaymentOrderSummaryModel(
                    _mockedSessionObject,
                    _CultureModel,
                    _statusResult.Result.OrderSummary)).Returns(_paymentOrderSummaryModel);


                _paymentDetailsModel = new _PaymentDetailsModel();
                GetMockFor<IPaymentDetailsModelService>().Setup(x => x.CreatePaymentDetailsModel(
                    _mockedSessionObject,
                    _CultureModel,
                    _statusResult.Result.UserDetails)).Returns(_paymentDetailsModel);

                _mockedContentTexts.MockItemWithReplacementModel(
                    PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Payment_CancelAndGoBackToWebsite,
                    _statusResult.Result.WebsiteAccount.WebsiteName);


                _cancelPaymentUrl = "http://www.cancel.com";
                GetMockFor<ISectionDataPresentationService>().Setup(x => x.GetSectionUrlWithRouteValuesFromIdentifier(
                    _mockedSessionObject,
                    _CultureModel,
                    PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_CancelPayment,
                    null,
                    It.Is<NameValueCollection>(
                        nvc => nvc[Constants.PARAM_IDENTIFIER] == _requestId),
                    true))
                    .Returns(_cancelPaymentUrl);



                _mockedContentTexts.MockItemWithReplacementModel(
                    PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Payment_HasExpiryMinutes,
                    (int)_statusResult.Result.PaymentExpiryTimespan.Value.TotalMinutes);
                _mockedContentTexts.MockItem(
                    PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Messages_FakePaymentEnabled);
            }

            protected override void When()
            {

                base.When();
                _result = SUT.InitalizeLayoutViewDataForCurrentRequest(_mockedSessionObject,
                    _CultureModel,
                    _actionExecutingContext);
                ;

            }

            [Test]
            public void then_fake_payment_message_should_be_set()
            {
                _result.FakePaymentsEnabledMessage.MessageContentTextModel.ShouldEqual(
                    _mockedContentTexts.Values[
                        PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Messages_FakePaymentEnabled]);
                _result.FakePaymentsEnabledMessage.MessageType.ShouldEqual(NotificationMessageType.Info);
            }

            [Test]
            public void then_expiration_content_text_should_be_present()
            {
                _result.ExpirationMessage.MessageContentTextModel.ShouldEqual(
                    _mockedContentTexts.Values[PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Payment_HasExpiryMinutes]);
                _result.ExpirationMessage.MessageType.ShouldEqual(NotificationMessageType.Info);
            }

            [Test]
            public void then_payment_request_should_be_correct()
            {
                _result.PaymentRequestDataModel.ShouldEqual(_statusResult.Result);
            }

            [Test]
            public void then_order_summary_should_be_correct()
            {
                _result.OrderSummaryModel.ShouldEqual(_paymentOrderSummaryModel);
            }

            [Test]
            public void then_user_details_should_be_correct()
            {
                _result.PaymentDetailsModel.ShouldEqual(_paymentDetailsModel);
            }

            [Test]
            public void then_cancel_and_return_back_should_be_correct()
            {
                _result.CancelAndReturnBackLink.TitleContentText.ShouldEqual(
                    _mockedContentTexts.Values[
                        PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Payment_CancelAndGoBackToWebsite]);
                _result.CancelAndReturnBackLink.Href.ShouldEqual(_cancelPaymentUrl);
            }
        }

        public class given_request_identifier_not_present : LayoutViewDataInitializerServiceSpecs
        {
            private string _requestId;

            private StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>
                _statusResult;

            private LayoutViewData _result;

            protected override void Given()
            {
                base.Given();




            }

            protected override void When()
            {

                base.When();
                _result = SUT.InitalizeLayoutViewDataForCurrentRequest(_mockedSessionObject,
                    _CultureModel,
                    _actionExecutingContext);
                ;

            }

            [Test]
            public void then_invalid_request_message_should_be_present()
            {
                ((ContentResult) _actionExecutingContext.Result).Content.ShouldEqual(_invalidRequestMessage);
                _result.ShouldBeNull();

            }
        }

        public class given_request_identifier_not_valid : LayoutViewDataInitializerServiceSpecs
        {
            private string _requestId;

            private StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>
                _statusResult;

            private LayoutViewData _result;
            private string _errorMessage;
            private string _fakepaymentid;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup
                _requestId = "Request";
                GetMockFor<IPageService>()
                    .Setup(
                        x =>
                            x.GetVariableFromRoutingQuerystringOrForm<string>(
                                Constants.PARAM_IDENTIFIER,
                                false))
                    .Returns(_requestId);
                _fakepaymentid = "fakePaymentId";
                GetMockFor<IPageService>()
                    .Setup(
                        x =>
                            x.GetVariableFromRoutingQuerystringOrForm<string>(
                                PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_FakePaymentId,
                                false))
                    .Returns(_fakepaymentid);
                _statusResult = new StatusResult
                    <GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>()
                {
                    Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Error,
                    Result = new PaymentRequestDataModel()
                };
                GetMockFor<IPaymentRequestDataModelService>().Setup(x => x.GetPaymentMethodRequestForPaymentRequestId(
                    _mockedSessionObject,
                    _CultureModel,
                    _requestId,
                    _fakepaymentid)).Returns(_statusResult);

                _errorMessage = "Error!";
                _mockedContentTexts.MockItem(_statusResult.Status, _errorMessage);


            }

            protected override void When()
            {

                base.When();
                _result = SUT.InitalizeLayoutViewDataForCurrentRequest(_mockedSessionObject,
                    _CultureModel,
                    _actionExecutingContext);
                ;

            }

            [Test]
            public void then_error_should_be_shown()
            {
                ((ContentResult) _actionExecutingContext.Result).Content.ShouldEqual(_errorMessage);
                _result.ShouldBeNull();


            }
        }

        public class given_request_expired : LayoutViewDataInitializerServiceSpecs
        {
            private string _requestId;

            private StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>
                _statusResult;

            private LayoutViewData _result;
            private string _errorMessage;
            private string _expiredUrl;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup
                _requestId = "Request";
                GetMockFor<IPageService>()
                    .Setup(
                        x =>
                            x.GetVariableFromRoutingQuerystringOrForm<string>(
                                Constants.PARAM_IDENTIFIER,
                                false))
                    .Returns(_requestId);

                _statusResult = new StatusResult
                    <GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>()
                {
                    Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Expired,
                    Result = new PaymentRequestDataModel()
                    {
                        CancelUrl = "http://www.google.com"
                    }
                };
                GetMockFor<IPaymentRequestDataModelService>().Setup(x => x.GetPaymentMethodRequestForPaymentRequestId(
                    _mockedSessionObject,
                    _CultureModel,
                    _requestId,
                    null)).Returns(_statusResult);


                _expiredUrl = "http://expired/";
                GetMockFor<ISectionDataPresentationService>().Setup(x => x.GetSectionUrlWithRouteValuesFromIdentifier(
                    _mockedSessionObject,
                    _CultureModel,
                    PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Expired,
                    null,
                    null,
                    true)).Returns(_expiredUrl);
            }

            protected override void When()
            {

                base.When();
                _result = SUT.InitalizeLayoutViewDataForCurrentRequest(_mockedSessionObject,
                    _CultureModel,
                    _actionExecutingContext);
                ;

            }

            [Test]
            public void then_result_should_be_redirect_to_expired_page()
            {
                RedirectResult redirectResult = ((RedirectResult) _actionExecutingContext.Result);
                redirectResult.Url.ShouldStartWith(_expiredUrl);
                redirectResult.Url.ShouldContain(string.Format("{0}={1}",
                    PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_CancelUrl,
                    _statusResult.Result.CancelUrl));


            }
        }
    }
}
