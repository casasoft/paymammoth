﻿using System.Web.Mvc;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Presentation.Code.ContextData;
using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.Sections;
using BusinessLogic_CS_v6.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v6.Presentation.Services.Context;
using BusinessLogic_CS_v6.Presentation.Services.NotificationMessages;
using BusinessLogic_CS_v6.Presentation.Services.Sections;
using CS.General_CS_v6.Modules.Pages;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using Moq;
using NUnit.Framework;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.HelperClasses;
using PayMammoth_v6.Presentation.Code.ViewData;
using PayMammoth_v6.Presentation.Models.Transactium;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v6.Presentation.Services.Payments;
using PayMammoth_v6Deploy.Controllers;
using Should;

namespace PayMammoth_v6_Tests.FrontendTests.NormalTests.Controllers
{
    [TestFixture]
    public abstract class TransactiumControllerSpecs
    {

       
        public abstract class PaymentSpecs :
            MySpecsForControllerAction<TransactiumController, TransactiumPaymentModel>
        {
            private CommonViewContextData _commonViewContextData;
            private SectionDataModel _sectionModel;
            private LayoutViewData _layoutViewData;
            private string _redirectUrl;

            public override ActionResult ExecuteAction()
            {
                return SUT.Payment();
            }



            protected override void Given()
            {
                //nothing
                base.Given();

                //Common Given setup

                _commonViewContextData = new CommonViewContextData();
                GetMockFor<ICommonViewContextDataService>()
                    .Setup(x => x.GetCommonViewContextData())
                    .Returns(_commonViewContextData);

                _sectionModel = new SectionDataModel();
                GetMockFor<ISectionDataPresentationService>()
                    .Setup(x => x.GetSectionDataModelByIdentifierOrCreateNew(_mockedSessionObject,
                        _CultureModel,
                        PayMammoth_v6.Enums.PayMammoth_v6Enums.Section.Payments_Payment_Transactium))
                    .Returns(_sectionModel);


                _layoutViewData = new LayoutViewData()
                {
                    PaymentRequestDataModel = new PaymentRequestDataModel()
                    {
                        PaymentRequestId = "RequestABC"
                    }
                };
                GetMockFor<IPageService>()
                    .Setup(
                        x =>
                            x.GetContextObject<LayoutViewData>(
                                PayMammoth_v6.Constants.PayMammoth_v6Constants.PayMammoth_v6LayoutViewDataKey,
                                true))
                    .Returns(_layoutViewData);

                _redirectUrl = "http://www.google.com";
                GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetVariableFromRoutingQuerystringOrForm<string>(
                                    PayMammoth_v6.Constants.PayMammoth_v6Constants.QuerystringVar_Transactium_RedirectUrl,
                                    false))
                        .Returns(_redirectUrl);

                _mockedContentTexts.MockItem(PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Transactium_PaymentDetailsTitle);
            }

            protected override void When()
            {
                //Home's action PaymentSelection is called automatically through the When() method of this base class

                base.When();
            }

            public override string GetViewNameThroughT4Mvc()
            {
                return MVCPayMammoth_v6.Transactium.Views.Payment;
            }
            public class given_payment_is_called : PaymentSpecs
            {

                protected override void Given()
                {
                    //nothing
                    base.Given();

                    //Common Given setup




                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_common_view_context_section_should_be_set()
                {
                    _commonViewContextData.Page.CurrentSection.ShouldEqual(_sectionModel);
                }
                
                [Test]
                public void then_redirect_url_should_be_fine()
                {
                    Model.RedirectUrl.ShouldEqual(_redirectUrl);

                }
                [Test]
                public void then_content_text_should_be_fine()
                {
                    Model.TitleContentTextModel.ShouldEqual(_mockedContentTexts.Values[PayMammoth_v6.Enums.PayMammoth_v6Enums.ContentText.Transactium_PaymentDetailsTitle]);
                }
               

            }
            

            
        }


        public abstract class StatusHandlerSpecs :
            MySpecsForControllerActionWithoutModelAndView<TransactiumController>
        {
            private LayoutViewData _layoutViewData;
            private string _hpsId;
            private string _paymentRequestTransactionDataId;

            public override ActionResult ExecuteAction()
            {
                return SUT.StatusHandler();
            }



            protected override void Given()
            {
                //nothing
                base.Given();

                //Common Given setup

                _layoutViewData = new LayoutViewData()
                {
                    PaymentRequestDataModel = new PaymentRequestDataModel()
                    {
                        PaymentRequestId = "PaymentRequestTransactionData/ABC"
                    }
                };
                GetMockFor<IPageService>().Setup(x => x.GetContextObject<LayoutViewData>(
                    PayMammoth_v6.Constants.PayMammoth_v6Constants.PayMammoth_v6LayoutViewDataKey,
                    true)).Returns(_layoutViewData);

                _hpsId = "HPS_ID";
                GetMockFor<IPageService>()
                    .Setup(x => x.GetVariableFromRoutingQuerystringOrForm<string>(ConstantsTransactium.HPSID, false))
                    .Returns(_hpsId);

                _paymentRequestTransactionDataId = "PaymentRequestTransactionData/123";
                GetMockFor<IPageService>()
                   .Setup(x => x.GetVariableFromRoutingQuerystringOrForm<string>(PayMammoth_v6.Connector.Constants.PARAM_TransactionId, false))
                   .Returns(_paymentRequestTransactionDataId);


            }

            protected override void When()
            {
                //Home's action PaymentSelection is called automatically through the When() method of this base class

                base.When();
            }

            public class given_request_is_success : StatusHandlerSpecs
            {
                private CheckPaymentResponse _checkPaymentResponse;

                protected override void Given()
                {
                    //nothing
                    base.Given();

                    _checkPaymentResponse = new CheckPaymentResponse()
                    {
                        Result = CheckPaymentResponse.CheckPaymentStatusType.Success,
                        SuccessRedirectUrl = "http://www.goodsite.com"
                    };
                    GetMockFor<ITransactiumService>().Setup(x => x.CheckTransactiumPayment(
                        It.Is<ReferenceLink<PaymentRequestTransactionData>>(
                            refLink => refLink.GetLinkId() == _paymentRequestTransactionDataId),
                        _hpsId)).Returns(_checkPaymentResponse);

                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_view_result_must_be_correct()
                {
                    var view = ((ViewResult)_actionResult);
                    ((TransactiumStatusHandlerModel)view.Model).RedirectUrl.ShouldEqual(_checkPaymentResponse.SuccessRedirectUrl);
                    view.ViewName.ShouldEqual(MVCPayMammoth_v6.Transactium.Views.StatusHandler);
                }



            }
            public class given_request_is_not_success : StatusHandlerSpecs
            {
                private CheckPaymentResponse _checkPaymentResponse;
                private string _selectionUrl;
                private string _finalUrl;

                protected override void Given()
                {
                    //nothing
                    base.Given();

                    _checkPaymentResponse = new CheckPaymentResponse()
                    {
                        Result = CheckPaymentResponse.CheckPaymentStatusType.Error,
                        SuccessRedirectUrl = "http://www.goodsite.com"
                    };
                    GetMockFor<ITransactiumService>().Setup(x => x.CheckTransactiumPayment(
                        It.Is<ReferenceLink<PaymentRequestTransactionData>>(
                            refLink => refLink.GetLinkId() == _paymentRequestTransactionDataId),
                        _hpsId)).Returns(_checkPaymentResponse);


                    _selectionUrl = "/selection/";
                    GetMockFor<IPaymentUrlModelService>().Setup(x => x.GetPaymentSelectionPage(
                        _mockedSessionObject, _CultureModel, _layoutViewData.PaymentRequestDataModel.PaymentRequestId)).Returns(_selectionUrl);

                    _finalUrl = "/final-url/";
                    GetMockFor<INotificationMessagesPresentationService>().Setup(x => x.AppendNotificationMessageToUrlFromIdentifier(
                        _mockedSessionObject, _CultureModel, _selectionUrl, _checkPaymentResponse.Result, NotificationMessageType.Error)).Returns(_finalUrl);





                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_view_result_must_be_correct()
                {
                    var view = ((ViewResult)_actionResult);
                    ((TransactiumStatusHandlerModel)view.Model).RedirectUrl.ShouldEqual(_finalUrl);
                    view.ViewName.ShouldEqual(MVCPayMammoth_v6.Transactium.Views.StatusHandler);
                }



            }


        }

    }
}
