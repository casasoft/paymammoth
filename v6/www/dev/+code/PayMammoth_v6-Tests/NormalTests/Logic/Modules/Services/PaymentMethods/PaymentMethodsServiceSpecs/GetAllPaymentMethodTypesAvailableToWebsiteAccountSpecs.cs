﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.PaymentMethods;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Services.PaymentMethods.PaymentMethodsServiceSpecs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v6.TestUtil;
    using BusinessLogic_CS_v6.Extensions;
    using CS.General_CS_v6.Extensions;
    using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;


    [TestFixture]
    public class GetAllPaymentMethodTypesAvailableToWebsiteAccountSpecs
    {

        [TestFixture]
        public class given_such_data : MySpecsFor_v2<PaymentMethodsService>
        {
            private WebsiteAccountData _websiteAccount;
            private List<EnumsPayMammothConnector.PaymentMethodType> _result;
            //just a default class where each test will inherit from, which can setup the initial data for all the test



            protected override void Given()
            {
                base.Given();
                //add any code after base.Given!

                _websiteAccount = new WebsiteAccountData();

                givenTheseValues();
                initialiseMocks();
            }

            /// <summary>
            /// Here, you should override values defined in the base-class.
            /// </summary>
            protected virtual void givenTheseValues()
            {

            }

            /// <summary>
            /// Here, you should initialise mocks. This is so we can define them only once, and then override them in other sub-tests.
            /// </summary>
            protected virtual void initialiseMocks()
            {

            }


            protected virtual void executeMethodUnderTest()
            {
                _result = SUT.GetAllPaymentMethodTypesAvailableToWebsiteAccount(_websiteAccount);
            }


            [TestFixture]
            public class given_this_contains_a_mix_of_enabled_and_disabled : given_such_data
            {



                protected override void givenTheseValues()
                {
                    base.givenTheseValues();
                    //override any base values here

                    _websiteAccount.PaymentsInformation.Cheque.Enabled = true;
                    _websiteAccount.PaymentsInformation.PayPal.Enabled = false;
                    _websiteAccount.PaymentsInformation.Realex.Enabled = true;
                    _websiteAccount.PaymentsInformation.Transactium.Enabled = false;
                    _websiteAccount.PaymentsInformation.Apco.Enabled = true;
                    




                }

                protected override void initialiseMocks()
                {
                    //If you want to customise the return values of a particular mock, update it here.





                    base.initialiseMocks();
                }

                protected override void When()
                {
                    base.When();
                    executeMethodUnderTest();
                }

                [Test]
                public void then_result_should_contain_just_that()
                {
                    _result.ShouldContainAllAndNothingElseFrom(EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Realex,
                        EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Apco,
                        EnumsPayMammothConnector.PaymentMethodType.Cheque);


                }
            }

            [TestFixture]
            public class given_this_contains_all_enabled : given_such_data
            {



                protected override void givenTheseValues()
                {
                    base.givenTheseValues();
                    //override any base values here

                    _websiteAccount.PaymentsInformation.Cheque.Enabled = true;
                    _websiteAccount.PaymentsInformation.PayPal.Enabled = true;
                    _websiteAccount.PaymentsInformation.Realex.Enabled = true;
                    _websiteAccount.PaymentsInformation.Transactium.Enabled = true;
                    _websiteAccount.PaymentsInformation.Apco.Enabled = true;





                }

                protected override void initialiseMocks()
                {
                    //If you want to customise the return values of a particular mock, update it here.





                    base.initialiseMocks();
                }

                protected override void When()
                {
                    base.When();
                    executeMethodUnderTest();
                }

                [Test]
                public void then_result_should_contain_just_that()
                {
                    _result.ShouldContainAllAndNothingElseFrom(EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Realex,
                        EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Apco,
                        EnumsPayMammothConnector.PaymentMethodType.Cheque,
                        EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Transactium,
                        EnumsPayMammothConnector.PaymentMethodType.PayPal_ExpressCheckout
                        );


                }
            }


        }

    }
}
