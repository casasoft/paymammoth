﻿using System;
using System.Collections.Generic;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services.Settings;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Util;
using CS.General_CS_v6.Modules.IPAddress;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using Moq;
using NLog;
using NUnit.Framework;
using PayMammoth_v6.Enums;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.Emails;
using PayMammoth_v6.Modules.Services.InitialRequests;
using PayMammoth_v6.Modules.Services.Logging;
using PayMammoth_v6.Modules.Services.Notifications;
using PayMammoth_v6.Modules.Services.PaymentMethods;
using PayMammoth_v6.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;
using Raven.Client;
using Raven.Client.Linq;
using Should;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Services.PaymentRequests
{
    [TestFixture]
    public class PaymentRequestDataServiceSpecs
    {
        [TestFixture]
        public class GeneratePaymentRequestSpecs : MySpecsFor<PaymentRequestDataService>
        {
            public class given_request_details_are_not_valid : MySpecsFor<PaymentRequestDataService>
            {
                private InitialRequestInfo _initialRequestInfo;
                private PaymentRequestDataService.GeneratePaymentRequestResult _result;
                private EnumsPayMammothConnector.InitialRequestResponseStatus _validateRequestStatus;
                private WebsiteAccountData _websiteAccount;
                private Mock<IDocumentSession> _mockSession;

                protected override void Given()
                {
                    _websiteAccount = new WebsiteAccountData();
                    _websiteAccount.Id = "Website/1";

                    _initialRequestInfo = new InitialRequestInfo();
                    _validateRequestStatus = EnumsPayMammothConnector.InitialRequestResponseStatus.PriceMustBeGreaterThanZero;
                    GetMockFor<IPaymentRequestValidatorService>()
                        .Setup(x => x.ValidateInitialRequest(_initialRequestInfo))
                        .Returns(_validateRequestStatus);

                    _mockSession = new Mock<IDocumentSession>();

                    base.Given();
                }

                protected override void When()
                {
                    //todo: [For: Karl | 2014/01/08] verify this test still works (WrittenBy: Karl)        			
                    _result = SUT.GeneratePaymentRequestFromInitialRequest(
                        _websiteAccount,
                        _initialRequestInfo,
                        _mockSession.Object);
                    base.When();
                }

                [Test]
                public void then_result_should_be_output_of_validate_request()
                {
                    _result.Status.ShouldEqual(_validateRequestStatus);
                }

                [Test]
                public void then_generated_request_should_be_null()
                {
                    _result.GeneratedRequest.ShouldBeNull();
                }
            }

            public class given_request_details_are_valid : MySpecsFor<PaymentRequestDataService>
            {
                private InitialRequestInfo _initialRequestInfo;
                private PaymentRequestDataService.GeneratePaymentRequestResult _result;
                private string _ipAddress;
                private DateTimeOffset _currentDateTime;
                private Mock<IDocumentSession> _mockSession;
                private PaymentRequestData _newRequest;
                private WebsiteAccountData _websiteAccount;

                protected override void Given()
                {
                    base.Given();
                    _websiteAccount = new WebsiteAccountData();
                    _websiteAccount.Id = "Website/1";

                    _initialRequestInfo = new InitialRequestInfo();
                    _ipAddress = "192.158.24.238";
                    _currentDateTime = new DateTimeOffset(
                        2013,
                        12,
                        30,
                        15,
                        47,
                        0,
                        0,
                        new TimeSpan(0));
                    _newRequest = new PaymentRequestData();
                    GetMockFor<IPaymentRequestValidatorService>()
                        .Setup(x => x.ValidateInitialRequest(_initialRequestInfo))
                        .Returns(EnumsPayMammothConnector.InitialRequestResponseStatus.Success);
                    GetMockFor<IDataObjectFactory>()
                        .Setup(x => x.CreateNewDataObject<PaymentRequestData>())
                        .Returns(_newRequest);
                    GetMockFor<IIPAddressRetrieverService>()
                        .Setup(x => x.GetIPAddress(true))
                        .Returns(_ipAddress);
                    GetMockFor<ICurrentDateTimeRetrieverService>()
                        .Setup(x => x.GetCurrentDateTime())
                        .Returns(_currentDateTime);
                    _mockSession = new Mock<IDocumentSession>();
                    setupSessionToLoadObject(_mockSession, _websiteAccount);
                    
                }

                protected override void When()
                {
                    _result = SUT.GeneratePaymentRequestFromInitialRequest(
                        _websiteAccount,
                        _initialRequestInfo,
                        _mockSession.Object);
                    base.When();
                }

                [Test]
                public void then_details_are_copied_from_initial_request()
                {
                    GetMockFor<IInitialRequestService>()
                        .Verify(
                            x => x.CopyDetailsFromInitialRequestToPaymentRequest(
                                _initialRequestInfo,
                                _newRequest),
                            Times.Once());
                }

                [Test]
                public void then_website_account_is_linked()
                {
                    _result.GeneratedRequest.LinkedWebsiteAccountId.ToString()
                        .ShouldEqual(_websiteAccount.Id);
                }

                [Test]
                public void then_ip_address_is_stored()
                {
                    _result.GeneratedRequest.RequestServerIp.ShouldEqual(_ipAddress);
                }

                [Test]
                public void then_current_datetime_is_stored()
                {
                    _result.GeneratedRequest.RequestDateTime.ShouldEqual(_currentDateTime);
                }

                [Test]
                public void then_log_is_added()
                {
                    GetMockFor<IPayMammothLogService>()
                        .Verify(
                            x => x.AddLogEntry(
                                _result.GeneratedRequest.StatusLog,
                                It.IsAny<string>(),
                                It.IsAny<Logger>(),
                                It.IsAny<GenericEnums.NlogLogLevel>(),
                                It.IsAny<Exception>()),
                            Times.Once());
                }

                [Test]
                public void then_request_is_stored()
                {
                    _mockSession.Verify(
                        x => x.Store(_result.GeneratedRequest),
                        Times.Once());
                }

                [Test]
                public void then_result_is_new_request_and_succeess()
                {
                    _result.GeneratedRequest.ShouldEqual(_newRequest);
                    _result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.Success);
                }
            }
        }

        [TestFixture]
        public class GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsSpecs : MySpecsFor<PaymentRequestDataService>
        {
            public class given_payment_method_transaction_generator_does_not_exist : MySpecsFor<PaymentRequestDataService>
            {
                private EnumsPayMammothConnector.PaymentMethodType _paymentMethodEnum;

                private Mock<IDocumentSession> _mockSession;
                private PaymentMethodData _paymentMethod;
                private ReferenceLink<PaymentRequestData> _requestId;

                protected override void Given()
                {
                    _paymentMethod = new PaymentMethodData();

                    _paymentMethodEnum = EnumsPayMammothConnector.PaymentMethodType.Moneybookers;
                    _paymentMethod.PaymentMethod = _paymentMethodEnum;
                    GetMockFor<IPaymentMethodsService>()
                        .Setup(x => x.GetTransactionGeneratorServiceForPaymentMethod(_paymentMethodEnum))
                        .Returns((IPaymentMethodTransactionGeneratorService) null);
                    _requestId = new ReferenceLink<PaymentRequestData>("request/1");

                    _mockSession = new Mock<IDocumentSession>();
                    base.Given();
                }

                protected override void When()
                {
                    //2014-07-21 commented out as it was'nt building [karl]
                    //Assert.Throws<InvalidOperationException>(
                    //    () =>
                    //    {
                    //        SUT.GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructions(
                    //            _paymentMethod.PaymentMethod,
                    //            _requestId,
                    //            _mockSession.Object);
                    //    });
                    base.When();
                }

                [Test]
                public void then_error_should_be_thrown()
                {
                }
            }

            public class given_payment_method_transaction_generator_exists : MySpecsFor<GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructionsService>
            {
                private EnumsPayMammothConnector.PaymentMethodType _paymentMethodEnum;

                private Mock<IDocumentSession> _mockSession;
                private PaymentMethodData _paymentMethod;
                private ReferenceLink<PaymentRequestData> _requestId;
                private TransactionGeneratorResult _transactionGeneratorResult;
                private TransactionGeneratorResult _result;

                protected override void Given()
                {
                    _paymentMethod = new PaymentMethodData();

                    _paymentMethodEnum = EnumsPayMammothConnector.PaymentMethodType.Moneybookers;
                    _paymentMethod.PaymentMethod = _paymentMethodEnum;
                    _requestId = new ReferenceLink<PaymentRequestData>("request/1");

                    _mockSession = new Mock<IDocumentSession>();
                    _transactionGeneratorResult = new TransactionGeneratorResult();

                    GetMockFor<IPaymentMethodTransactionGeneratorService>()
                        .Setup(
                            x => x.GenerateTransaction(
                                _requestId,
                                _mockSession.Object))
                        .Returns(_transactionGeneratorResult);

                    GetMockFor<IPaymentMethodsService>()
                        .Setup(x => x.GetTransactionGeneratorServiceForPaymentMethod(_paymentMethodEnum))
                        .Returns(
                            GetMockFor<IPaymentMethodTransactionGeneratorService>()
                                .Object);

                    base.Given();
                }

                protected override void When()
                {
                  
                    _result = SUT.GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructions(
                        _paymentMethod.PaymentMethod,
                        _requestId,
                        _mockSession.Object);
                    base.When();
                }

                [Test]
                public void then_result_should_be_returned()
                {
                    _result.ShouldEqual(_transactionGeneratorResult);
                }

              
            }
        }

        [TestFixture]
        public class MarkRequestAsSuccessfulSpecs
        {
            public class given_such_data : MySpecsFor<PaymentRequestDataMarkerService>
            {
                protected PaymentRequestData _request;
                protected PaymentRequestTransactionData _transaction;
                protected Mock<IDocumentSession> _session;

                protected override void Given()
                {
                    base.Given();
                    _request = new PaymentRequestData();
                    _request.Id = "Request/1";
                    _transaction = new PaymentRequestTransactionData();
                    _transaction.Id = "Transaction/1";
                    _session = new Mock<IDocumentSession>();
                    _session.Setup(x => x.Load<PaymentRequestData>(_request.Id))
                        .Returns(_request);
                    _session.Setup(x => x.Load<PaymentRequestTransactionData>(_transaction.Id))
                        .Returns(_transaction);
                    
                }
            }

            public class given_request_already_marked_as_paid : given_such_data
            {
                protected override void Given()
                {
                    base.Given();
                    _request.PaymentStatus.Status = PaymentStatusInfo.PaymentStatus.Paid;
                }

                protected override void When()
                {
                    Assert.Throws<InvalidOperationException>(
                        () => SUT.MarkRequestAsPaidSuccessfulAndSendNotification(
                            _request,
                            _transaction,
                            _session.Object));
                    base.When();
                }

                [Test]
                public void then_error_should_be_thrown()
                {

                }
            }

            public class given_request_not_marked_as_paid : given_such_data
            {
                protected DateTimeOffset _currentDate;

                protected override void Given()
                {
                    base.Given();
                    _transaction.PaymentMethod = EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Apco;

                    _currentDate = new DateTimeOffset(
                        2014,
                        1,
                        2,
                        16,
                        5,
                        0,
                        0,
                        new TimeSpan(0));
                    GetMockFor<ICurrentDateTimeRetrieverService>()
                        .Setup(x => x.GetCurrentDateTime())
                        .Returns(_currentDate);

                    //_request.PaymentStatus.Paid = true;
                }

                protected override void When()
                {
                    SUT.MarkRequestAsPaidSuccessfulAndSendNotification(
                        _request,
                        _transaction,
                        _session.Object);
                    base.When();
                }

                [Test]
                public void then_request_is_marked_as_paid_and_details_updated()
                {
                    _request.PaymentStatus.Status.ShouldEqual(PaymentStatusInfo.PaymentStatus.Paid);
                    _request.PaymentStatus.PaidOn.ShouldEqual(_currentDate);
                    _request.PaymentStatus.PaymentMethodUsed.ShouldEqual(_transaction.PaymentMethod);
                    _request.PaymentStatus.SuccessfulTransactionId.ShouldEqual(_transaction);
                }

                [Test]
                public void then_notification_should_be_created()
                {
                    GetMockFor<INotificationMessageDataCreatorService>()
                        .Verify(
                            x => x.CreateNotificationMessageForSuccessfulPaymentAndSend(
                                _session.Object,_transaction),
                            Times.Once());
                    //GetMockFor<INotificationMessageDataCreatorService>()
                    //    .Verify(
                    //        x => x.CreateNotificationMessageForSuccessfulPaymentAndSend(
                    //            _session.Object,
                    //            It.Is<ReferenceLink<PaymentRequestTransactionData>>(t => t.GetLinkId() == _transaction.Id)),
                    //        Times.Once());
                }

                [Test]
                public void then_emails_should_be_sent()
                {
                    GetMockFor<IPayMammothEmailsService>()
                        .Verify(
                            x => x.SendEmailsAboutSuccessfulPayment(_transaction),
                            Times.Once());
                }
            }
        }


    }
}