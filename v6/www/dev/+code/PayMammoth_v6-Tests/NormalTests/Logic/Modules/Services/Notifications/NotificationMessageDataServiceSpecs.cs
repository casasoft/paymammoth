﻿using System;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Modules.Services._Shared.Data;
using BusinessLogic_CS_v6.Util;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using Moq;
using NUnit.Framework;
using PayMammoth_v6.Modules.Data.Notifications;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Services.Notifications;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.Connector;
using Should;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Services.Notifications
{
    [TestFixture]
    public class NotificationMessageDataServiceSpecs
    {
        [TestFixture]
        public class CreateNotificationMessageDataForPaymentRequestDataWithNotificationMessageTypeSpecs
        {
            private static PaymentRequestData _paymentRequestData;
            private static NotificationMessageData _result;
            private static EnumsPayMammothConnector.NotificationMessageType _notificationMessageType;
            private static string _sendToUrl;

            [TestFixture]
            public class given_dataset : MySpecsFor<NotificationMessageDataService>
            {
                protected override void Given()
                {
                    base.Given();
                    _paymentRequestData = new PaymentRequestData();
                    _paymentRequestData.Id = "PaymentRequestData/1";
                    _sendToUrl = "[SendToUrl]";
                    GetMockFor<IPaymentRequestNotificationsUtilService>().Setup(x => x.GetNotificationUrlForPaymentRequest(_mockedSession.Object,
                        _paymentRequestData)).Returns(_sendToUrl);

                    _paymentRequestData.RequestDetails.Details.OrderReference = "[OrderReference]";

                    GetMockFor<IDataObjectFactory>().Setup(x => x.CreateNewDataObject<NotificationMessageData>()).Returns(new NotificationMessageData());
                }
            }

            public class given_PaymentRequestData_exists : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                    GetMockFor<IRavenDbService>()
                        .Setup(
                            x => x.GetById(
                                _mockedSession.Object,
                                It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestData.Id),
                                ThrowErrorIfNotAlreadyLoadedInSession.No,
                        LoadOnlyAvailableToFrontend.Yes))
                        .Returns(_paymentRequestData);
                }

                public class and_NotificationMessageType_is_ImmediatePaymentSuccess : given_PaymentRequestData_exists
                {
                    protected override void Given()
                    {
                        base.Given();
                        _notificationMessageType = EnumsPayMammothConnector.NotificationMessageType.ImmediatePaymentSuccess;
                    }

                    protected override void When()
                    {
                        base.When();
                        _result = SUT.CreateNotificationMessageDataForPaymentRequestDataWithNotificationMessageType(
                            _paymentRequestData,
                            _notificationMessageType,
                            _mockedSession.Object);
                    }

                    [Test]
                    public void then_Identifier_should_be_ok()
                    {
                        _result.Identifier.ShouldEqual("[OrderReference]");
                    }

                    [Test]
                    public void then_CreatedOn_should_be_ok()
                    {
                        _result.CreatedOn.ShouldEqual(_dateTime);
                    }

                    [Test]
                    public void then_LinkedPaymentRequestId_should_be_ok()
                    {
                        _result.LinkedPaymentRequestId.ShouldEqual(_paymentRequestData);
                    }

                    [Test]
                    public void then_NotificationType_should_be_ok()
                    {
                        _result.NotificationType.ShouldEqual(EnumsPayMammothConnector.NotificationMessageType.ImmediatePaymentSuccess);
                    }

                    [Test]
                    public void then_SendToUrl_should_be_ok()
                    {
                        _result.SendToUrl.ShouldEqual("[SendToUrl]");
                    }

                    [Test]
                    public void then_Status_should_be_ok()
                    {
                        _result.Status.ShouldEqual(EnumsPayMammothConnector.NotificationMessageStatus.Pending);
                    }

                    [Test]
                    public void then_NotificationMessageData_should_be_stored()
                    {
                        _mockedSession.Verify(x => x.Store(_result), Times.Once());
                    }
                }
            }

            public class given_PaymentRequestData_DOESNT_exists : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                    GetMockFor<IRavenDbService>()
                        .Setup(
                            x => x.GetById(
                                _mockedSession.Object,
                                It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestData.Id),
                                ThrowErrorIfNotAlreadyLoadedInSession.No,
                        LoadOnlyAvailableToFrontend.Yes))
                        .Returns((PaymentRequestData)null);
                }

                [Test]
                public void then_error_is_thrown()
                {
                    Assert.Throws<InvalidOperationException>(() => _result = SUT.CreateNotificationMessageDataForPaymentRequestDataWithNotificationMessageType(
                        _paymentRequestData,
                        _notificationMessageType,
                        _mockedSession.Object));
                }
            }
        }
    }
}