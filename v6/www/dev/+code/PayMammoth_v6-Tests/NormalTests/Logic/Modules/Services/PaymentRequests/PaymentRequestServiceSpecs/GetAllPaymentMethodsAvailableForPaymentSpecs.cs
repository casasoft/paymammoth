﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Repositories.Payments;
using PayMammoth_v6.Modules.Services.PaymentMethods;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Services.PaymentRequests.PaymentRequestServiceSpecs
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v6.TestUtil;
    using BusinessLogic_CS_v6.Extensions;
    using CS.General_CS_v6.Extensions;
    using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;


    [TestFixture]
    public class GetAllPaymentMethodsAvailableForPaymentSpecs
    {

        [TestFixture]
        public class given_such_data : MySpecsFor_v2<PaymentRequestDataService>
        {
            private PaymentRequestData _paymentRequest;
            private List<PaymentMethodData> _result;
            private List<PaymentMethodData> _originalAvailablePaymentMethods;
            private WebsiteAccountData _websiteAccount;
            private List<EnumsPayMammothConnector.PaymentMethodType> _paymentMethodTypesAvailableToAccount;
            //just a default class where each test will inherit from, which can setup the initial data for all the test

            private void addOriginalAvailablePaymentMethod(EnumsPayMammothConnector.PaymentMethodType paymentMethodType)
            {
                var newItem = new PaymentMethodData();

                newItem.Title["en"] = paymentMethodType.ToString();
                newItem.PaymentMethod = paymentMethodType;

                _originalAvailablePaymentMethods.Add(newItem);
            }

            protected override void Given()
            {
                base.Given();
                //add any code after base.Given!

                var session = getCurrentActualRavenDbSession();

                _paymentRequest = new PaymentRequestData();

                _websiteAccount = new WebsiteAccountData();

                session.Store(_paymentRequest);
                session.Store(_websiteAccount);
                _paymentRequest.LinkedWebsiteAccountId = _websiteAccount;


                _originalAvailablePaymentMethods = new List<PaymentMethodData>();

                addOriginalAvailablePaymentMethod(EnumsPayMammothConnector.PaymentMethodType.PayPal_ExpressCheckout);
                addOriginalAvailablePaymentMethod(EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Apco);
                addOriginalAvailablePaymentMethod(EnumsPayMammothConnector.PaymentMethodType.Neteller);
                addOriginalAvailablePaymentMethod(EnumsPayMammothConnector.PaymentMethodType.Cheque);

                _paymentMethodTypesAvailableToAccount = new List<EnumsPayMammothConnector.PaymentMethodType>();


                session.SaveChanges();

                givenTheseValues();
                initialiseMocks();
            }

            /// <summary>
            /// Here, you should override values defined in the base-class.
            /// </summary>
            protected virtual void givenTheseValues()
            {

            }

            /// <summary>
            /// Here, you should initialise mocks. This is so we can define them only once, and then override them in other sub-tests.
            /// </summary>
            protected virtual void initialiseMocks()
            {
                GetMockFor<IPaymentMethodsRepository>()
                    .Setup(x => x.GetAllPaymentMethods(It.IsAny<IDocumentSession>()))
                    .Returns(_originalAvailablePaymentMethods);

                GetMockFor<IPaymentMethodsService>()
                    .Setup(x => x.GetAllPaymentMethodTypesAvailableToWebsiteAccount(_websiteAccount))
                    .Returns(this._paymentMethodTypesAvailableToAccount);

            }


            protected virtual void executeMethodUnderTest()
            {
                _result = SUT.GetAllPaymentMethodsAvailableForPayment(_paymentRequest,base.getCurrentActualRavenDbSession());
            }


            [TestFixture]
            public class given_two_payment_methods_available : given_such_data
            {



                protected override void givenTheseValues()
                {
                    base.givenTheseValues();
                    //override any base values here
                    _paymentMethodTypesAvailableToAccount.Add(EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Apco);
                    _paymentMethodTypesAvailableToAccount.Add(EnumsPayMammothConnector.PaymentMethodType.PayPal_ExpressCheckout); 

                }

                protected override void initialiseMocks()
                {
                    //If you want to customise the return values of a particular mock, update it here.





                    base.initialiseMocks();
                }

                protected override void When()
                {
                    base.When();
                    executeMethodUnderTest();
                }

                [Test]
                public void then_result_should_contain_apco_and_paypal()
                {
                    _result.Count.ShouldEqual(2);
                    _result.Any(x => x.PaymentMethod == EnumsPayMammothConnector.PaymentMethodType.PaymentGateway_Apco).ShouldBeTrue();
                    _result.Any(x => x.PaymentMethod == EnumsPayMammothConnector.PaymentMethodType.PayPal_ExpressCheckout).ShouldBeTrue();
                    


                }
            }



        }

    }

}
