﻿using System.Collections.Generic;
using BusinessLogic_CS_v6.Modules.Services._Shared.BaseIsoEnumDatas;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using NUnit.Framework;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;
using Should;
using BusinessLogic_CS_v6.Modules.Data._Shared.BaseObjectWithIdentifiers;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Services.PaymentRequests
{
    [TestFixture]
    public class PaymentRequestValidatorServiceSpecs
    {
        public class class_under_test : MySpecsFor<PaymentRequestValidatorService>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time


            [TestFixture]
            public class ValidateRequestSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {
                    private InitialRequestInfo _request;
                    private PaymentRequestItemLine _line1;


                    protected override void Given()
                    {
                        _request = new InitialRequestInfo();

                        base.Given();

                        _request.Details = new PaymentRequestDetails();
                        _request.Details.ClientContactDetails = new ClientContactDetails();
                        _request.Details.ClientContactDetails.Address1 = "Address1";
                        _request.Details.ClientContactDetails.Address2 = "Address2";
                        _request.Details.ClientContactDetails.Country3LetterCode = "MLT";
                        _request.Details.ClientContactDetails.Email = "test@testifnasdfsad.com";
                        _request.Details.ClientContactDetails.IpAddress = "127.0.0.1";
                        _request.Details.ClientContactDetails.LastName = "Cassar";
                        _request.Details.ClientContactDetails.Locality = "Zurrieq";
                        _request.Details.ClientContactDetails.MiddleName = "None";
                        _request.Details.ClientContactDetails.Mobile = "7979 5566";
                        _request.Details.ClientContactDetails.Name = "Karl";
                        _request.Details.ClientContactDetails.PostCode = "PST12354";
                        _request.Details.ClientContactDetails.State = "state";
                        _request.Details.ClientContactDetails.Telephone = "123";
                        _request.Details.ClientReference = "Client-Ref";
                        _request.Details.Description = "Desc";
                        _request.Details.OrderReference = "Order-Ref";
                        _request.Details.Title = "Title";
                        _request.ItemDetails = new List<PaymentRequestItemLine>();
                        _line1 = new PaymentRequestItemLine();
                        _request.ItemDetails.Add(_line1);
                        _line1.Description = "line 1 desc";
                        _line1.Quantity = 1;
                        _line1.TaxAmountPerUnit = 18;
                        _line1.Title = "line 1 title";
                        _line1.UnitPrice = 100;
                        _request.Language = EnumsPayMammothConnector.SupportedLanguage.English;
                        _request.NotificationUrl = "http://notificationUrl";
                        _request.OrderLinkOnClientWebsite = "http://orderlink.onclinet.webstit";
                        _request.Pricing.CurrencyCode3Letter = "EUR";
                        _request.Pricing.HandlingAmount = 5;
                        _request.Pricing.ShippingAmount = 10;
                        _request.ReturnUrls.SuccessUrl = "http://return/success";
                        _request.ReturnUrls.CancelUrl= "http://return/failure";
                        

                        Iso4217_CurrencyData eurCurrency = CS.General_CS_v6.Util.ReflectionUtil.CreateObjectWithPrivateConstructor<Iso4217_CurrencyData>();


                        GetMockFor<IIso4217_CurrencyDataCache>().Setup(x => x.GetCurrencyByCurrencyCode(_request.Pricing.CurrencyCode3Letter))
                            .Returns(eurCurrency);
                        





                    }

                    [TestFixture]
                    public class given_data_is_ok : given_such_data
                    {
                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!




                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_success()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.Success);


                        }
                    }
                 

                    [TestFixture]
                    public class given_success_url_is_empty : given_such_data
                    {


                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!


                            _request.ReturnUrls.SuccessUrl = "";

                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_failure()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.ReturnUrlSuccessIsRequired);


                        }
                    }

                    [TestFixture]
                    public class given_failure_url_is_empty : given_such_data
                    {


                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!


                            _request.ReturnUrls.CancelUrl = "";

                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_failure()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.ReturnUrlFailureIsRequired);


                        }
                    }

                    [TestFixture]
                    public class given_notification_url_is_empty : given_such_data
                    {


                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!


                            _request.NotificationUrl = "";

                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_failure()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.NotificationUrlIsRequred);


                        }
                    }

                    [TestFixture]
                    public class given_no_items : given_such_data
                    {


                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!


                            _request.ItemDetails.Clear();

                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_failure()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.PleaseSpecifyAtLeastOneItem);


                        }
                    }
                    [TestFixture]
                    public class given_no_currency : given_such_data
                    {


                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!


                            _request.Pricing.CurrencyCode3Letter = "";

                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_failure()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.CurrencyCodeNotFilledIn);


                        }
                    }

                    [TestFixture]
                    public class given_invalid_currency : given_such_data
                    {


                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!


                            _request.Pricing.CurrencyCode3Letter = "12F";

                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_failure()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.CurrencyCodeInvalid);


                        }
                    }

                    [TestFixture]
                    public class given_item_price_is_zero : given_such_data
                    {


                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _line1.UnitPrice = 0;
                            

                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_failure()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.ItemPriceMustNeverBeZeroOrLess);


                        }
                    }
                    [TestFixture]
                    public class given_item_price_is_negative : given_such_data
                    {


                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _line1.UnitPrice = -50;


                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_failure()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.ItemPriceMustNeverBeZeroOrLess);


                        }
                    }
                    [TestFixture]
                    public class given_quantity_is_zero : given_such_data
                    {


                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _line1.Quantity = 0;


                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_failure()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.ItemQuantityMustBeOneOrGreater);


                        }
                    }
                    [TestFixture]
                    public class given_quantity_price_is_negative : given_such_data
                    {


                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _line1.Quantity = -50;


                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_failure()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.ItemQuantityMustBeOneOrGreater);


                        }
                    }

                    [TestFixture]
                    public class given_line_title_is_empty : given_such_data
                    {


                        private EnumsPayMammothConnector.InitialRequestResponseStatus _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _line1.Title = null;


                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.ValidateInitialRequest(_request);
                        }

                        [Test]
                        public void then_result_should_be_failure()
                        {
                            _result.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.ItemTitleMustBeFilledIn);


                        }
                    }



                }
            }
        }


    }
    

}
