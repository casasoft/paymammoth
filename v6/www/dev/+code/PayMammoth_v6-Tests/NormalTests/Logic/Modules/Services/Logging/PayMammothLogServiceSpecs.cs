﻿using System;
using BusinessLogic_CS_v6.EnumsBL;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using CS.General_CS_v6.Extensions;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using NUnit.Framework;
using PayMammoth_v6.Modules.Data._Shared;
using PayMammoth_v6.Modules.Services.Logging;
using Should;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Services.Logging
{
    [TestFixture]
    public class PayMammothLogServiceSpecs
    {

        [TestFixture]
        public class AddLogEntrySpecs
        {
            [TestFixture]
            public class given_such_data : MySpecsFor<PayMammothLogService>
            {
                protected PayMammothLog _log;
                protected int _initialLogLen;
                protected DateTimeOffset _currentDate;


                protected override void Given()
                {
                    base.Given();
                    _log = new PayMammothLog();
                    _log.LogData =
@"
ABCDEFGHJIJKLMONPQRSTUVWXYZ
ABCDEFGHJIJKLMONPQRSTUVWXYZ
ABCDEFGHJIJKLMONPQRSTUVWXYZ
ABCDEFGHJIJKLMONPQRSTUVWXYZ
ABCDEFGHJIJKLMONPQRSTUVWXYZ
ABCDEFGHJIJKLMONPQRSTUVWXYZ
ABCDEFGHJIJKLMONPQRSTUVWXYZ
---------------------------";
                    _initialLogLen = _log.LogData.Length;


                    _currentDate = new DateTimeOffset(2014,1,2,4,3,4,new TimeSpan());

                    GetMockFor<ICurrentDateTimeRetrieverService>().Setup(x => x.GetCurrentDateTime()).Returns(_currentDate);

                    
                }
            }

            [TestFixture]
            public class given_add_log_entry_is_called_and_exception_is_null : given_such_data
            {


                protected override void Given()
                {

                    base.Given();
                }

                protected override void When()
                {
                    
                    base.When();
                    SUT.AddLogEntry(_log, "TestMsgGenerated", null, GenericEnums.NlogLogLevel.Error, null);
                }

                [Test]
                public void then_msg_should_contain_correct_data()
                {
                    _log.LogData.ShouldContain("ABCDEFGHJIJKLMONPQRSTUVWXYZ");
                    
                    _log.LogData.ShouldContain("TestMsgGenerated");
                    _log.LogData.ShouldContain(_currentDate.ToString("yyyy-MM-dd"));
                    _log.LogData.ShouldContain(System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());
                    _log.LogData.Length.IsGreaterThan(_initialLogLen);
                    _log.LogData.ShouldNotContain("EXCEPTION DETAILS");

                }
            }
            [TestFixture]
            public class given_add_log_entry_is_called_and_exception_is_NOT_null : given_such_data
            {
                private InvalidOperationException _testEx;


                protected override void Given()
                {
                    base.Given();
                    _testEx = new InvalidOperationException("KarlException");
                }

                protected override void When()
                {

                    base.When();
                    SUT.AddLogEntry(_log, "TestMsgGenerated", null, GenericEnums.NlogLogLevel.Error, _testEx);
                }

                [Test]
                public void then_msg_should_contain_exception_data()
                {
                    _log.LogData.ShouldContain("KarlException");
                    _log.LogData.ShouldContain("EXCEPTION DETAILS");

                }
            }
        }
    }

}
