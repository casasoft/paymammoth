﻿using System;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using BusinessLogic_CS_v6.Util;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using Moq;
using NUnit.Framework;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.Payments.Helpers;
using PayMammoth_v6.Modules.Services.Notifications;
using PayMammoth_v6.Modules.Services.PaymentRequests;
using Raven.Abstractions.Exceptions;
using Raven.Client;
using Should;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Services.PaymentRequests
{
    [TestFixture]
    public class MarkRequestAsExpiredServiceSpecs
    {
        [TestFixture]
        public class MarkPaymentRequestAsExpiredSpecs
        {
            private static PaymentRequestData _paymentRequestData;
            private static PaymentRequestTransactionData _paymentRequestTransactionData;

            [TestFixture]
            public class given_dataset : MySpecsFor<PaymentRequestDataMarkerService>
            {
                protected override void Given()
                {
                    base.Given();
                    _paymentRequestData = new PaymentRequestData();
                    _paymentRequestTransactionData = new PaymentRequestTransactionData();

                    _paymentRequestData.Id = "PaymentRequestData/1";
                    _paymentRequestTransactionData.Id = "PaymentRequestTransactionData/1";
                    

                }
            }

            public class given_PaymentRequestData_exists : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                    GetMockFor<IRavenDbService>().Setup(x => x.GetById(
                        _mockedSession.Object,
                        It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestData.Id),
                        ThrowErrorIfNotAlreadyLoadedInSession.No, 
                        LoadOnlyAvailableToFrontend.Yes)).Returns(_paymentRequestData);
                }

                public class and_PaymentRequestTransactionData_exists : given_PaymentRequestData_exists
                {
                    protected override void Given()
                    {
                        base.Given();
                        GetMockFor<IRavenDbService>().Setup(x => x.GetById(
                           _mockedSession.Object,
                        It.Is<ReferenceLink<PaymentRequestTransactionData>>(y => y == _paymentRequestTransactionData.Id),
                           ThrowErrorIfNotAlreadyLoadedInSession.No,
                        LoadOnlyAvailableToFrontend.Yes)).Returns(_paymentRequestTransactionData);
                    }

                    public class and_PaymentRequestData_PaymentStatusInfo_Status_is_PENDING : and_PaymentRequestTransactionData_exists
                    {
                        protected override void Given()
                        {
                            base.Given();
                            _paymentRequestData.PaymentStatus.Status = PaymentStatusInfo.PaymentStatus.Pending;
                        }

                        public class and_SaveChanges_is_successful : and_PaymentRequestData_PaymentStatusInfo_Status_is_PENDING
                        {
                            protected override void Given()
                            {
                                base.Given();
                            }

                            protected override void When()
                            {
                                base.When();
                                SUT.MarkPaymentRequestAsExpiredAndSendNotification(
                                    _paymentRequestData,
                             //       _paymentRequestTransactionData,
                                    _mockedSession.Object);
                            }

                            [Test]
                            public void then_Status_should_be_Expired()
                            {
                                _paymentRequestData.PaymentStatus.Status.ShouldEqual(PaymentStatusInfo.PaymentStatus.Expired);
                            }

                            [Test]
                            public void then_ExpiredOn_should_be_ok()
                            {
                                _paymentRequestData.PaymentStatus.ExpiredOn.ShouldEqual(_dateTime);
                            }

                            [Test]
                            public void then_CreatePaymentRequestExpirationNotificationMessageAndPushOnAzureQueue_should_be_called()
                            {
                                GetMockFor<INotificationMessageDataCreatorService>().Verify(x => x.CreateNotificationMessageDataForImmediatePaymentExpiredAndPushToAzureQueue(
                                    _mockedSession.Object,
                                    _paymentRequestData), Times.Once());
                            }
                        }

                        public class and_SaveChanges_is_NOT_successful : and_PaymentRequestData_PaymentStatusInfo_Status_is_PENDING
                        {
                            protected override void Given()
                            {
                                base.Given();
                                _mockedSession.Setup(x => x.SaveChanges()).Callback(() => { throw new ConcurrencyException(); });
                            }

                            [Test]
                            public void then_error_should_throw()
                            {
                                Assert.Throws<ConcurrencyException>(() => SUT.MarkPaymentRequestAsExpiredAndSendNotification(
                                    _paymentRequestData,
                                //    _paymentRequestTransactionData,
                                    _mockedSession.Object));
                            }

                            [Test]
                            public void then_CreatePaymentRequestExpirationNotificationMessageAndPushOnAzureQueue_should_NOT_be_called()
                            {
                                GetMockFor<INotificationMessageDataCreatorService>().Verify(x => x.CreateNotificationMessageDataForImmediatePaymentExpiredAndPushToAzureQueue(
                                    It.IsAny<IDocumentSession>(),
                                    It.IsAny<ReferenceLink<PaymentRequestData>>()), Times.Never());
                            }
                        }
                    }

                    public class and_PaymentRequestData_PaymentStatusInfo_Status_is_NOT_PENDING : and_PaymentRequestTransactionData_exists
                    {
                        protected override void Given()
                        {
                            base.Given();
                            _paymentRequestData.PaymentStatus.Status = PaymentStatusInfo.PaymentStatus.Expired;
                        }

                        [Test]
                        public void then_error_should_throw()
                        {
                            Assert.Throws<InvalidOperationException>(() => SUT.MarkPaymentRequestAsExpiredAndSendNotification(
                                _paymentRequestData,
                             //   _paymentRequestTransactionData,
                                _mockedSession.Object));
                        }
                    }
                }

               
            }

            public class given_PaymentRequestData_DOESNT_exists : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                    GetMockFor<IRavenDbService>()
                        .Setup(
                            x => x.GetById(
                                _mockedSession.Object,
                        It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestData.Id),
                                ThrowErrorIfNotAlreadyLoadedInSession.No,
                        LoadOnlyAvailableToFrontend.Yes
                                ))
                        .Returns((PaymentRequestData)null);
                }

                [Test]
                public void then_error_should_throw()
                {
                    Assert.Throws<InvalidOperationException>(() => SUT.MarkPaymentRequestAsExpiredAndSendNotification(
                        _paymentRequestData,
                       // _paymentRequestTransactionData,
                        _mockedSession.Object));
                }
            }
        }

        [TestFixture]
        public class MarkPaymentRequestAsCancelledSpecs
        {
            private static PaymentRequestData _paymentRequestData;
            private static PaymentRequestTransactionData _paymentRequestTransactionData;

            [TestFixture]
            public class given_dataset : MySpecsFor<PaymentRequestDataMarkerService>
            {
                protected override void Given()
                {
                    base.Given();
                    _paymentRequestData = new PaymentRequestData();
                    _paymentRequestTransactionData = new PaymentRequestTransactionData();

                    _paymentRequestData.Id = "PaymentRequestData/1";
                    _paymentRequestTransactionData.Id = "PaymentRequestTransactionData/1";


                }
            }

            public class given_PaymentRequestData_exists : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                    GetMockFor<IRavenDbService>().Setup(x => x.GetById(
                        _mockedSession.Object,
                        It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestData.Id),
                        ThrowErrorIfNotAlreadyLoadedInSession.No,
                        LoadOnlyAvailableToFrontend.Yes)).Returns(_paymentRequestData);
                }

                public class and_PaymentRequestTransactionData_exists : given_PaymentRequestData_exists
                {
                    protected override void Given()
                    {
                        base.Given();
                        GetMockFor<IRavenDbService>().Setup(x => x.GetById(
                           _mockedSession.Object,
                        It.Is<ReferenceLink<PaymentRequestTransactionData>>(y => y == _paymentRequestTransactionData.Id),
                           ThrowErrorIfNotAlreadyLoadedInSession.No,
                        LoadOnlyAvailableToFrontend.Yes)).Returns(_paymentRequestTransactionData);
                    }

                    public class and_PaymentRequestData_PaymentStatusInfo_Status_is_PENDING : and_PaymentRequestTransactionData_exists
                    {
                        protected override void Given()
                        {
                            base.Given();
                            _paymentRequestData.PaymentStatus.Status = PaymentStatusInfo.PaymentStatus.Pending;
                        }

                        public class and_SaveChanges_is_successful : and_PaymentRequestData_PaymentStatusInfo_Status_is_PENDING
                        {
                            protected override void Given()
                            {
                                base.Given();
                            }

                            protected override void When()
                            {
                                base.When();
                                SUT.MarkPaymentRequestAsCancelledAndSendNotification(
                                    _paymentRequestData,
                                   // _paymentRequestTransactionData,
                                    _mockedSession.Object);
                            }

                            [Test]
                            public void then_Status_should_be_Expired()
                            {
                                _paymentRequestData.PaymentStatus.Status.ShouldEqual(PaymentStatusInfo.PaymentStatus.Cancelled);
                            }

                            [Test]
                            public void then_CancelledOn_should_be_ok()
                            {
                                _paymentRequestData.PaymentStatus.CancelledOn.ShouldEqual(_dateTime);
                            }

                            [Test]
                            public void then_CreatePaymentRequestExpirationNotificationMessageAndPushOnAzureQueue_should_be_called()
                            {
                                GetMockFor<INotificationMessageDataCreatorService>().Verify(x => x.CreateNotificationMessageDataForImmediatePaymentCancelledAndPushToAzureQueue(
                                    _mockedSession.Object,
                                    _paymentRequestData), Times.Once());
                            }
                        }

                        public class and_SaveChanges_is_NOT_successful : and_PaymentRequestData_PaymentStatusInfo_Status_is_PENDING
                        {
                            private Exception _thrownEx;

                            protected override void Given()
                            {
                                base.Given();
                                _mockedSession.Setup(x => x.SaveChanges()).Callback(() => { throw new ConcurrencyException(); });
                            }

                            protected override void When()
                            {
                                base.When();

                                try
                                {
                                    SUT.MarkPaymentRequestAsCancelledAndSendNotification(
                                    _paymentRequestData,
                                        // _paymentRequestTransactionData,
                                    _mockedSession.Object);
                                }
                                catch (Exception ex)
                                {
                                    _thrownEx = ex;
                                    
                                }

                            }

                            [Test]
                            public void then_error_should_throw()
                            {
                                Assert.IsTrue(_thrownEx is ConcurrencyException);
                            }

                            [Test]
                            public void then_CreateNotificationMessageDataForImmediatePaymentCancelledAndPushToAzureQueue_should_NOT_be_called()
                            {
                                GetMockFor<INotificationMessageDataCreatorService>().Verify(x => x.CreateNotificationMessageDataForImmediatePaymentCancelledAndPushToAzureQueue(
                                    It.IsAny<IDocumentSession>(),
                                    It.IsAny<ReferenceLink<PaymentRequestData>>()), Times.Never());
                            }
                        }
                    }

                    public class and_PaymentRequestData_PaymentStatusInfo_Status_is_NOT_PENDING : and_PaymentRequestTransactionData_exists
                    {
                        protected override void Given()
                        {
                            base.Given();
                            _paymentRequestData.PaymentStatus.Status = PaymentStatusInfo.PaymentStatus.Expired;
                        }

                        [Test]
                        public void then_error_should_throw()
                        {
                            Assert.Throws<InvalidOperationException>(() => SUT.MarkPaymentRequestAsCancelledAndSendNotification(
                                _paymentRequestData,
                               // _paymentRequestTransactionData,
                                _mockedSession.Object));
                        }
                    }
                }

                
            }

            public class given_PaymentRequestData_DOESNT_exists : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                    GetMockFor<IRavenDbService>()
                        .Setup(
                            x => x.GetById(
                                _mockedSession.Object,
                        It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestData.Id),
                                ThrowErrorIfNotAlreadyLoadedInSession.No,
                        LoadOnlyAvailableToFrontend.Yes
                                ))
                        .Returns((PaymentRequestData)null);
                }

                [Test]
                public void then_error_should_throw()
                {
                    Assert.Throws<InvalidOperationException>(() => SUT.MarkPaymentRequestAsExpiredAndSendNotification(
                        _paymentRequestData,
                       // _paymentRequestTransactionData,
                        _mockedSession.Object));
                }
            }
        }
    }
}
