﻿using System.Collections.Generic;
using BusinessLogic_CS_v6.Framework.DbObjects.References;
using BusinessLogic_CS_v6.Modules.Services.Settings;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using Moq;
using NUnit.Framework;
using PayMammoth_v6.Modules.Data.Notifications;
using PayMammoth_v6.Modules.Repositories.Notifications;
using PayMammoth_v6.Modules.Services.Notifications;
using Raven.Client;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Services.Notifications
{
    [TestFixture]
    public class NotificationsServiceSpecs
    {

        [TestFixture]
        public class CheckAndSendPendingNotificationsSpecs
        {
            [TestFixture]
            public class given_such_data : MySpecsFor<NotificationMessageDataService>
            {
                protected List<NotificationMessageData> _msgList;
                protected NotificationMessageData _msg3;
                protected NotificationMessageData _msg1;
                protected NotificationMessageData _msg2;


                protected override void Given()
                {
                    base.Given();

                    _msg1 = new NotificationMessageData();
                    _msg1.Id = "Notification/1";
                    _msg2 = new NotificationMessageData();
                    _msg2.Id = "Notification/2";
                    _msg3 = new NotificationMessageData();
                    _msg3.Id = "Notification/3";
                    _msgList = new List<NotificationMessageData>();
                    _msgList.Add(_msg1);
                    _msgList.Add(_msg2);
                    _msgList.Add(_msg3);
                }


            }

            [TestFixture]
            public class given_x_notifications : given_such_data
            {
                private Mock<IDocumentSession> _session;


                protected override void Given()
                {
                    base.Given();
                    //add any code after base.Given!
                    int pgSize = 1000;
                    _session = new Mock<IDocumentSession>();
                    GetMockFor<INotificationMessageDataRepository>().Setup(x => x.GetAllNotificationsYetToBeSent(_session.Object,pgSize)).Returns(_msgList);
                    GetMockFor<ISettingsService>().Setup(x => x.GetSettingValue<int>(PayMammoth_v6.Enums.PayMammoth_v6Enums.PayMammothSettings.Notifications_BatchSizeToSend)).Returns(pgSize);

                    

                }

                protected override void When()
                {
                    base.When();
                  
                    SUT.CheckAndSendPendingNotificationMessageDatas(_session.Object);



                }

                [Test]
                public void then_all_notifications_should_be_sent()
                {
                    GetMockFor<INotificationMessageDataSendingService>().Verify(x => x.SendNotificationMessageData(new ReferenceLink<NotificationMessageData>(_msg1.Id), null), Times.Once());
                    GetMockFor<INotificationMessageDataSendingService>().Verify(x => x.SendNotificationMessageData(new ReferenceLink<NotificationMessageData>(_msg2.Id), null), Times.Once());
                    GetMockFor<INotificationMessageDataSendingService>().Verify(x => x.SendNotificationMessageData(new ReferenceLink<NotificationMessageData>(_msg3.Id), null), Times.Once());


                }
            }
        }
    }

}
