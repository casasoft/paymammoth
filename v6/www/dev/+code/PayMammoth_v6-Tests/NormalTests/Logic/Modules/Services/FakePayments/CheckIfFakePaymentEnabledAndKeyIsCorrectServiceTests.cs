﻿using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using NUnit.Framework;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Modules.Services.FakePayments;
using Should;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Services.FakePayments
{
    [TestFixture]
    public class CheckIfPakePaymentEnabledAndKeyIsCorrectServiceSpecs
    {
        public class class_under_test : MySpecsFor<CheckIfFakePaymentEnabledAndKeyIsCorrectService>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time

            [TestFixture]
            public class CheckIfFakePaymentEnabledAndKeyIsCorrectServiceSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {
                    private WebsiteAccountData _websiteAccount;


                    protected override void Given()
                    {
                        _websiteAccount = new WebsiteAccountData();

                        base.Given();
                    }

                    [TestFixture]
                    public class given_website_account_has_fake_payments_disabled : given_such_data
                    {
                        private bool _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _websiteAccount.FakePayments.Enabled = false;


                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.CheckIfFakePaymentEnabledAndKeyIsCorrect(_websiteAccount, "any");
                        }

                        [Test]
                        public void then_result_should_be_false()
                        {
                            _result.ShouldBeFalse();


                        }
                    }

                    [TestFixture]
                    public class given_website_account_has_fake_payments_enabled : given_such_data
                    {
                        

                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _websiteAccount.FakePayments.Enabled = true;


                        }

                        [TestFixture]
                        public class given_fake_payment_key_is_empty_but_matches : given_website_account_has_fake_payments_enabled
                        {
                            private bool _result;


                            protected override void Given()
                            {
                                base.Given();
                                //add any code after base.Given!
                                _websiteAccount.FakePayments.FakePaymentKey = "";



                            }

                            protected override void When()
                            {
                                base.When();
                                _result = SUT.CheckIfFakePaymentEnabledAndKeyIsCorrect(_websiteAccount, "");
                            }

                            [Test]
                            public void then_result_should_be_false_still()
                            {

                                _result.ShouldBeFalse();

                            }
                        }
                        [TestFixture]
                        public class given_fake_payment_key_does_not_match : given_website_account_has_fake_payments_enabled
                        {
                            private bool _result;


                            protected override void Given()
                            {
                                base.Given();
                                //add any code after base.Given!
                                _websiteAccount.FakePayments.FakePaymentKey = "Test1234";



                            }

                            protected override void When()
                            {
                                base.When();
                                _result = SUT.CheckIfFakePaymentEnabledAndKeyIsCorrect(_websiteAccount, "Non-Match");
                            }

                            [Test]
                            public void then_result_should_be_false()
                            {

                                _result.ShouldBeFalse();

                            }
                        }
                        [TestFixture]
                        public class given_fake_payment_key_matches : given_website_account_has_fake_payments_enabled
                        {
                            private bool _result;


                            protected override void Given()
                            {
                                base.Given();
                                //add any code after base.Given!
                                _websiteAccount.FakePayments.FakePaymentKey = "Test1234";



                            }

                            protected override void When()
                            {
                                base.When();
                                _result = SUT.CheckIfFakePaymentEnabledAndKeyIsCorrect(_websiteAccount, "Test1234");
                            }

                            [Test]
                            public void then_result_should_be_true()
                            {

                                _result.ShouldBeTrue();

                            }
                        }


                    }
                }
            }

        }


    }
}
