﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.General_CS_v6.Util;
using NUnit.Framework;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Framework.Application
{
    [TestFixture]
    public class VerifyIocContainerTest
    {
        [Test]
        public void TestVerifyIoc()
        {
            try
            {
                InversionUtil.VerifyContainer();
            }
            catch (Exception ex)
            {
                Assert.Fail("Failed to verify container - " + ex.Message);
                throw;
            }
            
            
        }
    }
}
