﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Repositories.Payments;
using PayMammoth_v6.Modules.Services.PaymentRequests;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Services.PaymentRequests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;


    [TestFixture]
    public class PaymentRequestExpirerServiceSpecs
    {
        public class class_under_test : MySpecsFor<PaymentRequestExpirerService>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time

            [TestFixture]
            public class CheckForBatchOfExpiredPaymentRequestsAndMarkedThemAsExpiredSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {


                    protected override void Given()
                    {


                        base.Given();
                    }

                    [TestFixture]
                    public class given_condition : given_such_data
                    {
                        private List<PaymentRequestData> _list;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _list = new List<PaymentRequestData>() {
                                new PaymentRequestData() { Id = "Payment/1"},
                                new PaymentRequestData() { Id = "Payment/2"}};

                            GetMockFor<IPaymentRequestRepository>()
                                .Setup(x => x.GetAllPendingPaymentRequestsWhichAreExpired(_mockedSessionObject)).Returns(_list);


                        }

                        protected override void When()
                        {
                            base.When();

                            SUT.CheckForBatchOfExpiredPaymentRequestsAndMarkedThemAsExpired(_mockedSessionObject);

                        }

                        [Test]
                        public void then_both_should_be_marked_as_expired()
                        {
                            GetMockFor<IPaymentRequestDataMarkerService>().Verify(x=>x.MarkPaymentRequestAsExpiredAndSendNotification(
                                _list[0] ,null),Times.Once());

                            GetMockFor<IPaymentRequestDataMarkerService>().Verify(x => x.MarkPaymentRequestAsExpiredAndSendNotification(
                                _list[1], null), Times.Once());


                        }
                    }


                }
            }

        }


    }
}
