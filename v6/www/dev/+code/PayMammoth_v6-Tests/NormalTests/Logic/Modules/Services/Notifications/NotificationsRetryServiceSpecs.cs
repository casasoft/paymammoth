﻿using System;
using System.Collections.Generic;
using BusinessLogic_CS_v6.Framework.DateAndTime;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using Moq;
using NUnit.Framework;
using PayMammoth_v6.Modules.Data.Notifications;
using PayMammoth_v6.Modules.Services.Notifications;
using PayMammoth_v6.Connector;
using Raven.Client;
using Should;

namespace PayMammoth_v6_Tests.NormalTests.Logic.Modules.Services.Notifications
{
    [TestFixture]
    public class NotificationsRetryServiceSpec
    {

        [TestFixture]
        public class NotificationsRetryServiceSpecs
        {
            [TestFixture]
            public class given_such_data : MySpecsFor<NotificationsRetryService>
            {
                protected NotificationMessageData _message;
                protected Mock<IDocumentSession> _mockSession;
                protected DateTimeOffset _currentDateTime;
                protected List<int> _timeIntervalList;


                protected override void Given()
                {
                    base.Given();
                    _message = new NotificationMessageData();
                    _message.Id = "Notif/1";

                    _mockSession = new Mock<IDocumentSession>();
                    _mockSession.Setup(x=>x.Load<NotificationMessageData>(_message.Id)).Returns(_message);

                    _currentDateTime = new DateTimeOffset(2014, 1, 3, 18, 51, 0, new TimeSpan());
                    GetMockFor<ICurrentDateTimeRetrieverService>().Setup(x => x.GetCurrentDateTime()).Returns(_currentDateTime);

                    _timeIntervalList = PayMammoth_v6.Constants.GetPaymentNotificationsRetryCounts();
                       _message.Status = EnumsPayMammothConnector.NotificationMessageStatus.Pending;

                    
                }


            }

            [TestFixture]
            public class given_retry_count_was_negative : given_such_data
            {


                protected override void Given()
                {
                    base.Given();
                    _message.RetryCount = -5;
                }

                protected override void When()
                {

                    base.When();
                    SUT.IncrementRetryCountAndRepushOnAzureQueueOrMarkAsFailedIfMaximumReached(_message);

                }

                [Test]
                public void then_retry_count_should_be_zero()
                {
                    _message.RetryCount.ShouldEqual(0);
                }
                [Test]
                public void then_next_retry_should_be_correct()
                {
                    var intervalSeconds = _timeIntervalList[0];

                    _message.NextRetryOn.ShouldEqual(_currentDateTime.AddSeconds(intervalSeconds));
                }
            }
            [TestFixture]
            public class given_retry_count_was_two : given_such_data
            {


                protected override void Given()
                {
                    base.Given();
                    _message.RetryCount = 2;
                }

                protected override void When()
                {

                    base.When();
                    SUT.IncrementRetryCountAndRepushOnAzureQueueOrMarkAsFailedIfMaximumReached(_message);

                }

                [Test]
                public void then_retry_count_should_be_three()
                {
                    _message.RetryCount.ShouldEqual(3);
                }
                [Test]
                public void then_next_retry_should_be_correct()
                {
                    var intervalSeconds = _timeIntervalList[3];

                    _message.NextRetryOn.ShouldEqual(_currentDateTime.AddSeconds(intervalSeconds));
                }
            }
            [TestFixture]
            public class given_retry_count_was_exactly_like_total_interval_count_minus_one : given_such_data
            {


                protected override void Given()
                {
                    base.Given();
                    _message.RetryCount = _timeIntervalList.Count-1;
                }

                protected override void When()
                {

                    base.When();
                    SUT.IncrementRetryCountAndRepushOnAzureQueueOrMarkAsFailedIfMaximumReached(_message);

                }
                [Test]
                public void then_notification_should_be_marked_as_failed()
                {
                    _message.Status.ShouldEqual(EnumsPayMammothConnector.NotificationMessageStatus.Failed);
                }
            }
            [TestFixture]
            public class given_retry_count_was_exactly_like_total_interval_count_minus_two : given_such_data
            {


                protected override void Given()
                {
                    base.Given();
                    _message.RetryCount = _timeIntervalList.Count - 2;
                }

                protected override void When()
                {

                    base.When();
                    SUT.IncrementRetryCountAndRepushOnAzureQueueOrMarkAsFailedIfMaximumReached(_message);

                }

                [Test]
                public void then_retry_count_should_be_correct()
                {
                    _message.RetryCount.ShouldEqual(_timeIntervalList.Count-1);
                }
                [Test]
                public void then_next_retry_should_be_correct()
                {
                    var intervalSeconds = _timeIntervalList[_message.RetryCount];

                    _message.NextRetryOn.ShouldEqual(_currentDateTime.AddSeconds(intervalSeconds));
                }
            }
            [TestFixture]
            public class given_retry_count_was_larger_than_interval_list : given_such_data
            {


                protected override void Given()
                {
                    base.Given();
                    _message.RetryCount = 9999;
                }

                protected override void When()
                {

                    base.When();
                    SUT.IncrementRetryCountAndRepushOnAzureQueueOrMarkAsFailedIfMaximumReached(_message);

                }

              
                [Test]
                public void then_notification_should_be_marked_as_failed()
                {
                    _message.Status.ShouldEqual(EnumsPayMammothConnector.NotificationMessageStatus.Failed);
                }
            }
            [TestFixture]
            public class given_notification_was_already_successful: given_such_data
            {


                protected override void Given()
                {
                    base.Given();
                    _message.RetryCount = 0;
                    _message.Status = EnumsPayMammothConnector.NotificationMessageStatus.Success;
                }

                protected override void When()
                {

                    base.When();
                    Assert.Throws<InvalidOperationException>(() => SUT.IncrementRetryCountAndRepushOnAzureQueueOrMarkAsFailedIfMaximumReached(_message));

                }


                [Test]
                public void then_error_should_be_thrown()
                {
                    
                }
            }
            [TestFixture]
            public class given_notification_was_already_failed : given_such_data
            {


                protected override void Given()
                {
                    base.Given();
                    _message.RetryCount = 0;
                    _message.Status = EnumsPayMammothConnector.NotificationMessageStatus.Failed;
                }

                protected override void When()
                {

                    base.When();
                    Assert.Throws<InvalidOperationException>(() => SUT.IncrementRetryCountAndRepushOnAzureQueueOrMarkAsFailedIfMaximumReached(_message));

                }


                [Test]
                public void then_error_should_be_thrown()
                {

                }
            }

        }
    }

}
