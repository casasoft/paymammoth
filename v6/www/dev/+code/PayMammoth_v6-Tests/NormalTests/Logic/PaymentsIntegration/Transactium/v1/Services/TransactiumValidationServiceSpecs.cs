﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using PayMammoth_v6.PaymentsIntegration.Transactium.Exceptions;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.Services;
using PayMammoth_v6.PaymentsIntegration.Transactium.v1.Wrappers;

namespace PayMammoth_v6_Tests.NormalTests.Logic.PaymentsIntegration.Transactium.v1.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;


    [TestFixture]
    public class TransactiumValidationServiceSpecs
    {
        public class class_under_test : MySpecsFor<TransactiumValidationService>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time

            [TestFixture]
            public class ValidateTransactiumRequestSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {
                    private MyHPSCreatePaymentRequest _transactiumRequest;


                    protected override void Given()
                    {
                        _transactiumRequest = new MyHPSCreatePaymentRequest();
                        _transactiumRequest.SuccessURL = "http://paymammoth.com/success";
                        _transactiumRequest.FailURL = "http://paymammoth.com/failure";
                        base.Given();
                    }

                    [TestFixture]
                    public class given_success_url_is_not_a_valid_fully_qualified_url : given_such_data
                    {
                        private Exception _thrownEx;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _transactiumRequest.SuccessURL = "/invalidUrl";


                        }

                        protected override void When()
                        {
                            base.When();
                            try
                            {
                                SUT.ValidateTransactiumRequest(_transactiumRequest);
                            }
                            catch (Exception ex)
                            {
                                _thrownEx = ex;
                                
                            }
                        }

                        [Test]
                        public void then_error_should_be_thrown()
                        {
                            Assert.True(_thrownEx is TransactiumValidationException);


                        }
                    }
                    [TestFixture]
                    public class given_failure_url_is_not_a_valid_fully_qualified_url : given_such_data
                    {
                        private Exception _thrownEx;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _transactiumRequest.SuccessURL = "/invalidFail";


                        }

                        protected override void When()
                        {
                            base.When();
                            try
                            {
                                SUT.ValidateTransactiumRequest(_transactiumRequest);
                            }
                            catch (Exception ex)
                            {
                                _thrownEx = ex;

                            }
                        }

                        [Test]
                        public void then_error_should_be_thrown()
                        {
                            Assert.True(_thrownEx is TransactiumValidationException);


                        }
                    }

                    [TestFixture]
                    public class given_success_url_is_empty : given_such_data
                    {
                        private Exception _thrownEx;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _transactiumRequest.SuccessURL = "";


                        }

                        protected override void When()
                        {
                            base.When();
                            try
                            {
                                SUT.ValidateTransactiumRequest(_transactiumRequest);
                            }
                            catch (Exception ex)
                            {
                                _thrownEx = ex;

                            }
                        }

                        [Test]
                        public void then_no_error_should_be_thrown()
                        {
                            _thrownEx.ShouldBeNull();


                        }
                    }

                }
            }
        
        
        }


    }
}
