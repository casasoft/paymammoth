﻿using BusinessLogic_CS_v6.Modules.Services.RavenDb;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using Moq;
using NUnit.Framework;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications;
using PayMammoth_v6.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers;
using Raven.Client;

namespace PayMammoth_v6_Tests.NormalTests.Logic.PaymentsIntegration.PayPalIntegration.v1.ExpressCheckout.InstantPaymentNotifications
{
    [TestFixture]
    public class ProcessIpnMessageIfNotAlreadyProcessedServiceSpecs
    {
        public class class_under_test : MySpecsFor<ProcessIpnMessageIfNotAlreadyProcessedService>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time
            [TestFixture]
            public class ProcessSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {
                    protected PaymentRequestData _paymentRequest;
                    protected Mock<IDocumentSession> _mockSession;

                    //run this test
                   
                    protected override void Given()
                    {
                        _paymentRequest = new PaymentRequestData();
                        _mockSession = new Mock<IDocumentSession>();

                        _paymentRequest.Id = "PaymentRequest/1";
                        base.setupSessionToLoadObject(_mockSession, _paymentRequest);
                        base.Given();
                    }
                }

                [TestFixture]
                public class given_ipn_message_already_processed : given_such_data
                {
                    private IpnMessage _msg;
                    

                    protected override void Given()
                    {
                        base.Given();
                        //add any code after base.Given!

                        _msg = new IpnMessage(null);
                        
                        _msg.PayPalTransactionID = "test-1";
                        _msg.PaymentStatus = PayPalEnums.PAYMENT_STATUS.Completed;
                        GetMockFor<IIpnService>().Setup(x => x.CheckIfIpnMessageAlreadyVerifiedInDatabase(_msg.PayPalTransactionID, PayPalEnums.PAYMENT_STATUS.Completed,  _mockSession.Object))
                            .Returns(true);
                        GetMockFor<IRavenDbService>().Setup(x => x.CreateNewSession()).Returns(_mockedSessionObject);
                    }

                    protected override void When()
                    {
                        base.When();
                        SUT.Process(_paymentRequest,_msg);


                    }

                    [Test]
                    public void then_it_should_not_be_processed()
                    {
                        GetMockFor<IIpnService>().Verify(x => x.SaveIpnMessageAsProcessedInDatabase(It.IsAny<IpnMessage>(),
                            It.IsAny<IDocumentSession>()), Times.Never());


                    }
                }
                [TestFixture]
                public class given_ipn_message_not_processed : given_such_data
                {
                    private IpnMessage _msg;
                    
                    private Mock<IIpnMessageParser> _mockIpnParser;


                    protected override void Given()
                    {
                        base.Given();
                        //add any code after base.Given!

                        _msg = new IpnMessage(null);

                        _mockIpnParser = new Mock<IIpnMessageParser>();

                        
                        _msg.PayPalTransactionID = "test-1";
                        _msg.TransactionType = PayPalEnums.TRANSACTION_TYPE.ExpressCheckout;
                        _msg.PaymentStatus = PayPalEnums.PAYMENT_STATUS.Completed; 
                        GetMockFor<IIpnService>().Setup(x => x.CheckIfIpnMessageAlreadyVerifiedInDatabase(_msg.PayPalTransactionID,
                            _msg.PaymentStatus,
                             
                            _mockSession.Object))
                            .Returns(false);
                        GetMockFor<IGetIpnMessageParserBasedOnTransactionTypeService>()
                            .Setup(x => x.GetIpnMessageParserBasedOnTransactionType(_msg.TransactionType)).Returns(_mockIpnParser.Object);

                        GetMockFor<IRavenDbService>().Setup(x => x.CreateNewSession()).Returns(_mockedSessionObject);

                    }

                    protected override void When()
                    {
                        base.When();
                        SUT.Process(_paymentRequest,_msg);


                    }

                    [Test]
                    public void then_it_should_be_saved_in_database()
                    {
                        GetMockFor<IIpnService>().Verify(x => x.SaveIpnMessageAsProcessedInDatabase(_msg,null),Times.Once());
                        


                    }
                    [Test]
                    public void then_ipn_parser_should_be_called()
                    {

                        _mockIpnParser.Verify(x => x.HandleIpnMessage(_msg, null), Times.Once());


                    }
                }
            }

        }


    }
}
