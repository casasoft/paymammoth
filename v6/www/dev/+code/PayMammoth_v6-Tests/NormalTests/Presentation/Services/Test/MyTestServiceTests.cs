﻿using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using NUnit.Framework;
using PayMammoth_v6.Presentation.Services.Test;
using Should;

namespace PayMammoth_v6_Tests.NormalTests.Presentation.Services.Test
{
    [TestFixture]
    public class MyTestServiceSpecs
    {

        [TestFixture]
        public class AddSpecs : MySpecsFor<MyTestService>
        { 
            
            
            /*
             * Test Data

             Num 1 = 40
             Num 2 = 28
             
             */
            public class given_the_above_test_data : MySpecsFor<MyTestService>
            {
                private int _num1;
                private int _num2;
                private int _result;


                protected override void Given()
                {
                    _num1 = 40;
                    _num2 = 28;


                    base.Given();
                }

                protected override void When()
                {
                    _result = SUT.AddValues(_num1, _num2);
                    base.When();
                }

                [Test]
                public void then_result_should_be_68()
                {
                    _result.ShouldEqual(68);
                }
            }

        }
    }

    
}
