﻿using BusinessLogic_CS_v6.Presentation.Models._Shared.Data.BaseIsoEnumDatas;
using BusinessLogic_CS_v6.Presentation.Services._Shared.BaseIsoEnumDatas;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using NUnit.Framework;
using PayMammoth_v6.Modules.Data.Payments;
using PayMammoth_v6.Modules.Data.WebsiteAccounts;
using PayMammoth_v6.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v6.Presentation.Services.Models;
using Should;

namespace PayMammoth_v6_Tests.NormalTests.Presentation.Services.Models
{
    [TestFixture]
    public class FillPaymentRequestDataModelFromPaymentRequestDataServiceSpecs
    {
        [TestFixture]
        public class CreatePaymentRequestDataModelFromPaymentRequestDataSpecs
        {
            private static PaymentRequestData _paymentRequestData;
            private static WebsiteAccountData _websiteAccountData;
            private static string _fakePaymentKey;
            private static PaymentRequestDataModel _result;

            [TestFixture]
            public class given_dataset : MySpecsFor<FillPaymentRequestDataModelFromPaymentRequestDataService>
            {
                protected override void Given()
                {
                    base.Given();
                    _paymentRequestData = new PaymentRequestData();
                    _websiteAccountData = new WebsiteAccountData();
                    _paymentRequestData.RequestDetails.Pricing.CurrencyCode3Letter = "EUR";
                    _fakePaymentKey = "[FakePaymentKey]";
                }
            }
            //2014-07-21 [karl] this was commented out as it was in no way actually testing the logic of this.
            //public class when_called : given_dataset
            //{
            //    protected override void Given()
            //    {
            //        base.Given();
                    
            //        Iso4217_CurrencyDataModel eurCurrency = new Iso4217_CurrencyDataModel();
            //        GetMockFor<IIso4217_CurrencyDataPresentationService>()
            //            .Setup(x => x.GetCurrencyModelByCurrencyCode(_CultureModel,
            //                _paymentRequestData.RequestDetails.Pricing.CurrencyCode3Letter))
            //                .Returns(eurCurrency);
            //    }

            //    protected override void When()
            //    {
            //        base.When();
            //        _result = SUT.CreatePaymentRequestDataModelFromPaymentRequestData(
            //            _paymentRequestData,
            //            _websiteAccountData,
            //            _CultureModel,
            //            _mockedSession.Object,
            //            _fakePaymentKey);
            //    }

            //    //[Test]
            //    //public void then_FakePaymentKey_should_Be_ok()
            //    //{
            //    //2014-07-21 [karl] this was commented out as it was in no way actually testing the logic of this.
            //    //    _result.FakePaymentKey.ShouldEqual("[FakePaymentKey]");

            //    //}
            //}
        }
    }
}
