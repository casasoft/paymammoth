﻿using System.Net;
using General_Tests_CS_v6.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using Moq;
using NUnit.Framework;
using PayMammoth_v6.Connector;
using PayMammoth_v6.Connector.InitialRequests;
using PayMammoth_v6.Connector.Services;
using PayMammoth_v6.Connector.Services.HttpRequests;
using Should;

namespace PayMammoth_v6_Tests.ConnectorTests.Services
{
    [TestFixture]
    public class InitialRequestServiceSpecs
    {
        public class class_under_test : MySpecsFor<PayMammothConnectorInitialRequestService>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time
            [TestFixture]
            public class CreatePaymentRequestOnPayMammothSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {


                    protected override void Given()
                    {


                        base.Given();
                    }

                    [TestFixture]
                    public class given_the_request_is_successful : given_such_data
                    {
                        private string _websiteAccountCode;
                        private string _secretWord;
                        private InitialRequestInfo _initialRequest;
                        private InitialRequestResponseMessage _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _websiteAccountCode = "Website";
                            _secretWord = "123456";
                            _initialRequest = new InitialRequestInfo();
                            _initialRequest.OrderLinkOnClientWebsite = "http://www.testwebsite.com";

                            string url = Constants.PayMammoth_InitialRequestUrl;

                            
                            var mockResponse = new Mock<IHttpWebResponse>();
                            mockResponse.Setup(x=>x.StatusCode).Returns(HttpStatusCode.OK);
                            

                            InitialRequestResponseMessage initialRequestResponseMessage = new InitialRequestResponseMessage();
                            initialRequestResponseMessage.RequestId = "Request/5";
                            initialRequestResponseMessage.Status = EnumsPayMammothConnector.InitialRequestResponseStatus.Success;

                            string initialRequestResponseMessageJson = CS.General_CS_v6.Util.JsonUtil.Serialize(initialRequestResponseMessage);

                            mockResponse.Setup(x=>x.GetResponseAsString()).Returns(initialRequestResponseMessageJson);

                            GetMockFor<IHttpRequestSenderService>().Setup(x=>x.SendHttpRequest(url,
                                 EnumsPayMammothConnector.HttpMethod.Post, 
                                 It.IsAny<WebHeaderCollection>(),
                                 It.IsAny<string>(),
                                 null,60000,null,"UserAgent")).Returns(mockResponse.Object);




                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.CreatePaymentRequestOnPayMammoth(_websiteAccountCode, _secretWord, _initialRequest);
                        }

                        [Test]
                        public void then_result_should_be_as_sent()
                        {
                            _result.RequestId.ShouldEqual("Request/5");
                            _result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.Success);
                        }
                    }
                    [TestFixture]
                    public class given_the_request_fails : given_such_data
                    {
                        private string _websiteAccountCode;
                        private string _secretWord;
                        private InitialRequestInfo _initialRequest;
                        private InitialRequestResponseMessage _result;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!

                            _websiteAccountCode = "Website";
                            _secretWord = "123456";
                            _initialRequest = new InitialRequestInfo();
                            _initialRequest.OrderLinkOnClientWebsite = "http://www.testwebsite.com";

                            string url = Constants.PayMammoth_InitialRequestUrl;


                            var mockResponse = new Mock<IHttpWebResponse>();
                            mockResponse.Setup(x => x.StatusCode).Returns(HttpStatusCode.Forbidden);


                            
                            
                            //mockResponse.Setup(x => x.GetResponseAsString()).Returns(null);

                            GetMockFor<PayMammoth_v6.Connector.Services.HttpRequests.IHttpRequestSenderService>()
                                .Setup(x => x.SendHttpRequest(url,
                                     EnumsPayMammothConnector.HttpMethod.Post,
                                 It.IsAny<WebHeaderCollection>(),
                                 It.IsAny<string>(),
                                 null, It.IsAny<int>(), null, It.IsAny<string>()))
                                 .Returns(mockResponse.Object);




                        }

                        protected override void When()
                        {
                            base.When();

                            _result = SUT.CreatePaymentRequestOnPayMammoth(_websiteAccountCode, _secretWord, _initialRequest);
                        }

                        [Test]
                        public void then_result_should_be_as_sent()
                        {
                            _result.Status.ShouldEqual(EnumsPayMammothConnector.InitialRequestResponseStatus.ErrorWhenSendingMessage);
                        }
                    }

                }
            }
        }


    }
}
