﻿using BusinessLogic_CS_v6.Framework.RavenDb;
using General_Tests_CS_v6.BackendTests.SlowTests.BusinessLogic.Framework.RavenDb;
using NUnit.Framework;
using PayMammoth_v6.Framework.Application;
using StructureMap;

namespace PayMammoth_v6_Tests
{
    [SetUpFixture]
    public class TestsSetup
    {
      

        public class AppInstancePaymammothv2Tests : AppInstancePayMammoth_v6
        {
            

        }

        [SetUp]
        public void Setup()
        {
            RavenDbManager._RavenDbManagerToInstantiate = typeof(TestRavenDbManager);
            CS.General_CS_v6.Util.OtherUtil.SetIsInUnitTestingEnvironment();
            var app = new AppInstancePaymammothv2Tests();

            // app.AddProjectAssembly(typeof(General_Tests_CS_v6.TestsSetup).Assembly);
            app.OnApplicationStart();

        }
        [TearDown]
        public void Teardown()
        {

        }
    }
}
