﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CS.General_CS_v5.Util;
using Microsoft.WindowsAzure.Diagnostics;
using NLog;
using PayMammoth_v2.Connector;
using PayMammoth_v2.Framework.Application;
using PayMammoth_v2.Modules.Services.TestData;
using PayMammoth_v2Deploy.App_Start;
using Raven.Client.Linq;
using LogLevel = NLog.LogLevel;

namespace PayMammoth_v2Deploy
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : MVCApplicationPayMammoth_v2
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        public MvcApplication()
        {
            Trace.WriteLine("MvcApplication - Constructor()");
        }


     

        protected override void onApplicationStart()
        {

         

            _log.LogToLoggerAndTrace(LogLevel.Info, "global.asax.onApplicationStart() [START]");
            
            var app = new AppInstancePayMammoth_v2();
            app.AddProjectAssembly(typeof(MvcApplication).Assembly); 
            app.OnApplicationStart();
            _log.LogToLoggerAndTrace(LogLevel.Info, "global.asax.onApplicationStart() [FINISH]");


            BusinessLogic_CS_v5.Util.InversionUtil.Get<PayMammoth_v2.Connector.Services.INotificationsHandlerService>()
                .OnNotificationMessageRecevied += MvcApplication_OnPayMammothNotificationMessageRecevied;
            
            

        }

        void MvcApplication_OnPayMammothNotificationMessageRecevied(PayMammoth_v2.Connector.Notifications.NotificationMessage msg)
        {
            if (msg.MessageType == Enums.NotificationMessageType.ImmediatePaymentSuccess)
            {
                var testPaymentsDataService = BusinessLogic_CS_v5.Util.InversionUtil.Get<ITestDataPaymentsService>();
                testPaymentsDataService.OnTestPaymentPaidSuccessfully(msg);
            }
            else
            {
                int k = 5;
            }
        }
        
        /// <summary>
        /// This is only use to confirm the test payments
        /// </summary>
        /// <param name="msg"></param>
        void PayMammothNotificationHandler_OnPaidSuccessfully(PayMammoth_v2.Connector.Notifications.NotificationMessage msg)
        {
            
        }

        protected override void onApplicationEnd()
        {
        }

        protected override void onSessionStart()
        {
            Constants.PayMammoth_ServerUrl = CS.General_CS_v5.Util.PageUtil.GetApplicationBaseUrl().ToString();

        }

        protected override void onSessionEnd()
        {
        }
    }
}