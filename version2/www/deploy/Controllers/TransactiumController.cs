﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Presentation.Services.ContentTexts;
using BusinessLogic_CS_v5.Presentation.Services.Context;
using BusinessLogic_CS_v5.Presentation.Services.Cultures;
using BusinessLogic_CS_v5.Presentation.Services.NotificationMessages;
using BusinessLogic_CS_v5.Presentation.Services.Sections;
using CS.General_CS_v5.Modules.Pages;
using CS.MvcGeneral_CS_v5.Code.Attributes;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.PaymentsIntegration.Transactium.v1;
using PayMammoth_v2.PaymentsIntegration.Transactium.v1.HelperClasses;
using PayMammoth_v2.Presentation.Code.Mvc;
using PayMammoth_v2.Presentation.Code.ViewData;
using PayMammoth_v2.Presentation.Models.Home;
using PayMammoth_v2.Presentation.Models.Transactium;
using PayMammoth_v2.Presentation.Services.Payments;

namespace PayMammoth_v2Deploy.Controllers
{
    [InitializeAllViewDataFilters]
    [CommonViewContextDataSettingsFilter(AllowAllEmpty = true)]
    public partial class TransactiumController : CsControllerPayMammoth_v2
    {
        private readonly ICultureModelService _cultureModelService;
        private readonly ICommonViewContextDataService _commonViewContextDataService;
        private readonly ISectionModelService _sectionModelService;
        private readonly IPageService _pageService;
        private readonly IContentTextModelService _contentTextModelService;
        private readonly IRavenDbService _ravenDbService;
        private readonly ITransactiumService _transactiumService;
        private readonly INotificationMessagesModelService _notificationMessagesModelService;
        private readonly IPaymentUrlModelService _paymentUrlModelService;
        //
        // GET: /PaymentTransactium/

        public TransactiumController(ICultureModelService cultureModelService,
            ICommonViewContextDataService commonViewContextDataService,
            ISectionModelService sectionModelService,
            IPageService pageService,
            IContentTextModelService contentTextModelService,
            ITransactiumService transactiumService,
            INotificationMessagesModelService notificationMessagesModelService,
            IPaymentUrlModelService paymentUrlModelService)
        {
            _paymentUrlModelService = paymentUrlModelService;
            _notificationMessagesModelService = notificationMessagesModelService;
            _transactiumService = transactiumService;
            _contentTextModelService = contentTextModelService;
            _pageService = pageService;
            _sectionModelService = sectionModelService;
            _commonViewContextDataService = commonViewContextDataService;
            _cultureModelService = cultureModelService;
        }

        [CommonViewContextDataSettingsFilter(
            AllowEmptyCanonicalUrl = true,
            AllowEmptyOpenGraphTitle = true,
            AllowEmptySection = true,
            DoNotPerformPermanentRedirectionIfPageUrlIsNotSameAsCanonicalUrl = true)]
        public virtual ActionResult Payment()
        {

            var culture = _cultureModelService.GetCultureFromCurrentRouteOrDefaultCultureAndStoreInContext();
            var commonViewContextData = _commonViewContextDataService.GetCommonViewContextData();
            string redirectUrl =
                _pageService.GetVariableFromRoutingQuerystringOrForm<string>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Transactium_RedirectUrl);

            var section = _sectionModelService.GetSectionDataModelByIdentifierOrCreateNew(
                CurrentSession,
                culture,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Payment_Transactium);
            commonViewContextData.Page.CurrentSection = section;

            TransactiumPaymentModel model = new TransactiumPaymentModel()
            {
                RedirectUrl = redirectUrl,
                TitleContentTextModel = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    CurrentSession,
                    culture,
                    PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.Transactium_PaymentDetailsTitle)
            };
            return View(MVCPayMammoth_v2.Transactium.Views.Payment, model);

        }

        [CommonViewContextDataSettingsFilter(
            AllowEmptyCanonicalUrl = true,
            AllowEmptyOpenGraphTitle = true,
            AllowEmptySection = true,
            DoNotPerformPermanentRedirectionIfPageUrlIsNotSameAsCanonicalUrl = true)]
        public virtual ActionResult StatusHandler()
        {
            string hpsId = _pageService.GetVariableFromRoutingQuerystringOrForm<string>(ConstantsTransactium.HPSID);
            TransactiumStatusHandlerModel model = new TransactiumStatusHandlerModel();
            var layoutData =
                _pageService.GetContextObject<LayoutViewData>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey);
            var result =
                _transactiumService.CheckPayment(
                    new ReferenceLink<PaymentRequestTransactionData>(layoutData.PaymentRequestDataModel.PaymentRequestId),
                    hpsId,
                    CurrentSession);
            if (result.Result == CheckPaymentResponse.CheckPaymentStatusType.Success)
            {
                model.RedirectUrl = result.SuccessRedirectUrl;
            }
            else
            {
                var culture = _cultureModelService.GetCultureFromCurrentRouteOrDefaultCultureAndStoreInContext();
                string paymentUrl = _paymentUrlModelService.GetPaymentSelectionPage(CurrentSession,
                    culture,
                    layoutData.PaymentRequestDataModel.PaymentRequestId);

                string paymentSelectionUrlWithMessage = _notificationMessagesModelService
                    .AppendNotificationMessageToUrlFromIdentifier(
                        CurrentSession,
                        culture,
                        paymentUrl,
                        result.Result);
                model.RedirectUrl = paymentSelectionUrlWithMessage;
            }
            return View(MVCPayMammoth_v2.Transactium.Views.StatusHandler, model);


        }
    }
}
