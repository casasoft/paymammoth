﻿using System.Net.Configuration;
using BusinessLogic_CS_v5.Extensions;

using BusinessLogic_CS_v5.Framework.Caching;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Modules.Data.Sections;
using BusinessLogic_CS_v5.Presentation.Code.ContextData.Old;
using BusinessLogic_CS_v5.Presentation.Data.Fields;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.ContentTexts;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Sections;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.ContentPage;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.Placeholder;
using BusinessLogic_CS_v5.Presentation.Services.ContentTexts;
using BusinessLogic_CS_v5.Presentation.Services.Context;
using BusinessLogic_CS_v5.Presentation.Services.NotificationMessages;
using BusinessLogic_CS_v5.Presentation.Services.Sections;
using CS.General_CS_v5;
using CS.General_CS_v5.Modules.Pages;
using CS.General_CS_v5.Modules.Urls;
using CS.MvcGeneral_CS_v5.Code.Attributes;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Services.FakePayments;
using PayMammoth_v2.Presentation.Code.Attributes;
using PayMammoth_v2.Presentation.Code.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PayMammoth_v2.Presentation.Code.ViewData;
using PayMammoth_v2.Presentation.Models.Home;
using BusinessLogic_CS_v5.Modules.Services.Sections;
using BusinessLogic_CS_v5.Presentation.Services.ContentPage;
using BusinessLogic_CS_v5.Framework.Culture;
using BusinessLogic_CS_v5.Presentation.Services.MailingList;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Presentation.Services.Bundling;
using BusinessLogic_CS_v5.Presentation.Data.Bundling;
using System.Web.Optimization;
using PayMammoth_v2.Presentation.Services.Payments;
using PayMammoth_v2.Presentation.Services.Payments.PayPal;
using PayMammoth_v2Deploy.App_Start;
using BusinessLogic_CS_v5.Presentation.Services.Fields;
using BusinessLogic_CS_v5.Modules.Services.ContentTexts;
using BusinessLogic_CS_v5.Presentation.Services.Cultures;
using PostSharp.Reflection;
using Raven.Client.Linq;
using Enums = PayMammoth_v2.Connector.Enums;

namespace PayMammoth_v2Deploy.Controllers
{
    [CommonViewContextDataSettingsFilter(AllowAllEmpty = true)]
    public partial class HomeController : CsControllerPayMammoth_v2
    {
        private readonly ICultureModelService _cultureModelService;
        private readonly ICommonViewContextDataService _commonViewContextDataService;
        private readonly ISectionModelService _sectionModelService;
        private readonly IHomePaymentSelectionModelService _homePaymentSelectionModelService;
        private readonly IPageService _pageService;
        private readonly IPaymentRequestDataModelService _paymentRequestDataModelService;
        private readonly INotificationMessagesModelService _notificationMessagesModelService;
        private readonly IContentTextModelService _contentTextModelService;
        private readonly IPaypalDataModelService _paypalDataModelService;
        private readonly IMakeFakePaymentService _makeFakePaymentService;

        public HomeController(
            ICultureModelService cultureModelService,
            ICommonViewContextDataService commonViewContextDataService,
            ISectionModelService sectionModelService,
            IHomePaymentSelectionModelService homePaymentSelectionModelService,
            IPageService pageService,
            IPaymentRequestDataModelService paymentRequestDataModelService,
            INotificationMessagesModelService notificationMessagesModelService,
            IContentTextModelService contentTextModelService,
            IPaypalDataModelService paypalDataModelService,
            IMakeFakePaymentService makeFakePaymentService)
        {
            _makeFakePaymentService = makeFakePaymentService;
            _paypalDataModelService = paypalDataModelService;
            _contentTextModelService = contentTextModelService;
            _notificationMessagesModelService = notificationMessagesModelService;
            _paymentRequestDataModelService = paymentRequestDataModelService;
            _pageService = pageService;
            _homePaymentSelectionModelService = homePaymentSelectionModelService;
            _sectionModelService = sectionModelService;
            _commonViewContextDataService = commonViewContextDataService;
            _cultureModelService = cultureModelService;
        }
        [CommonViewContextDataSettingsFilter(AllowAllEmpty = true)]
        [CommonViewContextDataRetrieverFilterAttribute]
        public virtual ActionResult Index()
        {
            HomeIndexModel homeIndexModel = new HomeIndexModel();
            return View(MVCPayMammoth_v2.Home.Views.Index, homeIndexModel);
        }

        [CommonViewContextDataRetrieverFilterAttribute]
        public virtual ActionResult Expired()
        {
            var culture = _cultureModelService.GetCultureFromCurrentRouteOrDefaultCultureAndStoreInContext();
            var section = _sectionModelService.GetSectionDataModelByIdentifierOrCreateNew(CurrentSession,
                culture,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Expired);
            var cancelUrl =
                _pageService.GetVariableFromRoutingQuerystringOrForm<string>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_CancelUrl);

            HomeExpiredModel model = new HomeExpiredModel()
            {
                Title = section.TitleFieldModel,
                ExpiredContentText = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    CurrentSession,
                    culture,
                    PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.Payment_Expired,
                    cancelUrl)
            };
            return View(MVCPayMammoth_v2.Home.Views.Expired, model);
        }

        [LayoutViewDataInitializerFilter]
        [CommonViewContextDataRetrieverFilterAttribute]
        public virtual ActionResult CancelPayment()
        {
            var layoutData =
                _pageService.GetContextObject<LayoutViewData>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey);

            var culture = _cultureModelService.GetCultureFromCurrentRouteOrDefaultCultureAndStoreInContext();
            var result = _paymentRequestDataModelService.CancelPaymentForPaymentRequestId(CurrentSession,
                culture,
                layoutData.PaymentRequestDataModel.PaymentRequestId);
            if (result.Status == CancelPaymentStatus.Success)
            {
                return Redirect(result.Result);
            }
            else
            {
                var errorMessage = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    CurrentSession,
                    culture,
                    result.Status);
                return Content(errorMessage.ReplacedValue);
            }
        }

        [CommonViewContextDataSettingsFilter(DoNotPerformPermanentRedirectionIfPageUrlIsNotSameAsCanonicalUrl = true)]
        [LayoutViewDataInitializerFilter]
        [CommonViewContextDataRetrieverFilterAttribute]

        public virtual ActionResult PaymentSelection()
        {

            var layoutData =
                _pageService.GetContextObject<LayoutViewData>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey);

            var culture = _cultureModelService.GetCultureFromCurrentRouteOrDefaultCultureAndStoreInContext();


            var model = _homePaymentSelectionModelService
                .CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestId(
                    CurrentSession,
                    culture,
                    layoutData.PaymentRequestDataModel);

            var commonViewContextData = _commonViewContextDataService.GetCommonViewContextData();
            var section = _sectionModelService.GetSectionDataModelByIdentifierOrCreateNew(
                CurrentSession,
                culture,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Selection);

            commonViewContextData.Page.CurrentSection = section;
            //_groupDataPreloaderService.RegisterItemsByIdentifiersForCurrentLanguage(_mailingListModelService.GetDefaultModelRequiredIdentifiers());
            var v = View(MVCPayMammoth_v2.Home.Views.PaymentSelection, model);
            return v;

        }

        [LayoutViewDataInitializerFilter]
        [CommonViewContextDataRetrieverFilterAttribute]
        public virtual ActionResult PaymentSelectionHandler()
        {

            var layoutData =
                _pageService.GetContextObject<LayoutViewData>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey);

            var culture = _cultureModelService.GetCultureFromCurrentRouteOrDefaultCultureAndStoreInContext();

            var paymentMethodType =
                _pageService.GetVariableFromRoutingQuerystringOrForm<Enums.PaymentMethodType?>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_PaymentMethod);
            if (paymentMethodType.HasValue)
            {
                bool success = false;
                string successRedirectUrl = null;
                Enum errorMsgIdentifier = null;
                if (!string.IsNullOrWhiteSpace(layoutData.PaymentRequestDataModel.FakePaymentKey))
                {
                    //Fake Payment
                    var fakePaymentResult =
                        _makeFakePaymentService.MakeFakePayment(
                            new ReferenceLink<PaymentRequestData>(layoutData.PaymentRequestDataModel.PaymentRequestId),
                            paymentMethodType.Value,
                            layoutData.PaymentRequestDataModel.FakePaymentKey);
                    success = fakePaymentResult.Status == MakeFakePaymentService.MakeFakePaymentResult.Success;
                    if (success)
                    {
                        successRedirectUrl = fakePaymentResult.Result.RedirectUrl;
                    }
                    else
                    {
                        errorMsgIdentifier = fakePaymentResult.Status;
                    }
                }
                else
                {
                    //Real Payment

                    var paymentRequest =
                        _paymentRequestDataModelService.GenerateTransactionResultFromPaymentMethod(CurrentSession,
                            culture,
                            paymentMethodType.Value,
                            layoutData.PaymentRequestDataModel.PaymentRequestId);

                    success = paymentRequest.Status == PayMammoth_v2Enums.RedirectionResultStatus.Success;
                    if (success)
                    {
                        successRedirectUrl = paymentRequest.RedirectUrl;
                    }
                    else
                    {
                        errorMsgIdentifier = paymentRequest.Status;
                    }
                }

                if (success)
                {
                    var model = new HomePaymentSelectionHandlerModel()
                    {
                        RedirectUrl = successRedirectUrl
                    };
                    var vr =  View(MVCPayMammoth_v2.Home.Views.PaymentSelectionHandler, model);
                    return vr;
                }
                else
                {
                    //Show error and take them back to payment selection page
                    string url = _notificationMessagesModelService.GetUrlWithNotificationMessageBySectionIdentifier(
                        CurrentSession,
                        culture,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Selection,
                        errorMsgIdentifier,
                        NotificationMessageType.Error);
                    UrlHandler urlHandler = new UrlHandler(url);
                    urlHandler.QueryString[PayMammoth_v2.Connector.Constants.PARAM_IDENTIFIER] =
                        layoutData.PaymentRequestDataModel.PaymentRequestId;
                    string urlRedirect = urlHandler.GetUrl();
                    return Redirect(urlRedirect);
                }

            }
            else
            {
                throw new Exception(string.Format("Invalid payment method type value of parameter '{0}'",
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_PaymentMethod));
            }

        }

    }
}
