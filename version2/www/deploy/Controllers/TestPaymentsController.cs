﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS.MvcGeneral_CS_v5.Code.Attributes;
using PayMammoth_v2.KarlTestUI.Models;

namespace PayMammoth_v2Deploy.Controllers
{
    [InitializeAllViewDataFilters]
    public partial class TestPaymentsController : Controller
    {
        public TestPaymentsController()
        {

        }

        //
        // GET: /TestPayments/

        public virtual ActionResult Index()
        {
            IndexModel model = new IndexModel();
            model.Layout.PageTitle = "Choose a payment method:";
            var paymentId = CS.General_CS_v5.Util.PageUtil.GetVariableFromRoutingQuerystringOrForm<string>("paymentId");
            if (!string.IsNullOrWhiteSpace(paymentId))
            {

            }
            else
            {
                model.Layout.ErrorMessage = string.Format("No payment exists with id [{0}]", paymentId);
            }
            return View();
        }

    }
}
