﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic_CS_v5.Framework.Caching;
using BusinessLogic_CS_v5.Modules.Data.Sections;
using CS.MvcCmsGeneral_CS_v5.Code.Mvc;
using PayMammoth_v2.Presentation.Code.Mvc;
using PayMammoth_v2.Presentation.Models.CacheTesting;
using DevTrends.MvcDonutCaching;

namespace PayMammoth_v2Deploy.Controllers
{
    public partial class CacheTestingController : CsControllerPayMammoth_v2
    {
        //
        // GET: /CacheTesting/
        [CsOutputCache(Duration = 60)]
        public virtual ActionResult Index()
        {
            return View();
        }

        [CsOutputCache(Duration = 15)]
        public virtual ActionResult _TestLoginController()
        {
            _TestLoginControlModel m = new _TestLoginControlModel();

            m.IsLoggedIn = false;
            return PartialView(MVCPayMammoth_v2.CacheTesting.Views._TestLoginControl, m);
        }
        public virtual ActionResult _TestLoginController2()
        {
            _TestLoginControlModel m = new _TestLoginControlModel();

            m.IsLoggedIn = false;
            return PartialView(MVCPayMammoth_v2.CacheTesting.Views._TestLoginControl, m);
        }
    }
}
