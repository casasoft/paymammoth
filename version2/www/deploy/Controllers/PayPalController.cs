﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v5.Presentation.Services.ContentTexts;
using BusinessLogic_CS_v5.Presentation.Services.Context;
using BusinessLogic_CS_v5.Presentation.Services.Cultures;
using BusinessLogic_CS_v5.Presentation.Services.Sections;
using CS.General_CS_v5.Modules.Pages;
using CS.General_CS_v5.Modules.Urls;
using CS.MvcGeneral_CS_v5.Code.Attributes;
using PayMammoth_v2.Presentation.Code.Mvc;
using PayMammoth_v2.Presentation.Code.ViewData;
using PayMammoth_v2.Presentation.Models.Home;
using PayMammoth_v2.Presentation.Models.PayPal;
using PayMammoth_v2.Presentation.Services.Payments.PayPal;

namespace PayMammoth_v2Deploy.Controllers
{
    [InitializeAllViewDataFilters]
    [CommonViewContextDataSettingsFilter(AllowAllEmpty = true)]
    public partial class PayPalController : CsControllerPayMammoth_v2
    {
        private readonly ICultureModelService _cultureModelService;
        private readonly ICommonViewContextDataService _commonViewContextDataService;
        private readonly ISectionModelService _sectionModelService;
        private readonly IPageService _pageService;
        private readonly IContentTextModelService _contentTextModelService;
        private readonly IPaypalDataModelService _paypalDataModelService;
        //
        // GET: /PaymentTransactium/

        public PayPalController(ICultureModelService cultureModelService,
            ICommonViewContextDataService commonViewContextDataService,
            ISectionModelService sectionModelService,
            IPageService pageService,
            IContentTextModelService contentTextModelService,
            IPaypalDataModelService paypalDataModelService)
        {
            _paypalDataModelService = paypalDataModelService;
            _contentTextModelService = contentTextModelService;
            _pageService = pageService;
            _sectionModelService = sectionModelService;
            _commonViewContextDataService = commonViewContextDataService;
            _cultureModelService = cultureModelService;
        }

        [CommonViewContextDataSettingsFilter(
            AllowEmptyCanonicalUrl = true,
            AllowEmptyOpenGraphTitle = true,
            AllowEmptySection = true,
            DoNotPerformPermanentRedirectionIfPageUrlIsNotSameAsCanonicalUrl = true)]
        public virtual ActionResult PaymentConfirmation()
        {

            var culture = _cultureModelService.GetCultureFromCurrentRouteOrDefaultCultureAndStoreInContext();
            var commonViewContextData = _commonViewContextDataService.GetCommonViewContextData();
            bool isConfirmingRequest =
                _pageService.GetVariableFromRoutingQuerystringOrForm<bool>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Confirm);
            if (isConfirmingRequest)
            {
                var layoutData =
                    _pageService.GetContextObject<LayoutViewData>(
                        PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey);

                string identifier = layoutData.PaymentRequestDataModel.PaymentRequestId;
                string token = _pageService.GetVariableFromRoutingQuerystringOrForm<string>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Token);
                string payerId = _pageService.GetVariableFromRoutingQuerystringOrForm<string>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_PayerId);


                var confirmResult = _paypalDataModelService.ConfirmPaypalRequestFromPresentation(CurrentSession,
                    culture, identifier, token, payerId);
                if (confirmResult.Status == ConfirmPaypalRequestStatus.Success)
                {
                    return new RedirectResult(confirmResult.Result.RedirectUrl);
                }
                else
                {
                    //Show error
                    commonViewContextData.NotificationMessage = new _NotificationMessageModel()
                    {
                        MessageContentTextModel =
                            _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                                CurrentSession,
                                culture,
                                confirmResult.Status),
                        MessageType = NotificationMessageType.Error
                    };
                }
            }


            string url = _pageService.GetCurrentPageUrl();
            UrlHandler urlHandler = new UrlHandler(url);
            urlHandler.QueryString[PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Confirm] =
                CS.General_CS_v5.Util.ConversionUtil.ConvertBasicDataTypeToString(true);
            string confirmUrl = urlHandler.GetUrl();
            var section = _sectionModelService.GetSectionDataModelByIdentifierOrCreateNew(
                CurrentSession,
                culture,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Payment_PaypalConfirmation);
            commonViewContextData.Page.CurrentSection = section;

            PaypalConfirmationModel model = new PaypalConfirmationModel()
            {
                ConfirmButtonTextModel =
                    _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                        CurrentSession,
                        culture,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PayPalConfirmation_ConfirmButton),
                ConfirmTextModel = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    CurrentSession,
                    culture,
                    PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PayPalConfirmation_DescriptionHtml),
                TitleContentTextModel =
                    _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                        CurrentSession,
                        culture,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PayPalConfirmation_Title),
                ConfirmUrl = confirmUrl
            };
            return View(MVCPayMammoth_v2.PayPal.Views.Confirmation, model);

        }
    }
}
