﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.PaymentsIntegration.Transactium.v1;
using PayMammoth_v2.PaymentsIntegration.Transactium.v1.HelperClasses;

namespace PayMammoth_v2Deploy._tests.transactium
{
    public partial class status : System.Web.UI.Page
    {

        public string TransactionId
        {
            get
            {
                return this.Request.QueryString[PayMammoth_v2.Connector.Constants.PARAM_IDENTIFIER];
            }
        }
        public string HpsId
        {
            get
            {
                return this.Request.QueryString[ConstantsTransactium.HPSID];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var ravenDbService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IRavenDbService>();
            var transactiumService= BusinessLogic_CS_v5.Util.InversionUtil.Get<ITransactiumService>();
            var session = ravenDbService.CreateNewSession();
            var result = transactiumService.CheckPayment(new ReferenceLink<PaymentRequestTransactionData>(TransactionId), HpsId, session);

            session.SaveChanges();
            session.Dispose();
            spanStatus.InnerText = result.Result.ToString();

            
        }
    }
}