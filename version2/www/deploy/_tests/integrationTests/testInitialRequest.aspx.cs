﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using BusinessLogic_CS_v5.Util;
using CS.General_CS_v5;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Connector.Services;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.TestData;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v2Deploy._tests.integrationTests
{
    public partial class testInitialRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Start();
        }

        
        public class TestData
        {
            public TestPaymentData TestPayment { get; set; }
            public PaymentRequestData PaymentRequest { get; set; }


        }

        private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestDataService _paymentRequestDataService;

        public testInitialRequest()
        {
            _ravenDbService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = BusinessLogic_CS_v5.Util.InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestDataService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPaymentRequestDataService>();
           
        }

    

        private WebsiteAccountData createWebsiteAccount(IDocumentSession session)
        {
            WebsiteAccountData testWebsite = _dataObjectFactory.CreateNewDataObject<WebsiteAccountData>();
            testWebsite.WebsiteName = "TestWebsiteAccount-" + DateTime.Now.Ticks;
            //testWebsite.AccountCode = "TestWebsite";
            testWebsite.SecretWord = "123456";

            session.Store(testWebsite);
            return testWebsite;

        }


         private InitialRequestResponseMessage sendInitialRequest(WebsiteAccountData websiteAccount)
         {
             InitialRequestInfo initialRequest = new InitialRequestInfo();
             initialRequest.Details.ClientContactDetails.Address1 = "Triq il-Kittenija";
             initialRequest.Details.ClientContactDetails.Country3LetterCode = CS.General_CS_v5.Enums.CountryIso3166To3LetterCode(Enums.CountryIso3166.Malta);
             initialRequest.Details.ClientContactDetails.Email = "karl@casasoft.com.mt";
             initialRequest.Details.ClientContactDetails.Name = "Karl";
             initialRequest.Details.ClientContactDetails.LastName = "Cassar";
             initialRequest.Details.ClientReference = "Karl-Test1";
             initialRequest.Details.Description = "Test item 1";
             initialRequest.Details.OrderReference = "test-Order-1";
             initialRequest.Details.Title = "Test Payment";
             initialRequest.ItemDetails.Add(new PaymentRequestItemLine()
             {
                 Description = "Line 1",
                 Quantity = 1,
                 TaxAmountPerUnit = 18,
                 Title = "Item 1",
                 UnitPrice = 100
             });
             initialRequest.Language = PayMammoth_v2.Connector.Enums.SupportedLanguage.English; ;
             initialRequest.NotificationUrl = "Http://www.testwebsite.com/notifications";
             initialRequest.OrderLinkOnClientWebsite = "http://www.clientwebsite.com/order-link.aspx";
             initialRequest.Pricing.CurrencyCode3Letter = CS.General_CS_v5.Enums.CurrencyCode_Iso4217ToCode(Enums.CurrencyCode_Iso4217.EuroMemberCountriesEuro);
             initialRequest.Pricing.HandlingAmount = 20;
             initialRequest.Pricing.ShippingAmount = 50;
             initialRequest.ReturnUrls.FailureUrl = "http://www.clientwebsite.com/failure.aspx";
             initialRequest.ReturnUrls.SuccessUrl= "http://www.clientwebsite.com/success.aspx";

             //PayMammoth_v2.Connector.Constants.PayMammoth_InitialRequestUrl = Constants.PaymentClientBaseUrl + Constants.InitialRequestHandler;
             //PayMammoth_v2.Connector.Constants.PayMammoth_InitialRequestUrl = "http://localhost:8087/_tests/initialRequest.ashx";

             var payMammothConnectorInitialRequestService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPayMammothConnectorInitialRequestService>();

             var result = payMammothConnectorInitialRequestService.CreatePaymentRequestOnPayMammoth(websiteAccount.GetRavenDbIdOnlyFromItem(),
                 websiteAccount.SecretWord,
                 initialRequest);

             return result;


         }

       
        public void Start()
        {
            var session = _ravenDbService.CreateNewSession();

            var websiteAccount = createWebsiteAccount(session);
            session.SaveChanges();

            session.Dispose();
            session = _ravenDbService.CreateNewSession();


            var result = sendInitialRequest(websiteAccount);


           
        }

    }
}