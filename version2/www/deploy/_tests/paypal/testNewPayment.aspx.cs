﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using CS.General_CS_v5;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v2Deploy._tests.paypal
{
    public partial class testNewPayment : System.Web.UI.Page
    {
        private IRavenDbService _ravenDbService;
        private IDataObjectFactory _dataObjectFactory;
        private IPaymentRequestDataService _paymentRequestDataService;

        private WebsiteAccountData createWebsiteAccount(IDocumentSession session)
        {
            WebsiteAccountData testWebsite = _dataObjectFactory.CreateNewDataObject<WebsiteAccountData>();
            testWebsite.WebsiteName = "TestWebsiteAccount-" + DateTime.Now.Ticks;
            testWebsite.PaymentsInformation.PayPal.Enabled = true;
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.MerchantEmail = "paypal.cs.test.business.en@dispostable.com";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.MerchantId = "SAN5FMKHMTM9C";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.Password = "1389117990";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.Signature = "AxRMHSZhKz2kBtwBMbnqHM0tDC7XA8hhARUerqhHUKWfOaz0b1MN80Zd";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.Username = "paypal.cs.test.business.en_api1.dispostable.com";
            testWebsite.PaymentsInformation.PayPal.UseLiveEnvironment = false;
            session.Store(testWebsite);
            return testWebsite;

        }
        private PaymentRequestData createTestPaymentRequest( WebsiteAccountData websiteAccount, IDocumentSession session)
        {
            InitialRequestInfo initialRequest = new InitialRequestInfo();
            initialRequest.Details.ClientContactDetails.Address1 = "20, Cor Jesu";
            initialRequest.Details.ClientContactDetails.Address2 = "Triq il-Kittenija";
            initialRequest.Details.ClientReference = "Test-Client";
            initialRequest.Details.ClientContactDetails.Country3LetterCode = CS.General_CS_v5.Enums.CountryIso3166To3LetterCode(Enums.CountryIso3166.Malta);
            initialRequest.Details.ClientContactDetails.Email = "karlcassar@gmail.com";

            var newPaymentRequest = _paymentRequestDataService.GeneratePaymentRequestFromInitialRequest(websiteAccount, initialRequest, null);
            return newPaymentRequest.GeneratedRequest;

        }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            _dataObjectFactory = BusinessLogic_CS_v5.Util.InversionUtil.Get<IDataObjectFactory>();
            _ravenDbService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IRavenDbService>();
            _paymentRequestDataService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPaymentRequestDataService>();
            var session = _ravenDbService.CreateNewSession();

            var websiteAccount = createWebsiteAccount(session);

            session.SaveChanges();
            session.Dispose();
            session = _ravenDbService.CreateNewSession();

        }
    }
}