﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using NLog;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1;
using Raven.Client.Linq;

namespace PayMammoth_v2Deploy._tests.paypal
{
    public partial class paypalTestConfirm : System.Web.UI.Page
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {

            




            _log.Info("paypalTestConfirm [Start]");

            btnConfirm.Click += btnConfirm_Click;
            int k = 5;
        }

        void btnConfirm_Click(object sender, EventArgs e)
        {
            string identifier = Request.QueryString["identifier"];
            string tokenId = Request.QueryString["token"];
            string payer = Request.QueryString["PayerID"];

            var paypalManager = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPayPalManager>();
            var result = paypalManager.ConfirmPayment(new ReferenceLink<PaymentRequestData>(identifier),
                tokenId, payer);
            lblStatus.Text = string.Format("Confirm complete! Result: {0}", result);
        }
    }
}