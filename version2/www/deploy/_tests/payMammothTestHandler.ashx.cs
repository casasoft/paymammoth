﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayMammoth_v2.Connector.Services;

namespace PayMammoth_v2Deploy._tests
{
    /// <summary>
    /// This is a test handler which would be implemented on the client website, e.g JK Villas.
    /// </summary>
    public class payMammothTestHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {

            var notificationsHandlerService = BusinessLogic_CS_v5.Util.InversionUtil.Get<INotificationsHandlerService>();

            string response = PayMammoth_v2.Connector.Constants.RESPONSE_ERROR;

            string data = CS.General_CS_v5.Util.StreamUtil.ReadStreamAsStringToEnd(context.Request.InputStream);
            response = notificationsHandlerService.HandleNotificationResponseData(data);
            context.Response.Clear();
            context.Response.Write(response);
            context.Response.End();
            

        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}