﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.Xml;
using System.IO;

namespace FastPayInitCSharp
{
     public partial class _Default : System.Web.UI.Page
     {
          protected void Page_Load(object sender, EventArgs e)
          {
               try
               {
                   String xmlText = "<Transaction hash=\"secretword\"><ProfileID>PROFILEID</ProfileID><Value>21.46</Value><Curr>840</Curr><Lang>en</Lang><ORef>B87654</ORef><RedirectionURL>http://www.google.com</RedirectionURL><UDF1>test1</UDF1><UDF2>test2</UDF2><UDF3>test3</UDF3><ActionType>1</ActionType></Transaction>";
                    XmlTextReader xmlTextRead = new XmlTextReader(new StringReader(xmlText));
                    XmlDocument xmlDoc = new XmlDocument();

                    xmlDoc.Load  (xmlTextRead);
                    String md5HashedXml = GetMD5Hash(xmlDoc.InnerXml);

                    //Dim SecretWord As String = XMLDoc.ChildNodes(0).Attributes("hash").Value
                    xmlDoc.ChildNodes[0].Attributes["hash"].Value = md5HashedXml;
                    Post("https://www.apsp.biz/pay/fp4/checkout.aspx", HttpUtility.HtmlEncode(xmlDoc.InnerXml));
               }
               catch (Exception ex)
               {
                    Response.Write(ex.Message);
               }

          }

          private string GetMD5Hash(string input)
          {
               System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
               byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
               bs = x.ComputeHash(bs);
               System.Text.StringBuilder s = new System.Text.StringBuilder();
               foreach (byte b in bs)
               {
                    s.Append(b.ToString("x2").ToLower());
               }
               string password = s.ToString();
               return password;
          }

          private void Post(String URL, String XMLData)
          {
               System.Collections.Specialized.NameValueCollection Inputs = new System.Collections.Specialized.NameValueCollection();

               Inputs.Add("params", XMLData);

               System.Web.HttpContext.Current.Response.Clear();

               System.Web.HttpContext.Current.Response.Write("<html><head>");
               System.Web.HttpContext.Current.Response.Write(String.Format("</head><body onload=\"document.form1.submit()\">"));
               System.Web.HttpContext.Current.Response.Write(String.Format("<form name=\"form1\" method=\"post\" action=\"{0}\" >", URL));
               System.Web.HttpContext.Current.Response.Write(String.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", Inputs.Keys[0], Inputs[Inputs.Keys[0]]));
               System.Web.HttpContext.Current.Response.Write("</form>");
               System.Web.HttpContext.Current.Response.Write("</body></html>");
               System.Web.HttpContext.Current.Response.End();
          }
     }
}
