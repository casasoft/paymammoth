using System;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Dovetail.Realex.Realauth;
using Dovetail.Realex.Realauth.Mpi;

namespace Dovetail.Realex.DemoSite.Mpi
{
    public partial class MpiRequest : RealexMpiRequestPageBase
    {
        #region member variables for config settings

        // get the Realex settings from the web config file
        readonly string _merchantID = WebConfigurationManager.AppSettings["RealexMerchantID"];
        readonly string _sharedSecret = WebConfigurationManager.AppSettings["RealexSharedSecret"];
        readonly string _accountName = WebConfigurationManager.AppSettings["RealexMpiAccountName"];
        readonly string _mpiResponseUrl= WebConfigurationManager.AppSettings["RealexMpiResponseUrl"];

        #endregion

        #region Implement abstract properties from base class

        /// <summary>
        /// Provides access to the ASPX page's head
        /// </summary>
        public override HtmlGenericControl Head
        {
            get{return head;}
        }

        /// <summary>
        /// Provides access to the ASPX page's body
        /// </summary>
        public override HtmlGenericControl Body
        {
            get{return body;}
        }

        /// <summary>
        /// The URL to which the bank's ACS should respond
        /// </summary>
        public override string ResponseUrl
        {
            get { return _mpiResponseUrl; }
        }

        #endregion

        // This is the key method in this demo.
        protected void btnSend_Click(object sender, EventArgs e)
        {
            // Setup the RequestProcessor
            base.Processor.Request.MerchantId = _merchantID;
            base.Processor.Request.SharedSecret = _sharedSecret;
            base.Processor.Request.AccountName = _accountName;

            /* The Order ID is a unique identifier for this transaction.  
             * This would normally be the Primary Key value from your Order table.
             * Here we just use a timestamp to ensure it is unique. */
            base.Processor.Request.OrderId = DateTime.Now.ToString("yyyyMMddhhmmssffff");

            // whether to automatically settle this transaction in the next overnight batch
            base.Processor.Request.Autosettle = true;

            // the credit card information supplied by the user
            base.Processor.Request.CardHolderName = txtCardHolderName.Text;
            base.Processor.Request.CardNumber = txtCardNumber.Text;
            base.Processor.Request.CardType = (CardType)Convert.ToInt32(cboCardType.SelectedValue);
            base.Processor.Request.Amount = Convert.ToInt32(txtAmount.Text);
            base.Processor.Request.Currency = Currency.EUR;         // for this sample we only process Euro
            base.Processor.Request.CardExpDate = txtExpiryDate.Text;

            // the largest transaction that the merchant is willing to accept then there has been no liability shift (for whatever reason)
            base.Processor.Request.MaximumLiabilityAccepted = Convert.ToInt32(txtMaxLiability.Text);

            /* calling "TryAuthorise" starts the MPI process.  The library decides 
             * what actions to take based on the results received from Realex
             * and the merchant's maximum liability accepted
             * */
            switch (TryAuthorise())
            {
                case MpiResult.Authorised:
                    // the users credit card has been authorised
                    ShowResponse("Authorisation accepted");
                    break;
                case MpiResult.ProceedToAcs:
                    // the system has received data from Realex indicating that
                    // an MPI transaction is possible and provided enough information
                    // to transfer the next step of the transaction to the card's issuing
                    // bank Access Control Server (ACS).  
                    // Do nothing here: the base class will handle everything for you, and your Response page will be called later where
                    // you can handle the final result.
                    break;
                case MpiResult.RiskTooHigh:
                    // based on the response from the ACS no liability shift was possible.  The library has determined that
                    // the value of the transaction is greater than MaximumLiabilityAccepted so it has not been processed
                    ShowResponse("Risk not acceptable");
                    break;
                case MpiResult.Declined:
                    // the system attempted to authorise the credit card but it was declined
                    ShowResponse("Authorisation declined");
                    break;
                case MpiResult.Error:
                    // an error has occured
                    ShowResponse("An error occured");
                    break;
            }
        }

        #region UI code

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                // populate the dropdowns using their associated enums.

                cboCardType.Items.Clear();
                foreach(CardType cardType in Enum.GetValues(typeof(CardType)))
                    cboCardType.Items.Add(new ListItem(cardType.ToString(),((int)cardType).ToString()));
            }
        }



        // show messages to the user
        private void ShowResponse(string outcomeMessage)
        {
            lblTimestamp.Text = DateTime.Now.ToString("d/MM/yyyy HH:mm:ss");
            lblResult.Text = Processor.Response.ResultCode;
            lblMessage.Text = Processor.Response.ResponseMessage;
            lblOrderID.Text = Processor.Request.OrderId;
            lblOutcome.Text = outcomeMessage;
        }

        //DELETE_FROM_HERE
        // a quick and dirty way of populating the textboxes with the appropiate credit card numbers etc when the user clicks a "Quick Populator"
        protected void btnScenarioX_Click(object sender, EventArgs e)
        {
            LinkButton button = (LinkButton) sender;
            int scenarioNumber = Convert.ToInt32(button.ID.Replace("btnScenario", ""));

            string[] cardNumbers = new[] { "4012001038443335", "4012001038488884", "4012001036298889", "4012001036853337", "4012001037141112", "4005559876540", "4012001037167778", "4012001037461114", "4012001037484447", "4012001037490006" };

            txtCardHolderName.Text = "AN Other Scenario " + scenarioNumber;
            txtCardNumber.Text = cardNumbers[scenarioNumber - 1];
            txtAmount.Text = "100";
            txtExpiryDate.Text = "1212"; // december 2012
        }
        //DELETE_TO_HERE

        #endregion

    }
}