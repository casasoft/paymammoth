﻿using System;
using System.Text;
using System.Web.Configuration;
using Dovetail.Realex.Realauth;

namespace Dovetail.Realex.DemoSite.Redirect
{
    public partial class Redirect : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                DoStep1();
            }
            else
            {
                lblAmount.Text = "Please enter amount";
            }
        }

        private void DoStep1()
        {
            // this should be validated as numeric before here.  We multiply by 100 to convert to cent as this is what Realex expects.
            int amountInCent = decimal.ToInt16((decimal.Parse(txtAmount.Text) * 100m)); 

            // we are showing the amount for verification only.  The user cannot change it at this stage so we won't display the textbox.
            // This is similar to how a user would checkout from a shopping cart as they can not normally choose their own amount.
            txtAmount.Visible = false;
            lblAmount.Text = "You are paying " + amountInCent + " cent.";
            btnSubmit.Text = "Confirm";

            // get a timestamp in the format Realex requires
            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            // here we make a unique order ID by appending a random number onto the timestamp.  In your application
            // use any unique value, such as the OrderID from the Order table of your database.
            Random r = new Random();
            int rand = r.Next(1, 1000);
            string orderID = timestamp + rand;

            // get the Realex settings from the web config
            string merchantID = WebConfigurationManager.AppSettings["RealexMerchantID"];
            string sharedSecret = WebConfigurationManager.AppSettings["RealexSharedSecret"];
            string accountName = WebConfigurationManager.AppSettings["RealexAccountName"];

            // for simplicity we are hardcoding the currency as Euro
            string currencyCode = "EUR";

            // generate the hash according to Realex's rules
            string temp1 = RealexProcessor.ComputeSha1Hash(timestamp + "." + merchantID + "." + orderID + "." + amountInCent + "." + currencyCode);
            temp1 = temp1.ToLower();
            string temp2 = temp1 + "." + sharedSecret;
            string hash = RealexProcessor.ComputeSha1Hash(temp2);
            hash = hash.ToLower();

            /* insert the hidden fields that Realex expects.  
             * We have to use a literal because if we used ASP.Net hidden fields the names would be corrupted 
             * to something like ctl00_ContentPlaceHolder1_MERCHANT_ID which Realex would not be expecting.
             * */
            StringBuilder hiddenFields = new StringBuilder();
            hiddenFields.AppendLine("<input type=\"hidden\" name=\"MERCHANT_ID\" value=\"" + merchantID + "\" />");
            hiddenFields.AppendLine("<input type=\"hidden\" name=\"ORDER_ID\" value=\"" + orderID + "\" />");
            hiddenFields.AppendLine("<input type=\"hidden\" name=\"ACCOUNT\" value=\"" + accountName + "\" />");
            hiddenFields.AppendLine("<input type=\"hidden\" name=\"CURRENCY\" value=\"" + currencyCode + "\" />");
            hiddenFields.AppendLine("<input type=\"hidden\" name=\"AMOUNT\" value=\"" + amountInCent + "\" />");
            hiddenFields.AppendLine("<input type=\"hidden\" name=\"TIMESTAMP\" value=\"" + timestamp + "\" />");
            hiddenFields.AppendLine("<input type=\"hidden\" name=\"SHA1HASH\" value=\"" + hash + "\" />");
            litHiddenFields.Text = hiddenFields.ToString();

            /* By setting the PostBackUrl, when the user clicks submit the form will be submitted to Realex rather than to this application.
             * Realex's server will then present the user with a form for the user to enter their credit card details.
             * Realex will then attempt to take payment.
             * The next thing our application will do is process MpiResponse.aspx when Realex posts the transaction result to it.
             * */
            btnSubmit.PostBackUrl = "https://epage.payandshop.com/epage.cgi";

        }
    }
}