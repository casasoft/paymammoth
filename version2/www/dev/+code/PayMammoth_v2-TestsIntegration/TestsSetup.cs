﻿using NUnit.Framework;
using PayMammoth_v2.Framework.Application;

namespace PayMammoth_v2_TestsIntegration
{
    [SetUpFixture]
    public class TestsSetup
    {

        public class AppInstancePaymammothv2TestsIntegration : AppInstancePayMammoth_v2
        {
            protected override void initialCustomComponentsWithInversionOfControl()
            {
                base.initialCustomComponentsWithInversionOfControl();
            }
        }

        [SetUp]
        public void Setup()
        {
            CS.General_CS_v5.Util.OtherUtil.SetIsInIntegrationTestingEnvironment();
            //CS.General_CS_v5.Util.OtherUtil.SetIsInUnitTestingEnvironment();

            var app = new AppInstancePaymammothv2TestsIntegration();
            app.AddProjectAssembly(typeof(General_Tests_CS_v5.TestsSetup).Assembly);
            app.AddProjectAssembly(typeof(TestsSetup).Assembly);
            app.OnApplicationStart();


            PayMammoth_v2.Connector.Constants.PayMammoth_ServerUrl = PayMammoth_v2.Constants.TestPayMammothLocalDomain;
            //PayMammoth_v2.Connector.Constants.PayMammoth_ServerUrl = PayMammoth_v2.Constants.TestPayMammothAzureDomain;

        }

        [TearDown]
        public void Teardown()
        {

        }
    }
}
