﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Connector.Services;
using PayMammoth_v2.Modules.Data.TestData;

namespace PayMammoth_v2_TestsIntegration.IntegrationTests
{
    public static class IntegrationUtil
    {
        private static readonly IRavenDbService _ravenDbService;
        static IntegrationUtil()
        {
            _ravenDbService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IRavenDbService>();
            
        }
        public static TestPaymentData CreateTestPaymentInfo()
        {
            var session = _ravenDbService.CreateNewSession();

            TestPaymentData testPayment = new TestPaymentData();
            testPayment.Title = "Test payment in PaymentCompleteTests";
            session.Store(testPayment);
            session.SaveChanges();
            session.Dispose();
            return testPayment;
        }
        public static InitialRequestResponseMessage CreateTestInitialRequestAndSendToPayMammoth(string orderReference = null)
        {
            if (orderReference == null)
            {
                orderReference = "Karl-Test1";
            }
            InitialRequestInfo initialRequest = new InitialRequestInfo();
            initialRequest.Details.ClientContactDetails.Address1 = "Triq il-Kittenija";
            initialRequest.Details.ClientContactDetails.Country3LetterCode = CS.General_CS_v5.Enums.CountryIso3166To3LetterCode(Enums.CountryIso3166.Malta);
            initialRequest.Details.ClientContactDetails.Email = "karl@casasoft.com.mt";
            initialRequest.Details.ClientContactDetails.Name = "Karl";
            initialRequest.Details.ClientContactDetails.LastName = "Cassar";
            initialRequest.Details.ClientReference = "Karl-Test1";
            initialRequest.Details.Description = "Test item 1";
            initialRequest.Details.OrderReference = orderReference;
            initialRequest.Details.Title = "Test Payment";
            initialRequest.ItemDetails.Add(new PaymentRequestItemLine()
            {
                Description = "Line 1",
                Quantity = 1,
                TaxAmountPerUnit = 0.18m,
                Title = "Item 1",
                UnitPrice = 1
            });
            initialRequest.Language = PayMammoth_v2.Connector.Enums.SupportedLanguage.English; ;
            initialRequest.NotificationUrl = PayMammoth_v2.Constants.TestPayMammothLocalDomain + PayMammoth_v2.Constants.TestNotificationHandler;
            initialRequest.OrderLinkOnClientWebsite = "http://www.clientwebsite.com/order-link.aspx";
            initialRequest.Pricing.CurrencyCode3Letter = CS.General_CS_v5.Enums.CurrencyCode_Iso4217ToCode(Enums.CurrencyCode_Iso4217.EuroMemberCountriesEuro);
            initialRequest.Pricing.HandlingAmount = 1;
            initialRequest.Pricing.ShippingAmount = 2;
            initialRequest.ReturnUrls.FailureUrl = "http://www.clientwebsite.com/failure.aspx";
            initialRequest.ReturnUrls.SuccessUrl = "http://www.clientwebsite.com/success.aspx";

            //PayMammoth_v2.Connector.Constants.PayMammoth_InitialRequestUrl = Constants.PaymentClientBaseUrl + Constants.InitialRequestHandler;
            //PayMammoth_v2.Connector.Constants.PayMammoth_InitialRequestUrl = "http://localhost/" + Constants.InitialRequestHandler;
            //PayMammoth_v2.Connector.Constants.PayMammoth_InitialRequestUrl = "http://localhost/_tests/initialrequest.ashx";
            //PayMammoth_v2.Connector.Constants.TestSendRequest();
            var result = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPayMammothConnectorInitialRequestService>().CreatePaymentRequestOnPayMammoth(
                RavenDbUtil.GetIdOnlyFromFullId(Constants.TestWebsiteAccountId),
                Constants.TestWebsiteAccount_SecretWord,
                initialRequest);

            return result;


        }

        public static void WaitUntilTestPaymentIsMarkedAsPaid(string testPaymentId)
        {
            
            bool isPaid = false;
            do
            {
                var session = _ravenDbService.CreateNewSession();
                var checkTestPayment = session.GetById<TestPaymentData>(new GetByIdParams<TestPaymentData>(testPaymentId)
                {
                    ThrowErrorIfNotAlreadyLoadedInSession = false
                });

                isPaid = checkTestPayment.MarkedAsPaid;


                session.Dispose();
                if (!isPaid)
                {
                    System.Threading.Thread.Sleep(30 * 1000);
                }
            } while (!isPaid);

            int k = 5;
            //System.Threading.Thread.Sleep(30 * 1000); //one last time

        }
    }
}
