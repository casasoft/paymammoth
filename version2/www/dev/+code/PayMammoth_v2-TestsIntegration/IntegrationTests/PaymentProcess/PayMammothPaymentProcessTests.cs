﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5;
using NUnit.Framework;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.TestData;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2_TestsIntegration.IntegrationTests.PaymentMethods.PayPal.v1;
using Raven.Client;
using Should;

namespace PayMammoth_v2_TestsIntegration.IntegrationTests.PaymentProcess
{
    /// <summary>
    /// This integration tests out having a website account, creating a test payment request, test transaction and marking it as paid
    /// 
    /// </summary>
    [TestFixture]
    public class PayMammothPaymentProcessTests
    {
        private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestTransactionDataService _paymentRequestTransactionDataService;

        public PayMammothPaymentProcessTests()
        {
            _ravenDbService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = BusinessLogic_CS_v5.Util.InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestTransactionDataService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPaymentRequestTransactionDataService>();
        }


        private WebsiteAccountData createWebsiteAccount(IDocumentSession session)
        {
            WebsiteAccountData testWebsite = _dataObjectFactory.CreateNewDataObject<WebsiteAccountData>();
            testWebsite.WebsiteName = "TestWebsiteAccount-PaymentCompleteTests-" + DateTime.Now.Ticks;
            testWebsite.Notifications.ClientWebsiteResponseUrl = "http://office.casasoft.com.mt/test-notify.ashx";
            session.Store(testWebsite);
            return testWebsite;

        }
        private StartExpressCheckoutIntegrationTest.TestPaymentsData createTestPaymentRequest(WebsiteAccountData websiteAccount, IDocumentSession session)
        {
            StartExpressCheckoutIntegrationTest.TestPaymentsData testPaymentsData = new StartExpressCheckoutIntegrationTest.TestPaymentsData();

            TestPaymentData testPayment = new TestPaymentData();
            testPayment.Title = "Test payment in PaymentCompleteTests";
            session.Store(testPayment);
            
            testPaymentsData.TestPayment = testPayment;

            PaymentRequestData requestData = _dataObjectFactory.CreateNewDataObject<PaymentRequestData>();
            requestData.RequestDetails = new InitialRequestInfo();
            requestData.RequestDetails.Details.OrderReference = testPayment.GetRavenDbIdOnlyFromItem();
            requestData.RequestDetails.NotificationUrl = PayMammoth_v2.Constants.TestPayMammothLocalDomain + PayMammoth_v2.Constants.TestNotificationHandler;
            requestData.LinkedWebsiteAccountId = websiteAccount;
            testPaymentsData.PaymentRequest = requestData;
            session.Store(requestData);
            return testPaymentsData;;

        }
        private PaymentRequestTransactionData createTestPaymentRequestTransaction(PaymentRequestData request, IDocumentSession session)
        {
            PaymentRequestTransactionData transaction = _dataObjectFactory.CreateNewDataObject<PaymentRequestTransactionData>();
            transaction.PaymentRequestId = request;
            session.Store(transaction);
            return transaction;

        }
        [Test]
        public void StartTest()
        {
            var session = _ravenDbService.CreateNewSession();
            var websiteAccount =createWebsiteAccount(session);
            var testData = createTestPaymentRequest(websiteAccount, session);
            var transaction = createTestPaymentRequestTransaction(testData.PaymentRequest, session);
            session.SaveChanges();
            session.Dispose();
            //-------/


            {
                session = _ravenDbService.CreateNewSession();
                transaction =
                   session.GetById<PaymentRequestTransactionData>(
                       new GetByIdParams<PaymentRequestTransactionData>(transaction.Id)
                       {
                           ThrowErrorIfNotAlreadyLoadedInSession = false
                       });
                _paymentRequestTransactionDataService.MarkTransactionAsPaid(transaction, false, "TEST-AUTH", session);
                session.SaveChanges();
                session.Dispose();
            }
            {
                session = _ravenDbService.CreateNewSession();
                transaction =
                    session.GetById<PaymentRequestTransactionData>(
                        new GetByIdParams<PaymentRequestTransactionData>(transaction.Id)
                        {
                            ThrowErrorIfNotAlreadyLoadedInSession = false
                        });
                transaction.IsSuccessful.ShouldBeTrue();
                session.Dispose();
            }
        //    waitUntilTestPaymentIsMarkedAsPaid(testData);




        }

    }
}
