﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using BusinessLogic_CS_v5.Util;
using NUnit.Framework;
using PayMammoth_v2.Modules.Data.TestData;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v2_TestsIntegration.IntegrationTests.PaymentProcess
{
    /// <summary>
    /// This test tests out the full interaction with PayMammoth.  This needs to have PayMammoth listening locally.
    /// This will first create an initial request via the test website account.  Then, you need to MANUALLY mark it as paid on PayMammoth.
    /// It will wait indefinitely until transaction is marked as paid
    /// </summary>
    [TestFixture]
    public class FullClientPaymammothInteractionIncludingNotificationsTests
    {

         private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestTransactionDataService _paymentRequestTransactionDataService;

        public FullClientPaymammothInteractionIncludingNotificationsTests()
        {
            _ravenDbService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = BusinessLogic_CS_v5.Util.InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestTransactionDataService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPaymentRequestTransactionDataService>();
        }
        

        [Test]
        public void StartTest()
        {
            var testPayment = IntegrationUtil.CreateTestPaymentInfo();
           
            var initialRequest = IntegrationUtil.CreateTestInitialRequestAndSendToPayMammoth(testPayment.GetRavenDbIdOnlyFromItem());

            string requestId = initialRequest.RequestId;
            System.Diagnostics.Trace.WriteLine("RequestID: " + requestId);

            IntegrationUtil.WaitUntilTestPaymentIsMarkedAsPaid(testPayment.Id);

        }
    }
}
