﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using BusinessLogic_CS_v5.Util;
using CS.General_CS_v5;
using General_Tests_CS_v5.TestUtil;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.TestData;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1;
using PayMammoth_v2_TestsIntegration.IntegrationTests.PaymentMethods.PayPal.v1;
using Raven.Client;
using Should;

namespace PayMammoth_v2_TestsIntegration.IntegrationTests.InitialRequests
{
     [TestFixture]
    public class InitialRequestIntegrationTest
    {
       
        

        public class TestData
        {
            public TestPaymentData TestPayment { get; set; }
            public PaymentRequestData PaymentRequest { get; set; }


        }

        private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestDataService _paymentRequestDataService;
        
        public InitialRequestIntegrationTest()
        {
            _ravenDbService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = BusinessLogic_CS_v5.Util.InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestDataService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPaymentRequestDataService>();
           
        }

    

        //private WebsiteAccountData createWebsiteAccount(IDocumentSession session)
        //{
        //    WebsiteAccountData testWebsite = _dataObjectFactory.CreateNewDataObject<WebsiteAccountData>();
        //    testWebsite.WebsiteName = "TestWebsiteAccount-" + DateTime.Now.Ticks;
        //    //testWebsite.AccountCode = "TestWebsite";
        //    testWebsite.SecretWord = "123456";

        //    session.Store(testWebsite);
        //    return testWebsite;

        //}


        

         [Test]
        public void Start()
        {
          //  PayMammoth_v2.Connector.Constants.TestSendRequest();


            var result = IntegrationUtil.CreateTestInitialRequestAndSendToPayMammoth();

            result.ShouldNotBeNull();
            result.Status.ShouldEqual(PayMammoth_v2.Connector.Enums.InitialRequestResponseStatus.Success);
            result.RequestId.ShouldNotBeEmpty();
            

           
        }


    }
}
