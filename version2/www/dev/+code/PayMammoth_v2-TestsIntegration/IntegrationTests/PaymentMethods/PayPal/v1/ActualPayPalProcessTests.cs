﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v5.Presentation.Services.Cultures;
using BusinessLogic_CS_v5.Util;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1;
using PayMammoth_v2.Presentation.Services.Payments;

namespace PayMammoth_v2_TestsIntegration.IntegrationTests.PaymentMethods.PayPal.v1
{
    /// <summary>
    /// This performs a full integration test, by creating a request, selecting PayPal as payment, filling in the form submitting, and waiting to mark as paid
    /// </summary>
    [TestFixture]
    public class ActualPayPalProcessTests
    {
         private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestDataService _paymentRequestDataService;
        private readonly IPayPalManager _payPalManager;
        private readonly IPaymentUrlModelService _paymentUrlModelService;
        private readonly ICultureModelService _cultureModelService;

        public ActualPayPalProcessTests()
        {
            _ravenDbService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = BusinessLogic_CS_v5.Util.InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestDataService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPaymentRequestDataService>();
            _payPalManager = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPayPalManager>();
            _paymentUrlModelService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPaymentUrlModelService>();
            _cultureModelService = BusinessLogic_CS_v5.Util.InversionUtil.Get<ICultureModelService>();
        }

        [Test]
        public void Start()
        {

            var testPayment = IntegrationUtil.CreateTestPaymentInfo();


            var initialRequest = IntegrationUtil.CreateTestInitialRequestAndSendToPayMammoth(testPayment.GetRavenDbIdOnlyFromItem());


            automatePayPal(initialRequest.RequestId);
            

            string testPaymentId = testPayment.Id;

            IntegrationUtil.WaitUntilTestPaymentIsMarkedAsPaid(testPayment.Id);
            System.Threading.Thread.Sleep(300 * 1000);

        }

        private void automatePayPal(string requestId)
        {
            var selectionUrl = PayMammoth_v2.Connector.Constants.PayMammoth_ServerUrl + "en/payment/?identifier=" + HttpUtility.UrlEncode(requestId);


            IWebDriver webDriver = new FirefoxDriver();
            webDriver.Navigate().GoToUrl(selectionUrl);

            

            int k = 5;

        
        }


    }
}
