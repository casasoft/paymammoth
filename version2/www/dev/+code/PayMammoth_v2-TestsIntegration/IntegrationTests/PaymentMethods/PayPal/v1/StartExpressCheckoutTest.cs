﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5;
using CS.General_CS_v5.Modules.InversionOfControl;
using General_Tests_CS_v5.TestUtil;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.TestData;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1;
using Raven.Client;
using Should;

namespace PayMammoth_v2_TestsIntegration.IntegrationTests.PaymentMethods.PayPal.v1
{

   

    [TestFixture]
    public class StartExpressCheckoutIntegrationTest 
    {

        /*
Integration Test - PayPal
----------------

1. generates a test payment, and stores its equivalent TestPaymentData.
2. redirects to PayPal
3. user pays on paypal, and then is redirected to the confirm page on PayMammoth
4. user accepts confirm page, confirmation sent to PayPal
5. PayPal redirects user to success page, and sends IPN message in the background
6. PayMammoth receives IPN, and marks the payment as successful.  
7. PayMammoth sends a notification msg, which is received by PayMammoth itself.
8. Notification is confirmed, and TestData is marked as paid
9. Integration test is waiting until TestData is marked as paid.
         * 
         */

        

        public class TestPaymentsData
        {
            public TestPaymentData TestPayment { get; set; }
            public PaymentRequestData PaymentRequest { get; set; }


        }

        private readonly IRavenDbService _ravenDbService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestDataService _paymentRequestDataService;
        private readonly IPayPalManager _payPalManager;

        public StartExpressCheckoutIntegrationTest()
        {
            _ravenDbService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IRavenDbService>();
            _dataObjectFactory = BusinessLogic_CS_v5.Util.InversionUtil.Get<IDataObjectFactory>();
            _paymentRequestDataService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPaymentRequestDataService>();
            _payPalManager = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPayPalManager>();
            
        }

    

        private WebsiteAccountData createWebsiteAccount(IDocumentSession session)
        {
            WebsiteAccountData testWebsite = _dataObjectFactory.CreateNewDataObject<WebsiteAccountData>();
            testWebsite.WebsiteName = "TestWebsiteAccount-" + DateTime.Now.Ticks;
            testWebsite.PaymentsInformation.PayPal.Enabled = true;
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.MerchantEmail = "paypal.cs.test.business.en@dispostable.com";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.MerchantId = "SAN5FMKHMTM9C";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.Password = "1389117990";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.Signature = "AxRMHSZhKz2kBtwBMbnqHM0tDC7XA8hhARUerqhHUKWfOaz0b1MN80Zd";
            testWebsite.PaymentsInformation.PayPal.SandboxAccount.Username = "paypal.cs.test.business.en_api1.dispostable.com";
            testWebsite.PaymentsInformation.PayPal.UseLiveEnvironment = false;
            session.Store(testWebsite);
            return testWebsite;

        }
        private TestPaymentsData createTestPaymentRequest(WebsiteAccountData websiteAccount, IDocumentSession session)
        {
            TestPaymentsData testData = new TestPaymentsData();


            TestPaymentData testingPayment = new TestPaymentData();
            


            InitialRequestInfo initialRequest = new InitialRequestInfo();
            //{
            //    initialRequest.Details.ClientContactDetails.Address1 = "20, Cor Jesu";
            //    initialRequest.Details.ClientContactDetails.Address2 = "Triq il-Kittenija";
            //    initialRequest.Details.ClientContactDetails.ClientReference = "Test-Client";
            //    initialRequest.Details.ClientContactDetails.Country3LetterCode =
            //        CS.General_CS_v5.Enums.CountryIso3166To3LetterCode(Enums.CountryIso3166.Malta);
            //    initialRequest.Details.ClientContactDetails.Email = "karlcassar@gmail.com";
            //}
            initialRequest.Pricing.CurrencyCode3Letter = CS.General_CS_v5.Enums.CurrencyCode_Iso4217ToCode(Enums.CurrencyCode_Iso4217.EuroMemberCountriesEuro);
            {
                var paymentRequestItemLine = new PaymentRequestItemLine();
                initialRequest.ItemDetails.Add(paymentRequestItemLine);
                paymentRequestItemLine.Quantity = 2;
                paymentRequestItemLine.UnitPrice = 1;
                paymentRequestItemLine.Title = "Test Item";
            }
            {
                var paymentRequestItemLine = new PaymentRequestItemLine();
                initialRequest.ItemDetails.Add(paymentRequestItemLine);
                paymentRequestItemLine.Quantity = 1;
                paymentRequestItemLine.UnitPrice = 2;
                paymentRequestItemLine.Title = "Test Item2" ;
            }
            testingPayment.Total = 3;

            testingPayment.Title = string.Format("Test payment generated on [{0}]", DateTime.Now.ToString(("dd/MM/yyyy HH:mm:ss")));
            session.Store(testingPayment);
            session.SaveChanges();


            testData.TestPayment = testingPayment;

            initialRequest.Details.OrderReference = testingPayment.Id;
            initialRequest.NotificationUrl = PayMammoth_v2.Constants.TestPayMammothLocalDomain + PayMammoth_v2.Constants.TestNotificationHandler;
            initialRequest.ReturnUrls.SuccessUrl = PayMammoth_v2.Constants.TestPayMammothLocalDomain + "_tests/success.aspx";
            initialRequest.ReturnUrls.FailureUrl = PayMammoth_v2.Constants.TestPayMammothLocalDomain + "_tests/failure.aspx";
            var newPaymentRequest = _paymentRequestDataService.GeneratePaymentRequestFromInitialRequest(websiteAccount, initialRequest, null);
            newPaymentRequest.Status.ShouldEqual(PayMammoth_v2.Connector.Enums.InitialRequestResponseStatus.Success);
            testData.PaymentRequest = newPaymentRequest.GeneratedRequest;
            return testData;

        }


        [Test]
        public void Start()
        {
            var session = _ravenDbService.CreateNewSession();

            var websiteAccount = createWebsiteAccount(session);
            session.SaveChanges();

            var testData = createTestPaymentRequest(websiteAccount, session);
            var request = testData.PaymentRequest;
            session.SaveChanges();

            request.ShouldNotBeNull();
            request.LinkedWebsiteAccountId.ShouldNotBeNull();
            

            
            session.Dispose();
            session = _ravenDbService.CreateNewSession();

            var generateResult = _paymentRequestDataService
                .GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructions(PayMammoth_v2.Connector.Enums.PaymentMethodType.PayPal_ExpressCheckout, 
                request,null);
            

            //Assertions
            
            generateResult.Result.ShouldEqual(PayMammoth_v2Enums.RedirectionResultStatus.Success);
            generateResult.UrlToRedirectTo.ShouldNotBeNullOrWhitespace();
            
            visitPage(generateResult);
            session.Dispose();

            string testPaymentId = testData.TestPayment.Id;
            bool isPaid = false;
            do
            {
                session = _ravenDbService.CreateNewSession();
                var checkTestPayment = session.GetById<TestPaymentData>(new GetByIdParams<TestPaymentData>(testPaymentId)
                {
                    ThrowErrorIfNotAlreadyLoadedInSession = false
                });

                isPaid  = checkTestPayment.MarkedAsPaid;
                

                session.Dispose();
                if (!isPaid)
                {
                    System.Threading.Thread.Sleep(30 * 1000);
                }

            } while (!isPaid);

            System.Threading.Thread.Sleep(300 * 1000);
           
        }

        private void visitPage(TransactionGeneratorResult result)
        {
            IWebDriver webDriver = new FirefoxDriver();
            webDriver.Navigate().GoToUrl(result.UrlToRedirectTo);
        }

    }
}
