﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Links;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.NotificationMessages;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Models._Shared.PartialViews.Payments;

namespace PayMammoth_v2.Presentation.Code.ViewData
{
    public class LayoutViewData
    {
        /// <summary>
        /// If an action is present, you can safely assume that this is present as if
        /// an error is present, it is shown before executing action
        /// </summary>
        public PaymentRequestDataModel PaymentRequestDataModel { get; set; }

        public _PaymentOrderSummaryModel OrderSummaryModel { get; set; }
        public _PaymentDetailsModel PaymentDetailsModel { get; set; }

        public _NotificationMessageModel ExpirationMessage { get; set; }
        public _NotificationMessageModel FakePaymentsEnabledMessage { get; set; }
        public AnchorModel CancelAndReturnBackLink { get; set; }

    }
}
