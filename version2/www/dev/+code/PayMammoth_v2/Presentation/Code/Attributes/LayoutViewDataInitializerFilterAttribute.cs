﻿using System.Web.Mvc;
using System.Web.UI;
using BusinessLogic_CS_v5.Presentation.Services.CasaSoft;
using BusinessLogic_CS_v5.Presentation.Services.ContentTexts;
using BusinessLogic_CS_v5.Presentation.Services.Cultures;
using BusinessLogic_CS_v5.Presentation.Services.Navigation;
using CS.General_CS_v5.Modules.Pages;
using CS.MvcGeneral_CS_v5.Code.Attributes;
using PayMammoth_v2.Presentation.Code.ViewData;
using PayMammoth_v2.Presentation.Services.Attributes;
using PayMammoth_v2.Presentation.Services.Payments;
using Raven.Client;

namespace PayMammoth_v2.Presentation.Code.Attributes
{
    public class LayoutViewDataInitializerFilterAttribute : ViewDataAndContextInitializerBaseFilterAttribute

    {
        public LayoutViewDataInitializerFilterAttribute() : 
            base(ActionFilterTriggerType.OnActionExecuting)
        {
            string s = "";
        }

        public override string ViewDataAndContextKey
        {
            get { return PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey; }
        }

        public override object CreateViewDataObject(
            IDocumentSession documentSession,
            ControllerContext controllerContext)
        {
            var culture =
                BusinessLogic_CS_v5.Util.InversionUtil.Get<ICultureModelService>()
                    .GetCultureFromCurrentRouteOrDefaultCultureAndStoreInContext();
            return BusinessLogic_CS_v5.Util.InversionUtil.Get<ILayoutViewDataInitializerService>()
                .InitalizeLayoutViewDataForCurrentRequest(
                    documentSession,
                    culture,
                    controllerContext as ActionExecutingContext);
        }

    }
}
