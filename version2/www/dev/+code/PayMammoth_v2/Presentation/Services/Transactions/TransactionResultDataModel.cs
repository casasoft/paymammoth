﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Presentation.Services.Transactions
{
    public class TransactionResultDataModel
    {
        public PayMammoth_v2.Enums.PayMammoth_v2Enums.RedirectionResultStatus Status { get; set; }
        public string RedirectUrl { get; set; }
    }
}
