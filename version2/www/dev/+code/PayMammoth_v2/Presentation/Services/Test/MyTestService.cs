﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.General_CS_v5.Modules.InversionOfControl;

namespace PayMammoth_v2.Presentation.Services.Test
{
    public interface IMyTestService
    {
        int AddValues(int a, int b);
        List<int> SearchItems(string keywords, CS.General_CS_v5.Enums.EnumsSortBy sortBy);
    }

    [IocComponent]
    public class MyTestService : IMyTestService
    {
        public MyTestService()
        {

        }

        public int AddValues(int a, int b)
        {
            return a + b;
        }


        public List<int> SearchItems(string keywords, CS.General_CS_v5.Enums.EnumsSortBy sortBy)
        {
            var list = new List<int>();
            for (int i = 0; i < 10; i++)
            {
                list.Add(i);
            }
            return list;
        }
    }
    
}
