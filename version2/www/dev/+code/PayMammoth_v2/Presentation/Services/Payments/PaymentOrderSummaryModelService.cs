﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v5.Presentation.Services.ContentTexts;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Models._Shared.PartialViews.Payments;
using Raven.Client;

namespace PayMammoth_v2.Presentation.Services.Payments
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IPaymentOrderSummaryModelService
    {
        _PaymentOrderSummaryModel CreatePaymentOrderSummaryModel(
            IDocumentSession documentSession,
            CultureModel culture,
            PaymentOrderSummaryDataModel order);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentOrderSummaryModelService : IPaymentOrderSummaryModelService
    {
        private readonly IContentTextModelService _contentTextModelService;

        public PaymentOrderSummaryModelService(IContentTextModelService contentTextModelService)
        {
            _contentTextModelService = contentTextModelService;
        }

        public _PaymentOrderSummaryModel CreatePaymentOrderSummaryModel(
            IDocumentSession documentSession,
            CultureModel culture,
            PaymentOrderSummaryDataModel order)
        {
            _PaymentOrderSummaryModel model = new _PaymentOrderSummaryModel()
            {
                OrderSummaryDataModel = order,
                TitleContentText =
                    string.IsNullOrWhiteSpace(order.Title)
                        ? _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                            documentSession,
                            culture,
                            PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_Title)
                        : null,
                TotalContentText = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    documentSession,
                    culture,
                    PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_Total),
                TotalHandlingContentText =
                    _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                        documentSession,
                        culture,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalHandling),
                TotalNetContentText = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    documentSession,
                    culture,
                    PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalNet),
                TotalShippingContentText =
                    _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                        documentSession,
                        culture,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalShipping),
                TotalTaxContentText = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    documentSession,
                    culture,
                    PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalTax),
                ViewFullOrderDetailsContentText =
                    _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                        documentSession,
                        culture,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_ViewFullOrderDetails),
                Total = BusinessLogic_CS_v5.Util.NumberUtilBL.FormatCurrency(
                    order.Total,
                    culture,
                    order.OrderCurrency),

                TotalHandling = order.TotalHandling.HasValue
                    ? BusinessLogic_CS_v5.Util.NumberUtilBL.FormatCurrency(
                        order.TotalHandling.Value,
                        culture,
                        order.OrderCurrency)
                    : null,

                TotalShipping = order.TotalShipping.HasValue
                    ? BusinessLogic_CS_v5.Util.NumberUtilBL.FormatCurrency(
                        order.TotalShipping.Value,
                        culture,
                        order.OrderCurrency)
                    : null,

                TotalTax = order.TotalTax.HasValue
                    ? BusinessLogic_CS_v5.Util.NumberUtilBL.FormatCurrency(
                        order.TotalTax.Value,
                        culture,
                        order.OrderCurrency)
                    : null,
                TotalNet = BusinessLogic_CS_v5.Util.NumberUtilBL.FormatCurrency(
                    order.TotalNet,
                    culture,
                    order.OrderCurrency)

            };
            return model;
        }
    }
}
