﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Classes.HelperClasses;
using CS.General_CS_v5.Modules.Attributes;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments.PayPal;
using Raven.Client;

namespace PayMammoth_v2.Presentation.Services.Payments.PayPal
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public enum ConfirmPaypalRequestStatus
    {
        Success,
        [ContentTextDataDefaultValues("Error has been encountered.  Please try again and if problem persists, contact us.")]
        Error
    }

    public interface IPaypalDataModelService
    {
        StatusResult<ConfirmPaypalRequestStatus, PayPalConfirmationResultDataModel> ConfirmPaypalRequestFromPresentation(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestIdentifier,
            string token,
            string payerId);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaypalDataModelService : IPaypalDataModelService
    {
        private readonly IPayPalManager _payPalManager;

        public PaypalDataModelService(IPayPalManager payPalManager)
        {
            _payPalManager = payPalManager;
        }

        public StatusResult<ConfirmPaypalRequestStatus, PayPalConfirmationResultDataModel> ConfirmPaypalRequestFromPresentation(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestIdentifier,
            string token,
            string payerId)
        {

            //todo: [For: Karl | 2014/04/29] add unit-tests for ConfirmPayPalRequestFromPresentation (WrittenBy: Karl)        			

            var requestId = new ReferenceLink<PaymentRequestData>(requestIdentifier);
            var requestGetByIdParams = new GetByIdParams<PaymentRequestData>(requestId);
            requestGetByIdParams.ThrowErrorIfNotAlreadyLoadedInSession=false;
            var request = BusinessLogic_CS_v5.Util.RavenDbUtil.GetById(documentSession, requestGetByIdParams);

            var payPalResult = _payPalManager.ConfirmPayment(requestId,
                token, payerId);
            var result = new StatusResult<ConfirmPaypalRequestStatus, PayPalConfirmationResultDataModel>();
            result.Result = new PayPalConfirmationResultDataModel();
            result.Status = ConfirmPaypalRequestStatus.Error;
            if (payPalResult == PayPalEnums.CONFIRM_RESULT.OK)
            {
                result.Status = ConfirmPaypalRequestStatus.Success;
                result.Result.RedirectUrl = request.RequestDetails.ReturnUrls.SuccessUrl;
            }
            return result;
        }
    }
}
