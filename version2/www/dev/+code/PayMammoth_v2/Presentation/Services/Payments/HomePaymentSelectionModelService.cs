﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v5.Presentation.Services.ContentTexts;
using CS.General_CS_v5.Classes.HelperClasses;
using CS.General_CS_v5.Modules.Pages;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2.Presentation.Models.Home;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using Raven.Client;

namespace PayMammoth_v2.Presentation.Services.Payments
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IHomePaymentSelectionModelService
    {
        HomePaymentSelectionModel CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestId(
            IDocumentSession documentSession,
            CultureModel culture,
            PaymentRequestDataModel paymentRequestDataModel);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class HomePaymentSelectionModelService : IHomePaymentSelectionModelService
    {
        private readonly IPageService _pageService;
        private readonly IPaymentRequestDataModelService _paymentRequestDataModelService;
        private readonly IPaymentMethodModelService _paymentMethodModelService;
        private readonly IContentTextModelService _contentTextModelService;

        public HomePaymentSelectionModelService(
            IPaymentRequestDataModelService paymentRequestDataModelService,
            IPageService pageService,
            IPaymentMethodModelService paymentMethodModelService,
            IContentTextModelService contentTextModelService)

        {
            _contentTextModelService = contentTextModelService;
            _paymentMethodModelService = paymentMethodModelService;
            _paymentRequestDataModelService = paymentRequestDataModelService;
            _pageService = pageService;
        }

        public HomePaymentSelectionModel
            CreateHomePaymentsSelectionModelFromQuerystringPaymentRequestId(
            IDocumentSession documentSession,
            CultureModel culture,
            PaymentRequestDataModel paymentRequestDataModel)
        {
            HomePaymentSelectionModel model = new HomePaymentSelectionModel();
            model.PaymentMethods = paymentRequestDataModel.PaymentMethodsAvailable.ConvertAll(
                x => _paymentMethodModelService.ConvertPaymentRequestDataModelToModel(
                    documentSession,
                    culture,
                    x,
                    paymentRequestDataModel.PaymentRequestId,
                    paymentRequestDataModel.FakePaymentKey));
            
            return model;
        }

    }
}
