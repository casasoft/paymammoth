﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.ContactDetails;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Currency;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Fields;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.MediaItems;
using BusinessLogic_CS_v5.Presentation.Services.Bundling;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Classes.HelperClasses;
using CS.General_CS_v5.Modules.Attributes;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Models._Shared.Data.WebsiteAccounts;
using PayMammoth_v2.Presentation.Services.Models;
using PayMammoth_v2.Presentation.Services.Transactions;
using PayPal.OpenIdConnect;
using Raven.Client;

namespace PayMammoth_v2.Presentation.Services.Payments
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    [IocComponent(Priority = -100)]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentRequestDataModelServiceLocal : IPaymentRequestDataModelService
    {
        private readonly IFillPaymentRequestDataModelFromPaymentRequestDataService _fillPaymentRequestDataModelFromPaymentRequestDataService;
        private readonly IPaymentRequestDataService _paymentRequestDataService;

        public PaymentRequestDataModelServiceLocal(
            IFillPaymentRequestDataModelFromPaymentRequestDataService fillPaymentRequestDataModelFromPaymentRequestDataService,
            IPaymentRequestDataService paymentRequestDataService)
        {
            _paymentRequestDataService = paymentRequestDataService;
            _fillPaymentRequestDataModelFromPaymentRequestDataService = fillPaymentRequestDataModelFromPaymentRequestDataService;
        }

        public StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel> GetPaymentMethodRequestForPaymentRequestId(
            IDocumentSession documentSession,
            CultureModel cultureModel,
            string paymentRequestId,
            [AllowNull] string fakePaymentKey)
        {
            if (!string.IsNullOrWhiteSpace(paymentRequestId))
            {
                var result = new StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>()
                {
                    Result =
                        new PaymentRequestDataModel()
                        {
                            PaymentRequestId = paymentRequestId,
                            FakePaymentKey = fakePaymentKey,
                            FakePaymentsEnabled = !string.IsNullOrWhiteSpace(fakePaymentKey),
                            PaymentMethodsAvailable =
                                new List<PaymentMethodDataModel>()
                                {
                                    new PaymentMethodDataModel()
                                    {
                                        PaymentMethodType = Connector.Enums.PaymentMethodType.PayPal_ExpressCheckout,
                                        Icon = new MediaItemModel<PaymentMethodData.PaymentMethodDataIconImageSize>(),
                                        Title = new FieldModel<string>() {Value = "PayPal"},
                                        Description = new FieldModel<string>() {Value = "desc here"}
                                    }
                                },
                            OrderSummary =
                                new PaymentOrderSummaryDataModel()
                                {
                                    OrderCurrency =
                                        new CurrencyDataModel()
                                        {
                                            Currency3LetterCode = "EUR",
                                            CurrencyCode = CS.General_CS_v5.Enums.CurrencyCode_Iso4217.EuroMemberCountriesEuro,
                                            CurrencySymbol = "€",
                                            ExchangeRate = 1.443m
                                        },
                                    Items = new List<PaymentOrderSummaryItemDataModel>() {new PaymentOrderSummaryItemDataModel() {Title = "My Item", DescriptionHtml = "Decription"}}
                                },
                            CancelUrl = "http://www.cancel.com",
                            UserDetails =
                                new PaymentUserDetailsDataModel()
                                {
                                    ContactDetailsModel =
                                        new ContactDetailsModel()
                                        {
                                            Address =
                                                new AddressDetailsModel() {Country = new FieldModel<CS.General_CS_v5.Enums.CountryIso3166?>() {Value = CS.General_CS_v5.Enums.CountryIso3166.Malta}}
                                        }
                                },
                            PaymentExpiryDate = new DateTimeOffset(
                                2014,
                                07,
                                10,
                                16,
                                30,
                                0,
                                0,
                                new TimeSpan()),
                            WebsiteAccount = new WebsiteAccountDataModel() {Logo = new MediaItemModel<WebsiteAccountData.WebsiteAccountDataLogoImageSize>()}
                        },
                    Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Success
                };

                if (paymentRequestId == "Expired")
                {
                    result.Status = GetPaymentMethodRequestForPaymentRequestIdStatus.Expired;
                    result.Result.CancelUrl = "http://www.cancel.com";
                }
                return result;
            }
            else
            {
                return new StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel>()
                {
                    Status = GetPaymentMethodRequestForPaymentRequestIdStatus.ValidRequestIdNotPresent
                };
            }
        }

        public TransactionResultDataModel GenerateTransactionResultFromPaymentMethod(
            IDocumentSession documentSession,
            CultureModel cultureModel,
            Connector.Enums.PaymentMethodType paymentMethod,
            string paymentRequestId)
        {
            return new TransactionResultDataModel()
            {
                Status = PayMammoth_v2Enums.RedirectionResultStatus.Success,
                RedirectUrl = "https://www.sandbox.paypal.com/cgi-bin/webscr?token=EC-68214744R89530450&cmd=_express-checkout"
            };
        }

        public StatusResult<CancelPaymentStatus, string> CancelPaymentForPaymentRequestId(
            IDocumentSession documentSession,
            CultureModel cultureModel,
            string paymentRequestId)
        {
            var status = new StatusResult<CancelPaymentStatus, string>() {Status = paymentRequestId == "Error" ? CancelPaymentStatus.Error : CancelPaymentStatus.Success};
            if (status.Status == CancelPaymentStatus.Success)
            {
                status.Result = "http://www.cancel.com";
            }
            return status;
        }
    }
}