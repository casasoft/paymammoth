﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v5.Presentation.Services.Sections;
using CS.General_CS_v5.Modules.Urls;
using Raven.Client;

namespace PayMammoth_v2.Presentation.Services.Payments
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IPaymentUrlModelService
    {
        string GetPaymentSelectionPage(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestId);

        string GetPayPalConfirmationPageUrl(
            IDocumentSession documentSession,
            CultureModel culture,
            string requestId);

        string GetTransactiumStatusPageUrl(
           IDocumentSession documentSession,
           CultureModel culture,
           string requestId);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentUrlModelService : IPaymentUrlModelService
    {
        private readonly ISectionModelService _sectionModelService;

        public PaymentUrlModelService(ISectionModelService sectionModelService)
        {
            _sectionModelService = sectionModelService;
        }

        private string addRequestIdToUrl(string url, string requestId)
        {
            UrlHandler urlHandler = new UrlHandler(url);
            urlHandler.QueryString[PayMammoth_v2.Connector.Constants.PARAM_IDENTIFIER] = requestId;
            return urlHandler.GetUrl(true);

        }
        //public string GetPaymentSelectionPage(
        //    IDocumentSession documentSession,
        //    CultureModel culture,
        //    string requestId)
        //{
        //    string url = _sectionModelService.GetSectionUrlWithRouteValuesFromIdentifier(documentSession, culture,
        //        PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Selection);
        //    url = addRequestIdToUrl(url, requestId);
        //    return url;
        //}
        public string GetTransactiumPaymentPage(
           IDocumentSession documentSession,
           CultureModel culture,
           string requestId,
            string redirectUrl)
        {
            string url = _sectionModelService.GetSectionUrlWithRouteValuesFromIdentifier(documentSession, culture,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Payment_Transactium);
            url = addRequestIdToUrl(url, requestId);

            UrlHandler urlHandler = new UrlHandler(url);
            urlHandler.QueryString[PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Transactium_RedirectUrl] = redirectUrl;
            url = urlHandler.GetUrl();

            return url;
        }

        public string GetPayPalConfirmationPageUrl(IDocumentSession documentSession,
            CultureModel culture,
            string requestId)
        {
            string url = _sectionModelService.GetSectionUrlWithRouteValuesFromIdentifier(documentSession,
                culture,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Payment_PaypalConfirmation);
            url = addRequestIdToUrl(url, requestId);

            UrlHandler urlHandler = new UrlHandler(url);
            //urlHandler.QueryString[PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Token] = token;
         //   urlHandler.QueryString[PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_PayerId] = payerId;
            url = urlHandler.GetUrl();
            return url;
        }
        public string GetPayPalConfirmationPageUrl_OLD(IDocumentSession documentSession,
          CultureModel culture,
          string requestId,
          string token,
          string payerId)
        {
            string url = _sectionModelService.GetSectionUrlWithRouteValuesFromIdentifier(documentSession,
                culture,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Payment_PaypalConfirmation);
            url = addRequestIdToUrl(url, requestId);

            UrlHandler urlHandler = new UrlHandler(url);
            urlHandler.QueryString[PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Token] = token;
            urlHandler.QueryString[PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_PayerId] = payerId;
            url = urlHandler.GetUrl();
            return url;
        }



        public string GetTransactiumStatusPageUrl(IDocumentSession documentSession, CultureModel culture, string requestId)
        {
            string url = _sectionModelService.GetSectionUrlWithRouteValuesFromIdentifier(documentSession,
               culture,
               PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Payment_Transactium_Status);
            url = addRequestIdToUrl(url, requestId);

            UrlHandler urlHandler = new UrlHandler(url);
            url = urlHandler.GetUrl();
            return url;
        }

        public string GetPaymentSelectionPage(IDocumentSession documentSession, CultureModel culture, string requestId)
        {
            string url = _sectionModelService.GetSectionUrlWithRouteValuesFromIdentifier(documentSession,
              culture,
              PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Selection);
            url = addRequestIdToUrl(url, requestId);

            UrlHandler urlHandler = new UrlHandler(url);
            url = urlHandler.GetUrl();
            return url;
        }
    }
}
