﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v5.Presentation.Services.Sections;
using CS.General_CS_v5.Modules.Urls;
using PayMammoth_v2.KarlTestUI.Models;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using Raven.Client;

namespace PayMammoth_v2.Presentation.Services.Payments
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IPaymentMethodModelService
    {
        PaymentMethodModel ConvertPaymentRequestDataModelToModel(IDocumentSession documentSession, CultureModel culture, PaymentMethodDataModel paymentMethodDataModel, string paymentRequestId, string fakePaymentId);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentMethodModelService : IPaymentMethodModelService
    {
        private readonly ISectionModelService _sectionModelService;

        public PaymentMethodModelService(ISectionModelService sectionModelService)
        {
            _sectionModelService = sectionModelService;
        }

        public PaymentMethodModel ConvertPaymentRequestDataModelToModel(
            IDocumentSession documentSession,
            CultureModel culture,
            PaymentMethodDataModel paymentMethodDataModel,
            string paymentRequestId,
            [AllowNull]
            string fakePaymentId)
        {
            string url = _sectionModelService.GetSectionUrlWithRouteValuesFromIdentifier(
                documentSession,
                culture,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Selection_Handler,
                routeValues: null);
            UrlHandler urlHandler = new UrlHandler(url);
            urlHandler.QueryString[PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_PaymentMethod] = CS
                .General_CS_v5.Util.ConversionUtil.ConvertBasicDataTypeToString(
                    paymentMethodDataModel.PaymentMethodType);
            if (!string.IsNullOrWhiteSpace(fakePaymentId))
            {
                urlHandler.QueryString[PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_FakePaymentId] = fakePaymentId;
            }
            urlHandler.QueryString[PayMammoth_v2.Connector.Constants.PARAM_IDENTIFIER] = paymentRequestId;
            string urlPayment = urlHandler.GetUrl();
            PaymentMethodModel model = new PaymentMethodModel()
            {
                PaymentMethodDataModel = paymentMethodDataModel,
                Href = urlPayment
            };
            return model;
        }
    }
}
