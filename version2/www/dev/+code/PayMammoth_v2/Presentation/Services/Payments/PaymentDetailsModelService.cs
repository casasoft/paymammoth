﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v5.Presentation.Services.Address;
using BusinessLogic_CS_v5.Presentation.Services.Contact;
using BusinessLogic_CS_v5.Presentation.Services.ContentTexts;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Models._Shared.PartialViews.Payments;
using Raven.Client;

namespace PayMammoth_v2.Presentation.Services.Payments
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IPaymentDetailsModelService
    {
        _PaymentDetailsModel CreatePaymentDetailsModel(
            IDocumentSession documentSession,
            CultureModel culture,
            PaymentUserDetailsDataModel userDetails);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentDetailsModelService : IPaymentDetailsModelService
    {
        private readonly IContactDetailsModelService _contactDetailsModelService;
        private readonly IAddressDetailsModelService _addressDetailsModelService;
        private readonly IContentTextModelService _contentTextModelService;

        public PaymentDetailsModelService(
            IContactDetailsModelService contactDetailsModelService,
            IAddressDetailsModelService addressDetailsModelService,
            IContentTextModelService contentTextModelService)
        {
            _contentTextModelService = contentTextModelService;
            _addressDetailsModelService = addressDetailsModelService;
            _contactDetailsModelService = contactDetailsModelService;
        }

        public _PaymentDetailsModel CreatePaymentDetailsModel(
            IDocumentSession documentSession,
            CultureModel culture,
            PaymentUserDetailsDataModel userDetails)
        {
            return new _PaymentDetailsModel()
            {
                FullName = _contactDetailsModelService.GetFullNameFromContactDetails(userDetails.ContactDetailsModel),
                AddressAsOneLineHtml =
                    _addressDetailsModelService.GetAddressDetailsAsHtml(documentSession,
                        culture,
                        userDetails.ContactDetailsModel.Address),
                TitleContentText = _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                    documentSession,
                    culture,
                    PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PaymentDetails_Title),
                YourDetailsContentTextModel =
                    _contentTextModelService.GetContentTextModelForCultureByIdentifierOrCreateNew(
                        documentSession,
                        culture,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PaymentDetails_YourDetails),
                UserDetails = userDetails
            };
        }
    }
}
