﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.MediaItems.Services;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.ContactDetails;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Culture;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Currency;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Locations;
using BusinessLogic_CS_v5.Presentation.Services.Contact;
using BusinessLogic_CS_v5.Presentation.Services.Fields;
using BusinessLogic_CS_v5.Presentation.Services.MediaItems;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.FakePayments;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Models._Shared.Data.WebsiteAccounts;
using Raven.Client;

namespace PayMammoth_v2.Presentation.Services.Models
{
	using BusinessLogic_CS_v5.Aspects;
	using BusinessLogic_CS_v5.Aspects.NullAspects;
	using CS.General_CS_v5.Modules.InversionOfControl;

	public interface IFillPaymentRequestDataModelFromPaymentRequestDataService
	{
		PaymentRequestDataModel CreatePaymentRequestDataModelFromPaymentRequestData(
			PaymentRequestData paymentRequest,
			WebsiteAccountData websiteAccountData,
			CultureModel cultureModel,
			IDocumentSession session,
			[AllowNull] string fakePaymentKey);
	}

	[IocComponent]
	[LogAspect]
	[EnsureNonNullAspect]
	public class FillPaymentRequestDataModelFromPaymentRequestDataService : IFillPaymentRequestDataModelFromPaymentRequestDataService
	{
		private readonly IGetTotalPriceForRequestService _getTotalPriceForRequestService;
		private readonly IPaymentRequestDataService _paymentRequestDataService;
		private readonly IBaseObjectFieldModelService _baseObjectFieldModelService;
		private readonly IMediaItemModelService _mediaItemModelService;
		private readonly IMediaItemService _mediaItemService;
		private readonly ICheckIfFakePaymentEnabledAndKeyIsCorrectService _checkIfFakePaymentEnabledAndKeyIsCorrectService;

		public FillPaymentRequestDataModelFromPaymentRequestDataService(
			IGetTotalPriceForRequestService getTotalPriceForRequestService,
			IPaymentRequestDataService paymentRequestDataService,
			IBaseObjectFieldModelService baseObjectFieldModelService,
			IMediaItemModelService mediaItemModelService,
			IMediaItemService mediaItemService,
			ICheckIfFakePaymentEnabledAndKeyIsCorrectService checkIfFakePaymentEnabledAndKeyIsCorrectService)
		{
			_checkIfFakePaymentEnabledAndKeyIsCorrectService = checkIfFakePaymentEnabledAndKeyIsCorrectService;
			_mediaItemService = mediaItemService;
			_mediaItemModelService = mediaItemModelService;
			_baseObjectFieldModelService = baseObjectFieldModelService;
			_paymentRequestDataService = paymentRequestDataService;
			_getTotalPriceForRequestService = getTotalPriceForRequestService;
		}

		public PaymentRequestDataModel CreatePaymentRequestDataModelFromPaymentRequestData(
			PaymentRequestData paymentRequest,
			WebsiteAccountData websiteAccountData,
			CultureModel cultureModel,
			IDocumentSession session,
			[AllowNull] string fakePaymentKey)
		{
			var model = new PaymentRequestDataModel();
			model.OrderSummary = new PaymentOrderSummaryDataModel();
			model.OrderSummary.DescriptionHtml = "OrderSummary.DescriptionHtml";
			model.OrderSummary.FullDetailsUrl = paymentRequest.RequestDetails.OrderLinkOnClientWebsite;

			fillOrderSummaryItems(
				paymentRequest,
				model);
			fillOrderSummaryCurrency(
				paymentRequest,
				model);
			model.OrderSummary.Title = "OrderSummary.Title";

			var totalPriceIncTaxes = _getTotalPriceForRequestService.GetTotalPrice(
				paymentRequest,
				includeTaxes: true,
				includeShippingAndHandling: true);
			var totalPriceExcTaxes = _getTotalPriceForRequestService.GetTotalPrice(
				paymentRequest,
				includeTaxes: false,
				includeShippingAndHandling: true);
			var taxesOnly = totalPriceIncTaxes - totalPriceExcTaxes;

			model.OrderSummary.Total = _getTotalPriceForRequestService.GetTotalPrice(
				paymentRequest,
				true,
				includeShippingAndHandling: true);
			model.OrderSummary.TotalHandling = paymentRequest.RequestDetails.Pricing.HandlingAmount;
			model.OrderSummary.TotalNet = totalPriceExcTaxes;
			model.OrderSummary.TotalShipping = paymentRequest.RequestDetails.Pricing.ShippingAmount;
			model.OrderSummary.TotalTax = taxesOnly;

			fillPaymentMethods(
				paymentRequest,
				session,
				model,
				cultureModel);

			model.PaymentRequestId = paymentRequest.Id;
			fillUserDetails(
				cultureModel,
				model,
				paymentRequest);
			fillWebsiteAccount(
				model,
				paymentRequest,
				websiteAccountData);

			model.PaymentExpiryDate = paymentRequest.RequestDetails.ExpirationInfo.ExpiresAt;

			model.FakePaymentKey = fakePaymentKey;
			model.FakePaymentsEnabled = _checkIfFakePaymentEnabledAndKeyIsCorrectService.CheckIfFakePaymentEnabledAndKeyIsCorrect(
				websiteAccountData,
				fakePaymentKey);

			return model;
		}

		private void fillWebsiteAccount(
			PaymentRequestDataModel model,
			PaymentRequestData paymentRequest,
			WebsiteAccountData websiteAccountData)
		{
			model.WebsiteAccount = new WebsiteAccountDataModel();
			if (websiteAccountData.CustomCssFile != null && !websiteAccountData.CustomCssFile.IsEmpty())
			{
				model.WebsiteAccount.CssFile = _mediaItemService.GetWebPathForOriginalFile(websiteAccountData.CustomCssFile);
			}
			model.WebsiteAccount.Logo = _mediaItemModelService.ConvertMediaItemDataToMediaItemModel(
				websiteAccountData,
				websiteAccountData.Logo,
				x => x.Logo);
			model.WebsiteAccount.WebsiteName = websiteAccountData.WebsiteName;
		}

		private void fillUserDetails(
			CultureModel cultureModel,
			PaymentRequestDataModel model,
			PaymentRequestData paymentRequest)
		{
			model.UserDetails = new PaymentUserDetailsDataModel();
			fillUserDetails_ContactDetails(
				cultureModel,
				model,
				paymentRequest);
		}

		private void fillUserDetails_ContactDetails(
			CultureModel cultureModel,
			PaymentRequestDataModel model,
			PaymentRequestData paymentRequest)
		{
			//var clientContactDetails =paymentRequest.RequestDetails.Details.ClientContactDetails;

			var contactDetailsModel = new ContactDetailsModel();
			model.UserDetails.ContactDetailsModel = contactDetailsModel;

			contactDetailsModel.Address = new AddressDetailsModel();
			contactDetailsModel.Address.Address1 = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.Address1,
				paymentRequest.RequestDetails.Details.ClientContactDetails.Address1);

			contactDetailsModel.Address.Address2 = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.Address2,
				paymentRequest.RequestDetails.Details.ClientContactDetails.Address2);

			contactDetailsModel.Address.Country = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.CountryAsEnum,
				paymentRequest.RequestDetails.Details.ClientContactDetails.CountryAsEnum);

			contactDetailsModel.Address.Locality = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.Locality,
				paymentRequest.RequestDetails.Details.ClientContactDetails.Locality);

			//todo: [For: Karl | 2014/04/29] why exactly is this needed - LocalityDataModel? (WrittenBy: Karl)    
			//Alan - because we have 2 ways to set a locality
			// 1 - LinkedLocalityId
			// 2 - Hardcoded as string Locality;
			// If its 1) - then we need to load LinkedLocalityId into its model.
			contactDetailsModel.Address.LocalityDataModel = new LocalityDataModel();

			contactDetailsModel.Address.PostCode = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.PostCode,
				paymentRequest.RequestDetails.Details.ClientContactDetails.PostCode);

			contactDetailsModel.Address.State = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
				cultureModel,
				paymentRequest,
				x => paymentRequest.RequestDetails.Details.ClientContactDetails.State,
				paymentRequest.RequestDetails.Details.ClientContactDetails.State);
		}

		private void fillPaymentMethods(
			PaymentRequestData paymentRequest,
			IDocumentSession session,
			PaymentRequestDataModel model,
			CultureModel cultureModel)
		{
			model.PaymentMethodsAvailable = new List<PaymentMethodDataModel>();
			var availablePaymentMethods = _paymentRequestDataService.GetAllPaymentMethodsAvailableForPayment(
				paymentRequest,
				session);

			foreach (var paymentMethod in availablePaymentMethods)
			{
				var paymentMethodModel = new PaymentMethodDataModel();

				paymentMethodModel.Description = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
					cultureModel,
					paymentMethod,
					x => x.Description,
					paymentMethod.Description[cultureModel]);

				paymentMethodModel.Icon = _mediaItemModelService.ConvertMediaItemDataToMediaItemModel(
					paymentMethod,
					paymentMethod.Icon,
					x => x.Icon);

				paymentMethodModel.PaymentMethodType = paymentMethod.PaymentMethod;

				paymentMethodModel.Title = _baseObjectFieldModelService.CreateFieldModelForBaseObject(
					cultureModel,
					paymentMethod,
					x => x.Title,
					paymentMethod.Title[cultureModel]);

				model.PaymentMethodsAvailable.Add(paymentMethodModel);
			}
		}

		private static void fillOrderSummaryCurrency(
			PaymentRequestData paymentRequest,
			PaymentRequestDataModel model)
		{
			model.OrderSummary.OrderCurrency = new CurrencyDataModel();
			model.OrderSummary.OrderCurrency.Currency3LetterCode = paymentRequest.RequestDetails.Pricing.CurrencyCode3Letter;
			var currency = paymentRequest.RequestDetails.Pricing.GetCurrencyAsCurrencyCodeIso4217();
			if (currency == null)
				throw new InvalidOperationException("Currency cannot be null.  Code: " + paymentRequest.RequestDetails.Pricing.CurrencyCode3Letter);
			model.OrderSummary.OrderCurrency.CurrencyCode = currency.Value;
			model.OrderSummary.OrderCurrency.CurrencySymbol = CS.General_CS_v5.Enums.GetCurrencySymbol(model.OrderSummary.OrderCurrency.CurrencyCode);
		}

		private static void fillOrderSummaryItems(
			PaymentRequestData paymentRequest,
			PaymentRequestDataModel model)
		{
			model.OrderSummary.Items = new List<PaymentOrderSummaryItemDataModel>();
			if (paymentRequest.RequestDetails.ItemDetails != null)
			{
				foreach (var item in paymentRequest.RequestDetails.ItemDetails)
				{
					if (item != null)
					{
						var itemModel = new PaymentOrderSummaryItemDataModel();
						itemModel.DescriptionHtml = item.Description;
						itemModel.Title = item.Title;
						model.OrderSummary.Items.Add(itemModel);
					}
				}
			}
		}
	}
}