﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Services.HierarchyData;

namespace PayMammoth_v2.Presentation.Models.Home
{
    public class TestSearchModel
    {
        public List<int> Results { get; set; }
    }
}
