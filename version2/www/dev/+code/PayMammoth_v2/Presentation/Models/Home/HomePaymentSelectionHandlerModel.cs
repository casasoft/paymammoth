using System.Collections.Generic;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.MailingList;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;

namespace PayMammoth_v2.Presentation.Models.Home
{
    public class HomePaymentSelectionHandlerModel
    {
        public string RedirectUrl { get; set; }
    }
}