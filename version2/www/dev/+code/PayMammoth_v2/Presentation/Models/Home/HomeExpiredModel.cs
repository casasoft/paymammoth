using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.ContentTexts;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Fields;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.MailingList;

namespace PayMammoth_v2.Presentation.Models.Home
{
    public class HomeExpiredModel
    {
        public FieldModel<string> Title { get; set; }
        public ContentTextModel ExpiredContentText { get; set; }
    }
}