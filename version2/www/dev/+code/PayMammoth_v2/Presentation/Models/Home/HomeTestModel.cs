using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.ContentPage;
namespace PayMammoth_v2.Presentation.Models.Home
{
    public class HomeTestModel
    {
        public string Name { get; set; }

        public _ContentPageModel ContentPage { get; set; }

        public HomeTestModel()
        {
            
            
        }
    }
}