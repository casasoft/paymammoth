﻿using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.ContentTexts;

namespace PayMammoth_v2.Presentation.Models.PayPal
{
    public class PaypalConfirmationModel
    {
        public string ConfirmUrl { get; set; }
        public ContentTextModel TitleContentTextModel { get; set; }
        public ContentTextModel ConfirmTextModel { get; set; }
        public ContentTextModel ConfirmButtonTextModel { get; set; }


    }
}
