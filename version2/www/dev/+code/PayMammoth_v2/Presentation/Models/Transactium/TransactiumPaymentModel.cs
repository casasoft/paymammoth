﻿using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.ContentTexts;

namespace PayMammoth_v2.Presentation.Models.Transactium
{
    public class TransactiumPaymentModel
    {
        public ContentTextModel TitleContentTextModel { get; set; }
        public string RedirectUrl { get; set; }


    }
}
