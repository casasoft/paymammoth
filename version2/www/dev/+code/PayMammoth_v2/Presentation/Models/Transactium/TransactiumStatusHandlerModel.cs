using System.Collections.Generic;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.MailingList;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;

namespace PayMammoth_v2.Presentation.Models.Transactium
{
    public class TransactiumStatusHandlerModel
    {
        public string RedirectUrl { get; set; }
    }
}