﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.ContentTexts;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;

namespace PayMammoth_v2.Presentation.Models._Shared.PartialViews.Payments
{
    public class _PaymentDetailsModel
    {
        public ContentTextModel TitleContentText { get; set; }
        public ContentTextModel YourDetailsContentTextModel { get; set; }
        public PaymentUserDetailsDataModel UserDetails { get; set; }
        public string AddressAsOneLineHtml { get; set; }
        public string FullName { get; set; }
    }
}
