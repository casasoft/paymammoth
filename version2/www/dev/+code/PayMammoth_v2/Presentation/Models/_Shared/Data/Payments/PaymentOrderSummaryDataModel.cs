﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Currency;

namespace PayMammoth_v2.Presentation.Models._Shared.Data.Payments
{
    public class PaymentOrderSummaryDataModel
    {
        public CurrencyDataModel OrderCurrency { get; set; }

        /// <summary>
        /// Title of order.  If you leave empty, this will show 'Your Order Summary'
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Description of order.  Not required
        /// </summary>
        public string DescriptionHtml { get; set; }
        /// <summary>
        /// URL of original page of items (on client website)
        /// </summary>
        public string FullDetailsUrl { get; set; }

        public List<PaymentOrderSummaryItemDataModel> Items { get; set; }

        public decimal TotalNet { get; set; }
        /// <summary>
        /// Optional
        /// </summary>
        public decimal? TotalTax { get; set; }
        /// <summary>
        /// Optional
        /// </summary>
        public decimal? TotalShipping { get; set; }
        /// <summary>
        /// Optional
        /// </summary>
        public decimal? TotalHandling { get; set; }
        public decimal Total { get; set; }

    }
}
