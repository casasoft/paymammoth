﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.ContactDetails;

namespace PayMammoth_v2.Presentation.Models._Shared.Data.Payments
{
    public class PaymentUserDetailsDataModel
    {
        public ContactDetailsModel ContactDetailsModel { get; set; }

    }
}
