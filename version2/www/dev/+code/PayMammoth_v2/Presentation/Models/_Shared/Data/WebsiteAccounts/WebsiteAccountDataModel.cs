﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.MediaItems;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;

namespace PayMammoth_v2.Presentation.Models._Shared.Data.WebsiteAccounts
{
    public class WebsiteAccountDataModel
    {
        public MediaItemModel<WebsiteAccountData.WebsiteAccountDataLogoImageSize> Logo { get; set; }
        public string CssFile { get; set; }
        public string WebsiteName { get; set; }
    }
}
