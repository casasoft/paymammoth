﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Fields;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.MediaItems;
using PayMammoth_v2.Modules.Data.Payments;

namespace PayMammoth_v2.Presentation.Models._Shared.Data.Payments
{
    public class PaymentMethodDataModel
    {


        public FieldModel<string> Title { get; set; }
        public FieldModel<string> Description { get; set; }

        public PayMammoth_v2.Connector.Enums.PaymentMethodType PaymentMethodType { get; set; }

        public MediaItemModel<PaymentMethodData.PaymentMethodDataIconImageSize> Icon { get; set; }

    }
}
