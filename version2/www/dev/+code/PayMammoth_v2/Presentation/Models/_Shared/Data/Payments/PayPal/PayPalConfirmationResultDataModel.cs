﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Presentation.Models._Shared.Data.Payments.PayPal
{
    public class PayPalConfirmationResultDataModel
    {
        public string RedirectUrl { get; set; }
    }
}
