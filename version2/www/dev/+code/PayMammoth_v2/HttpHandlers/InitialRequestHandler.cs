﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using CS.General_CS_v5.Modules.Urls;
using NLog;
using PayMammoth_v2.Modules.Services.InitialRequests;

namespace PayMammoth_v2.HttpHandlers
{
    public class InitialRequestHandler : IHttpHandler
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IInitialRequestRawMessageParserService _initialRequestRawMessageParserService;

        public bool IsReusable
        {
            get { return true; }
        }

        public InitialRequestHandler()
        {
            _initialRequestRawMessageParserService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IInitialRequestRawMessageParserService>();
        }

        public void ProcessRequest(HttpContext context)
        {
            
            string data = CS.General_CS_v5.Util.StreamUtil.ReadStreamAsStringToEnd(context.Request.InputStream, Encoding.Default);
            _log.Trace("New Initial Request. Data: {{0}}", data);
            //test this out
            //cont
            
            var result = _initialRequestRawMessageParserService.ParseRawMessage(data);

            
            string resultJson = CS.General_CS_v5.Util.JsonUtil.Serialize(result);
            
            context.Response.Write(resultJson);
            context.Response.Flush();
            context.Response.End();

            
        }
    }
}
