﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Services.Cultures;
using CS.General_CS_v5.Modules.Urls;
using PayMammoth_v2.Presentation.Services.Payments;

namespace PayMammoth_v2
{
    public static class PayMammothUrls
    {
        //public static string Url_Payment_Choice { get { return CS.General_CS_v5.Util.PageUtil.GetApplicationBaseUrl() + "payment/"; } }
        //[Obsolete]
        //public static string GetPaymentChoiceUrl(string identifier)
        //{
        //    var ravenDbService= BusinessLogic_CS_v5.Util.InversionUtil.Get<ICultureModelService>();
        //    var cultureModelService= BusinessLogic_CS_v5.Util.InversionUtil.Get<ICultureModelService>();


        //    var urlService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPaymentUrlModelService>();
        //    urlService .GetPaymentSelectionPage(

        //    var urlClass = new UrlClass(Url_Payment_Choice);
        //    urlClass[PayMammoth_v2.Connector.Constants.PARAM_IDENTIFIER] = identifier;
        //    return urlClass.ToString();
        //}

        //public static string GetSuccessUrl()
        //{
        //    var urlClass = new UrlClass(Url_Payment_Choice);
            
        //    urlClass[Constants.PARAM_SUCCESS] = "yes";
        //    return urlClass.ToString();

        //}
        //public static string GetFailureUrl()
        //{
        //    var urlClass = new UrlClass(Url_Payment_Choice);
        //    urlClass[Constants.PARAM_SUCCESS] = "no";
        //    return urlClass.ToString();

        //}
        public static string Url_Test_PaymentSuccess { get { return CS.General_CS_v5.Util.PageUtil.GetApplicationBaseUrl() + "testing/testpayment-success.aspx"; } }
        public static string Url_Test_PaymentFailure { get { return CS.General_CS_v5.Util.PageUtil.GetApplicationBaseUrl() + "testing/testpayment-failure.aspx"; } }
        public static string Url_Test_OrderLinkOnClient { get { return CS.General_CS_v5.Util.PageUtil.GetApplicationBaseUrl() + "testing/test-orderlinkonclient.aspx"; } }
        // public static string Url_Payment_Cancelled { get { return CS.General_v3.Util.PageUtil.GetBaseURL() + "payment/?cancelled=true"; } }
        public static string Url_Payment_Cheque_Info { get { return CS.General_CS_v5.Util.PageUtil.GetApplicationBaseUrl() + "payment/cheque/"; } }
    }
}
