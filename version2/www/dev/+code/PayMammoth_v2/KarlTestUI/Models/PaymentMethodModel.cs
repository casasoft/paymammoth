﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.KarlTestUI.Models
{
    public class PaymentMethodModel_Karl
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}
