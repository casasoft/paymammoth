﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.KarlTestUI.Models
{
    public class LayoutModel
    {
        public string PageTitle { get; set; }
        public string ErrorMessage { get; set; }
    }
}
