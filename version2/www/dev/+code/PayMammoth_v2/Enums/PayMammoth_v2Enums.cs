﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Modules.Data.ContentTexts;
using BusinessLogic_CS_v5.Modules.Data.Emails.EmailTemplates;
using BusinessLogic_CS_v5.Modules.Data.Sections;
using BusinessLogic_CS_v5.Modules.Data.Settings;
using CS.General_CS_v5.Modules.Attributes;

namespace PayMammoth_v2.Enums
{
	public class PayMammoth_v2Enums
	{
	    public enum GetAllDatabaseObjectsForDatabaseTypeWithPagingSettings
	    {
            [SettingDataDefaultValues(Value = 1024)]
            CheckForExpiredPaymentRequestsAndMarkedThemAsExpired_LoadInBatchesOf,
	    }

	    public enum BackgroundTasksSettings
	    {
	        [SettingDataDefaultValues(Value = 300)]
            CheckForExpiredPaymentRequestsAndMarkedThemAsExpiredTask_RecurringIntervalInSeconds,
            
	    }

	    public enum RedirectionResultStatus
		{
			Success,
            [ContentTextDataDefaultValues("An error has been encountered while processing your request.  Please try again later or contact us if problem persists.")]
			Error
		}
		public enum RecurringProfileStatus
		{
			Pending,
			Active,
			/// <summary>
			/// Expired is when the recurring profile has reached its 'end of life', i.e example it was for 1 year and it hasnow elapsed
			/// </summary>
			Expired,
			/// <summary>
			/// Suspended is when the payment could not be taken, and profile is suspended
			/// </summary>
			Suspended,
			CouldNotCreate,
			/// <summary>
			/// Cancelled is when user manually cancels it
			/// </summary>
			Cancelled
		}
		public enum RecurringProfileTransactionStatus
		{
			Success,
			Failure
		}


        public enum ContentText
        {
            [ContentTextDataDefaultValues("Testing Title")]
            Sample,

            [ContentTextDataDefaultValues(
                "Invalid transaction request.  Please try again later and if problem persists, contact us.")]
            InvalidRequestIdentifier,

            [ContentTextDataDefaultValues("Your Order Summary")]
            OrderSummary_Title,

            [ContentTextDataDefaultValues("Net Total:")]
            OrderSummary_TotalNet,

            [ContentTextDataDefaultValues("view full order details")]
            OrderSummary_ViewFullOrderDetails,

            [ContentTextDataDefaultValues("Total:")]
            OrderSummary_Total,

            [ContentTextDataDefaultValues("Handling Total:")]
            OrderSummary_TotalHandling,

            [ContentTextDataDefaultValues("Shipping Total:")]
            OrderSummary_TotalShipping,

            [ContentTextDataDefaultValues("Tax Total:")]
            OrderSummary_TotalTax,

            [ContentTextDataDefaultValues("Payment Details")]
            PaymentDetails_Title,
            PaymentDetails_YourDetails,

            [ContentTextDataDefaultValues("Cancel transaction and go back to $Model$")]
            Payment_CancelAndGoBackToWebsite,

            [ContentTextDataDefaultValues("PayPal Confirmation")]
            PayPalConfirmation_Title,

            [ContentTextDataDefaultValues("<p>Please confirm your order below:</p>",
                Type = CS.General_CS_v5.Enums.ContentTextType.Html)]
            PayPalConfirmation_DescriptionHtml,

            [ContentTextDataDefaultValues("Confirm & Place Order")]
            PayPalConfirmation_ConfirmButton,

            [ContentTextDataDefaultValues("Card Details")]
            Transactium_PaymentDetailsTitle,

            [ContentTextDataDefaultValues(
                "Warning: By proceeding, you will be doing a fake payment using the payment method selected - No real money will be taken."
                )]
            Messages_FakePaymentEnabled,


            [ContentTextDataDefaultValues(@"<p>Your request has expired due to inactivity.</p>
<p>Please <a href='$Model$'>restart the process</a> and if your have any problems, kindly contact us.</p>",
                Type = CS.General_CS_v5.Enums.ContentTextType.Html,
                ModelType = typeof(string) //Cancel Url
                )]
            Payment_Expired,
            [ContentTextDataDefaultValues(
@"Kindly finalise payment before $Model$ as else transaction will expire and be cancelled.  In case you have any problems, please contact us.",
//Date
ModelType = typeof(string) )]
            Payment_HasExpiryDate
        }

	    public enum PayMammothSettings
		{
			[SettingDataDefaultValues(Value = 5)]
			BackgroundTasks_NotificationsSender_IntervalInSeconds,
			[SettingDataDefaultValues(Value = 1000)]
			Notifications_BatchSizeToSend,
            [SettingDataDefaultValues(Value = "Endpoint=sb://paymammoth.servicebus.windows.net/;SharedSecretIssuer=owner;SharedSecretValue=LDUEFXRXzRccQiAOatYeK9Lkfa7qwX0Sn/33e0hiWNQ=")]
            Notifications_AzureQueue_ConnectionString,
            [SettingDataDefaultValues(Value = "paymammoth-notifications")]
            Notifications_AzureQueue_QueueName,

            [SettingDataDefaultValues(Value = 30000, Description = "Request timeout, in milleseconds")]
			PayPal_v1_RequestTimeout,
            [SettingDataDefaultValues(Value = 5, Description = "Request retries")]
            PayPal_v1_RequestRetries,

			[SettingDataDefaultValues(Value = "https://api-3t.sandbox.paypal.com/nvp")]
			PayPal_v1_Live_NvpServerUrl,
			[SettingDataDefaultValues(Value = "https://www.paypal.com/cgi-bin/webscr")]
			PayPal_v1_Live_ApiServerUrl,
			[SettingDataDefaultValues(Value = "https://api-3t.sandbox.paypal.com/nvp")]
			PayPal_v1_Sandbox_NvpServerUrl,
			[SettingDataDefaultValues(Value = "https://www.sandbox.paypal.com/cgi-bin/webscr")]
			PayPal_v1_Sandbox_ApiServerUrl,
			[SettingDataDefaultValues(Value = 1.05, Description = "This is used to set the maximum amount allowed for a recurring payment. Percentage should be in the form of 1.05 to signify five percent and NOT 5%!")]
			RecurringPayments_MaxAmountExtraPercentage,
			[SettingDataDefaultValues(Value = 30000, Description = "Timeout to wait for mark as paid, in milleseconds.")]
			Other_Timeouts_MarkPaid
		}

	    public enum Section
	    {
	        //[SectionDataDefaultValues(
	        //    Title = "Sample Page",
	        //    RouteUrl = "/members/area/events/",
	        //    RouteController = "Events",
	        //    RouteAction = "Index",
	        //    RouteArea = BusinessLogic_CS_v5.Constants.Areas.Members)] Sample,


	        [SectionDataDefaultValues(
	            Title = "Payment Selection")]
	        [SectionDataRoutingInfo(
	            RouteController = "Home",
	            RouteAction = "PaymentSelection",
	            RouteUrl = "/payment/")]
	        Payments_Selection,

	        [SectionDataDefaultValues(
	            Title = "Payment Selection Method Handler")]
	        [SectionDataRoutingInfo(
	            RouteController = "Home",
	            RouteAction = "PaymentSelectionHandler",
	            RouteUrl = "/payment/method/")]
	        Payments_Selection_Handler,


	        [SectionDataDefaultValues(
	            Title = "Payment - PayPal Confirmation")]
	        [SectionDataRoutingInfo(
	            RouteController = "PayPal",
	            RouteAction = "PaymentConfirmation",
	            RouteUrl = "/payment/paypal/confirmation")]
	        Payments_Payment_PaypalConfirmation,

	        [SectionDataDefaultValues(
	            Title = "Payment - Transactium")]
	        [SectionDataRoutingInfo(
	            RouteController = "Transactium",
	            RouteAction = "Payment",
	            RouteUrl = "/payment/transactium/")]
	        Payments_Payment_Transactium,

	        [SectionDataDefaultValues(
	            Title = "Payment - Transactium")]
	        [SectionDataRoutingInfo(
	            RouteController = "Transactium",
	            RouteAction = "Status",
	            RouteUrl = "/payment/transactium/status/")]
	        Payments_Payment_Transactium_Status,

	        [SectionDataDefaultValues(
	            Name = "Payment - Expired",
	            Title = "Transaction Expired",
	            PageContent = @"<p>Your request has expired due to inactivity.</p>
<p>Please <a href='$Model$'>restart the process</a> and if your have any problems, kindly contact us.</p>")]
	        [SectionDataRoutingInfo(
	            RouteController = "Home",
	            RouteAction = "Expired",
	            RouteUrl = "/payment/expired/")]
	        Payments_Expired,

	        [SectionDataDefaultValues(
	            Title = "Payment - Cancel Payment")]
	        [SectionDataRoutingInfo(
	            RouteController = "Home",
	            RouteAction = "CancelPayment",
	            RouteUrl = "/payment/cancel/")]
	        Payments_CancelPayment
	    }

	    public enum EmailTemplate
		{

			[EmailTemplateDefaultValues(
				Name = "New Review Added",
				Subject = "A New Review Has Been Added to your Tourism Product: @Model.TourismProductTitle",
				WhenIsThisEmailSent = "This email is sent to the owner of the TourismProduct indicating that a new review has been added to it.",
				Content =
					@"<p>Dear @Model.MemberDetails.Name,</p>
		<p>A new review has been added to your Tourism Product (@Model.TourismProductTitle).</p>
		<p>Review Details:</p>
		<ul>
			<li>Review Title : @Model.TourismProductReview.Title</li>
			<li>Review Description: @Model.TourismProductReview.Description</li>
			<li>Review Rating : @Model.TourismProductReview.Rating</li>
			<li>Review Date : @Model.TourismProductReview.ReviewDate</li>
			<li>Reviewer: @Model.MemberDetails.Name </li>
		</ul>
		<p>You can view your TourismProduct by clicking <a href='@Model.TourismProductUrl'>here</a>.</p>")] 
			Sample,
		}
	}
}
