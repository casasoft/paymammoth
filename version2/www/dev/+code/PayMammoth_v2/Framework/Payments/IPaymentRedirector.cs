﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using Raven.Client;

namespace PayMammoth_v2.Framework.Payments
{
    public interface IPaymentRedirector
    {
        RedirectionResult Redirect(ReferenceLink<PaymentRequestData> paymentRequestId, IDocumentSession session);
    }
}
