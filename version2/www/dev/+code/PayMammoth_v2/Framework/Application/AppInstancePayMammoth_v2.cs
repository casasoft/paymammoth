﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Modules.Services.ContentTexts;
using BusinessLogic_CS_v5.Modules.Services.Emails.EmailTemplates;
using BusinessLogic_CS_v5.Modules.Services.Sections;
using PayMammoth_v2.Modules.Services.Notifications;

namespace PayMammoth_v2.Framework.Application
{
    public class AppInstancePayMammoth_v2 : CS.MvcCmsGeneral_CS_v5.Code.Framework.Application.AppInstanceMvcCmsGeneral
    {
        public new static AppInstancePayMammoth_v2 Instance
        {
            get { return (AppInstancePayMammoth_v2)CS.General_CS_v5.Modules.Application.AppInstanceGeneral.Instance; }
            
        }
        
        protected override void addAssemblies()
        {
            this.ProjectAssemblies.Add(typeof(PayMammoth_v2.Framework.Application.AppInstancePayMammoth_v2).Assembly);
            this.ProjectAssemblies.Add(typeof(PayMammoth_v2.Connector.Services.PayMammothConnectorInitialRequestService).Assembly);
            base.addAssemblies();
        }
        
       
        protected override void onApplicationStart()
        {
            base.onApplicationStart();
         
        }

        protected override void onPostApplicationStart()
        {
            base.onPostApplicationStart();
            //now it is being automatically initailsied
            //BusinessLogic_CS_v5.Util.InversionUtil.Get<INotificationsAzureQueueManager>().Initialise();

        }

        protected override void checkAndCreateData()
        {
            base.checkAndCreateData();
            ContentTextService.Instance.CreateAllContentTextsDefinedbyEnum(typeof(PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText));
            EmailTemplateService.Instance.CreateAllEmailTemplatesForEnum(typeof(PayMammoth_v2.Enums.PayMammoth_v2Enums.EmailTemplate));

        }
        public override List<Type> GetAllRouteEnumTypes()
        {
            var types = base.GetAllRouteEnumTypes();
            types.Add(typeof(PayMammoth_v2.Enums.PayMammoth_v2Enums.Section));
            return types;
        }
        static AppInstancePayMammoth_v2()
        {
            //Instance = new AppInstancePayMammoth_v2();
        }

        protected override void mapCustomRoutes()
        {
            //Map any custom routes
            //e.g.
            /*sectionRoutingDictionaryModelService.SetSectionDataRoutingInfoInStaticDictionary(
             BusinessLogic_CS_v5.EnumsBL.SectionEnums.Section.Members, 
              new SectionDataRoutingInfoAttribute()
            {
                RouteUrl = "/members/mark/test",
                RouteController = "CustomMembers",
                RouteAction = "Testing"
            });*/
            base.mapCustomRoutes();
        }
        public override string GetDeployApplicationName()
        {
            return "PayMammoth_v2Deploy";
        }
        



    }
}
