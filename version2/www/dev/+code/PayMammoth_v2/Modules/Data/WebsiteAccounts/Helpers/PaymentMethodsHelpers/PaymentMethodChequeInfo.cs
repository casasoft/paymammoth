﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Modules.Data.WebsiteAccounts.Helpers.PaymentMethodsHelpers
{
    public class PaymentMethodChequeInfo
    {
        public PaymentMethodChequeAddressInfo Address { get; set; }
        public bool Enabled { get; set; }
        public string PayableTo { get; set; }

    }
}
