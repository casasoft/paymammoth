﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.Cms.DefaultMetadataValues;
using BusinessLogic_CS_v5.Framework.DbObjects.Objects;
using BusinessLogic_CS_v5.Framework.RavenDb.Indexes;
using BusinessLogic_CS_v5.Util;
using PayMammoth_v2.Modules.Data.WebsiteAccounts.Helpers;
using BusinessLogic_CS_v5.Framework.MediaItems;
using BusinessLogic_CS_v5.Framework.MediaItems.Attributes;

namespace PayMammoth_v2.Modules.Data.WebsiteAccounts
{

    public class WebsiteAccountData : BaseObject
    {
        public WebsiteAccountData()
        {
            this.Id = RavenDbUtil.GetDocumentIdForType<WebsiteAccountData>(CS.General_CS_v5.Util.RandomUtil.GetGuid(removeDashes: true));
            
            this.ContactInfo = new WebsiteAccountContactDetailsInfo();
            this.Notifications = new WebsiteAccountNotificationsInfo();
            
        }



        public string AllowedIps { get; set; }
        //public string AccountCode { get; set; }

        private WebsiteAccountContactDetailsInfo _m_WebsiteAccountContactDetailsInfo;

        public WebsiteAccountContactDetailsInfo ContactInfo
        {
            get
            {
                if (_m_WebsiteAccountContactDetailsInfo == null)
                {
                    _m_WebsiteAccountContactDetailsInfo = new WebsiteAccountContactDetailsInfo();
                }
                return _m_WebsiteAccountContactDetailsInfo;
            }
            set { _m_WebsiteAccountContactDetailsInfo = value; }
        }


        private WebsiteAccountPaymentMethodsInfo _m_WebsiteAccountPaymentMethodsInfo;

        public WebsiteAccountPaymentMethodsInfo PaymentsInformation
        {
            get
            {
                if (_m_WebsiteAccountPaymentMethodsInfo == null)
                {
                    _m_WebsiteAccountPaymentMethodsInfo = new WebsiteAccountPaymentMethodsInfo();
                }
                return _m_WebsiteAccountPaymentMethodsInfo;
            }
            set { _m_WebsiteAccountPaymentMethodsInfo = value; }
        }


        public string SecretWord { get; set; }

        #region WebsiteAccountData.CustomCssFile (Media Item)


        public enum WebsiteAccountDataCustomCssFileImageSize
        {
            
            [SpecificSizeDefaults(Width = 640, Height = 480,
                CropIdentifier = CS.General_CS_v5.Enums.ImageCropIdentifier.MiddleMiddle,
                ImageFormat = CS.General_CS_v5.Enums.ImageType.JPEG)]
            Normal

        }

        public class WebsiteAccountDataCustomCssFile : MediaItem<WebsiteAccountDataCustomCssFileImageSize>
        {
            public WebsiteAccountDataCustomCssFile() : base()
            {
                var p = this.Parameters;

                p.Identifier = "WebsiteAccountData.CustomCssFile";
                p.UploadFolder = "/WebsiteAccountData/CustomCssFile/";
                    // "/" means relative to the base folder, which by default is /uploads/.  Must end with a slash

                p.ImageDefaults.MaximumWidth = 2048;
                p.ImageDefaults.MaximumHeight = 2048;

            }
        }

        public WebsiteAccountDataCustomCssFile CustomCssFile { get; set; }

        private WebsiteAccountNotificationsInfo _m_WebsiteAccountNotificationsInfo;

        public WebsiteAccountNotificationsInfo Notifications
        {
            get
            {
                if (_m_WebsiteAccountNotificationsInfo == null)
                {
                    _m_WebsiteAccountNotificationsInfo = new WebsiteAccountNotificationsInfo();
                }
                return _m_WebsiteAccountNotificationsInfo;
            }
            set { _m_WebsiteAccountNotificationsInfo = value; }
        }
        #endregion
       

        public bool WaitUntilPaymentRequestAcknowledgedBeforeRedirectToSuccessPage { get; set; }
        public string WebsiteName { get; set; }

        private WebsiteAccountFakePaymentsInfo _m_WebsiteAccountFakePaymentsInfo;

        public WebsiteAccountFakePaymentsInfo FakePayments
        {
            get
            {
                if (_m_WebsiteAccountFakePaymentsInfo == null)
                {
                    _m_WebsiteAccountFakePaymentsInfo = new WebsiteAccountFakePaymentsInfo();
                }
                return _m_WebsiteAccountFakePaymentsInfo;
            }
            set { _m_WebsiteAccountFakePaymentsInfo = value; }
        }
        

        #region WebsiteAccountData.Logo (Media Item)


        public enum WebsiteAccountDataLogoImageSize
        {
            

            [SpecificSizeDefaults(Width = 240, Height = 120,
                CropIdentifier = CS.General_CS_v5.Enums.ImageCropIdentifier.NoCrop,
                ImageFormat = CS.General_CS_v5.Enums.ImageType.PNG)]
            Normal

        }

        public class WebsiteAccountDataLogo : MediaItem<WebsiteAccountDataLogoImageSize>
        {
            public WebsiteAccountDataLogo() : base()
            {
                var p = this.Parameters;

                p.Identifier = "WebsiteAccountData.Logo";
                p.UploadFolder = "/WebsiteAccountData/Logo/";
                    // "/" means relative to the base folder, which by default is /uploads/.  Must end with a slash

                p.ImageDefaults.MaximumWidth = 2048;
                p.ImageDefaults.MaximumHeight = 2048;

            }
        }

        public WebsiteAccountDataLogo Logo { get; set; }

        #endregion

      


    }


    [CmsSectionDefaults(ShowInMainMenu = true)]
    public class WebsiteAccountDataIndex : DocumentIndexCreationTask<WebsiteAccountData>
    {
        public WebsiteAccountDataIndex()
        {

        }

        public override string IndexName
        {
            get
            {
                return typeof (WebsiteAccountDataIndex).Name;
            }
        }

        protected override void createIndex()
        {
            // index definition goes here


            this.Map = (items => items.Select(item => new
            {
                //index fields go here
                CommonMetaData_CreatedOn = item.CommonMetaData.CreatedOn,
                CommonMetaData_Published = item.CommonMetaData.Published,
                CommonMetaData_PublishedOn = item.CommonMetaData.PublishedOn,
                CommonMetaData_PublishedUntil = item.CommonMetaData.PublishedUntil,
                CommonMetaData_Deleted = item.CommonMetaData.Deleted,
                CommonMetaData_SortPriority = item.CommonMetaData.SortPriority,
                CommonMetaData_IsTemporary = item.CommonMetaData.IsTemporary,
                CommonMetaData_LastEditedOn = item.CommonMetaData.LastEditedOn


            }));

        }

    }
}
