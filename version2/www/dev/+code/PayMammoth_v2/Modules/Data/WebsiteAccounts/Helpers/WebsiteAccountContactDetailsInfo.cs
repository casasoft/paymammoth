﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Modules.Data.WebsiteAccounts.Helpers
{
    public class WebsiteAccountContactDetailsInfo
    {
        public string ContactEmail { get; set; }
        public string ContactName { get; set; }
    }
}
