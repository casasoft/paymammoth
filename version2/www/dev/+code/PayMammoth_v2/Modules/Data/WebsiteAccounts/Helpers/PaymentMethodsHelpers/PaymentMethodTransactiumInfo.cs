﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Modules.Data.WebsiteAccounts.Helpers.PaymentMethodsHelpers
{
    public class PaymentMethodTransactiumInfo
    {
        public PaymentMethodTransactiumInfo()
        {
            this.LiveAccount = new TransactiumAccountInfo();
            this.StagingAccount = new TransactiumAccountInfo();
        }

        public class TransactiumAccountInfo
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public bool Enabled { get; set; }
        public TransactiumAccountInfo LiveAccount { get; set; }
        public TransactiumAccountInfo StagingAccount { get; set; }
        public string ProfileTag { get; set; }
        public bool UseLiveEnvironment { get; set; }

    }
}
