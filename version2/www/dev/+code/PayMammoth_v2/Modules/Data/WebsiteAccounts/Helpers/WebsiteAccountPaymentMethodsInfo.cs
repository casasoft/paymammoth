﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v2.Modules.Data.WebsiteAccounts.Helpers.PaymentMethodsHelpers;

namespace PayMammoth_v2.Modules.Data.WebsiteAccounts.Helpers
{
    public class WebsiteAccountPaymentMethodsInfo
    {
        private PaymentMethodChequeInfo _m_PaymentMethodChequeInfo;

        public PaymentMethodChequeInfo Cheque
        {
            get
            {
                if (_m_PaymentMethodChequeInfo == null)
                {
                    _m_PaymentMethodChequeInfo = new PaymentMethodChequeInfo();
                }
                return _m_PaymentMethodChequeInfo;
            }
            set { _m_PaymentMethodChequeInfo = value; }
        }
        private PaymentMethodApcoInfo _m_PaymentMethodApcoInfo;

        public PaymentMethodApcoInfo apco
        {
            get
            {
                if (_m_PaymentMethodApcoInfo == null)
                {
                    _m_PaymentMethodApcoInfo = new PaymentMethodApcoInfo();
                }
                return _m_PaymentMethodApcoInfo;
            }
            set { _m_PaymentMethodApcoInfo = value; }
        }
        private PaymentMethodRealexInfo _m_PaymentMethodRealexInfo;

        public PaymentMethodRealexInfo Realex
        {
            get
            {
                if (_m_PaymentMethodRealexInfo == null)
                {
                    _m_PaymentMethodRealexInfo = new PaymentMethodRealexInfo();
                }
                return _m_PaymentMethodRealexInfo;
            }
            set { _m_PaymentMethodRealexInfo = value; }
        }
        private PaymentMethodTransactiumInfo _m_PaymentMethodTransactiumInfo;

        public PaymentMethodTransactiumInfo Transactium
        {
            get
            {
                if (_m_PaymentMethodTransactiumInfo == null)
                {
                    _m_PaymentMethodTransactiumInfo = new PaymentMethodTransactiumInfo();
                }
                return _m_PaymentMethodTransactiumInfo;
            }
            set { _m_PaymentMethodTransactiumInfo = value; }
        }
        private PaymentMethodPayPalInfo _m_PayPal;

        public PaymentMethodPayPalInfo PayPal
        {
            get
            {
                if (_m_PayPal == null)
                {
                    _m_PayPal = new PaymentMethodPayPalInfo();
                }
                return _m_PayPal;
            }
            set { _m_PayPal = value; }
        }

        

    }
}
