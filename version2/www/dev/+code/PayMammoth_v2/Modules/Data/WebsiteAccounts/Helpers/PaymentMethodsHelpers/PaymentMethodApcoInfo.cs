﻿using BusinessLogic_CS_v5.Framework.Multilingual;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Modules.Data.WebsiteAccounts.Helpers.PaymentMethodsHelpers
{
    public class PaymentMethodApcoInfo
    {
        public class ApcoProfile
        {
            public string AccountId { get; set; }
            public string SecretWord { get; set; }
        }

        public bool Enabled { get; set; }

        private MultilingualValue<string> _BankStatementText = new MultilingualValue<string>();

        public MultilingualValue<string> BankStatementText
        {
            get { return _BankStatementText; }
            set { _BankStatementText = value; }
        }

        private MultilingualValue<string> _Description = new MultilingualValue<string>();

        public MultilingualValue<string> Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public ApcoProfile LiveProfile { get; set; }
        public ApcoProfile StagingProfile { get; set; }

        public bool UseLiveEnvironment { get; set; }

    }
}
