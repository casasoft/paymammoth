﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.Multilingual;

namespace PayMammoth_v2.Modules.Data.WebsiteAccounts.Helpers.PaymentMethodsHelpers
{
    public class PaymentMethodRealexInfo
    {
        public class RealexAccount
        {
            public string AccountName { get; set; }
        }

        private MultilingualValue<string> _Description = new MultilingualValue<string>();

        public MultilingualValue<string> Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public bool Enabled { get; set; }
        public RealexAccount LiveAccount { get; set; }
        public RealexAccount StagingAccount { get; set; }
        private MultilingualValue<string> _BankStatementText = new MultilingualValue<string>();

        public MultilingualValue<string> BankStatementText
        {
            get { return _BankStatementText; }
            set { _BankStatementText = value; }
        }

        public string MerchantId { get; set; }
        public string SecretWord { get; set; }

    }
}
