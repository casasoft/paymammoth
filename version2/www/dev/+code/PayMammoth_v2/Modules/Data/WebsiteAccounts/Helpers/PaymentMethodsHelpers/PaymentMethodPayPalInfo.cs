﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Modules.Data.WebsiteAccounts.Helpers.PaymentMethodsHelpers
{
    public class PaymentMethodPayPalInfo
    {
        public class PayPalAccountInfo
        {
            public string MerchantEmail { get; set; }
            public string MerchantId { get; set; }
            public string Password { get; set; }
            public string Signature { get; set; }
            public string Username { get; set; }


        }

        public bool Enabled { get; set; }
        private PayPalAccountInfo _m_LiveAccount;

        public PayPalAccountInfo LiveAccount
        {
            get
            {
                if (_m_LiveAccount == null)
                {
                    _m_LiveAccount = new PayPalAccountInfo();
                }
                return _m_LiveAccount;
            }
            set { _m_LiveAccount = value; }
        }
        private PayPalAccountInfo _m_SandboxAccount;

        public PayPalAccountInfo SandboxAccount
        {
            get
            {
                if (_m_SandboxAccount == null)
                {
                    _m_SandboxAccount = new PayPalAccountInfo();
                }
                return _m_SandboxAccount;
            }
            set { _m_SandboxAccount = value; }
        }
        

        
        
        public bool UseLiveEnvironment { get; set; }
    }
}
