﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.Collections;

namespace PayMammoth_v2.Modules.Data.Notifications
{
    public class NotificationSendingInstanceInfo: SubCollectionItem
    {
        public enum NotificationSendingInstanceInfoStatus
        {
            Success,
            NoSendToUrl,
            NotificationIsNotPending,
            ErrorOccurred,
            IncorrectResponse
        }

        public DateTimeOffset DateTime { get; set; }
        public NotificationSendingInstanceInfoStatus Status { get; set; }
        public string Message { get; set; }
    }
}
