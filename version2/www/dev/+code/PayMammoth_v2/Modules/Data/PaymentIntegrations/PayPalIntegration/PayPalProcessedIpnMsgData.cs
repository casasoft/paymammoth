﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.Cms.DefaultMetadataValues;
using BusinessLogic_CS_v5.Framework.DbObjects.Objects;
using BusinessLogic_CS_v5.Framework.RavenDb.Indexes;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications;

namespace PayMammoth_v2.Modules.Data.PaymentIntegrations.PayPalIntegration
{

    public class PayPalProcessedIpnMsgData : BaseObject
    {
        public PayPalProcessedIpnMsgData()
        {

        }

        public IpnMessage IpnMessage { get; set; }
        public string TranscationId { get; set; }
        public PayPalEnums.PAYMENT_STATUS PaymentStatus { get; set; }

        public DateTimeOffset ProcessedOn { get; set; }

    }

    public class PayPalProcessedIpnMsgDataIndex : DocumentIndexCreationTask<PayPalProcessedIpnMsgData>
    {
        public PayPalProcessedIpnMsgDataIndex()
        {

        }

        public override string IndexName
        {
            get
            {
                return typeof(PayPalProcessedIpnMsgDataIndex).Name;
            }
        }

        protected override void createIndex()
        {
            // index definition goes here


            this.Map = (items => items.Select(item => new
            {
                item.TranscationId,
                item.PaymentStatus,

                //index fields go here
                CommonMetaData_CreatedOn = item.CommonMetaData.CreatedOn,
                CommonMetaData_Published = item.CommonMetaData.Published,
                CommonMetaData_PublishedOn = item.CommonMetaData.PublishedOn,
                CommonMetaData_PublishedUntil = item.CommonMetaData.PublishedUntil,
                CommonMetaData_Deleted = item.CommonMetaData.Deleted,
                CommonMetaData_SortPriority = item.CommonMetaData.SortPriority,
                CommonMetaData_IsTemporary = item.CommonMetaData.IsTemporary,
                CommonMetaData_LastEditedOn = item.CommonMetaData.LastEditedOn


            }));

        }

    }

}
