﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1;
using Raven.Client;
using Raven.Client.Linq;

namespace PayMammoth_v2.Modules.Data.PaymentIntegrations.PayPalIntegration
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IPayPalProcessedIpnMsgRepository
    {
        PayPalProcessedIpnMsgData GetIpnMsgByTransactionIdAndStatus(string transactionId, PayPalEnums.PAYMENT_STATUS paymentStatus, IDocumentSession session);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PayPalProcessedIpnMsgRepository : IPayPalProcessedIpnMsgRepository
    {
        public PayPalProcessedIpnMsgRepository()
        {

        }

        public PayPalProcessedIpnMsgData GetIpnMsgByTransactionIdAndStatus(string transactionId, 
            PayPalEnums.PAYMENT_STATUS paymentStatus, IDocumentSession session)
        {
            //todo: [For: Backend | 2014/03/28] create unit-tests (WrittenBy: Karl)        			
            IRavenQueryable<PayPalProcessedIpnMsgData> q = session.Query<PayPalProcessedIpnMsgData>(typeof(PayPalProcessedIpnMsgDataIndex).Name);
            q.Customize(x => x.WaitForNonStaleResultsAsOfNow());
            q = q.Where(x=>x.TranscationId == transactionId && x.PaymentStatus == paymentStatus);
            return q.ToList().FirstOrDefault();
            
        }
    }
}
