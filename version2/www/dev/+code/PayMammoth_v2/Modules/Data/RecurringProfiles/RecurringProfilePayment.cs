﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.Collections;

namespace PayMammoth_v2.Modules.Data.RecurringProfiles
{
    public class RecurringProfilePayment : SubCollectionItem
    {
        public int FailureCount { get; set; }
        public string Identifier { get; set; }
        public bool InitiatedByPayMammoth { get; set; }
        public DateTimeOffset? NextRetryOn { get; set; }
        public DateTimeOffset PaymentDueOn { get; set; }
        public Connector.Enums.RecurringProfilePaymentStatus Status { get; set; }
        

    }
}
