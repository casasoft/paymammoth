﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Modules.Data.Payments.Helpers
{
    public class PaymentNotificationsInfo
    {
        public bool NotificationsEnabled { get; set; }
        public string NotificationUrl { get; set; }
        
    }
}
