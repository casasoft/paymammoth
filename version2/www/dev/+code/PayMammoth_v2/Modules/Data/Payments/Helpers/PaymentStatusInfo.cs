﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.Cms.DefaultMetadataValues;
using BusinessLogic_CS_v5.Framework.DbObjects.References;

namespace PayMammoth_v2.Modules.Data.Payments.Helpers
{
    public class PaymentStatusInfo
    {

        public PaymentStatusInfo()
        {
            this.Status = PaymentStatus.Pending;
        }

        public enum PaymentStatus
        {
            Pending, Paid, Expired, Cancelled
        }

        [CmsFieldDefaults(ShowInListing = true, ListingSortPriority = 200,
               DefaultSortable = BusinessLogic_CS_v5.EnumsBL.CmsEnums_BL.CmsFieldDefaultSortable.DefaultSortableDescending)]
        public PaymentStatus Status { get; set; }

        [CmsFieldDefaults(ShowInListing = true, ListingSortPriority = 300,
            DefaultSortable = BusinessLogic_CS_v5.EnumsBL.CmsEnums_BL.CmsFieldDefaultSortable.DefaultSortableDescending)]
        public DateTimeOffset? PaidOn { get; set; }
        [CmsFieldDefaults(ShowInListing = true, ListingSortPriority = 300,
            DefaultSortable = BusinessLogic_CS_v5.EnumsBL.CmsEnums_BL.CmsFieldDefaultSortable.DefaultSortableDescending)]
        public DateTimeOffset? ExpiredOn { get; set; }
        [CmsFieldDefaults(ShowInListing = true, ListingSortPriority = 300,
            DefaultSortable = BusinessLogic_CS_v5.EnumsBL.CmsEnums_BL.CmsFieldDefaultSortable.DefaultSortableDescending)]
        public DateTimeOffset? CancelledOn { get; set; }

        public Connector.Enums.PaymentMethodType PaymentMethodUsed { get; set; }
        public ReferenceLink<PaymentRequestTransactionData> SuccessfulTransactionId { get; set; }

    }
}
