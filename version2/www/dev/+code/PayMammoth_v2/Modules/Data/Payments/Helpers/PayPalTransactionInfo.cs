﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Modules.Data.Payments.Helpers
{
    public class PayPalTransactionInfo
    {
        public string TokenId { get; set; }
        public string PayerId { get; set; }
    }
}
