﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Modules.Data._Shared
{
    public class PayMammothLog
    {
        public string LogData { get; set; }
        public override string ToString()
        {
            return this.LogData;
            
        }
    }
}
