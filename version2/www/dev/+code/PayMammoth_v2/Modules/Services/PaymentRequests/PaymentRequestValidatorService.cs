﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Util;
using CS.General_CS_v5.Extensions;
using CS.General_CS_v5.Modules.InversionOfControl;
using CS.General_CS_v5.Util;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using Raven.Client;


namespace PayMammoth_v2.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestValidatorService
    {
        Connector.Enums.InitialRequestResponseStatus ValidateInitialRequest(InitialRequestInfo initialRequestInfo);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentRequestValidatorService : IPaymentRequestValidatorService
    {
        public PaymentRequestValidatorService()
        {

        }

        public Connector.Enums.InitialRequestResponseStatus ValidateInitialRequest(InitialRequestInfo initialRequestInfo)
        {


            initialRequestInfo.MustNotBeNullable("InitialRequestInfo is required");
            //defensive checks done

            Connector.Enums.InitialRequestResponseStatus result = Connector.Enums.InitialRequestResponseStatus.Success;
            if (initialRequestInfo.ReturnUrls == null)
            {
                result = Connector.Enums.InitialRequestResponseStatus.ReturnUrlSuccessIsRequired;
            }
            else if (initialRequestInfo.ReturnUrls != null &&
                     string.IsNullOrWhiteSpace(initialRequestInfo.ReturnUrls.SuccessUrl))
            {
                result = Connector.Enums.InitialRequestResponseStatus.ReturnUrlSuccessIsRequired;
            }
            else if (initialRequestInfo.ReturnUrls != null &&
                     string.IsNullOrWhiteSpace(initialRequestInfo.ReturnUrls.FailureUrl))
            {
                result = Connector.Enums.InitialRequestResponseStatus.ReturnUrlFailureIsRequired;
            }
            else if (initialRequestInfo.NotificationUrl.IsNullOrWhiteSpace())
            {
                result = Connector.Enums.InitialRequestResponseStatus.NotificationUrlIsRequred;
            }
            else if (initialRequestInfo.ItemDetails == null || initialRequestInfo.ItemDetails.Count == 0)
            {
                result = Connector.Enums.InitialRequestResponseStatus.PleaseSpecifyAtLeastOneItem;
            }
            else if (string.IsNullOrWhiteSpace(initialRequestInfo.Pricing.CurrencyCode3Letter))
            {
                result = Connector.Enums.InitialRequestResponseStatus.CurrencyCodeNotFilledIn;
            }
            if (result == Connector.Enums.InitialRequestResponseStatus.Success &&
                !string.IsNullOrWhiteSpace(initialRequestInfo.Pricing.CurrencyCode3Letter))
            {
                var currency = CS.General_CS_v5.Enums.CurrencyCode_Iso4217FromCode(initialRequestInfo.Pricing.CurrencyCode3Letter);
                if (currency == null)
                {
                    result = Connector.Enums.InitialRequestResponseStatus.CurrencyCodeInvalid;
                }
            }

            if (result == Connector.Enums.InitialRequestResponseStatus.Success && initialRequestInfo.ItemDetails != null)
            {
                foreach (var item in initialRequestInfo.ItemDetails)
                {
                    if (item.UnitPrice <= 0)
                    {
                        result = Connector.Enums.InitialRequestResponseStatus.ItemPriceMustNeverBeZeroOrLess;
                        
                    }
                    else if (item.Quantity <= 0)
                    {
                        result = Connector.Enums.InitialRequestResponseStatus.ItemQuantityMustBeOneOrGreater;

                    }
                    else if (string.IsNullOrWhiteSpace(item.Title))
                    {
                        result = Connector.Enums.InitialRequestResponseStatus.ItemTitleMustBeFilledIn;

                    }
                    if (result != Connector.Enums.InitialRequestResponseStatus.Success)
                    {
                        break;
                    }
                }
            }

            return result;

        }
    }
}
