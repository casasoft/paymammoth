﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects;
using BusinessLogic_CS_v5.EnumsBL;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DateAndTime;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Framework.RavenDb.Services;
using BusinessLogic_CS_v5.Modules.Importer.Helpers;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Modules.Services.Settings;
using BusinessLogic_CS_v5.Modules.Services._Shared.BaseDataService;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Extensions;
using CS.General_CS_v5.Modules.InversionOfControl;
using CS.General_CS_v5.Modules.IPAddress;
using NLog;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.Payments.Helpers;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Repositories.Payments;
using PayMammoth_v2.Modules.Services.Emails;
using PayMammoth_v2.Modules.Services.InitialRequests;
using PayMammoth_v2.Modules.Services.Logging;
using PayMammoth_v2.Modules.Services.Notifications;
using PayMammoth_v2.Modules.Services.PaymentMethods.TransactionGenerators;
using Raven.Client;
using PayMammoth_v2.Modules.Services.PaymentMethods;
using Raven.Client.Connection.Profiling;
using Raven.Client.Extensions;
using Raven.Client.Linq;

namespace PayMammoth_v2.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestDataService : IBaseDataService<PaymentRequestData>
    {
        PaymentRequestDataService.GeneratePaymentRequestResult GeneratePaymentRequestFromInitialRequest(
            ReferenceLink<WebsiteAccountData> websiteAccountId,
            InitialRequestInfo initialRequestInfo,
            IDocumentSession documentSession = null);

        TransactionGeneratorResult GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructions(
            Connector.Enums.PaymentMethodType paymentMethod,
            ReferenceLink<PaymentRequestData> paymentRequestId,
            IDocumentSession documentSession = null);

        bool WaitUntilRequestIsMarkedAsPaid(ReferenceLink<PaymentRequestData> paymentRequestId);

        List<PaymentMethodData> GetAllPaymentMethodsAvailableForPayment(
            ReferenceLink<PaymentRequestData> requestId,
            IDocumentSession documentSession);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentRequestDataService : BaseDataService<PaymentRequestData>, IPaymentRequestDataService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();

        public class GeneratePaymentRequestResult
        {
            public PayMammoth_v2.Connector.Enums.InitialRequestResponseStatus Status { get; set; }

            public PaymentRequestData GeneratedRequest { get; set; }
        }

        private readonly IPaymentRequestValidatorService _paymentRequestValidatorService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IInitialRequestService _initialRequestService;
        private readonly IIPAddressRetrieverService _ipAddressRetrieverService;
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;
        private readonly IPayMammothLogService _payMammothLogService;
        private readonly IPaymentMethodsService _paymentMethodsService;
        private readonly IRavenDbService _ravenDbService;
        private readonly ISettingsService _settingsService;
        private readonly IPaymentRequestStatusCheckService _paymentRequestStatusCheckService;
        private readonly IPaymentMethodsRepository _paymentMethodsRepository;

        public PaymentRequestDataService(
            IDataObjectFactory dataObjectFactory,
            IPaymentRequestValidatorService paymentRequestValidatorService,
            IInitialRequestService initialRequestService,
            IIPAddressRetrieverService ipAddressRetrieverService,
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService,
            IPayMammothLogService payMammothLogService,
            IPaymentMethodsService paymentMethodsService,
            IRavenDbService ravenDbService,
            ISettingsService settingsService,
            IPaymentRequestStatusCheckService paymentRequestStatusCheckService,
            IPaymentMethodsRepository paymentMethodsRepository,
            IEntireListOfItemsFromDatabaseRetrieverService entireListOfItemsFromDatabaseRetrieverService) 
            : base(dataObjectFactory,entireListOfItemsFromDatabaseRetrieverService)
        {
            _paymentMethodsRepository = paymentMethodsRepository;
            _paymentRequestStatusCheckService = paymentRequestStatusCheckService;
            _settingsService = settingsService;
            _ravenDbService = ravenDbService;
            _paymentMethodsService = paymentMethodsService;
            _payMammothLogService = payMammothLogService;
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
            _ipAddressRetrieverService = ipAddressRetrieverService;
            _initialRequestService = initialRequestService;
            _dataObjectFactory = dataObjectFactory;
            _paymentRequestValidatorService = paymentRequestValidatorService;
        }

        [RavenTaskAspect]
        [LogAspect]
        public GeneratePaymentRequestResult GeneratePaymentRequestFromInitialRequest(
            ReferenceLink<WebsiteAccountData> websiteAccountId,
            InitialRequestInfo initialRequestInfo,
            IDocumentSession documentSession = null)
        {
            //todo: [For: Karl | 2014/01/07] add test case such that website account is copied sucessfully to payment request (WrittenBy: YourName)        			

            GeneratePaymentRequestResult result = new GeneratePaymentRequestResult();
            result.Status = _paymentRequestValidatorService.ValidateInitialRequest(initialRequestInfo);
            if (result.Status == Connector.Enums.InitialRequestResponseStatus.Success)
            {
                var websiteAccount = documentSession.GetById(new GetByIdParams<WebsiteAccountData>(websiteAccountId) {ThrowErrorIfNotAlreadyLoadedInSession = false});

                var newRequest = _dataObjectFactory.CreateNewDataObject<PaymentRequestData>();
                newRequest.LinkedWebsiteAccountId = websiteAccount;
                _initialRequestService.CopyDetailsFromInitialRequestToPaymentRequest(
                    initialRequestInfo,
                    newRequest);
                newRequest.RequestServerIp = _ipAddressRetrieverService.GetIPAddress();
                newRequest.RequestDateTime = _currentDateTimeRetrieverService.GetCurrentDateTime();
                _payMammothLogService.AddLogEntry(
                    newRequest.StatusLog,
                    "Generated payment request");
                result.GeneratedRequest = newRequest;
                documentSession.Store(newRequest);
            }
            return result;
        }

        [LogAspect]
        [RavenTaskAspect]
        public TransactionGeneratorResult GenerateTransactionBasedOnPaymentMethodAndGetRedirectInstructions(
            Connector.Enums.PaymentMethodType paymentMethod,
            ReferenceLink<PaymentRequestData> paymentRequestId,
            IDocumentSession documentSession = null)
        {
            var transactionGenerator = _paymentMethodsService.GetTransactionGeneratorServiceForPaymentMethod(paymentMethod);
            if (transactionGenerator == null)
                throw new InvalidOperationException(
                    string.Format(
                        "TransactionGenerator not implemented for '{0}'",
                        paymentMethod));
            var result = transactionGenerator.GenerateTransaction(
                paymentRequestId,
                documentSession);
            return result;
        }

        [LogAspect(GenericEnums.NlogLogLevel.Trace)]
        public bool WaitUntilRequestIsMarkedAsPaid(ReferenceLink<PaymentRequestData> paymentRequestId)
        {
            //todo: [For: Karl | 2014/01/07] add unit tests for PaymentRequestService.WaitUntilRequestIsMarkedAsPaid (WrittenBy: Karl)
            bool markedAsPaid = false;
            int totalWait = 0;
            int waitInterval = 2000;

            int timeoutInMilleSec = _settingsService.GetSettingValue<int>(PayMammoth_v2.Enums.PayMammoth_v2Enums.PayMammothSettings.Other_Timeouts_MarkPaid);
            _log.Trace(
                "Timeout to wait until request is marked as paid: {0}",
                timeoutInMilleSec);
            do
            {
                var session = _ravenDbService.CreateNewSession();
                var request = session.GetById(new GetByIdParams<PaymentRequestData>(paymentRequestId) {ThrowErrorIfNotAlreadyLoadedInSession = false});
                session.Dispose();
                bool isPaid = _paymentRequestStatusCheckService.CheckIfRequestIsPaid(request);
                if (isPaid)
                {
                    markedAsPaid = true;
                    _log.Debug("Request marked as paid");
                }
                else
                {
                    _log.Trace(
                        "Request still not yet paid.  Waiting {0}ms",
                        waitInterval);

                    System.Threading.Thread.Sleep(waitInterval);
                    totalWait += waitInterval;
                }
            }
            while (!markedAsPaid && totalWait <= timeoutInMilleSec);
            if (!markedAsPaid)
            {
                _log.Warn(
                    "Request {0} - called WaitUntilRequestIsMarkedAsPaid but was not marked as paid",
                    paymentRequestId);
            }

            return markedAsPaid;
        }

        public List<PaymentMethodData> GetAllPaymentMethodsAvailableForPayment(
            ReferenceLink<PaymentRequestData> requestId,
            IDocumentSession documentSession)
        {
            //todo: [For: Karl | 2014/01/14] implement GetAllPaymentMethodsAvailableForPayment (WrittenBy: Karl)        			

            //var request = session.GetById(requestId,true, x=>x.WebsiteAccountId);
            //var websiteAccount = session.GetById(request.WebsiteAccountId, true);
            //--------
            List<PaymentMethodData> result = _paymentMethodsRepository.GetAllPaymentMethods(documentSession);

            return result;
        }

      
    }
}