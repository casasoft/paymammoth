﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DateAndTime;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Classes.HelperClasses;
using CS.General_CS_v5.Modules.InversionOfControl;
using NLog;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.Payments.Helpers;
using PayMammoth_v2.Modules.Services.Emails;
using PayMammoth_v2.Modules.Services.Notifications;
using Raven.Client;

namespace PayMammoth_v2.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestDataMarkerService
    {
        void MarkPaymentRequestAsExpiredAndSendNotification(
            ReferenceLink<PaymentRequestData> paymentRequestId,
            IDocumentSession documentSession = null);

        PaymentRequestData MarkPaymentRequestAsCancelledAndSendNotification(
            ReferenceLink<PaymentRequestData> paymentRequestId,
            IDocumentSession documentSession = null);

        void MarkRequestAsPaidSuccessfulAndSendNotification(
            ReferenceLink<PaymentRequestData> paymentRequestId,
            ReferenceLink<PaymentRequestTransactionData> successfulTransactionId,
            IDocumentSession documentSession);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentRequestDataMarkerService : IPaymentRequestDataMarkerService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IPaymentRequestDataService _paymentRequestDataService;
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;
        private readonly INotificationMessageDataCreatorService _notificationMessageDataCreatorService;
        private readonly IPayMammothEmailsService _payMammothEmailsService;

        public PaymentRequestDataMarkerService(
            IPaymentRequestDataService paymentRequestDataService,
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService,
            INotificationMessageDataCreatorService notificationMessageDataCreatorService,
            IPayMammothEmailsService payMammothEmailsService)
        {
            _payMammothEmailsService = payMammothEmailsService;
            _notificationMessageDataCreatorService = notificationMessageDataCreatorService;
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
            _paymentRequestDataService = paymentRequestDataService;
        }

        [LogAspect]
        public void MarkRequestAsPaidSuccessfulAndSendNotification(
            ReferenceLink<PaymentRequestData> paymentRequestId,
            ReferenceLink<PaymentRequestTransactionData> successfulTransactionId,
            IDocumentSession documentSession)
        {
            var request = documentSession.GetById(new GetByIdParams<PaymentRequestData>(paymentRequestId) { ThrowErrorIfNotAlreadyLoadedInSession = false });
            var transaction = documentSession.GetById(new GetByIdParams<PaymentRequestTransactionData>(successfulTransactionId) { ThrowErrorIfNotAlreadyLoadedInSession = false });
            if (request == null) throw new InvalidOperationException("Request cannot be null. Id: " + paymentRequestId);
            if (transaction == null) throw new InvalidOperationException("Transaction cannot be null. Id: " + successfulTransactionId);
            if (PaymentRequestStatusUtil.CheckIfRequestIsStillPending(request))
                throw new InvalidOperationException(string.Format("Cannot call this method on a request which is already marked as paid. RequestId: " + paymentRequestId));
            //--------

            request.PaymentStatus.Status = PaymentStatusInfo.PaymentStatus.Paid;
            request.PaymentStatus.PaidOn = _currentDateTimeRetrieverService.GetCurrentDateTime();
            request.PaymentStatus.PaymentMethodUsed = transaction.PaymentMethod;
            request.PaymentStatus.SuccessfulTransactionId = transaction;

            _notificationMessageDataCreatorService.CreateNotificationMessageForSuccessfulPaymentAndSend(
                documentSession,
                transaction);

            _payMammothEmailsService.SendEmailsAboutSuccessfulPayment(transaction);
        }

        [RavenTaskAspect]
        public PaymentRequestData MarkPaymentRequestAsCancelledAndSendNotification(
            ReferenceLink<PaymentRequestData> paymentRequestId,
            IDocumentSession documentSession = null)
        {
            var paymentRequestData = _paymentRequestDataService.GetById(
                documentSession,
                paymentRequestId,
                throwErrorIfNotAlreadyLoadedInSession: false,
                loadItemOnlyIfAvailableToFrontend: true);

            if (paymentRequestData == null) throw new InvalidOperationException(String.Format("Couldn't load PaymentRequestData [{0}]", paymentRequestId.GetLinkId()));

            if (paymentRequestData.PaymentStatus.Status != PaymentStatusInfo.PaymentStatus.Pending) throw new InvalidOperationException(String.Format("Cannot mark PaymentRequestData [{0}] Cancelled if its not Pending!", paymentRequestId.GetLinkId()));

            paymentRequestData.PaymentStatus.Status = PaymentStatusInfo.PaymentStatus.Cancelled;
            paymentRequestData.PaymentStatus.CancelledOn = _currentDateTimeRetrieverService.GetCurrentDateTime();

            documentSession.Store(paymentRequestData);
            documentSession.SaveChanges();

            _notificationMessageDataCreatorService.CreateNotificationMessageDataForImmediatePaymentCancelledAndPushToAzureQueue(
                documentSession,
                paymentRequestId);
            return paymentRequestData;
        }

        [RavenTaskAspect]
        public void MarkPaymentRequestAsExpiredAndSendNotification(
            ReferenceLink<PaymentRequestData> paymentRequestId,

            IDocumentSession documentSession = null)
        {
            var paymentRequestData = _paymentRequestDataService.GetById(
                documentSession,
                paymentRequestId,
                throwErrorIfNotAlreadyLoadedInSession: false,
                loadItemOnlyIfAvailableToFrontend: true);

            if (paymentRequestData == null) throw new InvalidOperationException(String.Format("Couldn't load PaymentRequestData [{0}]", paymentRequestId.GetLinkId()));

            if (paymentRequestData.PaymentStatus.Status != PaymentStatusInfo.PaymentStatus.Pending) throw new InvalidOperationException(String.Format("Cannot mark PaymentRequestData [{0}] Expired if its not Pending!", paymentRequestId.GetLinkId()));

            paymentRequestData.PaymentStatus.Status = PaymentStatusInfo.PaymentStatus.Expired;
            paymentRequestData.PaymentStatus.ExpiredOn = _currentDateTimeRetrieverService.GetCurrentDateTime();

            documentSession.Store(paymentRequestData);
            documentSession.SaveChanges();

            _notificationMessageDataCreatorService.CreateNotificationMessageDataForImmediatePaymentExpiredAndPushToAzureQueue(
                documentSession,
                paymentRequestId);
        }
    }
}