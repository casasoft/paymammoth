﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Extensions;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using Raven.Client;
using Raven.Client.Linq;

namespace PayMammoth_v2.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestNotificationsUtilService
    {
        string GetNotificationUrlForPaymentRequest(
            IDocumentSession documentSession,
            ReferenceLink<PaymentRequestData> paymentRequestId);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentRequestNotificationsUtilService : IPaymentRequestNotificationsUtilService
    {
        public PaymentRequestNotificationsUtilService()
        {
        }

        public string GetNotificationUrlForPaymentRequest(
            IDocumentSession documentSession,
            ReferenceLink<PaymentRequestData> paymentRequestId)
        {
            //todo: [For: Karl | 2014/01/10] add unit-tests PaymentRequestService.GetNotificationUrlForPaymentRequest (WrittenBy: Karl)

            GetByIdParams<PaymentRequestData> getByIdParams = new GetByIdParams<PaymentRequestData>(paymentRequestId);
            getByIdParams.ThrowErrorIfNotAlreadyLoadedInSession = false;
            getByIdParams.AddInclude(x => x.LinkedWebsiteAccountId);
            var request = documentSession.GetById(getByIdParams);

            var websiteAccount = documentSession.GetById(new GetByIdParams<WebsiteAccountData>(request.LinkedWebsiteAccountId));
            //-------

            string url = "";

            if (request.RequestDetails.NotificationUrl.IsNotNullOrWhitespace())
            {
                url = request.RequestDetails.NotificationUrl;
            }
            else
            {
                url = websiteAccount.Notifications.ClientWebsiteResponseUrl;
            }
            return url;
        }
    }
}