﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DateAndTime;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Modules.Services.Settings;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.Payments.Helpers;
using Raven.Client;
using Raven.Client.Linq;

namespace PayMammoth_v2.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestExpirerService
    {
        void CheckForExpiredPaymentRequestsAndMarkedThemAsExpired(
          IDocumentSession documentSession);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaymentRequestExpirerService : IPaymentRequestExpirerService
    {
        private readonly ISettingsService _settingsService;
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;
        private readonly IRavenDbService _ravenDbService;
        private readonly IPaymentRequestDataMarkerService _paymentRequestDataMarkerService;

        public PaymentRequestExpirerService(
            ISettingsService settingsService,
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService,
            IRavenDbService ravenDbService,
            IPaymentRequestDataMarkerService paymentRequestDataMarkerService)
        {
            _paymentRequestDataMarkerService = paymentRequestDataMarkerService;
            _ravenDbService = ravenDbService;
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
            _settingsService = settingsService;
        }

        public void CheckForExpiredPaymentRequestsAndMarkedThemAsExpired(
          IDocumentSession documentSession)
        {
            var currentDateTime = _currentDateTimeRetrieverService.GetCurrentDateTime();
            int start = 0;
            var translateInBatchesOf =
                _settingsService.GetSettingValue<int>(
                    PayMammoth_v2Enums.GetAllDatabaseObjectsForDatabaseTypeWithPagingSettings.CheckForExpiredPaymentRequestsAndMarkedThemAsExpired_LoadInBatchesOf);
            while (true)
            {
                var results = _ravenDbService.GetAllDatabaseObjectsForDatabaseTypeWithPaging<PaymentRequestData>(
                    documentSession,
                    skip: start,
                    take: translateInBatchesOf,
                    loadFrontendItemsOnly: true,
                    customActionOnQuery:
                    q => q.Where(x => x.PaymentStatus.Status == PaymentStatusInfo.PaymentStatus.Pending &&
                                                        (x.RequestDetails.ExpirationInfo.ExpiresAt != null && x.RequestDetails.ExpirationInfo.ExpiresAt > currentDateTime)));

                if (results.Count == 0)
                {
                    break;
                }

                foreach (var item in results)
                {
                    _paymentRequestDataMarkerService.MarkPaymentRequestAsExpiredAndSendNotification(
                        item);
                }

                start += results.Count;
            }
        }
    }
}
