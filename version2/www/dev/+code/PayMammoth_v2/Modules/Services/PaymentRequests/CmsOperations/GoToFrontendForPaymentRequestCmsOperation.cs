﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.EnumsBL;
using BusinessLogic_CS_v5.Framework.Authorization;
using BusinessLogic_CS_v5.Framework.Cms.CustomOperations;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v5.Util;
using PayMammoth_v2.Connector.Services;
using PayMammoth_v2.Modules.Data.Payments;

namespace PayMammoth_v2.Modules.Services.PaymentRequests.CmsOperations
{
    [AccessRequiredDefaults(GenericEnums.AccessTypes.General)]
    [CmsOperationDefaults(Title = "Go to frontend for this request")]
    public class GoToFrontendForPaymentRequestCmsOperation : CmsSpecificOperation<PaymentRequestData>
    {
        private readonly IRavenDbService _ravenDbService;
        private readonly IPayMammothConnectorUrlService _payMammothConnectorUrlService;


        public GoToFrontendForPaymentRequestCmsOperation()
        {
            _ravenDbService = InversionUtil.Get<IRavenDbService>();
            _payMammothConnectorUrlService = InversionUtil.Get<IPayMammothConnectorUrlService>();
        }

        protected override CmsOperationResult ExecuteOperation(PaymentRequestData item, List<BusinessLogic_CS_v5.Framework.Cms.CmsFieldDataValue> fieldInputValues)
        {

            
            var url= _payMammothConnectorUrlService.GetPayMammothUrlForRequest(item.GetRavenDbIdOnlyFromItem(), Connector.Enums.SupportedLanguage.English);
            System.Web.HttpContext.Current.Response.Redirect(url);

         
            var result = new CmsOperationResult();

            result.NotificationMessage = "Redirecting...";
            result.ResultType= NotificationMessageType.Success;
            return result;
        }
    }
    
}
