﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DateAndTime;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Framework.RavenDb.Services;
using BusinessLogic_CS_v5.Modules.Services._Shared.BaseDataService;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Services.RecurringProfiles;
using Raven.Client;

namespace PayMammoth_v2.Modules.Services.PaymentRequests
{
    public interface IPaymentRequestTransactionDataService : IBaseDataService<PaymentRequestTransactionData>
    {
        void MarkTransactionAsPaid(
            PaymentRequestTransactionData transaction,
            bool requiresManualIntervention,
            string authorisationCode,
            IDocumentSession session);

        PaymentRequestTransactionData CreateNewTransaction(
            ReferenceLink<PaymentRequestData> requestId,
            Connector.Enums.PaymentMethodType paymentMethodType,
            IDocumentSession session);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentRequestTransactionDataService : BaseDataService<PaymentRequestTransactionData>, IPaymentRequestTransactionDataService
    {
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;
        private readonly IRecurringProfilesService _recurringProfilesService;
        private readonly IDataObjectFactory _dataObjectFactory;
        private readonly IPaymentRequestDataMarkerService _markRequestAsSuccessfulService;

        public PaymentRequestTransactionDataService(
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService,
            IRecurringProfilesService recurringProfilesService,
            IDataObjectFactory dataObjectFactory,
            IPaymentRequestDataMarkerService markRequestAsSuccessfulService,
            IEntireListOfItemsFromDatabaseRetrieverService entireListOfItemsFromDatabaseRetrieverService)
            : base(dataObjectFactory, entireListOfItemsFromDatabaseRetrieverService)
        {
            _markRequestAsSuccessfulService = markRequestAsSuccessfulService;
            _dataObjectFactory = dataObjectFactory;
            _recurringProfilesService = recurringProfilesService;
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
        }

        [LogAspect]
        public void MarkTransactionAsPaid(
            PaymentRequestTransactionData transaction,
            bool requiresManualIntervention,
            string authorisationCode,
            IDocumentSession session)
        {
            //defensive checks
            //var getByIdParams = new GetByIdParams<PaymentRequestTransactionData>(transactionId);
            //getByIdParams.AddInclude(x => x.PaymentRequestId);
            //getByIdParams.ThrowErrorIfNotAlreadyLoadedInSession = false;
            //var transaction = session.GetById(getByIdParams);
            //if (transaction == null) throw new InvalidOperationException("Cannot call MarkTransactionAsPaid on a transaction that does not exist - Id: " + transactionId);
            if (transaction.IsSuccessful) throw new InvalidOperationException("Cannot call MarkTransactionAsPaid on a transaction that is already successful - Id: " + transaction);
            var request = session.GetById(new GetByIdParams<PaymentRequestData>(transaction.PaymentRequestId) {ThrowErrorIfNotAlreadyLoadedInSession = false});
            ;
            if (request == null)
                throw new InvalidOperationException(
                    string.Format(
                        "Cannot call MarkTransactionAsPaid on a transaction whose request does not exist.  TransactionId: {0} | RequestId: {1}",
                        transaction,
                        transaction.PaymentRequestId));
            if (PaymentRequestStatusUtil.CheckIfRequestIsStillPending(request))
                throw new InvalidOperationException(
                    string.Format(
                        "Cannot call MarkTransactionAsPaid on a transaction whose request is already marked as paid.  TransactionId: {0} | RequestId: {1}",
                        transaction,
                        transaction.PaymentRequestId));

            //------------

            transaction.DateFinished = transaction.DateLastActivity = _currentDateTimeRetrieverService.GetCurrentDateTime();
            transaction.IsSuccessful = true;
            transaction.AuthCode = authorisationCode;
            transaction.RequiresManualIntervention = requiresManualIntervention;

            if (request.RequestDetails.RecurringProfile.Required)
            {
                _recurringProfilesService.CreateRecurringProfileIfRequired(
                    request,
                    session);
            }

            _markRequestAsSuccessfulService.MarkRequestAsPaidSuccessfulAndSendNotification(
                request,
                transaction,
                session);
        }

        public PaymentRequestTransactionData CreateNewTransaction(
            ReferenceLink<PaymentRequestData> requestId,
            Connector.Enums.PaymentMethodType paymentMethodType,
            IDocumentSession session)
        {
            //todo: [For: Karl | 2014/01/07] create unit tests PaymentRequestTransactionsService.CreateNewTransaction (WrittenBy: Karl)
            PaymentRequestTransactionData transaction = _dataObjectFactory.CreateNewDataObject<PaymentRequestTransactionData>();
            transaction.PaymentRequestId = requestId;
            transaction.PaymentMethod = paymentMethodType;

            session.Store(transaction);
            return transaction;
        }
    }
}