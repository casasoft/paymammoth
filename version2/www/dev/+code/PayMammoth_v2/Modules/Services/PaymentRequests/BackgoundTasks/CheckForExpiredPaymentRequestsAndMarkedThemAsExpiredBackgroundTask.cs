﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.Tasks;
using BusinessLogic_CS_v5.Util;
using PayMammoth_v2.Modules.Services.PaymentRequests;

namespace PayMammoth_v2.Presentation.Services.Payments.BackgoundTasks
{
    public class CheckForExpiredPaymentRequestsAndMarkedThemAsExpiredBackgroundTask : BaseRecurringBackgroundTask
    {
        public CheckForExpiredPaymentRequestsAndMarkedThemAsExpiredBackgroundTask()
            : base(Enums.PayMammoth_v2Enums.BackgroundTasksSettings.CheckForExpiredPaymentRequestsAndMarkedThemAsExpiredTask_RecurringIntervalInSeconds)
        {
        }

        protected override void recurringTask()
        {
            var documentSession = RavenDbUtil.DocumentStore.OpenSession();
            InversionUtil.Get<IPaymentRequestExpirerService>().CheckForExpiredPaymentRequestsAndMarkedThemAsExpired(
                documentSession);
            documentSession.Dispose();
        }
    }
}
