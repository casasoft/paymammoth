﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.EnumsBL;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DateAndTime;
using CS.General_CS_v5.Modules.InversionOfControl;
using NLog;
using PayMammoth_v2.Modules.Data._Shared;
using PayMammoth_v2.Modules.Data.Payments;

namespace PayMammoth_v2.Modules.Services.Logging
{
    public interface IPayMammothLogService
    {
        void AddLogEntry(PaymentRequestTransactionData transaction, string msgToAdd, Logger nLogger = null,
            GenericEnums.NlogLogLevel nlogLogLevel = GenericEnums.NlogLogLevel.Debug, Exception ex = null);
        void AddLogEntry(PaymentRequestData log, string msgToAdd, Logger nLogger = null,
            GenericEnums.NlogLogLevel nlogLogLevel = GenericEnums.NlogLogLevel.Debug, Exception ex = null);
        void AddLogEntry(PayMammothLog log, string msgToAdd, Logger nLogger = null, 
            GenericEnums.NlogLogLevel nlogLogLevel = GenericEnums.NlogLogLevel.Debug, Exception ex = null);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PayMammothLogService : IPayMammothLogService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;

        public PayMammothLogService(ICurrentDateTimeRetrieverService currentDateTimeRetrieverService)
        {
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
        }

        public void AddLogEntry(PayMammothLog log, string msgToAdd, Logger nLogger = null, 
            GenericEnums.NlogLogLevel nlogLogLevel = GenericEnums.NlogLogLevel.Debug, Exception ex = null)
        {
            
            if (nLogger == null) nLogger = _log;
            
            string s = log.LogData;
            var currDateTime = _currentDateTimeRetrieverService.GetCurrentDateTime();
            var currThreadId = System.Threading.Thread.CurrentThread.ManagedThreadId;

            s += string.Format("[{0} | <{1}> | ThreadID: {2}] {3}\r\n", 
                currDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff"), nlogLogLevel.ToString(), currThreadId, msgToAdd);

            if (ex != null)
            {
                s += "\r\n----- EXCEPTION DETAILS -----\r\n";
                s += CS.General_CS_v5.Util.ExceptionsUtil.ConvertExceptionToPlainTextString(ex);
                s += "\r\n===================================================\r\n";
                s += "\r\n===================================================\r\n\r\n";
            }


            log.LogData = s;
            if (nlogLogLevel != GenericEnums.NlogLogLevel.Off)
            {
                var nlogLevel = BusinessLogic_CS_v5.Util.NLogUtil.ConvertOurLogLevelToNLogLogLevel(nlogLogLevel);
                if (ex == null)
                {
                    nLogger.Log(nlogLevel, msgToAdd);
                }
                else
                {
                    nLogger.LogException(nlogLevel, msgToAdd, ex);
                }
            }

        }

        public void AddLogEntry(PaymentRequestTransactionData transaction, string msgToAdd, Logger nLogger = null, GenericEnums.NlogLogLevel nlogLogLevel = GenericEnums.NlogLogLevel.Debug, Exception ex = null)
        {
            AddLogEntry(transaction.TransactionLog, msgToAdd, nLogger, nlogLogLevel, ex);
            
        }

        public void AddLogEntry(PaymentRequestData request, string msgToAdd, Logger nLogger = null, GenericEnums.NlogLogLevel nlogLogLevel = GenericEnums.NlogLogLevel.Debug, Exception ex = null)
        {
            AddLogEntry(request.StatusLog, msgToAdd, nLogger, nlogLogLevel, ex);
            
        }
    }
}
