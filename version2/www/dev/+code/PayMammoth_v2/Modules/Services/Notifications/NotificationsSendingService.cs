﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects;
using BusinessLogic_CS_v5.EnumsBL;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DateAndTime;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Modules.InversionOfControl;
using CS.General_CS_v5.Modules.Web;
using CS.General_CS_v5.Util;
using NLog;
using PayMammoth_v2.Modules.Data.Notifications;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Services.Logging;
using Raven.Abstractions.Data;
using Raven.Client;
using PayMammoth_v2.Connector.Notifications;

namespace PayMammoth_v2.Modules.Services.Notifications
{
    public interface INotificationMessageDataSendingService
    {
        //void SendNotificationMessageOnSeperateThread(ReferenceLink<NotificationMessageData> notificationId);
        void SendNotificationMessageData(
            ReferenceLink<NotificationMessageData> notificationId,
            IDocumentSession session = null);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class NotificationMessageDataSendingService : INotificationMessageDataSendingService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IHttpRequestSenderService _httpRequestSenderService;
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;
        private readonly INotificationsRetryService _notificationsRetryService;
        private readonly IPayMammothLogService _payMammothLogService;

        public NotificationMessageDataSendingService(
            IHttpRequestSenderService httpRequestSenderService,
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService,
            INotificationsRetryService notificationsRetryService,
            IPayMammothLogService payMammothLogService)
        {
            _payMammothLogService = payMammothLogService;
            _notificationsRetryService = notificationsRetryService;
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
            _httpRequestSenderService = httpRequestSenderService;
        }

        //public void SendNotificationMessageOnSeperateThread(ReferenceLink<NotificationMessageData> notificationId)
        //{
        //    //todo: [For: Backend | 2014/04/03] make this launch on background task (WrittenBy: Karl)        			
        //    CS.General_CS_v5.Util.ThreadUtil.CallMethodOnSeperateThread(() => SendNotificationMessage(notificationId), 
        //        "SendNotificationMsg-" + notificationId, ThreadPriority.Normal, dontLaunchOnSeperateThread:true);
        //}
        private readonly object _currentlySendingMessagesLock = new object();
        private readonly ConcurrentDictionary<string, bool> _currentlySendingMessages = new ConcurrentDictionary<string, bool>();

        [RavenTaskAspect]
        [LogAspect(GenericEnums.NlogLogLevel.Info)]
        public void SendNotificationMessageData(
            ReferenceLink<NotificationMessageData> notificationId,
            IDocumentSession session = null)
        {
            System.Diagnostics.Trace.WriteLine("SendNotificationMessage - " + notificationId);
            //todo: [For: Karl | 2014/04/30] update unit-tests for logic, such that the SuccessfullySentOn is updated if it is success (WrittenBy: YourName)        			
            //and that if the notification msg is not Pending, it is not sent again.
            bool alreadySending = false;
            lock (_currentlySendingMessagesLock)
            {
                if (_currentlySendingMessages.ContainsKey(notificationId))
                {
                    _log.Warn(
                        "Not sending message [{0}] because it is already being sent.  With AzureQueues, this should never happen",
                        notificationId);
                    alreadySending = true;
                }
                else
                {
                    _currentlySendingMessages[notificationId] = true;
                }
            }
            if (!alreadySending)
            {
                bool proceed = true;
                var notificationGetByIdParams = new GetByIdParams<NotificationMessageData>(notificationId);
                notificationGetByIdParams.ThrowErrorIfNotAlreadyLoadedInSession = false;
                notificationGetByIdParams.AddInclude(x => x.LinkedPaymentRequestId);
                var notificationMsg = session.GetById(notificationGetByIdParams);
                var request = session.GetById(new GetByIdParams<PaymentRequestData>(notificationMsg.LinkedPaymentRequestId));
                var infoOnCurrentSend = notificationMsg.NotificationSendingInformation.CreateNewSubCollectionItem();
                infoOnCurrentSend.DateTime = _currentDateTimeRetrieverService.GetCurrentDateTime();
                if (string.IsNullOrWhiteSpace(notificationMsg.SendToUrl))
                {
                    proceed = false;
                    infoOnCurrentSend.Status = NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.NoSendToUrl;
                    infoOnCurrentSend.Message = "Could not send notification as no SendToUrl exists";
                }

                if (proceed && notificationMsg.Status != Connector.Enums.NotificationMessageStatus.Pending)
                {
                    proceed = false;
                    infoOnCurrentSend.Status = NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.NotificationIsNotPending;
                    infoOnCurrentSend.Message = "Could not send notification as notification is not pending";
                }

                //--------defensive checks done

                if (proceed)
                {
                    //create msg to send and convert to json
                    NotificationMessage msgToSend = new NotificationMessage();
                    msgToSend.DateTimeSent = _currentDateTimeRetrieverService.GetCurrentDateTime();
                    msgToSend.Identifier = notificationMsg.Identifier;
                    msgToSend.Message = notificationMsg.Message;
                    msgToSend.MessageType = notificationMsg.NotificationType;
                    msgToSend.NotificationId = notificationMsg.Id;
                    msgToSend.PaymentRequestId = notificationMsg.LinkedPaymentRequestId;
                    msgToSend.RetryCount = notificationMsg.RetryCount;
                    string msgAsJson = CS.General_CS_v5.Util.JsonUtil.Serialize(msgToSend);

                    //initialise headers
                    WebHeaderCollection headers = new WebHeaderCollection();
                    headers.Add(
                        "Cache-Control",
                        "no-cache");
                    headers.Add(
                        "Accept-Language",
                        "en-us");
                    headers.Add(
                        "Content-Type",
                        "application/x-www-form-urlencoded");

                    //send the actual notification
                    try
                    {
                        try
                        {
                            _payMammothLogService.AddLogEntry(
                                request.StatusLog,
                                string.Format(
                                    "Sending notification #{0} [{1}] to {2}",
                                    notificationMsg.RetryCount,
                                    notificationId,
                                    notificationMsg.SendToUrl),
                                _log);

                            var webResponse = _httpRequestSenderService.SendHttpRequest(
                                notificationMsg.SendToUrl,
                                CS.General_CS_v5.Enums.HttpMethod.POST,
                                headers,
                                msgAsJson,
                                null,
                                60*1000,
                                null,
                                null);

                            _payMammothLogService.AddLogEntry(
                                request.StatusLog,
                                string.Format("Notification sent. Checking response"),
                                _log);

                            var statusCode = webResponse.StatusCode;
                            if (statusCode == HttpStatusCode.OK)
                            {
                                var responseString = webResponse.GetResponseAsString();
                                if (responseString == PayMammoth_v2.Connector.Constants.RESPONSE_OK)
                                {
                                    infoOnCurrentSend.Message = "Sent successfully";
                                    infoOnCurrentSend.Status = NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.Success;
                                    _payMammothLogService.AddLogEntry(
                                        request.StatusLog,
                                        string.Format("Response OK - SUCCESS"),
                                        _log);
                                    notificationMsg.Status = Connector.Enums.NotificationMessageStatus.Success;
                                    notificationMsg.SentSuccessfullyOn = _currentDateTimeRetrieverService.GetCurrentDateTime();
                                }
                                else
                                {
                                    infoOnCurrentSend.Message = "Incorrect response recieved - '" + responseString + "'";
                                    infoOnCurrentSend.Status = NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.IncorrectResponse;
                                    _payMammothLogService.AddLogEntry(
                                        request.StatusLog,
                                        infoOnCurrentSend.Message,
                                        _log);
                                }
                            }
                            else
                            {
                                infoOnCurrentSend.Message = "Invalid status code - " + statusCode;
                                infoOnCurrentSend.Status = NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.ErrorOccurred;
                                _payMammothLogService.AddLogEntry(
                                    request.StatusLog,
                                    infoOnCurrentSend.Message,
                                    _log);
                            }
                            webResponse.Close();
                        }
                        catch (WebException ex)
                        {
                            string exResponse = "[NULL]";
                            if (ex.Response != null)
                            {
                                var responseStream = ex.Response.GetResponseStream();
                                StreamReader sr = new StreamReader(responseStream);
                                exResponse = sr.ReadToEnd();
                            }

                            infoOnCurrentSend.Message = string.Format(
                                @"WebException occurred while trying to send notification. Msg: {0} | StatusCode: {1}

Response
--------
{2}

Exception
---------
{3}",
                                ex.Message,
                                ex.Status,
                                exResponse,
                                CS.General_CS_v5.Util.ExceptionsUtil.ConvertExceptionToPlainTextString(ex));

                            infoOnCurrentSend.Status = NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.ErrorOccurred;

                            _payMammothLogService.AddLogEntry(
                                request.StatusLog,
                                "WebException occurred while trying to send notification",
                                _log,
                                GenericEnums.NlogLogLevel.Warn,
                                ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        infoOnCurrentSend.Message = string.Format(
                            @"Exception occurred while trying to send notification. Msg: {0}

Exception
---------
{1}",
                            ex.Message,
                            CS.General_CS_v5.Util.ExceptionsUtil.ConvertExceptionToPlainTextString(ex));

                        infoOnCurrentSend.Status = NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.ErrorOccurred;

                        _payMammothLogService.AddLogEntry(
                            request.StatusLog,
                            "Exception occurred while trying to send notification",
                            _log,
                            GenericEnums.NlogLogLevel.Warn,
                            ex);
                    }

                    if (infoOnCurrentSend.Status != NotificationSendingInstanceInfo.NotificationSendingInstanceInfoStatus.Success)
                    {
                        _notificationsRetryService.IncrementRetryCountAndRepushOnAzureQueueOrMarkAsFailedIfMaximumReached(notificationMsg);
                    }
                }

                {
                    bool b;
                    bool removed = _currentlySendingMessages.TryRemove(
                        notificationId,
                        out b);
                }
            }
        }
    }
}