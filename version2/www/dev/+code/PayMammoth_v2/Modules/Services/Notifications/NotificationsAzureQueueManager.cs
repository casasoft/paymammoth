﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Modules.Services.AzureServices.Queues;

namespace PayMammoth_v2.Modules.Services.Notifications
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface INotificationsAzureQueueManager : IAzureQueueManager
    {

    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class NotificationsAzureQueueManager : AzureQueueManager<QueueNotificationMessage>, INotificationsAzureQueueManager 
    {
        private readonly IAzureQueueParametersService _azureQueueParametersService;
       
        public NotificationsAzureQueueManager (IInitialiseAzureQueueClientService initialiseAzureQueueClientService,
            IAzureQueueParametersService azureQueueParametersService)
             : base(initialiseAzureQueueClientService)
        {
           
            _azureQueueParametersService = azureQueueParametersService;
        }

        protected override AzureQueueParameters initialiseQueueParameters()
        {
            AzureQueueParameters parameters = new AzureQueueParameters();
            _azureQueueParametersService.FillAzureQueueParametersFromSettings(parameters,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.PayMammothSettings.Notifications_AzureQueue_ConnectionString,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.PayMammothSettings.Notifications_AzureQueue_QueueName);
            return parameters;
        }

        protected override void onMessageReceived(QueueNotificationMessage msgBody)
        {
            //send the notification
            BusinessLogic_CS_v5.Util.InversionUtil.Get<INotificationMessageDataSendingService>().SendNotificationMessageData(msgBody.NotificationId);

            


        }
    }
   
}
