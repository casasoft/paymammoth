﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using PayMammoth_v2.Modules.Data.Notifications;
using PayMammoth_v2.Modules.Data.Payments;

namespace PayMammoth_v2.Modules.Services.Notifications
{
    /// <summary>
    /// This is the notification message stored in the AzureQueue
    /// </summary>
    public class QueueNotificationMessage 
    {
        public QueueNotificationMessage()
        {
            //this.StatusLog = new PayMammothLog();
        }

        public ReferenceLink<NotificationMessageData> NotificationId { get; set; }



        public override string ToString()
        {
            return this.NotificationId;
        }

        //public PayMammothLog StatusLog { get; set; }

    }
}
