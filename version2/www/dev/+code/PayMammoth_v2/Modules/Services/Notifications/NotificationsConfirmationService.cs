﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Presentation.Services.Fields;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Modules.Data.Notifications;

namespace PayMammoth_v2.Modules.Services.Notifications
{
    public interface INotificationsConfirmationService
    {
        bool ConfirmNotification(ReferenceLink<NotificationMessageData> notificationId, Connector.Enums.NotificationMessageType messageType);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class NotificationsConfirmationService : INotificationsConfirmationService
    {
        private readonly IRavenDbService _ravenDbService;

        public NotificationsConfirmationService(IRavenDbService ravenDbService)
        {
            _ravenDbService = ravenDbService;
        }

        public bool ConfirmNotification(ReferenceLink<NotificationMessageData> notificationId, Connector.Enums.NotificationMessageType messageType)
        {
            //todo: [For: Backend | 2014/04/03] implement unit-tests for NotificationsConfirmationService.ConfirmNotification (WrittenBy: Backend)

            NotificationMessageData msg;
            var session = _ravenDbService.CreateNewSession();
            var notification = session.GetById<NotificationMessageData>(new GetByIdParams<NotificationMessageData>(notificationId) 
                    { ThrowErrorIfNotAlreadyLoadedInSession = false , ThrowErrorIfDoesNotExist = false});
            bool confirmed = false;
            if (notification != null && notification.NotificationType == messageType)
            {
                confirmed = true;
            }
            return confirmed;

            
            
        }
    }
}
