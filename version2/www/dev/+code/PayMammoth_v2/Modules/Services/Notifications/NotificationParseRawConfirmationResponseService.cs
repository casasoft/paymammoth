﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Connector.Notifications;
using PayMammoth_v2.Modules.Data.Notifications;

namespace PayMammoth_v2.Modules.Services.Notifications
{
    public interface INotificationParseRawConfirmationResponseService
    {
        Connector.Enums.NotificationConfirmationStatus ParseRawConfirmationResponse(string formData);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class NotificationParseRawConfirmationResponseService : INotificationParseRawConfirmationResponseService
    {
        private readonly INotificationsConfirmationService _notificationsConfirmationService;

        public NotificationParseRawConfirmationResponseService(INotificationsConfirmationService notificationsConfirmationService)
        {
            _notificationsConfirmationService = notificationsConfirmationService;
        }

        public Connector.Enums.NotificationConfirmationStatus ParseRawConfirmationResponse(string formData)
        {
            Connector.Enums.NotificationConfirmationStatus result = Connector.Enums.NotificationConfirmationStatus.InvalidFormData;
            if (!string.IsNullOrWhiteSpace(formData))
            {
                result = Connector.Enums.NotificationConfirmationStatus.NotificationCouldNotBeConfirmed;
                NotificationConfirmMessage msg = CS.General_CS_v5.Util.JsonUtil.Deserialise<NotificationConfirmMessage>(formData, throwErrorIfCannotDeserialize:false);
                if (msg != null && !string.IsNullOrWhiteSpace(msg.NotificationId))
                {
                    var confirmResult = _notificationsConfirmationService.ConfirmNotification(
                                                    new ReferenceLink<NotificationMessageData>(msg.NotificationId),
                                                     msg.MessageType);
                    if (confirmResult)
                    {
                        result = Connector.Enums.NotificationConfirmationStatus.OK;
                    }
                   
                }

            }
            return result;
        }
     }
}
