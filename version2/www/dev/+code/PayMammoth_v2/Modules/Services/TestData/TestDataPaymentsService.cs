﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DateAndTime;
using BusinessLogic_CS_v5.Modules.Services.RavenDb;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using PayMammoth_v2.Connector.Notifications;
using PayMammoth_v2.Modules.Data.TestData;

namespace PayMammoth_v2.Modules.Services.TestData
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface ITestDataPaymentsService
    {
        void OnTestPaymentPaidSuccessfully(NotificationMessage msg);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class TestDataPaymentsService : ITestDataPaymentsService
    {
        private readonly IRavenDbService _ravenDbService;
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;

        public TestDataPaymentsService(IRavenDbService ravenDbService, ICurrentDateTimeRetrieverService currentDateTimeRetrieverService)
        {
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
            _ravenDbService = ravenDbService;
        }

        public void OnTestPaymentPaidSuccessfully(NotificationMessage msg)
        {
            if (msg.MessageType == Connector.Enums.NotificationMessageType.ImmediatePaymentSuccess)
            {
                var session =  _ravenDbService.CreateNewSession();
                string testPaymentId = BusinessLogic_CS_v5.Util.RavenDbUtil.GetDocumentIdForType<TestPaymentData>(msg.Identifier);
                var testPayment = session.GetById(new GetByIdParams<TestPaymentData>(testPaymentId) { ThrowErrorIfNotAlreadyLoadedInSession = false });
                testPayment.MarkedAsPaid = true;
                testPayment.MarkedAsPaidOn = _currentDateTimeRetrieverService.GetCurrentDateTime();
                session.Store(testPayment);
                session.SaveChanges();

            }

            
        }
    }
}
