﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Modules.Data.Payments;
using Raven.Abstractions.Data;
using Raven.Client;

namespace PayMammoth_v2.Modules.Services.RecurringProfiles
{
    public interface IRecurringProfilesService
    {
        void CreateRecurringProfileIfRequired(ReferenceLink<PaymentRequestData> requestId, IDocumentSession session); 
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class RecurringProfilesService : IRecurringProfilesService
    {
        public RecurringProfilesService()
        {

        }

        public void CreateRecurringProfileIfRequired(ReferenceLink<PaymentRequestData> requestId, IDocumentSession session)
        {
            //todo: [For: Karl | 2013/12/31] implement RecurringProfilesService.CreateRecurringProfileIfRequired (WrittenBy: Karl)
            throw new NotImplementedException();
        }
    }
}
