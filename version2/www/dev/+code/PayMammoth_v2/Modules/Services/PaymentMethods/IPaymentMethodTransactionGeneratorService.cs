﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Services.PaymentMethods.TransactionGenerators;
using Raven.Client;

namespace PayMammoth_v2.Modules.Services.PaymentMethods
{
    public interface IPaymentMethodTransactionGeneratorService
    {
        TransactionGeneratorResult GenerateTransaction(ReferenceLink<PaymentRequestData> requestId, IDocumentSession session);
    }
}
