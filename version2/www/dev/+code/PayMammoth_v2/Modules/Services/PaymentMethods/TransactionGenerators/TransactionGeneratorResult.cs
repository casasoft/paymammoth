﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v2.Modules.Data.Payments;

namespace PayMammoth_v2.Modules.Services.PaymentMethods.TransactionGenerators
{
    public class TransactionGeneratorResult
    {
        

        public PayMammoth_v2.Enums.PayMammoth_v2Enums.RedirectionResultStatus Result { get; set; }
        public PaymentRequestTransactionData GeneratedTransaction { get; set; }
        public string UrlToRedirectTo { get; set; }
        public CS.General_CS_v5.Enums.HrefTarget RedirectionType { get; set; }
        

    }
}
