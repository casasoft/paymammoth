﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1;
using PayMammoth_v2.PaymentsIntegration.Transactium.v1;

namespace PayMammoth_v2.Modules.Services.PaymentMethods
{
    public interface IPaymentMethodsService
    {
        IPaymentMethodTransactionGeneratorService GetTransactionGeneratorServiceForPaymentMethod(Connector.Enums.PaymentMethodType paymentMethod);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentMethodsService : IPaymentMethodsService
    {
        private readonly IPayPalTransactionGeneratorService _payPalTransactionGeneratorService;
        private readonly ITransactiumTransactionGeneratorService _transactiumTransactionGeneratorService;

        public PaymentMethodsService(IPayPalTransactionGeneratorService payPalTransactionGeneratorService,
            ITransactiumTransactionGeneratorService transactiumTransactionGeneratorService)
        {
            _transactiumTransactionGeneratorService = transactiumTransactionGeneratorService;
            _payPalTransactionGeneratorService = payPalTransactionGeneratorService;
        }

        public IPaymentMethodTransactionGeneratorService GetTransactionGeneratorServiceForPaymentMethod(Connector.Enums.PaymentMethodType paymentMethod)
        {
            //todo: [For: Karl | 2013/12/31] GetTransactionGeneratorServiceForPaymentMethod - Implement this once they have been properly implemented (WrittenBy: Karl)        			
            switch (paymentMethod)
            {
                case Connector.Enums.PaymentMethodType.PayPal_ExpressCheckout:
                    return _payPalTransactionGeneratorService;
                case  Connector.Enums.PaymentMethodType.PaymentGateway_Transactium:
                    return _transactiumTransactionGeneratorService;
            }
            throw new InvalidOperationException(string.Format("TransactionGeneratorService not implemented for payment method '{0}'", paymentMethod));
            
        }
    }
}
