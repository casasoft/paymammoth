﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Util;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v2.Modules.Services.InitialRequests
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IInitialRequestRawMessageParserService
    {
        /// <summary>
        /// This parses the raw contents from the client website
        /// </summary>
        /// <param name="formContents"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        InitialRequestResponseMessage ParseRawMessage(string formContents, IDocumentSession session = null);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class InitialRequestRawMessageParserService : IInitialRequestRawMessageParserService
    {
        private readonly IInitialRequestService _initialRequestService;
        private readonly IPaymentRequestDataService _paymentRequestDataService;

        public InitialRequestRawMessageParserService(IInitialRequestService initialRequestService,
            IPaymentRequestDataService paymentRequestDataService)
        {
            _paymentRequestDataService = paymentRequestDataService;
            _initialRequestService = initialRequestService;
        }

        [RavenTaskAspect]
        public InitialRequestResponseMessage ParseRawMessage(string formContents, IDocumentSession session = null)
        {
            //System.Web.HttpContext.Current.Request.
            InitialRequestResponseMessage result = new InitialRequestResponseMessage();
            result.Status = Connector.Enums.InitialRequestResponseStatus.InvalidData;
            var rawResult=  _initialRequestService.ParseRawStringToInitialRequest(formContents);
            if (rawResult != null)
            {
                var decryptedResult = _initialRequestService.DecryptInitialRequest(session, rawResult);
                if (decryptedResult.Status == Connector.Enums.InitialRequestResponseStatus.Success)
                {
                    var generatedResult = _paymentRequestDataService.GeneratePaymentRequestFromInitialRequest(decryptedResult.WebsiteAccountId,
                        decryptedResult.InitialRequest);
                    result.Status = generatedResult.Status;
                    if (generatedResult.Status == Connector.Enums.InitialRequestResponseStatus.Success)
                    {
                        result.RequestId = generatedResult.GeneratedRequest.GetRavenDbIdOnlyFromItem();

                    }

                }
                else
                {
                    result.Status = decryptedResult.Status;
                }
            }
            return result;
            
        }
    }
}
