﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DbObjects.Collections;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Modules.InversionOfControl;
using Newtonsoft.Json;
using NLog;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.Payments.Helpers;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.InitialRequests.Helpers;
using Raven.Client;
using Raven.Client.Linq;

namespace PayMammoth_v2.Modules.Services.InitialRequests
{
    public interface IInitialRequestService
    {
        /// <summary>
        /// Decrypts the initial request.  If everything is ok, returns Success and the decrypted result.  Possible error statuses: [CouldNotDecrypt, InvalidData]
        /// </summary>
        /// <param name="session"></param>
        /// <param name="initialRequestEncryptedData"></param>
        /// <returns></returns>
        InitialRequestDecryptResult DecryptInitialRequest(IDocumentSession session, InitialRequestEncryptedData initialRequestEncryptedData);
        InitialRequestEncryptedData ParseRawStringToInitialRequest(string rawData);

        void CopyDetailsFromInitialRequestToPaymentRequest(InitialRequestInfo initialRequestInfoToCopyFrom, PaymentRequestData paymentRequestDataToCopyTo);

    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class InitialRequestService : IInitialRequestService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        public InitialRequestService()
        {


        }

        public InitialRequestDecryptResult DecryptInitialRequest(IDocumentSession session, InitialRequestEncryptedData initialRequestEncryptedData)
        {
            InitialRequestDecryptResult result = new InitialRequestDecryptResult();
            result.Status = Connector.Enums.InitialRequestResponseStatus.WebsiteAccountDoesNotExist;
            if (!string.IsNullOrWhiteSpace(initialRequestEncryptedData.Data) &&
                !string.IsNullOrWhiteSpace(initialRequestEncryptedData.WebsiteAccountCode))
            {

                string websiteAccountFullId = RavenDbUtil.GetDocumentIdForType<WebsiteAccountData>(initialRequestEncryptedData.WebsiteAccountCode);

                var websiteAccount = RavenDbUtil.GetById<WebsiteAccountData>(session,
                    new GetByIdParams<WebsiteAccountData>(websiteAccountFullId)
                    {
                        ThrowErrorIfNotAlreadyLoadedInSession = false,
                    ThrowErrorIfDoesNotExist = false});
                if (websiteAccount != null)
                {
                    string decryptedJson = null;
                    try
                    {
                        decryptedJson = CS.General_CS_v5.Util.CryptographyUtil.Decrypt(initialRequestEncryptedData.Data, websiteAccount.SecretWord);
                    }
                    catch (Exception ex)
                    {
                        result.Status = Connector.Enums.InitialRequestResponseStatus.CouldNotDecrypt;
                        _log.WarnException(string.Format("Could not decrypt data [[{0}]] with current secret word", initialRequestEncryptedData.Data), ex);
                        decryptedJson = null;
                    }
                    if (decryptedJson != null)
                    {
                        try
                        {
                            result.InitialRequest =CS.General_CS_v5.Util.JsonUtil.Deserialise<InitialRequestInfo>(decryptedJson);
                            result.WebsiteAccountId = websiteAccount;
                            result.Status = Connector.Enums.InitialRequestResponseStatus.Success;
                        }
                        catch (JsonException ex)
                        {
                            result.InitialRequest = null;
                            _log.WarnException(string.Format("Could not deserialise JSON data [[{0}]] into a valid InitialRequestInfo", decryptedJson), ex);
                            result.Status = Connector.Enums.InitialRequestResponseStatus.InvalidData;

                        }
                    }
                }
                else
                {
                    _log.Trace("No website account exists with code '{0}'", initialRequestEncryptedData.WebsiteAccountCode);
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(initialRequestEncryptedData.Data))
                {
                    _log.Trace("Data cannot be null or emtpy");
                }
                if (string.IsNullOrWhiteSpace(initialRequestEncryptedData.WebsiteAccountCode))
                {
                    _log.Trace("WebsiteAccountCode cannot be null or emtpy");
                }
            }
            return result;

        }


        public InitialRequestEncryptedData ParseRawStringToInitialRequest(string rawData)
        {
            InitialRequestEncryptedData result = null;
            try
            {
                result=CS.General_CS_v5.Util.JsonUtil.Deserialise<InitialRequestEncryptedData>(rawData);
            }
            catch (JsonException ex)
            {
                
                result = null;
            }
            
            return result;
        }


        public void CopyDetailsFromInitialRequestToPaymentRequest(InitialRequestInfo initialRequestInfoToCopyFrom, PaymentRequestData paymentRequestDataToCopyTo)
        {
            var from = initialRequestInfoToCopyFrom;
            var to = paymentRequestDataToCopyTo;
            to.RequestDetails = from;
            
        }
    }
}