﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;


namespace PayMammoth_v2.Modules.Services.InitialRequests.Helpers
{
    public class InitialRequestDecryptResult
    {
        public ReferenceLink<WebsiteAccountData> WebsiteAccountId { get; set; }
        public InitialRequestInfo InitialRequest { get; set; }
        public PayMammoth_v2.Connector.Enums.InitialRequestResponseStatus Status { get; set; }
    }
}
