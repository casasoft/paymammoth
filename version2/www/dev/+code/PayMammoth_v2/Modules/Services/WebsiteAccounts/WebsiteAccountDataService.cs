﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.RavenDb.Services;
using BusinessLogic_CS_v5.Modules.Services._Shared.BaseDataService;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;

namespace PayMammoth_v2.Modules.Services.WebsiteAccounts
{
    public interface IWebsiteAccountDataService : IBaseDataService<WebsiteAccountData>
    {

    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class WebsiteAccountDataService : BaseDataService<WebsiteAccountData>, IWebsiteAccountDataService
    {
        public WebsiteAccountDataService(
            IDataObjectFactory dataObjectFactory,
            IEntireListOfItemsFromDatabaseRetrieverService entireListOfItemsFromDatabaseRetrieverService) 
            : base(dataObjectFactory,entireListOfItemsFromDatabaseRetrieverService)
        {
        }
    }
}
