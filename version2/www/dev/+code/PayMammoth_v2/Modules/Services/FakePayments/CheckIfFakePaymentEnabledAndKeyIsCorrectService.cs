﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;

namespace PayMammoth_v2.Modules.Services.FakePayments
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface ICheckIfFakePaymentEnabledAndKeyIsCorrectService
    {
        bool CheckIfFakePaymentEnabledAndKeyIsCorrect(WebsiteAccountData websiteAccount,
            string fakePaymentKey);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class CheckIfFakePaymentEnabledAndKeyIsCorrectService : ICheckIfFakePaymentEnabledAndKeyIsCorrectService
    {
        public CheckIfFakePaymentEnabledAndKeyIsCorrectService()
        {

        }

        public bool CheckIfFakePaymentEnabledAndKeyIsCorrect(WebsiteAccountData websiteAccount, string fakePaymentKey)
        {
            bool enabled = false;
            if (!string.IsNullOrWhiteSpace(fakePaymentKey) && websiteAccount.FakePayments != null && websiteAccount.FakePayments.Enabled &&
                websiteAccount.FakePayments.FakePaymentKey == fakePaymentKey)
            {
                enabled = true;
            }
            return enabled;

            
        }
    }
}
