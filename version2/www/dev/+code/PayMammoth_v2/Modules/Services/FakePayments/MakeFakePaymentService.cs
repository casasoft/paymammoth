﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Classes.HelperClasses;
using CS.General_CS_v5.Modules.IPAddress;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v2.Modules.Services.FakePayments
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;
using BusinessLogic_CS_v5.Framework.DbObjects.References;

    public class MakeFakePaymentResponse
    {
        public string RedirectUrl { get; set; }

    }

    public interface IMakeFakePaymentService
    {
        StatusResult<MakeFakePaymentService.MakeFakePaymentResult, MakeFakePaymentResponse> MakeFakePayment(
            ReferenceLink<PaymentRequestData> paymentRequestDataId,
            Connector.Enums.PaymentMethodType paymentMethod,
            string fakePaymentKey,
            IDocumentSession session = null);

    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class MakeFakePaymentService : IMakeFakePaymentService
    {
        

        private readonly ICheckIfFakePaymentEnabledAndKeyIsCorrectService _checkIfFakePaymentEnabledAndKeyIsCorrectService;
        
        private readonly IPaymentRequestTransactionDataService _paymentRequestTransactionDataService;
        private readonly IIPAddressRetrieverService _ipAddressRetrieverService;

        public enum MakeFakePaymentResult
        {
            Success,
            FakePaymentsNotEnabled
            
        }


        public MakeFakePaymentService(ICheckIfFakePaymentEnabledAndKeyIsCorrectService checkIfFakePaymentEnabledAndKeyIsCorrectService,
           
            IPaymentRequestTransactionDataService paymentRequestTransactionDataService,
            IIPAddressRetrieverService ipAddressRetrieverService)
        {
            _ipAddressRetrieverService = ipAddressRetrieverService;
            _paymentRequestTransactionDataService = paymentRequestTransactionDataService;
            _checkIfFakePaymentEnabledAndKeyIsCorrectService = checkIfFakePaymentEnabledAndKeyIsCorrectService;
        }

        [RavenTaskAspect]
        public StatusResult<MakeFakePaymentResult, MakeFakePaymentResponse> MakeFakePayment(
            ReferenceLink<PaymentRequestData> paymentRequestDataId,
            Connector.Enums.PaymentMethodType paymentMethod,
            string fakePaymentKey,
            IDocumentSession session = null)
        {
            MakeFakePaymentService.MakeFakePaymentResult result = MakeFakePaymentResult.FakePaymentsNotEnabled;

            var paymentRequestGetByIdParams = new GetByIdParams<PaymentRequestData>(paymentRequestDataId);
            paymentRequestGetByIdParams.AddInclude(x => x.LinkedWebsiteAccountId);
            paymentRequestGetByIdParams.ThrowErrorIfNotAlreadyLoadedInSession = false;
            var paymentRequest = session.GetById(paymentRequestGetByIdParams);
            var websiteAccountGetByIdParams = new GetByIdParams<WebsiteAccountData>(paymentRequest.LinkedWebsiteAccountId);
            var websiteAccount = session.GetById(websiteAccountGetByIdParams);
            //------
            bool ok =
                _checkIfFakePaymentEnabledAndKeyIsCorrectService.CheckIfFakePaymentEnabledAndKeyIsCorrect(
                    websiteAccount,
                    fakePaymentKey);

            if (ok)
            {
                //todo: [For: Karl | 2014/05/15] update this such that it calls a method which adds a payment transaction, with methods, and saves it (WrittenBy: Karl)  
                //one shouldn't have to do 'SaveChanges' here

                result = MakeFakePaymentResult.Success;
                var transaction = _paymentRequestTransactionDataService.CreateNewTransaction(paymentRequestDataId,
                    paymentMethod,
                    session);
                transaction.Comments = "Fake Payment";
                transaction.FakePaymentKeyUsed = fakePaymentKey;
                transaction.IsFakePayment = true;
                transaction.IpAddress = _ipAddressRetrieverService.GetIPAddress(ipv4: true);

                session.Store(transaction);
                session.SaveChanges();
                _paymentRequestTransactionDataService.MarkTransactionAsPaid(transaction,
                    requiresManualIntervention: false,
                    authorisationCode: "FakePayment-AuthCode",
                    session: session);



            }

            var methodResult = new StatusResult<MakeFakePaymentResult, MakeFakePaymentResponse>();
            methodResult.Status = result;
            methodResult.Result = new MakeFakePaymentResponse();
            methodResult.Result.RedirectUrl = paymentRequest.RequestDetails.ReturnUrls.SuccessUrl;

            return  methodResult;
        }
    }
}
