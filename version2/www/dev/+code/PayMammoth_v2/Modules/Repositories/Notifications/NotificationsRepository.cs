﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DateAndTime;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Modules.Data.Notifications;
using Raven.Client;
using Raven.Client.Linq;

namespace PayMammoth_v2.Modules.Repositories.Notifications
{
    public interface INotificationMessageDataRepository
    {
        List<NotificationMessageData> GetAllNotificationsYetToBeSent(
            IDocumentSession documentSession,
            int pgSize);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class NotificationMessageDataRepository : INotificationMessageDataRepository
    {
        private readonly ICurrentDateTimeRetrieverService _currentDateTimeRetrieverService;

        public NotificationMessageDataRepository(
            ICurrentDateTimeRetrieverService currentDateTimeRetrieverService)
        {
            _currentDateTimeRetrieverService = currentDateTimeRetrieverService;
        }

        public List<NotificationMessageData> GetAllNotificationsYetToBeSent(
            IDocumentSession documentSession,
            int pgSize)
        {
            var currentDate = _currentDateTimeRetrieverService.GetCurrentDateTime();
            var query = documentSession.Query<NotificationMessageData, NotificationMessageDataIndex>();
            query = query.Where(x => x.NextRetryOn <= currentDate && x.Status == Connector.Enums.NotificationMessageStatus.Pending);
            var list = query.Take(pgSize).ToList();
            return list;
        }
    }
}