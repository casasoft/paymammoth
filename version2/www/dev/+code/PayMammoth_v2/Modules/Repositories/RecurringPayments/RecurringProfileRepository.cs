﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Modules.Data.RecurringProfiles;

namespace PayMammoth_v2.Modules.Repositories.RecurringPayments
{
    public interface IRecurringProfileRepository
    {
        RecurringProfileData GetRecurringProfileByGatewayIdentifier(string gatewayIdentifier, Connector.Enums.PaymentMethodType gateway);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class RecurringProfileRepository : IRecurringProfileRepository
    {
        public RecurringProfileRepository()
        {
            
        }
    
        public RecurringProfileData GetRecurringProfileByGatewayIdentifier(string gatewayIdentifier, Connector.Enums.PaymentMethodType gateway)
        {
            //todo: [For: Karl | 2014/01/07] implement RecurringPaymentsRepository.GetRecurringProfileByGatewayIdentifier (WrittenBy: Karl)
 	        //  var q = GetQuery();
            //q.Where(item => item.PaymentGateway == gateway && item.ProfileIdentifierOnGateway == gatewayIdentifier);
            //q.Take(1);
            //return FindItem(q);
            return null;
        }
    }
}
