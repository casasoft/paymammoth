﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Modules.Services.Repositories;
using BusinessLogic_CS_v5.Util;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Modules.Data.Payments;
using Raven.Client;

namespace PayMammoth_v2.Modules.Repositories.Payments
{
    public interface IPaymentMethodsRepository
    {
        List<PaymentMethodData> GetAllPaymentMethods(IDocumentSession session); 
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentMethodsRepository : IPaymentMethodsRepository
    {
        private readonly IRepositoryService _repositoryService;

        public PaymentMethodsRepository(IRepositoryService repositoryService)
        {
            _repositoryService = repositoryService;
        }

        public List<PaymentMethodData> GetAllPaymentMethods(IDocumentSession session)
        {
            //todo: [For: Karl | 2014/01/14] create unit tests PaymentMethodsRepository.GetAllPaymentMethods (WrittenBy: Karl)
            var q = session.Query<PaymentMethodData>();
            q=_repositoryService.AddConditionsToQueryToLoadOnlyAvailableForFrontend(q);
            var list = q.ToList();
            return list;
        }
    }
}
