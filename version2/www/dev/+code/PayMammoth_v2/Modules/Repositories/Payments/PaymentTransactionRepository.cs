﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Modules.Data.Payments;

namespace PayMammoth_v2.Modules.Repositories.Payments
{
    public interface IPaymentTransactionRepository
    {
        PaymentRequestTransactionData GetLatestTransactionForPaymentRequest(ReferenceLink<PaymentRequestData> requestId,
            Connector.Enums.PaymentMethodType paymentMethodType); 
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PaymentTransactionRepository : IPaymentTransactionRepository
    {
        public PaymentTransactionRepository()
        {

        }

        public PaymentRequestTransactionData GetLatestTransactionForPaymentRequest(ReferenceLink<PaymentRequestData> requestId,
            Connector.Enums.PaymentMethodType paymentMethodType)
        {
            
            //todo: [For: Karl | 2014/01/07] implement PaymentTransactionRepository.GetLatestTransactionForPaymentRequest (WrittenBy: Karl)
            throw new NotImplementedException();
        }
    }
}
