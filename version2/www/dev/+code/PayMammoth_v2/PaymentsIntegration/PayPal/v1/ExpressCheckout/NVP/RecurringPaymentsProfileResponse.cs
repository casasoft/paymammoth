﻿using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class RecurringPaymentsProfileResponse : BasePayPalResponse
    {
        /// <summary>
        /// A unique identifier for future reference to the details of this recurring payment.
        /// </summary>
        [PayPalFieldInfo(FieldName = "PROFILEID")]
        public string ProfileId { get; set; }

        [PayPalFieldInfo(FieldName = "PROFILESTATUS")]
        protected string profileStatus { get; set; }

        public bool IsGatewayFailure
        {
            get
            {
                return ProfileId == null || profileStatus == null;
            }
        }

        [PayPalFieldInfo(FieldName = "L_LONGMESSAGE0")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Status of the recurring payment profile.
        /// </summary>
        public PayPalEnums.RECURRING_AGREEMENT_STATUS ProfileStatus
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.RECURRING_AGREEMENT_STATUS>(profileStatus).GetValueOrDefault();
                
            }
            set { profileStatus = value.ToString(); }
        }
    }
}
