﻿namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses
{
    public interface IPayPalSettings
    {
        IPayPalCredentials SandboxCredentials { get;  }
        IPayPalCredentials LiveCredentials { get; }
        bool UseLiveEnvironment { get; }
        
        
    }
}
