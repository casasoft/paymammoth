﻿using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class AddressInfo : BasePayPalObject
    {
        [PayPalFieldInfo(FieldName = "ShipToName", MaxLength = 32)]
        public string CustomerName
        {
            get { return CS.General_CS_v5.Util.TextUtil.AppendStrings(" ", this._clientFirstName, _clientMiddleName, _clientLastName); }
            set 
            {
                _clientFirstName = value;
                _clientLastName = null;
                _clientMiddleName = null;
            }
        }
        [PayPalFieldInfo(FieldName = "ShipToStreet", MaxLength = 100)]
        public string Street1 { get; set; }
        [PayPalFieldInfo(FieldName = "ShipToStreet2", MaxLength = 100)]
        public string Street2
        {
            get { return CS.General_CS_v5.Util.TextUtil.AppendStrings(", ", _street2, _street3); }
            set
            {
                _street2 = value;
                _street3 = null;
            }

        }
        [PayPalFieldInfo(FieldName = "ShipToCity", MaxLength = 40)]
        public string City { get; set; }
        [PayPalFieldInfo(FieldName = "ShipToState", MaxLength = 40)]
        public string State { get; set; }
        [PayPalFieldInfo(FieldName = "ShipToZIP", MaxLength = 20)]
        public string ZIPCode { get; set; }

        [PayPalFieldInfo(FieldName = "ShipToCountryCode", MaxLength = 2)]
        private string _CountryCode_Str { get; set; }
        public CS.General_CS_v5.Enums.CountryIso3166? CountryCode
        {
            get
            {
                return (CS.General_CS_v5.Enums.CountryIso3166?)CS.General_CS_v5.Enums.CountryIso3166FromCode(_CountryCode_Str);
            }
            set
            {
                _CountryCode_Str = CS.General_CS_v5.Enums.Country_ISO3166_To2LetterCode(value);

            }
        }


        [PayPalFieldInfo(FieldName = "ShipToPhoneNum", MaxLength = 32)]
        public string ClientPhoneNumber { get; set; }


        private string _clientFirstName;
        private string _clientMiddleName;
        private string _clientLastName;

        private string _street2;
        private string _street3;

       


        #region IClientDetails Members


        public string ClientReference { get; set; }

        #endregion
    }
        
}
