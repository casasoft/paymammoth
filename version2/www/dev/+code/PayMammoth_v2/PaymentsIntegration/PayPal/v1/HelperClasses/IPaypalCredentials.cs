﻿namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses
{
    public interface IPayPalCredentials
    {
        string Username { get;  }
        string Password { get;  }
        string Signature { get;  }
        string MerchantEmail { get; }
       // string MerchantId { get; }
        string NvpServerUrl { get; }
        string ApiServerUrl { get; }
    }
}
