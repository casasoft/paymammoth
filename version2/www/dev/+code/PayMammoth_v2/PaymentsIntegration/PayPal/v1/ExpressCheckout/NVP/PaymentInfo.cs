﻿using System.Collections.Generic;
using System.Collections.Specialized;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class PaymentInfo 
    {
        [PayPalFieldInfo(FieldName = "TransactionID")]
        public string TransactionID {get;set;}

        
        [PayPalFieldInfo(FieldName = "PaymentType")]
        public string PaymentType { get; set; }

        [PayPalFieldInfo(FieldName = "OrderTime")]
        public string OrderTime { get; set; }

        [PayPalFieldInfo(FieldName = "Amt")]
        public double TotalAmountChargedtoClient { get; set; }

        /// <summary>
        /// A three-character currency code. Default: USD
        /// </summary>
        [PayPalFieldInfo(FieldName = "CurrencyCode")]
        protected string _currencyCode { get; set; }
        public CS.General_CS_v5.Enums.CurrencyCode_Iso4217 CurrencyCode
        {
            get
            {
                return CS.General_CS_v5.Enums.CurrencyCode_Iso4217FromCode(_currencyCode).Value;
                
            }
            set
            {
                _currencyCode = CS.General_CS_v5.Util.EnumUtil.ConvertEnumToString(value);
            }
        }

        [PayPalFieldInfo(FieldName = "FeeAmt")]
        public double? PayPalFeeAmount { get; set; }

        /// <summary>
        /// Amount deposited in your PayPal account after a currency conversion
        /// </summary>
        [PayPalFieldInfo(FieldName = "SettleAmt")]
        public double? TotalAmountDepositedInMerchantAccount { get; set; }

        /// <summary>
        /// Tax charged on the transaction.
        /// </summary>
        [PayPalFieldInfo(FieldName = "TaxAmt")]
        public double? TotalTaxAmount { get; set; }

        [PayPalFieldInfo(FieldName = "ExchangeRate")]
        public double? ExchangeRate { get; set; }


        [PayPalFieldInfo(FieldName = "PaymentStatus")]
        protected string _PaymentStatus { get; set; }
        public PayPalEnums.PAYMENT_STATUS PaymentStatus
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.PAYMENT_STATUS>(_PaymentStatus).GetValueOrDefault();
            }
        }


        [PayPalFieldInfo(FieldName = "PendingReason")]
        protected string _PendingReason { get; set; }

        [PayPalFieldInfo(FieldName = "ReasonCode")]
        protected string _ReasonCode { get; set; }

        /// <summary>
        /// Prior to Version 64.4, the kind of seller protection in force for the transaction, which
///is one of the following values:
/// Eligible – Seller is protected by PayPal's Seller Protection Policy for
///nauthorized Payments and Item Not Received
/// PartiallyEligible – Seller is protected by PayPal's Seller Protection Policy
///for Item Not Received
/// Ineligible – Seller is not protected under the Seller Protection Policy
        /// </summary>
        [PayPalFieldInfo(FieldName = "ProtectionEligibility")]
        protected string _ProtectionEligibility { get; set; }

        [PayPalFieldInfo(FieldName = "ProtectionEligibilityType")]
        protected string _ProtectionEligibilityType { get; set; }

        [PayPalFieldInfo(FieldName = "EbayItemAuctionTxnID")]
        public string EbayItemAuctionTxnID { get; set; }
        
        public ResponseStatusInfo ResponseStatusInfo { get; private set; }
        public UserSelectedOptionsInfo UserSelectedOptions {get; private set;}
        public SellerDetailsInfo SellerDetails {get; private set;}

        public PaymentInfo()
        {
            //this.ClientShippingInfo = new AddressTypeInfo();
            this.SellerDetails = new SellerDetailsInfo();
            this.UserSelectedOptions = new UserSelectedOptionsInfo();
            this.ResponseStatusInfo = new ResponseStatusInfo();
        }

        internal static string getPrependKey(int id)
        {
            return "PaymentInfo_" + id + "_";
        }

        public void ParseFromNameValueCollection(NameValueCollection nv, int id)
        {
            string keyPrepend = getPrependKey(id);
            Util.GeneralUtil.FillObjectFromPaypalNameValueColl(nv, this, keyPrepend, null);
            
            
            this.ResponseStatusInfo = new ResponseStatusInfo(); this.ResponseStatusInfo.ParseFromNameValueCollection(nv, keyPrepend, null);
            this.SellerDetails = new SellerDetailsInfo(); this.SellerDetails.ParseFromNameValueCollection(nv, keyPrepend, null);
            

        }
        protected bool isValid()
        {
            return (!string.IsNullOrEmpty(this.TransactionID));
        }

        public static List<PaymentInfo> ParseFromNameValueCollection(NameValueCollection nv)
        {
            List<PaymentInfo> list = new List<PaymentInfo>();
            for (int i = 0; i < 9; i++)
            {
                string prependKey = getPrependKey(i);
                PaymentInfo info = new PaymentInfo();
                info.ParseFromNameValueCollection(nv, i);
                if (info.isValid())
                    list.Add(info);
            }
            return list;
        }
    }
        
}
