﻿using PayMammoth_v2.Framework.Payments;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.Interfaces
{
    public interface IPayPalRecurringProfileManager : IRecurringProfileManager
    {
    }
}
