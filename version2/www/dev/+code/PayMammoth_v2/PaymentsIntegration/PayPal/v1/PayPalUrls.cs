﻿namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1
{
    public static class PayPalUrls
    {
        //public static string IpnHandlerUrl { get { return CS.General_CS_v5.Util.PageUtil.GetApplicationBaseUrl() + "payment/paypal/v1/ipn.ashx"; } }
        //public static string ConfirmUrl { get { return CS.General_CS_v5.Util.PageUtil.GetApplicationBaseUrl() + "payment/paypal/v1/confirm.aspx"; } }

        public static string IpnHandlerUrl { get { return CS.General_CS_v5.Util.PageUtil.GetApplicationBaseUrl() + "_tests/paypal/paypalIpn.ashx"; } }
        //public static string ConfirmUrl { get { return CS.General_CS_v5.Util.PageUtil.GetApplicationBaseUrl() + "_tests/paypal/paypalTestConfirm.aspx"; } }

    }
}
