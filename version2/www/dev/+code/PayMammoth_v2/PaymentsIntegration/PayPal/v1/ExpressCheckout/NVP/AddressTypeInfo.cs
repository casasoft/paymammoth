﻿using System.Collections.Specialized;
using CS.General_CS_v5.Modules.Urls;
using CS.General_CS_v5.Util;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.Util;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    /// <summary>
    /// Deprecated
    /// </summary>
    public class AddressTypeInfo
    {

        public string ClientName { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientAddress1 { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientAddress2 { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientCity { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientState { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientZIPCode { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientCountry { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientCountryName { get; set; }

        public string ShipToPhoneNum { get; set; }
        public CS.General_CS_v5.Enums.CountryIso3166 ClientCountryCode { get; set; }

        public void AppendToNameValue(NameValueCollection nv, int id)
        {
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToName", this.ClientName);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToStreet", this.ClientAddress1);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToStreet2", ClientAddress2);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToCity", ClientCity);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToState", ClientState);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToZIP", ClientZIPCode);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToCountry", ClientCountry);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToPhoneNum", this.ShipToPhoneNum);
        }


        public void ParseInfoFromQueryString(QueryString qs)
        {
            this.ClientAddress1 =PageUtil.UrlDecode(qs["ShipToStreet"]);
            this.ClientAddress2 = "";
            this.ClientCity = PageUtil.UrlDecode(qs["ShipToCity"]);
            this.ClientState = PageUtil.UrlDecode(qs["ShipToState"]);
            this.ClientZIPCode = PageUtil.UrlDecode(qs["ShipToZip"]);
            this.ClientCountry = PageUtil.UrlDecode(qs["ShipToCountryCode"]);
            this.ClientCountryCode = CS.General_CS_v5.Enums.CountryIso3166FromCode(PageUtil.UrlDecode(qs["ShipToCountryCode"])).Value;
            this.ClientCountryName = PageUtil.UrlDecode(qs["ShipToCountryName"]);

        }
    }
        
}
