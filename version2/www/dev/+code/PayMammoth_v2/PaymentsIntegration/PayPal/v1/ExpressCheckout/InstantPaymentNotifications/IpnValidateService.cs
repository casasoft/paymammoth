﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using CS.General_CS_v5.Modules.InversionOfControl;
using CS.General_CS_v5.Modules.Web;
using NLog;
using BusinessLogic_CS_v5.Modules.Services.Settings;
using Raven.Client;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public interface IIpnValidateService
    {
        IpnResponse ValidateIpnMessage(IpnMessage msg, IDocumentSession session);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class IpnValidateService : IIpnValidateService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly ISettingsService _settingsService;
        private readonly IHttpRequestSenderService _httpRequestSenderService;

        public IpnValidateService(ISettingsService settingsService, IHttpRequestSenderService httpRequestSenderService)
        {
            _httpRequestSenderService = httpRequestSenderService;
            _settingsService = settingsService;
        }

        public IpnResponse ValidateIpnMessage(IpnMessage msg, IDocumentSession session)
        {
            string apiServerUrl = null;
            if (msg.IsTestIpn)
            {
                apiServerUrl = _settingsService.GetSettingValue<string>(session, PayMammoth_v2.Enums.PayMammoth_v2Enums.PayMammothSettings.PayPal_v1_Sandbox_ApiServerUrl);
            }
            else
            {
                apiServerUrl = _settingsService.GetSettingValue<string>(session, PayMammoth_v2.Enums.PayMammoth_v2Enums.PayMammothSettings.PayPal_v1_Live_ApiServerUrl);
            }
            //todo: [For: Karl | 2014/03/26] switch these to use PayPal IPN (WrittenBy: Karl)        			
            IpnResponse ipnResponse = new IpnResponse();
            string msgBlock = msg.MessageBlockRaw;
            string ipnMsg = msgBlock + "&" + PayPalConstants.PARAM_CMD + "=" + PayPalConstants.COMMAND_NOTIFY_VALIDATE;
            //todo: [For: Karl | 2014/01/22] add try-catch here (WrittenBy: Karl)        			
            var httpResponse = _httpRequestSenderService.SendHttpRequest(apiServerUrl, CS.General_CS_v5.Enums.HttpMethod.POST, null,
                ipnMsg, null);
            string response = null;
            {
                try
                {
                    Stream responseStream = httpResponse.GetResponseStream();
                    if (responseStream != null)
                    {
                        StreamReader sr = new StreamReader(responseStream);
                        response = sr.ReadToEnd();
                        sr.Close();
                    }
                }
                catch (Exception ex)
                {
                    _log.Warn("Error occured while trying to get response stream", ex);
                }

            }
            if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                if (response == PayPalConstants.IPN_VERIFIED)
                {
                    ipnResponse.Success = true;

                    if (_log.IsDebugEnabled) _log.Debug("ValidateIPNMessage() - IPN_Verified");
                }
                else
                {
                    _log.Warn("ValidateIPNMessage() - IPN Not verified");
                }
            }
            else
            {
                if (_log.IsWarnEnabled) _log.Warn("ValidateIPNMessage() - Status code shows error - [" + httpResponse.StatusCode + "]");

            }
            ipnResponse.PaypalResponse = response;
            return ipnResponse;

        }
    }
}
