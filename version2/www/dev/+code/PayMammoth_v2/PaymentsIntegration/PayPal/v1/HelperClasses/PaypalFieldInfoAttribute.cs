﻿using System;
using PayMammoth_v2.Framework.Attributes;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses
{
    [AttributeUsage(AttributeTargets.Property)]
    public class PayPalFieldInfoAttribute : PaymentInfoFieldAttribute
    {


        protected override string getStringForPaymentServerFromValue(object value, bool validateValueBefore)
        {
            return GetPaypalValue(value);
            
        }
        public string GetPaypalValue(object value)
        {
            if (value is int || value is double || value is float || value is Int32 || value is long || value is byte || value is short || value is Int64)
            {
                double d = Convert.ToDouble(value);
                if (d > MaxAmount)
                {
                    throw new InvalidOperationException(this.FieldName + " - Value must not be larger than " + this.MaxAmount.ToString("0.00") + " <" + d + ">");
                }
            }
            

            string s = Util.GeneralUtil.ConvertObjectToPayPal(value);
            if (s != null && s.Length > MaxLength)
            {
                throw new InvalidOperationException(this.FieldName + " - Value must not be longer than " + MaxLength + " characters <" + s + ">");
            }
            return s;
        }
    }
}
