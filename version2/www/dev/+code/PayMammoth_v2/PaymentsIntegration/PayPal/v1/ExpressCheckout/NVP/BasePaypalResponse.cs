﻿using System.Collections.Specialized;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public abstract class BasePayPalResponse
    {

        public string RawResponse { get; set; }

        public PayPalEnums.ACKNOWLEDGEMENT? Acknowledgement
        {
            get
            {
                return (CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.ACKNOWLEDGEMENT>(_acknowledgement));
            }
        }
        [PayPalFieldInfo(FieldName = "ack")]
        protected string _acknowledgement { get; set; }


        public bool Success
        {
            get
            {
                return (this.Acknowledgement.HasValue && (this.Acknowledgement.Value == PayPalEnums.ACKNOWLEDGEMENT.Success || this.Acknowledgement == PayPalEnums.ACKNOWLEDGEMENT.SuccessWithWarning));
            }
        }
        [PayPalFieldInfo(FieldName="CorrelationID")]
        public string CorrelationID { get; set; }
        [PayPalFieldInfo(FieldName = "TimeStamp")]
        public string TimeStamp { get; set; }
        [PayPalFieldInfo(FieldName = "Version")]
        public string Version { get; set; }
        [PayPalFieldInfo(FieldName = "Build")]
        public string Build { get; set; }

        /// <summary>
        /// Contains information about the success of the response and any error information
        /// </summary>
        public ResponseStatusInfo ResponseStatusInfo { get; private set; }

        public virtual void ParseFromNameValueCollection(NameValueCollection nv)
        {
            
            Util.GeneralUtil.FillObjectFromPaypalNameValueColl(nv, this, null, null);

            this.ResponseStatusInfo = new ResponseStatusInfo();
            this.ResponseStatusInfo.ParseFromNameValueCollection(nv, "L_", "0");


           /* TimeStamp = qs["timestamp"];
            Version = qs["version"];
            Build = qs["build"];
            Acknowledgement = (Enums.ACKNOWLEDGEMENT) Enums.AcknowledgementFromPaypalCode(qs["ack"]);
            CorrelationID = qs["CorrelationID"];
            ErrorCode = qs["L_ErrorCode0"];
            ShortMessage = qs["L_ShortMessage0"];
            LongMessage = qs["L_LongMessage0"];
            SeverityCode = qs["L_SeverityCode0"];*/

        }

        //public void UpdateTransactionWithInfo(IPaymentRequestTransaction t)
        //{
        //    //todo: [For: Karl | 2014/01/07] This is not needed anymore - probably delete (WrittenBy: Karl)        			
        //    t.SetPaymentParameters("CorrelationId", this.CorrelationID, "Timestamp", this.TimeStamp, "Version", this.Version);
        //    //t.SaveAndCommit();
        //}

    }
        
}
