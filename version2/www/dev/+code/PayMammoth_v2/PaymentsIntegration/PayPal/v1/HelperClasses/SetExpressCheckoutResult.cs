﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP;
using PayPal.PayPalAPIInterfaceService.Model;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses
{
    public class SetExpressCheckoutResult
    {
        public SetExpressCheckoutResponseType PayPalResponse { get; set; }
        public string UrlToRedirectTo { get; set; }
    }
}
