﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IGetIpnMessageParserBasedOnTransactionTypeService
    {
        IIpnMessageParser GetIpnMessageParserBasedOnTransactionType(PayPalEnums.TRANSACTION_TYPE transactionType);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class GetIpnMessageParserBasedOnTransactionTypeService : IGetIpnMessageParserBasedOnTransactionTypeService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IExpressCheckoutIpnMessageParser _expressCheckoutIpnMessageParser;
        private readonly IRecurringPaymentIpnMessageParser _recurringPaymentIpnMessageParser;

        public GetIpnMessageParserBasedOnTransactionTypeService(IExpressCheckoutIpnMessageParser expressCheckoutIpnMessageParser,
            IRecurringPaymentIpnMessageParser recurringPaymentIpnMessageParser)
        {
            _recurringPaymentIpnMessageParser = recurringPaymentIpnMessageParser;
            _expressCheckoutIpnMessageParser = expressCheckoutIpnMessageParser;
        }

        public IIpnMessageParser GetIpnMessageParserBasedOnTransactionType(PayPalEnums.TRANSACTION_TYPE transactionType)
        {



            switch (transactionType)
            {
                case PayPalEnums.TRANSACTION_TYPE.Cart:
                case PayPalEnums.TRANSACTION_TYPE.ExpressCheckout: return _expressCheckoutIpnMessageParser;
               
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentExpired:
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentProfileCreated:
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentCancelled:
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentReceived:
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentSkipped:
                    return _recurringPaymentIpnMessageParser;
                default:
                    {
                        _log.Warn(string.Format("No Ipn message parser found for transaction type <{0}>", transactionType));
                    }
                    break;
            }
            return null;
        }
    }
}
