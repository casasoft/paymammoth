﻿namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public class IpnResponse
    {
        public IpnResponse()
        {
            
        }

        public IpnMessage IpnMessage { get; internal set; }
        public bool Success { get; internal set; }

        public string PaypalResponse { get; set; }
    }
}
