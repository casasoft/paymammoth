﻿using BusinessLogic_CS_v5.Aspects;
using BusinessLogic_CS_v5.Aspects.NullAspects;

using BusinessLogic_CS_v5.Aspects.NullAspects;
using CS.General_CS_v5.Modules.InversionOfControl;
using NLog;
using PayMammoth_v2.Modules.Data.RecurringProfiles;
using PayMammoth_v2.Modules.Repositories.RecurringPayments;
using Raven.Client;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers
{
    public interface IRecurringPaymentIpnMessageParser : IIpnMessageParser
    {

    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class RecurringPaymentIpnMessageParser : IRecurringPaymentIpnMessageParser
    {
        public RecurringPaymentIpnMessageParser(IRecurringProfileRepository recurringProfileRepository)
        {
            _recurringProfileRepository = recurringProfileRepository;
        }


        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IRecurringProfileRepository _recurringProfileRepository;

        #region IIpnMessageParser Members

        private void parseMessage(IpnMessage ipnMessage,RecurringProfileData profile)
        {
            //todo: [For: Karl | 2014/01/07] these need to be copied from old PayMammoth (WrittenBy: Karl)        			
            //switch (ipnMessage.TransactionType)
            //{
            //    case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentProfileCreated:
            //        {
            //            RecurringProfileService.Instance.ActivateProfile(profile,ipnMessage.MessageBlockRaw);
            //        }
            //        break;
            //    case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentCancelled:
            //        {

            //            RecurringProfileService.Instance.MarkProfileAsCancelled(profile, ipnMessage.MessageBlockRaw);
            //            break;
            //        }
            //    case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentSuspendedDueToMaxFailedPayment:
            //        {
            //            profile.SuspendProfile(ipnMessage.MessageBlockRaw);
            //        }
            //        break;
            //    case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentExpired:
            //        {
            //            RecurringProfileService.Instance.MarkProfileAsExpired(profile, ipnMessage.MessageBlockRaw);
                        
            //        }
            //        break;
            //    case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentReceived:
            //        {
            //            RecurringProfileService.Instance.MarkPaymentReceived(profile, ipnMessage.PayPalTransactionID, ipnMessage.MessageBlockRaw);
            //        }
            //        break;
            //    case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentFailed:
            //        {
            //            RecurringProfileService.Instance.MarkPaymentAsFailed(profile, ipnMessage.PayPalTransactionID, ipnMessage.MessageBlockRaw);
            //        }
            //        break;
                
                    
            //    case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentSkipped:
            //        {
            //            RecurringProfileService.Instance.MarkLastPaymentAsSkipped(profile, ipnMessage.PayPalTransactionID, ipnMessage.MessageBlockRaw);
                        
            //            break;
            //        }
            //    default:
            //        throw new InvalidOperationException(string.Format("This parser is not meant for this type of message <{0}>", ipnMessage.TransactionType));
            //}
        }

        [RavenTaskAspect]
        public void HandleIpnMessage(IpnMessage ipnMessage, IDocumentSession session)
        {
            
            string profileId = ipnMessage.RecurringPaymentPaymentId;



            var profile = _recurringProfileRepository.GetRecurringProfileByGatewayIdentifier(profileId, Connector.Enums.PaymentMethodType.PayPal_ExpressCheckout);

            if (profile != null)
            {
                
                parseMessage(ipnMessage, profile);
              
                
            }
            else
            {
                _log.Warn("Could not locate profile for PayPal with gateway ID [{0}]", ipnMessage.RecurringPaymentPaymentId);
            }
           

            
        }

        #endregion
    }
}
