﻿using System.Collections.Specialized;
using CS.General_CS_v5.Modules.Urls;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class GetExpressCheckoutRequest : BasePayPalRequest
    {
        
        public GetExpressCheckoutRequest(PayPalClient paypal, string token) : base(paypal,"GetExpressCheckoutDetails")
        {
            this.Token = token;
            
        }

        [PayPalFieldInfo]
        public string Token { get; set; }
        
        
        public GetExpressCheckoutResponse GetResponse()
        {

            NameValueCollection nv = createNameValueCollectionWithPayPalInfo();

            string sResponse = getResponse(nv);
            GetExpressCheckoutResponse response = new GetExpressCheckoutResponse();
            
            var qs = new QueryString(sResponse);
            response.ParseFromNameValueCollection(qs);
            return response;
        }
    }   
}
