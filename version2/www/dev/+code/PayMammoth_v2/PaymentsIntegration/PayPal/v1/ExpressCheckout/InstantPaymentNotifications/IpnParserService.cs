﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BusinessLogic_CS_v5.Aspects;
using BusinessLogic_CS_v5.EnumsBL;

using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Modules.InversionOfControl;
using CS.General_CS_v5.Modules.Urls;
using NLog;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Services.Logging;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers;
using Raven.Client;
using PayPalCoreSDK = PayPal;
namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public interface IIpnParserService
    {
        bool ParseIpnMessage(ReferenceLink<PaymentRequestData> paymentRequestId, string formContents, string qsContents, IDocumentSession session);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class IpnParserService : IIpnParserService
    {

        private static readonly ConcurrentDictionary<string, bool> _processedPayPalIpnTransactionIds = new ConcurrentDictionary<string, bool>();

        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IPayMammothLogService _payMammothLogService;
        private readonly IIpnValidateService _ipnValidateService;
        private readonly IGetIpnMessageParserBasedOnTransactionTypeService _getIpnMessageParserBasedOnTransactionTypeService;
        private readonly IProcessIpnMessageIfNotAlreadyProcessedService _processIpnMessageIfNotAlreadyProcessedService;

        public IpnParserService(IPayMammothLogService payMammothLogService, 
            IIpnValidateService ipnValidateService,
            IGetIpnMessageParserBasedOnTransactionTypeService getIpnMessageParserBasedOnTransactionTypeService,
            IProcessIpnMessageIfNotAlreadyProcessedService processIpnMessageIfNotAlreadyProcessedService)
        {
            _processIpnMessageIfNotAlreadyProcessedService = processIpnMessageIfNotAlreadyProcessedService;
            _getIpnMessageParserBasedOnTransactionTypeService = getIpnMessageParserBasedOnTransactionTypeService;
            _ipnValidateService = ipnValidateService;
            
            _payMammothLogService = payMammothLogService;
            
        }


       
        private void addIpnResponseToTransaction(QueryString qs, IpnResponse response)
        {

            qs.Add("Ipn PayPal Response", response.PaypalResponse);



        }
        [RavenTaskAspect]
        public bool ParseIpnMessage(ReferenceLink<PaymentRequestData> paymentRequestId, string formContents, string qsContents, IDocumentSession session)
        {
            var paymentRequest = session.GetById(new GetByIdParams<PaymentRequestData>(paymentRequestId) { ThrowErrorIfNotAlreadyLoadedInSession = false });
            //----------
            
            
            QueryString formContentsParsed = new QueryString(formContents);



            _payMammothLogService.AddLogEntry(paymentRequest.StatusLog, 
                string.Format("ProcessRequest() - Started. Querystring: {0} | Form: {1}", qsContents ,formContents),_log);

            IpnMessage ipnMessage = new IpnMessage(formContents);
            bool validationOk = false;

            ipnMessage.FillNameValueCollection(formContentsParsed, prepend: null, append: null);
            var validateResult = _ipnValidateService.ValidateIpnMessage(ipnMessage,session);
            bool result = false;
            if (validateResult.Success)
            {
                result = true;
                CS.General_CS_v5.Util.ThreadUtil.CallMethodAsAsyncTask(() => _processIpnMessageIfNotAlreadyProcessedService.Process(paymentRequestId, ipnMessage, null));
               
            }
            else
            {
                _payMammothLogService.AddLogEntry(paymentRequest.StatusLog, string.Format("ProcessRequest() - Could not validate IPN message - Form contents: {0}", formContents), _log,
                            GenericEnums.NlogLogLevel.Warn);

            }
            return result;
           
        }
        [RavenTaskAspect]
        public bool ParseIpnMessage_old(ReferenceLink<PaymentRequestData> paymentRequestId, string formContents, string qsContents, IDocumentSession session)
        {
            var paymentRequest = session.GetById(new GetByIdParams<PaymentRequestData>(paymentRequestId) { ThrowErrorIfNotAlreadyLoadedInSession = false });
            //----------


            QueryString formContentsParsed = new QueryString(formContents);
            //string formContents = context.Request.Form.ToString();
            //formContents = CS.General_v3.Util.ListUtil.ConvertNameValueCollectionToString(context.Request.Form);



            _payMammothLogService.AddLogEntry(paymentRequest.StatusLog,
                string.Format("ProcessRequest() - Started. Querystring: {0} | Form: {1}", qsContents, formContents), _log);

            //var formNv = HttpUtility.ParseQueryString(formContents);
            //var formQs = HttpUtility.ParseQueryString(formContents);


            //IPNM//context.Response.Clear();
            //PayPal.v1..IPNMessage
            IpnMessage ipnMessage = new IpnMessage(formContents);
            bool validationOk = false;

            ipnMessage.FillNameValueCollection(formContentsParsed, prepend: null, append: null);
            var validateResult = _ipnValidateService.ValidateIpnMessage(ipnMessage, session);
            bool result = false;
            if (validateResult.Success)
            {
                validationOk = true;

                if (!string.IsNullOrWhiteSpace(ipnMessage.IpnTrackId) && !_processedPayPalIpnTransactionIds.ContainsKey(ipnMessage.IpnTrackId))
                {

                    //todo: [For: Karl | 2014/01/07] This needs to be stored in database (WrittenBy: Karl)        			

                    _processedPayPalIpnTransactionIds[ipnMessage.IpnTrackId] = true;
                    var msgParser = _getIpnMessageParserBasedOnTransactionTypeService.GetIpnMessageParserBasedOnTransactionType(ipnMessage.TransactionType);
                    if (msgParser != null)
                    {
                        _payMammothLogService.AddLogEntry(paymentRequest.StatusLog, string.Format("ProcessRequest() - Handling Message"), _log);



                        msgParser.HandleIpnMessage(ipnMessage);
                        _payMammothLogService.AddLogEntry(paymentRequest.StatusLog, string.Format("ProcessRequest() - Handling Message - Result: {0}", result), _log);

                        if (result)
                        {
                            _payMammothLogService.AddLogEntry(paymentRequest.StatusLog, string.Format("ProcessRequest() - IPN_OK"), _log);
                        }
                        else
                        {
                            _payMammothLogService.AddLogEntry(paymentRequest.StatusLog, string.Format("ProcessRequest() - IPN_FAIL"), _log,
                                GenericEnums.NlogLogLevel.Warn);
                        }
                        _payMammothLogService.AddLogEntry(paymentRequest.StatusLog, string.Format("ProcessRequest() - Finished"), _log);

                    }
                    else
                    {
                        _payMammothLogService.AddLogEntry(paymentRequest.StatusLog, string.Format("ProcessRequest() - Could not process IPN message as no message parser exists. Form contents: {0}", formContents), _log,
                             GenericEnums.NlogLogLevel.Warn);

                    }
                }
            }
            else
            {
                _payMammothLogService.AddLogEntry(paymentRequest.StatusLog, string.Format("ProcessRequest() - Could not validate IPN message - Form contents: {0}", formContents), _log,
                            GenericEnums.NlogLogLevel.Warn);

            }
            return result;

        }

    }
}
