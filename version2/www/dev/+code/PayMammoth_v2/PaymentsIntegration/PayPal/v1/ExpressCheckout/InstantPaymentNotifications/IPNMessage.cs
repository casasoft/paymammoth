﻿using System;
using CS.General_CS_v5.Modules.Urls;
using NLog;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public class IpnMessage : BasePayPalObject
    {

        /// <summary>
        /// This is the raw paypal response
        /// </summary>
        public string MessageBlockRaw { get; private set; }

        public IpnMessage(string formContents)
        {
            this.MessageBlockRaw = formContents;
            QueryString qs = new QueryString(formContents);
            this.ParseFromNameValueCollection(qs,null,null);
            
        }


        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        
        [PayPalFieldInfo(FieldName="mc_gross")]
        public double GrossTotal { get; set; }

        [PayPalFieldInfo(FieldName = "invoice")]
        public string Reference { get; set; }

        [PayPalFieldInfo(FieldName = "protection_eligibility")]
        public string ProtectionEligibility { get; set; }

        [PayPalFieldInfo(FieldName = "address_status")]
        public string AddressStatus { get; set; }

        [PayPalFieldInfo(FieldName = "payer_id")]
        public string PayerID { get; set; }

        [PayPalFieldInfo(FieldName = "tax")]
        public double TaxTotal { get; set; }

        [PayPalFieldInfo(FieldName = "address_street")]
        public string AddressStreet1 { get; set; }

        [PayPalFieldInfo(FieldName = "payment_date")]
        public string PaymentDate { get; set; }

        [PayPalFieldInfo(FieldName = "payment_status")]
        protected string _PaymentStatus { get; set; }
        public PayPalEnums.PAYMENT_STATUS PaymentStatus
        {
            get
            {
                string s = _PaymentStatus;
                PayPalEnums.PAYMENT_STATUS status = PayPalEnums.PAYMENT_STATUS.None;
                try
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        status = CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.PAYMENT_STATUS>(s).GetValueOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    _log.Warn("Could not parse <" + s + "> into a valid PayPal PAYMENT_STATUS enum value", ex);
                    
                }
                return status;
            }
            set
            {
                _PaymentStatus = CS.General_CS_v5.Util.EnumUtil.ConvertEnumToString<PayPalEnums.PAYMENT_STATUS>(value);
            }
        }

        [PayPalFieldInfo(FieldName = "address_zip")]
        public string AddressZIPCode { get; set; }

        [PayPalFieldInfo(FieldName = "mc_shipping")]
        public double ShippingTotal { get; set; }

        [PayPalFieldInfo(FieldName = "mc_handling")]
        public double HandlingTotal { get; set; }

        [PayPalFieldInfo(FieldName = "first_name")]
        public string ClientFirstName { get; set; }

        [PayPalFieldInfo(FieldName = "address_country_code")]
        public string AddressCountryCode { get; set; }

        [PayPalFieldInfo(FieldName = "address_name")]
        public string AddressPersonName { get; set; }

        [PayPalFieldInfo(FieldName = "notify_version")]
        public string NotifyVersion { get; set; }

        [PayPalFieldInfo(FieldName = "custom")]
        public string CustomField { get; set; }

        [PayPalFieldInfo(FieldName = "payer_status")]
        public string PayerStatus { get; set; }

        [PayPalFieldInfo(FieldName = "address_country")]
        public string AddressCountry { get; set; }

        [PayPalFieldInfo(FieldName = "num_cart_items")]
        public int TotalItemsInCart { get; set; }

        [PayPalFieldInfo(FieldName = "address_city")]
        public string AddressCity { get; set; }

        [PayPalFieldInfo(FieldName = "verify_sign")]
        public string VerifySignature { get; set; }

        [PayPalFieldInfo(FieldName = "payer_email")]
        public string PayerEmail { get; set; }

        [PayPalFieldInfo(FieldName = "txn_id")]
        public string PayPalTransactionID { get; set; }

        [PayPalFieldInfo(FieldName = "payment_type")]
        public string PaymentType { get; set; }

        [PayPalFieldInfo(FieldName = "last_name")]
        public string ClientLastName { get; set; }

        [PayPalFieldInfo(FieldName = "address_state")]
        public string AddressState { get; set; }

        [PayPalFieldInfo(FieldName = "receiver_email")]
        public string MerchantEmail { get; set; }

        [PayPalFieldInfo(FieldName = "shipping_discount")]
        public double ShippingDiscountTotal { get; set; }

        [PayPalFieldInfo(FieldName = "insurance_amount")]
        public double InsuranceTotalAmount { get; set; }

        [PayPalFieldInfo(FieldName = "receiver_id")]
        public string MerchantPaypalID { get; set; }

        [PayPalFieldInfo(FieldName = "pending_reason")]
        public string PendingReason { get; set; }

        /// <summary>
        /// This should not be used to identify a IPN message, as this is UNIQUE per sent notification
        /// </summary>
        [PayPalFieldInfo(FieldName = "ipn_track_id")]
        
        public string IpnTrackId { get; set; }

        [PayPalFieldInfo(FieldName = PayPalConstants.PARAM_TRANSACTION_TYPE )]
        protected string _transactionType { get; set; }
        public PayPalEnums.TRANSACTION_TYPE TransactionType
        {
            get
            {
                string s = _transactionType;
                PayPalEnums.TRANSACTION_TYPE status = PayPalEnums.TRANSACTION_TYPE.CouldNotParse;
                try
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        status = PayPalEnums.GetTransactionTypeFromPayPalCode(s, throwErrorIfCannotMatch: false);
                    }
                }
                catch (Exception ex)
                {
                    _log.Error("Could not parse <" + s + "> into a valid PayPal TRANSACTION_TYPE enum value", ex);

                }
                return status;
            }
            set
            {
                _transactionType = CS.General_CS_v5.Util.EnumUtil.ConvertEnumToString<PayPalEnums.TRANSACTION_TYPE>(value);
            }
        }
        

        [PayPalFieldInfo(FieldName = "discount")]
        public double TotalDiscount { get; set; }

        [PayPalFieldInfo(FieldName = "mc_currency")]
        public string CurrencyCode { get; set; }

        [PayPalFieldInfo(FieldName = "residence_country")]
        public string ResidenceCountry { get; set; }

        [PayPalFieldInfo(FieldName = "shipping_method")]
        public string ShippingMethod { get; set; }

        [PayPalFieldInfo(FieldName = "transaction_subject")]
        public string TransactionSubject { get; set; }

        [PayPalFieldInfo(FieldName = "payment_gross")]
        public string PaymentGross { get; set; }

        [PayPalFieldInfo(FieldName = "test_ipn")]
        public bool IsTestIpn { get; set; }



        public bool IsSuccess()
        {
            return PayPalEnums.CheckIfPaymentStatusIsSuccess(this.PaymentStatus);
        }
        public bool CheckIfRequiresManualIntervention()
        {
            return PayPalEnums.CheckIfPaymentStatusRequiresManualIntervention(this.PaymentStatus);
        }

        #region Recurring Payments Variables

        [PayPalFieldInfo(FieldName = "amount")]
        public double RecurringPaymentAmount { get; set; }

        [PayPalFieldInfo(FieldName = "amount_per_cycle")]
        public string RecurringPaymentAmountPerCycle { get; set; }

        [PayPalFieldInfo(FieldName = "initial_payment_amount")]
        public double RecurringPaymentInitialPaymentAmount { get; set; }

        [PayPalFieldInfo(FieldName = "next_payment_date")]
        public string RecurringPaymentNextPaymentDueDate { get; set; }


        [PayPalFieldInfo(FieldName = "outstanding_balance")]
        public string RecurringPaymentOutstandingBalance { get; set; }

        [PayPalFieldInfo(FieldName = "payment_cycle")]
        public string RecurringPaymentPaymentCycle { get; set; }

        [PayPalFieldInfo(FieldName = "period_type")]
        public string RecurringPaymentPeriodType { get; set; }


        [PayPalFieldInfo(FieldName = "product_name")]
        public string RecurringPaymentProductName { get; set; }

        [PayPalFieldInfo(FieldName = "product_type")]
        public string RecurringPaymentProductType { get; set; }

        [PayPalFieldInfo(FieldName = "profile_status")]
        public string RecurringPaymentProfileStatus { get; set; }

        [PayPalFieldInfo(FieldName = "recurring_payment_id")]
        public string RecurringPaymentPaymentId { get; set; }

        [PayPalFieldInfo(FieldName = "rp_invoice_id")]
        public string RecurringPaymentMerchangeUniqueReference { get; set; }

        [PayPalFieldInfo(FieldName = "time_created")]
        public string RecurringPaymentTimeCreated { get; set; }

        

        
        #endregion
    }
}
