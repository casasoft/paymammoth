﻿using System.Collections.Generic;
using System.Collections.Specialized;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class DoExpressCheckoutResponse : BasePayPalResponse
    {
        /// <summary>
        /// A token that uniquely identifiers this response for your user.  If the token was filled in in the request, the same
        /// token is returned
        /// </summary>

        [PayPalFieldInfo(FieldName="Token")]
        public string Token { get; set; }
        [PayPalFieldInfo(FieldName = "PaymentType")]
        public string PaymentType { get; set; }
        [PayPalFieldInfo(FieldName = "Note")]
        public string Note { get; set; }

        public List<PaymentInfo> PaymentInfos { get; private set; }
        public DoExpressCheckoutResponse()
        {
            this.PaymentInfos = new List<PaymentInfo>();
        }
        public override void ParseFromNameValueCollection(NameValueCollection nv)
        {
            base.ParseFromNameValueCollection(nv);
            this.PaymentInfos = PaymentInfo.ParseFromNameValueCollection(nv);

        }
    }
           
}
