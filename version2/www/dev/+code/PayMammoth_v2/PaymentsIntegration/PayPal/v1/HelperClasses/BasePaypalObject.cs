﻿using System.Collections.Specialized;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses
{
    public abstract class BasePayPalObject
    {
        
        protected virtual NameValueCollection createNameValueCollectionWithPayPalInfo()
        {
            NameValueCollection nv = new NameValueCollection();
            FillNameValueCollection(nv,null,null);
            return nv;
        }
        public virtual void ParseFromNameValueCollection(NameValueCollection nv, string prepend, string append)
        {


            Util.GeneralUtil.FillObjectFromPaypalNameValueColl(nv, this, prepend, append);
        }
        public virtual void FillNameValueCollection(NameValueCollection nv, string prepend, string append)
        {
            Util.GeneralUtil.AddPaypalFieldsToNameValueColl(nv, this, prepend, append);
        }
    }
        
}
