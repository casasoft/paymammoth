﻿using System.Collections.Specialized;
using System.Net;
using CS.General_CS_v5.Modules.Web;
using CS.General_CS_v5.Util;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public abstract class BasePayPalRequest : BasePayPalObject
    {
        public BasePayPalRequest(PayPalClient client, string methodName)
        {
            _paypal = client;
            this.MethodName = methodName;

            _payPalSettingsService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPayPalSettingsService>();
            _httpRequestSenderService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IHttpRequestSenderService>();

        }
        protected PayPalClient _paypal = null;
        private readonly IPayPalSettingsService _payPalSettingsService;
        private readonly IHttpRequestSenderService _httpRequestSenderService;

        [PayPalFieldInfo(FieldName = "METHOD")]
        public string MethodName { get; private set; }
        protected override NameValueCollection createNameValueCollectionWithPayPalInfo()
        {
            var nv = base.createNameValueCollectionWithPayPalInfo();
            _paypal.AppendAuthParametersToNV(nv);
            return nv;
        }
        protected virtual string getResponse(NameValueCollection nv)
        {
            var currentSettings = _payPalSettingsService.GetCurrentSettings(this._paypal.Settings);

            WebHeaderCollection headers = new WebHeaderCollection();

            headers.Add("Cache-Control", "no-cache");
            //client.Headers.Add("Accept-Encoding", "gzip, deflate");
            headers.Add("Accept-Language", "en-us");
            headers.Add("Content-Type", "application/x-www-form-urlencoded");


            string postData = ListUtil.ConvertNameValueCollectionToString(nv, "&", "=", (item => PageUtil.UrlEncode(item)),
                (item => PageUtil.UrlEncode(item)));

            var webResponse = _httpRequestSenderService.SendHttpRequest(currentSettings.NvpServerUrl, CS.General_CS_v5.Enums.HttpMethod.POST, headers,postData);
            string sResponse = webResponse.GetResponseAsString();
            
            return sResponse;
        }
    }
        
}
