﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using PaymentGateways.v1.PayPal.Util;

namespace PaymentGateways.v1.PayPal.ExpressCheckout.NVP
{
    public class PaymentItem
    {
        /// <summary>
        /// Name of item. Max 127-characters
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Amount of item.
        /// </summary>
        public double? Amount { get; set; }
        /// <summary>
        /// Quantity
        /// </summary>
        public int? Quantity { get; set; }
        /// <summary>
        /// Tax Amount
        /// </summary>
        public double? Tax { get; set; }

        public void AppendToNameValue(NameValueCollection nv, int id)
        {
            GeneralUtil.AddItemToNVForPaypal(nv, "L_" + "Name" + id, Name);
            GeneralUtil.AddItemToNVForPaypal(nv, "L_" + "Amt" + id, Amount);
            GeneralUtil.AddItemToNVForPaypal(nv, "L_" + "Qty" + id, Quantity);
            GeneralUtil.AddItemToNVForPaypal(nv, "L_" + "TaxAmt" + id, Tax);
        }


    }
        
}
