﻿using System;
using System.Collections.Specialized;
using CS.General_CS_v5.Modules.Urls;
using CS.General_CS_v5.Util;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class RecurringPaymentsProfileRequest : BasePayPalRequest
    {
        public RecurringPaymentsProfileRequest(PayPalClient paypal, string token)
            : base(paypal, "CreateRecurringPaymentsProfile")
        {
            this.Token = token;
        }

        [PayPalFieldInfo(FieldName = "TOKEN")]
        public string Token { get; set; }

        /// <summary>
        /// (Required) Description of the recurring payment.
        /// NOTE:You must ensure that this field matches the corresponding billing agreement description included in the SetExpressCheckout request.
        /// </summary>
        [PayPalFieldInfo(FieldName = "DESC")]
        public string Description { get; set; }

        /// <summary>
        /// (Optional) Number of scheduled payments that can fail before the profile is automatically suspended.
        /// An IPN message is sent to the merchant when the specified number of failed payments is reached.
        /// </summary>
        [PayPalFieldInfo(FieldName = "MAXFAILEDPAYMENTS")]
        public int MaximumFailedPayments { get; set; }

        /// <summary>
        /// (Optional) Full name of the person receiving the product or service paid for by the recurring payment. If not present, the name in the buyer’s PayPal account is used.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SUBSCRIBERNAME")]
        public string SubscriberName { get; set; }

        [PayPalFieldInfo(FieldName = "BILLINGPERIOD")]
        protected string billingPeriod { get; set; }

        /// <summary>
        /// (Required) Unit for billing during this subscription period.
        /// For SemiMonth, billing is done on the 1st and 15th of each month.
        /// NOTE:The combination of BillingPeriod and BillingFrequency cannot exceed one year.
        /// </summary>
        public Connector.Enums.RecurringBillingPeriod BillingPeriod
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<Connector.Enums.RecurringBillingPeriod>( billingPeriod).GetValueOrDefault();
            }
            set { billingPeriod = value.ToString(); }
        }

        [PayPalFieldInfo(FieldName = "AUTOBILLOUTAMT")]
        protected string autoBilloutAmt { get; set; }

        /// <summary>
        /// (Optional) Indicates whether you would like PayPal to automatically bill the outstanding balance amount in the next billing cycle.
        /// The outstanding balance is the total amount of any previously failed scheduled payments that have yet to be successfully paid.
        /// </summary>
        public PayPalEnums.RECURRING_AUTO_BILL_OUTSTANDING_AMOUNT AutoBillOutstandingAmt
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.RECURRING_AUTO_BILL_OUTSTANDING_AMOUNT>(autoBilloutAmt).GetValueOrDefault();
                
            }
            set { autoBilloutAmt = value.ToString(); }
        }

        [PayPalFieldInfo(FieldName = "TRIALBILLINGPERIOD")]
        protected string trialBillingPeriod { get; set; }

        /// <summary>
        /// Unit for billing during this subscription period; required if you specify an optional trial period.
        /// For SemiMonth, billing is done on the 1st and 15th of each month.
        /// NOTE:The combination of BillingPeriod and BillingFrequency cannot exceed one year.
        /// </summary>
        public Connector.Enums.RecurringBillingPeriod TrialBillingPeriod
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<Connector.Enums.RecurringBillingPeriod>(trialBillingPeriod).GetValueOrDefault();

            }
            set { trialBillingPeriod = value.ToString(); }
        }

        [PayPalFieldInfo(FieldName = "TRIALAMT")]
        protected string trialAmount { get; set; }

        /// <summary>
        /// Billing amount for each billing cycle during this payment period; required if you specify an optional trial period.
        /// Excluding shipping and tax amounts and not exceeding $10,000 USD.
        /// </summary>
        public double TrialAmount
        {
            get { return double.Parse(trialAmount); }
            set { trialAmount = string.Format("{0:0.00}", value); }
        }

        /// <summary>
        /// (Optional) Number of billing cycles for trial payment period.
        /// </summary>
        [PayPalFieldInfo(FieldName = "TRIALTOTALBILLINGCYCLES")]
        public int TrialTotalBillingCycles { get; set; }

        /// <summary>
        /// Item name. This field is required when ItemCategory is passed.
        /// Item name. This field is required when L_PAYMENTREQUEST_n_ITEMCATEGORYm is passed. 
        /// </summary>
        [PayPalFieldInfo(FieldName = "L_PAYMENTREQUEST_0_NAME0")]
        public string PaymentRequestName { get; set; }

        /// <summary>
        /// Optional) Item description. 
        /// </summary>
        [PayPalFieldInfo(FieldName = "L_PAYMENTREQUEST_0_DESC0")]
        public string PaymentRequestDescription { get; set; }

        /// <summary>
        /// (Optional) The merchant’s own unique reference or invoice number.
        /// </summary>
        [PayPalFieldInfo(FieldName = "PROFILEREFERENCE")]
        public string ProfileReference { get; set; }

        /// <summary>
        /// Person’s name associated with this shipping address. It is required if using a shipping address.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SHIPTONAME")]
        public string ShipToName { get; set; }

        /// <summary>
        /// First street address. It is required if using a shipping address.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SHIPTOSTREET")]
        public string ShipToStreet { get; set; }

        /// <summary>
        /// (Optional) Second street address.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SHIPTOSTREET2")]
        public string ShipToStreet2 { get; set; }

        /// <summary>
        /// Name of city. It is required if using a shipping address.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SHIPTOCITY")]
        public string ShipToCity { get; set; }

        /// <summary>
        /// State or province. It is required if using a shipping address.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SHIPTOSTATE")]
        public string ShipToState { get; set; }

        /// <summary>
        /// U.S. ZIP code or other country-specific postal code. It is required if using a U.S. shipping address; may be required for other countries.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SHIPTOZIP")]
        public string ShipToZip { get; set; }

        /// <summary>
        /// Country code. It is required if using a shipping address.
        /// </summary>
        public CS.General_CS_v5.Enums.CountryIso3166? ShipToCountry
        {
            get
            {
                return _shipToCountryCode;
            }
            set
            {
                _shipToCountryCode = value;
                _shipToCountryCode_str = value != null ? CS.General_CS_v5.Enums.Country_ISO3166_To2LetterCode(_shipToCountryCode) : null;

            }
        }
        [PayPalFieldInfo(FieldName = "SHIPTOCOUNTRY")]
        private string _shipToCountryCode_str { get; set; }
        private CS.General_CS_v5.Enums.CountryIso3166? _shipToCountryCode;

        /// <summary>
        /// (Required) First street address.
        /// </summary>
        [PayPalFieldInfo(FieldName = "STREET")]
        public string BuyerStreet { get; set; }

        /// <summary>
        /// (Optional) Second street address.
        /// </summary>
        [PayPalFieldInfo(FieldName = "STREET2")]
        public string BuyerStreet2 { get; set; }

        /// <summary>
        /// (Required) Name of city.
        /// </summary>
        [PayPalFieldInfo(FieldName = "CITY")]
        public string BuyerCity { get; set; }

        /// <summary>
        /// (Required) State or province.
        /// </summary>
        [PayPalFieldInfo(FieldName = "STATE")]
        public string BuyerState { get; set; }

        /// <summary>
        /// (Required) U.S. ZIP code or other country-specific postal code.
        /// </summary>
        [PayPalFieldInfo(FieldName = "ZIP")]
        public string BuyerZip { get; set; }

        /// <summary>
        /// (Optional) Phone number.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SHIPTOPHONENUM")]
        public string ShipToPhoneNumber { get; set; }

        /// <summary>
        /// Item quantity. This field is required when ItemCategory is passed.
        /// Item quantity. This field is required when L_PAYMENTREQUEST_n_ITEMCATEGORYm is passed. 
        /// </summary>
        [PayPalFieldInfo(FieldName = "L_PAYMENTREQUEST_0_QTY0")]
        public int PaymentRequestQuantity { get; set; }

        /// <summary>
        /// (Optional) Item number.
        /// </summary>
        [PayPalFieldInfo(FieldName = "L_PAYMENTREQUEST_0_NUMBER0")]
        public int PaymentRequestNumber { get; set; }

        [PayPalFieldInfo(FieldName = "L_PAYMENTREQUEST_0_AMT0")]
        protected string paymentRequestAmount { get; set; }

        /// <summary>
        /// Cost of item. This field is required when ItemCategory is passed.
        /// Cost of item. This field is required when L_PAYMENTREQUEST_n_ITEMCATEGORYm is passed.
        /// </summary>
        public double PaymentRequestAmount
        {
            get { return double.Parse(paymentRequestAmount); }
            set { paymentRequestAmount = string.Format("{0:0.00}", value); }
        }

        [PayPalFieldInfo(FieldName = "L_PAYMENTREQUEST_0_TAXAMT0")]
        protected string paymentRequestTaxAmount { get; set; }

        /// <summary>
        /// (Optional) Item sales tax - must not exceed $10,000 USD.
        /// </summary>
        public double PaymentRequestTaxAmount
        {
            get { return double.Parse(paymentRequestTaxAmount); }
            set { paymentRequestTaxAmount = string.Format("{0:0.00}", value); }
        }

        [PayPalFieldInfo(FieldName = "L_PAYMENTREQUEST_0_ITEMCATEGORY0")]
        protected string itemCategory { get; set; }

        /// <summary>
        /// Indicates whether the item is digital or physical. For digital goods, this field is required and must be set to Digital to get the best rates.
        /// </summary>
        public PayPalEnums.RECURRING_ITEMCATEGORY PaymentRequestItemCategory
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.RECURRING_ITEMCATEGORY>(itemCategory).GetValueOrDefault();
            }
            set { itemCategory = value.ToString(); }
        }

        [PayPalFieldInfo(FieldName = "CREDITCARDTYPE")]
        protected string creditCardType { get; set; }

        /// <summary>
        /// (Optional) Type of credit card. For UK, only Maestro, MasterCard, Discover, and Visa are allowable. For Canada, only MasterCard and Visa are allowable and Interac debit cards are not supported.
        /// </summary>
        public PayPalEnums.CARD_TYPES CreditCardType
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.CARD_TYPES>(creditCardType).GetValueOrDefault();
                
            }
            set { creditCardType = value.ToString(); }
        }

        /// <summary>
        /// (Required) Credit card number. Required if using card fields.
        /// </summary>
        [PayPalFieldInfo(FieldName = "ACCT")]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// Credit card expiration date. This field is required if you are using recurring payments with direct payments.
        /// </summary>
        [PayPalFieldInfo(FieldName = "EXPDATE")]
        public string CreditCardExpiryDate { get; set; }

        /// <summary>
        /// Card Verification Value, version 2. Your Merchant Account settings determine whether this field is required. 
        /// To comply with credit card processing regulations, you must not store this value after a transaction has been completed.
        /// Character length and limitations: For Visa, MasterCard, and Discover, the value is exactly 3 digits. For American Express, 
        /// the value is exactly 4 digits.
        /// </summary>
        [PayPalFieldInfo(FieldName = "CVV2")]
        public string CreditCardCvv2 { get; set; }

        /// <summary>
        /// (Optional) Month and year that Maestro card was issued.
        /// </summary>
        [PayPalFieldInfo(FieldName = "STARTDATE")]
        public string CreditCardMaestroStartDate { get; set; }

        /// <summary>
        /// (Optional) Issue number of Maestro card.
        /// </summary>
        [PayPalFieldInfo(FieldName = "ISSUENUMBER")]
        public string CreditCardMaestroIssueNumber { get; set; }

        /// <summary>
        /// (Optional) Unique PayPal Customer Account identification number.
        /// </summary>
        [PayPalFieldInfo(FieldName = "PAYERID")]
        public string PayerId { get; set; }

        [PayPalFieldInfo(FieldName = "FAILEDINITAMTACTION")]
        protected string failedInitAmtAction { get; set; }

        /// <summary>
        /// By Default, this is Cancel on Failure - see actual options XML Documentation (for the Enum) to see explanation of different options.
        /// </summary>
        public PayPalEnums.RECURRING_PAYPAL_SETUP_FAILED_INITIAL_AMOUNT_ACTION FailedInitialAmountAction
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.RECURRING_PAYPAL_SETUP_FAILED_INITIAL_AMOUNT_ACTION>(failedInitAmtAction).GetValueOrDefault();
            }
            set { failedInitAmtAction = value.ToString(); }
        }

        /// <summary>
        /// (Optional) Number of billing cycles for payment period.
        /// For the regular payment period, if no value is specified or the value is 0, the regular payment period continues 
        /// until the profile is canceled or deactivated.
        /// For the regular payment period, if the value is greater than 0, the regular payment period will expire after the 
        /// trial period is finished and continue at the billing frequency for TotalBillingCycles cycles.
        /// </summary>
        [PayPalFieldInfo(FieldName = "TOTALBILLINGCYCLES")]
        public int TotalBillingCycles { get; set; }

        /// <summary>
        /// (Required) Number of billing periods that make up one billing cycle.
        ///The combination of billing frequency and billing period must be less than or equal to one year. 
        /// For example, if the billing cycle is Month, the maximum value for billing frequency is 12. Similarly, 
        /// if the billing cycle is Week, the maximum value for billing frequency is 52.
        /// NOTE: If the billing period is SemiMonth., the billing frequency must be 1.
        /// </summary>
        [PayPalFieldInfo(FieldName = "BILLINGFREQUENCY")]
        public int BillingFrequency { get; set; }

        [PayPalFieldInfo(FieldName = "PROFILESTARTDATE")]
        protected string profileStartDate { get; set; }

        /// <summary>
        /// (Required) The date when billing for this profile begins.
        /// NOTE: The profile may take up to 24 hours for activation.
        /// </summary>
        public DateTime ProfileStartDate
        {
            get { return DateTime.Parse(profileStartDate); }
            set { profileStartDate = value.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss"); }
        }

        [PayPalFieldInfo(FieldName = "AMT")]
        protected string amount { get; set; }

        /// <summary>
        /// Billing amount for each billing cycle during this payment period excluding shipping and tax amounts and not exceeding $10,000 USD.
        /// </summary>
        public double Amount
        {
            get { return double.Parse(amount); }
            set { amount = string.Format("{0:0.00}", value); }
        }

        [PayPalFieldInfo(FieldName = "SHIPPINGAMT")]
        protected string shippingAmount { get; set; }

        /// <summary>
        /// (Optional) Shipping amount for each billing cycle during this payment period excluding tax amounts and not exceeding $10,000 USD.
        /// </summary>
        public double ShippingAmount
        {
            get { return double.Parse(shippingAmount); }
            set { shippingAmount = string.Format("{0:0.00}", value); }
        }

        [PayPalFieldInfo(FieldName = "INITAMT")]
        protected string initialAmount { get; set; }

        /// <summary>
        /// (Optional) Initial non-recurring payment amount. Use an initial amount for enrollment or set-up fees and must not exceed $10,000 USD.
        /// </summary>
        public double InitialAmount
        {
            get { return double.Parse(initialAmount); }
            set { initialAmount = string.Format("{0:0.00}", value); }
        }

        [PayPalFieldInfo(FieldName = "TAXAMT")]
        protected string taxAmount { get; set; }

        /// <summary>
        /// (Optional) Tax amount for each billing cycle during this payment period excluding shipping and not exceeding $10,000 USD.
        /// </summary>
        public double TaxAmount
        {
            get { return double.Parse(taxAmount); }
            set { taxAmount = string.Format("{0:0.00}", value); }
        }

        /// <summary>
        /// (Required) Currency code (default is USD).
        /// </summary>
        public CS.General_CS_v5.Enums.CurrencyCode_Iso4217 CurrencyCode
        {
            get
            {
                return _CurrencyCode;
            }
            set
            {
                _CurrencyCode = value;
                _CurrencyCode_Str = CS.General_CS_v5.Enums.CurrencyCode_Iso4217ToCode(_CurrencyCode);

            }
        }
        [PayPalFieldInfo(FieldName = "CURRENCYCODE")]
        private string _CurrencyCode_Str { get; set; }
        private CS.General_CS_v5.Enums.CurrencyCode_Iso4217 _CurrencyCode;

        /// <summary>
        /// (Required) Email address of buyer.
        /// </summary>
        [PayPalFieldInfo(FieldName = "EMAIL")]
        public string Email { get; set; }

        [PayPalFieldInfo(FieldName = "PAYERSTATUS")]
        protected string payerStatus { get; set; }

        /// <summary>
        /// (Optional) Status of buyer.
        /// </summary>
        public PayPalEnums.PAYPAL_PAYER_STATUS PayerStatus
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.PAYPAL_PAYER_STATUS>(TextUtil.CapitaliseFirstLetterOfEachWord(failedInitAmtAction)).GetValueOrDefault();
                
            }
            set { payerStatus = value.ToString().ToLower(); }
        }

        /// <summary>
        /// (Optional) Buyer’s country of residence in the form of ISO standard 3166 two-character country codes.
        /// </summary>
        public CS.General_CS_v5.Enums.CountryIso3166? BuyersCountryCode
        {
            get
            {
                return _buyersCountryCode;
            }
            set
            {
                _buyersCountryCode = value;
                _buyersCountryCoded_str = value != null ? CS.General_CS_v5.Enums.Country_ISO3166_To2LetterCode(_buyersCountryCode) : null;

            }
        }
        [PayPalFieldInfo(FieldName = "COUNTRYCODE")]
        private string _buyersCountryCoded_str { get; set; }
        private CS.General_CS_v5.Enums.CountryIso3166? _buyersCountryCode;

        /// <summary>
        /// (Optional) Buyer’s business name.
        /// </summary>
        [PayPalFieldInfo(FieldName = "BUSINESS")]
        public string BusinessName { get; set; }

        /// <summary>
        /// (Optional) Buyer’s salutation.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SALUTATION")]
        public string PayerSalutation { get; set; }

        /// <summary>
        /// (Optional) Buyer’s first name.
        /// </summary>
        [PayPalFieldInfo(FieldName = "FIRSTNAME")]
        public string PayerFirstName { get; set; }

        /// <summary>
        /// (Optional) Buyer’s middle name.
        /// </summary>
        [PayPalFieldInfo(FieldName = "MIDDLENAME")]
        public string PayerMiddleName { get; set; }

        /// <summary>
        /// (Optional) Buyer’s last name.
        /// </summary>
        [PayPalFieldInfo(FieldName = "LASTNAME")]
        public string PayerLastName { get; set; }

        /// <summary>
        /// (Optional) Buyer’s suffix.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SUFFIX")]
        public string PayerSuffix { get; set; }

        public RecurringPaymentsProfileResponse GetResponse()
        {
            NameValueCollection nv = createNameValueCollectionWithPayPalInfo();

            string sResponse = getResponse(nv);
            RecurringPaymentsProfileResponse response = new RecurringPaymentsProfileResponse();

            var qs = new QueryString(sResponse);
            response.ParseFromNameValueCollection(qs);
            return response;
        }
    }
}
