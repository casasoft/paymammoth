﻿using System;
using NLog;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public class IpnClient
    {

        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        public IPayPalSettings Settings { get; private set; }

        //public static string SERVER_SANDBOX = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        //public static string SERVER_LIVE = "https://www.paypal.com/cgi-bin/webscr";
        //public string Server {get; private set;}
        //public string MerchantEmail { get; set; }

        public IpnClient(IPayPalSettings settings)
        {
            this.Settings = settings;
        }

       


        public IpnResponse VerifyTransaction(IpnMessage msg)
        {
            throw new InvalidOperationException("not implemented");
            //if (_log.IsInfoEnabled) _log.Info("VerifyTransaction() - Msg Block [" + msg.MessageBlockRaw + "]");
            //IpnResponse ipnResponse = new IpnResponse();
            
            //var currentSettings = Extensions.IPaypalSettingsExtensions.GetCurrentSettings(this.Settings);

            //string paypalResponse = null;
            //bool verifyMsg = IPNUtil.ValidateIPNMessage(msg.MessageBlockRaw, currentSettings.ApiServerUrl, out paypalResponse);


            //ipnResponse.Success = false;
            //ipnResponse.PaypalResponse = paypalResponse;
            //if (verifyMsg)
            //{

                
            //    if (_log.IsDebugEnabled) _log.Debug("VerifyTransaction() - IPN_Verified");
            //    ipnResponse.IpnMessage = msg;

                
            //    if (msg.PaymentStatus == PayPalEnums.PAYMENT_STATUS.None)
            //    {
            //        _log.Error("Payment Status is NONE - Msg Block:\r\n\r\n" + msg.MessageBlockRaw);
            //    }
            //    if (msg.IsSuccess())
            //    {
            //        if (true || string.Compare(msg.MerchantEmail, currentSettings.MerchantEmail, true) == 0) //[2012/jun/18 - [Karl] this was removed as I think it is unnecessary
            //        {
            //            if (_log.IsDebugEnabled) _log.Debug("VerifyTransaction() - Email matched as well");
            //            ipnResponse.Success = true;
            //        }
            //        else
            //        {
            //            if (_log.IsWarnEnabled) _log.Warn("VerifyTransaction() - Email DID NOT match - [" + msg.MerchantEmail + "] / [" + currentSettings.MerchantEmail + "]");
            //        }
            //    }
            //    else
            //    {
            //        _log.Warn("VerifyTransaction() - PayPal Error [Status: " + msg.PaymentStatus + " - PendingReason: " + msg.PendingReason + "]\r\n\r\nMsg Block: " + msg.MessageBlockRaw);

            //    }
            
            //}
           
            //if (_log.IsDebugEnabled) _log.Debug( "VerifyTransaction() - Result: " + CS.General_v3.Util.Text.ShowBool(ipnResponse.Success));
            //return ipnResponse;


        }

        
    }
}
