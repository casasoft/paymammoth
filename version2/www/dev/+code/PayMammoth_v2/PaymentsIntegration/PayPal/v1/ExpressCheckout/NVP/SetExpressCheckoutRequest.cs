﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using CS.General_CS_v5.Modules.Urls;
using CS.General_CS_v5.Modules.Web;
using CS.General_CS_v5.Util;
using NLog;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class SetExpressCheckoutRequest : BasePayPalRequest
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private IPayPalSettingsService _paypalSettingService;
        private readonly IHttpRequestSenderService _httpRequestSenderService;

        public SetExpressCheckoutRequest(PayPalClient payPal,
            string returnURL, string cancelURL) : base(payPal,"SetExpressCheckout")
        {
            //this.PaymentAction = Enums.PAYMENT_ACTION.Sale;
                
            
            this.PaymentDetails = new List<PaymentDetailsInfo>();
            //this.ClientShippingInfo = new AddressTypeInfo();
            //this.TotalAmount = totalAmount;
            this.ReturnUrl = returnURL;
            this.CancelUrl = cancelURL;
            
            //this.CurrencyCode = CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedStatesOfAmericaDollars;
            this.Language = PayPalEnums.LANGUAGE.EnglishUS;
            this.PaypalAccountRequired = PayPalEnums.SOLUTION_TYPE.PaypalAccountOptional;
            this.SurveyChoices = new List<string>();
            this.BillingTypes = new List<string>();
            this.BillingAgreementDescriptions = new List<string>();

            _paypalSettingService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IPayPalSettingsService>();
            _httpRequestSenderService = BusinessLogic_CS_v5.Util.InversionUtil.Get<IHttpRequestSenderService>();

        }


        /// <summary>
        /// A token that uniquely identifies this request for your user
        /// </summary>
        [PayPalFieldInfo]
        public string Token { get; set; }
        

        /// <summary>
        /// Expected maximum total amount of the complete order, including shipping cost and tax charges"
        /// </summary>
        [PayPalFieldInfo(FieldName = "MAXAMT")]
        public decimal? MaxAmount {get;set;}
        /// <summary>
        /// Return URL after payment. Required
        /// </summary>
        [PayPalFieldInfo(FieldName = "RETURNURL")]
        public string ReturnUrl { get; set; }
        /// <summary>
        /// Cancel URL if payment not done. Required
        /// </summary>
        [PayPalFieldInfo(FieldName = "CANCELURL")]
        public string CancelUrl { get; set; }
        
        /// <summary>
        /// (Optional) URL to which the callback request from PayPal is sent. It must start with
/// HTTPS for production integration. It can start with HTTPS or HTTP for sandbox
/// testing.
/// Character length and limitations: 1024 characters
        /// This field is available since version 53.0.
        /// </summary>
        [PayPalFieldInfo(FieldName = "CALLBACK")]
        public string CallBackUrl { get; set; }
        /// <summary>
        /// (Optional) An override for you to request more or less time to be able to process the
///callback request and respond. The acceptable range for the override is 1 to 6 seconds.
///If you specify a value greater than 6, PayPal uses the default value of 3 seconds.
///Character length and limitations: An integer between 1 and 6
        /// </summary>
        [PayPalFieldInfo(FieldName = "CALLBACKTIMEOUT")]
        public int? CallbackTimeout { get; set; }

        [PayPalFieldInfo(FieldName = "ReqConfirmShipping")]
        /// <summary>
        /// Whether you require customer to have a confirmed paypal shipping address
        /// </summary>
        public bool? RequireConfirmedShippingAddress { get; set; }

        /// <summary>
        /// No shipping information is needed
        /// </summary>
        [PayPalFieldInfo(FieldName = "NoShipping")]
        public bool? DontDisplayShippingAddress { get; set; }

        /// <summary>
        /// Allow client to enter a note to merchant
        /// </summary>
        [PayPalFieldInfo(FieldName = "AllowNote")]
        public bool? AllowNoteClientToMerchant { get; set; }

        /// <summary>
        /// Override paypal address with the one you specify in ClientAddress1, ClientAddress2, etc.
        /// </summary>
        [PayPalFieldInfo(FieldName = "AddOverride")]
        public bool? OverrideAddress { get; set; }

        /// <summary>
        /// Language.  Default EnglishUS.
        /// </summary>
        public PayPalEnums.LANGUAGE Language
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.LANGUAGE>(_Language_Str).GetValueOrDefault();
                
            }
            set
            {
                _Language_Str = CS.General_CS_v5.Util.EnumUtil.ConvertEnumToString(value);

            }
        }
        [PayPalFieldInfo(FieldName = "LOCALECODE", MaxLength = 5)]
        protected string _Language_Str { get; set; }
        




        /// <summary>
        /// (Optional) Sets the Custom Payment Page Style for payment pages associated with this button/link. 
        /// This value corresponds to the HTML variable page_style for customizing payment pages. The value is 
        /// the same as the Page Style Name you chose when adding or editing the page style from the Profile subtab
        /// of the My Account tab of your PayPal account. Character length and limitations: 30 single-byte alphabetic characters.
        /// </summary>
        [PayPalFieldInfo(FieldName="PageStyle", MaxLength=  30)]
        public string PageStyle { get; set; }

        /// <summary>
        /// URL of image to load in payment page. Max size of 750 x 90 pixels. Preferably on an HTTPS location
        /// </summary>
        [PayPalFieldInfo(FieldName="HdrImg", MaxLength=  127)]
        public string ImageURL { get; set; }

        /// <summary>
        /// The header border color.  6-digit hex
        /// </summary>
        [PayPalFieldInfo(FieldName="HdrBorderColor", MaxLength=  6)]
        public string HeaderBorderColor { get; set; }

        /// <summary>
        /// The header background color: 6-digit hex.
        /// </summary>
        [PayPalFieldInfo(FieldName="HdrBackColor", MaxLength=  6)]
        public string HeaderBackgroundColor { get; set; }

        /// <summary>
        /// Payment flow background color: 6-digit hex.
        /// </summary>
        [PayPalFieldInfo(FieldName="PayFlowColor", MaxLength=  6)]
        public string PayFlowBackgroundColor { get; set; }

        
        /// <summary>
        ///Optional) Email address of the buyer as entered during checkout. PayPal uses this
///value to pre-fill the PayPal membership sign-up portion of the PayPal login page.
        /// </summary>
        [PayPalFieldInfo(FieldName="Email", MaxLength=  127)]
        public string ClientEmail { get; set; }

        /// <summary>
        /// (Optional) Type of checkout flow:
/// Sole: Buyer does not need to create a PayPal account to check out. This is
///referred to as PayPal Account Optional.
/// Mark: Buyer must have a PayPal account to check out.
///NOTE: You can pass Mark to selectively override the PayPal Account Optional
///setting if PayPal Account Optional is turned on in your merchant account.
///Passing Sole has no effect if PayPal Account Optional is turned off in your
        ///account
        /// </summary>
        public PayPalEnums.SOLUTION_TYPE? PaypalAccountRequired
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.SOLUTION_TYPE>(_PaypalAccountRequired_Str);
                
            }
            set
            {
                string s = "";
                if (value.HasValue) s = CS.General_CS_v5.Util.EnumUtil.ConvertEnumToString(value);
                _PaypalAccountRequired_Str = s;
                

            }
        }
        [PayPalFieldInfo(FieldName = "SolutionType")]
        protected string _PaypalAccountRequired_Str { get; set; }
        



        /// <summary>
        /// Type of PayPal page to display:
        /// </summary>
        public PayPalEnums.LANDING_PAGE_TYPE? LandingPageType
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.LANDING_PAGE_TYPE>(_LandingPageType_Str);
                
            }
            set
            {
                string s = "";
                if (value.HasValue) s = CS.General_CS_v5.Util.EnumUtil.ConvertEnumToString(value);
                _LandingPageType_Str = s;
                

            }
        }
        [PayPalFieldInfo(FieldName = "LandingPage")]
        protected string _LandingPageType_Str { get; set; }
        




        /// <summary>
        /// Type of PayPal page to display:
        /// </summary>
        public PayPalEnums.CHANNEL_TYPE? ChannelType
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.CHANNEL_TYPE>(_ChannelType_Str);
                
            }
            set
            {
                string s = "";
                if (value.HasValue) s = CS.General_CS_v5.Util.EnumUtil.ConvertEnumToString(value);
                _ChannelType_Str = s;
                
            }
        }
        [PayPalFieldInfo(FieldName = "ChannelType")]
        protected string _ChannelType_Str  { get; set; }
        



        /// <summary>
        /// Optional) The URL on the merchant site to redirect to after a successful giropay payment.
        /// </summary>
        [PayPalFieldInfo(FieldName="GiroPaySuccessUrl")]
        public string GiroPaySuccessUrl { get; set; }
        
        /// <summary>
        /// (Optional) The URL on the merchant site to redirect to after a successful giropay payment.
        /// </summary>
        [PayPalFieldInfo(FieldName="GiroPayCancelUrl")]
        public string GiroPayCancelUrl { get; set; }

        /// <summary>
        /// (Conditional) Required when using Recurring Payments - must be set to 'RecurringPayments'.
        /// </summary>
        [PayPalFieldInfo(FieldName = "L_BILLINGTYPE{n}")]
        public List<string> BillingTypes { get; private set; }

        /// <summary>
        /// (Conditional) Required when using Recurring Payments - provides the description of each recurring payment created so Index N must match N of BIlling Types.
        /// </summary>
        [PayPalFieldInfo(FieldName = "L_BILLINGAGREEMENTDESCRIPTION{n}")]
        public List<string> BillingAgreementDescriptions { get; private set; }
        
        /// <summary>
        /// (Optional) The URL on the merchant site to transfer to after a bank transfer payment. Use this field only if you are using giropay or bank transfer payment methods in Germany.
        /// </summary>
        [PayPalFieldInfo(FieldName="BankTxnPendingUrl")]
        public string BankTxnPendingUrl { get; set; }

        /// <summary>
        /// (Optional) A label that overrides the business name in the PayPal account on the
///PayPal hosted checkout pages.
///Character length and limitations: 127 single-byte alphanumeric characters
        /// </summary>
        [PayPalFieldInfo(FieldName="BrandName")]
        public string BrandName { get; set; }


        /// <summary>
        /// (Optional) Merchant Customer Service number displayed on the PayPal Review
///page.
///Limitations: 16 single-byte characters
        /// </summary>
        [PayPalFieldInfo(FieldName="CustomerServiceNumber")]
        public string CustomerServiceNumber { get; set; } 

        /// <summary>
                /// Enable gift message widget on the PayPal Review page.
        /// </summary>
        [PayPalFieldInfo]
        public bool? GiftMessageEnable { get; set; }
 
        /// <summary>
        /// Enable gift receipt widget on the PayPal Review page.
        /// </summary>
        [PayPalFieldInfo]
        public bool? GiftReceiptEnable { get; set; }
        /// <summary>
        /// Enable gift wrap widget on the PayPal Review page.
/// NOTE: If the value 1 is passed for this field values for the gift wrap amount and gift
        /// vwrap name are not passed, the gift wrap name will not be displayed and the
/// gift wrap amount will display as 0.00.
        /// </summary>
        [PayPalFieldInfo]
        public bool? GiftWrapEnable { get; set; }

        /// <summary>
        /// (Optional) Label for the gift wrap option such as “Box with ribbon”.
        /// </summary>
        [PayPalFieldInfo(MaxLength=  25)]
        public string GiftWrapName { get; set; }

        [PayPalFieldInfo( MaxAmount=  10000)]
        public double? GiftWrapAmount { get; set; }

        /// <summary>
        /// (Optional) Enable buyer email opt-in on the PayPal Review page.
        /// </summary>
        [PayPalFieldInfo]
        public bool? BuyerEmailOptInEnable { get; set; }

        /// <summary>
        /// Optional) Text for the survey question on the PayPal Review page. If the survey
///question is present, at least 2 survey answer options need to be present
        /// </summary>
        [PayPalFieldInfo]
        public string SurveyQuestion { get; set; }
        /// <summary>
        /// (Optional) The version of the Instant Update API that your callback server uses. The
///default is the current version.
        /// </summary>
        [PayPalFieldInfo]
        public double? CallbackVersion { get; set; }

        [PayPalFieldInfo]
        public bool SurveyEnable
        {
            get
            {
                return (!string.IsNullOrWhiteSpace(this.SurveyQuestion) && this.SurveyChoices.Count > 0);
            }
        }

        /// <summary>
        /// Optional) Possible options for the survey answers on the PayPal Review page.
///Answers are displayed only if a valid survey question is present
        /// </summary>
        [PayPalFieldInfo(FieldName= "L_SurveyChoice{n}", MaxLength= 15)]
        public List<string> SurveyChoices { get; private set; }





        /// <summary>
        /// A list of payment items corresponding to the order
        /// </summary>
        public List<PaymentDetailsInfo> PaymentDetails { get; private set; }
        public PaymentDetailsInfo AddNewPaymentDetails()
        {
            PaymentDetailsInfo info = new PaymentDetailsInfo();
            this.PaymentDetails.Add(info);
            return info;
        }
        private void checkRequirements()
        {
            StringBuilder errMsg = new StringBuilder();
            if (string.IsNullOrEmpty(ReturnUrl))
                errMsg.AppendLine("ReturnURL is required");
            if (string.IsNullOrEmpty(CancelUrl))
                errMsg.AppendLine("CancelURL is required");
            if (this.PaymentDetails.Count == 0)
                errMsg.AppendLine("At least one PaymentDetail is required");
            if (!string.IsNullOrEmpty(errMsg.ToString()))
            {
                throw new InvalidOperationException("SetExpressCheckouttRequest: " +errMsg.ToString());
            }
        }

        

        public SetExpressCheckoutPayPalResponse GetResponse(bool autoRedirectOnSuccess = false)
        {
            
            checkRequirements();
            NameValueCollection nv = createNameValueCollectionWithPayPalInfo();
            
            
            int index =0;
            foreach (var item in this.PaymentDetails)
            {
                item.FillNameValueCollection(nv, index);
                index++;

            }

            var currentSettings = _paypalSettingService.GetCurrentSettings(this._paypal.Settings);
            string url = currentSettings.NvpServerUrl;
            if (_log.IsDebugEnabled) _log.Debug( "GetResponse() - Posting to [" + url + "], Data: " + ListUtil.ConvertNameValueCollectionToString(nv));


            string postData = ListUtil.ConvertNameValueCollectionToString(nv, "&", "=", (item => PageUtil.UrlEncode(item)),
                (item => PageUtil.UrlEncode(item)));

            var webResponse = _httpRequestSenderService.SendHttpRequest(url, CS.General_CS_v5.Enums.HttpMethod.POST, _httpRequestSenderService.GetDefaultHeaderCollectionForPost(),
                postData);
            string sResponse = webResponse.GetResponseAsString();

            SetExpressCheckoutPayPalResponse response = new SetExpressCheckoutPayPalResponse(_paypal);
            response.RawResponse = CS.General_CS_v5.Util.PageUtil.UrlDecode(sResponse);
            var qs = new QueryString(sResponse);
            response.ParseFromNameValueCollection(qs);
            
            if (autoRedirectOnSuccess)
            {
                if (response.Success)
                    response.RedirectBrowserToPaypalCheckout();
            }
            return response;
        }

     
    }
        
}
