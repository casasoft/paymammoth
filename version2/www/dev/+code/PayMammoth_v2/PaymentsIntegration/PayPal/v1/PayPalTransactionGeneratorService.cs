﻿using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.EnumsBL;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Modules.InversionOfControl;
using NLog;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Services.Logging;
using PayMammoth_v2.Modules.Services.PaymentMethods;
using PayMammoth_v2.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayPal.PayPalAPIInterfaceService.Model;
using Raven.Client;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1
{
    public interface IPayPalTransactionGeneratorService:IPaymentMethodTransactionGeneratorService
    {

    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PayPalTransactionGeneratorService : IPayPalTransactionGeneratorService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IPayPalManager _payPalManager;
        private readonly IPaymentRequestTransactionDataService _paymentRequestTransactionDataService;
        private readonly IPayMammothLogService _payMammothLogService;

        public PayPalTransactionGeneratorService(IPayPalManager payPalManager, IPaymentRequestTransactionDataService paymentRequestTransactionDataService,
            IPayMammothLogService payMammothLogService)
        {
            _payMammothLogService = payMammothLogService;
            _paymentRequestTransactionDataService = paymentRequestTransactionDataService;
            this._payPalManager = payPalManager;
        }

       
        public TransactionGeneratorResult GenerateTransaction(ReferenceLink<PaymentRequestData> requestId, IDocumentSession session)
        {
            //todo: [For: Karl | 2013/12/31] create unit tests PayPalTransactionGeneratorService.GenerateTransaction (WrittenBy: Karl)
            
            TransactionGeneratorResult result = new TransactionGeneratorResult();
            
            

            var request = session.GetById(new GetByIdParams<PaymentRequestData>(requestId) {ThrowErrorIfNotAlreadyLoadedInSession = false});
            //-------


            var transaction = _paymentRequestTransactionDataService.CreateNewTransaction(requestId,
                Connector.Enums.PaymentMethodType.PayPal_ExpressCheckout, session);
            var response = _payPalManager.StartExpressCheckout(transaction,session);
            result.Result = PayMammoth_v2Enums.RedirectionResultStatus.Error;
            if (response.PayPalResponse.Ack == AckCodeType.SUCCESS)
            {
                 result.Result = PayMammoth_v2Enums.RedirectionResultStatus.Success;
                 result.RedirectionType= CS.General_CS_v5.Enums.HrefTarget.Top;
                 result.UrlToRedirectTo = response.UrlToRedirectTo;
                

            }
            else
            {
              _payMammothLogService.AddLogEntry(transaction.TransactionLog,"Error calling 'StartExpressCheckout'",
                    _log, GenericEnums.NlogLogLevel.Warn);
                
            }


            return result;

        }
    }
}
