﻿using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class UserSelectedOptionsInfo : BasePayPalObject
    {

        /// <summary>
        /// The Yes/No option that was chosen by the buyer for insurance
        /// </summary>
        [PayPalFieldInfo(FieldName = "InsuranceOptionSelected")]
        public string InsuranceOptionSelected { get; set; }

        /// <summary>
        /// Is true if the buyer chose the default shipping option.
        /// </summary>
        [PayPalFieldInfo(FieldName = "ShippingOptionIsDefault")]
        public bool? ShippingOptionIsDefault { get; set; }

        /// <summary>
        /// The shipping amount that was chosen by the buyer
        /// </summary>
        [PayPalFieldInfo(FieldName = "ShippingOptionAmount")]
        public double? ShippingOptionAmount { get; set; }

        /// <summary>
        /// The name of the shipping option such as air or ground.
        /// </summary>
        [PayPalFieldInfo(FieldName = "ShippingOptionName")]
        public string ShippingOptionName { get; set; }

        

    }
        
}
