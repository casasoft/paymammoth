﻿
using BusinessLogic_CS_v5.Aspects;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.EnumsBL;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Modules.InversionOfControl;
using CS.General_CS_v5.Modules.Urls;
using NLog;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Repositories.Payments;
using PayMammoth_v2.Modules.Services.Logging;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using Raven.Client;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers
{
	public interface IExpressCheckoutIpnMessageParser:IIpnMessageParser
	{

	}

	[IocComponent]
	[EnsureNonNullAspect]
	public class ExpressCheckoutIpnMessageParser : IExpressCheckoutIpnMessageParser
	{
	   
		private static readonly Logger _log = LogManager.GetCurrentClassLogger();
		
			
		private readonly IPaymentTransactionRepository _paymentTransactionRepository;
		private readonly IPaymentRequestTransactionDataService _paymentRequestTransactionDataService;
		private readonly IPayMammothLogService _payMammothLogService;

		public ExpressCheckoutIpnMessageParser(IPaymentTransactionRepository paymentTransactionRepository,
			IPaymentRequestTransactionDataService paymentRequestTransactionDataService,
			IPayMammothLogService payMammothLogService)
		{
			_payMammothLogService = payMammothLogService;
			_paymentRequestTransactionDataService = paymentRequestTransactionDataService;
			_paymentTransactionRepository = paymentTransactionRepository;
		}

		#region IpnMessageParser Members

		//private void addIpnResponseToTransaction(QueryString qs, IpnResponse response)
		//{

		//    qs.Add("Ipn PayPal Response", response.PaypalResponse);



		//}
		[RavenTaskAspect]
		public void HandleIpnMessage(IpnMessage ipnMessage, IDocumentSession session = null)
		{
			string paymentRequestTransactionId = ipnMessage.Reference;

			var getParams =new GetByIdParams<PaymentRequestTransactionData>(paymentRequestTransactionId) { ThrowErrorIfNotAlreadyLoadedInSession = false };
			getParams.AddInclude(x=>x.PaymentRequestId);
			var transaction = BusinessLogic_CS_v5.Util.RavenDbUtil.GetById(session,getParams);
			//############
			
			
			_payMammothLogService.AddLogEntry(transaction.TransactionLog,
				string.Format("IpnMessage received related to transaction | {0}",ipnMessage.MessageBlockRaw),_log, GenericEnums.NlogLogLevel.Info);
			//##########
			if (!transaction.IsSuccessful)
			{
				

				if (ipnMessage.PaymentStatus == PayPalEnums.PAYMENT_STATUS.Completed)
				{
					_paymentRequestTransactionDataService.MarkTransactionAsPaid(transaction, false, "", session);
					
				}
				else if (ipnMessage.PaymentStatus == PayPalEnums.PAYMENT_STATUS.Pending)
				{
					// this means that the payment has been 'authorized', but needs to be confirmed by the business owner first
					//check ipnMessage.PendingReason for more info
					_log.Debug("IpnMessage.Pending recieved for transaction [{0}] | Request [{1}].  This is because it needs to be approved by business owner first",
						paymentRequestTransactionId, transaction.PaymentRequestId);
				}

			}
			else
			{
				_payMammothLogService.AddLogEntry(transaction.TransactionLog,
				string.Format("This should never be called when the transaction is already marked as successful. TransactionId: {0}", transaction.Id), _log, GenericEnums.NlogLogLevel.Warn);
				
			}
			
			

		}

		#endregion
	}
}
