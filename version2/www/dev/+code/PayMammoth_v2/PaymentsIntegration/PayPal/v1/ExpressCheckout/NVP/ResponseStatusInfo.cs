﻿using System.Text;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class ResponseStatusInfo : BasePayPalObject
    {
        
        
        [PayPalFieldInfo(FieldName = "ErrorCode")]
        public string ErrorCode { get; protected set; }
        [PayPalFieldInfo(FieldName = "ShortMessage")]
        public string ShortMessage { get; protected set; }
        [PayPalFieldInfo(FieldName = "LongMessage")]
        public string LongMessage { get; protected set; }
        [PayPalFieldInfo(FieldName = "SeverityCode")]
        public string SeverityCode { get; protected set; }

        public string GetAsString()
        {
            return "[" + this.ErrorCode + "] " + this.ShortMessage + " - " + this.LongMessage + " (" + this.SeverityCode + ")";
        }

        

        //public void AddToOperationResult(OperationResult result)
        //{
        //    if (!string.IsNullOrEmpty(this.ErrorCode))
        //    {
        //        result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, GetAsString());
        //    }
        //}

        public bool HasError()
        {
            return !string.IsNullOrEmpty(this.ErrorCode);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Status Info");
            sb.AppendLine("------------");
            sb.AppendLine();
            sb.AppendLine("ErrorCode: " + this.ErrorCode);
            sb.AppendLine();
            sb.AppendLine("ShortMessage: " + this.ErrorCode);
            sb.AppendLine();
            sb.AppendLine("LongMessage: " + this.ErrorCode);
            sb.AppendLine();
            sb.AppendLine("SeverityCode: " + this.ErrorCode);
            sb.AppendLine();

            return sb.ToString();
        } 
    }
        
}
