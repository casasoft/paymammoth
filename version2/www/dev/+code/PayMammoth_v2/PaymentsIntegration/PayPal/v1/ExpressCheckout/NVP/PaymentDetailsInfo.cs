﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP
{
    public class PaymentDetailsInfo 
    {
        public AddressInfo AddressInfo { get; private set; }



        /// <summary>
        /// (Required) The total cost of the transaction to the customer. If shipping cost and tax
/// charges are known, include them in this value; if not, this value should be the
/// current sub-total of the order.
/// If the transaction includes one or more one-time purchases, this field must be equal
/// to the sum of the purchases.
/// Set this field to 0 if the transaction does not include a one-time purchase; for
/// example, when you set up a billing agreement for a recurring payment that is not
/// immediately charged. Purchase-specific fields will be ignored.
/// You can specify up to 10 payments, where n is a digit between 0 and 9, inclusive.
/// Limitations: Must not exceed $10,000 USD in any currency. No currency symbol.
/// Must have two decimal places, decimal separator must be a period (.), and the
        /// optional thousands separator must be a comma (,).
        /// </summary>
        [PayPalFieldInfo(FieldName = "Amt", MaxAmount = 10000)]
        public decimal? TotalCostOfTransaction
        {
            get
            {
                decimal d = 0;
                if (this.ItemsTotalAmountExcTax.HasValue)
                    d += this.ItemsTotalAmountExcTax.Value;
                if (this.ShippingAmount.HasValue)
                    d += this.ShippingAmount.Value;
                if (this.ShippingDiscountAmount.HasValue)
                    d += this.ShippingDiscountAmount.Value;
                if (this.HandlingAmount.HasValue)
                    d += this.HandlingAmount.Value;
                if (this.TaxTotal.HasValue)
                    d += this.TaxTotal.Value;
                //if (this.InsuranceTotalAmount.HasValue)
                  //  d += this.InsuranceTotalAmount.Value;
                if (d > 0)
                    return d;
                else
                    return null;

                
            }
        }


        
        public CS.General_CS_v5.Enums.CurrencyCode_Iso4217? CurrencyCode
        {
            get
            {
                return _CurrencyCode;
            }
            set
            {
                _CurrencyCode = value;
                _CurrencyCode_Str = CS.General_CS_v5.Enums.CurrencyCode_Iso4217ToCode(_CurrencyCode);

            }
        }
        [PayPalFieldInfo(FieldName = "CurrencyCode")]
        private string _CurrencyCode_Str { get; set; }
        private CS.General_CS_v5.Enums.CurrencyCode_Iso4217? _CurrencyCode;

        /// <summary>
        /// (Optional) Sum of cost of all items in this order.
        /// NOTE: PAYMENTREQUEST_n_ITEMAMT is required if you specify
        /// L_PAYMENTREQUEST_n_AMTm.
        /// You can specify up to 10 payments, where n is a digit between 0 and 9, inclusive.
        /// Limitations: Must not exceed $10,000 USD in any currency. No currency symbol.
        /// Must have two decimal places, decimal separator must be a period (.), and the
        /// optional thousands separator must be a comma (,).
        /// </summary>
        [PayPalFieldInfo(FieldName = "ItemAmt", MaxAmount = 10000)]
        public decimal? ItemsTotalAmountExcTax
        {
            get
            {
                decimal d = 0;
                foreach (var item in this.PaymentItems)
                {
                    if (item != null)
                    {
                        decimal? itemTotal = item.GetTotalPriceExcTax();
                        if (itemTotal.HasValue)
                            d += itemTotal.Value;
                    }
                }
               // if (this.InsuranceTotalAmount.HasValue)
               //     d += this.InsuranceTotalAmount.Value;
                if (d > 0)
                    return d;
                else
                    return null;
            }
        }
        /// <summary>
        /// (Optional) Total shipping costs for this order.
        /// NOTE: If you specify a value for PAYMENTREQUEST_n_SHIPPINGAMT, you must
        /// also specify a value for PAYMENTREQUEST_n_ITEMAMT.
        /// You can specify up to 10 payments, where n is a digit between 0 and 9, inclusive.
        /// Character length and limitations: Must not exceed $10,000 USD in any currency.
        /// No currency symbol. Regardless of currency, decimal separator must be a period
        /// (.), and the optional thousands separator must be a comma (,). Equivalent to nine
        /// characters maximum for USD.
        /// </summary>
        [PayPalFieldInfo(FieldName = "ShippingAmt", MaxAmount = 10000)]
        public decimal? ShippingAmount { get; set; }

        /// <summary>
        /// (Optional) Total shipping insurance costs for this order. The value must be a
///non-negative currency amount or null if insurance options are offered.
        /// </summary>
        [PayPalFieldInfo(FieldName = "ShippingAmt", MaxAmount = 10000)]
        public double? InsuranceTotalAmount { get; set; }

        /// <summary>
        /// (Optional) Shipping discount for this order, specified as a negative number
        /// </summary>
        [PayPalFieldInfo(FieldName = "ShipDiscAmt", MaxAmount = 0)]
        public decimal? ShippingDiscountAmount { get; set; }

        /// <summary>
        /// (Optional) If true, the Insurance drop-down on the PayPal Review page displays
        /// the string ‘Yes’ and the insurance amount. If true, the total shipping insurance
        /// for this order must be a positive number.
        /// You can specify up to 10 payments, where n is a digit between 0 and 9, inclusive.
        /// Character length and limitations: true or false
        /// </summary>
        [PayPalFieldInfo(FieldName = "InsuranceOptionOffered")]
        public bool? InsuranceOptionOffered { get; set; }

        /// <summary>
        /// Optional) Total handling costs for this order.
        /// NOTE: If you specify a value for PAYMENTREQUEST_n_HANDLINGAMT, you must
        /// also specify a value for PAYMENTREQUEST_n_ITEMAMT.
        /// You can specify up to 10 payments, where n is a digit between 0 and 9, inclusive.
        /// Character length and limitations: Must not exceed $10,000 USD in any currency.
        /// No currency symbol. Regardless of currency, decimal separator must be a period
        /// (.), and the optional thousands separator must be a comma (,). Equivalent to nine
        /// characters maximum for USD.
        /// </summary>
        [PayPalFieldInfo(FieldName = "HandlingAmt")]
        public decimal? HandlingAmount { get; set; }

        /// <summary>
        /// Tax Amount
        /// </summary>
        [PayPalFieldInfo(FieldName = "TaxAmt")]
        public decimal? TaxTotal
        {
            get
            {
                decimal d = 0;
                foreach (var item in this.PaymentItems)
                {
                    if (item != null)
                    {
                        decimal? taxAmt = item.GetTotalTaxAmount();
                        if (taxAmt.HasValue)
                        {
                            d += taxAmt.Value;
                        }

                    }
                }
                if (d > 0)
                    return d;
                else
                    return null;


            }
        }
        /// <summary>
        /// (Optional) Description of items the customer is purchasing.
        /// The value you specify is only available if the transaction includes a purchase; this
        /// field is ignored if you set up a billing agreement for a recurring payment that is not
        /// immediately charged.
        /// You can specify up to 10 payments,
        /// </summary>
        [PayPalFieldInfo(FieldName = "Desc")]
        public string Description { get; set; }

        /// <summary>
        /// Do not prompt buyers to include a note with their payments. Allowable values are:  0 – provide a text box and prompt for the note  1 – hide the text box and the prompt The default is 0.
        /// </summary>
        [PayPalFieldInfo(FieldName = "no_note", MaxLength =1)]
        private string _IncludeNote { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IncludeNote
        {
            get { return _IncludeNote == "0"; }
            set { _IncludeNote = value ? "0" : "1";}
        }


        /// <summary>
        /// Optional) A free-form field for your own use.
        /// </summary>
        [PayPalFieldInfo(FieldName = "Custom")]
        public string CustomField { get; set; }

        

        /// <summary>
        /// (Optional) Your own invoice or tracking number.
        /// </summary>
        [PayPalFieldInfo(FieldName = "InvNum", MaxLength = 127)]
        public string ReferenceNum { get; set; }

        /// <summary>
        /// (Optional) Your URL for receiving Instant Payment Notification (IPN) about this
        /// transaction. If you do not specify this value in the request, the notification URL
        /// from your Merchant Profile is used, if one exists.
        /// The notify URL only applies to
///DoExpressCheckoutPayment. This value is ignored when
///set in SetExpressCheckout or
///GetExpressCheckoutDetails.
        /// </summary>
        [PayPalFieldInfo(FieldName = "NotifyUrl", MaxLength = 2048)]
        public string NotifyUrl { get; set; }

        /// <summary>
        /// 
        /// Note to the merchant.
        /// </summary>
        [PayPalFieldInfo(FieldName = "NoteText", MaxLength = 255)]
        public string Note { get; set; }

        /// <summary>
        /// A per transaction description of the payment that is passed to the buyer’s credit
///card statement.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SoftDescriptor", MaxLength = 255)]
        public string TransactionSoftDescriptor { get; set; }

        /// <summary>
        /// Optional) Transaction identification number of the transaction that was
///created.
        /// </summary>
        [PayPalFieldInfo(FieldName = "TransactionID", MaxLength = 255)]
        public string PaypalTransactionID { get; set; }


        /// <summary>
        /// How you want to obtain payment. When implementing parallel payments, this field
/// is required and must be set to Order.
/// Sale indicates that this is a final sale for which you are requesting payment.
/// (Default)
/// Authorization indicates that this payment is a basic authorization subject to
/// settlement with PayPal Authorization & Capture.
/// Order indicates that this payment is an order authorization subject to
/// settlement with PayPal Authorization & Capture. 
        /// If the transaction does not include a one-time purchase, this field is ignored.
        /// </summary>
        public PayPalEnums.PAYMENT_ACTION? PaymentAction
        {
            get
            {
                return CS.General_CS_v5.Util.EnumUtil.ConvertStringToNullableEnum<PayPalEnums.PAYMENT_ACTION>(_PaymentAction_Str);
            }
            set
            {
                string s = "";
                if (value.HasValue)
                {
                    s = CS.General_CS_v5.Util.EnumUtil.ConvertEnumToString(value.Value);
                }
                _PaymentAction_Str = s; 

            }
        }
        [PayPalFieldInfo(FieldName = "PaymentAction")]
        protected string _PaymentAction_Str { get; set; }

        
       
        /// <summary>
        /// A unique identifier of the specific payment request, which is required for parallel payments.
        /// </summary>
        [PayPalFieldInfo(FieldName = "PaymentRequestID", MaxLength = 127)]
        public string PaymentRequestID { get; set; }

        

        public List<PaymentDetailsItemInfo> PaymentItems { get; private set; }
        public PaymentDetailsItemInfo AddNewPaymentItem()
        {
            PaymentDetailsItemInfo info = new PaymentDetailsItemInfo();
            this.PaymentItems.Add(info);
            return info;
        }

        public SellerDetailsInfo SellerInfo { get; private set; }
        

        /// <summary>
        /// Contains information about the success of the response and any error information
        /// </summary>
        public ResponseStatusInfo ResponseStatusInfo { get; private set; }
        

        private void checkReq()
        {
            StringBuilder errMsg = new StringBuilder();
            if (this.PaymentItems.Count == 0)
            
                errMsg.AppendLine("At least one payment item is required");
            if (!string.IsNullOrEmpty(errMsg.ToString()))
            {
                throw new InvalidOperationException("SetExpressCheckoutRequest: " + errMsg.ToString());
            }
        }
        private static string getPrependKey(int id)
        {
            return "PaymentRequest_" + id + "_";
        }
        private static string getPrependKeyForItem(int id)
        {
            return "L_" + getPrependKey(id);
        }
        public void FillNameValueCollection(NameValueCollection nv, int id)
        {
            checkReq();


            string prependkey = getPrependKey(id);
            Util.GeneralUtil.AddPaypalFieldsToNameValueColl(nv, this, prependkey, null);
            int index =0;

            string itemPrepend = getPrependKeyForItem(id);
            foreach (var item in this.PaymentItems)
            {
                item.AppendToNameValue(nv, itemPrepend, index);
               index++;
            }
            //this.AddressInfo.FillNameValueCollection(nv, prependkey, null); ///18/Nov/2011 - this was like this, but i removed the prependkey
            this.AddressInfo.FillNameValueCollection(nv, prependkey, null);
            
        }

        public PaymentDetailsInfo()
        {
            //this.ClientShippingInfo = new AddressTypeInfo();
            this.CurrencyCode = CS.General_CS_v5.Enums.CurrencyCode_Iso4217.EuroMemberCountriesEuro;
            this.PaymentItems = new List<PaymentDetailsItemInfo>();
            this.AddressInfo = new AddressInfo();
           
        }

        public void ParseFromNameValueCollection(NameValueCollection nv, int id)
        {
            string keyPrepend = getPrependKey(id);
            Util.GeneralUtil.FillObjectFromPaypalNameValueColl(nv, this, keyPrepend, null);
            
            this.PaymentItems = PaymentDetailsItemInfo.ParseFromNameValueCollection(nv, getPrependKeyForItem(id));

            this.ResponseStatusInfo = new ResponseStatusInfo(); this.ResponseStatusInfo.ParseFromNameValueCollection(nv, keyPrepend, null);
            this.AddressInfo = new AddressInfo(); this.AddressInfo.ParseFromNameValueCollection(nv, keyPrepend, null);
            this.SellerInfo = new SellerDetailsInfo(); this.SellerInfo.ParseFromNameValueCollection(nv, keyPrepend, null);
            if (this.PaymentAction == null)
                this.PaymentAction = PayPalEnums.PAYMENT_ACTION.Sale;

        }

        
        protected bool isValid()
        {
            return (this.PaymentItems.Count > 0 || !string.IsNullOrEmpty(this.AddressInfo.CustomerName));
        }

        public static List<PaymentDetailsInfo> ParseFromNameValueCollection(NameValueCollection nv)
        {
            List<PaymentDetailsInfo> list = new List<PaymentDetailsInfo>();
            for (int i = 0; i < 9; i++)
            {
                string prependKey = getPrependKey(i);
                PaymentDetailsInfo info = new PaymentDetailsInfo();
                info.ParseFromNameValueCollection(nv, i);
                if (info.isValid())
                    list.Add(info);
            }
            return list;
        }
    }
        
}
