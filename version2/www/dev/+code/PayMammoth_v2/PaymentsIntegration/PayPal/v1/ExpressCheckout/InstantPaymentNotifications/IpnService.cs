﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Util.Helpers;
using PayMammoth_v2.Modules.Data.PaymentIntegrations.PayPalIntegration;
using Raven.Client;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IIpnService
    {
        bool CheckIfIpnMessageAlreadyVerifiedInDatabase(string ipnTransactionId, PayPalEnums.PAYMENT_STATUS paymentStatus, IDocumentSession session);
        PayPalProcessedIpnMsgData SaveIpnMessageAsProcessedInDatabase(IpnMessage ipnMessage, IDocumentSession session = null);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class IpnService : IIpnService
    {
        private readonly IPayPalProcessedIpnMsgRepository _payPalProcessedIpnMsgRepository;

        public IpnService(IPayPalProcessedIpnMsgRepository payPalProcessedIpnMsgRepository)
        {
            _payPalProcessedIpnMsgRepository = payPalProcessedIpnMsgRepository;
        }

        public bool CheckIfIpnMessageAlreadyVerifiedInDatabase(string ipnTransactionId, PayPalEnums.PAYMENT_STATUS paymentStatus, IDocumentSession session)
        {
            var ipnMsg= _payPalProcessedIpnMsgRepository.GetIpnMsgByTransactionIdAndStatus(ipnTransactionId, paymentStatus, session);

            return (ipnMsg != null);
        }


        [RavenTaskAspect]
        public PayPalProcessedIpnMsgData SaveIpnMessageAsProcessedInDatabase(IpnMessage ipnMessage, IDocumentSession session = null)
        {
            //todo: [For: Backend | 2014/03/28] update unit tests to check for TransactionId and PaymentStatus (WrittenBy: Karl)        			
            PayPalProcessedIpnMsgData msg = new PayPalProcessedIpnMsgData();
            msg.IpnMessage = ipnMessage;
            msg.TranscationId = ipnMessage.PayPalTransactionID;
            msg.PaymentStatus = ipnMessage.PaymentStatus;

            session.Store(msg);
            return msg;
        }
    }
}
