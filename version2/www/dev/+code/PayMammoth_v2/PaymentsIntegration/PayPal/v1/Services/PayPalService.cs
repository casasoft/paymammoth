﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Modules.InversionOfControl;
using CS.General_CS_v5.Modules.Urls;
using NLog;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.Logging;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.NVP;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses;
using PayPal.PayPalAPIInterfaceService.Model;
using Raven.Client;


namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1
{
    public interface IPayPalService
    {
        void UpdateTransactionWithPayPalResponse(ReferenceLink<PaymentRequestTransactionData> transactionId, BasePayPalResponse payPalResponse,
            IDocumentSession session);
        void UpdateTransactionWithPayPalResponse(ReferenceLink<PaymentRequestTransactionData> transactionId, AbstractResponseType payPalResponse,
            IDocumentSession session);
        string GetUrlToRedirectToAfterSetExpressCheckout(SetExpressCheckoutResponseType setExpressCheckoutResponse, IPayPalCredentials payPalCredentials);
    }

    [IocComponent]
    [EnsureNonNullAspect]
    public class PayPalService : IPayPalService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IPayMammothLogService _payMammothLogService;


        public PayPalService(IPayMammothLogService payMammothLogService)
        {
            _payMammothLogService = payMammothLogService;
        }

        public string GetUrlToRedirectToAfterSetExpressCheckout(SetExpressCheckoutResponseType setExpressCheckoutResponse,
            IPayPalCredentials payPalCredentials)
        {
            //todo: [For: Backend | 2014/03/26] unit-test PayPalService.GetUrlToRedirectToAfterSetExpressCheckout (WrittenBy: Backend)
            var nv = new QueryString();
            nv[PayPalConstants.PARAM_TOKEN] = setExpressCheckoutResponse.Token;
            nv[PayPalConstants.PARAM_CMD] = PayPalConstants.COMMAND_EXPRESS_CHECKOUT;
            //var currentSettings = _paypalSettingService.GetCurrentSettings(this._paypal.Settings);
            string url = payPalCredentials.ApiServerUrl + "?" + nv.ToString();
            
            return url;
            
        
        }




        public void UpdateTransactionWithPayPalResponse(ReferenceLink<PaymentRequestTransactionData> transactionId, BasePayPalResponse payPalResponse, 
            IDocumentSession session)
        {
            var transaction = session.GetById(new GetByIdParams<PaymentRequestTransactionData>(transactionId));
            _payMammothLogService.AddLogEntry(transaction.TransactionLog,
                string.Format("CorrelationID: {0} | Timestamp: {1} | Version: {2}",payPalResponse.CorrelationID,payPalResponse.TimeStamp,payPalResponse.Version),_log);
            session.Store(transaction);
        }
        public void UpdateTransactionWithPayPalResponse(ReferenceLink<PaymentRequestTransactionData> transactionId, AbstractResponseType payPalResponse,
            IDocumentSession session)
        {
            //todo: [For: Backend | 2014/03/26] unit tests for PayPalService.UpdateTransactionWithPayPalResponse (WrittenBy: Backend)
            var transaction = session.GetById(new GetByIdParams<PaymentRequestTransactionData>(transactionId));
            _payMammothLogService.AddLogEntry(transaction.TransactionLog,
                string.Format("CorrelationID: {0} | Timestamp: {1} | Version: {2}", payPalResponse.CorrelationID, payPalResponse.Timestamp, payPalResponse.Version), _log);
            session.Store(transaction);
        }
    }
}
