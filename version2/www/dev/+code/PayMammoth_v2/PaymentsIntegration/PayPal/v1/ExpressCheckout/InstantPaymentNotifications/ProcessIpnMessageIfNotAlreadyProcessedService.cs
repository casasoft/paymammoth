﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.EnumsBL;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Util.Helpers;
using NLog;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Services.Logging;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers;
using Raven.Client;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IProcessIpnMessageIfNotAlreadyProcessedService
    {
        /// <summary>
        /// This processes an IPN message, AFTER it has been verified from PayPal that it is authentic. 
        /// </summary>
        void Process(ReferenceLink<PaymentRequestData> paymentRequestId, IpnMessage ipnMessage, IDocumentSession session = null);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class ProcessIpnMessageIfNotAlreadyProcessedService : IProcessIpnMessageIfNotAlreadyProcessedService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IIpnService _ipnService;
        private readonly IGetIpnMessageParserBasedOnTransactionTypeService _getIpnMessageParserBasedOnTransactionTypeService;
        private readonly IPayMammothLogService _payMammothLogService;

        public ProcessIpnMessageIfNotAlreadyProcessedService(IIpnService ipnService,
            IGetIpnMessageParserBasedOnTransactionTypeService getIpnMessageParserBasedOnTransactionTypeService,
            IPayMammothLogService payMammothLogService)
        {
            _payMammothLogService = payMammothLogService;
            _getIpnMessageParserBasedOnTransactionTypeService = getIpnMessageParserBasedOnTransactionTypeService;
            _ipnService = ipnService;
        }

        [RavenTaskAspect]
        public void Process(ReferenceLink<PaymentRequestData> paymentRequestId, IpnMessage ipnMessage, IDocumentSession session = null)
        {
            var paymentRequest = BusinessLogic_CS_v5.Util.RavenDbUtil.GetById<PaymentRequestData>(session,
                new GetByIdParams<PaymentRequestData>(paymentRequestId) { ThrowErrorIfNotAlreadyLoadedInSession = false});

            //##########

            bool alreadyVerified = _ipnService.CheckIfIpnMessageAlreadyVerifiedInDatabase(ipnMessage.PayPalTransactionID,ipnMessage.PaymentStatus, session);
            if (!alreadyVerified)
            {
                _payMammothLogService.AddLogEntry(paymentRequest.StatusLog,
                    string.Format("IpnMessage processed - PayPalTransactionID: {0} | IpnTrackId: {1}", ipnMessage.PayPalTransactionID, ipnMessage.IpnTrackId), _log,
                            GenericEnums.NlogLogLevel.Debug);
                _ipnService.SaveIpnMessageAsProcessedInDatabase(ipnMessage, null);
                var ipnMsgParser = _getIpnMessageParserBasedOnTransactionTypeService.GetIpnMessageParserBasedOnTransactionType(ipnMessage.TransactionType);
                ipnMsgParser.HandleIpnMessage(ipnMessage, null);
            }
            else
            {
                _payMammothLogService.AddLogEntry(paymentRequest.StatusLog,
                    string.Format("Skipping, IpnMessage already processed - PayPalTransactionID: {0} | IpnTrackId: {1}", ipnMessage.PayPalTransactionID, ipnMessage.IpnTrackId), _log,
                            GenericEnums.NlogLogLevel.Debug);
                
                //do nothing
            }

            
        }
    }
}
