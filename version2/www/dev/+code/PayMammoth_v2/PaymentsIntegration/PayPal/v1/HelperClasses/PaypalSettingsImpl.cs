﻿namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.HelperClasses
{
    public class PayPalSettingsImpl : IPayPalSettings
    {
        public PayPalSettingsImpl()
        {
            this.SandboxCredentials = new PayPalCredentialsImpl();
            this.LiveCredentials = new PayPalCredentialsImpl();
        }

        #region IPaypalSettings Members

        public PayPalCredentialsImpl SandboxCredentials { get; private set; }

        public PayPalCredentialsImpl LiveCredentials { get; private set; }
        public bool UseLiveEnvironment { get; set; }

        #endregion

        #region IPaypalSettings Members

        IPayPalCredentials IPayPalSettings.SandboxCredentials
        {
            get { return this.SandboxCredentials; }
        }

        IPayPalCredentials IPayPalSettings.LiveCredentials
        {
            get { return this.LiveCredentials; }
        }

        #endregion
    }
}
