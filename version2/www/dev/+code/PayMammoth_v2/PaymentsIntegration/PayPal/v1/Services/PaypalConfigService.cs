﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Modules.Services.Settings;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.Util;
using Raven.Client;

namespace PayMammoth_v2.PaymentsIntegration.PayPal.v1.Services
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IPaypalConfigService
    {
        Dictionary<string, string> GetPayPalConfig(ReferenceLink<WebsiteAccountData> websiteAccountId, IDocumentSession session); 
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class PaypalConfigService : IPaypalConfigService
    {
        private readonly IPayPalSettingsService _payPalSettingsService;
        private readonly ISettingsService _settingsService;


        public PaypalConfigService(IPayPalSettingsService payPalSettingsService,
            ISettingsService settingsService)
        {
            _settingsService = settingsService;
            _payPalSettingsService = payPalSettingsService;
            
        }

        public Dictionary<string, string> GetPayPalConfig(ReferenceLink<WebsiteAccountData> websiteAccountId, IDocumentSession session)
        {
            //Check out https://github.com/paypal/merchant-sdk-dotnet/tree/master/PayPalMerchantSDK

            //todo: [For: Backend | 2014/03/26] unit-test PaypalConfigService.GetPayPalConfig (WrittenBy: Backend)
            Dictionary<string, string> paypalConfig = new Dictionary<string, string>();

            var payPalSettings = _payPalSettingsService.GetPaypalSettings(websiteAccountId, session);
            var currentCredentials = _payPalSettingsService.GetCurrentSettings(payPalSettings);

            paypalConfig.Add("mode", GeneralUtil.GetSandboxOrLiveEnvironmentMode(payPalSettings.UseLiveEnvironment)); //one of 'sandbox' or 'live'
            paypalConfig.Add("connectionTimeout", _settingsService.GetSettingValue<int>(PayMammoth_v2Enums.PayMammothSettings.PayPal_v1_RequestTimeout).ToString());
            paypalConfig.Add("requestRetries", _settingsService.GetSettingValue<int>(PayMammoth_v2Enums.PayMammothSettings.PayPal_v1_RequestRetries).ToString());
            

            paypalConfig.Add("account1.apiUsername", currentCredentials.Username);
            paypalConfig.Add("account1.apiPassword", currentCredentials.Password);
            paypalConfig.Add("account1.apiSignature", currentCredentials.Signature);
            return paypalConfig;

        }
    }
}
