﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Aspects;
using BusinessLogic_CS_v5.Aspects.NullAspects;
using BusinessLogic_CS_v5.EnumsBL;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Util;
using BusinessLogic_CS_v5.Util.Helpers;
using CS.General_CS_v5.Modules.InversionOfControl;
using NLog;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.Payments.Helpers;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.Logging;
using PayMammoth_v2.Modules.Services.PaymentMethods;
using PayMammoth_v2.Modules.Services.PaymentMethods.TransactionGenerators;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2.PaymentsIntegration.Transactium.v1.Services;
using PayMammoth_v2.PaymentsIntegration.Transactium.v1.Wrappers;
using PayPal;
using Raven.Client;

namespace PayMammoth_v2.PaymentsIntegration.Transactium.v1
{
    public interface ITransactiumTransactionGeneratorService : IPaymentMethodTransactionGeneratorService
    {

    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class TransactiumTransactionGeneratorService : ITransactiumTransactionGeneratorService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IPaymentRequestTransactionDataService _paymentRequestTransactionDataService;
        private readonly IPayMammothLogService _payMammothLogService;
        private readonly IGetGatewayClientForWebsiteAccountService _getGatewayClientForWebsiteAccountService;
        private readonly IFillCreatePaymentRequestFromTransactionService _fillCreatePaymentRequestFromTransactionService;

        public TransactiumTransactionGeneratorService(IPaymentRequestTransactionDataService paymentRequestTransactionDataService,
            IPayMammothLogService payMammothLogService,
            IGetGatewayClientForWebsiteAccountService getGatewayClientForWebsiteAccountService,
            IFillCreatePaymentRequestFromTransactionService fillCreatePaymentRequestFromTransactionService)
        {
            _fillCreatePaymentRequestFromTransactionService = fillCreatePaymentRequestFromTransactionService;
            _getGatewayClientForWebsiteAccountService = getGatewayClientForWebsiteAccountService;
            _payMammothLogService = payMammothLogService;
            _paymentRequestTransactionDataService = paymentRequestTransactionDataService;
          
        }


        public TransactionGeneratorResult GenerateTransaction(ReferenceLink<PaymentRequestData> requestId, IDocumentSession session = null)
        {
            //todo: [For: Karl | 2013/12/31] create unit tests PayPalTransactionGeneratorService.GenerateTransaction (WrittenBy: Karl)

            TransactionGeneratorResult result = new TransactionGeneratorResult();
            result.Result = PayMammoth_v2Enums.RedirectionResultStatus.Error;

            var getParams = new GetByIdParams<PaymentRequestData>(requestId) { ThrowErrorIfNotAlreadyLoadedInSession = false };
            getParams.AddInclude(x => x.LinkedWebsiteAccountId);

            var request = session.GetById(getParams);
            var websiteAccount = session.GetById(new GetByIdParams<WebsiteAccountData>(request.LinkedWebsiteAccountId));
            //-------


            var transaction = _paymentRequestTransactionDataService.CreateNewTransaction(requestId, Connector.Enums.PaymentMethodType.PaymentGateway_Transactium, session);
            transaction.PaymentGatewaySpecificInfo.TransactiumInfo = new TransactiumInfo();
            var gatewayClient = _getGatewayClientForWebsiteAccountService.GetGatewayClientForWebsiteAccount(websiteAccount);

            var transactiumRequest = _fillCreatePaymentRequestFromTransactionService.FillCreatePaymentRequestFromTransaction(websiteAccount,
                transaction,request, session);
            bool error = false;
            MyHPSPayment transactiumResponse = null;
            try
            {
                transactiumResponse = gatewayClient.CreatePaymentRequest(transactiumRequest);

            }
                
            catch (Exception ex)
            {
                string errMsg = "";

                if (ex.Message.Contains("AccessDenied"))
                {
                    errMsg = "Access Denied while calling Transactium.CreatePaymentRequest.  This is probably due to an invalid username/password/profile " +
                        "tag in the Transactium Details.  Also, make sure you check Staging if you are testing Staging, and live if you are testing Live." ;
                }
                else
                {
                    errMsg = "Error occurred while calling Transactium.CreatePaymentRequest";
                }


                error = true;
                _payMammothLogService.AddLogEntry(request, errMsg,
                    _log, GenericEnums.NlogLogLevel.Error,ex);
            }
            if (!error)
            {
                result.Result = PayMammoth_v2Enums.RedirectionResultStatus.Success; ;
                result.RedirectionType = CS.General_CS_v5.Enums.HrefTarget.Self;
                result.UrlToRedirectTo = transactiumResponse.RedirectURL;

                transaction.PaymentGatewaySpecificInfo.TransactiumInfo.HpsId = transactiumResponse.HPSID;
                transaction.PaymentGatewaySpecificInfo.TransactiumInfo.PaymentStatuses.Add(
                                 new TransactiumInfo.MyHpsPaymentInfo("CreatePayment",transactiumResponse));
            }



            return result;

        }
    }
}
