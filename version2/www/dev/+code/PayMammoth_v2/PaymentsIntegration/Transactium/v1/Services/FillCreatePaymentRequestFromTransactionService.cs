﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Culture;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2.PaymentsIntegration.Transactium.v1.Wrappers;
using PayMammoth_v2.Presentation.Services.Payments;
using Raven.Client;

namespace PayMammoth_v2.PaymentsIntegration.Transactium.v1.Services
{
    using BusinessLogic_CS_v5.Aspects;
    using BusinessLogic_CS_v5.Aspects.NullAspects;
    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface IFillCreatePaymentRequestFromTransactionService
    {
        MyHPSCreatePaymentRequest FillCreatePaymentRequestFromTransaction(WebsiteAccountData websiteAccount, 
            PaymentRequestTransactionData transaction,
            PaymentRequestData request, IDocumentSession session);
    }

    [IocComponent]
    [LogAspect]
    [EnsureNonNullAspect]
    public class FillCreatePaymentRequestFromTransactionService : IFillCreatePaymentRequestFromTransactionService
    {
        
        private readonly IGetTotalPriceForRequestService _getTotalPriceForRequestService;
        private readonly IPaymentUrlModelService _paymentUrlModelService;
        private readonly IGetCultureModelForPaymentRequestService _getCultureModelForPaymentRequestService;

        public FillCreatePaymentRequestFromTransactionService(
            IGetTotalPriceForRequestService getTotalPriceForRequestService,
            IPaymentUrlModelService paymentUrlModelService,
            IGetCultureModelForPaymentRequestService getCultureModelForPaymentRequestService)
        {
            _getCultureModelForPaymentRequestService = getCultureModelForPaymentRequestService;
            _paymentUrlModelService = paymentUrlModelService;
            _getTotalPriceForRequestService = getTotalPriceForRequestService;
        }

        public MyHPSCreatePaymentRequest FillCreatePaymentRequestFromTransaction(WebsiteAccountData websiteAccount,
            PaymentRequestTransactionData transaction, PaymentRequestData request, IDocumentSession session)
        {
            //todo: [For: Backend | 2014/03/28] implement unit-test (WrittenBy: Karl)        			

            MyHPSCreatePaymentRequest transactiumRequest = new MyHPSCreatePaymentRequest();
            decimal total = _getTotalPriceForRequestService.GetTotalPrice(request, true, true);
            int totalInCents = (int)Math.Round(total * 100);
            transactiumRequest.Amount = totalInCents;
            transactiumRequest.Currency = EnumsTransactium.GetCurrencyCodeFromString(request.RequestDetails.Pricing.CurrencyCode3Letter);
            transactiumRequest.PaymentType = EnumsTransactium.HPSPaymentType.Sale;

            var transactionIdOnly = BusinessLogic_CS_v5.Util.RavenDbUtil.GetRavenDbIdOnlyFromFullId(transaction.Id);

            transactiumRequest.HPSProfileTag = websiteAccount.PaymentsInformation.Transactium.ProfileTag; //payment profile
            transactiumRequest.MaxTimeLimit = 0; //max time limit
            transactiumRequest.OrderReference = transactionIdOnly; //order reference
            transactiumRequest.ClientReference = null; //client reference

            
          //  transactiumRequest.ClientIPRestriction = CS.General_v3.Util.PageUtil.GetUserIP(); //customer IP

            

            transactiumRequest.ClientBillingCountry = EnumsTransactium.GetCountryCodeFromISOAsTwoLetterCode(request.RequestDetails.Details.ClientContactDetails.CountryAsEnum).GetValueOrDefault( EnumsTransactium.CountryCodes.NotSet);
            transactiumRequest.ClientEmail = request.RequestDetails.Details.ClientContactDetails.Email; //customer email address

            var cultureModel = _getCultureModelForPaymentRequestService.GetCultureModelForPaymentRequest(request);

            string successUrl = TransactiumUrls.GetSuccessUrl(transactionIdOnly);
            string failureUrl = TransactiumUrls.GetFailureUrl(transactionIdOnly);
            //string failureUrl = _paymentUrlModelService.GetPaymentSelectionPage(session, cultureModel, request.Id);
            //string paymentUrl = TransactiumUrls.GetPaymentStatusUrl(transactionIdOnly);

            transactiumRequest.SuccessURL = successUrl; //URL to redirect in case of success
            transactiumRequest.FailURL = failureUrl; //URL to redirect in case of error
            

            
            transactiumRequest.LanguageCode = ConstantsTransactium.GetLanguageCode(request.RequestDetails.Language,
                request.RequestDetails.Details.ClientContactDetails.CountryAsEnum);

            return transactiumRequest;

            
        }
    }
}
