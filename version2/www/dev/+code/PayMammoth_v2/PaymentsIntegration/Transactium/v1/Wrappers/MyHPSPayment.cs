﻿using System;
using System.Collections.Generic;

namespace PayMammoth_v2.PaymentsIntegration.Transactium.v1.Wrappers
{
    public class MyHPSPayment
    {
        private object _HPSPayment = null;
        public MyHPSPayment()
        {

        }
        public MyHPSPayment(object HPSPayment)
        {


            if (HPSPayment is TransactiumWebService.v1.Live.HPSPayment)
                FillFromLive((TransactiumWebService.v1.Live.HPSPayment)HPSPayment);
            else if (HPSPayment is TransactiumWebService.v1.Staging.HPSPayment)
                FillFromStaging((TransactiumWebService.v1.Staging.HPSPayment)HPSPayment);
            else
                throw new InvalidOperationException("Object must be either of type Live/Staging HPSPayment");
            
        }
        public string GetAuthCode()
        {
            return this.Transactions[0].HostAuthorisationCode;
        }

        //public void AddDetailsToTransactionParameters(IPaymentRequestTransaction transaction)
        //{
        //    transaction.SetPaymentParametersFromObject(this,null,
        //                                                         x => x.HPSID,
        //                                                         x => x.WSVersion,
        //                                                         x => x.PaymentMethod,
        //                                                         x => x.PaymentType,
        //                                                         x => x.Deferred,
        //                                                         x => x.ErrorCode);
        //    if (this.Transactions != null)
        //    {
        //        for (int i = 0; i < this.Transactions.Count; i++)
        //        {
        //            string key = "transaction"+(i+1)+"_";
        //            var t= this.Transactions[i];
        //            transaction.SetPaymentParametersFromObject(t, key,
        //                x => x.FSBlockReason,
        //                x => x.HostAuthorisationCode,
        //                x => x.HostCode,
        //                x => x.HostMessage,
        //                x => x.IPCountry,
        //                x => x.TransactionDateTime,
        //                x => x.TransactionID,
        //                x => x.TransactionResultStatus);

        //        }
        //    }
        //}


        public long Amount { get; set; }
        public EnumsTransactium.CountryCodes ClientBillingCountry { get; set; }
        public string ClientEmail { get; set; }
        public string ClientIPRestriction { get; set; }
        public string ClientReference { get; set; }
        public DateTime CreateDateTime { get; set; }
        public EnumsTransactium.CurrencyCode Currency { get; set; }
        public bool Deferred { get; set; }
        public DateTime EndDateTime { get; set; }
        public string GetErrorCode()
        {
            string s = this.ErrorCode;
            if (string.IsNullOrWhiteSpace(s))
            {
                if (this.Transactions != null)
                {
                    for (int i = 0; i < this.Transactions.Count; i++)
                    {
                        var t = this.Transactions[i];
                        if (!string.IsNullOrWhiteSpace(t.HostCode))
                        {
                            s = t.HostCode;
                            break;
                        }
                    }
                }
            }
            return s;
            
        }

        public string ErrorCode { get; set; }
        public string FailURL { get; set; }
        public string HPSID { get; set; }
        public string HPSProfileTag { get; set; }
        public string LanguageCode { get; set; }
        public EnumsTransactium.UserActivity LastUserActivity { get; set; }
        public DateTime LastUserActivityDateTime { get; set; }
        public int MaxTimeLimit { get; set; }
        public string OrderReference { get; set; }
        public EnumsTransactium.PaymentMethodType PaymentMethod { get; set; }
        public EnumsTransactium.HPSPaymentType PaymentType { get; set; }
        public bool ProceedWithPurchase { get; set; }
        public string RedirectURL { get; set; }
        public DateTime StartDateTime { get; set; }
        public EnumsTransactium.HPStatus Status { get; set; }
        public string SuccessURL { get; set; }
        public string WSVersion { get; set; }
        public List<MyCardPayment> Transactions { get; set; }


        public TransactiumWebService.v1.Live.HPSPayment ConvertToLive()
        {
            if (!(_HPSPayment is TransactiumWebService.v1.Live.HPSPayment))
            {
                TransactiumWebService.v1.Live.HPSPayment payment = new TransactiumWebService.v1.Live.HPSPayment();
                payment.Amount = this.Amount;
                payment.ClientBillingCountry = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Live.CountryCodes>(this.ClientBillingCountry);
                payment.ClientEmail = this.ClientEmail;
                payment.ClientIPRestriction = this.ClientIPRestriction;
                payment.ClientReference = this.ClientReference;
                payment.CreateDateTime = this.CreateDateTime;
                payment.Currency = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Live.CurrencyCode>(this.Currency); ;
                payment.Deferred = this.Deferred;
                payment.EndDateTime = this.EndDateTime;
                payment.ErrorCode = this.ErrorCode;
                payment.FailURL = this.FailURL;
                payment.HPSID = this.HPSID;
                payment.HPSProfileTag = this.HPSProfileTag;
                payment.LanguageCode = this.LanguageCode;
                payment.LastUserActivity = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Live.UserActivity>(this.LastUserActivity); ;
                payment.LastUserActivityDateTime = this.LastUserActivityDateTime;
                payment.MaxTimeLimit = this.MaxTimeLimit;
                payment.OrderReference = this.OrderReference;
                payment.PaymentMethod = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Live.PaymentMethodType>(this.PaymentMethod); ;
                payment.PaymentType = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Live.HPSPaymentType>(this.PaymentType); ;
                payment.ProceedWithPurchase = this.ProceedWithPurchase;
                payment.RedirectURL = this.RedirectURL;
                payment.StartDateTime = this.StartDateTime;
                payment.Status = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Live.HPStatus>(this.Status); ;
                payment.SuccessURL = this.SuccessURL;
                payment.WSVersion = this.WSVersion;

                return payment;
            }
            else
            {
                return (TransactiumWebService.v1.Live.HPSPayment)_HPSPayment;
            }
        }
        public TransactiumWebService.v1.Staging.HPSPayment ConvertToStaging()
        {
            if (!(_HPSPayment is TransactiumWebService.v1.Staging.HPSPayment))
            {
                TransactiumWebService.v1.Staging.HPSPayment payment = new TransactiumWebService.v1.Staging.HPSPayment();
                payment.Amount = this.Amount;
                payment.ClientBillingCountry = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Staging.CountryCodes>(this.ClientBillingCountry); 
                payment.ClientEmail = this.ClientEmail;
                payment.ClientIPRestriction = this.ClientIPRestriction;
                payment.ClientReference = this.ClientReference;
                payment.CreateDateTime = this.CreateDateTime;
                payment.Currency = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Staging.CurrencyCode>(this.Currency); 
                payment.Deferred = this.Deferred;
                payment.EndDateTime = this.EndDateTime;
                payment.ErrorCode = this.ErrorCode;
                payment.FailURL = this.FailURL;
                payment.HPSID = this.HPSID;
                payment.HPSProfileTag = this.HPSProfileTag;
                payment.LanguageCode = this.LanguageCode;
                payment.LastUserActivity = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Staging.UserActivity>(this.LastUserActivity);
                payment.LastUserActivityDateTime = this.LastUserActivityDateTime;
                payment.MaxTimeLimit = this.MaxTimeLimit;
                payment.OrderReference = this.OrderReference;
                payment.PaymentMethod = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Staging.PaymentMethodType>(this.PaymentMethod);
                payment.PaymentType = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Staging.HPSPaymentType>(this.PaymentType);
                payment.ProceedWithPurchase = this.ProceedWithPurchase;
                payment.RedirectURL = this.RedirectURL;
                payment.StartDateTime = this.StartDateTime;
                payment.Status = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Staging.HPStatus>(this.Status);
                payment.SuccessURL = this.SuccessURL;
                payment.WSVersion = this.WSVersion;

                return payment;
            }
            else
            {
                return (TransactiumWebService.v1.Staging.HPSPayment)_HPSPayment;
            }
        }
        public void FillFromStaging(TransactiumWebService.v1.Staging.HPSPayment payment)
        {
            _HPSPayment = payment;

            this.Amount = payment.Amount;
            this.ClientBillingCountry = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.CountryCodes>(payment.ClientBillingCountry); 
            
            this.ClientEmail = payment.ClientEmail;
            this.ClientIPRestriction = payment.ClientIPRestriction;
            this.ClientReference = payment.ClientReference;
            this.CreateDateTime = payment.CreateDateTime;
            this.Currency = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.CurrencyCode>(payment.Currency);
            this.Deferred = payment.Deferred;
            this.EndDateTime = payment.EndDateTime;
            this.ErrorCode = payment.ErrorCode;
            this.FailURL = payment.FailURL;
            this.HPSID = payment.HPSID;
            this.HPSProfileTag = payment.HPSProfileTag;
            this.LanguageCode = payment.LanguageCode;
            this.LastUserActivity = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.UserActivity>(payment.LastUserActivity); 
            this.LastUserActivityDateTime = payment.LastUserActivityDateTime;
            this.MaxTimeLimit = payment.MaxTimeLimit;
            this.OrderReference = payment.OrderReference;
            this.PaymentMethod = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.PaymentMethodType>(payment.PaymentMethod);
            this.PaymentType = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.HPSPaymentType>(payment.PaymentType); 
            this.ProceedWithPurchase = payment.ProceedWithPurchase;
            this.RedirectURL = payment.RedirectURL;
            this.StartDateTime = payment.StartDateTime;
            this.Status = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.HPStatus>(payment.Status); 
            this.SuccessURL = payment.SuccessURL;
            this.WSVersion = payment.WSVersion;

            this.Transactions = new List<MyCardPayment>();
            foreach (var t in payment.Transactions)
            {
                if (t is TransactiumWebService.v1.Staging.CardPayment)
                {
                    this.Transactions.Add(new MyCardPayment(t));
                }
            }

        }
        public void FillFromLive(TransactiumWebService.v1.Live.HPSPayment payment)
        {
            _HPSPayment = payment;

            this.Amount = payment.Amount;
            this.ClientBillingCountry = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.CountryCodes>(payment.ClientBillingCountry); 
            this.ClientEmail = payment.ClientEmail;
            this.ClientIPRestriction = payment.ClientIPRestriction;
            this.ClientReference = payment.ClientReference;
            this.CreateDateTime = payment.CreateDateTime;
            this.Currency = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.CurrencyCode>(payment.Currency); 
            this.Deferred = payment.Deferred;
            this.EndDateTime = payment.EndDateTime;
            this.ErrorCode = payment.ErrorCode;
            this.FailURL = payment.FailURL;
            this.HPSID = payment.HPSID;
            this.HPSProfileTag = payment.HPSProfileTag;
            this.LanguageCode = payment.LanguageCode;
            this.LastUserActivity = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.UserActivity>(payment.LastUserActivity); 
            this.LastUserActivityDateTime = payment.LastUserActivityDateTime;
            this.MaxTimeLimit = payment.MaxTimeLimit;
            this.OrderReference = payment.OrderReference;
            this.PaymentMethod = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.PaymentMethodType>(payment.PaymentMethod); 
            this.PaymentType = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.HPSPaymentType>(payment.PaymentType); 
            this.ProceedWithPurchase = payment.ProceedWithPurchase;
            this.RedirectURL = payment.RedirectURL;
            this.StartDateTime = payment.StartDateTime;
            this.Status = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.HPStatus>(payment.Status); 
            this.SuccessURL = payment.SuccessURL;
            this.WSVersion = payment.WSVersion;
            this.Transactions = new List<MyCardPayment>();
            foreach (var t in payment.Transactions)
            {
                if (t is TransactiumWebService.v1.Live.CardPayment)
                {
                    this.Transactions.Add(new MyCardPayment(t));
                }
            }

        }
    }
}
