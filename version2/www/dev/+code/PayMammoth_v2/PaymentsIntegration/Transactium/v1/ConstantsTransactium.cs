﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.General_CS_v5.Util;

namespace PayMammoth_v2.PaymentsIntegration.Transactium.v1
{
    public class ConstantsTransactium
    {
        public const string HPSID = "HPSID";
        //public static string GetHpsIdFromQuerystring()
        //{
        //    return PageUtil.GetVariableFromQuerystring(HPSID);
        //}
        public static string GetLanguageCode(Connector.Enums.SupportedLanguage language,
            CS.General_CS_v5.Enums.CountryIso3166? languageCountry)
        {
            string s = "en-GB";
            switch (language)
            {
                case Connector.Enums.SupportedLanguage.English:
                    {
                        if (languageCountry == CS.General_CS_v5.Enums.CountryIso3166.UnitedStates)
                            s = "en-US";
                        else
                            s = "en-GB";
                        break;

                    }
                //case CS.General_CS_v5.Enums.LanguageCode_Iso639.Spanish: s = "es-ES"; break;
                //case CS.General_CS_v5.Enums.LanguageCode_Iso639.Italian: s = "it-IT"; break;
                //case CS.General_CS_v5.Enums.LanguageCode_Iso639.Maltese: s = "mt-MT"; break;
                //case CS.General_CS_v5.Enums.LanguageCode_Iso639.Portuguese: s = "pt-BR"; break;
                //case CS.General_CS_v5.Enums.LanguageCode_Iso639.French: s = "fr-FR"; break;
                //case CS.General_CS_v5.Enums.LanguageCode_Iso639.German: s = "de-DE"; break;
                default:
                    s = "en-GB"; break;

            }
            return s;
        }
    }
}
