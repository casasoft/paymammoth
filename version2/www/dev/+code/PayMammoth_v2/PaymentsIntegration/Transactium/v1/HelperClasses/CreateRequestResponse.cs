﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayMammoth_v2.PaymentsIntegration.Transactium.v1.Wrappers;

namespace PayMammoth_v2.PaymentsIntegration.Transactium.v1.HelperClasses
{
    public class CreateRequestResponse
    {
        public MyHPSPayment PaymentResponse { get; set; }
        
        public CreateRequestResponse()
        {
            
        }

    }
}
