﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Raven.Client.Embedded;

namespace PayMammoth_v2_Tests
{
    [TestFixture]
    public class Class1
    {
        public class StupidClass2
        {
            public string Title { get; set; }
            public string Id { get; set; }

        }
         [Test]
        public void Test2()
        {
            int k = 5;
        }

        
        public void Test()
        {
            CS.General_CS_v5.Util.OtherUtil.SetIsInUnitTestingEnvironment();
            
            PayMammoth_v2.Framework.Application.AppInstancePayMammoth_v2.Instance.OnApplicationStart();

            var documentStore = BusinessLogic_CS_v5.Framework.RavenDb.RavenDbManager.Instance.DocumentStore;

            string id = null;
            using (var session = documentStore.OpenSession())
            {
                StupidClass2 s = new StupidClass2();
                s.Title = "hello";
                session.Store(s);
                session.SaveChanges();
                id = s.Id;
            }
            using (var session = documentStore.OpenSession())
            {
                StupidClass2 s = session.Load<StupidClass2>(id);
                s.Title = "hello2";
                
            }
            System.Threading.Thread.Sleep(30000);
        }
    }
}
