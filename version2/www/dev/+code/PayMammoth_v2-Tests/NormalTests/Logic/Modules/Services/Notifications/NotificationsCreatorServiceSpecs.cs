﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DateAndTime;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using General_Tests_CS_v5.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using PayMammoth_v2.Connector;
using PayMammoth_v2.Modules.Data.Notifications;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.Notifications;
using Raven.Client;

namespace PayMammoth_v2_Tests.NormalTests.Logic.Modules.Services.Notifications
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v5.TestUtil;
    using BusinessLogic_CS_v5.Extensions;
    using CS.General_CS_v5.Extensions;


    [TestFixture]
    public class NotificationsCreatorServiceSpecs
    {

        [TestFixture]
        public class CreateNotificationMessageForSuccessfulPaymentAndSendSpecs
        {
            [TestFixture]
            public class given_such_data : MySpecsFor<NotificationsCreatorService>
            {
                protected PaymentRequestTransactionData _transaction;
                protected PaymentRequestData _paymentRequest;
                protected WebsiteAccountData _websiteAccount;


                protected override void Given()
                {

                    _transaction = new PaymentRequestTransactionData();
                    _transaction.Id = "Transaction/1";
                    _paymentRequest = new PaymentRequestData();
                    _paymentRequest.Id = "Request/1";

                    _websiteAccount = new WebsiteAccountData();
                    _websiteAccount.Id = "WebsiteAccount/1";


                    _transaction.PaymentRequestId = _paymentRequest;
                    _paymentRequest.WebsiteAccountId = _websiteAccount;


                    var mockSession = new Mock<IDocumentSession>();
                    mockSession.SetupLoad(_transaction);
                    mockSession.SetupLoad(_paymentRequest);
                    mockSession.SetupLoad(_websiteAccount);

                    base.Given();
                }


            }

            [TestFixture]
            public class given_website_account_does_not_contain_a_notification_url : given_such_data
            {


                protected override void Given()
                {
                    

                    

                    base.Given();

                    _websiteAccount.Notifications.ClientWebsiteResponseUrl = "";


                }

                protected override void When()
                {
                    SUT.CreateNotificationMessageForSuccessfulPaymentAndSend(_transaction, _mockedSession.Object);
                    base.When();
                }

                [Test]
                public void then_no_message_should_be_created()
                {
                    GetMockFor<IDataObjectFactory>().Verify(x => x.CreateNewDataObject<NotificationMessageData>(), Times.Never());


                }
            }

            [TestFixture]
            public class given_website_account_contains_a_notification_url : given_such_data
            {
                private DateTimeOffset _currentTime;
                private NotificationMessageData _notificationMessage;


                protected override void Given()
                {
                    base.Given();
                    _notificationMessage = new NotificationMessageData();
                    GetMockFor<IDataObjectFactory>().Setup(x => x.CreateNewDataObject<NotificationMessageData>()).Returns(_notificationMessage);
                    _currentTime = new DateTimeOffset(2012, 1, 2, 16, 7, 35, new TimeSpan());
                    GetMockFor<ICurrentDateTimeRetrieverService>().Setup(x => x.GetCurrentDateTime()).Returns(_currentTime);
                    _websiteAccount.Notifications.ClientWebsiteResponseUrl = "http://www.karlcassar.com/test-notify.ashx"; 

                }

                protected override void When()
                {

                    base.When();
                    SUT.CreateNotificationMessageForSuccessfulPaymentAndSend(_transaction, _mockedSession.Object);
                }

                [Test]
                public void then_notification_msg_should_be_created_and_saved()
                {

                    _mockedSession.Verify(x => x.Store(_notificationMessage), Times.Once());

                }
                [Test]
                public void then_details_should_be_correct()
                {

                    _notificationMessage.CreatedOn.ShouldEqual(_currentTime);
                    _notificationMessage.LinkedPaymentRequestId.ShouldBeSameAs(_paymentRequest.Id);
                    _notificationMessage.NotificationType.ShouldEqual(Enums.NotificationMessageType.ImmediatePayment);
                    _notificationMessage.SendToUrl.ShouldEqual(_websiteAccount.Notifications.ClientWebsiteResponseUrl);
                    _notificationMessage.Status.ShouldEqual(Enums.NotificationMessageStatus.Pending);
                    

                }
            }

        }
    }

}
