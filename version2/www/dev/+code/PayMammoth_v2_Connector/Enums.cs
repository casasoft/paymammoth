﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Connector
{
    public static class Enums
    {
        public enum NotificationConfirmationStatus
        {
            InvalidFormData,
            OK,
            NotificationCouldNotBeConfirmed
        }

        public enum RecurringProfilePaymentStatus
        {
            Pending,
            Failed,
            Success
        }
        /// <summary>
        /// Status messages used when created a recurring profile on a payment gateway.
        /// </summary>
        public enum RecurringProfileCreateStatusMsg
        {
            /// <summary>
            /// The Recurring profile was created successfully.
            /// </summary>
            Success,
            /// <summary>
            /// The user account does not allow recurring profile from being created for a reason or another.
            /// </summary>
            UserProfileError,
            /// <summary>
            /// An error occured from the gateway side - this could be caused also by incorrect information being supplied to the gateway, see logs.
            /// </summary>
            GatewayError,
            /// <summary>
            /// A buyer's country of residence (Payment Account Holder) does not match the shipping country of the transaction.
            /// </summary>
            BuyerCountryDoesNotMatchShippingCountry,
            /// <summary>
            /// An error in code logic within PayPippa has occured - see the logs.
            /// </summary>
            InternalError
        }
        public enum NotificationMessageStatus
        {
            Pending,
            Failed,
            Success
        }
        public enum NotificationMessageType
        {
            ImmediatePaymentSuccess,
            ImmediatePaymentExpired,
            ImmediatePaymentCancelled,
            RecurringPaymentFailure,
            /// <summary>
            /// This occurs when a payment is skipped. A skipped payment means when a payment is not done for some reason.  This is done in line to mimic PayPal functionality, where it will try 3 times, before
            /// marking a payment as failed.
            /// </summary>
            RecurringPaymentSkipped,
            RecurringPaymentSuccess,

            /// <summary>
            /// Cancelled means that the user willfully stopped it
            /// </summary>
            RecurringProfileCancelled,
            /// <summary>
            /// Suspended means that payment could not be taken, hence it was suspended
            /// </summary>
            RecurringProfileSuspended,
            /// <summary>
            /// Expired means that the profile has reached its billing limit / time period, e.g profile was for a total of 1 year.
            /// </summary>
            RecurringProfileExpired,
            RecurringProfileCouldNotCreate,


            RecurringProfileActivated,

            CancelRecurringProfile,




        }
        //When you add a PaymentMethodSpecific, update the 'GetPaymentMethodFromSpecific' method below.
        public enum PaymentMethodType
        {
            
            [Description("PayPal")]
            PayPal_ExpressCheckout = 1000,
            [Description("Online Payment")]
            PaymentGateway_Transactium = 2000,
            [Description("Online Payment")]
            PaymentGateway_Apco = 2200,
            [Description("Online Payment")]
            PaymentGateway_Realex = 2400,
            [Description("Money Bookers")]
            Moneybookers = 3000,
            [Description("Cheque")]
            Cheque = 4000,
            [Description("Bank Transfer")]
            BankTransfer = 5000,
            [Description("Neteller")]
            Neteller = 6000,
            [Description("Testing")]
            NUnitTest = 10000
           
        }
        public static bool GetWhetherPaymentMethodRequiresManualIntervention(PaymentMethodType method)
        {
            switch (method)
            {
                case PaymentMethodType.Cheque:
                    return true;
            }
            return false;
        }
        public enum RecurringBillingPeriod
        {
            Day,
            Month,
            Year
        }
        public enum SupportedLanguage
        {
            English
        }

        public static CS.General_CS_v5.Enums.LanguageCode_Iso639 ConvertSupportedLanguageToGeneralLangaugeCode(
            SupportedLanguage supportedLanguage)
        {
            switch (supportedLanguage)
            {
                case SupportedLanguage.English: return CS.General_CS_v5.Enums.LanguageCode_Iso639.English;
                default:
                    throw new InvalidOperationException("Not yet implemented for this language code");
            }
            

        }
        public enum InitialRequestResponseStatus
        {
            WebsiteAccountDoesNotExist,
            InvalidData,
            CouldNotConnect,
            ErrorWhenSendingMessage,
            CouldNotDecrypt,
            PriceMustBeGreaterThanZero,
            ReturnUrlSuccessIsRequired,
            ReturnUrlFailureIsRequired,
            NotificationUrlIsRequred,
            IpAddressNotValid,
            Success,
            PleaseSpecifyAtLeastOneItem,
            ItemPriceMustNeverBeZeroOrLess,
            CurrencyCodeNotFilledIn,
            CurrencyCodeInvalid,
            ItemQuantityMustBeOneOrGreater,
            ItemTitleMustBeFilledIn

        }
    }
}
