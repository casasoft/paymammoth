﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Connector.Exceptions
{
    public class PayMammothConnectorException : Exception
    {
        public PayMammothConnectorException(string msg, Exception innerEx) : base(msg,innerEx)
        {

        }
    }
}
