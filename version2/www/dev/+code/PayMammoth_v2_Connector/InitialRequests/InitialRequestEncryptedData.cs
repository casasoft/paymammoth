﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Connector.InitialRequests
{
    public class InitialRequestEncryptedData
    {
        /// <summary>
        /// This should be equal to primary key of website account.
        /// </summary>
        public string WebsiteAccountCode { get; set; }
        /// <summary>
        /// This should be encrypted
        /// </summary>
        public string Data { get; set; }
    }
}
