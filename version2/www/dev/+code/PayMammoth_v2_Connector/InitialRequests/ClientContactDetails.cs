﻿using System.Collections.Generic;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;

namespace PayMammoth_v2.Connector.InitialRequests
{
    public class ClientContactDetails
    {
        

        public string Name { get; set; }


        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string State { get; set; }

        

        public string Country3LetterCode { get; set; }

        [JsonIgnore]
        public CS.General_CS_v5.Enums.CountryIso3166? CountryAsEnum
        {
            get
            {
                return CS.General_CS_v5.Enums.CountryIso3166FromCode(this.Country3LetterCode);
            }
            set
            {
                this.Country3LetterCode = CS.General_CS_v5.Enums.CountryIso3166To3LetterCode(value);
            }
        }

        public string Email { get; set; }

        public string IpAddress { get; set; }

        public string LastName { get; set; }

        public string Locality { get; set; }

        public string MiddleName { get; set; }

        public string Mobile { get; set; }

        public string PostCode { get; set; }

        
        public string Telephone { get; set; }

        public string GetFullName()
        {
            List<string> list = new List<string>();
            list.Add(Name);
            list.Add(MiddleName);
            list.Add(LastName);
            list.RemoveAll(x => string.IsNullOrWhiteSpace(x));
            string s = "";
            for (int i = 0; i < list.Count; i++)
            {
                if (i > 0) s += " ";
                s += list[i];
            }
            return s;
        }

    }
}
