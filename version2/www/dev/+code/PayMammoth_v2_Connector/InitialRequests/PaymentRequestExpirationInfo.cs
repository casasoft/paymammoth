﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Connector.InitialRequests
{
    public class PaymentRequestExpirationInfo
    {
        /// <summary>
        /// This means that if this is set, the payment request will expire if this time is elapsed, and a message is sent back
        /// to the client website that the request is expired
        /// </summary>
        public DateTimeOffset? ExpiresAt { get; set; }
    }
}
