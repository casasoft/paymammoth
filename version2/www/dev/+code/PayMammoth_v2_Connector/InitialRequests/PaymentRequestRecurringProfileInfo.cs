﻿namespace PayMammoth_v2.Connector.InitialRequests
{
    public class PaymentRequestRecurringProfileInfo
    {
        public bool Required { get; set; }
        public int IntervalFrequency { get; set; }
        public Connector.Enums.RecurringBillingPeriod IntervalType { get; set; }
        public int MaxFailedAttempts { get; set; }
        public int? TotalBillingCycles { get; set; }
    }
}
