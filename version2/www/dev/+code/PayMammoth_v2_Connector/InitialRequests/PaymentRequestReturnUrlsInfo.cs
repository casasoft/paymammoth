﻿namespace PayMammoth_v2.Connector.InitialRequests
{
    public class PaymentRequestReturnUrlsInfo
    {
        /// <summary>
        /// Required.  The URL for the user to be redirected to, upon failure.
        /// </summary>
        public string FailureUrl { get; set; }
        /// <summary>
        /// Required.  The URL for the user to be redirected to, upon success.
        /// </summary>
        public string SuccessUrl { get; set; }

    }
}
