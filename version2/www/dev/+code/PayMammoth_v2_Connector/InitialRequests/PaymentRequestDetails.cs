﻿namespace PayMammoth_v2.Connector.InitialRequests
{
    public class PaymentRequestDetails
    {
        private ClientContactDetails _m_ClientContactDetails;

        public ClientContactDetails ClientContactDetails
        {
            get
            {
                if (_m_ClientContactDetails == null)
                {
                    _m_ClientContactDetails = new ClientContactDetails();
                }
                return _m_ClientContactDetails;
            }
            set { _m_ClientContactDetails = value; }
        }
        
        public string OrderReference { get; set; }
        public string ClientReference { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        
        
    }
}
