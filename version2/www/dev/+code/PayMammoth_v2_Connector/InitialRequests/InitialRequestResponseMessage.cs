﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PayMammoth_v2.Connector.InitialRequests
{
    public class InitialRequestResponseMessage
    {
        public string RequestId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Enums.InitialRequestResponseStatus Status { get; set; }
    }
}
