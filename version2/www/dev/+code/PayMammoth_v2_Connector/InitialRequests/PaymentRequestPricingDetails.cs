﻿namespace PayMammoth_v2.Connector.InitialRequests
{
    public class PaymentRequestPricingDetails
    {
        public decimal ShippingAmount { get; set; }
        public decimal HandlingAmount { get; set; }

        /// <summary>
        /// Required.  Must be one from the ISO4217 standard
        /// </summary>
        public string CurrencyCode3Letter { get; set; }

        public CS.General_CS_v5.Enums.CurrencyCode_Iso4217? GetCurrencyAsCurrencyCodeIso4217()
        {
            return CS.General_CS_v5.Enums.CurrencyCode_Iso4217FromCode(CurrencyCode3Letter);
        }
        public void SetCurrencyCodeFromCurrencyCodeIso4217(CS.General_CS_v5.Enums.CurrencyCode_Iso4217 currency)
        {
            this.CurrencyCode3Letter = CS.General_CS_v5.Enums.CurrencyCode_Iso4217ToCode(currency);
            
        }
    }
}
