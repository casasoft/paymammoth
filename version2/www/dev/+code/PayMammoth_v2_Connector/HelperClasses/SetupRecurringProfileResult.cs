﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMammoth_v2.Connector.HelperClasses
{
    public class SetupRecurringProfileResult
    {
        public Enums.RecurringProfileCreateStatusMsg Status { get; set; }
        public string Message { get; set; }
    }
}
