﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CS.General_CS_v5.Modules.InversionOfControl;
using CS.General_CS_v5.Modules.Web;
using NLog;
using PayMammoth_v2.Connector.Notifications;

namespace PayMammoth_v2.Connector.Services
{
    
    public interface IConfirmNotificationMessageOnPayMammothService
    {
        bool ConfirmNotificationMessageOnPayMammoth(string formData);
    }

    [IocComponent]
    public class ConfirmNotificationMessageOnPayMammothService : IConfirmNotificationMessageOnPayMammothService
    {
      
    

        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IHttpRequestSenderService _httpRequestSenderService;
        //private static readonly ConfirmNotificationMessageOnPayMammothService _instance = new ConfirmNotificationMessageOnPayMammothService();
        

        //public static ConfirmNotificationMessageOnPayMammothService Instance
        //{
        //    get { return _instance; }
        //}

        public ConfirmNotificationMessageOnPayMammothService(IHttpRequestSenderService httpRequestSenderService)
        {
            _httpRequestSenderService = httpRequestSenderService;
        }

        /// <summary>
        /// This will confirm back on PayMammoth, the notification message data sent by to the client.
        /// </summary>
        /// <param name="formData"></param>
        /// <returns></returns>
        public bool ConfirmNotificationMessageOnPayMammoth(string formData)
        {
            //todo: [For: Backend | 2014/03/26] unit-test NotificationsService.ConfirmNotificationMessageOnPayMammoth (WrittenBy: Backend)

            bool confirmed = false;
            NotificationMessage msg = CS.General_CS_v5.Util.JsonUtil.Deserialise<NotificationMessage>(formData, throwErrorIfCannotDeserialize: false);
            if (msg != null)
            {
                try
                {
                    NotificationConfirmMessage confirmMessage = new NotificationConfirmMessage();
                    confirmMessage.MessageType = msg.MessageType;
                    confirmMessage.NotificationId = msg.NotificationId;
                    string confirmMessageJson = CS.General_CS_v5.Util.JsonUtil.Serialize(confirmMessage);

                    var webResponse = _httpRequestSenderService.SendHttpRequest(Constants.PayMammoth_ConfirmMsgUrl,
                               CS.General_CS_v5.Enums.HttpMethod.POST, null,
                               confirmMessageJson, null, 60 * 1000, null, null);
                    var statusCode = webResponse.StatusCode;
                    if (statusCode == HttpStatusCode.OK)
                    {
                        var responseString = webResponse.GetResponseAsString();
                        if (responseString == PayMammoth_v2.Connector.Constants.RESPONSE_OK)
                        {
                            confirmed = true;
                        }
                        else
                        {
                            _log.Warn("Notification message could not be confirmed. Result: {0} | FormData: {1}", responseString,confirmMessageJson);
                        }
                    }
                    else
                    {
                        _log.Warn("Notification message could not be confirmed. StatusCode: {0}",statusCode);
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorException("Error occurred in ConfirmNotificationMessageOnPayMammoth", ex);
                    confirmed = false;
                }

            }
            else
            {
                _log.Trace("Notification message coult not be parsed. FormData: '{0}'", formData);
                
            }
            return confirmed;
        }



    }
}
