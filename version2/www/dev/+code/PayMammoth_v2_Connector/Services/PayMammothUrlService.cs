﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CS.General_CS_v5.Modules.InversionOfControl;
using PayMammoth_v2.Connector.InitialRequests;

namespace PayMammoth_v2.Connector.Services
{
    public interface IPayMammothConnectorUrlService
    {
        string GetPayMammothUrlForRequest(string requestId, PayMammoth_v2.Connector.Enums.SupportedLanguage language);
        string GetPayMammothUrlForRequest(string requestId, InitialRequestInfo requestInfo);
    }

    [IocComponent]
    public class PayMammothConnectorConnectorUrlService : IPayMammothConnectorUrlService
    {
        /// <summary>
        /// {0} is the culture, e.g 'en'.  {1} is the request ID.
        /// </summary>
        public static string PayMammothRequestUrl = Constants.PayMammoth_ServerUrl + "{0}/payment?identifier={1}";
             
        //private static readonly PayMammothConnectorConnectorUrlService _instance = new PayMammothConnectorConnectorUrlService();

        //public static PayMammothConnectorConnectorUrlService Instance
        //{
        //    get { return _instance; }
        //}

        public string GetPayMammothUrlForRequest(string requestId, PayMammoth_v2.Connector.Enums.SupportedLanguage language)
        {
            //todo: [For: Karl | 2014/05/05] make this use the supported language (WrittenBy: Karl)        			
            return string.Format(PayMammothRequestUrl,HttpUtility.UrlEncode("en"),
                HttpUtility.UrlEncode(requestId));
        }
        public string GetPayMammothUrlForRequest(string requestId, InitialRequestInfo requestInfo)
        {
            return GetPayMammothUrlForRequest(requestId, requestInfo.Language);
        }
    }
}
