﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS.General_CS_v5.Modules.InversionOfControl;
using NLog;
using PayMammoth_v2.Connector.Notifications;

namespace PayMammoth_v2.Connector.Services
{

    using CS.General_CS_v5.Modules.InversionOfControl;

    public interface INotificationsHandlerService
    {
        string HandleNotificationResponseData(string data);
        event NotificationsHandlerService.OnNotificationMessageReceviedDelegate OnNotificationMessageRecevied;
    }

    [IocComponent]
    public class NotificationsHandlerService : INotificationsHandlerService
    {
        public NotificationsHandlerService(IConfirmNotificationMessageOnPayMammothService confirmNotificationMessageOnPayMammothService)
        {
            _confirmNotificationMessageOnPayMammothService = confirmNotificationMessageOnPayMammothService;
        }

        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IConfirmNotificationMessageOnPayMammothService _confirmNotificationMessageOnPayMammothService;

        public delegate void OnNotificationMessageReceviedDelegate(NotificationMessage msg);

        /// <summary>
        /// This event is called when a notification is successfully received and confirmed back with PayMammoth
        /// </summary>
        public event OnNotificationMessageReceviedDelegate OnNotificationMessageRecevied;
        public string HandleNotificationResponseData(string data)
        {
            string response = PayMammoth_v2.Connector.Constants.RESPONSE_ERROR;


            bool confirmed = _confirmNotificationMessageOnPayMammothService.ConfirmNotificationMessageOnPayMammoth(data);
            if (confirmed && OnNotificationMessageRecevied != null)
            {
                NotificationMessage msg = CS.General_CS_v5.Util.JsonUtil.Deserialise<NotificationMessage>(data,
                    throwErrorIfCannotDeserialize: false);

                response = PayMammoth_v2.Connector.Constants.RESPONSE_OK;
                CS.General_CS_v5.Util.ThreadUtil.CallMethodAsAsyncTask(() => OnNotificationMessageRecevied(msg));
            }
            return response;
        }
    }
}
