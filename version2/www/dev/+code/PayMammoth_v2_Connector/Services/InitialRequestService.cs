﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CS.General_CS_v5.Modules.InversionOfControl;
using CS.General_CS_v5.Modules.Web;
using NLog;
using PayMammoth_v2.Connector.Exceptions;
using PayMammoth_v2.Connector.InitialRequests;

namespace PayMammoth_v2.Connector.Services
{
    public interface IInitialRequestService
    {
        InitialRequestResponseMessage CreatePaymentRequestOnPayMammoth(
            string websiteAccountCode, 
            string secretWord,
            InitialRequestInfo initialRequestInfo);
    }

    [IocComponent]

    public class InitialRequestService : IInitialRequestService
    {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();
        //private static readonly InitialRequestService _instance = new InitialRequestService(HttpRequestSenderService.Instance);
        private readonly IHttpRequestSenderService _httpRequestSenderService;

        //public static InitialRequestService Instance
        //{
        //    get { return _instance; }
        //}

        public InitialRequestService(
            IHttpRequestSenderService httpRequestSenderService)
        {
            _httpRequestSenderService = httpRequestSenderService;
        }

        public InitialRequestResponseMessage CreatePaymentRequestOnPayMammoth(
            string websiteAccountCode, 
            string secretWord,
            InitialRequestInfo initialRequestInfo)
        {

            if (string.IsNullOrWhiteSpace(websiteAccountCode)) throw new InvalidOperationException("websiteAccountCode cannot be empty or null");
            if (string.IsNullOrWhiteSpace(secretWord)) throw new InvalidOperationException("secretWord cannot be empty or null");
            //------------
            InitialRequestResponseMessage result = new InitialRequestResponseMessage();
            result.Status = Enums.InitialRequestResponseStatus.CouldNotConnect;

            InitialRequestEncryptedData encryptedData = new InitialRequestEncryptedData();
            encryptedData.WebsiteAccountCode = websiteAccountCode;

            string initialRequestAsJson = CS.General_CS_v5.Util.JsonUtil.Serialize(initialRequestInfo);
            string encryptedInitialRequestJson = CS.General_CS_v5.Util.CryptographyUtil.Encrypt(initialRequestAsJson,secretWord);

            encryptedData.Data = encryptedInitialRequestJson;

            string encryptedJson = CS.General_CS_v5.Util.JsonUtil.Serialize(encryptedData);
            //string encryptedJsonBase64 = CS.General_CS_v5.Util.TextUtil.Base64Encode(encryptedJson);
            try
            {
                var defaultHeaders = _httpRequestSenderService.GetDefaultHeaderCollectionForPost();

                
                

                var response = _httpRequestSenderService.SendHttpRequest(PayMammoth_v2.Connector.Constants.PayMammoth_InitialRequestUrl,
                     CS.General_CS_v5.Enums.HttpMethod.POST,
                     defaultHeaders, encryptedJson);
                _log.Debug("Response received. Status: {0}", response.StatusCode);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string responseResultJson = response.GetResponseAsString();
                    var responseMsg = CS.General_CS_v5.Util.JsonUtil.Deserialise<InitialRequestResponseMessage>(responseResultJson);
                    result = responseMsg;
                }
                else
                {
                    _log.Warn("Response status code not OK. Status: {0}", response.StatusCode);
                    result.Status = Enums.InitialRequestResponseStatus.ErrorWhenSendingMessage; 
                }
            }
            catch (Exception ex)
            {
                throw new PayMammothConnectorException(
                    string.Format("Cound not send request to paymammoth - PayMammothUrl: {0}", PayMammoth_v2.Connector.Constants.PayMammoth_InitialRequestUrl)
                    , ex);
                
            }
            return result;
        }

    }
}
