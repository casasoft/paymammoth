﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PayMammoth_v2.Connector.Notifications
{
    public class NotificationConfirmMessage
    {
        public string NotificationId { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Enums.NotificationMessageType MessageType { get; set; }

    }
}
