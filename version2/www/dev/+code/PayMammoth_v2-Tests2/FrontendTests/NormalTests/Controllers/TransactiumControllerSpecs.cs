﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Layouts;
using BusinessLogic_CS_v5.Presentation.Models._Shared.PartialViews.NotificationMessages;
using BusinessLogic_CS_v5.Presentation.Services.NotificationMessages;
using CS.General_CS_v5.Classes.HelperClasses;
using CS.General_CS_v5.Modules.Pages;
using PayMammoth_v2.Connector;
using PayMammoth_v2.Enums;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.PaymentsIntegration.Transactium.v1;
using PayMammoth_v2.PaymentsIntegration.Transactium.v1.HelperClasses;
using PayMammoth_v2.Presentation.Code.ViewData;
using PayMammoth_v2.Presentation.Models.Home;
using PayMammoth_v2.Presentation.Models.Transactium;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments.PayPal;
using PayMammoth_v2.Presentation.Services.Payments;
using PayMammoth_v2.Presentation.Services.Payments.PayPal;
using PayMammoth_v2.Presentation.Services.Transactions;
using PayMammoth_v2Deploy.Controllers;

namespace PayMammoth_v2_Tests2.FrontendTests.NormalTests.Controllers
{
    using Moq;
    using NUnit.Framework;
    using General_Tests_CS_v5.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v5.TestUtil;
    using BusinessLogic_CS_v5.Extensions;
    using CS.General_CS_v5.Extensions;
    using System.Web.Mvc;
    using BusinessLogic_CS_v5.Presentation.Code.ContextData;
    using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Sections;
    using BusinessLogic_CS_v5.Presentation.Services.Context;
    using BusinessLogic_CS_v5.Presentation.Services.Sections;

    [TestFixture]
    public class TransactiumControllerSpecs
    {

       
        public abstract class PaymentSpecs :
            MySpecsForControllerAction<TransactiumController, TransactiumPaymentModel>
        {
            private CommonViewContextData _commonViewContextData;
            private SectionDataModel _sectionModel;
            private LayoutViewData _layoutViewData;
            private string _redirectUrl;

            public override ActionResult ExecuteAction()
            {
                return SUT.Payment();
            }



            protected override void Given()
            {
                //nothing
                base.Given();

                //Common Given setup

                _commonViewContextData = new CommonViewContextData();
                GetMockFor<ICommonViewContextDataService>()
                    .Setup(x => x.GetCommonViewContextData())
                    .Returns(_commonViewContextData);

                _sectionModel = new SectionDataModel();
                GetMockFor<ISectionModelService>()
                    .Setup(x => x.GetSectionDataModelByIdentifierOrCreateNew(_mockedSessionObject,
                        _cultureModel,
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Payment_Transactium))
                    .Returns(_sectionModel);


                _layoutViewData = new LayoutViewData()
                {
                    PaymentRequestDataModel = new PaymentRequestDataModel()
                    {
                        PaymentRequestId = "RequestABC"
                    }
                };
                GetMockFor<IPageService>()
                    .Setup(
                        x =>
                            x.GetContextObject<LayoutViewData>(
                                PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey,
                                true))
                    .Returns(_layoutViewData);

                _redirectUrl = "http://www.google.com";
                GetMockFor<IPageService>()
                        .Setup(
                            x =>
                                x.GetVariableFromRoutingQuerystringOrForm<string>(
                                    PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_Transactium_RedirectUrl,
                                    false))
                        .Returns(_redirectUrl);

                _mockedContentTexts.MockItem(PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.Transactium_PaymentDetailsTitle);
            }

            protected override void When()
            {
                //Home's action PaymentSelection is called automatically through the When() method of this base class

                base.When();
            }

            public override string GetViewNameThroughT4Mvc()
            {
                return MVCPayMammoth_v2.Transactium.Views.Payment;
            }
            public class given_payment_is_called : PaymentSpecs
            {

                protected override void Given()
                {
                    //nothing
                    base.Given();

                    //Common Given setup




                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_common_view_context_section_should_be_set()
                {
                    _commonViewContextData.Page.CurrentSection.ShouldEqual(_sectionModel);
                }
                
                [Test]
                public void then_redirect_url_should_be_fine()
                {
                    Model.RedirectUrl.ShouldEqual(_redirectUrl);

                }
                [Test]
                public void then_content_text_should_be_fine()
                {
                    Model.TitleContentTextModel.ShouldEqual(_mockedContentTexts.Values[PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.Transactium_PaymentDetailsTitle]);
                }
               

            }
            

            
        }


        public abstract class StatusSpecs :
            MySpecsForControllerActionWithoutModelAndView<TransactiumController>
        {
            private LayoutViewData _layoutViewData;
            private string _hpsId;

            public override ActionResult ExecuteAction()
            {
                return SUT.StatusHandler();
            }



            protected override void Given()
            {
                //nothing
                base.Given();

                //Common Given setup

                _layoutViewData = new LayoutViewData()
                {
                    PaymentRequestDataModel = new PaymentRequestDataModel()
                    {
                        PaymentRequestId = "PaymentRequestTransactionData/ABC"
                    }
                };
                GetMockFor<IPageService>().Setup(x => x.GetContextObject<LayoutViewData>(
                    PayMammoth_v2.Constants.PayMammoth_v2Constants.PayMammoth_v2LayoutViewDataKey,
                    true)).Returns(_layoutViewData);

                _hpsId = "HPS_ID";
                GetMockFor<IPageService>().Setup(x => x.GetVariableFromRoutingQuerystringOrForm<string>(ConstantsTransactium.HPSID, false)).Returns(_hpsId);

               
            }

            protected override void When()
            {
                //Home's action PaymentSelection is called automatically through the When() method of this base class

                base.When();
            }

            public class given_request_is_success : StatusSpecs
            {
                private CheckPaymentResponse _checkPaymentResponse;

                protected override void Given()
                {
                    //nothing
                    base.Given();

                    _checkPaymentResponse = new CheckPaymentResponse()
                    {
                        Result = CheckPaymentResponse.CheckPaymentStatusType.Success,
                        SuccessRedirectUrl = "http://www.goodsite.com"
                    };
                    GetMockFor<ITransactiumService>().Setup(x => x.CheckPayment(
                        It.Is<ReferenceLink<PaymentRequestTransactionData>>(
                            refLink => refLink.GetLinkId() == _layoutViewData.PaymentRequestDataModel.PaymentRequestId),
                        _hpsId,
                        _mockedSessionObject)).Returns(_checkPaymentResponse);

                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_view_result_must_be_correct()
                {
                    var view = ((ViewResult)_actionResult);
                    ((TransactiumStatusHandlerModel)view.Model).RedirectUrl.ShouldEqual(_checkPaymentResponse.SuccessRedirectUrl);
                    view.ViewName.ShouldEqual(MVCPayMammoth_v2.Transactium.Views.StatusHandler);
                }



            }
            public class given_request_is_not_success : StatusSpecs
            {
                private CheckPaymentResponse _checkPaymentResponse;
                private string _selectionUrl;
                private string _finalUrl;

                protected override void Given()
                {
                    //nothing
                    base.Given();

                    _checkPaymentResponse = new CheckPaymentResponse()
                    {
                        Result = CheckPaymentResponse.CheckPaymentStatusType.Error,
                        SuccessRedirectUrl = "http://www.goodsite.com"
                    };
                    GetMockFor<ITransactiumService>().Setup(x => x.CheckPayment(
                        It.Is<ReferenceLink<PaymentRequestTransactionData>>(
                            refLink => refLink.GetLinkId() == _layoutViewData.PaymentRequestDataModel.PaymentRequestId),
                        _hpsId,
                        _mockedSessionObject)).Returns(_checkPaymentResponse);


                    _selectionUrl = "/selection/";
                    GetMockFor<IPaymentUrlModelService>().Setup(x => x.GetPaymentSelectionPage(
                        _mockedSessionObject, _cultureModel, _layoutViewData.PaymentRequestDataModel.PaymentRequestId)).Returns(_selectionUrl);

                    _finalUrl = "/final-url/";
                    GetMockFor<INotificationMessagesModelService>().Setup(x => x.AppendNotificationMessageToUrlFromIdentifier(
                        _mockedSessionObject, _cultureModel, _selectionUrl, _checkPaymentResponse.Result, NotificationMessageType.Error)).Returns(_finalUrl);





                }

                protected override void When()
                {
                    //Home Controller's action PaymentSelection is called automatically through the When() method of this base class

                    base.When();
                }

                [Test]
                public void then_view_result_must_be_correct()
                {
                    var view = ((ViewResult)_actionResult);
                    ((TransactiumStatusHandlerModel)view.Model).RedirectUrl.ShouldEqual(_finalUrl);
                    view.ViewName.ShouldEqual(MVCPayMammoth_v2.Transactium.Views.StatusHandler);
                }



            }


        }

    }
}
