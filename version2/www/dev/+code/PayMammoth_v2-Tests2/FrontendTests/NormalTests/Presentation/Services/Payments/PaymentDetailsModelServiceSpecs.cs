﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.ContactDetails;
using BusinessLogic_CS_v5.Presentation.Services.Contact;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Models._Shared.PartialViews.Payments;
using PayMammoth_v2.Presentation.Services.Payments;
using Should;

namespace PayMammoth_v2_Tests2.FrontendTests.NormalTests.Presentation.Services.Payments
{

    using General_Tests_CS_v5.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
    using NUnit.Framework;

    [TestFixture]
    public abstract class CreatePaymentDetailsModelSpecs : MySpecsFor<PaymentDetailsModelService>
    {
        private PaymentUserDetailsDataModel _paymentUserDetailsDataModel;

        protected override void Given()
        {
            //Common data setup between test cases for same method

            base.Given();
            _paymentUserDetailsDataModel = new PaymentUserDetailsDataModel()
            {
                ContactDetailsModel = new ContactDetailsModel()
                {
                    Address = new AddressDetailsModel()
                    {

                    }
                }
            };


            _mockedContentTexts.MockItems(
                PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PaymentDetails_Title,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PaymentDetails_YourDetails);
        }

        protected override void When()
        {
            base.When();
        }


        public class given_following_user_details : CreatePaymentDetailsModelSpecs
        {
            private _PaymentDetailsModel _result;
            private string _fullName;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup

                _fullName = "Mark Cassar";
                GetMockFor<IContactDetailsModelService>().Setup(x => x.GetFullNameFromContactDetails(
                    _paymentUserDetailsDataModel.ContactDetailsModel)).Returns(_fullName);

            }

            protected override void When()
            {
                base.When();
                _result = SUT.CreatePaymentDetailsModel(_mockedSessionObject,
                    _cultureModel, _paymentUserDetailsDataModel);
            }

            [Test]
            public void then_address_should_be_correct()
            {
                _result.AddressAsOneLineHtml.ShouldBeNull();
            }

            [Test]
            public void then_user_details_should_be_correct()
            {
                _result.UserDetails.ShouldEqual(_paymentUserDetailsDataModel);
            }
            [Test]
            public void then_full_name_should_be_correct()
            {
                _result.FullName.ShouldEqual(_fullName);
            }

            [Test]
            public void then_content_texts_should_be_correct()
            {
                _result.TitleContentText.ShouldEqual(
                   _mockedContentTexts.Values[PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PaymentDetails_Title]);

                _result.YourDetailsContentTextModel.ShouldEqual(
                    _mockedContentTexts.Values[
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.PaymentDetails_YourDetails]);


            }
        }

    }
}
