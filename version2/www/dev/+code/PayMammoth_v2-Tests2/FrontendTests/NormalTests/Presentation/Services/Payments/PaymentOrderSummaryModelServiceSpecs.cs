﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.ContentTexts;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Currency;
using CS.General_CS_v5;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Models._Shared.PartialViews.Payments;
using PayMammoth_v2.Presentation.Services.Payments;
using Should;

namespace PayMammoth_v2_Tests2.FrontendTests.NormalTests.Presentation.Services.Payments
{

    using General_Tests_CS_v5.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
    using NUnit.Framework;

    [TestFixture]
    public abstract class CreatePaymentOrderSummaryModelSpecs : MySpecsFor<PaymentOrderSummaryModelService>
    {
            private PaymentOrderSummaryDataModel _paymentOrderSummaryDataModel;

        protected override void Given()
        {
            //Common data setup between test cases for same method

            base.Given();

            _paymentOrderSummaryDataModel = new PaymentOrderSummaryDataModel()
            {
                DescriptionHtml = "<p>Test</p>",
                OrderCurrency = new CurrencyDataModel()
                {
                    Currency3LetterCode = "GBP",
                    CurrencyCode = Enums.CurrencyCode_Iso4217.UnitedKingdomPounds,
                    CurrencySymbol = "£"
                },
                Total = 100,
                TotalHandling = 200,
                TotalNet = 300,
                TotalTax = 500
            };


            _mockedContentTexts.MockItems(
                PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_Title,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalNet,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalTax,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalShipping,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalHandling,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_Total,
                PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_ViewFullOrderDetails);

                /*
                public ContentTextModel TitleContentText { get; set; }
        public ContentTextModel TotalNetContentText { get; set; }
        public ContentTextModel TotalTaxContentText { get; set; }
        public ContentTextModel TotalShippingContentText { get; set; }
        public ContentTextModel TotalHandlingContentText { get; set; }
        public ContentTextModel TotalContentText { get; set; }
        public ContentTextModel ViewFullOrderDetailsContentText { get; set; }*/
        }

        protected override void When()
        {
            base.When();
        }


        public class given_the_following_order_details_with_title : CreatePaymentOrderSummaryModelSpecs
        {
            private _PaymentOrderSummaryModel _result;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup

                _paymentOrderSummaryDataModel.Title = "Test";

            }

            protected override void When()
            {
                base.When();
                _result = SUT.CreatePaymentOrderSummaryModel(
                    _mockedSessionObject,
                    _cultureModel,
                    _paymentOrderSummaryDataModel);
            }

            [Test]
            public void then_title_content_text_should_be_null()
            {
                _result.TitleContentText.ShouldBeNull();

            }
           
        }
        public class given_the_following_order_details_without_title : CreatePaymentOrderSummaryModelSpecs
        {
            private _PaymentOrderSummaryModel _result;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup



            }

            protected override void When()
            {
                base.When();
                _result = SUT.CreatePaymentOrderSummaryModel(
                    _mockedSessionObject,
                    _cultureModel,
                    _paymentOrderSummaryDataModel);
            }

            [Test]
            public void then_totals_should_be_correct_with_currency()
            {
                _result.Total.ShouldEqual(
                    BusinessLogic_CS_v5.Util.NumberUtilBL.FormatCurrency(_paymentOrderSummaryDataModel.Total,
                        _cultureModel,
                        _paymentOrderSummaryDataModel.OrderCurrency));

                _result.TotalHandling.ShouldEqual(
                    BusinessLogic_CS_v5.Util.NumberUtilBL.FormatCurrency(
                        _paymentOrderSummaryDataModel.TotalHandling.GetValueOrDefault(),
                        _cultureModel,
                        _paymentOrderSummaryDataModel.OrderCurrency));

                _result.TotalNet.ShouldEqual(
                    BusinessLogic_CS_v5.Util.NumberUtilBL.FormatCurrency(_paymentOrderSummaryDataModel.TotalNet,
                        _cultureModel,
                        _paymentOrderSummaryDataModel.OrderCurrency));

                _result.TotalShipping.ShouldBeNull();

                _result.TotalTax.ShouldEqual(
                    BusinessLogic_CS_v5.Util.NumberUtilBL.FormatCurrency(
                        _paymentOrderSummaryDataModel.TotalTax.GetValueOrDefault(),
                        _cultureModel,
                        _paymentOrderSummaryDataModel.OrderCurrency));

            }
            [Test]
            public void then_order_summary_should_be_correct()
            {
                _result.OrderSummaryDataModel.ShouldEqual(_paymentOrderSummaryDataModel);
            }
            [Test]
            public void then_content_texts_should_be_correct()
            {
                _result.TitleContentText.ShouldEqual(
                    _mockedContentTexts.Values[PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_Title]);
                _result.TotalContentText.ShouldEqual(
                    _mockedContentTexts.Values[PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_Total]);
                _result.TotalHandlingContentText.ShouldEqual(
                    _mockedContentTexts.Values[
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalHandling]);
                _result.TotalNetContentText.ShouldEqual(
                    _mockedContentTexts.Values[PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalNet]);
                _result.TotalShippingContentText.ShouldEqual(
                    _mockedContentTexts.Values[
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalShipping]);
                _result.TotalTaxContentText.ShouldEqual(
                    _mockedContentTexts.Values[PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_TotalTax]);
                _result.ViewFullOrderDetailsContentText.ShouldEqual(
                    _mockedContentTexts.Values[
                        PayMammoth_v2.Enums.PayMammoth_v2Enums.ContentText.OrderSummary_ViewFullOrderDetails]);
            }
        }

    }
}
