﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Fields;
using BusinessLogic_CS_v5.Presentation.Services.Sections;
using CS.General_CS_v5.Modules.Urls;
using General_Tests_CS_v5.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using NUnit.Framework;
using PayMammoth_v2.Connector;
using PayMammoth_v2.KarlTestUI.Models;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Services.Payments;
using Should;
using SpecsFor.ShouldExtensions;

namespace PayMammoth_v2_Tests2.FrontendTests.NormalTests.Presentation.Services.Payments
{



    [TestFixture]
    public abstract class ConvertPaymentRequestDataModelToModelSpecs : MySpecsFor<PaymentMethodModelService>
    {
        protected override void Given()
        {
            //Common data setup between test cases for same method

            base.Given();
        }

        protected override void When()
        {
            base.When();
        }


        public class given_method_is_called : ConvertPaymentRequestDataModelToModelSpecs
        {
            private PaymentMethodModel _result;
            private PaymentMethodDataModel _paymentMethodDataModel;
            private string _url;
            private string _paymentRequestId;
            private string _fakePaymentId;

            protected override void Given()
            {
                base.Given();

                //Setup test case specific data here as the base will setup common data setup

                _paymentMethodDataModel = new PaymentMethodDataModel()
                {
                    Title = new FieldModel<string>() {Value = "Test"},
                    PaymentMethodType = Enums.PaymentMethodType.PayPal_ExpressCheckout
                };

                _url = "/payment/method/";
                GetMockFor<ISectionModelService>().Setup(x => x.GetSectionUrlWithRouteValuesFromIdentifier(
                    _mockedSessionObject,
                    _cultureModel,
                    PayMammoth_v2.Enums.PayMammoth_v2Enums.Section.Payments_Selection_Handler,
                    null,
                    null)).Returns(_url);
                _paymentRequestId = "1234";


            }

            protected override void When()
            {
                base.When();
                _fakePaymentId = "fake-id";
                _result = SUT.ConvertPaymentRequestDataModelToModel(_mockedSessionObject,
                    _cultureModel, _paymentMethodDataModel, _paymentRequestId, _fakePaymentId);
            }

            [Test]
            public void then_href_must_be_fine()
            {
                UrlHandler url = new UrlHandler(_result.Href);
                url.UriBuilder.Path.ShouldContain(_url);
                url.QueryString[PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_PaymentMethod].ShouldEqual(
                    CS.General_CS_v5.Util.ConversionUtil.ConvertBasicDataTypeToString(_paymentMethodDataModel.PaymentMethodType));
                url.QueryString[PayMammoth_v2.Constants.PayMammoth_v2Constants.QuerystringVar_FakePaymentId].ShouldEqual(
                   CS.General_CS_v5.Util.ConversionUtil.ConvertBasicDataTypeToString(_fakePaymentId));

                url.QueryString[PayMammoth_v2.Connector.Constants.PARAM_IDENTIFIER].ShouldEqual(
                    CS.General_CS_v5.Util.ConversionUtil.ConvertBasicDataTypeToString(_paymentRequestId));

                


            }
        }
      
    }
}
