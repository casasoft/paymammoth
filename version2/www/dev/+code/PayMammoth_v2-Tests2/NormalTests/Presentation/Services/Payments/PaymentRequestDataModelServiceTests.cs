﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Presentation.Models._Shared.Data.Culture;
using CS.General_CS_v5.Classes.HelperClasses;
using General_Tests_CS_v5.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using Moq;
using NUnit.Framework;
using PayMammoth_v2.Connector.InitialRequests;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2.Modules.Services.WebsiteAccounts;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Services.Models;
using PayMammoth_v2.Presentation.Services.Payments;
using Raven.Client;
using Raven.Client.Linq;
using Should;

namespace PayMammoth_v2_Tests2.NormalTests.Presentation.Services.Payments
{
    [TestFixture]
    public class PaymentRequestDataModelServiceSpecs
    {
        //public class class_under_test : MySpecsFor<PaymentRequestDataModelService>
        //{
        //    //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time

        //    [TestFixture]
        //    public class GetPaymentMethodRequestForPaymentRequestIdSpecs
        //    {
        //        [TestFixture]
        //        public class given_such_data : class_under_test
        //        {
        //            private CultureModel _cultureModel;
        //            private Mock<IDocumentSession> _mockSession;
        //            private string _paymentRequestId;


        //            protected override void Given()
        //            {
        //                _mockSession = new Mock<IDocumentSession>();
        //                _cultureModel = new CultureModel();
        //                _paymentRequestId = "";

        //                base.Given();
        //            }

        //            [TestFixture]
        //            public class given_request_does_not_exist : given_such_data
        //            {
        //                private StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel> _result;


        //                protected override void Given()
        //                {
        //                    base.Given();
        //                    //add any code after base.Given!


        //                    _paymentRequestId = "Request/5";



        //                }

        //                protected override void When()
        //                {
        //                    base.When();
        //                    _result = SUT.GetPaymentMethodRequestForPaymentRequestId(_mockSession.Object,
        //                        _cultureModel,
        //                        _paymentRequestId,
        //                        //todo: [For: Backend | 2014/06/27] PAYMFIVE-37 (WrittenBy: Mark)        			
        //                        null);
        //                }

        //                [Test]
        //                public void then_status_should_be_RequestIdNotPreset()
        //                {

        //                    _result.Status.ShouldEqual(GetPaymentMethodRequestForPaymentRequestIdStatus.ValidRequestIdNotPresent);

        //                }
        //            }

        //            [TestFixture]
        //            public class given_request_exists : given_such_data
        //            {
        //                private StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel> _result;
        //                private PaymentRequestData _paymentRequest;
        //                private PaymentRequestDataModel _paymentRequestDataModel;


        //                protected override void Given()
        //                {
        //                    base.Given();
        //                    //add any code after base.Given!


                            

        //                    _paymentRequest = new PaymentRequestData();
        //                    _paymentRequestId = _paymentRequest.Id;
        //                    setupSessionToLoadObject(_mockSession, _paymentRequest);

        //                    _paymentRequestDataModel = new PaymentRequestDataModel();

        //                    //todo: [For: Karl | 2014/04/30] fix unit-test below,uncomment (WrittenBy: Karl)        			

        //                    //GetMockFor<IFillPaymentRequestDataModelFromPaymentRequestDataService>()
        //                    //    .Setup(x => x.CreatePaymentRequestDataModelFromPaymentRequestData(_paymentRequest,
        //                    //        )).Returns(_paymentRequestDataModel);
        //                }

        //                protected override void When()
        //                {
        //                    base.When();
        //                    _result = SUT.GetPaymentMethodRequestForPaymentRequestId(_mockSession.Object,
        //                        _cultureModel,
        //                        _paymentRequestId,
        //                        //todo: [For: Backend | 2014/06/27] PAYMFIVE-37 (WrittenBy: Mark)        			
        //                        null);
        //                }

        //                [Test]
        //                public void then_status_should_be_Success()
        //                {
        //                    _result.Status.ShouldEqual(GetPaymentMethodRequestForPaymentRequestIdStatus.Success);
                            

        //                }
        //                [Test]
        //                public void then_model_should_be_filled()
        //                {
                            
        //                    _result.Result.ShouldEqual(_paymentRequestDataModel);

        //                }
        //            }


        //        }
        //    }

        //}

        [TestFixture]
        public class GetPaymentMethodRequestForPaymentRequestIdSpecs
        {
            private static PaymentRequestData _paymentRequestData;
            private static WebsiteAccountData _websiteAccountData;
            private static string _fakePaymentKey;
            private static StatusResult<GetPaymentMethodRequestForPaymentRequestIdStatus, PaymentRequestDataModel> _result; 

            public class given_dataset : MySpecsFor<PaymentRequestDataModelService>
            {
                protected override void Given()
                {
                    base.Given();
                    _paymentRequestData = new PaymentRequestData();
                    _paymentRequestData.Id = "PaymentRequestData/1";

                    _websiteAccountData = new WebsiteAccountData();
                    _websiteAccountData.Id = "WebsiteAccountData/1";

                    _paymentRequestData.LinkedWebsiteAccountId = _websiteAccountData;
                }
            }

            public class given_PaymentRequestData_exists : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                    GetMockFor<IPaymentRequestDataService>().Setup(x => x.GetById(
                        _mockedSession.Object,
                        It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestData.Id),
                        false,
                        true,
                        y => y.LinkedWebsiteAccountId)).Returns(_paymentRequestData);
                }

                public class and_WebsiteAccountData_exists : given_PaymentRequestData_exists
                {
                    protected override void Given()
                    {
                        base.Given();
                        GetMockFor<IWebsiteAccountDataService>().Setup(x => x.GetById(
                       _mockedSession.Object,
                       It.Is<ReferenceLink<WebsiteAccountData>>(y => y == _paymentRequestData.LinkedWebsiteAccountId),
                       true,
                       true)).Returns(_websiteAccountData);
                    }

                    protected override void When()
                    {
                        base.When();
                        _result = SUT.GetPaymentMethodRequestForPaymentRequestId(
                            _mockedSession.Object,
                            _cultureModel,
                            _paymentRequestData.Id,
                            _fakePaymentKey);
                    }

                    [Test]
                    public void then_CreatePaymentRequestDataModelFromPaymentRequestData_should_be_called()
                    {
                        GetMockFor<IFillPaymentRequestDataModelFromPaymentRequestDataService>().Verify(x => x.CreatePaymentRequestDataModelFromPaymentRequestData(
                            _paymentRequestData,
                            _websiteAccountData,
                            _cultureModel,
                            _mockedSession.Object,
                            _fakePaymentKey), Times.Once());
                    }

                    [Test]
                    public void then_result_should_be_success()
                    {
                        _result.Status.ShouldEqual(GetPaymentMethodRequestForPaymentRequestIdStatus.Success);
                    }
                }

                public class and_WebsiteAccountData_DOESNT_exists : given_PaymentRequestData_exists
                {
                    protected override void Given()
                    {
                        base.Given();
                        GetMockFor<IWebsiteAccountDataService>().Setup(x => x.GetById(
                       _mockedSession.Object,
                       It.Is<ReferenceLink<WebsiteAccountData>>(y => y == _paymentRequestData.LinkedWebsiteAccountId),
                       true,
                       true)).Returns((WebsiteAccountData)null);
                    }

                    protected override void When()
                    {
                        base.When();
                        _result = SUT.GetPaymentMethodRequestForPaymentRequestId(
                            _mockedSession.Object,
                            _cultureModel,
                            _paymentRequestData.Id,
                            _fakePaymentKey);
                    }

                    [Test]
                    public void then_CreatePaymentRequestDataModelFromPaymentRequestData_should_NOT_be_called()
                    {
                        GetMockFor<IFillPaymentRequestDataModelFromPaymentRequestDataService>().Verify(x => x.CreatePaymentRequestDataModelFromPaymentRequestData(
                            It.IsAny<PaymentRequestData>(),
                            It.IsAny<WebsiteAccountData>(),
                            It.IsAny<CultureModel>(),
                            It.IsAny<IDocumentSession>(),
                            It.IsAny<string>()), Times.Never());
                    }

                    [Test]
                    public void then_result_should_be_Error()
                    {
                        _result.Status.ShouldEqual(GetPaymentMethodRequestForPaymentRequestIdStatus.Error);
                    }
                }
            }

            public class given_PaymentRequestData_DOESNT_exists : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                    GetMockFor<IPaymentRequestDataService>()
                        .Setup(
                            x => x.GetById(
                                _mockedSession.Object,
                                It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestData.Id),
                                false,
                                true,
                                y => y.LinkedWebsiteAccountId))
                        .Returns((PaymentRequestData)null);
                }

                protected override void When()
                {
                    base.When();
                    _result = SUT.GetPaymentMethodRequestForPaymentRequestId(
                        _mockedSession.Object,
                        _cultureModel,
                        _paymentRequestData.Id,
                        _fakePaymentKey);
                }

                [Test]
                public void then_CreatePaymentRequestDataModelFromPaymentRequestData_should_NOT_be_called()
                {
                    GetMockFor<IFillPaymentRequestDataModelFromPaymentRequestDataService>().Verify(x => x.CreatePaymentRequestDataModelFromPaymentRequestData(
                        It.IsAny<PaymentRequestData>(),
                        It.IsAny<WebsiteAccountData>(),
                        It.IsAny<CultureModel>(),
                        It.IsAny<IDocumentSession>(),
                        It.IsAny<string>()), Times.Never());
                }

                [Test]
                public void then_result_should_be_ValidRequestIdNotPresent()
                {
                    _result.Status.ShouldEqual(GetPaymentMethodRequestForPaymentRequestIdStatus.ValidRequestIdNotPresent);
                }
            }
        }

        [TestFixture]
        public class CancelPaymentForPaymentRequestIdSpecs
        {
            private static string _paymentRequestId;
            private static PaymentRequestData _markPaymentRequestAsCancelledStatusResult;
            private static StatusResult<CancelPaymentStatus, string> _result;
            public class given_dataset : MySpecsFor<PaymentRequestDataModelService>
            {
                protected override void Given()
                {
                    base.Given();
                    _paymentRequestId = "PaymentRequestData/1";
                    _markPaymentRequestAsCancelledStatusResult = new PaymentRequestData();
                    _markPaymentRequestAsCancelledStatusResult.RequestDetails.ReturnUrls.FailureUrl = "[FailureUrl]";
                }
            }

            public class given_Success : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                GetMockFor<IPaymentRequestDataMarkerService>().Setup(x => x.MarkPaymentRequestAsCancelledAndSendNotification(
                        It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestId),
                        null)).Returns(_markPaymentRequestAsCancelledStatusResult);
                }

                protected override void When()
                {
                    base.When();
                    _result = SUT.CancelPaymentForPaymentRequestId(
                        _mockedSession.Object,
                        _cultureModel,
                        _paymentRequestId);
                }

                [Test]
                public void then_result_should_be_ok()
                {
                    _result.Result.ShouldEqual("[FailureUrl]");
                    _result.Status.ShouldEqual(CancelPaymentStatus.Success);
                }
            }

            public class given_Error : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                    GetMockFor<IPaymentRequestDataMarkerService>().Setup(x => x.MarkPaymentRequestAsCancelledAndSendNotification(
                        It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestId),
                        null)).Callback(
                            () => { throw new InvalidOperationException(); });
                }

                protected override void When()
                {
                    base.When();
                    Assert.Throws<InvalidOperationException>(() =>_result = SUT.CancelPaymentForPaymentRequestId(
                        _mockedSession.Object,
                        _cultureModel,
                        _paymentRequestId));
                }

            }
        }
    }
}
