﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using General_Tests_CS_v5.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using NUnit.Framework;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Presentation.Models._Shared.Data.Payments;
using PayMammoth_v2.Presentation.Services.Models;
using Should;

namespace PayMammoth_v2_Tests2.NormalTests.Presentation.Services.Models
{
    [TestFixture]
    public class FillPaymentRequestDataModelFromPaymentRequestDataServiceSpecs
    {
        [TestFixture]
        public class CreatePaymentRequestDataModelFromPaymentRequestDataSpecs
        {
            private static PaymentRequestData _paymentRequestData;
            private static WebsiteAccountData _websiteAccountData;
            private static string _fakePaymentKey;
            private static PaymentRequestDataModel _result;

            [TestFixture]
            public class given_dataset : MySpecsFor<FillPaymentRequestDataModelFromPaymentRequestDataService>
            {
                protected override void Given()
                {
                    base.Given();
                    _paymentRequestData = new PaymentRequestData();
                    _websiteAccountData = new WebsiteAccountData();

                    _fakePaymentKey = "[FakePaymentKey]";
                }
            }

            public class when_called : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                }

                protected override void When()
                {
                    base.When();
                    _result = SUT.CreatePaymentRequestDataModelFromPaymentRequestData(
                        _paymentRequestData,
                        _websiteAccountData,
                        _cultureModel,
                        _mockedSession.Object,
                        _fakePaymentKey);
                }

                [Test]
                public void then_FakePaymentKey_should_Be_ok()
                {
                    _result.FakePaymentKey.ShouldEqual("[FakePaymentKey]");

                }
            }
        }
    }
}
