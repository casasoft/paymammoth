﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic_CS_v5.Framework.DateAndTime;
using BusinessLogic_CS_v5.Framework.DbObjects.References;
using BusinessLogic_CS_v5.Modules.Services._Shared.Data;
using General_Tests_CS_v5.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using General_Tests_CS_v5.TestUtil;
using Moq;
using NUnit.Framework;
using PayMammoth_v2.Connector;
using PayMammoth_v2.Modules.Data.Notifications;
using PayMammoth_v2.Modules.Data.Payments;
using PayMammoth_v2.Modules.Data.WebsiteAccounts;
using PayMammoth_v2.Modules.Services.Notifications;
using PayMammoth_v2.Modules.Services.PaymentRequests;
using PayMammoth_v2.Modules.Services.WebsiteAccounts;
using Raven.Client;
using Should;

namespace PayMammoth_v2_Tests.NormalTests.Logic.Modules.Services.Notifications
{
    [TestFixture]
    public class NotificationMessageDataCreatorServiceSpecs
    {
        [TestFixture]
        public class CreateNotificationMessageDataForImmediatePaymentExpiredAndPushToAzureQueueSpecs
        {
            private static PaymentRequestData _paymentRequestData;
            private static WebsiteAccountData _websiteAccountData;
            private static string _notificationUrl;
            private static NotificationMessageData _notificationMessageData;

            public class given_dataset : MySpecsFor<NotificationMessageDataCreatorService>
            {
                protected override void Given()
                {
                    base.Given();
                    _paymentRequestData = new PaymentRequestData();
                    _paymentRequestData.Id = "PaymentRequestData/1";

                    _websiteAccountData = new WebsiteAccountData();
                    _websiteAccountData.Id = "WebsiteAccountData/1";

                    _paymentRequestData.LinkedWebsiteAccountId = _websiteAccountData;
                    _notificationMessageData = new NotificationMessageData();
                    GetMockFor<INotificationMessageDataService>().Setup(x => x.CreateNotificationMessageDataForPaymentRequestDataWithNotificationMessageType(
                        It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestData.Id),
                        Enums.NotificationMessageType.ImmediatePaymentExpired,
                        null)).Returns(_notificationMessageData);
                }
            }

            public class given_PaymentRequestData_exists : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                    GetMockFor<IPaymentRequestDataService>().Setup(x => x.GetById(
                        _mockedSession.Object,
                        It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestData.Id),
                        false,
                        true,
                        y=>y.LinkedWebsiteAccountId)).Returns(_paymentRequestData);
                }

                public class given_WebsiteAccountData_exists : given_PaymentRequestData_exists
                {
                    protected override void Given()
                    {
                        base.Given();
                        GetMockFor<IWebsiteAccountDataService>().Setup(x => x.GetById(
                      _mockedSession.Object,
                      It.Is<ReferenceLink<WebsiteAccountData>>(y => y == _websiteAccountData.Id),
                      true,
                      true)).Returns(_websiteAccountData);
                    }

                    public class and_NotificationUrl_is_ok : given_WebsiteAccountData_exists
                    {
                        protected override void Given()
                        {
                            base.Given();
                            _notificationUrl = "[NotificationUrl]";
                            GetMockFor<IPaymentRequestNotificationsUtilService>().Setup(x => x.GetNotificationUrlForPaymentRequest(
                                _mockedSession.Object,
                                _paymentRequestData)).Returns(_notificationUrl);

                        }

                        protected override void When()
                        {
                            base.When();
                            SUT.CreateNotificationMessageDataForImmediatePaymentExpiredAndPushToAzureQueue(
                                _mockedSession.Object,
                                _paymentRequestData);
                        }

                        [Test]
                        public void then_PushNotificationOnAzureQueue_should_be_called()
                        {
                            GetMockFor<INotificationsAzureService>().Verify(x => x.PushNotificationOnAzureQueue(
                                _notificationMessageData,
                                null), Times.Once());
                        }
                    }

                    public class and_NotificationUrl_is_NOT_ok : given_WebsiteAccountData_exists
                    {
                        protected override void Given()
                        {
                            base.Given();
                            _notificationUrl = "[NotificationUrl]";
                            GetMockFor<IPaymentRequestNotificationsUtilService>().Setup(x => x.GetNotificationUrlForPaymentRequest(
                                _mockedSession.Object,
                                _paymentRequestData)).Returns(String.Empty);

                        }

                        protected override void When()
                        {
                            base.When();
                            SUT.CreateNotificationMessageDataForImmediatePaymentExpiredAndPushToAzureQueue(
                                _mockedSession.Object,
                                _paymentRequestData);
                        }

                        [Test]
                        public void then_PushNotificationOnAzureQueue_should_NOT_be_called()
                        {
                            GetMockFor<INotificationsAzureService>().Verify(x => x.PushNotificationOnAzureQueue(
                                It.IsAny<NotificationMessageData>(),
                                It.IsAny<DateTimeOffset?>()), Times.Never());
                        }
                    }
                }

                public class given_WebsiteAccountData_DOESNT_exists : given_PaymentRequestData_exists
                {
                    protected override void Given()
                    {
                        base.Given();
                        GetMockFor<IWebsiteAccountDataService>()
                            .Setup(
                                x => x.GetById(
                                    _mockedSession.Object,
                                    It.Is<ReferenceLink<WebsiteAccountData>>(y => y == _websiteAccountData.Id),
                                    true,
                                    true))
                            .Returns((WebsiteAccountData)null);
                    }

                    [Test]
                    public void then_error_is_thrown()
                    {
                        Assert.Throws<InvalidOperationException>(() => SUT.CreateNotificationMessageDataForImmediatePaymentExpiredAndPushToAzureQueue(
                            _mockedSession.Object,
                            _paymentRequestData));
                    }
                }
            }

            public class given_PaymentRequestData_DOESNT_exists : given_dataset
            {
                protected override void Given()
                {
                    base.Given();
                    GetMockFor<IPaymentRequestDataService>()
                        .Setup(
                            x => x.GetById(
                                _mockedSession.Object,
                                It.Is<ReferenceLink<PaymentRequestData>>(y => y == _paymentRequestData.Id),
                                false,
                                true,
                        y => y.LinkedWebsiteAccountId))
                        .Returns((PaymentRequestData)null);
                }

                [Test]
                public void then_error_is_thrown()
                {
                    Assert.Throws<InvalidOperationException>(() => SUT.CreateNotificationMessageDataForImmediatePaymentExpiredAndPushToAzureQueue(
                        _mockedSession.Object,
                        _paymentRequestData));
                }
            }
        }

        [TestFixture]
        public class CreateNotificationMessageForSuccessfulPaymentAndSendSpecs
        {
            [TestFixture]
            public class given_such_data : MySpecsFor<NotificationMessageDataCreatorService>
            {
                protected PaymentRequestTransactionData _transaction;
                protected PaymentRequestData _paymentRequest;
                protected WebsiteAccountData _websiteAccount;


                protected override void Given()
                {

                    _transaction = new PaymentRequestTransactionData();
                    _transaction.Id = "Transaction/1";
                    _paymentRequest = new PaymentRequestData();
                    _paymentRequest.Id = "Request/1";

                    _websiteAccount = new WebsiteAccountData();
                    _websiteAccount.Id = "WebsiteAccount/1";


                    _transaction.PaymentRequestId = _paymentRequest;
                    _paymentRequest.LinkedWebsiteAccountId = _websiteAccount;


                    var mockSession = new Mock<IDocumentSession>();
                    mockSession.SetupLoad(_transaction);
                    mockSession.SetupLoad(_paymentRequest);
                    mockSession.SetupLoad(_websiteAccount);

                    base.Given();
                }


            }

            [TestFixture]
            public class given_website_account_does_not_contain_a_notification_url : given_such_data
            {


                protected override void Given()
                {
                    

                    

                    base.Given();

                    _websiteAccount.Notifications.ClientWebsiteResponseUrl = "";


                }

                protected override void When()
                {
                    SUT.CreateNotificationMessageForSuccessfulPaymentAndSend(_mockedSession.Object,_transaction);
                    base.When();
                }

                [Test]
                public void then_no_message_should_be_created()
                {
                    GetMockFor<IDataObjectFactory>().Verify(x => x.CreateNewDataObject<NotificationMessageData>(), Times.Never());


                }
            }

            [TestFixture]
            public class given_website_account_contains_a_notification_url : given_such_data
            {
                private DateTimeOffset _currentTime;
                private NotificationMessageData _notificationMessage;


                protected override void Given()
                {
                    base.Given();
                    _notificationMessage = new NotificationMessageData();
                    GetMockFor<IDataObjectFactory>().Setup(x => x.CreateNewDataObject<NotificationMessageData>()).Returns(_notificationMessage);
                    _currentTime = new DateTimeOffset(2012, 1, 2, 16, 7, 35, new TimeSpan());
                    GetMockFor<ICurrentDateTimeRetrieverService>().Setup(x => x.GetCurrentDateTime()).Returns(_currentTime);
                    _websiteAccount.Notifications.ClientWebsiteResponseUrl = "http://www.karlcassar.com/test-notify.ashx"; 

                }

                protected override void When()
                {

                    base.When();
                    SUT.CreateNotificationMessageForSuccessfulPaymentAndSend(_mockedSession.Object,_transaction);
                }

                [Test]
                public void then_notification_msg_should_be_created_and_saved()
                {

                    _mockedSession.Verify(x => x.Store(_notificationMessage), Times.Once());

                }
                [Test]
                public void then_details_should_be_correct()
                {

                    _notificationMessage.CreatedOn.ShouldEqual(_currentTime);
                    _notificationMessage.LinkedPaymentRequestId.ShouldBeSameAs(_paymentRequest.Id);
                    _notificationMessage.NotificationType.ShouldEqual(Enums.NotificationMessageType.ImmediatePaymentSuccess);
                    _notificationMessage.SendToUrl.ShouldEqual(_websiteAccount.Notifications.ClientWebsiteResponseUrl);
                    _notificationMessage.Status.ShouldEqual(Enums.NotificationMessageStatus.Pending);
                    

                }
            }

        }
    }

}
