﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using General_Tests_CS_v5.BackendTests.NormalTests.BusinessLogic.Framework.UnitTesting;
using General_Tests_CS_v5.TestUtil;
using PayMammoth_v2.Modules.Data.PaymentIntegrations.PayPalIntegration;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1;
using PayMammoth_v2.PaymentsIntegration.PayPal.v1.ExpressCheckout.InstantPaymentNotifications;
using Raven.Client;

namespace PayMammoth_v2_Tests2.NormalTests.Logic.PaymentsIntegration.PayPalIntegration.v1.ExpressCheckout.InstantPaymentNotifications
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Should;
    using SpecsFor;
    using SpecsFor.ShouldExtensions;
    using General_Tests_CS_v5.TestUtil;
    using BusinessLogic_CS_v5.Extensions;
    using CS.General_CS_v5.Extensions;


    [TestFixture]
    public class IpnService_RavenDbSpecs
    {
        public class class_under_test : SpecsForRavenDb<IpnService, PayPalProcessedIpnMsgData>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time
            [TestFixture]
            public class CheckIfIpnMessageAlreadyVerifiedInDatabaseSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {


                    protected override void Given()
                    {
                        {
                            PayPalProcessedIpnMsgData msg1 = new PayPalProcessedIpnMsgData();
                            msg1.TranscationId = "TXN-1";
                            msg1.PaymentStatus = PayPalEnums.PAYMENT_STATUS.Pending;
                            currentSession.Store(msg1);
                        }
                        {
                            PayPalProcessedIpnMsgData msg1b = new PayPalProcessedIpnMsgData();
                            msg1b.TranscationId = "TXN-1";
                            msg1b.PaymentStatus = PayPalEnums.PAYMENT_STATUS.Completed;
                            currentSession.Store(msg1b);
                            
                        }

                        {
                            PayPalProcessedIpnMsgData msg2 = new PayPalProcessedIpnMsgData();
                            msg2.TranscationId = "TXN-2";
                            
                            currentSession.Store(msg2);
                        }

                        base.Given();
                        
                    }

                    [TestFixture]
                    public class given_it_exists : given_such_data
                    {
                        private bool _result;
                        private string _id;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!
                            _id = "TXN-1";
                        }

                        protected override void When()
                        {
                            _result = SUT.CheckIfIpnMessageAlreadyVerifiedInDatabase(_id, PayPalEnums.PAYMENT_STATUS.Completed,  currentSession);
                            base.When();
                        }

                        [Test]
                        public void then_result_should_be_true()
                        {
                            _result.ShouldBeTrue();



                        }
                    }
                    [TestFixture]
                    public class given_it_does_not_exists : given_such_data
                    {
                        private bool _result;
                        private string _id;


                        protected override void Given()
                        {
                            base.Given();
                            //add any code after base.Given!
                            _id = "TXN-2";
                        }

                        protected override void When()
                        {
                            _result = SUT.CheckIfIpnMessageAlreadyVerifiedInDatabase(_id, PayPalEnums.PAYMENT_STATUS.Denied,  currentSession);
                            base.When();
                        }

                        [Test]
                        public void then_result_should_be_false()
                        {
                            _result.ShouldBeFalse();



                        }
                    }


                }
            }

         

        }


    }
    [TestFixture]
    public class IpnService_Specs
    {
        public class class_under_test : SpecsFor<IpnService>
        {
            //just a blank class, so that methods can inherit from this rather than specify the actual SUT each time

            [TestFixture]
            public class SaveIpnMessageAsProcessedInDatabaseSpecs
            {
                [TestFixture]
                public class given_such_data : class_under_test
                {


                    protected override void Given()
                    {


                        base.Given();
                    }


                }

                [TestFixture]
                public class given_it_is_called : given_such_data
                {
                    private string _ipnTransactionId;
                    private PayPalProcessedIpnMsgData _result;
                    private IpnMessage _msg;
                    private Mock<IDocumentSession> _mockSession;


                    protected override void Given()
                    {
                        base.Given();
                        //add any code after base.Given!

                        
                        _msg = new IpnMessage(null);
                        _msg.PayPalTransactionID = "test-1";
                        _mockSession = new Mock<IDocumentSession>();
                    }

                    protected override void When()
                    {
                        _result = SUT.SaveIpnMessageAsProcessedInDatabase(_msg,_mockSession.Object);
                        base.When();
                    }

                    [Test]
                    public void then_it_should_be_saved_in_db()
                    {
                        _result.Id.ShouldEqual(BusinessLogic_CS_v5.Util.RavenDbUtil.GetDocumentIdForType<PayPalProcessedIpnMsgData>(_msg.PayPalTransactionID));
                        _result.IpnMessage.ShouldEqual(_msg);
                        _mockSession.Verify(x=>x.Store(_result),Times.Once());


                    }
                }

            }



        }


    }
}
