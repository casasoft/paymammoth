﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Core.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="PayMammothDeploy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="coreHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="coreContentPlaceHolder" runat="server">
    <h1>PayMammoth.com</h1>
    <p>PayMammoth is a payment service provided by <a href="http://www.casasoft.com.mt" target="_blank">CasaSoft Ltd.</a>, an advanced web design and web development company based in Malta, Europe.</p>
</asp:Content>
