﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMammothDeploy
{
    using CS.WebComponentsGeneralV3.Code.Classes.Pages;
    using PayMammoth;
    using PayMammoth.Frontend.PaymentRequestModule;
    using PayMammoth.Modules.WebsiteAccountModule;

    public partial class Core : BaseCoreMasterPage
    {
        private bool _currentPaymentLoaded = false;
        private PaymentRequestFrontend _currentPayment = null;

        public PaymentRequestFrontend CurrentPaymentRequest
        {
            get
            {
                if (!_currentPaymentLoaded)
                {
                    _currentPaymentLoaded = true;
                    var data = QuerystringData.GetPaymentRequestFromQuerystring();
                    _currentPayment = PaymentRequestFrontend.Get(data);
                }
                return _currentPayment;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            var request = this.CurrentPaymentRequest;
            if (request != null)
            {
                var websiteAccount = request.Data.WebsiteAccount;
                if (websiteAccount != null)
                {
                    initCustomCss(websiteAccount);

                }
            }
            base.OnLoad(e);
        }

        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get { return Master.HtmlTag; }
        }
        private void initCustomCss(IWebsiteAccount websiteAccount)
        {
            var css = websiteAccount.Css;

            if (!css.IsEmpty())
            {
                customCss.Attributes["href"] = css.GetThumbnailImageUrl();
            }
            else
            {
                customCss.Parent.Controls.Remove(customCss);
            }
        }

    }
}