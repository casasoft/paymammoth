﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderSummary.ascx.cs"
    Inherits="PayMammothDeploy.usercontrols.masterpage.v1.OrderSummary" %>
<%@ Register Src="~/usercontrols/masterpage/v1/OrderSummaryDetails.ascx" TagName="OrderSummaryDetails"
    TagPrefix="PayMammoth" %>
<%@ Register Src="ItemDetails.ascx" TagName="ItemDetails" TagPrefix="PayMammoth" %>
<!--MasterPage/v1/OrderSummary-->
<div class="order-summary-wrapper">
    <div class="order-summary-title-container clearfix">
        <h2 runat="server" id="h2OrderSummary" class="order-summary-title rounded-border">
            Your order summary</h2>
    </div>
    <div class="order-description-content">
        <div class="order-description-title-container clearfix">
            <h3 runat="server" id="h3OrderDescription" class="order-description-title rounded-border">
                Description</h3>
        </div>
        <div class="order-main-content">
            <p class="order-description-text">
                <asp:Literal runat="server" ID="orderDescription">
Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,
             when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
             It has survived not only five centuries, but also the leap into electronic typesetting, 
             remaining essentially unchanged. It was popularised in the 1960s with the release
             of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing 
             software like Aldus PageMaker including versions of Lorem Ipsum.>
                </asp:Literal>
            </p>
            <PayMammoth:ItemDetails ID="ItemDetails" runat="server" />
            <div class="view-full-order-details-container">
                <a href="#" id="aViewOrderDetails" target="_blank" runat="server" class="view-order-details">
                    View full order details</a></div>
        </div>
    </div>
    <div class="order-summary-details-wrapper">
        <PayMammoth:OrderSummaryDetails runat="server" ID="OrderSummaryDetails" />
    </div>
</div>
