﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemDetail.ascx.cs"
    Inherits="PayMammothDeploy.usercontrols.masterpage.v1.ItemDetail" %>
<!--MasterPage/v1/ItemDetail-->
<div class="item-detail">
    <div id="spanTitle" class="item-detail-title" runat="server"></div>
    <p id="pDescription" runat="server" class="item-detail-description">
    </p>
</div>
