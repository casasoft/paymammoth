﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.Modules;
using PayMammoth.Controls.UserControls;
using PayMammoth.Connector.Classes.Requests;
namespace PayMammothDeploy.usercontrols.masterpage.v1
{
    public partial class ItemDetails : BaseUserControl
    {

        #region ItemDetails Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected ItemDetails _item = null;
            internal FUNCTIONALITY(ItemDetails item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "Item is required");
                this._item = item;
            }
            public IEnumerable<IItemDetail> ItemDetails { get; set; }
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }
        private void loadInfo()
        {
            var details = this.Functionality.ItemDetails;
            var itemList = details.ToList();
            if (itemList.Count > 0)
            {
                foreach (var item in itemList)
                {
                    var ucItemDetail = ItemDetail.LoadControl(this.Page);
                    ucItemDetail.Functionality.ItemDetail = item;
                    divItemDetails.Controls.Add(ucItemDetail);
                }
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divItemDetails);
            }
        }

        private void init()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(Functionality.ItemDetails, "Item Details is required");

            loadInfo();


        }

        
    }
}