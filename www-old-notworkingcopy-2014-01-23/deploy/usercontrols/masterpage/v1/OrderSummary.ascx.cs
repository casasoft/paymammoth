﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Extensions;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.Modules;
using PayMammoth.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
namespace PayMammothDeploy.usercontrols.masterpage.v1
{
    public partial class OrderSummary : BaseUserControl
    {
        #region OrderSummary Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected OrderSummary _item = null;
            internal FUNCTIONALITY(OrderSummary item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "Item is required");
                this._item = item;
            }
            public IPaymentRequestDetails PaymentRequest { get; set; }
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }
        private void loadItemDetails()
        {
            var parsedItemDetails = this.Functionality.PaymentRequest.GetItemDetails();
            if (!PayMammoth.Connector.Extensions.ItemDetailExtensions.CheckIfItemDetailsAreEmpty(parsedItemDetails))
            {
                this.ItemDetails.Functionality.ItemDetails = parsedItemDetails;
            }
            else
            {
                this.ItemDetails.RemoveControlFromParentContainer();
            }


        }

        private void init()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(Functionality.PaymentRequest, "Payment Request is required");
            if(Functionality.PaymentRequest != null)
            {
                var request = Functionality.PaymentRequest;

                MyContentText.AttachContentTextWithControl(h2OrderSummary, ((ContentText)Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.OrderSummary_SectionTitle)).ToFrontend());

                h3OrderDescription.InnerHtml = Functionality.PaymentRequest.Title;
                if (!string.IsNullOrWhiteSpace(request.OrderLinkOnClientWebsite))
                {
                    aViewOrderDetails.InnerText =
                        Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                            PayMammoth.Enums.CONTENT_TEXT.OrderSummary_ViewFullOrderDetailsLinkText).GetContent();
                    aViewOrderDetails.HRef = request.OrderLinkOnClientWebsite;
                }
                else
                {
                    CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(aViewOrderDetails);
                }

                orderDescription.Text = request.Description;
                OrderSummaryDetails.Functionality.PaymentRequest = Functionality.PaymentRequest;
                loadItemDetails();

            }
        }
    }
}