﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Controls.UserControls;
using PayMammoth.Util;

namespace PayMammothDeploy.usercontrols.masterpage.v1
{
    public partial class ContactDetails : BaseUserControl
    {
        private void updateContactDetails()
        {
            var paymentRequest = FrontendUtil.GetCurrentPaymentRequest();
            if (paymentRequest != null)
            {
                ContactUsDetails.Functionality.LoadDetailsFromSettings = false;
                ContactUsDetails.Functionality.Email = paymentRequest.Data.WebsiteAccount.ContactEmail;
                ContactUsDetails.Functionality.DoNotShowHeading = true;
                string addressLine1 = CS.General_v3.Util.Text.JoinList(", ", false, paymentRequest.Data.WebsiteAccount.ChequeAddress1, paymentRequest.Data.WebsiteAccount.ChequeAddress2);
                string addressLine2 = CS.General_v3.Util.Text.JoinList(", ", false, paymentRequest.Data.WebsiteAccount.ChequeAddressLocality, paymentRequest.Data.WebsiteAccount.ChequeAddressPostCode, paymentRequest.Data.WebsiteAccount.ChequeAddressCountry);

                string address = CS.General_v3.Util.Text.JoinList("<br />", false,
                                                                  addressLine1,
                                                                  addressLine2);

                ContactUsDetails.Functionality.Address = address;
            }
            else
            {
                this.Visible = false;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            updateContactDetails();
            ContactUsDetails.Functionality.IsAddressInTable = false;
            ContactUsDetails.Functionality.ContactDetailsTitle = null;
            base.OnLoad(e);
        }
    }
}