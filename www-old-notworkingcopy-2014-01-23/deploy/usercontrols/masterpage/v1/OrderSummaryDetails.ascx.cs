﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Extensions;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.Modules;
using PayMammoth.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
namespace PayMammothDeploy.usercontrols.masterpage.v1
{
    public partial class OrderSummaryDetails : BaseUserControl
    {
        #region OrderSummaryDetails Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected OrderSummaryDetails _item = null;
            internal FUNCTIONALITY(OrderSummaryDetails item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "Item is required");
                this._item = item;
            }
            public IPaymentRequestDetails PaymentRequest { get; set; }
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(Functionality.PaymentRequest, "Payment Request is required");
            
            if(Functionality.PaymentRequest != null)
            {
                initUpdateTexts();

                var currency = CS.General_v3.Enums.ISO_ENUMS.GetCurrencySymbol(Functionality.PaymentRequest.CurrencyCode, true);
                {
                    var netTotal = Functionality.PaymentRequest.PriceExcTax;
                    netTotal += Functionality.PaymentRequest.TaxAmount;

                    if (netTotal > 0)
                    {
                        spanOrderNetTotal.InnerHtml = CS.General_v3.Util.Text.GetPriceForHTML(netTotal, currency);
                    }
                    else
                    {
                        trOrderNetTotal.Visible = false;
                    }
                }

                {

                    //Karl - May 28/2012
                    //tax is removed for now, and incorporated in the price exc. tax. The reason being that if there are 
                    //a fixed discount of 30, and say the price was 50 excluding discount, it looks weird having a net total of
                    //20, and tax of 9.  Discount is after tax, not before.  

                    var tax = Functionality.PaymentRequest.TaxAmount;
                    tax = 0;
                    if (tax > 0)
                    {
                        spanTax.InnerHtml = CS.General_v3.Util.Text.GetPriceForHTML(tax, currency);
                    }
                    else
                    {
                        trTax.Visible = false;
                    }
                }
                var shipping = Functionality.PaymentRequest.ShippingAmount;
                if (shipping > 0)
                {

                    spanShipping.InnerHtml = CS.General_v3.Util.Text.GetPriceForHTML(shipping, currency);
                }
                else
                {
                    trShipping.Visible = false;
                }

                var handling = Functionality.PaymentRequest.HandlingAmount;
                if (handling > 0)
                {
                    spanHandling.InnerHtml = CS.General_v3.Util.Text.GetPriceForHTML(handling, currency);
                }
                else
                {
                    trHandling.Visible = false;
                }

                var totalAmountInCents = Functionality.PaymentRequest.GetTotalPriceInCents();
                double totalAmount = (double)totalAmountInCents / 100d;
                spanTotal.InnerHtml = CS.General_v3.Util.Text.GetPriceForHTML(totalAmount, currency);
            }
        }

        private void initUpdateTexts()
        {
            MyContentText.AttachContentTextWithControl(lblOrderNetTotal, ((ContentText)Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.OrderSummaryDetails_OrderNetTotalLabel)).ToFrontend());
            MyContentText.AttachContentTextWithControl(lblHandling, ((ContentText)Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.OrderSummaryDetails_HandlingLabel)).ToFrontend());
            MyContentText.AttachContentTextWithControl(lblShipping, ((ContentText)Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.OrderSummaryDetails_ShippingLabel)).ToFrontend());
            MyContentText.AttachContentTextWithControl(lblTotal, ((ContentText)Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.OrderSummaryDetails_TotalLabel)).ToFrontend());
            MyContentText.AttachContentTextWithControl(lblTax, ((ContentText)Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.OrderSummaryDetails_TaxLabel)).ToFrontend());
        }
    }
}