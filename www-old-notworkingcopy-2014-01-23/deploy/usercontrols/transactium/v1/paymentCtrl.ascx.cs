﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Controls.UserControls;

namespace PayMammothDeploy.usercontrols.transactium.v1
{
    public partial class paymentCtrl : BaseUserControl
    {
            

        #region paymentCtrl Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected paymentCtrl _item = null;
            internal FUNCTIONALITY(paymentCtrl item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "Item is required");
                this._item = item;
            }
            public string ResponseUrl { get; set; }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			

        public string JavascriptSrc
        {
            get { return CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Transactium_v1_JavascriptScriptUrl); }
        } 
        public string ResponseUrl
        {
            get { return this.Functionality.ResponseUrl; }
        }

     

        
        protected void Page_Load(object sender, EventArgs e)
        {
            
           
        }
    }
}