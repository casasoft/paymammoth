﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Controls.UserControls;
using PayMammoth.Modules.PaymentMethodModule;

namespace PayMammothDeploy.usercontrols.payment
{
    public partial class PaymentMethod : BaseUserControl
    {
        #region PaymentMethod Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected PaymentMethod _item = null;
            internal FUNCTIONALITY(PaymentMethod item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "Item is required");
                this._item = item;
            }
            public IPaymentMethodInfo PaymentMethod { get; set; }
            public bool IsChecked()
            {
                return _item.isChecked();
            }
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initUpdateText();
        }

        private bool isChecked()
        {
            if(rdBtnPayment.GetFormValueObjectAsBool())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void initUpdateText()
        {
            if (Functionality.PaymentMethod != null)
            {
                //var paymentMethodType = Functionality.PaymentMethod.PaymentMethod.ToString();
                var paymentMethodType = Functionality.PaymentMethod.PaymentMethod.ToString();
                rdBtnPayment.ID = "rdBtnPayment_" + paymentMethodType;
                rdBtnPayment.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                spanPaymentTitle.InnerHtml = Functionality.PaymentMethod.Title;
                spanPaymentDescription.InnerHtml = Functionality.PaymentMethod.Description;

                spanPaymentMethodIcon.Attributes["class"] = "payment-method-" + paymentMethodType.ToLower();
            }
        }


        public static PaymentMethod LoadControl(Page pg, string id)
        {
            var ctrl = pg.LoadControl("/usercontrols/payment/PaymentMethod.ascx") as PaymentMethod;
            if (!string.IsNullOrEmpty(id))
            {
                ctrl.ID = id;
            }
            return ctrl;
        }
    }
}