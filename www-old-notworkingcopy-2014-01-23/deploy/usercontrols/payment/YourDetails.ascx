﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="YourDetails.ascx.cs" Inherits="PayMammothDeploy.usercontrols.payment.YourDetails" %>

<div class="your-details-wrapper">
    <div class="your-details-container clearfix">
        <h3 class="h3-your-details rounded-border" runat="server" id="h3YourDetails">Your Details</h3>    
    </div>
    <p>
    <span class="details-client-name" runat="server" id="spanName"></span>
    <span class="details-client-email" runat="server" id="spanEmail"></span>
    <span class="details-client-address">
        <asp:Literal runat="server" ID="detailsAddress">John Borg,                20, Albatross,                Redundance Street,                Sliema,                SLM 1020,                Malta
        </asp:Literal>
    </span>
</div>
