﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Controls.UserControls;
using PayMammoth.Frontend.PaymentRequestModule;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.Modules;
namespace PayMammothDeploy.usercontrols.payment
{
    public partial class StagingNote : BaseUserControl
    {
        #region StagingNote Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected StagingNote _item = null;
            public bool? IsStaging { get; set; }
            internal FUNCTIONALITY(StagingNote item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "Item is required");
                this._item = item;
            }
            public PaymentRequestFrontend PaymentRequest { get; set; }
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            if (!this.Functionality.IsStaging.HasValue && CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                throw new InvalidOperationException("Please fill in the 'IsStaging' parameter");
            }

            init();
            base.OnLoad(e);
        }

        private void init()
        {
            var websiteAccount = Functionality.PaymentRequest.Data.WebsiteAccount;
            if(websiteAccount != null)
            {
                divStagingNote.Visible = this.Functionality.IsStaging.GetValueOrDefault();
                var cpStagingNoteContent = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.General_StagingError);
                string stagingNoteContent = BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(cpStagingNoteContent, "[COMPANY_NAME]", websiteAccount.WebsiteName);
                divStagingNote.InnerHtml = stagingNoteContent; 
            }
        }
    }
}