﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Pages.Frontend;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.PaymentMethods.Cheque;
using BusinessLogic_v3.Modules;

namespace PayMammothDeploy.payment.cheque
{
    public partial class _default : BasePaymentPage
    {
        //private string _returnUrlSuccess;

        public _default()
        {
            string title = 
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.Payment_Method_Cheque_PageTitle).GetContent();

            this.PageTitle = title;
            this.Functionality.UpdateTitle(title,false);
        }

        private void loadInfo()
        {
            btnProceed.Text =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.General_Button_ConfirmAndProceed).GetContent();

            var r = this.CurrentPaymentRequest;
            if (!r.Data.WebsiteAccount.IsChequeEnabled())
            {
                throw new InvalidOperationException("Cheque Payments must be enabled in order to access this page");
            }

            var w = r.Data.WebsiteAccount;
            spanTransactionId.InnerText = r.Data.Identifier;
            var cpPayableToText =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.Payment_Method_Cheque_PayableToText);
            var tr = cpPayableToText.CreateTokenReplacer();
            string strPayableToText = BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(cpPayableToText, "[payable_to]", w.ChequePayableTo);
                
            
            
            pAddress.InnerHtml = CS.General_v3.Util.Text.AppendStrings("<br />", r.Data.WebsiteAccount.ChequeAddress1,
                r.Data.WebsiteAccount.ChequeAddress2,
                r.Data.WebsiteAccount.ChequeAddressLocality,
                r.Data.WebsiteAccount.ChequeAddressPostCode,
                r.Data.WebsiteAccount.ChequeAddressCountry);

            string chequePaymentInstructionsText = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.Payment_Method_Cheque_Instructions).GetContent();
            if(!String.IsNullOrEmpty(chequePaymentInstructionsText))
            {
                chequePaymentInstructions.Text = chequePaymentInstructionsText;
            }
            //_returnUrlSuccess = ((IPaymentRequestDetails)r.Data).ReturnUrlSuccess;
            aGoBack.Href = PayMammoth.Urls.GetPaymentChoiceUrl(this.CurrentPaymentRequest.Data.Identifier);
            aGoBack.InnerHtml = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_CreditCard_GoBackText).GetContent();
            btnProceed.Click += new EventHandler(btnProceed_Click);
        }

        void btnProceed_Click(object sender, EventArgs e)
        {
            ChequeManager.Instance.ConfirmByCheque(this.CurrentPaymentRequest.Data);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            loadInfo();

        }

        public new BasePagesMasterPage Master { get { return (BasePagesMasterPage)base.Master; } }
    }
}