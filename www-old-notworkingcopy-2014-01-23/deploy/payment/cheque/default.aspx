﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages.master" AutoEventWireup="true"
    CodeBehind="default.aspx.cs" Inherits="PayMammothDeploy.payment.cheque._default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div class="pay-by-cheque-wrapper">
        <p>
            <asp:Literal runat="server" ID="payableToText"></asp:Literal>
        </p>
        <p id="pAddress" runat="server" class="cheque-pay-to-address">
        </p>
        <div runat="server" visible="false">
            <span>Transaction ID:</span> <span id="spanTransactionId" runat="server"></span>
        </div>
        <asp:Literal runat="server" ID="chequePaymentInstructions">
        
        </asp:Literal>
        <div class="button-wrapper">
            <CSControls:MyButton runat="server" ID="btnProceed" Text="Confirm & Proceed" />
            <CSControls:MyAnchor ID="aGoBack" Text="Go Back" runat="server" />
        </div>
    </div>
</asp:Content>
