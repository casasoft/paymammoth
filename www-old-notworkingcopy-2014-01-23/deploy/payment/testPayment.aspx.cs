﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Other;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Frontend.PaymentRequestModule;
using PayMammoth.Classes.Pages.Frontend;

namespace PayMammothDeploy.payment
{
    public partial class testPayment : BasePageUsingPagesMasterPage
    {

        public int? AmountFromQs
        {
            get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<int?>("amt"); }
        }

        private void testAdd()
        {
            WebsiteAccount account = WebsiteAccount.Factory.CreateNewItem();
            account.WebsiteName = "Test";
            account.Code = "TEST";
            account.PaypalEnabled = true;
            account.PaypalSandboxSignature = account.PaypalLiveSignature = "AXf0mvFdjMzfJ5O7GSxqthe5lBXuAN.LnIRkEjJTk2fyUE-0xPa4.bCY";
            account.PaypalLiveUsername= account.PaypalSandboxUsername = "info_1290107500_biz_api1.karlcassar.com";
            
            account.PaypalLiveMerchantId = account.PaypalSandboxMerchantId = "G8EC4S8YZTGSY";
            account.PaypalSandboxMerchantEmail = "info_1290107500_biz@karlcassar.com";
            account.PaypalLivePassword= account.PaypalSandboxPassword = "1290107511";
            account.PaypalUseLiveEnvironment = false;
            account.IterationsToDeriveHash = 256;
            account.ResponseUrl = "http://www.casasoft.com.mt/response";
            account.SecretWord = "123456";
            account.CreateAndCommit();


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //testAdd();

            var account = WebsiteAccountFactory.Instance.GetAccountByCode("TEST");
            double amount = (double)this.AmountFromQs.GetValueOrDefault(500) / 100d;
            InitialRequestInfo reqInfo = new InitialRequestInfo();
            reqInfo.ClientAddress1 = "20, Cor Jesu";
            reqInfo.ClientAddress2 = "Triq Il-Kittenija";
            reqInfo.ClientCountry =CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Malta;
            reqInfo.ClientEmail = "KarlCassar@gmail.com";
            reqInfo.ClientFirstName = "Karl";
            reqInfo.ClientLastName = "Cassar";
            reqInfo.ClientLocality = "Zurrieq";
            reqInfo.ClientPostCode = "ZRQ4020";
            reqInfo.CurrencyCode = CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro;
            reqInfo.Description = "This is the <strong>order</strong> done for testing";
            reqInfo.Title = "Order #2144-test";
            reqInfo.PriceExcTax = amount;
            reqInfo.ReturnUrlSuccess = "http://localhost/success";
            reqInfo.ReturnUrlFailure = "http://localhost/failure";
            reqInfo.HandlingAmount = 0;
            reqInfo.OrderLinkOnClientWebsite = "http://www.casasoft.com.mt/testorder";
            reqInfo.Reference = "2144";
            reqInfo.ShippingAmount = 0;
            reqInfo.TaxAmount = 0;
            reqInfo.WebsiteAccountCode = "TEST";

            reqInfo.HashValue = PayMammoth.Connector.Util.HashUtil.GenerateInitialRequestHash(account.Code,reqInfo.Title, 
            PayMammoth.Connector.Extensions.PaymentRequestDetailsExtensions.GetTotalPriceInCents(reqInfo),account.SecretWord);;
            var paymentRequestResponse = reqInfo.GeneratePaymentRequest();

            PaymentRequestFrontend requestFrontend = PaymentRequestFrontend.Get(paymentRequestResponse.Request);
            requestFrontend.RedirectToChoicePage();

                



        }
    }
}