﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayMammoth.Classes.Other;
using PayMammoth.Classes.PaymentNotifications;
using PayMammoth.Connector.Classes.Responses;
using CS.General_v3.URL;
using PayMammoth.Connector.Classes.Notifications;

namespace PayMammothDeploy.payment
{
    /// <summary>
    /// Summary description for confirm
    /// </summary>
    public class confirmNotification : CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers.BaseHandler
    {

        

        protected override void processRequest(HttpContext ctx)
        {
            //27Nov12 - This is the handler in use right now
            NotificationMsg msg = new NotificationMsg();
            msg.DeserializerFromNameValueCollection(ctx.Request.Form);

            if (msg.NotificationType.HasValue && !string.IsNullOrWhiteSpace(msg.WebsiteAccountCode))
            {
                var confirmerService = BusinessLogic_v3.Util.InversionOfControlUtil.Get<INotificationConfirmerService>();
                bool confirmed = confirmerService.ConfirmMessage(msg);
                if (confirmed)
                {
                    var confirmer = PayMammoth.Classes.PaymentNotifications.NotificationConfirmations.NotificationTypeConfirmationResponseSenderManager.GetConfirmerForNotificationType(msg.NotificationType.Value);
                    var response = confirmer.GenerateResponse(msg);

                    QueryString qs = new QueryString();
                    response.SerializeToNameValueCollection(qs);
                    ctx.Response.Write(qs.ToString());
                }


            }
            ctx.Response.End();
           
        }
    }
}