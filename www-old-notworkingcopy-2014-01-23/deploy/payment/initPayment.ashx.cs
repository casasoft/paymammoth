﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayMammoth.Classes.Other;
using PayMammoth;
using log4net;
using CS.General_v3.URL;
using PayMammoth.Util;
using System.Collections.Specialized;

namespace PayMammothDeploy.payment
{
    /// <summary>
    /// Summary description for initPayment
    /// </summary>
    public class initPayment : CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers.BaseHandler
    {

        protected static readonly ILog _log = LogManager.GetLogger(typeof(initPayment));


        protected override void processRequest(HttpContext ctx)
        {
            InitialRequestInfo initReq = new InitialRequestInfo();

            
            string data =null;
            List<NameValueCollection> listNvs = new List<NameValueCollection>();
            listNvs.Add(ctx.Request.Form);
            listNvs.Add(ctx.Request.QueryString);
            NameValueCollection currNv = null;
            for (int i=0; i < listNvs.Count;i++)
            {
                var nv = listNvs[i];
                
                bool ok = initReq.LoadFromNameValueCollection(nv);
                if (ok)
                {
                    currNv = nv;
                    data= nv.ToString();
                    break;
                }

            }

            _log.Debug( "New Payment Request - Data: " + data) ;
            var reqResponse = initReq.GeneratePaymentRequest();
            QueryString qs = new QueryString();

            if (reqResponse.Status == Enums.GenerateResponseStatus.Ok)
            { //if request is ok, return the GUID of the transaction, else don't worry
                _log.Info(LogUtil.AddRequestIdentifierToLogMsg(reqResponse.Request, "New Payment Request - Generated Request OK"));
                var req = reqResponse.Request;
                qs[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = reqResponse.Request.Identifier;
                qs[PayMammoth.Connector.Constants.PARAM_HASH] = 
                    PayMammoth.Connector.Util.HashUtil.GenerateInitialResponseHash(req.Identifier, req.WebsiteAccount.SecretWord);
                
            }
            else
            {
                _log.Info("New Payment Request - Generated Request FAIL - " + reqResponse.Status);
            
                qs[PayMammoth.Connector.Constants.PARAM_ERRORCODE] = CS.General_v3.Util.EnumUtils.StringValueOf(reqResponse.Status);
            }
            ctx.Response.Write(qs.ToString());

        }
    }
}