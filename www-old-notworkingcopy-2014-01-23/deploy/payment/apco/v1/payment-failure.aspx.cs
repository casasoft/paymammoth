﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Pages.Frontend;
using PayMammoth.PaymentMethods.PaymentGateways.Apco.v1;

namespace PayMammothDeploy.payment.apco.v1
{
    public partial class payment_failure : BasePaymentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var req = this.CurrentPaymentRequest;
            ApcoManager.Instance.RedirectToFailurePageInParent(this, req.Data);
        }
    }
}