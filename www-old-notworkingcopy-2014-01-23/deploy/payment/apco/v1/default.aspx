﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages.master" AutoEventWireup="true"
    CodeBehind="default.aspx.cs" Inherits="PayMammothDeploy.payment.apco.v1._default" %>

<%@ Register Src="~/usercontrols/payment/SecurityInformation.ascx" TagName="SecurityInformation"
    TagPrefix="PayMammoth" %>
<%@ Register Src="~/usercontrols/payment/StagingNote.ascx" TagName="StagingNote"
    TagPrefix="PayMammoth" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <PayMammoth:StagingNote runat="server" ID="StagingNote" />
    <div class="payment payment-apco">
        <iframe id="iframePayment" runat="server"></iframe>
    </div>
    <PayMammoth:SecurityInformation runat="server" ID="SecurityInformation" />
    <div class="button-wrapper">
        <a href="" id="aGoBack" runat="server">Go back to payment choice menu</a>
    </div>
</asp:Content>