﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages.master" AutoEventWireup="true"
    CodeBehind="confirm.aspx.cs" Inherits="PayMammothDeploy.payment.paypal.v1.confirm" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="mainContent" runat="server">
    <h3 class="confirm-ordertitle rounded-border" runat="server" id="h3ConfirmOrder">
        Confirm Order</h3>
  
 
    <div class="button-wrapper">
    <p>
            <span runat="server" id="spanConfirmOrderText"></span>
    </p>

        <CSControls:MyButton ID="btnConfirm" runat="server" Text="Confirm Payment" />
       
    </div>

</asp:Content>
