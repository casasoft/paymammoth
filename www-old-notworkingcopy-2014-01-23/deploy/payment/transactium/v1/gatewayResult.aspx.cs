﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Pages.Frontend;
using PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1.Wrappers;
using PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1;

namespace PayMammothDeploy.payment.transactium.v1
{
    public partial class gatewayResult : BasePaymentPage
    {
       

        private void checkPayment()
        {

            TransactiumManager.Instance.CheckPayment(this.CurrentPaymentRequest.Data);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            checkPayment();
        }
    }
}