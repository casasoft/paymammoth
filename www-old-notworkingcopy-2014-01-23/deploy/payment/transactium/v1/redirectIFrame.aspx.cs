﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Pages.Frontend;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth;
using log4net;

namespace PayMammothDeploy.payment.transactium.v1
{
    public partial class redirectIFrame : BasePaymentPage
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(redirectIFrame));
        public bool Success
        {
            get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<bool>(PayMammoth.Constants.PARAM_SUCCESS); }
        }

        private void checkRedirection()
        {
            _log.Debug("checkRedirection [START]");
            var req = this.CurrentPaymentRequest;
            if (req != null)
            {
                string url = null;
                var response = PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1.TransactiumManager.Instance.CheckPayment(this.CurrentPaymentRequest.Data);
                if (_log.IsDebugEnabled) _log.Debug("Response - Status: " + response.Status + " | Result: " + response.Result.GetMessageAsString());
                switch (response.Status)
                {
                    case PayMammoth.Enums.PaymentStatus.Success:
                        {

                            url = req.GetSuccessUrl();
                            break;
                        }
                    case PayMammoth.Enums.PaymentStatus.Cancelled:
                        {
                            url = PayMammoth.Urls.GetPaymentChoiceUrl(this.CurrentPaymentRequest.Data.Identifier);
                            
                            break;
                        }
                    default:
                    
                        {


                            this.SetStatusMsg(Enums.GetMessageFromPaymentStatus(response.Status), Enums.GetStatusTypeFromPaymentStatus(response.Status));
                            url = PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1.TransactiumUrls.GetPaymentPageUrl(req.Data.Identifier);
                            break;
                        }

                }

               // CS.General_v3.Util.PageUtil.RedirectParent(this, url);
                aRedirectLink.HRef = url;
                CS.General_v3.Util.PageUtil.RedirectPage(url);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            checkRedirection();
            

        }
    }
}