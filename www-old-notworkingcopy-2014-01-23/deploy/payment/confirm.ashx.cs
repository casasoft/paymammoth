﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayMammoth.Classes.Other;
using PayMammoth.Connector.Classes.Responses;
using CS.General_v3.URL;

namespace PayMammothDeploy.payment
{
    /// <summary>
    /// Summary description for confirm
    /// </summary>
    public class confirm : CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers.BaseHandler
    {

        

        protected override void processRequest(HttpContext ctx)
        {

            //27/nov/2012 - this handler is not used anymore

            ImmediatePaymentConfirmationResponse resp = new ImmediatePaymentConfirmationResponse();
            PayMammoth.Classes.Other.ConfirmRequestDetails request = new PayMammoth.Classes.Other.ConfirmRequestDetails();
            request.LoadFromQuerystring();
            var paymentRequest = request.VerifyAndGetRequest();
            
            if (paymentRequest != null)
            {
                
                var successfulTransaction = paymentRequest.GetSuccessfulTransaction();
                if (successfulTransaction != null && successfulTransaction.Successful)
                {
                    resp.ConfirmationSuccess = true;
                    successfulTransaction.FillConfirmResponse(resp);
                    
                }
            }
            QueryString qs = new QueryString();
            resp.SerializeToNameValueCollection(qs);
            ctx.Response.Write(qs.ToString());
            
            ctx.Response.End();
        }
    }
}