﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages.master" AutoEventWireup="true"
    CodeBehind="default.aspx.cs" Inherits="PayMammothDeploy.payment.gateways.realex._default" %>

<%@ Register Src="~/usercontrols/payment/StagingNote.ascx" TagName="StagingNote"
    TagPrefix="PayMammoth" %>
<asp:Content ID="content2" ContentPlaceHolderID="mainContent" runat="server">
    <script type="text/javascript">

        function submitForm() {
            jQuery('.btnSubmit').focus();
            jQuery('.btnSubmit').click();
            //jQuery("form").submit();
        }

        jQuery(
function () {
    submitForm();
}
)
    </script>
    <PayMammoth:StagingNote runat="server" ID="StagingNote" />
<%--    <span runat="server" id="spanTotal">Total:</span> <span id="spanTotalAmount" runat="server"></span>--%>
    <div class="button-wrapper">
        <div class="button-wrapper">
            <CSControls:MyButton ID="btnSubmit" CssClass="btnSubmit" Text="Proceed to payment"
                runat="server" ValidationGroup="none" />
        </div>
        <p>
            <asp:Literal runat="server" ID="redirectText"></asp:Literal>
        </p>
        <hr />
        <CSControls:MyAnchor ID="aGoBack" Text="Go Back" runat="server" />
    </div>
</asp:Content>
