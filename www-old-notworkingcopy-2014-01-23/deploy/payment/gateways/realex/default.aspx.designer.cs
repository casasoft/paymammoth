﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayMammothDeploy.payment.gateways.realex {
    
    
    public partial class _default {
        
        /// <summary>
        /// StagingNote control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PayMammothDeploy.usercontrols.payment.StagingNote StagingNote;
        
        /// <summary>
        /// btnSubmit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.MyButton btnSubmit;
        
        /// <summary>
        /// redirectText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal redirectText;
        
        /// <summary>
        /// aGoBack control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.MyAnchor aGoBack;
    }
}
