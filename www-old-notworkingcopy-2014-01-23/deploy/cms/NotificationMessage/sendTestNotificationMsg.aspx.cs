﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using PayMammoth;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;
using PayMammoth.Connector.Classes.Requests;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using Enums = CS.General_v3.Enums;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.WebsiteAccountModule;


namespace PayMammothDeploy.cms.NotificationMessage
{
    public partial class sendTestNotificationMsg : BaseCMSPage
    {

       



        private MyTxtBoxString txtMessage = null;
        private MyTxtBoxString txtSendToUrl = null;
        private MyTxtBoxString txtIdentifier = null;
        private MyDropDownList cmbWebsiteAccount = null;
        private MyDropDownList cmbStatusCode = null;
        private MyDropDownList cmbNotificationType = null;

        private void initForm()
        {
            int width = 400;

            cmbNotificationType = formFields.Functionality.AddSingleChoiceListFromEnum<PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE>("cmbNotificationType", "Notification Type", true, addSpacesToCamelCasedName: true);
            {
                var allWebsiteAccounts = WebsiteAccountFactory.Instance.FindAll().ToList();
                allWebsiteAccounts.Sort((x1, x2) => (x1.WebsiteName.CompareTo(x2.WebsiteName)));
                MyDropDownListItems listItems = new MyDropDownListItems();
                foreach (var w in allWebsiteAccounts)
                {
                    listItems.AddOption(w.WebsiteName, w.ID.ToString());
                }
                cmbWebsiteAccount = formFields.Functionality.AddSingleChoiceList("txtWebsiteAccount", "Website Account", true, listItems);
            }
            txtMessage = formFields.Functionality.AddString("txtMessage", "Message", false, null, initialValue: "Test Message - " + CS.General_v3.Util.Date.Now.ToString("dd/MM/yyy HH:mm:ss"));
            txtIdentifier = formFields.Functionality.AddString("txtIdentifier", "Identifier", false, null, initialValue: CS.General_v3.Util.Random.GetAlpha(6,15));
            txtSendToUrl = formFields.Functionality.AddString("txtSendToUrl", "Send To Url", false, helpMessage: " If left null, this is sent to the notification url of the website account", 
                initialValue: CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "testing/test-notify.ashx");
            cmbStatusCode = formFields.Functionality.AddSingleChoiceListFromEnum<PayMammoth.Connector.Enums.NOTIFICATION_STATUS_CODE>("cmbStatusCode", "Status", true, addSpacesToCamelCasedName: true,
                selectedValue: PayMammoth.Connector.Enums.NOTIFICATION_STATUS_CODE.Success);
            
            
           


            formFields.Functionality.ClickSubmit += new EventHandler(formFields_ClickSubmit);
            formFields.Functionality.ButtonSubmitText = "Send Test Notification";
            formFields.Functionality.ButtonSubmit.WebControlFunctionality.ConfirmMessage = "Are you sure you want to send?";

        }

        void formFields_ClickSubmit(object sender, EventArgs e)
        {
            var notificationSender = BusinessLogic_v3.Util.InversionOfControlUtil.Get<INotificationCreatorAndSender>();

            var websiteAccount = WebsiteAccountFactory.Instance.GetByPrimaryKey(cmbWebsiteAccount.GetFormValueAsLongNullable().GetValueOrDefault());

            string sendToUrl = txtSendToUrl.GetFormValue();
            if (string.IsNullOrWhiteSpace(sendToUrl)) sendToUrl = websiteAccount.ResponseUrl;
            CreateNewNotificationCommand cmd = new CreateNewNotificationCommand((PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE)cmbStatusCode.GetFormValueAsInt(),
                websiteAccount, sendToUrl);

            cmd.Message = txtMessage.GetFormValue();
            cmd.Identifier = txtIdentifier.GetFormValue();
            cmd.StatusCode = (PayMammoth.Connector.Enums.NOTIFICATION_STATUS_CODE)cmbStatusCode.GetFormValueAsInt();

            var notificationMsg = notificationSender.CreateAndQueueNotification(cmd);
            

            int waitingSecs = 0;
            int maxToWait = 30;
            while (!notificationMsg.SendingFailed && !notificationMsg.AcknowledgedByRecipient && waitingSecs < maxToWait)
            {
                CS.General_v3.Util.ThreadUtil.Sleep(5000);
                notificationMsg.RefreshFromDb();
                waitingSecs+=5;

            }



            if (notificationMsg.AcknowledgedByRecipient)
            {
                CmsUtil.ShowStatusMessageInCMSAndRefresh(CS.General_v3.Util.Text.ConvertPlainTextToHTML("Notification sent successfully and acknowledged\r\n\r\n" + notificationMsg.StatusLog), Enums.STATUS_MSG_TYPE.Success);
            }
            else
            {
                CmsUtil.ShowStatusMessageInCMSAndRefresh(CS.General_v3.Util.Text.ConvertPlainTextToHTML("Notification could not be sent\r\n\r\n" + notificationMsg.StatusLog), Enums.STATUS_MSG_TYPE.Error);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            initForm();

        }
    }
}