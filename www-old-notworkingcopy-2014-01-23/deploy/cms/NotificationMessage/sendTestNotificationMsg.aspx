﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/CMS/pages/cms.master" AutoEventWireup="true" CodeBehind="sendTestNotificationMsg.aspx.cs" Inherits="PayMammothDeploy.cms.NotificationMessage.sendTestNotificationMsg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">

<h2>Sends a test notification message</h2>
<p>Please fill in the fields below, in order to send a test notification message.</p>
<CSControlsFormFields:FormFields ID="formFields" runat="server" />


</asp:Content>
