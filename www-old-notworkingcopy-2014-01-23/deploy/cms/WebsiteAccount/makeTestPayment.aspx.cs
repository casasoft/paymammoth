﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using PayMammoth;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;
using PayMammoth.Connector.Classes.Requests;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using Enums = CS.General_v3.Enums;


namespace PayMammothDeploy.cms.WebsiteAccount
{
    public partial class makeTestPayment : BaseCMSPage
    {

        public long WebsiteAccountId
        {
            get 
            {
                return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long>(BusinessLogic_v3.Constants.ParameterNames.ID);
            }
        }
        private PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount _websiteAccount = null;
        public PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount WebsiteAccount
        {
            get
            {
                if (_websiteAccount == null)
                    _websiteAccount = PayMammoth.Modules.Factories.WebsiteAccountFactory.GetByPrimaryKey(WebsiteAccountId);
                return _websiteAccount;
            }
        }

        private void checkRequirements()
        {
            if (WebsiteAccount == null)
            {
                CmsUtil.ShowStatusMessageInCMS("Website account is required", CS.General_v3.Enums.STATUS_MSG_TYPE.Warning);
                PayMammoth.Cms.WebsiteAccountModule.WebsiteAccountCmsFactory.Instance.GetListingUrl().RedirectTo();

            }
        }


        private MyTxtBoxString txtTitle = null;
        private MyTxtBoxStringMultiLine txtDescription = null;
        private MyTxtBoxString txtReference = null;
        private MyTxtBoxString txtClientAddress1 = null;
        private MyTxtBoxString txtClientAddress2 = null;
        private MyTxtBoxString txtClientAddress3 = null;
        private MyTxtBoxString txtClientLocality = null;
        private MyTxtBoxString txtClientPostCode = null;
        private MyDropDownList txtClientCountry = null;
        private MyTxtBoxString txtClientFirstName = null;
        private MyTxtBoxString txtClientMiddleName = null;
        private MyTxtBoxString txtClientLastName = null;
        private MyTxtBoxString txtClientEmail = null;
        private MyTxtBoxString txtClientTelephone = null;
        private MyTxtBoxString txtClientMobile = null;
        private MyDropDownList txtCurrency = null;
        private MyDropDownList txtLanguage = null;
        private MyDropDownList txtLanguageCountryCode = null;
        private MyTxtBoxString txtLanguageSuffix = null;
        private MyTxtBoxStringMultiLine txtNotes = null;
        private MyTxtBoxString txtOrderLinkOnClientWebsite = null;
        private MyTxtBoxString txtNotificationUrl= null;
        private MyTxtBoxString txtReturnUrlFailure = null;
        private MyTxtBoxString txtReturnUrlSuccess = null;
        private MyTxtBoxNumber txtPriceExcTax = null;
        private MyTxtBoxNumber txtHandlingAmount = null;
        private MyTxtBoxNumber txtShippingAmount = null;
        private MyTxtBoxNumber txtTaxAmount = null;
        private MyCheckBox chkRecurringProfileRequired = null;
        private MyDropDownList txtRecurringProfileBillingPeriod = null;
        private MyTxtBoxNumber txtRecurringProfileTotalBillingCycles = null;
        private MyTxtBoxNumber txtRecurringProfileBillingFrequency = null;



        private void initForm()
        {
            int width = 400;

            txtTitle = formFields.Functionality.AddString("txtTitle", "Title", false, null,initialValue: "Test Item for Payment");
            txtDescription = formFields.Functionality.AddStringMultiline("txtDescription", "Description", false, null, initialValue: "Description");
            txtReference = formFields.Functionality.AddString("txtReference", "Reference", false, null, initialValue: "TEST-" + CS.General_v3.Util.Date.Now.ToString("ddMMyyyyHHmmss"));
            txtClientAddress1 = formFields.Functionality.AddString("txtClientAddress1", "Client Address 1", false, null, initialValue: "Test Address 1");
            txtClientAddress2 = formFields.Functionality.AddString("txtClientAddress2", "Client Address 2", false, null, initialValue: "Test Address 2");
            txtClientAddress3 = formFields.Functionality.AddString("txtClientAddress3", "Client Address 3", false, null, initialValue: "Test Address 3");
            txtClientLocality = formFields.Functionality.AddString("txtClientLocality", "Client Locality", false, null, initialValue: "Test Locality");
            txtClientPostCode = formFields.Functionality.AddString("txtClientPostCode", "Client Post Code", false, null, initialValue: "Test Post Code");
            txtClientCountry = formFields.Functionality.AddSingleChoiceListFromEnum<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166>("txtClientCountry", "Client Country", false, null, selectedValue: CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.UnitedKingdom, addSpacesToCamelCasedName: true);
            txtClientFirstName = formFields.Functionality.AddString("txtClientFirstName", "Client First Name", false, null, initialValue: "John");
            txtClientMiddleName = formFields.Functionality.AddString("txtClientMiddleName", "Client Middle Name", false, null, initialValue: "'Gangsta'");
            txtClientLastName = formFields.Functionality.AddString("txtClientLastName", "Client Last Name", false, null, initialValue: "Doe");
            txtClientEmail = formFields.Functionality.AddString("txtClientEmail", "Client Locality", false, null, initialValue: "email.test@casasoft.com.mt");
            txtClientTelephone = formFields.Functionality.AddString("txtClientTelephone", "Client Telephone", false, null, initialValue: "Test Tel");
            txtClientMobile = formFields.Functionality.AddString("txtClientMobile", "Client Mobile", false, null, initialValue: "Test Mob");
            txtCurrency = formFields.Functionality.AddSingleChoiceListFromEnum<CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217>("txtCurrency", "Currency", false, null, null, CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro, addSpacesToCamelCasedName: false);
            txtLanguage = formFields.Functionality.AddSingleChoiceListFromEnum<CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639>("txtLanguage", "Language", false, null, null, CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.English, addSpacesToCamelCasedName: true);
            txtLanguageCountryCode = formFields.Functionality.AddSingleChoiceListFromEnum<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166>("txtLanguageCountryCode", "Language (Country Code)", false, addSpacesToCamelCasedName: true);
            txtLanguageSuffix = formFields.Functionality.AddString("txtLanguageSuffix", "Language Suffix", false, null, null);


            txtNotes = formFields.Functionality.AddStringMultiline("txtNotes", "Notes", false, null, initialValue: "Test Notes");
            txtOrderLinkOnClientWebsite = formFields.Functionality.AddString("txtOrderLinkOnClientWebsite", "Order Link on client's website", false, null, initialValue: Urls.Url_Test_OrderLinkOnClient);
            txtNotificationUrl = formFields.Functionality.AddString("txtNotificationUrl", "Notification Url", false, 
                "The URL to send the background notifications related to this request to.  If this is empty, the url in the website account is taken", 
                initialValue: WebsiteAccount.ResponseUrl);
            txtReturnUrlFailure = formFields.Functionality.AddString("txtReturnUrlFailure", "Return Url (Failure)", false, null, initialValue: Urls.Url_Test_PaymentFailure);
            txtReturnUrlSuccess = formFields.Functionality.AddString("txtReturnUrlSuccess", "Return Url (Success)", false, null, initialValue: Urls.Url_Test_PaymentSuccess);

            txtPriceExcTax = formFields.Functionality.AddDouble("txtPriceExcTax", "Price (exc Tax)", false, null, initialValue: 0.03);
            txtHandlingAmount = formFields.Functionality.AddDouble("txtHandlingAmount", "Handling Amount", false, null, initialValue: 0);
            txtShippingAmount = formFields.Functionality.AddDouble("txtShippingAmount", "Shipping Amount", false, null, initialValue: 0);
            txtTaxAmount = formFields.Functionality.AddDouble("txtTaxAmount", "Tax Amount", false, null, initialValue: 0);
            
            //recurring profile - start
            chkRecurringProfileRequired = formFields.Functionality.AddBool("chkRecurringProfileRequired",
                                                                           "Recurring Profile Required", false);
            txtRecurringProfileBillingPeriod =
                formFields.Functionality.AddSingleChoiceListFromEnum<PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD>
                    ("txtRecurringProfileBillingPeriod", "Recurring Profile Billing Period", false, 
                    "If you specify this as 'Day', and doing a PayPal test, paypal will send the recurring profile payment every X minutes, based on the frequency",
                    null,
                     PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD.Day, addSpacesToCamelCasedName: true);
            txtRecurringProfileTotalBillingCycles =
                formFields.Functionality.AddInteger("txtRecurringProfileTotalBillingCycles",
                "Recurring Profile Total Billing Cycles", false, helpMessage:"If 0, the recurring profile will never expire, otherwise the value refers to the number of times this will recur.", initialValue: 0);
            txtRecurringProfileBillingFrequency =
                formFields.Functionality.AddInteger("txtRecurringProfileBillingFrequency",
                                                    "Recurring Profile Billing Frequency", false,
                                                    helpMessage:
                                                        "This is the number relating to the billing cycle ex. 1/Month or 1/Week etc",
                                                    initialValue: 1);
            //recurring profile - end

            formFields.Functionality.ClickSubmit += new EventHandler(formFields_ClickSubmit);
            formFields.Functionality.ButtonSubmitText = "Make Test Payment";
            formFields.Functionality.ButtonSubmit.WebControlFunctionality.ConfirmMessage = "Are you sure you want to do the test payment?";

        }

        void formFields_ClickSubmit(object sender, EventArgs e)
        {
            PayMammoth.Connector.Classes.Requests.InitialRequestDetails initialReq = new PayMammoth.Connector.Classes.Requests.InitialRequestDetails(WebsiteAccount.Code, WebsiteAccount.SecretWord);
            initialReq.ClientAddress1 = txtClientAddress1.GetFormValueAsString();
            initialReq.ClientAddress2 = txtClientAddress2.GetFormValueAsString();
            initialReq.ClientAddress3 = txtClientAddress3.GetFormValueAsString(); 
            {
                Enums.ISO_ENUMS.Country_ISO3166? code = txtClientCountry.GetFormValueAsEnumNullable<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166>();
                initialReq.ClientCountry = code;
                
            }

            initialReq.ClientEmail = txtClientEmail.GetFormValueAsString(); 
            initialReq.ClientFirstName = txtClientFirstName.GetFormValueAsString();
            initialReq.ClientLastName = txtClientLastName.GetFormValueAsString();
            initialReq.ClientTelephone = txtClientTelephone.GetFormValueAsString(); 
            initialReq.ClientMobile = txtClientMobile.GetFormValueAsString(); 
            initialReq.ClientLocality = txtClientLocality.GetFormValueAsString(); 
            initialReq.ClientMiddleName = txtClientMiddleName.GetFormValueAsString(); 
            initialReq.ClientPostCode = txtClientPostCode.GetFormValueAsString();
            
            {
                var code = txtCurrency.GetFormValueAsEnumNullable<CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217>();
                initialReq.CurrencyCode = (code.Value);
                
            }
            initialReq.Description = txtDescription.GetFormValueAsString(); 
            initialReq.HandlingAmount = txtHandlingAmount.GetFormValueAsDoubleNullable().GetValueOrDefault();
            initialReq.Notes = txtNotes.GetFormValueAsString(); 
            initialReq.OrderLinkOnClientWebsite = txtOrderLinkOnClientWebsite.GetFormValueAsString(); 

            {
                var code = txtLanguage.GetFormValueAsEnumNullable<CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639>();
                initialReq.LanguageCode = code;
                
            } 
            {
                var code = txtLanguageCountryCode.GetFormValueAsEnumNullable<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166>();
                initialReq.LanguageCountryCode = code;
                
            }

            initialReq.LanguageSuffix = txtLanguageSuffix.GetFormValueAsString(); 
            
            initialReq.ItemDetails.Add(new ItemDetailImpl("Test Item 1", "Test Description 1"));
            initialReq.ItemDetails.Add(new ItemDetailImpl("Test Item 2", "Test Description 2"));
            initialReq.ItemDetails.Add(new ItemDetailImpl("Test Item 3", "Test Description 3"));
            initialReq.PriceExcTax = txtPriceExcTax.GetFormValueAsDoubleNullable().GetValueOrDefault();
            initialReq.Reference = txtReference.GetFormValueAsString(); 
            initialReq.ReturnUrlFailure = txtReturnUrlFailure.GetFormValueAsString(); 
            initialReq.ReturnUrlSuccess = txtReturnUrlSuccess.GetFormValueAsString();
            initialReq.ShippingAmount = txtShippingAmount.GetFormValueAsDoubleNullable().GetValueOrDefault();
            initialReq.TaxAmount = txtTaxAmount.GetFormValueAsDoubleNullable().GetValueOrDefault();
            initialReq.Title = txtTitle.GetFormValueAsString(); 
            //initialReq.SecretWord = WebsiteAccount.SecretWord;
            //initialReq.WebsiteAccountCode = WebsiteAccount.Code;
            PayMammoth.Connector.Constants.PAYMAMMOTH_BASE_URL = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl().GetURL(fullyQualified: true);//set the base url to the current url
            
            //recurring profile start
            initialReq.RecurringProfileRequired = chkRecurringProfileRequired.GetFormValueAsBool();
            initialReq.RecurringProfile_BillingPeriod = txtRecurringProfileBillingPeriod.GetFormValueAsEnum<PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD>();
            initialReq.RecurringProfile_TotalBillingCycles = txtRecurringProfileTotalBillingCycles.GetFormValueAsInt();
            initialReq.RecurringProfile_BillingFrequency = txtRecurringProfileBillingFrequency.GetFormValueAsInt();

            //recurring profile end

            var response = initialReq.CreateRequestWithPayMammoth();
            response.RedirectToPayMammoth();
            

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            checkRequirements();
            initForm();

        }
    }
}