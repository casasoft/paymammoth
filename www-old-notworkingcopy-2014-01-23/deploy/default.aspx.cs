﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Pages.Frontend;

namespace PayMammothDeploy
{
    public partial class _default : BasePage
    {
        public _default()
        {
            if (!CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                
                
                this.Functionality.RequiresSSL = CS.General_v3.Settings.GetSettingFromDatabase<bool>(CS.General_v3.Enums.SETTINGS_ENUM.OnlinePayment_PaymentPageUsesSSL);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "PayMammoth";
        }
    }
}