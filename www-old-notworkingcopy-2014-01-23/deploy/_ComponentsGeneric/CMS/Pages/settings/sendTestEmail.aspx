﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/CMS/Pages/cms.master" AutoEventWireup="true" CodeBehind="sendTestEmail.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.settings.sendTestEmail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">
<p>
Use this feature to test out the SMTP settings of the system and verify email-sending functionality.  Simply fill in the 
fields below and click on send to send out an email.  Results will be displayed afterwards.
</p>
<CSControlsFormFields:FormFields ID="formFields" runat="server" />
</asp:Content>
