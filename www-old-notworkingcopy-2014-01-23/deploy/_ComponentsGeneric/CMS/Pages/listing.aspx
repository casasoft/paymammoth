<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/CMS/Pages/cms.master"
    AutoEventWireup="true" CodeBehind="listing.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.listingPage" %>
<%@ Register TagPrefix="CMSListing" Namespace="CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses" Assembly="WebComponentsGeneralV3" %>

<%@ Register Src="~/_ComponentsGeneric/CMS/UserControls/Listing/ucPreListing.ascx" TagPrefix="CMSCommon"
    TagName="PreListing" %>
<%@ Register Src="~/_ComponentsGeneric/CMS/UserControls/Listing/ucPostListing.ascx" TagPrefix="CMSCommon"
    TagName="PostListing" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="cphMain" runat="server">
    <div class="search-panel-container">
        <a href="javascript:" id="aSearchPanelToggle" runat="server" class="button-search-panel-toggle">
            Search</a>
        <CSControlsFormFields:FormFields id="formSearch" runat="server">
        </CSControlsFormFields:FormFields>
    </div>
    <p id="pSearchDescription" runat="server" visible="false">
    </p>
    <p id="pUpdateNote" runat="server" visible="false" class="note update-msg">
        <strong>Note: If you update fields (textboxes, checkboxes) directly in the listing, it is IMPORTANT that you
            click on the SAVE button at the bottom or top of the page!</strong>
    </p>
    <p id="pDeletedMsg" runat="server" class="note deleted-items" visible="false">
        * Deleted items are shown slightly <span class="deleted-faded">faded</span>.
    </p>
    <asp:PlaceHolder ID="phListing" Visible="false" runat="server">
        <div class="listing-update">
            <CSControls:MyButton ID="btnListingUpdateTop" CssClass="listing-update" runat="server"
                Text="Save" />
        </div>
        <CMSListing:ListingWithPaging ID="listing" runat="server" />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phTree" Visible="false" runat="server">
        <CMS:CmsTreeStructure ID="cmsTreeStructure" runat="server" />
    </asp:PlaceHolder>
    <div class="listing-update">
        <CSControls:MyButton ID="btnListingUpdateBottom" CssClass="listing-update" runat="server"
            Text="Save" />
    </div>
</asp:Content>
