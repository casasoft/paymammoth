﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/CMS/Pages/cms.master" AutoEventWireup="true" CodeBehind="chooseFields.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.Export.chooseFields" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
<p>
Please tick / un-tick which fields you want in the export.  After you click on export, a report will be generated, based on the items available in the previous window.
This export can be downloaded in CSV (Excel) format if required
</p>
<div class="buttons-container report-buttons">
    <button class="button report-button report-button-select-all">Select All</button>
    <button class="button report-button report-button-unselect-all">Un-Select All</button>
    <button class="button report-button report-button-toggle-all">Toggle All</button>
</div>
<CSControlsFormFields:FormFields CssClass="report-fields" ID="formFieldList" runat="server"></CSControlsFormFields:FormFields>
<div id="divExport" runat="server" class="export" visible="false">
</div>

<script type="text/javascript">
    function setCheckboxesValue(value) {
        jQuery(".report-fields input[type=checkbox]").each(function (i, x) {
            var jX = jQuery(x);
            var newValue = value == null ? !jX.attr('checked') : value;
            jX.attr('checked', newValue);
        });
    }
    jQuery(function () {

        jQuery(".report-button-select-all").click(function () {
            setCheckboxesValue(true);
        });
        jQuery(".report-button-unselect-all").click(function () {
            setCheckboxesValue(false);
        });
        jQuery(".report-button-toggle-all").click(function () {
            setCheckboxesValue();
        });
    });
</script>

</asp:Content>
