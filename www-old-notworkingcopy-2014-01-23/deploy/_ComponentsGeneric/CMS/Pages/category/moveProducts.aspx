﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/CMS/Pages/cms.master" AutoEventWireup="true" CodeBehind="moveProducts.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.category.moveProducts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">

<p>
This will allow you to move <strong>ALL</strong> products from this category, to the selected category.  Please note that this is irreversible!
</p>
<CSControlsFormFields:FormFields ID="formMove" runat="server" />



</asp:Content>
