﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="dataImportResults.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.DataImport.dataImportResults" %>
<div id="divResults" runat="server" class="results" visible="false">
    
    <CSControls:MyButton ID="btnTerminate" ValidationGroup="none" runat="server" Text="Terminate Job"></CSControls:MyButton>

    <CSControls:MyButton ID="btnRefresh" ValidationGroup="none" runat="server" Text="Refresh Status"></CSControls:MyButton>

        <h3>
            Results</h3>
        <table class="import-info">
            <tr>
                <td class="label">
                    Filename:
                </td>
                <td id="tdFilename" runat="server">
                    N / A
                </td>
            </tr>
            <tr id="trZipFile" runat="server">
                <td class="label">
                    ZIP File:
                </td>
                <td id="tdZIPFile" runat="server">
                    N / A
                </td>
            </tr>
            <tr>
                <td class="label">
                    Started On:
                </td>
                <td id="tdStartedOn" runat="server">
                    N / A
                </td>
            </tr>
            <tr>
                <td class="label">
                    Finished On:
                </td>
                <td id="tdFinishedOn" runat="server">
                    N / A
                </td>
            </tr>
        </table>
        <asp:PlaceHolder ID="phMessages" runat="server"></asp:PlaceHolder>
        <CSControls:MyUnorderedList ID="ulMessages" runat="server" Visible="false" />
        
        <asp:PlaceHolder ID="phResultsTable" runat="server"></asp:PlaceHolder>
    </div>