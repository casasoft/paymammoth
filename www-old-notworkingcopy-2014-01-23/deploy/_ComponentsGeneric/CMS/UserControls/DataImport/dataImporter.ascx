﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="dataImporter.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.DataImport.dataImporter" %>
<%@ Register TagPrefix="CSSpecialized" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields"
    Assembly="WebComponentsGeneralV3" %>
<%@ Register Src="dataImportUploadForm.ascx" TagName="dataImportUploadForm" TagPrefix="uc1" %>
<%@ Register Src="dataImportResults.ascx" TagName="dataImportResults" TagPrefix="uc2" %>
<%@ Register Src="dataImportColumnPreParser.ascx" TagName="dataImportColumnPreParser"
    TagPrefix="uc3" %>
<div class="data-import">
    <uc1:dataImportUploadForm ID="dataImportUploadForm" runat="server" />
    <uc3:dataImportColumnPreParser ID="dataImportColumnPreParser" runat="server" />
    <uc2:dataImportResults ID="dataImportResults" runat="server" />
    
</div>
