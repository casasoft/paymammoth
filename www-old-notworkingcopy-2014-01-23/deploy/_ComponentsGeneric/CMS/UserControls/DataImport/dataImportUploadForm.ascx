﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="dataImportUploadForm.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.DataImport.dataImportUploadForm" %>
<h2>
    Data Importer</h2>
<p>
    Here you can bulk-import items. Import should be done from an excel file (*.xls,
    *.xlsx) or a comma-seperated file (*.csv).
</p>
<CSControlsFormFields:FormFields ID="formImport" runat="server" />
<div id="divImportJobRunning" runat="server" class="import-job-running" visible="false">
    <p>
        An import job is currently running. You cannot run more than one import job at once.
        Refresh to view the status. If you wish, you can choose to terminate the job by
        clicking on the below button</p>
    <CSControls:MyButton ID="btnTerminateJob" ValidationGroup="none" runat="server" Text="Terminate Job" ConfirmMessage="Are you sure you want to terminate the current job?" />
</div>
