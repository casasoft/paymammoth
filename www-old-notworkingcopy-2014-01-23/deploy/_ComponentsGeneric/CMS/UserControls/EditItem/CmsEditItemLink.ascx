﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CmsEditItemLink.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.EditItem.CmsEditItemLink" %>

<div class="cms-edit-item-container">
    <a class="cms-edit-item-link" id="aLink" runat="server" href="#" title="Edit in CMS">
        Edit in CMS</a>
</div>
<script>
    jQuery(function () {
        jQuery('body').append(jQuery('.cms-edit-item-container')); //Move it to the top most
    });
</script>