﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"    CodeBehind="search.aspx.cs" 
Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Categories.search" %>
<%@ Register TagPrefix="General" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Navigation"
    Assembly="WebComponentsGeneralV3" %>

<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/GenericListingItemsFullPage.ascx" TagName="GenericListingItemsFullPage" TagPrefix="Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
<script src="/_common/static/js/jQuery/plugins/searchhighlight/0.33/SearchHighlight.pack.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <div class="content-page-breadcrumbs" runat="server" id="divNavigationBreadcrumbs" visible="false">
       <General:NavigationBreadcrumbs runat="server" ID="navigationBreadcrumbs" />
    </div>
    <div class="search-listing-full-page-container">
    <Common:GenericListingItemsFullPage ID="searchFullPage" runat="server" />
    </div>
    <asp:PlaceHolder ID="placeholderSearchHighlightJS" runat="server">
    <script>
        jQuery(function () {
            var options = {
                exact: "partial",
                keys: "<asp:Literal id='jsSearchHighlightKeywords' runat='server'></asp:Literal>",
                highlight: jQuery(".generic-items-listing-container")
            }
            jQuery(document).SearchHighlight(options);
        });
</script></asp:PlaceHolder>

</asp:Content>
