﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="register.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member.register" %>

<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ProfileFields" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Profile/ProfileFields.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <div class="register-wrapper">
        <CommonControls:ContentPage ID="contentPage" runat="server">
            <BottomContent>
                <CommonControls:ProfileFields ID="registerFields" runat="server" />
            </BottomContent>
        </CommonControls:ContentPage>
    </div>
</asp:Content>
