﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
     CodeBehind="reset-password.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member.reset_password" %>

<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ResetPasswordFields" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Password/ResetPasswordFields.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:ContentPage ID="contentPage" runat="server" />
    <CommonControls:ResetPasswordFields runat="server" ID="resetPasswordFields" />
</asp:Content>