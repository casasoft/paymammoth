﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Core.master" AutoEventWireup="true"
    CodeBehind="dialog-article.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Article.dialogArticle" %>

<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="coreHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="coreContentPlaceHolder" runat="server">
<div class="content-page-dialog-container">
    <CommonControls:ContentPage ID="contentPage" runat="server" />    
    </div>
   
</asp:Content>
