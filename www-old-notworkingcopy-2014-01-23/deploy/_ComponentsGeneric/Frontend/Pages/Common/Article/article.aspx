﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="article.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Article.article" %>

<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ArticleComments" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ArticleComments.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="PreviousNextArticle" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/PreviousNextArticle.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:ContentPage ID="contentPage" runat="server" />    
    <CommonControls:PreviousNextArticle ID="previousNextEntry" runat="server" />
    <CommonControls:ArticleComments ID="contentPageComments" runat="server" />
</asp:Content>
