﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="events.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Events.events" %>

<%@ Register TagPrefix="CommonControls" TagName="GenericListingItemsFullPage" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/GenericListingItemsFullPage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:GenericListingItemsFullPage runat="server" ID="eventsListing" />
</asp:Content>
