﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="contact.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Contact.contact" %>

<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ContactUsForm" Src="~/_ComponentsGeneric/Frontend/UserControls/Contact/ContactUsForm.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ContactUsDetails" Src="~/_ComponentsGeneric/Frontend/UserControls/Contact/ContactUsDetails.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <div class="contact-page-container">
        <CommonControls:ContentPage ID="contentPage" runat="server">
            <BottomContent>
                <div class="contact-page-content-wrapper clearfix">
                    <div class="online-enquiry-form-wrapper">
                        <CommonControls:ContactUsForm ID="contactUsForm" runat="server" />
                    </div>
                    <div class="contact-details-wrapper">
                        <CommonControls:ContactUsDetails ID="contactUsDetails" runat="server" />
                    </div>
                </div>
            </BottomContent>
        </CommonControls:ContentPage>
    </div>
</asp:Content>
