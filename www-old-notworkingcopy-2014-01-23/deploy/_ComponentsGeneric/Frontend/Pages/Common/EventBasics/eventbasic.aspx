﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="eventbasic.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.EventBasics.eventbasic" %>

<%@ Register TagPrefix="CommonControls" TagName="PrettyPhotoImageGallery" Src="~/_ComponentsGeneric/Frontend/UserControls/Gallery/PrettyPhotoImageGallery.ascx" %>
<%@ Register TagPrefix="Control" TagName="AddThisControl" Src="~/_ComponentsGeneric/Frontend/UserControls/SocialNetworking/AddThis/AddThisControl.ascx" %>
<%@ Register TagPrefix="General" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Navigation"
    Assembly="WebComponentsGeneralV3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <div class="event-page-container">
        <div class="content-page-breadcrumbs" runat="server" id="divNavigationBreadcrumbs">
            <General:NavigationBreadcrumbs runat="server" ID="navigationBreadcrumbs" />
        </div>
        <div class="event-page-heading-container clearfix">
            <div class="content-page-heading-content">
                <h1 runat="server" id="h1PageTitle"></h1>
                <span class="event-page-heading-subtitle" runat="server" id="spanCourseSubTitle"></span>
                <div class="event-page-date-and-location" id="divDateAndLocation" runat="server">
                    11th September at Excelsior, Malta
                </div>
            </div>
            <div class="content-page-heading-add-this-wrapper" runat="server" id="divHeadingAddThisWrapper"
                visible="false">
                <Control:AddThisControl runat="server" ID="headingAddThis" />
            </div>
        </div>

        <div class="event-page-description-container clearfix">
            <div class="event-page-description-image">
                <CSControls:MyImage runat="server" ID="imgMainImage">
                </CSControls:MyImage>
            </div>
            <div class="event-page-description-content html-container" runat="server" id="divCourseDescription">
            </div>

        </div>
        <div class="event-page-media-gallery-container" runat="server" id="divMediaGallery">
            <CommonControls:PrettyPhotoImageGallery runat="server" ID="eventGallery" />
        </div>
    </div>
</asp:Content>
