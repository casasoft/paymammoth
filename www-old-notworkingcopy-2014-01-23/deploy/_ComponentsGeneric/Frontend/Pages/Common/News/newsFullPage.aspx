﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="newsFullPage.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.News.newsFullPage" %>

<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" TagPrefix="Common" TagName="ContentPage" %>
<%@ Register TagPrefix="General" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Navigation"
    Assembly="WebComponentsGeneralV3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <div class="content-page-breadcrumbs" runat="server" id="divNavigationBreadcrumbs">
        <General:NavigationBreadcrumbs runat="server" ID="navigationBreadcrumbs" />
    </div>
    <Common:ContentPage runat="server" ID="ContentPage" />
</asp:Content>
