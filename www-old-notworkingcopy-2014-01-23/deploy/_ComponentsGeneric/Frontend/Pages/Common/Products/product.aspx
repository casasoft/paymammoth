﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
 CodeBehind="product.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Products.product" %>

<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Products/ProductFullPage.ascx" TagName="ProductFullPage" TagPrefix="Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
    <script src="/_common/static/js/jQuery/plugins/anythingSlider/1.7.12/js/jquery.anythingslider.js" type="text/javascript"></script>
    <link href="/_common/static/js/jQuery/plugins/anythingSlider/1.7.12/css/anythingSlider.css" type="text/css" rel="Stylesheet" />
    <link href="/_common/static/js/jQuery/plugins/anythingSlider/1.7.12/css/anythingSlider-ie.css" type="text/css" rel="Stylesheet" />
    <script src="/_common/static/js/jQuery/plugins/easing/jquery.easing.1.3.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <Common:ProductFullPage id="productFullPage" runat="server" />
</asp:Content>
