﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PleaseLogin.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Login.PleaseLogin" %>
<%@ Register TagPrefix="SocialLoginButtons" TagName="Facebook" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/FacebookLoginButton.ascx" %>
<%@ Register TagPrefix="SocialLoginButtons" TagName="Google" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/GoogleLoginButton.ascx" %>
<%@ Register TagPrefix="SocialLoginButtons" TagName="Twitter" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/TwitterLoginButton.ascx" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialLoginButtons.ascx" TagPrefix="SocialLoginButtons" TagName="SocialLoginButtons" %>

<div class="submit-comments-please-login" id="divNeedLogin" runat="server">
    <h2 runat="server" id="h3Title"></h2>
    <div class="submit-comments-please-login-text" >
        <p id="divNeedLoginText" runat="server">
            You must be logged in to your account in order to submit a comment. Choose one of
            the below options:</p>
    </div>
    <ul class="submit-comments-choose-options">
            <li class="submit-comments-login-all clearfix">
                <SocialLoginButtons:SocialLoginButtons runat="server" id="SocialLoginButtons" />
            </li>
            <li class="submit-comments-register">
                <a id="aRegisterAccount" runat="server">Register an account</a>
            </li>
            <li class="submit-comments-login">
                <a id="aLogin" runat="server" class="top-login-link">Login using your account</a>
            </li>
        </ul>
        <script type="text/javascript">

            jQuery(function () {
                jQuery('.top-login-link').click(function (e) {
                    e.preventDefault();
                    jQuery("<div><iframe src='<asp:Literal id='ltlLoginURL' runat='server' />' class='iframe-login'/></div>").dialog({ title: "<asp:Literal id='ltlLoginPageTitle' runat='server' />", modal: true, width: "auto" });
                });
            });
    </script>
</div>