﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CarouFredSelMediaGallery.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Carousel.CarouFredSel.CarouFredSelMediaGallery" %>
<%@ Register TagPrefix="CommonControls" TagName="CarouFredSelControl" Src="CarouFredSelControl.ascx" %>

<CommonControls:CarouFredSelControl ID="carousel" runat="server" />
