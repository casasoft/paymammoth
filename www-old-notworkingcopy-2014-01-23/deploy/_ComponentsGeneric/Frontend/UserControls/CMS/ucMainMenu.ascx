﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.CMS.ucMainMenu" Codebehind="ucMainMenu.ascx.cs" %>
<div class="main-menu">
    <asp:Repeater ID="repGroups" runat="server">
        <ItemTemplate>
            <table cellpadding="0" cellspacing="0" class="main-menu-group">
                <asp:Repeater ID="repLinks" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td class="link" id="tdLink" runat="server">
                                <a href="#" id="aHref" runat="server"><span id="spanImageIcon" runat="server"></span>
                                    <asp:Literal ID="ltlLinkTitle" runat="server">Manage Users</asp:Literal></a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </ItemTemplate>
    </asp:Repeater>
</div>
