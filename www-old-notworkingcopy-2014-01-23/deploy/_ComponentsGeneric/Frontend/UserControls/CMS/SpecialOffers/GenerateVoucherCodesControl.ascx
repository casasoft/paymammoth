﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenerateVoucherCodesControl.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.CMS.SpecialOffers.GenerateVoucherCodesControl" %>
<CSControlsFormFieldsNew:FormFields id="formVoucherCodes" runat="server"></CSControlsFormFieldsNew:FormFields>
<asp:PlaceHolder Visible="false" ID="phResults" runat="server">
<h2>Results</h2>
<p>Below is a list of all the generated voucher codes</p>
<CSControlsCommon:MyUnorderedList ID="ulResults" runat="server" />

</asp:PlaceHolder>