﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LanguageSelectionHorizontalListing.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Languages.LanguageSelectionHorizontalListing" %>
<div class="language-selection-container" id="divLanguageSelectionContainer" runat="server">
    <div class="language-selection-available-languages clearfix" runat="server" id="divAvailableLanguage">
    </div>
</div>