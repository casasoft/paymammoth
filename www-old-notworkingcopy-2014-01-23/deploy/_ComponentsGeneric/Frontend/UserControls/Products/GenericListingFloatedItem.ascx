﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericListingFloatedItem.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.GenericListingFloatedItem" %>
<div class="listing-item-container" runat="server" id="divListingItem">
    <div class="listing-item-title-container">
        <a runat="server" id="anchorListingTitle" class="listing-item-title" href="/"></a>
    </div>
    <div class="listing-item-image">
        <CSControls:MyImage runat="server" ID="imgProduct">
        </CSControls:MyImage>
    </div>
    <div class="listing-item-code-container" id="listingItemCodeContainer" runat="server">
        <span class='listing-item-code-label' id="listingItemCodeLabel" runat="server">Item Code</span>: <span class='listing-item-code-value'
            id="listingItemCodeValue" runat="server">BCDD032</span>
    </div>
    <div class="listing-item-description" runat="server" id="divProductItemDescription">
    </div>
    <div class="listing-item-footer clearfix">
        <span class="listing-item-price" runat="server" id="spanPrice"></span>
        <span class="listing-item-new-price" runat="server" id="spanNewPrice" visible="false"></span>
        <a runat="server" id="anchorViewMore">
            </a>
    </div>
</div>
