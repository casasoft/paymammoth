﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductFullPage.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.ProductFullPage" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Contact/ContactUsForm.ascx"
    TagName="ContactUsForm" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Products/RelatedProducts.ascx"
    TagName="RelatedProducts" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/CMS/UserControls/EditItem/CmsEditItemLink.ascx"
    TagName="CmsEditItemLink" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/SocialNetworking/AddThis/AddThisControl.ascx"
    TagName="AddThisControl" TagPrefix="Common" %>
<%@ Register TagPrefix="Navigation" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Navigation" Assembly="WebComponentsGeneralV3" %>
<%@ Register TagPrefix="Common" TagName="ProductFullDetails" Src="~/_ComponentsGeneric/Frontend/UserControls/Products/ProductFullDetails.ascx" %>
<div class="product-item-top-container clearfix">
    <div class="product-item-top-breadcrumbs">
        <Navigation:NavigationBreadcrumbs runat="server" ID="navigationBreadcrumbs" />
    </div>
    <div class="product-item-top-add-this" runat="server" id="divProductTopAddThis">
        <Common:AddThisControl runat="server" ID="addThisCtrl" />
    </div>
</div>
<Common:ProductFullDetails id="productDetails" runat="server" />
<div class="online-enquiry-form-wrapper product-online-enquiry" runat="server" id="divContactFormContainer">
    <div class="toggle-button-container clearfix" id="divToggleButtonContainer" runat="server">
        <CSControls:MyAnchor runat="server" ID="anchorToggleEnquiry" CssClass="button online-enquiry-form-wrapper-toggle-button"></CSControls:MyAnchor>
    </div>
    <Common:ContactUsForm runat="server" ID="contactForm"/>
</div>
<Common:RelatedProducts runat="server" ID="RelatedProducts" />
<Common:CmsEditItemLink ID="cmsEditItem" runat="server" />
