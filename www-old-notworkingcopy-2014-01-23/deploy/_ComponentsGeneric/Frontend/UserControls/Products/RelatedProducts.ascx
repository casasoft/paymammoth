﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RelatedProducts.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.RelatedProducts" %>
<%@ Register TagPrefix="Common" TagName="GenericListingItems" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/GenericListingItems.ascx" %>
<div class="related-products-container">
    <h2 class="h2" runat="server" id="spanRelatedProducts">Related Products</h2>
    <Common:GenericListingItems runat="server" ID="relatedProductsListing" />
</div>
