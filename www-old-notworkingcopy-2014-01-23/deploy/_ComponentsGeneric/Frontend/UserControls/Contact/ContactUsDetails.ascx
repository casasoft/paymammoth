﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUsDetails.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Contact.ContactUsDetails" %>
<%@ Register TagPrefix="CommonControls" TagName="SocialNetworksIcons" Src="~/_ComponentsGeneric/Frontend/UserControls/SocialNetworking/SocialNetworksIcons.ascx" %>
<div class="contact-details-content">
    <h2 runat="server" id="h2ContactDetailsTitle" class="contact-details-title">
        Contact Details</h2>
    <span class="contact-details-director-name" runat="server" id="spanDirector" visible="false">
    </span><span class="contact-details-address" runat="server" id="spanAddress" visible="false">
    </span>
    <CSControls:MyTable runat="server" ID="tblDetails">
    </CSControls:MyTable>
    <CommonControls:SocialNetworksIcons runat="server" ID="socialNetworksIcons" CssClass="contact-details-socialnetworks-icons-container" />
    <div class="contact-details-map-container" runat="server" id="divMapContainer">
        <h2 id="hLocationMap" runat="server">
            Location Map</h2>

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="/_common/static/js/jQuery/plugins/googlemaps/v3/jquery.ui.map.full.min.js"></script>
        <div class="contact-details-google-map">
        </div>
        <div class="contact-details-google-map-view-larger" id="divViewLargerMap" runat="server">
            <a id="aViewLargerMap" runat="server" href="#" target="_blank">View larger map</a>
        </div>
        <script>
            jQuery(function () {
                jQuery('.contact-details-google-map').gmap({center:new google.maps.LatLng(<asp:Literal id="ltlGoogleMapsPosition" runat="server" />), zoom: <asp:Literal id="ltlGoogleMapsZoomLevel" runat="server" />}).bind('init', function (ev, map) {
                    var item = jQuery('.contact-details-google-map').gmap('addMarker', { 'position': '<asp:Literal id="ltlGoogleMapsPosition2" runat="server" />', 'bounds': true });
                    
                    map.setZoom(<asp:Literal id="ltlGoogleMapsZoomLevel2" runat="server" />);
                });
            });
        </script>
    </div>
</div>
