﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUsForm.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Contact.ContactUsForm" %>
<div class="contact-us-online-enquiry-form">
    <div class="contact-us-online-enquiry-form-description clearfix">
      <h2 runat="server" id="h2EnquiryFormTitle">Online Enquiry Form</h2>
      <p runat="server" id="EnquiryFormDescription">We would be glad to hear from you on any enquiry you might have. Our team will get back to you shortly.</p>  
    </div>
    <CSControlsFormFields:FormFields ID="formContactUs" runat="server"></CSControlsFormFields:FormFields>
</div>
