﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentDetails.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Orders.Information.PaymentDetails" %>
<div class="payment-success-payment-details-wrapper">
    <div id="divImmediatePaymentContainer" runat="server" visible="false">
        <span class="payment-success-immediate-payment-text" id="spanCTImmediatePayment"
            runat="server"></span>
    </div>
    <div id="divChequePaymentContainer" runat="server" visible="false">
        <span class="payment-success-cheque-info-payment-text" id="spanChequeText" runat="server"></span>
        <span class="payment-success-cheque-address-text" id="spanChequeAddress" runat="server">
        </span>
    </div>
    <div id="divOtherManualPaymentDetailsContainer" runat="server" visible="false">
    </div>
</div>
