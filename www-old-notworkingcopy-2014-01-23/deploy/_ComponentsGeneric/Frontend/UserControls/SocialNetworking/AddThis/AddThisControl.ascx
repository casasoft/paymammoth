﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddThisControl.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.SocialNetworking.AddThis.AddThisControl" %>
<div class="addthis-core-container" runat="server" id="divAddthisContainer">
    <asp:literal runat="server" ID="ltlAddThisContent"></asp:literal>
    <script type="text/javascript">        var addthis_config = { "data_track_clickback": true };</script>
    <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=<%= this.GetAddThisPubId() %>"></script>
</div>
