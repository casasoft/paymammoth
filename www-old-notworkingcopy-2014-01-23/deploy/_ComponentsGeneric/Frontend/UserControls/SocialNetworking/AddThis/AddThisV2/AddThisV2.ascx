﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddThisV2.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.SocialNetworking.AddThis.AddThisV2.AddThisV2" %>
<div class="addthis-container">
    <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox" runat="server" id="divAddThis">
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=<%= this.GetAddThisPubId() %>"></script>
    <!-- AddThis Button END -->
</div>
