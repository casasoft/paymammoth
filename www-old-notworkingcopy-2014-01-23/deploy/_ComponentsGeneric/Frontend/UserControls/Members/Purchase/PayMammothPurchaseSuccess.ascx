﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayMammothPurchaseSuccess.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Purchase.PayMammothPurchaseSuccess" %>
<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="PaymentDetails" Src="~/_ComponentsGeneric/Frontend/UserControls/Orders/Information/PaymentDetails.ascx" %>
<CommonControls:ContentPage ID="contentPage" runat="server" />
<CommonControls:PaymentDetails runat="server" ID="paymentDetails" />
