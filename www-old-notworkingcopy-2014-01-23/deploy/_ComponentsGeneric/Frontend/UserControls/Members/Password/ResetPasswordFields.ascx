﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResetPasswordFields.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Password.ResetPasswordFields" %>
<div class="reset-password-fields-container">
    <CSControlsFormFields:FormFields ID="formResetPassword" runat="server"></CSControlsFormFields:FormFields>
</div>