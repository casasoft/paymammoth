﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreditStatementListing.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.CreditStatement.CreditStatementListing" %>
<%@ Register TagPrefix="CommonControls" TagName="PagingBar" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/Paging/PagingBar.ascx" %>
<div class="credit-statement-results-container">
    <CommonControls:PagingBar ID="pagingBarTop" runat="server" />
    <asp:PlaceHolder ID="phTableContainer" runat="server"></asp:PlaceHolder>
    <CommonControls:PagingBar ID="pagingBarBottom" runat="server" />
</div>
