﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoogleLoginButton.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login.SocialButtons.GoogleLoginButton" %>
<CSControls:MyButton CssClass="social-login-google social-button"  ID="btnGoogleLogin" runat="server"></CSControls:MyButton>
<CSControls:MyAnchor CssClass="social-login-google social-button"  ID="aGoogleLogin" runat="server"></CSControls:MyAnchor>

<asp:PlaceHolder ID="jsContainer" runat="server"></asp:PlaceHolder>