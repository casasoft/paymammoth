﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacebookLoginButton.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login.SocialButtons.FacebookLoginButton" %>
<CSControls:MyButton CssClass="social-login-facebook social-button"  ID="btnFacebookLogin" runat="server"></CSControls:MyButton>
<CSControls:MyAnchor CssClass="social-login-facebook social-button"  ID="aFacebookLogin" runat="server"></CSControls:MyAnchor>
<asp:PlaceHolder ID="jsContainer" runat="server"></asp:PlaceHolder>