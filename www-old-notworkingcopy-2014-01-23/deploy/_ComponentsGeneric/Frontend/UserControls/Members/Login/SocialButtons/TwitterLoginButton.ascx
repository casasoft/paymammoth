﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwitterLoginButton.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login.SocialButtons.TwitterLoginButton" %>

<CSControls:MyButton CssClass="social-login-twitter social-button"  ID="btnTwitterLogin" runat="server"></CSControls:MyButton>
<CSControls:MyAnchor CssClass="social-login-twitter social-button"  ID="aTwitterLogin" runat="server"></CSControls:MyAnchor>

<asp:PlaceHolder ID="jsContainer" runat="server"></asp:PlaceHolder>