﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoggedInWrapper.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login.LoggedInWrapper" %>
<%@ Register Src="/_ComponentsGeneric/Frontend/UserControls/Members/Login/LogoutButton.ascx" TagPrefix="Common" TagName="LogoutButton" %>

<div class="logged-in-wrapper clearfix" runat="server" id="divContainer">
    <div class="logged-in-welcome-message">
        <span runat="server" id="spanMessage"></span>
        <span runat="server" id="spanFullName" class="logged-in-name"></span>
    </div>
    <div class="logged-in-my-account">
        <a runat="server" id="aClientsArea"></a>
    </div>
    <Common:LogoutButton runat="server" ID="LogoutButton" />
</div>
