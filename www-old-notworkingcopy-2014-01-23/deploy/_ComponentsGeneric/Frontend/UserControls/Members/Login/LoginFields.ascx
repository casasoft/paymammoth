﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginFields.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login.LoginFields" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialLoginButtons.ascx" TagPrefix="uc1" TagName="SocialLoginButtons" %>

<div class="login-fields-container clearfix">
    <CSControlsFormFields:FormFields ID="formLogin" runat="server">
    </CSControlsFormFields:FormFields>
    <uc1:SocialLoginButtons runat="server" id="SocialLoginButtons" />
</div>
