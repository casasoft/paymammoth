﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SocialLoginButtons.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login.SocialLoginButtons" %>
<%@ Register TagPrefix="SocialLoginButtons" TagName="Facebook" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/FacebookLoginButton.ascx" %>
<%@ Register TagPrefix="SocialLoginButtons" TagName="Google" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/GoogleLoginButton.ascx" %>
<%@ Register TagPrefix="SocialLoginButtons" TagName="Twitter" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/TwitterLoginButton.ascx" %>
<div class="social-login-container html-container" id="divSocialLoginButtonsContainer" runat="server">
    <p runat="server" id="pSocialLoginMessage"></p>
    <div class="social-login-buttons-container clearfix">
        <SocialLoginButtons:Facebook ID="ucFacebookLoginButton" runat="server" />
        <SocialLoginButtons:Google ID="ucGoogleLoginButton" runat="server" />
        <SocialLoginButtons:Twitter ID="ucTwitterLoginButton" runat="server" />
    </div>
    <div class="social-remeber-me-container" runat="server" id="divSocialRememberMeContainer">
    </div>
</div>
