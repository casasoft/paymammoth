﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginPageContent.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login.LoginPageContent" %>
<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="LoginFields" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/LoginFields.ascx" %>
<CommonControls:ContentPage ID="contentPage" runat="server" />
<CommonControls:LoginFields ID="loginFields" runat="server" />
