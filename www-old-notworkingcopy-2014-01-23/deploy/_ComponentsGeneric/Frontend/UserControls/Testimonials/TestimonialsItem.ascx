﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestimonialsItem.ascx.cs" 
Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Testimonials.TestimonialsItem" %>
<div class="testimonials-item-container">
    <div class="testimonial-iten-content html-container" runat="server" id="divTestimonialContent">
        <div class="testimonial-item-text-content" id="divTestimonialContentText" runat="server"></div>
        <div class="testimonial-item-apostrophe-open" ></div>
        <div class="testimonial-item-apostrophe-close" ></div>
    </div>
    <div class="testimonial-item-footer clearfix">
        <div class="testimonial-item-user-container">
            <div class="testimonial-item-user-content clearfix">
                <span class="testimonial-item-user-name" runat="server" id="spanTestimonialUser"></span>
                <span class="testimonial-item-course" runat="server" id="spanTestimonialCourse"></span>
            </div>
            <div class="testimonial-item-course-icon" runat="server" id="divCourseIcon">
                <CSControls:MyImage runat="server" ID="imgIcon"></CSControls:MyImage>
            </div>
        </div>
    </div>
    <div class="testimonials-item-separator" runat="server" id="divSeparator"></div>
</div>