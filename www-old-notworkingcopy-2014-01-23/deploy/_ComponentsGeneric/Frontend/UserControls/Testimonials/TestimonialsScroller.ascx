﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestimonialsScroller.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Testimonials.TestimonialsScroller" %>
<%@ Register TagPrefix="CommonControls" TagName="CarouFredSelControl" Src="~/_ComponentsGeneric/Frontend/UserControls/Carousel/CarouFredSel/CarouFredSelControl.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="TestimonialsItems" Src="~/_ComponentsGeneric/Frontend/UserControls/Testimonials/TestimonialsItems.ascx" %>

<div class="testimonials-scroller-container">  
    <CommonControls:CarouFredSelControl runat="server" ID="testimonialsScroller" />
</div>

