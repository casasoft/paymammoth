﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentPage.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage.ContentPage" %>
<%@ Register TagPrefix="General" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Navigation"
    Assembly="WebComponentsGeneralV3" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPageFooter.ascx"
    TagName="ContentPageFooter" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPageMediaItems.ascx"
    TagName="ContentPageMediaItems" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/SocialNetworking/AddThis/AddThisControl.ascx"
    TagName="AddThisControl" TagPrefix="Control" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/SocialNetworking/AddThis/AddThisV2/AddThisV2.ascx"
    TagName="AddThisV2Control" TagPrefix="Control" %>
<article class="content-page-container" id="divContentPageContainer" runat="server">
    <div class="content-page-breadcrumbs" runat="server" id="divNavigationBreadcrumbs">
        <General:NavigationBreadcrumbs runat="server" ID="navigationBreadcrumbs" />
    </div>
    <div class="content-page-heading-wrapper clearfix">
        <div class="content-page-heading-content">
            <h1 runat="server" id="h1PageTitle">
            </h1>
            <span class="content-page-heading-subtitle" runat="server" id="spanArticleSubtitle">
            </span>
        </div>
        <div class="content-page-heading-add-this-wrapper" runat="server" id="divHeadingAddThisWrapper"
            visible="false">
            <Control:AddThisControl runat="server" ID="headingAddThis" />
        </div>
    </div>
    
    <div class="content-page-content-container">
        <Common:ContentPageMediaItems runat="server" ID="contentPageMediaItems" />
        <div class="content-page-extra-top-content" runat="server" id="divExtraTopContent"
            visible="false">
        </div>
        <div class="content-page-content clearfix" runat="server" id="divContentPageContent">
            <div class="content-page-image" runat="server" id="divContentPageImage">
                <CSControls:MyImage runat="server" ID="imgItem" />
            </div>
            <Control:AddThisV2Control runat="server" ID="addThisV2" Visible="false" />
            <div class="clearfix html-container content-page-content" id="divHtmlContent" runat="server">
            </div>
        </div>
        <div class="content-page-extra-bottom-content" runat="server" id="divExtraBottomContent"
            visible="false">
        </div>
    </div>
    <Common:ContentPageFooter runat="server" ID="contentPageFooter" />
</article>
