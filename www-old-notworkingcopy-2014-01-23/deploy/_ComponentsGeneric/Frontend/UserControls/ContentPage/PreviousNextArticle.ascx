﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PreviousNextArticle.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage.PreviousNextArticle" %>
<div class="previous-next-article-container clearfix">
    <table>
        <tr>
            <td class='previous-article-container'>
                <a runat="server" id="anchorPreviousArticle" class="previous-article-link"></a>
            </td>
            <td class='next-article-container'>
                <a runat="server" id="anchorNextArticle" class="next-article-link"></a>
            </td>
        </tr>
    </table>
</div>
