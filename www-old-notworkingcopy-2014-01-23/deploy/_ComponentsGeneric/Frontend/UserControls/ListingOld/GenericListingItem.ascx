﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericListingItem.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.GenericListingItem" %>
<article class="listing-item-wrapper">
    <div class="listing-item-container clearfix">
        <div class="listing-item-image">
            <CSControls:MyImage runat="server" ID="imgItem" />
        </div>
        <div class="listing-item-description-container">
            <a class="listing-item-description-title" runat="server" id="aItemTitle">
            </a>
                <div class="listing-item-description" runat="server" id="divItemDescription">
                </div>
            <div class="listing-item-view-more-container clearfix">
                <span runat="server" id="spanDate" class="item-date-created"></span>
                <a href="/" class="item-view-more" runat="server" id="anchorReadMore">read more&raquo;</a> 
                <div class="listing-item-freetext" runat="server" id="divFreeText" visible="false"></div>
            </div>
        </div>
    </div>
    <div class="listing-item-separator" runat="server" id="divItemSeparator"></div>
</article>
