﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListingResultsText.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.ListingResultsText" %>
<div class="listing-results-texts-container" id="divText" runat="server">
Showing [FROM] - [TO] of [TOTAL] [ITEMS]
</div>
