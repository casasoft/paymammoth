﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericListingItemsFullPage.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.GenericListingItemsFullPage" %>
<%@ Register TagPrefix="CommonControls" TagName="PagingBar" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/Paging/PagingBar.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ListingResultsText" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/ListingResultsText.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="GenericListingItems" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/GenericListingItems.ascx" %>
<div class="generic-listing-items-page-container">
    <h1 runat="server" id="hTitle" visible="false">
        Page Title</h1>
    <div runat="server" id="headingContainer">
    </div>
    <CommonControls:ContentPage runat="server" ID="contentPage" Visible="false" />
    <div class="listing-page-top-section-container clearfix" runat="server" id="divTopSection">
        <CommonControls:PagingBar runat="server" ID="pagingBarTop" />
    </div>
    <div class="html-container" visible="false" runat="server" id="divNoResultsText">
    </div>
    <CommonControls:GenericListingItems runat="server" ID="listingItems" />
    <div class="listing-page-bottom-section-container clearfix" runat="server" id="divBottomSection">
        <CommonControls:PagingBar runat="server" ID="pagingBarBottom" />
    </div>
</div>
