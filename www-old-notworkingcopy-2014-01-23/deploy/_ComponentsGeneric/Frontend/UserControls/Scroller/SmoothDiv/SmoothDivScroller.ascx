﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SmoothDivScroller.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Scroller.SmoothDiv.SmoothDivScroller" %>
<div id="divScroller" runat="server" class="smooth-div-scroller-container" style="position:relative;">
    <div class="scrollingHotSpotLeft">
    </div>
    <div class="scrollingHotSpotRight">
    </div>
    <div class="scrollWrapper">
        <ul class="scrollableArea" id="ulScrollableArea" runat="server">
            <img src="images/demo/field.jpg" alt="Demo image" />
            <img src="images/demo/gnome.jpg" alt="Demo image" />
            <img src="images/demo/pencils.jpg" alt="Demo image" />
            <img src="images/demo/golf.jpg" alt="Demo image" />
            <img src="images/demo/river.jpg" alt="Demo image" />
            <img src="images/demo/train.jpg" alt="Demo image" />
            <img src="images/demo/leaf.jpg" alt="Demo image" />
            <img src="images/demo/dog.jpg" alt="Demo image" />
        </ul>
    </div>
</div>