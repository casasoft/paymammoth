﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationMainMenu.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Navigation.NavigationMainMenu" %>
<%@ Register TagPrefix="Hierarchy" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Hierarchy" Assembly="WebComponentsGeneralV3" %>
<nav class="main-menu-navigation-wrapper">
    <Hierarchy:HierarchicalControlNavigation ID="NavigationHierarchyMenu" runat="server" />
</nav>


<asp:PlaceHolder ID="placeholderJs" runat="server"></asp:PlaceHolder>