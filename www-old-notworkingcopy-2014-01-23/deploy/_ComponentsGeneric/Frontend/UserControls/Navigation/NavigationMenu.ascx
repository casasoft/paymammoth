﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationMenu.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Navigation.NavigationMenu" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Navigation/NavigationMainMenu.ascx" TagPrefix="CommonControls" TagName="NavigationMainMenu" %>
<CommonControls:NavigationMainMenu runat="server" id="NavigationMainMenu" />

<script>
    $(document).ready(function() {
        jQuery('.main-menu-navigation-wrapper li[data-id=<asp:literal id='ltlContentPageID' runat='server'></asp:literal>]').addClass('selected');
    });
</script>