﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrettyPhotoImageGallery.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Gallery.PrettyPhotoImageGallery" %>
<div class="gallery-container">
    <span runat="server" id="spanGalleryTitle" class="gallery-title"></span>
    <div class="image-gallery-container clearfix" runat="server" id="divImageGalleryContainer">
    </div>
    <div class="gallery-container-text" runat="server" id="divGalleryText">
        <span runat="server" id="spanGalleryText" class="gallery-text"></span>
    </div>
</div>
