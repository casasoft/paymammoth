﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderHistory.ascx.cs" Inherits="CS.WebComponentsGeneralV1._ComponentsGeneric.Frontend.UserControls.Member.OrderHistory.OrderHistory" %>

<%@ Register Assembly="General_v3" Namespace="CS.General_v3.Controls.WebControls.Specialized.Paging"
    TagPrefix="Paging" %>
<%@ Register Src="~/Code/Controls/WebControls/Navigation/PagingBar/PagingBar.ascx" TagName="PagingBar" TagPrefix="Control" %>

<div class="order-history-wrapper">
    <div class="order-history-filter-selection">
        <div class="order-history-filter-selection-content">
            <Paging:Combos runat="server" ID="PagingCombos" />
            <div class="clear">
            </div>
        </div>
    </div>

    <CSControls:MyTable runat="server" ID="tableOrderHistory" CssClass="order-history-table">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell CssClass="first-item date-uploaded" Text="Date" runat="server" ID="thDate"></asp:TableHeaderCell>
            <asp:TableHeaderCell CssClass="order-ref" Text="Order Ref." runat="server" ID="thOrderRef"></asp:TableHeaderCell>
            <asp:TableHeaderCell CssClass="last-item status" Text="Status" runat="server" ID="thStatus"></asp:TableHeaderCell>
        </asp:TableHeaderRow>
    </CSControls:MyTable>
</div>

<div class="paging-panel-wrapper">
    <Control:PagingBar runat="server" ID="PagingBar">
    </Control:PagingBar>
</div>