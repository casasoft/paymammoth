﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReferAFriend.ascx.cs" Inherits="CS.WebComponentsGeneralV1._ComponentsGeneric.Frontend.UserControls.Member.Referrals.ReferAFriend" %>
<%@ Register Assembly="General_v3" Namespace="CS.WebComponentsGeneralV1.Code.Controls.WebControls.FormFieldsOld"
    TagPrefix="CSFormFields" %>

<div class="refer-a-friend">
    <CSFormFields:FormFields id="profileFields" runat="server" />
</div>
