﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileFieldsWrapper.ascx.cs" Inherits="CS.WebComponentsGeneralV1._ComponentsGeneric.Frontend.UserControls.Member.ProfileFields.ProfileFieldsWrapper" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Member/ProfileFields/ProfileFieldsPersonalDetails.ascx" TagName="ProfileFieldsPersonalDetails" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Member/ProfileFields/ProfileFieldsPasswordDetails.ascx" TagName="ProfileFieldsPasswordDetails" TagPrefix="Common" %>

<div class="personal-details-wrapper">
    <Common:ProfileFieldsPersonalDetails runat="server" ID="ProfileFieldsPersonalDetails" />
    <Common:ProfileFieldsPasswordDetails runat="server" ID="ProfileFieldsPasswordDetails" />

    <div class="button-wrapper" runat="server" id="divButtonWrapper">
        <CSControls:MyButton runat="server" ID="btnSubmit" Text="Proceed" />
    </div>
</div>
