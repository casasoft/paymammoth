﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaintenanceMessage.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls._Common.MaintenanceMessage" %>
<div class="maintenance-message-container-content clearfix">
    <div class="maintenance-message-close-button" runat="server" id="divCloseButton"></div>
    <div class="maintenance-message-content" id="divContent" runat="server">
    </div>
</div>
