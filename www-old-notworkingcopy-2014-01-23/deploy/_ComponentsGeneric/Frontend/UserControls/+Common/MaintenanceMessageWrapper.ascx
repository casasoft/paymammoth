﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaintenanceMessageWrapper.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls._Common.MaintenanceMessageWrapper" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/+Common/MaintenanceMessage.ascx" TagPrefix="Common" TagName="MaintenanceMessage" %>
<div class="maintenance-message-container">
    <Common:MaintenanceMessage runat="server" ID="MaintenanceMessage" />
</div>
<script>
    jQuery('.maintenance-message-close-button').click(function () {
        jQuery('.maintenance-message-container').slideUp('fast', function () {
            jQuery.get("/_ComponentsGeneric/Frontend/Handlers/AJAX/memberSetMaintenanceMessageIsHidden.ashx");
        });
    });
</script>
