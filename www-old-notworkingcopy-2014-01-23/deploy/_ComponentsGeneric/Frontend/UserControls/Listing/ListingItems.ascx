﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListingItems.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.ListingItems" %>
<%@ Register TagPrefix="CommonControls" TagName="PagingBar" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/Paging/PagingBar.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="PagingBarCombos" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/Paging/PagingBarCombos.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ListingResultsText" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/ListingResultsText.ascx" %>
<div class="listing-page-top-section-container clearfix" runat="server" id="divTopSection">
    <CommonControls:PagingBar runat="server" ID="pagingBarTop" />
</div>
<div class="html-container listing-page-results-section clearfix" id="divResults" runat="server">
  
</div>
<div class="listing-page-bottom-section-container clearfix" runat="server" id="divBottomSection">
    <CommonControls:PagingBar runat="server" ID="pagingBarBottom" />
</div>
