﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagingBar.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.Paging.PagingBar" %>
<%@ Register TagPrefix="CommonControls" TagName="ListingResultsText" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/ListingResultsText.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="PagingPages" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/Paging/PagingPages.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="PagingBarCombos" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/Paging/PagingBarCombos.ascx" %>
<div class="clearfix paging-bar-container">
    <CommonControls:ListingResultsText ID="resultsText" runat="server" />
    <CommonControls:PagingPages ID="pagingPages" runat="server" />
    
    <CommonControls:PagingBarCombos ID="pagingCombos" runat="server" />
</div>
