﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagingBarCombos.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.Paging.PagingBarCombos" %>
<%@ Register TagPrefix="CSControls" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common" %>
<div class="paging-bar-combos-container">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="paging-bar-combos-label-cell">
                <label for="cmbSortBy" id="lblSortBy" runat="server">
                    </label>
            </td>
            <td class="paging-bar-combos-dropdown-cell">
                <CSControls:MyDropdownList OnChangeRedirectToValue="true" id="cmbSortBy" runat="server" />
            </td>
            <td class="paging-bar-combos-label-cell">
                <label for="cmbViewAmt" id="lblViewAmt" runat="server">
                    </label>
            </td>
            <td class="paging-bar-combos-dropdown-cell">
                <CSControls:MyDropdownList OnChangeRedirectToValue="true" id="cmbViewAmt" runat="server" />
            </td>
        </tr>
    </table>
</div>
