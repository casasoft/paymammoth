﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClassifiedSubmitEnquiry.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Classifieds.ClassifiedSubmitEnquiry" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/FacebookLoginButton.ascx"
    TagPrefix="SocialLoginButtons" TagName="Facebook" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/GoogleLoginButton.ascx"
    TagPrefix="SocialLoginButtons" TagName="Google" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/TwitterLoginButton.ascx"
    TagPrefix="SocialLoginButtons" TagName="Twitter" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Login/PleaseLogin.ascx" TagPrefix="Common" TagName="PleaseLogin" %>

<div class="submit-enquiry-container">
    <div class="submit-enquiry-title-container">
        <h3 class="submit-enquiry-title" runat="server" id="spanPostEnquiryTitle">
            Post Comment</h3>
    </div>
    <Common:PleaseLogin runat="server" ID="PleaseLogin" />
    <div class="submit-enquiry-fields-container" id="divFields" runat="server">
        <div class="submit-enquiry-description-text" id="divEnquiryDescription" runat="server">
            <p>
                You can also send a direct only enquiry online by filling in the form below and the email will be delivered directly to the owner.</p>
        </div>
        <CSControlsFormFields:FormFields ID="submitEnquiryFields" runat="server">
        </CSControlsFormFields:FormFields>
    </div>
</div>