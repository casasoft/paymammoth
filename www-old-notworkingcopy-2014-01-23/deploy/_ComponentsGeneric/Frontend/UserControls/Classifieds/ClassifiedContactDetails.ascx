﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClassifiedContactDetails.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Classifieds.ClassifiedContactDetails" %>
<div class="classifieds-contact-details">
    <h2 runat="server" id="h2Title"></h2>
    <div class="classified-contact-details-table" runat="server" id="divTblWrapper"></div>
</div>