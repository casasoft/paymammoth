﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralOverlayLoginFields.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.GeneralOverlayLoginFields" %>
    
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/LoginFields.ascx"
    TagName="LoginFields" TagPrefix="CommonControls" %>

<asp:PlaceHolder ID="phLoginFieldsLoggedOut" runat="server">
    <div id="divLoginFieldsContainer" style="display: none;">
        <div class="login-ctrl-wrapper clearfix">
            <div class="login-credentials-content" runat="server" id="divLoginCredentialsContainer">
                <commoncontrols:loginfields runat="server" id="loginCtrl" />
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#divLoginFieldsContainer").appendTo('#divGenericLightboxOverlayContent');
        });
    </script>
</asp:PlaceHolder>
