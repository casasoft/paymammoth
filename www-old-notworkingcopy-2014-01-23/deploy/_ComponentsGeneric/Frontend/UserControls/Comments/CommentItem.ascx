﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommentItem.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Comments.CommentItem" %>
<%@ Register TagPrefix="CommonControls" TagName="SubmitComment" Src="~/_ComponentsGeneric/Frontend/UserControls/Comments/SubmitComment.ascx" %>
<article class="comment-item-container clearfix">
    <div class="comment-author-container">
        <span class="comment-item-author" runat="server" id="spanCommentAuthor"></span>
    </div>
    <div class="comment-date-container" id="divCommentDateUpper" runat="server">
        <datetime class="comment-item-date" runat="server" id="spanCommentDateUpper"></datetime>
    </div>
    <div class="comment-content">
        <p runat="server" id="pCommentContent">
        </p>
    </div>

    <div class="comment-reply-container">
        
    <a href="javascript:" class="comment-reply-button" id="aReply" runat="server">Reply</a>
    </div>
     <div class="comment-submit-reply-container" id="divSubmitReply" runat="server">
        <CommonControls:SubmitComment ID="submitReply" runat="server" />
    </div>
    
    <div class="comment-replies-container clearfix" id="divCommentReplies" runat="server">

    </div>

   
</article>
<asp:PlaceHolder ID="jsSubmitReply" runat="server"></asp:PlaceHolder>
