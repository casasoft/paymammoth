﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubmitComment.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Comments.SubmitComment" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/FacebookLoginButton.ascx"
    TagPrefix="SocialLoginButtons" TagName="Facebook" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/GoogleLoginButton.ascx"
    TagPrefix="SocialLoginButtons" TagName="Google" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/TwitterLoginButton.ascx"
    TagPrefix="SocialLoginButtons" TagName="Twitter" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Login/PleaseLogin.ascx" TagPrefix="Login" TagName="PleaseLogin" %>

<div class="submit-comment-container">
    <div class="submit-comment-title-container">
        <h3 class="submit-comment-title" runat="server" id="spanPostCommentTitle">
            Post Comment</h3>
    </div>
    <Login:PleaseLogin runat="server" id="PleaseLogin" />
    <div class="submit-comments-fields-container" id="divFields" runat="server">
        <CSControlsFormFields:FormFields ID="submitCommentFields" runat="server">
        </CSControlsFormFields:FormFields>
        <div class="forms-submit-note" runat="server" id="divSubmitNote"></div>         
    </div>
</div>
