﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartItem.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.ShoppingCartItem" %>
<tr runat="server" id="trShoppingCartItem">

    <td class="shopping-cart-item-delete" runat="server" id="tdDelete">
        <CSControls:MyButton runat="server" ID="btnDelete">
        </CSControls:MyButton>
    </td>
    <td class="shopping-cart-product">
        <div class="shopping-cart-item-container clearfix">
            <div class="shopping-cart-item-image">
                <CSControls:MyImage runat="server" ID="imgProduct">
                </CSControls:MyImage>
            </div>
            <div class="shopping-cart-item-info">
                <CSControls:MyAnchor runat="server" ID="aProductTitle" CssClass="shopping-cart-item-info-title">
                </CSControls:MyAnchor>
                <span runat="server" id="spanProductExtraInfo" class="shopping-cart-item-info-extra">
                </span>
            </div>
        </div>
    </td>
    <td runat="server" id="tdQuantity">
        <div runat="server" id="divQuantityContainer" class="shopping-cart-quantity">
            <CSControls:MyTxtBoxNumber runat="server" ID="txtQuantity">
            </CSControls:MyTxtBoxNumber>
        </div>
    </td>
    <td runat="server" id="tdUnitPrice" class="shopping-cart-unit-price">
        <div runat="server" id="div1" class="shopping-cart-item-unit-price-container">
            <span runat="server" id="spanUnitOldPrice" class="shopping-cart-item-unit-price-oldprice">
            </span><span runat="server" id="spanUnitPrice" class="shopping-cart-item-unit-price-retail-price">
            </span>
        </div>
    </td>
    <td runat="server" id="tdPrice" class="shopping-cart-total-price">
        <div runat="server" id="divUnitPriceContainer" class="shopping-cart-item-total-price-container">
            <span runat="server" id="spanOldPrice" class="shopping-cart-item-total-price-oldprice">
            </span><span runat="server" id="spanPrice" class="shopping-cart-item-total-price-retail-price">
            </span>
        </div>
    </td>
</tr>
