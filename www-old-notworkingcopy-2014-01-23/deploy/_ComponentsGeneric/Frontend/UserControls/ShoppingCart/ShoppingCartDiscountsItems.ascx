﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartDiscountsItems.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.ShoppingCartDiscountItems" %>
<div class="discounts-totals-container">
    <h2 runat="server" id="h2ApplicableDiscounts"></h2>
    <div class="discounts-total-text html-container" runat="server" id="divDiscountsText"></div>
    <ul runat="server" id="ulDiscounts">
    </ul>
</div>
