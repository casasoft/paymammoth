﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeliveryDetails.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.DeliveryDetails" %>
<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="DeliveryDetailsFields" Src="~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/DeliveryDetailsFields.ascx" %>
<CommonControls:ContentPage ID="contentPage" runat="server" />
<CommonControls:DeliveryDetailsFields ID="deliveryDetailsFields" runat="server" />

