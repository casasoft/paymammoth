﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsletterSubscriptionTableForm.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Newsletter.NewsletterSubscriptionTableForm" %>
<div class="newsletter-subscription-list-container" id="divContainer" runat="server">
    <h2 id="hTitle" runat="server">Stay up to date &amp; join our e-mail club</h2>
    <div class="html-container newsletter-subscription-list-html-content" id="divHtmlContent" runat="server"></div>
    <CSControlsFormFields:FormFields ID="formFields" runat="server"></CSControlsFormFields:FormFields>
</div>
