﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventDetails.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Events.EventDetails" %>
<div class="event-details-container clearfix">
    <div class="event-details-heading-container clearfix">
        <div class="event-details-heading">
            <h2 runat="server" id="h2EventDetailsTitle">
            </h2>
            <div class="event-details-text" runat="server" id="divEventDetailsText">
            </div>
        </div>
        <div class="event-details-icon-container" runat="server" id="divEventDetailsIcon">
            <CSControls:MyImage runat="server" ID="imgIcon">
            </CSControls:MyImage>
        </div>
    </div>
    <div class="event-details-content">
        <CSControls:MyTable runat="server" ID="tblEventDetails" class="event-details-table">
        </CSControls:MyTable>
    </div>
</div>
