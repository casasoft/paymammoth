﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NivoSlider.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Sliders.NivoSlider.v1.NivoSlider" %>
<asp:PlaceHolder ID="placeholderScripts" runat="server">
<script src="/_common/static/js/jQuery/plugins/nivoSlider/2.6/jquery.nivo.slider.js"
    type="text/javascript"></script>
<link href="/_common/static/js/jQuery/plugins/nivoSlider/2.6/nivo-slider.css"
    rel="stylesheet" type="text/css" />
</asp:PlaceHolder>
<div style="width:500px;height:500px" id="slider" class="nivoSlider" runat="server">
    <img src="images/slide1.jpg" alt="" />
    <img src="images/slide3.jpg" alt="" title="This is an example of a caption" />
    <img src="images/slide4.jpg" alt="" />
</div>
<div id="htmlcaption" class="nivo-html-caption">
    <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>.
</div>
