<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/Frontend/CMS/cms.master"
    AutoEventWireup="true" CodeBehind="listing.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.CMS.listingPage" %>

<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/CMS/Listing/ucPreListing.ascx" TagPrefix="CMSCommon"
    TagName="PreListing" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/CMS/Listing/ucPostListing.ascx" TagPrefix="CMSCommon"
    TagName="PostListing" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="cphMain" runat="server">
    <div class="search-panel-container">
        <a href="javascript:" id="aSearchPanelToggle" runat="server" class="button-search-panel-toggle">
            Search</a>
            
        <CSControlsFormFieldsNew:FormFields id="formSearch" runat="server">
        </CSControlsFormFieldsNew:FormFields>
    </div>
    <p id="pSearchDescription" runat="server" visible="false">
    </p>
    <p id="pUpdateNote" runat="server" visible="false" class="note update-msg">
        <strong>Note: If you update fields (textboxes, checkboxes) directly in the listing, it is IMPORTANT that you
            click on the SAVE button at the bottom or top of the page!</strong>
    </p>
    <p id="pDeletedMsg" runat="server" class="note deleted-items" visible="false">
        * Deleted items are shown slightly <span class="deleted-faded">faded</span>.
    </p>
    <asp:PlaceHolder ID="phListing" Visible="false" runat="server">
        <div class="listing-update">
            <CSControlsCommon:MyButton ID="btnListingUpdateTop" CssClass="listing-update" runat="server"
                Text="Save" />
        </div>
        <CSControlsListings:ListingWithPaging ID="listing" runat="server" />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phTree" Visible="false" runat="server">
        <CMS:CmsTreeStructure ID="cmsTreeStructure" runat="server" />
    </asp:PlaceHolder>
    <div class="listing-update">
        <CSControlsCommon:MyButton ID="btnListingUpdateBottom" CssClass="listing-update" runat="server"
            Text="Save" />
    </div>
</asp:Content>
