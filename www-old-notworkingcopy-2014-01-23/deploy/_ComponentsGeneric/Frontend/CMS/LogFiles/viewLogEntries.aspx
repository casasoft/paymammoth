﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/Frontend/CMS/cms.master"
    AutoEventWireup="true" CodeBehind="viewLogEntries.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.CMS.LogFiles.viewLogEntries" %>

<%@ Register Namespace="CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure"
    TagPrefix="CSTreeStructure" Assembly="General_v3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <div class="logbrowser">
        <h2>
            Search</h2>
            
        <CSControlsFormFieldsNew:FormFields ID="formSearch" runat="server" />
        <p>Please note that refreshing the browser page will not automatically refresh the log entries.  In order to refresh the listing, click on the button below.</p>
        <CSControls:MyButton ID="btnRefresh" runat="server"  Text="Refresh"/>
        <h2>
            Log Entries</h2>
        <CSControlsListings:ListingWithPaging CssClass="logbrowser" ID="listing" runat="server" />
    </div>
</asp:Content>
