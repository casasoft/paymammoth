﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/Frontend/CMS/cms.master" AutoEventWireup="true" CodeBehind="logBrowser.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.CMS.LogFiles.logBrowser" %>
<%@ Register Namespace="CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure"  TagPrefix="CSTreeStructure" Assembly="General_v3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
<CSSpecialized:Listing ID="listingLogs" runat="server" />
<CSControls:MyButton id="btnViewForAll" runat="server" Text="View for all checked" />
</asp:Content>
