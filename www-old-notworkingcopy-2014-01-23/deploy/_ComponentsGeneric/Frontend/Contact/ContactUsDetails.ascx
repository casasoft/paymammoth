﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUsDetails.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Contact.ContactUsDetails" %>
<div class="contact-details-wrapper">
    <h2 runat="server" id="h2ContactDetails">
        Contact Details</h2>
    <span class="contact-details-director-name" runat="server" id="spanDirector" visible="false"></span>
    <span class="contact-details-address" runat="server" id="spanAddress" visible="false"></span>
    <table>
        <tr id="trTel" runat="server">
            <td class="label">
                <asp:Literal runat="server" ID="lblTel">Tel:</asp:Literal>
            </td>
            <td class="field">
                <asp:Literal runat="server" ID="TelField"></asp:Literal>
            </td>
        </tr>
        <tr id="trFax" runat="server">
            <td class="label">
                <asp:Literal runat="server" ID="lblFax">Fax:</asp:Literal>
            </td>
            <td class="field">
                <asp:Literal runat="server" ID="FaxField"></asp:Literal>
            </td>
        </tr>
        <tr id="trEmail" runat="server">
            <td class="label">
                <asp:Literal runat="server" ID="lblEmail">Email:</asp:Literal>
            </td>
            <td class="field">
            
                <CSControlsCommon:MyEmail CssClass="contact-details-email" ID="EmailField" runat="server" />
            </td>
        </tr>
        <tr id="trWebsite" runat="server">
            <td class="label">
                <asp:Literal runat="server" ID="lblWebsite">Website:</asp:Literal>
            </td>
            <td class="field">
                <CSControlsCommon:MyAnchor CssClass="contact-details-website" runat="server" ID="WebsiteField" />
            </td>
        </tr>
        <tr id="trAddress" runat="server">
            <td class="label">
                <asp:Literal runat="server" ID="lblAddres">Address:</asp:Literal>
            </td>
            <td class="field">
                <asp:Literal runat="server" ID="AddressField"></asp:Literal>
            </td>
        </tr>
       <tr id="trMap" runat="server">
            <td class="label">
                <asp:Literal runat="server" ID="lblMap">Map:</asp:Literal>
            </td>
            <td class="field">
                <CSControlsCommon:MyAnchor runat="server" ID="anchorMap" />
            </td>
        </tr>
    </table>
</div>
