﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Attributes;
using BusinessLogic_v3.Modules.EmailTextModule.DefaultValues;
using PayMammoth.Modules.ContentTextModule;
using BusinessLogic_v3.Modules.ContentTextModule.DefaultValues;
using BusinessLogic_v3.Modules;

namespace PayMammoth
{
    public static class Enums
    {
        public enum RecurringProfileTransactionStatus
        {
            Success,
            Failure
        }

        public enum RedirectionResult
        {
            Success,
            Error
        }

        public enum RecurringProfileStatus
        {
            Pending,
            Active,
            /// <summary>
            /// Expired is when the recurring profile has reached its 'end of life', i.e example it was for 1 year and it hasnow elapsed
            /// </summary>
            Expired,
            /// <summary>
            /// Suspended is when the payment could not be taken, and profile is suspended
            /// </summary>
            Suspended,
            CouldNotCreate,
            /// <summary>
            /// Cancelled is when user manually cancels it
            /// </summary>
            Cancelled
        }

        

        public enum GenerateResponseStatus
        {
            HashVerificationFailed,
            GenericError,
            Ok,
            InvalidDetails,
            AccountDoesNotExist
        }

        public enum PaymentStatus
        {
            InvalidCardDetails,
            CommunicationError,
            Success,
            GenericError,
            Cancelled
        }

        public static CS.General_v3.Enums.STATUS_MSG_TYPE GetStatusTypeFromPaymentStatus(PaymentStatus status)
        {
            switch (status)
            {
                case PaymentStatus.Success: return CS.General_v3.Enums.STATUS_MSG_TYPE.Success;
                case PaymentStatus.Cancelled: return CS.General_v3.Enums.STATUS_MSG_TYPE.Information;
                default:
                    return CS.General_v3.Enums.STATUS_MSG_TYPE.Error;
            }
        }

        private static string GetMessageFromContentText(PaymentStatus status)
        {
            switch(status)
            {
                case PaymentStatus.InvalidCardDetails:
                    return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                            PayMammoth.Enums.CONTENT_TEXT.General_PaymentStatusMessage_InvalidCardDetails).GetContent();
                case PaymentStatus.CommunicationError:
                    return
                        Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                            PayMammoth.Enums.CONTENT_TEXT.General_PaymentStatusMessage_CommunicationError).GetContent();
                case PaymentStatus.GenericError:
                    return
                        Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                            PayMammoth.Enums.CONTENT_TEXT.General_PaymentStatusMessage_GenericError).GetContent();
                case PaymentStatus.Cancelled:
                    return
                        Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                            PayMammoth.Enums.CONTENT_TEXT.General_PaymentStatusMessage_Cancelled).GetContent();
                case PaymentStatus.Success:
                    return
                        Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                            PayMammoth.Enums.CONTENT_TEXT.General_PaymentStatusMessage_Success).GetContent();
                default:
                    throw new InvalidOperationException("Enum Value for " + status + " not mapped"); 
            }
        }

        public static string GetMessageFromPaymentStatus(PaymentStatus status)
        {
            return GetMessageFromContentText(status);
            //switch (status)
            //{
            //    case PaymentStatus.InvalidCardDetails: return "Invalid card details - Please try again";
            //    case PaymentStatus.CommunicationError: return "There was an error communicating with the server - Please try again.  If the problem persists, please contact website's administration";
            //    case PaymentStatus.GenericError: return "A error has occurred. Kindly contact website administration";
            //    case PaymentStatus.Cancelled: return "Payment was cancelled";
            //    case PaymentStatus.Success: return "Payment was successful";
            //    default:
            //        throw new InvalidOperationException("Enum Value for " + status + " not mapped");
            //}
        }

        //public const string SERVER_NVP_SANDBOX = "https://api-3t.sandbox.paypal.com/nvp";
        //public const string SERVER_NVP_LIVE = "https://api-3t.paypal.com/nvp";
        ////public const string SERVER_NVP_SANDBOX_CERTIFICATE = "https://api.sandbox.paypal.com/nvp";
        ////public const string SERVER_NVP_LIVE_CERTIFICATE = "https://api.paypal.com/nvp";
        //public const string SERVER_SANDBOX = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        //public const string SERVER_LIVE = "https://www.paypal.com/cgi-bin/webscr";

        public enum EmailIdentifierPayMammoth
        {
            [EmailTextDefaultValues(Subject="Payment successful",WhenIsThisEmailSent="This email is sent to administration whenever a successful payment has been done")]
            Administration_PaymentRequest_ImmediatePaymentSuccessful,

            [EmailTextDefaultValues(Subject = "Manual payment processed", WhenIsThisEmailSent = "This email is sent to administration whenever a payment has been done that requires manual intervention from the client, like cheques & bank transfers")]
            Administration_PaymentRequest_ManualPayment,

            [EmailTextDefaultValues(Subject = "Thank you for your payment", 
                WhenIsThisEmailSent = @"This email is sent to the user whenever a cheque payment has been done AND the flag 'Send notifications directly to clients' is switched on
in the website account")]
            User_PaymentRequest_ManualPayment_Cheque,


            [EmailTextDefaultValues(Subject = "Thank you for your payment",
                WhenIsThisEmailSent = @"This email is sent to the user whenever a manual payment that requires further actions from the client has been done AND the flag 'Send notifications directly to clients' is switched on
in the website account")]
            User_PaymentRequest_ManualPayment_Other,
            
            [EmailTextDefaultValues(Subject = "Thank you for your payment",
                WhenIsThisEmailSent = @"This email is sent to the user whenever an immediate payment (credit cards, paypal, etc) has been done, AND the flag 'Send notifications directly to clients' is switched on
in the website account")]
            User_PaymentRequest_ImmediatePaymentSuccessful


        }
        public static EmailIdentifierPayMammoth GetAdminEmailToSendBasedOnPaymentMethod(PayMammoth.Connector.Enums.PaymentMethodSpecific method)
        {
            switch (method)
            {
                case Connector.Enums.PaymentMethodSpecific.BankTransfer: 
                case Connector.Enums.PaymentMethodSpecific.Cheque: return EmailIdentifierPayMammoth.Administration_PaymentRequest_ManualPayment;
                case Connector.Enums.PaymentMethodSpecific.Neteller:
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Apco:
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Transactium:
                case Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout:
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Realex:
                case Connector.Enums.PaymentMethodSpecific.Moneybookers: return EmailIdentifierPayMammoth.Administration_PaymentRequest_ImmediatePaymentSuccessful;
            }
            throw new InvalidOperationException("Please link payment method to email sent");
        }

        public static EmailIdentifierPayMammoth GetUserEmailToSendBasedOnPaymentMethod(PayMammoth.Connector.Enums.PaymentMethodSpecific method)
        {
            switch (method)
            {
                case Connector.Enums.PaymentMethodSpecific.Cheque: return EmailIdentifierPayMammoth.User_PaymentRequest_ManualPayment_Cheque;
                case Connector.Enums.PaymentMethodSpecific.BankTransfer: return EmailIdentifierPayMammoth.User_PaymentRequest_ManualPayment_Other;
                case Connector.Enums.PaymentMethodSpecific.Neteller:
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Apco:
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Transactium:
                case Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout:
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Realex: 
                        
                case Connector.Enums.PaymentMethodSpecific.Moneybookers: return EmailIdentifierPayMammoth.User_PaymentRequest_ImmediatePaymentSuccessful;
            }
            throw new InvalidOperationException("Please link payment method to email sent");
        }

        public enum PAYMAMMOTH_SETTINGS
        {
            [SettingsInfo(DefaultValue = 300, Description = "This is the interval in seconds to wait before trying to charge payments")]
            BackgroundTasks_RecurringProfilePaymentsCheckerBackgroundTask_RepeatInterval,
            [SettingsInfo(DefaultValue = 15, Description = "This is the interval to wait before trying to send again notifications")]
            Payment_Notifier_SendEveryXSeconds,

            [SettingsInfo(DefaultValue = 50, Description = "The total number of notification messages to send in one batch")]
            Payment_Notifier_SendPerBatch,

            [SettingsInfo(DefaultValue = "65.0")]
            PayPal_v1_ApiVersion,
            [SettingsInfo(DefaultValue = "https://api-3t.sandbox.paypal.com/nvp")]
            PayPal_v1_Sandbox_NvpServerUrl,
            [SettingsInfo(DefaultValue = "https://www.sandbox.paypal.com/cgi-bin/webscr")]
            PayPal_v1_Sandbox_ApiServerUrl,
            [SettingsInfo(DefaultValue = "https://api-3t.paypal.com/nvp")]
            PayPal_v1_Live_NvpServerUrl,
            [SettingsInfo(DefaultValue = "https://www.paypal.com/cgi-bin/webscr")]
            PayPal_v1_Live_ApiServerUrl,

            //PayPal_v1_ConfirmWaitTimeout,
            [SettingsInfo(DefaultValue = "https://epage.payandshop.com/epage.cgi")]
            Realex_v1_ServerUrl,
            [SettingsInfo(DefaultValue = "https://psp.transactium.com/hpservices/site/js/startHPS.js")]
            Transactium_v1_JavascriptScriptUrl,
            [SettingsInfo(DefaultValue = 30000)]
            Other_Timeouts_MarkAsPaid,
            [SettingsInfo(DefaultValue = 30000)]
            Other_Timeouts_PaymentSuccessNotificationAcknowledge,
            [SettingsInfo(DefaultValue = 1.05, Description="This is used to set the maximum amount allowed for a recurring payment. Percentage should be in the form of 1.05 to signify five percent and NOT 5%!")]
            RecurringPayments_MaxAmountExtraPercentage,
            [SettingsInfo(DefaultValue = "8,24,48,96", Description="A comma seperated list of values, containing the intervals (in hours) to use when a recurring payment is failed")]
            RecurringPayments_PaymentFailedRetryIntervalList
            
            
        }

        public enum CONTENT_TEXT
        {
            Payment_Method_Cheque_PageTitle,
            Payment_Method_Realex_PageTitle,
            Payment_Method_Cheque_Instructions,
            Payment_Gateways_Relaex_RedirectText,
            Payment_Method_Cheque_PayableToText,
            General_Button_ConfirmAndProceed,
            General_StagingError,
            Payment_Details_PageTitle,
            Payment_Details_PaymentMethodSectionTitle,
            [ContentTextDefaultValues(ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html, Content = "Please choose your payment method from the list of available options below:")]
            Payment_Details_ChoosePaymentText,
            [ContentTextDefaultValues(ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html, Content = "You’re almost done. You will be taken to the respective payment provider depending on your above choice.")]
            Payment_Details_PreConfirmationPayMethodText,
            [ContentTextDefaultValues(ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText, Content = "Proceed")]
            General_Button_Proceed,
            Payment_Details_PaymentCancelText,
            [ContentTextDefaultValues(ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText, Content = "Your Details")]
            Payment_Details_YourDetailsSectionTitle,
            OrderSummary_SectionTitle,
            OrderSummary_DescriptionTitle,
            [ContentTextDefaultValues(ContentType= BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText, Content="View full order details")]
            OrderSummary_ViewFullOrderDetailsLinkText,
            [ContentTextDefaultValues(ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText, Content = "Net Total")]
            OrderSummaryDetails_OrderNetTotalLabel,
            [ContentTextDefaultValues(ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText, Content = "Handling")]
            OrderSummaryDetails_HandlingLabel,
            [ContentTextDefaultValues(ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText, Content = "Shipping")]
            OrderSummaryDetails_ShippingLabel,
            [ContentTextDefaultValues(ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText, Content = "Tax")]
            OrderSummaryDetails_TaxLabel,
            [ContentTextDefaultValues(ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText, Content = "Total")]
            OrderSummaryDetails_TotalLabel,
            PaymentMethod_CreditCard_SecurityInformation,
            Payment_Method_CreditCard_PageTitle,
            PaymentMethod_CreditCard_SecurityInformationSectionTitle,
            PaymentMethod_CreditCard_GoBackText,
            PaymentMethod_Realex_SuccessMessage,
            PaymentMethod_Realex_FailureMessage,
            General_PaymentStatusMessage_InvalidCardDetails,
            General_PaymentStatusMessage_CommunicationError,
            General_PaymentStatusMessage_GenericError,
            General_PaymentStatusMessage_Cancelled,
            General_PaymentStatusMessage_Success,
            PaymentMethod_Paypal_OrderDetailsText,

            Test_ContentText_ThisIsTheMsg,
            Payment_Method_PageTitle,
            PaymentMethod_Paypal_ConfirmOrderText,
            PaymentMethod_Paypal_ConfirmOrderTitleText
        }
    }
}
