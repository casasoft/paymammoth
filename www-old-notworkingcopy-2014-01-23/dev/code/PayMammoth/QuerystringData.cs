﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.PaymentRequestModule;
using System.Collections.Specialized;

namespace PayMammoth
{
    public static class QuerystringData
    {
        //public const string PARAM_REQUEST_GUID = "r";

        public static string GetPaymentRequestIdentifierFromQuerystring(NameValueCollection nv = null)
        {
            if (nv == null)
                nv = CS.General_v3.Util.PageUtil.GetRequestQueryString();
            return nv[PayMammoth.Connector.Constants.PARAM_IDENTIFIER];
        }

        public static IPaymentRequest GetPaymentRequestFromQuerystring(NameValueCollection nv = null)
        {

            string identifier = GetPaymentRequestIdentifierFromQuerystring(nv);
            return PaymentRequestFactory.Instance.GetRequestByIdentifier(identifier);

        }

        public static string FakePayments
        {
            get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring("enableFakePayments"); }
        }


    }
}
