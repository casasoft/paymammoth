using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;

namespace PayMammoth.Classes.Pages.CMS
{
    public abstract class BaseListingPage : CS.WebComponentsGeneralV3.Code.Cms.Pages.CmsBaseListingPage
    {
        public BaseListingPage(ICmsItemFactory cmsItemFactory)
            : base(cmsItemFactory)
        {

        }
    }
}
