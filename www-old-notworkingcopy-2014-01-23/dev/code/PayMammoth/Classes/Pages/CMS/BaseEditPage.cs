using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;

namespace PayMammoth.Classes.Pages.CMS
{
    public abstract class BaseEditPage : CS.WebComponentsGeneralV3.Code.Cms.Pages.CmsBaseEditPage
    {
        public BaseEditPage(ICmsItemFactory cmsItemFactory)
            : base(cmsItemFactory)
        {

        }
    }
}
