﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Frontend.PaymentRequestModule;
using PayMammoth.Util;
using PayMammoth.Classes.Interfaces;

namespace PayMammoth.Classes.Pages.Frontend
{
    public abstract class BasePaymentPage : BasePageUsingPagesMasterPage
    {

        public BasePaymentPage()
        {
            this.RedirectAndShowErrorIfAlreadyPaid = true;
            this.ShowTitleFromPaymentMethod = false;
            if (!CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                this.Functionality.RequiresSSL = CS.General_v3.Settings.GetSettingFromDatabase<bool>(CS.General_v3.Enums.SETTINGS_ENUM.OnlinePayment_PaymentPageUsesSSL);
            }
        }

        //private bool _currentPaymentLoaded = false;
        //private PaymentRequestFrontend _currentPayment = null;
        public bool RedirectAndShowErrorIfAlreadyPaid { get; set; }
        public bool ShowTitleFromPaymentMethod { get; set; }

        public PaymentRequestFrontend CurrentPaymentRequest
        {
            get
            {
                return FrontendUtil.GetCurrentPaymentRequest();
                /*if (!_currentPaymentLoaded)
                {
                    _currentPaymentLoaded = true;
                    var data= QuerystringData.GetPaymentRequestFromQuerystring();
                    _currentPayment = PaymentRequestFrontend.Get(data);
                }
                return _currentPayment; */
            }
        }
        public bool Success
        {
            get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<bool>(PayMammoth.Constants.PARAM_SUCCESS); }
        }



        protected void checkPayment()
        {
            bool ok = true;
            if (CurrentPaymentRequest == null )
            {
                this.SetStatusMsg("No request identifier defined in querystring", CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
                
                ok = false;

                
            }
            else if (RedirectAndShowErrorIfAlreadyPaid && CurrentPaymentRequest != null && CurrentPaymentRequest.Data.CheckIfPaid())
            {
                
                this.SetStatusMsg("Payment request has already been paid successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Warning);
                ok = false;
            }
            if (!ok)
            {
                CS.General_v3.Util.PageUtil.RedirectPage("/");
                
            }

        }
        protected override void OnPreInit(EventArgs e)
        {
            checkPayment();
            this.Functionality.UpdateTitle("TEST", false);
            base.OnPreInit(e);
        }
        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get { return null; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            SetPageTitleFromPaymentMethod();
            base.OnPreRender(e);
        }


        public void SetPageTitleFromPaymentMethod()
        {
            PaymentMethodInfoImpl paymentMethod = new PaymentMethodInfoImpl();
            var request = this.CurrentPaymentRequest;
            if (request != null)
            {
                if(request.Data.CurrentTransaction != null)
                {
                    paymentMethod.FillFromPaymentMethod(request.Data);
                    var pageTitleText = Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        PayMammoth.Enums.CONTENT_TEXT.Payment_Method_PageTitle).ToFrontendBase();
                    var tr = pageTitleText.Data.CreateTokenReplacer();
                    tr["payment_method_title"] = tr["[payment_method_title]"] = paymentMethod.Title;

                    string title = tr.ReplaceString(pageTitleText.Data.GetContent());

                    this.PageTitle = title;

                    this.Functionality.UpdateTitle(title, false);
                }
            }
        }
    }
}
