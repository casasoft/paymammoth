﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Classes.Pages.Frontend
{
    public class BasePageUsingMainMasterPage : BasePage
    {
        public new BaseMainMasterPage Master { get { return (BaseMainMasterPage)base.Master; } }
    }
}
