﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Classes.PaymentNotifications;

namespace PayMammoth.Classes.Interfaces
{
    public interface INotifiable
    {
        string Identifier { get; }
        WebsiteAccount WebsiteAccount { get; }
        int NotificationRetryCount { get; }
        PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE NotificationType { get; }
        void DisablePaymentNotifications();
        INotifyMsg CreateNotificationMessage();
    }
}
