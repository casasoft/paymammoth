﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Helpers;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules.RecurringProfileModule;

namespace PayMammoth.Classes.Interfaces
{
    public interface IRecurringProfileManager
    {
        SetupRecurringProfileResult SetupRecurringProfile(IRecurringProfile profile);
    }
}
