﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using PayMammoth.Modules.PaymentRequestModule;

namespace PayMammoth.Classes.Interfaces
{
    public interface IPaymentRedirector
    {
        Enums.RedirectionResult Redirect(IPaymentRequest paymentRequest);
    }
}
