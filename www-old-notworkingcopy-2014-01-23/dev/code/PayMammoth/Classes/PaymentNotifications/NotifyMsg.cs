﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using PayMammoth.Modules.PaymentRequestModule;
using CS.General_v3.Util;
using PayMammoth.Util;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Connector.Classes.Notifications;
namespace PayMammoth.Classes.PaymentNotifications
{
    public class NotifyMsg 
    {
        

        //***********************

        protected static readonly ILog _log = LogManager.GetLogger(typeof(NotifyMsg));
        
        public NotifyMsg(long notificationMessageToSendId)
        {
            this.NotificationMsgToSendId = notificationMessageToSendId;
            
        }

        public Connector.Enums.NOTIFICATION_RESPONSE_TYPE NotificationType { get; private set; }

        private void loadNotificationMsgToSend()
        {
            _notificationMsgToSend = PayMammoth.Modules.NotificationMessageModule.NotificationMessageFactory.Instance.GetByPrimaryKey(NotificationMsgToSendId);
        }

        private INotificationMessage _notificationMsgToSend { get; set; }
        public long NotificationMsgToSendId { get; set; }
        public bool IsFinished { get; set; }

        public event Action<NotifyMsg> OnReady;

        /// <summary>
        /// Sends the actual notification
        /// </summary>
        /// <returns>Whether it was acknowledged or not</returns>
        private bool sendNotificationMsg()
        {
            bool ok = false;
            string identifier = this._notificationMsgToSend.Identifier;
            CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass(this._notificationMsgToSend.SendToUrl);
            urlClass[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            urlClass[PayMammoth.Connector.Constants.PARAM_NOTIFICATION_TYPE] = CS.General_v3.Util.EnumUtils.StringValueOf(this._notificationMsgToSend.NotificationType);

            System.Collections.Specialized.NameValueCollection formdata = new System.Collections.Specialized.NameValueCollection();
            CS.General_v3.Util.ReflectionUtil.SerializeObjectToNameValueCollection<INotificationMsg>(formdata, (INotificationMsg)this._notificationMsgToSend);
            //formdata[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            //formdata[PayMammoth.Connector.Constants.PARAM_NOTIFICATION_TYPE] = CS.General_v3.Util.EnumUtils.StringValueOf(this._notificationMsgToSend.NotificationType);
            //formdata[PayMammoth.Connector.Constants.PARAM_STATUS_CODE] = CS.General_v3.Util.EnumUtils.StringValueOf(this._notificationMsgToSend.StatusCode);
            //formdata[PayMammoth.Connector.Constants.PARAM_MESSAGE] = this._notificationMsgToSend.Message;
            //formdata[PayMammoth.Connector.Constants.PARAM_UNIQUEID] = this._notificationMsgToSend.ID.ToString();
            
            
            
            

            string sUrl = urlClass.ToString();
            if (_log.IsDebugEnabled)
            {
                _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(identifier, string.Format("Notification #{0} for payment <{1}>| Sending to url: " + sUrl,
                                                                              this._notificationMsgToSend.RetryCount, identifier)));
            }
            string response = null;
            var notificationMsgService = BusinessLogic_v3.Util.InversionOfControlUtil.Get<INotificationMessageService>();
            try
            {

                response = CS.General_v3.Util.Web.HTTPPost(sUrl,formdata);
                notificationMsgService.AddToStatusLog(_notificationMsgToSend, CS.General_v3.Enums.STATUS_MSG_TYPE.Information, "Response received - " + response);
                
            }
            catch (Exception ex)
            {
                _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Error occured while trying to contact [" + sUrl + "] with notification of payment"), ex);
                notificationMsgService.AddToStatusLog(_notificationMsgToSend, CS.General_v3.Enums.STATUS_MSG_TYPE.Error,
                    "Error occured while trying to contact [" + sUrl + "] with notification of payment | Error Msg: " + ex.Message + " (" + ex.GetType().FullName + ")");
                response = null;
            }
            if (_log.IsDebugEnabled)
            {
                _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(identifier, string.Format("Notification #{0} for payment <{1}> | Response Received : '{2}'",
                                                                              this._notificationMsgToSend.RetryCount, identifier, response)));
                
            }

            bool concurrencyProb = false;

            BusinessLogic_v3.Util.nHibernateUtil.RepeatUntilNoStaleObjExceptionIsRaised(
                () => //action to repeat
                {
                    if (_notificationMsgToSend != null)
                    {
                        ok = requestAcknowledged(response);
                    }
                },
                ()=> //action to do on stale object exception
                    {
                        _notificationMsgToSend = PayMammoth.Modules.Factories.NotificationMessageFactory.GetByPrimaryKey(_notificationMsgToSend.ID);
                    });

           


            return ok;
        }

        

       

       
        /// <summary>
        /// Starts the request - Sends an amount of notifictions, with increasing intervals, until one is confirmed.
        /// </summary>
        private void startSendingRequest()
        {
            BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            // int waitInterval = 3000;
            loadNotificationMsgToSend();
            if (this._notificationMsgToSend != null && this._notificationMsgToSend.SendToUrl != null)
            {

                var identifier = this._notificationMsgToSend.Identifier;
                var retryCount = this._notificationMsgToSend.RetryCount;
                var url = this._notificationMsgToSend.SendToUrl;

                if (_log.IsInfoEnabled)
                {
                    _log.Info(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + " for payment <" + identifier + "> - Started"));
                }

                // System.Threading.Thread.Sleep(waitInterval); // this is to give a buffer to the other things to get ready.
                //bool markedAsPaid = waitUntilRequestConfirmedAsPaid();

                bool notified = false;

                if (_log.IsInfoEnabled)
                {
                    _log.Info(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> - OK, starting now"));
                }


                bool notificationStillNeedsToBeSent = this._notificationMsgToSend.CheckIfStillRequiresNotificationToBeSent();
                if (notificationStillNeedsToBeSent)
                {
                    if (!string.IsNullOrWhiteSpace(url))
                    {
                        if (_log.IsInfoEnabled) _log.Info(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> - Starting notification requests"));




                        notified = sendNotificationMsg();

                        if (!notified)
                        {
                            _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> - Failed to notify about payment"));
                        }

                    }
                    else
                    {
                        _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> not sent as it was not even marked as paid"));
                    }
                }
                else
                {
                    _log.Info(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> not sent as it was already sent"));

                }
            }
            this.Dispose();
            BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();

        }

        public void Start()
        {
            
            CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(startSendingRequest, "NotifyMsg-" + this.NotificationMsgToSendId);
            
        }

        #region IDisposable Members

        private void readyProcessing()
        {
            this.IsFinished = true;
            
            if (OnReady != null)
                OnReady(this);
        }

        public void Dispose()
        {
            readyProcessing();
        }

        #endregion

        //*******************
        protected bool requestAcknowledged(string response)
        {
            //var request = PayMammoth.Modules.Factories.PaymentRequestFactory.GetRequestByIdentifier(this.NotificationMsgToSend.Identifier);
            bool ok = false;
            var identifier = this._notificationMsgToSend.Identifier;
            var retryCount = this._notificationMsgToSend.RetryCount;

            lock (this._notificationMsgToSend.ItemLock)
            {
                if (!this._notificationMsgToSend.AcknowledgedByRecipient)
                {

                    if (System.String.Compare(response, PayMammoth.Connector.Constants.RESPONSE_OK, System.StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        //ok, response received
                        this._notificationMsgToSend.MarkAsAcknowledged(autoSave: false);
                        if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> - Notification marked as acknowledged"));
                        ok = true;
                        
                    }
                    else
                    {

                        this._notificationMsgToSend.IncrementRetryCount(autoSave: false);
                        if (_log.IsWarnEnabled) _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> - Notification NOT marked acknowledged"));
                    }
                }
            }
            
            return ok;
        }


    }
}
