﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Connector.Classes.Notifications;
using PayMammoth.Connector.Util;
using PayMammoth.Modules.WebsiteAccountModule;

namespace PayMammoth.Classes.PaymentNotifications
{
    public interface INotificationConfirmerService
    {
        bool ConfirmMessage(INotificationMsg msg);
    }

    [IocComponent]
    public class NotificationConfirmerService : INotificationConfirmerService
    {
        private readonly IWebsiteAccountFactory websiteAccountFactory = null;
        public NotificationConfirmerService(IWebsiteAccountFactory websiteAccountFactory)
        {
            this.websiteAccountFactory = websiteAccountFactory;
        }

        #region INotificationConfirmerService Members

        public bool ConfirmMessage(INotificationMsg msg)
        {
           
            bool ok = false;
            if (!string.IsNullOrWhiteSpace(msg.ConfirmationHash) && !string.IsNullOrWhiteSpace(msg.WebsiteAccountCode) && !string.IsNullOrWhiteSpace(msg.Identifier))
            {
                var websiteAccount = websiteAccountFactory.GetAccountByCode(msg.WebsiteAccountCode);
                if (websiteAccount != null)
                {
                    
                    string secretWord = websiteAccount.SecretWord;
                    string computedHash = HashUtil.GenerateConfirmHash(msg.Identifier, secretWord);
                    if (computedHash == msg.ConfirmationHash)
                    {
                        ok = true;
                    }

                }

            }
            return ok;
        }

        #endregion
    }
}
