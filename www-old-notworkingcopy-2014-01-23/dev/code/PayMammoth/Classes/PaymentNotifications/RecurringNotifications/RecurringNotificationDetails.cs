﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Classes.PaymentNotifications.RecurringNotifications
{
    public class RecurringNotificationDetails
    {
        public string RecurringPaymentId { get; set; }
        public Connector.Enums.RECURRING_PAYMENT_NOTIFICATION_STATUS PaymentStatus { get; set; }
        public Connector.Enums.PaymentMethodSpecific PaymentMethod { get; set; }
        public string ResponseContent { get; set; }
    }
}
