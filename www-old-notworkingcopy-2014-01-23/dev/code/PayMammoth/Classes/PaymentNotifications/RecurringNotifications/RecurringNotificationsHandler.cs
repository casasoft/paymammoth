﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.RecurringProfileTransactionModule;
using BusinessLogic_v3.Util;

namespace PayMammoth.Classes.PaymentNotifications.RecurringNotifications
{
    public class RecurringNotificationsHandler
    {
        public static RecurringNotificationsHandler Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<RecurringNotificationsHandler>(); } }
        private RecurringNotificationsHandler()
        {

        }

        public void ProcessRecurringNotification(RecurringNotificationDetails notification)
        {
            if (notification != null)
            {
                IRecurringProfile profile = Factories.RecurringProfileFactory.GetRecurringProfileByGatewayIdentifier(
                    notification.RecurringPaymentId, notification.PaymentMethod);
                if (profile != null)
                {
                    sendRecurringPaymentNotificationToWebsite(notification, profile);
                }
            }
        }

        private void sendRecurringPaymentNotificationToWebsite(RecurringNotificationDetails notification, IRecurringProfile profile)
        {
            createInDatabase(notification, profile);
        }

        private void createInDatabase(RecurringNotificationDetails notification, IRecurringProfile profile)
        {
            using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                IRecurringProfileTransaction recurringProfileTransaction =
                    Factories.RecurringProfileTransactionFactory.CreateNewItem();
                //recurringProfileTransaction.RecurringProfile = profile;
                recurringProfileTransaction.ResponseContent = notification.ResponseContent;
                //recurringProfileTransaction.NotificationEnabled = true;
                recurringProfileTransaction.Save();
                t.Commit();
            }
        }
    }
}
