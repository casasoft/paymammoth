﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Classes.PaymentNotifications
{
    public class NotifyMsgRecurringPayment : BaseNotifyMsg<NotifyMsgRecurringPayment>
    {
        public NotifyMsgRecurringPayment() : base(Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPayment) { }

        protected override bool requestAcknowledged(string response)
        {
            throw new NotImplementedException();
        }

        protected override bool waitUntilRequestConfirmedAsPaid()
        {
            throw new NotImplementedException();
        }

        protected override bool checkIfRequestStillRequiresNotificationToBeSent(out int retryCount)
        {
            throw new NotImplementedException();
        }
    }
}
