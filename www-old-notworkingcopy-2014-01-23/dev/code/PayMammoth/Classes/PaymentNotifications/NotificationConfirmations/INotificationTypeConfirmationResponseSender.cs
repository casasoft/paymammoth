﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PayMammoth.Connector.Classes.Notifications;
using PayMammoth.Connector.Classes.Responses;

namespace PayMammoth.Classes.PaymentNotifications.NotificationConfirmations
{
    public interface INotificationTypeConfirmationResponseSender
    {
        IConfirmationResponse GenerateResponse(INotificationMsg msg);
    }
}
