﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.PaymentNotifications.NotificationConfirmations.Confirmers;

namespace PayMammoth.Classes.PaymentNotifications.NotificationConfirmations
{
    public static class NotificationTypeConfirmationResponseSenderManager
    {
        public static INotificationTypeConfirmationResponseSender GetConfirmerForNotificationType(PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE notificationType)
        {

            
			

            switch (notificationType)
            {
                case Connector.Enums.NOTIFICATION_RESPONSE_TYPE.ImmediatePayment:
                    return ImmediatePaymentConfirmationResponseSender.Instance;
                case Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentFailure:
                case Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentSuccess:
                    //implement this;
                    return RecurringPaymentConfirmationResponseSender.Instance;
                case Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCancelled:
                case Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileExpired:
                case Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileSuspended:
                case Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileActivated:
                case Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCouldNotCreate:
                    return RecurringProfileUpdateConfirmationResponseSender.Instance;
                case Connector.Enums.NOTIFICATION_RESPONSE_TYPE.CancelRecurringProfile:
                    {
                        return CancelRecurringProfileNotificationProcessor.Instance;
                        
			
                        break;
                    }

                default:
                    throw new InvalidOperationException(string.Format("Not implemented for notification type <" + notificationType + ">"));
            }
        }
    }
}
