﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using CS.General_v3.URL;
using PayMammoth.Connector.Classes.Notifications;
using PayMammoth.Connector.Classes.Responses;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammoth.Classes.PaymentNotifications.NotificationConfirmations.Confirmers
{

    public interface IRecurringPaymentConfirmationResponseSender : INotificationTypeConfirmationResponseSender
    {

    }

    [IocComponent(typeof(IRecurringPaymentConfirmationResponseSender))]
    public class RecurringPaymentConfirmationResponseSender : IRecurringPaymentConfirmationResponseSender
    {

        
        public static IRecurringPaymentConfirmationResponseSender Instance
        {
            get { return BusinessLogic_v3.Util.InversionOfControlUtil.Get<IRecurringPaymentConfirmationResponseSender>(); }
        }






        #region INotificationTypeConfirmationResponseSender Members

        public IConfirmationResponse GenerateResponse(INotificationMsg msg)
        {


            RecurringPaymentConfirmationResponse resp = null;
            if (msg.NotificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentFailure ||
                msg.NotificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentSuccess)
            {
                resp=  new RecurringPaymentConfirmationResponse(msg.NotificationType.Value);

                var request = PayMammoth.Modules.Factories.RecurringProfilePaymentFactory.GetPaymentByIdentifier(msg.Identifier);
                if (request != null)
                {
                    var service = BusinessLogic_v3.Util.InversionOfControlUtil.Get<IRecurringProfilePaymentService>();
                    service.FillConfirmResponse(request, resp);
                }
            }
            return resp;
        }

        #endregion
    }
}
