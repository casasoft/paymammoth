﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.URL;
using PayMammoth.Connector.Classes.Notifications;
using PayMammoth.Connector.Classes.Responses;
using PayMammoth.Modules.PaymentRequestModule;

namespace PayMammoth.Classes.PaymentNotifications.NotificationConfirmations.Confirmers
{
    public class ImmediatePaymentConfirmationResponseSender : INotificationTypeConfirmationResponseSender
    {
        
        private static readonly ImmediatePaymentConfirmationResponseSender _instance = new ImmediatePaymentConfirmationResponseSender();

        public static ImmediatePaymentConfirmationResponseSender Instance
        {
            get { return _instance; }
        }




        //#region INotificationTypeConfirmer Members

        //public bool ConfirmNotification(Connector.Classes.Notifications.NotificationMsg msg)
        //{
        //    bool ok = false;
        //    PaymentRequest request = PaymentRequest.Factory.GetRequestByIdentifier(msg.Identifier);
        //    if (request != null)
        //    {
        //        string computedHash = PayMammoth.Connector.Util.HashUtil.GenerateConfirmHash(msg.Identifier, request.WebsiteAccount.SecretWord);
        //        if (string.Compare(computedHash, msg.ConfirmationHash, true) == 0)
        //        {
        //            ok = true;
                    
        //        }

        //    }
        //    return ok;
            
        //}

        //#endregion

        #region INotificationTypeConfirmationResponseSender Members

        public IConfirmationResponse GenerateResponse(INotificationMsg msg)
        {
            

            ImmediatePaymentConfirmationResponse resp = new ImmediatePaymentConfirmationResponse();
            if (msg.NotificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.ImmediatePayment)
            {
                PaymentRequest request = PaymentRequest.Factory.GetRequestByIdentifier(msg.Identifier);
                if (request != null)
                {

                    var successfulTransaction = request.GetSuccessfulTransaction();
                    if (successfulTransaction != null && successfulTransaction.Successful)
                    {
                        resp.ConfirmationSuccess = true;
                        successfulTransaction.FillConfirmResponse(resp);

                    }
                }
            }
            return resp;
        }

        #endregion
    }
}
