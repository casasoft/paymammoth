﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using CS.General_v3.URL;
using PayMammoth.Connector.Classes.Notifications;
using PayMammoth.Connector.Classes.Responses;
using PayMammoth.Connector.Util;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammoth.Classes.PaymentNotifications.NotificationConfirmations.Confirmers
{

    public interface IRecurringProfileUpdateConfirmationResponseSender : INotificationTypeConfirmationResponseSender
    {

    }

    [IocComponent(typeof(IRecurringProfileUpdateConfirmationResponseSender))]
    public class RecurringProfileUpdateConfirmationResponseSender : IRecurringProfileUpdateConfirmationResponseSender
    {


        public static IRecurringProfileUpdateConfirmationResponseSender Instance
        {
            get { return BusinessLogic_v3.Util.InversionOfControlUtil.Get<IRecurringProfileUpdateConfirmationResponseSender>(); }
        }




        public static bool ConfirmMatchingProfileStatusToNotificationType(Enums.RecurringProfileStatus profileStatus, Connector.Enums.NOTIFICATION_RESPONSE_TYPE notificationType)
        {
            if ((profileStatus == Enums.RecurringProfileStatus.Suspended && notificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileSuspended) ||
                (profileStatus == Enums.RecurringProfileStatus.Cancelled && notificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCancelled) ||
                (profileStatus == Enums.RecurringProfileStatus.CouldNotCreate && notificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCouldNotCreate) ||
                (profileStatus == Enums.RecurringProfileStatus.Active && notificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileActivated) ||
                (profileStatus == Enums.RecurringProfileStatus.Expired && notificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileExpired))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public IConfirmationResponse GenerateResponse(INotificationMsg msg)
        {

            

            RecurringProfileUpdateConfirmationResponse resp = null;
            if (msg.NotificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileSuspended ||
                msg.NotificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileExpired ||
                msg.NotificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCancelled ||
                msg.NotificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileActivated ||
                msg.NotificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCouldNotCreate)
            {
                resp=  new RecurringProfileUpdateConfirmationResponse(msg.NotificationType.Value);

                var profile = PayMammoth.Modules.Factories.RecurringProfileFactory.GetRecurringProfileByPayMammothIdentifier(msg.Identifier);
                if (profile != null)
                {
                    if (ConfirmMatchingProfileStatusToNotificationType(profile.Status, msg.NotificationType.Value))
                    {
                        resp.ProfileIdentifier = profile.Identifier;
                        resp.ConfirmationSuccess = true;
                        resp.ExternalGatewayReference = profile.ProfileIdentifierOnGateway;
                        resp.Hash = HashUtil.GenerateConfirmHash(resp.ProfileIdentifier, profile.WebsiteAccount.SecretWord);
                    }

                }
            }
            return resp;
        }

       
    }
}
