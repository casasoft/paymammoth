﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using CS.General_v3.URL;
using PayMammoth.Connector.Classes.Notifications;
using PayMammoth.Connector.Classes.Responses;
using PayMammoth.Connector.Util;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammoth.Classes.PaymentNotifications.NotificationConfirmations.Confirmers
{

    public interface ICancelRecurringProfileNotificationProcessor : INotificationTypeConfirmationResponseSender
    {

    }

    [IocComponent(typeof(ICancelRecurringProfileNotificationProcessor))]
    public class CancelRecurringProfileNotificationProcessor : ICancelRecurringProfileNotificationProcessor
    {


        public static ICancelRecurringProfileNotificationProcessor Instance
        {
            get { return BusinessLogic_v3.Util.InversionOfControlUtil.Get<ICancelRecurringProfileNotificationProcessor>(); }
        }

        private readonly IRecurringProfileService recurringProfileServices;
        private readonly IRecurringProfileFactory recurringProfileFactory;
        public CancelRecurringProfileNotificationProcessor(IRecurringProfileService recurringProfileServices,
            IRecurringProfileFactory recurringProfileFactory)
        {
            this.recurringProfileServices = recurringProfileServices;
            this.recurringProfileFactory = recurringProfileFactory;
        }


        public static bool ConfirmMatchingProfileStatusToNotificationType(Enums.RecurringProfileStatus profileStatus, Connector.Enums.NOTIFICATION_RESPONSE_TYPE notificationType)
        {
            if ((profileStatus == Enums.RecurringProfileStatus.Suspended && notificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileSuspended) ||
                (profileStatus == Enums.RecurringProfileStatus.Cancelled && notificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCancelled) ||
                (profileStatus == Enums.RecurringProfileStatus.CouldNotCreate && notificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCouldNotCreate) ||
                (profileStatus == Enums.RecurringProfileStatus.Active && notificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileActivated) ||
                (profileStatus == Enums.RecurringProfileStatus.Expired && notificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileExpired))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public IConfirmationResponse GenerateResponse(INotificationMsg msg)
        {


            

            CancelRecurringProfileConfirmationResponse resp = null;
            if (msg.NotificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.CancelRecurringProfile)
            {
                resp = new CancelRecurringProfileConfirmationResponse(msg.NotificationType.Value);

                var profile = recurringProfileFactory.GetRecurringProfileByPayMammothIdentifier(msg.Identifier);
                if (profile != null)
                {
                    if (profile.Status == Enums.RecurringProfileStatus.Active)
                    {
                        recurringProfileServices.MarkProfileAsCancelled(profile, "");
                        
                        resp.ConfirmationSuccess = true;
                        resp.Message = "Profile cancelled successfully";
                    }
                    else
                    {
                        resp.ConfirmationSuccess = false;
                        resp.Message = "Could not cancel profile as status is not active";
                    }


                }
            }
            else
            {
                throw new InvalidOperationException(string.Format("This processor is not intended for this type of message <{0}>, but only for 'CancelRecurringProfile'", msg.NotificationType));
            }
            return resp;
        }

       
    }
}
