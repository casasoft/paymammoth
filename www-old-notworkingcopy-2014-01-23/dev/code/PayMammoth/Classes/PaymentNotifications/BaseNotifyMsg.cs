﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using PayMammoth.Util;

namespace PayMammoth.Classes.PaymentNotifications
{
    public abstract class BaseNotifyMsg<T> : INotifyMsg
    {
        public Connector.Enums.NOTIFICATION_RESPONSE_TYPE NotificationType { get; private set; }

        public BaseNotifyMsg(Connector.Enums.NOTIFICATION_RESPONSE_TYPE notificationType)
        {
            this.NotificationType = notificationType;
        }

        protected static readonly ILog _log = LogManager.GetLogger(typeof(T));
        public string Identifier { get; set; }
        public string Url { get; set; }
        public bool Finished { get; set; }
        public bool Success { get; set; }

        public int RetryCount { get; set; }
        public event Action<INotifyMsg> OnReady;

        /// <summary>
        /// Sends the actual notification
        /// </summary>
        /// <returns>Whether it was acknowledged or not</returns>
        private bool sendNotificationMsg()
        {
            bool ok = false;
            CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass(this.Url);
            urlClass[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = this.Identifier;

            string sUrl = urlClass.ToString();
            if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> (Try #" + this.RetryCount + ") - Sending to url: " + sUrl));
            string response = null;
            try
            {

                response = CS.General_v3.Util.Web.HTTPGet(sUrl);
            }
            catch (Exception ex)
            {
                _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Error occured while trying to contact [" + sUrl + "] with notification of payment"), ex);

                response = null;
            }
            if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> - Response received: '" + response + "'"));

            bool concurrencyProb = false;
            do
            {
                concurrencyProb = false;
                
                var session = BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
                try
                {
                    ok = requestAcknowledged(response);
                }
                catch (NHibernate.StaleStateException ex)
                {
                    concurrencyProb = true;
                    session.RollbackCurrentTransaction();
                    //                    session.Clear(); //dont save any changes,
                    //this happens if another thread updated this notification msg
                }

                BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
            } while (concurrencyProb);

            return ok;
        }

        protected abstract bool requestAcknowledged(string response);

        /// <summary>
        /// This ensures that the request is saved and confirmed as paid first, before trying to send a notification
        /// </summary>
        /// <returns></returns>
        protected abstract bool waitUntilRequestConfirmedAsPaid();

        /// <summary>
        /// This checks whether the request still requires the notification to be sent.
        /// </summary>
        /// <returns></returns>
        protected abstract bool checkIfRequestStillRequiresNotificationToBeSent(out int retryCount);

        /// <summary>
        /// Starts the request - Sends an amount of notifictions, with increasing intervals, until one is confirmed.
        /// </summary>
        private void _start()
        {
            // int waitInterval = 3000;

            if (_log.IsInfoEnabled) _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + " for payment <" + this.Identifier + "> - Started"));

            // System.Threading.Thread.Sleep(waitInterval); // this is to give a buffer to the other things to get ready.
            bool markedAsPaid = waitUntilRequestConfirmedAsPaid();
            bool notified = false;
            if (_log.IsInfoEnabled) _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> - OK, starting now"));
            int currRetryCount = 0;
            bool notificationStillNeedsToBeSent = checkIfRequestStillRequiresNotificationToBeSent(out currRetryCount);
            if (notificationStillNeedsToBeSent)
            {
                if (!string.IsNullOrWhiteSpace(this.Url) && markedAsPaid)
                {
                    if (_log.IsInfoEnabled) _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> - Starting notification requests"));
                    var retryCounts = Constants.GetPaymentNotificationsRetryCounts(); //gets the list of notification retry intervals
                    notified = false;

                    notified = sendNotificationMsg();

                    if (!notified)
                    {
                        _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> - Failed to notify about payment"));
                    }

                }
                else
                {
                    _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> not sent as it was not even marked as paid"));
                }
            }
            else
            {
                _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> not sent as it was already sent"));

            }
            this.Dispose();



        }

        public void Start()
        {
            if (Url != null)
            {
                CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(_start, "NotifyMsg-" + this.Identifier);
            }
        }

        #region IDisposable Members

        private void readyProcessing()
        {
            this.Finished = true;
            if (OnReady != null)
                OnReady(this);
        }

        public void Dispose()
        {
            readyProcessing();
        }

        #endregion
    }
}
