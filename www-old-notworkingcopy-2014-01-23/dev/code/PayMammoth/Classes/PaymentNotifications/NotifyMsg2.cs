﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using PayMammoth.Modules.PaymentRequestModule;
using CS.General_v3.Util;
using PayMammoth.Util;
using PayMammoth.Modules.NotificationMessageModule;
namespace PayMammoth.Classes.PaymentNotifications
{
    public class NotifyMsg2 
    {
        

        //***********************

        protected static readonly ILog _log = LogManager.GetLogger(typeof(NotifyMsg2));
        
        public NotifyMsg2(INotificationMessage notificationMessageToSend)
        {
            this.NotificationMsgToSend = notificationMessageToSend;
            
        }

        public Connector.Enums.NOTIFICATION_RESPONSE_TYPE NotificationType { get; private set; }

        public INotificationMessage NotificationMsgToSend { get; set; }
        public bool IsFinished { get; set; }

        public event Action<NotifyMsg2> OnReady;

        /// <summary>
        /// Sends the actual notification
        /// </summary>
        /// <returns>Whether it was acknowledged or not</returns>
        private bool sendNotificationMsg()
        {
            bool ok = false;
            string identifier = this.NotificationMsgToSend.Identifier;
            CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass(this.NotificationMsgToSend.SendToUrl);
            urlClass[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            urlClass[PayMammoth.Connector.Constants.PARAM_STATUS_CODE] = CS.General_v3.Util.EnumUtils.StringValueOf(this.NotificationMsgToSend.StatusCode);
            urlClass[PayMammoth.Connector.Constants.PARAM_NOTIFICATION_TYPE] = CS.General_v3.Util.EnumUtils.StringValueOf(this.NotificationMsgToSend.NotificationType);
            urlClass[PayMammoth.Connector.Constants.PARAM_MESSAGE] = this.NotificationMsgToSend.Message;
            

            string sUrl = urlClass.ToString();
            if (_log.IsDebugEnabled)
            {
                _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(identifier, string.Format("Notification #{0} for payment <{1}>| Sending to url: " + sUrl,
                                                                              this.NotificationMsgToSend.RetryCount, identifier)));
            }
            string response = null;
            try
            {

                response = CS.General_v3.Util.Web.HTTPGet(sUrl);
            }
            catch (Exception ex)
            {
                _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Error occured while trying to contact [" + sUrl + "] with notification of payment"), ex);

                response = null;
            }
            if (_log.IsDebugEnabled)
            {
                _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(identifier, string.Format("Notification #{0} for payment <{1}> | Response Received : '{2}'",
                                                                              this.NotificationMsgToSend.RetryCount, identifier, response)));
                
            }

            bool concurrencyProb = false;
            do
            {
                concurrencyProb = false;
                
                var session = BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
                try
                {
                    ok = requestAcknowledged(response);
                }
                catch (NHibernate.StaleStateException ex)
                {
                    concurrencyProb = true;
                    session.RollbackCurrentTransaction();
                    //                    session.Clear(); //dont save any changes,
                    //this happens if another thread updated this notification msg
                }

                BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
            } while (concurrencyProb);

            return ok;
        }

        

       

       
        /// <summary>
        /// Starts the request - Sends an amount of notifictions, with increasing intervals, until one is confirmed.
        /// </summary>
        private void startSendingRequest()
        {
            // int waitInterval = 3000;
            var identifier = this.NotificationMsgToSend.Identifier;
            var retryCount = this.NotificationMsgToSend.RetryCount;
            var url = this.NotificationMsgToSend.SendToUrl;

            if (_log.IsInfoEnabled)
            {
                _log.Info(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + " for payment <" + identifier + "> - Started"));
            }

            // System.Threading.Thread.Sleep(waitInterval); // this is to give a buffer to the other things to get ready.
            //bool markedAsPaid = waitUntilRequestConfirmedAsPaid();

            bool notified = false;

            if (_log.IsInfoEnabled)
            {
                _log.Info(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> - OK, starting now"));
            }

            
            bool notificationStillNeedsToBeSent = this.NotificationMsgToSend.CheckIfStillRequiresNotificationToBeSent(); 
            if (notificationStillNeedsToBeSent)
            {
                if (!string.IsNullOrWhiteSpace(url))
                {
                    if (_log.IsInfoEnabled) _log.Info(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> - Starting notification requests"));

                   
                    

                    notified = sendNotificationMsg();

                    if (!notified)
                    {
                        _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> - Failed to notify about payment"));
                    }

                }
                else
                {
                    _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> not sent as it was not even marked as paid"));
                }
            }
            else
            {
                _log.Info(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> not sent as it was already sent"));

            }
            this.Dispose();



        }

        public void Start()
        {
            if (this.NotificationMsgToSend.SendToUrl != null)
            {
                CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(startSendingRequest, "NotifyMsg-" + this.NotificationMsgToSend.ID);
            }
        }

        #region IDisposable Members

        private void readyProcessing()
        {
            this.IsFinished = true;
            
            if (OnReady != null)
                OnReady(this);
        }

        public void Dispose()
        {
            readyProcessing();
        }

        #endregion

        //*******************
        protected bool requestAcknowledged(string response)
        {
            //var request = PayMammoth.Modules.Factories.PaymentRequestFactory.GetRequestByIdentifier(this.NotificationMsgToSend.Identifier);
            bool ok = false;
            var identifier = this.NotificationMsgToSend.Identifier;
            var retryCount = this.NotificationMsgToSend.RetryCount;
            lock (this.NotificationMsgToSend.ItemLock)
            {
                if (!this.NotificationMsgToSend.AcknowledgedByRecipient)
                {

                    if (System.String.Compare(response, PayMammoth.Connector.Constants.RESPONSE_OK, System.StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        //ok, response received
                        this.NotificationMsgToSend.MarkAsAcknowledged();
                        if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> - Notification marked as acknowledged"));
                        ok = true;
                        
                    }
                    else
                    {

                        this.NotificationMsgToSend.IncrementRetryCount();
                        if (_log.IsWarnEnabled) _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(identifier, "Notification #" + retryCount + "for payment <" + identifier + "> - Notification NOT marked acknowledged"));
                    }
                }
            }
            
            return ok;
        }


    }
}
