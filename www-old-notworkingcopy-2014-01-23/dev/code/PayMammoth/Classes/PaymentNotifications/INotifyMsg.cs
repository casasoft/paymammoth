﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Classes.PaymentNotifications
{
    public interface INotifyMsg : IDisposable
    {
        Connector.Enums.NOTIFICATION_RESPONSE_TYPE NotificationType { get; }
        void Start();
        bool Finished { get; set; }
        bool Success { get; set; }
        string Identifier { get; set; }
        string Url { get; set; }
        int RetryCount { get; set; }
        event Action<INotifyMsg> OnReady;
    }
}
