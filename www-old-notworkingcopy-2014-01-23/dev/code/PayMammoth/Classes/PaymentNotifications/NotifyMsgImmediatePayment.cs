﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using PayMammoth.Modules.PaymentRequestModule;
using CS.General_v3.Util;
using PayMammoth.Util;
namespace PayMammoth.Classes.PaymentNotifications
{
    public class NotifyMsgImmediatePayment : BaseNotifyMsg<NotifyMsgImmediatePayment>
    {
        public NotifyMsgImmediatePayment() : base(Connector.Enums.NOTIFICATION_RESPONSE_TYPE.ImmediatePayment) { }



        protected override bool requestAcknowledged(string response)
        {
            var request = PayMammoth.Modules.Factories.PaymentRequestFactory.GetRequestByIdentifier(this.Identifier);
            bool ok = false;
            if (request != null)
            {
                lock (request.ItemLock)
                {
                    if (!request.IsPaymentNotificationAcknowledge())
                    {

                        if (string.Compare(response, PayMammoth.Connector.Constants.RESPONSE_OK, true) == 0)
                        {
                            //ok, response received
                            request.MarkAsNotificationResponseAcknowledged();
                            if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> - Notification marked as acknowledged"));
                            ok = true;
                            this.Success = true;
                        }
                        else
                        {
                            request.IncrementPaymentNotificationRetryCount();
                            if (_log.IsWarnEnabled) _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> - Notification NOT marked acknowledged"));
                        }
                    }
                }
            }
            return ok;
        }

        protected override bool waitUntilRequestConfirmedAsPaid()
        {
            bool ok = false;
            IPaymentRequest request = null;
            BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            request = PayMammoth.Modules.Factories.PaymentRequestFactory.GetRequestByIdentifier(this.Identifier);
            ok = request.WaitUntilRequestIsMarkedAsPaid();
            BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();

            if (_log.IsInfoEnabled)
            {
                if (ok)
                    _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Request <" + this.Identifier + "> - Paid OK"));
                else
                    _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Request <" + this.Identifier + "> - Paid NOT ok"));

            }
            return ok;
        }

        protected override bool checkIfRequestStillRequiresNotificationToBeSent(out int retryCount)
        {
            retryCount = 0;
            bool toSend = false;

            BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            var request = PayMammoth.Modules.Factories.PaymentRequestFactory.GetRequestByIdentifier(this.Identifier);
            toSend = request.CheckIfPaymentNotificationStillNeedsToBeSent();
            if (toSend)
                retryCount = request.PaymentNotificationRetryCount;
            BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
            return toSend;
        }
    }
}
