﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Classes.PaymentNotifications;
using log4net;
using System.Threading;
using BusinessLogic_v3.Modules.SettingModule;
using System.Collections;
using PayMammoth.Modules.RecurringProfileTransactionModule;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Classes.Interfaces;

namespace PayMammoth.Classes.PaymentNotifications
{
    public class PaymentDoneNotifier
    {

        private readonly object _lock = new object();
        private const string JOB_NAME = "PaymentDoneNotifier-SendPayments";
        public static PaymentDoneNotifier Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<PaymentDoneNotifier>(); } }
        private static readonly ILog _log = LogManager.GetLogger(typeof(PaymentDoneNotifier));


        /// <summary>
        /// Keeps a list of the currently sending items
        /// </summary>
        private ConcurrentDictionary<string, bool> _currentlySending = null;

        public PaymentDoneNotifier()
        {

            _currentlySending = new ConcurrentDictionary<string, bool>();
            BusinessLogic_v3.Modules.SettingModule.SettingBaseFactory.Instance.OnItemUpdate += new BusinessLogic_v3.Classes.DbObjects.General.OnItemUpdateHandler(SettingBaseFactoryInstance_OnItemUpdate);
        }

        void SettingBaseFactoryInstance_OnItemUpdate(IBaseDbObject item, BusinessLogic_v3.Enums.UPDATE_TYPE updateType)
        {
            SettingBase setting = item as SettingBase;
            if (this.Started && setting != null && string.Compare(setting.Identifier, CS.General_v3.Util.EnumUtils.StringValueOf(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Payment_Notifier_SendEveryXSeconds), true) == 0)
            {
                Stop();
                Start();
            }

        }

        private void waitUntilAllAreSent(IEnumerable<INotifyMsg> list)
        {
            int waitInterval = 500;
            int maxTimeout = 120 * 1000; //1 minute
            int currTimeout = 0;
            bool allSent = false;
            while (currTimeout < maxTimeout && !allSent)
            {
                allSent = true;
                foreach (var item in list)
                {
                    if (!item.Finished)
                    {
                        allSent = false;
                        break;
                    }
                }
                System.Threading.Thread.Sleep(waitInterval);
                currTimeout += waitInterval;
            }

            if (currTimeout >= maxTimeout)
            {
                _log.Warn("Not all notification were sent in the timeout of <" + maxTimeout + " ms>");
            }
        }

        private List<INotifyMsg> sendBatch(IEnumerable<INotifiable> list)
        {
            List<INotifyMsg> result = new List<INotifyMsg>();
            foreach (var p in list)
            {
                //check if already being sent 
                bool isCurrentlySending = _currentlySending.ContainsKey(p.Identifier);
                if (!isCurrentlySending)
                {
                    _currentlySending[p.Identifier]=true;
                    var msg = p.CreateNotificationMessage();
                    //add to currently sending list

                    if (msg != null)
                    {
                        msg.OnReady += new Action<INotifyMsg>(msg_OnReady);
                        msg.Start();
                        result.Add(msg);
                    }
                }
            }
            return result;
        }

        void msg_OnReady(INotifyMsg obj)
        {
            bool tmp;
            bool removedOk = _currentlySending.TryRemove(obj.Identifier, out tmp);
        }

        public void SendPendingNotifications()
        {
            lock (_lock) //only one at a time
            {
                int pgSize = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Payment_Notifier_SendPerBatch);

                if (CS.General_v3.Util.Other.IsLocalTestingMachine) pgSize = 1;//temp, should be removed

                int pgNum = 1;
                bool retry = false;
                do
                {
                    BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
                    IEnumerable<INotifiable> listNormalPayments = PaymentRequestFactory.Instance.GetPaymentRequestsThatRequireNotificationsToBeSent(pgNum, pgSize);
                    List<INotifyMsg> currentBatch = sendBatch(listNormalPayments);
                    waitUntilAllAreSent(currentBatch);
                    BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
                    retry = listNormalPayments.Count() > 0;
                    pgNum++;
                } while (retry);

                pgNum = 1;
                do
                {
                    BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
                    IEnumerable<INotifiable> listRecurringPayments = RecurringProfileTransactionFactory.Instance.
                            GetRecurringProfileTransactionsThatRequireNotificationsToBeSent(pgNum, pgSize);
                    List<INotifyMsg> currentBatch = sendBatch(listRecurringPayments);
                    waitUntilAllAreSent(currentBatch);
                    BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
                    retry = listRecurringPayments.Count() > 0;
                    pgNum++;
                } while (retry);
            }
        }

        public void Start()
        {

            lock (_lock)
            {
                if (this.Started) throw new InvalidOperationException("Cannot start the payment notifier when it has already been started");

                int checkEverySeconds = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Payment_Notifier_SendEveryXSeconds);

                BusinessLogic_v3.Util.SchedulingUtil.ScheduleRepeatMethodCall(SendPendingNotifications,
                                                                              JOB_NAME, null, checkEverySeconds, null);

                this.Started = true;
            }
            _log.Debug("PaymentDoneNotifier - Started");
        }

        public bool Started { get; set; }

        public void Stop()
        {
            lock (_lock)
            {
                if (!this.Started) throw new InvalidOperationException("Cannot stop the payment notifier when it has already been stopped ");

                BusinessLogic_v3.Util.SchedulingUtil.StopJob(JOB_NAME);
                this.Started = false;
            }
            _log.Debug("PaymentDoneNotifier - Stopped");
        }


    }
}
