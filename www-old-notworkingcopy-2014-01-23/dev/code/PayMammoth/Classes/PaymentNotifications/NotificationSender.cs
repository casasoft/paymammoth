﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Classes.PaymentNotifications;
using log4net;
using System.Threading;
using BusinessLogic_v3.Modules.SettingModule;
using System.Collections;
using PayMammoth.Modules.RecurringProfileTransactionModule;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Modules.NotificationMessageModule;

namespace PayMammoth.Classes.PaymentNotifications
{
    public class NotificationSender
    {

        private readonly object _lock = new object();
        private const string JOB_NAME = "NotificationSender-SendPayments";
        public static NotificationSender Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<NotificationSender>(); } }
        private static readonly ILog _log = LogManager.GetLogger(typeof(NotificationSender));


        /// <summary>
        /// Keeps a list of the currently sending items
        /// </summary>
        private ConcurrentDictionary<long, NotifyMsg> _currentlySending = null;

        public NotificationSender()
        {

            _currentlySending = new ConcurrentDictionary<long, NotifyMsg>();
            BusinessLogic_v3.Modules.SettingModule.SettingBaseFactory.Instance.OnItemUpdate += new BusinessLogic_v3.Classes.DbObjects.General.OnItemUpdateHandler(SettingBaseFactoryInstance_OnItemUpdate);
        }

        void SettingBaseFactoryInstance_OnItemUpdate(IBaseDbObject item, BusinessLogic_v3.Enums.UPDATE_TYPE updateType)
        {
            SettingBase setting = item as SettingBase;
            if (this.Started && setting != null && string.Compare(setting.Identifier, CS.General_v3.Util.EnumUtils.StringValueOf(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Payment_Notifier_SendEveryXSeconds), true) == 0)
            {
                Stop();
                Start();
            }

        }

        private void waitUntilAllAreSent()
        {
            int waitInterval = 500;
            int maxTimeout = 120 * 1000; //1 minute
            int currTimeout = 0;
            bool allSent = true;
            var list = _currentlySending.Values.ToList();
            do
            {
                allSent = true;
                foreach (var item in list)
                {
                    if (!item.IsFinished)
                    {
                        allSent = false;
                        break;
                    }
                }
                if (!allSent)
                {
                    System.Threading.Thread.Sleep(waitInterval);
                    currTimeout += waitInterval;
                }
            } while (currTimeout < maxTimeout && !allSent);
           // _currentlySending.Clear();
            if (currTimeout >= maxTimeout)
            {
                _log.Warn("Not all notification were sent in the timeout of <" + maxTimeout + " ms>");
            }
        }

        private List<NotifyMsg> sendBatch(IEnumerable<INotificationMessage> msgsToStartSending)
        {
            List<NotifyMsg> result = new List<NotifyMsg>();
            foreach (INotificationMessage msg in msgsToStartSending)
            {
                //check if already being sent 
                bool isCurrentlySending = _currentlySending.ContainsKey(msg.ID);
                if (!isCurrentlySending)
                {
                    var notificationMsg = createNotificationMessageAndStartSending(msg);
                    
                    result.Add(notificationMsg);
                    
                }
            }
            return result;
        }

        void msg_OnReady(NotifyMsg obj)
        {
            NotifyMsg tmp;
            bool removedOk = _currentlySending.TryRemove(obj.NotificationMsgToSendId, out tmp);
        }

        public void SendPendingNotifications()
        {
            lock (_lock) //only one at a time
            {
                int pgSize = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Payment_Notifier_SendPerBatch);

                if (CS.General_v3.Util.Other.IsLocalTestingMachine) pgSize = 1;//temp, should be removed

                int pgNum = 1;
                bool retry = false;
                do
                {
                    BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
                    List<INotificationMessage> notificationMsgsToBeSent = PayMammoth.Modules.Factories.NotificationMessageFactory.GetMessagesThatRequireNotificationToBeSent(pgNum, pgSize).ToList();
                    
                    List<NotifyMsg> currentBatchBeingSent = sendBatch(notificationMsgsToBeSent);

                    waitUntilAllAreSent();
                    BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
                    retry = notificationMsgsToBeSent.Any();
                    pgNum++;
                } while (retry);



            }
        }

        public void Start()
        {

            lock (_lock)
            {
                if (this.Started) throw new InvalidOperationException("Cannot start the payment notifier when it has already been started");

                int checkEverySeconds = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Payment_Notifier_SendEveryXSeconds);

                BusinessLogic_v3.Util.SchedulingUtil.ScheduleRepeatMethodCall(SendPendingNotifications,
                                                                              JOB_NAME, null, checkEverySeconds, null);

                this.Started = true;
            }
            _log.Debug("PaymentDoneNotifier - Started");
        }

        public bool Started { get; set; }

        public void Stop()
        {
            lock (_lock)
            {
                if (!this.Started) throw new InvalidOperationException("Cannot stop the payment notifier when it has already been stopped ");

                BusinessLogic_v3.Util.SchedulingUtil.StopJob(JOB_NAME);
                this.Started = false;
            }
            _log.Debug("PaymentDoneNotifier - Stopped");
        }



        private NotifyMsg createNotificationMessageAndStartSending(INotificationMessage msg)
        {
            NotifyMsg notifyMsg = new NotifyMsg(msg.ID);
            _currentlySending[msg.ID] = notifyMsg;
            notifyMsg.OnReady += msg_OnReady;
            notifyMsg.Start();
            return notifyMsg;
        }

        public void StartSendingNotificationForMessage(INotificationMessage msg)
        {
            if (!_currentlySending.ContainsKey(msg.ID))
            {
                createNotificationMessageAndStartSending(msg);
                
            }

        }
    }
}
