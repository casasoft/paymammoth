﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Background;

namespace PayMammoth.Classes.RecurringPayments
{
    public interface IRecurringProfilePaymentsCheckerBackgroundTask
    {
        void CheckRecurringPayments();
    }
    [IocComponent]
    public class RecurringProfilePaymentsCheckerBackgroundTask : BaseRecurringTask<RecurringProfilePaymentsCheckerBackgroundTask>
    {
        public RecurringProfilePaymentsCheckerBackgroundTask(): base(Enums.PAYMAMMOTH_SETTINGS.BackgroundTasks_RecurringProfilePaymentsCheckerBackgroundTask_RepeatInterval)
        {

        }

        public void CheckRecurringPayments()
        {
            var service = BusinessLogic_v3.Util.InversionOfControlUtil.Get<IRecurringPaymentsCheckerService>();
            service.CheckPayments();
        }

        protected override void recurringTask()
        {
            BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            CheckRecurringPayments();
            BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
            
        }
    }
}
