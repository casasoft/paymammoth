﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules.RecurringProfileModule;

namespace PayMammoth.Classes.RecurringPayments
{
    public interface IRecurringPaymentProfileRequiredChecker
    {
        IRecurringProfile CreateRecurringProfileIfRequired(IPaymentRequestTransaction transaction);
    }
}
