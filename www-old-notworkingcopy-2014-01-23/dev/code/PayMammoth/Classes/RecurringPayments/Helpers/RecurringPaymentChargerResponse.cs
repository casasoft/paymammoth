﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Classes.RecurringPayments.Helpers
{
    public class RecurringPaymentChargerResponse
    {
        public Enums.RecurringProfileTransactionStatus Status { get; set; }
        public string PaymentDetails { get; set; }

        public string PaymentReference { get; set; }
    }
}
