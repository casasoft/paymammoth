﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Classes.RecurringPayments.RecurringPaymentChargers;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfileModule.Services;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Util;

namespace PayMammoth.Classes.RecurringPayments
{
    public interface IRecurringPaymentChargePaymentByPayMammothService
    {
        void ChargePayment(IRecurringProfilePayment payment);
    }

    [IocComponent]
    public class RecurringPaymentChargePaymentByPayMammothService : IRecurringPaymentChargePaymentByPayMammothService
    {


        
        private readonly IRecurringProfilePaymentMarkLastPaymentAsSuccessfulService markPaymentAsSuccessfulService = null;
        private readonly IRecurringProfilePaymentMarkAsFailureService markAsFailureService = null;
        public RecurringPaymentChargePaymentByPayMammothService(IRecurringProfilePaymentMarkLastPaymentAsSuccessfulService markPaymentAsSuccessfulService,
            IRecurringProfilePaymentMarkAsFailureService markAsFailureService)
        {
            
            this.markPaymentAsSuccessfulService = markPaymentAsSuccessfulService;
            this.markAsFailureService = markAsFailureService;
            

        }
        


    
        #region IRecurringPaymentChargePaymentByPayMammothService Members

        public void ChargePayment(IRecurringProfilePayment payment)
        {
            IRecurringPaymentCharger recurringPaymentCharger = GeneralUtil.GetRecurringPaymentChargerForPaymentMethod(payment.RecurringProfile.PaymentGateway);
            var result = recurringPaymentCharger.ChargePayment(payment);
            switch (result.Status)
            {
                case Enums.RecurringProfileTransactionStatus.Success:
                    {
                        markPaymentAsSuccessfulService.MarkAsSuccessful(payment,  result.PaymentReference, result.PaymentDetails);
                        break;
                    }
                case Enums.RecurringProfileTransactionStatus.Failure:
                    {

                        markAsFailureService.MarkPaymentAsFailure(payment,result.PaymentReference, result.PaymentDetails);
                        break;
                    }
                default:
                    throw new InvalidOperationException("Not implemented for value <" + result.Status + ">");
            }
        }

        #endregion
    }
}
