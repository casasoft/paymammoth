﻿using PayMammoth.Classes.RecurringPayments.Helpers;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammoth.Classes.RecurringPayments.RecurringPaymentChargers
{
    /// <summary>
    /// This is an interface to a payment-method specific recurring payment charger
    /// </summary>
    public interface IRecurringPaymentCharger
    {

        RecurringPaymentChargerResponse ChargePayment(IRecurringProfilePayment payment);

    }
}
