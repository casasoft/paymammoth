﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Classes.Helpers;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Modules;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Util;

namespace PayMammoth.Classes.RecurringPayments.Impl
{
    [IocComponent]
    public class RecurringPaymentProfileRequiredCheckerImpl : IRecurringPaymentProfileRequiredChecker
    {
        #region IRecurringPaymentChecker Members

        private readonly IRecurringProfileCreationFailedNotificationSender profileCreationFailedNotificationSender = null;
        public RecurringPaymentProfileRequiredCheckerImpl(IRecurringProfileCreationFailedNotificationSender profileCreationFailedNotificationSender)
        {
            this.profileCreationFailedNotificationSender = profileCreationFailedNotificationSender;
        }


        public IRecurringProfile CreateRecurringProfileIfRequired(Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction transaction)
        {
            IRecurringProfile profile = null;
            IPaymentRequestDetails requestDetails = transaction.PaymentRequest;
            if (requestDetails.RecurringProfileRequired && Util.PaymentUtil.CheckIfPaymentMethodAllowsForRecurringProfiles(transaction.PaymentMethod))
            {
                profile = null;
                using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                {
                    IPaymentRequestDetails payment = transaction.PaymentRequest;
                    profile = Factories.RecurringProfileFactory.CreateNewItem();
                    transaction.PaymentRequest.RecurringProfileLink = profile;
                    
                    
                    profile.WebsiteAccount = transaction.PaymentRequest.WebsiteAccount;
                    profile.NotificationUrl = payment.NotificationUrl;
                    CS.General_v3.Util.ReflectionUtil.CopyProperties<IRecurringProfileDetails>((IRecurringProfileDetails)transaction.PaymentRequest, (IRecurringProfileDetails)profile);
                    profile.Status = Enums.RecurringProfileStatus.Pending;
                    profile.PaymentGateway = transaction.PaymentMethod;
                    profile.Save();
                    transaction.PaymentRequest.Save();
                    t.Commit();
                }
                IRecurringProfileManager recurringProfileManager = PaymentUtil.GetRecurringProfileManagerBasedOnPaymentMethod(transaction.PaymentMethod);
                SetupRecurringProfileResult profileSetupResult = recurringProfileManager.SetupRecurringProfile(profile);
                if (profileSetupResult.Status != Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE.Success)
                {
                    profileCreationFailedNotificationSender.SendNotification(profile, profileSetupResult);
                    
                }
            }
            
            return profile;
        }

        #endregion
    }
}
