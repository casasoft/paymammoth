﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Modules.RecurringProfileModule.Services;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammoth.Classes.RecurringPayments
{
    public interface IRecurringPaymentsCheckerService
    {
        void CheckPayments();
    }

    [IocComponent]
    public class RecurringPaymentsCheckerService : IRecurringPaymentsCheckerService
    {
        private readonly IRecurringProfilePaymentFactory recurringProfilePaymentFactory;
        private readonly IRecurringPaymentChargePaymentByPayMammothService recurringPaymentChargePaymentByPayMammothService;
        private readonly IRecurringProfilePaymentMarkAsFailureService markAsFailureService;
        public RecurringPaymentsCheckerService(IRecurringProfilePaymentFactory recurringProfilePaymentFactory,
            IRecurringPaymentChargePaymentByPayMammothService recurringPaymentChargePaymentByPayMammothService,
            IRecurringProfilePaymentMarkAsFailureService markAsFailureService)
        {
            this.recurringProfilePaymentFactory = recurringProfilePaymentFactory;
            this.recurringPaymentChargePaymentByPayMammothService = recurringPaymentChargePaymentByPayMammothService;
            this.markAsFailureService = markAsFailureService;
        }



        #region IRecurringPaymentsCheckerService Members

        public void CheckPayments()
        {
            {
                var requiringPayments = recurringProfilePaymentFactory.GetRecurringPaymentsRequiringNextPaymentByPayMammoth();
                foreach (var payment in requiringPayments)
                {
                    this.recurringPaymentChargePaymentByPayMammothService.ChargePayment(payment);
                }
            }
            {
                var failingPayments = recurringProfilePaymentFactory.GetRecurringPaymentsWithExternalPaymentsWhichHaveNotSucceeded();
                foreach (var payment in failingPayments)
                {
                    markAsFailureService.MarkPaymentAsFailure(payment, CS.General_v3.Util.Date.Now.ToString("yyyyMMddHHmmss") + "-" + CS.General_v3.Util.Random.GetAlpha(6, 6),
                        "Payment marked as failed as time elapsed and still not marked as paid by external service");
                    
                }
            }
        }

        #endregion
    }
}
