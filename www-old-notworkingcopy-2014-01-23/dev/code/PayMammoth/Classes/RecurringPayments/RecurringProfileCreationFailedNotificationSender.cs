﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Classes.Helpers;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.RecurringProfileModule;

namespace PayMammoth.Classes.RecurringPayments
{
    public interface IRecurringProfileCreationFailedNotificationSender
    {
        void SendNotification(IRecurringProfile profile, SetupRecurringProfileResult result);

    }

    [IocComponent]
    public class RecurringProfileCreationFailedNotificationSender : IRecurringProfileCreationFailedNotificationSender
    {
        #region IRecurringProfileCreationFailedNotificationSender Members

        public void SendNotification(IRecurringProfile profile, SetupRecurringProfileResult result)
        {
            NotificationCreatorAndSender sender = BusinessLogic_v3.Util.InversionOfControlUtil.Get<NotificationCreatorAndSender>();
            CreateNewNotificationCommand cmd = new CreateNewNotificationCommand(Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCouldNotCreate, profile.WebsiteAccount,profile.GetNotificationUrl());
            cmd.Identifier = profile.Identifier;
            
            
            cmd.Message = string.Format(result.Message + " [" + CS.General_v3.Util.EnumUtils.StringValueOf(result.Status) + "]"); ;
            sender.CreateAndQueueNotification(cmd);
        }

        #endregion
    }
}
