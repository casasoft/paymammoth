﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Serialization;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Connector.Classes.Interfaces;

namespace PayMammoth.Classes.Other
{
    public class ConfirmRequestDetails : BaseSerializable, IConfirmRequestDetails
    {
        #region IConfirmRequestDetails Members

        public string RequestIdentifier { get; set; }
        public string HashValue { get; set; }

        public PaymentRequest VerifyAndGetRequest()
        {

            PaymentRequest request = PaymentRequest.Factory.GetRequestByIdentifier(this.RequestIdentifier);
            if (request != null)
            {
                string computedHash = PayMammoth.Connector.Util.HashUtil.GenerateConfirmHash(this.RequestIdentifier, request.WebsiteAccount.SecretWord);
                if (string.Compare(computedHash, this.HashValue,true) != 0)
               
                    request = null;
                    
            }
            return request;
        }
        #endregion
        public void LoadFromQuerystring()
        {
            base.loadFromQuerystring<IConfirmRequestDetails>();
        }
    }
}
