using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.RecurringPayments;


namespace PayMammoth.Classes.Application
{
    public class AppInstance : PayMammoth.Modules._AutoGen.AppInstance_AutoGen
    {
		public AppInstance()
        {
            this.AddProjectAssembly(typeof(AppInstance));
        }
        protected override void onPostApplicationStart()
        {

            PayMammoth.Connector.Constants.SecretWord = "PAYMAMMOTH";
            ConnectorEventTests.ConnectorEvents.Initialise();
            base.onPostApplicationStart();
        }
        protected override void launchBackgroundTasks()
        {
            PaymentNotifications.NotificationSender.Instance.Start();
            RecurringProfilePaymentsCheckerBackgroundTask.Instance.Start();
            base.launchBackgroundTasks();
        }
        protected override void initialiseInversionOfControlMappings()
        {
            base.initialiseInversionOfControlMappings();
            IoCRegistration.RegisterComponents();
        }
    }
}
