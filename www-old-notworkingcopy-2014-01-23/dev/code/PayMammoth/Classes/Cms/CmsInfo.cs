using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Classes.Cms
{
    public class CmsInfo : PayMammoth.Classes.Cms._AutoGen.CmsInfo_Z
    {
		public static new CmsInfo Instance { get { return (CmsInfo)CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsSystem.Instance; } }
            
        
        private CmsInfo() 
        {

        }
    }
}
