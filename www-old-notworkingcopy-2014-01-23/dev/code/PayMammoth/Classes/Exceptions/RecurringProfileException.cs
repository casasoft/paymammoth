﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Classes.Exceptions
{
    public class RecurringProfileException : Exception
    {
        public RecurringProfileException(string msg)
            : base(msg)
        {

        }
    }
}
