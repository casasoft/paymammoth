﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Classes.Exceptions
{
    public class FakePaymentNotAllowedException : Exception
    {
        public FakePaymentNotAllowedException(string msg)
            : base(msg)
        {

        }
    }
}
