﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Interceptors;
using PayMammoth.Modules.PaymentRequestModule;

namespace PayMammoth.Classes.Nhibernate
{
    [Serializable]
    public class NhInterceptor : BaseNhInterceptor
    {

        protected NhInterceptor() : base()
        {
            int k = 5;
        }

        private void checkForPaymentRequestSave(object entity,object id, object[] state,string[] propertyNames)
        {
            //if (CS.General_v3.Util.ReflectionUtil.CheckIfObjectExtendsFrom(entity, typeof(PaymentRequest)))
            //{
            //    logObjectStatusChange(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Info, entity, id, state, propertyNames);
            //}

        }

        public override bool OnSave(object entity, object id, object[] state, string[] propertyNames, NHibernate.Type.IType[] types)
        {
            checkForPaymentRequestSave(entity, id, state, propertyNames);
            return base.OnSave(entity, id, state, propertyNames, types);
        }
        public override bool OnFlushDirty(object entity, object id, object[] currentState, object[] previousState, string[] propertyNames, NHibernate.Type.IType[] types)
        {
            checkForPaymentRequestSave(entity, id, currentState, propertyNames);
            return base.OnFlushDirty(entity, id, currentState, previousState, propertyNames, types);
        }
    }
}
