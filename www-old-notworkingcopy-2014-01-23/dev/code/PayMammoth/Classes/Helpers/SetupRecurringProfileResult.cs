﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Classes.Helpers
{
    public class SetupRecurringProfileResult
    {
        public PayMammoth.Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE Status { get; set; }
        public string Message { get; set; }
    }
}
