﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;

namespace PayMammoth.Extensions
{
    public static class IPaypalSettingsExtensions
    {
        public static IPayPalCredentials GetCurrentSettings(this IPayPalSettings settings)
        {
            if (settings.UseLiveEnvironment)
                return settings.LiveCredentials;
            else
                return settings.SandboxCredentials;


        }
    }
}
