﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.ConnectorEventTests
{
    public static class ConnectorEvents
    {
        public static void Initialise()
        {
            PayMammoth.Connector.Events.OnPaidSuccessfully += onPaidSuccessfully;
            PayMammoth.Connector.Events.OnRecurringPayment += onRecurringPayment;
            PayMammoth.Connector.Events.OnRecurringProfileStatusChange += onRecurringProfileStatusChange;
        }

        private static void onRecurringProfileStatusChange(Connector.Classes.Notifications.INotificationMsg msg, Connector.Classes.Responses.RecurringProfileUpdateConfirmationResponse response)
        {
            System.Diagnostics.Trace.WriteLine(""); System.Diagnostics.Trace.WriteLine("");
            System.Diagnostics.Trace.WriteLine(string.Format("OnRecurringProfileStatusChange Called - [NotificationMsg- Identifier: {0}, NotificationType: {1}] Response: {2}, Success: {3}",
                msg.Identifier, msg.NotificationType, response.ProfileIdentifier, response.ConfirmationSuccess));
            System.Diagnostics.Trace.WriteLine(""); System.Diagnostics.Trace.WriteLine("");
        }

        private static void  onRecurringPayment(Connector.Classes.Notifications.INotificationMsg msg, Connector.Classes.Responses.RecurringPaymentConfirmationResponse response)
        {
            System.Diagnostics.Trace.WriteLine(""); System.Diagnostics.Trace.WriteLine("");
            System.Diagnostics.Trace.WriteLine(string.Format("OnRecurringPayment Called - [NotificationMsg- Identifier: {0}, NotificationType: {1}] Response: {2}, Success: {3}",
                msg.Identifier, msg.NotificationType, response.RequestIdentifier, response.ConfirmationSuccess));
            System.Diagnostics.Trace.WriteLine(""); System.Diagnostics.Trace.WriteLine("");
        }

        private static void onPaidSuccessfully(Connector.Classes.Notifications.INotificationMsg msg, Connector.Classes.Responses.ImmediatePaymentConfirmationResponse response)
        {
            System.Diagnostics.Trace.WriteLine(""); System.Diagnostics.Trace.WriteLine("");
            System.Diagnostics.Trace.WriteLine(string.Format("OnPaidSuccessfully Called - [NotificationMsg- Identifier: {0}, NotificationType: {1}] Response: {2}, Success: {3}" , msg.Identifier,msg.NotificationType,response.PayMammothRequestIdentifier, response.ConfirmationSuccess));
            System.Diagnostics.Trace.WriteLine(""); System.Diagnostics.Trace.WriteLine("");
        }

    }
}
