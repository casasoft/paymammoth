﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using PayMammoth.Classes.RecurringPayments.RecurringPaymentChargers;

namespace PayMammoth.Util
{
    public static class GeneralUtil
    {
        
        private static readonly ILog _log = LogManager.GetLogger(typeof(GeneralUtil));

        public static IRecurringPaymentCharger GetRecurringPaymentChargerForPaymentMethod(PayMammoth.Connector.Enums.PaymentMethodSpecific paymentMethod)
        {
            switch (paymentMethod)
            {
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Transactium:
                    return TransactiumRecurringPaymentCharger.Instance;
                default:
                    throw new InvalidOperationException(string.Format("Not implemented for value <{0}>", paymentMethod));
            }
        }

        public static string AddMessageToStringLog(string originalLog, CS.General_v3.Enums.LOG4NET_MSG_TYPE msgType, string msgToLog, Exception ex = null)
        {
            
            string s = originalLog;
            if (!string.IsNullOrEmpty(s))
            {
                s += "\r\n---------------------------------------------\r\n";
            }

            CS.General_v3.Util.Log4NetUtil.LogMsg(_log, msgType, msgToLog, ex);

            s += string.Format("[{0} | {1}] {2}", CS.General_v3.Util.EnumUtils.StringValueOf(msgType), CS.General_v3.Util.Date.Now.ToString("dd/MM/yyyy HH:mm:ss"), msgToLog);
            


            return s;
        }
        /// <summary>
        /// Returns whether a recurring paymnt is initiated by PayMammoth, i.e PayMammoth does the payment, or it waits for the payment like for PayPal
        /// </summary>
        /// <param name="paymentMethod"></param>
        /// <returns></returns>
        public static bool CheckIfRecurringPaymentIsInitiatedByPayMammothForPaymentMethod(PayMammoth.Connector.Enums.PaymentMethodSpecific paymentMethod)
        {
            switch (paymentMethod)
            {
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Apco:
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Realex:
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Transactium:
                
                    return true;
                case Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout:
                    return false;
                default:
                    throw new InvalidOperationException(string.Format("Not implemented for value <{0}>", paymentMethod));
                
            }

        }

        /// <summary>
        /// Adds an interval to a date time, e.g adds 5 months to relative date
        /// </summary>
        /// <param name="relativeDate"></param>
        /// <param name="intervalType"></param>
        /// <param name="intervalCount"></param>
        /// <returns></returns>
        public static DateTime AddIntervalToDateTime(DateTime relativeDate, PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD intervalType, int intervalCount)
        {
          
            DateTime result;
            switch (intervalType)
            {
                case Connector.Enums.RECURRING_BILLINGPERIOD.Day: result = relativeDate.AddDays(intervalCount); break;
                case Connector.Enums.RECURRING_BILLINGPERIOD.Month: result = relativeDate.AddMonths(intervalCount); break;
                case Connector.Enums.RECURRING_BILLINGPERIOD.Year: result = relativeDate.AddYears(intervalCount); break;
                default: 
                    throw new InvalidOperationException(string.Format("Not yet implemented for value {0}", intervalType));
            }
            return result;
        }
    }
}
