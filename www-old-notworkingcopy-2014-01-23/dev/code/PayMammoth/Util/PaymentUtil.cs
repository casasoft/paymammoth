﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Interfaces;
using PayMammoth.PaymentMethods.PayPal.v1;
using PayMammoth.PaymentMethods.Cheque;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.PaymentMethods.PayPal.v1.Interfaces;
using PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1;

namespace PayMammoth.Util
{
    public static class PaymentUtil
    {

        public static bool CheckIfPaymentMethodAllowsForRecurringProfiles(PayMammoth.Connector.Enums.PaymentMethodSpecific method)
        {
            switch (method)
            {
                case Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout: return true;
            }
            return false;
        }

        public static IPaymentRedirector GetRedirectorBasedOnPaymentMethod(PayMammoth.Connector.Enums.PaymentMethodSpecific method)
        {
            switch (method)
            {
                case PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout: return PayPalRedirector.Instance;
                case PayMammoth.Connector.Enums.PaymentMethodSpecific.PaymentGateway_Transactium: return new PaymentMethods.PaymentGateways.Transactium.v1.TransactiumRedirector();
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Realex: return new PaymentMethods.PaymentGateways.Realex.v1.RealexRedirector();
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Apco: return new PaymentMethods.PaymentGateways.Apco.v1.ApcoPaymentRedirector();
                case PayMammoth.Connector.Enums.PaymentMethodSpecific.Cheque: return new ChequeRedirector();
                default:
                    throw new InvalidOperationException("GetRedirectorBasedOnPaymentMethod() - Payment Method not yet implemented");
            }
            return null;
        }

        public static IRecurringProfileManager GetRecurringProfileManagerBasedOnPaymentMethod(PayMammoth.Connector.Enums.PaymentMethodSpecific method)
        {
            switch (method)
            {
                case PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout: return BusinessLogic_v3.Util.InversionOfControlUtil.Get<IPayPalRecurringProfileManager>();
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Transactium:
                    return new TransactiumRecurringProfileManager();
                default:
                    throw new InvalidOperationException("GetRecurringProfileManagerBasedOnPaymentMethod() - Payment Method <" + method + "> not yet implemented");
            }
            return null;
        }


        public static bool CheckIfPaymentMethodRequiresManualPayment(PayMammoth.Connector.Enums.PaymentMethodSpecific method)
        {
            bool ok = false;
            switch (method)
            {
                case PayMammoth.Connector.Enums.PaymentMethodSpecific.Cheque:
                case Connector.Enums.PaymentMethodSpecific.BankTransfer:
                    ok = true;
                    break;

            }
            return ok;

        }
    }
}
