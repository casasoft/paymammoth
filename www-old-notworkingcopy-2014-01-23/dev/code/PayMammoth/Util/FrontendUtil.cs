﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Util
{
    using Frontend.PaymentRequestModule;

    public static class FrontendUtil
    {





        public static PaymentRequestFrontend GetCurrentPaymentRequest()
        {
            const string key = "currPaymentRequest";
            var paymentRequest = CS.General_v3.Util.PageUtil.GetContextObject<PaymentRequestFrontend>(key);
            if (paymentRequest == null)
            {
                 var data = QuerystringData.GetPaymentRequestFromQuerystring();
                paymentRequest = PaymentRequestFrontend.Get(data);
                CS.General_v3.Util.PageUtil.SetContextObject(key, paymentRequest);
            }
            return paymentRequest;
                    
        }
    }
}
