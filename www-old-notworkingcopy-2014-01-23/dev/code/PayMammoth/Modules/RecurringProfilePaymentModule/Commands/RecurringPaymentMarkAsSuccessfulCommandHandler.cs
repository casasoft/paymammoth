﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammoth.Modules.RecurringProfilePaymentModule.Commands
{
    [IocComponent]
    public class RecurringPaymentMarkAsSuccessfulCommandHandler : IDbCommandHandler<RecurringPaymentMarkAsSuccessfulCommand>
    {
        public RecurringPaymentMarkAsSuccessfulCommandHandler()
        {
            
        }

        #region IDbCommandHandler<RecurringPaymentMarkAsSuccessfulCommand> Members




        public void Handle(RecurringPaymentMarkAsSuccessfulCommand command)
        {
            //IRecurringProfileTransaction transaction = payment.Transactions.CreateNewItem();

            //transaction.OtherData = PayMammoth.Util.GeneralUtil.AddMessageToStringLog(transaction.OtherData, CS.General_v3.Enums.LOG4NET_MSG_TYPE.Info, failureData);
            //transaction.Status = Enums.RecurringProfileTransactionStatus.Failure;



            //incrementFailureCount(payment);

            //if (hasMaximumFailedAttemptsBeenReached(payment))
            //{
            //    recurringProfileSuspender.SuspendRecurringProfile(payment.RecurringProfile, command.FailureData);
            //}
            //else
            //{
            //    paymentFailedNotificationSender.CreateAndQueueNotification(transaction);
            //    nextRetryDateUpdater.UpdateNextRetryDate(payment);
            //}
            //transaction.Save();
            //command.Output_Transaction = transaction;






            var payment = command.Payment;
            IRecurringProfileTransaction transaction = payment.Transactions.CreateNewItem();
            transaction.AddToStatusLog(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Info, "Recurring payment successful - " + command.PaymentDetails);
            transaction.Status = Enums.RecurringProfileTransactionStatus.Success;
            transaction.IsFakePayment = command.IsFakePayment;
            transaction.PaymentGatewayReference = command.PaymentGatewayReference;
            payment.FailureCount = 0;
            payment.NextRetryOn = null;
            payment.Status = Connector.Enums.RecurringProfilePaymentStatus.Success;
            transaction.Save();
            payment.Save();
            command.Output_Transaction = transaction;

        }

        #endregion
    }
}
