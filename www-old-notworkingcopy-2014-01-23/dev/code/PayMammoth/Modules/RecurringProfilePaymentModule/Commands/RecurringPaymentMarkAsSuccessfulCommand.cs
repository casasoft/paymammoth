﻿using BusinessLogic_v3.Classes.Interfaces.DbCommands;

namespace PayMammoth.Modules.RecurringProfilePaymentModule.Commands
{
    public class RecurringPaymentMarkAsSuccessfulCommand : BaseDbCommand
    {
        public IRecurringProfilePayment Payment { get; set; }
        public string PaymentGatewayReference { get; set; }

        #region IDbCommand Members

        #endregion

        public string PaymentDetails { get; set; }

        public RecurringProfileTransactionModule.IRecurringProfileTransaction Output_Transaction { get; set; }
        public override bool Validate()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullableBySelectors(this, x => x.Payment);
            return base.Validate();
        }

        public bool IsFakePayment { get; set; }
    }
}
