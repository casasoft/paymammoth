using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;

using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;



namespace PayMammoth.Modules.RecurringProfilePaymentModule
{

//UserFactory-File

    public class RecurringProfilePaymentFactory : PayMammoth.Modules._AutoGen.RecurringProfilePaymentFactoryAutoGen, IRecurringProfilePaymentFactory
    {   
    
		
            
       private RecurringProfilePaymentFactory()
  	   {
    	   
   	   }


       #region IRecurringProfilePaymentFactory Members

       public List<IRecurringProfilePayment> GetRecurringPaymentsRequiringNextPaymentByPayMammoth()
       {
           var q = GetQuery(new GetQueryParams(orderByPriority: false));
           q.Where(x => x.InitiatedByPayMammoth && x.Status == Connector.Enums.RecurringProfilePaymentStatus.Pending && x.NextRetryOn <= CS.General_v3.Util.Date.Now);
           var list = FindAll(q);
           return list.Cast<IRecurringProfilePayment>().ToList();
       }


       public List<IRecurringProfilePayment> GetRecurringPaymentsWithExternalPaymentsWhichHaveNotSucceeded()
       {
           var q = GetQuery(new GetQueryParams(orderByPriority: false));
           q.Where(x => !x.InitiatedByPayMammoth && x.Status == Connector.Enums.RecurringProfilePaymentStatus.Pending && x.MarkAsPaymentFailedIfNotPaidBy <= CS.General_v3.Util.Date.Now);
           var list = FindAll(q);
           return list.Cast<IRecurringProfilePayment>().ToList();
       }
       #endregion




       #region IRecurringProfilePaymentFactory Members


       public IRecurringProfilePayment GetPaymentByIdentifier(string identifier)
       {
           
           var q = GetQuery(new GetQueryParams(orderByPriority: false));
           q = q.Where(x => x.Identifier == identifier);
           return FindItem(q);
       }

       #endregion

    

    }
}
