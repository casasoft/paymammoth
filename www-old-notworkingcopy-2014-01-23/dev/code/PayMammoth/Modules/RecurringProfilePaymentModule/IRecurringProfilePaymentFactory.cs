using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules.RecurringProfilePaymentModule
{   

//IUserFactory-File


	public interface IRecurringProfilePaymentFactory : PayMammoth.Modules._AutoGen.IRecurringProfilePaymentFactoryAutoGen
    {
        List<IRecurringProfilePayment> GetRecurringPaymentsRequiringNextPaymentByPayMammoth();
        List<IRecurringProfilePayment> GetRecurringPaymentsWithExternalPaymentsWhichHaveNotSucceeded();
        IRecurringProfilePayment GetPaymentByIdentifier(string identifier);

    }

}
