﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Connector.Util;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammoth.Modules.RecurringProfilePaymentModule
{
    public interface IRecurringProfilePaymentService
    {
        /// <summary>
        /// Returns the successful transaction, if any
        /// </summary>
        /// <returns></returns>
        IRecurringProfileTransaction GetSuccessfulTransaction(IRecurringProfilePayment payment);
        /// <summary>
        /// Returns the successful transaction, if any
        /// </summary>
        /// <returns></returns>
        IRecurringProfileTransaction GetLastFailedTransaction(IRecurringProfilePayment payment);
        void FillConfirmResponse(IRecurringProfilePayment payment, Connector.Classes.Responses.RecurringPaymentConfirmationResponse resp);
    }

    [IocComponent]
    public class RecurringProfilePaymentService : IRecurringProfilePaymentService
    {

        #region IRecurringProfilePaymentService Members

        public IRecurringProfileTransaction GetSuccessfulTransaction(IRecurringProfilePayment payment)
        {
            
            return payment.Transactions.FirstOrDefault(x => x.Status == Enums.RecurringProfileTransactionStatus.Success);
        }

        #endregion

        #region IRecurringProfilePaymentService Members


        public IRecurringProfileTransaction GetLastFailedTransaction(IRecurringProfilePayment payment)
        {
            
            var list = payment.Transactions.Where(x => x.Status == Enums.RecurringProfileTransactionStatus.Failure).ToList();
            list.Sort((x1, x2) => -(x1.Timestamp.CompareTo(x2.Timestamp)));
            return list.FirstOrDefault();

        }

        public void FillConfirmResponse(IRecurringProfilePayment payment, Connector.Classes.Responses.RecurringPaymentConfirmationResponse resp)
        {
            

            resp.ConfirmationSuccess = true;
            resp.Status = payment.Status;
            resp.RequestIdentifier = payment.Identifier;
            resp.Hash = HashUtil.GenerateConfirmHash(payment.Identifier, payment.RecurringProfile.WebsiteAccount.SecretWord);
            resp.PaymentMethod = payment.RecurringProfile.PaymentGateway;
            resp.ProfileReference = payment.RecurringProfile.Identifier;
            
            IRecurringProfileTransaction relatedTransaction = null;
            relatedTransaction=  (resp.ConfirmationSuccess ? GetSuccessfulTransaction(payment) : GetLastFailedTransaction(payment));
            
            if (relatedTransaction != null)
            {
                resp.ExternalGatewayReference = relatedTransaction.PaymentGatewayReference;
                resp.TransactionTimestamp = relatedTransaction.Timestamp;
                resp.TotalAmount = relatedTransaction.TotalAmount;
                resp.IsFakePayment = relatedTransaction.IsFakePayment;
               
            }
            
            
        }
        #endregion
    }
}
