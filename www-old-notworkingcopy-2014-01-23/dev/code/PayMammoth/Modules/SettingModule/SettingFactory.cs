using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;



namespace PayMammoth.Modules.SettingModule
{

//UserFactory-File

    public class SettingFactory : PayMammoth.Modules._AutoGen.SettingFactoryAutoGen, ISettingFactory
    {   
    
		
            
       private SettingFactory()
  	   {
    	
           
   	   }
       public string GetPayMammothSetting(Enums.PAYMAMMOTH_SETTINGS setting)
       {
           return GetPayMammothSetting<string>(setting);
           
       }
       public T GetPayMammothSetting<T>(Enums.PAYMAMMOTH_SETTINGS setting)
       {
           return base.GetSetting<T>(setting, true);
       }


    }
}
