using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;


namespace PayMammoth.Modules.WebsiteAccountModule
{

//ClassMap-File
    
    public class WebsiteAccountMap : PayMammoth.Modules._AutoGen.WebsiteAccountMap_AutoGen
    {
        public WebsiteAccountMap() 
        {

        }
        protected override void CodeMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            base.CodeMappingInfo(mapInfo);
        }


    }
   
}
