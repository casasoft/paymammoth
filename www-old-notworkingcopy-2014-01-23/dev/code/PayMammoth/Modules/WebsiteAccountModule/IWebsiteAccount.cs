using BusinessLogic_v3.Classes.MediaItems;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentMethodModule;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.NotificationMessageModule;


namespace PayMammoth.Modules.WebsiteAccountModule
{

//IUserClass-File
	
    public interface IWebsiteAccount : PayMammoth.Modules._AutoGen.IWebsiteAccountAutoGen
    {

        IPayPalSettings GetPaypalSettings();

        IEnumerable<IPaymentMethodInfo> GetAvailablePaymentMethods();


        bool IsApcoEnabled();

        bool IsTransactiumEnabled();

        bool IsChequeEnabled();

        PaymentMethods.PaymentGateways.Apco.v1.ApcoCredentials GetApcoCredentials();

        PaymentMethods.PaymentGateways.Realex.v1.Classes.IRealexCredentials GetRealexCredentials();

        IMediaItem Css { get; }
        IMediaItemImage<WebsiteAccount.LogoSizingEnum> Logo { get; }


        bool CheckFakePaymentKey(string key);

        bool VerifyRequest(IInitialRequestDetails initialRequestInfo);

        IPaymentRequest GenerateNewRequest(IInitialRequestDetails initialRequestInfo);


    }
}
