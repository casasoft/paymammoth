using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;



namespace PayMammoth.Modules.WebsiteAccountModule
{

//UserFactory-File

    public class WebsiteAccountFactory : PayMammoth.Modules._AutoGen.WebsiteAccountFactoryAutoGen, IWebsiteAccountFactory
    {   
    
		
            
       private WebsiteAccountFactory()
  	   {
    	   
   	   }


       public IWebsiteAccount GetAccountByCode(string code)
       {
           return getAccountsByCode(code).FirstOrDefault();
           
           
       }
       internal List<WebsiteAccount> getAccountsByCode(string code)
       {
           var q = GetQuery();
           q.Where(x => x.Code == code);
           return FindAll(q).ToList();

       }


       #region IWebsiteAccountFactory Members

       //private IWebsiteAccount createUnitTestingWebsiteAccount()
       //{
       //    WebsiteAccount account = CreateNewItem();
       //    account.ResponseUrl = "http://office.casasoft.com.mt:8090/";
       //    account.WebsiteName = "Unit Testing Account";
       //    account.Code = Constants.UnitTestingWebsiteAccountIdentifier;
       //    account.ChequeAddress1 = "6, Spencer Flats, Flat4";
       //    account.ChequeAddress2 = "Triq Dun Gorg Preca";
       //    account.ChequeAddressCountry = "Malta";
       //    account.ChequeAddressLocality = "Hamrun";
       //    account.ChequeAddressPostCode = "HMR1605";
       //    account.ChequeEnabled = true;
       //    account.ChequePayableTo = "CasaSoft Ltd.";
       //    account.ContactEmail = "info@casasoft.com.mt";
       //    account.ContactName = "Karl Cassar / CasaSoft";
       //    account.DisableResponseUrlNotifications = false;
       //    account.EnableFakePayments = true;
       //    account.


       //}

       // public IWebsiteAccount GetUnitTestingWebsiteAccount()
       //{
       //    var account = GetAccountByCode(Constants.UnitTestingWebsiteAccountIdentifier);
       //    if (account == null)
       //    {
       //        createUnitTestingWebsiteAccount();

       //    }
       //    return account;
       //}

       #endregion

     
    }
}
