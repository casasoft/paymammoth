using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;

using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;



namespace PayMammoth.Modules.NotificationMessageModule
{

//UserFactory-File

    public class NotificationMessageFactory : PayMammoth.Modules._AutoGen.NotificationMessageFactoryAutoGen, INotificationMessageFactory
    {   
    
		
            
       private NotificationMessageFactory()
  	   {
    	   
   	   }
       public IEnumerable<NotificationMessage> GetMessagesThatRequireNotificationToBeSent(int pageNum, int pageSize = 100)
       {
           IEnumerable<NotificationMessage> result = null;
           int maxRetryCount = Constants.GetPaymentNotificationsRetryCounts().Count;
           using (var t = beginTransaction())
           {

               var q = GetQuery(new GetQueryParams(orderByPriority: false));


               q = q.Where(x => !x.SendingFailed &&  //notification must be enabled
                   !x.AcknowledgedByRecipient&&//not already acknoweldged
                   x.RetryCount < maxRetryCount &&  //retry count not over
                   x.NextRetryOn <= CS.General_v3.Util.Date.Now);//next retry is after 'now'

               BusinessLogic_v3.Util.nHibernateUtil.LimitQueryByPrimaryKeys(q, pageNum, pageSize);
               result = FindAll(q);

           }
           return result;
       }
       IEnumerable<INotificationMessage> INotificationMessageFactory.GetMessagesThatRequireNotificationToBeSent(int pageNum, int pageSize = 100)
       {
           return GetMessagesThatRequireNotificationToBeSent(pageNum, pageSize);
       }

    }
}
