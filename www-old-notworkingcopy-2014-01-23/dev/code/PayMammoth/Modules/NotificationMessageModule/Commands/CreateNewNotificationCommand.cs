﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.WebsiteAccountModule;

namespace PayMammoth.Modules.NotificationMessageModule.Commands
{
    public class CreateNewNotificationCommand : BaseDbCommand
    {
        public IWebsiteAccount WebsiteAccount { get; set; }
        public PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE NotificationType { get; set; }

        public string Identifier { get; set; }
        public string Message { get; set; }
        public string SendToUrl { get; set; }

        public Connector.Enums.NOTIFICATION_STATUS_CODE StatusCode { get; set; }
        public IPaymentRequest LinkedPaymentRequest { get; set; }
        public INotificationMessage Output_Notification { get; set; }
        public CreateNewNotificationCommand(Connector.Enums.NOTIFICATION_RESPONSE_TYPE notificationType, IWebsiteAccount websiteAccount,
            string sendToUrl)
        {
            this.NotificationType = notificationType;
            this.WebsiteAccount = websiteAccount;
            this.SendToUrl = sendToUrl;
            this.StatusCode = Connector.Enums.NOTIFICATION_STATUS_CODE.Success;
            if (string.IsNullOrWhiteSpace(this.SendToUrl)) throw new InvalidOperationException("Send To Url cannot be null");


        }




        #region IDbCommand Members

        public override bool Validate()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullableBySelectors(this, x => x.WebsiteAccount, x => x.Identifier);
            return base.Validate();
        }
        
        #endregion

        
    }
}
