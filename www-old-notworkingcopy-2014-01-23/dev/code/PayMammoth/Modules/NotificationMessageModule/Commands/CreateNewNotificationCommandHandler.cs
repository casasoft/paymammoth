﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Classes.PaymentNotifications;

namespace PayMammoth.Modules.NotificationMessageModule.Commands
{
    [IocComponent]
    public class CreateNewNotificationCommandHandler : IDbCommandHandler<CreateNewNotificationCommand>

    {
        #region IDbCommandHandler<CreateNewNotificationCommand> Members

        public INotificationMessage Handle(CreateNewNotificationCommand command)
        {


            INotificationMessage notification = PayMammoth.Modules.NotificationMessageModule.NotificationMessageFactory.Instance.CreateNewItem();
            
            notification.DateCreated = CS.General_v3.Util.Date.Now;
            notification.Identifier = command.Identifier;
            notification.LinkedPaymentRequest = command.LinkedPaymentRequest;
            notification.NotificationType = command.NotificationType;
            notification.Message = command.Message;
            notification.SendToUrl = command.SendToUrl;
            notification.StatusCode = command.StatusCode;
            notification.WebsiteAccount = command.WebsiteAccount;


            notification.Save();
            command.Output_Notification = notification;
            return notification;

        }

        #endregion

        #region IDbCommandHandler<CreateNewNotificationCommand> Members

        void IDbCommandHandler<CreateNewNotificationCommand>.Handle(CreateNewNotificationCommand command)
        {
            Handle(command);
            
        }

        #endregion
    }
}
