using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using PayMammoth.Modules._AutoGen;


namespace PayMammoth.Modules.NotificationMessageModule
{

//IUserClass-File
	
    public interface INotificationMessage : PayMammoth.Modules._AutoGen.INotificationMessageAutoGen
    {

        bool CheckIfStillRequiresNotificationToBeSent();

        void MarkAsAcknowledged(bool autoSave = true);
        void IncrementRetryCount(bool autoSave = true);

       
    }
}
