﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Classes.PaymentNotifications;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.WebsiteAccountModule;

namespace PayMammoth.Modules.NotificationMessageModule
{
    public interface INotificationCreatorAndSender
    {
        INotificationMessage CreateAndQueueNotification(CreateNewNotificationCommand commandParams);
    }

    [IocComponent]
    public class NotificationCreatorAndSender : INotificationCreatorAndSender
    {
        public INotificationMessage CreateAndQueueNotification( CreateNewNotificationCommand commandParams)
        {
            IDbCommandHandler<CreateNewNotificationCommand> cmdHandler = 
                BusinessLogic_v3.Util.InversionOfControlUtil.Get<IDbCommandHandler<CreateNewNotificationCommand>>();
            cmdHandler.Handle(commandParams);
            var notification = commandParams.Output_Notification;
            PayMammoth.Classes.PaymentNotifications.NotificationSender.Instance.StartSendingNotificationForMessage(notification);
            return notification;
            
        }

        
        
    }
}
