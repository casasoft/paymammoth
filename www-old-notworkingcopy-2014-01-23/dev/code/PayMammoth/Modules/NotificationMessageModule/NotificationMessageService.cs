﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;

namespace PayMammoth.Modules.NotificationMessageModule
{
    public interface INotificationMessageService
    {
        void AddToStatusLog(INotificationMessage msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType, string message, Exception ex = null);
    }

    [IocComponent]
    public class NotificationMessageService : INotificationMessageService
    {
        public void AddToStatusLog(INotificationMessage msg,  CS.General_v3.Enums.STATUS_MSG_TYPE msgType, string message, Exception ex = null)
        {
            
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(msg.StatusLog))
            {
                sb.AppendLine();
                sb.AppendLine("=======");
                sb.AppendLine();
            }
            sb.AppendLine(string.Format("{0} - [{1}] (Retry #{2}) {3}", CS.General_v3.Util.Date.Now.ToString("dd/MM/yyyy HH:mm:ss"), msgType, msg.RetryCount, message));
            if (ex != null)
            {
                sb.AppendLine("---");
                sb.AppendLine(CS.General_v3.Util.ErrorsUtil.ConvertExceptionToString(ex));
            }
            msg.StatusLog += sb.ToString();

        }
    }
}
