using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.RecurringProfileModule;

namespace PayMammoth.Modules.RecurringProfileTransactionModule
{   

//IUserFactory-File

	public interface IRecurringProfileTransactionFactory : PayMammoth.Modules._AutoGen.IRecurringProfileTransactionFactoryAutoGen
    {

        IRecurringProfileTransaction GetByPaymentGatewayReferenceAndProfile(string paymentGatewayRef, IRecurringProfile profile);
        
    }

}
