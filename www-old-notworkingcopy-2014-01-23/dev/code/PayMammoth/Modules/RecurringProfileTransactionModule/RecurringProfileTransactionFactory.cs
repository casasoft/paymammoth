using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;

using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.RecurringProfileModule;


namespace PayMammoth.Modules.RecurringProfileTransactionModule
{

    //UserFactory-File

    public class RecurringProfileTransactionFactory : PayMammoth.Modules._AutoGen.RecurringProfileTransactionFactoryAutoGen, IRecurringProfileTransactionFactory
    {



        private RecurringProfileTransactionFactory()
        {

        }


        public IEnumerable<RecurringProfileTransaction> GetRecurringProfileTransactionsThatRequireNotificationsToBeSent(int pageNum, int pageSize = 100)
        {
            throw new InvalidOperationException("not yet implemented");
            //IEnumerable<RecurringProfileTransaction> result = null;
            //var q = GetQuery();
            //int maxRetryCount = Constants.GetPaymentNotificationsRetryCounts().Count;

            //q = q.Where(x => x.NotificationEnabled == true && x.NotificationRetryCount < maxRetryCount);
            //BusinessLogic_v3.Util.nHibernateUtil.LimitQueryByPrimaryKeys(q, pageNum, pageSize);
            //result = FindAll(q);
            //return result;
        }
        public IRecurringProfileTransaction GetByPaymentGatewayReferenceAndProfile(string paymentGatewayRef, IRecurringProfile profile)
        {
            var q = GetQuery(new GetQueryParams(orderByPriority: false));
            q.Where(x => x.PaymentGatewayReference == paymentGatewayRef);
            q.Left.JoinQueryOver(x => x.RecurringProfilePayment).Where(x => x.RecurringProfile.ID == profile.ID);
            return FindItem(q);
        }

    }
}
