using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using NHibernate;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Classes.Interfaces;


namespace PayMammoth.Modules.RecurringProfileTransactionModule
{

    //UserClass-File

    public abstract class RecurringProfileTransaction : PayMammoth.Modules._AutoGen.RecurringProfileTransaction_AutoGen, PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction
    {

        #region UserClass-AutoGenerated
        #endregion



        public RecurringProfileTransaction()
        {
            
        }
        protected override void initPropertiesForNewItem()
        {
            //Fill any default values for NEW items.
            this.Timestamp = Date.Now;
            base.initPropertiesForNewItem();
        }
        public override string ToString()
        {
            return base.ToString();
        }


        //public void DisablePaymentNotifications()
        //{
        //    using (var t = beginTransaction())
        //    {
        //        this.NotificationEnabled = false;
        //        this.Save();
        //        t.Commit();
        //    }
        //}

        //string INotifiable.Identifier
        //{
        //    get { return this.RecurringProfilePayment != null ? this.RecurringProfilePayment.ID.ToString() : null; }
        //}

        //WebsiteAccountModule.WebsiteAccount INotifiable.WebsiteAccount
        //{
        //    get
        //    {
        //        if (this.RecurringProfilePayment != null && this.RecurringProfilePayment.RecurringProfile != null && this.RecurringProfilePayment.RecurringProfile.PaymentTransaction  != null &&
        //            this.RecurringProfilePayment.RecurringProfile.PaymentTransaction.PaymentRequest != null)
        //            return this.RecurringProfilePayment.RecurringProfile.PaymentTransaction.PaymentRequest.WebsiteAccount;
        //        else
        //            return null;
                
        //    }
        //}


        //Connector.Enums.NOTIFICATION_RESPONSE_TYPE INotifiable.NotificationType
        //{
        //    get { return Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPayment; }
        //}


        


        #region IRecurringProfileTransaction Members

        public void AddToStatusLog(CS.General_v3.Enums.LOG4NET_MSG_TYPE msgType, string msg, Exception ex = null)
        {
            this.StatusLog = PayMammoth.Util.GeneralUtil.AddMessageToStringLog(this.StatusLog, msgType, msg, ex); 

            
        }

        #endregion
    }
}




