﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;

namespace PayMammoth.Modules.RecurringProfileModule.Services
{




    //ProfileActivationNotificationSenderService - This is the component name, e.g ProfileActivationNotificationSenderService				

    public interface IProfileActivationNotificationSenderService
    {
        void SendNotification(IRecurringProfile profile);
    }

    [IocComponent]
    public class ProfileActivationNotificationSenderService : IProfileActivationNotificationSenderService
    {

        private readonly INotificationCreatorAndSender notificationSender;
        public ProfileActivationNotificationSenderService(INotificationCreatorAndSender notificationSender)
        {
            this.notificationSender = notificationSender;
        }

        public void SendNotification(IRecurringProfile profile)
        {


            
            

            
			
            string sendToUrl = profile.GetNotificationUrl();
            CreateNewNotificationCommand cmd = new CreateNewNotificationCommand(Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileActivated,
                profile.WebsiteAccount, sendToUrl);
            cmd.Identifier = profile.Identifier;
            cmd.Message = "Profile activated";
            cmd.StatusCode = Connector.Enums.NOTIFICATION_STATUS_CODE.Success;
            
            notificationSender.CreateAndQueueNotification(cmd);

        }


}
			

			
}
