﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.RecurringProfileModule.Commands;

namespace PayMammoth.Modules.RecurringProfileModule.Services
{
    public interface IMarkRecurringProfileAsExpiredService
    {
        void MarkProfileAsExpired(IRecurringProfile profile, string expirationDetails);
    }

    [IocComponent]
    public class MarkRecurringProfileAsExpiredService : IMarkRecurringProfileAsExpiredService
    {
        private readonly INotificationCreatorAndSender notificationCreatorAndSender = null;
        private readonly IDbCommandHandler<MarkProfileAsExpiredCommand> markProfileAsExpiredCommandHandler = null;
        public MarkRecurringProfileAsExpiredService(INotificationCreatorAndSender notificationCreatorAndSender, IDbCommandHandler<MarkProfileAsExpiredCommand> markProfileAsExpiredCommandHandler)
        {
            this.notificationCreatorAndSender = notificationCreatorAndSender;
            this.markProfileAsExpiredCommandHandler = markProfileAsExpiredCommandHandler;
        }

        #region IMarkRecurringProfileAsExpiredService Members

        public void MarkProfileAsExpired(IRecurringProfile profile, string expirationDetails)
        {
            
            {
                MarkProfileAsExpiredCommand cmd = new MarkProfileAsExpiredCommand(profile);
                cmd.ExpirationDetails = expirationDetails;
                this.markProfileAsExpiredCommandHandler.Handle(cmd);
            }
            {
                CreateNewNotificationCommand cmd = new CreateNewNotificationCommand(Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileExpired, profile.WebsiteAccount,
                    profile.GetNotificationUrl());
                cmd.Identifier = profile.Identifier;
                cmd.Message = "Profile expired";
                notificationCreatorAndSender.CreateAndQueueNotification(cmd);
            }
        }

        #endregion
    }
}
