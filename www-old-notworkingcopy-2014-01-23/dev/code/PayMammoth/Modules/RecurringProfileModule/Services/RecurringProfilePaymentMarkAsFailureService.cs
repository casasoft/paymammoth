﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammoth.Modules.RecurringProfileModule.Services
{
    public interface IRecurringProfilePaymentMarkAsFailureService
    {
        IRecurringProfileTransaction MarkPaymentAsFailure(IRecurringProfilePayment payment ,string transactionID,  string failureMsg, bool isFakePayment = false);
    }


    [IocComponent]
    public class RecurringProfilePaymentMarkAsFailureService : IRecurringProfilePaymentMarkAsFailureService
    {
        private readonly IDbCommandHandler<RecurringPaymentMarkAsFailureCommand> recurringPaymentMarkAsFailureCommandHandler = null;
        private readonly IPaymentFailedNotificationSender paymentFailedNotificationSender;
        private readonly IRecurringProfileSuspender recurringProfileSuspender;
        private readonly IRecurringProfileNextRetryDateUpdater nextRetryDateUpdater;
       
        public RecurringProfilePaymentMarkAsFailureService(IDbCommandHandler<RecurringPaymentMarkAsFailureCommand> recurringPaymentMarkAsFailureCommandHandler,
            IPaymentFailedNotificationSender paymentFailedNotificationSender, IRecurringProfileSuspender recurringProfileSuspender,
            IRecurringProfileNextRetryDateUpdater nextRetryDateUpdater)
        {
            this.recurringPaymentMarkAsFailureCommandHandler = recurringPaymentMarkAsFailureCommandHandler;
            this.paymentFailedNotificationSender = paymentFailedNotificationSender;
            this.recurringProfileSuspender = recurringProfileSuspender;
            this.nextRetryDateUpdater = nextRetryDateUpdater;
        }

        private bool hasMaximumFailedAttemptsBeenReached(IRecurringProfilePayment payment)
        {
            return payment.FailureCount >= payment.RecurringProfile.MaximumFailedAttempts;
        }

        public IRecurringProfileTransaction MarkPaymentAsFailure(IRecurringProfilePayment payment, string transactionID, string failureMsg, bool isFakePayment = false)
        {
            var transaction = RecurringProfileTransactionFactory.Instance.GetByPaymentGatewayReferenceAndProfile(transactionID,payment.RecurringProfile);
            if (transaction == null)
            {

                if (payment.Status == Connector.Enums.RecurringProfilePaymentStatus.Pending)
                {
                    RecurringPaymentMarkAsFailureCommand cmd = new RecurringPaymentMarkAsFailureCommand(payment);
                    recurringPaymentMarkAsFailureCommandHandler.Handle(cmd);
                    cmd.FailureData = failureMsg;
                    cmd.TransactionID = transactionID;
                    cmd.IsFakePayment = isFakePayment;
                    var cmdHandler = BusinessLogic_v3.Util.InversionOfControlUtil.Get<IDbCommandHandler<RecurringPaymentMarkAsFailureCommand>>();
                    cmdHandler.Handle(cmd);
                    transaction = cmd.Output_Transaction;
                    if (hasMaximumFailedAttemptsBeenReached(payment))
                    {
                        recurringProfileSuspender.SuspendRecurringProfile(payment.RecurringProfile, cmd.FailureData);
                    }
                    else
                    {
                        paymentFailedNotificationSender.CreateAndQueueNotification(payment, failureMsg);
                        nextRetryDateUpdater.UpdateNextRetryDate(payment);
                    }
                    return transaction;
                }
                else
                {
                    throw new InvalidOperationException(string.Format("Cannot mark payment as failed if status it not Pending. Status: {0} | PaymentID: {1}", payment.Status, payment.ID));

                }
            }
            return transaction;


        }

    }
}
