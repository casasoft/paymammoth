﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.RecurringProfileModule.Commands;

namespace PayMammoth.Modules.RecurringProfileModule.Services
{
    public interface IMarkRecurringProfileAsCancelledService
    {
        void MarkProfileAsCancelled(IRecurringProfile profile, string cancellationDetails);
    }

    [IocComponent]
    public class MarkRecurringProfileAsCancelledService : IMarkRecurringProfileAsCancelledService
    {
        private readonly INotificationCreatorAndSender notificationCreatorAndSender = null;
        private readonly IDbCommandHandler<MarkProfileAsCancelledCommand> markProfileAsCancelledCmdHandler = null;
        public MarkRecurringProfileAsCancelledService(INotificationCreatorAndSender notificationCreatorAndSender, IDbCommandHandler<MarkProfileAsCancelledCommand> markProfileAsCancelledCmdHandler)
        {
            this.notificationCreatorAndSender = notificationCreatorAndSender;
            this.markProfileAsCancelledCmdHandler = markProfileAsCancelledCmdHandler;
        }

        #region IMarkRecurringProfileAsCancelledService Members

        public void MarkProfileAsCancelled(IRecurringProfile profile, string cancellationDetails)
        {
            
            {
                MarkProfileAsCancelledCommand cmd = new MarkProfileAsCancelledCommand(profile);
                cmd.CancellationDetails = cancellationDetails;
                this.markProfileAsCancelledCmdHandler.Handle(cmd);
            }
            {
                CreateNewNotificationCommand cmd = new CreateNewNotificationCommand(Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCancelled, profile.WebsiteAccount,
                    profile.GetNotificationUrl());
                cmd.Identifier = profile.Identifier;
                cmd.Message = "Profile cancelled";
                notificationCreatorAndSender.CreateAndQueueNotification(cmd);
            }
        }

        #endregion
    }
}
