﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Modules.RecurringProfileModule
{
    public interface ISuspendedProfileNotificationSender
    {

        PayMammoth.Modules.NotificationMessageModule.INotificationMessage CreateAndQueueNotification(IRecurringProfile profile);
    }
}
