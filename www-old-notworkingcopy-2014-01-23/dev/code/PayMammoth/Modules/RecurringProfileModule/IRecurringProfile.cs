using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;
using PayMammoth.Modules._AutoGen;


namespace PayMammoth.Modules.RecurringProfileModule
{

    //IUserClass-File

    public interface IRecurringProfile : PayMammoth.Modules._AutoGen.IRecurringProfileAutoGen
    {
       // void ActivateProfile(string details);

       
        
       
        RecurringProfilePaymentModule.IRecurringProfilePayment GetLastPaymentDone();
        //void MarkPaymentReceived(string paymentReference, string paymentDetails, bool isFake= false);
        
       // void AddToStatusLog(CS.General_v3.Enums.LOG4NET_MSG_TYPE msgType, string msg, Exception ex = null);

        void SuspendProfile(string suspensionDetails);
        PaymentRequestModule.IPaymentRequest GetPaymentRequest();
        bool HaveAllBillingCyclesBeenCompleted();
        INotificationMessage CreateNewNotificationMessage();
        string GetNotificationUrl();

        
    }
}
