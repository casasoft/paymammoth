﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammoth.Modules.RecurringProfileModule
{

    public interface IRecurringProfilePaymentMarkLastPaymentAsSuccessful
    {
        IRecurringProfileTransaction MarkAsSuccessful(IRecurringProfilePayment payment, string paymentReference, string paymentDetails);
    }

    [IocComponent]
    public class RecurringProfilePaymentMarkLastPaymentAsSuccessful : IRecurringProfilePaymentMarkLastPaymentAsSuccessful
    {
        private readonly IDbCommandHandler<RecurringPaymentMarkAsSuccessfulCommand> paymentMarkAsSuccessfulCommandHandler = null;
        private readonly IMarkAsSuccessfulNotificationSender successfulNotificationSender = null;
        public RecurringProfilePaymentMarkLastPaymentAsSuccessful(IDbCommandHandler<RecurringPaymentMarkAsSuccessfulCommand> paymentMarkAsSuccessfulCommandHandler, 
            IMarkAsSuccessfulNotificationSender successfulNotificationSender)
        {
            this.paymentMarkAsSuccessfulCommandHandler = paymentMarkAsSuccessfulCommandHandler;
            this.successfulNotificationSender = successfulNotificationSender;

        }




        public IRecurringProfileTransaction MarkAsSuccessful(IRecurringProfilePayment payment, string paymentReference, string paymentDetails)
        {
            var profile = payment.RecurringProfile;
            var transaction = PayMammoth.Modules.Factories.RecurringProfileTransactionFactory.GetByPaymentGatewayReferenceAndProfile(paymentReference, profile);
            if (transaction == null)
            {

                
                
                if (payment == null) throw new InvalidOperationException("Cannot mark a recurring profile as successful if there is no last payment");
                if (payment.Status != Enums.RecurringProfilePaymentStatus.Pending) throw new InvalidOperationException("Cannot mark a recurring profile as successful if the last payment is not marked as Pending");


                RecurringPaymentMarkAsSuccessfulCommand cmd = new RecurringPaymentMarkAsSuccessfulCommand();
                cmd.Payment = payment;
                cmd.PaymentGatewayReference = paymentReference;
                cmd.PaymentDetails = paymentDetails;
                paymentMarkAsSuccessfulCommandHandler.Handle(cmd);

                successfulNotificationSender.SendNotification(payment);



                if (!profile.HaveAllBillingCyclesBeenCompleted())
                {
                    RecurringProfileService.Instance.CreateNextRecurringProfilePayment(profile);

                }
                else
                {
                    profile.SuspendProfile("Profile completed successfully");
                }
                transaction = cmd.Output_Transaction;
            }
            return transaction;


        }

    }
}
