﻿using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfileModule.Services;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammoth.Modules.RecurringProfilePaymentModule.Commands
{
    [IocComponent]
    public class RecurringPaymentMarkAsFailureCommandHandler : IDbCommandHandler<RecurringPaymentMarkAsFailureCommand>
        
    {
        private readonly IRecurringProfileNextRetryDateUpdater nextRetryDateUpdater;
       
        public RecurringPaymentMarkAsFailureCommandHandler()
        {
            this.nextRetryDateUpdater = nextRetryDateUpdater;
            //this.recurringProfileMarkAsFailureService = recurringProfileMarkAsFailureService;
            
            
        }

        private void checkProfileStatusAndThrowErrorIfNotActivated(IRecurringProfile profile)
        {
            if (profile.Status != PayMammoth.Enums.RecurringProfileStatus.Active)
            {
                throw new PayMammoth.Classes.Exceptions.RecurringProfileException("This cannot be called when the profile is not marked as active");
            }
        }


        private void incrementFailureCount(IRecurringProfilePayment payment)
        {
            payment.FailureCount++;
        }

        public void Handle(RecurringPaymentMarkAsFailureCommand command)
        {
            
            var payment = command.Payment;
            var failureData = command.FailureData;

            checkProfileStatusAndThrowErrorIfNotActivated(payment.RecurringProfile);

            IRecurringProfileTransaction transaction = payment.Transactions.CreateNewItem();

            transaction.OtherData = PayMammoth.Util.GeneralUtil.AddMessageToStringLog(transaction.OtherData, CS.General_v3.Enums.LOG4NET_MSG_TYPE.Info, failureData);
            transaction.Status = Enums.RecurringProfileTransactionStatus.Failure;
            transaction.PaymentGatewayReference = command.TransactionID;

            transaction.IsFakePayment = command.IsFakePayment;
            incrementFailureCount(payment);
            
            transaction.Save();
            payment.Save();
            command.Output_Transaction = transaction;
            
        }

    }
}
