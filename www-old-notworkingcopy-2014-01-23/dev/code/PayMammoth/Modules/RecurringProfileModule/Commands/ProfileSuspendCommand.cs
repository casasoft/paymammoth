﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;

namespace PayMammoth.Modules.RecurringProfileModule.Commands
{
    public class ProfileSuspendCommand : BaseDbCommand
    {
        public IRecurringProfile RecurringProfile { get; set; }
        public string SuspensionDetails { get; set; }

        public override bool Validate()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullableBySelectors(this, x => x.RecurringProfile);

            return base.Validate();
        }

    }
}
