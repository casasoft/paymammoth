﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammoth.Modules.RecurringProfileModule.Commands
{


    //MarkRecurringProfileLastPaymentAsSkipped - This is the command name, e.g MarkProfileAsCancelled				

    public class MarkRecurringProfileLastPaymentAsSkippedCommand : BaseDbCommand
    {
        //Any optional parameters go as properties

        public IRecurringProfile Profile { get; set; }
        public string TransactionID { get; set; }
        public string OtherDetails { get; set; }

        public MarkRecurringProfileLastPaymentAsSkippedCommand(IRecurringProfile profile, string transactionID) //any required parameters go in constructor
        {
            this.Profile = profile;
            this.TransactionID = transactionID;

        }
    }

    [IocComponent]
    public class MarkRecurringProfileLastPaymentAsSkippedCommandHandler : IDbCommandHandler<MarkRecurringProfileLastPaymentAsSkippedCommand>
    {

        #region IDbCommandHandler<MarkProfileAsExpiredCommand> Members

        public void Handle(MarkRecurringProfileLastPaymentAsSkippedCommand command)
        {
            
            
            var payment = command.Profile.GetLastPaymentDone();
            if (payment.Status == Connector.Enums.RecurringProfilePaymentStatus.Pending)
            {
                var transaction = payment.Transactions.CreateNewItem();

                transaction.OtherData = PayMammoth.Util.GeneralUtil.AddMessageToStringLog(transaction.OtherData, CS.General_v3.Enums.LOG4NET_MSG_TYPE.Info, "Payment Skipped - " + command.OtherDetails);
                transaction.Status = Enums.RecurringProfileTransactionStatus.Failure;
                transaction.PaymentGatewayReference = command.TransactionID;
                transaction.Save();
            }
            else
            {
                throw new InvalidOperationException(string.Format("Cannot mark a payment as skipped, if payment is not marked as Pending. Status: {0} | PaymentID: {1}", payment.Status, payment.ID));

            }
            

        }

        #endregion
    }        
			
}
