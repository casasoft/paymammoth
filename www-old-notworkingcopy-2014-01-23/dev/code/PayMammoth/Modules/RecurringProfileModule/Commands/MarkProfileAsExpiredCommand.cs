﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;

namespace PayMammoth.Modules.RecurringProfileModule.Commands
{
    public class MarkProfileAsExpiredCommand : BaseDbCommand
    {
        public IRecurringProfile Profile { get; set; }
        public string ExpirationDetails { get; set; }
        public MarkProfileAsExpiredCommand(IRecurringProfile profile)
        {
            this.Profile = profile;
        }
    }

    [IocComponent]
    public class MarkProfileAsExpiredCommandHandler : IDbCommandHandler<MarkProfileAsExpiredCommand>
    {

        #region IDbCommandHandler<MarkProfileAsExpiredCommand> Members

        public void Handle(MarkProfileAsExpiredCommand command)
        {
            
            var profile = command.Profile;
            profile.Status = Enums.RecurringProfileStatus.Expired;
            RecurringProfileService.Instance.AddToStatusLog(profile, CS.General_v3.Enums.LOG4NET_MSG_TYPE.Info, "Profile expired - " + command.ExpirationDetails);
            profile.Save();
            
        }

        #endregion
    }
}
