﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;

namespace PayMammoth.Modules.RecurringProfileModule.Commands
{



    //MarkProfileAsCancelled - This is the command name, e.g MarkProfileAsCancelled				

    public class MarkProfileAsCancelledCommand : BaseDbCommand
    {
        //Any optional parameters go as properties
        public IRecurringProfile Profile { get; set; }
        public string CancellationDetails { get; set; }

        public MarkProfileAsCancelledCommand(IRecurringProfile profile) //any required parameters go in constructor
        {
            this.Profile = profile;
            

        }
    }

    [IocComponent]
    public class MarkProfileAsCancelledCommandHandler : IDbCommandHandler<MarkProfileAsCancelledCommand>
    {

        #region IDbCommandHandler<MarkProfileAsExpiredCommand> Members

        public void Handle(MarkProfileAsCancelledCommand command)
        {

            //handle command here - do not use transactions etc, those are decorated
            var profile = command.Profile;
            profile.Status = Enums.RecurringProfileStatus.Cancelled;
            RecurringProfileService.Instance.AddToStatusLog(profile, CS.General_v3.Enums.LOG4NET_MSG_TYPE.Info, "Profile cancelled - " + command.CancellationDetails);
            profile.Save();
        }

        #endregion
    }        
			

}
