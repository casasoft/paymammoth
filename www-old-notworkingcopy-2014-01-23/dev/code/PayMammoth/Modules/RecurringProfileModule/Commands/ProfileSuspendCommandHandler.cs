﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;

namespace PayMammoth.Modules.RecurringProfileModule.Commands
{
    
    [IocComponent]
    public class ProfileSuspendCommandHandler : IDbCommandHandler<ProfileSuspendCommand>
    {
        #region IDbCommandHandler<ProfileSuspendCommand> Members

        public void Handle(ProfileSuspendCommand command)
        {
            var profile = command.RecurringProfile;
            profile.Status = Enums.RecurringProfileStatus.Suspended;
            RecurringProfileService.Instance.AddToStatusLog(profile,CS.General_v3.Enums.LOG4NET_MSG_TYPE.Info, "Profile suspended - " + command.SuspensionDetails);
            profile.Save();


        }

        #endregion
    }
}
