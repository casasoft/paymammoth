﻿using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammoth.Modules.RecurringProfilePaymentModule.Commands
{
    public class RecurringPaymentMarkAsFailureCommand:BaseDbCommand
    {
        public IRecurringProfilePayment Payment { get; set; }
        public string FailureData { get; set; }
        public IRecurringProfileTransaction Output_Transaction { get; set; }
        public string TransactionID { get; set; }
        public RecurringPaymentMarkAsFailureCommand(IRecurringProfilePayment payment)
        {
            this.Payment = payment;
        }

        #region IDbCommand Members



        #endregion





        public bool IsFakePayment { get; set; }
    }
}
