﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.RecurringProfileModule.Commands;
using PayMammoth.Modules.RecurringProfileModule.Services;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammoth.Modules.RecurringProfileModule
{
    public interface IRecurringProfileService
    {
        void AddToStatusLog(IRecurringProfile profile, CS.General_v3.Enums.LOG4NET_MSG_TYPE msgType, string msg, Exception ex = null);
        void ActivateProfile(IRecurringProfile profile, string details);
        IRecurringProfilePayment CreateNextRecurringProfilePayment(IRecurringProfile profile, bool autoSave = true);
        void MarkProfileAsExpired(IRecurringProfile profile, string expiryDetails);
        void MarkProfileAsCancelled(IRecurringProfile profile, string expiryDetails);
        void MarkLastPaymentAsSkipped(IRecurringProfile profile, string transactionID, string otherDetails);
        void MarkPaymentReceived(IRecurringProfile profile, string paymentReference, string paymentDetails, bool isFakePayment = false);
        RecurringProfileTransactionModule.IRecurringProfileTransaction MarkPaymentAsFailed(IRecurringProfile profile, string transactionID, string failureMsg, bool isFakePayment = false);

    }

    [IocComponent]
    public class RecurringProfileService : IRecurringProfileService
    {



        public static RecurringProfileService Instance
        {
            get { return (RecurringProfileService)BusinessLogic_v3.Util.InversionOfControlUtil.Get<IRecurringProfileService>(); }
        }
        private readonly IRecurringProfilePaymentMarkAsFailureService markAsFailureService = null;
        private readonly IRecurringProfilePaymentMarkLastPaymentAsSuccessfulService markLastPaymentAsSuccessfulService = null;
        private readonly IProfileActivationNotificationSenderService profileActivationNotificationSenderService;
        private readonly IRecurringProfileNextPaymentCreator recurringProfileNextPaymentCreator;
        private readonly INotificationCreatorAndSender notificationCreatorAndSender;
        private readonly IRecurringProfileTransactionFactory recurringProfileTransactionFactory;

        public RecurringProfileService(IRecurringProfilePaymentMarkAsFailureService markAsFailureService, 
            IRecurringProfilePaymentMarkLastPaymentAsSuccessfulService markLastPaymentAsSuccessfulService,
            IProfileActivationNotificationSenderService profileActivationNotificationSenderService,
            IRecurringProfileNextPaymentCreator recurringProfileNextPaymentCreator,
            INotificationCreatorAndSender notificationCreatorAndSender,
            IRecurringProfileTransactionFactory recurringProfileTransactionFactory)
        {
            this.markAsFailureService = markAsFailureService;
            this.markLastPaymentAsSuccessfulService = markLastPaymentAsSuccessfulService;
            this.recurringProfileNextPaymentCreator = recurringProfileNextPaymentCreator;
            this.profileActivationNotificationSenderService = profileActivationNotificationSenderService;
            this.notificationCreatorAndSender = notificationCreatorAndSender;
            this.recurringProfileTransactionFactory = recurringProfileTransactionFactory;
        }

        public void AddToStatusLog(IRecurringProfile profile, CS.General_v3.Enums.LOG4NET_MSG_TYPE msgType, string msg, Exception ex = null)
        {
            profile.StatusLog = PayMammoth.Util.GeneralUtil.AddMessageToStringLog(profile.StatusLog, msgType, msg, ex);

        }

         public void ActivateProfile(IRecurringProfile profile, string details)
        {

            BusinessLogic_v3.Util.nHibernateUtil.RepeatUntilNoStaleObjExceptionIsRaised(
                () =>
                {
                    profile = PayMammoth.Modules.Factories.RecurringProfileFactory.GetByPrimaryKey(profile.ID);

                    profile.Status = Enums.RecurringProfileStatus.Active;
                    profile.ActivatedOn = CS.General_v3.Util.Date.Now;
                    AddToStatusLog(profile, CS.General_v3.Enums.LOG4NET_MSG_TYPE.Info, "Activated profile successfully - " + details);

                    CreateNextRecurringProfilePayment(profile, autoSave: false);
                    profile.Save();

                });



			
             profileActivationNotificationSenderService.SendNotification(profile);


        }

        
         public IRecurringProfilePayment CreateNextRecurringProfilePayment(IRecurringProfile profile, bool autoSave = true)
         {

             var nextPaymentCreator = recurringProfileNextPaymentCreator;
             return nextPaymentCreator.CreateNextRecurringProfilePayment(profile, autoSave: autoSave);

         }


         public void MarkProfileAsExpired(IRecurringProfile profile, string expiryDetails)
         {
             var service = BusinessLogic_v3.Util.InversionOfControlUtil.Get<IMarkRecurringProfileAsExpiredService>();
             service.MarkProfileAsExpired(profile, expiryDetails);
             
         }
         public void MarkProfileAsCancelled(IRecurringProfile profile, string expiryDetails)
         {
             var service = BusinessLogic_v3.Util.InversionOfControlUtil.Get<IMarkRecurringProfileAsCancelledService>();
             service.MarkProfileAsCancelled(profile, expiryDetails);

         }

         public void MarkLastPaymentAsSkipped(IRecurringProfile profile, string transactionID, string otherDetails)
         {
             IRecurringProfileTransaction transaction = this.recurringProfileTransactionFactory.GetByPaymentGatewayReferenceAndProfile(transactionID, profile);
             if (transaction == null)
             {
                 {
                     MarkRecurringProfileLastPaymentAsSkippedCommand cmd = new MarkRecurringProfileLastPaymentAsSkippedCommand(profile, transactionID);
                     cmd.OtherDetails = otherDetails;
                     var cmdHandler = BusinessLogic_v3.Util.InversionOfControlUtil.Get<IDbCommandHandler<MarkRecurringProfileLastPaymentAsSkippedCommand>>();
                     cmdHandler.Handle(cmd);
                 }
                 
   			

                 {
                     CreateNewNotificationCommand cmd = new CreateNewNotificationCommand(
                         Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentSkipped, profile.WebsiteAccount,
                         profile.GetNotificationUrl());
                     cmd.Identifier = profile.Identifier;
                     cmd.StatusCode = Connector.Enums.NOTIFICATION_STATUS_CODE.Success;
                     this.notificationCreatorAndSender.CreateAndQueueNotification(cmd);
                     
                 }


             }
         }
         public void MarkPaymentReceived(IRecurringProfile profile, string paymentReference, string paymentDetails, bool isFakePayment = false)
         {
             var payment = profile.GetLastPaymentDone();
             if (payment != null)
             {
                 IRecurringProfilePaymentMarkLastPaymentAsSuccessfulService successfulMarker = markLastPaymentAsSuccessfulService;
             
                 successfulMarker.MarkAsSuccessful(payment, paymentReference, paymentDetails, isFakePayment);
             }
             else
             {
                 throw new InvalidOperationException("Cannot mark a recurring profile as successful if there is no last payment");

             }
         }
         public RecurringProfileTransactionModule.IRecurringProfileTransaction MarkPaymentAsFailed(IRecurringProfile profile, string transactionID, string failureMsg, bool isFakePayment = false)
         {
             IRecurringProfilePaymentMarkAsFailureService service = markAsFailureService;
             var payment = profile.GetLastPaymentDone();
             var transaction = service.MarkPaymentAsFailure(payment, transactionID, failureMsg, isFakePayment);

             return transaction;
         }

         public void AddFakePaymentTransaction()
         {
             
         }


    }
}
