﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammoth.Modules.RecurringProfileModule
{
    public interface IRecurringProfileNextPaymentCreator
    {
        
        IRecurringProfilePayment CreateNextRecurringProfilePayment(IRecurringProfile profile, bool autoSave = true);

    }
}
