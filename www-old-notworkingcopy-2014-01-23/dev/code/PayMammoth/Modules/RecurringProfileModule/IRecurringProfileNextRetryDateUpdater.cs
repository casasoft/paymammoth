﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Modules.RecurringProfileModule
{
    public interface IRecurringProfileNextRetryDateUpdater
    {

        void UpdateNextRetryDate(RecurringProfilePaymentModule.IRecurringProfilePayment payment);
    }
}
