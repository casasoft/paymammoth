﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Modules.RecurringProfileModule
{
    public interface IRecurringProfileDetails
    {
        PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD RecurringIntervalType { get; set; }
        int RecurringIntervalFrequency { get; set; }
        int MaximumFailedAttempts { get; set; }
        
        /// <summary>
        /// The total amount of billing cycles required.  If 0, it means indefinite
        /// </summary>
        int? TotalBillingCyclesRequired { get; set; }

    }
}
