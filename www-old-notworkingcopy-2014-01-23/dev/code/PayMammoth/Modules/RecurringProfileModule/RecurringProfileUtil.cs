﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Modules.RecurringProfileModule
{
    public static class RecurringProfileUtil
    {
        public static bool HaveAllBillingCyclesBeenCompleted(IRecurringProfile profile, int paymentsDone)
        {
            
            if (profile.TotalBillingCyclesRequired > 0 && paymentsDone >= profile.TotalBillingCyclesRequired)
                return true;
            else
                return false;

        }
    }
}
