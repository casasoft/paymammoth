﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using PayMammoth.Modules.RecurringProfileModule.Commands;

namespace PayMammoth.Modules.RecurringProfileModule.Impl
{
    [IocComponent]
    public class RecurringProfileSuspender : IRecurringProfileSuspender
    {

        private readonly ISuspendedProfileNotificationSender suspendedProfileNotificationSender = null;
        public RecurringProfileSuspender(ISuspendedProfileNotificationSender suspendedProfileNotificationSender)
        {
            this.suspendedProfileNotificationSender = suspendedProfileNotificationSender;
        }


     

        #region IRecurringProfileSuspender Members

        public void SuspendRecurringProfile(IRecurringProfile profile, string suspensionDetails)
        {
            ProfileSuspendCommand cmd = new ProfileSuspendCommand();
            cmd.RecurringProfile = profile;
            cmd.SuspensionDetails = suspensionDetails;
            var profileSuspendCommandHandler = BusinessLogic_v3.Util.InversionOfControlUtil.Get<IDbCommandHandler<ProfileSuspendCommand>>();
            profileSuspendCommandHandler.Handle(cmd);

            suspendedProfileNotificationSender.CreateAndQueueNotification(profile);
        }

        #endregion
    }
}
