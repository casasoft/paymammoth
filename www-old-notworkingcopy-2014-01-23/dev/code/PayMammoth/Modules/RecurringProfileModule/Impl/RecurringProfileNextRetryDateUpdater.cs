﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammoth.Modules.RecurringProfileModule.Impl
{
    [IocComponent]
    public class RecurringProfileNextRetryDateUpdater : IRecurringProfileNextRetryDateUpdater 
    {


        #region IRecurringProfileNextRetryDateUpdater Members

        private DateTime getRelativeDate(IRecurringProfilePayment payment)
        {
            DateTime relativeDate = CS.General_v3.Util.Date.Now;

            if (payment.NextRetryOn.HasValue)
                relativeDate = payment.NextRetryOn.Value;
            return relativeDate;
        }

        private int getNextRetryInterval(RecurringProfilePaymentModule.IRecurringProfilePayment payment, List<int> retryIntervalsInHours)
        {
            int index = 0;
            index = payment.FailureCount - 1;
            if (index < 0) index = 0;
            if (index >= retryIntervalsInHours.Count)
                index = retryIntervalsInHours.Count - 1;
            return retryIntervalsInHours[index];
            
        }

        public void UpdateNextRetryDate(RecurringProfilePaymentModule.IRecurringProfilePayment payment, List<int> retryIntervalsInHours)
        {

            DateTime relativeDate = getRelativeDate(payment);

            int nextIntervalInHours = getNextRetryInterval(payment, retryIntervalsInHours);

            payment.NextRetryOn = relativeDate.AddHours(nextIntervalInHours);
        }

        #endregion

        #region IRecurringProfileNextRetryDateUpdater Members

        public void UpdateNextRetryDate(IRecurringProfilePayment payment)
        {
            string sIntervalList = PayMammoth.Modules.Factories.SettingFactory.GetSettingValue<string>(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.RecurringPayments_PaymentFailedRetryIntervalList);
            var intervalList = CS.General_v3.Util.ListUtil.GetListOfIntFromString(sIntervalList, ",");
            UpdateNextRetryDate(payment, intervalList);
        }

        #endregion
    }
}
