﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Modules.NotificationMessageModule;

namespace PayMammoth.Modules.RecurringProfileModule.Impl
{
    [IocComponent]
    public class SuspendedProfileNotificationSender : ISuspendedProfileNotificationSender
    {

        
          
        #region ISuspendedProfileNotificationSender Members

        public NotificationMessageModule.INotificationMessage CreateAndQueueNotification(IRecurringProfile profile)
        {
            INotificationMessage notification = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                

                notification = profile.CreateNewNotificationMessage();
                notification.NotificationType = Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileSuspended;


                notification.Identifier = profile.Identifier;

                notification.Save();
                t.Commit();
            }
            return notification;

           
        }

        #endregion
    }
}
