﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;

namespace PayMammoth.Modules.RecurringProfileModule.Impl
{
    [IocComponent]
    public class PaymentFailedNotificationSender : IPaymentFailedNotificationSender
    {
        #region IPaymentFailedNotificationSender Members

        private INotificationCreatorAndSender notificationSender;

        public PaymentFailedNotificationSender(INotificationCreatorAndSender notificationSender)
        {
            this.notificationSender = notificationSender;
        }

        public INotificationMessage CreateAndQueueNotification(RecurringProfilePaymentModule.IRecurringProfilePayment payment, string failureDetails)
        {
            var recurringProfile = payment.RecurringProfile;
            
            CreateNewNotificationCommand cmd = new CreateNewNotificationCommand(Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentFailure, recurringProfile.WebsiteAccount, recurringProfile.GetNotificationUrl());
            cmd.Identifier = payment.RecurringProfile.Identifier;
            
            this.notificationSender.CreateAndQueueNotification(cmd);
            var notification = cmd.Output_Notification;

            return notification;
            //var recurringProfile = transaction.RecurringProfilePayment.RecurringProfile;
            //var websiteAccount = recurringProfile.PaymentTransaction.PaymentRequest.WebsiteAccount;
           
            //var notification = websiteAccount.CreateNewNotificationMessage();
            //transaction.LinkedNotificationMessage = notification;
            //notification.NotificationType = Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentFailure;
            //notification.Identifier = transaction.RecurringProfilePayment.RecurringProfile.Identifier;
                
            //notification.Save();
            //transaction.Save();
               
            //return notification;
            

        }

        #endregion

        #region IPaymentFailedNotificationSender Members

       


        #endregion
    }
}
