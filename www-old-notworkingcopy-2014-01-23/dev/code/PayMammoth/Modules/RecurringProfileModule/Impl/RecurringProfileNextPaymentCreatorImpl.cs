﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammoth.Modules.RecurringProfileModule.Impl
{
    [IocComponent]
    public class RecurringProfileNextPaymentCreatorImpl : IRecurringProfileNextPaymentCreator
    {
        #region IRecurringProfileNextPaymentCreator Members


        private DateTime getLastPaymentDate(IRecurringProfile profile)
        {
           
            if (profile.Payments.Any())
            {
                IRecurringProfilePayment payment = profile.GetLastPaymentDone();
                return payment.PaymentDueOn;
            }
            else
            {
                return profile.ActivatedOn.Value;
            }
        }

        public RecurringProfilePaymentModule.IRecurringProfilePayment CreateNextRecurringProfilePayment( IRecurringProfile profile,bool autoSave = true)
        {
           
            

            if (profile.Status != Enums.RecurringProfileStatus.Active) throw new InvalidOperationException("Cannot create a payment if profile is not activated");

            
            DateTime relativeDate = getLastPaymentDate(profile);

            DateTime newDate = PayMammoth.Util.GeneralUtil.AddIntervalToDateTime(relativeDate, profile.RecurringIntervalType, profile.RecurringIntervalFrequency);
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionBasedOnAutoSaveBool(autoSave))
            {
                var newPayment = profile.Payments.CreateNewItem();
                newPayment.PaymentDueOn = newDate;
                newPayment.NextRetryOn = newDate;
                newPayment.InitiatedByPayMammoth = PayMammoth.Util.GeneralUtil.CheckIfRecurringPaymentIsInitiatedByPayMammothForPaymentMethod(profile.PaymentGateway);
                newPayment.MarkAsPaymentFailedIfNotPaidBy = PayMammoth.Util.GeneralUtil.AddIntervalToDateTime(newDate, profile.RecurringIntervalType, profile.RecurringIntervalFrequency); //this should be the next recurring interval
                newPayment.Status = Connector.Enums.RecurringProfilePaymentStatus.Pending;
                newPayment.Save();
                if (t != null) t.Commit();
                return newPayment;
            }
        }

        #endregion

      
    }
}
