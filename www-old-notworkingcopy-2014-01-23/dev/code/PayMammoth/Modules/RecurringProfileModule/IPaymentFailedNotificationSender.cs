﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammoth.Modules.RecurringProfileModule
{
    public interface IPaymentFailedNotificationSender
    {
        INotificationMessage CreateAndQueueNotification(IRecurringProfilePayment payment, string failureDetails);
    }
}
