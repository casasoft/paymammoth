﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammoth.Modules.RecurringProfileModule
{
    public interface IMarkAsSuccessfulNotificationSender
    {
        INotificationMessage SendNotification(IRecurringProfilePayment payment);
    }
    [IocComponent]
    public class MarkAsSuccessfulNotificationSender : IMarkAsSuccessfulNotificationSender
    {
        private INotificationCreatorAndSender notificationSender = null;
        public MarkAsSuccessfulNotificationSender( INotificationCreatorAndSender notificationSender)
        {
            this.notificationSender = notificationSender;

        }

        #region IMarkAsSuccessfulNotificationSender Members

        public INotificationMessage SendNotification(IRecurringProfilePayment payment)
        {
            CreateNewNotificationCommand cmd = new CreateNewNotificationCommand(Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentSuccess, payment.RecurringProfile.WebsiteAccount,
                payment.RecurringProfile.GetNotificationUrl());
            cmd.Identifier = payment.RecurringProfile.Identifier;
            cmd.StatusCode = Connector.Enums.NOTIFICATION_STATUS_CODE.Success;
            
            this.notificationSender.CreateAndQueueNotification(cmd);
            return cmd.Output_Notification;
        }

        #endregion
    }
}
