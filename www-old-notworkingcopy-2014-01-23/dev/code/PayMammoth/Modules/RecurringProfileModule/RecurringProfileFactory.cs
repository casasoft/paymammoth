using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;

using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;



namespace PayMammoth.Modules.RecurringProfileModule
{

    //UserFactory-File

    public class RecurringProfileFactory : PayMammoth.Modules._AutoGen.RecurringProfileFactoryAutoGen, IRecurringProfileFactory
    {
        private RecurringProfileFactory()
        {

        }

        public IRecurringProfile GetRecurringProfileByGatewayIdentifier(string gatewayIdentifier, Connector.Enums.PaymentMethodSpecific gateway)
        {
            var q = GetQuery();
            q.Where(item => item.PaymentGateway == gateway && item.ProfileIdentifierOnGateway == gatewayIdentifier);
            q.Take(1);
            return FindItem(q);
        }

        

        public IRecurringProfile GetRecurringProfileByPayMammothIdentifier(string identifier)
        {
            
            var q = GetQuery();
            q.Where(item => item.Identifier == identifier);
            q.Take(1);
            return FindItem(q);
            
        }

        


    }
}
