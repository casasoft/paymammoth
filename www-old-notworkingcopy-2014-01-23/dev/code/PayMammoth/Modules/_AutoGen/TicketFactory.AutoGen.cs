using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.TicketModule;
using PayMammoth.Modules.TicketModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class TicketFactoryAutoGen : BusinessLogic_v3.Modules.TicketModule.TicketBaseFactory, 
    				PayMammoth.Modules._AutoGen.ITicketFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<ITicket>
    
    {
    
     	public new Ticket ReloadItemFromDatabase(BusinessLogic_v3.Modules.TicketModule.TicketBase item)
        {
         	return (Ticket)base.ReloadItemFromDatabase(item);
        }
     	
        public TicketFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(TicketImpl);
			PayMammoth.Cms.TicketModule.TicketCmsFactory._initialiseStaticInstance();
        }
        public static new TicketFactory Instance
        {
            get
            {
                return (TicketFactory)TicketBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Ticket> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Ticket>();
            
        }
        public new IEnumerable<Ticket> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Ticket>();
            
            
        }
    
        public new IEnumerable<Ticket> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Ticket>();
            
        }
        public new IEnumerable<Ticket> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Ticket>();
        }
        
        public new Ticket FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Ticket)base.FindItem(query);
        }
        public new Ticket FindItem(IQueryOver query)
        {
            return (Ticket)base.FindItem(query);
        }
        
        ITicket ITicketFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Ticket CreateNewItem()
        {
            return (Ticket)base.CreateNewItem();
        }

        public new Ticket GetByPrimaryKey(long pKey)
        {
            return (Ticket)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Ticket, Ticket> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Ticket>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.TicketModule.Ticket GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.TicketModule.Ticket) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.TicketModule.Ticket> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TicketModule.Ticket>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.TicketModule.Ticket> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TicketModule.Ticket>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.TicketModule.Ticket> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TicketModule.Ticket>) base.FindAll(session);

       }
       public new PayMammoth.Modules.TicketModule.Ticket FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.TicketModule.Ticket) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.TicketModule.Ticket FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.TicketModule.Ticket) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.TicketModule.Ticket FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.TicketModule.Ticket) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.TicketModule.Ticket> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TicketModule.Ticket>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.TicketModule.ITicket> Members
     
        PayMammoth.Modules.TicketModule.ITicket IBaseDbFactory<PayMammoth.Modules.TicketModule.ITicket>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.TicketModule.ITicket IBaseDbFactory<PayMammoth.Modules.TicketModule.ITicket>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.TicketModule.ITicket> IBaseDbFactory<PayMammoth.Modules.TicketModule.ITicket>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.TicketModule.ITicket> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.TicketModule.ITicket>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.TicketModule.ITicket> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.TicketModule.ITicket>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.TicketModule.ITicket IBaseDbFactory<PayMammoth.Modules.TicketModule.ITicket>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.TicketModule.ITicket BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.TicketModule.ITicket>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.TicketModule.ITicket BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.TicketModule.ITicket>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.TicketModule.ITicket> IBaseDbFactory<PayMammoth.Modules.TicketModule.ITicket>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ITicket> ITicketFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ITicket> ITicketFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ITicket> ITicketFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       ITicket ITicketFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ITicket ITicketFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ITicket ITicketFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ITicket> ITicketFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ITicket ITicketFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
