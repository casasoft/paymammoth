using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.RecurringProfilesModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.RecurringProfilesModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class RecurringProfilesCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfilesBaseCmsInfo
    {

		public RecurringProfilesCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.RecurringProfilesBase item)
            : base(item)
        {

        }
        

        private RecurringProfilesFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = RecurringProfilesFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new RecurringProfilesFrontend FrontendItem
        {
            get { return (RecurringProfilesFrontend) base.FrontendItem; }
        }
        
        
		public new RecurringProfiles DbItem
        {
            get
            {
                return (RecurringProfiles)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.PaymentTransaction = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles>.GetPropertyBySelector( item => item.PaymentTransaction),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.RecurringProfileses)));

        
        this.ProfileIdentifierOnGateway = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles>.GetPropertyBySelector( item => item.ProfileIdentifierOnGateway),
        						isEditable: true, isVisible: true);

        this.PaymentGateway = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles>.GetPropertyBySelector( item => item.PaymentGateway),
        						isEditable: true, isVisible: true);

        this.PayPippaIdentifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles>.GetPropertyBySelector( item => item.PayPippaIdentifier),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
