using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.WallPrizeLinkModule;
using PayMammoth.Modules.WallPrizeLinkModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WallPrizeLinkFrontend_AutoGen : PayMammoth.Modules._AutoGen.WallPrizeLinkModule.WallPrizeLinkBaseFrontend

    {
		
        
        protected WallPrizeLinkFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink Data
        {
            get { return (PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink)base.Data; }
            set { base.Data = value; }

        }

        public new static WallPrizeLinkFrontend Get(PayMammoth.Modules._AutoGen.IWallPrizeLinkBase data)
        {
            return (WallPrizeLinkFrontend)PayMammoth.Modules._AutoGen.WallPrizeLinkModule.WallPrizeLinkBaseFrontend.Get(data);
        }
        public new static List<WallPrizeLinkFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IWallPrizeLinkBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.WallPrizeLinkModule.WallPrizeLinkBaseFrontend.GetList(dataList).Cast<WallPrizeLinkFrontend>().ToList();
        }


    }
}
