using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule;
using PayMammoth.Modules.ProductVariation_CultureInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariation_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IProductVariation_CultureInfoFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductVariation_CultureInfo>
    
    {
    
     	public new ProductVariation_CultureInfo ReloadItemFromDatabase(BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase item)
        {
         	return (ProductVariation_CultureInfo)base.ReloadItemFromDatabase(item);
        }
     	
        public ProductVariation_CultureInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ProductVariation_CultureInfoImpl);
			PayMammoth.Cms.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoCmsFactory._initialiseStaticInstance();
        }
        public static new ProductVariation_CultureInfoFactory Instance
        {
            get
            {
                return (ProductVariation_CultureInfoFactory)ProductVariation_CultureInfoBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<ProductVariation_CultureInfo> FindAll()
        {
            return base.FindAll().Cast<ProductVariation_CultureInfo>();
        }*/
    /*    public new IEnumerable<ProductVariation_CultureInfo> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<ProductVariation_CultureInfo>();

        }*/
        public new IEnumerable<ProductVariation_CultureInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ProductVariation_CultureInfo>();
            
        }
        public new IEnumerable<ProductVariation_CultureInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ProductVariation_CultureInfo>();
            
            
        }
     /*  public new IEnumerable<ProductVariation_CultureInfo> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<ProductVariation_CultureInfo>();


        }*/
        public new IEnumerable<ProductVariation_CultureInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ProductVariation_CultureInfo>();
            
        }
        public new IEnumerable<ProductVariation_CultureInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ProductVariation_CultureInfo>();
        }
        
        public new ProductVariation_CultureInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ProductVariation_CultureInfo)base.FindItem(query);
        }
        public new ProductVariation_CultureInfo FindItem(IQueryOver query)
        {
            return (ProductVariation_CultureInfo)base.FindItem(query);
        }
        
        IProductVariation_CultureInfo IProductVariation_CultureInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ProductVariation_CultureInfo CreateNewItem()
        {
            return (ProductVariation_CultureInfo)base.CreateNewItem();
        }

        public new ProductVariation_CultureInfo GetByPrimaryKey(long pKey)
        {
            return (ProductVariation_CultureInfo)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<ProductVariation_CultureInfo, ProductVariation_CultureInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<ProductVariation_CultureInfo>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> Members
     
        PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo IBaseDbFactory<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo IBaseDbFactory<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> IBaseDbFactory<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo IBaseDbFactory<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> IBaseDbFactory<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IProductVariation_CultureInfo> IProductVariation_CultureInfoFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductVariation_CultureInfo> IProductVariation_CultureInfoFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductVariation_CultureInfo> IProductVariation_CultureInfoFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IProductVariation_CultureInfo IProductVariation_CultureInfoFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductVariation_CultureInfo IProductVariation_CultureInfoFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductVariation_CultureInfo IProductVariation_CultureInfoFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IProductVariation_CultureInfo> IProductVariation_CultureInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IProductVariation_CultureInfo IProductVariation_CultureInfoFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
