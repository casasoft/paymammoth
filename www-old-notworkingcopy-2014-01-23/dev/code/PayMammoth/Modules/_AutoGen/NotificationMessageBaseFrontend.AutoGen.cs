using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules._AutoGen.NotificationMessageModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Modules._AutoGen.NotificationMessageModule.NotificationMessageBaseFrontend> ToFrontendBaseList(this IEnumerable<PayMammoth.Modules._AutoGen.INotificationMessageBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static PayMammoth.Modules._AutoGen.NotificationMessageModule.NotificationMessageBaseFrontend ToFrontendBase(this PayMammoth.Modules._AutoGen.INotificationMessageBase item)
        {
        	return PayMammoth.Modules._AutoGen.NotificationMessageModule.NotificationMessageBaseFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class NotificationMessageBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<NotificationMessageBaseFrontend, PayMammoth.Modules._AutoGen.INotificationMessageBase>

    {
		
        
        protected NotificationMessageBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
