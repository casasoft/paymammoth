using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.RecurringProfileModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class RecurringProfileCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfileBaseCmsInfo
    {

		public RecurringProfileCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.RecurringProfileBase item)
            : base(item)
        {

        }
        

        private RecurringProfileFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = RecurringProfileFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new RecurringProfileFrontend FrontendItem
        {
            get { return (RecurringProfileFrontend) base.FrontendItem; }
        }
        
        
		public new RecurringProfile DbItem
        {
            get
            {
                return (RecurringProfile)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]

        public CmsCollectionInfo CreatedByPaymentRequests { get; protected set; }
        

        public CmsCollectionInfo Payments { get; protected set; }
        



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.ProfileIdentifierOnGateway = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.ProfileIdentifierOnGateway),
        						isEditable: true, isVisible: true);

        this.PaymentGateway = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.PaymentGateway),
        						isEditable: true, isVisible: true);

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

        this.CreatedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.CreatedOn),
        						isEditable: true, isVisible: true);

        this.BillingCyclesCompleted2 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.BillingCyclesCompleted2),
        						isEditable: true, isVisible: true);

        this.TotalBillingCyclesRequired = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.TotalBillingCyclesRequired),
        						isEditable: true, isVisible: true);

        this.Status = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.Status),
        						isEditable: true, isVisible: true);

        this.MaximumFailedAttempts = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.MaximumFailedAttempts),
        						isEditable: true, isVisible: true);

        this.ActivatedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.ActivatedOn),
        						isEditable: true, isVisible: true);

        this.CancelledOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.CancelledOn),
        						isEditable: true, isVisible: true);

        this.ExpiredOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.ExpiredOn),
        						isEditable: true, isVisible: true);

        this.RecurringIntervalFrequency = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.RecurringIntervalFrequency),
        						isEditable: true, isVisible: true);

        this.RecurringIntervalType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.RecurringIntervalType),
        						isEditable: true, isVisible: true);

        this.StatusLog = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.StatusLog),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.WebsiteAccount = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.WebsiteAccount),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.RecurringProfiles)));

        
        this.NotificationUrl = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.NotificationUrl),
        						isEditable: true, isVisible: true);

        this.CreationStatus = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.CreationStatus),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]

		
		//InitCollectionUserOneToMany
		this.CreatedByPaymentRequests = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector(item => item.CreatedByPaymentRequests),
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector(item => item.RecurringProfileLink)));
		
		//InitCollectionUserOneToMany
		this.Payments = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector(item => item.Payments),
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector(item => item.RecurringProfile)));


			base.initBasicFields();
		}
    
    }
}
