using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;

using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberModule;
using PayMammoth.Modules.MemberModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IMemberFactoryAutoGen : BusinessLogic_v3.Modules.MemberModule.IMemberBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IMember>
    
    {
	    new PayMammoth.Modules.MemberModule.IMember CreateNewItem();
        new PayMammoth.Modules.MemberModule.IMember GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberModule.IMember> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberModule.IMember> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberModule.IMember> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberModule.IMember FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberModule.IMember FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberModule.IMember FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberModule.IMember> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
