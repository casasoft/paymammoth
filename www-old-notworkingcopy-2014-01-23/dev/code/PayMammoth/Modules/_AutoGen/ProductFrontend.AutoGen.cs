using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ProductModule;
using PayMammoth.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ProductModule.ProductFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ProductModule.IProduct> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ProductModule.ProductFrontend ToFrontend(this PayMammoth.Modules.ProductModule.IProduct item)
        {
        	return PayMammoth.Frontend.ProductModule.ProductFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductFrontend_AutoGen : BusinessLogic_v3.Frontend.ProductModule.ProductBaseFrontend

    {
		
        
        protected ProductFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ProductModule.IProduct Data
        {
            get { return (PayMammoth.Modules.ProductModule.IProduct)base.Data; }
            set { base.Data = value; }

        }

        public new static ProductFrontend CreateNewItem()
        {
            return (ProductFrontend)BusinessLogic_v3.Frontend.ProductModule.ProductBaseFrontend.CreateNewItem();
        }
        
        public new static ProductFrontend Get(BusinessLogic_v3.Modules.ProductModule.IProductBase data)
        {
            return (ProductFrontend)BusinessLogic_v3.Frontend.ProductModule.ProductBaseFrontend.Get(data);
        }
        public new static List<ProductFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ProductModule.IProductBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ProductModule.ProductBaseFrontend.GetList(dataList).Cast<ProductFrontend>().ToList();
        }


    }
}
