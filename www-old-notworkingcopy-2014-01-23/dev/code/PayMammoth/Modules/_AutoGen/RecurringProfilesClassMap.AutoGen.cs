using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.RecurringProfilesModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class RecurringProfilesMap_AutoGen : RecurringProfilesMap_AutoGen_Z
    {
        public RecurringProfilesMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.PaymentTransaction, PaymentTransactionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ProfileIdentifierOnGateway, ProfileIdentifierOnGatewayMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGateway, PaymentGatewayMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PayPippaIdentifier, PayPippaIdentifierMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class RecurringProfilesMap_AutoGen_Z : PayMammoth.Modules._AutoGen.RecurringProfilesBaseMap<RecurringProfilesImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void PaymentTransactionMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "PaymentTransactionId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

            
        
        
    }
}
