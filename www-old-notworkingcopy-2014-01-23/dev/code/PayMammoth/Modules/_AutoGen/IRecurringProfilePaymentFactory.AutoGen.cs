using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.RecurringProfilePaymentModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IRecurringProfilePaymentFactoryAutoGen : PayMammoth.Modules._AutoGen.IRecurringProfilePaymentBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IRecurringProfilePayment>
    
    {
	    new PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment CreateNewItem();
        new PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
