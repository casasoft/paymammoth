using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using CS.General_v3.Classes.DbObjects.Collections;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public abstract class PaypalSettingsBaseAutoGen 
    : BusinessLogic_v3.Classes.DB.BaseDbObject, IPaypalSettingsBaseAutoGen
    
      
      
      
    
    
    		
    {
    
   
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	
        public static PayMammoth.Modules._AutoGen.PaypalSettingsBaseCmsFactory CmsFactory
        {
            get { return PayMammoth.Modules._AutoGen.PaypalSettingsBaseCmsFactory.Instance; }
        }
        
     	public static PaypalSettingsBaseFactory Factory
        {
            get
            {
                return PaypalSettingsBaseFactory.Instance; 
            }
        }    
        /*
        public static PaypalSettingsBase CreateNewItem()
        {
            return (PaypalSettingsBase)Factory.CreateNewItem();
        }

		public static PaypalSettingsBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<PaypalSettingsBase, PaypalSettingsBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static PaypalSettingsBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static PaypalSettingsBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<PaypalSettingsBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<PaypalSettingsBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<PaypalSettingsBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? WebsiteAccountID
		{
		 	get 
		 	{
		 		return (this.WebsiteAccount != null ? (long?)this.WebsiteAccount.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.WebsiteAccountBase _websiteaccount;
        public virtual PayMammoth.Modules._AutoGen.WebsiteAccountBase WebsiteAccount 
        {
        	get
        	{
        		return _websiteaccount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_websiteaccount,value);
        		_websiteaccount = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IWebsiteAccountBase PayMammoth.Modules._AutoGen.IPaypalSettingsBaseAutoGen.WebsiteAccount 
        {
            get
            {
            	return this.WebsiteAccount;
            }
            set
            {
            	this.WebsiteAccount = (PayMammoth.Modules._AutoGen.WebsiteAccountBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _enabled;
        public virtual bool Enabled 
        {
        	get
        	{
        		return _enabled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_enabled,value);
        		_enabled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _merchantemail;
        public virtual string MerchantEmail 
        {
        	get
        	{
        		return _merchantemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_merchantemail,value);
        		_merchantemail = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    
#region Multilingual
          


#endregion    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
