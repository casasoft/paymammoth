using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
using PayMammoth.Modules.ImageSizingInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ImageSizingInfoFactoryAutoGen : BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IImageSizingInfoFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IImageSizingInfo>
    
    {
    
     	public new ImageSizingInfo ReloadItemFromDatabase(BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase item)
        {
         	return (ImageSizingInfo)base.ReloadItemFromDatabase(item);
        }
     	static ImageSizingInfoFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public ImageSizingInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ImageSizingInfoImpl);
			PayMammoth.Cms.ImageSizingInfoModule.ImageSizingInfoCmsFactory._initialiseStaticInstance();
        }
        public static new ImageSizingInfoFactory Instance
        {
            get
            {
                return (ImageSizingInfoFactory)ImageSizingInfoBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<ImageSizingInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ImageSizingInfo>();
            
        }
        public new IEnumerable<ImageSizingInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ImageSizingInfo>();
            
            
        }
    
        public new IEnumerable<ImageSizingInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ImageSizingInfo>();
            
        }
        public new IEnumerable<ImageSizingInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ImageSizingInfo>();
        }
        
        public new ImageSizingInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ImageSizingInfo)base.FindItem(query);
        }
        public new ImageSizingInfo FindItem(IQueryOver query)
        {
            return (ImageSizingInfo)base.FindItem(query);
        }
        
        IImageSizingInfo IImageSizingInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ImageSizingInfo CreateNewItem()
        {
            return (ImageSizingInfo)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<ImageSizingInfo, ImageSizingInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ImageSizingInfo>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo> Members
     
        PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo IBaseDbFactory<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo IBaseDbFactory<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo> IBaseDbFactory<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo IBaseDbFactory<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo> IBaseDbFactory<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IImageSizingInfo> IImageSizingInfoFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IImageSizingInfo> IImageSizingInfoFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IImageSizingInfo> IImageSizingInfoFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IImageSizingInfo IImageSizingInfoFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IImageSizingInfo IImageSizingInfoFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IImageSizingInfo IImageSizingInfoFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IImageSizingInfo> IImageSizingInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IImageSizingInfo IImageSizingInfoFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
