using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace PayMammoth.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "RecurringProfileTransaction")]
    public abstract class RecurringProfileTransactionBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IRecurringProfileTransactionBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static RecurringProfileTransactionBaseFactory Factory
        {
            get
            {
                return RecurringProfileTransactionBaseFactory.Instance; 
            }
        }    
        /*
        public static RecurringProfileTransactionBase CreateNewItem()
        {
            return (RecurringProfileTransactionBase)Factory.CreateNewItem();
        }

		public static RecurringProfileTransactionBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<RecurringProfileTransactionBase, RecurringProfileTransactionBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static RecurringProfileTransactionBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static RecurringProfileTransactionBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<RecurringProfileTransactionBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<RecurringProfileTransactionBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<RecurringProfileTransactionBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? RecurringProfilePaymentID
		{
		 	get 
		 	{
		 		return (this.RecurringProfilePayment != null ? (long?)this.RecurringProfilePayment.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.RecurringProfilePaymentBase _recurringprofilepayment;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Modules._AutoGen.RecurringProfilePaymentBase RecurringProfilePayment 
        {
        	get
        	{
        		return _recurringprofilepayment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_recurringprofilepayment,value);
        		_recurringprofilepayment = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IRecurringProfilePaymentBase PayMammoth.Modules._AutoGen.IRecurringProfileTransactionBaseAutoGen.RecurringProfilePayment 
        {
            get
            {
            	return this.RecurringProfilePayment;
            }
            set
            {
            	this.RecurringProfilePayment = (PayMammoth.Modules._AutoGen.RecurringProfilePaymentBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private DateTime _timestamp;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime Timestamp 
        {
        	get
        	{
        		return _timestamp;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_timestamp,value);
        		_timestamp = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _responsecontent;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string ResponseContent 
        {
        	get
        	{
        		return _responsecontent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_responsecontent,value);
        		_responsecontent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayreference;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string PaymentGatewayReference 
        {
        	get
        	{
        		return _paymentgatewayreference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayreference,value);
        		_paymentgatewayreference = value;
        		
        	}
        }
        
/*
		public virtual long? LinkedNotificationMessageID
		{
		 	get 
		 	{
		 		return (this.LinkedNotificationMessage != null ? (long?)this.LinkedNotificationMessage.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.NotificationMessageBase _linkednotificationmessage;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Modules._AutoGen.NotificationMessageBase LinkedNotificationMessage 
        {
        	get
        	{
        		return _linkednotificationmessage;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkednotificationmessage,value);
        		_linkednotificationmessage = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.INotificationMessageBase PayMammoth.Modules._AutoGen.IRecurringProfileTransactionBaseAutoGen.LinkedNotificationMessage 
        {
            get
            {
            	return this.LinkedNotificationMessage;
            }
            set
            {
            	this.LinkedNotificationMessage = (PayMammoth.Modules._AutoGen.NotificationMessageBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private double _totalamount;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual double TotalAmount 
        {
        	get
        	{
        		return _totalamount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_totalamount,value);
        		_totalamount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _otherdata;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string OtherData 
        {
        	get
        	{
        		return _otherdata;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_otherdata,value);
        		_otherdata = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private PayMammoth.Enums.RecurringProfileTransactionStatus _status;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Enums.RecurringProfileTransactionStatus Status 
        {
        	get
        	{
        		return _status;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_status,value);
        		_status = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _statuslog;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string StatusLog 
        {
        	get
        	{
        		return _statuslog;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_statuslog,value);
        		_statuslog = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isfakepayment;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual bool IsFakePayment 
        {
        	get
        	{
        		return _isfakepayment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isfakepayment,value);
        		_isfakepayment = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
