using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WallBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.WallBase>
    {
		public WallBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.WallBase dbItem)
            : base(PayMammoth.Modules._AutoGen.WallBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new WallBase DbItem
        {
            get
            {
                return (WallBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo StartDate { get; private set; }

        public CmsPropertyInfo ClosedOn { get; private set; }

        public CmsPropertyInfo LotteryDrawDate { get; private set; }

        public CmsPropertyInfo StarsRequiredForTicket { get; private set; }

        public CmsPropertyInfo IsFinalised { get; private set; }

        public CmsPropertyInfo FinalisedOn { get; private set; }

        public CmsPropertyInfo LotteryHasBeenDrawn { get; private set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.WallBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.WallBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.StartDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.WallBase>.GetPropertyBySelector( item => item.StartDate),
        			isEditable: true,
        			isVisible: true
        			);

        this.ClosedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.WallBase>.GetPropertyBySelector( item => item.ClosedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.LotteryDrawDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.WallBase>.GetPropertyBySelector( item => item.LotteryDrawDate),
        			isEditable: true,
        			isVisible: true
        			);

        this.StarsRequiredForTicket = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.WallBase>.GetPropertyBySelector( item => item.StarsRequiredForTicket),
        			isEditable: true,
        			isVisible: true
        			);

        this.IsFinalised = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.WallBase>.GetPropertyBySelector( item => item.IsFinalised),
        			isEditable: true,
        			isVisible: true
        			);

        this.FinalisedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.WallBase>.GetPropertyBySelector( item => item.FinalisedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.LotteryHasBeenDrawn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.WallBase>.GetPropertyBySelector( item => item.LotteryHasBeenDrawn),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
