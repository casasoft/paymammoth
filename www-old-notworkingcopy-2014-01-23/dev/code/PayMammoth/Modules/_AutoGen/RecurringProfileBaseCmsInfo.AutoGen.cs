using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen.RecurringProfileModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfileBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.RecurringProfileBase>
    {
		public RecurringProfileBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.RecurringProfileBase dbItem)
            : base(PayMammoth.Modules._AutoGen.RecurringProfileBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new RecurringProfileBaseFrontend FrontendItem
        {
            get { return (RecurringProfileBaseFrontend)base.FrontendItem; }

        }
        public new RecurringProfileBase DbItem
        {
            get
            {
                return (RecurringProfileBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ProfileIdentifierOnGateway { get; protected set; }

        public CmsPropertyInfo PaymentGateway { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo CreatedOn { get; protected set; }

        public CmsPropertyInfo BillingCyclesCompleted2 { get; protected set; }

        public CmsPropertyInfo TotalBillingCyclesRequired { get; protected set; }

        public CmsPropertyInfo Status { get; protected set; }

        public CmsPropertyInfo MaximumFailedAttempts { get; protected set; }

        public CmsPropertyInfo ActivatedOn { get; protected set; }

        public CmsPropertyInfo CancelledOn { get; protected set; }

        public CmsPropertyInfo ExpiredOn { get; protected set; }

        public CmsPropertyInfo RecurringIntervalFrequency { get; protected set; }

        public CmsPropertyInfo RecurringIntervalType { get; protected set; }

        public CmsPropertyInfo StatusLog { get; protected set; }

        public CmsPropertyInfo WebsiteAccount { get; protected set; }

        public CmsPropertyInfo NotificationUrl { get; protected set; }

        public CmsPropertyInfo CreationStatus { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
