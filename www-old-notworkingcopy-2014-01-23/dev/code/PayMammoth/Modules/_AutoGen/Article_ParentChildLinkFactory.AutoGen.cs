using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using PayMammoth.Modules.Article_ParentChildLinkModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Article_ParentChildLinkFactoryAutoGen : BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBaseFactory, 
    				PayMammoth.Modules._AutoGen.IArticle_ParentChildLinkFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IArticle_ParentChildLink>
    
    {
    
     	public new Article_ParentChildLink ReloadItemFromDatabase(BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase item)
        {
         	return (Article_ParentChildLink)base.ReloadItemFromDatabase(item);
        }
     	static Article_ParentChildLinkFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public Article_ParentChildLinkFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(Article_ParentChildLinkImpl);
			PayMammoth.Cms.Article_ParentChildLinkModule.Article_ParentChildLinkCmsFactory._initialiseStaticInstance();
        }
        public static new Article_ParentChildLinkFactory Instance
        {
            get
            {
                return (Article_ParentChildLinkFactory)Article_ParentChildLinkBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Article_ParentChildLink> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Article_ParentChildLink>();
            
        }
        public new IEnumerable<Article_ParentChildLink> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Article_ParentChildLink>();
            
            
        }
    
        public new IEnumerable<Article_ParentChildLink> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Article_ParentChildLink>();
            
        }
        public new IEnumerable<Article_ParentChildLink> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Article_ParentChildLink>();
        }
        
        public new Article_ParentChildLink FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Article_ParentChildLink)base.FindItem(query);
        }
        public new Article_ParentChildLink FindItem(IQueryOver query)
        {
            return (Article_ParentChildLink)base.FindItem(query);
        }
        
        IArticle_ParentChildLink IArticle_ParentChildLinkFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Article_ParentChildLink CreateNewItem()
        {
            return (Article_ParentChildLink)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<Article_ParentChildLink, Article_ParentChildLink> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Article_ParentChildLink>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>) base.FindAll(session);

       }
       public new PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> Members
     
        PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink IBaseDbFactory<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink IBaseDbFactory<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> IBaseDbFactory<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink IBaseDbFactory<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> IBaseDbFactory<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IArticle_ParentChildLink> IArticle_ParentChildLinkFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticle_ParentChildLink> IArticle_ParentChildLinkFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticle_ParentChildLink> IArticle_ParentChildLinkFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IArticle_ParentChildLink IArticle_ParentChildLinkFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticle_ParentChildLink IArticle_ParentChildLinkFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticle_ParentChildLink IArticle_ParentChildLinkFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IArticle_ParentChildLink> IArticle_ParentChildLinkFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IArticle_ParentChildLink IArticle_ParentChildLinkFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
