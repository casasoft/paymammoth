using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ArticleModule;
using PayMammoth.Modules.ArticleModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IArticleFactoryAutoGen : BusinessLogic_v3.Modules.ArticleModule.IArticleBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IArticle>
    
    {
	    new PayMammoth.Modules.ArticleModule.IArticle CreateNewItem();
        new PayMammoth.Modules.ArticleModule.IArticle GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.ArticleModule.IArticle> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleModule.IArticle> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleModule.IArticle> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleModule.IArticle FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleModule.IArticle FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleModule.IArticle FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleModule.IArticle> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
