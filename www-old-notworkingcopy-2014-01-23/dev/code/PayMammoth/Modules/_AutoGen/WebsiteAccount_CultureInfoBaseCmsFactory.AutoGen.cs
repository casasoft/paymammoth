using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace PayMammoth.Modules._AutoGen
{
	public abstract class WebsiteAccount_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<WebsiteAccount_CultureInfoBaseCmsInfo, WebsiteAccount_CultureInfoBase>
    {
       
       public new static WebsiteAccount_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (WebsiteAccount_CultureInfoBaseCmsFactory)CmsFactoryBase<WebsiteAccount_CultureInfoBaseCmsInfo, WebsiteAccount_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = WebsiteAccount_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "WebsiteAccount_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            if (string.IsNullOrWhiteSpace(cmsInfo.Name )) cmsInfo.Name = "WebsiteAccount_CultureInfo";

            if (string.IsNullOrWhiteSpace(this.QueryStringParamID )) this.QueryStringParamID = "WebsiteAccount_CultureInfoId";

            if (string.IsNullOrWhiteSpace(cmsInfo.TitlePlural)) cmsInfo.TitlePlural = "Website Account_ Culture Infos";

            if (string.IsNullOrWhiteSpace(cmsInfo.TitleSingular )) cmsInfo.TitleSingular =  "Website Account_ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public WebsiteAccount_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "WebsiteAccount_CultureInfo/";
			UsedInProject = WebsiteAccount_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
