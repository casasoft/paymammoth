using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.MemberResponsibleLimitsModule;
using BusinessLogic_v3.Modules.MemberResponsibleLimitsModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberResponsibleLimits_AutoGen : MemberResponsibleLimitsBase, PayMammoth.Modules._AutoGen.IMemberResponsibleLimitsAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.MemberModule.Member Member
        {
            get
            {
                return (PayMammoth.Modules.MemberModule.Member)base.Member;
            }
            set
            {
                base.Member = value;
            }
        }

		PayMammoth.Modules.MemberModule.IMember IMemberResponsibleLimitsAutoGen.Member
        {
            get
            {
            	return this.Member;
                
            }
            set
            {
                this.Member = (PayMammoth.Modules.MemberModule.Member)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new MemberResponsibleLimitsFactory Factory
        {
            get
            {
                return (MemberResponsibleLimitsFactory)MemberResponsibleLimitsBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<MemberResponsibleLimits, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
