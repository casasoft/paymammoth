using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.Article_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class Article_CultureInfoMap_AutoGen : Article_CultureInfoMap_AutoGen_Z
    {
        public Article_CultureInfoMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.CultureInfo, CultureInfoMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Article, ArticleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HtmlText, HtmlTextMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HtmlText_Search, HtmlText_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaKeywords, MetaKeywordsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaKeywords_Search, MetaKeywords_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaDescription, MetaDescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaDescription_Search, MetaDescription_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PageTitle, PageTitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PageTitle_Search, PageTitle_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DialogWidth, DialogWidthMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DialogHeight, DialogHeightMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SubTitle, SubTitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item._ComputedURL, _ComputedURLMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item._Computed_ParentArticleNames, _Computed_ParentArticleNamesMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaTitle, MetaTitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Tags, TagsMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class Article_CultureInfoMap_AutoGen_Z : BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBaseMap<Article_CultureInfoImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
