using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.RecurringProfilesModule;
using PayMammoth.Modules.RecurringProfilesModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.RecurringProfilesModule.RecurringProfilesFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.RecurringProfilesModule.RecurringProfilesFrontend ToFrontend(this PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles item)
        {
        	return PayMammoth.Frontend.RecurringProfilesModule.RecurringProfilesFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfilesFrontend_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfilesModule.RecurringProfilesBaseFrontend

    {
		
        
        protected RecurringProfilesFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles Data
        {
            get { return (PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles)base.Data; }
            set { base.Data = value; }

        }

        public new static RecurringProfilesFrontend CreateNewItem()
        {
            return (RecurringProfilesFrontend)PayMammoth.Modules._AutoGen.RecurringProfilesModule.RecurringProfilesBaseFrontend.CreateNewItem();
        }
        
        public new static RecurringProfilesFrontend Get(PayMammoth.Modules._AutoGen.IRecurringProfilesBase data)
        {
            return (RecurringProfilesFrontend)PayMammoth.Modules._AutoGen.RecurringProfilesModule.RecurringProfilesBaseFrontend.Get(data);
        }
        public new static List<RecurringProfilesFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IRecurringProfilesBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.RecurringProfilesModule.RecurringProfilesBaseFrontend.GetList(dataList).Cast<RecurringProfilesFrontend>().ToList();
        }


    }
}
