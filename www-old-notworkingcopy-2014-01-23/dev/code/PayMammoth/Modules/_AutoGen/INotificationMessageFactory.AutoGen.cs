using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.NotificationMessageModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface INotificationMessageFactoryAutoGen : PayMammoth.Modules._AutoGen.INotificationMessageBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<INotificationMessage>
    
    {
	    new PayMammoth.Modules.NotificationMessageModule.INotificationMessage CreateNewItem();
        new PayMammoth.Modules.NotificationMessageModule.INotificationMessage GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.NotificationMessageModule.INotificationMessage> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.NotificationMessageModule.INotificationMessage> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.NotificationMessageModule.INotificationMessage> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.NotificationMessageModule.INotificationMessage FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.NotificationMessageModule.INotificationMessage FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.NotificationMessageModule.INotificationMessage FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.NotificationMessageModule.INotificationMessage> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
