using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentMethodModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentMethodFactoryAutoGen : PayMammoth.Modules._AutoGen.PaymentMethodBaseFactory, 
    				PayMammoth.Modules._AutoGen.IPaymentMethodFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IPaymentMethod>
    
    {
    
     	public new PaymentMethod ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.PaymentMethodBase item)
        {
         	return (PaymentMethod)base.ReloadItemFromDatabase(item);
        }
     	static PaymentMethodFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public PaymentMethodFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(PaymentMethodImpl);
			PayMammoth.Cms.PaymentMethodModule.PaymentMethodCmsFactory._initialiseStaticInstance();
        }
        public static new PaymentMethodFactory Instance
        {
            get
            {
                return (PaymentMethodFactory)PaymentMethodBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<PaymentMethod> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<PaymentMethod>();
            
        }
        public new IEnumerable<PaymentMethod> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<PaymentMethod>();
            
            
        }
    
        public new IEnumerable<PaymentMethod> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<PaymentMethod>();
            
        }
        public new IEnumerable<PaymentMethod> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<PaymentMethod>();
        }
        
        public new PaymentMethod FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (PaymentMethod)base.FindItem(query);
        }
        public new PaymentMethod FindItem(IQueryOver query)
        {
            return (PaymentMethod)base.FindItem(query);
        }
        
        IPaymentMethod IPaymentMethodFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new PaymentMethod CreateNewItem()
        {
            return (PaymentMethod)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<PaymentMethod, PaymentMethod> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<PaymentMethod>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

            var itemList = PaymentMethod.Factory.FindAll();

            var CultureInfos = CultureDetails.Factory.FindAll();

            foreach (var item in itemList)
            {
            	item.__CheckItemCultureInfos(autoSaveInDB: true, CultureInfos: CultureInfos);
            }

  

        }        


#endregion
        public new PayMammoth.Modules.PaymentMethodModule.PaymentMethod GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.PaymentMethodModule.PaymentMethod) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.PaymentMethodModule.PaymentMethod> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentMethodModule.PaymentMethod>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentMethodModule.PaymentMethod> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentMethodModule.PaymentMethod>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentMethodModule.PaymentMethod> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentMethodModule.PaymentMethod>) base.FindAll(session);

       }
       public new PayMammoth.Modules.PaymentMethodModule.PaymentMethod FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentMethodModule.PaymentMethod) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.PaymentMethodModule.PaymentMethod FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentMethodModule.PaymentMethod) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.PaymentMethodModule.PaymentMethod FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentMethodModule.PaymentMethod) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentMethodModule.PaymentMethod> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentMethodModule.PaymentMethod>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod> Members
     
        PayMammoth.Modules.PaymentMethodModule.IPaymentMethod IBaseDbFactory<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.PaymentMethodModule.IPaymentMethod IBaseDbFactory<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod> IBaseDbFactory<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.PaymentMethodModule.IPaymentMethod IBaseDbFactory<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentMethodModule.IPaymentMethod BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentMethodModule.IPaymentMethod BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod> IBaseDbFactory<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IPaymentMethod> IPaymentMethodFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentMethod> IPaymentMethodFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentMethod> IPaymentMethodFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IPaymentMethod IPaymentMethodFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentMethod IPaymentMethodFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentMethod IPaymentMethodFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IPaymentMethod> IPaymentMethodFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IPaymentMethod IPaymentMethodFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
