using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;
using PayMammoth.Modules._AutoGen;


namespace PayMammoth.Modules._AutoGen
{
   
    public interface IRecurringProfileBaseFactoryAutoGen : IBaseDbFactory<PayMammoth.Modules._AutoGen.IRecurringProfileBase>
    {
    
    }
}
