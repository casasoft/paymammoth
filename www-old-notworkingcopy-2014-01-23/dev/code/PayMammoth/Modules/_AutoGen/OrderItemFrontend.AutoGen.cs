using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.OrderItemModule;
using PayMammoth.Modules.OrderItemModule;
using BusinessLogic_v3.Modules.OrderItemModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.OrderItemModule.OrderItemFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.OrderItemModule.IOrderItem> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.OrderItemModule.OrderItemFrontend ToFrontend(this PayMammoth.Modules.OrderItemModule.IOrderItem item)
        {
        	return PayMammoth.Frontend.OrderItemModule.OrderItemFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class OrderItemFrontend_AutoGen : BusinessLogic_v3.Frontend.OrderItemModule.OrderItemBaseFrontend

    {
		
        
        protected OrderItemFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.OrderItemModule.IOrderItem Data
        {
            get { return (PayMammoth.Modules.OrderItemModule.IOrderItem)base.Data; }
            set { base.Data = value; }

        }

        public new static OrderItemFrontend CreateNewItem()
        {
            return (OrderItemFrontend)BusinessLogic_v3.Frontend.OrderItemModule.OrderItemBaseFrontend.CreateNewItem();
        }
        
        public new static OrderItemFrontend Get(BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase data)
        {
            return (OrderItemFrontend)BusinessLogic_v3.Frontend.OrderItemModule.OrderItemBaseFrontend.Get(data);
        }
        public new static List<OrderItemFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase> dataList)
        {
            return BusinessLogic_v3.Frontend.OrderItemModule.OrderItemBaseFrontend.GetList(dataList).Cast<OrderItemFrontend>().ToList();
        }


    }
}
