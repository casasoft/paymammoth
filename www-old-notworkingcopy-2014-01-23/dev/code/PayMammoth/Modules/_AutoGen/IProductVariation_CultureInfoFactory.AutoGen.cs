using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;

using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule;
using PayMammoth.Modules.ProductVariation_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IProductVariation_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfoBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductVariation_CultureInfo>
    
    {
	    new PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo CreateNewItem();
        new PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
