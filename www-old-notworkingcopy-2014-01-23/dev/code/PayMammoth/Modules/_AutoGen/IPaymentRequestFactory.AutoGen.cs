using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentRequestModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IPaymentRequestFactoryAutoGen : PayMammoth.Modules._AutoGen.IPaymentRequestBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IPaymentRequest>
    
    {
	    new PayMammoth.Modules.PaymentRequestModule.IPaymentRequest CreateNewItem();
        new PayMammoth.Modules.PaymentRequestModule.IPaymentRequest GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestModule.IPaymentRequest FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestModule.IPaymentRequest FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestModule.IPaymentRequest FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
