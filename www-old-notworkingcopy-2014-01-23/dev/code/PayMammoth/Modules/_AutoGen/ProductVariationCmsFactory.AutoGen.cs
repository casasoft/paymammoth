using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ProductVariationModule;
using PayMammoth.Cms.ProductVariationModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariationCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductVariationModule.ProductVariationBaseCmsFactory
    {
        public static new ProductVariationBaseCmsFactory Instance 
        {
         	get { return (ProductVariationBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ProductVariationModule.ProductVariationBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ProductVariationBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = ProductVariation.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            ProductVariationCmsInfo cmsItem = new ProductVariationCmsInfo(item);
            return cmsItem;
        }    
        
        public override ProductVariationBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase item)
        {
            return new ProductVariationCmsInfo((ProductVariation)item);
            
        }

    }
}
