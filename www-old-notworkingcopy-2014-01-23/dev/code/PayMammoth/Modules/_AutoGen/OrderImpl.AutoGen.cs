using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using NHibernate;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.OrderModule;

namespace PayMammoth.Modules._AutoGen
{

//UserClassImpl-File

    public class OrderImpl : PayMammoth.Modules.OrderModule.Order
    {
/*
 		public new CmsUserImpl LastEditedBy
        {
            get { return (CmsUserImpl) base.LastEditedBy; }
            set { base.LastEditedBy = value; }
        }
        public new CmsUserImpl DeletedBy
        {
            get { return (CmsUserImpl)base.DeletedBy; }
            set { base.DeletedBy = value; }
        }
*/
		#region UserClass-AutoGenerated
		#endregion

      
      
        private OrderImpl()
        {
            
        }    
    	

// [userclassimpl_collections]

        //UserClassImpl_CollectionOneToManyLeftSide
        
        protected override IEnumerable<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase> __collection__OrderItems
        {
            get
            {
                return this._OrderItems;
            }
        }

        private Iesi.Collections.Generic.ISet<OrderItemImpl> _OrderItems = new Iesi.Collections.Generic.HashedSet<OrderItemImpl>();
        public new virtual Iesi.Collections.Generic.ISet<OrderItemImpl> OrderItems
        {
            get { return _OrderItems; }

        }
    	


    	
        
    }
}




