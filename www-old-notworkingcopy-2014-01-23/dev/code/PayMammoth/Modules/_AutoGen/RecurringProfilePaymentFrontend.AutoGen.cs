using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.RecurringProfilePaymentModule;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.RecurringProfilePaymentModule.RecurringProfilePaymentFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.RecurringProfilePaymentModule.RecurringProfilePaymentFrontend ToFrontend(this PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment item)
        {
        	return PayMammoth.Frontend.RecurringProfilePaymentModule.RecurringProfilePaymentFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfilePaymentFrontend_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfilePaymentModule.RecurringProfilePaymentBaseFrontend

    {
		
        
        protected RecurringProfilePaymentFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment Data
        {
            get { return (PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment)base.Data; }
            set { base.Data = value; }

        }

        public new static RecurringProfilePaymentFrontend CreateNewItem()
        {
            return (RecurringProfilePaymentFrontend)PayMammoth.Modules._AutoGen.RecurringProfilePaymentModule.RecurringProfilePaymentBaseFrontend.CreateNewItem();
        }
        
        public new static RecurringProfilePaymentFrontend Get(PayMammoth.Modules._AutoGen.IRecurringProfilePaymentBase data)
        {
            return (RecurringProfilePaymentFrontend)PayMammoth.Modules._AutoGen.RecurringProfilePaymentModule.RecurringProfilePaymentBaseFrontend.Get(data);
        }
        public new static List<RecurringProfilePaymentFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IRecurringProfilePaymentBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.RecurringProfilePaymentModule.RecurringProfilePaymentBaseFrontend.GetList(dataList).Cast<RecurringProfilePaymentFrontend>().ToList();
        }


    }
}
