using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ContentText_CultureInfoModule;
using BusinessLogic_v3.Modules.ContentText_CultureInfoModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class ContentText_CultureInfo_AutoGen : ContentText_CultureInfoBase, PayMammoth.Modules._AutoGen.IContentText_CultureInfoAutoGen
	
      
      
      , IMultilingualContentInfo
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.CultureDetailsModule.CultureDetails CultureInfo
        {
            get
            {
                return (PayMammoth.Modules.CultureDetailsModule.CultureDetails)base.CultureInfo;
            }
            set
            {
                base.CultureInfo = value;
            }
        }

		PayMammoth.Modules.CultureDetailsModule.ICultureDetails IContentText_CultureInfoAutoGen.CultureInfo
        {
            get
            {
            	return this.CultureInfo;
                
            }
            set
            {
                this.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.ContentTextModule.ContentText ContentText
        {
            get
            {
                return (PayMammoth.Modules.ContentTextModule.ContentText)base.ContentText;
            }
            set
            {
                base.ContentText = value;
            }
        }

		PayMammoth.Modules.ContentTextModule.IContentText IContentText_CultureInfoAutoGen.ContentText
        {
            get
            {
            	return this.ContentText;
                
            }
            set
            {
                this.ContentText = (PayMammoth.Modules.ContentTextModule.ContentText)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new ContentText_CultureInfoFactory Factory
        {
            get
            {
                return (ContentText_CultureInfoFactory)ContentText_CultureInfoBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<ContentText_CultureInfo, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
