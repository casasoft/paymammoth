using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using PayMammoth.Classes.Application;
using BusinessLogic_v3.Classes.NHibernateClasses;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class AppInstance_AutoGen : BusinessLogic_v3.Classes.Application.AppInstance
    {

        public new static AppInstance Instance
        {
            get
            {
                if (BusinessLogic_v3.Classes.Application.AppInstance.Instance == null)
                {
                    BusinessLogic_v3.Classes.Application.AppInstance.Instance = CS.General_v3.Classes.Singletons.Singleton.GetInstance<Classes.Application.AppInstance>();
                }
                return (AppInstance)BusinessLogic_v3.Classes.Application.AppInstance.Instance;
            }
        }
        public AppInstance_AutoGen()
        {
            this.AddProjectAssembly(Assembly.GetAssembly(typeof(AppInstance_AutoGen)));
            this.AddProjectAssembly(Assembly.GetAssembly(typeof(CS.WebComponentsGeneralV3.Code.Classes.Contact.ContactDetailsData)));
            
        }
        protected override void onPostApplicationStart()
        {
            
            base.onPostApplicationStart();
            CmsSystem.Instance.OnApplicationStart();
            CmsSystem.Instance.FinishedInitialisingFactories();
        }
       protected override void initFactories()
        {
            

// [appinstance_initfactories]

                PayMammoth.Modules.ArticleModule.ArticleFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.ArticleModule.IArticleFactory>(PayMammoth.Modules.ArticleModule.ArticleFactory.Instance);
				
                PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfoFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfoFactory>(PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfoFactory.Instance);
				
                PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkFactory>(PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkFactory.Instance);
				
                PayMammoth.Modules.AuditLogModule.AuditLogFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.AuditLogModule.IAuditLogFactory>(PayMammoth.Modules.AuditLogModule.AuditLogFactory.Instance);
				
                PayMammoth.Modules.CmsUserModule.CmsUserFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.CmsUserModule.ICmsUserFactory>(PayMammoth.Modules.CmsUserModule.CmsUserFactory.Instance);
				
                PayMammoth.Modules.CmsUserRoleModule.CmsUserRoleFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRoleFactory>(PayMammoth.Modules.CmsUserRoleModule.CmsUserRoleFactory.Instance);
				
                PayMammoth.Modules.ContactFormModule.ContactFormFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.ContactFormModule.IContactFormFactory>(PayMammoth.Modules.ContactFormModule.ContactFormFactory.Instance);
				
                PayMammoth.Modules.ContentTextModule.ContentTextFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.ContentTextModule.IContentTextFactory>(PayMammoth.Modules.ContentTextModule.ContentTextFactory.Instance);
				
                PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoFactory>(PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoFactory.Instance);
				
                PayMammoth.Modules.CultureDetailsModule.CultureDetailsFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.CultureDetailsModule.ICultureDetailsFactory>(PayMammoth.Modules.CultureDetailsModule.CultureDetailsFactory.Instance);
				
                PayMammoth.Modules.CurrencyModule.CurrencyFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.CurrencyModule.ICurrencyFactory>(PayMammoth.Modules.CurrencyModule.CurrencyFactory.Instance);
				
                PayMammoth.Modules.EmailLogModule.EmailLogFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.EmailLogModule.IEmailLogFactory>(PayMammoth.Modules.EmailLogModule.EmailLogFactory.Instance);
				
                PayMammoth.Modules.EmailTextModule.EmailTextFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.EmailTextModule.IEmailTextFactory>(PayMammoth.Modules.EmailTextModule.EmailTextFactory.Instance);
				
                PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoFactory>(PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoFactory.Instance);
				
                PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfoFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfoFactory>(PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfoFactory.Instance);
				
                PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoFactory>(PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoFactory.Instance);
				
                PayMammoth.Modules.NotificationMessageModule.NotificationMessageFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.NotificationMessageModule.INotificationMessageFactory>(PayMammoth.Modules.NotificationMessageModule.NotificationMessageFactory.Instance);
				
                PayMammoth.Modules.PaymentMethodModule.PaymentMethodFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.PaymentMethodModule.IPaymentMethodFactory>(PayMammoth.Modules.PaymentMethodModule.PaymentMethodFactory.Instance);
				
                PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfoFactory>(PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoFactory.Instance);
				
                PayMammoth.Modules.PaymentRequestModule.PaymentRequestFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.PaymentRequestModule.IPaymentRequestFactory>(PayMammoth.Modules.PaymentRequestModule.PaymentRequestFactory.Instance);
				
                PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetailFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetailFactory>(PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetailFactory.Instance);
				
                PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransactionFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransactionFactory>(PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransactionFactory.Instance);
				
                PayMammoth.Modules.RecurringProfileModule.RecurringProfileFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.RecurringProfileModule.IRecurringProfileFactory>(PayMammoth.Modules.RecurringProfileModule.RecurringProfileFactory.Instance);
				
                PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePaymentFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePaymentFactory>(PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePaymentFactory.Instance);
				
                PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransactionFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransactionFactory>(PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransactionFactory.Instance);
				
                PayMammoth.Modules.RoutingInfoModule.RoutingInfoFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.RoutingInfoModule.IRoutingInfoFactory>(PayMammoth.Modules.RoutingInfoModule.RoutingInfoFactory.Instance);
				
                PayMammoth.Modules.SettingModule.SettingFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.SettingModule.ISettingFactory>(PayMammoth.Modules.SettingModule.SettingFactory.Instance);
				
                PayMammoth.Modules.WebsiteAccountModule.WebsiteAccountFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccountFactory>(PayMammoth.Modules.WebsiteAccountModule.WebsiteAccountFactory.Instance);
				
                PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoFactory._initialiseStaticInstance();
				BusinessLogic_v3.Util.InversionOfControlUtil.Register<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfoFactory>(PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoFactory.Instance);
				
        
            base.initFactories();
        }
        
        protected override void initNHibernateMappings()
        { 
        	

// [appinstance_initmappings]

                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ArticleModule.ArticleMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.AuditLogModule.AuditLogMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.CmsUserModule.CmsUserMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.CmsUserRoleModule.CmsUserRoleMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ContactFormModule.ContactFormMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ContentTextModule.ContentTextMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.CultureDetailsModule.CultureDetailsMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.CurrencyModule.CurrencyMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.EmailLogModule.EmailLogMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.EmailTextModule.EmailTextMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.NotificationMessageModule.NotificationMessageMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.PaymentMethodModule.PaymentMethodMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.PaymentRequestModule.PaymentRequestMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetailMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransactionMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.RecurringProfileModule.RecurringProfileMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePaymentMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransactionMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.RoutingInfoModule.RoutingInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.SettingModule.SettingMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccountMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoMap>();
        
				
			base.initNHibernateMappings();
        }

    }
}
