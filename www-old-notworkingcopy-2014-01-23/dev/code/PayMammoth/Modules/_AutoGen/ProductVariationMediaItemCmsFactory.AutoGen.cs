using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ProductVariationMediaItemModule;
using PayMammoth.Cms.ProductVariationMediaItemModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationMediaItemModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariationMediaItemCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductVariationMediaItemModule.ProductVariationMediaItemBaseCmsFactory
    {
        public static new ProductVariationMediaItemBaseCmsFactory Instance 
        {
         	get { return (ProductVariationMediaItemBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ProductVariationMediaItemModule.ProductVariationMediaItemBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ProductVariationMediaItemBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = ProductVariationMediaItem.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            ProductVariationMediaItemCmsInfo cmsItem = new ProductVariationMediaItemCmsInfo(item);
            return cmsItem;
        }    
        
        public override ProductVariationMediaItemBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase item)
        {
            return new ProductVariationMediaItemCmsInfo((ProductVariationMediaItem)item);
            
        }

    }
}
