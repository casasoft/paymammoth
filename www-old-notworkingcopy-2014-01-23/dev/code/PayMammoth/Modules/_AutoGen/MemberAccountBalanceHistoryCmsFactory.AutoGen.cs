using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.MemberAccountBalanceHistoryModule;
using PayMammoth.Cms.MemberAccountBalanceHistoryModule;
using CS.WebComponentsGeneralV3.Cms.MemberAccountBalanceHistoryModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberAccountBalanceHistoryCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseCmsFactory
    {
        public static new MemberAccountBalanceHistoryBaseCmsFactory Instance 
        {
         	get { return (MemberAccountBalanceHistoryBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override MemberAccountBalanceHistoryBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = MemberAccountBalanceHistory.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            MemberAccountBalanceHistoryCmsInfo cmsItem = new MemberAccountBalanceHistoryCmsInfo(item);
            return cmsItem;
        }    
        
        public override MemberAccountBalanceHistoryBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase item)
        {
            return new MemberAccountBalanceHistoryCmsInfo((MemberAccountBalanceHistory)item);
            
        }

    }
}
