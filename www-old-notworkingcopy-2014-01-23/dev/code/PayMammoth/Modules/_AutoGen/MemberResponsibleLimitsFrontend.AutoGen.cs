using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.MemberResponsibleLimitsModule;
using PayMammoth.Modules.MemberResponsibleLimitsModule;
using BusinessLogic_v3.Modules.MemberResponsibleLimitsModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.MemberResponsibleLimitsModule.MemberResponsibleLimitsFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.MemberResponsibleLimitsModule.MemberResponsibleLimitsFrontend ToFrontend(this PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits item)
        {
        	return PayMammoth.Frontend.MemberResponsibleLimitsModule.MemberResponsibleLimitsFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberResponsibleLimitsFrontend_AutoGen : BusinessLogic_v3.Frontend.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseFrontend

    {
		
        
        protected MemberResponsibleLimitsFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits Data
        {
            get { return (PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits)base.Data; }
            set { base.Data = value; }

        }

        public new static MemberResponsibleLimitsFrontend CreateNewItem()
        {
            return (MemberResponsibleLimitsFrontend)BusinessLogic_v3.Frontend.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseFrontend.CreateNewItem();
        }
        
        public new static MemberResponsibleLimitsFrontend Get(BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBase data)
        {
            return (MemberResponsibleLimitsFrontend)BusinessLogic_v3.Frontend.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseFrontend.Get(data);
        }
        public new static List<MemberResponsibleLimitsFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBase> dataList)
        {
            return BusinessLogic_v3.Frontend.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseFrontend.GetList(dataList).Cast<MemberResponsibleLimitsFrontend>().ToList();
        }


    }
}
