using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.Product_CultureInfoModule;
using PayMammoth.Cms.Product_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Product_CultureInfoModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Product_CultureInfoCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.Product_CultureInfoModule.Product_CultureInfoBaseCmsFactory
    {
        public static new Product_CultureInfoBaseCmsFactory Instance 
        {
         	get { return (Product_CultureInfoBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.Product_CultureInfoModule.Product_CultureInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override Product_CultureInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Product_CultureInfo.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            Product_CultureInfoCmsInfo cmsItem = new Product_CultureInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override Product_CultureInfoBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase item)
        {
            return new Product_CultureInfoCmsInfo((Product_CultureInfo)item);
            
        }

    }
}
