using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ImageSizingInfoModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ImageSizingInfoMap_AutoGen : ImageSizingInfoMap_AutoGen_Z
    {
        public ImageSizingInfoMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MaximumWidth, MaximumWidthMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MaximumHeight, MaximumHeightMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.SizingInfos, SizingInfosMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ImageSizingInfoMap_AutoGen_Z : BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBaseMap<ImageSizingInfoImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
