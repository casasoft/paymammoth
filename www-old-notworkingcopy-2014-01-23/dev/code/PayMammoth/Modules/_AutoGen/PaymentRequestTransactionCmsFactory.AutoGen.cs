using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Cms.PaymentRequestTransactionModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestTransactionCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.PaymentRequestTransactionBaseCmsFactory
    {
        public static new PaymentRequestTransactionBaseCmsFactory Instance 
        {
         	get { return (PaymentRequestTransactionBaseCmsFactory)PayMammoth.Modules._AutoGen.PaymentRequestTransactionBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override PaymentRequestTransactionBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	PaymentRequestTransaction item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = PaymentRequestTransaction.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        PaymentRequestTransactionCmsInfo cmsItem = new PaymentRequestTransactionCmsInfo(item);
            return cmsItem;
        }    
        
        public override PaymentRequestTransactionBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase item)
        {
            return new PaymentRequestTransactionCmsInfo((PaymentRequestTransaction)item);
            
        }

    }
}
