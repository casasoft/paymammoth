using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>
    {
		public PaymentRequestItemBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentRequestItemBase dbItem)
            : base(PayMammoth.Modules._AutoGen.PaymentRequestItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new PaymentRequestItemBase DbItem
        {
            get
            {
                return (PaymentRequestItemBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Quantity { get; private set; }

        public CmsPropertyInfo PriceExcTax { get; private set; }

        public CmsPropertyInfo TaxAmount { get; private set; }

        public CmsPropertyInfo ShippingAmount { get; private set; }

        public CmsPropertyInfo HandlingAmount { get; private set; }

        public CmsPropertyInfo Discount { get; private set; }

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo WidthInMm { get; private set; }

        public CmsPropertyInfo HeightInMm { get; private set; }

        public CmsPropertyInfo LengthInMm { get; private set; }

        public CmsPropertyInfo PaymentRequest { get; private set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Quantity = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.Quantity),
        			isEditable: true,
        			isVisible: true
        			);

        this.PriceExcTax = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.PriceExcTax),
        			isEditable: true,
        			isVisible: true
        			);

        this.TaxAmount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.TaxAmount),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShippingAmount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.ShippingAmount),
        			isEditable: true,
        			isVisible: true
        			);

        this.HandlingAmount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.HandlingAmount),
        			isEditable: true,
        			isVisible: true
        			);

        this.Discount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.Discount),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.WidthInMm = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.WidthInMm),
        			isEditable: true,
        			isVisible: true
        			);

        this.HeightInMm = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.HeightInMm),
        			isEditable: true,
        			isVisible: true
        			);

        this.LengthInMm = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.LengthInMm),
        			isEditable: true,
        			isVisible: true
        			);

        this.PaymentRequest = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaymentRequestItemBase>.GetPropertyBySelector( item => item.PaymentRequest),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
