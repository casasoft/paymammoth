using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ContentTextModule;
using PayMammoth.Modules.ContentTextModule;
using BusinessLogic_v3.Modules.ContentTextModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ContentTextModule.ContentTextFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ContentTextModule.IContentText> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ContentTextModule.ContentTextFrontend ToFrontend(this PayMammoth.Modules.ContentTextModule.IContentText item)
        {
        	return PayMammoth.Frontend.ContentTextModule.ContentTextFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ContentTextFrontend_AutoGen : BusinessLogic_v3.Frontend.ContentTextModule.ContentTextBaseFrontend

    {
		
        
        protected ContentTextFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ContentTextModule.IContentText Data
        {
            get { return (PayMammoth.Modules.ContentTextModule.IContentText)base.Data; }
            set { base.Data = value; }

        }

        public new static ContentTextFrontend CreateNewItem()
        {
            return (ContentTextFrontend)BusinessLogic_v3.Frontend.ContentTextModule.ContentTextBaseFrontend.CreateNewItem();
        }
        
        public new static ContentTextFrontend Get(BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase data)
        {
            return (ContentTextFrontend)BusinessLogic_v3.Frontend.ContentTextModule.ContentTextBaseFrontend.Get(data);
        }
        public new static List<ContentTextFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ContentTextModule.ContentTextBaseFrontend.GetList(dataList).Cast<ContentTextFrontend>().ToList();
        }


    }
}
