using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ArticleRatingModule;
using PayMammoth.Modules.ArticleRatingModule;
using BusinessLogic_v3.Modules.ArticleRatingModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ArticleRatingModule.ArticleRatingFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ArticleRatingModule.IArticleRating> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ArticleRatingModule.ArticleRatingFrontend ToFrontend(this PayMammoth.Modules.ArticleRatingModule.IArticleRating item)
        {
        	return PayMammoth.Frontend.ArticleRatingModule.ArticleRatingFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleRatingFrontend_AutoGen : BusinessLogic_v3.Frontend.ArticleRatingModule.ArticleRatingBaseFrontend

    {
		
        
        protected ArticleRatingFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ArticleRatingModule.IArticleRating Data
        {
            get { return (PayMammoth.Modules.ArticleRatingModule.IArticleRating)base.Data; }
            set { base.Data = value; }

        }

        public new static ArticleRatingFrontend CreateNewItem()
        {
            return (ArticleRatingFrontend)BusinessLogic_v3.Frontend.ArticleRatingModule.ArticleRatingBaseFrontend.CreateNewItem();
        }
        
        public new static ArticleRatingFrontend Get(BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBase data)
        {
            return (ArticleRatingFrontend)BusinessLogic_v3.Frontend.ArticleRatingModule.ArticleRatingBaseFrontend.Get(data);
        }
        public new static List<ArticleRatingFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ArticleRatingModule.ArticleRatingBaseFrontend.GetList(dataList).Cast<ArticleRatingFrontend>().ToList();
        }


    }
}
