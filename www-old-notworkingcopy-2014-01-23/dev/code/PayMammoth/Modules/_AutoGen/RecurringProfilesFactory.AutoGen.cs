using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.RecurringProfilesModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfilesFactoryAutoGen : PayMammoth.Modules._AutoGen.RecurringProfilesBaseFactory, 
    				PayMammoth.Modules._AutoGen.IRecurringProfilesFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IRecurringProfiles>
    
    {
    
     	public new RecurringProfiles ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.RecurringProfilesBase item)
        {
         	return (RecurringProfiles)base.ReloadItemFromDatabase(item);
        }
     	static RecurringProfilesFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public RecurringProfilesFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(RecurringProfilesImpl);
			PayMammoth.Cms.RecurringProfilesModule.RecurringProfilesCmsFactory._initialiseStaticInstance();
        }
        public static new RecurringProfilesFactory Instance
        {
            get
            {
                return (RecurringProfilesFactory)RecurringProfilesBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<RecurringProfiles> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<RecurringProfiles>();
            
        }
        public new IEnumerable<RecurringProfiles> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<RecurringProfiles>();
            
            
        }
    
        public new IEnumerable<RecurringProfiles> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<RecurringProfiles>();
            
        }
        public new IEnumerable<RecurringProfiles> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<RecurringProfiles>();
        }
        
        public new RecurringProfiles FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (RecurringProfiles)base.FindItem(query);
        }
        public new RecurringProfiles FindItem(IQueryOver query)
        {
            return (RecurringProfiles)base.FindItem(query);
        }
        
        IRecurringProfiles IRecurringProfilesFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new RecurringProfiles CreateNewItem()
        {
            return (RecurringProfiles)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<RecurringProfiles, RecurringProfiles> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<RecurringProfiles>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles>) base.FindAll(session);

       }
       public new PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfilesModule.RecurringProfiles>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles> Members
     
        PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles IBaseDbFactory<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles IBaseDbFactory<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles> IBaseDbFactory<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles IBaseDbFactory<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles> IBaseDbFactory<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IRecurringProfiles> IRecurringProfilesFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IRecurringProfiles> IRecurringProfilesFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IRecurringProfiles> IRecurringProfilesFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IRecurringProfiles IRecurringProfilesFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IRecurringProfiles IRecurringProfilesFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IRecurringProfiles IRecurringProfilesFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IRecurringProfiles> IRecurringProfilesFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IRecurringProfiles IRecurringProfilesFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
