using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.RoutingInfoModule;
using PayMammoth.Modules.RoutingInfoModule;
using BusinessLogic_v3.Modules.RoutingInfoModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.RoutingInfoModule.RoutingInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.RoutingInfoModule.RoutingInfoFrontend ToFrontend(this PayMammoth.Modules.RoutingInfoModule.IRoutingInfo item)
        {
        	return PayMammoth.Frontend.RoutingInfoModule.RoutingInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class RoutingInfoFrontend_AutoGen : BusinessLogic_v3.Frontend.RoutingInfoModule.RoutingInfoBaseFrontend

    {
		
        
        protected RoutingInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.RoutingInfoModule.IRoutingInfo Data
        {
            get { return (PayMammoth.Modules.RoutingInfoModule.IRoutingInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static RoutingInfoFrontend CreateNewItem()
        {
            return (RoutingInfoFrontend)BusinessLogic_v3.Frontend.RoutingInfoModule.RoutingInfoBaseFrontend.CreateNewItem();
        }
        
        public new static RoutingInfoFrontend Get(BusinessLogic_v3.Modules.RoutingInfoModule.IRoutingInfoBase data)
        {
            return (RoutingInfoFrontend)BusinessLogic_v3.Frontend.RoutingInfoModule.RoutingInfoBaseFrontend.Get(data);
        }
        public new static List<RoutingInfoFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.RoutingInfoModule.IRoutingInfoBase> dataList)
        {
            return BusinessLogic_v3.Frontend.RoutingInfoModule.RoutingInfoBaseFrontend.GetList(dataList).Cast<RoutingInfoFrontend>().ToList();
        }


    }
}
