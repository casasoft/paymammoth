using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.SettingModule;
using PayMammoth.Cms.SettingModule;
using CS.WebComponentsGeneralV3.Cms.SettingModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class SettingCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.SettingModule.SettingBaseCmsFactory
    {
        public static new SettingBaseCmsFactory Instance 
        {
         	get { return (SettingBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.SettingModule.SettingBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override SettingBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	Setting item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = Setting.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        SettingCmsInfo cmsItem = new SettingCmsInfo(item);
            return cmsItem;
        }    
        
        public override SettingBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.SettingModule.SettingBase item)
        {
            return new SettingCmsInfo((Setting)item);
            
        }

    }
}
