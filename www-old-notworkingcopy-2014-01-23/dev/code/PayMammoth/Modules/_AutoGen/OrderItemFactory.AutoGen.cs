using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.OrderItemModule;
using PayMammoth.Modules.OrderItemModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class OrderItemFactoryAutoGen : BusinessLogic_v3.Modules.OrderItemModule.OrderItemBaseFactory, 
    				PayMammoth.Modules._AutoGen.IOrderItemFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<IOrderItem>
    
    {
    
     	public new OrderItem ReloadItemFromDatabase(BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase item)
        {
         	return (OrderItem)base.ReloadItemFromDatabase(item);
        }
     	
        public OrderItemFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(OrderItemImpl);
			PayMammoth.Cms.OrderItemModule.OrderItemCmsFactory._initialiseStaticInstance();
        }
        public static new OrderItemFactory Instance
        {
            get
            {
                return (OrderItemFactory)OrderItemBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<OrderItem> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<OrderItem>();
            
        }
        public new IEnumerable<OrderItem> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<OrderItem>();
            
            
        }
    
        public new IEnumerable<OrderItem> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<OrderItem>();
            
        }
        public new IEnumerable<OrderItem> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<OrderItem>();
        }
        
        public new OrderItem FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (OrderItem)base.FindItem(query);
        }
        public new OrderItem FindItem(IQueryOver query)
        {
            return (OrderItem)base.FindItem(query);
        }
        
        IOrderItem IOrderItemFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new OrderItem CreateNewItem()
        {
            return (OrderItem)base.CreateNewItem();
        }

        public new OrderItem GetByPrimaryKey(long pKey)
        {
            return (OrderItem)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<OrderItem, OrderItem> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<OrderItem>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.OrderItemModule.OrderItem GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.OrderItemModule.OrderItem) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem>) base.FindAll(session);

       }
       public new PayMammoth.Modules.OrderItemModule.OrderItem FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.OrderItemModule.OrderItem) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.OrderItemModule.OrderItem FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.OrderItemModule.OrderItem) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.OrderItemModule.OrderItem FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.OrderItemModule.OrderItem) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.OrderItemModule.IOrderItem> Members
     
        PayMammoth.Modules.OrderItemModule.IOrderItem IBaseDbFactory<PayMammoth.Modules.OrderItemModule.IOrderItem>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.OrderItemModule.IOrderItem IBaseDbFactory<PayMammoth.Modules.OrderItemModule.IOrderItem>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.OrderItemModule.IOrderItem> IBaseDbFactory<PayMammoth.Modules.OrderItemModule.IOrderItem>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.OrderItemModule.IOrderItem> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.OrderItemModule.IOrderItem>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.OrderItemModule.IOrderItem> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.OrderItemModule.IOrderItem>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.OrderItemModule.IOrderItem IBaseDbFactory<PayMammoth.Modules.OrderItemModule.IOrderItem>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.OrderItemModule.IOrderItem BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.OrderItemModule.IOrderItem>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.OrderItemModule.IOrderItem BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.OrderItemModule.IOrderItem>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.OrderItemModule.IOrderItem> IBaseDbFactory<PayMammoth.Modules.OrderItemModule.IOrderItem>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IOrderItem> IOrderItemFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IOrderItem> IOrderItemFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IOrderItem> IOrderItemFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IOrderItem IOrderItemFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IOrderItem IOrderItemFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IOrderItem IOrderItemFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IOrderItem> IOrderItemFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IOrderItem IOrderItemFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
