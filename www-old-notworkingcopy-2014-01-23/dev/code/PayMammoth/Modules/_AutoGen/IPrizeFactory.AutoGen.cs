using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PrizeModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IPrizeFactoryAutoGen : PayMammoth.Modules._AutoGen.IPrizeBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IPrize>
    
    {
	    new PayMammoth.Modules.PrizeModule.IPrize CreateNewItem();
        new PayMammoth.Modules.PrizeModule.IPrize GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PrizeModule.IPrize> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PrizeModule.IPrize> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PrizeModule.IPrize> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.PrizeModule.IPrize FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PrizeModule.IPrize FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PrizeModule.IPrize FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PrizeModule.IPrize> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
