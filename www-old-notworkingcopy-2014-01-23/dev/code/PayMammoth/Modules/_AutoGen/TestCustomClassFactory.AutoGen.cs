using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.TestCustomClassModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class TestCustomClassFactoryAutoGen : PayMammoth.Modules._AutoGen.TestCustomClassBaseFactory, 
    				PayMammoth.Modules._AutoGen.ITestCustomClassFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<ITestCustomClass>
    
    {
    
     	
        public TestCustomClassFactoryAutoGen ()
        {

			this.UsedInProject = true;
			_mainTypeCreated = typeof(TestCustomClassImpl);
			PayMammoth.Cms.TestCustomClassModule.TestCustomClassCmsFactory._initialiseStaticInstance();
        }
        public static new TestCustomClassFactory Instance
        {
            get
            {
                return (TestCustomClassFactory)TestCustomClassBaseFactory.Instance;
            }
      
      

        }
        
        
        public new IEnumerable<TestCustomClass> FindAll()
        {
            return base.FindAll().Cast<TestCustomClass>();
        }
        public new IEnumerable<TestCustomClass> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<TestCustomClass>();

        }
        public new IEnumerable<TestCustomClass> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<TestCustomClass>();
            
        }
        public new IEnumerable<TestCustomClass> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<TestCustomClass>();
            
            
        }
        public new IEnumerable<TestCustomClass> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<TestCustomClass>();


        }
        public new IEnumerable<TestCustomClass> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<TestCustomClass>();
            
        }
        public new IEnumerable<TestCustomClass> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<TestCustomClass>();
        }
        
        public new TestCustomClass FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (TestCustomClass)base.FindItem(query);
        }
        public new TestCustomClass FindItem(IQueryOver query)
        {
            return (TestCustomClass)base.FindItem(query);
        }
        
        ITestCustomClass ITestCustomClassFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new TestCustomClass CreateNewItem()
        {
            return (TestCustomClass)base.CreateNewItem();
        }

        public new TestCustomClass GetByPrimaryKey(long pKey)
        {
            return (TestCustomClass)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<TestCustomClass, TestCustomClass> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<TestCustomClass>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public PayMammoth.Modules.TestCustomClassModule.TestCustomClass GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.TestCustomClassModule.TestCustomClass) base.GetByPrimaryKey(id, session);
        }
       public IEnumerable<PayMammoth.Modules.TestCustomClassModule.TestCustomClass> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TestCustomClassModule.TestCustomClass>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.TestCustomClassModule.TestCustomClass> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TestCustomClassModule.TestCustomClass>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.TestCustomClassModule.TestCustomClass> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TestCustomClassModule.TestCustomClass>) base.FindAll(session);

       }
       public PayMammoth.Modules.TestCustomClassModule.TestCustomClass FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.TestCustomClassModule.TestCustomClass) base.FindItem(query, session);

       }
       public PayMammoth.Modules.TestCustomClassModule.TestCustomClass FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.TestCustomClassModule.TestCustomClass) base.FindItem(query, session);

       }

       public PayMammoth.Modules.TestCustomClassModule.TestCustomClass FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.TestCustomClassModule.TestCustomClass) base.FindItem(session);

       }
       public IEnumerable<PayMammoth.Modules.TestCustomClassModule.TestCustomClass> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TestCustomClassModule.TestCustomClass>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass> Members
     
        PayMammoth.Modules.TestCustomClassModule.ITestCustomClass IBaseDbFactory<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.TestCustomClassModule.ITestCustomClass IBaseDbFactory<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass> IBaseDbFactory<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.TestCustomClassModule.ITestCustomClass IBaseDbFactory<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.TestCustomClassModule.ITestCustomClass CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.TestCustomClassModule.ITestCustomClass CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass> IBaseDbFactory<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ITestCustomClass> ITestCustomClassFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ITestCustomClass> ITestCustomClassFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ITestCustomClass> ITestCustomClassFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }

       ITestCustomClass ITestCustomClassFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       ITestCustomClass ITestCustomClassFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       ITestCustomClass ITestCustomClassFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ITestCustomClass> ITestCustomClassFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ITestCustomClass ITestCustomClassFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
