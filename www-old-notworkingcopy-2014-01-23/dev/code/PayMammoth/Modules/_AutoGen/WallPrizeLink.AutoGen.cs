using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.WallPrizeLinkModule;
using PayMammoth.Modules._AutoGen;

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WallPrizeLink_AutoGen : WallPrizeLinkBase, PayMammoth.Modules._AutoGen.IWallPrizeLinkAutoGen
	
      
      
    {
        


    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.WallModule.Wall Wall
        {
            get
            {
                return (PayMammoth.Modules.WallModule.Wall)base.Wall;
            }
            set
            {
                base.Wall = value;
            }
        }

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.PrizeModule.Prize Prize
        {
            get
            {
                return (PayMammoth.Modules.PrizeModule.Prize)base.Prize;
            }
            set
            {
                base.Prize = value;
            }
        }




// [userclass_collections_nhibernate]


        
          
		public  static new WallPrizeLinkFactory Factory
        {
            get
            {
                return (WallPrizeLinkFactory)WallPrizeLinkBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<WallPrizeLink, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
