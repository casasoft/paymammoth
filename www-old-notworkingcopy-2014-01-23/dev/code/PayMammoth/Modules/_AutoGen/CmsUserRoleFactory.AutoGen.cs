using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.CmsUserRoleModule;
using PayMammoth.Modules.CmsUserRoleModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class CmsUserRoleFactoryAutoGen : BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBaseFactory, 
    				PayMammoth.Modules._AutoGen.ICmsUserRoleFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<ICmsUserRole>
    
    {
    
     	public new CmsUserRole ReloadItemFromDatabase(BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase item)
        {
         	return (CmsUserRole)base.ReloadItemFromDatabase(item);
        }
     	static CmsUserRoleFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public CmsUserRoleFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(CmsUserRoleImpl);
			PayMammoth.Cms.CmsUserRoleModule.CmsUserRoleCmsFactory._initialiseStaticInstance();
        }
        public static new CmsUserRoleFactory Instance
        {
            get
            {
                return (CmsUserRoleFactory)CmsUserRoleBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<CmsUserRole> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<CmsUserRole>();
            
        }
        public new IEnumerable<CmsUserRole> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<CmsUserRole>();
            
            
        }
    
        public new IEnumerable<CmsUserRole> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<CmsUserRole>();
            
        }
        public new IEnumerable<CmsUserRole> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<CmsUserRole>();
        }
        
        public new CmsUserRole FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (CmsUserRole)base.FindItem(query);
        }
        public new CmsUserRole FindItem(IQueryOver query)
        {
            return (CmsUserRole)base.FindItem(query);
        }
        
        ICmsUserRole ICmsUserRoleFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new CmsUserRole CreateNewItem()
        {
            return (CmsUserRole)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<CmsUserRole, CmsUserRole> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<CmsUserRole>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.CmsUserRoleModule.CmsUserRole GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.CmsUserRoleModule.CmsUserRole) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole>) base.FindAll(session);

       }
       public new PayMammoth.Modules.CmsUserRoleModule.CmsUserRole FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CmsUserRoleModule.CmsUserRole) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.CmsUserRoleModule.CmsUserRole FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CmsUserRoleModule.CmsUserRole) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.CmsUserRoleModule.CmsUserRole FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CmsUserRoleModule.CmsUserRole) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole> Members
     
        PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole IBaseDbFactory<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole IBaseDbFactory<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole> IBaseDbFactory<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole IBaseDbFactory<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole> IBaseDbFactory<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ICmsUserRole> ICmsUserRoleFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICmsUserRole> ICmsUserRoleFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICmsUserRole> ICmsUserRoleFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       ICmsUserRole ICmsUserRoleFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICmsUserRole ICmsUserRoleFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICmsUserRole ICmsUserRoleFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ICmsUserRole> ICmsUserRoleFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ICmsUserRole ICmsUserRoleFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
