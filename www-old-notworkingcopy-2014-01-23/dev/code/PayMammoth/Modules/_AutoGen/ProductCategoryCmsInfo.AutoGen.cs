using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ProductCategoryModule;
using CS.WebComponentsGeneralV3.Cms.ProductCategoryModule;
using PayMammoth.Frontend.ProductCategoryModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ProductCategoryCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductCategoryModule.ProductCategoryBaseCmsInfo
    {

		public ProductCategoryCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase item)
            : base(item)
        {

        }
        

        private ProductCategoryFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ProductCategoryFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ProductCategoryFrontend FrontendItem
        {
            get { return (ProductCategoryFrontend) base.FrontendItem; }
        }
        
        
		public new ProductCategory DbItem
        {
            get
            {
                return (ProductCategory)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.Product = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductCategoryModule.ProductCategory>.GetPropertyBySelector( item => item.Product),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.CategoryLinks)));

        
//InitFieldUser_LinkedProperty
			this.Category = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductCategoryModule.ProductCategory>.GetPropertyBySelector( item => item.Category),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.ProductLinks)));

        
	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
