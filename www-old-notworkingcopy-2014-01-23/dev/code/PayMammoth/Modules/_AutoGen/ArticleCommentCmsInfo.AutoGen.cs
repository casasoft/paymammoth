using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ArticleCommentModule;
using CS.WebComponentsGeneralV3.Cms.ArticleCommentModule;
using PayMammoth.Frontend.ArticleCommentModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ArticleCommentCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ArticleCommentModule.ArticleCommentBaseCmsInfo
    {

		public ArticleCommentCmsInfo_AutoGen(BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase item)
            : base(item)
        {

        }
        

        private ArticleCommentFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ArticleCommentFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ArticleCommentFrontend FrontendItem
        {
            get { return (ArticleCommentFrontend) base.FrontendItem; }
        }
        
        
		public new ArticleComment DbItem
        {
            get
            {
                return (ArticleComment)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Email = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleCommentModule.ArticleComment>.GetPropertyBySelector( item => item.Email),
        						isEditable: true, isVisible: true);

        this.Comment = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleCommentModule.ArticleComment>.GetPropertyBySelector( item => item.Comment),
        						isEditable: true, isVisible: true);

        this.IPAddress = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleCommentModule.ArticleComment>.GetPropertyBySelector( item => item.IPAddress),
        						isEditable: true, isVisible: true);

        this.PostedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleCommentModule.ArticleComment>.GetPropertyBySelector( item => item.PostedOn),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Article = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleCommentModule.ArticleComment>.GetPropertyBySelector( item => item.Article),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ArticleComments)));

        
        this.Author = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleCommentModule.ArticleComment>.GetPropertyBySelector( item => item.Author),
        						isEditable: true, isVisible: true);

        this.Date = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleCommentModule.ArticleComment>.GetPropertyBySelector( item => item.Date),
        						isEditable: true, isVisible: true);

        this.Company = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleCommentModule.ArticleComment>.GetPropertyBySelector( item => item.Company),
        						isEditable: true, isVisible: true);

        this.IsApproved = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleCommentModule.ArticleComment>.GetPropertyBySelector( item => item.IsApproved),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
