using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.RecurringProfileTransactionModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IRecurringProfileTransactionFactoryAutoGen : PayMammoth.Modules._AutoGen.IRecurringProfileTransactionBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IRecurringProfileTransaction>
    
    {
	    new PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction CreateNewItem();
        new PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
