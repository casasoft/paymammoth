using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.PrizeModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class PrizeMap_AutoGen : PrizeMap_AutoGen_Z
    {
        public PrizeMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.RetailValue, RetailValueMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class PrizeMap_AutoGen_Z : PayMammoth.Modules._AutoGen.PrizeBaseMap<PrizeImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
