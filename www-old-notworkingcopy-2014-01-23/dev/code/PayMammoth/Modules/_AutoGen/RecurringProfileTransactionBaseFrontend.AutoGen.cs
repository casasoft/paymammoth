using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules._AutoGen.RecurringProfileTransactionModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Modules._AutoGen.RecurringProfileTransactionModule.RecurringProfileTransactionBaseFrontend> ToFrontendBaseList(this IEnumerable<PayMammoth.Modules._AutoGen.IRecurringProfileTransactionBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static PayMammoth.Modules._AutoGen.RecurringProfileTransactionModule.RecurringProfileTransactionBaseFrontend ToFrontendBase(this PayMammoth.Modules._AutoGen.IRecurringProfileTransactionBase item)
        {
        	return PayMammoth.Modules._AutoGen.RecurringProfileTransactionModule.RecurringProfileTransactionBaseFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfileTransactionBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<RecurringProfileTransactionBaseFrontend, PayMammoth.Modules._AutoGen.IRecurringProfileTransactionBase>

    {
		
        
        protected RecurringProfileTransactionBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
