using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfilePaymentFactoryAutoGen : PayMammoth.Modules._AutoGen.RecurringProfilePaymentBaseFactory, 
    				PayMammoth.Modules._AutoGen.IRecurringProfilePaymentFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IRecurringProfilePayment>
    
    {
    
     	public new RecurringProfilePayment ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.RecurringProfilePaymentBase item)
        {
         	return (RecurringProfilePayment)base.ReloadItemFromDatabase(item);
        }
     	static RecurringProfilePaymentFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public RecurringProfilePaymentFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(RecurringProfilePaymentImpl);
			PayMammoth.Cms.RecurringProfilePaymentModule.RecurringProfilePaymentCmsFactory._initialiseStaticInstance();
        }
        public static new RecurringProfilePaymentFactory Instance
        {
            get
            {
                return (RecurringProfilePaymentFactory)RecurringProfilePaymentBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<RecurringProfilePayment> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<RecurringProfilePayment>();
            
        }
        public new IEnumerable<RecurringProfilePayment> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<RecurringProfilePayment>();
            
            
        }
    
        public new IEnumerable<RecurringProfilePayment> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<RecurringProfilePayment>();
            
        }
        public new IEnumerable<RecurringProfilePayment> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<RecurringProfilePayment>();
        }
        
        public new RecurringProfilePayment FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (RecurringProfilePayment)base.FindItem(query);
        }
        public new RecurringProfilePayment FindItem(IQueryOver query)
        {
            return (RecurringProfilePayment)base.FindItem(query);
        }
        
        IRecurringProfilePayment IRecurringProfilePaymentFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new RecurringProfilePayment CreateNewItem()
        {
            return (RecurringProfilePayment)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<RecurringProfilePayment, RecurringProfilePayment> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<RecurringProfilePayment>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>) base.FindAll(session);

       }
       public new PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment> Members
     
        PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment IBaseDbFactory<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment IBaseDbFactory<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment> IBaseDbFactory<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment IBaseDbFactory<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment> IBaseDbFactory<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IRecurringProfilePayment> IRecurringProfilePaymentFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IRecurringProfilePayment> IRecurringProfilePaymentFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IRecurringProfilePayment> IRecurringProfilePaymentFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IRecurringProfilePayment IRecurringProfilePaymentFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IRecurringProfilePayment IRecurringProfilePaymentFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IRecurringProfilePayment IRecurringProfilePaymentFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IRecurringProfilePayment> IRecurringProfilePaymentFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IRecurringProfilePayment IRecurringProfilePaymentFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
