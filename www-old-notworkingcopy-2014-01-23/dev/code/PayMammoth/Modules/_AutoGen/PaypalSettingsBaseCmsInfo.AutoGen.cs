using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaypalSettingsBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.PaypalSettingsBase>
    {
		public PaypalSettingsBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaypalSettingsBase dbItem)
            : base(PayMammoth.Modules._AutoGen.PaypalSettingsBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new PaypalSettingsBase DbItem
        {
            get
            {
                return (PaypalSettingsBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo WebsiteAccount { get; private set; }

        public CmsPropertyInfo Enabled { get; private set; }

        public CmsPropertyInfo MerchantEmail { get; private set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.WebsiteAccount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaypalSettingsBase>.GetPropertyBySelector( item => item.WebsiteAccount),
        			isEditable: true,
        			isVisible: true
        			);

        this.Enabled = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaypalSettingsBase>.GetPropertyBySelector( item => item.Enabled),
        			isEditable: true,
        			isVisible: true
        			);

        this.MerchantEmail = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PaypalSettingsBase>.GetPropertyBySelector( item => item.MerchantEmail),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
