using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.PaymentMethodModule;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class PaymentMethod_AutoGen : PaymentMethodBase, PayMammoth.Modules._AutoGen.IPaymentMethodAutoGen
	                 
      , IMultilingualItem
      
      
      
    {
        
            

    
    
    
		

#region Multilingual
        public PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo GetCurrentCultureInfo()
        {
            return (PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo)__getCurrentCultureInfo();
        }             
          
    	public new PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo)base.GetCultureInfoByLanguage(lang);
        }
        
		public override bool IsMultilingual()
        {
            return true;
        }
        
#endregion
        
        



// [userclass_linkedproperties]




// [userclass_collections_nhibernate]


        
          
		public  static new PaymentMethodFactory Factory
        {
            get
            {
                return (PaymentMethodFactory)PaymentMethodBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<PaymentMethod, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


        public override void __CheckItemCultureInfos(bool autoSaveInDB, IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> CultureInfos = null)
        {
        	BusinessLogic_v3.Classes.NHibernateClasses.Transactions.MyTransaction t = null;
            if (autoSaveInDB)
                t = beginTransaction();
            MultilingualChecker multilingualChecker = new MultilingualChecker();
            multilingualChecker.ItemToAdd+=new MultilingualChecker.ItemToAddHandler(multilingualChecker_ItemToAdd);
            multilingualChecker.ItemRemoved += new MultilingualChecker.ItemRemovedHandler(multilingualChecker_ItemRemoved);
            multilingualChecker.CheckItemCultureInfos(this, CultureInfos);
            if (autoSaveInDB)
            {
                this.Save();
                t.Commit();
                t.Dispose();
            }
        }

 		protected override IMultilingualContentInfo __createCultureInfoForCulture(long cultureID)
        {

            var cultureDetails = BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetByPrimaryKey(cultureID);
            IMultilingualContentInfo result = null;
            if (cultureDetails != null)
            {
                var itemCultureInfo = this.PaymentMethod_CultureInfos.CreateNewItem();
                result = itemCultureInfo;

//                itemCultureInfo.PaymentMethod = (PaymentMethod)this;  //item link
            	itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture
	

// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.Title = this.__Title_cultureBase;

                        itemCultureInfo.Description = this.__Description_cultureBase;


	
				itemCultureInfo.MarkAsNotTemporary();
//				this.PaymentMethod_CultureInfos.Add((PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo) itemCultureInfo);


            }
            return result;
        }


        private void multilingualChecker_ItemToAdd(BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase cultureDetails)
        {
            var itemCultureInfo = PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo.Factory.CreateNewItem();

            itemCultureInfo.PaymentMethod = (PaymentMethod)this;  //item link
            itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture


// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.Title = this.__Title_cultureBase;

                        itemCultureInfo.Description = this.__Description_cultureBase;



			itemCultureInfo.MarkAsNotTemporary();
			this.PaymentMethod_CultureInfos.Add((PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo) itemCultureInfo);
            
            

        }

		private void multilingualChecker_ItemRemoved(IMultilingualContentInfo item)
        {   
        	this.PaymentMethod_CultureInfos.Remove((PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo)item);
            
        }

        
        
         #region IMultilingualItem Members

        IEnumerable<IMultilingualContentInfo> IMultilingualItem.GetMultilingualContentInfos()
        {
            return this.PaymentMethod_CultureInfos;
            
        }

        protected override IEnumerable<IMultilingualContentInfo> __getMultilingualContentInfos()
        {
            return this.PaymentMethod_CultureInfos;
        }
        
		#endregion    

     
        
        


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        //UserClassOnly_CollectionOneToManyLeftSide
        
        
        public class __PaymentMethod_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.PaymentMethodModule.PaymentMethod, 
        	PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo,
        	PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.PaymentMethodModule.PaymentMethod, PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>
        {
            private PayMammoth.Modules.PaymentMethodModule.PaymentMethod _item = null;
            public __PaymentMethod_CultureInfosCollectionInfo(PayMammoth.Modules.PaymentMethodModule.PaymentMethod item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>)_item.__collection__PaymentMethod_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.PaymentMethodModule.PaymentMethod, PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>.SetLinkOnItem(PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo item, PayMammoth.Modules.PaymentMethodModule.PaymentMethod value)
            {
                item.PaymentMethod = value;
            }
        }
        
        private __PaymentMethod_CultureInfosCollectionInfo _PaymentMethod_CultureInfos = null;
        public __PaymentMethod_CultureInfosCollectionInfo PaymentMethod_CultureInfos
        {
            get
            {
                if (_PaymentMethod_CultureInfos == null)
                    _PaymentMethod_CultureInfos = new __PaymentMethod_CultureInfosCollectionInfo((PayMammoth.Modules.PaymentMethodModule.PaymentMethod)this);
                return _PaymentMethod_CultureInfos;
            }
        }

   
		ICollectionManager<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo> IPaymentMethodAutoGen.PaymentMethod_CultureInfos
        {
            get {  return this.PaymentMethod_CultureInfos; }
        }
   
        protected virtual IEnumerable<PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase> __collection__PaymentMethod_CultureInfos
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }
        
        
        
    }
    
}
