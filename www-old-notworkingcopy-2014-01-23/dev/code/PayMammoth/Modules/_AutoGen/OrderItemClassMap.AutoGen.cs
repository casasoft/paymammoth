using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.OrderItemModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class OrderItemMap_AutoGen : OrderItemMap_AutoGen_Z
    {
        public OrderItemMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Quantity, QuantityMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PriceExcTaxPerUnit, PriceExcTaxPerUnitMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TaxAmountPerUnit, TaxAmountPerUnitMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ItemReference, ItemReferenceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Remarks, RemarksMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DiscountPerUnit, DiscountPerUnitMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ItemID, ItemIDMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ItemType, ItemTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ShipmentWeight, ShipmentWeightMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ShippingCost, ShippingCostMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HandlingCost, HandlingCostMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Order, OrderMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class OrderItemMap_AutoGen_Z : BusinessLogic_v3.Modules.OrderItemModule.OrderItemBaseMap<OrderItemImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
