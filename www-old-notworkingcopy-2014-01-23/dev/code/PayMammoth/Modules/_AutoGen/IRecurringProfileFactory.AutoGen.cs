using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.RecurringProfileModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IRecurringProfileFactoryAutoGen : PayMammoth.Modules._AutoGen.IRecurringProfileBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IRecurringProfile>
    
    {
	    new PayMammoth.Modules.RecurringProfileModule.IRecurringProfile CreateNewItem();
        new PayMammoth.Modules.RecurringProfileModule.IRecurringProfile GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfileModule.IRecurringProfile FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfileModule.IRecurringProfile FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfileModule.IRecurringProfile FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
