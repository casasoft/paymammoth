using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ImageSizingInfoModule;
using CS.WebComponentsGeneralV3.Cms.ImageSizingInfoModule;
using PayMammoth.Frontend.ImageSizingInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ImageSizingInfoCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ImageSizingInfoModule.ImageSizingInfoBaseCmsInfo
    {

		public ImageSizingInfoCmsInfo_AutoGen(BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase item)
            : base(item)
        {

        }
        

        private ImageSizingInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ImageSizingInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ImageSizingInfoFrontend FrontendItem
        {
            get { return (ImageSizingInfoFrontend) base.FrontendItem; }
        }
        
        
		public new ImageSizingInfo DbItem
        {
            get
            {
                return (ImageSizingInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

        this.MaximumWidth = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo>.GetPropertyBySelector( item => item.MaximumWidth),
        						isEditable: true, isVisible: true);

        this.MaximumHeight = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo>.GetPropertyBySelector( item => item.MaximumHeight),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
