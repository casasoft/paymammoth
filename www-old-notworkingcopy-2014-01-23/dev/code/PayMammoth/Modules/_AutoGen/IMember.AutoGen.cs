using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberModule;
using PayMammoth.Modules.MemberModule;

//IUserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IMemberAutoGen : BusinessLogic_v3.Modules.MemberModule.IMemberBase
    {




// [interface_userclass_properties]

		//IProperty_LinkedToObject
        new PayMammoth.Modules.CultureDetailsModule.ICultureDetails PreferredCultureInfo {get; set; }

		//IProperty_LinkedToObject
        new PayMammoth.Modules.MemberModule.IMember ReferredByMember {get; set; }

		//IProperty_LinkedToObject
        new PayMammoth.Modules.OrderModule.IOrder CurrentShoppingCart {get; set; }

   
    

// [interface_userclassonly_properties]

   

                                     

// [interface_userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.ContactFormModule.IContactForm> ContactForms { get; }

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.MemberModule.IMember> Members { get; }

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> AccountBalanceHistoryItems { get; }

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.OrderModule.IOrder> Orders { get; }

        //UserClassClass_CollectionManyToManyLeftSide
        
        new ICollectionManager<PayMammoth.Modules.CategoryModule.ICategory> SubscribedCategories { get; }
		
        
        



// [interface_userclassonly_collections]

 


    	
    	
      
      

    }
}
