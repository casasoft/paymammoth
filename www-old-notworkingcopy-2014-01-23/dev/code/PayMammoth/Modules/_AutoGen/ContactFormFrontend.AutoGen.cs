using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ContactFormModule;
using PayMammoth.Modules.ContactFormModule;
using BusinessLogic_v3.Modules.ContactFormModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ContactFormModule.ContactFormFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ContactFormModule.IContactForm> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ContactFormModule.ContactFormFrontend ToFrontend(this PayMammoth.Modules.ContactFormModule.IContactForm item)
        {
        	return PayMammoth.Frontend.ContactFormModule.ContactFormFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ContactFormFrontend_AutoGen : BusinessLogic_v3.Frontend.ContactFormModule.ContactFormBaseFrontend

    {
		
        
        protected ContactFormFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ContactFormModule.IContactForm Data
        {
            get { return (PayMammoth.Modules.ContactFormModule.IContactForm)base.Data; }
            set { base.Data = value; }

        }

        public new static ContactFormFrontend CreateNewItem()
        {
            return (ContactFormFrontend)BusinessLogic_v3.Frontend.ContactFormModule.ContactFormBaseFrontend.CreateNewItem();
        }
        
        public new static ContactFormFrontend Get(BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase data)
        {
            return (ContactFormFrontend)BusinessLogic_v3.Frontend.ContactFormModule.ContactFormBaseFrontend.Get(data);
        }
        public new static List<ContactFormFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ContactFormModule.ContactFormBaseFrontend.GetList(dataList).Cast<ContactFormFrontend>().ToList();
        }


    }
}
