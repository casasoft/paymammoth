using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.RecurringProfileTransactionModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class RecurringProfileTransactionMap_AutoGen : RecurringProfileTransactionMap_AutoGen_Z
    {
        public RecurringProfileTransactionMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.RecurringProfilePayment, RecurringProfilePaymentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Timestamp, TimestampMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ResponseContent, ResponseContentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayReference, PaymentGatewayReferenceMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.LinkedNotificationMessage, LinkedNotificationMessageMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TotalAmount, TotalAmountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.OtherData, OtherDataMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Status, StatusMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.StatusLog, StatusLogMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsFakePayment, IsFakePaymentMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class RecurringProfileTransactionMap_AutoGen_Z : PayMammoth.Modules._AutoGen.RecurringProfileTransactionBaseMap<RecurringProfileTransactionImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void RecurringProfilePaymentMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "RecurringProfilePaymentId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        

//ClassMap_PropertyLinkedObject

        protected virtual void LinkedNotificationMessageMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "LinkedNotificationMessageId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

            
        
        
    }
}
