using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class NotificationMessage_AutoGen : NotificationMessageBase, PayMammoth.Modules._AutoGen.INotificationMessageAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.PaymentRequestModule.PaymentRequest LinkedPaymentRequest
        {
            get
            {
                return (PayMammoth.Modules.PaymentRequestModule.PaymentRequest)base.LinkedPaymentRequest;
            }
            set
            {
                base.LinkedPaymentRequest = value;
            }
        }

		PayMammoth.Modules.PaymentRequestModule.IPaymentRequest INotificationMessageAutoGen.LinkedPaymentRequest
        {
            get
            {
            	return this.LinkedPaymentRequest;
                
            }
            set
            {
                this.LinkedPaymentRequest = (PayMammoth.Modules.PaymentRequestModule.PaymentRequest)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount WebsiteAccount
        {
            get
            {
                return (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount)base.WebsiteAccount;
            }
            set
            {
                base.WebsiteAccount = value;
            }
        }

		PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount INotificationMessageAutoGen.WebsiteAccount
        {
            get
            {
            	return this.WebsiteAccount;
                
            }
            set
            {
                this.WebsiteAccount = (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new NotificationMessageFactory Factory
        {
            get
            {
                return (NotificationMessageFactory)NotificationMessageBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<NotificationMessage, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
