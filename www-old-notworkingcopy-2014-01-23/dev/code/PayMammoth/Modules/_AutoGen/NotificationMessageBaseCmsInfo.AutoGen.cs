using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen.NotificationMessageModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class NotificationMessageBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.NotificationMessageBase>
    {
		public NotificationMessageBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.NotificationMessageBase dbItem)
            : base(PayMammoth.Modules._AutoGen.NotificationMessageBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new NotificationMessageBaseFrontend FrontendItem
        {
            get { return (NotificationMessageBaseFrontend)base.FrontendItem; }

        }
        public new NotificationMessageBase DbItem
        {
            get
            {
                return (NotificationMessageBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo NotificationType { get; protected set; }

        public CmsPropertyInfo DateCreated { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo SendToUrl { get; protected set; }

        public CmsPropertyInfo AcknowledgedByRecipient { get; protected set; }

        public CmsPropertyInfo RetryCount { get; protected set; }

        public CmsPropertyInfo LastRetryOn { get; protected set; }

        public CmsPropertyInfo LinkedPaymentRequest { get; protected set; }

        public CmsPropertyInfo Message { get; protected set; }

        public CmsPropertyInfo StatusCode { get; protected set; }

        public CmsPropertyInfo NextRetryOn { get; protected set; }

        public CmsPropertyInfo SendingFailed { get; protected set; }

        public CmsPropertyInfo AcknowledgedByRecipientOn { get; protected set; }

        public CmsPropertyInfo WebsiteAccount { get; protected set; }

        public CmsPropertyInfo StatusLog { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
