using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.PaymentRequestItemDetailModule;
using PayMammoth.Cms.PaymentRequestItemDetailModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestItemDetailCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBaseCmsFactory
    {
        public static new PaymentRequestItemDetailBaseCmsFactory Instance 
        {
         	get { return (PaymentRequestItemDetailBaseCmsFactory)PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override PaymentRequestItemDetailBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	PaymentRequestItemDetail item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = PaymentRequestItemDetail.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        PaymentRequestItemDetailCmsInfo cmsItem = new PaymentRequestItemDetailCmsInfo(item);
            return cmsItem;
        }    
        
        public override PaymentRequestItemDetailBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBase item)
        {
            return new PaymentRequestItemDetailCmsInfo((PaymentRequestItemDetail)item);
            
        }

    }
}
