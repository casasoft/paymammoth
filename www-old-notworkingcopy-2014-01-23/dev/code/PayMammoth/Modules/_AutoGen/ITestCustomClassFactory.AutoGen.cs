using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.TestCustomClassModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ITestCustomClassFactoryAutoGen : PayMammoth.Modules._AutoGen.ITestCustomClassBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ITestCustomClass>
    
    {
	    new PayMammoth.Modules.TestCustomClassModule.ITestCustomClass CreateNewItem();
        new PayMammoth.Modules.TestCustomClassModule.ITestCustomClass GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.TestCustomClassModule.ITestCustomClass FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.TestCustomClassModule.ITestCustomClass FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.TestCustomClassModule.ITestCustomClass FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TestCustomClassModule.ITestCustomClass> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
