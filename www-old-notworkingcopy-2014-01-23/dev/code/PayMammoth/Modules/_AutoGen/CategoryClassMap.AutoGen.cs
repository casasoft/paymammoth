using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.CategoryModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class CategoryMap_AutoGen : CategoryMap_AutoGen_Z
    {
        public CategoryMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.TitleSingular, TitleSingularMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TitleSingular_Search, TitleSingular_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TitlePlural, TitlePluralMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TitlePlural_Search, TitlePlural_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TitleSEO, TitleSEOMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaKeywords, MetaKeywordsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaDescription, MetaDescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CanDelete, CanDeleteMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ComingSoon, ComingSoonMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PriorityAll, PriorityAllMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Brand, BrandMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ImportReference, ImportReferenceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DontShowInNavigation, DontShowInNavigationMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.Category_CultureInfos, Category_CultureInfosMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ChildCategoryLinks, ChildCategoryLinksMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ParentCategoryLinks, ParentCategoryLinksMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class CategoryMap_AutoGen_Z : BusinessLogic_v3.Modules.CategoryModule.CategoryBaseMap<CategoryImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
