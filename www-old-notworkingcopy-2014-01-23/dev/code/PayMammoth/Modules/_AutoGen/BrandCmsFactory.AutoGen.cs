using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.BrandModule;
using PayMammoth.Cms.BrandModule;
using CS.WebComponentsGeneralV3.Cms.BrandModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class BrandCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.BrandModule.BrandBaseCmsFactory
    {
        public static new BrandBaseCmsFactory Instance 
        {
         	get { return (BrandBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.BrandModule.BrandBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override BrandBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Brand.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            BrandCmsInfo cmsItem = new BrandCmsInfo(item);
            return cmsItem;
        }    
        
        public override BrandBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.BrandModule.BrandBase item)
        {
            return new BrandCmsInfo((Brand)item);
            
        }

    }
}
