using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.TicketCommentModule;
using PayMammoth.Cms.TicketCommentModule;
using CS.WebComponentsGeneralV3.Cms.TicketCommentModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class TicketCommentCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.TicketCommentModule.TicketCommentBaseCmsFactory
    {
        public static new TicketCommentBaseCmsFactory Instance 
        {
         	get { return (TicketCommentBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.TicketCommentModule.TicketCommentBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override TicketCommentBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = TicketComment.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            TicketCommentCmsInfo cmsItem = new TicketCommentCmsInfo(item);
            return cmsItem;
        }    
        
        public override TicketCommentBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase item)
        {
            return new TicketCommentCmsInfo((TicketComment)item);
            
        }

    }
}
