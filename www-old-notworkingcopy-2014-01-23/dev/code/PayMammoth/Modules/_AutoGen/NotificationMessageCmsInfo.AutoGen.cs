using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.NotificationMessageModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class NotificationMessageCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.NotificationMessageBaseCmsInfo
    {

		public NotificationMessageCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.NotificationMessageBase item)
            : base(item)
        {

        }
        

        private NotificationMessageFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = NotificationMessageFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new NotificationMessageFrontend FrontendItem
        {
            get { return (NotificationMessageFrontend) base.FrontendItem; }
        }
        
        
		public new NotificationMessage DbItem
        {
            get
            {
                return (NotificationMessage)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.NotificationType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.NotificationType),
        						isEditable: true, isVisible: true);

        this.DateCreated = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.DateCreated),
        						isEditable: true, isVisible: true);

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

        this.SendToUrl = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.SendToUrl),
        						isEditable: true, isVisible: true);

        this.AcknowledgedByRecipient = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.AcknowledgedByRecipient),
        						isEditable: true, isVisible: true);

        this.RetryCount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.RetryCount),
        						isEditable: true, isVisible: true);

        this.LastRetryOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.LastRetryOn),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.LinkedPaymentRequest = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.LinkedPaymentRequest),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.NotificationMessages)));

        
        this.Message = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.Message),
        						isEditable: true, isVisible: true);

        this.StatusCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.StatusCode),
        						isEditable: true, isVisible: true);

        this.NextRetryOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.NextRetryOn),
        						isEditable: true, isVisible: true);

        this.SendingFailed = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.SendingFailed),
        						isEditable: true, isVisible: true);

        this.AcknowledgedByRecipientOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.AcknowledgedByRecipientOn),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.WebsiteAccount = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.WebsiteAccount),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.NotificationMessages)));

        
        this.StatusLog = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>.GetPropertyBySelector( item => item.StatusLog),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
