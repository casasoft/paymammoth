using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.Brand_CultureInfoModule;
using BusinessLogic_v3.Modules.Brand_CultureInfoModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Brand_CultureInfo_AutoGen : Brand_CultureInfoBase, PayMammoth.Modules._AutoGen.IBrand_CultureInfoAutoGen
	
      
      
      , IMultilingualContentInfo
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CultureDetailsModule.CultureDetails CultureInfo
        {
            get
            {
                return (PayMammoth.Modules.CultureDetailsModule.CultureDetails)base.CultureInfo;
            }
            set
            {
                base.CultureInfo = value;
            }
        }

		PayMammoth.Modules.CultureDetailsModule.ICultureDetails IBrand_CultureInfoAutoGen.CultureInfo
        {
            get
            {
            	return this.CultureInfo;
                
            }
            set
            {
                this.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.BrandModule.Brand Brand
        {
            get
            {
                return (PayMammoth.Modules.BrandModule.Brand)base.Brand;
            }
            set
            {
                base.Brand = value;
            }
        }

		PayMammoth.Modules.BrandModule.IBrand IBrand_CultureInfoAutoGen.Brand
        {
            get
            {
            	return this.Brand;
                
            }
            set
            {
                this.Brand = (PayMammoth.Modules.BrandModule.Brand)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new Brand_CultureInfoFactory Factory
        {
            get
            {
                return (Brand_CultureInfoFactory)Brand_CultureInfoBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Brand_CultureInfo, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
