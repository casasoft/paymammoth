using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.RecurringProfileTransactionModule;
using PayMammoth.Cms.RecurringProfileTransactionModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfileTransactionCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfileTransactionBaseCmsFactory
    {
        public static new RecurringProfileTransactionBaseCmsFactory Instance 
        {
         	get { return (RecurringProfileTransactionBaseCmsFactory)PayMammoth.Modules._AutoGen.RecurringProfileTransactionBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override RecurringProfileTransactionBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	RecurringProfileTransaction item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = RecurringProfileTransaction.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        RecurringProfileTransactionCmsInfo cmsItem = new RecurringProfileTransactionCmsInfo(item);
            return cmsItem;
        }    
        
        public override RecurringProfileTransactionBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.RecurringProfileTransactionBase item)
        {
            return new RecurringProfileTransactionCmsInfo((RecurringProfileTransaction)item);
            
        }

    }
}
