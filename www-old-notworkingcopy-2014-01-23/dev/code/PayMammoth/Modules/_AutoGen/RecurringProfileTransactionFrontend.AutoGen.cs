using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.RecurringProfileTransactionModule;
using PayMammoth.Modules.RecurringProfileTransactionModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.RecurringProfileTransactionModule.RecurringProfileTransactionFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.RecurringProfileTransactionModule.RecurringProfileTransactionFrontend ToFrontend(this PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction item)
        {
        	return PayMammoth.Frontend.RecurringProfileTransactionModule.RecurringProfileTransactionFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfileTransactionFrontend_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfileTransactionModule.RecurringProfileTransactionBaseFrontend

    {
		
        
        protected RecurringProfileTransactionFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction Data
        {
            get { return (PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction)base.Data; }
            set { base.Data = value; }

        }

        public new static RecurringProfileTransactionFrontend CreateNewItem()
        {
            return (RecurringProfileTransactionFrontend)PayMammoth.Modules._AutoGen.RecurringProfileTransactionModule.RecurringProfileTransactionBaseFrontend.CreateNewItem();
        }
        
        public new static RecurringProfileTransactionFrontend Get(PayMammoth.Modules._AutoGen.IRecurringProfileTransactionBase data)
        {
            return (RecurringProfileTransactionFrontend)PayMammoth.Modules._AutoGen.RecurringProfileTransactionModule.RecurringProfileTransactionBaseFrontend.Get(data);
        }
        public new static List<RecurringProfileTransactionFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IRecurringProfileTransactionBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.RecurringProfileTransactionModule.RecurringProfileTransactionBaseFrontend.GetList(dataList).Cast<RecurringProfileTransactionFrontend>().ToList();
        }


    }
}
