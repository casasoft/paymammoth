using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ContentText_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.ContentText_CultureInfoModule;
using PayMammoth.Frontend.ContentText_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ContentText_CultureInfoCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ContentText_CultureInfoModule.ContentText_CultureInfoBaseCmsInfo
    {

		public ContentText_CultureInfoCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase item)
            : base(item)
        {

        }
        

        private ContentText_CultureInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ContentText_CultureInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ContentText_CultureInfoFrontend FrontendItem
        {
            get { return (ContentText_CultureInfoFrontend) base.FrontendItem; }
        }
        
        
		public new ContentText_CultureInfo DbItem
        {
            get
            {
                return (ContentText_CultureInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.CultureInfo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo>.GetPropertyBySelector( item => item.CultureInfo),
                null));

        
//InitFieldUser_LinkedProperty
			this.ContentText = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo>.GetPropertyBySelector( item => item.ContentText),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.ContentText_CultureInfos)));

        
        this.Content = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo>.GetPropertyBySelector( item => item.Content),
        						isEditable: true, isVisible: true);

        this.ImageAlternateText = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo>.GetPropertyBySelector( item => item.ImageAlternateText),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
