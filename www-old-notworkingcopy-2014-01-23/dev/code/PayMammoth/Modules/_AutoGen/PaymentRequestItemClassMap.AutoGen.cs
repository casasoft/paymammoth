using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.PaymentRequestItemModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class PaymentRequestItemMap_AutoGen : PaymentRequestItemMap_AutoGen_Z
    {
        public PaymentRequestItemMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Quantity, QuantityMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PriceExcTax, PriceExcTaxMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TaxAmount, TaxAmountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ShippingAmount, ShippingAmountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HandlingAmount, HandlingAmountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Discount, DiscountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.WidthInMm, WidthInMmMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HeightInMm, HeightInMmMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LengthInMm, LengthInMmMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.PaymentRequest, PaymentRequestMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class PaymentRequestItemMap_AutoGen_Z : PayMammoth.Modules._AutoGen.PaymentRequestItemBaseMap<PaymentRequestItemImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void PaymentRequestMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "PaymentRequestId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

            
        
        
    }
}
