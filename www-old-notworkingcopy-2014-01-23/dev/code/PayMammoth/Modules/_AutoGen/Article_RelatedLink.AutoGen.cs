using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.Article_RelatedLinkModule;
using BusinessLogic_v3.Modules.Article_RelatedLinkModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Article_RelatedLink_AutoGen : Article_RelatedLinkBase, PayMammoth.Modules._AutoGen.IArticle_RelatedLinkAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ArticleModule.Article ParentPage
        {
            get
            {
                return (PayMammoth.Modules.ArticleModule.Article)base.ParentPage;
            }
            set
            {
                base.ParentPage = value;
            }
        }

		PayMammoth.Modules.ArticleModule.IArticle IArticle_RelatedLinkAutoGen.ParentPage
        {
            get
            {
            	return this.ParentPage;
                
            }
            set
            {
                this.ParentPage = (PayMammoth.Modules.ArticleModule.Article)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ArticleModule.Article RelatedPage
        {
            get
            {
                return (PayMammoth.Modules.ArticleModule.Article)base.RelatedPage;
            }
            set
            {
                base.RelatedPage = value;
            }
        }

		PayMammoth.Modules.ArticleModule.IArticle IArticle_RelatedLinkAutoGen.RelatedPage
        {
            get
            {
            	return this.RelatedPage;
                
            }
            set
            {
                this.RelatedPage = (PayMammoth.Modules.ArticleModule.Article)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new Article_RelatedLinkFactory Factory
        {
            get
            {
                return (Article_RelatedLinkFactory)Article_RelatedLinkBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Article_RelatedLink, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
