using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.WebsiteAccount_CultureInfoModule;
using PayMammoth.Cms.WebsiteAccount_CultureInfoModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WebsiteAccount_CultureInfoCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBaseCmsFactory
    {
        public static new WebsiteAccount_CultureInfoBaseCmsFactory Instance 
        {
         	get { return (WebsiteAccount_CultureInfoBaseCmsFactory)PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override WebsiteAccount_CultureInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	WebsiteAccount_CultureInfo item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = WebsiteAccount_CultureInfo.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        WebsiteAccount_CultureInfoCmsInfo cmsItem = new WebsiteAccount_CultureInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override WebsiteAccount_CultureInfoBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase item)
        {
            return new WebsiteAccount_CultureInfoCmsInfo((WebsiteAccount_CultureInfo)item);
            
        }

    }
}
