using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.MemberResponsibleLimitsModule;
using PayMammoth.Modules.MemberResponsibleLimitsModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberResponsibleLimitsFactoryAutoGen : BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseFactory, 
    				PayMammoth.Modules._AutoGen.IMemberResponsibleLimitsFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IMemberResponsibleLimits>
    
    {
    
     	public new MemberResponsibleLimits ReloadItemFromDatabase(BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase item)
        {
         	return (MemberResponsibleLimits)base.ReloadItemFromDatabase(item);
        }
     	
        public MemberResponsibleLimitsFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(MemberResponsibleLimitsImpl);
			PayMammoth.Cms.MemberResponsibleLimitsModule.MemberResponsibleLimitsCmsFactory._initialiseStaticInstance();
        }
        public static new MemberResponsibleLimitsFactory Instance
        {
            get
            {
                return (MemberResponsibleLimitsFactory)MemberResponsibleLimitsBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<MemberResponsibleLimits> FindAll()
        {
            return base.FindAll().Cast<MemberResponsibleLimits>();
        }*/
    /*    public new IEnumerable<MemberResponsibleLimits> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<MemberResponsibleLimits>();

        }*/
        public new IEnumerable<MemberResponsibleLimits> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<MemberResponsibleLimits>();
            
        }
        public new IEnumerable<MemberResponsibleLimits> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<MemberResponsibleLimits>();
            
            
        }
     /*  public new IEnumerable<MemberResponsibleLimits> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<MemberResponsibleLimits>();


        }*/
        public new IEnumerable<MemberResponsibleLimits> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<MemberResponsibleLimits>();
            
        }
        public new IEnumerable<MemberResponsibleLimits> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<MemberResponsibleLimits>();
        }
        
        public new MemberResponsibleLimits FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (MemberResponsibleLimits)base.FindItem(query);
        }
        public new MemberResponsibleLimits FindItem(IQueryOver query)
        {
            return (MemberResponsibleLimits)base.FindItem(query);
        }
        
        IMemberResponsibleLimits IMemberResponsibleLimitsFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new MemberResponsibleLimits CreateNewItem()
        {
            return (MemberResponsibleLimits)base.CreateNewItem();
        }

        public new MemberResponsibleLimits GetByPrimaryKey(long pKey)
        {
            return (MemberResponsibleLimits)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<MemberResponsibleLimits, MemberResponsibleLimits> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<MemberResponsibleLimits>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits>) base.FindAll(session);

       }
       public new PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits> Members
     
        PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits IBaseDbFactory<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits IBaseDbFactory<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits> IBaseDbFactory<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits IBaseDbFactory<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits> IBaseDbFactory<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IMemberResponsibleLimits> IMemberResponsibleLimitsFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IMemberResponsibleLimits> IMemberResponsibleLimitsFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IMemberResponsibleLimits> IMemberResponsibleLimitsFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IMemberResponsibleLimits IMemberResponsibleLimitsFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IMemberResponsibleLimits IMemberResponsibleLimitsFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IMemberResponsibleLimits IMemberResponsibleLimitsFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IMemberResponsibleLimits> IMemberResponsibleLimitsFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IMemberResponsibleLimits IMemberResponsibleLimitsFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
