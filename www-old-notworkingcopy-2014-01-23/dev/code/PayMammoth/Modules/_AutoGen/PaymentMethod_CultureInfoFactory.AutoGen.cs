using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentMethod_CultureInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentMethod_CultureInfoFactoryAutoGen : PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IPaymentMethod_CultureInfoFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IPaymentMethod_CultureInfo>
    
    {
    
     	public new PaymentMethod_CultureInfo ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase item)
        {
         	return (PaymentMethod_CultureInfo)base.ReloadItemFromDatabase(item);
        }
     	static PaymentMethod_CultureInfoFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public PaymentMethod_CultureInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(PaymentMethod_CultureInfoImpl);
			PayMammoth.Cms.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoCmsFactory._initialiseStaticInstance();
        }
        public static new PaymentMethod_CultureInfoFactory Instance
        {
            get
            {
                return (PaymentMethod_CultureInfoFactory)PaymentMethod_CultureInfoBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<PaymentMethod_CultureInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<PaymentMethod_CultureInfo>();
            
        }
        public new IEnumerable<PaymentMethod_CultureInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<PaymentMethod_CultureInfo>();
            
            
        }
    
        public new IEnumerable<PaymentMethod_CultureInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<PaymentMethod_CultureInfo>();
            
        }
        public new IEnumerable<PaymentMethod_CultureInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<PaymentMethod_CultureInfo>();
        }
        
        public new PaymentMethod_CultureInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (PaymentMethod_CultureInfo)base.FindItem(query);
        }
        public new PaymentMethod_CultureInfo FindItem(IQueryOver query)
        {
            return (PaymentMethod_CultureInfo)base.FindItem(query);
        }
        
        IPaymentMethod_CultureInfo IPaymentMethod_CultureInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new PaymentMethod_CultureInfo CreateNewItem()
        {
            return (PaymentMethod_CultureInfo)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<PaymentMethod_CultureInfo, PaymentMethod_CultureInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<PaymentMethod_CultureInfo>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo> Members
     
        PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo IBaseDbFactory<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo IBaseDbFactory<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo> IBaseDbFactory<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo IBaseDbFactory<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo> IBaseDbFactory<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IPaymentMethod_CultureInfo> IPaymentMethod_CultureInfoFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentMethod_CultureInfo> IPaymentMethod_CultureInfoFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentMethod_CultureInfo> IPaymentMethod_CultureInfoFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IPaymentMethod_CultureInfo IPaymentMethod_CultureInfoFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentMethod_CultureInfo IPaymentMethod_CultureInfoFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentMethod_CultureInfo IPaymentMethod_CultureInfoFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IPaymentMethod_CultureInfo> IPaymentMethod_CultureInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IPaymentMethod_CultureInfo IPaymentMethod_CultureInfoFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
