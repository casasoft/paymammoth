using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.Article_CultureInfoModule;
using PayMammoth.Modules.Article_CultureInfoModule;

//IUserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IArticle_CultureInfoAutoGen : BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBase
    {




// [interface_userclass_properties]

		//IProperty_LinkedToObject
        new PayMammoth.Modules.CultureDetailsModule.ICultureDetails CultureInfo {get; set; }

		//IProperty_LinkedToObject
        new PayMammoth.Modules.ArticleModule.IArticle Article {get; set; }

   
    

// [interface_userclassonly_properties]

   

                                     

// [interface_userclass_collections_nhibernate]




// [interface_userclassonly_collections]

 


    	
    	
      
      

    }
}
