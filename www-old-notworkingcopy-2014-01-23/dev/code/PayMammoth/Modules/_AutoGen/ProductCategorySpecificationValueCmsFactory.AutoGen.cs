using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ProductCategorySpecificationValueModule;
using PayMammoth.Cms.ProductCategorySpecificationValueModule;
using CS.WebComponentsGeneralV3.Cms.ProductCategorySpecificationValueModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCategorySpecificationValueCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseCmsFactory
    {
        public static new ProductCategorySpecificationValueBaseCmsFactory Instance 
        {
         	get { return (ProductCategorySpecificationValueBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ProductCategorySpecificationValueBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = ProductCategorySpecificationValue.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            ProductCategorySpecificationValueCmsInfo cmsItem = new ProductCategorySpecificationValueCmsInfo(item);
            return cmsItem;
        }    
        
        public override ProductCategorySpecificationValueBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase item)
        {
            return new ProductCategorySpecificationValueCmsInfo((ProductCategorySpecificationValue)item);
            
        }

    }
}
