using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.CmsAuditLogModule;
using PayMammoth.Modules.CmsAuditLogModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class CmsAuditLogFactoryAutoGen : BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBaseFactory, 
    				PayMammoth.Modules._AutoGen.ICmsAuditLogFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<ICmsAuditLog>
    
    {
    
     	public new CmsAuditLog ReloadItemFromDatabase(BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase item)
        {
         	return (CmsAuditLog)base.ReloadItemFromDatabase(item);
        }
     	
        public CmsAuditLogFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(CmsAuditLogImpl);
			PayMammoth.Cms.CmsAuditLogModule.CmsAuditLogCmsFactory._initialiseStaticInstance();
        }
        public static new CmsAuditLogFactory Instance
        {
            get
            {
                return (CmsAuditLogFactory)CmsAuditLogBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<CmsAuditLog> FindAll()
        {
            return base.FindAll().Cast<CmsAuditLog>();
        }*/
    /*    public new IEnumerable<CmsAuditLog> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<CmsAuditLog>();

        }*/
        public new IEnumerable<CmsAuditLog> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<CmsAuditLog>();
            
        }
        public new IEnumerable<CmsAuditLog> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<CmsAuditLog>();
            
            
        }
     /*  public new IEnumerable<CmsAuditLog> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<CmsAuditLog>();


        }*/
        public new IEnumerable<CmsAuditLog> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<CmsAuditLog>();
            
        }
        public new IEnumerable<CmsAuditLog> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<CmsAuditLog>();
        }
        
        public new CmsAuditLog FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (CmsAuditLog)base.FindItem(query);
        }
        public new CmsAuditLog FindItem(IQueryOver query)
        {
            return (CmsAuditLog)base.FindItem(query);
        }
        
        ICmsAuditLog ICmsAuditLogFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new CmsAuditLog CreateNewItem()
        {
            return (CmsAuditLog)base.CreateNewItem();
        }

        public new CmsAuditLog GetByPrimaryKey(long pKey)
        {
            return (CmsAuditLog)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<CmsAuditLog, CmsAuditLog> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<CmsAuditLog>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>) base.FindAll(session);

       }
       public new PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog> Members
     
        PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog IBaseDbFactory<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog IBaseDbFactory<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog> IBaseDbFactory<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog IBaseDbFactory<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog> IBaseDbFactory<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ICmsAuditLog> ICmsAuditLogFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICmsAuditLog> ICmsAuditLogFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICmsAuditLog> ICmsAuditLogFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       ICmsAuditLog ICmsAuditLogFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICmsAuditLog ICmsAuditLogFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICmsAuditLog ICmsAuditLogFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ICmsAuditLog> ICmsAuditLogFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ICmsAuditLog ICmsAuditLogFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
