using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.EmailTextModule;
using PayMammoth.Cms.EmailTextModule;
using CS.WebComponentsGeneralV3.Cms.EmailTextModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class EmailTextCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.EmailTextModule.EmailTextBaseCmsFactory
    {
        public static new EmailTextBaseCmsFactory Instance 
        {
         	get { return (EmailTextBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.EmailTextModule.EmailTextBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override EmailTextBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	EmailText item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = EmailText.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        EmailTextCmsInfo cmsItem = new EmailTextCmsInfo(item);
            return cmsItem;
        }    
        
        public override EmailTextBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase item)
        {
            return new EmailTextCmsInfo((EmailText)item);
            
        }

    }
}
