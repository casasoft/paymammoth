using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.OrderItemModule;
using BusinessLogic_v3.Modules.OrderItemModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class OrderItem_AutoGen : OrderItemBase, PayMammoth.Modules._AutoGen.IOrderItemAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.OrderModule.Order Order
        {
            get
            {
                return (PayMammoth.Modules.OrderModule.Order)base.Order;
            }
            set
            {
                base.Order = value;
            }
        }

		PayMammoth.Modules.OrderModule.IOrder IOrderItemAutoGen.Order
        {
            get
            {
            	return this.Order;
                
            }
            set
            {
                this.Order = (PayMammoth.Modules.OrderModule.Order)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new OrderItemFactory Factory
        {
            get
            {
                return (OrderItemFactory)OrderItemBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<OrderItem, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
