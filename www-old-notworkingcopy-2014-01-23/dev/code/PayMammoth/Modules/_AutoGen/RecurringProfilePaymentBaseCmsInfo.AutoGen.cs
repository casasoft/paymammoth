using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen.RecurringProfilePaymentModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfilePaymentBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.RecurringProfilePaymentBase>
    {
		public RecurringProfilePaymentBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.RecurringProfilePaymentBase dbItem)
            : base(PayMammoth.Modules._AutoGen.RecurringProfilePaymentBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new RecurringProfilePaymentBaseFrontend FrontendItem
        {
            get { return (RecurringProfilePaymentBaseFrontend)base.FrontendItem; }

        }
        public new RecurringProfilePaymentBase DbItem
        {
            get
            {
                return (RecurringProfilePaymentBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo RecurringProfile { get; protected set; }

        public CmsPropertyInfo FailureCount { get; protected set; }

        public CmsPropertyInfo NextRetryOn { get; protected set; }

        public CmsPropertyInfo PaymentDueOn { get; protected set; }

        public CmsPropertyInfo Status { get; protected set; }

        public CmsPropertyInfo StatusLog { get; protected set; }

        public CmsPropertyInfo InitiatedByPayMammoth { get; protected set; }

        public CmsPropertyInfo MarkAsPaymentFailedIfNotPaidBy { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
