using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.SettingModule;
using PayMammoth.Modules.SettingModule;
using BusinessLogic_v3.Modules.SettingModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.SettingModule.SettingFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.SettingModule.ISetting> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.SettingModule.SettingFrontend ToFrontend(this PayMammoth.Modules.SettingModule.ISetting item)
        {
        	return PayMammoth.Frontend.SettingModule.SettingFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class SettingFrontend_AutoGen : BusinessLogic_v3.Frontend.SettingModule.SettingBaseFrontend

    {
		
        
        protected SettingFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.SettingModule.ISetting Data
        {
            get { return (PayMammoth.Modules.SettingModule.ISetting)base.Data; }
            set { base.Data = value; }

        }

        public new static SettingFrontend CreateNewItem()
        {
            return (SettingFrontend)BusinessLogic_v3.Frontend.SettingModule.SettingBaseFrontend.CreateNewItem();
        }
        
        public new static SettingFrontend Get(BusinessLogic_v3.Modules.SettingModule.ISettingBase data)
        {
            return (SettingFrontend)BusinessLogic_v3.Frontend.SettingModule.SettingBaseFrontend.Get(data);
        }
        public new static List<SettingFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.SettingModule.ISettingBase> dataList)
        {
            return BusinessLogic_v3.Frontend.SettingModule.SettingBaseFrontend.GetList(dataList).Cast<SettingFrontend>().ToList();
        }


    }
}
