using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.CmsUserRoleModule;
using BusinessLogic_v3.Modules.CmsUserRoleModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class CmsUserRole_AutoGen : CmsUserRoleBase, PayMammoth.Modules._AutoGen.ICmsUserRoleAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]




// [userclass_collections_nhibernate]

        //UserClass_CollectionManyToManyLeftSide
        
        internal class __CmsUsersCollectionInfo : DbCollectionManyToManyManager<
			PayMammoth.Modules.CmsUserRoleModule.CmsUserRole,
			PayMammoth.Modules.CmsUserModule.CmsUser,
			PayMammoth.Modules.CmsUserModule.ICmsUser>,
            IDbCollectionManyToManyInfo<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole, PayMammoth.Modules.CmsUserModule.CmsUser>
        {
            private PayMammoth.Modules.CmsUserRoleModule.CmsUserRole _item = null;
            public __CmsUsersCollectionInfo(PayMammoth.Modules.CmsUserRoleModule.CmsUserRole item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<PayMammoth.Modules.CmsUserModule.CmsUser> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.CmsUserModule.CmsUser>)_item.__collection__CmsUsers; }
            }

            #region IDbCollectionManyToManyInfo<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole,PayMammoth.Modules.CmsUserModule.CmsUser> Members

            public ICollectionManager<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole> GetLinkedCollectionForItem(PayMammoth.Modules.CmsUserModule.CmsUser item)
            {
                return item.CmsUserRoles;
                
            }


            #endregion
        }

        private __CmsUsersCollectionInfo _CmsUsers = null;
        internal new __CmsUsersCollectionInfo CmsUsers
        {
            get
            {
                if (_CmsUsers == null)
                    _CmsUsers = new __CmsUsersCollectionInfo((PayMammoth.Modules.CmsUserRoleModule.CmsUserRole)this);
                return _CmsUsers;
            }
        }    
           
        ICollectionManager<PayMammoth.Modules.CmsUserModule.ICmsUser> ICmsUserRoleAutoGen.CmsUsers
        {
            get {  return this.CmsUsers; }
        }
           
        

        
          
		public  static new CmsUserRoleFactory Factory
        {
            get
            {
                return (CmsUserRoleFactory)CmsUserRoleBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<CmsUserRole, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
