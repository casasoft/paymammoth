using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IRecurringProfilePaymentBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IRecurringProfileBase RecurringProfile {get; set; }

		//Iproperty_normal
        int FailureCount { get; set; }
		//Iproperty_normal
        DateTime? NextRetryOn { get; set; }
		//Iproperty_normal
        DateTime PaymentDueOn { get; set; }
		//Iproperty_normal
        PayMammoth.Connector.Enums.RecurringProfilePaymentStatus Status { get; set; }
		//Iproperty_normal
        string StatusLog { get; set; }
		//Iproperty_normal
        bool InitiatedByPayMammoth { get; set; }
		//Iproperty_normal
        DateTime? MarkAsPaymentFailedIfNotPaidBy { get; set; }
		//Iproperty_normal
        string Identifier { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
