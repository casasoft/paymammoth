using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace PayMammoth.Modules._AutoGen
{
	public abstract class PaymentRequestItemBaseCmsFactory_AutoGen : CmsFactoryBase<PaymentRequestItemBaseCmsInfo, PaymentRequestItemBase>
    {
       
       public new static PaymentRequestItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (PaymentRequestItemBaseCmsFactory)CmsFactoryBase<PaymentRequestItemBaseCmsInfo, PaymentRequestItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = PaymentRequestItemBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "PaymentRequestItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "PaymentRequestItem";

            this.QueryStringParamID = "PaymentRequestItemId";

            cmsInfo.TitlePlural = "Payment Request Items";

            cmsInfo.TitleSingular =  "Payment Request Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public PaymentRequestItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "PaymentRequestItem/";


        }
       
    }

}
