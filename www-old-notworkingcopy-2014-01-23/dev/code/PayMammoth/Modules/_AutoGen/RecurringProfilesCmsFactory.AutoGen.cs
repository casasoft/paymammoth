using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.RecurringProfilesModule;
using PayMammoth.Cms.RecurringProfilesModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfilesCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfilesBaseCmsFactory
    {
        public static new RecurringProfilesBaseCmsFactory Instance 
        {
         	get { return (RecurringProfilesBaseCmsFactory)PayMammoth.Modules._AutoGen.RecurringProfilesBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override RecurringProfilesBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	RecurringProfiles item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = RecurringProfiles.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        RecurringProfilesCmsInfo cmsItem = new RecurringProfilesCmsInfo(item);
            return cmsItem;
        }    
        
        public override RecurringProfilesBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.RecurringProfilesBase item)
        {
            return new RecurringProfilesCmsInfo((RecurringProfiles)item);
            
        }

    }
}
