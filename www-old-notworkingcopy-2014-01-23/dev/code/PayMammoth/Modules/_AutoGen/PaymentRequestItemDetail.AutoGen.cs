using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.PaymentRequestItemDetailModule;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class PaymentRequestItemDetail_AutoGen : PaymentRequestItemDetailBase, PayMammoth.Modules._AutoGen.IPaymentRequestItemDetailAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.PaymentRequestModule.PaymentRequest PaymentRequest
        {
            get
            {
                return (PayMammoth.Modules.PaymentRequestModule.PaymentRequest)base.PaymentRequest;
            }
            set
            {
                base.PaymentRequest = value;
            }
        }

		PayMammoth.Modules.PaymentRequestModule.IPaymentRequest IPaymentRequestItemDetailAutoGen.PaymentRequest
        {
            get
            {
            	return this.PaymentRequest;
                
            }
            set
            {
                this.PaymentRequest = (PayMammoth.Modules.PaymentRequestModule.PaymentRequest)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new PaymentRequestItemDetailFactory Factory
        {
            get
            {
                return (PaymentRequestItemDetailFactory)PaymentRequestItemDetailBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<PaymentRequestItemDetail, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
