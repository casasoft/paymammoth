using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.Article_RelatedLinkModule;
using PayMammoth.Modules.Article_RelatedLinkModule;
using BusinessLogic_v3.Modules.Article_RelatedLinkModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.Article_RelatedLinkModule.Article_RelatedLinkFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.Article_RelatedLinkModule.Article_RelatedLinkFrontend ToFrontend(this PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink item)
        {
        	return PayMammoth.Frontend.Article_RelatedLinkModule.Article_RelatedLinkFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class Article_RelatedLinkFrontend_AutoGen : BusinessLogic_v3.Frontend.Article_RelatedLinkModule.Article_RelatedLinkBaseFrontend

    {
		
        
        protected Article_RelatedLinkFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink Data
        {
            get { return (PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink)base.Data; }
            set { base.Data = value; }

        }

        public new static Article_RelatedLinkFrontend CreateNewItem()
        {
            return (Article_RelatedLinkFrontend)BusinessLogic_v3.Frontend.Article_RelatedLinkModule.Article_RelatedLinkBaseFrontend.CreateNewItem();
        }
        
        public new static Article_RelatedLinkFrontend Get(BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBase data)
        {
            return (Article_RelatedLinkFrontend)BusinessLogic_v3.Frontend.Article_RelatedLinkModule.Article_RelatedLinkBaseFrontend.Get(data);
        }
        public new static List<Article_RelatedLinkFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBase> dataList)
        {
            return BusinessLogic_v3.Frontend.Article_RelatedLinkModule.Article_RelatedLinkBaseFrontend.GetList(dataList).Cast<Article_RelatedLinkFrontend>().ToList();
        }


    }
}
