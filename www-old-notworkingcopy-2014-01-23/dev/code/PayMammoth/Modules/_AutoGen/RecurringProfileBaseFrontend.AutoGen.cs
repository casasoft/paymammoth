using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules._AutoGen.RecurringProfileModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Modules._AutoGen.RecurringProfileModule.RecurringProfileBaseFrontend> ToFrontendBaseList(this IEnumerable<PayMammoth.Modules._AutoGen.IRecurringProfileBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static PayMammoth.Modules._AutoGen.RecurringProfileModule.RecurringProfileBaseFrontend ToFrontendBase(this PayMammoth.Modules._AutoGen.IRecurringProfileBase item)
        {
        	return PayMammoth.Modules._AutoGen.RecurringProfileModule.RecurringProfileBaseFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfileBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<RecurringProfileBaseFrontend, PayMammoth.Modules._AutoGen.IRecurringProfileBase>

    {
		
        
        protected RecurringProfileBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
