using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
   
    public abstract class WebsiteAccountBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<WebsiteAccountBase, IWebsiteAccountBase>, IWebsiteAccountBaseFactoryAutoGen
    
    {
    
        public WebsiteAccountBaseFactoryAutoGen()
        {
        	
            
        }
        
		static WebsiteAccountBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static WebsiteAccountBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<WebsiteAccountBaseFactory>(null);
            }
        }
        
		public IQueryOver<WebsiteAccountBase, WebsiteAccountBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<WebsiteAccountBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
