using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.NotificationMessageModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class NotificationMessageMap_AutoGen : NotificationMessageMap_AutoGen_Z
    {
        public NotificationMessageMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.NotificationType, NotificationTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DateCreated, DateCreatedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SendToUrl, SendToUrlMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AcknowledgedByRecipient, AcknowledgedByRecipientMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.RetryCount, RetryCountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LastRetryOn, LastRetryOnMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.LinkedPaymentRequest, LinkedPaymentRequestMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Message, MessageMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.StatusCode, StatusCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NextRetryOn, NextRetryOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SendingFailed, SendingFailedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AcknowledgedByRecipientOn, AcknowledgedByRecipientOnMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.WebsiteAccount, WebsiteAccountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.StatusLog, StatusLogMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class NotificationMessageMap_AutoGen_Z : PayMammoth.Modules._AutoGen.NotificationMessageBaseMap<NotificationMessageImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void LinkedPaymentRequestMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "LinkedPaymentRequestId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        

//ClassMap_PropertyLinkedObject

        protected virtual void WebsiteAccountMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "WebsiteAccountId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

            
        
        
    }
}
