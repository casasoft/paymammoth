using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ArticleRatingModule;
using PayMammoth.Modules.ArticleRatingModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleRatingFactoryAutoGen : BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBaseFactory, 
    				PayMammoth.Modules._AutoGen.IArticleRatingFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<IArticleRating>
    
    {
    
     	public new ArticleRating ReloadItemFromDatabase(BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase item)
        {
         	return (ArticleRating)base.ReloadItemFromDatabase(item);
        }
     	
        public ArticleRatingFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ArticleRatingImpl);
			PayMammoth.Cms.ArticleRatingModule.ArticleRatingCmsFactory._initialiseStaticInstance();
        }
        public static new ArticleRatingFactory Instance
        {
            get
            {
                return (ArticleRatingFactory)ArticleRatingBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<ArticleRating> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ArticleRating>();
            
        }
        public new IEnumerable<ArticleRating> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ArticleRating>();
            
            
        }
    
        public new IEnumerable<ArticleRating> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ArticleRating>();
            
        }
        public new IEnumerable<ArticleRating> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ArticleRating>();
        }
        
        public new ArticleRating FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ArticleRating)base.FindItem(query);
        }
        public new ArticleRating FindItem(IQueryOver query)
        {
            return (ArticleRating)base.FindItem(query);
        }
        
        IArticleRating IArticleRatingFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ArticleRating CreateNewItem()
        {
            return (ArticleRating)base.CreateNewItem();
        }

        public new ArticleRating GetByPrimaryKey(long pKey)
        {
            return (ArticleRating)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<ArticleRating, ArticleRating> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ArticleRating>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ArticleRatingModule.ArticleRating GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.ArticleRatingModule.ArticleRating) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.ArticleRatingModule.ArticleRating> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleRatingModule.ArticleRating>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleRatingModule.ArticleRating> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleRatingModule.ArticleRating>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleRatingModule.ArticleRating> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleRatingModule.ArticleRating>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ArticleRatingModule.ArticleRating FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleRatingModule.ArticleRating) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ArticleRatingModule.ArticleRating FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleRatingModule.ArticleRating) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ArticleRatingModule.ArticleRating FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleRatingModule.ArticleRating) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleRatingModule.ArticleRating> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleRatingModule.ArticleRating>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ArticleRatingModule.IArticleRating> Members
     
        PayMammoth.Modules.ArticleRatingModule.IArticleRating IBaseDbFactory<PayMammoth.Modules.ArticleRatingModule.IArticleRating>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ArticleRatingModule.IArticleRating IBaseDbFactory<PayMammoth.Modules.ArticleRatingModule.IArticleRating>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.ArticleRatingModule.IArticleRating> IBaseDbFactory<PayMammoth.Modules.ArticleRatingModule.IArticleRating>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ArticleRatingModule.IArticleRating> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleRatingModule.IArticleRating>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ArticleRatingModule.IArticleRating> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleRatingModule.IArticleRating>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ArticleRatingModule.IArticleRating IBaseDbFactory<PayMammoth.Modules.ArticleRatingModule.IArticleRating>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ArticleRatingModule.IArticleRating BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleRatingModule.IArticleRating>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ArticleRatingModule.IArticleRating BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleRatingModule.IArticleRating>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ArticleRatingModule.IArticleRating> IBaseDbFactory<PayMammoth.Modules.ArticleRatingModule.IArticleRating>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IArticleRating> IArticleRatingFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticleRating> IArticleRatingFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticleRating> IArticleRatingFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IArticleRating IArticleRatingFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticleRating IArticleRatingFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticleRating IArticleRatingFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IArticleRating> IArticleRatingFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IArticleRating IArticleRatingFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
