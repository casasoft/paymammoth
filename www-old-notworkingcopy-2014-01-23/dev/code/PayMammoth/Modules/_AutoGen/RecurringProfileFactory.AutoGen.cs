using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfileFactoryAutoGen : PayMammoth.Modules._AutoGen.RecurringProfileBaseFactory, 
    				PayMammoth.Modules._AutoGen.IRecurringProfileFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IRecurringProfile>
    
    {
    
     	public new RecurringProfile ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.RecurringProfileBase item)
        {
         	return (RecurringProfile)base.ReloadItemFromDatabase(item);
        }
     	static RecurringProfileFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public RecurringProfileFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(RecurringProfileImpl);
			PayMammoth.Cms.RecurringProfileModule.RecurringProfileCmsFactory._initialiseStaticInstance();
        }
        public static new RecurringProfileFactory Instance
        {
            get
            {
                return (RecurringProfileFactory)RecurringProfileBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<RecurringProfile> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<RecurringProfile>();
            
        }
        public new IEnumerable<RecurringProfile> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<RecurringProfile>();
            
            
        }
    
        public new IEnumerable<RecurringProfile> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<RecurringProfile>();
            
        }
        public new IEnumerable<RecurringProfile> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<RecurringProfile>();
        }
        
        public new RecurringProfile FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (RecurringProfile)base.FindItem(query);
        }
        public new RecurringProfile FindItem(IQueryOver query)
        {
            return (RecurringProfile)base.FindItem(query);
        }
        
        IRecurringProfile IRecurringProfileFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new RecurringProfile CreateNewItem()
        {
            return (RecurringProfile)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<RecurringProfile, RecurringProfile> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<RecurringProfile>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.RecurringProfileModule.RecurringProfile GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.RecurringProfileModule.RecurringProfile) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.RecurringProfileModule.RecurringProfile> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfileModule.RecurringProfile> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfileModule.RecurringProfile> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>) base.FindAll(session);

       }
       public new PayMammoth.Modules.RecurringProfileModule.RecurringProfile FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfileModule.RecurringProfile) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.RecurringProfileModule.RecurringProfile FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfileModule.RecurringProfile) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.RecurringProfileModule.RecurringProfile FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfileModule.RecurringProfile) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfileModule.RecurringProfile> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile> Members
     
        PayMammoth.Modules.RecurringProfileModule.IRecurringProfile IBaseDbFactory<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.RecurringProfileModule.IRecurringProfile IBaseDbFactory<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile> IBaseDbFactory<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.RecurringProfileModule.IRecurringProfile IBaseDbFactory<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.RecurringProfileModule.IRecurringProfile BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.RecurringProfileModule.IRecurringProfile BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile> IBaseDbFactory<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IRecurringProfile> IRecurringProfileFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IRecurringProfile> IRecurringProfileFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IRecurringProfile> IRecurringProfileFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IRecurringProfile IRecurringProfileFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IRecurringProfile IRecurringProfileFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IRecurringProfile IRecurringProfileFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IRecurringProfile> IRecurringProfileFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IRecurringProfile IRecurringProfileFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
