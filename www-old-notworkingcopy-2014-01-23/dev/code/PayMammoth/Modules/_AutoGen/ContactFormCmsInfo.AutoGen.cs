using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ContactFormModule;
using CS.WebComponentsGeneralV3.Cms.ContactFormModule;
using PayMammoth.Frontend.ContactFormModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ContactFormCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ContactFormModule.ContactFormBaseCmsInfo
    {

		public ContactFormCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase item)
            : base(item)
        {

        }
        

        private ContactFormFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ContactFormFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ContactFormFrontend FrontendItem
        {
            get { return (ContactFormFrontend) base.FrontendItem; }
        }
        
        
		public new ContactForm DbItem
        {
            get
            {
                return (ContactForm)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.DateSent = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.DateSent),
        						isEditable: true, isVisible: true);

        this.Name = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Name),
        						isEditable: true, isVisible: true);

        this.Company = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Company),
        						isEditable: true, isVisible: true);

        this.Subject = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Subject),
        						isEditable: true, isVisible: true);

        this.Email = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Email),
        						isEditable: true, isVisible: true);

        this.Telephone = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Telephone),
        						isEditable: true, isVisible: true);

        this.Mobile = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Mobile),
        						isEditable: true, isVisible: true);

        this.Address1 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Address1),
        						isEditable: true, isVisible: true);

        this.Address2 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Address2),
        						isEditable: true, isVisible: true);

        this.Address3 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Address3),
        						isEditable: true, isVisible: true);

        this.ZIPCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.ZIPCode),
        						isEditable: true, isVisible: true);

        this.State = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.State),
        						isEditable: true, isVisible: true);

        this.CountryStr = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.CountryStr),
        						isEditable: true, isVisible: true);

        this.City = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.City),
        						isEditable: true, isVisible: true);

        this.Fax = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Fax),
        						isEditable: true, isVisible: true);

        this.Website = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Website),
        						isEditable: true, isVisible: true);

        this.IP = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.IP),
        						isEditable: true, isVisible: true);

        this.Enquiry = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Enquiry),
        						isEditable: true, isVisible: true);

        this.Remark = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Remark),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.SentByMember = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.SentByMember),
                null));

        
        this.Surname = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Surname),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Vacancy = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Vacancy),
                null));

        
        this.Username = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Username),
        						isEditable: true, isVisible: true);

        this.FileFilename = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.FileFilename),
        						isEditable: false, isVisible: false);

        this.HowDidYouFindUs = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.HowDidYouFindUs),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Product = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Product),
                null));

        
//InitFieldUser_LinkedProperty
			this.Event = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContactFormModule.ContactForm>.GetPropertyBySelector( item => item.Event),
                null));

        
	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
