using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace PayMammoth.Modules._AutoGen
{
	public abstract class PaymentMethod_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<PaymentMethod_CultureInfoBaseCmsInfo, PaymentMethod_CultureInfoBase>
    {
       
       public new static PaymentMethod_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (PaymentMethod_CultureInfoBaseCmsFactory)CmsFactoryBase<PaymentMethod_CultureInfoBaseCmsInfo, PaymentMethod_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = PaymentMethod_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "PaymentMethod_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            if (string.IsNullOrWhiteSpace(cmsInfo.Name )) cmsInfo.Name = "PaymentMethod_CultureInfo";

            if (string.IsNullOrWhiteSpace(this.QueryStringParamID )) this.QueryStringParamID = "PaymentMethod_CultureInfoId";

            if (string.IsNullOrWhiteSpace(cmsInfo.TitlePlural)) cmsInfo.TitlePlural = "Payment Method_ Culture Infos";

            if (string.IsNullOrWhiteSpace(cmsInfo.TitleSingular )) cmsInfo.TitleSingular =  "Payment Method_ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public PaymentMethod_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "PaymentMethod_CultureInfo/";
			UsedInProject = PaymentMethod_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
