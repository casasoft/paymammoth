using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ProductModule;
using PayMammoth.Modules.ProductModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductFactoryAutoGen : BusinessLogic_v3.Modules.ProductModule.ProductBaseFactory, 
    				PayMammoth.Modules._AutoGen.IProductFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProduct>
    
    {
    
     	public new Product ReloadItemFromDatabase(BusinessLogic_v3.Modules.ProductModule.ProductBase item)
        {
         	return (Product)base.ReloadItemFromDatabase(item);
        }
     	
        public ProductFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ProductImpl);
			PayMammoth.Cms.ProductModule.ProductCmsFactory._initialiseStaticInstance();
        }
        public static new ProductFactory Instance
        {
            get
            {
                return (ProductFactory)ProductBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<Product> FindAll()
        {
            return base.FindAll().Cast<Product>();
        }*/
    /*    public new IEnumerable<Product> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<Product>();

        }*/
        public new IEnumerable<Product> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Product>();
            
        }
        public new IEnumerable<Product> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Product>();
            
            
        }
     /*  public new IEnumerable<Product> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<Product>();


        }*/
        public new IEnumerable<Product> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Product>();
            
        }
        public new IEnumerable<Product> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Product>();
        }
        
        public new Product FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Product)base.FindItem(query);
        }
        public new Product FindItem(IQueryOver query)
        {
            return (Product)base.FindItem(query);
        }
        
        IProduct IProductFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Product CreateNewItem()
        {
            return (Product)base.CreateNewItem();
        }

        public new Product GetByPrimaryKey(long pKey)
        {
            return (Product)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Product, Product> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<Product>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

            var itemList = Product.Factory.FindAll();

            var CultureInfos = CultureDetails.Factory.FindAll();

            foreach (var item in itemList)
            {
            	item.CheckItemCultureInfos(autoSaveInDB: true, CultureInfos: CultureInfos);
            }

  

        }        


#endregion
        public new PayMammoth.Modules.ProductModule.Product GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.ProductModule.Product) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.ProductModule.Product> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductModule.Product>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductModule.Product> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductModule.Product>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductModule.Product> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductModule.Product>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ProductModule.Product FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductModule.Product) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ProductModule.Product FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductModule.Product) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ProductModule.Product FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductModule.Product) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductModule.Product> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductModule.Product>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ProductModule.IProduct> Members
     
        PayMammoth.Modules.ProductModule.IProduct IBaseDbFactory<PayMammoth.Modules.ProductModule.IProduct>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ProductModule.IProduct IBaseDbFactory<PayMammoth.Modules.ProductModule.IProduct>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductModule.IProduct> IBaseDbFactory<PayMammoth.Modules.ProductModule.IProduct>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ProductModule.IProduct> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductModule.IProduct>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ProductModule.IProduct> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductModule.IProduct>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ProductModule.IProduct IBaseDbFactory<PayMammoth.Modules.ProductModule.IProduct>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductModule.IProduct CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductModule.IProduct>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductModule.IProduct CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductModule.IProduct>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductModule.IProduct> IBaseDbFactory<PayMammoth.Modules.ProductModule.IProduct>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IProduct> IProductFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProduct> IProductFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProduct> IProductFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IProduct IProductFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProduct IProductFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProduct IProductFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IProduct> IProductFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IProduct IProductFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
