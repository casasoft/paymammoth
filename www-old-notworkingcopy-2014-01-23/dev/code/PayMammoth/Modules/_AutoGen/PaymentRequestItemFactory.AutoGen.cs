using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentRequestItemModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestItemFactoryAutoGen : PayMammoth.Modules._AutoGen.PaymentRequestItemBaseFactory, 
    				PayMammoth.Modules._AutoGen.IPaymentRequestItemFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IPaymentRequestItem>
    
    {
    
     	
        public PaymentRequestItemFactoryAutoGen ()
        {

			this.UsedInProject = true;
			_mainTypeCreated = typeof(PaymentRequestItemImpl);
			PayMammoth.Cms.PaymentRequestItemModule.PaymentRequestItemCmsFactory._initialiseStaticInstance();
        }
        public static new PaymentRequestItemFactory Instance
        {
            get
            {
                return (PaymentRequestItemFactory)PaymentRequestItemBaseFactory.Instance;
            }
      
      

        }
        
        
        public new IEnumerable<PaymentRequestItem> FindAll()
        {
            return base.FindAll().Cast<PaymentRequestItem>();
        }
        public new IEnumerable<PaymentRequestItem> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<PaymentRequestItem>();

        }
        public new IEnumerable<PaymentRequestItem> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<PaymentRequestItem>();
            
        }
        public new IEnumerable<PaymentRequestItem> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<PaymentRequestItem>();
            
            
        }
        public new IEnumerable<PaymentRequestItem> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<PaymentRequestItem>();


        }
        public new IEnumerable<PaymentRequestItem> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<PaymentRequestItem>();
            
        }
        public new IEnumerable<PaymentRequestItem> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<PaymentRequestItem>();
        }
        
        public new PaymentRequestItem FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (PaymentRequestItem)base.FindItem(query);
        }
        public new PaymentRequestItem FindItem(IQueryOver query)
        {
            return (PaymentRequestItem)base.FindItem(query);
        }
        
        IPaymentRequestItem IPaymentRequestItemFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new PaymentRequestItem CreateNewItem()
        {
            return (PaymentRequestItem)base.CreateNewItem();
        }

        public new PaymentRequestItem GetByPrimaryKey(long pKey)
        {
            return (PaymentRequestItem)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<PaymentRequestItem, PaymentRequestItem> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<PaymentRequestItem>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem) base.GetByPrimaryKey(id, session);
        }
       public IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem>) base.FindAll(session);

       }
       public PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem) base.FindItem(query, session);

       }
       public PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem) base.FindItem(query, session);

       }

       public PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem) base.FindItem(session);

       }
       public IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.PaymentRequestItem>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem> Members
     
        PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem> IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem> IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IPaymentRequestItem> IPaymentRequestItemFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentRequestItem> IPaymentRequestItemFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentRequestItem> IPaymentRequestItemFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }

       IPaymentRequestItem IPaymentRequestItemFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentRequestItem IPaymentRequestItemFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentRequestItem IPaymentRequestItemFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IPaymentRequestItem> IPaymentRequestItemFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IPaymentRequestItem IPaymentRequestItemFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
