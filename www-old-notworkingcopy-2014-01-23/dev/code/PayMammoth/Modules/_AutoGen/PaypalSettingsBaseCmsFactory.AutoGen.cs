using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace PayMammoth.Modules._AutoGen
{
	public abstract class PaypalSettingsBaseCmsFactory_AutoGen : CmsFactoryBase<PaypalSettingsBaseCmsInfo, PaypalSettingsBase>
    {
       
       public new static PaypalSettingsBaseCmsFactory Instance
	    {
	         get
	         {
                 return (PaypalSettingsBaseCmsFactory)CmsFactoryBase<PaypalSettingsBaseCmsInfo, PaypalSettingsBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = PaypalSettingsBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "PaypalSettings.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "PaypalSettings";

            this.QueryStringParamID = "PaypalSettingsId";

            cmsInfo.TitlePlural = "Paypal Settings";

            cmsInfo.TitleSingular =  "Paypal Settings";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public PaypalSettingsBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "PaypalSettings/";


        }
       
    }

}
