using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace PayMammoth.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "RecurringProfiles")]
    public abstract class RecurringProfilesBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IRecurringProfilesBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static RecurringProfilesBaseFactory Factory
        {
            get
            {
                return RecurringProfilesBaseFactory.Instance; 
            }
        }    
        /*
        public static RecurringProfilesBase CreateNewItem()
        {
            return (RecurringProfilesBase)Factory.CreateNewItem();
        }

		public static RecurringProfilesBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<RecurringProfilesBase, RecurringProfilesBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static RecurringProfilesBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static RecurringProfilesBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<RecurringProfilesBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<RecurringProfilesBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<RecurringProfilesBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? PaymentTransactionID
		{
		 	get 
		 	{
		 		return (this.PaymentTransaction != null ? (long?)this.PaymentTransaction.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase _paymenttransaction;
        public virtual PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase PaymentTransaction 
        {
        	get
        	{
        		return _paymenttransaction;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymenttransaction,value);
        		_paymenttransaction = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IPaymentRequestTransactionBase PayMammoth.Modules._AutoGen.IRecurringProfilesBaseAutoGen.PaymentTransaction 
        {
            get
            {
            	return this.PaymentTransaction;
            }
            set
            {
            	this.PaymentTransaction = (PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _profileidentifierongateway;
        
        public virtual string ProfileIdentifierOnGateway 
        {
        	get
        	{
        		return _profileidentifierongateway;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_profileidentifierongateway,value);
        		_profileidentifierongateway = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private PayMammoth.Connector.Enums.PaymentMethodSpecific _paymentgateway;
        
        public virtual PayMammoth.Connector.Enums.PaymentMethodSpecific PaymentGateway 
        {
        	get
        	{
        		return _paymentgateway;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgateway,value);
        		_paymentgateway = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paypippaidentifier;
        
        public virtual string PayPippaIdentifier 
        {
        	get
        	{
        		return _paypippaidentifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypippaidentifier,value);
        		_paypippaidentifier = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
