using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.OrderItemModule;
using CS.WebComponentsGeneralV3.Cms.OrderItemModule;
using PayMammoth.Frontend.OrderItemModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class OrderItemCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.OrderItemModule.OrderItemBaseCmsInfo
    {

		public OrderItemCmsInfo_AutoGen(BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase item)
            : base(item)
        {

        }
        

        private OrderItemFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = OrderItemFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new OrderItemFrontend FrontendItem
        {
            get { return (OrderItemFrontend) base.FrontendItem; }
        }
        
        
		public new OrderItem DbItem
        {
            get
            {
                return (OrderItem)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.Quantity = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.Quantity),
        						isEditable: true, isVisible: true);

        this.PriceExcTaxPerUnit = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.PriceExcTaxPerUnit),
        						isEditable: true, isVisible: true);

        this.TaxAmountPerUnit = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.TaxAmountPerUnit),
        						isEditable: true, isVisible: true);

        this.ItemReference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.ItemReference),
        						isEditable: true, isVisible: true);

        this.Remarks = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.Remarks),
        						isEditable: true, isVisible: true);

        this.DiscountPerUnit = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.DiscountPerUnit),
        						isEditable: true, isVisible: true);

        this.ItemID = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.ItemID),
        						isEditable: true, isVisible: true);

        this.ItemType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.ItemType),
        						isEditable: true, isVisible: true);

        this.ShipmentWeight = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.ShipmentWeight),
        						isEditable: true, isVisible: true);

        this.ShippingCost = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.ShippingCost),
        						isEditable: true, isVisible: true);

        this.HandlingCost = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.HandlingCost),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Order = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.Order),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.OrderItems)));

        
//InitFieldUser_LinkedProperty
			this.SpecialOffer = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.SpecialOffer),
                null));

        
//InitFieldUser_LinkedProperty
			this.SpecialOfferVoucherCode = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.SpecialOfferVoucherCode),
                null));

        
//InitFieldUser_LinkedProperty
			this.LinkedProductVariation = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderItemModule.OrderItem>.GetPropertyBySelector( item => item.LinkedProductVariation),
                null));

        
	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
