using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ProductVariationModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ProductVariationMap_AutoGen : ProductVariationMap_AutoGen_Z
    {
        public ProductVariationMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.ReferenceCode, ReferenceCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SupplierRefCode, SupplierRefCodeMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Product, ProductMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Barcode, BarcodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Quantity, QuantityMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ReorderAmount, ReorderAmountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ImportReference, ImportReferenceMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.OrderItems, OrderItemsMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ProductVariation_CultureInfos, ProductVariation_CultureInfosMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.MediaItems, MediaItemsMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ProductVariationMap_AutoGen_Z : BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBaseMap<ProductVariationImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
