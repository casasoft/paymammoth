using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.PaymentMethod_CultureInfoModule;
using PayMammoth.Cms.PaymentMethod_CultureInfoModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentMethod_CultureInfoCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBaseCmsFactory
    {
        public static new PaymentMethod_CultureInfoBaseCmsFactory Instance 
        {
         	get { return (PaymentMethod_CultureInfoBaseCmsFactory)PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override PaymentMethod_CultureInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	PaymentMethod_CultureInfo item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = PaymentMethod_CultureInfo.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        PaymentMethod_CultureInfoCmsInfo cmsItem = new PaymentMethod_CultureInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override PaymentMethod_CultureInfoBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase item)
        {
            return new PaymentMethod_CultureInfoCmsInfo((PaymentMethod_CultureInfo)item);
            
        }

    }
}
