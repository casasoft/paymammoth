using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace PayMammoth.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "WebsiteAccount_CultureInfo")]
    public abstract class WebsiteAccount_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IWebsiteAccount_CultureInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
      , IMultilingualContentInfo
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static WebsiteAccount_CultureInfoBaseFactory Factory
        {
            get
            {
                return WebsiteAccount_CultureInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static WebsiteAccount_CultureInfoBase CreateNewItem()
        {
            return (WebsiteAccount_CultureInfoBase)Factory.CreateNewItem();
        }

		public static WebsiteAccount_CultureInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<WebsiteAccount_CultureInfoBase, WebsiteAccount_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static WebsiteAccount_CultureInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static WebsiteAccount_CultureInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<WebsiteAccount_CultureInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<WebsiteAccount_CultureInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<WebsiteAccount_CultureInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? CultureInfoID
		{
		 	get 
		 	{
		 		return (this.CultureInfo != null ? (long?)this.CultureInfo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase _cultureinfo;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase CultureInfo 
        {
        	get
        	{
        		return _cultureinfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cultureinfo,value);
        		_cultureinfo = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase PayMammoth.Modules._AutoGen.IWebsiteAccount_CultureInfoBaseAutoGen.CultureInfo 
        {
            get
            {
            	return this.CultureInfo;
            }
            set
            {
            	this.CultureInfo = (BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase) value;
            }
        }
            
/*
		public virtual long? WebsiteAccountID
		{
		 	get 
		 	{
		 		return (this.WebsiteAccount != null ? (long?)this.WebsiteAccount.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.WebsiteAccountBase _websiteaccount;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Modules._AutoGen.WebsiteAccountBase WebsiteAccount 
        {
        	get
        	{
        		return _websiteaccount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_websiteaccount,value);
        		_websiteaccount = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IWebsiteAccountBase PayMammoth.Modules._AutoGen.IWebsiteAccount_CultureInfoBaseAutoGen.WebsiteAccount 
        {
            get
            {
            	return this.WebsiteAccount;
            }
            set
            {
            	this.WebsiteAccount = (PayMammoth.Modules._AutoGen.WebsiteAccountBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayrealexdescription;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string PaymentGatewayRealexDescription 
        {
        	get
        	{
        		return _paymentgatewayrealexdescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayrealexdescription,value);
        		_paymentgatewayrealexdescription = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayapcodescription;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string PaymentGatewayApcoDescription 
        {
        	get
        	{
        		return _paymentgatewayapcodescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayapcodescription,value);
        		_paymentgatewayapcodescription = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewaytransactiumdescription;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string PaymentGatewayTransactiumDescription 
        {
        	get
        	{
        		return _paymentgatewaytransactiumdescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewaytransactiumdescription,value);
        		_paymentgatewaytransactiumdescription = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)




#region IMultilingualContentInfo Members

        long IMultilingualContentInfo.CultureInfoID
        {
            get
            {                          
            	if (this.CultureInfo != null)
                	return this.CultureInfo.ID;
               	else
               		return 0;
            }
        }
  		BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject IMultilingualContentInfo.GetLinkedItem()
        {
            return this.WebsiteAccount;
        }
#endregion



#endregion       

		

    }
}
