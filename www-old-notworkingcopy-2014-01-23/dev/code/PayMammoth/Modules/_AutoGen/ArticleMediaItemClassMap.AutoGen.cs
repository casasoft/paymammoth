using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ArticleMediaItemModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ArticleMediaItemMap_AutoGen : ArticleMediaItemMap_AutoGen_Z
    {
        public ArticleMediaItemMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Caption, CaptionMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.ContentPage, ContentPageMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.VideoDuration, VideoDurationMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Filename, FilenameMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ArticleMediaItemMap_AutoGen_Z : BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBaseMap<ArticleMediaItemImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
