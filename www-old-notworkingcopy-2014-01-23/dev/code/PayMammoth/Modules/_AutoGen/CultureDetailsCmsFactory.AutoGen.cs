using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.CultureDetailsModule;
using PayMammoth.Cms.CultureDetailsModule;
using CS.WebComponentsGeneralV3.Cms.CultureDetailsModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class CultureDetailsCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.CultureDetailsModule.CultureDetailsBaseCmsFactory
    {
        public static new CultureDetailsBaseCmsFactory Instance 
        {
         	get { return (CultureDetailsBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.CultureDetailsModule.CultureDetailsBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override CultureDetailsBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	CultureDetails item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = CultureDetails.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        CultureDetailsCmsInfo cmsItem = new CultureDetailsCmsInfo(item);
            return cmsItem;
        }    
        
        public override CultureDetailsBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase item)
        {
            return new CultureDetailsCmsInfo((CultureDetails)item);
            
        }

    }
}
