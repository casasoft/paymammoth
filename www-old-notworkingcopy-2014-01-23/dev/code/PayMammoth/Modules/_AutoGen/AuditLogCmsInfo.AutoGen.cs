using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.AuditLogModule;
using CS.WebComponentsGeneralV3.Cms.AuditLogModule;
using PayMammoth.Frontend.AuditLogModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class AuditLogCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.AuditLogModule.AuditLogBaseCmsInfo
    {

		public AuditLogCmsInfo_AutoGen(BusinessLogic_v3.Modules.AuditLogModule.AuditLogBase item)
            : base(item)
        {

        }
        

        private AuditLogFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = AuditLogFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new AuditLogFrontend FrontendItem
        {
            get { return (AuditLogFrontend) base.FrontendItem; }
        }
        
        
		public new AuditLog DbItem
        {
            get
            {
                return (AuditLog)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.ItemType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.ItemType),
        						isEditable: true, isVisible: true);

        this.ItemID = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.ItemID),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.CmsUser = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.CmsUser),
                null));

        
        this.CmsUserIpAddress = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.CmsUserIpAddress),
        						isEditable: true, isVisible: true);

        this.DateTime = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.DateTime),
        						isEditable: true, isVisible: true);

        this.StackTrace = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.StackTrace),
        						isEditable: true, isVisible: true);

        this.Method = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.Method),
        						isEditable: true, isVisible: true);

        this.Changes = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.Changes),
        						isEditable: true, isVisible: true);

        this.Remarks = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.Remarks),
        						isEditable: true, isVisible: true);

        this.Message = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.Message),
        						isEditable: true, isVisible: true);

        this.ItemTypeFull = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.ItemTypeFull),
        						isEditable: true, isVisible: true);

        this.UpdateType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AuditLogModule.AuditLog>.GetPropertyBySelector( item => item.UpdateType),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
