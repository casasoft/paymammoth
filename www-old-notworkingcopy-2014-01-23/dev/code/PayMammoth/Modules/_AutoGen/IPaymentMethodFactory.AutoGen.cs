using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentMethodModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IPaymentMethodFactoryAutoGen : PayMammoth.Modules._AutoGen.IPaymentMethodBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IPaymentMethod>
    
    {
	    new PayMammoth.Modules.PaymentMethodModule.IPaymentMethod CreateNewItem();
        new PayMammoth.Modules.PaymentMethodModule.IPaymentMethod GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentMethodModule.IPaymentMethod FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentMethodModule.IPaymentMethod FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentMethodModule.IPaymentMethod FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
