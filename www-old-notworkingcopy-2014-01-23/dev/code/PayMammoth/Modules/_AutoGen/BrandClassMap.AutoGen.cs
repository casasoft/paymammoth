using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.BrandModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class BrandMap_AutoGen : BrandMap_AutoGen_Z
    {
        public BrandMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.BrandLogoFilename, BrandLogoFilenameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsFeatured, IsFeaturedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Url, UrlMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ImportReference, ImportReferenceMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.Brand_CultureInfos, Brand_CultureInfosMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class BrandMap_AutoGen_Z : BusinessLogic_v3.Modules.BrandModule.BrandBaseMap<BrandImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
