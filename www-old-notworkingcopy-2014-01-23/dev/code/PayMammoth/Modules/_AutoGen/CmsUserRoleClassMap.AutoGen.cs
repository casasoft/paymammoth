using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.CmsUserRoleModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class CmsUserRoleMap_AutoGen : CmsUserRoleMap_AutoGen_Z
    {
        public CmsUserRoleMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_ManyToManyRightInit             
			 addCollection(x => x.CmsUsers, CmsUsersMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.ManyToMany);
             
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class CmsUserRoleMap_AutoGen_Z : BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBaseMap<CmsUserRoleImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
