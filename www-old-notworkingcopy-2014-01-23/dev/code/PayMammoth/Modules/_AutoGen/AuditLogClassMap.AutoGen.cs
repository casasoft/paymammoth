using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.AuditLogModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class AuditLogMap_AutoGen : AuditLogMap_AutoGen_Z
    {
        public AuditLogMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.ItemType, ItemTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ItemID, ItemIDMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.CmsUser, CmsUserMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CmsUserIpAddress, CmsUserIpAddressMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DateTime, DateTimeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.StackTrace, StackTraceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Method, MethodMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Changes, ChangesMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Remarks, RemarksMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Message, MessageMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ItemTypeFull, ItemTypeFullMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.UpdateType, UpdateTypeMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class AuditLogMap_AutoGen_Z : BusinessLogic_v3.Modules.AuditLogModule.AuditLogBaseMap<AuditLogImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
