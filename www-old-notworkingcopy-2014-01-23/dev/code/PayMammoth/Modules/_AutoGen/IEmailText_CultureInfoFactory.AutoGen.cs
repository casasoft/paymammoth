using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.EmailText_CultureInfoModule;
using PayMammoth.Modules.EmailText_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IEmailText_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IEmailText_CultureInfo>
    
    {
	    new PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo CreateNewItem();
        new PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
