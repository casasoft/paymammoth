using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.CategoryModule;
using BusinessLogic_v3.Modules.CategoryModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Category_AutoGen : CategoryBase, PayMammoth.Modules._AutoGen.ICategoryAutoGen
	                 
      , IMultilingualItem
      
      
      
    {
        
            

    
    
    
		

#region Multilingual
        public PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo GetCurrentCultureInfo()
        {
            return (PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo)__getCurrentCultureInfo();
        }             
          
    	public new PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo)base.GetCultureInfoByLanguage(lang);
        }

#endregion
        
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.BrandModule.Brand Brand
        {
            get
            {
                return (PayMammoth.Modules.BrandModule.Brand)base.Brand;
            }
            set
            {
                base.Brand = value;
            }
        }

		PayMammoth.Modules.BrandModule.IBrand ICategoryAutoGen.Brand
        {
            get
            {
            	return this.Brand;
                
            }
            set
            {
                this.Brand = (PayMammoth.Modules.BrandModule.Brand)value;
            }
        }



// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __Category_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.CategoryModule.Category, 
        	PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo,
        	PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.CategoryModule.Category, PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>
        {
            private PayMammoth.Modules.CategoryModule.Category _item = null;
            public __Category_CultureInfosCollectionInfo(PayMammoth.Modules.CategoryModule.Category item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>)_item.__collection__Category_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.CategoryModule.Category, PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>.SetLinkOnItem(PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo item, PayMammoth.Modules.CategoryModule.Category value)
            {
                item.Category = value;
            }
        }
        
        private __Category_CultureInfosCollectionInfo _Category_CultureInfos = null;
        internal new __Category_CultureInfosCollectionInfo Category_CultureInfos
        {
            get
            {
                if (_Category_CultureInfos == null)
                    _Category_CultureInfos = new __Category_CultureInfosCollectionInfo((PayMammoth.Modules.CategoryModule.Category)this);
                return _Category_CultureInfos;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo> ICategoryAutoGen.Category_CultureInfos
        {
            get {  return this.Category_CultureInfos; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ChildCategoryLinksCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.CategoryModule.Category, 
        	PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink,
        	PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.CategoryModule.Category, PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>
        {
            private PayMammoth.Modules.CategoryModule.Category _item = null;
            public __ChildCategoryLinksCollectionInfo(PayMammoth.Modules.CategoryModule.Category item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>)_item.__collection__ChildCategoryLinks; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.CategoryModule.Category, PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>.SetLinkOnItem(PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink item, PayMammoth.Modules.CategoryModule.Category value)
            {
                item.ParentCategory = value;
            }
        }
        
        private __ChildCategoryLinksCollectionInfo _ChildCategoryLinks = null;
        internal new __ChildCategoryLinksCollectionInfo ChildCategoryLinks
        {
            get
            {
                if (_ChildCategoryLinks == null)
                    _ChildCategoryLinks = new __ChildCategoryLinksCollectionInfo((PayMammoth.Modules.CategoryModule.Category)this);
                return _ChildCategoryLinks;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> ICategoryAutoGen.ChildCategoryLinks
        {
            get {  return this.ChildCategoryLinks; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ParentCategoryLinksCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.CategoryModule.Category, 
        	PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink,
        	PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.CategoryModule.Category, PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>
        {
            private PayMammoth.Modules.CategoryModule.Category _item = null;
            public __ParentCategoryLinksCollectionInfo(PayMammoth.Modules.CategoryModule.Category item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>)_item.__collection__ParentCategoryLinks; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.CategoryModule.Category, PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>.SetLinkOnItem(PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink item, PayMammoth.Modules.CategoryModule.Category value)
            {
                item.ChildCategory = value;
            }
        }
        
        private __ParentCategoryLinksCollectionInfo _ParentCategoryLinks = null;
        internal new __ParentCategoryLinksCollectionInfo ParentCategoryLinks
        {
            get
            {
                if (_ParentCategoryLinks == null)
                    _ParentCategoryLinks = new __ParentCategoryLinksCollectionInfo((PayMammoth.Modules.CategoryModule.Category)this);
                return _ParentCategoryLinks;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> ICategoryAutoGen.ParentCategoryLinks
        {
            get {  return this.ParentCategoryLinks; }
        }
           

        
          
		public  static new CategoryFactory Factory
        {
            get
            {
                return (CategoryFactory)CategoryBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Category, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


        public override void __CheckItemCultureInfos(bool autoSaveInDB, IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> CultureInfos = null)
        {
        	BusinessLogic_v3.Classes.NHibernateClasses.Transactions.MyTransaction t = null;
            if (autoSaveInDB)
                t = beginTransaction();
            MultilingualChecker multilingualChecker = new MultilingualChecker();
            multilingualChecker.ItemToAdd+=new MultilingualChecker.ItemToAddHandler(multilingualChecker_ItemToAdd);
            multilingualChecker.ItemRemoved += new MultilingualChecker.ItemRemovedHandler(multilingualChecker_ItemRemoved);
            multilingualChecker.CheckItemCultureInfos(this, CultureInfos);
            if (autoSaveInDB)
            {
                this.Save();
                t.Commit();
                t.Dispose();
            }
        }


        private void multilingualChecker_ItemToAdd(BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase cultureDetails)
        {
            var itemCultureInfo = PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo.Factory.CreateNewItem();

            itemCultureInfo.Category = (Category)this;  //item link
            itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture


// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.TitleSingular = this.__TitleSingular_cultureBase;

                        itemCultureInfo.TitleSingular_Search = this.__TitleSingular_Search_cultureBase;

                        itemCultureInfo.TitlePlural = this.__TitlePlural_cultureBase;

                        itemCultureInfo.TitlePlural_Search = this.__TitlePlural_Search_cultureBase;

                        itemCultureInfo.TitleSEO = this.__TitleSEO_cultureBase;

                        itemCultureInfo.Description = this.__Description_cultureBase;

                        itemCultureInfo.MetaKeywords = this.__MetaKeywords_cultureBase;

                        itemCultureInfo.MetaDescription = this.__MetaDescription_cultureBase;



			itemCultureInfo.MarkAsNotTemporary();
			this.Category_CultureInfos.Add((PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo) itemCultureInfo);
            
            

        }

		private void multilingualChecker_ItemRemoved(IMultilingualContentInfo item)
        {   
        	this.Category_CultureInfos.Remove((PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo)item);
            
        }

        
        
         #region IMultilingualItem Members

        IEnumerable<IMultilingualContentInfo> IMultilingualItem.GetMultilingualContentInfos()
        {
            return this.Category_CultureInfos;
            
        }

        protected override IEnumerable<IMultilingualContentInfo> __getMultilingualContentInfos()
        {
            return this.Category_CultureInfos;
        }
        
		#endregion    

     
        
        


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
