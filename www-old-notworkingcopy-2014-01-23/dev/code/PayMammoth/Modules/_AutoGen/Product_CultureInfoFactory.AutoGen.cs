using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.Product_CultureInfoModule;
using PayMammoth.Modules.Product_CultureInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class Product_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IProduct_CultureInfoFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProduct_CultureInfo>
    
    {
    
     	public new Product_CultureInfo ReloadItemFromDatabase(BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase item)
        {
         	return (Product_CultureInfo)base.ReloadItemFromDatabase(item);
        }
     	
        public Product_CultureInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(Product_CultureInfoImpl);
			PayMammoth.Cms.Product_CultureInfoModule.Product_CultureInfoCmsFactory._initialiseStaticInstance();
        }
        public static new Product_CultureInfoFactory Instance
        {
            get
            {
                return (Product_CultureInfoFactory)Product_CultureInfoBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<Product_CultureInfo> FindAll()
        {
            return base.FindAll().Cast<Product_CultureInfo>();
        }*/
    /*    public new IEnumerable<Product_CultureInfo> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<Product_CultureInfo>();

        }*/
        public new IEnumerable<Product_CultureInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Product_CultureInfo>();
            
        }
        public new IEnumerable<Product_CultureInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Product_CultureInfo>();
            
            
        }
     /*  public new IEnumerable<Product_CultureInfo> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<Product_CultureInfo>();


        }*/
        public new IEnumerable<Product_CultureInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Product_CultureInfo>();
            
        }
        public new IEnumerable<Product_CultureInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Product_CultureInfo>();
        }
        
        public new Product_CultureInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Product_CultureInfo)base.FindItem(query);
        }
        public new Product_CultureInfo FindItem(IQueryOver query)
        {
            return (Product_CultureInfo)base.FindItem(query);
        }
        
        IProduct_CultureInfo IProduct_CultureInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Product_CultureInfo CreateNewItem()
        {
            return (Product_CultureInfo)base.CreateNewItem();
        }

        public new Product_CultureInfo GetByPrimaryKey(long pKey)
        {
            return (Product_CultureInfo)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Product_CultureInfo, Product_CultureInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<Product_CultureInfo>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo> Members
     
        PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo IBaseDbFactory<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo IBaseDbFactory<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo> IBaseDbFactory<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo IBaseDbFactory<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo> IBaseDbFactory<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IProduct_CultureInfo> IProduct_CultureInfoFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProduct_CultureInfo> IProduct_CultureInfoFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProduct_CultureInfo> IProduct_CultureInfoFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IProduct_CultureInfo IProduct_CultureInfoFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProduct_CultureInfo IProduct_CultureInfoFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProduct_CultureInfo IProduct_CultureInfoFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IProduct_CultureInfo> IProduct_CultureInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IProduct_CultureInfo IProduct_CultureInfoFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
