using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.CmsUserModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class CmsUserMap_AutoGen : CmsUserMap_AutoGen_Z
    {
        public CmsUserMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Name, NameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Surname, SurnameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Username, UsernameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Password, PasswordMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LastLoggedIn, LastLoggedInMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item._LastLoggedIn_New, _LastLoggedIn_NewMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AccessType, AccessTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SessionGUID, SessionGUIDMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PasswordSalt, PasswordSaltMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PasswordEncryptionType, PasswordEncryptionTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PasswordIterations, PasswordIterationsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HiddenFromNonCasaSoft, HiddenFromNonCasaSoftMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CmsCustomLogoFilename, CmsCustomLogoFilenameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CmsCustomLogoLinkUrl, CmsCustomLogoLinkUrlMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Disabled, DisabledMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TicketingSystemSupportUser, TicketingSystemSupportUserMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Email, EmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ImageFilename, ImageFilenameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsDefaultTicketingSystemAssignee, IsDefaultTicketingSystemAssigneeMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_ManyToManyLeftInit
             addCollection(x => x.CmsUserRoles, CmsUserRolesMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.ManyToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class CmsUserMap_AutoGen_Z : BusinessLogic_v3.Modules.CmsUserModule.CmsUserBaseMap<CmsUserImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
