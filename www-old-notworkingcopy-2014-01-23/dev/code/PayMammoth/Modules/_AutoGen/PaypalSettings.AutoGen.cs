using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.PaypalSettingsModule;
using PayMammoth.Modules._AutoGen;

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaypalSettings_AutoGen : PaypalSettingsBase, PayMammoth.Modules._AutoGen.IPaypalSettingsAutoGen
	
      
      
    {
        


    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount WebsiteAccount
        {
            get
            {
                return (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount)base.WebsiteAccount;
            }
            set
            {
                base.WebsiteAccount = value;
            }
        }




// [userclass_collections_nhibernate]


        
          
		public  static new PaypalSettingsFactory Factory
        {
            get
            {
                return (PaypalSettingsFactory)PaypalSettingsBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<PaypalSettings, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
