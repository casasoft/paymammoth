using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ContentTextModule;
using PayMammoth.Cms.ContentTextModule;
using CS.WebComponentsGeneralV3.Cms.ContentTextModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ContentTextCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ContentTextModule.ContentTextBaseCmsFactory
    {
        public static new ContentTextBaseCmsFactory Instance 
        {
         	get { return (ContentTextBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ContentTextModule.ContentTextBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ContentTextBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	ContentText item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = ContentText.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        ContentTextCmsInfo cmsItem = new ContentTextCmsInfo(item);
            return cmsItem;
        }    
        
        public override ContentTextBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase item)
        {
            return new ContentTextCmsInfo((ContentText)item);
            
        }

    }
}
