using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.MemberModule;
using CS.WebComponentsGeneralV3.Cms.MemberModule;
using PayMammoth.Frontend.MemberModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class MemberCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.MemberModule.MemberBaseCmsInfo
    {

		public MemberCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberModule.MemberBase item)
            : base(item)
        {

        }
        

        private MemberFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = MemberFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new MemberFrontend FrontendItem
        {
            get { return (MemberFrontend) base.FrontendItem; }
        }
        
        
		public new Member DbItem
        {
            get
            {
                return (Member)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.DateRegistered = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.DateRegistered),
        						isEditable: true, isVisible: true);

        this.Username = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Username),
        						isEditable: true, isVisible: true);

        this.Password = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Password),
        						isEditable: true, isVisible: true);

        this.FirstName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.FirstName),
        						isEditable: true, isVisible: true);

        this.LastName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.LastName),
        						isEditable: true, isVisible: true);

        this.Gender = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Gender),
        						isEditable: true, isVisible: true);

        this.DateOfBirth = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.DateOfBirth),
        						isEditable: true, isVisible: true);

        this.Address1 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Address1),
        						isEditable: true, isVisible: true);

        this.Address2 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Address2),
        						isEditable: true, isVisible: true);

        this.Address3 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Address3),
        						isEditable: true, isVisible: true);

        this.Locality = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Locality),
        						isEditable: true, isVisible: true);

        this.State = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.State),
        						isEditable: true, isVisible: true);

        this.PostCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.PostCode),
        						isEditable: true, isVisible: true);

        this.Country = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Country),
        						isEditable: true, isVisible: true);

        this.IDCard = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.IDCard),
        						isEditable: true, isVisible: true);

        this.LastLoggedIn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.LastLoggedIn),
        						isEditable: true, isVisible: true);

        this._LastLoggedIn_New = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item._LastLoggedIn_New),
        						isEditable: true, isVisible: true);

        this.Accepted = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Accepted),
        						isEditable: true, isVisible: true);

        this.Email = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Email),
        						isEditable: true, isVisible: true);

        this.Telephone = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Telephone),
        						isEditable: true, isVisible: true);

        this.Mobile = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Mobile),
        						isEditable: true, isVisible: true);

        this.Fax = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Fax),
        						isEditable: true, isVisible: true);

        this.Remarks = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Remarks),
        						isEditable: true, isVisible: true);

        this.Website = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Website),
        						isEditable: true, isVisible: true);

        this.ActivatedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.ActivatedOn),
        						isEditable: true, isVisible: true);

        this.ActivatedIP = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.ActivatedIP),
        						isEditable: true, isVisible: true);

        this.ActivationCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.ActivationCode),
        						isEditable: true, isVisible: true);

        this.SessionGUID = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.SessionGUID),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.PreferredCultureInfo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.PreferredCultureInfo),
                null));

        
//InitFieldUser_LinkedProperty
			this.ReferredByMember = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.ReferredByMember),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Members)));

        
        this.AccountTerminated = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.AccountTerminated),
        						isEditable: true, isVisible: true);

        this.AccountTerminatedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.AccountTerminatedOn),
        						isEditable: true, isVisible: true);

        this.AccountTerminatedIP = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.AccountTerminatedIP),
        						isEditable: true, isVisible: true);

        this.ForgottenPassCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.ForgottenPassCode),
        						isEditable: true, isVisible: true);

        this.SubscribedToNewsletter = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.SubscribedToNewsletter),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.CurrentShoppingCart = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.CurrentShoppingCart),
                null));

        
        this.HowDidYouFindUs = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.HowDidYouFindUs),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.LinkedAffiliate = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.LinkedAffiliate),
                null));

        
        this.CompanyName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.CompanyName),
        						isEditable: true, isVisible: true);

        this.PasswordEncryptionType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.PasswordEncryptionType),
        						isEditable: true, isVisible: true);

        this.PasswordSalt = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.PasswordSalt),
        						isEditable: true, isVisible: true);

        this.PasswordIterations = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.PasswordIterations),
        						isEditable: true, isVisible: true);

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.CurrentSubscription = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.CurrentSubscription),
                null));

        
        this.CurrentSubscriptionStartDate = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.CurrentSubscriptionStartDate),
        						isEditable: true, isVisible: true);

        this.CurrentSubscriptionEndDate = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.CurrentSubscriptionEndDate),
        						isEditable: true, isVisible: true);

        this.SubscriptionNotification1Sent = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.SubscriptionNotification1Sent),
        						isEditable: true, isVisible: true);

        this.SubscriptionNotification2Sent = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.SubscriptionNotification2Sent),
        						isEditable: true, isVisible: true);

        this.Paid = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Paid),
        						isEditable: true, isVisible: true);

        this.Activated = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Activated),
        						isEditable: true, isVisible: true);

        this.MiddleName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.MiddleName),
        						isEditable: true, isVisible: true);

        this.SubscriptionExpiryNotificationSent = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.SubscriptionExpiryNotificationSent),
        						isEditable: true, isVisible: true);

        this.IsSubscriptionExpired = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.IsSubscriptionExpired),
        						isEditable: true, isVisible: true);

        this.ReferralSuccessful = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.ReferralSuccessful),
        						isEditable: true, isVisible: true);

        this.ReferredFromUrl = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.ReferredFromUrl),
        						isEditable: true, isVisible: true);

        this.AccountBalance = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.AccountBalance),
        						isEditable: true, isVisible: true);

        this.Blocked = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.Blocked),
        						isEditable: true, isVisible: true);

        this.SelfExcludedUntil = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.SelfExcludedUntil),
        						isEditable: true, isVisible: true);

        this.SelfExclusionSetOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.SelfExclusionSetOn),
        						isEditable: true, isVisible: true);

        this.SelfExclusionSetByIp = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.SelfExclusionSetByIp),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
