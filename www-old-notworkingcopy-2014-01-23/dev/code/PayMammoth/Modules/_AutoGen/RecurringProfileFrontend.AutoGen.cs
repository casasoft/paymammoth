using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.RecurringProfileModule.RecurringProfileFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.RecurringProfileModule.RecurringProfileFrontend ToFrontend(this PayMammoth.Modules.RecurringProfileModule.IRecurringProfile item)
        {
        	return PayMammoth.Frontend.RecurringProfileModule.RecurringProfileFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfileFrontend_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfileModule.RecurringProfileBaseFrontend

    {
		
        
        protected RecurringProfileFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.RecurringProfileModule.IRecurringProfile Data
        {
            get { return (PayMammoth.Modules.RecurringProfileModule.IRecurringProfile)base.Data; }
            set { base.Data = value; }

        }

        public new static RecurringProfileFrontend CreateNewItem()
        {
            return (RecurringProfileFrontend)PayMammoth.Modules._AutoGen.RecurringProfileModule.RecurringProfileBaseFrontend.CreateNewItem();
        }
        
        public new static RecurringProfileFrontend Get(PayMammoth.Modules._AutoGen.IRecurringProfileBase data)
        {
            return (RecurringProfileFrontend)PayMammoth.Modules._AutoGen.RecurringProfileModule.RecurringProfileBaseFrontend.Get(data);
        }
        public new static List<RecurringProfileFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IRecurringProfileBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.RecurringProfileModule.RecurringProfileBaseFrontend.GetList(dataList).Cast<RecurringProfileFrontend>().ToList();
        }


    }
}
