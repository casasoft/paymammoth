using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.PaymentRequestItemDetailModule;
using PayMammoth.Modules.PaymentRequestItemDetailModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.PaymentRequestItemDetailModule.PaymentRequestItemDetailFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.PaymentRequestItemDetailModule.PaymentRequestItemDetailFrontend ToFrontend(this PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail item)
        {
        	return PayMammoth.Frontend.PaymentRequestItemDetailModule.PaymentRequestItemDetailFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestItemDetailFrontend_AutoGen : PayMammoth.Modules._AutoGen.PaymentRequestItemDetailModule.PaymentRequestItemDetailBaseFrontend

    {
		
        
        protected PaymentRequestItemDetailFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail Data
        {
            get { return (PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail)base.Data; }
            set { base.Data = value; }

        }

        public new static PaymentRequestItemDetailFrontend CreateNewItem()
        {
            return (PaymentRequestItemDetailFrontend)PayMammoth.Modules._AutoGen.PaymentRequestItemDetailModule.PaymentRequestItemDetailBaseFrontend.CreateNewItem();
        }
        
        public new static PaymentRequestItemDetailFrontend Get(PayMammoth.Modules._AutoGen.IPaymentRequestItemDetailBase data)
        {
            return (PaymentRequestItemDetailFrontend)PayMammoth.Modules._AutoGen.PaymentRequestItemDetailModule.PaymentRequestItemDetailBaseFrontend.Get(data);
        }
        public new static List<PaymentRequestItemDetailFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IPaymentRequestItemDetailBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.PaymentRequestItemDetailModule.PaymentRequestItemDetailBaseFrontend.GetList(dataList).Cast<PaymentRequestItemDetailFrontend>().ToList();
        }


    }
}
