using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ContactFormModule;
using PayMammoth.Modules.ContactFormModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IContactFormFactoryAutoGen : BusinessLogic_v3.Modules.ContactFormModule.IContactFormBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IContactForm>
    
    {
	    new PayMammoth.Modules.ContactFormModule.IContactForm CreateNewItem();
        new PayMammoth.Modules.ContactFormModule.IContactForm GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.ContactFormModule.IContactForm> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ContactFormModule.IContactForm> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ContactFormModule.IContactForm> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ContactFormModule.IContactForm FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ContactFormModule.IContactForm FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ContactFormModule.IContactForm FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ContactFormModule.IContactForm> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
