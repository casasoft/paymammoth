using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.RoutingInfoModule;
using PayMammoth.Modules.RoutingInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RoutingInfoFactoryAutoGen : BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IRoutingInfoFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IRoutingInfo>
    
    {
    
     	public new RoutingInfo ReloadItemFromDatabase(BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBase item)
        {
         	return (RoutingInfo)base.ReloadItemFromDatabase(item);
        }
     	static RoutingInfoFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public RoutingInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(RoutingInfoImpl);
			PayMammoth.Cms.RoutingInfoModule.RoutingInfoCmsFactory._initialiseStaticInstance();
        }
        public static new RoutingInfoFactory Instance
        {
            get
            {
                return (RoutingInfoFactory)RoutingInfoBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<RoutingInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<RoutingInfo>();
            
        }
        public new IEnumerable<RoutingInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<RoutingInfo>();
            
            
        }
    
        public new IEnumerable<RoutingInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<RoutingInfo>();
            
        }
        public new IEnumerable<RoutingInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<RoutingInfo>();
        }
        
        public new RoutingInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (RoutingInfo)base.FindItem(query);
        }
        public new RoutingInfo FindItem(IQueryOver query)
        {
            return (RoutingInfo)base.FindItem(query);
        }
        
        IRoutingInfo IRoutingInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new RoutingInfo CreateNewItem()
        {
            return (RoutingInfo)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<RoutingInfo, RoutingInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<RoutingInfo>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.RoutingInfoModule.RoutingInfo GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.RoutingInfoModule.RoutingInfo) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.RoutingInfoModule.RoutingInfo> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RoutingInfoModule.RoutingInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.RoutingInfoModule.RoutingInfo> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RoutingInfoModule.RoutingInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.RoutingInfoModule.RoutingInfo> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RoutingInfoModule.RoutingInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.RoutingInfoModule.RoutingInfo FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RoutingInfoModule.RoutingInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.RoutingInfoModule.RoutingInfo FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RoutingInfoModule.RoutingInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.RoutingInfoModule.RoutingInfo FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RoutingInfoModule.RoutingInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.RoutingInfoModule.RoutingInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RoutingInfoModule.RoutingInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo> Members
     
        PayMammoth.Modules.RoutingInfoModule.IRoutingInfo IBaseDbFactory<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.RoutingInfoModule.IRoutingInfo IBaseDbFactory<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo> IBaseDbFactory<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.RoutingInfoModule.IRoutingInfo IBaseDbFactory<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.RoutingInfoModule.IRoutingInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.RoutingInfoModule.IRoutingInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo> IBaseDbFactory<PayMammoth.Modules.RoutingInfoModule.IRoutingInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IRoutingInfo> IRoutingInfoFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IRoutingInfo> IRoutingInfoFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IRoutingInfo> IRoutingInfoFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IRoutingInfo IRoutingInfoFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IRoutingInfo IRoutingInfoFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IRoutingInfo IRoutingInfoFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IRoutingInfo> IRoutingInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IRoutingInfo IRoutingInfoFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
