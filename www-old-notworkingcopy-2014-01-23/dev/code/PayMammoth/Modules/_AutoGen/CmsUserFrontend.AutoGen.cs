using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.CmsUserModule;
using PayMammoth.Modules.CmsUserModule;
using BusinessLogic_v3.Modules.CmsUserModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.CmsUserModule.CmsUserFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.CmsUserModule.ICmsUser> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.CmsUserModule.CmsUserFrontend ToFrontend(this PayMammoth.Modules.CmsUserModule.ICmsUser item)
        {
        	return PayMammoth.Frontend.CmsUserModule.CmsUserFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class CmsUserFrontend_AutoGen : BusinessLogic_v3.Frontend.CmsUserModule.CmsUserBaseFrontend

    {
		
        
        protected CmsUserFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.CmsUserModule.ICmsUser Data
        {
            get { return (PayMammoth.Modules.CmsUserModule.ICmsUser)base.Data; }
            set { base.Data = value; }

        }

        public new static CmsUserFrontend CreateNewItem()
        {
            return (CmsUserFrontend)BusinessLogic_v3.Frontend.CmsUserModule.CmsUserBaseFrontend.CreateNewItem();
        }
        
        public new static CmsUserFrontend Get(BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase data)
        {
            return (CmsUserFrontend)BusinessLogic_v3.Frontend.CmsUserModule.CmsUserBaseFrontend.Get(data);
        }
        public new static List<CmsUserFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase> dataList)
        {
            return BusinessLogic_v3.Frontend.CmsUserModule.CmsUserBaseFrontend.GetList(dataList).Cast<CmsUserFrontend>().ToList();
        }


    }
}
