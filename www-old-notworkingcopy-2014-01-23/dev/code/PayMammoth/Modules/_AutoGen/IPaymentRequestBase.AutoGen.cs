using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IPaymentRequestBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Identifier { get; set; }
		//Iproperty_normal
        DateTime DateTime { get; set; }
		//Iproperty_normal
        string RequestIP { get; set; }
		//Iproperty_normal
        string ClientFirstName { get; set; }
		//Iproperty_normal
        string ClientMiddleName { get; set; }
		//Iproperty_normal
        string ClientLastName { get; set; }
		//Iproperty_normal
        string ClientAddress1 { get; set; }
		//Iproperty_normal
        string ClientAddress2 { get; set; }
		//Iproperty_normal
        string ClientAddress3 { get; set; }
		//Iproperty_normal
        string ClientLocality { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? ClientCountry { get; set; }
		//Iproperty_normal
        string ClientPostCode { get; set; }
		//Iproperty_normal
        string Description { get; set; }
		//Iproperty_normal
        string ClientEmail { get; set; }
		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IWebsiteAccountBase WebsiteAccount {get; set; }

		//Iproperty_normal
        double TaxAmount { get; set; }
		//Iproperty_normal
        double HandlingAmount { get; set; }
		//Iproperty_normal
        double ShippingAmount { get; set; }
		//Iproperty_normal
        string Reference { get; set; }
		//Iproperty_normal
        string Notes { get; set; }
		//Iproperty_normal
        double PriceExcTax { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 CurrencyCode { get; set; }
		//Iproperty_normal
        string OrderLinkOnClientWebsite { get; set; }
		//Iproperty_normal
        string Title { get; set; }
		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IPaymentRequestTransactionBase CurrentTransaction {get; set; }

		//Iproperty_normal
        DateTime? PaidOn { get; set; }
		//Iproperty_normal
        PayMammoth.Connector.Enums.PaymentMethodSpecific? PaidByPaymentMethod { get; set; }
		//Iproperty_normal
        string ReturnUrlSuccess { get; set; }
		//Iproperty_normal
        string ReturnUrlFailure { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? LanguageCode { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? LanguageCountryCode { get; set; }
		//Iproperty_normal
        string LanguageSuffix { get; set; }
		//Iproperty_normal
        string ClientTelephone { get; set; }
		//Iproperty_normal
        string ClientMobile { get; set; }
		//Iproperty_normal
        string ClientTitle { get; set; }
		//Iproperty_normal
        string ClientIp { get; set; }
		//Iproperty_normal
        string ClientReference { get; set; }
		//Iproperty_normal
        bool RequiresManualIntervention { get; set; }
		//Iproperty_normal
        bool PaymentNotificationEnabled { get; set; }
		//Iproperty_normal
        bool RecurringProfileRequired { get; set; }
		//Iproperty_normal
        PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD RecurringProfile_IntervalType { get; set; }
		//Iproperty_normal
        int? RecurringProfile_TotalBillingCycles { get; set; }
		//Iproperty_normal
        int RecurringProfile_IntervalFrequency { get; set; }
		//Iproperty_normal
        int RecurringProfile_MaxFailedAttempts { get; set; }
		//Iproperty_normal
        string StatusLog { get; set; }
		//Iproperty_normal
        string NotificationUrl { get; set; }
		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IRecurringProfileBase RecurringProfileLink {get; set; }

   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
