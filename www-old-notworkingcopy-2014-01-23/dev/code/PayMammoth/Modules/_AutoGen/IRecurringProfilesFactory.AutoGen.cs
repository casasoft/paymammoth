using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.RecurringProfilesModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IRecurringProfilesFactoryAutoGen : PayMammoth.Modules._AutoGen.IRecurringProfilesBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IRecurringProfiles>
    
    {
	    new PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles CreateNewItem();
        new PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.RecurringProfilesModule.IRecurringProfiles> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
