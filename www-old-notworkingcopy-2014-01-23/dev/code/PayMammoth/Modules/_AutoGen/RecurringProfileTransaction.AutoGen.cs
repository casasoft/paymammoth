using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.RecurringProfileTransactionModule;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class RecurringProfileTransaction_AutoGen : RecurringProfileTransactionBase, PayMammoth.Modules._AutoGen.IRecurringProfileTransactionAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment RecurringProfilePayment
        {
            get
            {
                return (PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment)base.RecurringProfilePayment;
            }
            set
            {
                base.RecurringProfilePayment = value;
            }
        }

		PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment IRecurringProfileTransactionAutoGen.RecurringProfilePayment
        {
            get
            {
            	return this.RecurringProfilePayment;
                
            }
            set
            {
                this.RecurringProfilePayment = (PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.NotificationMessageModule.NotificationMessage LinkedNotificationMessage
        {
            get
            {
                return (PayMammoth.Modules.NotificationMessageModule.NotificationMessage)base.LinkedNotificationMessage;
            }
            set
            {
                base.LinkedNotificationMessage = value;
            }
        }

		PayMammoth.Modules.NotificationMessageModule.INotificationMessage IRecurringProfileTransactionAutoGen.LinkedNotificationMessage
        {
            get
            {
            	return this.LinkedNotificationMessage;
                
            }
            set
            {
                this.LinkedNotificationMessage = (PayMammoth.Modules.NotificationMessageModule.NotificationMessage)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new RecurringProfileTransactionFactory Factory
        {
            get
            {
                return (RecurringProfileTransactionFactory)RecurringProfileTransactionBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<RecurringProfileTransaction, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
