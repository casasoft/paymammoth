using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using PayMammoth.Modules.ProductCategoryModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCategoryFactoryAutoGen : BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBaseFactory, 
    				PayMammoth.Modules._AutoGen.IProductCategoryFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductCategory>
    
    {
    
     	public new ProductCategory ReloadItemFromDatabase(BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase item)
        {
         	return (ProductCategory)base.ReloadItemFromDatabase(item);
        }
     	
        public ProductCategoryFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ProductCategoryImpl);
			PayMammoth.Cms.ProductCategoryModule.ProductCategoryCmsFactory._initialiseStaticInstance();
        }
        public static new ProductCategoryFactory Instance
        {
            get
            {
                return (ProductCategoryFactory)ProductCategoryBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<ProductCategory> FindAll()
        {
            return base.FindAll().Cast<ProductCategory>();
        }*/
    /*    public new IEnumerable<ProductCategory> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<ProductCategory>();

        }*/
        public new IEnumerable<ProductCategory> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ProductCategory>();
            
        }
        public new IEnumerable<ProductCategory> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ProductCategory>();
            
            
        }
     /*  public new IEnumerable<ProductCategory> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<ProductCategory>();


        }*/
        public new IEnumerable<ProductCategory> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ProductCategory>();
            
        }
        public new IEnumerable<ProductCategory> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ProductCategory>();
        }
        
        public new ProductCategory FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ProductCategory)base.FindItem(query);
        }
        public new ProductCategory FindItem(IQueryOver query)
        {
            return (ProductCategory)base.FindItem(query);
        }
        
        IProductCategory IProductCategoryFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ProductCategory CreateNewItem()
        {
            return (ProductCategory)base.CreateNewItem();
        }

        public new ProductCategory GetByPrimaryKey(long pKey)
        {
            return (ProductCategory)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<ProductCategory, ProductCategory> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<ProductCategory>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ProductCategoryModule.ProductCategory GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.ProductCategoryModule.ProductCategory) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.ProductCategoryModule.ProductCategory> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategoryModule.ProductCategory>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductCategoryModule.ProductCategory> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategoryModule.ProductCategory>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductCategoryModule.ProductCategory> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategoryModule.ProductCategory>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ProductCategoryModule.ProductCategory FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductCategoryModule.ProductCategory) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ProductCategoryModule.ProductCategory FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductCategoryModule.ProductCategory) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ProductCategoryModule.ProductCategory FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductCategoryModule.ProductCategory) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductCategoryModule.ProductCategory> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategoryModule.ProductCategory>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ProductCategoryModule.IProductCategory> Members
     
        PayMammoth.Modules.ProductCategoryModule.IProductCategory IBaseDbFactory<PayMammoth.Modules.ProductCategoryModule.IProductCategory>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ProductCategoryModule.IProductCategory IBaseDbFactory<PayMammoth.Modules.ProductCategoryModule.IProductCategory>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductCategoryModule.IProductCategory> IBaseDbFactory<PayMammoth.Modules.ProductCategoryModule.IProductCategory>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ProductCategoryModule.IProductCategory> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategoryModule.IProductCategory>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ProductCategoryModule.IProductCategory> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategoryModule.IProductCategory>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ProductCategoryModule.IProductCategory IBaseDbFactory<PayMammoth.Modules.ProductCategoryModule.IProductCategory>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductCategoryModule.IProductCategory CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategoryModule.IProductCategory>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductCategoryModule.IProductCategory CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategoryModule.IProductCategory>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductCategoryModule.IProductCategory> IBaseDbFactory<PayMammoth.Modules.ProductCategoryModule.IProductCategory>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IProductCategory> IProductCategoryFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductCategory> IProductCategoryFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductCategory> IProductCategoryFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IProductCategory IProductCategoryFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductCategory IProductCategoryFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductCategory IProductCategoryFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IProductCategory> IProductCategoryFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IProductCategory IProductCategoryFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
