using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ArticleRatingModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ArticleRatingMap_AutoGen : ArticleRatingMap_AutoGen_Z
    {
        public ArticleRatingMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.IPAddress, IPAddressMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Date, DateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Rating, RatingMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Article, ArticleMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ArticleRatingMap_AutoGen_Z : BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBaseMap<ArticleRatingImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
