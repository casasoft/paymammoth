using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.AttachmentModule;
using PayMammoth.Modules.AttachmentModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IAttachmentFactoryAutoGen : BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IAttachment>
    
    {
	    new PayMammoth.Modules.AttachmentModule.IAttachment CreateNewItem();
        new PayMammoth.Modules.AttachmentModule.IAttachment GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.AttachmentModule.IAttachment> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.AttachmentModule.IAttachment> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.AttachmentModule.IAttachment> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.AttachmentModule.IAttachment FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.AttachmentModule.IAttachment FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.AttachmentModule.IAttachment FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.AttachmentModule.IAttachment> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
