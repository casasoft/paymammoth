using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CmsUserModule;
using PayMammoth.Modules.CmsUserModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ICmsUserFactoryAutoGen : BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ICmsUser>
    
    {
	    new PayMammoth.Modules.CmsUserModule.ICmsUser CreateNewItem();
        new PayMammoth.Modules.CmsUserModule.ICmsUser GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.CmsUserModule.ICmsUser> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CmsUserModule.ICmsUser> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CmsUserModule.ICmsUser> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.CmsUserModule.ICmsUser FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CmsUserModule.ICmsUser FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CmsUserModule.ICmsUser FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CmsUserModule.ICmsUser> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
