using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.BrandModule;
using CS.WebComponentsGeneralV3.Cms.BrandModule;
using PayMammoth.Frontend.BrandModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class BrandCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.BrandModule.BrandBaseCmsInfo
    {

		public BrandCmsInfo_AutoGen(BusinessLogic_v3.Modules.BrandModule.BrandBase item)
            : base(item)
        {

        }
        

        private BrandFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = BrandFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new BrandFrontend FrontendItem
        {
            get { return (BrandFrontend) base.FrontendItem; }
        }
        
        
		public new Brand DbItem
        {
            get
            {
                return (Brand)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BrandModule.Brand>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BrandModule.Brand>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

        this.BrandLogoFilename = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BrandModule.Brand>.GetPropertyBySelector( item => item.BrandLogoFilename),
        						isEditable: false, isVisible: false);

        this.IsFeatured = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BrandModule.Brand>.GetPropertyBySelector( item => item.IsFeatured),
        						isEditable: true, isVisible: true);

        this.Url = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BrandModule.Brand>.GetPropertyBySelector( item => item.Url),
        						isEditable: true, isVisible: true);

        this.ImportReference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BrandModule.Brand>.GetPropertyBySelector( item => item.ImportReference),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
