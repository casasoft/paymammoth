using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.EmailLogModule;
using PayMammoth.Cms.EmailLogModule;
using CS.WebComponentsGeneralV3.Cms.EmailLogModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class EmailLogCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.EmailLogModule.EmailLogBaseCmsFactory
    {
        public static new EmailLogBaseCmsFactory Instance 
        {
         	get { return (EmailLogBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.EmailLogModule.EmailLogBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override EmailLogBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	EmailLog item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = EmailLog.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        EmailLogCmsInfo cmsItem = new EmailLogCmsInfo(item);
            return cmsItem;
        }    
        
        public override EmailLogBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase item)
        {
            return new EmailLogCmsInfo((EmailLog)item);
            
        }

    }
}
