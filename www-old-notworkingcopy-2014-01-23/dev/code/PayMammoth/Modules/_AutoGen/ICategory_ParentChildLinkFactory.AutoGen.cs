using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;
using PayMammoth.Modules.Category_ParentChildLinkModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ICategory_ParentChildLinkFactoryAutoGen : BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ICategory_ParentChildLink>
    
    {
	    new PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink CreateNewItem();
        new PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
