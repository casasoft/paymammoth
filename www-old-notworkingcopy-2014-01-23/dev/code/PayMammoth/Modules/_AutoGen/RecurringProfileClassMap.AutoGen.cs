using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.RecurringProfileModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class RecurringProfileMap_AutoGen : RecurringProfileMap_AutoGen_Z
    {
        public RecurringProfileMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.ProfileIdentifierOnGateway, ProfileIdentifierOnGatewayMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGateway, PaymentGatewayMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CreatedOn, CreatedOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.BillingCyclesCompleted2, BillingCyclesCompleted2MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TotalBillingCyclesRequired, TotalBillingCyclesRequiredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Status, StatusMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MaximumFailedAttempts, MaximumFailedAttemptsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ActivatedOn, ActivatedOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CancelledOn, CancelledOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ExpiredOn, ExpiredOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.RecurringIntervalFrequency, RecurringIntervalFrequencyMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.RecurringIntervalType, RecurringIntervalTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.StatusLog, StatusLogMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.WebsiteAccount, WebsiteAccountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NotificationUrl, NotificationUrlMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CreationStatus, CreationStatusMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.CreatedByPaymentRequests, CreatedByPaymentRequestsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.Payments, PaymentsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class RecurringProfileMap_AutoGen_Z : PayMammoth.Modules._AutoGen.RecurringProfileBaseMap<RecurringProfileImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void WebsiteAccountMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "WebsiteAccountId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

//BaseClassMap_Collection_OneToMany

        protected virtual void CreatedByPaymentRequestsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "RecurringProfileLinkID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
//BaseClassMap_Collection_OneToMany

        protected virtual void PaymentsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "RecurringProfileID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
            
        
        
    }
}
