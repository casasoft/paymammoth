using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using NHibernate;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.EmailTextModule;

namespace PayMammoth.Modules._AutoGen
{

//UserClassImpl-File
	[Serializable]
    public class EmailTextImpl : PayMammoth.Modules.EmailTextModule.EmailText
    {
/*
 		public new CmsUserImpl LastEditedBy
        {
            get { return (CmsUserImpl) base.LastEditedBy; }
            set { base.LastEditedBy = value; }
        }
        public new CmsUserImpl DeletedBy
        {
            get { return (CmsUserImpl)base.DeletedBy; }
            set { base.DeletedBy = value; }
        }
*/
		#region UserClass-AutoGenerated
		#endregion

      
      
        private EmailTextImpl()
        {
            
        }    
    	

// [userclassimpl_collections]

        //UserClassImpl_CollectionOneToManyLeftSide
        
        protected override IEnumerable<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase> __collection__ChildEmails
        {
            get
            {
                return this._ChildEmails;
            }
        }

        private ICollection<EmailTextImpl> _ChildEmails = new Iesi.Collections.Generic.HashedSet<EmailTextImpl>();
        public new virtual ICollection<EmailTextImpl> ChildEmails
        {
            get { return _ChildEmails; }

        }
        //UserClassImpl_CollectionOneToManyLeftSide
        
        protected override IEnumerable<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase> __collection__EmailText_CultureInfos
        {
            get
            {
                return this._EmailText_CultureInfos;
            }
        }

        private ICollection<EmailText_CultureInfoImpl> _EmailText_CultureInfos = new Iesi.Collections.Generic.HashedSet<EmailText_CultureInfoImpl>();
        public new virtual ICollection<EmailText_CultureInfoImpl> EmailText_CultureInfos
        {
            get { return _EmailText_CultureInfos; }

        }
    	


    	
        
    }
}




