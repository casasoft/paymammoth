using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.MemberAccountBalanceHistoryModule;
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberAccountBalanceHistory_AutoGen : MemberAccountBalanceHistoryBase, PayMammoth.Modules._AutoGen.IMemberAccountBalanceHistoryAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.MemberModule.Member Member
        {
            get
            {
                return (PayMammoth.Modules.MemberModule.Member)base.Member;
            }
            set
            {
                base.Member = value;
            }
        }

		PayMammoth.Modules.MemberModule.IMember IMemberAccountBalanceHistoryAutoGen.Member
        {
            get
            {
            	return this.Member;
                
            }
            set
            {
                this.Member = (PayMammoth.Modules.MemberModule.Member)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.OrderModule.Order OrderLink
        {
            get
            {
                return (PayMammoth.Modules.OrderModule.Order)base.OrderLink;
            }
            set
            {
                base.OrderLink = value;
            }
        }

		PayMammoth.Modules.OrderModule.IOrder IMemberAccountBalanceHistoryAutoGen.OrderLink
        {
            get
            {
            	return this.OrderLink;
                
            }
            set
            {
                this.OrderLink = (PayMammoth.Modules.OrderModule.Order)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new MemberAccountBalanceHistoryFactory Factory
        {
            get
            {
                return (MemberAccountBalanceHistoryFactory)MemberAccountBalanceHistoryBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<MemberAccountBalanceHistory, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
