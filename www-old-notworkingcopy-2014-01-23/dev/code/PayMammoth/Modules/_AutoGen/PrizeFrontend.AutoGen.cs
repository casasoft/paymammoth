using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.PrizeModule;
using PayMammoth.Modules.PrizeModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PrizeFrontend_AutoGen : PayMammoth.Modules._AutoGen.PrizeModule.PrizeBaseFrontend

    {
		
        
        protected PrizeFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.PrizeModule.IPrize Data
        {
            get { return (PayMammoth.Modules.PrizeModule.IPrize)base.Data; }
            set { base.Data = value; }

        }

        public new static PrizeFrontend Get(PayMammoth.Modules._AutoGen.IPrizeBase data)
        {
            return (PrizeFrontend)PayMammoth.Modules._AutoGen.PrizeModule.PrizeBaseFrontend.Get(data);
        }
        public new static List<PrizeFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IPrizeBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.PrizeModule.PrizeBaseFrontend.GetList(dataList).Cast<PrizeFrontend>().ToList();
        }


    }
}
