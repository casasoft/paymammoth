using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CurrencyModule;
using PayMammoth.Modules.CurrencyModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ICurrencyFactoryAutoGen : BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ICurrency>
    
    {
	    new PayMammoth.Modules.CurrencyModule.ICurrency CreateNewItem();
        new PayMammoth.Modules.CurrencyModule.ICurrency GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.CurrencyModule.ICurrency> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CurrencyModule.ICurrency> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CurrencyModule.ICurrency> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.CurrencyModule.ICurrency FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CurrencyModule.ICurrency FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CurrencyModule.ICurrency FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CurrencyModule.ICurrency> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
