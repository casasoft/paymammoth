using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ArticleModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ArticleMap_AutoGen : ArticleMap_AutoGen_Z
    {
        public ArticleMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AllowUpdate_AccessRequired, AllowUpdate_AccessRequiredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AllowDelete_AccessRequired, AllowDelete_AccessRequiredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AllowAddChildren_AccessRequired, AllowAddChildren_AccessRequiredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AllowAddSubChildren_AccessRequired, AllowAddSubChildren_AccessRequiredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomLink, CustomLinkMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HtmlText, HtmlTextMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HtmlText_Search, HtmlText_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaKeywords, MetaKeywordsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaKeywords_Search, MetaKeywords_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaDescription, MetaDescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaDescription_Search, MetaDescription_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Color, ColorMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Name, NameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PageTitle, PageTitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PageTitle_Search, PageTitle_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ExcludeFromRewriteUrl, ExcludeFromRewriteUrlMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ImageFilename, ImageFilenameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ShowAdverts, ShowAdvertsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.VisibleInCMS_AccessRequired, VisibleInCMS_AccessRequiredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DoNotShowLastEditedOn, DoNotShowLastEditedOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ViewCount, ViewCountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsFeatured, IsFeaturedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaTitle, MetaTitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NoFollow, NoFollowMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NoIndex, NoIndexMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.UsedInProject, UsedInProjectMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ArticleType, ArticleTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NotVisibleInFrontend, NotVisibleInFrontendMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.VisibleInFrontendOnlyWhenLoggedIn, VisibleInFrontendOnlyWhenLoggedInMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NotVisibleInFrontendWhenLoggedIn, NotVisibleInFrontendWhenLoggedInMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomContentTags, CustomContentTagsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DoNotRedirectOnLogin, DoNotRedirectOnLoginMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DoNotShowAddThis, DoNotShowAddThisMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DoNotShowFooter, DoNotShowFooterMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DialogWidth, DialogWidthMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DialogHeight, DialogHeightMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DialogPage, DialogPageMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.RatingCount, RatingCountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AvgRating, AvgRatingMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsCommentable, IsCommentableMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Summary, SummaryMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.OmitChildrenInNavigation, OmitChildrenInNavigationMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ConsiderAsRootNode, ConsiderAsRootNodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DoNotShowNavigationBreadcrumbs, DoNotShowNavigationBreadcrumbsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CreatedOnDate, CreatedOnDateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PhysicalFilePath, PhysicalFilePathMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ProcessContentUsingNVelocity, ProcessContentUsingNVelocityMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DialogCssClass, DialogCssClassMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DialogModal, DialogModalMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DialogCloseable, DialogCloseableMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DoNotRedirectOnLogout, DoNotRedirectOnLogoutMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.LinkedRoute, LinkedRouteMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SubTitle, SubTitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HrefTarget, HrefTargetMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsInheritedCommentable_Computed, IsInheritedCommentable_ComputedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AuthorName, AuthorNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DoNotShowInSearchResults, DoNotShowInSearchResultsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DoNotShowInSearchResults_Computed, DoNotShowInSearchResults_ComputedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item._Computed_CommentAndRepliesCount, _Computed_CommentAndRepliesCountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item._ComputedURL, _ComputedURLMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item._Computed_ParentArticleNames, _Computed_ParentArticleNamesMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MaxLevelDepthToShowInCms, MaxLevelDepthToShowInCmsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.UpdateVersion, UpdateVersionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DoNotShowAuthor, DoNotShowAuthorMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FeaturedDateFrom, FeaturedDateFromMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FeaturedDateTo, FeaturedDateToMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Tags, TagsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item._Computed_FullPath, _Computed_FullPathMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomProjOnlyField, CustomProjOnlyFieldMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomProjOnlyLinkedObject, CustomProjOnlyLinkedObjectMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.Article_CultureInfos, Article_CultureInfosMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ChildArticleLinks, ChildArticleLinksMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ParentArticleLinks, ParentArticleLinksMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ArticleMap_AutoGen_Z : BusinessLogic_v3.Modules.ArticleModule.ArticleBaseMap<ArticleImpl>
    {


// [userclassmap_properties]

//ClassMap_Property - User
             
        protected virtual void CustomProjOnlyFieldMappingInfo(IPropertyMapInfo mapInfo)
        {
            //do nothing
        }
        

//ClassMap_Property - User
             
        protected virtual void CustomProjOnlyLinkedObjectMappingInfo(IPropertyMapInfo mapInfo)
        {
            //do nothing
        }
        




// [userclassmap_collections]

            
        
        
    }
}
