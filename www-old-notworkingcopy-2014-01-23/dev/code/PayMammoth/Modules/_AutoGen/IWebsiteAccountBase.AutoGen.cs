using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IWebsiteAccountBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string WebsiteName { get; set; }
		//Iproperty_normal
        string Code { get; set; }
		//Iproperty_normal
        string AllowedIPs { get; set; }
		//Iproperty_normal
        string ResponseUrl { get; set; }
		//Iproperty_normal
        string SecretWord { get; set; }
		//Iproperty_normal
        int IterationsToDeriveHash { get; set; }
		//Iproperty_normal
        string PaypalSandboxUsername { get; set; }
		//Iproperty_normal
        string PaypalSandboxPassword { get; set; }
		//Iproperty_normal
        string PaypalLiveUsername { get; set; }
		//Iproperty_normal
        string PaypalLivePassword { get; set; }
		//Iproperty_normal
        bool PaypalEnabled { get; set; }
		//Iproperty_normal
        bool PaypalUseLiveEnvironment { get; set; }
		//Iproperty_normal
        string PaypalLiveSignature { get; set; }
		//Iproperty_normal
        string PaypalSandboxSignature { get; set; }
		//Iproperty_normal
        string PaypalLiveMerchantId { get; set; }
		//Iproperty_normal
        string PaypalSandboxMerchantId { get; set; }
		//Iproperty_normal
        string PaypalLiveMerchantEmail { get; set; }
		//Iproperty_normal
        string PaypalSandboxMerchantEmail { get; set; }
		//Iproperty_normal
        bool ChequeEnabled { get; set; }
		//Iproperty_normal
        bool MoneybookersEnabled { get; set; }
		//Iproperty_normal
        bool NetellerEnabled { get; set; }
		//Iproperty_normal
        bool EnableFakePayments { get; set; }
		//Iproperty_normal
        string ChequeAddress1 { get; set; }
		//Iproperty_normal
        string ChequeAddress2 { get; set; }
		//Iproperty_normal
        string ChequeAddressCountry { get; set; }
		//Iproperty_normal
        string ChequeAddressPostCode { get; set; }
		//Iproperty_normal
        string ChequeAddressLocality { get; set; }
		//Iproperty_normal
        string ChequePayableTo { get; set; }
		//Iproperty_normal
        string PaymentGatewayTransactiumLiveUsername { get; set; }
		//Iproperty_normal
        string PaymentGatewayTransactiumLivePassword { get; set; }
		//Iproperty_normal
        string PaymentGatewayTransactiumStagingUsername { get; set; }
		//Iproperty_normal
        string PaymentGatewayTransactiumStagingPassword { get; set; }
		//Iproperty_normal
        bool PaymentGatewayTransactiumUseLiveEnvironment { get; set; }
		//Iproperty_normal
        string PaymentGatewayTransactiumProfileTag { get; set; }
		//Iproperty_normal
        string PaymentGatewayTransactiumBankStatementText { get; set; }
		//Iproperty_normal
        bool PaymentGatewayTransactiumEnabled { get; set; }
		//Iproperty_normal
        bool PaymentGatewayApcoEnabled { get; set; }
		//Iproperty_normal
        string PaymentGatewayApcoLiveProfileId { get; set; }
		//Iproperty_normal
        string PaymentGatewayApcoLiveSecretWord { get; set; }
		//Iproperty_normal
        string PaymentGatewayApcoBankStatementText { get; set; }
		//Iproperty_normal
        string PaymentGatewayApcoStagingSecretWord { get; set; }
		//Iproperty_normal
        string PaymentGatewayApcoStagingProfileId { get; set; }
		//Iproperty_normal
        bool PaymentGatewayApcoUseLiveEnvironment { get; set; }
		//Iproperty_normal
        string CssFilename { get; set; }
		//Iproperty_normal
        string LogoFilename { get; set; }
		//Iproperty_normal
        bool NotifyClientsByEmailAboutPayment { get; set; }
		//Iproperty_normal
        string NotificationEmail { get; set; }
		//Iproperty_normal
        string ContactEmail { get; set; }
		//Iproperty_normal
        string ContactName { get; set; }
		//Iproperty_normal
        bool PaymentGatewayRealexEnabled { get; set; }
		//Iproperty_normal
        bool PaymentGatewayRealexUseLiveEnvironment { get; set; }
		//Iproperty_normal
        string PaymentGatewayRealexMerchantId { get; set; }
		//Iproperty_normal
        string PaymentGatewayRealexLiveAccountName { get; set; }
		//Iproperty_normal
        string PaymentGatewayRealexStagingAccountName { get; set; }
		//Iproperty_normal
        string PaymentGatewayRealexSecretWord { get; set; }
		//Iproperty_normal
        bool DisableResponseUrlNotifications { get; set; }
		//Iproperty_normal
        bool WaitUntilPaymentNotificationAcknowledgedBeforeRedirectToSuccess { get; set; }
		//IProperty_Multilingual
	   string PaymentGatewayTransactiumDescription { get; set ;}
		//IProperty_Multilingual
	   string PaymentGatewayApcoDescription { get; set ;}
		//IProperty_Multilingual
	   string PaymentGatewayRealexDescription { get; set ;}
		//Iproperty_normal
        string PaymentGatewayRealexStatementText { get; set; }
		//Iproperty_normal
        string FakePaymentKey { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
