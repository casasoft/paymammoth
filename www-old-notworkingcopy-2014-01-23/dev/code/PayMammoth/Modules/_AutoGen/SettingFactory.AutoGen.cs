using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.SettingModule;
using PayMammoth.Modules.SettingModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class SettingFactoryAutoGen : BusinessLogic_v3.Modules.SettingModule.SettingBaseFactory, 
    				PayMammoth.Modules._AutoGen.ISettingFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<ISetting>
    
    {
    
     	public new Setting ReloadItemFromDatabase(BusinessLogic_v3.Modules.SettingModule.SettingBase item)
        {
         	return (Setting)base.ReloadItemFromDatabase(item);
        }
     	static SettingFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public SettingFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(SettingImpl);
			PayMammoth.Cms.SettingModule.SettingCmsFactory._initialiseStaticInstance();
        }
        public static new SettingFactory Instance
        {
            get
            {
                return (SettingFactory)SettingBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Setting> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Setting>();
            
        }
        public new IEnumerable<Setting> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Setting>();
            
            
        }
    
        public new IEnumerable<Setting> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Setting>();
            
        }
        public new IEnumerable<Setting> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Setting>();
        }
        
        public new Setting FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Setting)base.FindItem(query);
        }
        public new Setting FindItem(IQueryOver query)
        {
            return (Setting)base.FindItem(query);
        }
        
        ISetting ISettingFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Setting CreateNewItem()
        {
            return (Setting)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<Setting, Setting> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Setting>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.SettingModule.Setting GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.SettingModule.Setting) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.SettingModule.Setting> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.SettingModule.Setting>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.SettingModule.Setting> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.SettingModule.Setting>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.SettingModule.Setting> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.SettingModule.Setting>) base.FindAll(session);

       }
       public new PayMammoth.Modules.SettingModule.Setting FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.SettingModule.Setting) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.SettingModule.Setting FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.SettingModule.Setting) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.SettingModule.Setting FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.SettingModule.Setting) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.SettingModule.Setting> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.SettingModule.Setting>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.SettingModule.ISetting> Members
     
        PayMammoth.Modules.SettingModule.ISetting IBaseDbFactory<PayMammoth.Modules.SettingModule.ISetting>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.SettingModule.ISetting IBaseDbFactory<PayMammoth.Modules.SettingModule.ISetting>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.SettingModule.ISetting> IBaseDbFactory<PayMammoth.Modules.SettingModule.ISetting>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.SettingModule.ISetting> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.SettingModule.ISetting>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.SettingModule.ISetting> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.SettingModule.ISetting>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.SettingModule.ISetting IBaseDbFactory<PayMammoth.Modules.SettingModule.ISetting>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.SettingModule.ISetting BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.SettingModule.ISetting>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.SettingModule.ISetting BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.SettingModule.ISetting>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.SettingModule.ISetting> IBaseDbFactory<PayMammoth.Modules.SettingModule.ISetting>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ISetting> ISettingFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ISetting> ISettingFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ISetting> ISettingFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       ISetting ISettingFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ISetting ISettingFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ISetting ISettingFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ISetting> ISettingFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ISetting ISettingFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
