using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ProductCategoryModule;
using PayMammoth.Modules.ProductCategoryModule;
using BusinessLogic_v3.Modules.ProductCategoryModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ProductCategoryModule.ProductCategoryFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ProductCategoryModule.IProductCategory> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ProductCategoryModule.ProductCategoryFrontend ToFrontend(this PayMammoth.Modules.ProductCategoryModule.IProductCategory item)
        {
        	return PayMammoth.Frontend.ProductCategoryModule.ProductCategoryFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCategoryFrontend_AutoGen : BusinessLogic_v3.Frontend.ProductCategoryModule.ProductCategoryBaseFrontend

    {
		
        
        protected ProductCategoryFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ProductCategoryModule.IProductCategory Data
        {
            get { return (PayMammoth.Modules.ProductCategoryModule.IProductCategory)base.Data; }
            set { base.Data = value; }

        }

        public new static ProductCategoryFrontend CreateNewItem()
        {
            return (ProductCategoryFrontend)BusinessLogic_v3.Frontend.ProductCategoryModule.ProductCategoryBaseFrontend.CreateNewItem();
        }
        
        public new static ProductCategoryFrontend Get(BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBase data)
        {
            return (ProductCategoryFrontend)BusinessLogic_v3.Frontend.ProductCategoryModule.ProductCategoryBaseFrontend.Get(data);
        }
        public new static List<ProductCategoryFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ProductCategoryModule.ProductCategoryBaseFrontend.GetList(dataList).Cast<ProductCategoryFrontend>().ToList();
        }


    }
}
