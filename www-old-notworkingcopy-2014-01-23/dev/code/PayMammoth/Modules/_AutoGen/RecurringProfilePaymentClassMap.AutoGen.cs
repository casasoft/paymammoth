using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.RecurringProfilePaymentModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class RecurringProfilePaymentMap_AutoGen : RecurringProfilePaymentMap_AutoGen_Z
    {
        public RecurringProfilePaymentMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.RecurringProfile, RecurringProfileMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FailureCount, FailureCountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NextRetryOn, NextRetryOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentDueOn, PaymentDueOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Status, StatusMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.StatusLog, StatusLogMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.InitiatedByPayMammoth, InitiatedByPayMammothMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MarkAsPaymentFailedIfNotPaidBy, MarkAsPaymentFailedIfNotPaidByMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.Transactions, TransactionsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class RecurringProfilePaymentMap_AutoGen_Z : PayMammoth.Modules._AutoGen.RecurringProfilePaymentBaseMap<RecurringProfilePaymentImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void RecurringProfileMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "RecurringProfileId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

//BaseClassMap_Collection_OneToMany

        protected virtual void TransactionsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "RecurringProfilePaymentID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
            
        
        
    }
}
