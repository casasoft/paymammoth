using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.CurrencyModule;
using PayMammoth.Modules.CurrencyModule;
using BusinessLogic_v3.Modules.CurrencyModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.CurrencyModule.CurrencyFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.CurrencyModule.ICurrency> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.CurrencyModule.CurrencyFrontend ToFrontend(this PayMammoth.Modules.CurrencyModule.ICurrency item)
        {
        	return PayMammoth.Frontend.CurrencyModule.CurrencyFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class CurrencyFrontend_AutoGen : BusinessLogic_v3.Frontend.CurrencyModule.CurrencyBaseFrontend

    {
		
        
        protected CurrencyFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.CurrencyModule.ICurrency Data
        {
            get { return (PayMammoth.Modules.CurrencyModule.ICurrency)base.Data; }
            set { base.Data = value; }

        }

        public new static CurrencyFrontend CreateNewItem()
        {
            return (CurrencyFrontend)BusinessLogic_v3.Frontend.CurrencyModule.CurrencyBaseFrontend.CreateNewItem();
        }
        
        public new static CurrencyFrontend Get(BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase data)
        {
            return (CurrencyFrontend)BusinessLogic_v3.Frontend.CurrencyModule.CurrencyBaseFrontend.Get(data);
        }
        public new static List<CurrencyFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase> dataList)
        {
            return BusinessLogic_v3.Frontend.CurrencyModule.CurrencyBaseFrontend.GetList(dataList).Cast<CurrencyFrontend>().ToList();
        }


    }
}
