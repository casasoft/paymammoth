using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ArticleModule;
using CS.WebComponentsGeneralV3.Cms.ArticleModule;
using PayMammoth.Frontend.ArticleModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ArticleCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ArticleModule.ArticleBaseCmsInfo
    {

		public ArticleCmsInfo_AutoGen(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item)
            : base(item)
        {

        }
        

        private ArticleFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ArticleFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ArticleFrontend FrontendItem
        {
            get { return (ArticleFrontend) base.FrontendItem; }
        }
        
        
		public new Article DbItem
        {
            get
            {
                return (Article)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CustomProjOnlyField { get; protected set; }

        public CmsPropertyInfo CustomProjOnlyLinkedObject { get; protected set; }



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

        this.AllowUpdate_AccessRequired = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.AllowUpdate_AccessRequired),
        						isEditable: true, isVisible: true);

        this.AllowDelete_AccessRequired = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.AllowDelete_AccessRequired),
        						isEditable: true, isVisible: true);

        this.AllowAddChildren_AccessRequired = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.AllowAddChildren_AccessRequired),
        						isEditable: true, isVisible: true);

        this.AllowAddSubChildren_AccessRequired = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.AllowAddSubChildren_AccessRequired),
        						isEditable: true, isVisible: true);

        this.CustomLink = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.CustomLink),
        						isEditable: true, isVisible: true);

        this.HtmlText = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.HtmlText),
        						isEditable: true, isVisible: true);

        this.HtmlText_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.HtmlText_Search),
        						isEditable: false, isVisible: false);

        this.MetaKeywords = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.MetaKeywords),
        						isEditable: true, isVisible: true);

        this.MetaKeywords_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.MetaKeywords_Search),
        						isEditable: false, isVisible: false);

        this.MetaDescription = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.MetaDescription),
        						isEditable: true, isVisible: true);

        this.MetaDescription_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.MetaDescription_Search),
        						isEditable: false, isVisible: false);

        this.Color = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.Color),
        						isEditable: true, isVisible: true);

        this.Name = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.Name),
        						isEditable: true, isVisible: true);

        this.PageTitle = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.PageTitle),
        						isEditable: true, isVisible: true);

        this.PageTitle_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.PageTitle_Search),
        						isEditable: false, isVisible: false);

        this.ExcludeFromRewriteUrl = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ExcludeFromRewriteUrl),
        						isEditable: true, isVisible: true);

        this.ImageFilename = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ImageFilename),
        						isEditable: false, isVisible: false);

        this.ShowAdverts = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ShowAdverts),
        						isEditable: true, isVisible: true);

        this.VisibleInCMS_AccessRequired = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.VisibleInCMS_AccessRequired),
        						isEditable: true, isVisible: true);

        this.DoNotShowLastEditedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DoNotShowLastEditedOn),
        						isEditable: true, isVisible: true);

        this.ViewCount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ViewCount),
        						isEditable: true, isVisible: true);

        this.IsFeatured = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.IsFeatured),
        						isEditable: true, isVisible: true);

        this.MetaTitle = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.MetaTitle),
        						isEditable: true, isVisible: true);

        this.NoFollow = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.NoFollow),
        						isEditable: true, isVisible: true);

        this.NoIndex = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.NoIndex),
        						isEditable: true, isVisible: true);

        this.UsedInProject = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.UsedInProject),
        						isEditable: true, isVisible: true);

        this.ArticleType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ArticleType),
        						isEditable: true, isVisible: true);

        this.NotVisibleInFrontend = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.NotVisibleInFrontend),
        						isEditable: true, isVisible: true);

        this.VisibleInFrontendOnlyWhenLoggedIn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.VisibleInFrontendOnlyWhenLoggedIn),
        						isEditable: true, isVisible: true);

        this.NotVisibleInFrontendWhenLoggedIn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.NotVisibleInFrontendWhenLoggedIn),
        						isEditable: true, isVisible: true);

        this.CustomContentTags = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.CustomContentTags),
        						isEditable: true, isVisible: true);

        this.DoNotRedirectOnLogin = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DoNotRedirectOnLogin),
        						isEditable: true, isVisible: true);

        this.DoNotShowAddThis = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DoNotShowAddThis),
        						isEditable: true, isVisible: true);

        this.DoNotShowFooter = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DoNotShowFooter),
        						isEditable: true, isVisible: true);

        this.DialogWidth = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DialogWidth),
        						isEditable: true, isVisible: true);

        this.DialogHeight = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DialogHeight),
        						isEditable: true, isVisible: true);

        this.DialogPage = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DialogPage),
        						isEditable: true, isVisible: true);

        this.RatingCount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.RatingCount),
        						isEditable: true, isVisible: true);

        this.AvgRating = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.AvgRating),
        						isEditable: true, isVisible: true);

        this.IsCommentable = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.IsCommentable),
        						isEditable: true, isVisible: true);

        this.Summary = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.Summary),
        						isEditable: true, isVisible: true);

        this.OmitChildrenInNavigation = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.OmitChildrenInNavigation),
        						isEditable: true, isVisible: true);

        this.ConsiderAsRootNode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ConsiderAsRootNode),
        						isEditable: true, isVisible: true);

        this.DoNotShowNavigationBreadcrumbs = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DoNotShowNavigationBreadcrumbs),
        						isEditable: true, isVisible: true);

        this.CreatedOnDate = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.CreatedOnDate),
        						isEditable: true, isVisible: true);

        this.PhysicalFilePath = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.PhysicalFilePath),
        						isEditable: true, isVisible: true);

        this.ProcessContentUsingNVelocity = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ProcessContentUsingNVelocity),
        						isEditable: true, isVisible: true);

        this.DialogCssClass = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DialogCssClass),
        						isEditable: true, isVisible: true);

        this.DialogModal = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DialogModal),
        						isEditable: true, isVisible: true);

        this.DialogCloseable = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DialogCloseable),
        						isEditable: true, isVisible: true);

        this.DoNotRedirectOnLogout = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DoNotRedirectOnLogout),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.LinkedRoute = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.LinkedRoute),
                null));

        
        this.SubTitle = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.SubTitle),
        						isEditable: true, isVisible: true);

        this.HrefTarget = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.HrefTarget),
        						isEditable: true, isVisible: true);

        this.IsInheritedCommentable_Computed = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.IsInheritedCommentable_Computed),
        						isEditable: true, isVisible: true);

        this.AuthorName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.AuthorName),
        						isEditable: true, isVisible: true);

        this.DoNotShowInSearchResults = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DoNotShowInSearchResults),
        						isEditable: true, isVisible: true);

        this.DoNotShowInSearchResults_Computed = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DoNotShowInSearchResults_Computed),
        						isEditable: true, isVisible: true);

        this._Computed_CommentAndRepliesCount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item._Computed_CommentAndRepliesCount),
        						isEditable: true, isVisible: true);

        this._ComputedURL = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item._ComputedURL),
        						isEditable: true, isVisible: true);

        this._Computed_ParentArticleNames = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item._Computed_ParentArticleNames),
        						isEditable: true, isVisible: true);

        this.MaxLevelDepthToShowInCms = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.MaxLevelDepthToShowInCms),
        						isEditable: true, isVisible: true);

        this.UpdateVersion = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.UpdateVersion),
        						isEditable: true, isVisible: true);

        this.DoNotShowAuthor = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.DoNotShowAuthor),
        						isEditable: true, isVisible: true);

        this.FeaturedDateFrom = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.FeaturedDateFrom),
        						isEditable: true, isVisible: true);

        this.FeaturedDateTo = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.FeaturedDateTo),
        						isEditable: true, isVisible: true);

        this.Tags = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.Tags),
        						isEditable: true, isVisible: true);

        this._Computed_FullPath = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item._Computed_FullPath),
        						isEditable: true, isVisible: true);

        this.CustomProjOnlyField = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.CustomProjOnlyField),
        						isEditable: true, isVisible: true);

        this.CustomProjOnlyLinkedObject = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.CustomProjOnlyLinkedObject),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
