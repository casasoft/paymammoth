using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using NHibernate;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.TicketCommentModule;

namespace PayMammoth.Modules._AutoGen
{

//UserClassImpl-File

    public class TicketCommentImpl : PayMammoth.Modules.TicketCommentModule.TicketComment
    {
/*
 		public new CmsUserImpl LastEditedBy
        {
            get { return (CmsUserImpl) base.LastEditedBy; }
            set { base.LastEditedBy = value; }
        }
        public new CmsUserImpl DeletedBy
        {
            get { return (CmsUserImpl)base.DeletedBy; }
            set { base.DeletedBy = value; }
        }
*/
		#region UserClass-AutoGenerated
		#endregion

      
      
        private TicketCommentImpl()
        {
            
        }    
    	

// [userclassimpl_collections]

        //UserClassImpl_CollectionOneToManyLeftSide
        
        protected override IEnumerable<BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase> __collection__Attachments
        {
            get
            {
                return this._Attachments;
            }
        }

        private Iesi.Collections.Generic.ISet<AttachmentImpl> _Attachments = new Iesi.Collections.Generic.HashedSet<AttachmentImpl>();
        public new virtual Iesi.Collections.Generic.ISet<AttachmentImpl> Attachments
        {
            get { return _Attachments; }

        }
    	


    	
        
    }
}




