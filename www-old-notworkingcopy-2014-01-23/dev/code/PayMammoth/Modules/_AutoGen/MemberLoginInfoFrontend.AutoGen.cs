using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.MemberLoginInfoModule;
using PayMammoth.Modules.MemberLoginInfoModule;
using BusinessLogic_v3.Modules.MemberLoginInfoModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.MemberLoginInfoModule.MemberLoginInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.MemberLoginInfoModule.MemberLoginInfoFrontend ToFrontend(this PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo item)
        {
        	return PayMammoth.Frontend.MemberLoginInfoModule.MemberLoginInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberLoginInfoFrontend_AutoGen : BusinessLogic_v3.Frontend.MemberLoginInfoModule.MemberLoginInfoBaseFrontend

    {
		
        
        protected MemberLoginInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo Data
        {
            get { return (PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static MemberLoginInfoFrontend CreateNewItem()
        {
            return (MemberLoginInfoFrontend)BusinessLogic_v3.Frontend.MemberLoginInfoModule.MemberLoginInfoBaseFrontend.CreateNewItem();
        }
        
        public new static MemberLoginInfoFrontend Get(BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBase data)
        {
            return (MemberLoginInfoFrontend)BusinessLogic_v3.Frontend.MemberLoginInfoModule.MemberLoginInfoBaseFrontend.Get(data);
        }
        public new static List<MemberLoginInfoFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBase> dataList)
        {
            return BusinessLogic_v3.Frontend.MemberLoginInfoModule.MemberLoginInfoBaseFrontend.GetList(dataList).Cast<MemberLoginInfoFrontend>().ToList();
        }


    }
}
