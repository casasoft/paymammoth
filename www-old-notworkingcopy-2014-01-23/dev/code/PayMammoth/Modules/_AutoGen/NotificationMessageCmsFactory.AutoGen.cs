using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Cms.NotificationMessageModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class NotificationMessageCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.NotificationMessageBaseCmsFactory
    {
        public static new NotificationMessageBaseCmsFactory Instance 
        {
         	get { return (NotificationMessageBaseCmsFactory)PayMammoth.Modules._AutoGen.NotificationMessageBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override NotificationMessageBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	NotificationMessage item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = NotificationMessage.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        NotificationMessageCmsInfo cmsItem = new NotificationMessageCmsInfo(item);
            return cmsItem;
        }    
        
        public override NotificationMessageBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.NotificationMessageBase item)
        {
            return new NotificationMessageCmsInfo((NotificationMessage)item);
            
        }

    }
}
