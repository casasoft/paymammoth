using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Product_AutoGen : ProductBase, PayMammoth.Modules._AutoGen.IProductAutoGen
	                 
      , IMultilingualItem
      
      
      
    {
        
            

    
    
    
		

#region Multilingual
        public new PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo getCurrentCulture()
        {
            return (PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo)__getCurrentCulture();
        }             
#endregion
        
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.BrandModule.Brand Brand
        {
            get
            {
                return (PayMammoth.Modules.BrandModule.Brand)base.Brand;
            }
            set
            {
                base.Brand = value;
            }
        }

		PayMammoth.Modules.BrandModule.IBrand IProductAutoGen.Brand
        {
            get
            {
            	return this.Brand;
                
            }
            set
            {
                this.Brand = (PayMammoth.Modules.BrandModule.Brand)value;
            }
        }



// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __Product_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ProductModule.Product, 
        	PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo,
        	PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductModule.Product, PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo>
        {
            private PayMammoth.Modules.ProductModule.Product _item = null;
            public __Product_CultureInfosCollectionInfo(PayMammoth.Modules.ProductModule.Product item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo>)_item.__collection__Product_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductModule.Product, PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo>.SetLinkOnItem(PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo item, PayMammoth.Modules.ProductModule.Product value)
            {
                item.Product = value;
            }
        }
        
        private __Product_CultureInfosCollectionInfo _Product_CultureInfos = null;
        internal new __Product_CultureInfosCollectionInfo Product_CultureInfos
        {
            get
            {
                if (_Product_CultureInfos == null)
                    _Product_CultureInfos = new __Product_CultureInfosCollectionInfo((PayMammoth.Modules.ProductModule.Product)this);
                return _Product_CultureInfos;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo> IProductAutoGen.Product_CultureInfos
        {
            get {  return this.Product_CultureInfos; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __CategoryLinksCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ProductModule.Product, 
        	PayMammoth.Modules.ProductCategoryModule.ProductCategory,
        	PayMammoth.Modules.ProductCategoryModule.IProductCategory>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductModule.Product, PayMammoth.Modules.ProductCategoryModule.ProductCategory>
        {
            private PayMammoth.Modules.ProductModule.Product _item = null;
            public __CategoryLinksCollectionInfo(PayMammoth.Modules.ProductModule.Product item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.ProductCategoryModule.ProductCategory> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.ProductCategoryModule.ProductCategory>)_item.__collection__CategoryLinks; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductModule.Product, PayMammoth.Modules.ProductCategoryModule.ProductCategory>.SetLinkOnItem(PayMammoth.Modules.ProductCategoryModule.ProductCategory item, PayMammoth.Modules.ProductModule.Product value)
            {
                item.Product = value;
            }
        }
        
        private __CategoryLinksCollectionInfo _CategoryLinks = null;
        internal new __CategoryLinksCollectionInfo CategoryLinks
        {
            get
            {
                if (_CategoryLinks == null)
                    _CategoryLinks = new __CategoryLinksCollectionInfo((PayMammoth.Modules.ProductModule.Product)this);
                return _CategoryLinks;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.ProductCategoryModule.IProductCategory> IProductAutoGen.CategoryLinks
        {
            get {  return this.CategoryLinks; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ProductCategoryFeatureValuesCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ProductModule.Product, 
        	PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue,
        	PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductModule.Product, PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue>
        {
            private PayMammoth.Modules.ProductModule.Product _item = null;
            public __ProductCategoryFeatureValuesCollectionInfo(PayMammoth.Modules.ProductModule.Product item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue>)_item.__collection__ProductCategoryFeatureValues; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductModule.Product, PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue>.SetLinkOnItem(PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue item, PayMammoth.Modules.ProductModule.Product value)
            {
                item.Product = value;
            }
        }
        
        private __ProductCategoryFeatureValuesCollectionInfo _ProductCategoryFeatureValues = null;
        internal new __ProductCategoryFeatureValuesCollectionInfo ProductCategoryFeatureValues
        {
            get
            {
                if (_ProductCategoryFeatureValues == null)
                    _ProductCategoryFeatureValues = new __ProductCategoryFeatureValuesCollectionInfo((PayMammoth.Modules.ProductModule.Product)this);
                return _ProductCategoryFeatureValues;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> IProductAutoGen.ProductCategoryFeatureValues
        {
            get {  return this.ProductCategoryFeatureValues; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ProductCategorySpecificationValuesCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ProductModule.Product, 
        	PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue,
        	PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductModule.Product, PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue>
        {
            private PayMammoth.Modules.ProductModule.Product _item = null;
            public __ProductCategorySpecificationValuesCollectionInfo(PayMammoth.Modules.ProductModule.Product item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue>)_item.__collection__ProductCategorySpecificationValues; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductModule.Product, PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue>.SetLinkOnItem(PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue item, PayMammoth.Modules.ProductModule.Product value)
            {
                item.Product = value;
            }
        }
        
        private __ProductCategorySpecificationValuesCollectionInfo _ProductCategorySpecificationValues = null;
        internal new __ProductCategorySpecificationValuesCollectionInfo ProductCategorySpecificationValues
        {
            get
            {
                if (_ProductCategorySpecificationValues == null)
                    _ProductCategorySpecificationValues = new __ProductCategorySpecificationValuesCollectionInfo((PayMammoth.Modules.ProductModule.Product)this);
                return _ProductCategorySpecificationValues;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> IProductAutoGen.ProductCategorySpecificationValues
        {
            get {  return this.ProductCategorySpecificationValues; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ProductVariationsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ProductModule.Product, 
        	PayMammoth.Modules.ProductVariationModule.ProductVariation,
        	PayMammoth.Modules.ProductVariationModule.IProductVariation>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductModule.Product, PayMammoth.Modules.ProductVariationModule.ProductVariation>
        {
            private PayMammoth.Modules.ProductModule.Product _item = null;
            public __ProductVariationsCollectionInfo(PayMammoth.Modules.ProductModule.Product item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.ProductVariationModule.ProductVariation> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.ProductVariationModule.ProductVariation>)_item.__collection__ProductVariations; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductModule.Product, PayMammoth.Modules.ProductVariationModule.ProductVariation>.SetLinkOnItem(PayMammoth.Modules.ProductVariationModule.ProductVariation item, PayMammoth.Modules.ProductModule.Product value)
            {
                item.Product = value;
            }
        }
        
        private __ProductVariationsCollectionInfo _ProductVariations = null;
        internal new __ProductVariationsCollectionInfo ProductVariations
        {
            get
            {
                if (_ProductVariations == null)
                    _ProductVariations = new __ProductVariationsCollectionInfo((PayMammoth.Modules.ProductModule.Product)this);
                return _ProductVariations;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.ProductVariationModule.IProductVariation> IProductAutoGen.ProductVariations
        {
            get {  return this.ProductVariations; }
        }
           

        
          
		public  static new ProductFactory Factory
        {
            get
            {
                return (ProductFactory)ProductBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Product, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


        public void CheckItemCultureInfos(bool autoSaveInDB, IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails> CultureInfos = null)
        {
            MultilingualChecker multilingualChecker = new MultilingualChecker();
            multilingualChecker.ItemToAdd+=new MultilingualChecker.ItemToAddHandler(multilingualChecker_ItemToAdd);
            multilingualChecker.ItemRemoved += new MultilingualChecker.ItemRemovedHandler(multilingualChecker_ItemRemoved);
            multilingualChecker.CheckItemCultureInfos(this, CultureInfos);
            if (autoSaveInDB)
                this.Save();
        }


        private void multilingualChecker_ItemToAdd(BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase cultureDetails)
        {
            var itemCultureInfo = PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo.Factory.CreateNewItem();

            itemCultureInfo.Product = (Product)this;  //item link
            itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture


// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.Title = this.__Title_cultureBase;

                        itemCultureInfo.Description = this.__Description_cultureBase;



			itemCultureInfo.MarkAsNotTemporary();
			this.Product_CultureInfos.Add((PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo) itemCultureInfo);
            
            

        }

		private void multilingualChecker_ItemRemoved(IMultilingualContentInfo item)
        {   
        	this.Product_CultureInfos.Remove((PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo)item);
            
        }

        
        protected override BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase __getCultureByCultureDBId(long cultureDBId)
        {
        	bool sessionExists = NHClasses.NhManager.CheckSessionExistsForContext();
            MyNHSessionBase session = null;
            if (!sessionExists)
            {

                CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Debug, "Session did not exist when retrieving the multilingual collection");

                 session = NHClasses.NhManager.CreateNewSessionForContextAndReturn();
                
                 this.Merge(session);
            }
        	foreach (var cultureInfo in this.Product_CultureInfos)
            {
                if (cultureInfo.CultureInfo.ID == cultureDBId)
                {
                    _currentCulture = cultureInfo;
                    break;
                }
            }
            if (!sessionExists)
            {
                NHClasses.NhManager.DisposeCurrentSessionInContext();
            }
            return _currentCulture;
        }
        private PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo _currentCulture = null;

        protected override BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase __getCurrentCulture()
        {

            var currentCultureID = GetCurrentCultureDBId();
            
            if (_currentCulture == null || _currentCulture.CultureInfo.ID != currentCultureID)
            {   //requires re-loading
            	_currentCulture = null;
                _currentCulture = (PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo)__getCultureByCultureDBId(currentCultureID);
            }
            
            if (_currentCulture == null)
            {
                this.CheckItemCultureInfos(autoSaveInDB:false);
                
            }
            
            if (_currentCulture == null)
            {
                _currentCulture = (PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo)__getCultureByCultureDBId(currentCultureID);
            }
            return _currentCulture;
        }
        

        
         #region IMultilingualItem Members

        IEnumerable<IMultilingualContentInfo> IMultilingualItem.GetMultilingualContentInfos()
        {
            return this.Product_CultureInfos;
            
        }


#endregion    

     
        
        


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
