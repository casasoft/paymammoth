using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ProductCategorySpecificationValueModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ProductCategorySpecificationValueMap_AutoGen : ProductCategorySpecificationValueMap_AutoGen_Z
    {
        public ProductCategorySpecificationValueMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Value, ValueMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Product, ProductMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ProductCategorySpecificationValueMap_AutoGen_Z : BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseMap<ProductCategorySpecificationValueImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
