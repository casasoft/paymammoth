using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace PayMammoth.Modules._AutoGen
{
	public abstract class RecurringProfileTransactionBaseCmsFactory_AutoGen : CmsFactoryBase<RecurringProfileTransactionBaseCmsInfo, RecurringProfileTransactionBase>
    {
       
       public new static RecurringProfileTransactionBaseCmsFactory Instance
	    {
	         get
	         {
                 return (RecurringProfileTransactionBaseCmsFactory)CmsFactoryBase<RecurringProfileTransactionBaseCmsInfo, RecurringProfileTransactionBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = RecurringProfileTransactionBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "RecurringProfileTransaction.aspx";

           // this.listingPageName = "default.aspx";

            if (string.IsNullOrWhiteSpace(cmsInfo.Name )) cmsInfo.Name = "RecurringProfileTransaction";

            if (string.IsNullOrWhiteSpace(this.QueryStringParamID )) this.QueryStringParamID = "RecurringProfileTransactionId";

            if (string.IsNullOrWhiteSpace(cmsInfo.TitlePlural)) cmsInfo.TitlePlural = "Recurring Profile Transactions";

            if (string.IsNullOrWhiteSpace(cmsInfo.TitleSingular )) cmsInfo.TitleSingular =  "Recurring Profile Transaction";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public RecurringProfileTransactionBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "RecurringProfileTransaction/";
			UsedInProject = RecurringProfileTransactionBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
