using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.MemberLoginInfoModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class MemberLoginInfoMap_AutoGen : MemberLoginInfoMap_AutoGen_Z
    {
        public MemberLoginInfoMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.DateTime, DateTimeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IP, IPMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LoginInfoType, LoginInfoTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AdditionalMsg, AdditionalMsgMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class MemberLoginInfoMap_AutoGen_Z : BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBaseMap<MemberLoginInfoImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
