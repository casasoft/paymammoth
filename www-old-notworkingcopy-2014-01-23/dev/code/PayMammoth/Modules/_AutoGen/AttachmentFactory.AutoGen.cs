using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.AttachmentModule;
using PayMammoth.Modules.AttachmentModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class AttachmentFactoryAutoGen : BusinessLogic_v3.Modules.AttachmentModule.AttachmentBaseFactory, 
    				PayMammoth.Modules._AutoGen.IAttachmentFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<IAttachment>
    
    {
    
     	public new Attachment ReloadItemFromDatabase(BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase item)
        {
         	return (Attachment)base.ReloadItemFromDatabase(item);
        }
     	
        public AttachmentFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(AttachmentImpl);
			PayMammoth.Cms.AttachmentModule.AttachmentCmsFactory._initialiseStaticInstance();
        }
        public static new AttachmentFactory Instance
        {
            get
            {
                return (AttachmentFactory)AttachmentBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Attachment> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Attachment>();
            
        }
        public new IEnumerable<Attachment> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Attachment>();
            
            
        }
    
        public new IEnumerable<Attachment> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Attachment>();
            
        }
        public new IEnumerable<Attachment> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Attachment>();
        }
        
        public new Attachment FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Attachment)base.FindItem(query);
        }
        public new Attachment FindItem(IQueryOver query)
        {
            return (Attachment)base.FindItem(query);
        }
        
        IAttachment IAttachmentFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Attachment CreateNewItem()
        {
            return (Attachment)base.CreateNewItem();
        }

        public new Attachment GetByPrimaryKey(long pKey)
        {
            return (Attachment)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Attachment, Attachment> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Attachment>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.AttachmentModule.Attachment GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.AttachmentModule.Attachment) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.AttachmentModule.Attachment> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.AttachmentModule.Attachment>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.AttachmentModule.Attachment> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.AttachmentModule.Attachment>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.AttachmentModule.Attachment> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.AttachmentModule.Attachment>) base.FindAll(session);

       }
       public new PayMammoth.Modules.AttachmentModule.Attachment FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.AttachmentModule.Attachment) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.AttachmentModule.Attachment FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.AttachmentModule.Attachment) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.AttachmentModule.Attachment FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.AttachmentModule.Attachment) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.AttachmentModule.Attachment> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.AttachmentModule.Attachment>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.AttachmentModule.IAttachment> Members
     
        PayMammoth.Modules.AttachmentModule.IAttachment IBaseDbFactory<PayMammoth.Modules.AttachmentModule.IAttachment>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.AttachmentModule.IAttachment IBaseDbFactory<PayMammoth.Modules.AttachmentModule.IAttachment>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.AttachmentModule.IAttachment> IBaseDbFactory<PayMammoth.Modules.AttachmentModule.IAttachment>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.AttachmentModule.IAttachment> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.AttachmentModule.IAttachment>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.AttachmentModule.IAttachment> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.AttachmentModule.IAttachment>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.AttachmentModule.IAttachment IBaseDbFactory<PayMammoth.Modules.AttachmentModule.IAttachment>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.AttachmentModule.IAttachment BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.AttachmentModule.IAttachment>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.AttachmentModule.IAttachment BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.AttachmentModule.IAttachment>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.AttachmentModule.IAttachment> IBaseDbFactory<PayMammoth.Modules.AttachmentModule.IAttachment>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IAttachment> IAttachmentFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IAttachment> IAttachmentFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IAttachment> IAttachmentFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IAttachment IAttachmentFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IAttachment IAttachmentFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IAttachment IAttachmentFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IAttachment> IAttachmentFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IAttachment IAttachmentFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
