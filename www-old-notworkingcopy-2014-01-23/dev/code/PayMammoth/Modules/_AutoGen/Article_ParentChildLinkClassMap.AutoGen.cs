using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.Article_ParentChildLinkModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class Article_ParentChildLinkMap_AutoGen : Article_ParentChildLinkMap_AutoGen_Z
    {
        public Article_ParentChildLinkMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Parent, ParentMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Child, ChildMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class Article_ParentChildLinkMap_AutoGen_Z : BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBaseMap<Article_ParentChildLinkImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
