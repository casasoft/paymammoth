using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentRequestItemDetailModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IPaymentRequestItemDetailFactoryAutoGen : PayMammoth.Modules._AutoGen.IPaymentRequestItemDetailBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IPaymentRequestItemDetail>
    
    {
	    new PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail CreateNewItem();
        new PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
