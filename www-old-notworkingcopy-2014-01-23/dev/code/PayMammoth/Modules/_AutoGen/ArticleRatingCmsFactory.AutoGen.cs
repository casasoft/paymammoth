using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ArticleRatingModule;
using PayMammoth.Cms.ArticleRatingModule;
using CS.WebComponentsGeneralV3.Cms.ArticleRatingModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleRatingCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ArticleRatingModule.ArticleRatingBaseCmsFactory
    {
        public static new ArticleRatingBaseCmsFactory Instance 
        {
         	get { return (ArticleRatingBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ArticleRatingModule.ArticleRatingBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ArticleRatingBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = ArticleRating.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            ArticleRatingCmsInfo cmsItem = new ArticleRatingCmsInfo(item);
            return cmsItem;
        }    
        
        public override ArticleRatingBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase item)
        {
            return new ArticleRatingCmsInfo((ArticleRating)item);
            
        }

    }
}
