using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.PaymentMethod_CultureInfoModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.PaymentMethod_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class PaymentMethod_CultureInfoCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBaseCmsInfo
    {

		public PaymentMethod_CultureInfoCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase item)
            : base(item)
        {

        }
        

        private PaymentMethod_CultureInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = PaymentMethod_CultureInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new PaymentMethod_CultureInfoFrontend FrontendItem
        {
            get { return (PaymentMethod_CultureInfoFrontend) base.FrontendItem; }
        }
        
        
		public new PaymentMethod_CultureInfo DbItem
        {
            get
            {
                return (PaymentMethod_CultureInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.CultureInfo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>.GetPropertyBySelector( item => item.CultureInfo),
                null));

        
//InitFieldUser_LinkedProperty
			this.PaymentMethod = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>.GetPropertyBySelector( item => item.PaymentMethod),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentMethodModule.PaymentMethod>.GetPropertyBySelector( item => item.PaymentMethod_CultureInfos)));

        
        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
