using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.Brand_CultureInfoModule;
using PayMammoth.Modules.Brand_CultureInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class Brand_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IBrand_CultureInfoFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<IBrand_CultureInfo>
    
    {
    
     	public new Brand_CultureInfo ReloadItemFromDatabase(BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase item)
        {
         	return (Brand_CultureInfo)base.ReloadItemFromDatabase(item);
        }
     	
        public Brand_CultureInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(Brand_CultureInfoImpl);
			PayMammoth.Cms.Brand_CultureInfoModule.Brand_CultureInfoCmsFactory._initialiseStaticInstance();
        }
        public static new Brand_CultureInfoFactory Instance
        {
            get
            {
                return (Brand_CultureInfoFactory)Brand_CultureInfoBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Brand_CultureInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Brand_CultureInfo>();
            
        }
        public new IEnumerable<Brand_CultureInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Brand_CultureInfo>();
            
            
        }
    
        public new IEnumerable<Brand_CultureInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Brand_CultureInfo>();
            
        }
        public new IEnumerable<Brand_CultureInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Brand_CultureInfo>();
        }
        
        public new Brand_CultureInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Brand_CultureInfo)base.FindItem(query);
        }
        public new Brand_CultureInfo FindItem(IQueryOver query)
        {
            return (Brand_CultureInfo)base.FindItem(query);
        }
        
        IBrand_CultureInfo IBrand_CultureInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Brand_CultureInfo CreateNewItem()
        {
            return (Brand_CultureInfo)base.CreateNewItem();
        }

        public new Brand_CultureInfo GetByPrimaryKey(long pKey)
        {
            return (Brand_CultureInfo)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Brand_CultureInfo, Brand_CultureInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Brand_CultureInfo>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> Members
     
        PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo IBaseDbFactory<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo IBaseDbFactory<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> IBaseDbFactory<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo IBaseDbFactory<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> IBaseDbFactory<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IBrand_CultureInfo> IBrand_CultureInfoFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IBrand_CultureInfo> IBrand_CultureInfoFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IBrand_CultureInfo> IBrand_CultureInfoFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IBrand_CultureInfo IBrand_CultureInfoFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IBrand_CultureInfo IBrand_CultureInfoFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IBrand_CultureInfo IBrand_CultureInfoFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IBrand_CultureInfo> IBrand_CultureInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IBrand_CultureInfo IBrand_CultureInfoFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
