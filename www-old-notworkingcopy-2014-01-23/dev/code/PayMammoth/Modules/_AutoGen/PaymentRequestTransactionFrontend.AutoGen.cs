using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.PaymentRequestTransactionModule;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.PaymentRequestTransactionModule.PaymentRequestTransactionFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.PaymentRequestTransactionModule.PaymentRequestTransactionFrontend ToFrontend(this PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction item)
        {
        	return PayMammoth.Frontend.PaymentRequestTransactionModule.PaymentRequestTransactionFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestTransactionFrontend_AutoGen : PayMammoth.Modules._AutoGen.PaymentRequestTransactionModule.PaymentRequestTransactionBaseFrontend

    {
		
        
        protected PaymentRequestTransactionFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction Data
        {
            get { return (PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction)base.Data; }
            set { base.Data = value; }

        }

        public new static PaymentRequestTransactionFrontend CreateNewItem()
        {
            return (PaymentRequestTransactionFrontend)PayMammoth.Modules._AutoGen.PaymentRequestTransactionModule.PaymentRequestTransactionBaseFrontend.CreateNewItem();
        }
        
        public new static PaymentRequestTransactionFrontend Get(PayMammoth.Modules._AutoGen.IPaymentRequestTransactionBase data)
        {
            return (PaymentRequestTransactionFrontend)PayMammoth.Modules._AutoGen.PaymentRequestTransactionModule.PaymentRequestTransactionBaseFrontend.Get(data);
        }
        public new static List<PaymentRequestTransactionFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IPaymentRequestTransactionBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.PaymentRequestTransactionModule.PaymentRequestTransactionBaseFrontend.GetList(dataList).Cast<PaymentRequestTransactionFrontend>().ToList();
        }


    }
}
