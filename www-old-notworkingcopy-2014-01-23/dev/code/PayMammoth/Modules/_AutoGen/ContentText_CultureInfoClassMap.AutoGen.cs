using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ContentText_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ContentText_CultureInfoMap_AutoGen : ContentText_CultureInfoMap_AutoGen_Z
    {
        public ContentText_CultureInfoMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.CultureInfo, CultureInfoMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.ContentText, ContentTextMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Content, ContentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ImageAlternateText, ImageAlternateTextMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ContentText_CultureInfoMap_AutoGen_Z : BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBaseMap<ContentText_CultureInfoImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
