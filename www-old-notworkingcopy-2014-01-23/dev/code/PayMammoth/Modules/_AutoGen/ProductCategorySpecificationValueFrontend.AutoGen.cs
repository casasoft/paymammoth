using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ProductCategorySpecificationValueModule;
using PayMammoth.Modules.ProductCategorySpecificationValueModule;
using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueFrontend ToFrontend(this PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue item)
        {
        	return PayMammoth.Frontend.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCategorySpecificationValueFrontend_AutoGen : BusinessLogic_v3.Frontend.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseFrontend

    {
		
        
        protected ProductCategorySpecificationValueFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue Data
        {
            get { return (PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue)base.Data; }
            set { base.Data = value; }

        }

        public new static ProductCategorySpecificationValueFrontend CreateNewItem()
        {
            return (ProductCategorySpecificationValueFrontend)BusinessLogic_v3.Frontend.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseFrontend.CreateNewItem();
        }
        
        public new static ProductCategorySpecificationValueFrontend Get(BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBase data)
        {
            return (ProductCategorySpecificationValueFrontend)BusinessLogic_v3.Frontend.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseFrontend.Get(data);
        }
        public new static List<ProductCategorySpecificationValueFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseFrontend.GetList(dataList).Cast<ProductCategorySpecificationValueFrontend>().ToList();
        }


    }
}
