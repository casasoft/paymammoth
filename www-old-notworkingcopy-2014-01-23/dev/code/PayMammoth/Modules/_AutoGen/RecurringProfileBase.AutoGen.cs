using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace PayMammoth.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "RecurringProfile")]
    public abstract class RecurringProfileBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IRecurringProfileBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static RecurringProfileBaseFactory Factory
        {
            get
            {
                return RecurringProfileBaseFactory.Instance; 
            }
        }    
        /*
        public static RecurringProfileBase CreateNewItem()
        {
            return (RecurringProfileBase)Factory.CreateNewItem();
        }

		public static RecurringProfileBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<RecurringProfileBase, RecurringProfileBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static RecurringProfileBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static RecurringProfileBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<RecurringProfileBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<RecurringProfileBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<RecurringProfileBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _profileidentifierongateway;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string ProfileIdentifierOnGateway 
        {
        	get
        	{
        		return _profileidentifierongateway;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_profileidentifierongateway,value);
        		_profileidentifierongateway = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private PayMammoth.Connector.Enums.PaymentMethodSpecific _paymentgateway;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Connector.Enums.PaymentMethodSpecific PaymentGateway 
        {
        	get
        	{
        		return _paymentgateway;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgateway,value);
        		_paymentgateway = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _createdon;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime CreatedOn 
        {
        	get
        	{
        		return _createdon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_createdon,value);
        		_createdon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _billingcyclescompleted2;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual int BillingCyclesCompleted2 
        {
        	get
        	{
        		return _billingcyclescompleted2;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_billingcyclescompleted2,value);
        		_billingcyclescompleted2 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int? _totalbillingcyclesrequired;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual int? TotalBillingCyclesRequired 
        {
        	get
        	{
        		return _totalbillingcyclesrequired;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_totalbillingcyclesrequired,value);
        		_totalbillingcyclesrequired = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private PayMammoth.Enums.RecurringProfileStatus _status;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Enums.RecurringProfileStatus Status 
        {
        	get
        	{
        		return _status;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_status,value);
        		_status = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _maximumfailedattempts;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual int MaximumFailedAttempts 
        {
        	get
        	{
        		return _maximumfailedattempts;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_maximumfailedattempts,value);
        		_maximumfailedattempts = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _activatedon;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime? ActivatedOn 
        {
        	get
        	{
        		return _activatedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_activatedon,value);
        		_activatedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _cancelledon;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime? CancelledOn 
        {
        	get
        	{
        		return _cancelledon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cancelledon,value);
        		_cancelledon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _expiredon;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime? ExpiredOn 
        {
        	get
        	{
        		return _expiredon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_expiredon,value);
        		_expiredon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _recurringintervalfrequency;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual int RecurringIntervalFrequency 
        {
        	get
        	{
        		return _recurringintervalfrequency;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_recurringintervalfrequency,value);
        		_recurringintervalfrequency = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD _recurringintervaltype;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD RecurringIntervalType 
        {
        	get
        	{
        		return _recurringintervaltype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_recurringintervaltype,value);
        		_recurringintervaltype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _statuslog;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string StatusLog 
        {
        	get
        	{
        		return _statuslog;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_statuslog,value);
        		_statuslog = value;
        		
        	}
        }
        
/*
		public virtual long? WebsiteAccountID
		{
		 	get 
		 	{
		 		return (this.WebsiteAccount != null ? (long?)this.WebsiteAccount.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.WebsiteAccountBase _websiteaccount;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Modules._AutoGen.WebsiteAccountBase WebsiteAccount 
        {
        	get
        	{
        		return _websiteaccount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_websiteaccount,value);
        		_websiteaccount = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IWebsiteAccountBase PayMammoth.Modules._AutoGen.IRecurringProfileBaseAutoGen.WebsiteAccount 
        {
            get
            {
            	return this.WebsiteAccount;
            }
            set
            {
            	this.WebsiteAccount = (PayMammoth.Modules._AutoGen.WebsiteAccountBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _notificationurl;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string NotificationUrl 
        {
        	get
        	{
        		return _notificationurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notificationurl,value);
        		_notificationurl = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private PayMammoth.Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE? _creationstatus;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE? CreationStatus 
        {
        	get
        	{
        		return _creationstatus;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_creationstatus,value);
        		_creationstatus = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
