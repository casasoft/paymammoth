using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using PayMammoth.Modules.MemberResponsibleLimitsModule;
using PayMammoth.Cms.MemberResponsibleLimitsModule;
using BusinessLogic_v3.Cms.MemberResponsibleLimitsModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberResponsibleLimitsCmsFactory_AutoGen : BusinessLogic_v3.Cms.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseCmsFactory
    {
        public static new MemberResponsibleLimitsBaseCmsFactory Instance 
        {
         	get { return (MemberResponsibleLimitsBaseCmsFactory)BusinessLogic_v3.Cms.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override MemberResponsibleLimitsBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = MemberResponsibleLimits.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            MemberResponsibleLimitsCmsInfo cmsItem = new MemberResponsibleLimitsCmsInfo(item);
            return cmsItem;
        }    
        
        public override MemberResponsibleLimitsBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase item)
        {
            return new MemberResponsibleLimitsCmsInfo((MemberResponsibleLimits)item);
            
        }

    }
}
