using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using CS.General_v3.Classes.DbObjects.Collections;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public abstract class TestCustomClassBaseAutoGen 
    : BusinessLogic_v3.Classes.DB.BaseDbObject, ITestCustomClassBaseAutoGen
    
      
      
      
    
    
    		
    {
    
   
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	
        public static PayMammoth.Modules._AutoGen.TestCustomClassBaseCmsFactory CmsFactory
        {
            get { return PayMammoth.Modules._AutoGen.TestCustomClassBaseCmsFactory.Instance; }
        }
        
     	public static TestCustomClassBaseFactory Factory
        {
            get
            {
                return TestCustomClassBaseFactory.Instance; 
            }
        }    
        /*
        public static TestCustomClassBase CreateNewItem()
        {
            return (TestCustomClassBase)Factory.CreateNewItem();
        }

		public static TestCustomClassBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<TestCustomClassBase, TestCustomClassBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static TestCustomClassBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static TestCustomClassBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<TestCustomClassBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<TestCustomClassBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<TestCustomClassBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _name;
        public virtual string Name 
        {
        	get
        	{
        		return _name;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_name,value);
        		_name = value;
        		
        	}
        }
        
/*
		public virtual long? TestAdvertID
		{
		 	get 
		 	{
		 		return (this.TestAdvert != null ? (long?)this.TestAdvert.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.AdvertModule.AdvertBase _testadvert;
        public virtual BusinessLogic_v3.Modules.AdvertModule.AdvertBase TestAdvert 
        {
        	get
        	{
        		return _testadvert;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_testadvert,value);
        		_testadvert = value;
        	}
        }
            
        BusinessLogic_v3.Modules.AdvertModule.IAdvertBase PayMammoth.Modules._AutoGen.ITestCustomClassBaseAutoGen.TestAdvert 
        {
            get
            {
            	return this.TestAdvert;
            }
            set
            {
            	this.TestAdvert = (BusinessLogic_v3.Modules.AdvertModule.AdvertBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    
#region Multilingual
          


#endregion    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
