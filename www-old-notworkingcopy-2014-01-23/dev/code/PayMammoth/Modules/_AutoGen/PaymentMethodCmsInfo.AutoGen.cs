using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.PaymentMethodModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.PaymentMethodModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class PaymentMethodCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.PaymentMethodBaseCmsInfo
    {

		public PaymentMethodCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentMethodBase item)
            : base(item)
        {

        }
        

        private PaymentMethodFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = PaymentMethodFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new PaymentMethodFrontend FrontendItem
        {
            get { return (PaymentMethodFrontend) base.FrontendItem; }
        }
        
        
		public new PaymentMethod DbItem
        {
            get
            {
                return (PaymentMethod)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]

        public CmsCollectionInfo PaymentMethod_CultureInfos { get; protected set; }
        



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.PaymentMethod = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentMethodModule.PaymentMethod>.GetPropertyBySelector( item => item.PaymentMethod),
        						isEditable: true, isVisible: true);

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentMethodModule.PaymentMethod>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentMethodModule.PaymentMethod>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

        this.ImageFilename = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentMethodModule.PaymentMethod>.GetPropertyBySelector( item => item.ImageFilename),
        						isEditable: false, isVisible: false);

	

// [usercmsinfo_initcollections]

		
		//InitCollectionUserOneToMany
		this.PaymentMethod_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentMethodModule.PaymentMethod>.GetPropertyBySelector(item => item.PaymentMethod_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfo>.GetPropertyBySelector(item => item.PaymentMethod)));


			base.initBasicFields();
		}
    
    }
}
