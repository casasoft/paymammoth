using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.MemberWallTicketModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IMemberWallTicketFactoryAutoGen : PayMammoth.Modules._AutoGen.IMemberWallTicketBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IMemberWallTicket>
    
    {
	    new PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket CreateNewItem();
        new PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
