using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.WebsiteAccountModule;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.WebsiteAccountModule.WebsiteAccountFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.WebsiteAccountModule.WebsiteAccountFrontend ToFrontend(this PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount item)
        {
        	return PayMammoth.Frontend.WebsiteAccountModule.WebsiteAccountFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class WebsiteAccountFrontend_AutoGen : PayMammoth.Modules._AutoGen.WebsiteAccountModule.WebsiteAccountBaseFrontend

    {
		
        
        protected WebsiteAccountFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount Data
        {
            get { return (PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount)base.Data; }
            set { base.Data = value; }

        }

        public new static WebsiteAccountFrontend CreateNewItem()
        {
            return (WebsiteAccountFrontend)PayMammoth.Modules._AutoGen.WebsiteAccountModule.WebsiteAccountBaseFrontend.CreateNewItem();
        }
        
        public new static WebsiteAccountFrontend Get(PayMammoth.Modules._AutoGen.IWebsiteAccountBase data)
        {
            return (WebsiteAccountFrontend)PayMammoth.Modules._AutoGen.WebsiteAccountModule.WebsiteAccountBaseFrontend.Get(data);
        }
        public new static List<WebsiteAccountFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IWebsiteAccountBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.WebsiteAccountModule.WebsiteAccountBaseFrontend.GetList(dataList).Cast<WebsiteAccountFrontend>().ToList();
        }


    }
}
