using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace PayMammoth.Modules._AutoGen
{
	public abstract class PaymentRequestItemDetailBaseCmsFactory_AutoGen : CmsFactoryBase<PaymentRequestItemDetailBaseCmsInfo, PaymentRequestItemDetailBase>
    {
       
       public new static PaymentRequestItemDetailBaseCmsFactory Instance
	    {
	         get
	         {
                 return (PaymentRequestItemDetailBaseCmsFactory)CmsFactoryBase<PaymentRequestItemDetailBaseCmsInfo, PaymentRequestItemDetailBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = PaymentRequestItemDetailBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "PaymentRequestItemDetail.aspx";

           // this.listingPageName = "default.aspx";

            if (string.IsNullOrWhiteSpace(cmsInfo.Name )) cmsInfo.Name = "PaymentRequestItemDetail";

            if (string.IsNullOrWhiteSpace(this.QueryStringParamID )) this.QueryStringParamID = "PaymentRequestItemDetailId";

            if (string.IsNullOrWhiteSpace(cmsInfo.TitlePlural)) cmsInfo.TitlePlural = "Payment Request Item Details";

            if (string.IsNullOrWhiteSpace(cmsInfo.TitleSingular )) cmsInfo.TitleSingular =  "Payment Request Item Detail";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public PaymentRequestItemDetailBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "PaymentRequestItemDetail/";
			UsedInProject = PaymentRequestItemDetailBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
