using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class RecurringProfile_AutoGen : RecurringProfileBase, PayMammoth.Modules._AutoGen.IRecurringProfileAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount WebsiteAccount
        {
            get
            {
                return (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount)base.WebsiteAccount;
            }
            set
            {
                base.WebsiteAccount = value;
            }
        }

		PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount IRecurringProfileAutoGen.WebsiteAccount
        {
            get
            {
            	return this.WebsiteAccount;
                
            }
            set
            {
                this.WebsiteAccount = (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new RecurringProfileFactory Factory
        {
            get
            {
                return (RecurringProfileFactory)RecurringProfileBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<RecurringProfile, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        //UserClassOnly_CollectionOneToManyLeftSide
        
        
        public class __CreatedByPaymentRequestsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.RecurringProfileModule.RecurringProfile, 
        	PayMammoth.Modules.PaymentRequestModule.PaymentRequest,
        	PayMammoth.Modules.PaymentRequestModule.IPaymentRequest>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.RecurringProfileModule.RecurringProfile, PayMammoth.Modules.PaymentRequestModule.PaymentRequest>
        {
            private PayMammoth.Modules.RecurringProfileModule.RecurringProfile _item = null;
            public __CreatedByPaymentRequestsCollectionInfo(PayMammoth.Modules.RecurringProfileModule.RecurringProfile item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>)_item.__collection__CreatedByPaymentRequests; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.RecurringProfileModule.RecurringProfile, PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.SetLinkOnItem(PayMammoth.Modules.PaymentRequestModule.PaymentRequest item, PayMammoth.Modules.RecurringProfileModule.RecurringProfile value)
            {
                item.RecurringProfileLink = value;
            }
        }
        
        private __CreatedByPaymentRequestsCollectionInfo _CreatedByPaymentRequests = null;
        public __CreatedByPaymentRequestsCollectionInfo CreatedByPaymentRequests
        {
            get
            {
                if (_CreatedByPaymentRequests == null)
                    _CreatedByPaymentRequests = new __CreatedByPaymentRequestsCollectionInfo((PayMammoth.Modules.RecurringProfileModule.RecurringProfile)this);
                return _CreatedByPaymentRequests;
            }
        }

   
		ICollectionManager<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> IRecurringProfileAutoGen.CreatedByPaymentRequests
        {
            get {  return this.CreatedByPaymentRequests; }
        }
   
        protected virtual IEnumerable<PayMammoth.Modules._AutoGen.PaymentRequestBase> __collection__CreatedByPaymentRequests
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }
        //UserClassOnly_CollectionOneToManyLeftSide
        
        
        public class __PaymentsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.RecurringProfileModule.RecurringProfile, 
        	PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment,
        	PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.RecurringProfileModule.RecurringProfile, PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>
        {
            private PayMammoth.Modules.RecurringProfileModule.RecurringProfile _item = null;
            public __PaymentsCollectionInfo(PayMammoth.Modules.RecurringProfileModule.RecurringProfile item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>)_item.__collection__Payments; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.RecurringProfileModule.RecurringProfile, PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.SetLinkOnItem(PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment item, PayMammoth.Modules.RecurringProfileModule.RecurringProfile value)
            {
                item.RecurringProfile = value;
            }
        }
        
        private __PaymentsCollectionInfo _Payments = null;
        public __PaymentsCollectionInfo Payments
        {
            get
            {
                if (_Payments == null)
                    _Payments = new __PaymentsCollectionInfo((PayMammoth.Modules.RecurringProfileModule.RecurringProfile)this);
                return _Payments;
            }
        }

   
		ICollectionManager<PayMammoth.Modules.RecurringProfilePaymentModule.IRecurringProfilePayment> IRecurringProfileAutoGen.Payments
        {
            get {  return this.Payments; }
        }
   
        protected virtual IEnumerable<PayMammoth.Modules._AutoGen.RecurringProfilePaymentBase> __collection__Payments
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }
        
        
        
    }
    
}
