using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.BrandModule;
using PayMammoth.Modules.BrandModule;

//IUserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IBrandAutoGen : BusinessLogic_v3.Modules.BrandModule.IBrandBase
    {




// [interface_userclass_properties]

   
    

// [interface_userclassonly_properties]

   

                                     

// [interface_userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> Brand_CultureInfos { get; }




// [interface_userclassonly_collections]

 


    	
    	
      
      

    }
}
