using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface INotificationMessageBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE NotificationType { get; set; }
		//Iproperty_normal
        DateTime DateCreated { get; set; }
		//Iproperty_normal
        string Identifier { get; set; }
		//Iproperty_normal
        string SendToUrl { get; set; }
		//Iproperty_normal
        bool AcknowledgedByRecipient { get; set; }
		//Iproperty_normal
        int RetryCount { get; set; }
		//Iproperty_normal
        DateTime? LastRetryOn { get; set; }
		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IPaymentRequestBase LinkedPaymentRequest {get; set; }

		//Iproperty_normal
        string Message { get; set; }
		//Iproperty_normal
        PayMammoth.Connector.Enums.NOTIFICATION_STATUS_CODE StatusCode { get; set; }
		//Iproperty_normal
        DateTime? NextRetryOn { get; set; }
		//Iproperty_normal
        bool SendingFailed { get; set; }
		//Iproperty_normal
        DateTime? AcknowledgedByRecipientOn { get; set; }
		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IWebsiteAccountBase WebsiteAccount {get; set; }

		//Iproperty_normal
        string StatusLog { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
