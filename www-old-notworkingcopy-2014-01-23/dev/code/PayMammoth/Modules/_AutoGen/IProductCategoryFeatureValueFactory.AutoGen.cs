using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;

using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule;
using PayMammoth.Modules.ProductCategoryFeatureValueModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IProductCategoryFeatureValueFactoryAutoGen : BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductCategoryFeatureValue>
    
    {
	    new PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue CreateNewItem();
        new PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
