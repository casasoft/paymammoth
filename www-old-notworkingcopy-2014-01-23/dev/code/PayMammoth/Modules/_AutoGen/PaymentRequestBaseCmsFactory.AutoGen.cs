using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace PayMammoth.Modules._AutoGen
{
	public abstract class PaymentRequestBaseCmsFactory_AutoGen : CmsFactoryBase<PaymentRequestBaseCmsInfo, PaymentRequestBase>
    {
       
       public new static PaymentRequestBaseCmsFactory Instance
	    {
	         get
	         {
                 return (PaymentRequestBaseCmsFactory)CmsFactoryBase<PaymentRequestBaseCmsInfo, PaymentRequestBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = PaymentRequestBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "PaymentRequest.aspx";

           // this.listingPageName = "default.aspx";

            if (string.IsNullOrWhiteSpace(cmsInfo.Name )) cmsInfo.Name = "PaymentRequest";

            if (string.IsNullOrWhiteSpace(this.QueryStringParamID )) this.QueryStringParamID = "PaymentRequestId";

            if (string.IsNullOrWhiteSpace(cmsInfo.TitlePlural)) cmsInfo.TitlePlural = "Payment Requests";

            if (string.IsNullOrWhiteSpace(cmsInfo.TitleSingular )) cmsInfo.TitleSingular =  "Payment Request";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public PaymentRequestBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "PaymentRequest/";
			UsedInProject = PaymentRequestBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
