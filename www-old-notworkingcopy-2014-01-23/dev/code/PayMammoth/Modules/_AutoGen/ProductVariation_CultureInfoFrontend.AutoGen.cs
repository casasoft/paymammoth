using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ProductVariation_CultureInfoModule;
using PayMammoth.Modules.ProductVariation_CultureInfoModule;
using BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoFrontend ToFrontend(this PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo item)
        {
        	return PayMammoth.Frontend.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariation_CultureInfoFrontend_AutoGen : BusinessLogic_v3.Frontend.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBaseFrontend

    {
		
        
        protected ProductVariation_CultureInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo Data
        {
            get { return (PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static ProductVariation_CultureInfoFrontend CreateNewItem()
        {
            return (ProductVariation_CultureInfoFrontend)BusinessLogic_v3.Frontend.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBaseFrontend.CreateNewItem();
        }
        
        public new static ProductVariation_CultureInfoFrontend Get(BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfoBase data)
        {
            return (ProductVariation_CultureInfoFrontend)BusinessLogic_v3.Frontend.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBaseFrontend.Get(data);
        }
        public new static List<ProductVariation_CultureInfoFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfoBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBaseFrontend.GetList(dataList).Cast<ProductVariation_CultureInfoFrontend>().ToList();
        }


    }
}
