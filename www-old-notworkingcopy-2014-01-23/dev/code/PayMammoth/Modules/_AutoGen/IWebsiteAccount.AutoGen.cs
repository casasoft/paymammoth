using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.WebsiteAccountModule;

//IUserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IWebsiteAccountAutoGen : PayMammoth.Modules._AutoGen.IWebsiteAccountBase
    {




// [interface_userclass_properties]

   
    

// [interface_userclassonly_properties]

   

                                     

// [interface_userclass_collections_nhibernate]




// [interface_userclassonly_collections]

        //UserClassOnly_CollectionOneToManyLeftSide
        ICollectionManager<PayMammoth.Modules.NotificationMessageModule.INotificationMessage> NotificationMessages { get; }

        //UserClassOnly_CollectionOneToManyLeftSide
        ICollectionManager<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> PaymentRequests { get; }

        //UserClassOnly_CollectionOneToManyLeftSide
        ICollectionManager<PayMammoth.Modules.RecurringProfileModule.IRecurringProfile> RecurringProfiles { get; }

        //UserClassOnly_CollectionOneToManyLeftSide
        ICollectionManager<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo> WebsiteAccount_CultureInfos { get; }

 


    	
    	
      
      

    }
}
