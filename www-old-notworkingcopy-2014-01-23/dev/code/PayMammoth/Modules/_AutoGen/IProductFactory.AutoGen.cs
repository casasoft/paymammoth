using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;

using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductModule;
using PayMammoth.Modules.ProductModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IProductFactoryAutoGen : BusinessLogic_v3.Modules.ProductModule.IProductBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProduct>
    
    {
	    new PayMammoth.Modules.ProductModule.IProduct CreateNewItem();
        new PayMammoth.Modules.ProductModule.IProduct GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductModule.IProduct> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductModule.IProduct> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductModule.IProduct> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductModule.IProduct FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductModule.IProduct FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductModule.IProduct FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductModule.IProduct> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
