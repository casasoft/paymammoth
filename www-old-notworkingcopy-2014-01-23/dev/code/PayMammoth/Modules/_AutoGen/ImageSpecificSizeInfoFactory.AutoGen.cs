using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule;
using PayMammoth.Modules.ImageSpecificSizeInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ImageSpecificSizeInfoFactoryAutoGen : BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IImageSpecificSizeInfoFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IImageSpecificSizeInfo>
    
    {
    
     	public new ImageSpecificSizeInfo ReloadItemFromDatabase(BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase item)
        {
         	return (ImageSpecificSizeInfo)base.ReloadItemFromDatabase(item);
        }
     	static ImageSpecificSizeInfoFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public ImageSpecificSizeInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ImageSpecificSizeInfoImpl);
			PayMammoth.Cms.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoCmsFactory._initialiseStaticInstance();
        }
        public static new ImageSpecificSizeInfoFactory Instance
        {
            get
            {
                return (ImageSpecificSizeInfoFactory)ImageSpecificSizeInfoBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<ImageSpecificSizeInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ImageSpecificSizeInfo>();
            
        }
        public new IEnumerable<ImageSpecificSizeInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ImageSpecificSizeInfo>();
            
            
        }
    
        public new IEnumerable<ImageSpecificSizeInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ImageSpecificSizeInfo>();
            
        }
        public new IEnumerable<ImageSpecificSizeInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ImageSpecificSizeInfo>();
        }
        
        public new ImageSpecificSizeInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ImageSpecificSizeInfo)base.FindItem(query);
        }
        public new ImageSpecificSizeInfo FindItem(IQueryOver query)
        {
            return (ImageSpecificSizeInfo)base.FindItem(query);
        }
        
        IImageSpecificSizeInfo IImageSpecificSizeInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ImageSpecificSizeInfo CreateNewItem()
        {
            return (ImageSpecificSizeInfo)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<ImageSpecificSizeInfo, ImageSpecificSizeInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ImageSpecificSizeInfo>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo> Members
     
        PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo IBaseDbFactory<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo IBaseDbFactory<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo> IBaseDbFactory<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo IBaseDbFactory<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo> IBaseDbFactory<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IImageSpecificSizeInfo> IImageSpecificSizeInfoFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IImageSpecificSizeInfo> IImageSpecificSizeInfoFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IImageSpecificSizeInfo> IImageSpecificSizeInfoFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IImageSpecificSizeInfo IImageSpecificSizeInfoFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IImageSpecificSizeInfo IImageSpecificSizeInfoFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IImageSpecificSizeInfo IImageSpecificSizeInfoFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IImageSpecificSizeInfo> IImageSpecificSizeInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IImageSpecificSizeInfo IImageSpecificSizeInfoFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
