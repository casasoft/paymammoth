using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.AuditLogModule;
using BusinessLogic_v3.Modules.AuditLogModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class AuditLog_AutoGen : AuditLogBase, PayMammoth.Modules._AutoGen.IAuditLogAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.CmsUserModule.CmsUser CmsUser
        {
            get
            {
                return (PayMammoth.Modules.CmsUserModule.CmsUser)base.CmsUser;
            }
            set
            {
                base.CmsUser = value;
            }
        }

		PayMammoth.Modules.CmsUserModule.ICmsUser IAuditLogAutoGen.CmsUser
        {
            get
            {
            	return this.CmsUser;
                
            }
            set
            {
                this.CmsUser = (PayMammoth.Modules.CmsUserModule.CmsUser)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new AuditLogFactory Factory
        {
            get
            {
                return (AuditLogFactory)AuditLogBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<AuditLog, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
