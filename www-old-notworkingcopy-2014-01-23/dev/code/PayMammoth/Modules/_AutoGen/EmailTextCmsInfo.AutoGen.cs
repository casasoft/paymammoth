using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.EmailTextModule;
using CS.WebComponentsGeneralV3.Cms.EmailTextModule;
using PayMammoth.Frontend.EmailTextModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class EmailTextCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.EmailTextModule.EmailTextBaseCmsInfo
    {

		public EmailTextCmsInfo_AutoGen(BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase item)
            : base(item)
        {

        }
        

        private EmailTextFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = EmailTextFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new EmailTextFrontend FrontendItem
        {
            get { return (EmailTextFrontend) base.FrontendItem; }
        }
        
        
		public new EmailText DbItem
        {
            get
            {
                return (EmailText)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Parent = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.Parent),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.ChildEmails)));

        
        this.Subject = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.Subject),
        						isEditable: true, isVisible: true);

        this.Subject_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.Subject_Search),
        						isEditable: false, isVisible: false);

        this.Body = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.Body),
        						isEditable: true, isVisible: true);

        this.Body_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.Body_Search),
        						isEditable: false, isVisible: false);

        this.Remarks = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.Remarks),
        						isEditable: true, isVisible: true);

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

        this.VisibleInCMS_AccessRequired = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.VisibleInCMS_AccessRequired),
        						isEditable: true, isVisible: true);

        this.UsedInProject = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.UsedInProject),
        						isEditable: true, isVisible: true);

        this.CustomContentTags = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.CustomContentTags),
        						isEditable: true, isVisible: true);

        this.WhenIsThisEmailSent = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.WhenIsThisEmailSent),
        						isEditable: true, isVisible: true);

        this.NotifyAdminAboutEmail = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.NotifyAdminAboutEmail),
        						isEditable: true, isVisible: true);

        this.NotifyAdminCustomEmail = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.NotifyAdminCustomEmail),
        						isEditable: true, isVisible: true);

        this.ProcessContentUsingNVelocity = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.ProcessContentUsingNVelocity),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
