using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Extensions;
using PayMammoth.Modules.WallModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public class WallCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.WallBaseCmsInfo
    {

		public WallCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.WallBase item)
            : base(item)
        {

        }
		public new Wall DbItem
        {
            get
            {
                return (Wall)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]

        public CmsCollectionInfo PurchasedTickets { get; private set; }
        

        public CmsCollectionInfo WallPrizes { get; private set; }
        



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

	

// [usercmsinfo_initcollections]

		
		//InitCollectionUserOneToMany
		this.PurchasedTickets = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WallModule.Wall>.GetPropertyBySelector(item => item.PurchasedTickets),
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket>.GetPropertyBySelector(item => item.Wall)));
		
		//InitCollectionUserOneToMany
		this.WallPrizes = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WallModule.Wall>.GetPropertyBySelector(item => item.WallPrizes),
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink>.GetPropertyBySelector(item => item.Wall)));


			base.initBasicFields();
		}
    
    }
}
