using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Extensions;
using PayMammoth.Modules.MemberResponsibleLimitsModule;
using BusinessLogic_v3.Cms.MemberResponsibleLimitsModule;
using PayMammoth.Frontend.MemberResponsibleLimitsModule;

namespace PayMammoth.Modules._AutoGen
{
    public class MemberResponsibleLimitsCmsInfo_AutoGen : BusinessLogic_v3.Cms.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseCmsInfo
    {

		public MemberResponsibleLimitsCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase item)
            : base(item)
        {

        }
        

        private MemberResponsibleLimitsFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = MemberResponsibleLimitsFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new MemberResponsibleLimitsFrontend FrontendItem
        {
            get { return (MemberResponsibleLimitsFrontend) base.FrontendItem; }
        }
        
        
		public new MemberResponsibleLimits DbItem
        {
            get
            {
                return (MemberResponsibleLimits)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.Member = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits>.GetPropertyBySelector( item => item.Member),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.ResponsibleLimits)));

        
        this.StartDate = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits>.GetPropertyBySelector( item => item.StartDate),
        						isEditable: true, isVisible: true);

        this.Value = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits>.GetPropertyBySelector( item => item.Value),
        						isEditable: true, isVisible: true);

        this.FrequencyType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimits>.GetPropertyBySelector( item => item.FrequencyType),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
