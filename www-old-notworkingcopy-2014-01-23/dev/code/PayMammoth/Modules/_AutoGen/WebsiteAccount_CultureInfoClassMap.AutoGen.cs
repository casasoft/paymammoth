using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.WebsiteAccount_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class WebsiteAccount_CultureInfoMap_AutoGen : WebsiteAccount_CultureInfoMap_AutoGen_Z
    {
        public WebsiteAccount_CultureInfoMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.CultureInfo, CultureInfoMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.WebsiteAccount, WebsiteAccountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayRealexDescription, PaymentGatewayRealexDescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayApcoDescription, PaymentGatewayApcoDescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayTransactiumDescription, PaymentGatewayTransactiumDescriptionMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class WebsiteAccount_CultureInfoMap_AutoGen_Z : PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBaseMap<WebsiteAccount_CultureInfoImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void WebsiteAccountMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "WebsiteAccountId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

            
        
        
    }
}
