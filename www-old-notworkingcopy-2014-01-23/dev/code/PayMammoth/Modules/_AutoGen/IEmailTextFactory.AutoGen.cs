using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.EmailTextModule;
using PayMammoth.Modules.EmailTextModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IEmailTextFactoryAutoGen : BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IEmailText>
    
    {
	    new PayMammoth.Modules.EmailTextModule.IEmailText CreateNewItem();
        new PayMammoth.Modules.EmailTextModule.IEmailText GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.EmailTextModule.IEmailText> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.EmailTextModule.IEmailText> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.EmailTextModule.IEmailText> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.EmailTextModule.IEmailText FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.EmailTextModule.IEmailText FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.EmailTextModule.IEmailText FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.EmailTextModule.IEmailText> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
