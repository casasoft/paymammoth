using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.EmailLogModule;
using CS.WebComponentsGeneralV3.Cms.EmailLogModule;
using PayMammoth.Frontend.EmailLogModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class EmailLogCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.EmailLogModule.EmailLogBaseCmsInfo
    {

		public EmailLogCmsInfo_AutoGen(BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase item)
            : base(item)
        {

        }
        

        private EmailLogFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = EmailLogFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new EmailLogFrontend FrontendItem
        {
            get { return (EmailLogFrontend) base.FrontendItem; }
        }
        
        
		public new EmailLog DbItem
        {
            get
            {
                return (EmailLog)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.DateTime = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.DateTime),
        						isEditable: true, isVisible: true);

        this.FromEmail = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.FromEmail),
        						isEditable: true, isVisible: true);

        this.FromName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.FromName),
        						isEditable: true, isVisible: true);

        this.ToEmail = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.ToEmail),
        						isEditable: true, isVisible: true);

        this.ToName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.ToName),
        						isEditable: true, isVisible: true);

        this.ReplyToEmail = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.ReplyToEmail),
        						isEditable: true, isVisible: true);

        this.ReplyToName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.ReplyToName),
        						isEditable: true, isVisible: true);

        this.Success = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.Success),
        						isEditable: true, isVisible: true);

        this.Subject = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.Subject),
        						isEditable: true, isVisible: true);

        this.PlainText = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.PlainText),
        						isEditable: true, isVisible: true);

        this.HtmlText = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.HtmlText),
        						isEditable: true, isVisible: true);

        this.Attachements = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.Attachements),
        						isEditable: true, isVisible: true);

        this.Resources = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.Resources),
        						isEditable: true, isVisible: true);

        this.Host = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.Host),
        						isEditable: true, isVisible: true);

        this.Port = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.Port),
        						isEditable: true, isVisible: true);

        this.User = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.User),
        						isEditable: true, isVisible: true);

        this.Pass = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.Pass),
        						isEditable: true, isVisible: true);

        this.UsesSecureConnection = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.UsesSecureConnection),
        						isEditable: true, isVisible: true);

        this.ResultDetails = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailLogModule.EmailLog>.GetPropertyBySelector( item => item.ResultDetails),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
