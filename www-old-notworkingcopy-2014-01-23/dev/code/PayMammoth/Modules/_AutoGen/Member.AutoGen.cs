using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.MemberModule;
using BusinessLogic_v3.Modules.MemberModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Member_AutoGen : MemberBase, PayMammoth.Modules._AutoGen.IMemberAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CultureDetailsModule.CultureDetails PreferredCultureInfo
        {
            get
            {
                return (PayMammoth.Modules.CultureDetailsModule.CultureDetails)base.PreferredCultureInfo;
            }
            set
            {
                base.PreferredCultureInfo = value;
            }
        }

		PayMammoth.Modules.CultureDetailsModule.ICultureDetails IMemberAutoGen.PreferredCultureInfo
        {
            get
            {
            	return this.PreferredCultureInfo;
                
            }
            set
            {
                this.PreferredCultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.MemberModule.Member ReferredByMember
        {
            get
            {
                return (PayMammoth.Modules.MemberModule.Member)base.ReferredByMember;
            }
            set
            {
                base.ReferredByMember = value;
            }
        }

		PayMammoth.Modules.MemberModule.IMember IMemberAutoGen.ReferredByMember
        {
            get
            {
            	return this.ReferredByMember;
                
            }
            set
            {
                this.ReferredByMember = (PayMammoth.Modules.MemberModule.Member)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.OrderModule.Order CurrentShoppingCart
        {
            get
            {
                return (PayMammoth.Modules.OrderModule.Order)base.CurrentShoppingCart;
            }
            set
            {
                base.CurrentShoppingCart = value;
            }
        }

		PayMammoth.Modules.OrderModule.IOrder IMemberAutoGen.CurrentShoppingCart
        {
            get
            {
            	return this.CurrentShoppingCart;
                
            }
            set
            {
                this.CurrentShoppingCart = (PayMammoth.Modules.OrderModule.Order)value;
            }
        }



// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ContactFormsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.MemberModule.Member, 
        	PayMammoth.Modules.ContactFormModule.ContactForm,
        	PayMammoth.Modules.ContactFormModule.IContactForm>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.MemberModule.Member, PayMammoth.Modules.ContactFormModule.ContactForm>
        {
            private PayMammoth.Modules.MemberModule.Member _item = null;
            public __ContactFormsCollectionInfo(PayMammoth.Modules.MemberModule.Member item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.ContactFormModule.ContactForm> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.ContactFormModule.ContactForm>)_item.__collection__ContactForms; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.MemberModule.Member, PayMammoth.Modules.ContactFormModule.ContactForm>.SetLinkOnItem(PayMammoth.Modules.ContactFormModule.ContactForm item, PayMammoth.Modules.MemberModule.Member value)
            {
                item.SentByMember = value;
            }
        }
        
        private __ContactFormsCollectionInfo _ContactForms = null;
        internal new __ContactFormsCollectionInfo ContactForms
        {
            get
            {
                if (_ContactForms == null)
                    _ContactForms = new __ContactFormsCollectionInfo((PayMammoth.Modules.MemberModule.Member)this);
                return _ContactForms;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.ContactFormModule.IContactForm> IMemberAutoGen.ContactForms
        {
            get {  return this.ContactForms; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __MembersCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.MemberModule.Member, 
        	PayMammoth.Modules.MemberModule.Member,
        	PayMammoth.Modules.MemberModule.IMember>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.MemberModule.Member, PayMammoth.Modules.MemberModule.Member>
        {
            private PayMammoth.Modules.MemberModule.Member _item = null;
            public __MembersCollectionInfo(PayMammoth.Modules.MemberModule.Member item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.MemberModule.Member> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.MemberModule.Member>)_item.__collection__Members; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.MemberModule.Member, PayMammoth.Modules.MemberModule.Member>.SetLinkOnItem(PayMammoth.Modules.MemberModule.Member item, PayMammoth.Modules.MemberModule.Member value)
            {
                item.ReferredByMember = value;
            }
        }
        
        private __MembersCollectionInfo _Members = null;
        internal new __MembersCollectionInfo Members
        {
            get
            {
                if (_Members == null)
                    _Members = new __MembersCollectionInfo((PayMammoth.Modules.MemberModule.Member)this);
                return _Members;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.MemberModule.IMember> IMemberAutoGen.Members
        {
            get {  return this.Members; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __AccountBalanceHistoryItemsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.MemberModule.Member, 
        	PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory,
        	PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.MemberModule.Member, PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>
        {
            private PayMammoth.Modules.MemberModule.Member _item = null;
            public __AccountBalanceHistoryItemsCollectionInfo(PayMammoth.Modules.MemberModule.Member item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>)_item.__collection__AccountBalanceHistoryItems; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.MemberModule.Member, PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>.SetLinkOnItem(PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory item, PayMammoth.Modules.MemberModule.Member value)
            {
                item.Member = value;
            }
        }
        
        private __AccountBalanceHistoryItemsCollectionInfo _AccountBalanceHistoryItems = null;
        internal new __AccountBalanceHistoryItemsCollectionInfo AccountBalanceHistoryItems
        {
            get
            {
                if (_AccountBalanceHistoryItems == null)
                    _AccountBalanceHistoryItems = new __AccountBalanceHistoryItemsCollectionInfo((PayMammoth.Modules.MemberModule.Member)this);
                return _AccountBalanceHistoryItems;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> IMemberAutoGen.AccountBalanceHistoryItems
        {
            get {  return this.AccountBalanceHistoryItems; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __OrdersCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.MemberModule.Member, 
        	PayMammoth.Modules.OrderModule.Order,
        	PayMammoth.Modules.OrderModule.IOrder>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.MemberModule.Member, PayMammoth.Modules.OrderModule.Order>
        {
            private PayMammoth.Modules.MemberModule.Member _item = null;
            public __OrdersCollectionInfo(PayMammoth.Modules.MemberModule.Member item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.OrderModule.Order> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.OrderModule.Order>)_item.__collection__Orders; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.MemberModule.Member, PayMammoth.Modules.OrderModule.Order>.SetLinkOnItem(PayMammoth.Modules.OrderModule.Order item, PayMammoth.Modules.MemberModule.Member value)
            {
                item.Member = value;
            }
        }
        
        private __OrdersCollectionInfo _Orders = null;
        internal new __OrdersCollectionInfo Orders
        {
            get
            {
                if (_Orders == null)
                    _Orders = new __OrdersCollectionInfo((PayMammoth.Modules.MemberModule.Member)this);
                return _Orders;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.OrderModule.IOrder> IMemberAutoGen.Orders
        {
            get {  return this.Orders; }
        }
           
        //UserClassClass_CollectionManyToManyLeftSide
        
		internal class __SubscribedCategoriesCollectionInfo : DbCollectionManyToManyManager<
			PayMammoth.Modules.MemberModule.Member, 
			PayMammoth.Modules.CategoryModule.Category,
			PayMammoth.Modules.CategoryModule.ICategory>,
            IDbCollectionManyToManyInfo<PayMammoth.Modules.MemberModule.Member, PayMammoth.Modules.CategoryModule.Category>
        {
            private PayMammoth.Modules.MemberModule.Member _item = null;
            public __SubscribedCategoriesCollectionInfo(PayMammoth.Modules.MemberModule.Member item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<PayMammoth.Modules.CategoryModule.Category> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.CategoryModule.Category>)_item.__collection__SubscribedCategories; }
            }

            #region IDbCollectionManyToManyInfo<PayMammoth.Modules.MemberModule.Member,PayMammoth.Modules.CategoryModule.Category> Members

            public ICollectionManager<PayMammoth.Modules.MemberModule.Member> GetLinkedCollectionForItem(PayMammoth.Modules.CategoryModule.Category item)
            {
                return item.SubscribedMembers;
                
            }


            #endregion
        }

        private __SubscribedCategoriesCollectionInfo _SubscribedCategories = null;
        internal new __SubscribedCategoriesCollectionInfo SubscribedCategories
        {
            get
            {
                if (_SubscribedCategories == null)
                    _SubscribedCategories = new __SubscribedCategoriesCollectionInfo((PayMammoth.Modules.MemberModule.Member)this);
                return _SubscribedCategories;
            }
        }
           
        ICollectionManager<PayMammoth.Modules.CategoryModule.ICategory> IMemberAutoGen.SubscribedCategories
        {
            get {  return this.SubscribedCategories; }
        }
           
        


        
          
		public  static new MemberFactory Factory
        {
            get
            {
                return (MemberFactory)MemberBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Member, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
