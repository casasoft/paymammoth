using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules._AutoGen.PaymentRequestItemDetailModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Modules._AutoGen.PaymentRequestItemDetailModule.PaymentRequestItemDetailBaseFrontend> ToFrontendBaseList(this IEnumerable<PayMammoth.Modules._AutoGen.IPaymentRequestItemDetailBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static PayMammoth.Modules._AutoGen.PaymentRequestItemDetailModule.PaymentRequestItemDetailBaseFrontend ToFrontendBase(this PayMammoth.Modules._AutoGen.IPaymentRequestItemDetailBase item)
        {
        	return PayMammoth.Modules._AutoGen.PaymentRequestItemDetailModule.PaymentRequestItemDetailBaseFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestItemDetailBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<PaymentRequestItemDetailBaseFrontend, PayMammoth.Modules._AutoGen.IPaymentRequestItemDetailBase>

    {
		
        
        protected PaymentRequestItemDetailBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
