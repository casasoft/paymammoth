using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.RecurringProfileTransactionModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.RecurringProfileTransactionModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class RecurringProfileTransactionCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfileTransactionBaseCmsInfo
    {

		public RecurringProfileTransactionCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.RecurringProfileTransactionBase item)
            : base(item)
        {

        }
        

        private RecurringProfileTransactionFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = RecurringProfileTransactionFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new RecurringProfileTransactionFrontend FrontendItem
        {
            get { return (RecurringProfileTransactionFrontend) base.FrontendItem; }
        }
        
        
		public new RecurringProfileTransaction DbItem
        {
            get
            {
                return (RecurringProfileTransaction)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.RecurringProfilePayment = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.GetPropertyBySelector( item => item.RecurringProfilePayment),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector( item => item.Transactions)));

        
        this.Timestamp = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.GetPropertyBySelector( item => item.Timestamp),
        						isEditable: true, isVisible: true);

        this.ResponseContent = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.GetPropertyBySelector( item => item.ResponseContent),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayReference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.GetPropertyBySelector( item => item.PaymentGatewayReference),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.LinkedNotificationMessage = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.GetPropertyBySelector( item => item.LinkedNotificationMessage),
                null));

        
        this.TotalAmount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.GetPropertyBySelector( item => item.TotalAmount),
        						isEditable: true, isVisible: true);

        this.OtherData = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.GetPropertyBySelector( item => item.OtherData),
        						isEditable: true, isVisible: true);

        this.Status = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.GetPropertyBySelector( item => item.Status),
        						isEditable: true, isVisible: true);

        this.StatusLog = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.GetPropertyBySelector( item => item.StatusLog),
        						isEditable: true, isVisible: true);

        this.IsFakePayment = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.GetPropertyBySelector( item => item.IsFakePayment),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
