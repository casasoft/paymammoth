using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen.WebsiteAccountModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WebsiteAccountBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.WebsiteAccountBase>
    {
		public WebsiteAccountBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.WebsiteAccountBase dbItem)
            : base(PayMammoth.Modules._AutoGen.WebsiteAccountBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new WebsiteAccountBaseFrontend FrontendItem
        {
            get { return (WebsiteAccountBaseFrontend)base.FrontendItem; }

        }
        public new WebsiteAccountBase DbItem
        {
            get
            {
                return (WebsiteAccountBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo WebsiteName { get; protected set; }

        public CmsPropertyInfo Code { get; protected set; }

        public CmsPropertyInfo AllowedIPs { get; protected set; }

        public CmsPropertyInfo ResponseUrl { get; protected set; }

        public CmsPropertyInfo SecretWord { get; protected set; }

        public CmsPropertyInfo IterationsToDeriveHash { get; protected set; }

        public CmsPropertyInfo PaypalSandboxUsername { get; protected set; }

        public CmsPropertyInfo PaypalSandboxPassword { get; protected set; }

        public CmsPropertyInfo PaypalLiveUsername { get; protected set; }

        public CmsPropertyInfo PaypalLivePassword { get; protected set; }

        public CmsPropertyInfo PaypalEnabled { get; protected set; }

        public CmsPropertyInfo PaypalUseLiveEnvironment { get; protected set; }

        public CmsPropertyInfo PaypalLiveSignature { get; protected set; }

        public CmsPropertyInfo PaypalSandboxSignature { get; protected set; }

        public CmsPropertyInfo PaypalLiveMerchantId { get; protected set; }

        public CmsPropertyInfo PaypalSandboxMerchantId { get; protected set; }

        public CmsPropertyInfo PaypalLiveMerchantEmail { get; protected set; }

        public CmsPropertyInfo PaypalSandboxMerchantEmail { get; protected set; }

        public CmsPropertyInfo ChequeEnabled { get; protected set; }

        public CmsPropertyInfo MoneybookersEnabled { get; protected set; }

        public CmsPropertyInfo NetellerEnabled { get; protected set; }

        public CmsPropertyInfo EnableFakePayments { get; protected set; }

        public CmsPropertyInfo ChequeAddress1 { get; protected set; }

        public CmsPropertyInfo ChequeAddress2 { get; protected set; }

        public CmsPropertyInfo ChequeAddressCountry { get; protected set; }

        public CmsPropertyInfo ChequeAddressPostCode { get; protected set; }

        public CmsPropertyInfo ChequeAddressLocality { get; protected set; }

        public CmsPropertyInfo ChequePayableTo { get; protected set; }

        public CmsPropertyInfo PaymentGatewayTransactiumLiveUsername { get; protected set; }

        public CmsPropertyInfo PaymentGatewayTransactiumLivePassword { get; protected set; }

        public CmsPropertyInfo PaymentGatewayTransactiumStagingUsername { get; protected set; }

        public CmsPropertyInfo PaymentGatewayTransactiumStagingPassword { get; protected set; }

        public CmsPropertyInfo PaymentGatewayTransactiumUseLiveEnvironment { get; protected set; }

        public CmsPropertyInfo PaymentGatewayTransactiumProfileTag { get; protected set; }

        public CmsPropertyInfo PaymentGatewayTransactiumBankStatementText { get; protected set; }

        public CmsPropertyInfo PaymentGatewayTransactiumEnabled { get; protected set; }

        public CmsPropertyInfo PaymentGatewayApcoEnabled { get; protected set; }

        public CmsPropertyInfo PaymentGatewayApcoLiveProfileId { get; protected set; }

        public CmsPropertyInfo PaymentGatewayApcoLiveSecretWord { get; protected set; }

        public CmsPropertyInfo PaymentGatewayApcoBankStatementText { get; protected set; }

        public CmsPropertyInfo PaymentGatewayApcoStagingSecretWord { get; protected set; }

        public CmsPropertyInfo PaymentGatewayApcoStagingProfileId { get; protected set; }

        public CmsPropertyInfo PaymentGatewayApcoUseLiveEnvironment { get; protected set; }

        public CmsPropertyInfo CssFilename { get; protected set; }

        public CmsPropertyInfo LogoFilename { get; protected set; }

        public CmsPropertyInfo NotifyClientsByEmailAboutPayment { get; protected set; }

        public CmsPropertyInfo NotificationEmail { get; protected set; }

        public CmsPropertyInfo ContactEmail { get; protected set; }

        public CmsPropertyInfo ContactName { get; protected set; }

        public CmsPropertyInfo PaymentGatewayRealexEnabled { get; protected set; }

        public CmsPropertyInfo PaymentGatewayRealexUseLiveEnvironment { get; protected set; }

        public CmsPropertyInfo PaymentGatewayRealexMerchantId { get; protected set; }

        public CmsPropertyInfo PaymentGatewayRealexLiveAccountName { get; protected set; }

        public CmsPropertyInfo PaymentGatewayRealexStagingAccountName { get; protected set; }

        public CmsPropertyInfo PaymentGatewayRealexSecretWord { get; protected set; }

        public CmsPropertyInfo DisableResponseUrlNotifications { get; protected set; }

        public CmsPropertyInfo WaitUntilPaymentNotificationAcknowledgedBeforeRedirectToSuccess { get; protected set; }

        public CmsPropertyInfo PaymentGatewayTransactiumDescription { get; protected set; }

        public CmsPropertyInfo PaymentGatewayApcoDescription { get; protected set; }

        public CmsPropertyInfo PaymentGatewayRealexDescription { get; protected set; }

        public CmsPropertyInfo PaymentGatewayRealexStatementText { get; protected set; }

        public CmsPropertyInfo FakePaymentKey { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
