using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.CmsUserModule;
using BusinessLogic_v3.Modules.CmsUserModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class CmsUser_AutoGen : CmsUserBase, PayMammoth.Modules._AutoGen.ICmsUserAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]




// [userclass_collections_nhibernate]

        //UserClass_CollectionManyToManyLeftSide
        
		internal class __CmsUserRolesCollectionInfo : DbCollectionManyToManyManager<
			PayMammoth.Modules.CmsUserModule.CmsUser, 
			PayMammoth.Modules.CmsUserRoleModule.CmsUserRole,
			PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole>,
            IDbCollectionManyToManyInfo<PayMammoth.Modules.CmsUserModule.CmsUser, PayMammoth.Modules.CmsUserRoleModule.CmsUserRole>
        {
            private PayMammoth.Modules.CmsUserModule.CmsUser _item = null;
            public __CmsUserRolesCollectionInfo(PayMammoth.Modules.CmsUserModule.CmsUser item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole>)_item.__collection__CmsUserRoles; }
            }

            #region IDbCollectionManyToManyInfo<PayMammoth.Modules.CmsUserModule.CmsUser,PayMammoth.Modules.CmsUserRoleModule.CmsUserRole> Members

            public ICollectionManager<PayMammoth.Modules.CmsUserModule.CmsUser> GetLinkedCollectionForItem(PayMammoth.Modules.CmsUserRoleModule.CmsUserRole item)
            {
                return item.CmsUsers;
                
            }


            #endregion
        }

        private __CmsUserRolesCollectionInfo _CmsUserRoles = null;
        internal new __CmsUserRolesCollectionInfo CmsUserRoles
        {
            get
            {
                if (_CmsUserRoles == null)
                    _CmsUserRoles = new __CmsUserRolesCollectionInfo((PayMammoth.Modules.CmsUserModule.CmsUser)this);
                return _CmsUserRoles;
            }
        }
           
        ICollectionManager<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole> ICmsUserAutoGen.CmsUserRoles
        {
            get {  return this.CmsUserRoles; }
        }
           
        


        
          
		public  static new CmsUserFactory Factory
        {
            get
            {
                return (CmsUserFactory)CmsUserBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<CmsUser, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
