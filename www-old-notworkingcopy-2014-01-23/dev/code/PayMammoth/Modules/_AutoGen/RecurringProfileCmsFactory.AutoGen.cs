using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Cms.RecurringProfileModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfileCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfileBaseCmsFactory
    {
        public static new RecurringProfileBaseCmsFactory Instance 
        {
         	get { return (RecurringProfileBaseCmsFactory)PayMammoth.Modules._AutoGen.RecurringProfileBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override RecurringProfileBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	RecurringProfile item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = RecurringProfile.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        RecurringProfileCmsInfo cmsItem = new RecurringProfileCmsInfo(item);
            return cmsItem;
        }    
        
        public override RecurringProfileBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.RecurringProfileBase item)
        {
            return new RecurringProfileCmsInfo((RecurringProfile)item);
            
        }

    }
}
