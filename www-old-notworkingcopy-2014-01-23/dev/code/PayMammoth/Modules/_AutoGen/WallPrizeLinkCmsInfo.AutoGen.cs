using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Extensions;
using PayMammoth.Modules.WallPrizeLinkModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public class WallPrizeLinkCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.WallPrizeLinkBaseCmsInfo
    {

		public WallPrizeLinkCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.WallPrizeLinkBase item)
            : base(item)
        {

        }
		public new WallPrizeLink DbItem
        {
            get
            {
                return (WallPrizeLink)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
