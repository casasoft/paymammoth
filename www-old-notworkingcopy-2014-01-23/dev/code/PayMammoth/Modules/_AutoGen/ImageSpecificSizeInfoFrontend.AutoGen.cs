using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ImageSpecificSizeInfoModule;
using PayMammoth.Modules.ImageSpecificSizeInfoModule;
using BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoFrontend ToFrontend(this PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo item)
        {
        	return PayMammoth.Frontend.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ImageSpecificSizeInfoFrontend_AutoGen : BusinessLogic_v3.Frontend.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseFrontend

    {
		
        
        protected ImageSpecificSizeInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo Data
        {
            get { return (PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static ImageSpecificSizeInfoFrontend CreateNewItem()
        {
            return (ImageSpecificSizeInfoFrontend)BusinessLogic_v3.Frontend.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseFrontend.CreateNewItem();
        }
        
        public new static ImageSpecificSizeInfoFrontend Get(BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBase data)
        {
            return (ImageSpecificSizeInfoFrontend)BusinessLogic_v3.Frontend.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseFrontend.Get(data);
        }
        public new static List<ImageSpecificSizeInfoFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseFrontend.GetList(dataList).Cast<ImageSpecificSizeInfoFrontend>().ToList();
        }


    }
}
