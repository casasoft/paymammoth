using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen.RecurringProfileTransactionModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfileTransactionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.RecurringProfileTransactionBase>
    {
		public RecurringProfileTransactionBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.RecurringProfileTransactionBase dbItem)
            : base(PayMammoth.Modules._AutoGen.RecurringProfileTransactionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new RecurringProfileTransactionBaseFrontend FrontendItem
        {
            get { return (RecurringProfileTransactionBaseFrontend)base.FrontendItem; }

        }
        public new RecurringProfileTransactionBase DbItem
        {
            get
            {
                return (RecurringProfileTransactionBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo RecurringProfilePayment { get; protected set; }

        public CmsPropertyInfo Timestamp { get; protected set; }

        public CmsPropertyInfo ResponseContent { get; protected set; }

        public CmsPropertyInfo PaymentGatewayReference { get; protected set; }

        public CmsPropertyInfo LinkedNotificationMessage { get; protected set; }

        public CmsPropertyInfo TotalAmount { get; protected set; }

        public CmsPropertyInfo OtherData { get; protected set; }

        public CmsPropertyInfo Status { get; protected set; }

        public CmsPropertyInfo StatusLog { get; protected set; }

        public CmsPropertyInfo IsFakePayment { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
