using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using PayMammoth.Modules._AutoGen;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Modules._AutoGen
{

//BaseCmsInfo-Class

    public abstract class RecurringProfileTransactionBaseCmsInfo : PayMammoth.Modules._AutoGen.RecurringProfileTransactionBaseCmsInfo_AutoGen
    {
    	public RecurringProfileTransactionBaseCmsInfo(RecurringProfileTransactionBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
