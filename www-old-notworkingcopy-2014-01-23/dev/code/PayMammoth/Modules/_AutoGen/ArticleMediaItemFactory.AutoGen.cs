using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ArticleMediaItemModule;
using PayMammoth.Modules.ArticleMediaItemModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleMediaItemFactoryAutoGen : BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBaseFactory, 
    				PayMammoth.Modules._AutoGen.IArticleMediaItemFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<IArticleMediaItem>
    
    {
    
     	public new ArticleMediaItem ReloadItemFromDatabase(BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase item)
        {
         	return (ArticleMediaItem)base.ReloadItemFromDatabase(item);
        }
     	
        public ArticleMediaItemFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ArticleMediaItemImpl);
			PayMammoth.Cms.ArticleMediaItemModule.ArticleMediaItemCmsFactory._initialiseStaticInstance();
        }
        public static new ArticleMediaItemFactory Instance
        {
            get
            {
                return (ArticleMediaItemFactory)ArticleMediaItemBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<ArticleMediaItem> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ArticleMediaItem>();
            
        }
        public new IEnumerable<ArticleMediaItem> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ArticleMediaItem>();
            
            
        }
    
        public new IEnumerable<ArticleMediaItem> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ArticleMediaItem>();
            
        }
        public new IEnumerable<ArticleMediaItem> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ArticleMediaItem>();
        }
        
        public new ArticleMediaItem FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ArticleMediaItem)base.FindItem(query);
        }
        public new ArticleMediaItem FindItem(IQueryOver query)
        {
            return (ArticleMediaItem)base.FindItem(query);
        }
        
        IArticleMediaItem IArticleMediaItemFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ArticleMediaItem CreateNewItem()
        {
            return (ArticleMediaItem)base.CreateNewItem();
        }

        public new ArticleMediaItem GetByPrimaryKey(long pKey)
        {
            return (ArticleMediaItem)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<ArticleMediaItem, ArticleMediaItem> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ArticleMediaItem>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem> Members
     
        PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem IBaseDbFactory<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem IBaseDbFactory<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem> IBaseDbFactory<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem IBaseDbFactory<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem> IBaseDbFactory<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IArticleMediaItem> IArticleMediaItemFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticleMediaItem> IArticleMediaItemFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticleMediaItem> IArticleMediaItemFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IArticleMediaItem IArticleMediaItemFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticleMediaItem IArticleMediaItemFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticleMediaItem IArticleMediaItemFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IArticleMediaItem> IArticleMediaItemFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IArticleMediaItem IArticleMediaItemFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
