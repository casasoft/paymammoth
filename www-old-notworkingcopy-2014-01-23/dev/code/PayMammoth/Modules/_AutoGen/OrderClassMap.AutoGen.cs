using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.OrderModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class OrderMap_AutoGen : OrderMap_AutoGen_Z
    {
        public OrderMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.DateCreated, DateCreatedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Reference, ReferenceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomerFirstName, CustomerFirstNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomerEmail, CustomerEmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomerAddress1, CustomerAddress1MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomerAddress2, CustomerAddress2MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomerAddress3, CustomerAddress3MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AuthCode, AuthCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomerCountry, CustomerCountryMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomerPostCode, CustomerPostCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomerLocality, CustomerLocalityMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomerState, CustomerStateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Paid, PaidMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaidOn, PaidOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Cancelled, CancelledMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CancelledOn, CancelledOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Remarks, RemarksMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CreatedByAdmin, CreatedByAdminMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.GUID, GUIDMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ItemID, ItemIDMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ItemType, ItemTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsShoppingCart, IsShoppingCartMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomerLastName, CustomerLastNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ShippedOn, ShippedOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ShipmentTrackingCode, ShipmentTrackingCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ShipmentType, ShipmentTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ShipmentRemarks, ShipmentRemarksMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsConfirmed, IsConfirmedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ShipmentTrackingUrl, ShipmentTrackingUrlMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FixedDiscount, FixedDiscountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.OrderStatus, OrderStatusMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TotalShippingCost, TotalShippingCostMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LastUpdatedOn, LastUpdatedOnMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.OrderCurrency, OrderCurrencyMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AffiliateCommissionRate, AffiliateCommissionRateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomerMiddleName, CustomerMiddleNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PayMammothIdentifier, PayMammothIdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PendingManualPayment, PendingManualPaymentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentMethod, PaymentMethodMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PayPippaProcessCompletedOn, PayPippaProcessCompletedOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AccountBalanceDifference, AccountBalanceDifferenceMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.OrderItems, OrderItemsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class OrderMap_AutoGen_Z : BusinessLogic_v3.Modules.OrderModule.OrderBaseMap<OrderImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
