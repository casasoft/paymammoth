using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules._AutoGen.PaymentMethodModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Modules._AutoGen.PaymentMethodModule.PaymentMethodBaseFrontend> ToFrontendBaseList(this IEnumerable<PayMammoth.Modules._AutoGen.IPaymentMethodBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static PayMammoth.Modules._AutoGen.PaymentMethodModule.PaymentMethodBaseFrontend ToFrontendBase(this PayMammoth.Modules._AutoGen.IPaymentMethodBase item)
        {
        	return PayMammoth.Modules._AutoGen.PaymentMethodModule.PaymentMethodBaseFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentMethodBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<PaymentMethodBaseFrontend, PayMammoth.Modules._AutoGen.IPaymentMethodBase>

    {
		
        
        protected PaymentMethodBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
