using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.MemberWallTicketModule;
using PayMammoth.Modules._AutoGen;

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberWallTicket_AutoGen : MemberWallTicketBase, PayMammoth.Modules._AutoGen.IMemberWallTicketAutoGen
	
      
      
    {
        


    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.WallModule.Wall Wall
        {
            get
            {
                return (PayMammoth.Modules.WallModule.Wall)base.Wall;
            }
            set
            {
                base.Wall = value;
            }
        }

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.MemberModule.Member Member
        {
            get
            {
                return (PayMammoth.Modules.MemberModule.Member)base.Member;
            }
            set
            {
                base.Member = value;
            }
        }

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink WonPrize
        {
            get
            {
                return (PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink)base.WonPrize;
            }
            set
            {
                base.WonPrize = value;
            }
        }




// [userclass_collections_nhibernate]


        
          
		public  static new MemberWallTicketFactory Factory
        {
            get
            {
                return (MemberWallTicketFactory)MemberWallTicketBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<MemberWallTicket, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
