using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ProductCategoryModule;
using PayMammoth.Cms.ProductCategoryModule;
using CS.WebComponentsGeneralV3.Cms.ProductCategoryModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCategoryCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductCategoryModule.ProductCategoryBaseCmsFactory
    {
        public static new ProductCategoryBaseCmsFactory Instance 
        {
         	get { return (ProductCategoryBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ProductCategoryModule.ProductCategoryBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ProductCategoryBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = ProductCategory.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            ProductCategoryCmsInfo cmsItem = new ProductCategoryCmsInfo(item);
            return cmsItem;
        }    
        
        public override ProductCategoryBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase item)
        {
            return new ProductCategoryCmsInfo((ProductCategory)item);
            
        }

    }
}
