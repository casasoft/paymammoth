using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.BrandModule;
using PayMammoth.Modules.BrandModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IBrandFactoryAutoGen : BusinessLogic_v3.Modules.BrandModule.IBrandBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IBrand>
    
    {
	    new PayMammoth.Modules.BrandModule.IBrand CreateNewItem();
        new PayMammoth.Modules.BrandModule.IBrand GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.BrandModule.IBrand> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.BrandModule.IBrand> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.BrandModule.IBrand> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.BrandModule.IBrand FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.BrandModule.IBrand FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.BrandModule.IBrand FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.BrandModule.IBrand> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
