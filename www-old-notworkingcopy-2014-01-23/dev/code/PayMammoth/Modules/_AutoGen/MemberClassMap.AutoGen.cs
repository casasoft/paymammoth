using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.MemberModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class MemberMap_AutoGen : MemberMap_AutoGen_Z
    {
        public MemberMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.DateRegistered, DateRegisteredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Username, UsernameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Password, PasswordMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FirstName, FirstNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LastName, LastNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Gender, GenderMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DateOfBirth, DateOfBirthMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Address1, Address1MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Address2, Address2MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Address3, Address3MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Locality, LocalityMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.State, StateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PostCode, PostCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Country, CountryMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IDCard, IDCardMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LastLoggedIn, LastLoggedInMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item._LastLoggedIn_New, _LastLoggedIn_NewMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Accepted, AcceptedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Email, EmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Telephone, TelephoneMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Mobile, MobileMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Fax, FaxMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Remarks, RemarksMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Website, WebsiteMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ActivatedOn, ActivatedOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ActivatedIP, ActivatedIPMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ActivationCode, ActivationCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SessionGUID, SessionGUIDMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.PreferredCultureInfo, PreferredCultureInfoMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.ReferredByMember, ReferredByMemberMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AccountTerminated, AccountTerminatedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AccountTerminatedOn, AccountTerminatedOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AccountTerminatedIP, AccountTerminatedIPMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ForgottenPassCode, ForgottenPassCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SubscribedToNewsletter, SubscribedToNewsletterMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.CurrentShoppingCart, CurrentShoppingCartMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HowDidYouFindUs, HowDidYouFindUsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CompanyName, CompanyNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PasswordEncryptionType, PasswordEncryptionTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PasswordSalt, PasswordSaltMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PasswordIterations, PasswordIterationsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CurrentSubscriptionStartDate, CurrentSubscriptionStartDateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CurrentSubscriptionEndDate, CurrentSubscriptionEndDateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SubscriptionNotification1Sent, SubscriptionNotification1SentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SubscriptionNotification2Sent, SubscriptionNotification2SentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Paid, PaidMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Activated, ActivatedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MiddleName, MiddleNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SubscriptionExpiryNotificationSent, SubscriptionExpiryNotificationSentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsSubscriptionExpired, IsSubscriptionExpiredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ReferralSuccessful, ReferralSuccessfulMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ReferredFromUrl, ReferredFromUrlMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AccountBalance, AccountBalanceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Blocked, BlockedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SelfExcludedUntil, SelfExcludedUntilMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SelfExclusionSetOn, SelfExclusionSetOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SelfExclusionSetByIp, SelfExclusionSetByIpMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ContactForms, ContactFormsMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.Members, MembersMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.AccountBalanceHistoryItems, AccountBalanceHistoryItemsMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.Orders, OrdersMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_ManyToManyLeftInit
             addCollection(x => x.SubscribedCategories, SubscribedCategoriesMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.ManyToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class MemberMap_AutoGen_Z : BusinessLogic_v3.Modules.MemberModule.MemberBaseMap<MemberImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
