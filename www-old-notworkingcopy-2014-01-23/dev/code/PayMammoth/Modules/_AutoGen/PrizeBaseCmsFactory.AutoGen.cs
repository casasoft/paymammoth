using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace PayMammoth.Modules._AutoGen
{
	public abstract class PrizeBaseCmsFactory_AutoGen : CmsFactoryBase<PrizeBaseCmsInfo, PrizeBase>
    {
       
       public new static PrizeBaseCmsFactory Instance
	    {
	         get
	         {
                 return (PrizeBaseCmsFactory)CmsFactoryBase<PrizeBaseCmsInfo, PrizeBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = PrizeBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Prize.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Prize";

            this.QueryStringParamID = "PrizeId";

            cmsInfo.TitlePlural = "Prizes";

            cmsInfo.TitleSingular =  "Prize";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public PrizeBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Prize/";


        }
       
    }

}
