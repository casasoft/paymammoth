using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen.RecurringProfilesModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfilesBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.RecurringProfilesBase>
    {
		public RecurringProfilesBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.RecurringProfilesBase dbItem)
            : base(PayMammoth.Modules._AutoGen.RecurringProfilesBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new RecurringProfilesBaseFrontend FrontendItem
        {
            get { return (RecurringProfilesBaseFrontend)base.FrontendItem; }

        }
        public new RecurringProfilesBase DbItem
        {
            get
            {
                return (RecurringProfilesBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo PaymentTransaction { get; protected set; }

        public CmsPropertyInfo ProfileIdentifierOnGateway { get; protected set; }

        public CmsPropertyInfo PaymentGateway { get; protected set; }

        public CmsPropertyInfo PayPippaIdentifier { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
