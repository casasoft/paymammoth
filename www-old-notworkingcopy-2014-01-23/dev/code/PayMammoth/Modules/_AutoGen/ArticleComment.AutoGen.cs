using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ArticleCommentModule;
using BusinessLogic_v3.Modules.ArticleCommentModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleComment_AutoGen : ArticleCommentBase, PayMammoth.Modules._AutoGen.IArticleCommentAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ArticleModule.Article Article
        {
            get
            {
                return (PayMammoth.Modules.ArticleModule.Article)base.Article;
            }
            set
            {
                base.Article = value;
            }
        }

		PayMammoth.Modules.ArticleModule.IArticle IArticleCommentAutoGen.Article
        {
            get
            {
            	return this.Article;
                
            }
            set
            {
                this.Article = (PayMammoth.Modules.ArticleModule.Article)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new ArticleCommentFactory Factory
        {
            get
            {
                return (ArticleCommentFactory)ArticleCommentBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<ArticleComment, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
