using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.SettingModule;
using BusinessLogic_v3.Modules.SettingModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class Setting_AutoGen : SettingBase, PayMammoth.Modules._AutoGen.ISettingAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.SettingModule.Setting Parent
        {
            get
            {
                return (PayMammoth.Modules.SettingModule.Setting)base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }

		PayMammoth.Modules.SettingModule.ISetting ISettingAutoGen.Parent
        {
            get
            {
            	return this.Parent;
                
            }
            set
            {
                this.Parent = (PayMammoth.Modules.SettingModule.Setting)value;
            }
        }



// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ChildSettingsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.SettingModule.Setting, 
        	PayMammoth.Modules.SettingModule.Setting,
        	PayMammoth.Modules.SettingModule.ISetting>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.SettingModule.Setting, PayMammoth.Modules.SettingModule.Setting>
        {
            private PayMammoth.Modules.SettingModule.Setting _item = null;
            public __ChildSettingsCollectionInfo(PayMammoth.Modules.SettingModule.Setting item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.SettingModule.Setting> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.SettingModule.Setting>)_item.__collection__ChildSettings; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.SettingModule.Setting, PayMammoth.Modules.SettingModule.Setting>.SetLinkOnItem(PayMammoth.Modules.SettingModule.Setting item, PayMammoth.Modules.SettingModule.Setting value)
            {
                item.Parent = value;
            }
        }
        
        private __ChildSettingsCollectionInfo _ChildSettings = null;
        internal new __ChildSettingsCollectionInfo ChildSettings
        {
            get
            {
                if (_ChildSettings == null)
                    _ChildSettings = new __ChildSettingsCollectionInfo((PayMammoth.Modules.SettingModule.Setting)this);
                return _ChildSettings;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.SettingModule.ISetting> ISettingAutoGen.ChildSettings
        {
            get {  return this.ChildSettings; }
        }
           

        
          
		public  static new SettingFactory Factory
        {
            get
            {
                return (SettingFactory)SettingBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Setting, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
