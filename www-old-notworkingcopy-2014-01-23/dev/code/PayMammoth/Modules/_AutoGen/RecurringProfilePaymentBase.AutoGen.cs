using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace PayMammoth.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "RecurringProfilePayment")]
    public abstract class RecurringProfilePaymentBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IRecurringProfilePaymentBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static RecurringProfilePaymentBaseFactory Factory
        {
            get
            {
                return RecurringProfilePaymentBaseFactory.Instance; 
            }
        }    
        /*
        public static RecurringProfilePaymentBase CreateNewItem()
        {
            return (RecurringProfilePaymentBase)Factory.CreateNewItem();
        }

		public static RecurringProfilePaymentBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<RecurringProfilePaymentBase, RecurringProfilePaymentBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static RecurringProfilePaymentBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static RecurringProfilePaymentBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<RecurringProfilePaymentBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<RecurringProfilePaymentBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<RecurringProfilePaymentBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? RecurringProfileID
		{
		 	get 
		 	{
		 		return (this.RecurringProfile != null ? (long?)this.RecurringProfile.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.RecurringProfileBase _recurringprofile;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Modules._AutoGen.RecurringProfileBase RecurringProfile 
        {
        	get
        	{
        		return _recurringprofile;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_recurringprofile,value);
        		_recurringprofile = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IRecurringProfileBase PayMammoth.Modules._AutoGen.IRecurringProfilePaymentBaseAutoGen.RecurringProfile 
        {
            get
            {
            	return this.RecurringProfile;
            }
            set
            {
            	this.RecurringProfile = (PayMammoth.Modules._AutoGen.RecurringProfileBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private int _failurecount;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual int FailureCount 
        {
        	get
        	{
        		return _failurecount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_failurecount,value);
        		_failurecount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _nextretryon;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime? NextRetryOn 
        {
        	get
        	{
        		return _nextretryon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_nextretryon,value);
        		_nextretryon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _paymentdueon;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime PaymentDueOn 
        {
        	get
        	{
        		return _paymentdueon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentdueon,value);
        		_paymentdueon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private PayMammoth.Connector.Enums.RecurringProfilePaymentStatus _status;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Connector.Enums.RecurringProfilePaymentStatus Status 
        {
        	get
        	{
        		return _status;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_status,value);
        		_status = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _statuslog;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string StatusLog 
        {
        	get
        	{
        		return _statuslog;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_statuslog,value);
        		_statuslog = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _initiatedbypaymammoth;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual bool InitiatedByPayMammoth 
        {
        	get
        	{
        		return _initiatedbypaymammoth;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_initiatedbypaymammoth,value);
        		_initiatedbypaymammoth = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _markaspaymentfailedifnotpaidby;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime? MarkAsPaymentFailedIfNotPaidBy 
        {
        	get
        	{
        		return _markaspaymentfailedifnotpaidby;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_markaspaymentfailedifnotpaidby,value);
        		_markaspaymentfailedifnotpaidby = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
