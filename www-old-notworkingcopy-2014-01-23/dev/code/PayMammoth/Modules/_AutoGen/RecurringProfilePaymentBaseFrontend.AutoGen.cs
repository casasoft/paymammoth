using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules._AutoGen.RecurringProfilePaymentModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Modules._AutoGen.RecurringProfilePaymentModule.RecurringProfilePaymentBaseFrontend> ToFrontendBaseList(this IEnumerable<PayMammoth.Modules._AutoGen.IRecurringProfilePaymentBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static PayMammoth.Modules._AutoGen.RecurringProfilePaymentModule.RecurringProfilePaymentBaseFrontend ToFrontendBase(this PayMammoth.Modules._AutoGen.IRecurringProfilePaymentBase item)
        {
        	return PayMammoth.Modules._AutoGen.RecurringProfilePaymentModule.RecurringProfilePaymentBaseFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfilePaymentBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<RecurringProfilePaymentBaseFrontend, PayMammoth.Modules._AutoGen.IRecurringProfilePaymentBase>

    {
		
        
        protected RecurringProfilePaymentBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
