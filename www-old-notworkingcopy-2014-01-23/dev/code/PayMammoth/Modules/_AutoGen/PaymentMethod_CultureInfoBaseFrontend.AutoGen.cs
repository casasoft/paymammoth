using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<PayMammoth.Modules._AutoGen.IPaymentMethod_CultureInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoBaseFrontend ToFrontendBase(this PayMammoth.Modules._AutoGen.IPaymentMethod_CultureInfoBase item)
        {
        	return PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoBaseFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentMethod_CultureInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<PaymentMethod_CultureInfoBaseFrontend, PayMammoth.Modules._AutoGen.IPaymentMethod_CultureInfoBase>

    {
		
        
        protected PaymentMethod_CultureInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
