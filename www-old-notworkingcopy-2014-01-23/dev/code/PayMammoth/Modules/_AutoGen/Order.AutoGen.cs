using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.OrderModule;
using BusinessLogic_v3.Modules.OrderModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Order_AutoGen : OrderBase, PayMammoth.Modules._AutoGen.IOrderAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CurrencyModule.Currency OrderCurrency
        {
            get
            {
                return (PayMammoth.Modules.CurrencyModule.Currency)base.OrderCurrency;
            }
            set
            {
                base.OrderCurrency = value;
            }
        }

		PayMammoth.Modules.CurrencyModule.ICurrency IOrderAutoGen.OrderCurrency
        {
            get
            {
            	return this.OrderCurrency;
                
            }
            set
            {
                this.OrderCurrency = (PayMammoth.Modules.CurrencyModule.Currency)value;
            }
        }



// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __OrderItemsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.OrderModule.Order, 
        	PayMammoth.Modules.OrderItemModule.OrderItem,
        	PayMammoth.Modules.OrderItemModule.IOrderItem>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.OrderModule.Order, PayMammoth.Modules.OrderItemModule.OrderItem>
        {
            private PayMammoth.Modules.OrderModule.Order _item = null;
            public __OrderItemsCollectionInfo(PayMammoth.Modules.OrderModule.Order item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem>)_item.__collection__OrderItems; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.OrderModule.Order, PayMammoth.Modules.OrderItemModule.OrderItem>.SetLinkOnItem(PayMammoth.Modules.OrderItemModule.OrderItem item, PayMammoth.Modules.OrderModule.Order value)
            {
                item.Order = value;
            }
        }
        
        private __OrderItemsCollectionInfo _OrderItems = null;
        internal new __OrderItemsCollectionInfo OrderItems
        {
            get
            {
                if (_OrderItems == null)
                    _OrderItems = new __OrderItemsCollectionInfo((PayMammoth.Modules.OrderModule.Order)this);
                return _OrderItems;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.OrderItemModule.IOrderItem> IOrderAutoGen.OrderItems
        {
            get {  return this.OrderItems; }
        }
           

        
          
		public  static new OrderFactory Factory
        {
            get
            {
                return (OrderFactory)OrderBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Order, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
