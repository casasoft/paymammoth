using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.Category_CultureInfoModule;
using PayMammoth.Modules.Category_CultureInfoModule;
using BusinessLogic_v3.Modules.Category_CultureInfoModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.Category_CultureInfoModule.Category_CultureInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.Category_CultureInfoModule.Category_CultureInfoFrontend ToFrontend(this PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo item)
        {
        	return PayMammoth.Frontend.Category_CultureInfoModule.Category_CultureInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class Category_CultureInfoFrontend_AutoGen : BusinessLogic_v3.Frontend.Category_CultureInfoModule.Category_CultureInfoBaseFrontend

    {
		
        
        protected Category_CultureInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo Data
        {
            get { return (PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static Category_CultureInfoFrontend CreateNewItem()
        {
            return (Category_CultureInfoFrontend)BusinessLogic_v3.Frontend.Category_CultureInfoModule.Category_CultureInfoBaseFrontend.CreateNewItem();
        }
        
        public new static Category_CultureInfoFrontend Get(BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBase data)
        {
            return (Category_CultureInfoFrontend)BusinessLogic_v3.Frontend.Category_CultureInfoModule.Category_CultureInfoBaseFrontend.Get(data);
        }
        public new static List<Category_CultureInfoFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBase> dataList)
        {
            return BusinessLogic_v3.Frontend.Category_CultureInfoModule.Category_CultureInfoBaseFrontend.GetList(dataList).Cast<Category_CultureInfoFrontend>().ToList();
        }


    }
}
