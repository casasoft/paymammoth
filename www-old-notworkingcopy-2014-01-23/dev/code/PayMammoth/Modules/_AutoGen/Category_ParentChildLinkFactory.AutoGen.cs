using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;
using PayMammoth.Modules.Category_ParentChildLinkModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class Category_ParentChildLinkFactoryAutoGen : BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBaseFactory, 
    				PayMammoth.Modules._AutoGen.ICategory_ParentChildLinkFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<ICategory_ParentChildLink>
    
    {
    
     	public new Category_ParentChildLink ReloadItemFromDatabase(BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase item)
        {
         	return (Category_ParentChildLink)base.ReloadItemFromDatabase(item);
        }
     	
        public Category_ParentChildLinkFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(Category_ParentChildLinkImpl);
			PayMammoth.Cms.Category_ParentChildLinkModule.Category_ParentChildLinkCmsFactory._initialiseStaticInstance();
        }
        public static new Category_ParentChildLinkFactory Instance
        {
            get
            {
                return (Category_ParentChildLinkFactory)Category_ParentChildLinkBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Category_ParentChildLink> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Category_ParentChildLink>();
            
        }
        public new IEnumerable<Category_ParentChildLink> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Category_ParentChildLink>();
            
            
        }
    
        public new IEnumerable<Category_ParentChildLink> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Category_ParentChildLink>();
            
        }
        public new IEnumerable<Category_ParentChildLink> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Category_ParentChildLink>();
        }
        
        public new Category_ParentChildLink FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Category_ParentChildLink)base.FindItem(query);
        }
        public new Category_ParentChildLink FindItem(IQueryOver query)
        {
            return (Category_ParentChildLink)base.FindItem(query);
        }
        
        ICategory_ParentChildLink ICategory_ParentChildLinkFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Category_ParentChildLink CreateNewItem()
        {
            return (Category_ParentChildLink)base.CreateNewItem();
        }

        public new Category_ParentChildLink GetByPrimaryKey(long pKey)
        {
            return (Category_ParentChildLink)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Category_ParentChildLink, Category_ParentChildLink> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Category_ParentChildLink>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>) base.FindAll(session);

       }
       public new PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> Members
     
        PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink IBaseDbFactory<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink IBaseDbFactory<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> IBaseDbFactory<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink IBaseDbFactory<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> IBaseDbFactory<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ICategory_ParentChildLink> ICategory_ParentChildLinkFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICategory_ParentChildLink> ICategory_ParentChildLinkFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICategory_ParentChildLink> ICategory_ParentChildLinkFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       ICategory_ParentChildLink ICategory_ParentChildLinkFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICategory_ParentChildLink ICategory_ParentChildLinkFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICategory_ParentChildLink ICategory_ParentChildLinkFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ICategory_ParentChildLink> ICategory_ParentChildLinkFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ICategory_ParentChildLink ICategory_ParentChildLinkFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
