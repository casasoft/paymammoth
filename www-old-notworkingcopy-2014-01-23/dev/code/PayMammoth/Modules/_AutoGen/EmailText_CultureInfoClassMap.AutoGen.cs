using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.EmailText_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class EmailText_CultureInfoMap_AutoGen : EmailText_CultureInfoMap_AutoGen_Z
    {
        public EmailText_CultureInfoMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.CultureInfo, CultureInfoMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.EmailText, EmailTextMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Subject, SubjectMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Subject_Search, Subject_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Body, BodyMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Body_Search, Body_SearchMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class EmailText_CultureInfoMap_AutoGen_Z : BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBaseMap<EmailText_CultureInfoImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
