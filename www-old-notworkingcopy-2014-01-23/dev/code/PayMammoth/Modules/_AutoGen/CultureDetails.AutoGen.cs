using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.CultureDetailsModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class CultureDetails_AutoGen : CultureDetailsBase, PayMammoth.Modules._AutoGen.ICultureDetailsAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.CurrencyModule.Currency DefaultCurrency
        {
            get
            {
                return (PayMammoth.Modules.CurrencyModule.Currency)base.DefaultCurrency;
            }
            set
            {
                base.DefaultCurrency = value;
            }
        }

		PayMammoth.Modules.CurrencyModule.ICurrency ICultureDetailsAutoGen.DefaultCurrency
        {
            get
            {
            	return this.DefaultCurrency;
                
            }
            set
            {
                this.DefaultCurrency = (PayMammoth.Modules.CurrencyModule.Currency)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new CultureDetailsFactory Factory
        {
            get
            {
                return (CultureDetailsFactory)CultureDetailsBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<CultureDetails, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
