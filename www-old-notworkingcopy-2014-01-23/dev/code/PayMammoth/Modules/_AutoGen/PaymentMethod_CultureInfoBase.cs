using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Modules._AutoGen
{

//BaseClass-File
	
    public abstract class PaymentMethod_CultureInfoBase : PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBaseAutoGen, IPaymentMethod_CultureInfoBase
    {
    
		#region BaseClass-AutoGenerated
		#endregion
    
        public PaymentMethod_CultureInfoBase()
        {
            
        }    
    	protected override void initPropertiesForNewItem()
        {
        	//Fill any default values for NEW items.
        
            base.initPropertiesForNewItem();
        }
    	public override string ToString()
        {
            return base.ToString();
        }
    
    }
}
