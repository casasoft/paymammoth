using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.WebsiteAccount_CultureInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WebsiteAccount_CultureInfoFactoryAutoGen : PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IWebsiteAccount_CultureInfoFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IWebsiteAccount_CultureInfo>
    
    {
    
     	public new WebsiteAccount_CultureInfo ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase item)
        {
         	return (WebsiteAccount_CultureInfo)base.ReloadItemFromDatabase(item);
        }
     	static WebsiteAccount_CultureInfoFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public WebsiteAccount_CultureInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(WebsiteAccount_CultureInfoImpl);
			PayMammoth.Cms.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoCmsFactory._initialiseStaticInstance();
        }
        public static new WebsiteAccount_CultureInfoFactory Instance
        {
            get
            {
                return (WebsiteAccount_CultureInfoFactory)WebsiteAccount_CultureInfoBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<WebsiteAccount_CultureInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<WebsiteAccount_CultureInfo>();
            
        }
        public new IEnumerable<WebsiteAccount_CultureInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<WebsiteAccount_CultureInfo>();
            
            
        }
    
        public new IEnumerable<WebsiteAccount_CultureInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<WebsiteAccount_CultureInfo>();
            
        }
        public new IEnumerable<WebsiteAccount_CultureInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<WebsiteAccount_CultureInfo>();
        }
        
        public new WebsiteAccount_CultureInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (WebsiteAccount_CultureInfo)base.FindItem(query);
        }
        public new WebsiteAccount_CultureInfo FindItem(IQueryOver query)
        {
            return (WebsiteAccount_CultureInfo)base.FindItem(query);
        }
        
        IWebsiteAccount_CultureInfo IWebsiteAccount_CultureInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new WebsiteAccount_CultureInfo CreateNewItem()
        {
            return (WebsiteAccount_CultureInfo)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<WebsiteAccount_CultureInfo, WebsiteAccount_CultureInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<WebsiteAccount_CultureInfo>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo> Members
     
        PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo IBaseDbFactory<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo IBaseDbFactory<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo> IBaseDbFactory<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo IBaseDbFactory<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo> IBaseDbFactory<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IWebsiteAccount_CultureInfo> IWebsiteAccount_CultureInfoFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IWebsiteAccount_CultureInfo> IWebsiteAccount_CultureInfoFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IWebsiteAccount_CultureInfo> IWebsiteAccount_CultureInfoFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IWebsiteAccount_CultureInfo IWebsiteAccount_CultureInfoFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IWebsiteAccount_CultureInfo IWebsiteAccount_CultureInfoFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IWebsiteAccount_CultureInfo IWebsiteAccount_CultureInfoFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IWebsiteAccount_CultureInfo> IWebsiteAccount_CultureInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IWebsiteAccount_CultureInfo IWebsiteAccount_CultureInfoFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
