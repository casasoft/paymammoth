using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.RecurringProfilePaymentModule;

//IUserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IRecurringProfilePaymentAutoGen : PayMammoth.Modules._AutoGen.IRecurringProfilePaymentBase
    {




// [interface_userclass_properties]

		//IProperty_LinkedToObject
        new PayMammoth.Modules.RecurringProfileModule.IRecurringProfile RecurringProfile {get; set; }

   
    

// [interface_userclassonly_properties]

   

                                     

// [interface_userclass_collections_nhibernate]




// [interface_userclassonly_collections]

        //UserClassOnly_CollectionOneToManyLeftSide
        ICollectionManager<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> Transactions { get; }

 


    	
    	
      
      

    }
}
