using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IRecurringProfileTransactionBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IRecurringProfilePaymentBase RecurringProfilePayment {get; set; }

		//Iproperty_normal
        DateTime Timestamp { get; set; }
		//Iproperty_normal
        string ResponseContent { get; set; }
		//Iproperty_normal
        string PaymentGatewayReference { get; set; }
		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.INotificationMessageBase LinkedNotificationMessage {get; set; }

		//Iproperty_normal
        double TotalAmount { get; set; }
		//Iproperty_normal
        string OtherData { get; set; }
		//Iproperty_normal
        PayMammoth.Enums.RecurringProfileTransactionStatus Status { get; set; }
		//Iproperty_normal
        string StatusLog { get; set; }
		//Iproperty_normal
        bool IsFakePayment { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
