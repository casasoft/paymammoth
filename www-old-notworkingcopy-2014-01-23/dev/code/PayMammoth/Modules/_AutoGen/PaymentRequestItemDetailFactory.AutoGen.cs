using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentRequestItemDetailModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestItemDetailFactoryAutoGen : PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBaseFactory, 
    				PayMammoth.Modules._AutoGen.IPaymentRequestItemDetailFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IPaymentRequestItemDetail>
    
    {
    
     	public new PaymentRequestItemDetail ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBase item)
        {
         	return (PaymentRequestItemDetail)base.ReloadItemFromDatabase(item);
        }
     	static PaymentRequestItemDetailFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public PaymentRequestItemDetailFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(PaymentRequestItemDetailImpl);
			PayMammoth.Cms.PaymentRequestItemDetailModule.PaymentRequestItemDetailCmsFactory._initialiseStaticInstance();
        }
        public static new PaymentRequestItemDetailFactory Instance
        {
            get
            {
                return (PaymentRequestItemDetailFactory)PaymentRequestItemDetailBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<PaymentRequestItemDetail> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<PaymentRequestItemDetail>();
            
        }
        public new IEnumerable<PaymentRequestItemDetail> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<PaymentRequestItemDetail>();
            
            
        }
    
        public new IEnumerable<PaymentRequestItemDetail> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<PaymentRequestItemDetail>();
            
        }
        public new IEnumerable<PaymentRequestItemDetail> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<PaymentRequestItemDetail>();
        }
        
        public new PaymentRequestItemDetail FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (PaymentRequestItemDetail)base.FindItem(query);
        }
        public new PaymentRequestItemDetail FindItem(IQueryOver query)
        {
            return (PaymentRequestItemDetail)base.FindItem(query);
        }
        
        IPaymentRequestItemDetail IPaymentRequestItemDetailFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new PaymentRequestItemDetail CreateNewItem()
        {
            return (PaymentRequestItemDetail)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<PaymentRequestItemDetail, PaymentRequestItemDetail> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<PaymentRequestItemDetail>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail>) base.FindAll(session);

       }
       public new PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail> Members
     
        PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail> IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail> IBaseDbFactory<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IPaymentRequestItemDetail> IPaymentRequestItemDetailFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentRequestItemDetail> IPaymentRequestItemDetailFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentRequestItemDetail> IPaymentRequestItemDetailFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IPaymentRequestItemDetail IPaymentRequestItemDetailFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentRequestItemDetail IPaymentRequestItemDetailFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentRequestItemDetail IPaymentRequestItemDetailFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IPaymentRequestItemDetail> IPaymentRequestItemDetailFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IPaymentRequestItemDetail IPaymentRequestItemDetailFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
