using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.EmailText_CultureInfoModule;
using PayMammoth.Modules.EmailText_CultureInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class EmailText_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IEmailText_CultureInfoFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IEmailText_CultureInfo>
    
    {
    
     	public new EmailText_CultureInfo ReloadItemFromDatabase(BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase item)
        {
         	return (EmailText_CultureInfo)base.ReloadItemFromDatabase(item);
        }
     	static EmailText_CultureInfoFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public EmailText_CultureInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(EmailText_CultureInfoImpl);
			PayMammoth.Cms.EmailText_CultureInfoModule.EmailText_CultureInfoCmsFactory._initialiseStaticInstance();
        }
        public static new EmailText_CultureInfoFactory Instance
        {
            get
            {
                return (EmailText_CultureInfoFactory)EmailText_CultureInfoBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<EmailText_CultureInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<EmailText_CultureInfo>();
            
        }
        public new IEnumerable<EmailText_CultureInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<EmailText_CultureInfo>();
            
            
        }
    
        public new IEnumerable<EmailText_CultureInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<EmailText_CultureInfo>();
            
        }
        public new IEnumerable<EmailText_CultureInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<EmailText_CultureInfo>();
        }
        
        public new EmailText_CultureInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (EmailText_CultureInfo)base.FindItem(query);
        }
        public new EmailText_CultureInfo FindItem(IQueryOver query)
        {
            return (EmailText_CultureInfo)base.FindItem(query);
        }
        
        IEmailText_CultureInfo IEmailText_CultureInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new EmailText_CultureInfo CreateNewItem()
        {
            return (EmailText_CultureInfo)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<EmailText_CultureInfo, EmailText_CultureInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<EmailText_CultureInfo>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo> Members
     
        PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo IBaseDbFactory<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo IBaseDbFactory<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo> IBaseDbFactory<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo IBaseDbFactory<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo> IBaseDbFactory<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IEmailText_CultureInfo> IEmailText_CultureInfoFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IEmailText_CultureInfo> IEmailText_CultureInfoFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IEmailText_CultureInfo> IEmailText_CultureInfoFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IEmailText_CultureInfo IEmailText_CultureInfoFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IEmailText_CultureInfo IEmailText_CultureInfoFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IEmailText_CultureInfo IEmailText_CultureInfoFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IEmailText_CultureInfo> IEmailText_CultureInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IEmailText_CultureInfo IEmailText_CultureInfoFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
