using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.RecurringProfilesModule;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class RecurringProfiles_AutoGen : RecurringProfilesBase, PayMammoth.Modules._AutoGen.IRecurringProfilesAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction PaymentTransaction
        {
            get
            {
                return (PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction)base.PaymentTransaction;
            }
            set
            {
                base.PaymentTransaction = value;
            }
        }

		PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction IRecurringProfilesAutoGen.PaymentTransaction
        {
            get
            {
            	return this.PaymentTransaction;
                
            }
            set
            {
                this.PaymentTransaction = (PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new RecurringProfilesFactory Factory
        {
            get
            {
                return (RecurringProfilesFactory)RecurringProfilesBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<RecurringProfiles, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
