using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IRecurringProfileBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string ProfileIdentifierOnGateway { get; set; }
		//Iproperty_normal
        PayMammoth.Connector.Enums.PaymentMethodSpecific PaymentGateway { get; set; }
		//Iproperty_normal
        string Identifier { get; set; }
		//Iproperty_normal
        DateTime CreatedOn { get; set; }
		//Iproperty_normal
        int BillingCyclesCompleted2 { get; set; }
		//Iproperty_normal
        int? TotalBillingCyclesRequired { get; set; }
		//Iproperty_normal
        PayMammoth.Enums.RecurringProfileStatus Status { get; set; }
		//Iproperty_normal
        int MaximumFailedAttempts { get; set; }
		//Iproperty_normal
        DateTime? ActivatedOn { get; set; }
		//Iproperty_normal
        DateTime? CancelledOn { get; set; }
		//Iproperty_normal
        DateTime? ExpiredOn { get; set; }
		//Iproperty_normal
        int RecurringIntervalFrequency { get; set; }
		//Iproperty_normal
        PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD RecurringIntervalType { get; set; }
		//Iproperty_normal
        string StatusLog { get; set; }
		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IWebsiteAccountBase WebsiteAccount {get; set; }

		//Iproperty_normal
        string NotificationUrl { get; set; }
		//Iproperty_normal
        PayMammoth.Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE? CreationStatus { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
