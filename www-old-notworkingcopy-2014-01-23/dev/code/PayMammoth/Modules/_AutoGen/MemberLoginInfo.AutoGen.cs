using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.MemberLoginInfoModule;
using BusinessLogic_v3.Modules.MemberLoginInfoModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberLoginInfo_AutoGen : MemberLoginInfoBase, PayMammoth.Modules._AutoGen.IMemberLoginInfoAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]




// [userclass_collections_nhibernate]


        
          
		public  static new MemberLoginInfoFactory Factory
        {
            get
            {
                return (MemberLoginInfoFactory)MemberLoginInfoBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<MemberLoginInfo, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
