using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace PayMammoth.Modules._AutoGen
{
	public abstract class WallBaseCmsFactory_AutoGen : CmsFactoryBase<WallBaseCmsInfo, WallBase>
    {
       
       public new static WallBaseCmsFactory Instance
	    {
	         get
	         {
                 return (WallBaseCmsFactory)CmsFactoryBase<WallBaseCmsInfo, WallBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = WallBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Wall.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Wall";

            this.QueryStringParamID = "WallId";

            cmsInfo.TitlePlural = "Walls";

            cmsInfo.TitleSingular =  "Wall";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public WallBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Wall/";


        }
       
    }

}
