using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.WallModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IWallFactoryAutoGen : PayMammoth.Modules._AutoGen.IWallBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IWall>
    
    {
	    new PayMammoth.Modules.WallModule.IWall CreateNewItem();
        new PayMammoth.Modules.WallModule.IWall GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.WallModule.IWall> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.WallModule.IWall> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.WallModule.IWall> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.WallModule.IWall FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.WallModule.IWall FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.WallModule.IWall FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.WallModule.IWall> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
