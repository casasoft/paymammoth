using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ArticleMediaItemModule;
using PayMammoth.Cms.ArticleMediaItemModule;
using CS.WebComponentsGeneralV3.Cms.ArticleMediaItemModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleMediaItemCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ArticleMediaItemModule.ArticleMediaItemBaseCmsFactory
    {
        public static new ArticleMediaItemBaseCmsFactory Instance 
        {
         	get { return (ArticleMediaItemBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ArticleMediaItemModule.ArticleMediaItemBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ArticleMediaItemBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = ArticleMediaItem.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            ArticleMediaItemCmsInfo cmsItem = new ArticleMediaItemCmsInfo(item);
            return cmsItem;
        }    
        
        public override ArticleMediaItemBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase item)
        {
            return new ArticleMediaItemCmsInfo((ArticleMediaItem)item);
            
        }

    }
}
