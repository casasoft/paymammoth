using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.PaypalSettingsModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class PaypalSettingsMap_AutoGen : PaypalSettingsMap_AutoGen_Z
    {
        public PaypalSettingsMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.WebsiteAccount, WebsiteAccountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Enabled, EnabledMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MerchantEmail, MerchantEmailMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class PaypalSettingsMap_AutoGen_Z : PayMammoth.Modules._AutoGen.PaypalSettingsBaseMap<PaypalSettingsImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void WebsiteAccountMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "WebsiteAccountId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

            
        
        
    }
}
