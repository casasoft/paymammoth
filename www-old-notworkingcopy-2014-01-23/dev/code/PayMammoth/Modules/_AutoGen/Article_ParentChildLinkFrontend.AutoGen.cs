using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.Article_ParentChildLinkModule;
using PayMammoth.Modules.Article_ParentChildLinkModule;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.Article_ParentChildLinkModule.Article_ParentChildLinkFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.Article_ParentChildLinkModule.Article_ParentChildLinkFrontend ToFrontend(this PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink item)
        {
        	return PayMammoth.Frontend.Article_ParentChildLinkModule.Article_ParentChildLinkFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class Article_ParentChildLinkFrontend_AutoGen : BusinessLogic_v3.Frontend.Article_ParentChildLinkModule.Article_ParentChildLinkBaseFrontend

    {
		
        
        protected Article_ParentChildLinkFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink Data
        {
            get { return (PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink)base.Data; }
            set { base.Data = value; }

        }

        public new static Article_ParentChildLinkFrontend CreateNewItem()
        {
            return (Article_ParentChildLinkFrontend)BusinessLogic_v3.Frontend.Article_ParentChildLinkModule.Article_ParentChildLinkBaseFrontend.CreateNewItem();
        }
        
        public new static Article_ParentChildLinkFrontend Get(BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBase data)
        {
            return (Article_ParentChildLinkFrontend)BusinessLogic_v3.Frontend.Article_ParentChildLinkModule.Article_ParentChildLinkBaseFrontend.Get(data);
        }
        public new static List<Article_ParentChildLinkFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBase> dataList)
        {
            return BusinessLogic_v3.Frontend.Article_ParentChildLinkModule.Article_ParentChildLinkBaseFrontend.GetList(dataList).Cast<Article_ParentChildLinkFrontend>().ToList();
        }


    }
}
