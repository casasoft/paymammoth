using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Cms.WebsiteAccountModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WebsiteAccountCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.WebsiteAccountBaseCmsFactory
    {
        public static new WebsiteAccountBaseCmsFactory Instance 
        {
         	get { return (WebsiteAccountBaseCmsFactory)PayMammoth.Modules._AutoGen.WebsiteAccountBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override WebsiteAccountBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	WebsiteAccount item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = WebsiteAccount.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        WebsiteAccountCmsInfo cmsItem = new WebsiteAccountCmsInfo(item);
            return cmsItem;
        }    
        
        public override WebsiteAccountBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.WebsiteAccountBase item)
        {
            return new WebsiteAccountCmsInfo((WebsiteAccount)item);
            
        }

    }
}
