using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using NHibernate;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.PaymentMethodModule;

namespace PayMammoth.Modules._AutoGen
{

//UserClassImpl-File
	[Serializable]
    public class PaymentMethodImpl : PayMammoth.Modules.PaymentMethodModule.PaymentMethod
    {
/*
 		public new CmsUserImpl LastEditedBy
        {
            get { return (CmsUserImpl) base.LastEditedBy; }
            set { base.LastEditedBy = value; }
        }
        public new CmsUserImpl DeletedBy
        {
            get { return (CmsUserImpl)base.DeletedBy; }
            set { base.DeletedBy = value; }
        }
*/
		#region UserClass-AutoGenerated
		#endregion

      
      
        private PaymentMethodImpl()
        {
            
        }    
    	

// [userclassimpl_collections]

        //UserClassImpl_CollectionOneToManyLeftSide
        
        protected override IEnumerable<PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase> __collection__PaymentMethod_CultureInfos
        {
            get
            {
                return this._PaymentMethod_CultureInfos;
            }
        }

        private ICollection<PaymentMethod_CultureInfoImpl> _PaymentMethod_CultureInfos = new Iesi.Collections.Generic.HashedSet<PaymentMethod_CultureInfoImpl>();
        public new virtual ICollection<PaymentMethod_CultureInfoImpl> PaymentMethod_CultureInfos
        {
            get { return _PaymentMethod_CultureInfos; }

        }
    	


    	
        
    }
}




