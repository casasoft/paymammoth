using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.TicketModule;
using PayMammoth.Cms.TicketModule;
using CS.WebComponentsGeneralV3.Cms.TicketModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class TicketCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.TicketModule.TicketBaseCmsFactory
    {
        public static new TicketBaseCmsFactory Instance 
        {
         	get { return (TicketBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.TicketModule.TicketBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override TicketBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Ticket.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            TicketCmsInfo cmsItem = new TicketCmsInfo(item);
            return cmsItem;
        }    
        
        public override TicketBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.TicketModule.TicketBase item)
        {
            return new TicketCmsInfo((Ticket)item);
            
        }

    }
}
