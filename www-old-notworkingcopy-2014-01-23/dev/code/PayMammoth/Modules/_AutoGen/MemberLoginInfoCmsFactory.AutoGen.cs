using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.MemberLoginInfoModule;
using PayMammoth.Cms.MemberLoginInfoModule;
using CS.WebComponentsGeneralV3.Cms.MemberLoginInfoModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberLoginInfoCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.MemberLoginInfoModule.MemberLoginInfoBaseCmsFactory
    {
        public static new MemberLoginInfoBaseCmsFactory Instance 
        {
         	get { return (MemberLoginInfoBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.MemberLoginInfoModule.MemberLoginInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override MemberLoginInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	MemberLoginInfo item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = MemberLoginInfo.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        MemberLoginInfoCmsInfo cmsItem = new MemberLoginInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override MemberLoginInfoBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase item)
        {
            return new MemberLoginInfoCmsInfo((MemberLoginInfo)item);
            
        }

    }
}
