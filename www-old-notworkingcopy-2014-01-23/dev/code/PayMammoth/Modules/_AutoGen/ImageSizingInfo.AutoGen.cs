using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ImageSizingInfoModule;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class ImageSizingInfo_AutoGen : ImageSizingInfoBase, PayMammoth.Modules._AutoGen.IImageSizingInfoAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]




// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __SizingInfosCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo, 
        	PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo,
        	PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo, PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>
        {
            private PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo _item = null;
            public __SizingInfosCollectionInfo(PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>)_item.__collection__SizingInfos; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo, PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>.SetLinkOnItem(PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo item, PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo value)
            {
                item.ImageSizingInfo = value;
            }
        }
        
        private __SizingInfosCollectionInfo _SizingInfos = null;
        internal new __SizingInfosCollectionInfo SizingInfos
        {
            get
            {
                if (_SizingInfos == null)
                    _SizingInfos = new __SizingInfosCollectionInfo((PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo)this);
                return _SizingInfos;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo> IImageSizingInfoAutoGen.SizingInfos
        {
            get {  return this.SizingInfos; }
        }
           

        
          
		public  static new ImageSizingInfoFactory Factory
        {
            get
            {
                return (ImageSizingInfoFactory)ImageSizingInfoBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<ImageSizingInfo, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
