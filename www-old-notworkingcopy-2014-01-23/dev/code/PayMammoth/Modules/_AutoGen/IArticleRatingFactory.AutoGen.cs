using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ArticleRatingModule;
using PayMammoth.Modules.ArticleRatingModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IArticleRatingFactoryAutoGen : BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IArticleRating>
    
    {
	    new PayMammoth.Modules.ArticleRatingModule.IArticleRating CreateNewItem();
        new PayMammoth.Modules.ArticleRatingModule.IArticleRating GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleRatingModule.IArticleRating> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleRatingModule.IArticleRating> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleRatingModule.IArticleRating> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleRatingModule.IArticleRating FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleRatingModule.IArticleRating FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleRatingModule.IArticleRating FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleRatingModule.IArticleRating> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
