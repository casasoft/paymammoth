using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace PayMammoth.Modules._AutoGen
{
	public abstract class RecurringProfilesBaseCmsFactory_AutoGen : CmsFactoryBase<RecurringProfilesBaseCmsInfo, RecurringProfilesBase>
    {
       
       public new static RecurringProfilesBaseCmsFactory Instance
	    {
	         get
	         {
                 return (RecurringProfilesBaseCmsFactory)CmsFactoryBase<RecurringProfilesBaseCmsInfo, RecurringProfilesBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = RecurringProfilesBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "RecurringProfiles.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "RecurringProfiles";

            this.QueryStringParamID = "RecurringProfilesId";

            cmsInfo.TitlePlural = "Recurring Profiles";

            cmsInfo.TitleSingular =  "Recurring Profiles";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public RecurringProfilesBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "RecurringProfiles/";
			UsedInProject = RecurringProfilesBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
