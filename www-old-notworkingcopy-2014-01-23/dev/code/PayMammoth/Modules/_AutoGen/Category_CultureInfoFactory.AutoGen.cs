using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.Category_CultureInfoModule;
using PayMammoth.Modules.Category_CultureInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class Category_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.ICategory_CultureInfoFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<ICategory_CultureInfo>
    
    {
    
     	public new Category_CultureInfo ReloadItemFromDatabase(BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase item)
        {
         	return (Category_CultureInfo)base.ReloadItemFromDatabase(item);
        }
     	
        public Category_CultureInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(Category_CultureInfoImpl);
			PayMammoth.Cms.Category_CultureInfoModule.Category_CultureInfoCmsFactory._initialiseStaticInstance();
        }
        public static new Category_CultureInfoFactory Instance
        {
            get
            {
                return (Category_CultureInfoFactory)Category_CultureInfoBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Category_CultureInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Category_CultureInfo>();
            
        }
        public new IEnumerable<Category_CultureInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Category_CultureInfo>();
            
            
        }
    
        public new IEnumerable<Category_CultureInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Category_CultureInfo>();
            
        }
        public new IEnumerable<Category_CultureInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Category_CultureInfo>();
        }
        
        public new Category_CultureInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Category_CultureInfo)base.FindItem(query);
        }
        public new Category_CultureInfo FindItem(IQueryOver query)
        {
            return (Category_CultureInfo)base.FindItem(query);
        }
        
        ICategory_CultureInfo ICategory_CultureInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Category_CultureInfo CreateNewItem()
        {
            return (Category_CultureInfo)base.CreateNewItem();
        }

        public new Category_CultureInfo GetByPrimaryKey(long pKey)
        {
            return (Category_CultureInfo)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Category_CultureInfo, Category_CultureInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Category_CultureInfo>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo> Members
     
        PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo IBaseDbFactory<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo IBaseDbFactory<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo> IBaseDbFactory<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo IBaseDbFactory<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo> IBaseDbFactory<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ICategory_CultureInfo> ICategory_CultureInfoFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICategory_CultureInfo> ICategory_CultureInfoFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICategory_CultureInfo> ICategory_CultureInfoFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       ICategory_CultureInfo ICategory_CultureInfoFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICategory_CultureInfo ICategory_CultureInfoFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICategory_CultureInfo ICategory_CultureInfoFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ICategory_CultureInfo> ICategory_CultureInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ICategory_CultureInfo ICategory_CultureInfoFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
