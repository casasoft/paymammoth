using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ArticleMediaItemModule;
using CS.WebComponentsGeneralV3.Cms.ArticleMediaItemModule;
using PayMammoth.Frontend.ArticleMediaItemModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ArticleMediaItemCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ArticleMediaItemModule.ArticleMediaItemBaseCmsInfo
    {

		public ArticleMediaItemCmsInfo_AutoGen(BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase item)
            : base(item)
        {

        }
        

        private ArticleMediaItemFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ArticleMediaItemFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ArticleMediaItemFrontend FrontendItem
        {
            get { return (ArticleMediaItemFrontend) base.FrontendItem; }
        }
        
        
		public new ArticleMediaItem DbItem
        {
            get
            {
                return (ArticleMediaItem)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Caption = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem>.GetPropertyBySelector( item => item.Caption),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.ContentPage = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem>.GetPropertyBySelector( item => item.ContentPage),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ArticleMediaItems)));

        
        this.VideoDuration = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem>.GetPropertyBySelector( item => item.VideoDuration),
        						isEditable: true, isVisible: true);

        this.Filename = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleMediaItemModule.ArticleMediaItem>.GetPropertyBySelector( item => item.Filename),
        						isEditable: false, isVisible: false);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
