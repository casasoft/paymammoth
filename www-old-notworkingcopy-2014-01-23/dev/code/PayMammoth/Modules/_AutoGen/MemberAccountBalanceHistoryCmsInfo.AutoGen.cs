using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.MemberAccountBalanceHistoryModule;
using CS.WebComponentsGeneralV3.Cms.MemberAccountBalanceHistoryModule;
using PayMammoth.Frontend.MemberAccountBalanceHistoryModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class MemberAccountBalanceHistoryCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseCmsInfo
    {

		public MemberAccountBalanceHistoryCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase item)
            : base(item)
        {

        }
        

        private MemberAccountBalanceHistoryFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = MemberAccountBalanceHistoryFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new MemberAccountBalanceHistoryFrontend FrontendItem
        {
            get { return (MemberAccountBalanceHistoryFrontend) base.FrontendItem; }
        }
        
        
		public new MemberAccountBalanceHistory DbItem
        {
            get
            {
                return (MemberAccountBalanceHistory)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.Member = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>.GetPropertyBySelector( item => item.Member),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberModule.Member>.GetPropertyBySelector( item => item.AccountBalanceHistoryItems)));

        
        this.BalanceUpdate = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>.GetPropertyBySelector( item => item.BalanceUpdate),
        						isEditable: true, isVisible: true);

        this.Date = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>.GetPropertyBySelector( item => item.Date),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.OrderLink = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>.GetPropertyBySelector( item => item.OrderLink),
                null));

        
        this.UpdateType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>.GetPropertyBySelector( item => item.UpdateType),
        						isEditable: true, isVisible: true);

        this.BalanceTotal = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>.GetPropertyBySelector( item => item.BalanceTotal),
        						isEditable: true, isVisible: true);

        this.Comments = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>.GetPropertyBySelector( item => item.Comments),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
