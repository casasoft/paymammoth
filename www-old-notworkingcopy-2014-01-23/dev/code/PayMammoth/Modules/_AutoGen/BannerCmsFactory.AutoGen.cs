using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.BannerModule;
using PayMammoth.Cms.BannerModule;
using CS.WebComponentsGeneralV3.Cms.BannerModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class BannerCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.BannerModule.BannerBaseCmsFactory
    {
        public static new BannerBaseCmsFactory Instance 
        {
         	get { return (BannerBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.BannerModule.BannerBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override BannerBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Banner.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            BannerCmsInfo cmsItem = new BannerCmsInfo(item);
            return cmsItem;
        }    
        
        public override BannerBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.BannerModule.BannerBase item)
        {
            return new BannerCmsInfo((Banner)item);
            
        }

    }
}
