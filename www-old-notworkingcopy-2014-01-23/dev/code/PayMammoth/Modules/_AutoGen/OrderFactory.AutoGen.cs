using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.OrderModule;
using PayMammoth.Modules.OrderModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class OrderFactoryAutoGen : BusinessLogic_v3.Modules.OrderModule.OrderBaseFactory, 
    				PayMammoth.Modules._AutoGen.IOrderFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<IOrder>
    
    {
    
     	public new Order ReloadItemFromDatabase(BusinessLogic_v3.Modules.OrderModule.OrderBase item)
        {
         	return (Order)base.ReloadItemFromDatabase(item);
        }
     	
        public OrderFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(OrderImpl);
			PayMammoth.Cms.OrderModule.OrderCmsFactory._initialiseStaticInstance();
        }
        public static new OrderFactory Instance
        {
            get
            {
                return (OrderFactory)OrderBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Order> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Order>();
            
        }
        public new IEnumerable<Order> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Order>();
            
            
        }
    
        public new IEnumerable<Order> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Order>();
            
        }
        public new IEnumerable<Order> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Order>();
        }
        
        public new Order FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Order)base.FindItem(query);
        }
        public new Order FindItem(IQueryOver query)
        {
            return (Order)base.FindItem(query);
        }
        
        IOrder IOrderFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Order CreateNewItem()
        {
            return (Order)base.CreateNewItem();
        }

        public new Order GetByPrimaryKey(long pKey)
        {
            return (Order)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Order, Order> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Order>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.OrderModule.Order GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.OrderModule.Order) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.OrderModule.Order> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.OrderModule.Order>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.OrderModule.Order> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.OrderModule.Order>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.OrderModule.Order> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.OrderModule.Order>) base.FindAll(session);

       }
       public new PayMammoth.Modules.OrderModule.Order FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.OrderModule.Order) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.OrderModule.Order FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.OrderModule.Order) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.OrderModule.Order FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.OrderModule.Order) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.OrderModule.Order> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.OrderModule.Order>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.OrderModule.IOrder> Members
     
        PayMammoth.Modules.OrderModule.IOrder IBaseDbFactory<PayMammoth.Modules.OrderModule.IOrder>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.OrderModule.IOrder IBaseDbFactory<PayMammoth.Modules.OrderModule.IOrder>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.OrderModule.IOrder> IBaseDbFactory<PayMammoth.Modules.OrderModule.IOrder>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.OrderModule.IOrder> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.OrderModule.IOrder>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.OrderModule.IOrder> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.OrderModule.IOrder>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.OrderModule.IOrder IBaseDbFactory<PayMammoth.Modules.OrderModule.IOrder>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.OrderModule.IOrder BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.OrderModule.IOrder>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.OrderModule.IOrder BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.OrderModule.IOrder>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.OrderModule.IOrder> IBaseDbFactory<PayMammoth.Modules.OrderModule.IOrder>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IOrder> IOrderFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IOrder> IOrderFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IOrder> IOrderFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IOrder IOrderFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IOrder IOrderFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IOrder IOrderFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IOrder> IOrderFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IOrder IOrderFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
