using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.SettingModule;
using PayMammoth.Modules.SettingModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ISettingFactoryAutoGen : BusinessLogic_v3.Modules.SettingModule.ISettingBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ISetting>
    
    {
	    new PayMammoth.Modules.SettingModule.ISetting CreateNewItem();
        new PayMammoth.Modules.SettingModule.ISetting GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.SettingModule.ISetting> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.SettingModule.ISetting> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.SettingModule.ISetting> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.SettingModule.ISetting FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.SettingModule.ISetting FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.SettingModule.ISetting FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.SettingModule.ISetting> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
