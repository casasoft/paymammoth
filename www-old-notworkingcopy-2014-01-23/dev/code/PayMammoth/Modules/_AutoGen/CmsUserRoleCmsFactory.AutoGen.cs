using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.CmsUserRoleModule;
using PayMammoth.Cms.CmsUserRoleModule;
using CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class CmsUserRoleCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsFactory
    {
        public static new CmsUserRoleBaseCmsFactory Instance 
        {
         	get { return (CmsUserRoleBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override CmsUserRoleBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	CmsUserRole item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = CmsUserRole.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        CmsUserRoleCmsInfo cmsItem = new CmsUserRoleCmsInfo(item);
            return cmsItem;
        }    
        
        public override CmsUserRoleBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase item)
        {
            return new CmsUserRoleCmsInfo((CmsUserRole)item);
            
        }

    }
}
