using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using PayMammoth.Modules.TestCustomClassModule;
using PayMammoth.Cms.TestCustomClassModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class TestCustomClassCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.TestCustomClassBaseCmsFactory
    {
        public static new TestCustomClassBaseCmsFactory Instance 
        {
         	get { return (TestCustomClassBaseCmsFactory)PayMammoth.Modules._AutoGen.TestCustomClassBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override TestCustomClassBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = TestCustomClass.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            TestCustomClassCmsInfo cmsItem = new TestCustomClassCmsInfo(item);
            return cmsItem;
        }    
        
        public override TestCustomClassBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.TestCustomClassBase item)
        {
            return new TestCustomClassCmsInfo((TestCustomClass)item);
            
        }

    }
}
