using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.WallModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class WallMap_AutoGen : WallMap_AutoGen_Z
    {
        public WallMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.StartDate, StartDateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClosedOn, ClosedOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LotteryDrawDate, LotteryDrawDateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.StarsRequiredForTicket, StarsRequiredForTicketMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsFinalised, IsFinalisedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FinalisedOn, FinalisedOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LotteryHasBeenDrawn, LotteryHasBeenDrawnMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.PurchasedTickets, PurchasedTicketsMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.WallPrizes, WallPrizesMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class WallMap_AutoGen_Z : PayMammoth.Modules._AutoGen.WallBaseMap<WallImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

//BaseClassMap_Collection_OneToMany

        protected virtual void PurchasedTicketsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
                
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "WallID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
//BaseClassMap_Collection_OneToMany

        protected virtual void WallPrizesMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
                
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "WallID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
            
        
        
    }
}
