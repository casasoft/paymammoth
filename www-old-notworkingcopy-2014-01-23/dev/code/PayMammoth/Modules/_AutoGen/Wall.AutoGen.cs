using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.WallModule;
using PayMammoth.Modules._AutoGen;

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Wall_AutoGen : WallBase, PayMammoth.Modules._AutoGen.IWallAutoGen
	
      
      
    {
        


    
    
    
		
        



// [userclass_linkedproperties]




// [userclass_collections_nhibernate]


        
          
		public  static new WallFactory Factory
        {
            get
            {
                return (WallFactory)WallBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Wall, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        //UserClassOnly_CollectionOneToManyLeftSide
        
        
        public class __PurchasedTicketsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.WallModule.Wall, 
        	PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket,
        	PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.WallModule.Wall, PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket>
        {
            private PayMammoth.Modules.WallModule.Wall _item = null;
            public __PurchasedTicketsCollectionInfo(PayMammoth.Modules.WallModule.Wall item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket>)_item.__collection__PurchasedTickets; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.WallModule.Wall, PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket>.SetLinkOnItem(PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket item, PayMammoth.Modules.WallModule.Wall value)
            {
                item.Wall = value;
            }
        }
        
        private __PurchasedTicketsCollectionInfo _PurchasedTickets = null;
        public __PurchasedTicketsCollectionInfo PurchasedTickets
        {
            get
            {
                if (_PurchasedTickets == null)
                    _PurchasedTickets = new __PurchasedTicketsCollectionInfo((PayMammoth.Modules.WallModule.Wall)this);
                return _PurchasedTickets;
            }
        }

   
		ICollectionManager<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket> IWallAutoGen.PurchasedTickets
        {
            get {  return this.PurchasedTickets; }
        }
   
        protected virtual IEnumerable<PayMammoth.Modules._AutoGen.MemberWallTicketBase> __collection__PurchasedTickets
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }
        //UserClassOnly_CollectionOneToManyLeftSide
        
        
        public class __WallPrizesCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.WallModule.Wall, 
        	PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink,
        	PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.WallModule.Wall, PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink>
        {
            private PayMammoth.Modules.WallModule.Wall _item = null;
            public __WallPrizesCollectionInfo(PayMammoth.Modules.WallModule.Wall item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink>)_item.__collection__WallPrizes; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.WallModule.Wall, PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink>.SetLinkOnItem(PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink item, PayMammoth.Modules.WallModule.Wall value)
            {
                item.Wall = value;
            }
        }
        
        private __WallPrizesCollectionInfo _WallPrizes = null;
        public __WallPrizesCollectionInfo WallPrizes
        {
            get
            {
                if (_WallPrizes == null)
                    _WallPrizes = new __WallPrizesCollectionInfo((PayMammoth.Modules.WallModule.Wall)this);
                return _WallPrizes;
            }
        }

   
		ICollectionManager<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink> IWallAutoGen.WallPrizes
        {
            get {  return this.WallPrizes; }
        }
   
        protected virtual IEnumerable<PayMammoth.Modules._AutoGen.WallPrizeLinkBase> __collection__WallPrizes
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }
        
        
        
    }
    
}
