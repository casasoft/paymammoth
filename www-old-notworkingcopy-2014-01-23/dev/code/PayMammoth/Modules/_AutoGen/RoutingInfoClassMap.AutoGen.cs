using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.RoutingInfoModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class RoutingInfoMap_AutoGen : RoutingInfoMap_AutoGen_Z
    {
        public RoutingInfoMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.RouteName, RouteNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PhysicalPath, PhysicalPathMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.VirtualPath, VirtualPathMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DataTokens, DataTokensMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class RoutingInfoMap_AutoGen_Z : BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBaseMap<RoutingInfoImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
