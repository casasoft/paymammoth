using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ImageSizingInfoModule;
using PayMammoth.Cms.ImageSizingInfoModule;
using CS.WebComponentsGeneralV3.Cms.ImageSizingInfoModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ImageSizingInfoCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ImageSizingInfoModule.ImageSizingInfoBaseCmsFactory
    {
        public static new ImageSizingInfoBaseCmsFactory Instance 
        {
         	get { return (ImageSizingInfoBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ImageSizingInfoModule.ImageSizingInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ImageSizingInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	ImageSizingInfo item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = ImageSizingInfo.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        ImageSizingInfoCmsInfo cmsItem = new ImageSizingInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override ImageSizingInfoBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase item)
        {
            return new ImageSizingInfoCmsInfo((ImageSizingInfo)item);
            
        }

    }
}
