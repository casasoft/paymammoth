using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using CS.General_v3.Classes.DbObjects.Collections;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public abstract class MemberWallTicketBaseAutoGen 
    : BusinessLogic_v3.Classes.DB.BaseDbObject, IMemberWallTicketBaseAutoGen
    
      
      
      
    
    
    		
    {
    
   
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	
        public static PayMammoth.Modules._AutoGen.MemberWallTicketBaseCmsFactory CmsFactory
        {
            get { return PayMammoth.Modules._AutoGen.MemberWallTicketBaseCmsFactory.Instance; }
        }
        
     	public static MemberWallTicketBaseFactory Factory
        {
            get
            {
                return MemberWallTicketBaseFactory.Instance; 
            }
        }    
        /*
        public static MemberWallTicketBase CreateNewItem()
        {
            return (MemberWallTicketBase)Factory.CreateNewItem();
        }

		public static MemberWallTicketBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MemberWallTicketBase, MemberWallTicketBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MemberWallTicketBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MemberWallTicketBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MemberWallTicketBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MemberWallTicketBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MemberWallTicketBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? WallID
		{
		 	get 
		 	{
		 		return (this.Wall != null ? (long?)this.Wall.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.WallBase _wall;
        public virtual PayMammoth.Modules._AutoGen.WallBase Wall 
        {
        	get
        	{
        		return _wall;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_wall,value);
        		_wall = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IWallBase PayMammoth.Modules._AutoGen.IMemberWallTicketBaseAutoGen.Wall 
        {
            get
            {
            	return this.Wall;
            }
            set
            {
            	this.Wall = (PayMammoth.Modules._AutoGen.WallBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private int _ticketnumber;
        public virtual int TicketNumber 
        {
        	get
        	{
        		return _ticketnumber;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ticketnumber,value);
        		_ticketnumber = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _purchasedon;
        public virtual DateTime PurchasedOn 
        {
        	get
        	{
        		return _purchasedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_purchasedon,value);
        		_purchasedon = value;
        		
        	}
        }
        
/*
		public virtual long? MemberID
		{
		 	get 
		 	{
		 		return (this.Member != null ? (long?)this.Member.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _member;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase Member 
        {
        	get
        	{
        		return _member;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_member,value);
        		_member = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase PayMammoth.Modules._AutoGen.IMemberWallTicketBaseAutoGen.Member 
        {
            get
            {
            	return this.Member;
            }
            set
            {
            	this.Member = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
/*
		public virtual long? WonPrizeID
		{
		 	get 
		 	{
		 		return (this.WonPrize != null ? (long?)this.WonPrize.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.WallPrizeLinkBase _wonprize;
        public virtual PayMammoth.Modules._AutoGen.WallPrizeLinkBase WonPrize 
        {
        	get
        	{
        		return _wonprize;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_wonprize,value);
        		_wonprize = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IWallPrizeLinkBase PayMammoth.Modules._AutoGen.IMemberWallTicketBaseAutoGen.WonPrize 
        {
            get
            {
            	return this.WonPrize;
            }
            set
            {
            	this.WonPrize = (PayMammoth.Modules._AutoGen.WallPrizeLinkBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    
#region Multilingual
          


#endregion    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
