using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentRequestTransactionModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IPaymentRequestTransactionFactoryAutoGen : PayMammoth.Modules._AutoGen.IPaymentRequestTransactionBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IPaymentRequestTransaction>
    
    {
	    new PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction CreateNewItem();
        new PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
