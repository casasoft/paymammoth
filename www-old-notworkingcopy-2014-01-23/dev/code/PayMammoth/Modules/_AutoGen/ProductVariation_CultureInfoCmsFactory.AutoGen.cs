using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ProductVariation_CultureInfoModule;
using PayMammoth.Cms.ProductVariation_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariation_CultureInfoModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariation_CultureInfoCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBaseCmsFactory
    {
        public static new ProductVariation_CultureInfoBaseCmsFactory Instance 
        {
         	get { return (ProductVariation_CultureInfoBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ProductVariation_CultureInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = ProductVariation_CultureInfo.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            ProductVariation_CultureInfoCmsInfo cmsItem = new ProductVariation_CultureInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override ProductVariation_CultureInfoBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase item)
        {
            return new ProductVariation_CultureInfoCmsInfo((ProductVariation_CultureInfo)item);
            
        }

    }
}
