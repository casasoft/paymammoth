using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.AuditLogModule;
using PayMammoth.Modules.AuditLogModule;
using BusinessLogic_v3.Modules.AuditLogModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.AuditLogModule.AuditLogFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.AuditLogModule.IAuditLog> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.AuditLogModule.AuditLogFrontend ToFrontend(this PayMammoth.Modules.AuditLogModule.IAuditLog item)
        {
        	return PayMammoth.Frontend.AuditLogModule.AuditLogFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class AuditLogFrontend_AutoGen : BusinessLogic_v3.Frontend.AuditLogModule.AuditLogBaseFrontend

    {
		
        
        protected AuditLogFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.AuditLogModule.IAuditLog Data
        {
            get { return (PayMammoth.Modules.AuditLogModule.IAuditLog)base.Data; }
            set { base.Data = value; }

        }

        public new static AuditLogFrontend CreateNewItem()
        {
            return (AuditLogFrontend)BusinessLogic_v3.Frontend.AuditLogModule.AuditLogBaseFrontend.CreateNewItem();
        }
        
        public new static AuditLogFrontend Get(BusinessLogic_v3.Modules.AuditLogModule.IAuditLogBase data)
        {
            return (AuditLogFrontend)BusinessLogic_v3.Frontend.AuditLogModule.AuditLogBaseFrontend.Get(data);
        }
        public new static List<AuditLogFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.AuditLogModule.IAuditLogBase> dataList)
        {
            return BusinessLogic_v3.Frontend.AuditLogModule.AuditLogBaseFrontend.GetList(dataList).Cast<AuditLogFrontend>().ToList();
        }


    }
}
