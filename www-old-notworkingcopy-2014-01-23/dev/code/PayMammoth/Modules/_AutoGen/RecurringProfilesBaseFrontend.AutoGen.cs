using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules._AutoGen.RecurringProfilesModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Modules._AutoGen.RecurringProfilesModule.RecurringProfilesBaseFrontend> ToFrontendBaseList(this IEnumerable<PayMammoth.Modules._AutoGen.IRecurringProfilesBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static PayMammoth.Modules._AutoGen.RecurringProfilesModule.RecurringProfilesBaseFrontend ToFrontendBase(this PayMammoth.Modules._AutoGen.IRecurringProfilesBase item)
        {
        	return PayMammoth.Modules._AutoGen.RecurringProfilesModule.RecurringProfilesBaseFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfilesBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<RecurringProfilesBaseFrontend, PayMammoth.Modules._AutoGen.IRecurringProfilesBase>

    {
		
        
        protected RecurringProfilesBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
