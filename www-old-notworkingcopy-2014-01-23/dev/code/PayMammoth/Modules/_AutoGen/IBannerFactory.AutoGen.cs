using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.BannerModule;
using PayMammoth.Modules.BannerModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IBannerFactoryAutoGen : BusinessLogic_v3.Modules.BannerModule.IBannerBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IBanner>
    
    {
	    new PayMammoth.Modules.BannerModule.IBanner CreateNewItem();
        new PayMammoth.Modules.BannerModule.IBanner GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.BannerModule.IBanner> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.BannerModule.IBanner> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.BannerModule.IBanner> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.BannerModule.IBanner FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.BannerModule.IBanner FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.BannerModule.IBanner FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.BannerModule.IBanner> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
