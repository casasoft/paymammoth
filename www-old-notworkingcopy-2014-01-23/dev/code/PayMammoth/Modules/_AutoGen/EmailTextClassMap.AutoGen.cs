using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.EmailTextModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class EmailTextMap_AutoGen : EmailTextMap_AutoGen_Z
    {
        public EmailTextMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Parent, ParentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Subject, SubjectMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Subject_Search, Subject_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Body, BodyMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Body_Search, Body_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Remarks, RemarksMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.VisibleInCMS_AccessRequired, VisibleInCMS_AccessRequiredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.UsedInProject, UsedInProjectMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomContentTags, CustomContentTagsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.WhenIsThisEmailSent, WhenIsThisEmailSentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NotifyAdminAboutEmail, NotifyAdminAboutEmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NotifyAdminCustomEmail, NotifyAdminCustomEmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ProcessContentUsingNVelocity, ProcessContentUsingNVelocityMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ChildEmails, ChildEmailsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.EmailText_CultureInfos, EmailText_CultureInfosMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class EmailTextMap_AutoGen_Z : BusinessLogic_v3.Modules.EmailTextModule.EmailTextBaseMap<EmailTextImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
