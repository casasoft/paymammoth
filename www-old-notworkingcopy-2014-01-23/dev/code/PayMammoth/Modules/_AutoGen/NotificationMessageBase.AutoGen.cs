using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace PayMammoth.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "NotificationMessage")]
    public abstract class NotificationMessageBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  INotificationMessageBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static NotificationMessageBaseFactory Factory
        {
            get
            {
                return NotificationMessageBaseFactory.Instance; 
            }
        }    
        /*
        public static NotificationMessageBase CreateNewItem()
        {
            return (NotificationMessageBase)Factory.CreateNewItem();
        }

		public static NotificationMessageBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<NotificationMessageBase, NotificationMessageBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static NotificationMessageBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static NotificationMessageBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<NotificationMessageBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<NotificationMessageBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<NotificationMessageBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE _notificationtype;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE NotificationType 
        {
        	get
        	{
        		return _notificationtype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notificationtype,value);
        		_notificationtype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _datecreated;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime DateCreated 
        {
        	get
        	{
        		return _datecreated;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datecreated,value);
        		_datecreated = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _sendtourl;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string SendToUrl 
        {
        	get
        	{
        		return _sendtourl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_sendtourl,value);
        		_sendtourl = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _acknowledgedbyrecipient;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual bool AcknowledgedByRecipient 
        {
        	get
        	{
        		return _acknowledgedbyrecipient;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_acknowledgedbyrecipient,value);
        		_acknowledgedbyrecipient = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _retrycount;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual int RetryCount 
        {
        	get
        	{
        		return _retrycount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_retrycount,value);
        		_retrycount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _lastretryon;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime? LastRetryOn 
        {
        	get
        	{
        		return _lastretryon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_lastretryon,value);
        		_lastretryon = value;
        		
        	}
        }
        
/*
		public virtual long? LinkedPaymentRequestID
		{
		 	get 
		 	{
		 		return (this.LinkedPaymentRequest != null ? (long?)this.LinkedPaymentRequest.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.PaymentRequestBase _linkedpaymentrequest;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Modules._AutoGen.PaymentRequestBase LinkedPaymentRequest 
        {
        	get
        	{
        		return _linkedpaymentrequest;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkedpaymentrequest,value);
        		_linkedpaymentrequest = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IPaymentRequestBase PayMammoth.Modules._AutoGen.INotificationMessageBaseAutoGen.LinkedPaymentRequest 
        {
            get
            {
            	return this.LinkedPaymentRequest;
            }
            set
            {
            	this.LinkedPaymentRequest = (PayMammoth.Modules._AutoGen.PaymentRequestBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _message;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string Message 
        {
        	get
        	{
        		return _message;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_message,value);
        		_message = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private PayMammoth.Connector.Enums.NOTIFICATION_STATUS_CODE _statuscode;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Connector.Enums.NOTIFICATION_STATUS_CODE StatusCode 
        {
        	get
        	{
        		return _statuscode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_statuscode,value);
        		_statuscode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _nextretryon;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime? NextRetryOn 
        {
        	get
        	{
        		return _nextretryon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_nextretryon,value);
        		_nextretryon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _sendingfailed;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual bool SendingFailed 
        {
        	get
        	{
        		return _sendingfailed;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_sendingfailed,value);
        		_sendingfailed = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _acknowledgedbyrecipienton;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual DateTime? AcknowledgedByRecipientOn 
        {
        	get
        	{
        		return _acknowledgedbyrecipienton;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_acknowledgedbyrecipienton,value);
        		_acknowledgedbyrecipienton = value;
        		
        	}
        }
        
/*
		public virtual long? WebsiteAccountID
		{
		 	get 
		 	{
		 		return (this.WebsiteAccount != null ? (long?)this.WebsiteAccount.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.WebsiteAccountBase _websiteaccount;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual PayMammoth.Modules._AutoGen.WebsiteAccountBase WebsiteAccount 
        {
        	get
        	{
        		return _websiteaccount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_websiteaccount,value);
        		_websiteaccount = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IWebsiteAccountBase PayMammoth.Modules._AutoGen.INotificationMessageBaseAutoGen.WebsiteAccount 
        {
            get
            {
            	return this.WebsiteAccount;
            }
            set
            {
            	this.WebsiteAccount = (PayMammoth.Modules._AutoGen.WebsiteAccountBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _statuslog;
        
        [BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public virtual string StatusLog 
        {
        	get
        	{
        		return _statuslog;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_statuslog,value);
        		_statuslog = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
