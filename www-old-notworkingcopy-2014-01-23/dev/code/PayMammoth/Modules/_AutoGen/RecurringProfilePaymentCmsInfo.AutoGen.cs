using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.RecurringProfilePaymentModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class RecurringProfilePaymentCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfilePaymentBaseCmsInfo
    {

		public RecurringProfilePaymentCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.RecurringProfilePaymentBase item)
            : base(item)
        {

        }
        

        private RecurringProfilePaymentFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = RecurringProfilePaymentFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new RecurringProfilePaymentFrontend FrontendItem
        {
            get { return (RecurringProfilePaymentFrontend) base.FrontendItem; }
        }
        
        
		public new RecurringProfilePayment DbItem
        {
            get
            {
                return (RecurringProfilePayment)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]

        public CmsCollectionInfo Transactions { get; protected set; }
        



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.RecurringProfile = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector( item => item.RecurringProfile),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileModule.RecurringProfile>.GetPropertyBySelector( item => item.Payments)));

        
        this.FailureCount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector( item => item.FailureCount),
        						isEditable: true, isVisible: true);

        this.NextRetryOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector( item => item.NextRetryOn),
        						isEditable: true, isVisible: true);

        this.PaymentDueOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector( item => item.PaymentDueOn),
        						isEditable: true, isVisible: true);

        this.Status = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector( item => item.Status),
        						isEditable: true, isVisible: true);

        this.StatusLog = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector( item => item.StatusLog),
        						isEditable: true, isVisible: true);

        this.InitiatedByPayMammoth = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector( item => item.InitiatedByPayMammoth),
        						isEditable: true, isVisible: true);

        this.MarkAsPaymentFailedIfNotPaidBy = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector( item => item.MarkAsPaymentFailedIfNotPaidBy),
        						isEditable: true, isVisible: true);

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]

		
		//InitCollectionUserOneToMany
		this.Transactions = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment>.GetPropertyBySelector(item => item.Transactions),
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.GetPropertyBySelector(item => item.RecurringProfilePayment)));


			base.initBasicFields();
		}
    
    }
}
