using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class NotificationMessageFactoryAutoGen : PayMammoth.Modules._AutoGen.NotificationMessageBaseFactory, 
    				PayMammoth.Modules._AutoGen.INotificationMessageFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<INotificationMessage>
    
    {
    
     	public new NotificationMessage ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.NotificationMessageBase item)
        {
         	return (NotificationMessage)base.ReloadItemFromDatabase(item);
        }
     	static NotificationMessageFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public NotificationMessageFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(NotificationMessageImpl);
			PayMammoth.Cms.NotificationMessageModule.NotificationMessageCmsFactory._initialiseStaticInstance();
        }
        public static new NotificationMessageFactory Instance
        {
            get
            {
                return (NotificationMessageFactory)NotificationMessageBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<NotificationMessage> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<NotificationMessage>();
            
        }
        public new IEnumerable<NotificationMessage> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<NotificationMessage>();
            
            
        }
    
        public new IEnumerable<NotificationMessage> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<NotificationMessage>();
            
        }
        public new IEnumerable<NotificationMessage> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<NotificationMessage>();
        }
        
        public new NotificationMessage FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (NotificationMessage)base.FindItem(query);
        }
        public new NotificationMessage FindItem(IQueryOver query)
        {
            return (NotificationMessage)base.FindItem(query);
        }
        
        INotificationMessage INotificationMessageFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new NotificationMessage CreateNewItem()
        {
            return (NotificationMessage)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<NotificationMessage, NotificationMessage> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<NotificationMessage>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.NotificationMessageModule.NotificationMessage GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.NotificationMessageModule.NotificationMessage) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.NotificationMessageModule.NotificationMessage> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.NotificationMessageModule.NotificationMessage> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.NotificationMessageModule.NotificationMessage> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>) base.FindAll(session);

       }
       public new PayMammoth.Modules.NotificationMessageModule.NotificationMessage FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.NotificationMessageModule.NotificationMessage) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.NotificationMessageModule.NotificationMessage FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.NotificationMessageModule.NotificationMessage) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.NotificationMessageModule.NotificationMessage FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.NotificationMessageModule.NotificationMessage) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.NotificationMessageModule.NotificationMessage> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.NotificationMessageModule.NotificationMessage>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.NotificationMessageModule.INotificationMessage> Members
     
        PayMammoth.Modules.NotificationMessageModule.INotificationMessage IBaseDbFactory<PayMammoth.Modules.NotificationMessageModule.INotificationMessage>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.NotificationMessageModule.INotificationMessage IBaseDbFactory<PayMammoth.Modules.NotificationMessageModule.INotificationMessage>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.NotificationMessageModule.INotificationMessage> IBaseDbFactory<PayMammoth.Modules.NotificationMessageModule.INotificationMessage>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.NotificationMessageModule.INotificationMessage> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.NotificationMessageModule.INotificationMessage>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.NotificationMessageModule.INotificationMessage> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.NotificationMessageModule.INotificationMessage>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.NotificationMessageModule.INotificationMessage IBaseDbFactory<PayMammoth.Modules.NotificationMessageModule.INotificationMessage>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.NotificationMessageModule.INotificationMessage BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.NotificationMessageModule.INotificationMessage>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.NotificationMessageModule.INotificationMessage BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.NotificationMessageModule.INotificationMessage>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.NotificationMessageModule.INotificationMessage> IBaseDbFactory<PayMammoth.Modules.NotificationMessageModule.INotificationMessage>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<INotificationMessage> INotificationMessageFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<INotificationMessage> INotificationMessageFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<INotificationMessage> INotificationMessageFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       INotificationMessage INotificationMessageFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       INotificationMessage INotificationMessageFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       INotificationMessage INotificationMessageFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<INotificationMessage> INotificationMessageFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       INotificationMessage INotificationMessageFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
