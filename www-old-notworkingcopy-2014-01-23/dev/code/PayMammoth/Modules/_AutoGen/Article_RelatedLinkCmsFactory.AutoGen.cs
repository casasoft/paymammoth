using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.Article_RelatedLinkModule;
using PayMammoth.Cms.Article_RelatedLinkModule;
using CS.WebComponentsGeneralV3.Cms.Article_RelatedLinkModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Article_RelatedLinkCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.Article_RelatedLinkModule.Article_RelatedLinkBaseCmsFactory
    {
        public static new Article_RelatedLinkBaseCmsFactory Instance 
        {
         	get { return (Article_RelatedLinkBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.Article_RelatedLinkModule.Article_RelatedLinkBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override Article_RelatedLinkBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Article_RelatedLink.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            Article_RelatedLinkCmsInfo cmsItem = new Article_RelatedLinkCmsInfo(item);
            return cmsItem;
        }    
        
        public override Article_RelatedLinkBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase item)
        {
            return new Article_RelatedLinkCmsInfo((Article_RelatedLink)item);
            
        }

    }
}
