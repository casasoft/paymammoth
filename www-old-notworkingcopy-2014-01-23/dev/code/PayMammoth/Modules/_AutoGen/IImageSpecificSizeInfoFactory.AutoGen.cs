using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule;
using PayMammoth.Modules.ImageSpecificSizeInfoModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IImageSpecificSizeInfoFactoryAutoGen : BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IImageSpecificSizeInfo>
    
    {
	    new PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo CreateNewItem();
        new PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
