using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ArticleCommentModule;
using PayMammoth.Modules.ArticleCommentModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IArticleCommentFactoryAutoGen : BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IArticleComment>
    
    {
	    new PayMammoth.Modules.ArticleCommentModule.IArticleComment CreateNewItem();
        new PayMammoth.Modules.ArticleCommentModule.IArticleComment GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleCommentModule.IArticleComment> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleCommentModule.IArticleComment> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleCommentModule.IArticleComment> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleCommentModule.IArticleComment FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleCommentModule.IArticleComment FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleCommentModule.IArticleComment FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleCommentModule.IArticleComment> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
