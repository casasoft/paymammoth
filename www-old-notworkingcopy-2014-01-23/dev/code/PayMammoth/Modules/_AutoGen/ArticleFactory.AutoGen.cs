using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ArticleModule;
using PayMammoth.Modules.ArticleModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleFactoryAutoGen : BusinessLogic_v3.Modules.ArticleModule.ArticleBaseFactory, 
    				PayMammoth.Modules._AutoGen.IArticleFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IArticle>
    
    {
    
     	public new Article ReloadItemFromDatabase(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item)
        {
         	return (Article)base.ReloadItemFromDatabase(item);
        }
     	static ArticleFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public ArticleFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ArticleImpl);
			PayMammoth.Cms.ArticleModule.ArticleCmsFactory._initialiseStaticInstance();
        }
        public static new ArticleFactory Instance
        {
            get
            {
                return (ArticleFactory)ArticleBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Article> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Article>();
            
        }
        public new IEnumerable<Article> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Article>();
            
            
        }
    
        public new IEnumerable<Article> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Article>();
            
        }
        public new IEnumerable<Article> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Article>();
        }
        
        public new Article FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Article)base.FindItem(query);
        }
        public new Article FindItem(IQueryOver query)
        {
            return (Article)base.FindItem(query);
        }
        
        IArticle IArticleFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Article CreateNewItem()
        {
            return (Article)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<Article, Article> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Article>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

            var itemList = Article.Factory.FindAll();

            var CultureInfos = CultureDetails.Factory.FindAll();

            foreach (var item in itemList)
            {
            	item.__CheckItemCultureInfos(autoSaveInDB: true, CultureInfos: CultureInfos);
            }

  

        }        


#endregion
        public new PayMammoth.Modules.ArticleModule.Article GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.ArticleModule.Article) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.ArticleModule.Article> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleModule.Article>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleModule.Article> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleModule.Article>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleModule.Article> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleModule.Article>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ArticleModule.Article FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleModule.Article) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ArticleModule.Article FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleModule.Article) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ArticleModule.Article FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleModule.Article) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleModule.Article> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleModule.Article>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ArticleModule.IArticle> Members
     
        PayMammoth.Modules.ArticleModule.IArticle IBaseDbFactory<PayMammoth.Modules.ArticleModule.IArticle>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ArticleModule.IArticle IBaseDbFactory<PayMammoth.Modules.ArticleModule.IArticle>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.ArticleModule.IArticle> IBaseDbFactory<PayMammoth.Modules.ArticleModule.IArticle>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ArticleModule.IArticle> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ArticleModule.IArticle>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ArticleModule.IArticle> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ArticleModule.IArticle>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ArticleModule.IArticle IBaseDbFactory<PayMammoth.Modules.ArticleModule.IArticle>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ArticleModule.IArticle BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ArticleModule.IArticle>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ArticleModule.IArticle BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ArticleModule.IArticle>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ArticleModule.IArticle> IBaseDbFactory<PayMammoth.Modules.ArticleModule.IArticle>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IArticle> IArticleFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticle> IArticleFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticle> IArticleFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IArticle IArticleFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticle IArticleFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticle IArticleFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IArticle> IArticleFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IArticle IArticleFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
