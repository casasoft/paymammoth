using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IPaymentRequestTransactionBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IPaymentRequestBase PaymentRequest {get; set; }

		//Iproperty_normal
        bool Successful { get; set; }
		//Iproperty_normal
        PayMammoth.Connector.Enums.PaymentMethodSpecific PaymentMethod { get; set; }
		//Iproperty_normal
        string Comments { get; set; }
		//Iproperty_normal
        string IP { get; set; }
		//Iproperty_normal
        string AuthCode { get; set; }
		//Iproperty_normal
        DateTime StartDate { get; set; }
		//Iproperty_normal
        DateTime LastUpdatedOn { get; set; }
		//Iproperty_normal
        DateTime EndDate { get; set; }
		//Iproperty_normal
        string Reference { get; set; }
		//Iproperty_normal
        string TransactionInfo { get; set; }
		//Iproperty_normal
        string PaymentParameters { get; set; }
		//Iproperty_normal
        string PaymentMethodVersion { get; set; }
		//Iproperty_normal
        bool FakePayment { get; set; }
		//Iproperty_normal
        bool RequiresManualIntervention { get; set; }
		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.INotificationMessageBase LinkedNotificationMessage {get; set; }

		//Iproperty_normal
        string StatusLog { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
