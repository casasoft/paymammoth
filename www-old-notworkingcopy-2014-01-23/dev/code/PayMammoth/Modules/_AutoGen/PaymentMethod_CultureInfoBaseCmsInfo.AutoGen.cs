using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentMethod_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase>
    {
		public PaymentMethod_CultureInfoBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase dbItem)
            : base(PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new PaymentMethod_CultureInfoBaseFrontend FrontendItem
        {
            get { return (PaymentMethod_CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new PaymentMethod_CultureInfoBase DbItem
        {
            get
            {
                return (PaymentMethod_CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; protected set; }

        public CmsPropertyInfo PaymentMethod { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
