using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Cms.RecurringProfilePaymentModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfilePaymentCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.RecurringProfilePaymentBaseCmsFactory
    {
        public static new RecurringProfilePaymentBaseCmsFactory Instance 
        {
         	get { return (RecurringProfilePaymentBaseCmsFactory)PayMammoth.Modules._AutoGen.RecurringProfilePaymentBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override RecurringProfilePaymentBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	RecurringProfilePayment item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = RecurringProfilePayment.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        RecurringProfilePaymentCmsInfo cmsItem = new RecurringProfilePaymentCmsInfo(item);
            return cmsItem;
        }    
        
        public override RecurringProfilePaymentBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.RecurringProfilePaymentBase item)
        {
            return new RecurringProfilePaymentCmsInfo((RecurringProfilePayment)item);
            
        }

    }
}
