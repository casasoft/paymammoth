using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.Article_RelatedLinkModule;
using CS.WebComponentsGeneralV3.Cms.Article_RelatedLinkModule;
using PayMammoth.Frontend.Article_RelatedLinkModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class Article_RelatedLinkCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.Article_RelatedLinkModule.Article_RelatedLinkBaseCmsInfo
    {

		public Article_RelatedLinkCmsInfo_AutoGen(BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase item)
            : base(item)
        {

        }
        

        private Article_RelatedLinkFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = Article_RelatedLinkFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new Article_RelatedLinkFrontend FrontendItem
        {
            get { return (Article_RelatedLinkFrontend) base.FrontendItem; }
        }
        
        
		public new Article_RelatedLink DbItem
        {
            get
            {
                return (Article_RelatedLink)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.ParentPage = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink>.GetPropertyBySelector( item => item.ParentPage),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.RelatedPages)));

        
//InitFieldUser_LinkedProperty
			this.RelatedPage = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink>.GetPropertyBySelector( item => item.RelatedPage),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ParentsOfWhichThisIsRelated)));

        
	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
