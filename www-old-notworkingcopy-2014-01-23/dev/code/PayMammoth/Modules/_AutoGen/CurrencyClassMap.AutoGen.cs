using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.CurrencyModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class CurrencyMap_AutoGen : CurrencyMap_AutoGen_Z
    {
        public CurrencyMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CurrencyISOCode, CurrencyISOCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Symbol, SymbolMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ExchangeRateMultiplier, ExchangeRateMultiplierMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.CultureDetailses, CultureDetailsesMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class CurrencyMap_AutoGen_Z : BusinessLogic_v3.Modules.CurrencyModule.CurrencyBaseMap<CurrencyImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
