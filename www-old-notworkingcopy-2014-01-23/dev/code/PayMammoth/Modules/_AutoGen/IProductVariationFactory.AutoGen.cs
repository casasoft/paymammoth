using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;

using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductVariationModule;
using PayMammoth.Modules.ProductVariationModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IProductVariationFactoryAutoGen : BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductVariation>
    
    {
	    new PayMammoth.Modules.ProductVariationModule.IProductVariation CreateNewItem();
        new PayMammoth.Modules.ProductVariationModule.IProductVariation GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariationModule.IProductVariation> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariationModule.IProductVariation> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariationModule.IProductVariation> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductVariationModule.IProductVariation FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductVariationModule.IProductVariation FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductVariationModule.IProductVariation FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariationModule.IProductVariation> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
