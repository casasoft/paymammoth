using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.BrandModule;
using PayMammoth.Modules.BrandModule;
using BusinessLogic_v3.Modules.BrandModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.BrandModule.BrandFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.BrandModule.IBrand> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.BrandModule.BrandFrontend ToFrontend(this PayMammoth.Modules.BrandModule.IBrand item)
        {
        	return PayMammoth.Frontend.BrandModule.BrandFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class BrandFrontend_AutoGen : BusinessLogic_v3.Frontend.BrandModule.BrandBaseFrontend

    {
		
        
        protected BrandFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.BrandModule.IBrand Data
        {
            get { return (PayMammoth.Modules.BrandModule.IBrand)base.Data; }
            set { base.Data = value; }

        }

        public new static BrandFrontend CreateNewItem()
        {
            return (BrandFrontend)BusinessLogic_v3.Frontend.BrandModule.BrandBaseFrontend.CreateNewItem();
        }
        
        public new static BrandFrontend Get(BusinessLogic_v3.Modules.BrandModule.IBrandBase data)
        {
            return (BrandFrontend)BusinessLogic_v3.Frontend.BrandModule.BrandBaseFrontend.Get(data);
        }
        public new static List<BrandFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.BrandModule.IBrandBase> dataList)
        {
            return BusinessLogic_v3.Frontend.BrandModule.BrandBaseFrontend.GetList(dataList).Cast<BrandFrontend>().ToList();
        }


    }
}
