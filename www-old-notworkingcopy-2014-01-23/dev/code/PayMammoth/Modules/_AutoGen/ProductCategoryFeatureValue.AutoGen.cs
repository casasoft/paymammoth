using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ProductCategoryFeatureValueModule;
using BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCategoryFeatureValue_AutoGen : ProductCategoryFeatureValueBase, PayMammoth.Modules._AutoGen.IProductCategoryFeatureValueAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ProductModule.Product Product
        {
            get
            {
                return (PayMammoth.Modules.ProductModule.Product)base.Product;
            }
            set
            {
                base.Product = value;
            }
        }

		PayMammoth.Modules.ProductModule.IProduct IProductCategoryFeatureValueAutoGen.Product
        {
            get
            {
            	return this.Product;
                
            }
            set
            {
                this.Product = (PayMammoth.Modules.ProductModule.Product)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new ProductCategoryFeatureValueFactory Factory
        {
            get
            {
                return (ProductCategoryFeatureValueFactory)ProductCategoryFeatureValueBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<ProductCategoryFeatureValue, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
