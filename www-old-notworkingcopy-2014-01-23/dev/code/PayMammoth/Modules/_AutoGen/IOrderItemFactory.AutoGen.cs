using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.OrderItemModule;
using PayMammoth.Modules.OrderItemModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IOrderItemFactoryAutoGen : BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IOrderItem>
    
    {
	    new PayMammoth.Modules.OrderItemModule.IOrderItem CreateNewItem();
        new PayMammoth.Modules.OrderItemModule.IOrderItem GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.OrderItemModule.IOrderItem> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.OrderItemModule.IOrderItem> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.OrderItemModule.IOrderItem> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.OrderItemModule.IOrderItem FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.OrderItemModule.IOrderItem FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.OrderItemModule.IOrderItem FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.OrderItemModule.IOrderItem> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
