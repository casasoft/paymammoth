using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.NotificationMessageModule.NotificationMessageFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.NotificationMessageModule.INotificationMessage> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.NotificationMessageModule.NotificationMessageFrontend ToFrontend(this PayMammoth.Modules.NotificationMessageModule.INotificationMessage item)
        {
        	return PayMammoth.Frontend.NotificationMessageModule.NotificationMessageFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class NotificationMessageFrontend_AutoGen : PayMammoth.Modules._AutoGen.NotificationMessageModule.NotificationMessageBaseFrontend

    {
		
        
        protected NotificationMessageFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.NotificationMessageModule.INotificationMessage Data
        {
            get { return (PayMammoth.Modules.NotificationMessageModule.INotificationMessage)base.Data; }
            set { base.Data = value; }

        }

        public new static NotificationMessageFrontend CreateNewItem()
        {
            return (NotificationMessageFrontend)PayMammoth.Modules._AutoGen.NotificationMessageModule.NotificationMessageBaseFrontend.CreateNewItem();
        }
        
        public new static NotificationMessageFrontend Get(PayMammoth.Modules._AutoGen.INotificationMessageBase data)
        {
            return (NotificationMessageFrontend)PayMammoth.Modules._AutoGen.NotificationMessageModule.NotificationMessageBaseFrontend.Get(data);
        }
        public new static List<NotificationMessageFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.INotificationMessageBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.NotificationMessageModule.NotificationMessageBaseFrontend.GetList(dataList).Cast<NotificationMessageFrontend>().ToList();
        }


    }
}
