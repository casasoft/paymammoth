using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.WebsiteAccount_CultureInfoModule;
using PayMammoth.Modules.WebsiteAccount_CultureInfoModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoFrontend ToFrontend(this PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo item)
        {
        	return PayMammoth.Frontend.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class WebsiteAccount_CultureInfoFrontend_AutoGen : PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoBaseFrontend

    {
		
        
        protected WebsiteAccount_CultureInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo Data
        {
            get { return (PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static WebsiteAccount_CultureInfoFrontend CreateNewItem()
        {
            return (WebsiteAccount_CultureInfoFrontend)PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoBaseFrontend.CreateNewItem();
        }
        
        public new static WebsiteAccount_CultureInfoFrontend Get(PayMammoth.Modules._AutoGen.IWebsiteAccount_CultureInfoBase data)
        {
            return (WebsiteAccount_CultureInfoFrontend)PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoBaseFrontend.Get(data);
        }
        public new static List<WebsiteAccount_CultureInfoFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IWebsiteAccount_CultureInfoBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoBaseFrontend.GetList(dataList).Cast<WebsiteAccount_CultureInfoFrontend>().ToList();
        }


    }
}
