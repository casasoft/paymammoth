using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.RecurringProfileTransactionModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RecurringProfileTransactionFactoryAutoGen : PayMammoth.Modules._AutoGen.RecurringProfileTransactionBaseFactory, 
    				PayMammoth.Modules._AutoGen.IRecurringProfileTransactionFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IRecurringProfileTransaction>
    
    {
    
     	public new RecurringProfileTransaction ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.RecurringProfileTransactionBase item)
        {
         	return (RecurringProfileTransaction)base.ReloadItemFromDatabase(item);
        }
     	static RecurringProfileTransactionFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public RecurringProfileTransactionFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(RecurringProfileTransactionImpl);
			PayMammoth.Cms.RecurringProfileTransactionModule.RecurringProfileTransactionCmsFactory._initialiseStaticInstance();
        }
        public static new RecurringProfileTransactionFactory Instance
        {
            get
            {
                return (RecurringProfileTransactionFactory)RecurringProfileTransactionBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<RecurringProfileTransaction> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<RecurringProfileTransaction>();
            
        }
        public new IEnumerable<RecurringProfileTransaction> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<RecurringProfileTransaction>();
            
            
        }
    
        public new IEnumerable<RecurringProfileTransaction> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<RecurringProfileTransaction>();
            
        }
        public new IEnumerable<RecurringProfileTransaction> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<RecurringProfileTransaction>();
        }
        
        public new RecurringProfileTransaction FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (RecurringProfileTransaction)base.FindItem(query);
        }
        public new RecurringProfileTransaction FindItem(IQueryOver query)
        {
            return (RecurringProfileTransaction)base.FindItem(query);
        }
        
        IRecurringProfileTransaction IRecurringProfileTransactionFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new RecurringProfileTransaction CreateNewItem()
        {
            return (RecurringProfileTransaction)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<RecurringProfileTransaction, RecurringProfileTransaction> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<RecurringProfileTransaction>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>) base.FindAll(session);

       }
       public new PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> Members
     
        PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction IBaseDbFactory<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction IBaseDbFactory<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> IBaseDbFactory<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction IBaseDbFactory<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> IBaseDbFactory<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IRecurringProfileTransaction> IRecurringProfileTransactionFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IRecurringProfileTransaction> IRecurringProfileTransactionFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IRecurringProfileTransaction> IRecurringProfileTransactionFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IRecurringProfileTransaction IRecurringProfileTransactionFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IRecurringProfileTransaction IRecurringProfileTransactionFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IRecurringProfileTransaction IRecurringProfileTransactionFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IRecurringProfileTransaction> IRecurringProfileTransactionFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IRecurringProfileTransaction IRecurringProfileTransactionFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
