using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class RecurringProfilePayment_AutoGen : RecurringProfilePaymentBase, PayMammoth.Modules._AutoGen.IRecurringProfilePaymentAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject 
		[BusinessLogic_v3.Classes.Attributes.DatabaseField]
        public new virtual PayMammoth.Modules.RecurringProfileModule.RecurringProfile RecurringProfile
        {
            get
            {
                return (PayMammoth.Modules.RecurringProfileModule.RecurringProfile)base.RecurringProfile;
            }
            set
            {
                base.RecurringProfile = value;
            }
        }

		PayMammoth.Modules.RecurringProfileModule.IRecurringProfile IRecurringProfilePaymentAutoGen.RecurringProfile
        {
            get
            {
            	return this.RecurringProfile;
                
            }
            set
            {
                this.RecurringProfile = (PayMammoth.Modules.RecurringProfileModule.RecurringProfile)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new RecurringProfilePaymentFactory Factory
        {
            get
            {
                return (RecurringProfilePaymentFactory)RecurringProfilePaymentBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<RecurringProfilePayment, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        //UserClassOnly_CollectionOneToManyLeftSide
        
        
        public class __TransactionsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment, 
        	PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction,
        	PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment, PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>
        {
            private PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment _item = null;
            public __TransactionsCollectionInfo(PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>)_item.__collection__Transactions; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment, PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction>.SetLinkOnItem(PayMammoth.Modules.RecurringProfileTransactionModule.RecurringProfileTransaction item, PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment value)
            {
                item.RecurringProfilePayment = value;
            }
        }
        
        private __TransactionsCollectionInfo _Transactions = null;
        public __TransactionsCollectionInfo Transactions
        {
            get
            {
                if (_Transactions == null)
                    _Transactions = new __TransactionsCollectionInfo((PayMammoth.Modules.RecurringProfilePaymentModule.RecurringProfilePayment)this);
                return _Transactions;
            }
        }

   
		ICollectionManager<PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction> IRecurringProfilePaymentAutoGen.Transactions
        {
            get {  return this.Transactions; }
        }
   
        protected virtual IEnumerable<PayMammoth.Modules._AutoGen.RecurringProfileTransactionBase> __collection__Transactions
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }
        
        
        
    }
    
}
