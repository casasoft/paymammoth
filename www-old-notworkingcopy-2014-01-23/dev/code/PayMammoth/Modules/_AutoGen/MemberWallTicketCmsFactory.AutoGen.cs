using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using PayMammoth.Modules.MemberWallTicketModule;
using PayMammoth.Cms.MemberWallTicketModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberWallTicketCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.MemberWallTicketBaseCmsFactory
    {
        public static new MemberWallTicketBaseCmsFactory Instance 
        {
         	get { return (MemberWallTicketBaseCmsFactory)PayMammoth.Modules._AutoGen.MemberWallTicketBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override MemberWallTicketBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = MemberWallTicket.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            MemberWallTicketCmsInfo cmsItem = new MemberWallTicketCmsInfo(item);
            return cmsItem;
        }    
        
        public override MemberWallTicketBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.MemberWallTicketBase item)
        {
            return new MemberWallTicketCmsInfo((MemberWallTicket)item);
            
        }

    }
}
