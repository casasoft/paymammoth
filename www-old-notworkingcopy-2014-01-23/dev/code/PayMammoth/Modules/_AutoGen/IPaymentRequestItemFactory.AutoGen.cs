using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentRequestItemModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IPaymentRequestItemFactoryAutoGen : PayMammoth.Modules._AutoGen.IPaymentRequestItemBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IPaymentRequestItem>
    
    {
	    new PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem CreateNewItem();
        new PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentRequestItemModule.IPaymentRequestItem> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
