using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.CurrencyModule;
using PayMammoth.Modules.CurrencyModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class CurrencyFactoryAutoGen : BusinessLogic_v3.Modules.CurrencyModule.CurrencyBaseFactory, 
    				PayMammoth.Modules._AutoGen.ICurrencyFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<ICurrency>
    
    {
    
     	public new Currency ReloadItemFromDatabase(BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase item)
        {
         	return (Currency)base.ReloadItemFromDatabase(item);
        }
     	static CurrencyFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public CurrencyFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(CurrencyImpl);
			PayMammoth.Cms.CurrencyModule.CurrencyCmsFactory._initialiseStaticInstance();
        }
        public static new CurrencyFactory Instance
        {
            get
            {
                return (CurrencyFactory)CurrencyBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Currency> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Currency>();
            
        }
        public new IEnumerable<Currency> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Currency>();
            
            
        }
    
        public new IEnumerable<Currency> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Currency>();
            
        }
        public new IEnumerable<Currency> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Currency>();
        }
        
        public new Currency FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Currency)base.FindItem(query);
        }
        public new Currency FindItem(IQueryOver query)
        {
            return (Currency)base.FindItem(query);
        }
        
        ICurrency ICurrencyFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Currency CreateNewItem()
        {
            return (Currency)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<Currency, Currency> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Currency>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.CurrencyModule.Currency GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.CurrencyModule.Currency) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.CurrencyModule.Currency> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CurrencyModule.Currency>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CurrencyModule.Currency> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CurrencyModule.Currency>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CurrencyModule.Currency> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CurrencyModule.Currency>) base.FindAll(session);

       }
       public new PayMammoth.Modules.CurrencyModule.Currency FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CurrencyModule.Currency) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.CurrencyModule.Currency FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CurrencyModule.Currency) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.CurrencyModule.Currency FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CurrencyModule.Currency) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.CurrencyModule.Currency> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CurrencyModule.Currency>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.CurrencyModule.ICurrency> Members
     
        PayMammoth.Modules.CurrencyModule.ICurrency IBaseDbFactory<PayMammoth.Modules.CurrencyModule.ICurrency>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.CurrencyModule.ICurrency IBaseDbFactory<PayMammoth.Modules.CurrencyModule.ICurrency>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.CurrencyModule.ICurrency> IBaseDbFactory<PayMammoth.Modules.CurrencyModule.ICurrency>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.CurrencyModule.ICurrency> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CurrencyModule.ICurrency>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.CurrencyModule.ICurrency> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CurrencyModule.ICurrency>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.CurrencyModule.ICurrency IBaseDbFactory<PayMammoth.Modules.CurrencyModule.ICurrency>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CurrencyModule.ICurrency BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CurrencyModule.ICurrency>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CurrencyModule.ICurrency BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CurrencyModule.ICurrency>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.CurrencyModule.ICurrency> IBaseDbFactory<PayMammoth.Modules.CurrencyModule.ICurrency>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ICurrency> ICurrencyFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICurrency> ICurrencyFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICurrency> ICurrencyFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       ICurrency ICurrencyFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICurrency ICurrencyFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICurrency ICurrencyFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ICurrency> ICurrencyFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ICurrency ICurrencyFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
