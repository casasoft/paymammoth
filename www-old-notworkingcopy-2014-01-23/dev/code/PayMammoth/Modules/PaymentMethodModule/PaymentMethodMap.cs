using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;


namespace PayMammoth.Modules.PaymentMethodModule
{

//ClassMap-File
    
    public class PaymentMethodMap : PayMammoth.Modules._AutoGen.PaymentMethodMap_AutoGen
    {
        public PaymentMethodMap() 
        {

        }
        public override BusinessLogic_v3.Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return BusinessLogic_v3.Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
    }
   
}
