using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;



namespace PayMammoth.Modules.PaymentMethodModule
{

//UserFactory-File

    public class PaymentMethodFactory : PayMammoth.Modules._AutoGen.PaymentMethodFactoryAutoGen, IPaymentMethodFactory
    {   
    
		
            
       private PaymentMethodFactory()
  	   {
    	   
   	   }


       #region IPaymentMethodFactory Members

       public IPaymentMethod GetPaymentMethodByIdentifier(Connector.Enums.PaymentMethodSpecific method)
       {
           var q = GetQuery();
           q = q.Where(x => x.PaymentMethod == method);
           q.Cacheable();

           var item = FindItem(q);
           if (item == null)
           {
               using (var t = beginTransaction())
               {
                   item = CreateNewItem();
                   item.Title = CS.General_v3.Util.EnumUtils.StringValueOf(method, addSpacesToCamelCasedName: true);
                   item.PaymentMethod = method;
                   item.Create();
                   t.CommitIfActiveElseFlush();
               }
           }
           return item;
           
           
       }

       #endregion
    }
}
