using BusinessLogic_v3.Classes.MediaItems;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.WebsiteAccountModule;


namespace PayMammoth.Modules.PaymentMethodModule
{

//IUserClass-File
	
    public interface IPaymentMethod : PayMammoth.Modules._AutoGen.IPaymentMethodAutoGen
    {
        
        IMediaItemImage<PaymentMethod.IconSizingEnum> Icon { get; }
    }
}
