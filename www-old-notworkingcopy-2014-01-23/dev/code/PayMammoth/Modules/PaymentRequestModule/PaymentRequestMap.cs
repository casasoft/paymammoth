using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;


namespace PayMammoth.Modules.PaymentRequestModule
{

//ClassMap-File
    
    public class PaymentRequestMap : PayMammoth.Modules._AutoGen.PaymentRequestMap_AutoGen
    {
        public PaymentRequestMap() 
        {

        }
        protected override void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            base.IdentifierMappingInfo(mapInfo);
        }
    }
   
}
