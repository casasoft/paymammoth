using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using PayMammoth.Modules.RecurringProfileModule;
using log4net;
using NHibernate;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Classes.Exceptions;
using PayMammoth.Classes.PaymentNotifications;
using PayMammoth.Connector.Classes.Interfaces;
using CS.General_v3.Classes.Email;
using PayMammoth.Modules.EmailTextModule;
using CS.General_v3.Classes.Text;
using PayMammoth.Util;
using PayMammoth.Connector.Classes.Requests;
using PayMammoth.Connector.Classes.Serialization;
using NHibernate.Exceptions;
using System.Collections.Concurrent;
using PayMammoth.Modules.NotificationMessageModule;


namespace PayMammoth.Modules.PaymentRequestModule
{

    //UserClass-File

    public abstract class PaymentRequest : PayMammoth.Modules._AutoGen.PaymentRequest_AutoGen, PayMammoth.Modules.PaymentRequestModule.IPaymentRequest, RecurringProfileModule.IRecurringProfileDetails
    {
        #region UserClass-AutoGenerated
        #endregion



        public PaymentRequest()
        {
            

        }
        protected override void initPropertiesForNewItem()
        {
            this.CurrencyCode = (CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro);

            this.DateTime = CS.General_v3.Util.Date.Now;
            //Fill any default values for NEW items.

            base.initPropertiesForNewItem();
        }
        public override string ToString()
        {
            return base.ToString();
        }


        public void CopyDetailsFromInitialRequest(IInitialRequestDetails initRequest)
        {
            CS.General_v3.Util.ReflectionUtil.CopyProperties<IPaymentRequestDetails>(initRequest, this);

        }



        public void GenerateCode()
        {
            if (this.IsTransient())
            {//so that the primary key is generated successfully
                this.Save();
            }
            this.Identifier = CS.General_v3.Util.Random.GetGUID(removeDashes: true) + this.ID;

        }

        public bool WaitUntilRequestIsMarkedAsPaid()
        {

            bool markedAsPaid = false;
            ISession session = (ISession)BusinessLogic_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();


            int timeoutInMilleSec = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Other_Timeouts_MarkAsPaid);


            //  session.Flush();
            PaymentRequest curr = this;


            int totalWait = 0;
            int waitInterval = 1000;
            do
            {
                if (curr.CheckIfPaid())
                {
                    markedAsPaid = true;
                    break;
                }
                else
                {
                    totalWait += waitInterval;
                    System.Threading.Thread.Sleep(waitInterval);
                    this.RefreshFromDb();
                }

            } while (totalWait <= timeoutInMilleSec);

            //var currSession = BusinessLogic_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
            //            currSession.Merge(curr);
            if (curr != null && !curr.CheckIfPaid())
            {
                _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(curr, "WaitUntilRequestIsMarkedAsPaid [" + curr.Identifier + "] - Not marked as paid"));
            }
            return markedAsPaid;




            //-------


            //var req = Factory.WaitUntilRequestIsMarkedAsPaid(this);
            //return req.Paid;

        }

        #region IPaymentRequestDetails Members



        #endregion
        public PaymentRequestTransaction GetSuccessfulTransaction()
        {
            return this.Transactions.Where(x => x.Successful).FirstOrDefault();
        }

        public double GetTotalPrice(bool incTaxes = true, bool incShipping = true, bool incHandling = true)
        {
            return PayMammoth.Connector.Extensions.PaymentRequestDetailsExtensions.GetTotalPrice(this, incTaxes, incShipping, incHandling);
        }

        public long GetTotalPriceInCents()
        {
            return (long)Math.Round(GetTotalPrice() * 100);
        }

        public string ComputeHash()
        {
            return PayMammoth.Connector.Util.HashUtil.GenerateInitialRequestHash(this.WebsiteAccount.Code, this.Title, this.GetTotalPriceInCents(), this.WebsiteAccount.SecretWord);

        }




        public PaymentRequestTransaction StartNewTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific paymentMethod)
        {
            if (this.Paid)
            {
                throw new InvalidOperationException("Cannot start a new transaction if request is already marked as paid!");
            }
            PaymentRequestTransaction transaction = null;
            bool ok = false;
            int retryTimes = 0;
            while (!ok)
            {
                try
                {
                    using (var t = beginTransaction())
                    {

                        transaction = this.Transactions.CreateNewItem();
                        transaction.StartDate = CS.General_v3.Util.Date.Now;
                        transaction.IP = CS.General_v3.Util.PageUtil.GetUserIP();
                        transaction.LastUpdatedOn = CS.General_v3.Util.Date.Now;
                        this.CurrentTransaction = transaction;
                        transaction.PaymentMethod = paymentMethod;

                        transaction.Save();
                        this.Save();
                        t.CommitIfActiveElseFlush();
                        ok = true;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is StaleStateException || ex is GenericADOException)
                    {
                        CS.General_v3.Util.Random.WaitARandomAmountOfTime(50, 250);
                        BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContextAndCreateNewOne(); //once an error occurs, session is not valid anymore
                        ok = false;
                        //_log.Warn(ex);
                        if (retryTimes > 3) throw; //throw the exception if it keeps happening
                    }
                    else
                    {
                        throw;
                    }
                }
                retryTimes++;

            }



            return transaction;
        }

        public bool CheckIfFakePaymentsAllowed()
        {
            return this.WebsiteAccount.EnableFakePayments;
        }

        public PaymentRequestTransaction MakeFakePayment(PayMammoth.Connector.Enums.PaymentMethodSpecific paymentMethod, string fakePaymentKey)
        {

            if (CheckIfFakePaymentsAllowed() || this.WebsiteAccount.CheckFakePaymentKey(fakePaymentKey)) //this is true as now you can enable fake payments using the key, and not just the boolean.
            {
                var t = StartNewTransaction(paymentMethod);
                t.MarkAsFakePayment();
                return t;

            }
            else
            {
                throw new FakePaymentNotAllowedException("Fake payments not allowed under this website account <" + this.WebsiteAccount.Code + ">");
            }

        }



        public bool VerifyHash(string Hash)
        {
            return string.Compare(Hash, this.ComputeHash(), true) == 0;

        }

        public bool CheckIfPaid()
        {
            return this.Paid;

        }


       

        /// <summary>
        /// This method initiates the posting of the notification message 
        /// </summary>

        public NotificationMessage CreateAndSendNotificationRegardingImmediatePayment()
        {
            NotificationMessage msg = null;
            if (this.PaymentNotificationEnabled)
            {

                

                var notificationSender = BusinessLogic_v3.Util.InversionOfControlUtil.Get<NotificationCreatorAndSender>();

                notificationSender.CreateAndQueueNotification(
                    new NotificationMessageModule.Commands.CreateNewNotificationCommand(Connector.Enums.NOTIFICATION_RESPONSE_TYPE.ImmediatePayment, 
                        this.WebsiteAccount, this.GetNotificationUrl())
                {
                    Identifier = this.Identifier,
                    Message = "Payment successful",
                    StatusCode = Connector.Enums.NOTIFICATION_STATUS_CODE.Success,
                    LinkedPaymentRequest = this
                }); ;
                

            }
            
            return msg;
        }


        public void MarkAsSuccessful(PayMammoth.Connector.Enums.PaymentMethodSpecific paymentMethod, bool requiresManualIntervention)
        {
            if (_log.IsDebugEnabled) _log.Debug("Marking payment as successful - Start");
            using (var t = beginTransaction())
            {
                this.Paid = true;
                this.PaidOn = CS.General_v3.Util.Date.Now;
                this.PaidByPaymentMethod = paymentMethod;
                this.RequiresManualIntervention = requiresManualIntervention;

                this.Update();
                t.Commit();

            }
            
            if (_log.IsDebugEnabled) _log.Debug("Starting posting notification on response url");
            this.CreateAndSendNotificationRegardingImmediatePayment();

            if (_log.IsDebugEnabled) _log.Debug("Sending out emails");
            this.SendPaymentSuccessfulEmails();

            if (_log.IsDebugEnabled) _log.Debug("Marking payment as successful - Finished");
        }

        public void MarkAsSuccessful(PaymentRequestTransaction paymentRequestTransaction)
        {
            MarkAsSuccessful(paymentRequestTransaction.PaymentMethod, paymentRequestTransaction.RequiresManualIntervention);
            
        }

        #region IPaymentRequest Members


        void IPaymentRequest.MakeFakePayment(PayMammoth.Connector.Enums.PaymentMethodSpecific paymentMethodSpecific, string fakePaymentKey)
        {
            this.MakeFakePayment(paymentMethodSpecific, fakePaymentKey);

        }

        #endregion

        #region IPaymentRequest Members




        #endregion

        #region IPaymentRequest Members



        #endregion

        #region IClientDetails Members


        public string GetAddressAsOneLine(bool includeCountry)
        {
            return PayMammoth.Connector.Extensions.ClientDetailsExtensions.GetAddressAsOneLine(this, includeCountry);

        }


        #endregion

        #region IPaymentRequestDetails Members



        #endregion



        public void FillTokenReplacer(ITokenReplacer tokenRep)
        {

            tokenRep.SetToken(this, null, x => x.DateTime, x => x.ToString("dd/MMM/yyyy hh:mm tt"));
            tokenRep.SetTokens(this, null,
                x => x.Title,
                x => x.Description,

                x => x.PriceExcTax,
                x => x.TaxAmount,
                x => x.ShippingAmount,
                x => x.HandlingAmount,
                x => x.ClientIp,
                x => x.PaidByPaymentMethod);
            tokenRep["[CurrencyCode]"] = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode(this.CurrencyCode);
            tokenRep["[TotalPrice]"] = this.GetTotalPrice().ToString("#,0.00");
            tokenRep["[ClientFullName]"] = this.GetClientFullName();
            tokenRep["[ClientAddressOneLine]"] = this.GetAddressAsOneLine(includeCountry: true);

            this.WebsiteAccount.FillTokenReplacer(tokenRep);
            tokenRep.AddTokensFromType<IClientDetails>(this);


        }

        private void sendPaymentSuccessfulEmailToAdministration()
        {
            Enums.EmailIdentifierPayMammoth identifierToUse = Enums.GetAdminEmailToSendBasedOnPaymentMethod(this.PaidByPaymentMethod.Value);
            var email = Factories.EmailTextFactory.GetPayMammothEmail(identifierToUse);
            var account = this.WebsiteAccount;
            if (!string.IsNullOrWhiteSpace(account.NotificationEmail))
            {
                EmailMessage msg = email.GetEmailMessage();
                FillTokenReplacer(msg.TokenReplacer);
                msg.AddToEmails(account.NotificationEmail, account.WebsiteName);
                msg.SetFromEmail(CS.General_v3.Settings.Emails.NotificationEmail, CS.General_v3.Settings.Emails.SentEmailsFromName);

                msg.SendAsynchronously();
            }



        }
        public void SendPaymentSuccessfulEmails()
        {
            sendPaymentSuccessfulEmailToAdministration();
            if (this.WebsiteAccount.NotifyClientsByEmailAboutPayment)
                sendPaymentSuccessfulEmailToUser();
        }
        public bool CheckIfPaymentMethodRequiresManualPayment()
        {
            return PayMammoth.Util.PaymentUtil.CheckIfPaymentMethodRequiresManualPayment(this.PaidByPaymentMethod.Value);
        }

        private void sendPaymentSuccessfulEmailToUser()
        {
            if (!this.WebsiteAccount.NotifyClientsByEmailAboutPayment)
                throw new InvalidOperationException("This cannot be called if notifications for clients are not enabled");

            if (!string.IsNullOrEmpty(this.ClientEmail))
            {
                Enums.EmailIdentifierPayMammoth identifierToUse = Enums.GetUserEmailToSendBasedOnPaymentMethod(this.PaidByPaymentMethod.Value);


                var email = Factories.EmailTextFactory.GetPayMammothEmail(identifierToUse);

                EmailMessage msg = email.GetEmailMessage();
                FillTokenReplacer(msg.TokenReplacer);
                var account = this.WebsiteAccount;

                msg.SetReplyToEmail(this.WebsiteAccount.ContactEmail, this.WebsiteAccount.ContactName);

                msg.AddToEmails(this.ClientEmail, this.GetClientFullName());
                msg.SetFromEmail(CS.General_v3.Settings.Emails.NotificationEmail, CS.General_v3.Settings.Emails.SentEmailsFromName);

                msg.SendAsynchronously();

            }


        }

        public string GetClientFullName()
        {
            return PayMammoth.Connector.Extensions.ClientDetailsExtensions.GetClientFullName(this);

        }

        #region IPaymentRequest Members


        public bool VerifyCurrentTransactionIsStill(Connector.Enums.PaymentMethodSpecific paymentMethodSpecific)
        {
            bool ok = true;
            var t = this.CurrentTransaction;
            if (t == null || t.PaymentMethod != paymentMethodSpecific)
            {
                ok = false;
                throw new InvalidOperationException("Current transaction is no longer < " + paymentMethodSpecific + ">");
            }
            return ok;


        }

        #endregion

        #region IPaymentRequest Members


        public bool WaitUntilImmediatePaymentNotificationAcknowledgementReceived()
        {
            bool isAcknowledged = false;
            ISession session = (ISession)BusinessLogic_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();


            int timeoutInMilleSec = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Other_Timeouts_PaymentSuccessNotificationAcknowledge);


            //  session.Flush();
            PaymentRequest curr = this;


            int totalWait = 0;
            int waitInterval = 1000;
            do
            {
                var list = this.NotificationMessages.Where(x => x.NotificationType == Connector.Enums.NOTIFICATION_RESPONSE_TYPE.ImmediatePayment);
                bool allAcknowledged = list.All(x => x.AcknowledgedByRecipient);
                if (allAcknowledged)
                {
                    isAcknowledged = true;
                    break;
                }
                else
                {
                    totalWait += waitInterval;
                    System.Threading.Thread.Sleep(waitInterval);
                    this.RefreshFromDb();
                }

            } while (totalWait <= timeoutInMilleSec);

            //var currSession = BusinessLogic_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
            //            currSession.Merge(curr);
            if (curr != null && !isAcknowledged)
            {
                _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(curr, "WaitUntilRequestNotificationReceived [" + curr.Identifier + "] - Not acknowledged"));
            }
            return isAcknowledged;






            //bool received = Factory.WaitUntilPaymentNotificationAcknowledgementReceived(this.ID);
            //return received;


        }

        //public void MarkAsNotificationResponseAcknowledged(bool autoSave = true)
        //{
        //    //odo: remove this later on [karl, 22/aug/2012]
        //    using (var t = beginTransaction())
        //    {

        //        this.PaymentNotificationAcknowledged = true;
        //        this.PaymentNotificationAcknowledgedOn = CS.General_v3.Util.Date.Now;
        //        if (autoSave)
        //        {
        //            this.Save();
        //            t.Commit();
        //        }
        //    }

        //}
        protected override void Save_Before(OperationResult result, SaveBeforeAfterParams sParams)
        {

            //if (_log.IsInfoEnabled) _log.Info(LogUtil.AddRequestIdentifierToLogMsg( this, "Save_Before - PaymentNotificationAcknowledged: " + this.PaymentNotificationAcknowledged));

            base.Save_Before(result, sParams);
        }
        #endregion

        //public bool CheckIfPaymentNotificationAcknowledged()
        //{
        //    return this.PaymentNotificationAcknowledged;

        //}



        #region IPaymentRequestDetails Members

        IEnumerable<IItemDetail> IPaymentRequestDetails.GetItemDetails()
        {
            return this.ItemDetails;

        }

        string IPaymentRequestDetails.ItemDetailsStr
        {
            get
            {
                return ItemDetailsSerializer.SerializeItemDetailsToString(this.ItemDetails);
            }
            set
            {
                this.ItemDetails.Clear();
                ItemDetailsSerializer.DeserializeStringToItemDetails(value, () => this.ItemDetails.CreateNewItem());

            }
        }

        #endregion

        #region IPaymentRequest Members


        //public bool CheckIfPaymentNotificationStillNeedsToBeSent()
        //{
        //    //odo: this method needs to be removed now [karl, 22/aug/2012]
        //    int maxRetryCount = (Constants.GetPaymentNotificationsRetryCounts().Count);

        //    return !this.PaymentNotificationAcknowledged && // not already acknowledged
        //        this.PaymentNotificationEnabled && //payment notification for this specific request is enabled
        //        this.PaymentNotificationRetryCount <= maxRetryCount;// the retry count is not greater or equal to the maximum.  This is 0-based, that is why it is greater or equal.

        //}

        //public void IncrementPaymentNotificationRetryCount()
        //{


        //    var retryIntervals = Constants.GetPaymentNotificationsRetryCounts();

        //    using (var t = beginTransaction())
        //    {
        //        this.PaymentNotificationRetryCount++;
        //        if (this.PaymentNotificationRetryCount < retryIntervals.Count) //has a valid interval
        //        {
        //            int intervalTotal = retryIntervals[this.PaymentNotificationRetryCount];
        //            this.PaymentNotificationNextRetryOn = CS.General_v3.Util.Date.Now.AddSeconds(intervalTotal);
        //        }
        //        else
        //        {
        //            this.PaymentNotificationNextRetryOn = null;
        //        }



        //        this.Save();
        //        t.Commit();
        //    }
        //}

        #endregion

        #region IPaymentRequest Members




        #endregion

        //public void DisablePaymentNotifications()
        //{
        //    using (var t = beginTransaction())
        //    {
        //        this.PaymentNotificationEnabled = false;
        //        this.Save();
        //        t.Commit();
        //    }

        //}

        //public int NotificationRetryCount
        //{
        //    get { return PaymentNotificationRetryCount; }
        //}


        //public Connector.Enums.NOTIFICATION_RESPONSE_TYPE NotificationType
        //{
        //    get { return Connector.Enums.NOTIFICATION_RESPONSE_TYPE.ImmediatePayment; }
        //}

    //    public INotifyMsg CreateNotificationMessage()
    //    {
    //        NotifyMsgImmediatePayment msg = null;

    //        if (this.WebsiteAccount != null)
    //        {
    //            if (!string.IsNullOrEmpty(this.WebsiteAccount.ResponseUrl))
    //            {
    //                msg = new NotifyMsgImmediatePayment();
    //                msg.Identifier = this.Identifier;
    //                msg.RetryCount = this.NotificationRetryCount;
    //                msg.Url = this.WebsiteAccount.ResponseUrl;
    //            }
    //            else
    //            {
    //                _log.Debug("NotifyResponseUrlAboutPayment called for request <" + this.Identifier + "> of type <" + this.NotificationType + "> but it does not have any response url. Disabling notifications");
    //                this.DisablePaymentNotifications();
    //            }
    //        }
    //        else
    //        {
    //            _log.Debug("NotifyResponseUrlAboutPayment called for request <" + this.Identifier + "> of type <" + this.NotificationType + "> but it does not have a linked website account");
    //        }
    //        return msg;
    //    }



        #region IRecurringProfileDetails Members

        Connector.Enums.RECURRING_BILLINGPERIOD IRecurringProfileDetails.RecurringIntervalType
        {
            get { return this.RecurringProfile_IntervalType; }
            set { this.RecurringProfile_IntervalType = value; }
        }

        int IRecurringProfileDetails.RecurringIntervalFrequency
        {
            get { return this.RecurringProfile_IntervalFrequency;}
            set { this.RecurringProfile_IntervalFrequency = value; }
        }

        int IRecurringProfileDetails.MaximumFailedAttempts
        {
            get { return this.RecurringProfile_MaxFailedAttempts; }
            set { this.RecurringProfile_MaxFailedAttempts = value; }
        }

        int? IRecurringProfileDetails.TotalBillingCyclesRequired
        {
            get { return this.RecurringProfile_TotalBillingCycles;}
            set { this.RecurringProfile_TotalBillingCycles = value; }
        }

        #endregion

        #region IPaymentRequestDetails Members

        int IPaymentRequestDetails.RecurringProfile_BillingFrequency
        {
            get
            {
                return this.RecurringProfile_IntervalFrequency;
                
            }
            set
            {
                this.RecurringProfile_IntervalFrequency = value;
                
            }
        }

        #endregion

        #region IPaymentRequestDetails Members

        Connector.Enums.RECURRING_BILLINGPERIOD IPaymentRequestDetails.RecurringProfile_BillingPeriod
        {
            get
            {
                return this.RecurringProfile_IntervalType;
                
            }
            set
            {
                this.RecurringProfile_IntervalType = value;
                
            }
        }

        #endregion

        public  string GetNotificationUrl()
        {
            string url = this.NotificationUrl;
            if (string.IsNullOrWhiteSpace(url))
            {
                url = this.WebsiteAccount.ResponseUrl;
            }
            return url;
        }

        public INotificationMessage CreateNewNotificationMessage()
        {
            var msg = Factories.NotificationMessageFactory.CreateNewItem();
            msg.SendToUrl = this.GetNotificationUrl();
            msg.WebsiteAccount = this.WebsiteAccount;
            
            return msg;

        }
    }
}




