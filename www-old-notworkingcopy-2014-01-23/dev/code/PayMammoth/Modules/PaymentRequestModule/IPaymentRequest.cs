using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Connector.Classes.Interfaces;


namespace PayMammoth.Modules.PaymentRequestModule
{

//IUserClass-File

    public interface IPaymentRequest : PayMammoth.Modules._AutoGen.IPaymentRequestAutoGen, IClientDetails, IPaymentRequestDetails
    {

        bool CheckIfPaid();

        

        void MakeFakePayment(PayMammoth.Connector.Enums.PaymentMethodSpecific paymentMethodSpecific, string fakePaymentKey);

        PaymentRequestTransactionModule.PaymentRequestTransaction StartNewTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific paymentMethodSpecific);
        double GetTotalPrice(bool incTaxes = true, bool incShipping = true, bool incHandling = true);
        //long GetTotalPriceInCents();
        INotificationMessage CreateNewNotificationMessage();



        /// <summary>
        /// Waits until the request is makred as paid
        /// </summary>
        /// <returns></returns>
        bool WaitUntilRequestIsMarkedAsPaid();

        ///// <summary>
        ///// Waits until the payment success request notification is acknowledged by the other server
        ///// </summary>
        ///// <returns></returns>
        //bool WaitUntilPaymentNotificationAcknowledgementReceived();

        bool VerifyCurrentTransactionIsStill(Connector.Enums.PaymentMethodSpecific paymentMethodSpecific);

        //void MarkAsNotificationResponseAcknowledged(bool autoSave = true);

        //bool CheckIfPaymentNotificationStillNeedsToBeSent();

        //void IncrementPaymentNotificationRetryCount();

        //bool IsPaymentNotificationAcknowledge();


        bool WaitUntilImmediatePaymentNotificationAcknowledgementReceived();
        string GetNotificationUrl();
    }
}
