using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Cms.RecurringProfileModule
{                          

//CmsFactory-Class

    public class RecurringProfileCmsFactory : PayMammoth.Modules._AutoGen.RecurringProfileCmsFactory_AutoGen
    {
        
        
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.
            cmsInfo.ShowInCmsMainMenu = true;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        protected RecurringProfileCmsFactory()
        {

        }
    }
}
