using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.RecurringProfileModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace PayMammoth.Cms.RecurringProfileModule
{

    //UserCmsInfo-Class

    public class RecurringProfileCmsInfo : PayMammoth.Modules._AutoGen.RecurringProfileCmsInfo_AutoGen
    {

        public RecurringProfileCmsInfo(PayMammoth.Modules._AutoGen.RecurringProfileBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            //custom init field logic here
            this.ID.ShowInListing = true;
            this.CreatedOn.ShowInListing = true;
            this.PaymentGateway.ShowInListing = true;
            this.Identifier.ShowInListing = true;
         //   this.PaymentTransaction.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(ID, CreatedOn, PaymentGateway,  Identifier);
            this.AccessTypeRequired_ToEdit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.EditOrderPriorities.AddColumnsToStart(ID, CreatedOn, PaymentGateway, 
                                                       Identifier, ProfileIdentifierOnGateway);
            base.customInitFields();
        }
    }
}
