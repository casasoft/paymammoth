using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.Article_ParentChildLinkModule;

namespace PayMammoth.Cms.Article_ParentChildLinkModule
{

//UserCmsInfo-Class

    public class Article_ParentChildLinkCmsInfo : PayMammoth.Modules._AutoGen.Article_ParentChildLinkCmsInfo_AutoGen
    {
    	
        public Article_ParentChildLinkCmsInfo(BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
