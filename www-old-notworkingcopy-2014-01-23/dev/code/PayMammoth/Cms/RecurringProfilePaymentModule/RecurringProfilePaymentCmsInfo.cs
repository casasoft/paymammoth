using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace PayMammoth.Cms.RecurringProfilePaymentModule
{

//UserCmsInfo-Class

    public class RecurringProfilePaymentCmsInfo : PayMammoth.Modules._AutoGen.RecurringProfilePaymentCmsInfo_AutoGen
    {
    	
        public RecurringProfilePaymentCmsInfo(PayMammoth.Modules._AutoGen.RecurringProfilePaymentBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
