using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.MemberModule;

namespace PayMammoth.Cms.MemberModule
{

//UserCmsInfo-Class

    public class MemberCmsInfo : PayMammoth.Modules._AutoGen.MemberCmsInfo_AutoGen
    {
    	
        public MemberCmsInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
