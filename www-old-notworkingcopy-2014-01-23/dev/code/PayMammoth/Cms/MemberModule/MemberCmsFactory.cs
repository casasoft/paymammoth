using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Cms.MemberModule
{                          

//CmsFactory-Class

    public class MemberCmsFactory : PayMammoth.Modules._AutoGen.MemberCmsFactory_AutoGen
    {
        
        
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        protected MemberCmsFactory()
        {

        }
    }
}
