using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.EmailText_CultureInfoModule;

namespace PayMammoth.Cms.EmailText_CultureInfoModule
{

//UserCmsInfo-Class

    public class EmailText_CultureInfoCmsInfo : PayMammoth.Modules._AutoGen.EmailText_CultureInfoCmsInfo_AutoGen
    {
    	
        public EmailText_CultureInfoCmsInfo(BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
