using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.Product_CultureInfoModule;

namespace PayMammoth.Cms.Product_CultureInfoModule
{

//UserCmsInfo-Class

    public class Product_CultureInfoCmsInfo : PayMammoth.Modules._AutoGen.Product_CultureInfoCmsInfo_AutoGen
    {
    	
        public Product_CultureInfoCmsInfo(BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
