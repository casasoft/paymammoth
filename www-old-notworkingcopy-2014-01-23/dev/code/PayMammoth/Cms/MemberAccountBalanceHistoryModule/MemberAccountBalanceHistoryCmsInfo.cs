using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.MemberAccountBalanceHistoryModule;

namespace PayMammoth.Cms.MemberAccountBalanceHistoryModule
{

//UserCmsInfo-Class

    public class MemberAccountBalanceHistoryCmsInfo : PayMammoth.Modules._AutoGen.MemberAccountBalanceHistoryCmsInfo_AutoGen
    {
    	
        public MemberAccountBalanceHistoryCmsInfo(BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
