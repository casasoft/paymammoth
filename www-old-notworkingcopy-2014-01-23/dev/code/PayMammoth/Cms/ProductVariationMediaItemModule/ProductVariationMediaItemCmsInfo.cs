using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.ProductVariationMediaItemModule;

namespace PayMammoth.Cms.ProductVariationMediaItemModule
{

//UserCmsInfo-Class

    public class ProductVariationMediaItemCmsInfo : PayMammoth.Modules._AutoGen.ProductVariationMediaItemCmsInfo_AutoGen
    {
    	
        public ProductVariationMediaItemCmsInfo(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
