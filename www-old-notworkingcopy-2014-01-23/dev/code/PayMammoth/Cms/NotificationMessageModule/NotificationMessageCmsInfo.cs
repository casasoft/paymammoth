using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.NotificationMessageModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace PayMammoth.Cms.NotificationMessageModule
{

//UserCmsInfo-Class

    public class NotificationMessageCmsInfo : PayMammoth.Modules._AutoGen.NotificationMessageCmsInfo_AutoGen
    {
    	
        public NotificationMessageCmsInfo(PayMammoth.Modules._AutoGen.NotificationMessageBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
