using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Cms.NotificationMessageModule
{                          

//CmsFactory-Class

    public class NotificationMessageCmsFactory : PayMammoth.Modules._AutoGen.NotificationMessageCmsFactory_AutoGen
    {
        
        
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
            addCustomButtons();
        }
        
        protected NotificationMessageCmsFactory()
        {

        }
        private void addCustomButtons()
        {
            addButton_SendTestNotification();
        }
        private void addButton_SendTestNotification()
        {
            string url = "/cms/NotificationMessage/sendTestNotificationMsg.aspx";
            CmsGeneralOperation op = new CmsGeneralOperation("Send Test Notification Msg", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Update,
                url);
            this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
        }
    }
}
