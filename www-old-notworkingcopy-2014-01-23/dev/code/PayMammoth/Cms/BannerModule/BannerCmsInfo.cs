using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.BannerModule;

namespace PayMammoth.Cms.BannerModule
{

//UserCmsInfo-Class

    public class BannerCmsInfo : PayMammoth.Modules._AutoGen.BannerCmsInfo_AutoGen
    {
    	
        public BannerCmsInfo(BusinessLogic_v3.Modules.BannerModule.BannerBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
