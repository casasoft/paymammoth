using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.CmsAuditLogModule;

namespace PayMammoth.Cms.CmsAuditLogModule
{

//UserCmsInfo-Class

    public class CmsAuditLogCmsInfo : PayMammoth.Modules._AutoGen.CmsAuditLogCmsInfo_AutoGen
    {
    	
        public CmsAuditLogCmsInfo(BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
