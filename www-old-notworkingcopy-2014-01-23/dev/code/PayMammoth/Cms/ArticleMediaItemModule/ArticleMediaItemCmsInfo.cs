using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.ArticleMediaItemModule;

namespace PayMammoth.Cms.ArticleMediaItemModule
{

//UserCmsInfo-Class

    public class ArticleMediaItemCmsInfo : PayMammoth.Modules._AutoGen.ArticleMediaItemCmsInfo_AutoGen
    {
    	
        public ArticleMediaItemCmsInfo(BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
