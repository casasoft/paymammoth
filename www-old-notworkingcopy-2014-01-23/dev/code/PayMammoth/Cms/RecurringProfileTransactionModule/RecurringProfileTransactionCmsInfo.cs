using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.RecurringProfileTransactionModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace PayMammoth.Cms.RecurringProfileTransactionModule
{

//UserCmsInfo-Class

    public class RecurringProfileTransactionCmsInfo : PayMammoth.Modules._AutoGen.RecurringProfileTransactionCmsInfo_AutoGen
    {
    	
        public RecurringProfileTransactionCmsInfo(PayMammoth.Modules._AutoGen.RecurringProfileTransactionBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
