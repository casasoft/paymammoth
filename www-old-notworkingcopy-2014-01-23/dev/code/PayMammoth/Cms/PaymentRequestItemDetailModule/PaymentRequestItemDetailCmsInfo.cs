using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.PaymentRequestItemDetailModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace PayMammoth.Cms.PaymentRequestItemDetailModule
{

//UserCmsInfo-Class

    public class PaymentRequestItemDetailCmsInfo : PayMammoth.Modules._AutoGen.PaymentRequestItemDetailCmsInfo_AutoGen
    {
    	
        public PaymentRequestItemDetailCmsInfo(PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here
            this.Title.ShowInListing = true;
            this.Description.ShowInListing = true;
            base.customInitFields();
        }
    }
}
