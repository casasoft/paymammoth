using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.ContentTextModule;

namespace PayMammoth.Cms.ContentTextModule
{

//UserCmsInfo-Class

    public class ContentTextCmsInfo : PayMammoth.Modules._AutoGen.ContentTextCmsInfo_AutoGen
    {
    	
        public ContentTextCmsInfo(BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
