using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.ContactFormModule;

namespace PayMammoth.Cms.ContactFormModule
{

//UserCmsInfo-Class

    public class ContactFormCmsInfo : PayMammoth.Modules._AutoGen.ContactFormCmsInfo_AutoGen
    {
    	
        public ContactFormCmsInfo(BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
