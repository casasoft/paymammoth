using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.SettingModule;

namespace PayMammoth.Cms.SettingModule
{

//UserCmsInfo-Class

    public class SettingCmsInfo : PayMammoth.Modules._AutoGen.SettingCmsInfo_AutoGen
    {
    	
        public SettingCmsInfo(BusinessLogic_v3.Modules.SettingModule.SettingBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
