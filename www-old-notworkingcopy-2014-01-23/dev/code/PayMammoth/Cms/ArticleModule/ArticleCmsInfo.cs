using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.ArticleModule;

namespace PayMammoth.Cms.ArticleModule
{

//UserCmsInfo-Class

    public class ArticleCmsInfo : PayMammoth.Modules._AutoGen.ArticleCmsInfo_AutoGen
    {
    	
        public ArticleCmsInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
