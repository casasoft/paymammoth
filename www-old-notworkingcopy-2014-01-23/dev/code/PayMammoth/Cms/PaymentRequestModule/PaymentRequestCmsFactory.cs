using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Cms.PaymentRequestModule
{                          

//CmsFactory-Class

    public class PaymentRequestCmsFactory : PayMammoth.Modules._AutoGen.PaymentRequestCmsFactory_AutoGen
    {
        
        
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.
            cmsInfo.ShowInCmsMainMenu = true;
            cmsInfo.ShowAddNewButton = false;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        protected PaymentRequestCmsFactory()
        {

        }
    }
}
