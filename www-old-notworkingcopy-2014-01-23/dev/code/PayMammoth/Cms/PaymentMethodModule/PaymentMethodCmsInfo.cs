using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.PaymentMethodModule;

namespace PayMammoth.Cms.PaymentMethodModule
{

//UserCmsInfo-Class

    public class PaymentMethodCmsInfo : PayMammoth.Modules._AutoGen.PaymentMethodCmsInfo_AutoGen
    {
    	
        public PaymentMethodCmsInfo(PayMammoth.Modules._AutoGen.PaymentMethodBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            addIconField();
            this.Title.ShowInListing = true;
            this.PaymentMethod.ShowInListing = true;
            this.Icon.ShowInListing = true;
            this.Description.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.Priority.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(this.Priority, this.PaymentMethod, this.Icon,this.Title);
            this.EditOrderPriorities.AddColumnsToStart(this.PaymentMethod, this.Title, this.Description, this.Icon);
            this.ImageFilename.ShowInEdit = false;
        }


        private void addIconField()
        {
            this.Icon= this.AddPropertyForMediaItem<PaymentMethod>(x => x.Icon, showInListing: true);

        }
        private CmsPropertyMediaItem<PaymentMethod> Icon { get; set; }
    }
}
