﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CS.General_v3.Classes.Attributes;

namespace PayMammoth.PaymentMethods.Transactium
{
    public static class EnumsTransactium
    {

        public enum CHECK_PAYMENT_RESPONSE
        {
            Success,
            Fail,
            Cancelled
        }

        /// <remarks/>
        [System.FlagsAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4016")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.transactium.com")]
        public enum FSBlockReasonType
        {

            /// <remarks/>
            NotBlocked = 1,

            /// <remarks/>
            CVVConditioning = 2,

            /// <remarks/>
            ClientReferenceWhitelist = 4,

            /// <remarks/>
            CardNumberWhitelist = 8,

            /// <remarks/>
            BINWhitelist = 16,

            /// <remarks/>
            ClientReferenceBlacklist = 32,

            /// <remarks/>
            CardNumberBlacklist = 64,

            /// <remarks/>
            EmailBlacklist = 128,

            /// <remarks/>
            CountryBlacklist = 256,

            /// <remarks/>
            CountryGreylist = 512,

            /// <remarks/>
            CardSpendLimit = 1024,

            /// <remarks/>
            ClientRefSpendLimit = 2048,

            /// <remarks/>
            TransactionLimit = 4096,

            /// <remarks/>
            VelocityLimit = 8192,

            /// <remarks/>
            LocationMismatch = 16384,

            /// <remarks/>
            NewCardCheck = 32768,

            /// <remarks/>
            NewClientReferenceCheck = 65536,

            /// <remarks/>
            USCountryGamingBlock = 131072,
        }
        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4016")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.transactium.com")]
        public enum TransactionStatusType
        {

            /// <remarks/>
            Authorised,

            /// <remarks/>
            Declined,

            /// <remarks/>
            Blocked,

            /// <remarks/>
            Error,
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4016")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.transactium.com")]
        public enum CVV2CheckResponseType
        {

            /// <remarks/>
            NotSet,

            /// <remarks/>
            NotChecked,

            /// <remarks/>
            NotMatched,

            /// <remarks/>
            Matched,
        }



        public static CountryCodes? GetCountryCodeFromString(string s)
        {
            return CS.General_v3.Util.EnumUtils.GetEnumByStringValueNullable<CountryCodes>(s);
        }
        public static CountryCodes? GetCountryCodeFromISO(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? country)
        {
            string s = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode(country);

            return GetCountryCodeFromString(s);
        }


        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4016")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.transactium.com")]
        public enum CountryCodes
        {

            /// <remarks/>
            NotSet,

            /// <remarks/>
            AD,

            /// <remarks/>
            AE,

            /// <remarks/>
            AF,

            /// <remarks/>
            AG,

            /// <remarks/>
            AI,

            /// <remarks/>
            AL,

            /// <remarks/>
            AM,

            /// <remarks/>
            AN,

            /// <remarks/>
            AO,

            /// <remarks/>
            AQ,

            /// <remarks/>
            AR,

            /// <remarks/>
            AS,

            /// <remarks/>
            AT,

            /// <remarks/>
            AU,

            /// <remarks/>
            AW,

            /// <remarks/>
            AZ,

            /// <remarks/>
            BA,

            /// <remarks/>
            BB,

            /// <remarks/>
            BD,

            /// <remarks/>
            BE,

            /// <remarks/>
            BF,

            /// <remarks/>
            BG,

            /// <remarks/>
            BH,

            /// <remarks/>
            BI,

            /// <remarks/>
            BJ,

            /// <remarks/>
            BM,

            /// <remarks/>
            BN,

            /// <remarks/>
            BO,

            /// <remarks/>
            BR,

            /// <remarks/>
            BS,

            /// <remarks/>
            BT,

            /// <remarks/>
            BV,

            /// <remarks/>
            BW,

            /// <remarks/>
            BY,

            /// <remarks/>
            BZ,

            /// <remarks/>
            CA,

            /// <remarks/>
            CC,

            /// <remarks/>
            CD,

            /// <remarks/>
            CF,

            /// <remarks/>
            CG,

            /// <remarks/>
            CH,

            /// <remarks/>
            CI,

            /// <remarks/>
            CK,

            /// <remarks/>
            CL,

            /// <remarks/>
            CM,

            /// <remarks/>
            CN,

            /// <remarks/>
            CO,

            /// <remarks/>
            CR,

            /// <remarks/>
            CU,

            /// <remarks/>
            CV,

            /// <remarks/>
            CX,

            /// <remarks/>
            CY,

            /// <remarks/>
            CZ,

            /// <remarks/>
            DE,

            /// <remarks/>
            DJ,

            /// <remarks/>
            DK,

            /// <remarks/>
            DM,

            /// <remarks/>
            DO,

            /// <remarks/>
            DZ,

            /// <remarks/>
            EC,

            /// <remarks/>
            EE,

            /// <remarks/>
            EG,

            /// <remarks/>
            EH,

            /// <remarks/>
            ER,

            /// <remarks/>
            ES,

            /// <remarks/>
            ET,

            /// <remarks/>
            FI,

            /// <remarks/>
            FJ,

            /// <remarks/>
            FK,

            /// <remarks/>
            FM,

            /// <remarks/>
            FO,

            /// <remarks/>
            FR,

            /// <remarks/>
            FX,

            /// <remarks/>
            GA,

            /// <remarks/>
            GB,

            /// <remarks/>
            GD,

            /// <remarks/>
            GE,

            /// <remarks/>
            GF,

            /// <remarks/>
            GH,

            /// <remarks/>
            GI,

            /// <remarks/>
            GL,

            /// <remarks/>
            GM,

            /// <remarks/>
            GN,

            /// <remarks/>
            GP,

            /// <remarks/>
            GQ,

            /// <remarks/>
            GR,

            /// <remarks/>
            GS,

            /// <remarks/>
            GT,

            /// <remarks/>
            GU,

            /// <remarks/>
            GW,

            /// <remarks/>
            GY,

            /// <remarks/>
            HK,

            /// <remarks/>
            HM,

            /// <remarks/>
            HN,

            /// <remarks/>
            HR,

            /// <remarks/>
            HT,

            /// <remarks/>
            HU,

            /// <remarks/>
            ID,

            /// <remarks/>
            IE,

            /// <remarks/>
            IL,

            /// <remarks/>
            IN,

            /// <remarks/>
            IO,

            /// <remarks/>
            IQ,

            /// <remarks/>
            IR,

            /// <remarks/>
            IS,

            /// <remarks/>
            IT,

            /// <remarks/>
            JM,

            /// <remarks/>
            JO,

            /// <remarks/>
            JP,

            /// <remarks/>
            KE,

            /// <remarks/>
            KG,

            /// <remarks/>
            KH,

            /// <remarks/>
            KI,

            /// <remarks/>
            KM,

            /// <remarks/>
            KN,

            /// <remarks/>
            KP,

            /// <remarks/>
            KR,

            /// <remarks/>
            KW,

            /// <remarks/>
            KY,

            /// <remarks/>
            KZ,

            /// <remarks/>
            LA,

            /// <remarks/>
            LB,

            /// <remarks/>
            LC,

            /// <remarks/>
            LI,

            /// <remarks/>
            LK,

            /// <remarks/>
            LR,

            /// <remarks/>
            LS,

            /// <remarks/>
            LT,

            /// <remarks/>
            LU,

            /// <remarks/>
            LV,

            /// <remarks/>
            LY,

            /// <remarks/>
            MA,

            /// <remarks/>
            MC,

            /// <remarks/>
            MD,

            /// <remarks/>
            MG,

            /// <remarks/>
            MH,

            /// <remarks/>
            MK,

            /// <remarks/>
            ML,

            /// <remarks/>
            MM,

            /// <remarks/>
            MN,

            /// <remarks/>
            MO,

            /// <remarks/>
            MP,

            /// <remarks/>
            MQ,

            /// <remarks/>
            MR,

            /// <remarks/>
            MS,

            /// <remarks/>
            MT,

            /// <remarks/>
            MU,

            /// <remarks/>
            MV,

            /// <remarks/>
            MW,

            /// <remarks/>
            MX,

            /// <remarks/>
            MY,

            /// <remarks/>
            MZ,

            /// <remarks/>
            NA,

            /// <remarks/>
            NC,

            /// <remarks/>
            NE,

            /// <remarks/>
            NF,

            /// <remarks/>
            NG,

            /// <remarks/>
            NI,

            /// <remarks/>
            NL,

            /// <remarks/>
            NO,

            /// <remarks/>
            NP,

            /// <remarks/>
            NR,

            /// <remarks/>
            NU,

            /// <remarks/>
            NZ,

            /// <remarks/>
            OM,

            /// <remarks/>
            PA,

            /// <remarks/>
            PE,

            /// <remarks/>
            PF,

            /// <remarks/>
            PG,

            /// <remarks/>
            PH,

            /// <remarks/>
            PK,

            /// <remarks/>
            PL,

            /// <remarks/>
            PM,

            /// <remarks/>
            PN,

            /// <remarks/>
            PR,

            /// <remarks/>
            PS,

            /// <remarks/>
            PT,

            /// <remarks/>
            PW,

            /// <remarks/>
            PY,

            /// <remarks/>
            QA,

            /// <remarks/>
            RE,

            /// <remarks/>
            RO,

            /// <remarks/>
            RU,

            /// <remarks/>
            RW,

            /// <remarks/>
            SA,

            /// <remarks/>
            SB,

            /// <remarks/>
            SC,

            /// <remarks/>
            SD,

            /// <remarks/>
            SE,

            /// <remarks/>
            SG,

            /// <remarks/>
            SH,

            /// <remarks/>
            SI,

            /// <remarks/>
            SJ,

            /// <remarks/>
            SK,

            /// <remarks/>
            SL,

            /// <remarks/>
            SM,

            /// <remarks/>
            SN,

            /// <remarks/>
            SO,

            /// <remarks/>
            SR,

            /// <remarks/>
            ST,

            /// <remarks/>
            SV,

            /// <remarks/>
            SY,

            /// <remarks/>
            SZ,

            /// <remarks/>
            TC,

            /// <remarks/>
            TD,

            /// <remarks/>
            TF,

            /// <remarks/>
            TG,

            /// <remarks/>
            TH,

            /// <remarks/>
            TJ,

            /// <remarks/>
            TK,

            /// <remarks/>
            TL,

            /// <remarks/>
            TM,

            /// <remarks/>
            TN,

            /// <remarks/>
            TO,

            /// <remarks/>
            TR,

            /// <remarks/>
            TT,

            /// <remarks/>
            TV,

            /// <remarks/>
            TW,

            /// <remarks/>
            TZ,

            /// <remarks/>
            UA,

            /// <remarks/>
            UG,

            /// <remarks/>
            UM,

            /// <remarks/>
            US,

            /// <remarks/>
            UY,

            /// <remarks/>
            UZ,

            /// <remarks/>
            VA,

            /// <remarks/>
            VC,

            /// <remarks/>
            VE,

            /// <remarks/>
            VG,

            /// <remarks/>
            VI,

            /// <remarks/>
            VN,

            /// <remarks/>
            VU,

            /// <remarks/>
            WF,

            /// <remarks/>
            WS,

            /// <remarks/>
            YE,

            /// <remarks/>
            YT,

            /// <remarks/>
            YU,

            /// <remarks/>
            ZA,

            /// <remarks/>
            ZM,

            /// <remarks/>
            ZW,
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4016")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.transactium.com")]
        public enum HPStatus
        {

            /// <remarks/>
            Initialised,

            /// <remarks/>
            InProgress,

            /// <remarks/>
            Cancelled,

            /// <remarks/>
            Completed,

            /// <remarks/>
            Expired,

            /// <remarks/>
            Abandoned,

            /// <remarks/>
            Error,
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4016")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.transactium.com")]
        public enum UserActivity
        {

            /// <remarks/>
            Initialised,

            /// <remarks/>
            ChoosingPayment,

            /// <remarks/>
            SubmittingPayment,

            /// <remarks/>
            RedirectedToThirdParty,

            /// <remarks/>
            ProcessingTransaction,

            /// <remarks/>
            TransactionProcessed,
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4016")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.transactium.com")]
        public enum PaymentMethodType
        {

            /// <remarks/>
            NotSet,

            /// <remarks/>
            CardPayment,
        }

        public static CurrencyCode GetCurrencyCodeFromString(string s)
        {
            return CS.General_v3.Util.EnumUtils.GetEnumByStringValue<CurrencyCode>(s, CurrencyCode.EUR);
        }
        public static CurrencyCode GetCurrencyCodeFromISO(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 currency)
        {
            string s = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode(currency);

            return GetCurrencyCodeFromString(s);
        }

        
        public static TSpecific ConvertFromGeneralTransactiumEnumToSpecific<TSpecific>(Enum general)
             where TSpecific : struct, IConvertible
        {
            string s = CS.General_v3.Util.EnumUtils.StringValueOf(general);

            return CS.General_v3.Util.EnumUtils.GetEnumByStringValue<TSpecific>(s, default(TSpecific));

        }

        public static TGeneral ConvertFromSpecificTransactiumEnumToGeneral<TGeneral>(Enum specific)
             where TGeneral : struct, IConvertible
        {
            string s = CS.General_v3.Util.EnumUtils.StringValueOf(specific);

            return CS.General_v3.Util.EnumUtils.GetEnumByStringValue<TGeneral>(s, default(TGeneral));

        }
        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4016")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.transactium.com")]
        public enum CurrencyCode
        {

            /// <remarks/>
            AED,

            /// <remarks/>
            AFA,

            /// <remarks/>
            ALL,

            /// <remarks/>
            AMD,

            /// <remarks/>
            ANG,

            /// <remarks/>
            AOA,

            /// <remarks/>
            ARS,

            /// <remarks/>
            AUD,

            /// <remarks/>
            AWG,

            /// <remarks/>
            AZM,

            /// <remarks/>
            BAM,

            /// <remarks/>
            BBD,

            /// <remarks/>
            BDT,

            /// <remarks/>
            BGL,

            /// <remarks/>
            BHD,

            /// <remarks/>
            BIF,

            /// <remarks/>
            BMD,

            /// <remarks/>
            BND,

            /// <remarks/>
            BOB,

            /// <remarks/>
            BRL,

            /// <remarks/>
            BSD,

            /// <remarks/>
            BTN,

            /// <remarks/>
            BWP,

            /// <remarks/>
            BYR,

            /// <remarks/>
            BZD,

            /// <remarks/>
            CAD,

            /// <remarks/>
            CDF,

            /// <remarks/>
            CHF,

            /// <remarks/>
            CLP,

            /// <remarks/>
            CNY,

            /// <remarks/>
            COP,

            /// <remarks/>
            CRC,

            /// <remarks/>
            CUP,

            /// <remarks/>
            CVE,

            /// <remarks/>
            CZK,

            /// <remarks/>
            DJF,

            /// <remarks/>
            DKK,

            /// <remarks/>
            DOP,

            /// <remarks/>
            DZD,

            /// <remarks/>
            ECS,

            /// <remarks/>
            EEK,

            /// <remarks/>
            EGP,

            /// <remarks/>
            ERN,

            /// <remarks/>
            ETB,

            /// <remarks/>
            EUR,

            /// <remarks/>
            FJD,

            /// <remarks/>
            FKP,

            /// <remarks/>
            GBP,

            /// <remarks/>
            GEL,

            /// <remarks/>
            GHC,

            /// <remarks/>
            GIP,

            /// <remarks/>
            GMD,

            /// <remarks/>
            GNF,

            /// <remarks/>
            GQE,

            /// <remarks/>
            GTQ,

            /// <remarks/>
            GYD,

            /// <remarks/>
            HKD,

            /// <remarks/>
            HNL,

            /// <remarks/>
            HRK,

            /// <remarks/>
            HTG,

            /// <remarks/>
            HUF,

            /// <remarks/>
            IDR,

            /// <remarks/>
            ILS,

            /// <remarks/>
            INR,

            /// <remarks/>
            IQD,

            /// <remarks/>
            IRR,

            /// <remarks/>
            ISK,

            /// <remarks/>
            JMD,

            /// <remarks/>
            JOD,

            /// <remarks/>
            JPY,

            /// <remarks/>
            KES,

            /// <remarks/>
            KGS,

            /// <remarks/>
            KHR,

            /// <remarks/>
            KMF,

            /// <remarks/>
            KPW,

            /// <remarks/>
            KRW,

            /// <remarks/>
            KWD,

            /// <remarks/>
            KYD,

            /// <remarks/>
            KZT,

            /// <remarks/>
            LAK,

            /// <remarks/>
            LBP,

            /// <remarks/>
            LKR,

            /// <remarks/>
            LRD,

            /// <remarks/>
            LSL,

            /// <remarks/>
            LTL,

            /// <remarks/>
            LVL,

            /// <remarks/>
            LYD,

            /// <remarks/>
            MAD,

            /// <remarks/>
            MDL,

            /// <remarks/>
            MGF,

            /// <remarks/>
            MKD,

            /// <remarks/>
            MMK,

            /// <remarks/>
            MNT,

            /// <remarks/>
            MOP,

            /// <remarks/>
            MRO,

            /// <remarks/>
            MUR,

            /// <remarks/>
            MVR,

            /// <remarks/>
            MWK,

            /// <remarks/>
            MXN,

            /// <remarks/>
            MYR,

            /// <remarks/>
            MZM,

            /// <remarks/>
            NAD,

            /// <remarks/>
            NGN,

            /// <remarks/>
            NIO,

            /// <remarks/>
            NOK,

            /// <remarks/>
            NPR,

            /// <remarks/>
            NZD,

            /// <remarks/>
            OMR,

            /// <remarks/>
            PAB,

            /// <remarks/>
            PEN,

            /// <remarks/>
            PGK,

            /// <remarks/>
            PHP,

            /// <remarks/>
            PKR,

            /// <remarks/>
            PLN,

            /// <remarks/>
            PYG,

            /// <remarks/>
            QAR,

            /// <remarks/>
            ROL,

            /// <remarks/>
            RUB,

            /// <remarks/>
            RWF,

            /// <remarks/>
            SAR,

            /// <remarks/>
            SBD,

            /// <remarks/>
            SCR,

            /// <remarks/>
            SDP,

            /// <remarks/>
            SEK,

            /// <remarks/>
            SGD,

            /// <remarks/>
            SHP,

            /// <remarks/>
            SKK,

            /// <remarks/>
            SLL,

            /// <remarks/>
            SOS,

            /// <remarks/>
            SRD,

            /// <remarks/>
            STD,

            /// <remarks/>
            SVC,

            /// <remarks/>
            SYP,

            /// <remarks/>
            SZL,

            /// <remarks/>
            THB,

            /// <remarks/>
            TJS,

            /// <remarks/>
            TMM,

            /// <remarks/>
            TND,

            /// <remarks/>
            TOP,

            /// <remarks/>
            TRL,

            /// <remarks/>
            TTD,

            /// <remarks/>
            TWD,

            /// <remarks/>
            TZS,

            /// <remarks/>
            UAH,

            /// <remarks/>
            UGX,

            /// <remarks/>
            USD,

            /// <remarks/>
            UYU,

            /// <remarks/>
            UZS,

            /// <remarks/>
            VND,

            /// <remarks/>
            VUV,

            /// <remarks/>
            WST,

            /// <remarks/>
            XAF,

            /// <remarks/>
            XCD,

            /// <remarks/>
            XDR,

            /// <remarks/>
            XOF,

            /// <remarks/>
            XPF,

            /// <remarks/>
            YER,

            /// <remarks/>
            YUM,

            /// <remarks/>
            ZAR,

            /// <remarks/>
            ZMK,

            /// <remarks/>
            ZWD,
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4016")]
        [System.SerializableAttribute()]
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.transactium.com")]
        public enum HPSPaymentType
        {

            /// <remarks/>
            Sale,

            /// <remarks/>
            PreAuthorisation,
        }
    }
}
