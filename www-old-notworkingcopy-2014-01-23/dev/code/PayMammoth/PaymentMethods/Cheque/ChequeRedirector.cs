﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Interfaces;
using CS.General_v3.Classes.HelperClasses;
using PayMammoth.Modules.PaymentRequestModule;

namespace PayMammoth.PaymentMethods.Cheque
{
    public class ChequeRedirector : IPaymentRedirector
    {


        #region IPaymentRedirector Members

        public Enums.RedirectionResult Redirect(IPaymentRequest request)
        {
            Enums.RedirectionResult result = Enums.RedirectionResult.Success;
            

            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(request, "Request is required");

            
            string url = Urls.Url_Payment_Cheque_Info;
            CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass(url);
            urlClass[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = request.Identifier;
            urlClass.RedirectTo();

            return result;


        }

        #endregion
    }
}
