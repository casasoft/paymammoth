﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.PayPal.v1
{
    public static class PayPalUrls
    {
        public static string IpnHandlerUrl { get { return CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "payment/paypal/v1/ipn.ashx"; } }
        public static string ConfirmUrl { get { return CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "payment/paypal/v1/confirm.aspx"; } }

    }
}
