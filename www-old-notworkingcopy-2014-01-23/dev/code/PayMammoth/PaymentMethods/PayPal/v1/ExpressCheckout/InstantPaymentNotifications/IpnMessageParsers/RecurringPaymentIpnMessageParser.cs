﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.RecurringProfileModule;
using log4net;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers
{
    public class RecurringPaymentIpnMessageParser : IIpnMessageParser
    {

        private readonly ILog _log = LogManager.GetLogger(typeof(RecurringPaymentIpnMessageParser));
		
			
        #region IIpnMessageParser Members
         private static readonly RecurringPaymentIpnMessageParser _instance = new RecurringPaymentIpnMessageParser();

        public static RecurringPaymentIpnMessageParser Instance
        {
            get { return _instance; }
        }

        private RecurringPaymentIpnMessageParser()
        {

        }
       

        #endregion

        #region IIpnMessageParser Members

        private void parseMessage(IPNMessage ipnMessage,IRecurringProfile profile)
        {
            switch (ipnMessage.TransactionType)
            {
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentProfileCreated:
                    {
                        RecurringProfileService.Instance.ActivateProfile(profile,ipnMessage.MessageBlockRaw);
                    }
                    break;
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentCancelled:
                    {

                        RecurringProfileService.Instance.MarkProfileAsCancelled(profile, ipnMessage.MessageBlockRaw);
                        break;
                    }
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentSuspendedDueToMaxFailedPayment:
                    {
                        profile.SuspendProfile(ipnMessage.MessageBlockRaw);
                    }
                    break;
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentExpired:
                    {
                        RecurringProfileService.Instance.MarkProfileAsExpired(profile, ipnMessage.MessageBlockRaw);
                        
                    }
                    break;
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentReceived:
                    {
                        RecurringProfileService.Instance.MarkPaymentReceived(profile, ipnMessage.PayPalTransactionID, ipnMessage.MessageBlockRaw);
                    }
                    break;
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentFailed:
                    {
                        RecurringProfileService.Instance.MarkPaymentAsFailed(profile, ipnMessage.PayPalTransactionID, ipnMessage.MessageBlockRaw);
                    }
                    break;
                
                    
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentSkipped:
                    {
                        RecurringProfileService.Instance.MarkLastPaymentAsSkipped(profile, ipnMessage.PayPalTransactionID, ipnMessage.MessageBlockRaw);
                        
                        break;
                    }
                default:
                    throw new InvalidOperationException(string.Format("This parser is not meant for this type of message <{0}>", ipnMessage.TransactionType));
            }
        }

        public bool HandleIpnMessage(IPNMessage ipnMessage)
        {
            bool ok = false;
            string profileId = ipnMessage.RecurringPaymentPaymentId;
            IRecurringProfile profile = PayMammoth.Modules.Factories.RecurringProfileFactory.GetRecurringProfileByGatewayIdentifier(profileId, Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout);

            if (profile != null)
            {
                
                parseMessage(ipnMessage, profile);
                ok = true;
                
            }
            else
            {
                _log.WarnFormat("Could not locate profile for PayPal with gateway ID [{0}]", ipnMessage.RecurringPaymentPaymentId);
            }
            return ok;

            
        }

        #endregion
    }
}
