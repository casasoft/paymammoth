﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using PaymentGateways.v1.PayPal.Util;

namespace PaymentGateways.v1.PayPal.ExpressCheckout.NVP
{
    public class SetExpressCheckoutRequest : BasePaypalRequest
    {
        private PayPalClient _paypal = null;
        public SetExpressCheckoutRequest(PayPalClient payPal,
            double totalAmount, string returnURL, string cancelURL)
        {
            _paypal = payPal;
            this.PaymentAction = Enums.PAYMENT_ACTION.Sale;
                
            
            this.PaymentItems = new List<PaymentItem>();
            this.ClientShippingInfo = new ClientShippingInfo();
            this.TotalAmount = totalAmount;
            this.ReturnUrl = returnURL;
            this.CancelUrl = cancelURL;
            this.CurrencyCode = CS.General_20090518.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedStatesOfAmericaDollars;
            this.Language = Enums.LANGUAGE.EnglishUS;

        }
        public override string MethodName
        {
            get { return "SetExpressCheckout"; }
        }
        
        /// <summary>
        /// Expected maximum total amount of the complete order, including shipping cost and tax charges"
        /// </summary>
        public double? MaxAmount { get; set; }
        /// <summary>
        /// Return URL after payment. Required
        /// </summary>
        public string ReturnUrl { get; set; }
        /// <summary>
        /// Cancel URL if payment not done. Required
        /// </summary>
        public string CancelUrl { get; set; }
        /// <summary>
        /// (Optional) URL to which the callback request from PayPal is sent. It must start with
/// HTTPS for production integration. It can start with HTTPS or HTTP for sandbox
/// testing.
/// Character length and limitations: 1024 characters
        /// This field is available since version 53.0.
        /// </summary>
        public string CallBackUrl { get; set; } 
         
        /// <summary>
        /// (Optional) An override for you to request more or less time to be able to process the
///callback request and respond. The acceptable range for the override is 1 to 6 seconds.
///If you specify a value greater than 6, PayPal uses the default value of 3 seconds.
///Character length and limitations: An integer between 1 and 6
        /// </summary>
        public int? CallbackTimeout { get; set; }
        /// <summary>
        /// Whether you require customer to have a confirmed paypal shipping address
        /// </summary>
        public bool RequireConfirmedShippingAddress { get; set; }
        /// <summary>
        /// No shipping information is needed
        /// </summary>
        public bool DontDisplayShippingAddress { get; set; }
        /// <summary>
        /// Allow client to enter a note to merchant
        /// </summary>
        public bool AllowNoteClientToMerchant { get; set; }
        /// <summary>
        /// Override paypal address with the one you specify in ClientAddress1, ClientAddress2, etc.
        /// </summary>
        public bool OverrideAddress { get; set; }
        /// <summary>
        /// Language.  Default EnglishUS.
        /// </summary>
        public Enums.LANGUAGE Language { get; set; }
        public void SetLanguageFromCountry(CS.General_20090518.Enums.ISO_ENUMS.Country_ISO3166 country)
        {
            this.Language = Enums.LanguageFromCountryCode(country);
        }
        /// <summary>
        /// (Optional) Sets the Custom Payment Page Style for payment pages associated with this button/link. 
        /// This value corresponds to the HTML variable page_style for customizing payment pages. The value is 
        /// the same as the Page Style Name you chose when adding or editing the page style from the Profile subtab
        /// of the My Account tab of your PayPal account. Character length and limitations: 30 single-byte alphabetic characters.
        /// </summary>
        public string PageStyle { get; set; }
        /// <summary>
        /// URL of image to load in payment page. Max size of 750 x 90 pixels. Preferably on an HTTPS location
        /// </summary>
        public string ImageURL { get; set; }
        /// <summary>
        /// The header border color.  6-digit hex
        /// </summary>
        public string HeaderBorderColor { get; set; }
        /// <summary>
        /// The header background color: 6-digit hex.
        /// </summary>
        public string HeaderBackgroundColor { get; set; }
        /// <summary>
        /// Payment flow background color: 6-digit hex.
        /// </summary>
        public string PayFlowBackgroundColor { get; set; }
        
        /// <summary>
        /// Payment Action.  Default is SALE
        /// </summary>
      //  public Enums.PAYMENT_ACTION PaymentAction { get; set; } //todo - Deprecated 
        public string ClientEmail { get; set; }

        /// <summary>
        /// (Optional) Type of checkout flow:
/// Sole: Buyer does not need to create a PayPal account to check out. This is
///referred to as PayPal Account Optional.
/// Mark: Buyer must have a PayPal account to check out.
///NOTE: You can pass Mark to selectively override the PayPal Account Optional
///setting if PayPal Account Optional is turned on in your merchant account.
///Passing Sole has no effect if PayPal Account Optional is turned off in your
        ///account
        /// </summary>
        public bool PaypalAccountRequired { get; set; } //SolutionType

        /// <summary>
        /// Type of PayPal page to display:
        /// </summary>
        public Enums.LANDING_PAGE? LandingPageType { get; set; } //todo

        /// <summary>
        /// (Optional) A label that overrides the business name in the PayPal account on the
///PayPal hosted checkout pages.
///Character length and limitations: 127 single-byte alphanumeric characters
        /// </summary>
        public string BrandName { get; set; }//todo

        /// <summary>
        /// (Optional) Merchant Customer Service number displayed on the PayPal Review
///page.
///Limitations: 16 single-byte characters
        /// </summary>
        public string CustomerServiceNumber { get; set; } //todo

        public string ClientName { get; set; }
        public ClientShippingInfo ClientShippingInfo { get; set; }
        public string ClientPhoneNum { get; set; }
        /// <summary>
        /// Total cost of transaction to customer.  Includes shipping cost and tax if known.  If not, shoul dbe set to sub-total. Required.
        /// </summary>
        public double TotalAmount { get; set; }
        /// <summary>
        /// Currency code.  Default is USD
        /// </summary>
        public CS.General_20090518.Enums.ISO_ENUMS.CURRENCY_ISO4217 CurrencyCode { get; set; }
        /// <summary>
        /// Total Items amount, exc. shipping, tax and handling
        /// </summary>
        public double? ItemsAmount { get; set; }
        /// <summary>
        /// Shipping amount only
        /// </summary>
        public double? ShippingAmount { get; set; }
        /// <summary>
        /// Handling amount only
        /// </summary>
        public double? HandlingAmount { get; set; }
        /// <summary>
        /// Tax amount only
        /// </summary>
        public double? TaxAmount { get; set; }
        /// <summary>
        /// Description of item.  Max 127-characters
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Extra custom text about the item.  Max 256-characters
        /// </summary>
        public string CustomText { get; set; }
        /// <summary>
        /// Your own invoice number/reference.  Max 127-characters
        /// </summary>
        public string InvoiceNo { get; set; }
        /// <summary>
        /// A code used by third party applications to identify the transaction. Max 32-characters
        /// </summary>
        public string ButtonSource { get; set; }
        /// <summary>
        /// URL for receiving Instant Payment Notification (IPN) about the transaction.  Max 2,048 characters.
        /// </summary>
        public string NotifyURL { get; set; }
        /// <summary>
        /// A token that uniquely identifies this request for your user
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// A list of payment items corresponding to the order
        /// </summary>
        public List<PaymentItem> PaymentItems { get; set; }
        public void AddPaymentItem(string name, double Amount, int quantity, double tax)
        {
            PaymentItem item = new PaymentItem();
            item.Amount = Amount;
            item.Name = name;
            item.Quantity = quantity;
            item.Tax = tax;
            this.PaymentItems.Add(item);
        }
        private void checkRequirements()
        {
            StringBuilder errMsg = new StringBuilder();
            if (string.IsNullOrEmpty(ReturnUrl))
                errMsg.AppendLine("ReturnURL is required");
            if (string.IsNullOrEmpty(CancelUrl))
                errMsg.AppendLine("CancelURL is required");
            if (this.TotalAmount <= 0)
                errMsg.AppendLine("TotalAmount is required");
            if (!string.IsNullOrEmpty(errMsg.ToString()))
            {
                throw new InvalidOperationException(errMsg.ToString());
            }
        }

        public SetExpressCheckoutResponse GetResponse(bool autoRedirectOnSuccess = false)
        {
            checkRequirements();
            NameValueCollection nv = new NameValueCollection();
            _paypal.AppendAuthParametersToNV(nv);
            GeneralUtil.AddItemToNVForPaypal(nv, "method", this.MethodName);
            GeneralUtil.AddItemToNVForPaypal(nv, "token", Token);
            GeneralUtil.AddItemToNVForPaypal(nv, "callback", CallBackUrl);
            GeneralUtil.AddItemToNVForPaypal(nv, "CallBackTimeout", this.CallbackTimeout);
            GeneralUtil.AddItemToNVForPaypal(nv, "LandingPage", this.LandingPageType);
            GeneralUtil.AddItemToNVForPaypal(nv, "maxamt", MaxAmount);
            GeneralUtil.AddItemToNVForPaypal(nv, "BrandName", this.BrandName);
            GeneralUtil.AddItemToNVForPaypal(nv, "CustomerServiceNumber", this.CustomerServiceNumber);
            GeneralUtil.AddItemToNVForPaypal(nv, "returnurl", ReturnUrl);
            GeneralUtil.AddItemToNVForPaypal(nv, "cancelurl", CancelUrl);
            //General.AddItemToNVForPaypal(nv, "solutiontype", "mark");
            //General.AddItemToNVForPaypal(nv, "channeltype", "merchant");
            GeneralUtil.AddItemToNVForPaypal(nv, "ReqConfirmShipping", RequireConfirmedShippingAddress);
            GeneralUtil.AddItemToNVForPaypal(nv, "NoShipping", DontDisplayShippingAddress);
            GeneralUtil.AddItemToNVForPaypal(nv, "AllowNote", AllowNoteClientToMerchant);
            GeneralUtil.AddItemToNVForPaypal(nv, "AddressOverride", OverrideAddress);
            if (this.Language != null)
                GeneralUtil.AddItemToNVForPaypal(nv, "LocaleCode", Enums.LanguageToPayPalCode(this.Language));
            GeneralUtil.AddItemToNVForPaypal(nv, "HDRImg", ImageURL);
            GeneralUtil.AddItemToNVForPaypal(nv, "HDRBorderColor", HeaderBorderColor);
            GeneralUtil.AddItemToNVForPaypal(nv, "HDRBackColor", HeaderBackgroundColor);
            GeneralUtil.AddItemToNVForPaypal(nv, "PayFlowColor", PayFlowBackgroundColor);
            if (this.PaymentAction != null)
                GeneralUtil.AddItemToNVForPaypal(nv, "PaymentAction", Enums.PaymentActionToPayPalCode(PaymentAction));
            GeneralUtil.AddItemToNVForPaypal(nv, "Email", ClientEmail);
            GeneralUtil.AddItemToNVForPaypal(nv, "Name", ClientName);
            ClientShippingInfo.AppendToNameValue(nv);
            GeneralUtil.AddItemToNVForPaypal(nv, "PhoneNum", ClientPhoneNum);
            GeneralUtil.AddItemToNVForPaypal(nv, "Amt", this.TotalAmount);
            if (this.CurrencyCode != null)
                GeneralUtil.AddItemToNVForPaypal(nv, "CurrencyCode", CS.General_20090518.Enums.ISO_ENUMS.Currency_ISO4217ToCode(this.CurrencyCode));
            GeneralUtil.AddItemToNVForPaypal(nv, "ItemAmt", this.ItemsAmount);
            GeneralUtil.AddItemToNVForPaypal(nv, "ShippingAmt", this.ShippingAmount);
            GeneralUtil.AddItemToNVForPaypal(nv, "HandlingAmt", this.HandlingAmount);
            GeneralUtil.AddItemToNVForPaypal(nv, "TaxAmt", this.TaxAmount);
            GeneralUtil.AddItemToNVForPaypal(nv, "Desc", Description);
            GeneralUtil.AddItemToNVForPaypal(nv, "Custom", CustomText);
            GeneralUtil.AddItemToNVForPaypal(nv, "InvNum", InvoiceNo);
            GeneralUtil.AddItemToNVForPaypal(nv, "ButtonSource", ButtonSource);
            GeneralUtil.AddItemToNVForPaypal(nv, "NotifyURL", NotifyURL);
            for (int i = 0; i < this.PaymentItems.Count; i++)
            {
                PaymentItem p = this.PaymentItems[i];
                p.AppendToNameValue(nv, i);
            }


            string sResponse = CS.General_20090518.Util.Web.HTTPPost(_paypal.NVPServer, nv);
            SetExpressCheckoutResponse response = new SetExpressCheckoutResponse(_paypal);
            response.ParseFromString(sResponse);
            if (autoRedirectOnSuccess)
            {
                if (response.Success)
                    response.RedirectBrowserToPaypalCheckout();
            }
            return response;
        }
    }
        
}
