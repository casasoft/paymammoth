﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers;
using log4net;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Util;
using CS.General_v3.URL;
using PayMammoth.Classes.PaymentNotifications;
using PayMammoth.Classes.PaymentNotifications.RecurringNotifications;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public abstract class IPNHandler : CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers.BaseHandler, IHttpHandler
    {
        private ILog _log = LogManager.GetLogger(typeof(IPNHandler));
        private IPaymentRequest _request = null;

        private IIpnMessageParser getIpnMessageParserBasedOnTransactionType(PayPalEnums.TRANSACTION_TYPE transactionType)
        {
            switch (transactionType)
            {
                case PayPalEnums.TRANSACTION_TYPE.Cart:
                case PayPalEnums.TRANSACTION_TYPE.ExpressCheckout: return IpnMessageParsers.ExpressCheckoutIpnMessageParser.Instance;
                default:
                    {
                        _log.Warn(string.Format("No Ipn message parser found for transaction type <{0}>", transactionType));
                    }
                    break;
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentExpired:
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentProfileCreated:
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentCancelled:
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentReceived:
                case PayPalEnums.TRANSACTION_TYPE.RecurringPaymentSkipped:
                    return IpnMessageParsers.RecurringPaymentIpnMessageParser.Instance;
            }
            return null;
        }

        //protected virtual void parseSuccessfulIpnMessage(IpnResponse response)
        //{
        //    _log.Debug(string.Format("Received successful Ipn message - Transaction Type: {0} | Paypal Transaction Id: {1} | Ref: {2}",
        //        response.IpnMessage.TransactionType, response.IpnMessage.PayPalTransactionID, response.IpnMessage.Reference));

        //    var t = _request.CurrentTransaction;
        //    var msg = response.IpnMessage;
        //    var messageParser = getIpnMessageParserBasedOnResponseType(msg.TransactionType);
        //    if (messageParser != null)
        //    {
        //        messageParser.HandleIpnMessage(_request, response);
        //    }
            

        //}

        private void addIpnResponseToTransaction(QueryString qs, IpnResponse response)
        {

            qs.Add("Ipn PayPal Response", response.PaypalResponse);



        }
        //protected virtual void TransactionInvalid(IpnResponse response)
        //{
        //    IPaymentRequestTransaction t = _request.CurrentTransaction;

        //    t.AddMessage("Invalid Transaction", CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
        //    using (var nhTransaction = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
        //    {
        //        var qs = t.GetParametersAsQueryString();

        //        addIpnResponseToTransaction(qs, response);
        //        t.SetPaymentParameters(qs);
        //        nhTransaction.Commit();

        //    }
        //}

        private string getFormContents(HttpContext context)
        {
            Encoding encoding = Encoding.UTF8;
            byte[] formContentBy = context.Request.BinaryRead(context.Request.ContentLength);
            string formContents = encoding.GetString(formContentBy);

            return formContents;
        }

        private static readonly ConcurrentDictionary<string, bool> _processedPayPalIpnTransactionIds = new ConcurrentDictionary<string, bool>();

      
        private void parseIpnMessage(string formContents, HttpContext context)
        {
            

            string qsContents = context.Request.QueryString.ToString();
            QueryString formContentsParsed = new QueryString(formContents);
            //string formContents = context.Request.Form.ToString();
            //formContents = CS.General_v3.Util.ListUtil.ConvertNameValueCollectionToString(context.Request.Form);

            if (_log.IsDebugEnabled)
            {
                _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(_request, "ProcessRequest() - Started. Querystring: " + qsContents + ", Form: " + formContents));
            }

            //context.Response.Clear();

            IPNMessage ipnMessage = new IPNMessage(formContents);
            bool validationOk = false;
            
            ipnMessage.FillNameValueCollection(formContentsParsed, prepend: null, append: null);
            var validateResult = IPNUtil.ValidateIpnMessage(ipnMessage);

            if (validateResult.Success)
            {
                validationOk = true;

                if (!string.IsNullOrWhiteSpace(ipnMessage.IpnTrackId) && !_processedPayPalIpnTransactionIds.ContainsKey(ipnMessage.IpnTrackId))
                {

                    //TODO: [For: Karl | 13 Nov 2012] These should be stored in database (WrittenBy: Karl)        			
			
                    _processedPayPalIpnTransactionIds[ipnMessage.IpnTrackId] = true; 
                    var msgParser = getIpnMessageParserBasedOnTransactionType(ipnMessage.TransactionType);
                    if (msgParser != null)
                    {
                        if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(_request, "ProcessRequest() - Handling Message"));
                        bool ok = msgParser.HandleIpnMessage(ipnMessage);
                        if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(_request, "ProcessRequest() - Handling Message - Result: " + ok));

                        if (ok)
                        {
                            if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(_request, "ProcessRequest() - IPN_OK"));


                               

                            if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(_request, "ProcessRequest() - IPN_OK, Ready'"));
                        }
                        else
                        {

                            if (_log.IsWarnEnabled) _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(_request, "ProcessRequest() - IPN_FAIL"));


                                

                            if (_log.IsWarnEnabled) _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(_request, "ProcessRequest() - IPN_FAIL, Ready"));
                        }
                        if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(_request, "ProcessRequest() - Finished"));

                    }
                    else
                    {
                        _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(_request, "ProcessRequest() - Could not process IPN message as no message parser exists. Form contents: " + formContents));

                    }
                }
            }
            else
            {
                _log.WarnFormat(LogUtil.AddRequestIdentifierToLogMsg(_request, "ProcessRequest() - Could not validate IPN message - Form: " + formContents));
            }
            if (validationOk)
            {
                context.Response.Write(PayPalConstants.IPN_OK);
            }
            else
            {
                context.Response.Write(PayPalConstants.IPN_FAIL);
            }
            
            context.Response.StatusCode = 200;
            context.Response.Flush();
            context.Response.Close();
            context.Response.End();
        }

        protected override void processRequest(HttpContext context)
        {
            string formContents = getFormContents(context);


            _log.Debug(string.Format("IPN Handler. Form: {0}   || QueryString: {1}", formContents, context.Request.QueryString.ToString()));


            if (CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                //System.Threading.Thread.Sleep(5000);
            }
            _request = QuerystringData.GetPaymentRequestFromQuerystring();


            
            parseIpnMessage(formContents, context);
           // context.Response.End();
            //}
            //else
            //{
            //    checkIfRecurringPayment(formContents);
            //}
        }

        //private bool checkIfRecurringPayment(string contents)
        //{
        //    bool isRecurringPaymentNotification = false;
        //    QueryString qs = new QueryString(contents);

        //    string paymentStatusStr = qs["payment_status"];
        //    PayPalEnums.PAYMENT_STATUS paymentStatus;
        //    if (Enum.TryParse<PayPalEnums.PAYMENT_STATUS>(paymentStatusStr, out paymentStatus))
        //    {
        //        string recurringPaymentId = qs["recurring_payment_id"];
        //        if (!string.IsNullOrWhiteSpace(recurringPaymentId))
        //        {
        //            RecurringNotificationDetails notification = new RecurringNotificationDetails();
        //            notification.RecurringPaymentId = recurringPaymentId;
        //            notification.PaymentStatus = (paymentStatus == PayPalEnums.PAYMENT_STATUS.Completed) 
        //                                                ? Connector.Enums.RECURRING_PAYMENT_NOTIFICATION_STATUS.Completed
        //                                             : Connector.Enums.RECURRING_PAYMENT_NOTIFICATION_STATUS.Failed;
        //            notification.PaymentMethod = Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout;
        //            notification.ResponseContent = contents;
        //            RecurringNotificationsHandler.Instance.ProcessRecurringNotification(notification);
        //            isRecurringPaymentNotification = true;
        //        }
        //    }
        //    return isRecurringPaymentNotification;
        //}

        public override bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}
