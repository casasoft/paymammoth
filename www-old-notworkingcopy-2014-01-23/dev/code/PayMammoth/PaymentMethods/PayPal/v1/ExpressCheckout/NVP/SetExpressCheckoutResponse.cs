﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP
{
    public class SetExpressCheckoutResponse : BasePayPalResponse
    {
        private PayPalClient _paypal = null;
        public SetExpressCheckoutResponse(PayPalClient paypal)
        {
            _paypal = paypal;
        }
        /// <summary>
        /// A token that uniquely identifiers this response for your user.  If the token was filled in in the request, the same
        /// token is returned
        /// </summary>
        [PayPalFieldInfo]
        public string Token { get; set; }
        public void RedirectBrowserToPaypalCheckout()
        {
            CS.General_v3.URL.QueryString nv = new CS.General_v3.URL.QueryString();
            nv[PayPalConstants.PARAM_TOKEN] = Token;
            nv[PayPalConstants.PARAM_CMD] = PayPalConstants.COMMAND_EXPRESS_CHECKOUT;
            var currentSettings = Extensions.IPaypalSettingsExtensions.GetCurrentSettings(this._paypal.Settings);
            string url = currentSettings.ApiServerUrl + "?" + nv.ToString();
            CS.General_v3.Util.PageUtil.RedirectPage(url);
        }
        public override void ParseFromNameValueCollection(NameValueCollection nv)
        {
            base.ParseFromNameValueCollection(nv);
        }
        
    }
        
        
}
