﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.PayPal.v1.Util;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP
{
    /// <summary>
    /// Deprecated
    /// </summary>
    public class AddressTypeInfo
    {

        public string ClientName { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientAddress1 { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientAddress2 { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientCity { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientState { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientZIPCode { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientCountry { get; set; }
        /// <summary>
        /// Deprecated
        /// </summary>
        public string ClientCountryName { get; set; }

        public string ShipToPhoneNum { get; set; }
        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 ClientCountryCode { get; set; }

        public void AppendToNameValue(NameValueCollection nv, int id)
        {
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToName", this.ClientName);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToStreet", this.ClientAddress1);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToStreet2", ClientAddress2);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToCity", ClientCity);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToState", ClientState);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToZIP", ClientZIPCode);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToCountry", ClientCountry);
            GeneralUtil.AddItemToNVForPaypal(nv, "PaymentRequest_" + id + "_" + "ShipToPhoneNum", this.ShipToPhoneNum);
        }


        public void ParseInfoFromQueryString(CS.General_v3.URL.QueryString qs)
        {
            this.ClientAddress1 = CS.General_v3.Util.PageUtil.UrlDecode(qs["ShipToStreet"]);
            this.ClientAddress2 = "";
            this.ClientCity = CS.General_v3.Util.PageUtil.UrlDecode(qs["ShipToCity"]);
            this.ClientState = CS.General_v3.Util.PageUtil.UrlDecode(qs["ShipToState"]);
            this.ClientZIPCode = CS.General_v3.Util.PageUtil.UrlDecode(qs["ShipToZip"]);
            this.ClientCountry = CS.General_v3.Util.PageUtil.UrlDecode(qs["ShipToCountryCode"]);
            this.ClientCountryCode = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(CS.General_v3.Util.PageUtil.UrlDecode(qs["ShipToCountryCode"])).Value;
            this.ClientCountryName = CS.General_v3.Util.PageUtil.UrlDecode(qs["ShipToCountryName"]);

        }
    }
        
}
