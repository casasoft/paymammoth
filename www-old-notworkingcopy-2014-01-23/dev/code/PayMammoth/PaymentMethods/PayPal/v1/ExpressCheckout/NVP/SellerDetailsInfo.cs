﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.PayPal.v1.Util;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP
{
    public class SellerDetailsInfo : BasePayPalObject
    {
        
        /// <summary>
        /// Optional) The unique non-changing identifier for the seller at the marketplace
///site. This ID is not displayed.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SellerID", MaxLength = 13)]
        public string SellerID { get; set; }

        /// <summary>
        ///Unique identifier for the merchant. For parallel payments, this field is required and
///must contain the Payer Id or the email address of the merchant.
        /// </summary>
        [PayPalFieldInfo(FieldName = "SELLERPAYPALACCOUNTID", MaxLength = 127)]
        public string SellerPaypalAccountID { get; set; }


        /// <summary>
        /// (Optional) The current name of the seller or business at the marketplace site. This
///name may be shown to the buyer
        /// </summary>
        [PayPalFieldInfo(FieldName = "SellerUsername", MaxLength = 127)]
        public string SellerUsername {get;set;}

        [PayPalFieldInfo(FieldName = "SellerRegistrationDate", MaxLength = 127)]
        public string SellerRegistrationDate {get;set;}

    }
        
}
