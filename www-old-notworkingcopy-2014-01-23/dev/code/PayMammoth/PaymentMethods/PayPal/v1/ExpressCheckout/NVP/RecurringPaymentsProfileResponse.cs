﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP
{
    public class RecurringPaymentsProfileResponse : BasePayPalResponse
    {
        /// <summary>
        /// A unique identifier for future reference to the details of this recurring payment.
        /// </summary>
        [PayPalFieldInfo(FieldName = "PROFILEID")]
        public string ProfileId { get; set; }

        [PayPalFieldInfo(FieldName = "PROFILESTATUS")]
        protected string profileStatus { get; set; }

        public bool IsGatewayFailure
        {
            get
            {
                return ProfileId == null || profileStatus == null;
            }
        }

        [PayPalFieldInfo(FieldName = "L_LONGMESSAGE0")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Status of the recurring payment profile.
        /// </summary>
        public PayPalEnums.RECURRING_AGREEMENT_STATUS ProfileStatus
        {
            get
            {
                return
                    (PayPalEnums.RECURRING_AGREEMENT_STATUS)
                    CS.General_v3.Util.EnumUtils.EnumValueOf(typeof(PayPalEnums.RECURRING_AGREEMENT_STATUS), profileStatus);
            }
            set { profileStatus = value.ToString(); }
        }
    }
}
