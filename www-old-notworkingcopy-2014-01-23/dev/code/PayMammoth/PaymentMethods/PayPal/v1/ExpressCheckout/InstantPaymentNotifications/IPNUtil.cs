﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using PayMammoth.Modules.SettingModule;
using log4net;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public static class IPNUtil
    {

        private static readonly ILog _log = LogManager.GetLogger(typeof(IPNUtil));

        public static IpnResponse ValidateIpnMessage(IPNMessage msg)
        {
            if (msg == null) throw new ArgumentNullException("msg");
            //------------

            _log.DebugFormat("ValidateIpnMessage.  IsTestIpn: {0} | MessageBlockRaw: {1}", msg.IsTestIpn, msg.MessageBlockRaw);
            string apiServerUrl = null;
            if (msg.IsTestIpn)
            {
                apiServerUrl = SettingFactory.Instance.GetPayMammothSetting(Enums.PAYMAMMOTH_SETTINGS.PayPal_v1_Sandbox_ApiServerUrl);
            }
            else
            {
                apiServerUrl = SettingFactory.Instance.GetPayMammothSetting(Enums.PAYMAMMOTH_SETTINGS.PayPal_v1_Live_ApiServerUrl);
            }

            IpnResponse ipnResponse = new IpnResponse();
            string msgBlock = msg.MessageBlockRaw;
            string ipnMsg = msgBlock + "&" + PayPalConstants.PARAM_CMD + "=" + PayPalConstants.COMMAND_NOTIFY_VALIDATE;
            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = CS.General_v3.Util.Web.HTTPRequest(apiServerUrl, CS.General_v3.Enums.HTTP_METHOD.POST, null, ipnMsg, null);
            }
            catch (Exception ex)
            {
                _log.Warn(string.Format("ValidateIpnMessage: Error occured while sending the http request. Server: {0} | IpnMsg: {1}",
                    apiServerUrl,ipnMsg), ex);
            }
            if (httpResponse != null)
            {

                string response = null;
                {
                    try
                    {
                        Stream responseStream = httpResponse.GetResponseStream();
                        if (responseStream != null)
                        {
                            StreamReader sr = new StreamReader(responseStream);
                            response = sr.ReadToEnd();
                            sr.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Warn("ValidateIpnMessage: Error occured while trying to get response stream", ex);
                    }

                }
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response == PayPalConstants.IPN_VERIFIED)
                    {
                        ipnResponse.Success = true;

                        if (_log.IsDebugEnabled) _log.Debug("ValidateIPNMessage() - IPN_Verified");
                    }
                    else
                    {
                        _log.Warn("ValidateIPNMessage() - IPN Not verified");
                    }
                }
                else
                {
                    if (_log.IsWarnEnabled)
                    {
                        _log.Warn("ValidateIPNMessage() - Status code shows error - [" + httpResponse.StatusCode + "]");
                    }

                }
                ipnResponse.PaypalResponse = response;
            }
            
            return ipnResponse;
            
        }
    }
}
