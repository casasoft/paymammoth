﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.PayPal.v1.Util;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP
{
    public class DoExpressCheckoutRequest : BasePayPalRequest
    {
        public DoExpressCheckoutRequest(PayPalClient payPal, GetExpressCheckoutResponse response) : base(payPal,"DoExpressCheckoutPayment")
        {
            
            this.OptionalUserInfo = new OptionalUserInfo();
            this.PaymentDetails = new List<PaymentDetailsInfo>();
            this.UserSelectedOptionsInfo = new UserSelectedOptionsInfo();
            this.SellerDetails = new SellerDetailsInfo();
            this.FillFromGetExpressCheckoutResponse(response);
        }
        public void FillFromGetExpressCheckoutResponse(GetExpressCheckoutResponse response)
        {
            this.OptionalUserInfo = response.OptionalUserInfo;
            this.PayerID = response.ClientPayerID;
            this.Token = response.Token;
            this.PaymentDetails = response.PaymentDetails;
            this.UserSelectedOptionsInfo = response.UserSelectedOptions;

            

            
        }
        /// <summary>
        /// A token that uniquely identifies this request for your user
        /// </summary>
        [PayPalFieldInfo(FieldName="Token")]
        public string Token { get; set; }

        /// <summary>
        /// Payer ID, returned by GetExpressCheckoutRequest
        /// </summary>
        [PayPalFieldInfo(FieldName = "PayerID")]
        public string PayerID { get; set; }

        /// <summary>
        /// (Optional) Flag to indicate whether you want the results returned by Fraud
///Management Filters. By default, you do not receive this information.
 ///0 - do not receive FMF details (default)
 ///1 - receive FMF details
        /// </summary>
        [PayPalFieldInfo(FieldName = "ReturnFMFDetails")]
        public bool? ReturnFraudManagementFiltersResults { get; set; }

        public OptionalUserInfo OptionalUserInfo { get; private set; }

        public List<PaymentDetailsInfo> PaymentDetails { get; private set; }

        public UserSelectedOptionsInfo UserSelectedOptionsInfo { get; private set; }

        public SellerDetailsInfo SellerDetails { get; private set; }

        /// <summary>
        /// Optional) An identification code for use by third-party applications to identify
///transactions.
        /// </summary>
        [PayPalFieldInfo(FieldName = "ButtonSource")]
        public string ButtonSource { get; set; }
        private void checkRequirements()
        {
            StringBuilder errMsg = new StringBuilder();
            

            if (!string.IsNullOrEmpty(errMsg.ToString()))
            {
                throw new InvalidOperationException(errMsg.ToString());
            }
        }

        public DoExpressCheckoutResponse GetResponse()
        {
            checkRequirements();

            NameValueCollection nv = createNameValueCollectionWithPayPalInfo();
            this.SellerDetails.FillNameValueCollection(nv, null, null);
            this.UserSelectedOptionsInfo.FillNameValueCollection(nv, null, null);
            this.OptionalUserInfo.FillNameValueCollection(nv, null, null);
            int index = 0;
            foreach (var item in this.PaymentDetails)
            {
                item.FillNameValueCollection(nv, index);
                index++;

            }


            string sResponse = getResponse(nv);
            DoExpressCheckoutResponse response = new DoExpressCheckoutResponse();
            CS.General_v3.URL.QueryString qs = new CS.General_v3.URL.QueryString(sResponse);
            response.ParseFromNameValueCollection(qs);
            return response;
        }
    }
        
}
