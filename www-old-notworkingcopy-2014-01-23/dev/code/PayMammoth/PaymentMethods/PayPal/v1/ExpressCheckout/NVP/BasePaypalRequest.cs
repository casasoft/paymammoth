﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;
using System.Reflection;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP
{
    public abstract class BasePayPalRequest : BasePayPalObject
    {
        public BasePayPalRequest(PayPalClient client, string methodName)
        {
            _paypal = client;
            this.MethodName = methodName;
        }
        protected PayPalClient _paypal = null;
        [PayPalFieldInfo(FieldName = "METHOD")]
        public string MethodName { get; private set; }
        protected override NameValueCollection createNameValueCollectionWithPayPalInfo()
        {
            var nv = base.createNameValueCollectionWithPayPalInfo();
            _paypal.AppendAuthParametersToNV(nv);
            return nv;
        }
        protected virtual string getResponse(NameValueCollection nv)
        {
            var currentSettings = Extensions.IPaypalSettingsExtensions.GetCurrentSettings(this._paypal.Settings);

            string sResponse = CS.General_v3.Util.Web.HTTPPost(currentSettings.NvpServerUrl, nv);
            
            return sResponse;
        }
    }
        
}
