﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP
{
    public class PayPalDoExpressCheckoutConfirmer
    {
        public string PayerID { get; private set; }
        public string Token { get; private set; }

        private PayPalClient payPalClient = null;
        /// <summary>
        /// Creates the object
        /// </summary>
        /// <param name="paypalClient">PayPal client to load from.  If null, loads from the default instance</param>
        /// <param name="loadFromQueryString">Whether to load from query string if id and token are none</param>
        /// <param name="payerID">Payer ID</param>
        /// <param name="token">Token</param>
        public PayPalDoExpressCheckoutConfirmer(PayPalClient paypalClient, bool loadFromQueryString = true,
            string payerID = null, string token = null)
        {
            this.payPalClient = paypalClient;
            if (loadFromQueryString)
            {
                if (payerID == null) payerID = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring("payerID");
                if (token == null) token = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring("token");
            }
            this.PayerID = payerID;
            this.Token = token;
        }

        public ResponseStatusInfo ResponseStatus { get; private set; }
        
        /// <summary>
        /// Confirms the payment authorization.  HOWEVER, this does not mean that the payment has really been effected
        /// </summary>
        /// <returns></returns>
        public bool ConfirmPayment()
        {
            
            bool ok = false;
            PayPalClient client = this.payPalClient;
            GetExpressCheckoutRequest req = new GetExpressCheckoutRequest(client, this.Token);
            GetExpressCheckoutResponse GECresp = req.GetResponse();

            if (GECresp.Success)
            {
                DoExpressCheckoutRequest DECReq = new DoExpressCheckoutRequest(this.payPalClient, GECresp);
                DoExpressCheckoutResponse DECResp = DECReq.GetResponse();
                this.ResponseStatus = DECResp.ResponseStatusInfo;
                if (DECResp.Success)
                {
                    ok = true;
                }
                
            }
            else
            {
                this.ResponseStatus = GECresp.ResponseStatusInfo;

            }
            return ok;

        }
    }
}
