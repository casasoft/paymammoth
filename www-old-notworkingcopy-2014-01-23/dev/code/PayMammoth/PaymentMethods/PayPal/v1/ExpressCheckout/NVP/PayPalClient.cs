﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP
{
    /// <summary>
    /// Summary description for PayPal
    /// </summary>
    public class PayPalClient
    {





        public IPayPalSettings Settings { get; private set; }

        public PayPalClient(IPayPalSettings settings)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(settings, "Settings is required");
            this.Settings = settings;
            init();
        }
        
        private void init()
        {
            
        }

       

       
        public void AppendAuthParametersToNV(NameValueCollection nv)
        {
            var cred = Extensions.IPaypalSettingsExtensions.GetCurrentSettings(this.Settings);
            nv[PayPalConstants.PARAM_USER] = cred.Username;
            nv[PayPalConstants.PARAM_PASSWORD] = cred.Password;
            nv[PayPalConstants.PARAM_SIGNATURE] = cred.Signature;
            nv[PayPalConstants.PARAM_VERSION] = PayPalConstants.APIVersion;
        }
        
        public SetExpressCheckoutResponse SetExpressCheckout(SetExpressCheckoutRequest req)
        {
            
            return req.GetResponse();

        }
        public DoExpressCheckoutResponse DoExpressCheckout(DoExpressCheckoutRequest req)
        {
            return req.GetResponse();
        }
        public GetExpressCheckoutResponse GetExpressCheckout(GetExpressCheckoutRequest req)
        {
            return req.GetResponse();
        }



    }
}
