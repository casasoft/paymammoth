﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.URL;
using PayMammoth.Modules.PaymentRequestModule;
using log4net;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers
{
    public class ExpressCheckoutIpnMessageParser : IIpnMessageParser
    {
        
        private readonly ILog _log = LogManager.GetLogger(typeof(ExpressCheckoutIpnMessageParser));
		
			
        private static readonly ExpressCheckoutIpnMessageParser _instance = new ExpressCheckoutIpnMessageParser();

        public static ExpressCheckoutIpnMessageParser Instance
        {
            get { return _instance; }
        }

        private ExpressCheckoutIpnMessageParser()
        {

        }




        #region IpnMessageParser Members

        private void addIpnResponseToTransaction(QueryString qs, IpnResponse response)
        {

            qs.Add("Ipn PayPal Response", response.PaypalResponse);



        }
        public bool HandleIpnMessage(IPNMessage ipnMessage)
        {
            bool ok = false;
            var request = QuerystringData.GetPaymentRequestFromQuerystring();
            
            
            var t = request.CurrentTransaction;
            var msg = ipnMessage;
            
            lock (t.ItemLock)
            {
                if (!t.Successful)
                {
                    var qs = t.GetParametersAsQueryString();
                    bool requiresManualPayment = PayPalEnums.CheckIfPaymentStatusRequiresManualIntervention(msg.PaymentStatus);
                    t.MarkAsSuccessful(requiresManualPayment);

                    using (var nhTransaction = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                    {
                        

                        qs.AddByProperty(msg, x => x.PayPalTransactionID);
                        qs.AddByProperty(msg, x => x.PaymentStatus);
                        qs.AddByProperty(msg, x => x.VerifySignature);
                        qs.AddByProperty(msg, x => x.PendingReason);
                        qs.AddByProperty(msg, x => x.NotifyVersion);
                        qs.AddByProperty(msg, x => x.TransactionSubject);
                        qs.AddByProperty(msg, x => x.TransactionType);

                        t.SetPaymentParameters(qs);
                        nhTransaction.Commit();

                    }
                }
            }
            ok = true;
           

            return ok;

        }

        #endregion
    }
}
