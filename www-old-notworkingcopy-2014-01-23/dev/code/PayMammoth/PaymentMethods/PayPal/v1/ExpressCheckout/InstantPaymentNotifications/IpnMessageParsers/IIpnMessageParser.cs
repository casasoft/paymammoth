﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.PaymentRequestModule;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications.IpnMessageParsers
{
    public interface IIpnMessageParser
    {
        bool HandleIpnMessage(IPNMessage ipnMessage);

    }
}
