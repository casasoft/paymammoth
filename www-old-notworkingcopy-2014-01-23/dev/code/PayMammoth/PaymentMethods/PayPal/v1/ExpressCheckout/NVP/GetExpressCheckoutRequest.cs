﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.PayPal.v1.Util;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP
{
    public class GetExpressCheckoutRequest : BasePayPalRequest
    {
        
        public GetExpressCheckoutRequest(PayPalClient paypal, string token) : base(paypal,"GetExpressCheckoutDetails")
        {
            this.Token = token;
            
        }

        [PayPalFieldInfo]
        public string Token { get; set; }
        
        
        public GetExpressCheckoutResponse GetResponse()
        {

            NameValueCollection nv = createNameValueCollectionWithPayPalInfo();

            string sResponse = getResponse(nv);
            GetExpressCheckoutResponse response = new GetExpressCheckoutResponse();
            
            CS.General_v3.URL.QueryString qs = new CS.General_v3.URL.QueryString(sResponse);
            response.ParseFromNameValueCollection(qs);
            return response;
        }
    }   
}
