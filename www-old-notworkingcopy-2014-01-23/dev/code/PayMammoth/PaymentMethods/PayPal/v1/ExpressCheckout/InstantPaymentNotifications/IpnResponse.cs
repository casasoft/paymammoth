﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public class IpnResponse
    {
        public IpnResponse()
        {
            
        }

        public IPNMessage IpnMessage { get; internal set; }
        public bool Success { get; internal set; }

        public string PaypalResponse { get; set; }
    }
}
