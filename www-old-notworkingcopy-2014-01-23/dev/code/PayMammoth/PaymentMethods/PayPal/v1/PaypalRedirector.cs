﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Interfaces;
using CS.General_v3.Classes.HelperClasses;
using PayMammoth.Modules.PaymentRequestModule;
using log4net;

namespace PayMammoth.PaymentMethods.PayPal.v1
{
    public class PayPalRedirector : IPaymentRedirector
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(PayPalRedirector));
        
						
		public static PayPalRedirector Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<PayPalRedirector>(); } }

		private PayPalRedirector()
		{

		}
		
			
        #region IPaymentRedirector Members

        public Enums.RedirectionResult Redirect(IPaymentRequest request)
        {
            //OperationResult result = new OperationResult();
            Enums.RedirectionResult result = Enums.RedirectionResult.Error;
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(request, "Request is required");


            var transaction = request.StartNewTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout);
            var response = PayPalManager.Instance.StartExpressCheckout(transaction);

            if (!response.Success)
            {
                _log.Debug(response.ResponseStatusInfo.GetAsString());
                
            }
            else
            {
                result = Enums.RedirectionResult.Success;
                    
            }


            return result;


        }

        #endregion
    }
}
