﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Interfaces;

namespace PayMammoth.PaymentMethods.PayPal.v1.Interfaces
{
    public interface IPayPalRecurringProfileManager : IRecurringProfileManager
    {
    }
}
