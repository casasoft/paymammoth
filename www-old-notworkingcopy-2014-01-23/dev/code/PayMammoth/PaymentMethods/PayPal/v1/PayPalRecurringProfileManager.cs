﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
using PayMammoth.Classes.Helpers;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using CS.General_v3.Util;
using PayMammoth.PaymentMethods.PayPal.v1.Interfaces;
using log4net;
using BusinessLogic_v3.Util;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules;
using BusinessLogic_v3.Classes.Attributes;

namespace PayMammoth.PaymentMethods.PayPal.v1
{
    [IocComponent]
    public class PayPalRecurringProfileManager : IPayPalRecurringProfileManager
    {
        ILog _log { get; set; }

        public PayPalRecurringProfileManager()
        {
            _log = LogManager.GetLogger(this.GetType().Name);
        }

        //private static readonly PayPalRecurringProfileManager _instance = new PayPalRecurringProfileManager();

        //public static PayPalRecurringProfileManager Instance
        //{
        //    get { return _instance; }
        //}




        private CreateRecurringPaymentsProfileRequest createRecurringPaymentsProfileRequest(PayPalClient paypalClient, IRecurringProfile profile,
            IPaymentRequestTransaction paymentRequestTransaction)
        {

            var paymentRequestDetails = ((IPaymentRequestDetails)paymentRequestTransaction.PaymentRequest);

            CreateRecurringPaymentsProfileRequest recurringRequest = new CreateRecurringPaymentsProfileRequest(paypalClient, paymentRequestTransaction.Reference);
            recurringRequest.ProfileStartDate = Date.Now;
            recurringRequest.Description = paymentRequestDetails.Description;
            recurringRequest.BillingPeriod = paymentRequestDetails.RecurringProfile_BillingPeriod;
            recurringRequest.BillingFrequency = paymentRequestDetails.RecurringProfile_BillingFrequency;
            recurringRequest.TotalBillingCycles = paymentRequestDetails.RecurringProfile_TotalBillingCycles.GetValueOrDefault();
            recurringRequest.MaximumFailedPayments = paymentRequestDetails.RecurringProfile_MaxFailedAttempts;
            recurringRequest.MerchantProfileReference = profile.Identifier;


            recurringRequest.PayerFirstName = paymentRequestDetails.ClientFirstName;
            recurringRequest.PayerLastName= paymentRequestDetails.ClientLastName;
            recurringRequest.PayerMiddleName= paymentRequestDetails.ClientMiddleName;
            recurringRequest.BuyerStreet = paymentRequestDetails.ClientAddress1;
            recurringRequest.BuyerStreet2 = CS.General_v3.Util.Text.AppendStrings(", ", paymentRequestDetails.ClientAddress2, paymentRequestDetails.ClientAddress3);
            recurringRequest.BuyerZip = paymentRequestDetails.ClientPostCode;
            recurringRequest.BuyersCountryCode = paymentRequestDetails.ClientCountry;
            

            
            double totalExclTax = paymentRequestTransaction.PaymentRequest.GetTotalPrice(incTaxes: false);
            double totalTax = paymentRequestTransaction.PaymentRequest.GetTotalPrice() - totalExclTax;

            recurringRequest.Amount = totalExclTax;
            recurringRequest.TaxAmount = totalTax;
            recurringRequest.CurrencyCode = paymentRequestDetails.CurrencyCode;
            recurringRequest.Email = paymentRequestDetails.ClientEmail;
            return recurringRequest;
        }
        public SetupRecurringProfileResult SetupRecurringProfile(IRecurringProfile profile)
        {
            SetupRecurringProfileResult result = new SetupRecurringProfileResult();
            profile.Status = Enums.RecurringProfileStatus.CouldNotCreate;
            var request = profile.GetPaymentRequest();
            
            if (request != null)
            {
                var transaction = request.CurrentTransaction;

                if (transaction != null && ((IPaymentRequestDetails)request).RecurringProfileRequired)
                {

                    PayPalClient payPalClient = PayPalManager.Instance.GetPayPalClient(request.WebsiteAccount);
                    if (payPalClient != null)
                    {
                        CreateRecurringPaymentsProfileRequest recurringRequest = createRecurringPaymentsProfileRequest(payPalClient, profile, transaction);
                        CreateRecurringPaymentsProfileResponse response = recurringRequest.GetResponse();
                        checkResponse(response, transaction, profile, result);
                        
                    }
                    else
                    {
                        result.Status = Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE.InternalError;
                        _log.Error("PayPalClient should never be null when this method is called!", new NullReferenceException());
                    }
                }
                else
                {
                    result.Status = Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE.InternalError;
                    throw new InvalidOperationException("This method should never be called if recurring payments were not enabled in the transaction!");
                    
                }
            }
            else
            {
                result.Status = Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE.InternalError;
                throw new InvalidOperationException("PaymentRequestTransaction should never be null when this method is called!");
                
            }
           
            
            return result;
        }

        private const string invalidCountryPaypalErrorMessage = "This transaction cannot be processed. The country code in the shipping address must match the buyer's country of residence.";

        private void checkResponse(CreateRecurringPaymentsProfileResponse response, IPaymentRequestTransaction paymentRequestTransaction,
            IRecurringProfile profile, SetupRecurringProfileResult result)
        {
            result.Status = Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE.InternalError;

            
            if (!response.IsGatewayFailure)
            {
                if (response.ProfileStatus == PayPalEnums.RECURRING_AGREEMENT_STATUS.ActiveProfile)
                {
                    result.Status = Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE.Success;
                     BusinessLogic_v3.Util.nHibernateUtil.RepeatUntilNoStaleObjExceptionIsRaised(
                        () =>
                    {
                        profile = PayMammoth.Modules.Factories.RecurringProfileFactory.GetByPrimaryKey(profile.ID);
                        profile.CreationStatus = result.Status;
                        profile.ProfileIdentifierOnGateway = response.ProfileId;
                        profile.Save();

                    });
                    
                    //createRecurringProfileInDb(response, paymentRequestTransaction);
                }
                else
                {
                    

                    profile.StatusLog = PayMammoth.Util.GeneralUtil.AddMessageToStringLog(profile.StatusLog, CS.General_v3.Enums.LOG4NET_MSG_TYPE.Error,
                        string.Format( "UserProfileError occurred while creating recurring profile. Response: {0}", response.ResponseStatusInfo.GetAsString()));
                }
            }
            else
            {
                if (response.ErrorMessage.ToLower() == invalidCountryPaypalErrorMessage.ToLower())
                {
                    result.Status = Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE.BuyerCountryDoesNotMatchShippingCountry;
                    profile.StatusLog = PayMammoth.Util.GeneralUtil.AddMessageToStringLog(profile.StatusLog, CS.General_v3.Enums.LOG4NET_MSG_TYPE.Error,
                        string.Format("An error occured with the gateway due to the buyer's country not matching the shipping country. Original error message: " + response.ErrorMessage));
                    
                }
                else
                {
                    result.Status = Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE.GatewayError;
                    profile.StatusLog = PayMammoth.Util.GeneralUtil.AddMessageToStringLog(profile.StatusLog, CS.General_v3.Enums.LOG4NET_MSG_TYPE.Error,
                        string.Format("An error occured with the gateway, this is probably due to incorrect information being supplied. PayPal error message: " + response.ErrorMessage));
                    
                }
                result.Message = response.ErrorMessage;
            }
           
        }



        //private void createRecurringProfileInDb(CreateRecurringPaymentsProfileResponse response, IPaymentRequestTransaction paymentRequestTransaction, bool autoSave = true)
        //{
        //    using (var t = nHibernateUtil.BeginTransactionBasedOnAutoSaveBool(autoSave))
        //    {
        //        IRecurringProfile profile = Factories.RecurringProfileFactory.CreateNewItem();
        //        profile.PaymentTransaction = paymentRequestTransaction;
        //        profile.ProfileIdentifierOnGateway = response.ProfileId;
        //        profile.PaymentGateway = Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout;
        //        profile.Status = PayMammoth.Enums.RecurringProfileStatus.Pending;
        //        profile.Save();
        //        paymentRequestTransaction.Save();
        //        t.CommitIfNotNull();
        //    }
        //}

       
    }
}
