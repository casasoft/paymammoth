﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMammoth.PaymentMethods.Apco.v1
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ApcoIFrame runat=server></{0}:ApcoIFrame>")]
    public class FormRedirect 
    {
        private Page _pg = null;
        public string DefaultGatewayURL {get;set;}
        public FormRedirect(Page pg, string defaultGatewayURL)
        {
            this._pg = pg;
            this.DefaultGatewayURL = defaultGatewayURL;
        }
        private void writeJS()
        {
            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine("function removeElem(elem) {");
            sb.AppendLine("   var e = document.getElementById(elem);");
            sb.AppendLine("   if (e != null) {");
            sb.AppendLine("       e.parentNode.removeChild(e);");
            sb.AppendLine("   }");
            sb.AppendLine("}");
            sb.AppendLine("function removeInputs(elem) {");
            sb.AppendLine("   if (elem != null) ");
            sb.AppendLine("   {");
            sb.AppendLine("      var i;");
            sb.AppendLine("      if (elem.childNodes != null) ");
            sb.AppendLine("      {");
            sb.AppendLine("           for (i = 0; i < elem.childNodes.length; i++) ");
            sb.AppendLine("           {");
            sb.AppendLine("               var curr = elem.childNodes[i];");
            sb.AppendLine("               if (curr != elem) ");
            sb.AppendLine("               {");
            sb.AppendLine("                   if (curr.tagName != null && curr.tagName.toLowerCase() == \"input\" && curr.id != null && curr.id != 'params') ");
            sb.AppendLine("                   {");
            sb.AppendLine("                       elem.removeChild(curr);");
            sb.AppendLine("                   }");
            sb.AppendLine("                   else"); 
            sb.AppendLine("                   {");
            sb.AppendLine("                       removeInputs(curr);");
            sb.AppendLine("                   }");
            sb.AppendLine("               }");
            sb.AppendLine("           }");
            sb.AppendLine("       }");
            sb.AppendLine("   }");
            sb.AppendLine("}");
            sb.AppendLine("");
            sb.AppendLine("function submitForm() {");
            sb.AppendLine("");
            sb.AppendLine("   removeInputs(document);");
            sb.AppendLine("   removeElem('__VIEWSTATE');");
            sb.AppendLine("   var form = document.forms[0];");
            sb.AppendLine("   form.action = '"+DefaultGatewayURL+"';");
            sb.AppendLine("");
            sb.AppendLine("   form.onSubmit = form.onsubmit = function() ");
            sb.AppendLine("   {");
            sb.AppendLine("       return true;");
            sb.AppendLine("   }");
            sb.AppendLine("   form.submit();");
            sb.AppendLine("}");
            _pg.ClientScript.RegisterClientScriptBlock(this.GetType(),"Apco_formredirect",sb.ToString(),true);
        }
        private void addBodyOnLoadCall()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("submitForm();");
            _pg.ClientScript.RegisterStartupScript(this.GetType(),"Apco_formredirect_onload",sb.ToString(),true);
        }
    }
}
