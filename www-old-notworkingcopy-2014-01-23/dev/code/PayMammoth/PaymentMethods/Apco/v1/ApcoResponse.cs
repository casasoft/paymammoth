﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Collections.Specialized;
using log4net;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.PaymentRequestTransactionModule;

namespace PayMammoth.PaymentMethods.Apco.v1
{
    
        public class ApcoResponse
        {
            private ILog _log = LogManager.GetLogger(typeof(ApcoResponse));
            public string OrderReference { get; set; }
            public string ReturnPspID { get; set; }
            public string Hash { get; set; }
            public bool Result { get; set; }
            public bool ValidHash { get; set; }
            public string ExtendedError { get; set; }
            public class RESPONSE_ANTIFRAUD
            {
                public int Score { get; set; }
                public string ActionRecommended { get; set; }
                
            }
            public RESPONSE_ANTIFRAUD Response_AntiFraud { get; private set; }
            public string AuthorisationCode { get; set; }
            
            private string readString(XmlNode node, string nodeID)
            {
                string s = "";
                if (node.SelectSingleNode(nodeID) != null)
                    s = node.SelectSingleNode(nodeID).InnerText;
                return s;
            }
            private int readInt(XmlNode node, string nodeID)
            {
                string s = readString(node, nodeID);
                int i = 0;
                if (!string.IsNullOrEmpty(s))
                    i = Convert.ToInt32(s);
                return i;
            }
            private void checkHash(string secretWord)
            {
                XmlNode hash = _doc.SelectSingleNode("Transaction/@hash");
                hash.InnerText = secretWord;
                string md5Hash = CS.General_v3.Util.Cryptography.GetMd5Sum(_doc.OuterXml);
                this.ValidHash = (md5Hash.ToLower() == this.Hash.ToLower());
            }

            public void LoadFromContext(HttpContext ctx)
            {
                this.LoadFromForm(nvForm: ctx.Request.Form, nvQS: ctx.Request.QueryString);
            }

            public void LoadFromForm(NameValueCollection nvForm = null, NameValueCollection nvQS = null)
            {
                if (nvForm == null)
                    nvForm = CS.General_v3.Util.PageUtil._getCurrentForm();
                if (nvQS == null)
                    nvQS = CS.General_v3.Util.PageUtil._getCurrentQueryString();
                string s = nvForm[ApcoConstants.PARAM_PARAMS] ?? "";
                if (string.IsNullOrEmpty(s))
                {
                    s = nvQS[ApcoConstants.PARAM_PARAMS] ?? "";
                }
                LoadFromXML(s);
            }
            public void LoadFromXML(string s)
            {
               
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(s);
                        
                    LoadFromXML(doc);
                }
                catch (Exception ex)
                {
                    
                }
                
            }
            private XmlDocument _doc = null;
            public void LoadFromXML(XmlDocument doc)
            {
                this._doc = doc;
                XmlNode transaction = doc.SelectSingleNode("Transaction");
                this.Hash = transaction.SelectSingleNode("@hash").InnerText;
                this.OrderReference = readString(transaction, "ORef");
                this.ReturnPspID = readString(transaction, "return_pspid");
                this.Result= (readString(transaction, "Result").ToLower() == "ok");
                this.ExtendedError= readString(transaction, "ExtendedErr");
                XmlNode antifraud = transaction.SelectSingleNode("AntiFraud");
                if (antifraud != null)
                {
                    this.Response_AntiFraud.ActionRecommended = readString(antifraud, "ActionRecomd");
                    this.Response_AntiFraud.Score = readInt(antifraud, "Score");

                }
                this.AuthorisationCode= readString(transaction, "AuthCode");
                
                
            }
            public bool VerifyHash(string secretword)
            {
                 checkHash(secretword);
                 return this.ValidHash;
            }

            public ApcoResponse()
            {
               
                this.Response_AntiFraud = new RESPONSE_ANTIFRAUD();

            }
            public ApcoResponse(string xml)
                : this()
            {
                LoadFromXML(xml);
            }
            public ApcoResponse(XmlDocument xml)
                : this()
            {
                LoadFromXML(xml);
            }
            public void WriteConfirmation(HttpResponse response)
            {

                response.Clear();
                response.Write(ApcoConstants.RESPONSE_OK);
                
            }


            public void FillPaymentRequestTransactionWithDetails(IPaymentRequestTransaction t)
            {
                t.AuthCode = this.AuthorisationCode;
                t.Reference = this.OrderReference;
                t.SetPaymentParametersFromObject(this, null,
                    x => x.ExtendedError,
                    x => x.Response_AntiFraud,
                    x => x.Result,
                    x => x.ReturnPspID,
                    x => x.ValidHash);
            }

            public ApcoEnums.RESPONSE_RESULT VerifyResponse(IPaymentRequest paymentRequest)
            {

                ApcoEnums.RESPONSE_RESULT result = ApcoEnums.RESPONSE_RESULT.InvalidHash;
                //var paymentRequest = PayMammoth.Modules.Factories.PaymentRequestFactory.GetRequestByIdentifier(identifier);
               // if (_log.IsInfoEnabled) _log.Info("VerifyResponse: QS: " + ctx.Request.QueryString.ToString() + ", Form: " + ctx.Request.Form.ToString());
                if (ApcoManager.Instance.VerifyTransaction(paymentRequest.CurrentTransaction))
                {

                    bool ok = false;
                    if (paymentRequest != null)
                    {


                        // ApcoClient Apco = new ApcoClient();

                        var apcoCredentials = paymentRequest.WebsiteAccount.GetApcoCredentials();

                        if (this.VerifyHash(apcoCredentials.SecretWord))
                        {
                            if (this.Result)
                            {
                                result = ApcoEnums.RESPONSE_RESULT.Success;
                            }
                            else
                            {
                                result = ApcoEnums.RESPONSE_RESULT.Failed;

                            }

                            ok = true;

                        }
                        this.FillPaymentRequestTransactionWithDetails(paymentRequest.CurrentTransaction);

                    }
                    if (!ok)
                    {
                        _log.Info("VerifyResponse: Hash verification failed");
                    }
                }
                return result;

            }

        }


    

}
