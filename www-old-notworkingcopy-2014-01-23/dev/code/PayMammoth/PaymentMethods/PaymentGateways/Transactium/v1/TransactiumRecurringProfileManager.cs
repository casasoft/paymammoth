﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules;
using CS.General_v3.Util;
using BusinessLogic_v3.Util;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1.Wrappers;
using System.Collections.Specialized;
using PayMammoth.Classes.Helpers;

namespace PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1
{
    public class TransactiumRecurringProfileManager : IRecurringProfileManager
    {
        public Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE CreateRecurringProfile(IPaymentRequestTransaction paymentRequestTransaction)
        {
            if (((IPaymentRequestDetails) paymentRequestTransaction.PaymentRequest).RecurringProfileRequired && !string.IsNullOrWhiteSpace(paymentRequestTransaction.Reference))
            {
                createActualProfile(paymentRequestTransaction);
                return Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE.Success;
            }
            else
            {
                return Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE.InternalError;
            }
        }

        private void createActualProfile(IPaymentRequestTransaction paymentRequestTransaction)
        {
            using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                IRecurringProfile profile = Factories.RecurringProfileFactory.CreateNewItem();
                profile.ProfileIdentifierOnGateway = paymentRequestTransaction.Reference;
                paymentRequestTransaction.PaymentRequest.RecurringProfileLink = profile;

                profile.PaymentGateway = Connector.Enums.PaymentMethodSpecific.PaymentGateway_Transactium;
                //profile.NextPaymentDueOn = getNextPaymentDueOn(paymentRequestTransaction.PaymentRequest);
              //  profile.BillingCyclesRequired = getTotalBillingCyclesRequired(paymentRequestTransaction.PaymentRequest);
                profile.Save();
                paymentRequestTransaction.Save();
                paymentRequestTransaction.PaymentRequest.Save();
                t.Commit();
            }
        }

        private int? getTotalBillingCyclesRequired(IPaymentRequest paymentRequest)
        {
            return ((IPaymentRequestDetails) paymentRequest).RecurringProfile_TotalBillingCycles;
            
        }

        //private DateTime? getNextPaymentDueOn(PaymentRequest paymentRequest)
        //{
        //    Connector.Enums.RECURRING_BILLINGPERIOD billingPeriod = paymentRequest.RecurringProfile_BillingPeriod;
        //    int frequency = paymentRequest.RecurringProfile_BillingFrequency;
        //    DateTime result = Date.Now;
        //    switch (billingPeriod)
        //    {
        //        case Connector.Enums.RECURRING_BILLINGPERIOD.Day:
        //            result = result.AddDays(frequency);
        //            break;
        //        case Connector.Enums.RECURRING_BILLINGPERIOD.Month:
        //            result = result.AddMonths(frequency);
        //            break;
        //        case Connector.Enums.RECURRING_BILLINGPERIOD.SemiMonth:
        //            result = result.AddDays(15); // semi month is half a month i.e. 15 days (or ~2 weeks)
        //            break;
        //        case Connector.Enums.RECURRING_BILLINGPERIOD.Week:
        //            result = result.AddDays(7 * frequency); //a week is 7 days long multiplied by the number of weeks of interval
        //            break;
        //        case Connector.Enums.RECURRING_BILLINGPERIOD.Year:
        //            result = result.AddYears(frequency);
        //            break;
        //        default:
        //            throw new InvalidOperationException("Billing Period of type <" + billingPeriod +
        //                                                "> not implemented yet!");
        //    }
        //    return result;
        //}

        public bool IsPaymentDueForProfile(RecurringProfile profile)
        
        {
            throw new InvalidOperationException("Not yet imp");
            //bool ok = true;
            ////ok = ok && profile.NextPaymentDueOn <= DateTime.Now;
            //ok = ok &&
            //     (profile.BillingCyclesRequired == 0 || profile.BillingCyclesCompleted < profile.BillingCyclesRequired);
            //return ok;
        }

        /// <summary>
        /// Performs payment for this recurring profile.
        /// </summary>
        /// <param name="recurringProfile"></param>
        public void RecurPayment(RecurringProfile recurringProfile)
        {
            if(IsPaymentDueForProfile(recurringProfile))
            {
                TransactiumSaleTransaction request = generateRequest(recurringProfile);
                string response = sendRequest(request);
            }
        }

        private string sendRequest(TransactiumSaleTransaction request)
        {
            NameValueCollection nv = request.SerializeObject();
            string reply =
                CS.General_v3.Util.Web.HTTPGet(
                    "https://psp.stg.transactium.com/merchantservices/gateway/process.aspx", qsParams: nv);
            return reply;
        }

        private TransactiumSaleTransaction generateRequest(RecurringProfile recurringProfile)
        {
            //create sale transaction
            TransactiumSaleTransaction sale = new TransactiumSaleTransaction();
            IPaymentRequest request = recurringProfile.GetPaymentRequest();
            sale.AMOUNT = request.GetTotalPriceInCents();
            sale.REFID = recurringProfile.ProfileIdentifierOnGateway;
            string username;
            string password;
            getUsernameAndPasswordFromProfile(recurringProfile, out username, out password);
            sale.USERNAME = username;
            sale.PASSWORD = password;
            sale.SetCurrency(((IPaymentRequestDetails)request).CurrencyCode);
            return sale;
        }

        private void getUsernameAndPasswordFromProfile(RecurringProfile recurringProfile, out string username, out string password)
        {
            IPaymentRequest request = recurringProfile.GetPaymentRequest();
            var website = request.WebsiteAccount;
            if (website.PaymentGatewayTransactiumUseLiveEnvironment)
            {
                username = website.PaymentGatewayTransactiumLiveUsername;
                password = website.PaymentGatewayTransactiumLivePassword;
            }
            else
            {
                username = website.PaymentGatewayTransactiumStagingUsername;
                password = website.PaymentGatewayTransactiumStagingPassword;
            }
        }

        #region IRecurringProfileManager Members

        public SetupRecurringProfileResult SetupRecurringProfile(IRecurringProfile profile)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
