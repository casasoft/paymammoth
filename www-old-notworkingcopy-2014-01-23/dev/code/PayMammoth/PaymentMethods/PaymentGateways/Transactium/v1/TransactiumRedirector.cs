﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Interfaces;
using CS.General_v3.Classes.HelperClasses;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.PaymentRequestTransactionModule;

namespace PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1
{
    public class TransactiumRedirector : IPaymentRedirector
    {


        #region IPaymentRedirector Members

        public Enums.RedirectionResult Redirect(IPaymentRequest paymentRequest)
        {
            Enums.RedirectionResult result = Enums.RedirectionResult.Success;
            
            var request = paymentRequest;
            
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(request, "Request is required");

            

            string url = TransactiumUrls.GetPaymentPageUrl(request.Identifier);
            CS.General_v3.Util.PageUtil.RedirectPage(url);
            

            return result;


        }

        #endregion
    }
}
