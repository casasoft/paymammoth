﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;

namespace PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1.HelperClasses
{
    public class CheckPaymentResponse
    {
        //public EnumsTransactium.CHECK_PAYMENT_RESPONSE Status { get; set; }
        public Enums.PaymentStatus Status { get; set; }
        public OperationResult Result { get; set; }
        public CheckPaymentResponse()
        {
            this.Result = new OperationResult();
        }
    }
}
