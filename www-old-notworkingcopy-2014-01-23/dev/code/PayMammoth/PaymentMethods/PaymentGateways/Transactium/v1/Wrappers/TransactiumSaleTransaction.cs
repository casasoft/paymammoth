﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util;
using System.Collections.Specialized;
using CS.General_v3.URL;

namespace PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1.Wrappers
{
    public class TransactiumSaleTransaction
    {
        /// <summary>
        /// $12.50 is expressed as 1250.
        /// </summary>
        public long AMOUNT { get; set; }
        public string REFID { get; set; }
        public int TYPE { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string CURRENCY { get; set; }

        public TransactiumSaleTransaction()
        {
            this.TYPE = 100; //as specified in page 29 of IG01 recurring.pdf
        }

        public void SetCurrency(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 curr)
        {
            CURRENCY = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode(curr);
        }

        public NameValueCollection SerializeObject()
        {
            NameValueCollection nv = new NameValueCollection();
            ReflectionUtil.SerializeObjectToNameValueCollection(nv, this);
            return nv;
        }
    }
}
