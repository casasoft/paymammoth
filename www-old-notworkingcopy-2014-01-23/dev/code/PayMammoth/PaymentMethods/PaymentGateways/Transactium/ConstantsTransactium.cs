﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.PaymentGateways.Transactium
{
    public class ConstantsTransactium
    {
        public const string HPSID = "HPSID";
        public static string GetHpsIdFromQuerystring()
        {
            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(HPSID);
        }
        public static string GetLanguageCode(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? language, CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? languageCountry, string suffix)
        {
            string s = "en-GB";
            switch (language)
            {
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.English:
                    {
                        if (languageCountry == CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.UnitedStates)
                            s = "en-US";
                        else
                            s = "en-GB";
                        break;

                    }
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Spanish: s = "es-ES"; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Italian: s = "it-IT"; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Maltese: s = "mt-MT"; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Portuguese: s = "pt-BR"; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.French: s = "fr-FR"; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.German: s = "de-DE"; break;
                default:
                    s = "en-GB"; break;
                    
            }
            return s;
        }
    }
}
