﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.PaymentGateways.Apco.v1
{
    public class ApcoCredentials : IApcoCredentials
    {
        public string ProfileId { get; set; }
        public string SecretWord { get; set; }
    }
}
