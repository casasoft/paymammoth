﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.Controls
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ApcoIFrame runat=server></{0}:ApcoIFrame>")]
    public class ApcoIFrame : WebControl
    {
        public ApcoIFrame()
            : base(HtmlTextWriterTag.Iframe)
        {

        }
        

    }
}
