﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Modules.PaymentRequestModule;


namespace PayMammoth.PaymentMethods.PaymentGateways.Apco.v1
{
    public class ApcoPaymentRedirector : IPaymentRedirector
    {

        #region IPaymentRedirector Members

        public Enums.RedirectionResult Redirect(IPaymentRequest request)
        {
            Enums.RedirectionResult result = Enums.RedirectionResult.Success;
            
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(request, "Request is required");



            string url = ApcoUrls.GetPaymentUrl(request.Identifier);
            CS.General_v3.Util.PageUtil.RedirectPage(url);


            return result;


        }

        #endregion
    }
}
