﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

namespace PayMammoth.PaymentMethods.PaymentGateways.Apco.v1
{
    public  static partial class ApcoEnums
    {

        public enum RESPONSE_RESULT
        {
            InvalidHash,
            Success,
            Failed
        }


        #region Enumerations
        public static bool checkSupportedCurrency(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 currency)
        {
            string code = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode(currency).ToUpper();

            switch (code)
            {
                case "USD":
                case "EUR":
                case "GBP":
                case "AUD":
                case "CAD":
                case "CHF":
                case "CYP":
                case "ISR":
                case "JPY":
                    return true;
            }
            return false;
        }

        public static string CurrencyToCode(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 curr)
        {
            switch (curr)
            {
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.AustraliaDollars:
                    return "36";
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedStatesOfAmericaDollars:
                    return "840";
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.SwitzerlandFrancs:
                    return "756";
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.CanadaDollars:
                    return "124";
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.CyprusPoundsExpiresJan:
                    return "196";
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedKingdomPounds:
                    return "826";
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.IsraelNewShekels:
                    return "376";
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.JapanYen:
                    return "392";
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro:
                    return "978";
            }
            return "";
        }
        public static CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 CurrencyFromCode(string code)
        {
            switch (code)
            {
                case "36": return CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.AustraliaDollars;
                case "840": return CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedStatesOfAmericaDollars;
                case "756": return CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.SwitzerlandFrancs;
                case "124": return CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.CanadaDollars;
                case "196": return CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.CyprusPoundsExpiresJan;
                case "826": return CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedKingdomPounds;
                case "376": return CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.IsraelNewShekels;
                case "392": return CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.JapanYen;
                case "978": return CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro;

            }
            throw new InvalidOperationException("Currency code '" + code + "' does not exist");
        }

        public enum LANGUAGE
        {
            EnglishUK, Maltese, Italian, French, German, Spanish, Croatian, Swedish,
            Romanian, Hungarian, Turkish, Greek, Finnish, Portuguese, Serbian, Slovenian,
            Dutch, ChineseSimplified, Norwegian, Russian, EnglishUS, Polish,
            Chechz, Slovak, Bulgarian, Japanese

        }

        public static LANGUAGE ConvertIsoEnumToApcoLanguageEnum(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? lang, CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? languageCountryCode)
        {
            LANGUAGE result = LANGUAGE.EnglishUK;
            switch (lang)
            {
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.English:
                    if (languageCountryCode.HasValue && languageCountryCode.Value == CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.UnitedStates)
                        result = LANGUAGE.EnglishUS;
                    else
                        result = LANGUAGE.EnglishUK;
                    break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Maltese: result = LANGUAGE.Maltese; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Italian: result = LANGUAGE.Italian; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.French: result = LANGUAGE.French; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.German: result = LANGUAGE.German; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Spanish: result = LANGUAGE.Spanish; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Croatian: result = LANGUAGE.Croatian; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Swedish: result = LANGUAGE.Swedish; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Romanian: result = LANGUAGE.Romanian; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Hungarian: result = LANGUAGE.Hungarian; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Turkish: result = LANGUAGE.Turkish; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Greek: result = LANGUAGE.Greek; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Finnish: result = LANGUAGE.Finnish; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Portuguese: result = LANGUAGE.Portuguese; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Serbian: result = LANGUAGE.Serbian; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Slovene: result = LANGUAGE.Slovenian; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Dutch: result = LANGUAGE.Dutch; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.ChineseSimplified: result = LANGUAGE.ChineseSimplified; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Norwegian: 
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.NorwegianBokmål:
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.NorwegianNynorsk: result = LANGUAGE.Norwegian; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Russian: result = LANGUAGE.Russian; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Polish: result = LANGUAGE.Polish; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Chechen: result = LANGUAGE.Chechz; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Slovak: result = LANGUAGE.Slovak; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Bulgarian: result = LANGUAGE.Bulgarian; break;
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Japanese: result = LANGUAGE.Japanese; break;
                default:
                    result= LANGUAGE.EnglishUK;
                    break;
            }
            return result;
        }

        public static string LanguageToCode(LANGUAGE lang)
        {
            switch (lang)
            {
                case LANGUAGE.EnglishUK:
                    return "en";
                case LANGUAGE.Maltese:
                    return "mt";
                case LANGUAGE.Italian:
                    return "it";
                case LANGUAGE.French:
                    return "fr";
                case LANGUAGE.German:
                    return "de";
                case LANGUAGE.Spanish:
                    return "es";
                case LANGUAGE.Croatian:
                    return "hr";
                case LANGUAGE.Swedish:
                    return "se";
                case LANGUAGE.Romanian: return "ro";
                case LANGUAGE.Hungarian: return "hu";
                case LANGUAGE.Turkish: return "tr";
                case LANGUAGE.Greek: return "gr";
                case LANGUAGE.Finnish: return "fi";
                case LANGUAGE.Portuguese: return "pt";
                case LANGUAGE.Serbian: return "rs";
                case LANGUAGE.Slovenian: return "si";
                case LANGUAGE.Dutch: return "nl";
                case LANGUAGE.ChineseSimplified: return "zh";
                case LANGUAGE.Norwegian: return "no";
                case LANGUAGE.Russian: return "ru";
                case LANGUAGE.EnglishUS: return "us";
                case LANGUAGE.Polish: return "pl";
                case LANGUAGE.Chechz: return "cz";
                case LANGUAGE.Slovak: return "sk";
                case LANGUAGE.Bulgarian: return "bg";
                case LANGUAGE.Japanese: return "jp";
                default:
                    throw new InvalidOperationException("Language not yet defined");
            }
            return "";
        }
        public static LANGUAGE LanguageFromCode(string lang)
        {
            switch (lang.ToLower())
            {
                case "en": return LANGUAGE.EnglishUK;
                case "mt":
                    return LANGUAGE.Maltese;
                case "it":
                    return LANGUAGE.Italian;
                case "fr":
                    return LANGUAGE.French;
                case "de":
                    return LANGUAGE.German;
                case "es":
                    return LANGUAGE.Spanish;
                case "hr":
                    return LANGUAGE.Croatian;
                case "se":
                    return LANGUAGE.Swedish;
                case "ro": return LANGUAGE.Romanian;
                case "hu": return LANGUAGE.Hungarian;
                case "tr": return LANGUAGE.Turkish;
                case "gr": return LANGUAGE.Greek;
                case "fi": return LANGUAGE.Finnish;
                case "pt": return LANGUAGE.Portuguese;
                case "rs": return LANGUAGE.Serbian;
                case "si": return LANGUAGE.Slovenian;
                case "nl": return LANGUAGE.Dutch;
                case "zh": return LANGUAGE.ChineseSimplified;
                case "no": return LANGUAGE.Norwegian;
                case "ru": return LANGUAGE.Russian;
                case "us": return LANGUAGE.EnglishUS;
                case "pl": return LANGUAGE.Polish;
                case "cz": return LANGUAGE.Chechz;
                case "sk": return LANGUAGE.Slovak;
                case "bg": return LANGUAGE.Bulgarian;
                case "jp": return LANGUAGE.Japanese;
                default: return LANGUAGE.EnglishUK;
            }
            throw new InvalidOperationException("Language code '" + lang + "' does not exist");
        }

        public enum ACTION_TYPE
        {
            Purchase, VoidPurchase, Authorisation, Capture, VoidCredit, VoidCapture, VoidAuth,
            ExtraCharge,
            PartialRefund,
            OriginalCredit
        }
        public static ACTION_TYPE ActionTypeFromCode(string code)
        {
            switch (code)
            {
                case "1": return ACTION_TYPE.Purchase;
                case "3": return ACTION_TYPE.VoidPurchase;
                case "4": return ACTION_TYPE.Authorisation;
                case "5": return ACTION_TYPE.Capture;
                case "6": return ACTION_TYPE.VoidCredit;
                case "7": return ACTION_TYPE.VoidCapture;
                case "9": return ACTION_TYPE.VoidAuth;
                case "11": return ACTION_TYPE.ExtraCharge;
                case "12": return ACTION_TYPE.PartialRefund;
                case "13": return ACTION_TYPE.OriginalCredit;
            }
            throw new InvalidOperationException("Action type code '" + code + "' does not exist");
        }
        public static string ActionTypeToCode(ACTION_TYPE code)
        {
            switch (code)
            {
                case ACTION_TYPE.Purchase: return "1";
                case ACTION_TYPE.VoidPurchase: return "3";
                case ACTION_TYPE.Authorisation: return "4";
                case ACTION_TYPE.Capture: return "5";
                case ACTION_TYPE.VoidCredit: return "6";
                case ACTION_TYPE.VoidCapture: return "7";
                case ACTION_TYPE.VoidAuth: return "9";
                case ACTION_TYPE.ExtraCharge: return "11";
                case ACTION_TYPE.PartialRefund: return "12";
                case ACTION_TYPE.OriginalCredit: return "13";
            }
            return "";
        }

        public enum LIST_ALL_CARDS
        {
            None, Last, All
        }
        public static string ListAllCardsToCode(LIST_ALL_CARDS code)
        {
            switch (code)
            {
                case LIST_ALL_CARDS.None:
                    return "";
                case LIST_ALL_CARDS.All:
                    return "ALL";
                case LIST_ALL_CARDS.Last:
                    return "LAST";
            }
            return "";
        }


        #endregion
    }

}
