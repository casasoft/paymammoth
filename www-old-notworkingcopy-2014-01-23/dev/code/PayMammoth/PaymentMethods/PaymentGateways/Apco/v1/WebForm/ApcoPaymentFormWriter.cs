﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.WebForm
{
    public class ApcoPaymentFormWriter 
    {

        public ApcoParameters Parameters { get; set; }

        private string getHtml()
        {
            string action = CS.General_v3.Util.PageUtil.GetPageName();
            string ApcoUrl = ApcoConstants.DEFAULT_GATEWAY_URL;
            string paramsValue = CS.General_v3.Util.Text.HtmlEncode(ApcoGateway.GetParamsAsString());
            string html = @"
<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">

<html xmlns=""http://www.w3.org/1999/xhtml"">
<head>
    <title></title>
    
</head>
<body onload=""submitForm();"">
    <form name=""form1"" method=""post"" action="""+action+@""" id=""form1"">

    <input type=""hidden"" name=""params"" id=""params"" value="""+paramsValue+@""" /></form>
    <script type=""text/javascript"">
        function removeElem(elem) {
            var e = document.getElementById(elem);
            if (e != null) {
                e.parentNode.removeChild(e);
            }
        }
        function removeInputs(elem) {
            if (elem != null) {

                var i;
                if (elem.childNodes != null) {
                    for (i = 0; i < elem.childNodes.length; i++) {
                        var curr = elem.childNodes[i];
                        if (curr != elem) {
                            if (curr.tagName != null && curr.tagName.toLowerCase() == ""input"" && curr.id != null && curr.id != 'params') {
                                elem.removeChild(curr);
                            }
                            else {
                                removeInputs(curr);
                            }
                        }
                    }
                }

            }
        }
        function submitForm() {

            removeInputs(document);
            removeElem('__VIEWSTATE');
            var form = document.forms[0];
            form.action = '"+ApcoUrl+@"';

            form.onSubmit = form.onsubmit = function () {
                return true;
            }
            form.submit();
        }

       
    </script>
    
</body>
</html>";
            return html;
        }
        private Page pg = null;
        public ApcoPaymentFormWriter(Page pg)
        {
            this.Parameters = new ApcoParameters();
            this.pg = pg;
         

        }
        private void writeFormToPage()
        {
            
            pg.Response.Clear();
            pg.Response.Write(getHtml());
            pg.Response.End();
        }

     
        
        private bool checkRequirements()
        {
            if (this.Parameters.TotalValue > 0 && !string.IsNullOrEmpty(this.Parameters.StatusURL))
            {
                return true;
            }
            else
            {
                throw new InvalidOperationException("Make sure that you fill in 'TotalValue' and 'StatusUrl'");
                return false;
            }

        }
        private PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.ApcoClient ApcoGateway = null;
        public void WriteApcoForm()
        {
            if (checkRequirements())
            {
                ApcoGateway = new PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.ApcoClient();
                ApcoGateway.Parameters = this.Parameters;
                writeFormToPage();

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

           
            
        }




    }
}
