﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.PaymentRequestTransactionModule;

namespace PayMammoth.PaymentMethods.PaymentGateways.Apco.v1
{





    public class ApcoParameters : IApcoCredentials
        {
            public void FillFromApcoCredentials(ApcoCredentials cred)
            {
                CS.General_v3.Util.ReflectionUtil.CopyProperties<IApcoCredentials>(cred, this);
                
            }
            public void FillFromTransaction(IPaymentRequestTransaction transaction)
            {
                var websiteAccount = transaction.PaymentRequest.WebsiteAccount;
                var request = transaction.PaymentRequest;
                FillFromApcoCredentials(request.WebsiteAccount.GetApcoCredentials());
                //this.ClientAccountReference
                this.ClientAddress = request.GetAddressAsOneLine(includeCountry: false);
                this.SetClientCountry(((IClientDetails)request).ClientCountry);
                this.ClientEmail = ((IClientDetails)request).ClientEmail;
                this.ClientMobileNo = ((IClientDetails)request).ClientMobile;
                this.Currency = ((IPaymentRequestDetails)request).CurrencyCode;
                this.Language = ApcoEnums.ConvertIsoEnumToApcoLanguageEnum(((IPaymentRequestDetails)request).LanguageCode, ((IPaymentRequestDetails)request).LanguageCountryCode);
                this.HideSSLLogo = false;
                this.OrderReference = transaction.ID.ToString();
                //this.Parameters_3DSecure.AllowOnly3DSecure;
                //this.Parameters_3DSecure.ByPass3DSecure;
                //this.Parameters_AntiFraud.PostCheck;
                //this.Parameters_AntiFraud.PreCheck;
                //this.Parameters_AntiFraud.Provider;
                this.ShowSSLPadlock = true;
                this.ShowExtendedError = true;
                this.TotalValue = request.GetTotalPrice();

                string statusUrl = PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.ApcoUrls.GetStatusUrl(request.Identifier);
                string returnUrl = PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.ApcoUrls.GetReturnUrl(request.Identifier);
                string failureUrl = PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.ApcoUrls.GetFailureUrl(request.Identifier);

                this.StatusURL = statusUrl;
                this.RedirectionURL = returnUrl;
              //  this.RedirectionURL = "http://office.casasoft.com.mt/payment/apco/v1/payment-success.aspx";


               // this.RedirectionURL = "http://office.casasoft.com.mt/payment/apco/v1/payment-success.aspx?__pm_id=e41e6556fd514599bef99f314431aa805177345&test=test";
                
                this.Parameters_FailedTransaction.FailedRedirectionURL = failureUrl;



            }

        /// <summary>
            /// A secret word by which to hash the XML document
            /// </summary>
            public string ProfileId { get; set; }
            public string SecretWord { get; set; }
            public double TotalValue { get; set; }
            public CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 Currency { get; set; }
            public ApcoEnums.LANGUAGE Language { get; set; }
            /// <summary>
            /// A unique order reference
            /// </summary>
            public string OrderReference { get; set; }
            /// <summary>
            /// Client account reference. Optional
            /// </summary>
            public string ClientAccountReference { get; set; }
            /// <summary>
            /// The payment gateway transaction id on which to perform an action. Optional. Not needed for Purchase/Authorisation transactions
            /// </summary>
            public string PspID { get; set; }

            public string ClientMobileNo { get; set; }
            public string ClientEmail { get; set; }
            public string ClientAddress { get; set; }
        /// <summary>
            /// The client`s country (3 letter ISO code)

        /// </summary>
            private string _clientCountry { get; set; }
            public void SetClientCountry(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? country)
            {
                if (country.HasValue)
                    _clientCountry = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To3LetterCode(country);
                else
                    _clientCountry = null;
            }

        public string ClientPassportNo { get; set; }
            public string ClientDrivingLicenseNo { get; set; }
            /// <summary>
            /// Whether to validate the card length or not
            /// </summary>
            public bool? ValidateCardLength { get; set; }
            /// <summary>
            /// Whether to do a mod10 check on the card number
            /// </summary>
            public bool? ValidateCardByMod10 { get; set; }
            public string RedirectionURL { get; set; }
            private string _StatusUrl = null;
            /// <summary>
            /// If specified, the result is posted to this address.
            /// </summary>
            
            public string StatusURL
            {
                get { return _StatusUrl; }
                set
                {
                    
                    _StatusUrl = value;
                    if (false) //ODO: temporary, to remove later on
                    {
                        if (!string.IsNullOrEmpty(_StatusUrl))
                        {
                            if (!_StatusUrl.ToLower().StartsWith("https://"))
                                throw new InvalidOperationException("Status url must be on an SSL url, on port 443!");
                        }
                    }
                }
            }
            
            public string UDF1 { get; set; }
            public string UDF2 { get; set; }
            public string UDF3 { get; set; }
            /// <summary>
            /// Specifies if an extended error should be shown to the user, i.e Actual error from the bank
            /// </summary>
            public bool? ShowExtendedError { get; set; }
            public ApcoEnums.ACTION_TYPE ActionType { get; set; }
            public class PARAMETERS_FASTPAY
            {
                /// <summary>
                /// This node is used if a new credit card entered by the client will be restricted only to be used by the same client
                /// </summary>
                public bool? RestrictCard { get; set; }
                /// <summary>
                /// The original URL from which the gateway was called
                /// </summary>
                public string OriginalURL { get; set; }
                /// <summary>
                /// Whether to list all cards used, the last one, or default (none).
                /// </summary>
                public ApcoEnums.LIST_ALL_CARDS ListAllCards { get; set; }
                /// <summary>
                /// This option is used to enable the client to enter a new credit card together with a list of available cards (if any) on his first attempt to process a transaction.
                /// </summary>
                public bool? AllowNewCardOnFirstTry { get; set; }
                /// <summary>
                /// This option is used if the client can have the option to enter a new credit card if his first attempt to process a transaction fails
                /// </summary>
                public bool? AllowNewCardOnFail { get; set; }
                /// <summary>
                /// This option is used if the client can enter the cvv for the selected credit card from the list
                /// </summary>
                public bool? PromptCVV { get; set; }
                /// <summary>
                /// This option is used if the client can enter a new credit card expiry date for the selected credit card from the list
                /// </summary>
                public bool? PromptExpiryDate { get; set; }

            }

            private PARAMETERS_FASTPAY _Parameters_FastPay = null;
            public PARAMETERS_FASTPAY Parameters_FastPay
            {
                get
                {
                    if (_Parameters_FastPay == null) _Parameters_FastPay = new PARAMETERS_FASTPAY();
                    return _Parameters_FastPay;
                }
            }

            public bool HideSSLLogo { get; set; }
            public bool ShowSSLPadlock { get; set; }
            public string ReturnPspID { get; set; }
            public class PARAMETERS_ANTIFRAUD
            {
                /// <summary>
                /// The service to be used for antifraud. Required if Antifraud node is defined.
                /// </summary>
                public string Provider { get; set; }
                /// <summary>
                /// The Anti Fraud check will be performed prior to the transaction being sent to the acquirer/financial institution. The transaction will be declined if the Anti Fraud check results in declined or the score are greater then what the merchant has in the configuration.
                /// </summary>
                public bool? PreCheck { get; set; }
                /// <summary>
                /// The Anti Fraud check will be perform after the transaction has been authorized with the Acquirer.
                /// </summary>
                public bool? PostCheck { get; set; }

            }
            private PARAMETERS_ANTIFRAUD _Parameters_AntiFraud = null;
            public PARAMETERS_ANTIFRAUD Parameters_AntiFraud
            {
                get
                {
                    if (_Parameters_AntiFraud == null) _Parameters_AntiFraud = new PARAMETERS_ANTIFRAUD();
                    return _Parameters_AntiFraud;
                }
            }
            /// <summary>
            /// This node will redirect the user automatically to the specified payment method.
            /// </summary>
            public string ForcePaymentBy { get; set; }
            public class PARAMETERS_3DSECURE
            {
                /// <summary>
                /// Bypass node will skip 3DSecure procedure
                /// </summary>
                public bool? ByPass3DSecure { get; set; }
                /// <summary>
                /// Abort transaction if cardholder is not eligible for 3DSecure
                /// </summary>
                public bool? AllowOnly3DSecure { get; set; }

            }
            private PARAMETERS_3DSECURE _Parameters_3DSecure = null;
            public PARAMETERS_3DSECURE Parameters_3DSecure
            {
                get
                {
                    if (_Parameters_3DSecure == null) _Parameters_3DSecure = new PARAMETERS_3DSECURE();
                    return _Parameters_3DSecure;
                }
            }
            public class PARAMETERS_FAILEDTRANSACTION
            {
                /// <summary>
                /// Redirect to a specified URL when transaction fails
                /// </summary>
                public string FailedRedirectionURL { get; set; }
                /// <summary>
                /// This tab should be used only for redirection URL to open up in a new window.
                /// </summary>
                public bool? RedirectInPopup { get; set; }

            }
            private PARAMETERS_FAILEDTRANSACTION _Parameters_FailedTransaction = null;
            public PARAMETERS_FAILEDTRANSACTION Parameters_FailedTransaction
            {
                get
                {
                    if (_Parameters_FailedTransaction == null) _Parameters_FailedTransaction = new PARAMETERS_FAILEDTRANSACTION();
                    return _Parameters_FailedTransaction;
                }
            }
            public void CheckRequirements()
            {

                StringBuilder sb = new StringBuilder();
                if (string.IsNullOrEmpty(SecretWord))
                    sb.AppendLine("Secret Word is required");
                if (string.IsNullOrEmpty(ProfileId))
                    sb.AppendLine("Profile ID is required");
                if (this.TotalValue <= 0)
                    sb.AppendLine("Value is required and must be a positive number");
                if (string.IsNullOrEmpty(OrderReference))
                    sb.AppendLine("Order Reference is required");
                if (string.IsNullOrEmpty(RedirectionURL))
                    sb.AppendLine("Redirection URL is required");
                if ((this.ActionType != ApcoEnums.ACTION_TYPE.Purchase && this.ActionType == ApcoEnums.ACTION_TYPE.Authorisation))
                {
                    if (string.IsNullOrEmpty(PspID))
                        sb.AppendLine("Psp ID is required");
                }
                if (sb.Length > 0)
                    throw new InvalidOperationException("Required parameters not entered:\r\n\r\n" + sb.ToString());

            }
            #region XML Generation
            private void createXMLNode(XmlDocument doc, XmlNode parent, string id, object value)
            {
                createXMLNode(doc, parent, id, value, true);
            }

            private void createXMLNode(XmlDocument doc, XmlNode parent, string id, object value, bool optional)
            {
                XmlNode node = doc.CreateElement(id);
                string s = "";
                if (value != null)
                {
                    if (value is string || value is int)
                    {
                        s = value.ToString();
                    }
                    else if (value is double)
                    {
                        s = ((double)value).ToString("0.00");
                    }
                    else if (value is bool)
                    {
                        if ((bool)value)
                            s = "Yes";
                        else
                            s = "No";
                    }
                }
                if (!string.IsNullOrEmpty(s) || !optional)
                {
                    node.InnerText = s;
                    parent.AppendChild(node);
                }
            }
            public ApcoParameters()
            {
               
                this.ShowSSLPadlock = true;
                
            }

            public XmlDocument ToXML()
            {
                CheckRequirements();
                XmlDocument doc = new XmlDocument();
                XmlNode transaction = doc.CreateElement("Transaction");
                doc.AppendChild(transaction);
                XmlAttribute hash = doc.CreateAttribute("hash");
                hash.InnerText = SecretWord;
                transaction.Attributes.Append(hash);
                createXMLNode(doc, transaction, "ProfileID", this.ProfileId);
                createXMLNode(doc, transaction, "Value", this.TotalValue);
                createXMLNode(doc, transaction, "Curr", ApcoEnums.CurrencyToCode(this.Currency));
                createXMLNode(doc, transaction, "Lang", ApcoEnums.LanguageToCode(this.Language));
                createXMLNode(doc, transaction, "ORef", this.OrderReference);
                createXMLNode(doc, transaction, "ClientAcc", this.ClientAccountReference);
                createXMLNode(doc, transaction, "PspID", this.PspID);
                createXMLNode(doc, transaction, "MobileNo", this.ClientMobileNo);
                createXMLNode(doc, transaction, "Email", this.ClientEmail);
                createXMLNode(doc, transaction, "Address", this.ClientAddress);
                createXMLNode(doc, transaction, "Country", this._clientCountry);
                createXMLNode(doc, transaction, "PassportNo", this.ClientPassportNo);
                createXMLNode(doc, transaction, "DrivingLic", this.ClientDrivingLicenseNo);
                createXMLNode(doc, transaction, "ValCardLen", this.ValidateCardLength);
                createXMLNode(doc, transaction, "ValCardMod10", this.ValidateCardByMod10);
                createXMLNode(doc, transaction, "RedirectionURL", this.RedirectionURL);
                createXMLNode(doc, transaction, "status_url", this.StatusURL);
                createXMLNode(doc, transaction, "UDF1", this.UDF1, false);
                createXMLNode(doc, transaction, "UDF2", this.UDF2, false);
                createXMLNode(doc, transaction, "UDF3", this.UDF3, false);
                createXMLNode(doc, transaction, "ExtendedErr", this.ShowExtendedError);
                createXMLNode(doc, transaction, "ActionType", ApcoEnums.ActionTypeToCode(this.ActionType));
                XmlNode fastpay = doc.CreateElement("FastPay");
                createXMLNode(doc, fastpay, "CardRestrict", this.Parameters_FastPay.RestrictCard);
                createXMLNode(doc, fastpay, "OriginURL", this.Parameters_FastPay.OriginalURL);
                createXMLNode(doc, fastpay, "ListAllCards", ApcoEnums.ListAllCardsToCode(this.Parameters_FastPay.ListAllCards));
                createXMLNode(doc, fastpay, "NewCard1Try", this.Parameters_FastPay.AllowNewCardOnFirstTry);
                createXMLNode(doc, fastpay, "NewCardOnFail", this.Parameters_FastPay.AllowNewCardOnFail);
                createXMLNode(doc, fastpay, "PromptCVV", this.Parameters_FastPay.PromptCVV);
                createXMLNode(doc, fastpay, "PromptExpiry", this.Parameters_FastPay.PromptExpiryDate);
                if (!string.IsNullOrEmpty(fastpay.InnerXml))
                    transaction.AppendChild(fastpay);
                createXMLNode(doc, transaction, "HideSSLLogo", this.HideSSLLogo);
                createXMLNode(doc, transaction, "ShowSSLPadlock", this.ShowSSLPadlock);

                XmlNode antifraud = doc.CreateElement("AntiFraud");
                createXMLNode(doc, antifraud, "Provider", this.Parameters_AntiFraud.Provider);
                createXMLNode(doc, antifraud, "PreCheck", this.Parameters_AntiFraud.PreCheck);
                createXMLNode(doc, antifraud, "PostCheck", this.Parameters_AntiFraud.PostCheck);
                if (!string.IsNullOrEmpty(antifraud.InnerXml))
                    transaction.AppendChild(antifraud);
                createXMLNode(doc, transaction, "return_pspid", this.ReturnPspID);

                XmlNode threedsecure = doc.CreateElement("3DSecure");
                createXMLNode(doc, threedsecure, "3DSOnly", this.Parameters_3DSecure.AllowOnly3DSecure);
                createXMLNode(doc, threedsecure, "Bypass", this.Parameters_3DSecure.ByPass3DSecure);
                if (!string.IsNullOrEmpty(threedsecure.InnerXml))
                    transaction.AppendChild(threedsecure);

                XmlNode failedtrans = doc.CreateElement("FailedTrans");

                createXMLNode(doc, failedtrans, "FailedRedirectionURL", this.Parameters_FailedTransaction.FailedRedirectionURL);
                createXMLNode(doc, failedtrans, "Popup", this.Parameters_FailedTransaction.RedirectInPopup);
                if (!string.IsNullOrEmpty(failedtrans.InnerXml))
                    transaction.AppendChild(failedtrans);

                string md5hash = CS.General_v3.Util.Cryptography.GetMd5Sum(doc.OuterXml);
                hash.InnerText = md5hash;
                return doc;
            }
            #endregion
        }
        

    

}
