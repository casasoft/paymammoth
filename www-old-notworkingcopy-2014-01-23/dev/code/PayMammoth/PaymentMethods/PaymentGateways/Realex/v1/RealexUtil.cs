﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.RedirectMethod;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1
{
    public static class RealexUtil
    {
        public static RealexRequest InitialiseRequest(IPaymentRequest requestDetails)
        {
            var cred = requestDetails.WebsiteAccount.GetRealexCredentials();
            RealexRequest realexRequest = new RealexRequest(cred);
            
            var transaction =  requestDetails.StartNewTransaction(Connector.Enums.PaymentMethodSpecific.PaymentGateway_Realex);
            realexRequest.FillFromRequest(transaction);
            return realexRequest;
        }

    }
}
