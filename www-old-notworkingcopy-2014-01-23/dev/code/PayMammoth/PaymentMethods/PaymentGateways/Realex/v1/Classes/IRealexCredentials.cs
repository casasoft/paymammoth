﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.Classes
{
    public interface IRealexCredentials
    {
        string MerchantId { get; set; }
        string SecretWord { get; set; }
        string AccountName { get; set; }
    }
}
