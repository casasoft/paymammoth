﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.RedirectMethod;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.Classes
{
    public class RealexResponseCheckResponse
    {
        public bool Success
        {
            get { return this.Status == Enums.PaymentStatus.Success; }
        }
        public Enums.PaymentStatus Status { get; set; }
        public RealexResponse RealexResponse { get; set; }
        public IPaymentRequestTransaction Transaction { get; set; }
        public RealexResponseCheckResponse()
        {
            
            this.Status = Enums.PaymentStatus.GenericError;
        }

    }
}
