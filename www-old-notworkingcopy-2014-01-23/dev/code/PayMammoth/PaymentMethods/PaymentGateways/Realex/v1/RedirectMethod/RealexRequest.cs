﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.Classes;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Util;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.RedirectMethod
{
    public class RealexRequest : BaseRealexParameters
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(RealexRequest));

        


        public RealexRequest(IRealexCredentials credentials) : base(credentials)
        {
           
            this.AutoSettleTransaction = true;
        }

        public string GenerateSHA1Hash()
        {
            string hash1 = null;
            string hash2 = null;
            string timestamp = this.timeStampStr;
            {
                string temp1 = CS.General_v3.Util.Cryptography.GetSHA1Hash(timestamp + "." + realexMerchantID + "." + OrderID + "." + amountInCents.GetValueOrDefault().ToString() + "." + currencyCode);
                temp1 = temp1.ToLower();

                string temp2 = temp1 + "." + this.realexCredentials.SecretWord;

                hash1 = CS.General_v3.Util.Cryptography.GetSHA1Hash(temp2);
                hash1 = hash1.ToLower();
            }
            {
                // generate the hash according to Realex's rules
                string temp1 = Dovetail.Realex.Realauth.RealexProcessor.ComputeSha1Hash(timestamp + "." + realexMerchantID + "." + OrderID + "." + amountInCents.GetValueOrDefault().ToString() + "." + currencyCode);
                temp1 = temp1.ToLower();
                string temp2 = temp1 + "." + this.realexCredentials.SecretWord;
                hash2 = Dovetail.Realex.Realauth.RealexProcessor.ComputeSha1Hash(temp2);
                hash2 = hash2.ToLower();

            }
            return hash1;
        }


        public void PostFormAndOutputResultToCurrentPage(System.Web.UI.Page pg)
        {
            this.SHA1Hash = GenerateSHA1Hash();

            string postData = CS.General_v3.Util.ListUtil.ConvertNameValueCollectionToString(base.createNameValueCollectionWithPaymentServerInfo());
            string serverUrl = this.realexServerUrl; 
            if (logger.IsDebugEnabled)
            {
                logger.Debug("Realex start request. Amount: " + this.Amount.ToString("0.000") + "| Form Post Data: " + postData + ". ServerURL: " + serverUrl);
            }
            
            var response = CS.General_v3.Util.Web.HTTPRequest( serverUrl, CS.General_v3.Enums.HTTP_METHOD.POST, null, postData);
            StreamReader sr = new StreamReader(response.GetResponseStream());
            string s = sr.ReadToEnd();
            pg.Response.Clear();
            pg.Response.Write(s);
            pg.Response.End();
        }

        private void fillDetailsToTransaction()
        {
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                _transaction.SetPaymentParametersFromObject(this, null,
                    x => x.realexCredentials.AccountName,
                    x => x.realexCredentials.MerchantId);
                _transaction.Update();
                t.Commit();
            }
        }

        public void OutputHiddenFieldsToFormAndChangeAction(System.Web.UI.Page pg)
        {
            string postData = CS.General_v3.Util.ListUtil.ConvertNameValueCollectionToString(base.createNameValueCollectionWithPaymentServerInfo());
            if (logger.IsDebugEnabled)
            {
                logger.Debug("Realex start request. Amount: " + this.Amount.ToString("0.000") + "| Form Post Data: " + postData + ". ServerURL: " + realexServerUrl);
            }
            this.SHA1Hash = GenerateSHA1Hash();
            
            

            var nv = base.createNameValueCollectionWithPaymentServerInfo();
            CS.General_v3.Util.PageUtil.WriteNameValueAsHiddenFieldsToForm(pg, nv);
            //2011-03-29 Commented by Mark
            fillDetailsToTransaction();
            CS.General_v3.Util.PageUtil.ChangeFormActionViaJavascript(pg, realexServerUrl);

            /*
            string postData = CS.General_v3.Util.ListUtil.ConvertNameValueCollectionToString(base.createNameValueCollectionWithPaymentServerInfo());
           
            var response = CS.General_v3.Util.Web.HTTPRequest(RealexConstants.REDIRECT_SERVER_URL, CS.General_v3.Enums.HTTP_METHOD.POST, null, postData);
            StreamReader sr = new StreamReader(response.GetResponseStream());
            string s = sr.ReadToEnd();
            pg.Response.Clear();
            pg.Response.Write(s);
            pg.Response.End();*/
        }


        public void SetCurrencyFromGeneralEnum(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 currency)
        {
            this.Currency = RealexEnums.GetRealexCurrencyCodeFromISOEnum(currency);
        }
        private IPaymentRequestTransaction _transaction = null;
        public void FillFromRequest(IPaymentRequestTransaction transaction)
        {
            _transaction = transaction;
            var requestDetails = transaction.PaymentRequest;
            IPaymentRequestDetails paymentDetails = requestDetails;
            IClientDetails clientDetails = requestDetails;
            this.Amount = requestDetails.GetTotalPrice();
            //this.BillingCountry = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To3LetterCode(clientDetails.ClientCountry);
            //this.BillingPostCode = clientDetails.ClientPostCode;
            this.Currency = RealexEnums.GetRealexCurrencyCodeFromISOEnum(paymentDetails.CurrencyCode);
            this.CustomerNumber = clientDetails.ClientReference;
            this.ExtraReference = paymentDetails.Reference;
            this.OrderID = transaction.ID.ToString();
            this.ShippingCountry = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To3LetterCode(clientDetails.ClientCountry);
            this.ShippingPostCode = clientDetails.ClientPostCode;
            
        }
    }
}
