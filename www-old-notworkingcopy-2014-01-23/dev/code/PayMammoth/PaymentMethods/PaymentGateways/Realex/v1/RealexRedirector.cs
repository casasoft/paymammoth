﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Modules.PaymentRequestModule;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1
{
    public class RealexRedirector : IPaymentRedirector
    {

        #region IPaymentRedirector Members

        public Enums.RedirectionResult Redirect(IPaymentRequest paymentRequest)
        {
            Enums.RedirectionResult result = Enums.RedirectionResult.Success;
            string url = RealexUrls.GetPrePaymentUrl(paymentRequest.Identifier);
            CS.General_v3.Util.PageUtil.RedirectPage(url);

            return result;
            

        }

        #endregion
    }
}
