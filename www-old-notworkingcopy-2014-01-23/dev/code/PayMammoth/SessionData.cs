﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth
{
    public static class SessionData
    {

        public static string LastMessage
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObject<string>("__LastMessage"); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject("__LastMessage", value); }
        }
        public static CS.General_v3.Enums.STATUS_MSG_TYPE? LastMessageType
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObject<CS.General_v3.Enums.STATUS_MSG_TYPE?>("__LastMessageType"); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject("__LastMessageType", value); }
        }

        public static void SetStatusMsg(string msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType)
        {
            LastMessage = msg;
            LastMessageType = msgType;
        }
        public static void ResetStatusMsg()
        {
            LastMessage = null;
            LastMessageType = null;
        }

    }
}
