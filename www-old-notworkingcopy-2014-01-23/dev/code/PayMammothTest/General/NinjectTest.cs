﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Ninject.Planning.Bindings;
using NUnit.Framework;

namespace PayMammothTest.General
{
    [TestFixture]
    public class NinjectTest
    {

        public interface IWeapon
        {
            string Fight();
        }

        public class Sword : IWeapon
        {

            #region IWeapon Members

            public string Fight()
            {
                return "Draws out his sword!!";
            }

            #endregion
        }

        public class Bow : IWeapon
        {
            public string Fight()
            {
                return "Throws an arrow";
            }

        }

        [Test]
        public void test()
        {
            StandardKernel k = new StandardKernel();
           // k.Rebind
            k.Rebind<IWeapon>().To<Sword>();

            k.Rebind<IWeapon>().To<Bow>();
            
            var weapon = k.Get<IWeapon>();

            string s = weapon.Fight();


            
        }

        public class AnimalImpl
        {
            public virtual int Test()
            {
                return 5;
            }
        }

        public interface IAnimal
        {
            int Test();
        }

        [Test]
        public void TestMocks()
        {

            var mock = new Moq.Mock<AnimalImpl>();

            mock.Verify(x => x.Test(), Moq.Times.Never(), null);
            int k = mock.Object.Test();
            mock.Verify(x => x.Test(), Moq.Times.Exactly(1), null);
            mock.Setup(x => x.Test()).Returns(8);
            k = mock.Object.Test();
            mock.Verify(x => x.Test(), Moq.Times.Exactly(2), null);
            //var transaction = Util.DbUtil.CreateTestPaymentRequestTransaction();
            //BusinessLogic_v3.Util.InversionOfControlUtil.Register(mockedRecurringPaymentChecker.Object);

            //transaction.MarkAsSuccessful(true);

            //mockedRecurringPaymentChecker.Verify(x => x.CreateRecurringProfileIfRequired(transaction), Moq.Times.Exactly(1));



        }
    }
}
