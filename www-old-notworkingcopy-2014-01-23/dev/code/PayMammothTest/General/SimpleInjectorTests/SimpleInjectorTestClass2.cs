﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using BusinessLogic_v3.Classes.Decorators;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using CS.TestGeneral_v3;
using NUnit.Framework;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammothTest.General.AutofacTests.SampleClasses;
using SimpleInjector;
using SimpleInjector.Extensions;
namespace PayMammothTest.General.SimpleInjectorTests
{
    public class SimpleInjectorTestClass2 : BaseTestFixtureMySQL
    {



        [Test]
        public void Test1()
        {
            Container container = new Container();
            container.Register<IWeapon>(() => new Bow());
            container.Register<HumanFighter>();

            var humanFighter1 = container.GetInstance<HumanFighter>();
            var humanFighter2 = container.GetInstance<HumanFighter>();
            string s = humanFighter1.Fight();
            Assert.That(humanFighter1 != humanFighter2);


        }
        [Test]
        public void Test2()
        {
            Container container = new Container();
            container.RegisterSingle<IWeapon>(() => new Bow());
            container.RegisterSingle<HumanFighter>();

            var humanFighter1 = container.GetInstance<HumanFighter>();
            var humanFighter2 = container.GetInstance<HumanFighter>();

            string s = humanFighter1.Fight();
            Assert.That(humanFighter1 == humanFighter2);


        }

        [Test]
        public void Test2c()
        {
            BusinessLogic_v3.Util.InversionOfControlUtil.RegisterAllComponentsInAssemblyDecoratedWithIocComponent(Assembly.GetExecutingAssembly());
         //   BusinessLogic_v3.Util.InversionOfControlUtil.RegisterAllComponentsInAssemblyDecoratedWithIocComponent(this.GetType().Assembly);
            var humanFighter1 = BusinessLogic_v3.Util.InversionOfControlUtil.Get<HumanFighter>();
            

            string s = humanFighter1.Fight();
            


        }
        [Test]
        public void Test2d()
        {
            BusinessLogic_v3.Util.InversionOfControlUtil.RegisterAllComponentsInAssemblyDecoratedWithIocComponent(Assembly.GetExecutingAssembly());
            //   BusinessLogic_v3.Util.InversionOfControlUtil.RegisterAllComponentsInAssemblyDecoratedWithIocComponent(this.GetType().Assembly);
            var humanFighter1 = BusinessLogic_v3.Util.InversionOfControlUtil.Get<HumanFighter>();
            var humanFighter2 = BusinessLogic_v3.Util.InversionOfControlUtil.Get<HumanFighter>();
            var humanFighter3 = BusinessLogic_v3.Util.InversionOfControlUtil.Get<HumanFighter>();
            humanFighter1.Fight();
            humanFighter2.Fight();
            humanFighter3.Fight();
            var humanFighter4 = BusinessLogic_v3.Util.InversionOfControlUtil.Get<HumanFighter>();
            humanFighter4.Fight();
            Assert.That(humanFighter1.FightCount == 4);
            Assert.That(humanFighter2.FightCount == 4);
            Assert.That(humanFighter3.FightCount == 4);
            Assert.That(humanFighter4.FightCount == 4);
            



        }
        [Test]
        public void Test2e()
        {
            BusinessLogic_v3.Util.InversionOfControlUtil.RegisterAllComponentsInAssemblyDecoratedWithIocComponent(Assembly.GetExecutingAssembly());
            //   BusinessLogic_v3.Util.InversionOfControlUtil.RegisterAllComponentsInAssemblyDecoratedWithIocComponent(this.GetType().Assembly);
            var humanFighter1 = BusinessLogic_v3.Util.InversionOfControlUtil.Get<HumanFighter>();
            var humanFighter2 = BusinessLogic_v3.Util.InversionOfControlUtil.Get<HumanFighter>();
            var humanFighter3 = BusinessLogic_v3.Util.InversionOfControlUtil.Get<HumanFighter>();
            humanFighter1.Fight();
            humanFighter2.Fight();
            humanFighter3.Fight();
            var humanFighter4 = BusinessLogic_v3.Util.InversionOfControlUtil.Get<HumanFighter>();
            humanFighter4.Fight();
            Assert.That(humanFighter1.FightCount == 1);
            Assert.That(humanFighter2.FightCount == 1);
            Assert.That(humanFighter3.FightCount == 1);
            Assert.That(humanFighter4.FightCount == 1);




        }
        [Test]
        public void Test2b()
        {
            Container container = new Container();
            
            container.RegisterSingle<Bow>();
            container.RegisterSingle<HumanFighter>();

            var humanFighter1 = container.GetInstance<HumanFighter>();
            var humanFighter2 = container.GetInstance<HumanFighter>();

            string s = humanFighter1.Fight();
            Assert.That(humanFighter1 == humanFighter2);


        }
        //[Test]
        //public void Test3()
        //{
        //    ContainerBuilder builder = new ContainerBuilder();
        //    builder.RegisterType<HumanFighter>();
        //    builder.RegisterType<Bow>().As<IWeapon>();

        //    var container = builder.Build();

        //    var humanFighter1 = container.Resolve<HumanFighter>();
        //    var humanFighter2 = container.Resolve<HumanFighter>();
        //    string s = humanFighter1.Fight();
        //    Assert.That(s.ToLower().Contains("bow"));


        //}
        //[Test]
        //public void Test4()
        //{
        //    ContainerBuilder builder = new ContainerBuilder();
        //    var a = Assembly.GetExecutingAssembly();

        //    builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
        //        .AsSelf()
        //        .AsImplementedInterfaces();


        //    var container = builder.Build();

        //    var humanFighter1 = container.Resolve<HumanFighter>();
        //    var humanFighter2 = container.Resolve<HumanFighter>();
        //    string s = humanFighter1.Fight();
        //    Assert.That(s.ToLower().Contains("bow"));

        //    var mockWeapon = new Mock<IWeapon>();

        //    HumanFighter fighterToTest = new HumanFighter(mockWeapon.Object);
        //    fighterToTest.Fight();
        //    mockWeapon.Verify(x => x.Attack(), Times.Once());
        //    //verify that  mockWeapon.Fight() was called
        //}
        //[Test]
        //public void Test5()
        //{


        //    var mockWeapon = new Mock<IWeapon>();

        //    HumanFighter fighterToTest = new HumanFighter(mockWeapon.Object);
        //    fighterToTest.Fight();
        //    mockWeapon.Verify(x => x.Attack(), Times.Once());
        //    //verify that  mockWeapon.Fight() was called
        //}
        //[Test]
        //public void Test6()
        //{

        //    ContainerBuilder builder = new ContainerBuilder();
        //    var a = Assembly.GetExecutingAssembly();

        //    builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
        //        .AsSelf()
        //        .AsImplementedInterfaces();


        //    builder.RegisterType<Bow>().Named<IWeapon>("bow");
        //    builder.RegisterDecorator<IWeapon>((c) => new WeaponWithJewelDecorator(c), "bow", "jewelDecorator");
        //    builder.RegisterDecorator<IWeapon>((c) => new WeaponWithSteelDecorator(c), "jewelDecorator");
        //    var container = builder.Build();

        //    var humanFighter1 = container.Resolve<HumanFighter>();
        //    var humanFighter2 = container.Resolve<HumanFighter>();
        //    string s = humanFighter1.Fight();
        //    Assert.That(s.ToLower().Contains("jewel"));

        //    var mockWeapon = new Mock<IWeapon>();

        //    HumanFighter fighterToTest = new HumanFighter(mockWeapon.Object);
        //    fighterToTest.Fight();
        //    mockWeapon.Verify(x => x.Attack(), Times.Once());
        //}
        //[Test]
        //public void Test7()
        //{

        //    ContainerBuilder builder = new ContainerBuilder();
        //    var a = Assembly.GetExecutingAssembly();

        //    builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
        //        .AsSelf()
        //        .AsImplementedInterfaces();


        //    builder.RegisterType<Bow>().As<IWeapon>();
        //    builder.RegisterDecorator<IWeapon>((c) => new WeaponWithJewelDecorator(c), null);

        //    var container = builder.Build();

        //    var humanFighter1 = container.Resolve<HumanFighter>();
        //    var humanFighter2 = container.Resolve<HumanFighter>();
        //    string s = humanFighter1.Fight();
        //    Assert.That(s.ToLower().Contains("jewel"));

        //    var mockWeapon = new Mock<IWeapon>();

        //    HumanFighter fighterToTest = new HumanFighter(mockWeapon.Object);
        //    fighterToTest.Fight();
        //    mockWeapon.Verify(x => x.Attack(), Times.Once());
        //}

        //[Test]
        //public void Test8()
        //{

        //    ContainerBuilder builder = new ContainerBuilder();
        //    var a = Assembly.GetExecutingAssembly();

        //    builder.RegisterType<CreateNewNotificationCommandHandler>().Named<IDbCommandHandler<CreateNewNotificationCommand>>("implementor");
        //    //  builder.RegisterType<RecurringPaymentMarkAsFailureCommandHandler>().AsSelf().AsImplementedInterfaces().Named("implementor", typeof(IDbCommandHandler<>));


        //    // builder.RegisterAssemblyTypes(a).Where(t => t.IsAssignableFrom(typeof(IDbCommandHandler<>)))
        //    //     .Named("dbCommandHandler", typeof(IDbCommandHandler<>))
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();

        //    //builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();

        //    builder.RegisterDecorator<IDbCommandHandler<CreateNewNotificationCommand>>((c) => new DbCommandWithTransactionHandlerDecorator<CreateNewNotificationCommand>(c), "implementor");


        //    builder.RegisterGenericDecorator(typeof(BusinessLogic_v3.Classes.Decorators.DbCommandWithTransactionHandlerDecorator<>),
        //                                    typeof(BusinessLogic_v3.Classes.Interfaces.DbCommands.IDbCommandHandler<>),
        //                                    fromKey: "implementor2");
        //    //builder.RegisterGenericDecorator(typeof(BusinessLogic_v3.Classes.Decorators.DbCommandWithTransactionHandlerDecorator<>),
        //    //                                typeof(BusinessLogic_v3.Classes.Interfaces.DbCommands.IDbCommandHandler<>),
        //    //                                fromKey: "implementor2");


        //    var container = builder.Build();
        //    var handler = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand>>();
        //    handler.Handle(null);
        //}
        //[Test]
        //public void Test9()
        //{

        //    ContainerBuilder builder = new ContainerBuilder();
        //    var a = Assembly.GetExecutingAssembly();

        //    //builder.RegisterType<CreateNewNotificationCommandHandler>().Named<IDbCommandHandler<CreateNewNotificationCommand>>("implementor");

        //    builder.RegisterType<CreateNewNotificationCommandHandler>().Named("implementor", typeof(IDbCommandHandler<CreateNewNotificationCommand>));
        //    //  builder.RegisterType<RecurringPaymentMarkAsFailureCommandHandler>().AsSelf().AsImplementedInterfaces().Named("implementor", typeof(IDbCommandHandler<>));


        //    // builder.RegisterAssemblyTypes(a).Where(t => t.IsAssignableFrom(typeof(IDbCommandHandler<>)))
        //    //     .Named("dbCommandHandler", typeof(IDbCommandHandler<>))
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();

        //    //builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();



        //    builder.RegisterGenericDecorator(typeof(DbCommandWithTransactionHandlerDecorator<>),
        //                                    typeof(IDbCommandHandler<>),
        //                                    fromKey: "implementor");
        //    //builder.RegisterGenericDecorator(typeof(BusinessLogic_v3.Classes.Decorators.DbCommandWithTransactionHandlerDecorator<>),
        //    //                                typeof(BusinessLogic_v3.Classes.Interfaces.DbCommands.IDbCommandHandler<>),
        //    //                                fromKey: "implementor2");


        //    var container = builder.Build();
        //    var handler = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand>>();
        //    handler.Handle(null);
        //}
        //[Test]
        //public void Test10()
        //{

        //    ContainerBuilder builder = new ContainerBuilder();
        //    var a = Assembly.GetExecutingAssembly();

        //    builder.RegisterType<CreateNewNotificationCommandHandler>().Named<IDbCommandHandler<CreateNewNotificationCommand>>("command");
        //    builder.RegisterType<CreateNewNotificationCommandHandler_2>().Named<IDbCommandHandler<CreateNewNotificationCommand_2>>("command");


        //    //  builder.RegisterType<RecurringPaymentMarkAsFailureCommandHandler>().AsSelf().AsImplementedInterfaces().Named("implementor", typeof(IDbCommandHandler<>));


        //    // builder.RegisterAssemblyTypes(a).Where(t => t.IsAssignableFrom(typeof(IDbCommandHandler<>)))
        //    //     .Named("dbCommandHandler", typeof(IDbCommandHandler<>))
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();

        //    //builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();



        //    builder.RegisterGenericDecorator(typeof(DbCommandWithTransactionHandlerDecorator<>),
        //                                    typeof(IDbCommandHandler<>),
        //                                    fromKey: "command");
        //    //builder.RegisterGenericDecorator(typeof(BusinessLogic_v3.Classes.Decorators.DbCommandWithTransactionHandlerDecorator<>),
        //    //                                typeof(BusinessLogic_v3.Classes.Interfaces.DbCommands.IDbCommandHandler<>),
        //    //                                fromKey: "implementor2");


        //    var container = builder.Build();
        //    var handler1 = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand>>();
        //    var handler2 = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand_2>>();
        //    handler1.Handle(null);
        //    handler2.Handle(null);
        //}

        //private List<Type> getTypesThatImplementIDbCommandHandler(IEnumerable<Assembly> assemblyList)
        //{
        //    List<Type> list = new List<Type>();
        //    foreach (var a in assemblyList)
        //    {
        //        var matches = a.GetTypes().Where(t => typeof(IDbCommandHandlerStub).IsAssignableFrom(t));
        //        list.AddRange(matches);
        //    }
        //    return list;
        //}


        //private void registerDbCommands(List<Type> dbCommandHandlerTypes, ContainerBuilder builder)
        //{
        //    foreach (var t in dbCommandHandlerTypes)
        //    {
        //        var interfaces = t.GetInterfaces();
        //        foreach (var i in interfaces)
        //        {
        //            builder.RegisterType(t).Named("dbCommand", i);
        //        }

        //    }
        //}

        //[Test]
        //public void Test11()
        //{

        //    ContainerBuilder builder = new ContainerBuilder();
        //    var a = Assembly.GetExecutingAssembly();



        //    var assemblies = CS.General_v3.Classes.Application.AppInstance.Instance.GetProjectAssemblies().ToList();
        //    assemblies.Add(a);
        //    var dbCommandHandlerTypes = getTypesThatImplementIDbCommandHandler(assemblies);
        //    registerDbCommands(dbCommandHandlerTypes, builder);



        //    //  builder.RegisterType<RecurringPaymentMarkAsFailureCommandHandler>().AsSelf().AsImplementedInterfaces().Named("implementor", typeof(IDbCommandHandler<>));


        //    // builder.RegisterAssemblyTypes(a).Where(t => t.IsAssignableFrom(typeof(IDbCommandHandler<>)))
        //    //     .Named("dbCommandHandler", typeof(IDbCommandHandler<>))
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();

        //    //builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();



        //    builder.RegisterGenericDecorator(typeof(DbCommandWithTransactionHandlerDecorator<>),
        //                                    typeof(IDbCommandHandler<>),
        //                                    fromKey: "dbCommand", toKey: "dbCommandWithTransaction").SingleInstance();

        //    builder.RegisterGenericDecorator(typeof(DbOptimisticConcurrencyRetryDecorator<>),
        //                                    typeof(IDbCommandHandler<>),
        //                                    fromKey: "dbCommandWithTransaction").SingleInstance();
        //    //builder.RegisterGenericDecorator(typeof(BusinessLogic_v3.Classes.Decorators.DbCommandWithTransactionHandlerDecorator<>),
        //    //                                typeof(BusinessLogic_v3.Classes.Interfaces.DbCommands.IDbCommandHandler<>),
        //    //                                fromKey: "implementor2");


        //    var container = builder.Build();
        //    var handler1 = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand>>();
        //    var handler2 = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand_2>>();
        //    handler1.Handle(null);
        //    handler2.Handle(null);
        //}
    }
}
