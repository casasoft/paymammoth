﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Autofac;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Decorators;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Modules.NotificationMessageModule.Commands;

using PayMammothTest.General.AutofacTests.SampleClasses;

namespace PayMammothTest.General.AutofacTests
{
    [TestFixture]
    public class AutofacTests1 : BaseTestFixtureMySQL
    {


        [Test]
        public void Test1()
        {
            ContainerBuilder builder = new ContainerBuilder();
            
            builder.Register(c => new SampleClasses.HumanFighter(c.Resolve<IWeapon>()));
            builder.Register<IWeapon>(c => new Bow());
            var container = builder.Build();
            
            var humanFighter1 = container.Resolve<HumanFighter>();
            var humanFighter2 = container.Resolve<HumanFighter>();
            string s = humanFighter1.Fight();
            Assert.That(humanFighter1 != humanFighter2);
            

        }
        [Test]
        public void Test2()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.Register(c => new SampleClasses.HumanFighter(c.Resolve<IWeapon>())).SingleInstance();
            builder.Register<IWeapon>(c => new Bow());
            var container = builder.Build();

            var humanFighter1 = container.Resolve<HumanFighter>();
            var humanFighter2 = container.Resolve<HumanFighter>();
            string s = humanFighter1.Fight();
            Assert.That(humanFighter1 == humanFighter2);


        }
        [Test]
        public void Test3()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<HumanFighter>();
            builder.RegisterType<Bow>().As<IWeapon>();
            
            var container = builder.Build();

            var humanFighter1 = container.Resolve<HumanFighter>();
            var humanFighter2 = container.Resolve<HumanFighter>();
            string s = humanFighter1.Fight();
            Assert.That(s.ToLower().Contains("bow"));


        }
        [Test]
        public void Test4()
        {
            ContainerBuilder builder = new ContainerBuilder();
            var a = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(a).Where(t=>t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
                .AsSelf()
                .AsImplementedInterfaces();
            

            var container = builder.Build();

            var humanFighter1 = container.Resolve<HumanFighter>();
            var humanFighter2 = container.Resolve<HumanFighter>();
            string s = humanFighter1.Fight();
            Assert.That(s.ToLower().Contains("bow"));

            var mockWeapon = new Mock<IWeapon>();

            HumanFighter fighterToTest = new HumanFighter(mockWeapon.Object);
            fighterToTest.Fight();
            mockWeapon.Verify(x => x.Attack(), Times.Once());
            //verify that  mockWeapon.Fight() was called
        }
        [Test]
        public void Test5()
        {
            

            var mockWeapon = new Mock<IWeapon>();

            HumanFighter fighterToTest = new HumanFighter(mockWeapon.Object);
            fighterToTest.Fight();
            mockWeapon.Verify(x => x.Attack(), Times.Once());
            //verify that  mockWeapon.Fight() was called
        }
        [Test]
        public void Test6()
        {

            ContainerBuilder builder = new ContainerBuilder();
            var a = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
                .AsSelf()
                .AsImplementedInterfaces();


            builder.RegisterType<Bow>().Named<IWeapon>("bow");
            builder.RegisterDecorator<IWeapon>((c) => new WeaponWithJewelDecorator(c), "bow", "jewelDecorator");
            builder.RegisterDecorator<IWeapon>((c) => new WeaponWithSteelDecorator(c), "jewelDecorator");
            var container = builder.Build();
            
            var humanFighter1 = container.Resolve<HumanFighter>();
            var humanFighter2 = container.Resolve<HumanFighter>();
            string s = humanFighter1.Fight();
            Assert.That(s.ToLower().Contains("jewel"));

            var mockWeapon = new Mock<IWeapon>();

            HumanFighter fighterToTest = new HumanFighter(mockWeapon.Object);
            fighterToTest.Fight();
            mockWeapon.Verify(x => x.Attack(), Times.Once());
        }
        [Test]
        public void Test7()
        {

            ContainerBuilder builder = new ContainerBuilder();
            var a = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
                .AsSelf()
                .AsImplementedInterfaces();


            builder.RegisterType<Bow>().As<IWeapon>();
            builder.RegisterDecorator<IWeapon>((c) => new WeaponWithJewelDecorator(c), null);
            
            var container = builder.Build();

            var humanFighter1 = container.Resolve<HumanFighter>();
            var humanFighter2 = container.Resolve<HumanFighter>();
            string s = humanFighter1.Fight();
            Assert.That(s.ToLower().Contains("jewel"));

            var mockWeapon = new Mock<IWeapon>();

            HumanFighter fighterToTest = new HumanFighter(mockWeapon.Object);
            fighterToTest.Fight();
            mockWeapon.Verify(x => x.Attack(), Times.Once());
        }

        [Test]
        public void Test8()
        {

            ContainerBuilder builder = new ContainerBuilder();
            var a = Assembly.GetExecutingAssembly();

            builder.RegisterType<CreateNewNotificationCommandHandler>().Named < IDbCommandHandler<CreateNewNotificationCommand>>("implementor");
          //  builder.RegisterType<RecurringPaymentMarkAsFailureCommandHandler>().AsSelf().AsImplementedInterfaces().Named("implementor", typeof(IDbCommandHandler<>));
            

            // builder.RegisterAssemblyTypes(a).Where(t => t.IsAssignableFrom(typeof(IDbCommandHandler<>)))
            //     .Named("dbCommandHandler", typeof(IDbCommandHandler<>))
            //    .AsSelf()
            //    .AsImplementedInterfaces();
            
            //builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
            //    .AsSelf()
            //    .AsImplementedInterfaces();

            builder.RegisterDecorator<IDbCommandHandler<CreateNewNotificationCommand>>((c) => new DbCommandWithTransactionHandlerDecorator<CreateNewNotificationCommand>(c), "implementor");

            
            builder.RegisterGenericDecorator(typeof(BusinessLogic_v3.Classes.Decorators.DbCommandWithTransactionHandlerDecorator<>),
                                            typeof(BusinessLogic_v3.Classes.Interfaces.DbCommands.IDbCommandHandler<>),
                                            fromKey: "implementor2");
            //builder.RegisterGenericDecorator(typeof(BusinessLogic_v3.Classes.Decorators.DbCommandWithTransactionHandlerDecorator<>),
            //                                typeof(BusinessLogic_v3.Classes.Interfaces.DbCommands.IDbCommandHandler<>),
            //                                fromKey: "implementor2");

            
            var container = builder.Build();
            var handler = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand>>();
            handler.Handle(null);
        }
        [Test]
        public void Test9()
        {

            ContainerBuilder builder = new ContainerBuilder();
            var a = Assembly.GetExecutingAssembly();

            //builder.RegisterType<CreateNewNotificationCommandHandler>().Named<IDbCommandHandler<CreateNewNotificationCommand>>("implementor");

            builder.RegisterType<CreateNewNotificationCommandHandler>().Named("implementor", typeof(IDbCommandHandler<CreateNewNotificationCommand>));
            //  builder.RegisterType<RecurringPaymentMarkAsFailureCommandHandler>().AsSelf().AsImplementedInterfaces().Named("implementor", typeof(IDbCommandHandler<>));


            // builder.RegisterAssemblyTypes(a).Where(t => t.IsAssignableFrom(typeof(IDbCommandHandler<>)))
            //     .Named("dbCommandHandler", typeof(IDbCommandHandler<>))
            //    .AsSelf()
            //    .AsImplementedInterfaces();

            //builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
            //    .AsSelf()
            //    .AsImplementedInterfaces();

            

            builder.RegisterGenericDecorator(typeof(DbCommandWithTransactionHandlerDecorator<>),
                                            typeof(IDbCommandHandler<>),
                                            fromKey: "implementor");
            //builder.RegisterGenericDecorator(typeof(BusinessLogic_v3.Classes.Decorators.DbCommandWithTransactionHandlerDecorator<>),
            //                                typeof(BusinessLogic_v3.Classes.Interfaces.DbCommands.IDbCommandHandler<>),
            //                                fromKey: "implementor2");


            var container = builder.Build();
            var handler = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand>>();
            handler.Handle(null);
        }
        //[Test]
        //public void Test10()
        //{

        //    ContainerBuilder builder = new ContainerBuilder();
        //    var a = Assembly.GetExecutingAssembly();

        //    builder.RegisterType<CreateNewNotificationCommandHandler>().Named<IDbCommandHandler<CreateNewNotificationCommand>>("command");
        //    builder.RegisterType<CreateNewNotificationCommandHandler_2>().Named<IDbCommandHandler<CreateNewNotificationCommand_2>>("command");


        //    //  builder.RegisterType<RecurringPaymentMarkAsFailureCommandHandler>().AsSelf().AsImplementedInterfaces().Named("implementor", typeof(IDbCommandHandler<>));


        //    // builder.RegisterAssemblyTypes(a).Where(t => t.IsAssignableFrom(typeof(IDbCommandHandler<>)))
        //    //     .Named("dbCommandHandler", typeof(IDbCommandHandler<>))
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();

        //    //builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();


            
        //    builder.RegisterGenericDecorator(typeof(DbCommandWithTransactionHandlerDecorator<>),
        //                                    typeof(IDbCommandHandler<>),
        //                                    fromKey: "command");
        //    //builder.RegisterGenericDecorator(typeof(BusinessLogic_v3.Classes.Decorators.DbCommandWithTransactionHandlerDecorator<>),
        //    //                                typeof(BusinessLogic_v3.Classes.Interfaces.DbCommands.IDbCommandHandler<>),
        //    //                                fromKey: "implementor2");


        //    var container = builder.Build();
        //    var handler1 = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand>>();
        //    var handler2 = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand_2>>();
        //    handler1.Handle(null);
        //    handler2.Handle(null);
        //}

        private List<Type> getTypesThatImplementIDbCommandHandler(IEnumerable<Assembly> assemblyList)
        {
            List<Type> list = new List<Type>();
            //foreach (var a in assemblyList)
            //{
            //    var matches = a.GetTypes().Where(t => typeof(IDbCommandHandlerStub).IsAssignableFrom(t));
            //    list.AddRange(matches);
            //}
            return list;
        }


        private void registerDbCommands(List<Type> dbCommandHandlerTypes, ContainerBuilder builder)
        {
            foreach (var t in dbCommandHandlerTypes)
            {
                var interfaces = t.GetInterfaces();
                foreach (var i in interfaces)
                {
                    builder.RegisterType(t).Named("dbCommand", i);
                }

            }
        }

        //[Test]
        //public void Test11()
        //{

        //    ContainerBuilder builder = new ContainerBuilder();
        //    var a = Assembly.GetExecutingAssembly();

            

        //    var assemblies = CS.General_v3.Classes.Application.AppInstance.Instance.GetProjectAssemblies().ToList();
        //    assemblies.Add(a);
        //    var dbCommandHandlerTypes = getTypesThatImplementIDbCommandHandler(assemblies);
        //    registerDbCommands(dbCommandHandlerTypes, builder);
         


        //    //  builder.RegisterType<RecurringPaymentMarkAsFailureCommandHandler>().AsSelf().AsImplementedInterfaces().Named("implementor", typeof(IDbCommandHandler<>));


        //    // builder.RegisterAssemblyTypes(a).Where(t => t.IsAssignableFrom(typeof(IDbCommandHandler<>)))
        //    //     .Named("dbCommandHandler", typeof(IDbCommandHandler<>))
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();

        //    //builder.RegisterAssemblyTypes(a).Where(t => t.GetCustomAttributes(typeof(IocComponentAttribute), true).Any())
        //    //    .AsSelf()
        //    //    .AsImplementedInterfaces();



        //    builder.RegisterGenericDecorator(typeof(DbCommandWithTransactionHandlerDecorator<>),
        //                                    typeof(IDbCommandHandler<>),
        //                                    fromKey: "dbCommand", toKey:"dbCommandWithTransaction").SingleInstance();

        //    builder.RegisterGenericDecorator(typeof(DbOptimisticConcurrencyRetryDecorator<>),
        //                                    typeof(IDbCommandHandler<>),
        //                                    fromKey: "dbCommandWithTransaction").SingleInstance();
        //    //builder.RegisterGenericDecorator(typeof(BusinessLogic_v3.Classes.Decorators.DbCommandWithTransactionHandlerDecorator<>),
        //    //                                typeof(BusinessLogic_v3.Classes.Interfaces.DbCommands.IDbCommandHandler<>),
        //    //                                fromKey: "implementor2");


        //    var container = builder.Build();
        //    var handler1 = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand>>();
        //    var handler2 = container.Resolve<IDbCommandHandler<CreateNewNotificationCommand_2>>();
        //    handler1.Handle(null);
        //    handler2.Handle(null);
        //}
    }
}
