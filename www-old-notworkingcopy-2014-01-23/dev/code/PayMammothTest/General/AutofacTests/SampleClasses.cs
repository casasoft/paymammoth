﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;

namespace PayMammothTest.General.AutofacTests.SampleClasses
{
  

   
  
    public class Sword : IWeapon
    {
        public string Attack()
        {
            return "Slit with a sword";
        }
    }

    [IocComponent]
    public class Bow : IWeapon
    {

        #region IWeapon Members

        public string Attack()
        {
            return "Shooted with a bow";
        }

        #endregion
    }

    public interface IWeapon 
    {
        string Attack();
        
    }

    public class WeaponWithJewelDecorator : IWeapon
    {
        private IWeapon _weapon = null;
        public WeaponWithJewelDecorator(IWeapon weaponToDecorate)
        {
            this._weapon = weaponToDecorate;
        }


        #region IWeapon Members

        public string Attack()
        {
            string s = this._weapon.Attack() + ", adorned with jewels";
            return s;
        }

        #endregion
    }

    public class WeaponWithSteelDecorator : IWeapon
    {
        private IWeapon _weapon = null;
        public WeaponWithSteelDecorator(IWeapon weaponToDecorate)
        {
            this._weapon = weaponToDecorate;
        }


        #region IWeapon Members

        public string Attack()
        {
            string s = this._weapon.Attack() + " made of solid-hard steel";
            return s;
        }

        #endregion
    }

    [IocComponent(Singleton = false)]
    public class HumanFighter 

    {
        private readonly IWeapon weapon = null;
        public HumanFighter(IWeapon weapon)
        {
            this.weapon = weapon;
        }

        public int FightCount { get; set; }

        public string Fight()
        {
            FightCount++;
            //retu//rn null;
            return this.weapon.Attack();
        }

    }
}
