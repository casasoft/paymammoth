﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using NUnit.Framework;

namespace PayMammothTest.UtilTests
{
    public class GeneralUtilTests : BaseTestFixtureMySQL
    {
        [Test]
        public void AddMessageToStringLogTest()
        {
            string result = PayMammoth.Util.GeneralUtil.AddMessageToStringLog("karl", CS.General_v3.Enums.LOG4NET_MSG_TYPE.Warn, "Hello there");
            Assert.That(result.Contains("karl"));
            Assert.That(result.Contains("-----------"));
            Assert.That(result.Contains("Hello there"));
            Assert.That(result.Contains("[Warn"));

            

            
        }
          [Test]
        public void AddIntervalToDateTimeTest()
        {
          
              DateTime relativeDate = new DateTime(2012,10,9, 15,00,00);

              var result = PayMammoth.Util.GeneralUtil.AddIntervalToDateTime(relativeDate, PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD.Month, 1);
              Assert.That(result == new DateTime(2012,11,9,15,00,00));

              result = PayMammoth.Util.GeneralUtil.AddIntervalToDateTime(relativeDate, PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD.Month, 3);
              Assert.That(result == new DateTime(2013,1,9,15,00,00));

              result = PayMammoth.Util.GeneralUtil.AddIntervalToDateTime(relativeDate, PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD.Year, 1);
              Assert.That(result == new DateTime(2013, 10, 9, 15, 00, 00));

              result = PayMammoth.Util.GeneralUtil.AddIntervalToDateTime(relativeDate, PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD.Day, 7);
              Assert.That(result == new DateTime(2012, 17, 9, 15, 00, 00));


        }

    }
}
