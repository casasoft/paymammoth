﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Classes.PaymentNotifications.NotificationConfirmations.Confirmers;
using PayMammoth.Connector.Classes.Notifications;
using PayMammoth.Modules.RecurringProfileModule;

namespace PayMammothTest.Classes.PaymentNotifications.NotificationConfirmations.Confirmers
{
    public class CancelRecurringProfileNotificationProcessorTests : BaseTestFixtureMySQL
    {
        [Test]
        public void test_that_generate_response_does_not_cancel_profile_if_not_active()
        {
            var mockProfile = new Mock<IRecurringProfile>();
            var mockRecurringProfileService = new Mock<IRecurringProfileService>();
            var mockFactory = new Mock<IRecurringProfileFactory>();
            var mockMsg = new Mock<INotificationMsg>();
            mockMsg.SetupAllProperties();
            mockProfile.SetupAllProperties();

            mockFactory.Setup(x => x.GetRecurringProfileByPayMammothIdentifier(It.IsAny<string>())).Returns(mockProfile.Object);

            CancelRecurringProfileNotificationProcessor processor = new CancelRecurringProfileNotificationProcessor(mockRecurringProfileService.Object,
                mockFactory.Object);

            {
                mockProfile.Object.Status = PayMammoth.Enums.RecurringProfileStatus.Expired;
                mockMsg.Object.NotificationType = PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE.CancelRecurringProfile;

                var response = processor.GenerateResponse(mockMsg.Object);

                mockRecurringProfileService.Verify(x => x.MarkProfileAsCancelled(It.IsAny<IRecurringProfile>(), It.IsAny<string>()), Times.Never());
                Assert.That(response.ConfirmationSuccess == false);
            }
            {
                mockProfile.Object.Status = PayMammoth.Enums.RecurringProfileStatus.CouldNotCreate;
                mockMsg.Object.NotificationType = PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE.CancelRecurringProfile;

                var response = processor.GenerateResponse(mockMsg.Object);

                mockRecurringProfileService.Verify(x => x.MarkProfileAsCancelled(It.IsAny<IRecurringProfile>(), It.IsAny<string>()), Times.Never());
                Assert.That(response.ConfirmationSuccess == false);
            }



        }
        [Test]
        public void test_that_generate_response_cancels_profile_if_active()
        {
            var mockProfile = new Mock<IRecurringProfile>();
            var mockRecurringProfileService = new Mock<IRecurringProfileService>();
            var mockFactory = new Mock<IRecurringProfileFactory>();
            var mockMsg = new Mock<INotificationMsg>();
            mockMsg.SetupAllProperties();
            mockProfile.SetupAllProperties();

            mockFactory.Setup(x => x.GetRecurringProfileByPayMammothIdentifier(It.IsAny<string>())).Returns(mockProfile.Object);

            CancelRecurringProfileNotificationProcessor processor = new CancelRecurringProfileNotificationProcessor(mockRecurringProfileService.Object,
                mockFactory.Object);

            {
                mockProfile.Object.Status = PayMammoth.Enums.RecurringProfileStatus.Active;
                mockMsg.Object.NotificationType = PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE.CancelRecurringProfile;

                var response = processor.GenerateResponse(mockMsg.Object);
                mockRecurringProfileService.Verify(x => x.MarkProfileAsCancelled(mockProfile.Object, It.IsAny<string>()), Times.Once());
               
                Assert.That(response.ConfirmationSuccess == true);
            }
            
        }
    }
}
