﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Classes.PaymentNotifications.NotificationConfirmations.Confirmers;
using PayMammoth.Connector.Classes.Notifications;
using PayMammoth.Connector.Classes.Responses;

namespace PayMammothTest.Classes.PaymentNotifications.NotificationConfirmations.Confirmers
{
    public class RecurringProfileUpdateConfirmationResponseSenderTests : BaseTestFixtureMySQL
    {

        /*\
generates a confirmation response, based on a notification message.

------------
If msg is not one of suspended, expired, cancelled or could not created, it should return a null response

if it is, it will try to get a profile with the given identifier.  If it does not exist, it must return null

If it exists, it then verifies that the profile is actually of the same status, similar to the message.

If it is, it will return a RecurringProfileUpdateConfirmationResponse with details about the profile.

==========
TO TEST
==========

create a sample notification msg with nothing, should return NULL

create a sample notification msg with notification type of immediatepayment, should return NULL

create a sample notification msg, with a profile that does not exist - should return NULL

create a msg with profile that exists but is not of the same status - should return null

create a msg with profile that exists and is of same status - should return a rspones.  Confirm that response.status and profile identifier match


         * */

        [Test]
        public void test_that_null_msg_should_return_null()
        {
            var mockMsg = new Mock<INotificationMsg>();
            RecurringProfileUpdateConfirmationResponseSender sender = new RecurringProfileUpdateConfirmationResponseSender();
            var result= sender.GenerateResponse(mockMsg.Object);
            Assert.That(result == null);
        }
        [Test]
        public void test_that_msg_with_type_of_immediate_payment_should_return_null()
        {
            var mockMsg = new Mock<INotificationMsg>();
            mockMsg.SetupAllProperties();
            mockMsg.Object.NotificationType = PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE.ImmediatePayment;
                
            RecurringProfileUpdateConfirmationResponseSender sender = new RecurringProfileUpdateConfirmationResponseSender();
            var result = sender.GenerateResponse(mockMsg.Object);
            Assert.That(result==null);
        }

        [Test]
        public void test_that_msg_with_correct_type_but_nonexistant_profile_should_return_null()
        {
            var mockMsg = new Mock<INotificationMsg>();
            mockMsg.SetupAllProperties();
            mockMsg.Object.NotificationType = PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCancelled;
            mockMsg.Object.Identifier = "non-existant";

            RecurringProfileUpdateConfirmationResponseSender sender = new RecurringProfileUpdateConfirmationResponseSender();
            var result = sender.GenerateResponse(mockMsg.Object);
            Assert.That(result.ConfirmationSuccess== false);
        }

        [Test]
        public void test_that_msg_with_existing_profile_but_not_matching_status_should_return_null()
        {
            string identifier = "test_profile";
            var profile = Util.DbUtil.CreateNewRecurringProfile(identifier: identifier, status: PayMammoth.Enums.RecurringProfileStatus.Active);


            
            var mockMsg = new Mock<INotificationMsg>();
            mockMsg.SetupAllProperties();
            mockMsg.Object.NotificationType = PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCancelled;
            mockMsg.Object.Identifier = identifier;

            disposeCurrentAndCreateNewSession();

            RecurringProfileUpdateConfirmationResponseSender sender = new RecurringProfileUpdateConfirmationResponseSender();
            var result = sender.GenerateResponse(mockMsg.Object);
            Assert.That(result.ConfirmationSuccess == false);
        }

        [Test]
        public void test_that_msg_with_existing_profile_and_matching_status_should_return_correct_response()
        {
            string identifier = "test_profile";
            var profile = Util.DbUtil.CreateNewRecurringProfile(identifier: identifier, status: PayMammoth.Enums.RecurringProfileStatus.Cancelled);



            var mockMsg = new Mock<INotificationMsg>();
            mockMsg.SetupAllProperties();
            mockMsg.Object.NotificationType = PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCancelled;
            mockMsg.Object.Identifier = identifier;

            disposeCurrentAndCreateNewSession();

            RecurringProfileUpdateConfirmationResponseSender sender = new RecurringProfileUpdateConfirmationResponseSender();
            RecurringProfileUpdateConfirmationResponse  result = (RecurringProfileUpdateConfirmationResponse )sender.GenerateResponse(mockMsg.Object);
            Assert.That(result != null);
            Assert.That(result.ProfileIdentifier == identifier);
            
        }
    }
}
