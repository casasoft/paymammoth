﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using NUnit.Framework;
using PayMammoth.Classes.PaymentNotifications.NotificationConfirmations;

namespace PayMammothTest.Classes.PaymentNotifications.NotificationConfirmations
{
    public class NotificationTypeConfirmationResponseSenderManagerTests : BaseTestFixtureMySQL
    {
        [Test]
        public void test_all_enum_values_return_an_instance()
        {
            var enumValues = CS.General_v3.Util.EnumUtils.GetListOfEnumValues<PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE>();
            foreach (var enumValue in enumValues)
            {
                var result= NotificationTypeConfirmationResponseSenderManager.GetConfirmerForNotificationType(enumValue);
                Assert.That(result != null);
            }

        }

    }
}
