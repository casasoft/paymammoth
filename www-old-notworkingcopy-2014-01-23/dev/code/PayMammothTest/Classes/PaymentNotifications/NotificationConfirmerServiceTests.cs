﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Classes.PaymentNotifications;
using PayMammoth.Connector.Classes.Notifications;
using PayMammoth.Connector.Util;
using PayMammoth.Modules.WebsiteAccountModule;

namespace PayMammothTest.Classes.PaymentNotifications
{
    public class NotificationConfirmerServiceTests: BaseTestFixtureMySQL
    {
        [Test]
        public void test_functionality()
        {
            string secretWord = "test_secret";
            string accountCode = "test_account_unit_test";
            var mockWebsiteFactory = new Mock<IWebsiteAccountFactory>();
            var mockWebsiteAccount = new Mock<IWebsiteAccount>();
            mockWebsiteAccount.SetupAllProperties();
                
            mockWebsiteAccount.Object.SecretWord = secretWord;
            mockWebsiteAccount.Object.Code = accountCode;
            mockWebsiteFactory.Setup(x => x.GetAccountByCode(accountCode)).Returns(mockWebsiteAccount.Object);



            NotificationConfirmerService service = new NotificationConfirmerService(mockWebsiteFactory.Object);
            {
                var mockNotificationMsg = new Mock<INotificationMsg>();
                bool result = service.ConfirmMessage(mockNotificationMsg.Object);
                Assert.That(result == false);
            }
            {
                var mockNotificationMsg = new Mock<INotificationMsg>();
                mockNotificationMsg.SetupAllProperties();
                
                mockNotificationMsg.Object.ConfirmationHash = "something";
                bool result = service.ConfirmMessage(mockNotificationMsg.Object);
                Assert.That(result == false);
            }
            {
                var mockNotificationMsg = new Mock<INotificationMsg>();
                mockNotificationMsg.SetupAllProperties();
                
                mockNotificationMsg.Object.Identifier = "123456";
                mockNotificationMsg.Object.WebsiteAccountCode = "test";
                mockNotificationMsg.Object.ConfirmationHash = HashUtil.GenerateConfirmHash(mockNotificationMsg.Object.Identifier, secretWord);
                bool result = service.ConfirmMessage(mockNotificationMsg.Object);
                Assert.That(result == false);
            }
            {
                var mockNotificationMsg = new Mock<INotificationMsg>();
                mockNotificationMsg.SetupAllProperties();
                
                
                mockNotificationMsg.Object.Identifier = "123456";
                mockNotificationMsg.Object.WebsiteAccountCode = accountCode;
                mockNotificationMsg.Object.ConfirmationHash = HashUtil.GenerateConfirmHash(mockNotificationMsg.Object.Identifier, secretWord);
                bool result = service.ConfirmMessage(mockNotificationMsg.Object);
                Assert.That(result == true);
            }

        }
    }
}
