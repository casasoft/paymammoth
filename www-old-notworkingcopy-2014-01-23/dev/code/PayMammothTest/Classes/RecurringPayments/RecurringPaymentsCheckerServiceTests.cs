﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Classes.RecurringPayments;
using PayMammoth.Modules.RecurringProfileModule.Services;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammothTest.Classes.RecurringPayments
{
    public class RecurringPaymentsCheckerServiceTests : BaseTestFixtureMySQL
    {
        [Test]
        public void test_correct_functionality()
        {
            /*
create a mock factory which returns 2 payments for GetRecurringPaymentsRequiringNextPaymentByPayMammoth
and a different 2 payments for GetRecurringPaymentsWithExternalPaymentsWhichHaveNotSucceeded

create a mock RecurringProfileMarkAsFailureService

verify that recurringPaymentChargePaymentByPayMammothService.ChargePayment(was called for first 2 payments)

verify that RecurringProfileMarkAsFailureService was called for each payment

*/
            var mockRequiringPayment1 = new Mock<IRecurringProfilePayment>();
            var mockRequiringPayment2 = new Mock<IRecurringProfilePayment>();

            var mockFailurePayment1 = new Mock<IRecurringProfilePayment>();
            var mockFailurePayment2 = new Mock<IRecurringProfilePayment>();

            var mockRecurringPaymentFactory = new Mock<IRecurringProfilePaymentFactory>();
            mockRecurringPaymentFactory.Setup(x => x.GetRecurringPaymentsRequiringNextPaymentByPayMammoth()).Returns(new List<IRecurringProfilePayment>() { mockRequiringPayment1.Object, mockRequiringPayment2.Object });
            mockRecurringPaymentFactory.Setup(x => x.GetRecurringPaymentsWithExternalPaymentsWhichHaveNotSucceeded()).Returns(new List<IRecurringProfilePayment>() { mockFailurePayment1.Object, mockFailurePayment2.Object });

            var mockChargePaymentByPayMammothService = new Mock<IRecurringPaymentChargePaymentByPayMammothService>();
            var mockMarkAsFailureService= new Mock<IRecurringProfilePaymentMarkAsFailureService>();
            RecurringPaymentsCheckerService service = new RecurringPaymentsCheckerService(mockRecurringPaymentFactory.Object,mockChargePaymentByPayMammothService.Object, mockMarkAsFailureService.Object);

            service.CheckPayments();

            mockChargePaymentByPayMammothService.Verify(x => x.ChargePayment(mockRequiringPayment1.Object), Times.Once());
            mockChargePaymentByPayMammothService.Verify(x => x.ChargePayment(mockRequiringPayment2.Object), Times.Once());
            mockChargePaymentByPayMammothService.Verify(x => x.ChargePayment(mockFailurePayment1.Object), Times.Never());
            mockChargePaymentByPayMammothService.Verify(x => x.ChargePayment(mockFailurePayment2.Object), Times.Never());

            mockMarkAsFailureService.Verify(x => x.MarkPaymentAsFailure(mockFailurePayment1.Object, It.IsAny<string>(), It.IsAny<string>(),  false), Times.Once());
            mockMarkAsFailureService.Verify(x => x.MarkPaymentAsFailure(mockFailurePayment2.Object, It.IsAny<string>(), It.IsAny<string>(),  false), Times.Once());
            mockMarkAsFailureService.Verify(x => x.MarkPaymentAsFailure(mockRequiringPayment1.Object, It.IsAny<string>(), It.IsAny<string>(),  false), Times.Never());
            mockMarkAsFailureService.Verify(x => x.MarkPaymentAsFailure(mockRequiringPayment2.Object, It.IsAny<string>(), It.IsAny<string>(),  false), Times.Never());
            



  

        }
    }
}
