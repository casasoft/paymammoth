﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Classes.RecurringPayments;
using PayMammoth.Classes.RecurringPayments.Helpers;
using PayMammoth.Classes.RecurringPayments.RecurringPaymentChargers;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfileModule.Services;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammothTest.Classes.RecurringPayments
{
    public class RecurringPaymentChargePaymentByPayMammothServiceTests : BaseTestFixtureMySQL
    {
        [Test]
        public void test_that_on_payment_failed_mark_as_failed_is_called()
        {
            var payment= Util.DbUtil.CreateNewRecurringProfilePayment();
            
            
            var mockRecurringPaymentCharger = new Mock<IRecurringPaymentCharger>();
             RecurringPaymentChargerResponse mockResponse = new RecurringPaymentChargerResponse()
             {
                 Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Failure,
                  PaymentDetails="TestPaymentDetails",
                  PaymentReference="TestFailureTransaction" 

             };

             var mockMarkPaymentAsSuccessful = new Mock<IRecurringProfilePaymentMarkLastPaymentAsSuccessfulService>();
            var mockIRecurringProfilePaymentMarkAsFailureService = new Mock<IRecurringProfilePaymentMarkAsFailureService>();
            mockRecurringPaymentCharger.Setup(x=>x.ChargePayment(It.IsAny<IRecurringProfilePayment>())).Returns(mockResponse);

            RecurringPaymentChargePaymentByPayMammothService service = new RecurringPaymentChargePaymentByPayMammothService(
                mockMarkPaymentAsSuccessful.Object, mockIRecurringProfilePaymentMarkAsFailureService.Object);
            

            service.ChargePayment(payment);
            
            
            mockRecurringPaymentCharger.Verify(x => x.ChargePayment(payment));
            mockIRecurringProfilePaymentMarkAsFailureService.Verify(x => x.MarkPaymentAsFailure(payment, mockResponse.PaymentReference, mockResponse.PaymentDetails, false));
            mockMarkPaymentAsSuccessful.Verify(x => x.MarkAsSuccessful(payment, It.IsAny<string>(), It.IsAny<string>(), false), Times.Never());
            


        }
        [Test]
        public void test_that_on_payment_success_mark_as_success_is_called()
        {
            var payment = Util.DbUtil.CreateNewRecurringProfilePayment();
            


            var mockRecurringPaymentCharger = new Mock<IRecurringPaymentCharger>();
            RecurringPaymentChargerResponse mockResponse = new RecurringPaymentChargerResponse()
            {
                Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Success,
                PaymentDetails = "TestPaymentDetails",
                PaymentReference = "TestSuccessTransaction"

            };

            var mockMarkPaymentAsSuccessful = new Mock<IRecurringProfilePaymentMarkLastPaymentAsSuccessfulService>();
            var mockIRecurringProfilePaymentMarkAsFailureService = new Mock<IRecurringProfilePaymentMarkAsFailureService>();
            mockRecurringPaymentCharger.Setup(x => x.ChargePayment(It.IsAny<IRecurringProfilePayment>())).Returns(mockResponse);

            RecurringPaymentChargePaymentByPayMammothService service = new RecurringPaymentChargePaymentByPayMammothService(
                mockMarkPaymentAsSuccessful.Object, mockIRecurringProfilePaymentMarkAsFailureService.Object);
            
            service.ChargePayment(payment);


            mockRecurringPaymentCharger.Verify(x => x.ChargePayment(payment));

            mockIRecurringProfilePaymentMarkAsFailureService.Verify(x => x.MarkPaymentAsFailure(payment, It.IsAny<string>(), It.IsAny<string>(),  false), Times.Never());
            mockMarkPaymentAsSuccessful.Verify(x => x.MarkAsSuccessful(payment, mockResponse.PaymentReference, mockResponse.PaymentDetails,  false), Times.Once());
            

            


        }
    }
}
