﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using NUnit.Framework;
using PayMammoth.Classes.Helpers;
using PayMammoth.Classes.RecurringPayments;
using PayMammoth.Classes.RecurringPayments.Impl;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.PaymentMethods.PayPal.v1.Interfaces;

namespace PayMammothTest.Classes.RecurringPayments.Impl
{

    public class RecurringPaymentCheckerImplTests : CS.TestGeneral_v3.BaseTestFixtureMySQL
    {
        [Test]
        public void CreateRecurringProfileIfRequiredTest()
        {

            //
			

            var mockPayPalRecurringProfileManager = new Mock<IPayPalRecurringProfileManager>();
            BusinessLogic_v3.Util.InversionOfControlUtil.Register(mockPayPalRecurringProfileManager.Object);
            var mockProfileCreationFailedNotificationsender = new Mock<IRecurringProfileCreationFailedNotificationSender>();
            RecurringPaymentProfileRequiredCheckerImpl checker = new RecurringPaymentProfileRequiredCheckerImpl(mockProfileCreationFailedNotificationsender.Object);


            var paymentRequest = Util.DbUtil.CreateNewRequest();
            paymentRequest.RecurringProfileRequired = false;
            paymentRequest.RecurringProfile_IntervalFrequency = 5;
            paymentRequest.RecurringProfile_IntervalType = PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD.Month;
            paymentRequest.RecurringProfile_MaxFailedAttempts = 3;
            paymentRequest.RecurringProfile_TotalBillingCycles = 10;


            var transaction = paymentRequest.StartNewTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout);
            var profile = checker.CreateRecurringProfileIfRequired(transaction);
            Assert.That(profile == null);
            mockPayPalRecurringProfileManager.Verify(x => x.SetupRecurringProfile(profile), Times.Never());

            transaction.PaymentRequest.RecurringProfileRequired = true;
            profile = checker.CreateRecurringProfileIfRequired(transaction);
            mockPayPalRecurringProfileManager.Verify(x => x.SetupRecurringProfile(profile), Times.Exactly(1));

            Assert.That(profile.RecurringIntervalFrequency == paymentRequest.RecurringProfile_IntervalFrequency);
            Assert.That(profile.RecurringIntervalType == paymentRequest.RecurringProfile_IntervalType);
            Assert.That(profile.MaximumFailedAttempts == paymentRequest.RecurringProfile_MaxFailedAttempts);
            Assert.That(profile.TotalBillingCyclesRequired == paymentRequest.RecurringProfile_TotalBillingCycles);
            

            



                
                //var t = Util.DbUtil.CreateTestPaymentRequestTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout);



        }
        [Test]
        public void CreateRecurringProfileOnErrorNotificationIsSent()
        {
            var mockPayPalRecurringProfileManager = new Mock<IPayPalRecurringProfileManager>();
            mockPayPalRecurringProfileManager.Setup(x => x.SetupRecurringProfile(It.IsAny<IRecurringProfile>())).Returns(new PayMammoth.Classes.Helpers.SetupRecurringProfileResult() { Status = PayMammoth.Connector.Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE.InternalError });

            BusinessLogic_v3.Util.InversionOfControlUtil.Register(mockPayPalRecurringProfileManager.Object);
            var mockProfileCreationFailedNotificationsender = new Mock<IRecurringProfileCreationFailedNotificationSender>();
            RecurringPaymentProfileRequiredCheckerImpl checker = new RecurringPaymentProfileRequiredCheckerImpl(mockProfileCreationFailedNotificationsender.Object);


            var paymentRequest = Util.DbUtil.CreateNewRequest();
            paymentRequest.RecurringProfileRequired = true;
            paymentRequest.RecurringProfile_IntervalFrequency = 5;
            paymentRequest.RecurringProfile_IntervalType = PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD.Month;
            paymentRequest.RecurringProfile_MaxFailedAttempts = 3;
            paymentRequest.RecurringProfile_TotalBillingCycles = 10;
            var transaction = paymentRequest.StartNewTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout);
            
            var profile = checker.CreateRecurringProfileIfRequired(transaction);
            mockPayPalRecurringProfileManager.Verify(x => x.SetupRecurringProfile(profile), Times.Exactly(1));
            Assert.That(profile.Status == PayMammoth.Enums.RecurringProfileStatus.Pending);
            mockProfileCreationFailedNotificationsender.Verify(x => x.SendNotification(profile, It.IsAny<SetupRecurringProfileResult>()), Times.Once());


        }

        public PayMammoth.Classes.RecurringPayments.IRecurringProfileCreationFailedNotificationSender IRecurringProfileCreationFailedNotificationSender { get; set; }
    }
}
