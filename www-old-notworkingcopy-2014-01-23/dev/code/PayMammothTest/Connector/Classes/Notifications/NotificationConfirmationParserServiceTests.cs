﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Connector.Classes.Notifications;
using PayMammoth.Connector.Classes.Responses;

namespace PayMammothTest.Connector.Classes.Notifications
{
    public class NotificationConfirmationParserServiceTests : BaseTestFixtureMySQL
    {
        [Test]
        public void test_that_for_all_enum_values_an_null_ref_exception_is_thrown()
        {
            //this test checks all values, and it should throw an exception 
            //if not, it means the enum is not handled

            var enumValues = CS.General_v3.Util.EnumUtils.GetListOfEnumValues<PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE>();
            NotificationConfirmationParserService service = new NotificationConfirmationParserService();
            
            foreach (var enumVal in enumValues)
            {
                var mockNotification = new Mock<INotificationMsg>();
                mockNotification.SetupAllProperties();
                mockNotification.Object.NotificationType = enumVal;

                var mockResponse = new Mock<IConfirmationResponse>();
                mockResponse.SetupAllProperties();
                mockResponse.Object.ResponseType = enumVal;

                Assert.Throws<InvalidCastException>(()=>service.ParseConfirmationResponse(mockNotification.Object, mockResponse.Object));

            }

        }
    }
}
