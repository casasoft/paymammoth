﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using NUnit.Framework;
using PayMammoth.Connector.Classes.Responses;

namespace PayMammothTest.Connector.Classes.Responses
{
    public class BaseConfirmResponseTests : BaseTestFixtureMySQL
    {
        [Test]
        public void test_functionality()
        {
            NameValueCollection nv = new NameValueCollection();
            string propertyName = CS.General_v3.Util.ReflectionUtil<BaseConfirmResponse>.GetPropertyName(x => x.ResponseType);

            var result = BaseConfirmResponse.CreateFromResponse(nv);
            Assert.That(result == null);

            nv[propertyName] = "ABC";
            result = BaseConfirmResponse.CreateFromResponse(nv);
            Assert.That(result == null);

            {
                nv[propertyName] = "ImmediatePayment";
                nv["ConfirmationSuccess"] = "true";
                result = BaseConfirmResponse.CreateFromResponse(nv);
                Assert.That(result != null);
                Assert.That(result is ImmediatePaymentConfirmationResponse);
                ImmediatePaymentConfirmationResponse castedResult = (ImmediatePaymentConfirmationResponse)result;
                Assert.That(castedResult.ConfirmationSuccess);

            }


        }
        [Test]
        public void test_that_all_enum_values_are_ok()
        {
            NameValueCollection nv = new NameValueCollection();
            string propertyName = CS.General_v3.Util.ReflectionUtil<BaseConfirmResponse>.GetPropertyName(x => x.ResponseType);
            var enumValues = CS.General_v3.Util.EnumUtils.GetListOfEnumValues<PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE>();
            foreach (var enumValue in enumValues)
            {
                nv[propertyName] = CS.General_v3.Util.EnumUtils.StringValueOf(enumValue);
                var response = BaseConfirmResponse.CreateFromResponse(nv);
                Assert.That(response != null);
            }

        }

    }
}
