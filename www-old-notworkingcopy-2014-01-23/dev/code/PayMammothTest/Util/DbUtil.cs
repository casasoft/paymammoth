﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Transactions;
using PayMammoth.Classes.Other;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.WebsiteAccountModule;

namespace PayMammothTest.Util
{
    public static class DbUtil
    {

        public static WebsiteAccount GetAnyWebsiteAccount()
        {
            return WebsiteAccount.Factory.FindAll().FirstOrDefault();
        }

        private static MyTransaction beginTransaction()
        {
            return BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext();
        }
        private static MyTransaction beginTransactionBasedOnAutoSave(bool autoSave)
        {
            return BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionBasedOnAutoSaveBool(autoSave);
        }
        public static WebsiteAccount CreateWebsiteAccount(string websiteName = "Test Website", 
            string allowedIPs = null, string code = "-TEST-", int iterationsToDeriveHash = 4096, string responseUrl = "http://www.casasoft.com.mt/payment/test",
            string secretWord = "TestTestTest", bool allowFakePayments = false, bool autoSave = true)
        {
            using (var t = beginTransactionBasedOnAutoSave(autoSave))
            {
                WebsiteAccount item = WebsiteAccount.Factory.CreateNewItem();
                item.AllowedIPs = allowedIPs;
                item.EnableFakePayments = allowFakePayments;
                item.Code = code;
                item.IterationsToDeriveHash = iterationsToDeriveHash;
                item.ResponseUrl = responseUrl;
                item.SecretWord = secretWord;
                item.WebsiteName = websiteName;
                if (autoSave)
                {
                    item.Create();
                    t.Commit();
                }
                return item;
            }
            
        }

        public static PaymentRequest CreateNewRequest(WebsiteAccount account = null, string address1 = "Test Address 1", string address2 = "Test address 3",
      string address3 = null, CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 country = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Malta,
          string email = "email.test@casasoft.com.mt", string firstName = "Test First Name", string middleName = null,
          string lastName = "Test Last Name", string locality = "Test Zurrieq", string postcode = "TST11234",
          CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 currency = CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro,
          double handlingAmount = 5, double priceExcTax = 10,
          string reference = null, double shippingAmount = 2, double taxAmount = 1, string title = "Testing",
            string clientIp = "192.168.1.1"
          )
        {
            if (account == null)
            {
                account = CreateWebsiteAccount(code: CS.General_v3.Util.Random.GetAlpha(5,10));
            }
            InitialRequestInfo info = new InitialRequestInfo();
            info.ClientIp = clientIp;
            info.ClientAddress1 = address1;
            info.ClientAddress2 = address2;
            info.ClientAddress3 = address3;
            info.ClientCountry = country;
            info.ClientEmail = email;
            info.ClientFirstName = firstName;
            info.ClientMiddleName = middleName;
            info.ClientLastName = lastName;
            info.ClientLocality = locality;
            info.ClientPostCode = postcode;
            info.CurrencyCode = currency;
            info.HandlingAmount = 5;
            info.PriceExcTax = 10;
            if (reference == null) reference = CS.General_v3.Util.Date.Now.ToString("yyyyMMddHHmmss");
            info.Reference = reference;
            info.ShippingAmount = shippingAmount;
            info.TaxAmount = taxAmount;
            info.Title = title;
            info.WebsiteAccountCode = account.Code;

            info.HashValue = PayMammoth.Connector.Util.HashUtil.GenerateInitialRequestHash(info, account.SecretWord);
            
            return account.GenerateNewRequest(info);
        }

        public static RecurringProfile CreateNewRecurringProfile(
            string identifier = null,
            int maxFailedAttempts = 3,
            PayMammoth.Connector.Enums.PaymentMethodSpecific paymentMethod = PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout,
            IPaymentRequestTransaction transaction = null,
            string profileIdentifierOnGateway = null,
            PayMammoth.Enums.RecurringProfileStatus status = PayMammoth.Enums.RecurringProfileStatus.Pending,
            bool autoSave = true

          )

        {
            
            if (identifier == null) identifier = CS.General_v3.Util.Random.GetGUID();
            if (profileIdentifierOnGateway == null) profileIdentifierOnGateway = CS.General_v3.Util.Random.GetGUID();
            if (transaction == null) transaction = CreateTestPaymentRequestTransaction(paymentMethod);
            var websiteAccount = CreateWebsiteAccount(code: CS.General_v3.Util.Random.GetAlpha(16,16));
            using (var t = beginTransaction())
            {
                RecurringProfile item = RecurringProfile.Factory.CreateNewItem();
                item.Identifier = identifier;
                item.MaximumFailedAttempts = maxFailedAttempts;
                item.PaymentGateway = paymentMethod;
                item.WebsiteAccount = websiteAccount;
                transaction.PaymentRequest.RecurringProfileLink = item;
                
                item.ProfileIdentifierOnGateway = profileIdentifierOnGateway;
                item.Status = status;
                item.CreatedOn = CS.General_v3.Util.Date.Now;
                if (autoSave)
                {
                    item.Create();
                    transaction.PaymentRequest.Save();
                    t.Commit();

                }
                return item;
            }
            
        }
        public static RecurringProfilePayment CreateNewRecurringProfilePayment(

          int failureCount = 0,
            DateTime? paymentDueOn = null,
            IRecurringProfile recurringProfile = null,
            DateTime? nextRetryOn = null,
            PayMammoth.Connector.Enums.RecurringProfilePaymentStatus status = PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Pending,
            bool initiatedByPayMammoth = true,
            DateTime? markAsPaymentFailedIfNotPaidBy =null,
            string identifier = null,
          bool autoSave = true

        )
        {
            if (recurringProfile == null) recurringProfile = CreateNewRecurringProfile();
            
            using (var t = beginTransaction())
            {
                RecurringProfilePayment item = RecurringProfilePayment.Factory.CreateNewItem();
                item.FailureCount = failureCount;
                item.Identifier = identifier;
                item.MarkAsPaymentFailedIfNotPaidBy = markAsPaymentFailedIfNotPaidBy;
                item.InitiatedByPayMammoth = initiatedByPayMammoth;
                item.PaymentDueOn = paymentDueOn.GetValueOrDefault(CS.General_v3.Util.Date.Now); ;
                item.RecurringProfile = (RecurringProfile) recurringProfile;
                item.NextRetryOn = nextRetryOn;
                item.Status =  status;
                
                if (autoSave)
                {
                    item.Create();
                    t.Commit();
                }
                return item;
            }

        }

        public static PaymentRequestTransaction CreateTestPaymentRequestTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific method = PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout)
        {
            var request = CreateNewRequest();
            return request.StartNewTransaction(method);


        }
    }
}
