﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;

namespace PayMammothTest.Util.PaymentMethods
{
    public static class PayPalUtil
    {
        public static PayPalClient GetGenericPayPalClient(IWebsiteAccount websiteAccount = null)
        {
            if (websiteAccount==null) websiteAccount = Util.DbUtil.GetAnyWebsiteAccount();

            IPayPalSettings settings = websiteAccount.GetPaypalSettings();
            return new PayPalClient(settings);
        }
    }
}
