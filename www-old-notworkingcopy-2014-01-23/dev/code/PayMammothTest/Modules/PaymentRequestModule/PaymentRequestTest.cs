﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using PayMammoth.Classes.Exceptions;
using PayMammoth.Classes.RecurringPayments;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules.WebsiteAccountModule;

namespace PayMammothTest.Modules.PaymentRequestModule
{
    [TestFixture]
    public class PaymentRequestTest :CS.TestGeneral_v3.BaseTestFixtureMySQL
    {
        [Test]
        public void MakeFakePaymentTest()
        {
            WebsiteAccount w = null;
            using (var t = beginTransaction())
            {

                w = Util.DbUtil.CreateWebsiteAccount(allowFakePayments: false);
                t.Commit();
            }
            var req = Util.DbUtil.CreateNewRequest(w);
            try
            {
                req.MakeFakePayment(PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout, null);
                Assert.Fail("An error should have been thrown");
            }
            catch (FakePaymentNotAllowedException ex)
            {

            }
            w.EnableFakePayments = true;
            var fakeTransaction = req.MakeFakePayment(PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout, null);

            var successfulTransaction = req.GetSuccessfulTransaction();
            Assert.That(successfulTransaction.ID, Is.EqualTo(fakeTransaction.ID));
            Assert.That(successfulTransaction, Is.Not.Null);
            Assert.That(successfulTransaction.Successful, Is.True);
            Assert.That(successfulTransaction.FakePayment, Is.True);

            


        }

        [Test]
        public void OnPaymentRequestSuccessfulAndRequiresRecurringProfile_ProfileIsCreatedTest()
        {
            var mockedRecurringPaymentChecker = new Moq.Mock<IRecurringPaymentProfileRequiredChecker>();
            var transaction = Util.DbUtil.CreateTestPaymentRequestTransaction();
            BusinessLogic_v3.Util.InversionOfControlUtil.Register(mockedRecurringPaymentChecker.Object);

            transaction.MarkAsSuccessful(true);

            mockedRecurringPaymentChecker.Verify(x => x.CreateRecurringProfileIfRequired(transaction), Moq.Times.Exactly(1)); 



        }

        [Test]
        public void EmailSendTest()
        {
            var w = Util.DbUtil.CreateWebsiteAccount(allowFakePayments: false);
            w.EnableFakePayments = true;
            w.NotifyClientsByEmailAboutPayment = true;
            w.NotificationEmail = "info@karlcassar.com, karlcassar@gmail.com, email.test1@casasoft.com.mt";
                
            {
                var req = Util.DbUtil.CreateNewRequest(w);
                req.ClientEmail = "karl@casasoft.com.mt";
                req.MakeFakePayment(PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout, null);
            }
            {
                var req = Util.DbUtil.CreateNewRequest(w);
                req.ClientEmail = "karl@casasoft.com.mt";
                req.MakeFakePayment(PayMammoth.Connector.Enums.PaymentMethodSpecific.Cheque, null);
            }
            {
                var req = Util.DbUtil.CreateNewRequest(w);
                req.ClientEmail = "karl@casasoft.com.mt";
                req.MakeFakePayment(PayMammoth.Connector.Enums.PaymentMethodSpecific.BankTransfer, null);
            }
            System.Threading.Thread.Sleep(5000);

        }
    }
}
