﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Exceptions;
using NUnit.Framework;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammothTest.Util;
using PayMammoth.Classes.Other;

namespace PayMammothTest.Modules.WebsiteAccountModule
{
    [TestFixture]
    public class WebsiteAccountTest : CS.TestGeneral_v3.BaseTestFixtureMySQL
    {
        [Test]
        public void GetAccountByCodeTest()
        {
            var account1 = DbUtil.CreateWebsiteAccount(code: "test1", websiteName: "TestAccount1");
            var account2 = DbUtil.CreateWebsiteAccount(code: "test2", websiteName: "TestAccount2");
            var account3 = DbUtil.CreateWebsiteAccount(code: "test3", websiteName: "TestAccount3");

            disposeCurrentAndCreateNewSession();

            var test = WebsiteAccountFactory.Instance.GetAccountByCode("test2");
            Assert.That(test.WebsiteName, Is.EqualTo("TestAccount2"));
            createNewSession();
        }
        [Test]
        public void NoDuplicateCodesAllowedTest()
        {
            var account1 = DbUtil.CreateWebsiteAccount(code: "test1", websiteName: "TestAccount1");
            try
            {
                var account2 = DbUtil.CreateWebsiteAccount(code: "test1", websiteName: "TestAccount2");
                Assert.Fail("Duplicate codes should not be allowed");
            }
            catch (DuplicateCodeException ex)
            {

            }

        }

        

        /// <summary>
        /// Checks whether an account allows a request form a certain IP
        /// </summary>
        [Test]
        public void CheckWhetherRequestFromIpAllowedTest()
        {



            var account1 = DbUtil.CreateWebsiteAccount(code: "test1", websiteName: "TestAccount1", allowedIPs:"192.168.1.1,192.168.2.*,   192.168.1.1?1  ,   250.*.*.*" );
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("192.168.1.1"), Is.True);
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("192.168.1.55"), Is.False);
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("192.168.2.0"), Is.True);
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("192.168.2.255"), Is.True);
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("192.168.2.251"), Is.True);
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("192.168.22.1"), Is.False);
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("192.168.1.101"), Is.True);
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("192.168.1.121"), Is.True);
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("192.168.1.131"), Is.True);
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("250.5.5.5"), Is.True);
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("250.5.5"), Is.False);
            Assert.That(account1.CheckWhetherRequestFromIpAllowed("200.148.156.56"), Is.False);


        }
        [Test]
        public void GenerateNewRequestTest()
        {
            int iterations = 4096;
            string secretWord = "TestSecret";
            var account = DbUtil.CreateWebsiteAccount(iterationsToDeriveHash: iterations, secretWord: secretWord);
            InitialRequestInfo initRequest = new InitialRequestInfo();
            initRequest.WebsiteAccountCode = account.Code;

            initRequest.HashValue = PayMammoth.Connector.Util.HashUtil.GenerateInitialRequestHash(initRequest, secretWord);
            var req = initRequest.GeneratePaymentRequest();
            Assert.That(req, Is.Not.Null);



        }


    }
}
