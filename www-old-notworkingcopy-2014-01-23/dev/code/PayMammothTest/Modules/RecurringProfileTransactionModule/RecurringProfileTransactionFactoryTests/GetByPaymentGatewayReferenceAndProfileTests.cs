﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;
using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammothTest.Modules.RecurringProfileTransactionModule.RecurringProfileTransactionFactoryTests
{
    public class GetByPaymentGatewayReferenceAndProfileTests : BaseTestFixtureMySQL
    {
        private IRecurringProfileTransaction createTransaction(string paymentRef, IRecurringProfile profile)
        {
            using (var t = beginTransaction())
            {
                IRecurringProfilePayment payment = RecurringProfilePaymentFactory.Instance.CreateNewItem();
                payment.RecurringProfile = profile;
                payment.Save();
                IRecurringProfileTransaction transaction = payment.Transactions.CreateNewItem();
                transaction.PaymentGatewayReference = paymentRef;

                transaction.Save();
                t.Commit();
                return transaction;
            }

        }

        private IRecurringProfile profileA;
        private IRecurringProfile profileB;
        private IRecurringProfileTransaction transaction1;
        private IRecurringProfileTransaction transaction2;
        private IRecurringProfileTransaction transaction3;
        private IRecurringProfileTransaction transaction4;
        private void createData()
        {
            profileA = Util.DbUtil.CreateNewRecurringProfile(identifier: "A" );
            profileB = Util.DbUtil.CreateNewRecurringProfile(identifier: "B");
            transaction1 = createTransaction("A", profileA);
            transaction2 = createTransaction("B", profileA);
            transaction3 = createTransaction("A", profileB);
            transaction4 = createTransaction(null, null);

        }

        [Test]
        public void test_that_returns_the_correct_transaction()
        {
            createData();

            var result = RecurringProfileTransactionFactory.Instance.GetByPaymentGatewayReferenceAndProfile("A", profileB);
            Assert.That(result == transaction3);

            result = RecurringProfileTransactionFactory.Instance.GetByPaymentGatewayReferenceAndProfile("A", profileA);
            Assert.That(result == transaction1);



        }

    }
}
