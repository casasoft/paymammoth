﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammothTest.Modules.RecurringProfilePaymentModule
{
    public class RecurringProfilePaymentServiceTests : BaseTestFixtureMySQL
    {
        [Test]
        public void test_that_GetSuccessfulTransaction_returns_successful_transaction()
        {
            {
                var testPayment = RecurringProfilePaymentFactory.Instance.CreateNewItem();

                RecurringProfilePaymentService service = new RecurringProfilePaymentService();
                var result = service.GetSuccessfulTransaction(testPayment);
                Assert.That(result == null);
            }
            {
                var testPayment = RecurringProfilePaymentFactory.Instance.CreateNewItem();

                var testTransaction = RecurringProfileTransactionFactory.Instance.CreateNewItem();
                testPayment.Transactions.Add(testTransaction);
                testTransaction.Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Failure;
                
                RecurringProfilePaymentService service = new RecurringProfilePaymentService();
                var result = service.GetSuccessfulTransaction(testPayment);
                Assert.That(result == null);
            }

            {

                var testPayment = RecurringProfilePaymentFactory.Instance.CreateNewItem();
                
                var testTransaction = RecurringProfileTransactionFactory.Instance.CreateNewItem();
                testPayment.Transactions.Add(testTransaction);
                testTransaction.Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Success;
                
                RecurringProfilePaymentService service = new RecurringProfilePaymentService();
                var result = service.GetSuccessfulTransaction(testPayment);
                Assert.That(result == testTransaction);
            }
            {

                var testPayment = RecurringProfilePaymentFactory.Instance.CreateNewItem();
                
                    var testTransaction1 = RecurringProfileTransactionFactory.Instance.CreateNewItem();
                    testPayment.Transactions.Add(testTransaction1);
                    testTransaction1.Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Success;
                
                
                    var testTransaction2 = RecurringProfileTransactionFactory.Instance.CreateNewItem();
                    testPayment.Transactions.Add(testTransaction2);
                    testTransaction2.Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Failure;
                

                RecurringProfilePaymentService service = new RecurringProfilePaymentService();
                var result = service.GetSuccessfulTransaction(testPayment);
                Assert.That(result == testTransaction1);
            }
        }

        [Test]
        public void test_that_GetLastFailedTransaction_returns_the_last_failed_transaction()
        {

            {
                var testPayment = RecurringProfilePaymentFactory.Instance.CreateNewItem();

                RecurringProfilePaymentService service = new RecurringProfilePaymentService();
                var result = service.GetLastFailedTransaction(testPayment);
                Assert.That(result == null);
            }
            {
                var testPayment = RecurringProfilePaymentFactory.Instance.CreateNewItem();

                var testTransaction = RecurringProfileTransactionFactory.Instance.CreateNewItem();
                testPayment.Transactions.Add(testTransaction);
                testTransaction.Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Failure;

                RecurringProfilePaymentService service = new RecurringProfilePaymentService();
                var result = service.GetLastFailedTransaction(testPayment);
                Assert.That(result == testTransaction);
            }
            {
                var testPayment = RecurringProfilePaymentFactory.Instance.CreateNewItem();

                var testTransaction = RecurringProfileTransactionFactory.Instance.CreateNewItem();
                testPayment.Transactions.Add(testTransaction);
                testTransaction.Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Success;

                RecurringProfilePaymentService service = new RecurringProfilePaymentService();
                var result = service.GetLastFailedTransaction(testPayment);
                Assert.That(result == null);
            }
            {
                var testPayment = RecurringProfilePaymentFactory.Instance.CreateNewItem();

                var testTransaction1 = RecurringProfileTransactionFactory.Instance.CreateNewItem();
                testPayment.Transactions.Add(testTransaction1);
                testTransaction1.Timestamp = new DateTime(2012, 12, 1);
                testTransaction1.Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Failure;
                
                var testTransaction2 = RecurringProfileTransactionFactory.Instance.CreateNewItem();
                testPayment.Transactions.Add(testTransaction2);
                testTransaction2.Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Success;
                
                var testTransaction3 = RecurringProfileTransactionFactory.Instance.CreateNewItem();
                testPayment.Transactions.Add(testTransaction3);
                testTransaction3.Timestamp = new DateTime(2012, 12, 5);
                testTransaction3.Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Failure;
                
                var testTransaction4 = RecurringProfileTransactionFactory.Instance.CreateNewItem();
                testPayment.Transactions.Add(testTransaction4);
                testTransaction4.Timestamp = new DateTime(2012, 12, 2);
                testTransaction4.Status = PayMammoth.Enums.RecurringProfileTransactionStatus.Failure;

                RecurringProfilePaymentService service = new RecurringProfilePaymentService();
                var result = service.GetLastFailedTransaction(testPayment);
                Assert.That(result == testTransaction3);
            }

        }
    }
}
