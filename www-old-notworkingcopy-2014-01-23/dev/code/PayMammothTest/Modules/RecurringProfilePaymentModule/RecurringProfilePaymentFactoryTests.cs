﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using NUnit.Framework;
using PayMammoth.Modules.RecurringProfilePaymentModule;

namespace PayMammothTest.Modules.RecurringProfilePaymentModule
{
    public class RecurringProfilePaymentFactoryTests : BaseTestFixtureMySQL
    {


        [Test]
        public void test_that_GetRecurringPaymentsRequiringNextPaymentByPayMammoth_returns_only_pending_and_initiated_by_PayMammoth_and_next_retry_on_elapsed()
        {
            /*
Test
Date = 12Nov2012
PaymentA – Success, NextRetryOn 12Nov2012, initiated by PayMammoth
PaymentB – Pending, NextRetryOn 12Nov2012, initiated byPM
PaymentC – Pending, NextRetryOn 14Nov2012, initiated byPM
PaymentD – Pending, NextRetryOn 10Nov2012, initiated byPM
PaymentE – Pending, NextRetryOn 10Nov2012, NOT initiated byPM


GetRecurringPaymentsRequiringNextPaymentByPayMammoth() -> B,D.
*/             
            CS.General_v3.Util.Date.CustomDate = new DateTime(2012, 11, 12);
            var paymentA = Util.DbUtil.CreateNewRecurringProfilePayment(status: PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Success, nextRetryOn: new DateTime(2012, 11, 12), initiatedByPayMammoth: true);
            var paymentB = Util.DbUtil.CreateNewRecurringProfilePayment(status: PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Pending, nextRetryOn: new DateTime(2012, 11, 12), initiatedByPayMammoth: true);
            var paymentC = Util.DbUtil.CreateNewRecurringProfilePayment(status: PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Pending, nextRetryOn: new DateTime(2012, 11, 14), initiatedByPayMammoth: true);
            var paymentD = Util.DbUtil.CreateNewRecurringProfilePayment(status: PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Pending, nextRetryOn: new DateTime(2012, 11, 10), initiatedByPayMammoth: true);
            var paymentE = Util.DbUtil.CreateNewRecurringProfilePayment(status: PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Pending, nextRetryOn: new DateTime(2012, 11, 10), initiatedByPayMammoth: false);

            var list = RecurringProfilePaymentFactory.Instance.GetRecurringPaymentsRequiringNextPaymentByPayMammoth();
            CS.TestGeneral_v3.Util.GeneralUtil.AssertThatListContainsAllAndExactCount(list, paymentB, paymentD);

        }
        [Test]
        public void test_that_GetRecurringPaymentsWithExternalPaymentsWhichHaveNotSucceeded_returns_correct_data()
        {


            /*
today: 12Nov 2012
PaymentA - Success, InitiatedByPm: false, MarkAsPaymentFailedIfNotPaidBy: 12 Nov 2012 N
PaymentB - Pending, InitiatedByPm: false, MarkAsPaymentFailedIfNotPaidBy: 12 Nov 2012 Y
PaymentC - Pending, InitiatedByPm: false, MarkAsPaymentFailedIfNotPaidBy: 10 Nov 2012 Y
PaymentD - Pending, InitiatedByPm: true, MarkAsPaymentFailedIfNotPaidBy: 10 Nov 2012  N
PaymentE - Pending, InitiatedByPm: false, MarkAsPaymentFailedIfNotPaidBy: 14 Nov 2012 N
PaymentF - Failed, InitiatedByPm: false, MarkAsPaymentFailedIfNotPaidBy: 12 Nov 2012  N
             * 
*/
            CS.General_v3.Util.Date.CustomDate = new DateTime(2012, 11, 12);
            var paymentA = Util.DbUtil.CreateNewRecurringProfilePayment(status: PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Success, markAsPaymentFailedIfNotPaidBy: new DateTime(2012, 11, 12), initiatedByPayMammoth: false);
            var paymentB = Util.DbUtil.CreateNewRecurringProfilePayment(status: PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Pending, markAsPaymentFailedIfNotPaidBy: new DateTime(2012, 11, 12), initiatedByPayMammoth: false);
            var paymentC = Util.DbUtil.CreateNewRecurringProfilePayment(status: PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Pending, markAsPaymentFailedIfNotPaidBy: new DateTime(2012, 11, 10), initiatedByPayMammoth: false);
            var paymentD = Util.DbUtil.CreateNewRecurringProfilePayment(status: PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Pending, markAsPaymentFailedIfNotPaidBy: new DateTime(2012, 11, 10), initiatedByPayMammoth: true);
            var paymentE = Util.DbUtil.CreateNewRecurringProfilePayment(status: PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Pending, markAsPaymentFailedIfNotPaidBy: new DateTime(2012, 11, 14), initiatedByPayMammoth: false);
            var paymentF = Util.DbUtil.CreateNewRecurringProfilePayment(status: PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Failed, markAsPaymentFailedIfNotPaidBy: new DateTime(2012, 11, 12), initiatedByPayMammoth: false);

            var list = RecurringProfilePaymentFactory.Instance.GetRecurringPaymentsWithExternalPaymentsWhichHaveNotSucceeded();
            CS.TestGeneral_v3.Util.GeneralUtil.AssertThatListContainsAllAndExactCount(list, paymentB, paymentC);

        }
        [Test]
        public void test_that_GetPaymentByIdentifier_returns_by_identifier()
        {
            CS.General_v3.Util.Date.CustomDate = new DateTime(2012, 11, 12);
            var paymentA = Util.DbUtil.CreateNewRecurringProfilePayment(identifier: "paymentA");
            var paymentB = Util.DbUtil.CreateNewRecurringProfilePayment(identifier: "paymentB");
            var paymentC = Util.DbUtil.CreateNewRecurringProfilePayment(identifier: "paymentC");
            var paymentD = Util.DbUtil.CreateNewRecurringProfilePayment(identifier: "");
            disposeCurrentAndCreateNewSession();
            
            var result = RecurringProfilePaymentFactory.Instance.GetPaymentByIdentifier("paymentB");
            Assert.That(object.Equals(result, paymentB));
            result = RecurringProfilePaymentFactory.Instance.GetPaymentByIdentifier("paymentB31232");
            Assert.That(object.Equals(result, null));

            
        }
    }
}
