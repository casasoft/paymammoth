﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Modules.RecurringProfileModule;

using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;
using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;

namespace PayMammothTest.Modules.RecurringProfileModule.Commands
{
    public class RecurringPaymentMarkAsSuccessfulCommandHandlerTest : BaseTestFixtureMySQL
    {
        [Test]
        public void TestCommand2()
        {
            var mockNotificationSender = new Mock<IMarkAsSuccessfulNotificationSender>();

            var commandHandler = new RecurringPaymentMarkAsSuccessfulCommandHandler();
            var command = new RecurringPaymentMarkAsSuccessfulCommand();

            var payment = Util.DbUtil.CreateNewRecurringProfilePayment();
            var profile = payment.RecurringProfile;
            var lastPayment = profile.GetLastPaymentDone();
            commandHandler.Handle(command);
            Assert.That(lastPayment.Status == PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Success);
            Assert.That(lastPayment.Transactions.Count() == 1);
            var transaction = lastPayment.Transactions.ToList()[0];
            Assert.That(transaction.Status == PayMammoth.Enums.RecurringProfileTransactionStatus.Success);

            var newPayment = profile.GetLastPaymentDone();
            Assert.That(!object.Equals(newPayment, lastPayment));
            var updatedDate = PayMammoth.Util.GeneralUtil.AddIntervalToDateTime(lastPayment.PaymentDueOn, profile.RecurringIntervalType,profile.RecurringIntervalFrequency);
            Assert.That(newPayment.PaymentDueOn == updatedDate);
            mockNotificationSender.Verify(x => x.SendNotification(lastPayment), Times.Once());



        }
        [Test]
        public void TestCommand()
        {
            
            var commandHandler = new RecurringPaymentMarkAsSuccessfulCommandHandler();
            var command = new RecurringPaymentMarkAsSuccessfulCommand();
            var payment = Util.DbUtil.CreateNewRecurringProfilePayment();
            command.Payment = payment;
            command.PaymentDetails = "payment details";
            using (var t = beginTransaction())
            {

                commandHandler.Handle(command);
                t.Commit();
            }
            var transaction = command.Output_Transaction;
            Assert.That(payment.Status == PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Success);
            Assert.That(transaction != null);
            Assert.That(transaction.StatusLog.Contains("payment details"));

            Assert.That(payment.Transactions.Count == 1);
            Assert.That(payment.Transactions.Contains(transaction));

                ;
            

        }
    }
}
