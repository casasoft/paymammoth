﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammothTest.Modules.RecurringProfileModule
{
    public class RecurringPaymentMarkAsFailure
    {
        private readonly IPaymentFailedNotificationSender paymentFailedNotificationSender;
        private readonly IRecurringProfileFailureNextNotificationDateUpdater failureNotificationDateUpdater;
        private readonly IRecurringProfileSuspender recurringProfileSuspender;

        public RecurringPaymentMarkAsFailure(IPaymentFailedNotificationSender paymentFailedNotificationSender, IRecurringProfileSuspender recurringProfileSuspender,
            IRecurringProfileFailureNextNotificationDateUpdater failureNotificationDateUpdater)
        {
            this.paymentFailedNotificationSender = paymentFailedNotificationSender;
            this.failureNotificationDateUpdater = failureNotificationDateUpdater;
            
            this.recurringProfileSuspender = recurringProfileSuspender;

        }

        private void checkProfileStatus(IRecurringProfile profile)
        {
            if (profile.Status != PayMammoth.Enums.RecurringProfileStatus.Active)
            {
                throw new PayMammoth.Classes.Exceptions.RecurringProfileException("This cannot be called when the profile is not marked as active");
            }
        }


        private void incrementFailureCount(IRecurringProfilePayment payment)
        {
            payment.FailureCount++;
        }

        public IRecurringProfileTransaction MarkPaymentAsFailed(IRecurringProfilePayment payment, string failureData)
        {
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                checkProfileStatus(payment.RecurringProfile);


                IRecurringProfileTransaction transaction = payment.Transactions.CreateNewItem();
                transaction.OtherData = PayMammoth.Util.GeneralUtil.AddMessageToStringLog(transaction.OtherData, CS.General_v3.Enums.LOG4NET_MSG_TYPE.Info, failureData);
                transaction.Status = Enums.RecurringProfileTransactionStatus.Failure;
                paymentFailedNotificationSender.CreateAndQueueNotification(transaction);
                failureNotificationDateUpdater.UpdateNextFailureNotificationDate(payment);
                //    setNextFailureNotificationMsgOn(payment);
                incrementFailureCount(payment);

                if (payment.FailureCount >= payment.RecurringProfile.MaximumFailedAttempts)
                {
                    recurringProfileSuspender.SuspendRecurringProfile(payment.RecurringProfile);
                }
                transaction.Save();
                t.Commit();
                return transaction;
            }

        }

    }
}
