﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using CS.TestGeneral_v3;
using NUnit.Framework;
using PayMammoth.Modules.RecurringProfileModule.Commands;

namespace PayMammothTest.Modules.RecurringProfileModule.Commands
{
    public class MarkProfileAsCancelledCommandTests : BaseTestFixtureMySQL
    {
        [Test]
        public void test_that_on_command_handle_profile_is_marked_as_cancelled()
        {
            var profile = Util.DbUtil.CreateNewRecurringProfile();
            Assert.That(profile.Status != PayMammoth.Enums.RecurringProfileStatus.Cancelled);
            MarkProfileAsCancelledCommand cmd = new MarkProfileAsCancelledCommand(profile);
            cmd.CancellationDetails = "TestCancellationDetails";
            var handler = BusinessLogic_v3.Util.InversionOfControlUtil.Get<IDbCommandHandler<MarkProfileAsCancelledCommand>>();
            handler.Handle(cmd);
            Assert.That(profile.Status == PayMammoth.Enums.RecurringProfileStatus.Cancelled);
            Assert.That(profile.StatusLog.Contains("TestCancellationDetails"));

        }

    }
}
