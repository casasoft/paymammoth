﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using CS.TestGeneral_v3;
using NUnit.Framework;
using PayMammoth.Modules.RecurringProfileModule.Commands;

namespace PayMammothTest.Modules.RecurringProfileModule.Commands
{
    public class MarkProfileAsExpiredCommandHandlerTests : BaseTestFixtureMySQL
    {
        [Test]
        public void Test()
        {
            var profile = Util.DbUtil.CreateNewRecurringProfile();

            Assert.That(profile.Status != PayMammoth.Enums.RecurringProfileStatus.Expired);
            
            MarkProfileAsExpiredCommand cmd = new MarkProfileAsExpiredCommand(profile);

            var cmdHandler = BusinessLogic_v3.Util.InversionOfControlUtil.Get<IDbCommandHandler<MarkProfileAsExpiredCommand>>();
            cmdHandler.Handle(cmd);
            Assert.That(profile.Status == PayMammoth.Enums.RecurringProfileStatus.Expired);
                
        }
    }
}
