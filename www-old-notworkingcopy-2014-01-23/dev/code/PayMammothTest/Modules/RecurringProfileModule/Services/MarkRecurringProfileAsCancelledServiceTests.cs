﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.RecurringProfileModule.Commands;
using PayMammoth.Modules.RecurringProfileModule.Services;

namespace PayMammothTest.Modules.RecurringProfileModule.Services
{
    public class MarkRecurringProfileAsCancelledServiceTests : BaseTestFixtureMySQL
    {
        [Test]
        public void test_that_on_service_profile_is_cancelled_and_notification_is_sent()
        {

            var profile = Util.DbUtil.CreateNewRecurringProfile();
            var mockedNotificationCreatorAndSender = new Mock<INotificationCreatorAndSender>();
            var mockedCommandHandler = new Mock<IDbCommandHandler<MarkProfileAsCancelledCommand>>();

            MarkRecurringProfileAsCancelledService serviceUnderTest = new MarkRecurringProfileAsCancelledService(mockedNotificationCreatorAndSender.Object,
                mockedCommandHandler.Object);
            serviceUnderTest.MarkProfileAsCancelled(profile, "TEST-CANCELLED");

            mockedNotificationCreatorAndSender.Verify(x => x.CreateAndQueueNotification(It.Is<CreateNewNotificationCommand>(cmd => cmd.WebsiteAccount == profile.WebsiteAccount &&
                cmd.Identifier == profile.Identifier && cmd.NotificationType == PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCancelled)), Times.Once());
            mockedCommandHandler.Verify(x => x.Handle(It.Is<MarkProfileAsCancelledCommand>(cmd => cmd.Profile == profile && cmd.CancellationDetails == "TEST-CANCELLED")), Times.Once());

        }
    }
}
