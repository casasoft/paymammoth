﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.RecurringProfileModule.Services;

namespace PayMammothTest.Modules.RecurringProfileModule.Services
{
    public class ProfileActivationNotificationSenderServiceTests : BaseTestFixtureMySQL
    {
        [Test]
        public void test_that_sends_actual_notification_with_correct_data()
        {
            var profile = Util.DbUtil.CreateNewRecurringProfile(identifier: "test_profile");

            var mockNotificationCreatorAndSender= new Mock<INotificationCreatorAndSender>();
            ProfileActivationNotificationSenderService service = new ProfileActivationNotificationSenderService(mockNotificationCreatorAndSender.Object);
            service.SendNotification(profile);
            mockNotificationCreatorAndSender.Verify(x => x.CreateAndQueueNotification(It.Is<CreateNewNotificationCommand>(cmd => cmd.Identifier == "test_profile" &&
                cmd.NotificationType == PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileActivated)), Times.Once());


        }
    }
}
