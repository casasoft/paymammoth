﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.RecurringProfileModule.Commands;
using PayMammoth.Modules.RecurringProfileModule.Services;

namespace PayMammothTest.Modules.RecurringProfileModule.Services
{
    public class MarkProfileAsExpiredServiceTests : BaseTestFixtureMySQL
    {
        [Test]
        public void MarkProfileAsExpiredTest()
        {
            var profile = Util.DbUtil.CreateNewRecurringProfile();
            var mockedNotificationCreatorAndSender = new Mock<INotificationCreatorAndSender>();
            var mockedMarkProfileAsExpiredCommandHandler = new Mock<IDbCommandHandler<MarkProfileAsExpiredCommand>>();

            MarkRecurringProfileAsExpiredService serviceUnderTest = new MarkRecurringProfileAsExpiredService(mockedNotificationCreatorAndSender.Object,
                mockedMarkProfileAsExpiredCommandHandler.Object);
            serviceUnderTest.MarkProfileAsExpired(profile, "TEST-EXPIRY");

            mockedNotificationCreatorAndSender.Verify(x => x.CreateAndQueueNotification(It.Is<CreateNewNotificationCommand>(cmd => cmd.WebsiteAccount == profile.WebsiteAccount &&
                cmd.Identifier == profile.Identifier && cmd.NotificationType == PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileExpired)), Times.Once());
            mockedMarkProfileAsExpiredCommandHandler.Verify(x=>x.Handle(It.Is<MarkProfileAsExpiredCommand>(cmd=>cmd.Profile == profile && cmd.ExpirationDetails == "TEST-EXPIRY")),Times.Once());

        }
    }
}
