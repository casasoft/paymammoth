﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using NUnit.Framework;
using PayMammoth.Modules.RecurringProfileModule;

namespace PayMammothTest.Modules.RecurringProfileModule
{
    public class RecurringProfileFactoryTests : BaseTestFixtureMySQL
    {
        [Test]
        public void test_that_GetRecurringProfileByPayMammothIdentifier_returns_by_identifier()
        {
            var profile1= Util.DbUtil.CreateNewRecurringProfile( identifier: "test1");
            var profile2 = Util.DbUtil.CreateNewRecurringProfile(identifier: "test2");
            var profile3 = Util.DbUtil.CreateNewRecurringProfile(identifier: "test3");
            var profile4 = Util.DbUtil.CreateNewRecurringProfile(identifier: null);

            disposeCurrentAndCreateNewSession();

            var result = RecurringProfileFactory.Instance.GetRecurringProfileByPayMammothIdentifier("test1");
            Assert.That(object.Equals(result ,profile1));

            result = RecurringProfileFactory.Instance.GetRecurringProfileByPayMammothIdentifier("test3");
            Assert.That(object.Equals(result, profile3));

            result = RecurringProfileFactory.Instance.GetRecurringProfileByPayMammothIdentifier("tefasdfsdfdst3");
            Assert.That(result == null);

        }
    }
}
