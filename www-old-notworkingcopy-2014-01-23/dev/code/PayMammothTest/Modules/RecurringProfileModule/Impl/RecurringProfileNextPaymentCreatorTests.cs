﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using NUnit.Framework;

namespace PayMammothTest.Modules.RecurringProfileModule.Impl
{

    public class RecurringProfileNextPaymentCreatorTests : BaseTestFixtureMySQL
    {
        [Test]
        public void CreateNextRecurringProfilePaymentTest()
        {
            var profile = Util.DbUtil.CreateNewRecurringProfile();
            profile.RecurringIntervalFrequency = 2;
            profile.RecurringIntervalType = PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD.Month;
            PayMammoth.Modules.RecurringProfileModule.Impl.RecurringProfileNextPaymentCreatorImpl nextPaymentCreator = new PayMammoth.Modules.RecurringProfileModule.Impl.RecurringProfileNextPaymentCreatorImpl();

            try
            {
                nextPaymentCreator.CreateNextRecurringProfilePayment(profile);

                Assert.Fail("An error should have been thrown");
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message.Contains("profile is not activated"))
                {
                    //ok
                }
                else
                {
                    Assert.Fail("An error should have been thrown");
                }
            }
            profile.Status = PayMammoth.Enums.RecurringProfileStatus.Active;
            profile.ActivatedOn = new DateTime(2012, 5, 1);
            

            var payment1 = nextPaymentCreator.CreateNextRecurringProfilePayment(profile);
            Assert.That(profile.Payments.Count == 1);

            Assert.That(payment1.PaymentDueOn == profile.ActivatedOn.Value.AddMonths(2));
            Assert.That(payment1.NextRetryOn == payment1.PaymentDueOn);
            Assert.That(payment1.MarkAsPaymentFailedIfNotPaidBy == profile.ActivatedOn.Value.AddMonths(4)); //twice
            Assert.That(payment1.InitiatedByPayMammoth == PayMammoth.Util.GeneralUtil.CheckIfRecurringPaymentIsInitiatedByPayMammothForPaymentMethod(profile.PaymentGateway));
            var payment2 = nextPaymentCreator.CreateNextRecurringProfilePayment(profile);

            Assert.That(payment2.PaymentDueOn == payment1.PaymentDueOn.AddMonths(2));
            Assert.That(payment2.MarkAsPaymentFailedIfNotPaidBy == payment1.PaymentDueOn.AddMonths(4)); //twice
            Assert.That(payment2.NextRetryOn == payment2.PaymentDueOn);

            Assert.That(profile.Payments.Count == 2);





        }
    }
}
