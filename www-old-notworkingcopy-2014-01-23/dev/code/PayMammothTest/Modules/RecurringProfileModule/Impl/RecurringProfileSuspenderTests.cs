﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfileModule.Impl;

namespace PayMammothTest.Modules.RecurringProfileModule.Impl
{
    public class RecurringProfileSuspenderTests : BaseTestFixtureMySQL
    {
        [Test]
        public void SuspendProfileTests()
        {
            var mockNotificationSender = new Mock<ISuspendedProfileNotificationSender>();

            var profile = Util.DbUtil.CreateNewRecurringProfile();
            RecurringProfileSuspender profileSuspender = new RecurringProfileSuspender(mockNotificationSender.Object);
            profileSuspender.SuspendRecurringProfile(profile,"Suspension Details");
            Assert.That(profile.Status == PayMammoth.Enums.RecurringProfileStatus.Suspended);
            Assert.That(profile.StatusLog.Contains("Suspension Details"));
            mockNotificationSender.Verify(x => x.CreateAndQueueNotification(profile), Times.Once());


        }
    }
}
