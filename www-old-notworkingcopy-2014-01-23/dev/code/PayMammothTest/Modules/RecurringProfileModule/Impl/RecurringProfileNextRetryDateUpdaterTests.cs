﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using NUnit.Framework;

namespace PayMammothTest.Modules.RecurringProfileModule.Impl
{
    public class RecurringProfileNextRetryDateUpdaterTests : BaseTestFixtureMySQL
    {
        [Test]
        public void UpdateNextRetryDateTest()
        {

            //setup the data
//create a NextRetryDateUpdater()
//create a fake payment, with failure count = 0
//create a list of intervals, say 8,16,24 hours
//set custom date to 2012/June/1
//Call NextRetryDateUpdater on created payment
//Assert that the next retry date is CUSTOM DATE + 8 hours

            var payment = Util.DbUtil.CreateNewRecurringProfilePayment();
            List<int> intervals = new List<int>() { 8, 14, 20 };
            CS.General_v3.Util.Date.CustomDate = new DateTime(2012, 6, 1);

            var updater = new PayMammoth.Modules.RecurringProfileModule.Impl.RecurringProfileNextRetryDateUpdater();
            updater.UpdateNextRetryDate(payment, intervals);
            Assert.That(payment.NextRetryOn == CS.General_v3.Util.Date.CustomDate.Value.AddHours(intervals[0]));

            DateTime lastDate = payment.NextRetryOn.Value;
//set failure count = 1
//Assert that the next retry date is LAST + 8 hours

            payment.FailureCount = 1;
            updater.UpdateNextRetryDate(payment, intervals);
            Assert.That(payment.NextRetryOn == lastDate.AddHours(intervals[0]));
            lastDate = payment.NextRetryOn.Value;
//set failure count = 2
//Assert that the next retry date is LAST + 16 hours

            payment.FailureCount = 2;
            updater.UpdateNextRetryDate(payment, intervals);
            Assert.That(payment.NextRetryOn == lastDate.AddHours(intervals[1]));
            lastDate = payment.NextRetryOn.Value;

//set failure count = 10 (this should revert to last)
//Assert that the next retry date is LAST + 24 hours

            payment.FailureCount = 10;
            updater.UpdateNextRetryDate(payment, intervals);
            Assert.That(payment.NextRetryOn == lastDate.AddHours(intervals[2]));





        }

    }
}
