﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Modules.NotificationMessageModule;
using PayMammoth.Modules.NotificationMessageModule.Commands;
using PayMammoth.Modules.RecurringProfileModule.Impl;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammothTest.Modules.RecurringProfileModule.Impl
{
    public class PaymentFailedNotificationSenderTests : BaseTestFixtureMySQL
    {
        [Test]
        public void CreateAndQueueNotificationTest()
        {
            string profileIdentifier = "1234";
            string notificationUrl = "http://test/";
            //var paymentTransaction = Util.DbUtil.CreateTestPaymentRequestTransaction();
            //var profile = Util.DbUtil.CreateNewRecurringProfile();
            //profile.PaymentTransaction = paymentTransaction;
            //profile.PaymentTransaction.PaymentRequest.WebsiteAccount.ResponseUrl = notificationUrl;

   
            var recurringProfilePayment = Util.DbUtil.CreateNewRecurringProfilePayment();
            recurringProfilePayment.RecurringProfile.RefreshFromDb();
            var request = recurringProfilePayment.RecurringProfile.GetPaymentRequest();
            request.WebsiteAccount.ResponseUrl = notificationUrl;

            IRecurringProfileTransaction transaction = PayMammoth.Modules.Factories.RecurringProfileTransactionFactory.CreateNewItem();
            transaction.RecurringProfilePayment = recurringProfilePayment;
            recurringProfilePayment.RecurringProfile.Identifier = profileIdentifier;

            var mockNotificationCreatorAndSender = new Mock<INotificationCreatorAndSender>();
            PaymentFailedNotificationSender sender = new PaymentFailedNotificationSender(mockNotificationCreatorAndSender.Object);

            transaction.RecurringProfilePayment.RecurringProfile.WebsiteAccount = request.WebsiteAccount;
            var msg = sender.CreateAndQueueNotification(transaction.RecurringProfilePayment, null);

            mockNotificationCreatorAndSender.Verify(x=>x.CreateAndQueueNotification(It.Is<CreateNewNotificationCommand>(
                cmd => cmd.NotificationType == PayMammoth.Connector.Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentFailure &&
                cmd.Identifier == recurringProfilePayment.Identifier)), Times.Once());
                
            


        }
        
    }
}
