﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Classes.Exceptions;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfileModule.Services;
using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;
using PayMammoth.Modules.RecurringProfileTransactionModule;

namespace PayMammothTest.Modules.RecurringProfileModule
{
    public class RecurringProfileTests : BaseTestFixtureMySQL
    {
        [Test]
        public void ActivateProfileTest()
        {
            DateTime customDate = new DateTime(2012, 5, 6, 12, 34, 56);
            CS.General_v3.Util.Date.CustomDate = customDate;

            var mockRecurringProfileNextPaymentCreator = new Mock<PayMammoth.Modules.RecurringProfileModule.IRecurringProfileNextPaymentCreator>();
            

            var profile = Util.DbUtil.CreateNewRecurringProfile();

            var mockProfileActivationNotificationSenderService = new Mock<IProfileActivationNotificationSenderService >();

            RecurringProfileService service = new RecurringProfileService(null, null, mockProfileActivationNotificationSenderService.Object,
                mockRecurringProfileNextPaymentCreator.Object,
                null,null);

            service.ActivateProfile(profile, "activate details");
            Assert.That(profile.Status == PayMammoth.Enums.RecurringProfileStatus.Active);
            Assert.That(profile.ActivatedOn == customDate);
            Assert.That(profile.StatusLog.Contains("activate details"));
            mockRecurringProfileNextPaymentCreator.Verify(x => x.CreateNextRecurringProfilePayment(It.Is<IRecurringProfile>(paramProfile => object.Equals(paramProfile, profile)), It.IsAny<bool>()), Times.Once());
            mockProfileActivationNotificationSenderService.Verify(x => x.SendNotification(profile), Times.Once());


        }

        private void addPaymentToProfile(IRecurringProfile profile, DateTime paymentDueOn)
        {
            var payment = profile.Payments.CreateNewItem();
            payment.PaymentDueOn = paymentDueOn;

        }

    

        [Test]
        public void GetLastPaymentDoneTests()
        {
            var profile = Util.DbUtil.CreateNewRecurringProfile();
            var payment = profile.GetLastPaymentDone();
            Assert.That(payment == null); 
            addPaymentToProfile(profile, new DateTime(2012, 7, 1));
            payment = profile.GetLastPaymentDone();
            Assert.That(payment.PaymentDueOn == new DateTime(2012, 7, 1));
            addPaymentToProfile(profile, new DateTime(2012, 5, 1));
            addPaymentToProfile(profile, new DateTime(2012, 7, 5));
            addPaymentToProfile(profile, new DateTime(2012, 2, 1));

            payment = profile.GetLastPaymentDone();
            Assert.That(payment.PaymentDueOn == new DateTime(2012, 7, 5));

            


        }

        [Test]
        public void HaveAllBillingCyclesBeenCompletedTest1()
        {
            {
                IRecurringProfile profile = Util.DbUtil.CreateNewRecurringProfile();
                profile.TotalBillingCyclesRequired = 0;


                Assert.That(RecurringProfileUtil.HaveAllBillingCyclesBeenCompleted(profile,0) == false);
                Assert.That(RecurringProfileUtil.HaveAllBillingCyclesBeenCompleted(profile, -5) == false);
                Assert.That(RecurringProfileUtil.HaveAllBillingCyclesBeenCompleted(profile, 999) == false);
                
            }



        }
        [Test]
        public void HaveAllBillingCyclesBeenCompletedTest2()
        {
            
            

            {
                IRecurringProfile profile = Util.DbUtil.CreateNewRecurringProfile();
                profile.TotalBillingCyclesRequired = 3;

                Assert.That(RecurringProfileUtil.HaveAllBillingCyclesBeenCompleted(profile, 0) == false);
                Assert.That(RecurringProfileUtil.HaveAllBillingCyclesBeenCompleted(profile, 1) == false);
                Assert.That(RecurringProfileUtil.HaveAllBillingCyclesBeenCompleted(profile, 2) == false);
                Assert.That(RecurringProfileUtil.HaveAllBillingCyclesBeenCompleted(profile, 3) == true);
                Assert.That(RecurringProfileUtil.HaveAllBillingCyclesBeenCompleted(profile, 4) == true);

            }


        }
        [Test]
        public void RecurringProfileMarkAsFailureServiceTest()
        {
            /*
Create a new recurring profile, with 3 maximum failure attempts
call PaymentFailed();
this should throw an error, as there are no payments due

activate profile()
set profile status to cancelled

call PaymentFailed();
this should throw an error, as profile is not active

set profile status to active

call PaymentFailed();
This should be ok now.  Assert that a notification is sent (IPaymentFailedNotificationSender.CreateAndSendNotification())

Assert.that the last payment due now has 1 payment transaction, with a status of failed

Get the list of intervals in the setting.

Assert that the last payment next failure notification is set based on the list of intervals, index #1

Assert that the last payment failure count is set to 1 now

call PaymentFailed();
Assert that the last payment failure count is set to 2 now

call PaymentFailed();
Assert that the last payment failure count is set to 3 now
Assert that a call to profile.SuspendAccount() is done. (Create an IRecurringProfileSuspender)


            


*/
            var mockNotificationSender = new Mock<IPaymentFailedNotificationSender>();
            // BusinessLogic_v3.Util.InversionOfControlUtil.Register(mockNotificationSender.Object);

            var mockProfileSuspender = new Mock<IRecurringProfileSuspender>();
            var mockRetryDateUpdater = new Mock<IRecurringProfileNextRetryDateUpdater>();
            var mockRecurringPaymentMarkAsFailureCommandHandler = new Mock<IDbCommandHandler<RecurringPaymentMarkAsFailureCommand>>();
            var classUnderTest = new RecurringProfilePaymentMarkAsFailureService(mockRecurringPaymentMarkAsFailureCommandHandler.Object, mockNotificationSender.Object,
                mockProfileSuspender.Object, mockRetryDateUpdater.Object);

            //BusinessLogic_v3.Util.InversionOfControlUtil.Register(mockProfileSuspender.Object);


            //Create a new recurring profile, with 3 maximum failure attempts
            IRecurringProfile profile = Util.DbUtil.CreateNewRecurringProfile();
            profile.MaximumFailedAttempts = 3;

            // activate profile()
            //set profile status to cancelled
            RecurringProfileService.Instance.ActivateProfile(profile,"activation details");
            profile.Status = PayMammoth.Enums.RecurringProfileStatus.Active;
            var payment = profile.GetLastPaymentDone();

            classUnderTest.MarkPaymentAsFailure(payment, "1", null);
            Assert.That(payment.FailureCount == 1);
            mockRetryDateUpdater.Verify(x => x.UpdateNextRetryDate(payment), Times.Exactly(1));
            mockNotificationSender.Verify(x => x.CreateAndQueueNotification(payment, null), Times.Exactly(1));
            mockProfileSuspender.Verify(x => x.SuspendRecurringProfile(profile, null), Times.Exactly(0));


            classUnderTest.MarkPaymentAsFailure(payment, "2", null);
            Assert.That(payment.FailureCount == 2);
            mockRetryDateUpdater.Verify(x => x.UpdateNextRetryDate(payment), Times.Exactly(2));
            mockNotificationSender.Verify(x => x.CreateAndQueueNotification(payment, null), Times.Exactly(2));
            mockProfileSuspender.Verify(x => x.SuspendRecurringProfile(profile, null), Times.Exactly(0));

            classUnderTest.MarkPaymentAsFailure(payment, "3", null);
            Assert.That(payment.FailureCount == 3);
            mockRetryDateUpdater.Verify(x => x.UpdateNextRetryDate(payment), Times.Exactly(2));
            mockNotificationSender.Verify(x => x.CreateAndQueueNotification(payment, null), Times.Exactly(2));
            mockProfileSuspender.Verify(x => x.SuspendRecurringProfile(profile, null), Times.Exactly(1));








        }

        [Test]
        public void PaymentFailureCommandHandlerTest()
        {
            /*
Create a new recurring profile, with 3 maximum failure attempts
call PaymentFailed();
this should throw an error, as there are no payments due

activate profile()
set profile status to cancelled

call PaymentFailed();
this should throw an error, as profile is not active

set profile status to active

call PaymentFailed();
This should be ok now.  Assert that a notification is sent (IPaymentFailedNotificationSender.CreateAndSendNotification())

Assert.that the last payment due now has 1 payment transaction, with a status of failed

Get the list of intervals in the setting.

Assert that the last payment next failure notification is set based on the list of intervals, index #1

Assert that the last payment failure count is set to 1 now

call PaymentFailed();
Assert that the last payment failure count is set to 2 now

call PaymentFailed();
Assert that the last payment failure count is set to 3 now
Assert that a call to profile.SuspendAccount() is done. (Create an IRecurringProfileSuspender)





*/

            
           // BusinessLogic_v3.Util.InversionOfControlUtil.Register(mockNotificationSender.Object);

          
            //BusinessLogic_v3.Util.InversionOfControlUtil.Register(mockProfileSuspender.Object);


            //Create a new recurring profile, with 3 maximum failure attempts
            IRecurringProfile profile = Util.DbUtil.CreateNewRecurringProfile();
           

            // activate profile()
//set profile status to cancelled
            RecurringProfileService.Instance.ActivateProfile(profile,"activation details");
            profile.Status = PayMammoth.Enums.RecurringProfileStatus.Cancelled;
            var payment = profile.GetLastPaymentDone();



            IDbCommandHandler<RecurringPaymentMarkAsFailureCommand> commandUnderTest = new RecurringPaymentMarkAsFailureCommandHandler();
            commandUnderTest = new BusinessLogic_v3.Classes.Decorators.DbCommandWithTransactionHandlerDecorator<RecurringPaymentMarkAsFailureCommand>(commandUnderTest);
            commandUnderTest = new BusinessLogic_v3.Classes.Decorators.DbOptimisticConcurrencyRetryDecorator<RecurringPaymentMarkAsFailureCommand>(commandUnderTest);

            //call PaymentFailed();
//this should throw an error, as profile is not active

            PayMammoth.Modules.RecurringProfileTransactionModule.IRecurringProfileTransaction transaction = null;
            try
            {
                commandUnderTest.Handle(new RecurringPaymentMarkAsFailureCommand(payment) {  FailureData = "test failure" });
            }
            catch (RecurringProfileException ex)
            {
                Assert.That(ex.Message.Contains("This cannot be called when the profile is not marked as active"));
            }
            
            /*set profile status to active

call PaymentFailed();
This should be ok now.  Assert that a notification is sent (IPaymentFailedNotificationSender.CreateAndSendNotification())
*/
            profile.Status = PayMammoth.Enums.RecurringProfileStatus.Active;

            CS.General_v3.Util.Date.CustomDate = new DateTime(2012, 5, 1);

            //Assert.that the last payment due now has 1 payment transaction, with a status of failed


            RecurringPaymentMarkAsFailureCommand cmd = new RecurringPaymentMarkAsFailureCommand(payment) {  FailureData = "test1" };
            commandUnderTest.Handle(cmd);
            transaction = cmd.Output_Transaction;
            Assert.That(payment.Transactions.Count() == 1);
            Assert.That(transaction.Status == PayMammoth.Enums.RecurringProfileTransactionStatus.Failure);
            Assert.That(payment.FailureCount == 1);
            Assert.That(payment.Transactions.Contains(transaction));
            Assert.That(payment.Transactions.ToList()[0] == transaction);


            
            
//call PaymentFailed();
//Assert that the last payment failure count is set to 2 now

            cmd = new RecurringPaymentMarkAsFailureCommand(payment) { FailureData = "test2" };
            commandUnderTest.Handle(cmd);
            Assert.That(payment.FailureCount == 2);
            transaction = cmd.Output_Transaction;

            Assert.That(payment.Transactions.Count() == 2);
            Assert.That(transaction.Status == PayMammoth.Enums.RecurringProfileTransactionStatus.Failure);
            Assert.That(payment.FailureCount == 2);
            Assert.That(payment.Transactions.Contains(transaction));
            Assert.That(payment.Transactions.ToList()[1] == transaction);


                
            



        }


    }
}
