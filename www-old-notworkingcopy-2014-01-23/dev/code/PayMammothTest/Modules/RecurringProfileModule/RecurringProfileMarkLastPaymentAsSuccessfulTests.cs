﻿using System;
using System.Linq;
using BusinessLogic_v3.Classes.Interfaces.DbCommands;
using CS.TestGeneral_v3;
using Moq;
using NUnit.Framework;
using PayMammoth.Modules.RecurringProfileModule;
using PayMammoth.Modules.RecurringProfilePaymentModule;
using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;
using PayMammoth.Modules.RecurringProfilePaymentModule.Commands;

namespace PayMammothTest.Modules.RecurringProfileModule
{
    public class RecurringProfileMarkLastPaymentAsSuccessfulTests : BaseTestFixtureMySQL
    {
        [Test]
        public void MarkAsSuccessfulTest()
        {
            //setup data
            var payment = Util.DbUtil.CreateNewRecurringProfilePayment();
            
            //unit test
            payment.RecurringProfile.Status = PayMammoth.Enums.RecurringProfileStatus.Active;
            payment.RecurringProfile.ActivatedOn = CS.General_v3.Util.Date.Now;
            var mockNotificationSender = new Mock<IMarkAsSuccessfulNotificationSender>();
            var mockCommandHandler = new Mock<IDbCommandHandler<RecurringPaymentMarkAsSuccessfulCommand>>();
            

            var classUnderTest = new RecurringProfilePaymentMarkLastPaymentAsSuccessfulService(mockCommandHandler.Object, mockNotificationSender.Object);
           // IRecurringProfilePayment payment2 = payment.RecurringProfile.GetLastPaymentDone();
            classUnderTest.MarkAsSuccessful(payment, "REF", "PAYMENT DETAILS");


            mockCommandHandler.Verify(x => x.Handle(It.Is<RecurringPaymentMarkAsSuccessfulCommand>(cmd => cmd.Payment == payment)), Times.Once());
            mockNotificationSender.Verify(x => x.SendNotification(payment), Times.Once());
          //  Assert.That(payment.Transactions.Count == 1);
                
            
            

        }

        [Test]
        public void profile_cannot_mark_as_successful_if_there_is_no_last_payment()
        {
            //setup data
            var profile = Util.DbUtil.CreateNewRecurringProfile();

            
            
            RecurringProfileService service = new RecurringProfileService(null,null, null,null,null,null);


            Assert.Throws<InvalidOperationException>(() => service.MarkPaymentReceived(profile,"REF", null), "Cannot mark a recurring profile as successful if there is no last payment");

        }
        [Test]
        public void profile_cannot_mark_as_successful_if_last_payment_is_not_pending()
        {
            //setup data
            
            var payment = Util.DbUtil.CreateNewRecurringProfilePayment();
            payment.Status = PayMammoth.Connector.Enums.RecurringProfilePaymentStatus.Failed;

            //unit test

            var mockNotificationSender = new Mock<IMarkAsSuccessfulNotificationSender>();
            var mockCommandHandler = new Mock<IDbCommandHandler<RecurringPaymentMarkAsSuccessfulCommand>>();

            var classUnderTest = new RecurringProfilePaymentMarkLastPaymentAsSuccessfulService(mockCommandHandler.Object, mockNotificationSender.Object);

            Assert.Throws<InvalidOperationException>(() => classUnderTest.MarkAsSuccessful(payment,"REF", null), "Cannot mark a recurring profile as successful if the last payment is not marked as Pending");

        }
    }
}
