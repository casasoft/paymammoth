﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.PaymentMethods.PayPal.v1;
using PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP;

namespace PayMammothTest.PaymentMethods.PayPal.v1.PayPalManagerTests
{
    public class PayPalManagerGeneralTests : CS.TestGeneral_v3.BaseTestFixtureMySQL
    {
        [Test]
        public void FillRecurringPaymentsInfoIfPaymentRequestIsRecurringTest()
        {
            var websiteAccount = Util.DbUtil.GetAnyWebsiteAccount();

            var client = Util.PaymentMethods.PayPalUtil.GetGenericPayPalClient(websiteAccount);
            
            SetExpressCheckoutRequest expressCheckoutRequest = new SetExpressCheckoutRequest(client, null, null);
            var request = Util.DbUtil.CreateNewRequest(websiteAccount);
            var transaction = request.StartNewTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout);

            PayPalManager.Instance.fillRecurringPaymentsInfoIfPaymentRequestIsRecurring(transaction, expressCheckoutRequest);
            Assert.That(!expressCheckoutRequest.BillingTypes.Contains(PayPalEnums.BILLING_TYPES.RecurringPayments.ToString()));

            string desc = "Test Recurring Payment";

            request.Description = desc;
            request.RecurringProfileRequired = true;
            request.RecurringProfile_IntervalType= PayMammoth.Connector.Enums.RECURRING_BILLINGPERIOD.Month;
            request.RecurringProfile_IntervalFrequency = 7;
            request.RecurringProfile_TotalBillingCycles = 14;

            PayPalManager.Instance.fillRecurringPaymentsInfoIfPaymentRequestIsRecurring(transaction, expressCheckoutRequest);
            Assert.That(expressCheckoutRequest.BillingTypes.Contains(PayPalEnums.BILLING_TYPES.RecurringPayments.ToString()));
            Assert.That(expressCheckoutRequest.BillingAgreementDescriptions.Contains(desc));

            Assert.That(expressCheckoutRequest.MaxAmount >0);





        }
    }
}
