﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.PaymentMethods.PayPal.v1;
using PayMammoth.Classes.Other;

namespace PayMammothTest.PaymentMethods.PayPal.v1
{
    [TestFixture]
    public class PayPalTest : CS.TestGeneral_v3.BaseTestFixtureMySQL
    {

        private WebsiteAccount createAccount()
        {
            var w= Util.DbUtil.CreateWebsiteAccount();
            w.PaypalUseLiveEnvironment = false;
            w.PaypalEnabled = true;
            w.PaypalSandboxMerchantEmail = "info_1290107500_biz@karlcassar.com";
            w.PaypalSandboxUsername = "info_1290107500_biz_api1.karlcassar.com";
            w.PaypalSandboxPassword = "1290107511";
            w.PaypalSandboxSignature = "AXf0mvFdjMzfJ5O7GSxqthe5lBXuAN.LnIRkEjJTk2fyUE-0xPa4.bCY";
            w.SaveAndCommit();
            return w;
        }

        [Test]
        public void StartExpressCheckoutTest()
        {


            /*
             * This requires that first you login to the Sandbox website, or else it will give you an error
             */ 
            var websiteAccount = createAccount();
            InitialRequestInfo info = new InitialRequestInfo();
            info.ClientAddress1 = "20, Cor Jesu";
            info.ClientAddress2 = "Triq Il-Kittenija";
            info.ClientCountry = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Malta;
            
            info.ClientEmail = "karlcassar@gmail.com";
            info.ClientFirstName = "Karl";
            info.ClientLastName = "Cassar";
            info.ClientLocality = "Zurrieq";
            info.ClientPostCode = "ZRQ4020";
            info.CurrencyCode = CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro;
            info.HandlingAmount= 5;
            info.PriceExcTax = 10;
            info.Reference = CS.General_v3.Util.Date.Now.ToString("yyyyMMddHHmmss");
            info.ShippingAmount = 2;
            info.TaxAmount = 1;
            info.Title = "Testing";
            info.WebsiteAccountCode = websiteAccount.Code;

                

            var request = websiteAccount.GenerateNewRequest(info);
            Assert.That(request, Is.Not.Null);
            var transaction = request.StartNewTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout);
            
            var response =  PayPalManager.Instance.StartExpressCheckout(transaction, autoRedirect: false);
            Assert.That(response.Success, Is.True);


        }

    }
}
