﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Remote.aspx.cs" Inherits="Dovetail.Realex.DemoSite.Remote.Remote" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Realex Remote Example</title>
     <style type="text/css">
        h2
        {
            font-weight: bolder;
            font-size: large;
        }
        #RealexRequest
        {
            border-color: #CCFFCC;
            border-style: solid;
            float:left;
            width: 550px;
            clear: both;
        }
        #RealexResponse
        {
            margin-top: 10px;
            border-style: solid;
            border-color: #FF9999;
            clear: both;
            width: 550px;
        }
        #DevelopersNote
        {
             margin-top: 10px;
             border-color: #FF9900;
             border-style: solid;
             /*width: 550px;*/
        }
        #autosettleinfo
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: smaller;
            background-color: Yellow;
            position: absolute;
            width: 280px;
            left: 300px;
            top: 270px;
            visibility: hidden;
        }
        .RequestInfo
        {
            float: left;
            background-color: #99CCFF;
            font-family: Arial, Helvetica, sans-serif;
            font-size: smaller;
            width: 400px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <h1>Realex Remote Example</h1>
    <a href="../Default.aspx" style="float:left;">Home Page</a>
    <div id="RealexRequest">
        <h2>Request Parameters</h2>
        
        Request type:
        <asp:RadioButtonList ID="rblRequestType" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
            RepeatLayout="Flow" OnSelectedIndexChanged="rblRequestType_SelectedIndexChanged">
            <asp:ListItem>Auth</asp:ListItem>
            <asp:ListItem>Manual</asp:ListItem>
            <asp:ListItem>Offline</asp:ListItem>
            <asp:ListItem>Rebate</asp:ListItem>
            <asp:ListItem>Void</asp:ListItem>
            <asp:ListItem>Realscore</asp:ListItem>
            <asp:ListItem>Settle</asp:ListItem>
        </asp:RadioButtonList>
        <br />&nbsp;<br />
        
        <asp:Panel runat="server" ID="pnlAmount">
            Amount in Euro
            <asp:TextBox ID="txtAmount" runat="server" /></asp:Panel>
        
        <asp:Panel ID="pnlCardDetails" runat="server">
            Cardholder name
            <asp:TextBox ID="txtCardholder" runat="server"></asp:TextBox><br />
            Card type:
            <asp:DropDownList ID="ddlCardType" runat="server">
                <asp:ListItem Selected="True">Visa</asp:ListItem>
                <asp:ListItem>MasterCard</asp:ListItem>
                <asp:ListItem>Switch</asp:ListItem>
                <asp:ListItem>Amex</asp:ListItem>
                <asp:ListItem>Laser</asp:ListItem>
            </asp:DropDownList>
            <br />
            Card number <asp:TextBox ID="txtNumber" runat="server"></asp:TextBox><br />
            Security code <asp:TextBox ID="txtCVV" runat="server"></asp:TextBox><br />
            Expiry date <asp:TextBox ID="txtExpiryDate" runat="server"></asp:TextBox>
        </asp:Panel>
        
        <asp:Panel runat="server" ID="pnlAutosettle">
            Autosettle <asp:CheckBox ID="chkAutosettle" runat="server" Checked="true" />
        </asp:Panel>
        
        <asp:Panel runat="server" ID="pnlOrderID">
            Order ID <asp:TextBox ID="txtOrderID" runat="server"></asp:TextBox>
        </asp:Panel>
        
        <asp:Panel runat="server" ID="pnlPasRef">
            Pasref <asp:TextBox ID="txtPasref" runat="server"></asp:TextBox>
        </asp:Panel>
        
        <asp:Panel runat="server" ID="pnlAuthCode">
            Auth Code <asp:TextBox ID="txtAuthcode" runat="server"></asp:TextBox>
        </asp:Panel>
        
        <asp:Button ID="btnSendRequest" runat="server" Text="Send Request" OnClick="btnSendRequest_Click" />
        
    </div>
    
    <!-- Start of info panels -->
    
    <asp:Panel runat="server" ID="pnlRequestInfoAuth" CssClass="RequestInfo">
        This is a basic payment authorisation request. It is the primary request used with
        RealAuth. By choosing which sample credit card number to use here, you can get different
        responses back from Realex including Success (00), Declined by Bank (101) and Referral
        by Bank (102).
        <br />
        <br />
        If AutoSettle is checked, the transaction will go into the "Pending" list for settlement tonight. 
        If not, it will remain on Realex's database indefinitely until you flag it for settlement.
        <br />
            
    <!--DELETE_FROM_HERE-->
    <!-- This section will be deleted in FinalBuilder before shipping to Realex -->
        <h3>Quick populators</h3>
        <p>For your convenience while testing, these links will populate the fields with appropiate data to generate 
        the named responses.  These use Realex's standard test credit card numbers which you should have already received.</p>
        <asp:LinkButton ID="btnSuccess" runat="server" OnClick="btnSuccess_Click">Success 00</asp:LinkButton>
        <asp:LinkButton ID="btnDeclined" runat="server" OnClick="btnDeclined_Click">Declined 101</asp:LinkButton>
        <asp:LinkButton ID="btnReferral" runat="server" OnClick="btnReferral_Click">Referral B 102</asp:LinkButton>
        <asp:LinkButton ID="btnCommsError" runat="server" OnClick="btnCommsError_Click">CommsError 205</asp:LinkButton>
        <asp:LinkButton ID="btnExpired" runat="server" OnClick="btnExpired_Click">Expired 509</asp:LinkButton>
    <!--DELETE_TO_HERE-->

    </asp:Panel>
    <asp:Panel runat="server" ID="pnlRequestInfoManual" CssClass="RequestInfo">
        To test the Manual request just make up an Authcode (up to 10 alphanumeric characters)
        as if the authorisation centre had supplied it to you.
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlRequestInfoOffline" CssClass="RequestInfo">
        The Offline request is used to assign an Authcode to a previous transaction that
        returned a result of 101 or 102 (Declined or Referral). Therefore to test this request
        your must first create a suitable transaction and then use the return values. Follow
        the following steps:<br />
        <ol>
            <li>Choose request type of Auth</li>
            <li>Create a new transaction using a credit card number that will generate a 101 or
                102 (Declined or Referral)</li>
            <li>Submit that transaction. The response will be displayed in the red box at the bottom</li>
            <li>Select request type of Offline</li>
            <li>Copy the Order ID and PasRef from the previous response into the appropiate boxes</li>
            <li>Make up an Authcode (this only works in Test mode)</li>
            <li>Submit the transaction</li>
            <li>The original transaction will now be authorised</li>
        </ol>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlRequestInfoRebate" CssClass="RequestInfo">
        You can only rebate an existing successfully settled authorisation, so to test this follow these steps:
        <ol>
            <li>Choose request type of Auth</li>
            <li>Create a successful transaction using a credit card number that will generate a response code of 00</li>
            <li>Submit that transaction. The response will be displayed in the red box at the bottom</li>
            <li>Select request type of Rebate</li>
            <li>Enter the amount to rebate (Max 115% of original value)</li>
            <li>Copy the Order ID, PasRef and Authcode from the previous response into the appropiate boxes</li>
            <li>Submit the transaction</li>
            <li>The rebate will be processed</li>
        </ol>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlRequestInfoVoid" CssClass="RequestInfo">
        It is possible to void an authorisation, manual, offline or rebate request, before settlement.  If the transaction has been settled or batched then 
        it cannot be voided.  To test follow these steps:
        <ol>
            <li>Choose request type of Auth</li>
            <li>Create a successful transaction using a credit card number that will generate a response code of 00</li>
            <li>Submit that transaction. The response will be displayed in the red box at the bottom</li>
            <li>Select request type of Void</li>
            <li>Copy the Order ID, PasRef and Authcode from the previous response into the appropiate boxes</li>
            <li>Submit the transaction</li>
            <li>The original transaction will be voided</li>
        </ol>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlRequestInfoRealscore" CssClass="RequestInfo">
        The only difference between an Auth request and a Realscore request is that no actual authorisation is performed.  Only the realscore and the results of the individual checks are returned. You can see this in the response message.
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlRequestInfoSettle" CssClass="RequestInfo">
        The Settle request is used to capture a transaction that has previously been authorised using an auth request with the autosettle flag turned off.
        
         To test follow these steps:
        <ol>
            <li>Choose request type of Auth</li>
            <li>Clear the AutoSettle checkbox</li>
            <li>Create a successful transaction using a credit card number that will generate a response code of 00</li>
            <li>Submit that transaction. The response will be displayed in the red box at the bottom</li>
            <li>Select request type of Settle</li>
            <li>Copy the Order ID, PasRef and Authcode from the previous response into the appropiate boxes</li>
            <li>Submit the transaction</li>
            <li>The original transaction will flagged for settlement in the next batch</li>
        </ol>
    </asp:Panel>


    <!-- End of info panels -->


    <div id="RealexResponse">
        <h2>Last response from Realex</h2>
        Time: <asp:Label ID="lblTimestamp" runat="server" Text=""></asp:Label><br />
        Response code: <asp:Label ID="lblResult" runat="server" Text=""></asp:Label><br />
        Message: <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label><br />
        Order ID: <asp:Label ID="lblOrderID" runat="server" Text=""></asp:Label><br />
        PAS Ref: <asp:Label ID="lblPasRef" runat="server" Text=""></asp:Label><br />
        Auth Code: <asp:Label ID="lblAuthCode" runat="server" Text=""></asp:Label><br />
    </div>
    
    <div id="DevelopersNote">
        <h2>Notes for Developers</h2>
        <ul>
            <li>Be sure to update the web.config file with your Merchant ID, Shared Secret and Account Name.  (Don't worry if you don't have a "Rebate Password": you do not need one to be able to take payments.  It is only needed for issuing refunds.)</li>
            <li>Ensure that Realex have configured their systems to expect requests from your IP address.  By default your IP will be blocked for security reasons.</li>
            <li>Certain request types, such as Manual, are not enabled by default.  Contact Realex if you need to have these enabled.</li>
            <li>This sample uses the Realex Payment Processor library, developed by 
                <a href="http://www.dovetail.ie">Dovetail Technologies</a> for Realex.&nbsp; 
                This is provided to you by Realex, and you are free to use the library in your 
                own applications.</li>
            <li>The most important routine in this sample code is <span style="font-weight: bold;">ProcessAuthRequest</span>, 
                located in the code-behind for this file.  It demonstrates use of the Realex Payment Processor Library to take a payment.  Pretty much everything else is user interface code, 
                or a variation on this basic request.</li>
            <li>Any queries regarding this sample application should be directed to 
                <a href="http://www.realex.ie">Realex Payments</a>.</li>
        </ul>
    </div>

    </form>
</body>
</html>
