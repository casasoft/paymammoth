<%@ Page Language="c#" CodeBehind="MpiResponse.aspx.cs" AutoEventWireup="True" Inherits="Dovetail.Realex.DemoSite.Mpi.MpiResponse" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Inbound</title>
    <style type="text/css">
        h2
        {
            font-weight: bolder;
            font-size: large;
        }
        #RealexRequest
        {
            border-color: #CCFFCC;
            border-style: solid;
            width: 550px;
            clear: both;
        }
        #RealexResponse
        {
            margin-top: 10px;
            border-style: solid;
            border-color: #FF9999;
            clear: both;
            width: 550px;
        }
        #DevelopersNote
        {
            margin-top: 10px;
            border-color: #FF9900;
            border-style: solid;
        }
        #autosettleinfo
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: smaller;
            background-color: Yellow;
            position: absolute;
            width: 280px;
            left: 300px;
            top: 270px;
            visibility: hidden;
        }
        .RequestInfo
        {
            float: left;
            background-color: #99CCFF;
            font-family: Arial, Helvetica, sans-serif;
            font-size: smaller;
            width: 400px;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <h1>Realex 3-D: Termination Page</h1>
    <a href="../Default.aspx" style="float:left;">Home Page</a>&nbsp;&nbsp;&nbsp;<a href="MpiRequest.aspx">Start again</a>
        <div id="RealexResponse">
            <h2>Response from Realex</h2>
            Time: <asp:Label ID="lblTimestamp" runat="server" Text=""></asp:Label><br />
            Outcome: <asp:Label ID="lblOutcome" runat="server" Text=""></asp:Label><br />
            Response code: <asp:Label ID="lblResult" runat="server" Text=""></asp:Label><br />
            Message: <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label><br />
            Order ID: <asp:Label ID="lblOrderID" runat="server" Text=""></asp:Label><br />
        </div>
    </form>
</body>
</html>
