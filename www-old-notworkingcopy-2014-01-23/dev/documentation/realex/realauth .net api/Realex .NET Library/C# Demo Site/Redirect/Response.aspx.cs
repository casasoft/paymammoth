﻿using System;
using System.Web.Configuration;
using Dovetail.Realex.Realauth;

namespace Dovetail.Realex.DemoSite.Redirect
{
    public partial class Response : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // variables to hold the values posted by Realex.
            string timestamp = Request.Form["TIMESTAMP"];
            string result = Request.Form["RESULT"];
            string orderID = Request.Form["ORDER_ID"];
            string message = Request.Form["MESSAGE"];
            string authcode = Request.Form["AUTHCODE"];
            string pasref = Request.Form["PASREF"];
            string realexHash = Request.Form["SHA1HASH"];

            // get the Realex settings from the web config
            string merchantID = WebConfigurationManager.AppSettings["RealexMerchantID"];
            string sharedSecret = WebConfigurationManager.AppSettings["RealexSharedSecret"];

            // generate the hash according to Realex's rules
            string temp1 = timestamp + "." + merchantID + "." + orderID + "." + result + "." + message + "." + pasref + "." + authcode;
            string temp2 = RealexProcessor.ComputeSha1Hash(temp1);
            string temp3 = temp2 + "." + sharedSecret;
            string hash = RealexProcessor.ComputeSha1Hash(temp3);

            // First check the hashes match
            if (hash != realexHash)
            {
                lblMessage.Text = "Critical error: the response did not authenticate.";
                return;
            }

            // Since the hashes match, we can be confident that the paramaters supplied are valid.

            // check if the payment authorised ok
            if (result == "00")
            {
                // Payment has been accepted.  Process the order, update the database etc.
                // Be sure to record the authcode and the pasref returned by Realex.
                // <YOUR BUSINESS LOGIC HERE! >

                // Then tell the user that it has succeeded.
                lblMessage.Text = "Order completed successfully.  Authorisation code: " + authcode;
            }
            else
            {
                // the payment did not go through.  Tell the user.  In a real application you should let them try again.
                lblMessage.Text = "Payment NOT accepted.  Result code: " + result + ".  Message: " + message;                   
            }
        }
    }
}