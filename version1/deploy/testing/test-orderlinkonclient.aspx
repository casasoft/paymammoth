﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="test-orderlinkonclient.aspx.cs" Inherits="PayMammothDeploy.testing.testOrderLinkOnClient" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
<h1>Order Link on client's website (Test)</h1>
<p>
This shows that the client would have been redirected to the order link as defined in the initial request.
</p>
</asp:Content>