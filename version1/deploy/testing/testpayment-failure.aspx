﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="testpayment-failure.aspx.cs" Inherits="PayMammothDeploy.testing.testpayment_failure" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
<h1>Payment - Failure</h1>
<p>
This shows that the payment was cancelled by the user.
</p>
</asp:Content>