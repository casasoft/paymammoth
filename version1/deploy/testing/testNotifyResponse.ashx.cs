﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;

namespace PayMammothDeploy.testing
{
    /// <summary>
    /// Summary description for testNotifyResponse
    /// </summary>
    public class testNotifyResponse : PayMammoth.Connector.Handlers.PaymentConfirmationHandler
    {
        protected readonly ILog _log = LogManager.GetLogger(typeof(testNotifyResponse));

        protected override void processRequest(HttpContext ctx)
        {
            System.Threading.Thread.Sleep(3000); //induce a lag

            base.processRequest(ctx);
        }

        protected override void OnPaidSuccessfully(PayMammoth.Connector.Classes.Responses.ConfirmResponse response)
        {
            if (response != null)
            {
                _log.Info(response.ToString());
            }
            else
            {
                _log.Info("OnPaidSuccessfully");
            }

        }
    }
}