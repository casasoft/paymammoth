﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth;
using System.Web.UI.HtmlControls;
using PayMammoth.Frontend.PaymentRequestModule;
using PayMammoth.Modules.WebsiteAccountModule;

namespace PayMammothDeploy
{
    using CS.WebComponentsGeneralV3.Code.Classes.Pages;
    using BaseMainMasterPage = PayMammoth.Classes.Pages.Frontend.BaseMainMasterPage;

    public partial class main : BaseMainMasterPage
    {

        public PaymentRequestFrontend CurrentPaymentRequest
        {
            get
            {
                return this.Master.CurrentPaymentRequest;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            var request = this.CurrentPaymentRequest;
            if (request != null)
            {
                var websiteAccount = request.Data.WebsiteAccount;
                if (websiteAccount != null)
                {
                    initCustomLogo(websiteAccount);

                }
            }
            base.OnLoad(e);
        }

        
        private void initCustomLogo(IWebsiteAccount websiteAccount)
        {
            var logo = websiteAccount.Logo;
            if(!logo.IsEmpty())
            {
                mainLogo.Functionality.logoUrl = logo.GetSpecificSizeUrl(WebsiteAccount.LogoSizingEnum.Normal);
                mainLogo.Functionality.logoAlt = websiteAccount.WebsiteName;  
                //URL TODO 20/12/11
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get { return null; }
        }
        public new Core Master { get { return (Core)base.Master; } }

            //protected override HtmlGenericControl _divMessage
        //{
        //    get { return divMessage; }
        //}
    }
}