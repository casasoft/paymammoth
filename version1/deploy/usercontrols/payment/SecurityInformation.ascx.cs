﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Controls.UserControls;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.Modules;
namespace PayMammothDeploy.usercontrols.payment
{
    public partial class SecurityInformation : BaseUserControl
    {
        #region SecurityInformation Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected SecurityInformation _item = null;
            internal FUNCTIONALITY(SecurityInformation item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "Item is required");
                this._item = item;
            }
            public string BankStatementText { get; set; }
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
        }

        private void updateTexts()
        {
            var ctSecurityInformationText =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_CreditCard_SecurityInformation);
            string strSecurityInformationText =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_CreditCard_SecurityInformation).GetContent();

            string transactionID = Functionality.BankStatementText;
            var tr = ctSecurityInformationText.CreateTokenReplacer();
            tr["[Transaction_ID]"] = transactionID;
            tr["Transaction_ID"] = transactionID;
            tr.ReplaceInString(ref strSecurityInformationText);
            //strSecurityInformationText = CS.General_v3.Util.Text.ReplaceTa2g(strSecurityInformationText, "transaction_id",
                                                                            //transactionID);
            securityInformationText.Text = strSecurityInformationText;
            h3SecurityInformationTitle.InnerHtml = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_CreditCard_SecurityInformationSectionTitle).GetContent();
        }
    }
}