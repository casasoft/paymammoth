﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using PayMammoth.Controls.UserControls;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Extensions;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.Modules;
namespace PayMammothDeploy.usercontrols.payment
{
    public partial class YourDetails : BaseUserControl
    {
        #region YourDetails Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected YourDetails _item = null;
            internal FUNCTIONALITY(YourDetails item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "Item is required");
                this._item = item;
            }
            public IClientDetails ClientDetails { get; set; }
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if(Functionality.ClientDetails != null)
            {
                MyContentText.AttachContentTextWithControl(h3YourDetails, ((ContentText)Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.Payment_Details_YourDetailsSectionTitle)).ToFrontend());
                
                spanName.InnerHtml = Functionality.ClientDetails.GetClientFullName();
                spanEmail.InnerHtml = Functionality.ClientDetails.ClientEmail;
                var address = Functionality.ClientDetails.GetAddressAsOneLine(true);
                detailsAddress.Text = CS.General_v3.Util.Text.ReplaceTexts(address, "<br/>", ",");
            }
        }
    }
}