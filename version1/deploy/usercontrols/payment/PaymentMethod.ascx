﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentMethod.ascx.cs"
    Inherits="PayMammothDeploy.usercontrols.payment.PaymentMethod" %>
<div class="payment-method-wrapper rounded-border">
    <div class="payment-method-selection-content">
        <CSControls:MyRadioButton runat="server" ID="rdBtnPayment" GroupName="paymentMethod" />
        <span runat="server" id="spanPaymentMethodIcon"></span>
    </div>
    <div class="payment-method-details">
        <span class="payment-method-details-title" runat="server" id="spanPaymentTitle">Paypal</span>
        <span class="payment-method-details-description" runat="server" id="spanPaymentDescription">
            Pay through the highly popular & secure payment gateway.</span>
    </div>
    <div class="clear">
    </div>
</div>
