﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecurityInformation.ascx.cs"
    Inherits="PayMammothDeploy.usercontrols.payment.SecurityInformation" %>
<div class="security-information-wrapper">
    <h3 class="h3-security-information rounded-border" runat="server" id="h3SecurityInformationTitle">
        Security Information</h3>
    <asp:Literal runat="server" ID="securityInformationText">
        
    </asp:Literal>
</div>
