﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.Modules;
using PayMammoth.Controls.UserControls;
using PayMammoth.Connector.Classes.Requests;
namespace PayMammothDeploy.usercontrols.masterpage.v1
{
    public partial class ItemDetail : BaseUserControl
    {
        public static ItemDetail LoadControl(Page pg)
        {

            return (ItemDetail)pg.LoadControl("/usercontrols/masterpage/v1/ItemDetail.ascx");
        }

        #region OrderSummary Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected ItemDetail _item = null;
            internal FUNCTIONALITY(ItemDetail item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "Item is required");
                this._item = item;
            }
            public IItemDetail ItemDetail { get; set; }
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(Functionality.ItemDetail, "Item Detail is required");
            var detail = this.Functionality.ItemDetail;
            spanTitle.InnerHtml = detail.Title;
            if (!string.IsNullOrWhiteSpace(detail.Description))
            {
                pDescription.InnerHtml = CS.General_v3.Util.Text.TxtForHTML(detail.Description);
                
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(pDescription);
            }

        }
    }
}