﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderSummaryDetails.ascx.cs"
    Inherits="PayMammothDeploy.usercontrols.masterpage.v1.OrderSummaryDetails" %>
<!--MasterPage/v1/OrderSummaryDetails-->
<table cellpadding="0" cellspacing="0">
    <tr runat="server" id="trOrderNetTotal">
        <td class="order-details-net-total">
            <label><span runat="server" id="lblOrderNetTotal">
                Order Total</span>:</label>
        </td>
        <td class="field">
            <span runat="server" id="spanOrderNetTotal">&euro;5.00</span>
        </td>
    </tr>
    <tr runat="server" id="trTax">
        <td class="order-details-tax">
            <label><span runat="server" id="lblTax">
                Tax</span>:</label>
        </td>
        <td class="field">
            <span runat="server" id="spanTax">&euro;2.15</span>
        </td>
    </tr>
    <tr runat="server" id="trShipping">
        <td class="order-details-shipping">
            <label><span runat="server" id="lblShipping">
                Shipping</span>:</label>
        </td>
        <td class="field">
            <span runat="server" id="spanShipping">&euro;1.60</span>
        </td>
    </tr>
    <tr runat="server" id="trHandling">
        <td class="order-details-handling">
            <label><span runat="server" id="lblHandling">
                Handling</span>:</label>
        </td>
        <td class="field order-details-handling-field">
            <span runat="server" id="spanHandling">&euro;1.25</span>
        </td>
    </tr>
    <tr runat="server" class="order-detail-spacer-row">
        <td class="order-detail-spacer-row-cell">
            &nbsp;
        </td>
        <td class="order-detail-spacer-row-cell">
            &nbsp;
        </td>
    </tr>
    <tr runat="server" id="trTotal" class="order-detail-total-row rounded-border">
        <td class="order-details-total">
            <label><span runat="server" id="lblTotal">
                Total</span>:</label>
        </td>
        <td class="field order-details-total-field">
            <span runat="server" id="spanTotal">&euro;10.25</span>
        </td>
    </tr>
</table>
<div class="clear">
</div>
