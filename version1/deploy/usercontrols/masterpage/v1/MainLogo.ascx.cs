﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Controls.UserControls;

namespace PayMammothDeploy.usercontrols.masterpage.v1
{
    public partial class MainLogo : BaseUserControl
    {
        #region MainLogo Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected MainLogo _item = null;
            public string anchorUrl { get; set; }
            public string logoUrl { get; set; }
            public string logoAlt { get; set; }

            internal FUNCTIONALITY(MainLogo item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "Item is required");
                this.anchorUrl = "http://www.discountpages.com.mt";
                this.logoUrl = "/images/logo.png";
                this.logoAlt = "Discount Pages";
            }
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if(!String.IsNullOrEmpty(Functionality.logoUrl))
            {
                this.imgMainLogo.ImageUrl = Functionality.logoUrl;
                this.imgMainLogo.AlternateText = Functionality.logoAlt;
                //this.anchorMainLogo.HRef = Functionality.anchorUrl;
            }
        }
    }
}