﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainLogo.ascx.cs" Inherits="PayMammothDeploy.usercontrols.masterpage.v1.MainLogo" %>
<h1>
    <a runat="server" id="anchorMainLogo" class="main-logo-anchor">
        <CSControls:MyImage runat="server" ID="imgMainLogo" />
    </a>
</h1>