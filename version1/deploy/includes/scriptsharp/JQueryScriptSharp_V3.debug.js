// JavascriptJQuery.js
//


Type.registerNamespace('js.com.jquery.v3');

////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3.JQUERY_EVENT_TYPE

js.com.jquery.v3.JQUERY_EVENT_TYPE = function() { 
    /// <field name="blur" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="focus" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="load" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="resize" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="scroll" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="unload" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="beforeUnload" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="click" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="doubleClick" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="mouseDown" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="mouseUp" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="mouseMove" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="mouseOver" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="mouseOut" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="mouseEnter" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="mouseLeave" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="change" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="select" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="submit" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="keyDown" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="keyPress" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="keyUp" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="error" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="ready" type="Number" integer="true" static="true">
    /// </field>
};
js.com.jquery.v3.JQUERY_EVENT_TYPE.prototype = {
    blur: 0, 
    focus: 1, 
    load: 2, 
    resize: 3, 
    scroll: 4, 
    unload: 5, 
    beforeUnload: 6, 
    click: 7, 
    doubleClick: 8, 
    mouseDown: 9, 
    mouseUp: 10, 
    mouseMove: 11, 
    mouseOver: 12, 
    mouseOut: 13, 
    mouseEnter: 14, 
    mouseLeave: 15, 
    change: 16, 
    select: 17, 
    submit: 18, 
    keyDown: 19, 
    keyPress: 20, 
    keyUp: 21, 
    error: 22, 
    ready: 23
}
js.com.jquery.v3.JQUERY_EVENT_TYPE.registerEnum('js.com.jquery.v3.JQUERY_EVENT_TYPE', false);


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3.JQUERY_POSITION

js.com.jquery.v3.JQUERY_POSITION = function() { 
    /// <field name="center" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="top" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="left" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="right" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="bottom" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="none" type="Number" integer="true" static="true">
    /// </field>
};
js.com.jquery.v3.JQUERY_POSITION.prototype = {
    center: 0, 
    top: 1, 
    left: 2, 
    right: 3, 
    bottom: 4, 
    none: 5
}
js.com.jquery.v3.JQUERY_POSITION.registerEnum('js.com.jquery.v3.JQUERY_POSITION', false);


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3.JQUERY_UI_EFFECT

js.com.jquery.v3.JQUERY_UI_EFFECT = function() { 
    /// <field name="blind" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="bounce" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="clip" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="drop" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="explode" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="fold" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="highlight" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="puff" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="pulsate" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="scale" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="shake" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="size" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="slide" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="transfer" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="fade" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="none" type="Number" integer="true" static="true">
    /// </field>
};
js.com.jquery.v3.JQUERY_UI_EFFECT.prototype = {
    blind: 0, 
    bounce: 1, 
    clip: 2, 
    drop: 3, 
    explode: 4, 
    fold: 5, 
    highlight: 6, 
    puff: 7, 
    pulsate: 8, 
    scale: 9, 
    shake: 10, 
    size: 11, 
    slide: 12, 
    transfer: 13, 
    fade: 14, 
    none: 9999
}
js.com.jquery.v3.JQUERY_UI_EFFECT.registerEnum('js.com.jquery.v3.JQUERY_UI_EFFECT', false);


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3.JQUERY_EASING

js.com.jquery.v3.JQUERY_EASING = function() { 
    /// <field name="linear" type="Number" integer="true" static="true">
    /// </field>
    /// <field name="swing" type="Number" integer="true" static="true">
    /// </field>
};
js.com.jquery.v3.JQUERY_EASING.prototype = {
    linear: 0, 
    swing: 10
}
js.com.jquery.v3.JQUERY_EASING.registerEnum('js.com.jquery.v3.JQUERY_EASING', false);


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryScrollToSettings

js.com.jquery.v3._jQueryScrollToSettings = function js_com_jquery_v3__jQueryScrollToSettings() {
    /// <field name="axis" type="String">
    /// Axes to be scrolled, 'x', 'y', 'xy' or 'yx'. 'xy' is the default.
    /// </field>
    /// <field name="duration" type="Number" integer="true">
    /// Length of the animation, none (sync) by default.
    /// </field>
    /// <field name="easing" type="String">
    /// Name of the easing equation.
    /// </field>
    /// <field name="margin" type="Boolean">
    /// If true, target's border and margin are deducted.
    /// </field>
    /// <field name="offset" type="Object">
    /// Number or hash {left: x, top:y }. This will be added to the final position(can be negative).
    /// </field>
    /// <field name="over" type="Number" integer="true">
    /// Add a fraction of the element's height/width (can also be negative).
    /// </field>
    /// <field name="queue" type="Boolean">
    /// If true and both axes are scrolled, it will animate on one axis, and then on the other. Note that 'axis' being 'xy' or 'yx', concludes the order
    /// </field>
    /// <field name="onAfterFirst" type="Action">
    /// If queing, a function to be called after scrolling the first axis.
    /// </field>
    /// <field name="onAfter" type="Action">
    /// A function to be called after the whole animation ended.
    /// </field>
}
js.com.jquery.v3._jQueryScrollToSettings.prototype = {
    axis: null,
    duration: 0,
    easing: null,
    margin: false,
    offset: null,
    over: 0,
    queue: false,
    onAfterFirst: null,
    onAfter: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryPopupWindowOptions

js.com.jquery.v3._jQueryPopupWindowOptions = function js_com_jquery_v3__jQueryPopupWindowOptions() {
    /// <field name="centerBrowser" type="Number" integer="true">
    /// </field>
    /// <field name="centerScreen" type="Number" integer="true">
    /// </field>
    /// <field name="height" type="Number" integer="true">
    /// </field>
    /// <field name="left" type="Number" integer="true">
    /// </field>
    /// <field name="location" type="Number" integer="true">
    /// </field>
    /// <field name="menubar" type="Number" integer="true">
    /// </field>
    /// <field name="resizable" type="Number" integer="true">
    /// </field>
    /// <field name="scrollbars" type="Number" integer="true">
    /// </field>
    /// <field name="width" type="Number" integer="true">
    /// </field>
    /// <field name="windowName" type="String">
    /// </field>
    /// <field name="windowURL" type="String">
    /// </field>
    /// <field name="top" type="Number" integer="true">
    /// </field>
    /// <field name="toolbar" type="Number" integer="true">
    /// </field>
}
js.com.jquery.v3._jQueryPopupWindowOptions.prototype = {
    centerBrowser: 0,
    centerScreen: 0,
    height: 0,
    left: 0,
    location: 0,
    menubar: 0,
    resizable: 0,
    scrollbars: 0,
    width: 0,
    windowName: null,
    windowURL: null,
    top: 0,
    toolbar: 0
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryUIResizeableOptionsEventUI

js.com.jquery.v3._jQueryUIResizeableOptionsEventUI = function js_com_jquery_v3__jQueryUIResizeableOptionsEventUI() {
    /// <field name="helper" type="Object" domElement="true">
    /// </field>
    /// <field name="originalPosition" type="js.com.jquery.v3._Position">
    /// </field>
    /// <field name="originalSize" type="js.com.jquery.v3.Objects._Size">
    /// </field>
    /// <field name="position" type="js.com.jquery.v3._Position">
    /// </field>
    /// <field name="size" type="js.com.jquery.v3.Objects._Size">
    /// </field>
}
js.com.jquery.v3._jQueryUIResizeableOptionsEventUI.prototype = {
    helper: null,
    originalPosition: null,
    originalSize: null,
    position: null,
    size: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryUIResizeableOptions

js.com.jquery.v3._jQueryUIResizeableOptions = function js_com_jquery_v3__jQueryUIResizeableOptions() {
    /// <field name="disabled" type="Boolean">
    /// Disables (true) or enables (false) the resizable. Can be set when initialising (first creating) the resizable.
    /// </field>
    /// <field name="alsoResize" type="Object">
    /// Can be Selector, jQuery, Element
    /// Resize these elements synchronous when resizing.
    /// </field>
    /// <field name="animate" type="Boolean">
    /// Animates to the final size after resizing.
    /// </field>
    /// <field name="animateDuration" type="Object">
    /// Can be Integer or String
    /// Duration time for animating, in milliseconds. Other possible values: 'slow', 'normal', 'fast'.
    /// </field>
    /// <field name="animateEasing" type="String">
    /// Easing effect for animating.
    /// </field>
    /// <field name="aspectRatio" type="Object">
    /// Can be Boolean or Float
    /// If set to true, resizing is constrained by the original aspect ratio. Otherwise a custom aspect
    /// ratio can be specified, such as 9 / 16, or 0.5.
    /// </field>
    /// <field name="autoHide" type="Boolean">
    /// If set to true, automatically hides the handles except when the mouse hovers over the element.
    /// </field>
    /// <field name="cancel" type="Object">
    /// Type: Selector
    /// Prevents resizing if you start on elements matching the selector.
    /// </field>
    /// <field name="containment" type="Object">
    /// Can be String, Element, Selector
    /// Constrains resizing to within the bounds of the specified element. Possible
    /// values: 'parent', 'document', a DOMElement, or a Selector.
    /// </field>
    /// <field name="delay" type="Number" integer="true">
    /// Tolerance, in milliseconds, for when resizing should start. If specified, resizing will not
    /// start until after mouse is moved beyond duration. This can help prevent unintended resizing
    /// when clicking on an element.
    /// </field>
    /// <field name="distance" type="Number" integer="true">
    /// Tolerance, in pixels, for when resizing should start. If specified, resizing will not start
    /// until after mouse is moved beyond distance. This can help prevent unintended resizing when
    /// clicking on an element.
    /// </field>
    /// <field name="ghost" type="Boolean">
    /// If set to true, a semi-transparent helper element is shown for resizing.
    /// </field>
    /// <field name="grid" type="Object">
    /// Type: Array
    /// Snaps the resizing element to a grid, every x and y pixels. Array values: [x, y]
    /// </field>
    /// <field name="handles" type="Object">
    /// </field>
    /// <field name="helper" type="String">
    /// This is the css class that will be added to a proxy element to outline the resize during the drag
    /// of the resize handle. Once the resize is complete, the original element is sized.
    /// </field>
    /// <field name="maxHeight" type="Number" integer="true">
    /// This is the maximum height the resizable should be allowed to resize to.
    /// </field>
    /// <field name="maxWidth" type="Number" integer="true">
    /// This is the maximum width the resizable should be allowed to resize to.
    /// </field>
    /// <field name="minHeight" type="Number" integer="true">
    /// This is the minimum height the resizable should be allowed to resize to.
    /// </field>
    /// <field name="minWidth" type="Number" integer="true">
    /// This is the minimum width the resizable should be allowed to resize to.
    /// </field>
    /// <field name="create" type="js.com.jquery.v3.createResizableEvent">
    /// This event is triggered when resizable is created.
    /// </field>
    /// <field name="start" type="js.com.jquery.v3.startResizableEvent">
    /// This event is triggered at the start of a resize operation.
    /// </field>
    /// <field name="resize" type="js.com.jquery.v3.resizeEvent">
    /// This event is triggered during the resize, on the drag of the resize handler.
    /// </field>
    /// <field name="stop" type="js.com.jquery.v3.stopResizableEvent">
    /// This event is triggered at the end of a resize operation.
    /// </field>
}
js.com.jquery.v3._jQueryUIResizeableOptions.prototype = {
    disabled: false,
    alsoResize: false,
    animate: false,
    animateDuration: 'slow',
    animateEasing: 'swing',
    aspectRatio: false,
    autoHide: false,
    cancel: ':input,option',
    containment: false,
    delay: 0,
    distance: 1,
    ghost: false,
    grid: false,
    handles: 'e, s, se',
    helper: null,
    maxHeight: 0,
    maxWidth: 0,
    minHeight: 0,
    minWidth: 0,
    create: null,
    start: null,
    resize: null,
    stop: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryUIDraggableOptionsEventUI

js.com.jquery.v3._jQueryUIDraggableOptionsEventUI = function js_com_jquery_v3__jQueryUIDraggableOptionsEventUI() {
    /// <field name="helper" type="Object" domElement="true">
    /// </field>
    /// <field name="position" type="js.com.jquery.v3._Position">
    /// </field>
    /// <field name="offset" type="js.com.jquery.v3._Position">
    /// </field>
}
js.com.jquery.v3._jQueryUIDraggableOptionsEventUI.prototype = {
    helper: null,
    position: null,
    offset: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryUIDraggableOptions

js.com.jquery.v3._jQueryUIDraggableOptions = function js_com_jquery_v3__jQueryUIDraggableOptions() {
    /// <field name="disabled" type="Boolean">
    /// Disables (true) or enables (false) the draggable. Can be set when initialising (first creating) the draggable.
    /// </field>
    /// <field name="addClasses" type="Boolean">
    /// If set to false, will prevent the ui-draggable class from being added. This may be desired as a performance
    /// optimization when calling .draggable() init on many hundreds of elements.
    /// </field>
    /// <field name="appendTo" type="Object" domElement="true">
    /// The element passed to or selected by the appendTo option will be used as the draggable helper's container
    /// during dragging. By default, the helper is appended to the same container as the draggable.
    /// </field>
    /// <field name="axis" type="String">
    /// Constrains dragging to either the horizontal (x) or vertical (y) axis. Possible values: 'x', 'y'.
    /// </field>
    /// <field name="cancel" type="Object" domElement="true">
    /// Prevents dragging from starting on specified elements.
    /// </field>
    /// <field name="connectToSortable" type="Object" domElement="true">
    /// Allows the draggable to be dropped onto the specified sortables. If this option is used (helper must be
    /// set to 'clone' in order to work flawlessly), a draggable can be dropped onto a sortable list and then
    /// becomes part of it.
    /// Note: Specifying this option as an array of selectors has been removed.
    /// </field>
    /// <field name="containment" type="Object">
    /// Valid types: Selector, Element, String, Array
    /// Constrains dragging to within the bounds of the specified element or region. Possible string
    /// values: 'parent', 'document', 'window', [x1, y1, x2, y2].
    /// </field>
    /// <field name="cursor" type="String">
    /// The css cursor during the drag operation.
    /// </field>
    /// <field name="cursorAt" type="Object">
    /// Sets the offset of the dragging helper relative to the mouse cursor. Coordinates can
    /// be given as a hash using a combination of one or two keys: { top, left, right, bottom }.
    /// </field>
    /// <field name="delay" type="Number" integer="true">
    /// Time in milliseconds after mousedown until dragging should start. This option can be
    /// used to prevent unwanted drags when clicking on an element.
    /// </field>
    /// <field name="distance" type="Number" integer="true">
    /// Distance in pixels after mousedown the mouse must move before dragging should start.
    /// This option can be used to prevent unwanted drags when clicking on an element.
    /// </field>
    /// <field name="grid" type="Array" elementType="Object">
    /// Snaps the dragging helper to a grid, every x and y pixels. Array values: [x, y]
    /// </field>
    /// <field name="handle" type="Object">
    /// Can be an element or a Selector
    /// If specified, restricts drag start click to the specified element(s).
    /// </field>
    /// <field name="helper" type="Object">
    /// Can be String or Function
    /// Allows for a helper element to be used for dragging display. Possible
    /// values: 'original', 'clone', Function. If a function is specified, it must
    /// return a DOMElement.
    /// </field>
    /// <field name="iframeFix" type="Object">
    /// Can be boolean or Selector
    /// Prevent iframes from capturing the mousemove events during a drag. Useful
    /// in combination with cursorAt, or in any case, if the mouse cursor is not
    /// over the helper. If set to true, transparent overlays will be placed over
    /// all iframes on the page. If a selector is supplied, the matched iframes will
    /// have an overlay placed over them.
    /// </field>
    /// <field name="opacity" type="Number">
    /// Opacity for the helper while being dragged.
    /// </field>
    /// <field name="refreshPositions" type="Boolean">
    /// If set to true, all droppable positions are calculated on every mousemove.
    /// Caution: This solves issues on highly dynamic pages, but dramatically decreases
    /// performance.
    /// </field>
    /// <field name="revert" type="Object">
    /// Can be boolean or string
    /// If set to true, the element will return to its start position when dragging stops.
    /// Possible string values: 'valid', 'invalid'. If set to invalid, revert will only occur
    /// if the draggable has not been dropped on a droppable. For valid, it's the other
    /// way around.
    /// </field>
    /// <field name="revertDuration" type="Number" integer="true">
    /// The duration of the revert animation, in milliseconds. Ignored if revert is false.
    /// </field>
    /// <field name="scope" type="String">
    /// Used to group sets of draggable and droppable items, in addition to droppable's
    /// accept option. A draggable with the same scope value as a droppable will be accepted
    /// by the droppable.
    /// </field>
    /// <field name="scroll" type="Boolean">
    /// If set to true, container auto-scrolls while dragging.
    /// </field>
    /// <field name="scrollSensitivity" type="Number" integer="true">
    /// Distance in pixels from the edge of the viewport after which the viewport should scroll.
    /// Distance is relative to pointer, not the draggable.
    /// </field>
    /// <field name="scrollSpeed" type="Number" integer="true">
    /// The speed at which the window should scroll once the mouse pointer gets within the
    /// scrollSensitivity distance.
    /// </field>
    /// <field name="snap" type="Object">
    /// Can be boolean or selector
    /// If set to a selector or to true (equivalent to '.ui-draggable'), the draggable will
    /// snap to the edges of the selected elements when near an edge of the element.
    /// </field>
    /// <field name="snapMode" type="String">
    /// Determines which edges of snap elements the draggable will snap to. Ignored if snap
    /// is false. Possible values: 'inner', 'outer', 'both'
    /// </field>
    /// <field name="snapTolerance" type="Number" integer="true">
    /// The distance in pixels from the snap element edges at which snapping should occur.
    /// Ignored if snap is false.
    /// </field>
    /// <field name="stack" type="Object" domElement="true">
    /// Must be a selector.
    /// Controls the z-Index of the set of elements that match the selector, always brings to
    /// front the dragged item. Very useful in things like window managers.
    /// </field>
    /// <field name="zIndex" type="Number" integer="true">
    /// z-index for the helper while being dragged.
    /// </field>
    /// <field name="create" type="js.com.jquery.v3.createDragEvent">
    /// This event is triggered when draggable is created.
    /// </field>
    /// <field name="start" type="js.com.jquery.v3.startDragEvent">
    /// This event is triggered when dragging starts.
    /// </field>
    /// <field name="drag" type="js.com.jquery.v3.dragEvent">
    /// This event is triggered when the mouse is moved during the dragging.
    /// </field>
    /// <field name="stop" type="js.com.jquery.v3.stopDragEvent">
    /// This event is triggered when dragging stops.
    /// </field>
}
js.com.jquery.v3._jQueryUIDraggableOptions.prototype = {
    disabled: false,
    addClasses: true,
    appendTo: null,
    axis: null,
    cancel: null,
    connectToSortable: null,
    containment: null,
    cursor: 'auto',
    cursorAt: false,
    delay: 0,
    distance: 1,
    grid: null,
    handle: null,
    helper: 'original',
    iframeFix: false,
    opacity: 0,
    refreshPositions: false,
    revert: false,
    revertDuration: 500,
    scope: 'default',
    scroll: true,
    scrollSensitivity: 20,
    scrollSpeed: 20,
    snap: false,
    snapMode: 'both',
    snapTolerance: 20,
    stack: null,
    zIndex: 0,
    create: null,
    start: null,
    drag: null,
    stop: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryQTipOptionsEvents

js.com.jquery.v3._jQueryQTipOptionsEvents = function js_com_jquery_v3__jQueryQTipOptionsEvents() {
    /// <field name="render" type="js.com.jquery.v3._jQueryQTipEventHandler">
    /// Fired when the tooltip is rendered
    /// </field>
    /// <field name="show" type="js.com.jquery.v3._jQueryQTipEventHandler">
    /// Fired when the tooltip is shown either by the library itself, or by the user calling the appropriate toggle or show API methods.
    /// </field>
    /// <field name="hide" type="js.com.jquery.v3._jQueryQTipEventHandler">
    /// Fired when the tooltip is hidden either by the library itself, or by the user calling the appropriate toggle or hide API methods.
    /// </field>
    /// <field name="toggle" type="js.com.jquery.v3._jQueryQTipEventHandler">
    /// Fired when the tooltip is toggled i.e. shown or hidden, by the user calling the appropriate toggle or hide API methods. This is a shortut method for binding to both tooltipshow and tooltiphide events above.
    /// </field>
    /// <field name="move" type="js.com.jquery.v3._jQueryQTipEventHandler">
    /// Fired when the tooltip is repositioned, either by the library itself, or when the user calls the reposition API method.
    /// </field>
    /// <field name="focus" type="js.com.jquery.v3._jQueryQTipEventHandler">
    /// Fired when the tooltip is focused i.e. most recently displayed or moused over, either by the library itself or the focus API method.
    /// </field>
    /// <field name="blur" type="js.com.jquery.v3._jQueryQTipEventHandler">
    /// Fired when the tooltip loses focus i.e. another tooltip becomes focused (see above) by the library itself or the focus API method.
    /// </field>
}
js.com.jquery.v3._jQueryQTipOptionsEvents.prototype = {
    render: null,
    show: null,
    hide: null,
    toggle: null,
    move: null,
    focus: null,
    blur: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryQTipOptionsStyle

js.com.jquery.v3._jQueryQTipOptionsStyle = function js_com_jquery_v3__jQueryQTipOptionsStyle() {
    /// <field name="classes" type="String">
    /// A space separated string containing all class names which should be added to the main qTip element.
    /// There are several base styles included in the CSS file provided, including:
    /// </field>
    /// <field name="width" type="Object">
    /// This propery allows you to override all applied CSS width styles for the tooltip. Can be any valid width CSS value. Please note that this does not override max/min width styles! Change those in the CSS file provided.
    /// </field>
    /// <field name="widget" type="Boolean">
    /// Determines whether or not the ui-widget classes of the Themeroller UI styles are applied to your tooltip
    /// </field>
}
js.com.jquery.v3._jQueryQTipOptionsStyle.prototype = {
    classes: null,
    width: null,
    widget: false
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryQTipOptionsShow

js.com.jquery.v3._jQueryQTipOptionsShow = function js_com_jquery_v3__jQueryQTipOptionsShow() {
    /// <field name="target" type="jQuery">
    /// Defines the HTML element(s) which will trigger your specified show.event(s). When set to false, the element the .qtip() method was called upon is used.
    /// </field>
    /// <field name="event" type="String">
    /// Event(s) which will trigger the tooltip to be shown. All possible values are documented under jQuery's Event bind() documentation. Multiple, space separated events are supported.
    /// </field>
    /// <field name="delay" type="Number" integer="true">
    /// Time in milliseconds by which to delay showing of the tooltip when the show.event is triggered on the show.target
    /// </field>
    /// <field name="solo" type="Boolean">
    /// Determines whether or not the tooltip will hide all others when the show.event is triggered on the show.target. If a jQuery object is used as its value, only tooltips found within that object will be hidden.
    /// </field>
    /// <field name="ready" type="Boolean">
    /// Determines whether or not the tooltip is shown once the document is loaded e.g. when the document.ready() event is triggered.
    /// </field>
    /// <field name="effect" type="js.com.jquery.v3._jQueryQTipEffectHandler">
    /// Determines the type of effect that takes place when showing the tooltip. A custom method can also be used whose scope is the tooltip element when called. If set to false, no animation takes place.
    /// </field>
}
js.com.jquery.v3._jQueryQTipOptionsShow.prototype = {
    target: null,
    event: 'mouseenter',
    delay: 140,
    solo: false,
    ready: false,
    effect: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryQTipOptionsHide

js.com.jquery.v3._jQueryQTipOptionsHide = function js_com_jquery_v3__jQueryQTipOptionsHide() {
    /// <field name="target" type="jQuery">
    /// Defines the HTML element(s) which will trigger your specified show.event(s). When set to false, the element the .qtip() method was called upon is used.
    /// </field>
    /// <field name="event" type="String">
    /// Event(s) which will trigger the tooltip to be shown. All possible values are documented under jQuery's Event bind() documentation. Multiple, space separated events are supported.
    /// </field>
    /// <field name="delay" type="Number" integer="true">
    /// Time in milliseconds by which to delay showing of the tooltip when the show.event is triggered on the show.target
    /// </field>
    /// <field name="inactive" type="Number" integer="true">
    /// Time in milliseconds in which the tooltip should be hidden if it remains inactive e.g. isn't interacted with. If set to false, tooltip will not hide when inactive.
    /// </field>
    /// <field name="fixed" type="Boolean">
    /// When set to true, the tooltip will not hide if moused over, allowing the contents to be clicked and interacted with.
    /// </field>
    /// <field name="leave" type="String">
    /// Additional hide setting that allows you to specify whether the tooltip will hide when leaving the window it's contained within. This option requires you to be using either mouseout or mouseleave as (one of) your hide events.
    /// </field>
    /// <field name="distance" type="Number" integer="true">
    /// This setting allows you to determine the distance after which the tooltip hides when the mouse is moved from the point it triggered the tooltip. This is what the regular browser tooltips behave like.
    /// </field>
    /// <field name="effect" type="js.com.jquery.v3._jQueryQTipEffectHandler">
    /// Determines the type of effect that takes place when showing the tooltip. A custom method can also be used whose scope is the tooltip element when called. If set to false, no animation takes place.
    /// </field>
}
js.com.jquery.v3._jQueryQTipOptionsHide.prototype = {
    target: null,
    event: 'mouseleave',
    delay: 0,
    inactive: 0,
    fixed: false,
    leave: null,
    distance: 0,
    effect: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryQTipOptionsAdjust

js.com.jquery.v3._jQueryQTipOptionsAdjust = function js_com_jquery_v3__jQueryQTipOptionsAdjust() {
    /// <field name="x" type="Number" integer="true">
    /// A positive or negative pixel value by which to offset the tooltip in the horizontal plane e.g. the x-axis. Negative values cause a reduction in the value e.g. moves tooltip to the left.
    /// </field>
    /// <field name="y" type="Number" integer="true">
    /// A positive or negative pixel value by which to offset the tooltip in the vertical plane e.g. the y-axis. Negative values cause a reduction in the value e.g. moves tooltip upwards.
    /// </field>
    /// <field name="method" type="String">
    /// This option determines the kind of viewport positioning that takes place.
    /// The default "flip" type basically flips the tooltip when it goes off-screen i.e. from top-right, to bottom-right etc. The "shift" type attempts to keep the tooltip on screen by adjusting only by the amount needed to keep it within the viewport boundaries.
    /// You can specify the behaviour of each axis (i.e. horizontal and vertical) separately, for example a value of "flip none" will cause the tooltip to flip accross the horizontal axis when it extends out the viewport, but do nothing when it extends out the viewport vertically. There are a number of combinations.
    /// </field>
    /// <field name="mouse" type="Boolean">
    /// When the position.target is set to mouse, this option determines whether the tooltip follows the mouse when hovering over the show.target.
    /// </field>
    /// <field name="resize" type="Boolean">
    /// Determines if the tooltips position is adjusted when the window is resized.
    /// </field>
}
js.com.jquery.v3._jQueryQTipOptionsAdjust.prototype = {
    x: 0,
    y: 0,
    method: 'flip',
    mouse: true,
    resize: true
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryQTipOptionsContent

js.com.jquery.v3._jQueryQTipOptionsContent = function js_com_jquery_v3__jQueryQTipOptionsContent() {
    /// <field name="text" type="String">
    /// Text/HTML which will appear inside the tooltip initially. If set to true the title attribute of the target will be used, if available. Can also specify an anonymous function that returns the content, and whose scope is the target element.
    /// </field>
    /// <field name="attr" type="String">
    /// Attribute of the target element to use for content if none is provided with the above content.text option, or no valid content can be found.
    /// </field>
    /// <field name="title" type="js.com.jquery.v3._jQueryQTipOptionsTitle">
    /// </field>
}
js.com.jquery.v3._jQueryQTipOptionsContent.prototype = {
    text: null,
    attr: 'title',
    title: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryQTipOptionsPosition

js.com.jquery.v3._jQueryQTipOptionsPosition = function js_com_jquery_v3__jQueryQTipOptionsPosition() {
    /// <field name="my" type="String">
    /// The corner of the tooltip to position in relation to the position.at. See the Basics section for all possible corner values.
    /// </field>
    /// <field name="at" type="String">
    /// The corner of the position.target element to position the tooltips corner at. See the Basics section for all possible corner values.
    /// </field>
    /// <field name="target" type="Object">
    /// HTML element the tooltip will be positioned in relation to. Can also be set to 'mouse' or the 'event' (position at target that triggered the tooltip), or an array containg an absolute x/y position on the page.
    /// If you also have position.adjust.mouse set to true, the qTip will follow the mouse until a hide event is triggered on the hide target
    /// Can be:
    /// - jQuery Element
    /// - 'mouse'
    /// - Array of [x,y]
    /// - 'false' will show in relation to selector
    /// </field>
    /// <field name="container" type="jQuery">
    /// Determines the HTML element which the tooltip is appended to e.g. it's containing element.
    /// </field>
    /// <field name="viewport" type="Object">
    /// Determines the viewport used to keep the tooltip visible i.e. the element whose boundaries the tooltip must stay visible within at all times if possible. If true it's value will be inherited from the position.container property.
    /// </field>
    /// <field name="effect" type="js.com.jquery.v3._jQueryQTipEffectHandler">
    /// </field>
    /// <field name="adjust" type="js.com.jquery.v3._jQueryQTipOptionsAdjust">
    /// </field>
}
js.com.jquery.v3._jQueryQTipOptionsPosition.prototype = {
    my: null,
    at: null,
    target: false,
    container: null,
    viewport: null,
    effect: null,
    adjust: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryQTipOptionsTitle

js.com.jquery.v3._jQueryQTipOptionsTitle = function js_com_jquery_v3__jQueryQTipOptionsTitle() {
    /// <field name="text" type="String">
    /// Text/HTML which will appear inside the title element of the content. If set to false, no title will be created. An anonymous function can also be used to return the title text, whose scope is the target element.
    /// </field>
    /// <field name="button" type="String">
    /// Text/HTML which will appear inside the title's button element (e.g. close link) located to the right of the title content. The button will close the tooltip when clicked.
    /// </field>
}
js.com.jquery.v3._jQueryQTipOptionsTitle.prototype = {
    text: null,
    button: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryQTipOptions

js.com.jquery.v3._jQueryQTipOptions = function js_com_jquery_v3__jQueryQTipOptions() {
    /// <field name="id" type="String">
    /// A unique string that determines the value of the qTip's "id" attribute that can be used to easily identify this qTip in the document.The attribute is prepended with 'ui-tooltip-'.
    /// </field>
    /// <field name="prerender" type="Boolean">
    /// By default, tooltips are rendered on thier first show event, rather than on page load. Setting this to true will cause tooltips to be created on page load.
    /// </field>
    /// <field name="overwrite" type="Boolean">
    /// </field>
    /// <field name="content" type="js.com.jquery.v3._jQueryQTipOptionsContent">
    /// </field>
    /// <field name="position" type="js.com.jquery.v3._jQueryQTipOptionsPosition">
    /// </field>
    /// <field name="show" type="js.com.jquery.v3._jQueryQTipOptionsShow">
    /// </field>
    /// <field name="hide" type="js.com.jquery.v3._jQueryQTipOptionsHide">
    /// </field>
    /// <field name="events" type="js.com.jquery.v3._jQueryQTipOptionsEvents">
    /// </field>
    /// <field name="style" type="js.com.jquery.v3._jQueryQTipOptionsStyle">
    /// </field>
}
js.com.jquery.v3._jQueryQTipOptions.prototype = {
    id: null,
    prerender: false,
    overwrite: true,
    content: null,
    position: null,
    show: null,
    hide: null,
    events: null,
    style: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryFullCalendarOptionsEventObject

js.com.jquery.v3._jQueryFullCalendarOptionsEventObject = function js_com_jquery_v3__jQueryFullCalendarOptionsEventObject() {
    /// <field name="id" type="String">
    /// String/Integer - Optional
    /// Uniquely identifies the given event. Different instances of repeating events should all have the same id.
    /// </field>
    /// <field name="title" type="String">
    /// String - Required
    /// The text on an event's element
    /// </field>
    /// <field name="allDay" type="Boolean">
    /// Boolean - Optional
    /// Whether an event occurs at a specific time-of-day. This property affects whether an event's time is shown.
    /// Also, in the agenda views, determines if it is displayed in the "all-day" section.
    /// When specifying Event Objects for events or eventSources, omitting this property will make it inherit from
    /// allDayDefault, which is normally true.
    /// </field>
    /// <field name="start" type="Date">
    /// DateTime - Required
    /// The date/time an event begins.
    /// When specifying Event Objects for events or eventSources, you may specify a string in IETF format
    /// (ex: "Wed, 18 Oct 2009 13:00:00 EST"), a string in ISO8601 format (ex: "2009-11-05T13:15:30Z") or
    /// a UNIX timestamp.
    /// </field>
    /// <field name="end" type="Date">
    /// DateTime - Optional
    /// The date/time an event ends.
    /// As with start, you may specify it in IETF, ISO8601, or UNIX timestamp format.
    /// If an event is all-day...
    /// the end date is inclusive. This means an event with start Nov 10 and end Nov 12 will span 3 days on the calendar.
    /// If an event is NOT all-day...
    /// the end date is exclusive. This is only a gotcha when your end has time 00:00. It means your event ends on
    /// midnight, and it will not span through the next day.
    /// </field>
    /// <field name="url" type="String">
    /// String - Optional
    /// A URL that will be visited when this event is clicked by the user. For more information on controlling this
    /// behavior, see the eventClick callback.
    /// </field>
    /// <field name="className" type="String">
    /// String/Array - Optional
    /// A CSS class (or array of classes) that will be attached to this event's element.
    /// </field>
    /// <field name="editable" type="Boolean">
    /// Boolean - Optional
    /// Overrides the master editable option for this single event.
    /// </field>
    /// <field name="source" type="Object">
    /// Event Source Object - Automatically populated
    /// A reference to the event source that this event came from.
    /// </field>
    /// <field name="color" type="String">
    /// String - Optional
    /// Sets an event's background and border color just like the calendar-wide eventColor option.
    /// </field>
    /// <field name="backgroundColor" type="String">
    /// String - Optional
    /// Sets an event's background color just like the calendar-wide eventBackgroundColor option.
    /// </field>
    /// <field name="borderColor" type="String">
    /// String - Optional
    /// Sets an event's border color just like the the calendar-wide eventBorderColor option.
    /// </field>
    /// <field name="textColor" type="String">
    /// String - Optional
    /// Sets an event's text color just like the calendar-wide eventTextColor option.
    /// </field>
}
js.com.jquery.v3._jQueryFullCalendarOptionsEventObject.prototype = {
    id: null,
    title: null,
    allDay: false,
    start: null,
    end: null,
    url: null,
    className: null,
    editable: false,
    source: null,
    color: null,
    backgroundColor: null,
    borderColor: null,
    textColor: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryFullCalendarOptionsViewObject

js.com.jquery.v3._jQueryFullCalendarOptionsViewObject = function js_com_jquery_v3__jQueryFullCalendarOptionsViewObject() {
    /// <field name="name" type="String">
    /// </field>
    /// <field name="title" type="String">
    /// </field>
    /// <field name="start" type="Date">
    /// </field>
    /// <field name="end" type="Date">
    /// </field>
    /// <field name="visStart" type="Date">
    /// </field>
    /// <field name="visEnd" type="Date">
    /// </field>
}
js.com.jquery.v3._jQueryFullCalendarOptionsViewObject.prototype = {
    name: null,
    title: null,
    start: null,
    end: null,
    visStart: null,
    visEnd: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryFullCalendarOptionsbuttonIconsObject

js.com.jquery.v3._jQueryFullCalendarOptionsbuttonIconsObject = function js_com_jquery_v3__jQueryFullCalendarOptionsbuttonIconsObject() {
    /// <field name="prev" type="String">
    /// </field>
    /// <field name="next" type="String">
    /// </field>
}
js.com.jquery.v3._jQueryFullCalendarOptionsbuttonIconsObject.prototype = {
    prev: 'circle-triangle-w',
    next: 'circle-triange-e'
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryFullCalendarOptionsHeader

js.com.jquery.v3._jQueryFullCalendarOptionsHeader = function js_com_jquery_v3__jQueryFullCalendarOptionsHeader() {
    /// <summary>
    /// Each option can contain one or more of these: 'title', 'prev', 'next',
    /// 'prevYear', 'nextYear', 'today', 'month', 'basicWeek', 'basicDay',
    /// 'agendaWeek', 'agendaDay'
    /// </summary>
    /// <field name="left" type="String">
    /// </field>
    /// <field name="center" type="String">
    /// </field>
    /// <field name="right" type="String">
    /// </field>
}
js.com.jquery.v3._jQueryFullCalendarOptionsHeader.prototype = {
    left: 'title',
    center: null,
    right: 'today prev,next'
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryFullCalendarOptionsButtonText

js.com.jquery.v3._jQueryFullCalendarOptionsButtonText = function js_com_jquery_v3__jQueryFullCalendarOptionsButtonText() {
    /// <field name="prev" type="String">
    /// </field>
    /// <field name="next" type="String">
    /// </field>
    /// <field name="prevYear" type="String">
    /// </field>
    /// <field name="nextYear" type="String">
    /// </field>
    /// <field name="today" type="String">
    /// </field>
    /// <field name="month" type="String">
    /// </field>
    /// <field name="week" type="String">
    /// </field>
    /// <field name="day" type="String">
    /// </field>
}
js.com.jquery.v3._jQueryFullCalendarOptionsButtonText.prototype = {
    prev: '&nbsp;&#9668;&nbsp;',
    next: '&nbsp;&#9658;&nbsp;',
    prevYear: '&nbsp;&lt;&lt;&nbsp;',
    nextYear: '&nbsp;&gt;&gt;&nbsp;',
    today: 'today',
    month: 'month',
    week: 'week',
    day: 'day'
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryFullCalendarOptions

js.com.jquery.v3._jQueryFullCalendarOptions = function js_com_jquery_v3__jQueryFullCalendarOptions() {
    /// <field name="header" type="js.com.jquery.v3._jQueryFullCalendarOptionsHeader">
    /// </field>
    /// <field name="buttonText" type="js.com.jquery.v3._jQueryFullCalendarOptionsButtonText">
    /// </field>
    /// <field name="buttonIcons" type="js.com.jquery.v3._jQueryFullCalendarOptionsbuttonIconsObject">
    /// This option only applies to calendars that have jQuery UI theming enabled with the theme option.
    /// A hash must be supplied that maps button names (from the header) to icon strings.
    /// The icon strings determine the CSS class that will be used on the button.
    /// For example, the string 'circle-triangle-w' will result in the class 'ui-icon-triangle-w'.
    /// If a button does not have an entry, it falls back to using buttonText.
    /// If you are using a jQuery UI theme and would prefer not to display any icons and would rather use buttonText instead, you can set
    /// the buttonIcons option to false.
    /// </field>
    /// <field name="theme" type="Boolean">
    /// Once you enable theming with true, you still need to include the CSS file for the theme you want.
    /// </field>
    /// <field name="viewDisplay" type="js.com.jquery.v3.ViewDisplayCallback">
    /// Triggered when the calendar loads and every time a different date-range is displayed.
    /// The calendar's date-range changes whenever the user switches to a new view (for example, if they switch
    /// from "month" to "agendaWeek") or when they click the prev/next buttons.
    /// view is the current View Object.
    /// Within the callback function, this will be set to the calendar's main element.
    /// </field>
    /// <field name="firstDay" type="Number" integer="true">
    /// The value must be a number that represents the day of the week.
    /// Sunday=0, Monday=1, Tuesday=2, etc.
    /// This option is useful for UK users who need the week to start on Monday (1).
    /// </field>
    /// <field name="isRTL" type="Boolean">
    /// Displays the calendar in right-to-left mode.
    /// This option is useful for right-to-left languages such as Arabic and Hebrew.
    /// </field>
    /// <field name="weekends" type="Boolean">
    /// Whether to include Saturday/Sunday columns in any of the calendar views
    /// </field>
    /// <field name="weekMode" type="String">
    /// Determines the number of weeks displayed in a month view. Also determines each week's height.
    /// 'fixed' - The calendar will always be 6 weeks tall. The height will always be the same, as determined by height, contentHeight, or aspectRatio.
    /// 'liquid' - The calendar will have either 4, 5, or 6 weeks, depending on the month. The height of the weeks will stretch to fill the available
    /// height, as determined by height, contentHeight, or aspectRatio.
    /// 'variable' - The calendar will have either 4, 5, or 6 weeks, depending on the month. Each week will have the same constant height,
    /// meaning the calendar�s height will change month-to-month.
    /// </field>
    /// <field name="height" type="Number" integer="true">
    /// Will make the entire calendar (including header) a pixel height.
    /// By default, this option is unset and the calendar's height is calculated by aspectRatio.
    /// </field>
    /// <field name="contentHeight" type="Number" integer="true">
    /// Will make the calendar's content area a pixel height.
    /// By default, this option is unset and the calendar's height is calculated by aspectRatio.
    /// </field>
    /// <field name="aspectRatio" type="Number">
    /// Determines the width-to-height aspect ratio of the calendar.
    /// A calendar is a block-level element that fills its entire avaiable width. The calendar�s height, however, is determined by
    /// this ratio of width-to-height. (Hint: larger numbers make smaller heights).
    /// </field>
    /// <field name="windowResize" type="js.com.jquery.v3.WindowResizeCallBack">
    /// Triggered after the calendar's dimensions have been changed due to the browser window being resized.
    /// The calendar has automatically adapted to the new size when windowResize is triggered.
    /// view is the current View Object.
    /// Within the callback function, this will be set to the calendar's main element.
    /// </field>
    /// <field name="defaultView" type="String">
    /// The initial view when the calendar loads.
    /// Can take any of these: 'month', 'basicWeek', 'basicDay', 'agendaWeek', 'agendaDay'
    /// </field>
    /// <field name="allDaySlot" type="Boolean">
    /// Determines if the "all-day" slot is displayed at the top of the calendar.
    /// When hidden with false, all-day events will not be displayed in agenda views.
    /// </field>
    /// <field name="allDayText" type="String">
    /// The text titling the "all-day" slot at the top of the calendar.
    /// </field>
    /// <field name="axisFormat" type="String">
    /// Determines the time-text that will be displayed on the vertical axis of the agenda views.
    /// The value is a format-string that will be processed by formatDate.
    /// The default value will produce times that look like "5pm" and "5:30pm".
    /// </field>
    /// <field name="slotMinutes" type="Number" integer="true">
    /// The frequency for displaying time slots, in minutes.
    /// The default will make a slot every half hour.
    /// </field>
    /// <field name="defaultEventMinutes" type="Number" integer="true">
    /// Determines the length (in minutes) an event appears to be when it has an unspecified end date.
    /// By default, if an Event Object as no end, it will appear to be 2 hours.
    /// This option only affects events that appear in the agenda slots, meaning they have allDay set to true.
    /// </field>
    /// <field name="firstHour" type="Number" integer="true">
    /// Determines the first hour that will be visible in the scroll pane.
    /// Values must be from 0-23, where 0=midnight, 1=1am, etc.
    /// The user will be able to scroll upwards to see events before this time. If you want to prevent users from doing this, use
    /// the minTime option instead.
    /// </field>
    /// <field name="minTime" type="Object">
    /// Determines the first hour/time that will be displayed, even when the scrollbars have been scrolled all the way up.
    /// This can be a number like 5 (which means 5am), a string like '5:30' (which means 5:30am) or a string like '5:30am'.
    /// </field>
    /// <field name="maxTime" type="Object">
    /// Determines the last hour/time (exclusively) that will be displayed, even when the scrollbars have been scrolled all the way down.
    /// This can be a number like 22 (which means 10pm), a string like '22:30' (which means 10:30pm) or a string like '10:30pm'.
    /// </field>
    /// <field name="year" type="Number" integer="true">
    /// The initial year when the calendar loads.
    /// Must be a 4-digit year like 2009.
    /// If year is unspecified, the calendar will begin at the current year.
    /// </field>
    /// <field name="month" type="Number" integer="true">
    /// The initial month when the calendar loads.
    /// IMPORTANT: The value is 0-based, meaning January=0, February=1, etc.
    /// If month is unspecified and year is set to the current year, the calendar will start on the current month.
    /// If month is unspecified and year is not set to the current year, the calendar will start on January.
    /// </field>
    /// <field name="date" type="Number" integer="true">
    /// </field>
    /// <field name="timeFormat" type="String">
    /// Determines the time-text that will be displayed on each event.
    /// A single format string will change the time-text for events in all
    /// views. A View Option Hash may be provided to target specific views (this is what the default does).
    /// Uses formatDate/formatDates formatting rules.
    /// Time-text will only be displayed for Event Objects that have allDay equal to false.
    /// </field>
    /// <field name="columnFormat" type="String">
    /// Determines the text that will be displayed on the calendar's column headings.
    /// A single string will set the title format for all views. A View Option Hash may be provided
    /// to target specific views (this is what the default does).
    /// Uses formatDate/formatDates formatting rules.
    /// </field>
    /// <field name="titleFormat" type="String">
    /// Determines the text that will be displayed in the header's title.
    /// A single string will set the title format for all views. A View Option Hash may be provided
    /// to target specific views (this is what the default does).
    /// Uses formatDate/formatDates formatting rules.
    /// </field>
    /// <field name="monthNames" type="Object">
    /// Full names of months. [Array]
    /// ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    /// </field>
    /// <field name="monthNamesShort" type="Object">
    /// Abbreviated names of months. [Array]
    /// ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    /// </field>
    /// <field name="dayNames" type="Object">
    /// Full names of days-of-week [Array]
    /// ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    /// </field>
    /// <field name="dayNamesShort" type="Object">
    /// Abbreviated names of days-of-week. [Array]
    /// ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    /// </field>
    /// <field name="dayClick" type="js.com.jquery.v3.DayClickCallback">
    /// </field>
    /// <field name="eventClick" type="js.com.jquery.v3.EventClickCallback">
    /// </field>
    /// <field name="eventMouseover" type="js.com.jquery.v3.EventMouseOverCallback">
    /// </field>
    /// <field name="eventMouseOut" type="js.com.jquery.v3.EventMouseOutCallback">
    /// </field>
    /// <field name="selectable" type="Boolean">
    /// Allows a user to highlight multiple days or timeslots by clicking and dragging.
    /// To let the user make selections by clicking and dragging, this option must be set to true.
    /// The select and unselect callbacks will be useful for monitoring when selections are made and cleared.
    /// To learn the ways in which selections can be cleared, read the docs for the unselect callback.
    /// </field>
    /// <field name="selectHelper" type="Boolean">
    /// Whether to draw a "placeholder" event while the user is dragging.
    /// This option only applies to the agenda views.
    /// A value of true will draw a "placeholder" event while the user is dragging (similar to what Google
    /// Calendar does for its week and day views). A value of false (the default) will draw the standard
    /// highlighting over each cell.
    /// A function can also be specified for drawing custom elements. It will be given 2 arguments: the
    /// selection's start date and end date (Date objects). It must return a DOM element that will be used.
    /// </field>
    /// <field name="unselectAuto" type="Boolean">
    /// Whether clicking elsewhere on the page will cause the current selection to be cleared.
    /// This option can only take effect when selectable is set to true.
    /// </field>
    /// <field name="unselectCancel" type="String">
    /// A way to specify elements that will ignore the unselectAuto option.
    /// Clicking on elements that match this jQuery selector will prevent the current selection from being cleared
    /// (due to the unselectAuto option).
    /// This option is useful if you have a "Create an event" form that shows up in response to the user making a
    /// selection. When the user clicks on this form, you probably don't want to the current selection to go away.
    /// Thus, you should add a class to your form such as "my-form", and set the unselectCancel option to ".my-form".
    /// </field>
    /// <field name="select" type="js.com.jquery.v3.SelectCallback">
    /// A callback that will fire after a selection is made.
    /// startDate is a Date object indicating the beginning of the selection.
    /// endDate is a Date object indicating the end of the selection. It follows the same rules as the Event Object
    /// for inclusivity/exclusivity: when allDay is true, endDate includes the last day.
    /// allDay is a boolean indicating if entire days were selected (days in month view or the "all-day" slot in the
    /// agenda view) or time slots were selected.
    /// jsEvent holds the primitive JavaScript event with information such as mouse coordinates. If select has been
    /// triggered via the select method, jsEvent will be undefined.
    /// </field>
    /// <field name="unselect" type="js.com.jquery.v3.UnselectCallback">
    /// A callback that will fire when the current selection is cleared.
    /// A selection might be cleared for a number of reasons:
    /// 1) The user clicks away from the current selection (doesn't happen when unselectAuto is false).
    /// 2) The user makes a new selection. The unselect callback will be fired before the new selection occurs.
    /// 3) The user moves forward or backward in the current view, or switches to a new view.
    /// 4) The unselect method is called through the API.
    /// jsEvent holds the primitive JavaScript event with information such as mouse coordinates. If unselect has been triggered via the unselect method, jsEvent will be undefined.
    /// </field>
    /// <field name="events" type="Object">
    /// This can be an array of events, a json feed or a function. Refer to documentation for more info.
    /// http://arshaw.com/fullcalendar/docs/event_data/events_array/
    /// </field>
    /// <field name="allDayDefault" type="Boolean">
    /// Determines the default value for each Event Object's allDay property, when it is unspecified.
    /// </field>
    /// <field name="ignoreTimezone" type="Boolean">
    /// When parsing ISO8601 dates, whether UTC offsets should be ignored while processing event source data.
    /// The default is true, which means the UTC offset for all ISO8601 dates will be ignored. For example, the
    /// date "2008-11-05T08:15:30-05:00" will be processed as November 5th, 2008 at 8:15am in the local offset of
    /// the browser.
    /// If you are using ISO8601 dates with UTC offsets, chances are you want them processed. You must set this
    /// option to false. In the future, the default for this option will probably be changed to false.
    /// </field>
    /// <field name="startParam" type="String">
    /// A GET parameter of this name will be inserted into each JSON feed's URL.
    /// The value of this GET parameter will be a UNIX timestamp denoting the start of the first visible day (inclusive).
    /// </field>
    /// <field name="endParam" type="String">
    /// A GET parameter of this name will be inserted into each JSON feed's URL.
    /// The value of this GET parameter will be a UNIX timestamp denoting the end of the last visible day (exclusive).
    /// </field>
    /// <field name="lazyFetching" type="Boolean">
    /// Determines when event fetching should occur
    /// When set to true (the default), the calendar will only fetch events when it absolutely needs to, minimizing AJAX
    /// calls. For example, say your calendar starts out in month view, in February. FullCalendar will fetch events for
    /// the entire month of February and store them in its internal cache. Then, say the user switches to week view and
    /// begins browsing the weeks in February. The calendar will avoid fetching events because it already has this
    /// information stored.
    /// When set to false, the calendar will fetch events any time the view is switched, or any time the current date
    /// changes (for example, as a result of the user clicking prev/next).
    /// </field>
    /// <field name="loading" type="js.com.jquery.v3.LoadingCallback">
    /// Triggered when event fetching starts/stops.
    /// Triggered with a true argument when the calendar begins fetching events via AJAX. Triggered with false when done.
    /// view is the current View Object.
    /// This function is often used to show/hide a loading indicator.
    /// </field>
    /// <field name="eventColor" type="String">
    /// Sets the background and border colors for all events on the calendar.
    /// You can use any of the CSS color formats such #f00, #ff0000, rgb(255,0,0), or red.
    /// This option can be overridden on a per-source basis with the color Event Source Object option or on a per-event
    /// basis with the color Event Object option.
    /// </field>
    /// <field name="eventBackgroundColor" type="String">
    /// Sets the background color for all events on the calendar.
    /// You can use any of the CSS color formats such #f00, #ff0000, rgb(255,0,0), or red.
    /// This option can be overridden on a per-source basis with the backgroundColor Event
    /// Source Object option or on a per-event basis with the backgroundColor Event Object option.
    /// </field>
    /// <field name="eventBorderColor" type="String">
    /// Sets the border color for all events on the calendar.
    /// You can use any of the CSS color formats such #f00, #ff0000, rgb(255,0,0), or red.
    /// This option can be overridden on a per-source basis with the borderColor Event Source
    /// Object option or on a per-event basis with the borderColor Event Object option.
    /// </field>
    /// <field name="eventTextColor" type="String">
    /// Sets the text color for all events on the calendar.
    /// You can use any of the CSS color formats such #f00, #ff0000, rgb(255,0,0), or red.
    /// This option can be overridden on a per-source basis with the textColor Event Source
    /// Object option or on a per-event basis with the textColor Event Object option.
    /// </field>
    /// <field name="eventRender" type="js.com.jquery.v3.eventRenderCallback">
    /// </field>
    /// <field name="eventAfterRender" type="js.com.jquery.v3.eventAfterRenderCallback">
    /// Triggered after an event has been placed on the calendar in its final position.
    /// </field>
    /// <field name="editable" type="Boolean">
    /// Determines whether the events on the calendar can be modified.
    /// This determines if the events can be dragged and resized. Enables/disables both at the
    /// same time. If you don't want both, use editable in conjunction with disableDragging
    /// and disableResizing.
    /// This option can be overridden on a per-event basis with the Event Object editable property.
    /// </field>
    /// <field name="disableDragging" type="Boolean">
    /// Disables all event dragging, even when events are editable.
    /// </field>
    /// <field name="disableResizing" type="Boolean">
    /// Disables all event resizing, even when events are editable.
    /// </field>
    /// <field name="dragRevertDuration" type="Number" integer="true">
    /// Time it takes for an event to revert to its original position after an unsuccessful drag.
    /// Time is in milliseconds (1 second = 1000 milliseconds).
    /// </field>
    /// <field name="dragOpacity" type="Number">
    /// The opacity of an event while it is being dragged.
    /// Float values range from 0.0 to 1.0.
    /// Specify a single number to affect all views, or a View Option Hash to target specific views
    /// (which is what the default does).
    /// </field>
    /// <field name="eventDragStart" type="js.com.jquery.v3.eventDragStartCallback">
    /// Triggered when event dragging begins.
    /// </field>
    /// <field name="eventDragStop" type="js.com.jquery.v3.eventDragStopCallback">
    /// Triggered when event dragging stops.
    /// </field>
    /// <field name="eventDrop" type="js.com.jquery.v3.eventDropCallback">
    /// Triggered when dragging stops and the event has moved to a different day/time.
    /// event is an Event Object that hold the event's information (date, title, etc).
    /// dayDelta holds the number of days the event was moved forward (a positive number) or backwards (a negative number).
    /// minuteDelta holds the number of minutes the event was moved forward (a positive number) or backwards (a negative number).
    /// Only useful for the agenda views. In other views, 0 is passed in.
    /// dayDelta and minuteDelta are elegant for dealing with multi-day and repeating events. If updating a remote database,
    /// just add these values to the start and end times of all events with the given event.id.
    /// allDay is true if the event has been dropped on a day in month view, or the "all-day" slot in the agenda views. It
    /// will be false if dropped on a slot in the agenda views (meaning it has been assigned a time).
    /// revertFunc is a function that, if called, reverts the event's start/end date to the values before the drag. This is
    /// useful if an ajax call should fail.
    /// jsEvent holds the native JavaScript event with low-level information such as mouse coordinates.
    /// ui holds the jQuery UI object.
    /// view holds the current View Object.
    /// </field>
    /// <field name="eventResizeStart" type="js.com.jquery.v3.eventResizeStartCallback">
    /// Triggered when event resizing begins.
    /// </field>
    /// <field name="eventResizeStop" type="js.com.jquery.v3.eventResizeStopCallback">
    /// Triggered when event resizing stops.
    /// </field>
    /// <field name="eventResize" type="js.com.jquery.v3.eventResizeCallback">
    /// Triggered when resizing stops and the event has changed in duration.
    /// event is an Event Object that hold the event's information (date, title, etc).
    /// dayDelta holds the number of days the event's end date was moved forward (a positive number) or
    /// backwards (a negative number).
    /// minuteDelta holds the number of minutes the event's end time was moved forward (a positive number)
    /// or backwards (a negative number). Only useful for the agenda views. In other views, 0 is passed in.
    /// dayDelta and minuteDelta are elegant for dealing with multi-day and repeating events. If updating a
    /// remote database, just add these values to the end of all events with the given event.id.
    /// revertFunc is a function that, if called, reverts the event's end date to the value before the drag.
    /// This is useful if an ajax call should fail.
    /// jsEvent holds the native javascript event with low-level information such as mouse coordinates.
    /// ui holds the jQuery UI object.
    /// view holds the current View Object.
    /// </field>
    /// <field name="droppable" type="Boolean">
    /// Determines if jQuery UI draggables can be dropped onto the calendar.
    /// </field>
    /// <field name="dropAccept" type="Object">
    /// Type: string/function
    /// Provides a way to filter which elements can be dropped onto the calendar.
    /// By default, after setting a calendar' droppable option to true, the calendar will accept any draggables
    /// that are dropped onto the calendar. The dropAccept option allows the calendar be more selective about
    /// which elements can/can't be dropped.
    /// The value of dropAccept can be a string jQuery selector. It can also be a function that accepts the
    /// draggable item as a single argument, and returns true if the element can be dropped onto the calendar.
    /// </field>
    /// <field name="drop" type="js.com.jquery.v3.dropCallback">
    /// Called when a valid jQuery UI draggable has been dropped onto the calendar.
    /// date holds the JavaScript Date object of where the draggable was dropped.
    /// allDay holds a boolean of whether the draggable was dropped on an all-day cell (like in month view) or
    /// in a slot with a specific time (like in agenda view).
    /// jsEvent holds the primitive JavaScript event, with information like mouse coordinates.
    /// ui holds the jQuery UI information.
    /// this holds the DOM element that has been dropped.
    /// </field>
    this.aspectRatio = parseFloat('1.35');
}
js.com.jquery.v3._jQueryFullCalendarOptions.prototype = {
    header: null,
    buttonText: null,
    buttonIcons: null,
    theme: false,
    viewDisplay: null,
    firstDay: 0,
    isRTL: false,
    weekends: false,
    weekMode: 'fixed',
    height: 0,
    contentHeight: 0,
    windowResize: null,
    defaultView: 'month',
    allDaySlot: true,
    allDayText: 'all-day',
    axisFormat: 'h(:mm)tt',
    slotMinutes: 30,
    defaultEventMinutes: 120,
    firstHour: 6,
    minTime: null,
    maxTime: null,
    year: 0,
    month: 0,
    date: 0,
    timeFormat: null,
    columnFormat: null,
    titleFormat: null,
    monthNames: null,
    monthNamesShort: null,
    dayNames: null,
    dayNamesShort: null,
    dayClick: null,
    eventClick: null,
    eventMouseover: null,
    eventMouseOut: null,
    selectable: false,
    selectHelper: false,
    unselectAuto: true,
    unselectCancel: '',
    select: null,
    unselect: null,
    events: null,
    allDayDefault: true,
    ignoreTimezone: true,
    startParam: 'start',
    endParam: 'end',
    lazyFetching: true,
    loading: null,
    eventColor: null,
    eventBackgroundColor: null,
    eventBorderColor: null,
    eventTextColor: null,
    eventRender: null,
    eventAfterRender: null,
    editable: false,
    disableDragging: false,
    disableResizing: false,
    dragRevertDuration: 500,
    dragOpacity: 0,
    eventDragStart: null,
    eventDragStop: null,
    eventDrop: null,
    eventResizeStart: null,
    eventResizeStop: null,
    eventResize: null,
    droppable: false,
    dropAccept: '*',
    drop: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryMultiSelectParams

js.com.jquery.v3._jQueryMultiSelectParams = function js_com_jquery_v3__jQueryMultiSelectParams() {
    /// <field name="sortable" type="Boolean">
    /// </field>
    /// <field name="searchable" type="Boolean">
    /// </field>
    /// <field name="doubleClickable" type="Boolean">
    /// </field>
    /// <field name="animated" type="String">
    /// </field>
    /// <field name="show" type="String">
    /// </field>
    /// <field name="hide" type="String">
    /// </field>
    /// <field name="dividerLocation" type="Number">
    /// </field>
    /// <field name="nodeComparator" type="js.com.jquery.v3.JqueryMultiSelectNodeComparer">
    /// </field>
}
js.com.jquery.v3._jQueryMultiSelectParams.prototype = {
    sortable: false,
    searchable: false,
    doubleClickable: false,
    animated: null,
    show: null,
    hide: null,
    dividerLocation: 0,
    nodeComparator: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryFX

js.com.jquery.v3._jQueryFX = function js_com_jquery_v3__jQueryFX() {
    /// <field name="elem" type="Object" domElement="true">
    /// </field>
    /// <field name="prop" type="String">
    /// </field>
}
js.com.jquery.v3._jQueryFX.prototype = {
    elem: null,
    prop: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jAnimateExtraParams

js.com.jquery.v3._jAnimateExtraParams = function js_com_jquery_v3__jAnimateExtraParams() {
    /// <field name="duration" type="Number" integer="true">
    /// Milliseconds
    /// </field>
    /// <field name="step" type="js.com.jquery.v3.AnimateStepHandler">
    /// A function to be called after each step of the animation.
    /// </field>
    /// <field name="complete" type="Action">
    /// A function to call once the animation is complete.
    /// </field>
    /// <field name="easing" type="String">
    /// A string indicating which easing function to use for the transition. Inbuilt: 'linear' or 'swing'
    /// </field>
    /// <field name="queue" type="Boolean">
    /// A Boolean indicating whether to place the animation in the effects queue. If false, the animation will begin immediately.
    /// </field>
}
js.com.jquery.v3._jAnimateExtraParams.prototype = {
    duration: 0,
    step: null,
    complete: null,
    easing: null,
    queue: false
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryUIDatePickerInstance

js.com.jquery.v3._jQueryUIDatePickerInstance = function js_com_jquery_v3__jQueryUIDatePickerInstance() {
    /// <field name="currentDay" type="Number" integer="true">
    /// </field>
    /// <field name="currentMonth" type="Number" integer="true">
    /// </field>
    /// <field name="currentYear" type="Number" integer="true">
    /// </field>
    /// <field name="dpDiv" type="jQuery">
    /// </field>
    /// <field name="drawMonth" type="Number" integer="true">
    /// </field>
    /// <field name="drawYear" type="Number" integer="true">
    /// </field>
    /// <field name="id" type="String">
    /// </field>
    /// <field name="inline" type="Boolean">
    /// </field>
    /// <field name="input" type="jQuery">
    /// </field>
    /// <field name="lastVal" type="String">
    /// </field>
    /// <field name="selectedDay" type="Number" integer="true">
    /// </field>
    /// <field name="selectedMonth" type="Number" integer="true">
    /// </field>
    /// <field name="selectedYear" type="Number" integer="true">
    /// </field>
    /// <field name="settings" type="js.com.jquery.v3._jQueryUIDatePickerOptions">
    /// </field>
    /// <field name="trigger" type="jQuery">
    /// </field>
    /// <field name="yearshtml" type="String">
    /// </field>
}
js.com.jquery.v3._jQueryUIDatePickerInstance.prototype = {
    currentDay: 0,
    currentMonth: 0,
    currentYear: 0,
    dpDiv: null,
    drawMonth: 0,
    drawYear: 0,
    id: null,
    inline: false,
    input: null,
    lastVal: null,
    selectedDay: 0,
    selectedMonth: 0,
    selectedYear: 0,
    settings: null,
    trigger: null,
    yearshtml: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryUIDatePickerOptions

js.com.jquery.v3._jQueryUIDatePickerOptions = function js_com_jquery_v3__jQueryUIDatePickerOptions() {
    /// <field name="disabled" type="Boolean">
    /// </field>
    /// <field name="altField" type="Object">
    /// </field>
    /// <field name="altFormat" type="String">
    /// </field>
    /// <field name="appendText" type="String">
    /// </field>
    /// <field name="autoSize" type="Boolean">
    /// </field>
    /// <field name="buttonImage" type="String">
    /// </field>
    /// <field name="buttonImageOnly" type="Boolean">
    /// </field>
    /// <field name="buttonText" type="String">
    /// </field>
    /// <field name="calculateWeek" type="Function">
    /// </field>
    /// <field name="changeMonth" type="Boolean">
    /// </field>
    /// <field name="changeYear" type="Boolean">
    /// </field>
    /// <field name="closeText" type="String">
    /// </field>
    /// <field name="constrainInput" type="Boolean">
    /// </field>
    /// <field name="currentText" type="String">
    /// </field>
    /// <field name="dateFormat" type="String">
    /// </field>
    /// <field name="dayNames" type="Array">
    /// </field>
    /// <field name="dayNamesMin" type="Array">
    /// </field>
    /// <field name="dayNamesShort" type="Array">
    /// </field>
    /// <field name="defaultDate" type="Object">
    /// </field>
    /// <field name="string" type="Object">
    /// </field>
    /// <field name="firstDay" type="Number">
    /// </field>
    /// <field name="gotoCurrent" type="Boolean">
    /// </field>
    /// <field name="hideIfNoPrevNext" type="Boolean">
    /// </field>
    /// <field name="isRTL" type="Boolean">
    /// </field>
    /// <field name="minDate" type="Object">
    /// </field>
    /// <field name="maxDate" type="Object">
    /// </field>
    /// <field name="date" type="Object">
    /// </field>
    /// <field name="monthNames" type="Array">
    /// </field>
    /// <field name="monthNamesShort" type="Array">
    /// </field>
    /// <field name="navigationAsDateFormat" type="Boolean">
    /// </field>
    /// <field name="nextText" type="String">
    /// </field>
    /// <field name="number" type="Object">
    /// </field>
    /// <field name="prevText" type="String">
    /// </field>
    /// <field name="selectOtherMonths" type="Boolean">
    /// </field>
    /// <field name="shortYearCutoff" type="String">
    /// </field>
    /// <field name="showAnim" type="String">
    /// </field>
    /// <field name="showButtonPanel" type="Boolean">
    /// </field>
    /// <field name="showCurrentAtPos" type="Number">
    /// </field>
    /// <field name="showMonthAfterYear" type="Boolean">
    /// </field>
    /// <field name="showOn" type="String">
    /// </field>
    /// <field name="showOptions" type="Object">
    /// </field>
    /// <field name="showOtherMonths" type="Boolean">
    /// </field>
    /// <field name="showWeek" type="Boolean">
    /// </field>
    /// <field name="stepMonths" type="Number">
    /// </field>
    /// <field name="weekHeader" type="String">
    /// </field>
    /// <field name="yearRange" type="String">
    /// </field>
    /// <field name="yearSuffix" type="String">
    /// </field>
    /// <field name="create" type="js.com.jquery.v3.DatePickerCreateHandler">
    /// </field>
    /// <field name="beforeShow" type="js.com.jquery.v3.DatePickerBeforeShowHandler">
    /// </field>
    /// <field name="beforeShowDay" type="js.com.jquery.v3.DatePickerBeforeShowHandler">
    /// </field>
    /// <field name="onChangeMonthYear" type="js.com.jquery.v3.DatePickerOnChangeMonthYearHandler">
    /// </field>
    /// <field name="onClose" type="js.com.jquery.v3.DatePickerEventHandler">
    /// </field>
    /// <field name="onSelect" type="js.com.jquery.v3.DatePickerEventHandler">
    /// </field>
}
js.com.jquery.v3._jQueryUIDatePickerOptions.prototype = {
    disabled: false,
    altField: null,
    altFormat: null,
    appendText: null,
    autoSize: false,
    buttonImage: null,
    buttonImageOnly: false,
    buttonText: null,
    calculateWeek: null,
    changeMonth: false,
    changeYear: false,
    closeText: null,
    constrainInput: false,
    currentText: null,
    dateFormat: null,
    dayNames: null,
    dayNamesMin: null,
    dayNamesShort: null,
    defaultDate: null,
    string: null,
    firstDay: null,
    gotoCurrent: false,
    hideIfNoPrevNext: false,
    isRTL: false,
    minDate: null,
    maxDate: null,
    date: null,
    monthNames: null,
    monthNamesShort: null,
    navigationAsDateFormat: false,
    nextText: null,
    number: null,
    prevText: null,
    selectOtherMonths: false,
    shortYearCutoff: null,
    showAnim: null,
    showButtonPanel: false,
    showCurrentAtPos: null,
    showMonthAfterYear: false,
    showOn: 'both',
    showOptions: null,
    showOtherMonths: false,
    showWeek: false,
    stepMonths: null,
    weekHeader: null,
    yearRange: null,
    yearSuffix: null,
    create: null,
    beforeShow: null,
    beforeShowDay: null,
    onChangeMonthYear: null,
    onClose: null,
    onSelect: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._JScrollPaneOptions

js.com.jquery.v3._JScrollPaneOptions = function js_com_jquery_v3__JScrollPaneOptions() {
    /// <field name="showArrows" type="Boolean">
    /// </field>
    /// <field name="maintainPosition" type="Boolean">
    /// </field>
    /// <field name="clickOnTrack" type="Boolean">
    /// </field>
    /// <field name="autoReinitialise" type="Boolean">
    /// </field>
    /// <field name="autoReinitialiseDelay" type="Number" integer="true">
    /// </field>
    /// <field name="verticalDragMinHeight" type="Number" integer="true">
    /// </field>
    /// <field name="verticalDragMaxHeight" type="Number" integer="true">
    /// </field>
    /// <field name="horizontalDragMinWidth" type="Number" integer="true">
    /// </field>
    /// <field name="horizontalDragMaxWidth" type="Number" integer="true">
    /// </field>
    /// <field name="animateScroll" type="Boolean">
    /// </field>
    /// <field name="animateDuration" type="Number" integer="true">
    /// </field>
    /// <field name="animateEase" type="String">
    /// </field>
    /// <field name="hijackInternalLinks" type="Boolean">
    /// </field>
    /// <field name="verticalGutter" type="Number" integer="true">
    /// </field>
    /// <field name="horizontalGutter" type="Number" integer="true">
    /// </field>
    /// <field name="mouseWheelSpeed" type="Number" integer="true">
    /// </field>
    /// <field name="arrowButtonSpeed" type="Number" integer="true">
    /// </field>
    /// <field name="arrowRepeatFreq" type="Number" integer="true">
    /// </field>
    /// <field name="arrowScrollOnHover" type="Boolean">
    /// </field>
    /// <field name="trackClickSpeed" type="Number" integer="true">
    /// </field>
    /// <field name="trackClickRepeatFreq" type="Number" integer="true">
    /// </field>
    /// <field name="verticalArrowPositions" type="String">
    /// </field>
    /// <field name="horizontalArrowPositions" type="String">
    /// </field>
    /// <field name="enableKeyboardNavigation" type="Boolean">
    /// </field>
    /// <field name="hideFocus" type="Boolean">
    /// </field>
}
js.com.jquery.v3._JScrollPaneOptions.prototype = {
    showArrows: false,
    maintainPosition: false,
    clickOnTrack: false,
    autoReinitialise: false,
    autoReinitialiseDelay: 0,
    verticalDragMinHeight: 0,
    verticalDragMaxHeight: 0,
    horizontalDragMinWidth: 0,
    horizontalDragMaxWidth: 0,
    animateScroll: false,
    animateDuration: 0,
    animateEase: null,
    hijackInternalLinks: false,
    verticalGutter: 0,
    horizontalGutter: 0,
    mouseWheelSpeed: 0,
    arrowButtonSpeed: 0,
    arrowRepeatFreq: 0,
    arrowScrollOnHover: false,
    trackClickSpeed: 0,
    trackClickRepeatFreq: 0,
    verticalArrowPositions: null,
    horizontalArrowPositions: null,
    enableKeyboardNavigation: false,
    hideFocus: false
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jEditableSettings

js.com.jquery.v3._jEditableSettings = function js_com_jquery_v3__jEditableSettings() {
    /// <field name="indicator" type="String">
    /// HTML to show as indicator.  Can be an image or just text.
    /// </field>
    /// <field name="tooltip" type="String">
    /// </field>
    /// <field name="type" type="String">
    /// Input type to use. Default input types are text, textarea  or select. Additional input types are provided using custom input type API
    /// </field>
    /// <field name="cancel" type="String">
    /// </field>
    /// <field name="submit" type="String">
    /// </field>
    /// <field name="id" type="String">
    /// Name of the submitted parameter which contains content id. Default is id.
    /// </field>
    /// <field name="name" type="String">
    /// Name of the submitted parameter which contains edited content. Default is value.
    /// </field>
    /// <field name="loadurl" type="String">
    /// The URL to load select values from instead of using the data.
    /// Normally content of the form will be same as content of the edited element. However using this parameter you can load form content from external URL.
    /// </field>
    /// <field name="cssclass" type="String">
    /// CSS class with input.  If you set it to 'inherit' it will take same class as parent.
    /// </field>
    /// <field name="style" type="String">
    /// Inline style to be added.  If you set it to 'inherit' it will take same style as parent.
    /// </field>
    /// <field name="callback" type="js.com.jquery.v3.JEditableHandler">
    /// Function is called after form has been submitted.
    /// Callback function receives two parameters.
    /// Value contains submitted form content.
    /// Settings contain all plugin settings.
    /// Inside function this  refers to the original element.
    /// </field>
    /// <field name="submitdata" type="Object">
    /// Extra parameters when submitting content. Can be either a hash or function returning a hash.
    /// </field>
    /// <field name="rows" type="Number" integer="true">
    /// Number of rows if using textarea.
    /// </field>
    /// <field name="cols" type="Number" integer="true">
    /// Number of columns if using textarea.
    /// </field>
    /// <field name="height" type="String">
    /// Height of the input element in pixels. Default is auto. This means height is calculated automatically. Can also be set to none.
    /// </field>
    /// <field name="width" type="String">
    /// Width of the input element in pixels. Default is auto. This means width is calculated automatically. Can also be set to none.
    /// </field>
    /// <field name="loadtype" type="String">
    /// Request type to use when using loadurl. Default is GET. You most likely want to use only GET or POST.
    /// </field>
    /// <field name="loaddata" type="Object">
    /// Extra parameter to add to request when using loadurl. Can be either a hash or function returning a hash.
    /// </field>
    /// <field name="data" type="js.com.jquery.v3.JEditableDataHandler">
    /// Form data passed as parameter. Can be either a string or function returning a string. Can be useful when you need to alter the text before editing.
    /// </field>
    /// <field name="onblur" type="String">
    /// Default action of when user clicks outside of editable area is to cancel edits. You can control this by setting onblur option. Possible values are:
    /// cancel Clicking outside editable area cancels changes. Clicking submit button submits changes.
    /// submit Clicking outside editable area submits changes.
    /// ignore Click outside editable area is ignored. Pressing ESC cancels changes. Clicking submit button submits changes.
    /// </field>
    /// <field name="onsubmit" type="js.com.jquery.v3.JEditableEventHandler">
    /// called before submit
    /// </field>
    /// <field name="onreset" type="js.com.jquery.v3.JEditableEventHandler">
    /// called before reset
    /// </field>
    /// <field name="onerror" type="js.com.jquery.v3.JEditableErrorEventHandler">
    /// called before error
    /// </field>
}
js.com.jquery.v3._jEditableSettings.prototype = {
    indicator: null,
    tooltip: null,
    type: null,
    cancel: null,
    submit: null,
    id: null,
    name: null,
    loadurl: null,
    cssclass: null,
    style: null,
    callback: null,
    submitdata: null,
    rows: 0,
    cols: 0,
    height: null,
    width: null,
    loadtype: null,
    loaddata: null,
    data: null,
    onblur: null,
    onsubmit: null,
    onreset: null,
    onerror: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jCropAPI

js.com.jquery.v3._jCropAPI = function js_com_jquery_v3__jCropAPI() {
}
js.com.jquery.v3._jCropAPI.prototype = {
    
    setSelect: function js_com_jquery_v3__jCropAPI$setSelect(array) {
        /// <param name="array" type="Array" elementType="Number" elementInteger="true">
        /// </param>
    },
    
    animateTo: function js_com_jquery_v3__jCropAPI$animateTo(array) {
        /// <param name="array" type="Array" elementType="Number" elementInteger="true">
        /// </param>
    },
    
    release: function js_com_jquery_v3__jCropAPI$release() {
    },
    
    destroy: function js_com_jquery_v3__jCropAPI$destroy() {
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._JCropCoords

js.com.jquery.v3._JCropCoords = function js_com_jquery_v3__JCropCoords() {
    /// <field name="x" type="Number" integer="true">
    /// </field>
    /// <field name="y" type="Number" integer="true">
    /// </field>
    /// <field name="x2" type="Number" integer="true">
    /// </field>
    /// <field name="y2" type="Number" integer="true">
    /// </field>
    /// <field name="w" type="Number" integer="true">
    /// </field>
    /// <field name="h" type="Number" integer="true">
    /// </field>
}
js.com.jquery.v3._JCropCoords.prototype = {
    x: 0,
    y: 0,
    x2: 0,
    y2: 0,
    w: 0,
    h: 0
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._JCropOptions

js.com.jquery.v3._JCropOptions = function js_com_jquery_v3__JCropOptions() {
    /// <field name="aspectRatio" type="Number">
    /// decimal 	 Aspect ratio of w/h (e.g. 1 for square)
    /// </field>
    /// <field name="minSize" type="Array" elementType="Number" elementInteger="true">
    /// array [ w, h ] 	 Minimum width/height, use 0 for unbounded dimension
    /// </field>
    /// <field name="maxSize" type="Array" elementType="Number" elementInteger="true">
    /// array [ w, h ] 	 Maximum width/height, use 0 for unbounded dimension
    /// </field>
    /// <field name="setSelect" type="Array" elementType="Number" elementInteger="true">
    /// array [ x, y, x2, y2 ] 	 Set an initial selection area
    /// </field>
    /// <field name="bgColor" type="String">
    /// </field>
    /// <field name="bgOpacity" type="Number">
    /// decimal 0 - 1 	 Opacity of outer image when cropping
    /// </field>
    /// <field name="onSelect" type="js.com.jquery.v3._JCropEventHandler">
    /// Called when selection is completed
    /// </field>
    /// <field name="onChange" type="js.com.jquery.v3._JCropEventHandler">
    /// Called when the selection is moving
    /// </field>
}
js.com.jquery.v3._JCropOptions.prototype = {
    aspectRatio: 0,
    minSize: null,
    maxSize: null,
    setSelect: null,
    bgColor: null,
    bgOpacity: 0,
    onSelect: null,
    onChange: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._ShadowBoxObjectParams

js.com.jquery.v3._ShadowBoxObjectParams = function js_com_jquery_v3__ShadowBoxObjectParams() {
    /// <field name="content" type="String">
    /// The actual content of the object (e.g. URL, HTML code, etc.)
    /// </field>
    /// <field name="player" type="String">
    /// </field>
    /// <field name="title" type="String">
    /// </field>
    /// <field name="gallery" type="String">
    /// </field>
    /// <field name="height" type="Number" integer="true">
    /// </field>
    /// <field name="width" type="Number" integer="true">
    /// </field>
}
js.com.jquery.v3._ShadowBoxObjectParams.prototype = {
    content: null,
    player: null,
    title: null,
    gallery: null,
    height: 0,
    width: 0
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._ShadowBoxOptions

js.com.jquery.v3._ShadowBoxOptions = function js_com_jquery_v3__ShadowBoxOptions() {
    /// <field name="animate" type="Boolean">
    /// </field>
    /// <field name="animateFade" type="Boolean">
    /// </field>
    /// <field name="animSequence" type="String">
    /// wh - width then height, hw - height then width , sync - together
    /// </field>
    /// <field name="autoplayMovies" type="Boolean">
    /// </field>
    /// <field name="continuous" type="Boolean">
    /// Set this true to enable "continuous" galleries. By default, the galleries will not let a user go before the first image or after the last. Enabling this feature will let the user go directly to the first image in a gallery from the last one by selecting "Next". Defaults to false.
    /// </field>
    /// <field name="counterLimit" type="Number" integer="true">
    /// Limits the number of counter links that will be displayed in a "skip" style counter. If the actual number of gallery elements is greater than this value, the counter will be restrained to the elements immediately preceding and following the current element. Defaults to 10.
    /// </field>
    /// <field name="counterType" type="String">
    /// skip / default - The mode to use for the gallery counter. May be either "default"  or "skip". The default counter is a simple "1 of 5" message. The skip counter displays a separate link to each piece in the gallery, enabling quick navigation in large galleries. Defaults to "default".
    /// </field>
    /// <field name="displayCounter" type="Boolean">
    /// </field>
    /// <field name="displayNav" type="Boolean">
    /// </field>
    /// <field name="enableKeys" type="Boolean">
    /// </field>
    /// <field name="fadeDuration" type="Number">
    /// </field>
    /// <field name="flashParams" type="Object">
    /// </field>
    /// <field name="flashVars" type="Object">
    /// </field>
    /// <field name="flashVersion" type="String">
    /// </field>
    /// <field name="handleOversize" type="String">
    /// none / resize / drag - The mode to use for handling content that is too large for the viewport. May be one of "none", "resize", or "drag"  (for images). The "none" setting will not alter the image dimensions, though clipping may occur. Setting this to "resize"  enables on-the-fly resizing of large content. In this mode, the height and width of large, resizable content will be adjusted so that it may still be viewed in its entirety while maintaining its original aspect ratio. The "drag" mode will display an oversized image at its original resolution, but will allow the user to drag it within the view to see portions that may be clipped. See the demo for a demonstration of all three modes of operation. Defaults to "resize".
    /// </field>
    /// <field name="handleUnsupported" type="String">
    /// The mode to use for handling unsupported media. May be either "link"  or "remove". Media are unsupported when the browser plugin required to display the media properly is not installed. The link option will display a user-friendly error message with a link to a page where the needed plugin can be downloaded. The remove option will simply remove any unsupported gallery elements from the gallery before displaying it. With this option, if the element is not part of a gallery, the link will simply be followed. Defaults to "link".
    /// </field>
    /// <field name="initialHeight" type="Number" integer="true">
    /// </field>
    /// <field name="initialWidth" type="Number" integer="true">
    /// </field>
    /// <field name="modal" type="Boolean">
    /// </field>
    /// <field name="onChange" type="js.com.jquery.v3._ShadowBoxElementHandler">
    /// </field>
    /// <field name="onClose" type="js.com.jquery.v3._ShadowBoxElementHandler">
    /// </field>
    /// <field name="onFinish" type="js.com.jquery.v3._ShadowBoxElementHandler">
    /// A hook function that will fire when Shadowbox finishes loading the current gallery piece (after all animations are complete). The single argument of this function will be the current gallery element.
    /// </field>
    /// <field name="onOpen" type="js.com.jquery.v3._ShadowBoxElementStoppableHandler">
    /// A hook function that will be fired when Shadowbox opens. The single argument of this function will be the current gallery element. Returning boolean false from this callback will prevent Shadowbox from opening.
    /// </field>
    /// <field name="overlayColor" type="String">
    /// </field>
    /// <field name="overlayOpacity" type="Number">
    /// </field>
    /// <field name="resizeDuration" type="Number">
    /// </field>
    /// <field name="showOverlay" type="Boolean">
    /// </field>
    /// <field name="showMovieControls" type="Boolean">
    /// </field>
    /// <field name="skipSetup" type="Boolean">
    /// </field>
    /// <field name="slideshowDelay" type="Number">
    /// </field>
    /// <field name="troubleElements" type="Array" elementType="String">
    /// An array of tag names of elements that should be hidden when a modal overlay is used. This option exists because some browsers are not able to display a semi-transparent overlay on top of these elements. These elements will have their visibility restored when Shadowbox closes. Defaults to ["select", "object", "embed", "canvas"].
    /// </field>
    /// <field name="viewportPadding" type="Number" integer="true">
    /// </field>
}
js.com.jquery.v3._ShadowBoxOptions.prototype = {
    animate: false,
    animateFade: false,
    animSequence: null,
    autoplayMovies: false,
    continuous: false,
    counterLimit: 0,
    counterType: null,
    displayCounter: false,
    displayNav: false,
    enableKeys: false,
    fadeDuration: 0,
    flashParams: null,
    flashVars: null,
    flashVersion: null,
    handleOversize: null,
    handleUnsupported: null,
    initialHeight: 0,
    initialWidth: 0,
    modal: false,
    onChange: null,
    onClose: null,
    onFinish: null,
    onOpen: null,
    overlayColor: null,
    overlayOpacity: 0,
    resizeDuration: 0,
    showOverlay: false,
    showMovieControls: false,
    skipSetup: false,
    slideshowDelay: 0,
    troubleElements: null,
    viewportPadding: 0
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._TableDnDOptions

js.com.jquery.v3._TableDnDOptions = function js_com_jquery_v3__TableDnDOptions() {
    /// <field name="onDragStyle" type="String">
    /// This is the style that is assigned to the row during drag. There are limitations to the styles that can be associated with a row (such as you can�t assign a border�well you can, but it won�t be displayed). (So instead consider using onDragClass.) The CSS style to apply is specified as a map (as used in the jQuery css(...) function).
    /// </field>
    /// <field name="onDropStyle" type="String">
    /// This is the style that is assigned to the row when it is dropped. As for onDragStyle, there are limitations  to what you can do. Also this replaces the original style, so again consider using onDragClass which is simply added and then removed on drop.
    /// </field>
    /// <field name="onDragClass" type="String">
    /// This class is added for the duration of the drag and then removed when the row is dropped. It is more  flexible than using onDragStyle since it can be inherited by the row cells and other content. The default is class is tDnD_whileDrag. So to use the default, simply customise this CSS class in your stylesheet.
    /// </field>
    /// <field name="onDrop" type="js.com.jquery.v3.DragHandler">
    /// Pass a function that will be called when the row is dropped. The function takes 2 parameters: the table and the row that was dropped. You can work out the new order of the rows by using table.tBodies[0].rows.
    /// </field>
    /// <field name="onDragStart" type="js.com.jquery.v3.DragHandler">
    /// Pass a function that will be called when the user starts dragging. The function takes 2 parameters: the table and the row which the user has started to drag.
    /// </field>
    /// <field name="scrollAmount" type="Number" integer="true">
    /// This is the number of pixels to scroll if the user moves the mouse cursor to the top or bottom of the window. The page should automatically scroll up or down as appropriate (tested in IE6, IE7, Safari, FF2, FF3 beta)
    /// </field>
    /// <field name="dragHandle" type="String">
    /// firstly only  the cell with the drag handle class is draggable and secondly it doesn�t automatically add the cursor: move style to the row (or the drag handle cell), so you are responsible for setting up the style as you see fit.
    /// </field>
    /// <field name="onAllowDrop" type="js.com.jquery.v3.AllowDropHandler">
    /// Pass a function that will be called as a row is over another row. If the function returns true, allow
    /// dropping on that row, otherwise not. The function takes 2 parameters: the dragged row and the row under
    /// the cursor. It returns a boolean: true allows the drop, false doesn't allow it.
    /// Else if you do not specify this, you canset the css class to the items dropping is not allowed to 'nodrop' and it will work.
    /// </field>
    /// <field name="onDragRowChange" type="js.com.jquery.v3.DragChangeHandler">
    /// </field>
}
js.com.jquery.v3._TableDnDOptions.prototype = {
    onDragStyle: null,
    onDropStyle: null,
    onDragClass: null,
    onDrop: null,
    onDragStart: null,
    scrollAmount: 0,
    dragHandle: null,
    onAllowDrop: null,
    onDragRowChange: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3.JQueryEnums

js.com.jquery.v3.JQueryEnums = function js_com_jquery_v3_JQueryEnums() {
}
js.com.jquery.v3.JQueryEnums.easingToString = function js_com_jquery_v3_JQueryEnums$easingToString(easing) {
    /// <param name="easing" type="js.com.jquery.v3.JQUERY_EASING">
    /// </param>
    /// <returns type="String"></returns>
    switch (easing) {
        case js.com.jquery.v3.JQUERY_EASING.linear:
            return 'linear';
        case js.com.jquery.v3.JQUERY_EASING.swing:
            return 'swing';
    }
    throw new Error("Invalid easing value '" + easing + "'");
}
js.com.jquery.v3.JQueryEnums.eventTypeToString = function js_com_jquery_v3_JQueryEnums$eventTypeToString(type) {
    /// <param name="type" type="js.com.jquery.v3.JQUERY_EVENT_TYPE">
    /// </param>
    /// <returns type="String"></returns>
    switch (type) {
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.beforeUnload:
            return 'beforeunload';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.blur:
            return 'blur';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.change:
            return 'change';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.click:
            return 'click';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.doubleClick:
            return 'dblclick';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.error:
            return 'error';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.focus:
            return 'focus';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.keyDown:
            return 'keydown';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.keyPress:
            return 'keypress';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.keyUp:
            return 'keyup';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.load:
            return 'load';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.mouseDown:
            return 'mousedown';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.mouseEnter:
            return 'mouseenter';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.mouseLeave:
            return 'mouseleave';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.mouseMove:
            return 'mousemove';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.mouseOut:
            return 'mouseout';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.mouseOver:
            return 'mouseover';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.mouseUp:
            return 'mouseup';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.ready:
            return 'ready';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.resize:
            return 'resize';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.scroll:
            return 'scroll';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.select:
            return 'select';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.submit:
            return 'submit';
        case js.com.jquery.v3.JQUERY_EVENT_TYPE.unload:
            return 'unload';
    }
    return null;
}
js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString = function js_com_jquery_v3_JQueryEnums$jQueryUIEffectToString(effect) {
    /// <param name="effect" type="js.com.jquery.v3.JQUERY_UI_EFFECT">
    /// </param>
    /// <returns type="String"></returns>
    switch (effect) {
        case js.com.jquery.v3.JQUERY_UI_EFFECT.blind:
            return 'blind';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.bounce:
            return 'bounce';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.clip:
            return 'clip';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.drop:
            return 'drop';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.explode:
            return 'explode';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.fold:
            return 'fold';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.highlight:
            return 'highlight';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.puff:
            return 'puff';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.pulsate:
            return 'pulsate';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.scale:
            return 'scale';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.shake:
            return 'shake';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.size:
            return 'size';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.slide:
            return 'slide';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.transfer:
            return 'transfer';
        case js.com.jquery.v3.JQUERY_UI_EFFECT.fade:
            return 'fade';
    }
    return null;
}
js.com.jquery.v3.JQueryEnums.jQueryPositionToString = function js_com_jquery_v3_JQueryEnums$jQueryPositionToString(pos) {
    /// <param name="pos" type="js.com.jquery.v3.JQUERY_POSITION">
    /// </param>
    /// <returns type="String"></returns>
    switch (pos) {
        case js.com.jquery.v3.JQUERY_POSITION.bottom:
            return 'bottom';
        case js.com.jquery.v3.JQUERY_POSITION.center:
            return 'center';
        case js.com.jquery.v3.JQUERY_POSITION.left:
            return 'left';
        case js.com.jquery.v3.JQUERY_POSITION.right:
            return 'right';
        case js.com.jquery.v3.JQUERY_POSITION.top:
            return 'top';
    }
    return null;
}
js.com.jquery.v3.JQueryEnums.stringToJQueryUIEffect = function js_com_jquery_v3_JQueryEnums$stringToJQueryUIEffect(effect) {
    /// <param name="effect" type="String">
    /// </param>
    /// <returns type="js.com.jquery.v3.JQUERY_UI_EFFECT"></returns>
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.blind)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.blind;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.bounce)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.bounce;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.clip)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.clip;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.drop)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.drop;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.explode)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.explode;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.fold)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.fold;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.highlight)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.highlight;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.puff)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.puff;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.pulsate)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.pulsate;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.scale)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.scale;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.shake)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.shake;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.size)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.size;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.slide)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.slide;
    }
    if (effect === js.com.jquery.v3.JQueryEnums.jQueryUIEffectToString(js.com.jquery.v3.JQUERY_UI_EFFECT.transfer)) {
        return js.com.jquery.v3.JQUERY_UI_EFFECT.transfer;
    }
    return js.com.jquery.v3.JQUERY_UI_EFFECT.none;
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3.fx

js.com.jquery.v3.fx = function js_com_jquery_v3_fx() {
    /// <field name="off" type="Boolean">
    /// </field>
}
js.com.jquery.v3.fx.prototype = {
    off: false
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3.jQueryUtil

js.com.jquery.v3.jQueryUtil = function js_com_jquery_v3_jQueryUtil() {
    /// <field name="_onReadyCallbackCalled" type="Boolean" static="true">
    /// </field>
    /// <field name="_jQueryFactory" type="Object" static="true">
    /// </field>
}
js.com.jquery.v3.jQueryUtil._init = function js_com_jquery_v3_jQueryUtil$_init() {
    if (ss.isNullOrUndefined(js.com.jquery.v3.jQueryUtil._jQueryFactory)) {
        jQuery.jQueryFactoryInstance = jQuery;
        js.com.jquery.v3.jQueryUtil._jQueryFactory = jQuery;
        js.com.jquery.v3.jQueryUtil.addOnReadyCallback(js.com.jquery.v3.jQueryUtil._onReadyCallback);
    }
}
js.com.jquery.v3.jQueryUtil._onReadyCallback = function js_com_jquery_v3_jQueryUtil$_onReadyCallback() {
    js.com.jquery.v3.jQueryUtil._onReadyCallbackCalled = true;
}
js.com.jquery.v3.jQueryUtil.selectByElements = function js_com_jquery_v3_jQueryUtil$selectByElements(elements) {
    /// <param name="elements" type="Array" elementType="Object" elementDomElement="true">
    /// </param>
    /// <returns type="jQuery"></returns>
    return js.com.jquery.v3.jQueryUtil._select(elements, null);
}
js.com.jquery.v3.jQueryUtil.selectByIds = function js_com_jquery_v3_jQueryUtil$selectByIds(ids) {
    /// <summary>
    /// Selects multiple IDs
    /// </summary>
    /// <param name="ids" type="Array" elementType="String">
    /// </param>
    /// <returns type="jQuery"></returns>
    if (ids == null) {
        return null;
    }
    var sIds = '';
    for (var i = 0; i < ids.length; i++) {
        if (!String.isNullOrEmpty(ids[i])) {
            if (!!sIds) {
                sIds += ',';
            }
            sIds += '#' + ids[i];
        }
    }
    return js.com.jquery.v3.jQueryUtil.select(sIds);
}
js.com.jquery.v3.jQueryUtil.selectById = function js_com_jquery_v3_jQueryUtil$selectById(id, context) {
    /// <param name="id" type="String">
    /// </param>
    /// <param name="context" type="Object" domElement="true">
    /// </param>
    /// <returns type="jQuery"></returns>
    if (String.isNullOrEmpty(id)) {
        return null;
    }
    if (Type.canCast(id, String)) {
        return js.com.jquery.v3.jQueryUtil.select('#' + id, context);
    }
    else {
        return js.com.jquery.v3.jQueryUtil.select(id);
    }
}
js.com.jquery.v3.jQueryUtil.selectByIdOrElements = function js_com_jquery_v3_jQueryUtil$selectByIdOrElements(id) {
    /// <param name="id" type="String">
    /// </param>
    /// <returns type="jQuery"></returns>
    if (Type.canCast(id, String)) {
        return js.com.jquery.v3.jQueryUtil.selectById(id);
    }
    else {
        return js.com.jquery.v3.jQueryUtil.selectByElements(id);
    }
}
js.com.jquery.v3.jQueryUtil._select = function js_com_jquery_v3_jQueryUtil$_select(expression, context) {
    /// <param name="expression" type="Object">
    /// </param>
    /// <param name="context" type="Object" domElement="true">
    /// </param>
    /// <returns type="jQuery"></returns>
    try {
        var t = window;
        return t.jQuery(expression, context);
    }
    catch (ex) {
        alert(ex.message);
    }
    return null;
}
js.com.jquery.v3.jQueryUtil.select = function js_com_jquery_v3_jQueryUtil$select(expression, context) {
    /// <param name="expression" type="String">
    /// </param>
    /// <param name="context" type="Object" domElement="true">
    /// </param>
    /// <returns type="jQuery"></returns>
    return js.com.jquery.v3.jQueryUtil._select(expression, context);
}
js.com.jquery.v3.jQueryUtil.exists = function js_com_jquery_v3_jQueryUtil$exists(expression, context) {
    /// <param name="expression" type="String">
    /// </param>
    /// <param name="context" type="jQuery">
    /// </param>
    /// <returns type="Boolean"></returns>
    var jItem = js.com.jquery.v3.jQueryUtil._select(expression, context);
    return jItem.length > 0;
}
js.com.jquery.v3.jQueryUtil.selectObject = function js_com_jquery_v3_jQueryUtil$selectObject(obj) {
    /// <param name="obj" type="Object">
    /// </param>
    /// <returns type="jQuery"></returns>
    return js.com.jquery.v3.jQueryUtil._select(obj, null);
}
js.com.jquery.v3.jQueryUtil.createElementFromHTML = function js_com_jquery_v3_jQueryUtil$createElementFromHTML(html, ownerDocument) {
    /// <param name="html" type="String">
    /// </param>
    /// <param name="ownerDocument" type="Object">
    /// </param>
    /// <returns type="jQuery"></returns>
    return js.com.jquery.v3.jQueryUtil.select(html, ownerDocument);
}
js.com.jquery.v3.jQueryUtil.addOnReadyCallback = function js_com_jquery_v3_jQueryUtil$addOnReadyCallback(callback) {
    /// <summary>
    /// Adds document on ready handler
    /// </summary>
    /// <param name="callback" type="Action">
    /// </param>
    if (js.com.jquery.v3.jQueryUtil._onReadyCallbackCalled) {
        callback();
    }
    else {
        js.com.jquery.v3.jQueryUtil._select(callback, null);
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._PrettyPhotoSettings

js.com.jquery.v3._PrettyPhotoSettings = function js_com_jquery_v3__PrettyPhotoSettings() {
    /// <field name="animationSpeed" type="String">
    /// fast/slow/normal
    /// </field>
    /// <field name="opacity" type="Number">
    /// </field>
    /// <field name="showTitle" type="Boolean">
    /// </field>
    /// <field name="allowresize" type="Boolean">
    /// </field>
    /// <field name="default_width" type="String">
    /// </field>
    /// <field name="default_height" type="String">
    /// </field>
    /// <field name="counter_separator_label" type="String">
    /// The separator for the gallery counter 1 "of" 2
    /// </field>
    /// <field name="theme" type="String">
    /// light_rounded / dark_rounded / light_square / dark_square / facebook
    /// </field>
    /// <field name="hideflash" type="Boolean">
    /// </field>
    /// <field name="wmode" type="String">
    /// </field>
    /// <field name="autoplay" type="Boolean">
    /// Automatically start videos: True/False
    /// </field>
    /// <field name="modal" type="Boolean">
    /// </field>
    /// <field name="changepicturecallback" type="Action">
    /// Triggered on change
    /// </field>
    /// <field name="callback" type="Action">
    /// Triggered on close
    /// </field>
    /// <field name="markup" type="String">
    /// The markup to show on click.  The content is shown inside &gt; div id="pp_full_res"
    /// </field>
    /// <field name="image_markup" type="String">
    /// </field>
    /// <field name="flash_markup" type="String">
    /// </field>
    /// <field name="quicktime_markup" type="String">
    /// </field>
    /// <field name="iframe_markup" type="String">
    /// </field>
    /// <field name="inline_markup" type="String">
    /// </field>
}
js.com.jquery.v3._PrettyPhotoSettings.prototype = {
    animationSpeed: null,
    opacity: 0,
    showTitle: false,
    allowresize: false,
    default_width: null,
    default_height: null,
    counter_separator_label: null,
    theme: null,
    hideflash: false,
    wmode: null,
    autoplay: false,
    modal: false,
    changepicturecallback: null,
    callback: null,
    markup: null,
    image_markup: null,
    flash_markup: null,
    quicktime_markup: null,
    iframe_markup: null,
    inline_markup: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryUISortableOptionsEventUI

js.com.jquery.v3._jQueryUISortableOptionsEventUI = function js_com_jquery_v3__jQueryUISortableOptionsEventUI() {
    /// <field name="helper" type="Object" domElement="true">
    /// </field>
    /// <field name="position" type="js.com.jquery.v3._Position">
    /// </field>
    /// <field name="offset" type="js.com.jquery.v3._Position">
    /// </field>
    /// <field name="item" type="Array" elementType="Object" elementDomElement="true">
    /// </field>
    /// <field name="placeholder" type="Object" domElement="true">
    /// </field>
    /// <field name="originalPosition" type="js.com.jquery.v3._Position">
    /// </field>
    /// <field name="sender" type="Object" domElement="true">
    /// </field>
}
js.com.jquery.v3._jQueryUISortableOptionsEventUI.prototype = {
    helper: null,
    position: null,
    offset: null,
    item: null,
    placeholder: null,
    originalPosition: null,
    sender: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._jQueryUISortableOptions

js.com.jquery.v3._jQueryUISortableOptions = function js_com_jquery_v3__jQueryUISortableOptions() {
    /// <field name="disabled" type="Boolean">
    /// Disables (true) or enables (false) the sortable. Can be set when initialising (first creating) the sortable.
    /// </field>
    /// <field name="appendTo" type="String">
    /// Defines where the helper that moves with the mouse is being appended to during the drag (for example, to resolve overlap/zIndex issues).
    /// Code examples
    /// Initialize a sortable with the appendTo option specified.
    /// $( ".selector" ).sortable({ appendTo: 'body' });
    /// Get or set the appendTo option, after init.        //getter
    /// var appendTo = $( ".selector" ).sortable( "option", "appendTo" );//setter
    /// $( ".selector" ).sortable( "option", "appendTo", 'body' );
    /// </field>
    /// <field name="axis" type="String">
    /// If defined, the items can be dragged only horizontally or vertically. Possible values:'x', 'y'.
    /// </field>
    /// <field name="cancel" type="String">
    /// Prevents sorting if you start on elements matching the selector. Default: input,button'
    /// </field>
    /// <field name="connectWith" type="String">
    /// Takes a jQuery selector with items that also have sortables applied. If used, the sortable is now connected to the other one-way, so you can drag from this sortable to the other.
    /// </field>
    /// <field name="containment" type="String">
    /// Constrains dragging to within the bounds of the specified element - can be a DOM element, 'parent', 'document', 'window', or a jQuery selector.
    /// </field>
    /// <field name="cursor" type="String">
    /// Defines the cursor that is being shown while sorting.
    /// </field>
    /// <field name="cursorAt" type="Object">
    /// Moves the sorting element or helper so the cursor always appears to drag from the same position. Coordinates can be given as a hash using a combination of one or two keys: { top, left, right, bottom }
    /// </field>
    /// <field name="delay" type="Number" integer="true">
    /// Time in milliseconds to define when the sorting should start. It helps preventing unwanted drags when clicking on an element.
    /// </field>
    /// <field name="distance" type="Number" integer="true">
    /// Tolerance, in pixels, for when sorting should start. If specified, sorting will not start until after mouse is dragged beyond distance. Can be used to allow for clicks on elements within a handle.
    /// </field>
    /// <field name="dragOnEmpty" type="Boolean">
    /// If false items from this sortable can't be dropped to an empty linked sortable.
    /// </field>
    /// <field name="forceHelperSize" type="Boolean">
    /// </field>
    /// <field name="forcePlaceholderSize" type="Boolean">
    /// </field>
    /// <field name="grid" type="Array">
    /// </field>
    /// <field name="handle" type="Object">
    /// Restricts sort start click to the specified element. ex 'h2',
    /// </field>
    /// <field name="helper" type="String">
    /// Allows for a helper element to be used for dragging display. The supplied function receives the event and the element being sorted, and should return a DOMElement to be used as a custom proxy helper. Possible values: 'original', 'clone
    /// </field>
    /// <field name="items" type="String">
    /// Specifies which items inside the element should be sortable.
    /// </field>
    /// <field name="opacity" type="Number">
    /// Defines the opacity of the helper while sorting. From 0.01 to 1
    /// </field>
    /// <field name="placeholder" type="String">
    /// Class that gets applied to the otherwise white space.
    /// </field>
    /// <field name="revert" type="Boolean">
    /// If set to true, the item will be reverted to its new DOM position with a smooth animation. Optionally, it can also be set to a number that controls the duration of the animation in ms.
    /// </field>
    /// <field name="scroll" type="Boolean">
    /// </field>
    /// <field name="scrollSensitivity" type="Number" integer="true">
    /// </field>
    /// <field name="scrollSpeed" type="Number" integer="true">
    /// </field>
    /// <field name="tolerance" type="String">
    /// This is the way the reordering behaves during drag. Possible values: 'intersect', 'pointer'. In some setups, 'pointer' is more natural.
    /// </field>
    /// <field name="zIndex" type="Number" integer="true">
    /// Z-index for element/helper while being sorted.
    /// </field>
    /// <field name="start" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
    /// <field name="sort" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
    /// <field name="change" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
    /// <field name="beforeStop" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
    /// <field name="stop" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
    /// <field name="update" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
    /// <field name="receive" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
    /// <field name="remove" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
    /// <field name="over" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
    /// <field name="out" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
    /// <field name="activate" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
    /// <field name="deactivate" type="js.com.jquery.v3._jQueryUIEventHandler">
    /// </field>
}
js.com.jquery.v3._jQueryUISortableOptions.prototype = {
    disabled: false,
    appendTo: null,
    axis: null,
    cancel: null,
    connectWith: null,
    containment: null,
    cursor: null,
    cursorAt: null,
    delay: 0,
    distance: 0,
    dragOnEmpty: false,
    forceHelperSize: false,
    forcePlaceholderSize: false,
    grid: null,
    handle: null,
    helper: null,
    items: null,
    opacity: null,
    placeholder: null,
    revert: false,
    scroll: false,
    scrollSensitivity: 0,
    scrollSpeed: 0,
    tolerance: null,
    zIndex: 0,
    start: null,
    sort: null,
    change: null,
    beforeStop: null,
    stop: null,
    update: null,
    receive: null,
    remove: null,
    over: null,
    out: null,
    activate: null,
    deactivate: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._UploadifySettings

js.com.jquery.v3._UploadifySettings = function js_com_jquery_v3__UploadifySettings() {
    /// <field name="uploader" type="String">
    /// The relative path to the uploadify.swf file. For absolute paths prefix the path with either �/� or �http�. Default = �uploadify.swf�
    /// </field>
    /// <field name="script" type="String">
    /// The relative path to the backend script that will be processing your uploaded files. For absolute paths prefix the path with either �/� or �http�
    /// Default = �uploadify.php�
    /// </field>
    /// <field name="checkScript" type="String">
    /// The relative path to the backend script that will check if the file selected already resides on the server.
    /// No Default. �check.php� is provided with core files.
    /// </field>
    /// <field name="scriptData" type="Object">
    /// An object containing name/value pairs of additional information you would like sent to the upload script. {�name�: �value�}
    /// </field>
    /// <field name="fileDataName" type="String">
    /// The name of your files array in the upload server script. Default = �Filedata�
    /// </field>
    /// <field name="method" type="String">
    /// GET / POST
    /// </field>
    /// <field name="scriptAccess" type="String">
    /// The access mode for scripts in the flash file. If you are testing locally, set to �always�.
    /// Default = �sameDomain�
    /// </field>
    /// <field name="folder" type="String">
    /// The path to the folder you would like to save the files to. Do not end the path with a �/�.
    /// For absolute paths prefix the path with either �/� or �http�. Note server security issues with trying to upload to remote destinations.
    /// </field>
    /// <field name="queueID" type="String">
    /// The ID of the element you want to use as your file queue. By default, one is created on the fly below the �Browse� button.
    /// </field>
    /// <field name="queueSizeLimit" type="Number" integer="true">
    /// The limit of the number of items that can be in the queue at one time. Default = 999.
    /// </field>
    /// <field name="multi" type="Boolean">
    /// Set to true if you want to allow multiple file uploads.
    /// </field>
    /// <field name="auto" type="Boolean">
    /// Set to true if you would like the files to be uploaded when they are selected.
    /// </field>
    /// <field name="fileDesc" type="String">
    /// The text that will appear in the file type drop down at the bottom of the browse dialog box.
    /// </field>
    /// <field name="fileExt" type="String">
    /// A list of file extensions you would like to allow for upload. Format like �*.ext1;*.ext2;*.ext3'.
    /// fileDesc is required when using this option.
    /// </field>
    /// <field name="sizeLimit" type="Number" integer="true">
    /// A number representing the limit in bytes for each upload.
    /// </field>
    /// <field name="simUploadLimit" type="Number" integer="true">
    /// A limit to the number of simultaneous uploads you would like to allow.
    /// Default: 1
    /// </field>
    /// <field name="buttonText" type="String">
    /// The text you would like to appear on the default button. Default = �BROWSE�
    /// </field>
    /// <field name="buttonImg" type="String">
    /// </field>
    /// <field name="hideButton" type="Boolean">
    /// </field>
    /// <field name="rollover" type="Boolean">
    /// </field>
    /// <field name="width" type="Number" integer="true">
    /// </field>
    /// <field name="height" type="Number" integer="true">
    /// </field>
    /// <field name="wmode" type="String">
    /// </field>
    /// <field name="cancelImg" type="String">
    /// </field>
    /// <field name="onInit" type="EventHandler">
    /// A function that triggers when the script is loaded. The default event handler hides the targeted element on the page and replaces it with the flash file, then creates a queue container after it. The default function will not trigger if the value of your custom function returns false. For custom functions, you can access the html for the flash file using the variable flashElement.
    /// </field>
    /// <field name="onSelect" type="js.com.jquery.v3.UploadifySelectHandler">
    /// A function that triggers for each element selected. The default event handler generates a 6 character random string as the unique identifier for the file item and creates a file queue item for the file. The default event handler will not trigger if the value of your custom function returns false.
    /// </field>
    /// <field name="onSelectOnce" type="js.com.jquery.v3.UploadifySelectOnceHandler">
    /// A function that triggers once for each select operation. There is no default event handler.
    /// </field>
    /// <field name="onCancel" type="js.com.jquery.v3.UploadifyOnCancelHandler">
    /// A function that triggers when a file upload is cancelled or removed from the queue. The default event handler removes the file from the upload queue. The default event handler will not trigger if the value of your custom function returns false.
    /// </field>
    /// <field name="onClearQueue" type="js.com.jquery.v3.UploadifyClearQueueHandler">
    /// A function that triggers when the fileUploadClearQueue  function is called. The default event handler removes all queue items from the upload queue. The default event handler will not trigger if the value of your custom function returns false.
    /// </field>
    /// <field name="onQueueFull" type="js.com.jquery.v3.UploadifyOnQueueFullHandler">
    /// </field>
    /// <field name="onError" type="js.com.jquery.v3.UploadifyOnErrorHandler">
    /// </field>
    /// <field name="onOpen" type="js.com.jquery.v3.UploadifyOnOpenHandler">
    /// </field>
    /// <field name="onProgress" type="js.com.jquery.v3.UploadifyOnProgressHandler">
    /// </field>
    /// <field name="onComplete" type="js.com.jquery.v3.UploadifyOnCompleteHandler">
    /// A function that triggers when a file upload has completed. The default function removes the file queue item from the upload queue. The default function will not trigger if the value of your custom function returns false.
    /// </field>
    /// <field name="onAllComplete" type="js.com.jquery.v3.UploadifyOnAllCompleteHandler">
    /// </field>
    /// <field name="onCheck" type="js.com.jquery.v3.UploadifyOnCheckHandler">
    /// </field>
    /// <field name="defaulT_CANCEL_IMAGE" type="String" static="true">
    /// </field>
}
js.com.jquery.v3._UploadifySettings.prototype = {
    uploader: null,
    script: null,
    checkScript: null,
    scriptData: null,
    fileDataName: null,
    method: null,
    scriptAccess: null,
    folder: null,
    queueID: null,
    queueSizeLimit: 0,
    multi: false,
    auto: false,
    fileDesc: null,
    fileExt: null,
    sizeLimit: 0,
    simUploadLimit: 0,
    buttonText: null,
    buttonImg: null,
    hideButton: false,
    rollover: false,
    width: 0,
    height: 0,
    wmode: null,
    cancelImg: null,
    onInit: null,
    onSelect: null,
    onSelectOnce: null,
    onCancel: null,
    onClearQueue: null,
    onQueueFull: null,
    onError: null,
    onOpen: null,
    onProgress: null,
    onComplete: null,
    onAllComplete: null,
    onCheck: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._UploadifyFileObject

js.com.jquery.v3._UploadifyFileObject = function js_com_jquery_v3__UploadifyFileObject() {
    /// <field name="name" type="String">
    /// </field>
    /// <field name="size" type="Number" integer="true">
    /// The size in bytes of the file
    /// </field>
    /// <field name="creationDate" type="Date">
    /// The date the file was created
    /// </field>
    /// <field name="modificationDate" type="Date">
    /// The last date the file was modified
    /// </field>
    /// <field name="type" type="String">
    /// The file extension beginning with a �.�
    /// </field>
}
js.com.jquery.v3._UploadifyFileObject.prototype = {
    name: null,
    size: 0,
    creationDate: null,
    modificationDate: null,
    type: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._UploadifySelectOnceData

js.com.jquery.v3._UploadifySelectOnceData = function js_com_jquery_v3__UploadifySelectOnceData() {
    /// <field name="filesSelected" type="Number" integer="true">
    /// The number of files selected in the select operation
    /// </field>
    /// <field name="filesReplaced" type="Number" integer="true">
    /// The number of files that were replaced in the queue
    /// </field>
    js.com.jquery.v3._UploadifySelectOnceData.initializeBase(this);
}
js.com.jquery.v3._UploadifySelectOnceData.prototype = {
    filesSelected: 0,
    filesReplaced: 0
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._UploadifyQueueData

js.com.jquery.v3._UploadifyQueueData = function js_com_jquery_v3__UploadifyQueueData() {
    /// <field name="fileCount" type="Number" integer="true">
    /// The total number of files in the queue
    /// </field>
    /// <field name="allBytesTotal" type="Number" integer="true">
    /// The total number of bytes for all files in the queue
    /// </field>
}
js.com.jquery.v3._UploadifyQueueData.prototype = {
    fileCount: 0,
    allBytesTotal: 0
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._UploadifyErrorData

js.com.jquery.v3._UploadifyErrorData = function js_com_jquery_v3__UploadifyErrorData() {
    /// <field name="type" type="String">
    /// Either �HTTP�, �IO�, or �Security�
    /// </field>
    /// <field name="info" type="String">
    /// An error message describing the type of error returned
    /// </field>
}
js.com.jquery.v3._UploadifyErrorData.prototype = {
    type: null,
    info: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._UploadifyProgressData

js.com.jquery.v3._UploadifyProgressData = function js_com_jquery_v3__UploadifyProgressData() {
    /// <field name="percentage" type="Number">
    /// The current percentage completed for the upload
    /// </field>
    /// <field name="bytesLoaded" type="Number" integer="true">
    /// The current amount of bytes uploaded
    /// </field>
    /// <field name="allBytesLoaded" type="Number" integer="true">
    /// The current amount of bytes loaded for all files in the queue
    /// </field>
    /// <field name="speed" type="Number">
    /// The current upload speed in KB/s
    /// </field>
}
js.com.jquery.v3._UploadifyProgressData.prototype = {
    percentage: 0,
    bytesLoaded: 0,
    allBytesLoaded: 0,
    speed: 0
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._UploadifyCompleteData

js.com.jquery.v3._UploadifyCompleteData = function js_com_jquery_v3__UploadifyCompleteData() {
    /// <field name="fileCount" type="Number" integer="true">
    /// The total number of files left in the queue
    /// </field>
    /// <field name="speed" type="Number">
    /// The average speed of the file upload in KB/s
    /// </field>
}
js.com.jquery.v3._UploadifyCompleteData.prototype = {
    fileCount: 0,
    speed: 0
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._UploadifyCompleteAllData

js.com.jquery.v3._UploadifyCompleteAllData = function js_com_jquery_v3__UploadifyCompleteAllData() {
    /// <field name="filesUploaded" type="Number" integer="true">
    /// The total number of files uploaded
    /// </field>
    /// <field name="errors" type="Number" integer="true">
    /// The total number of errors while uploading
    /// </field>
    /// <field name="allBytesLoaded" type="Number" integer="true">
    /// </field>
    /// <field name="speed" type="Number">
    /// The average speed of all uploaded files
    /// </field>
}
js.com.jquery.v3._UploadifyCompleteAllData.prototype = {
    filesUploaded: 0,
    errors: 0,
    allBytesLoaded: 0,
    speed: 0
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._CookieOptions

js.com.jquery.v3._CookieOptions = function js_com_jquery_v3__CookieOptions() {
    /// <field name="secure" type="Boolean">
    /// </field>
    /// <field name="path" type="String">
    /// </field>
    /// <field name="domain" type="String">
    /// </field>
    /// <field name="expires" type="Date">
    /// </field>
}
js.com.jquery.v3._CookieOptions.prototype = {
    secure: false,
    path: null,
    domain: null,
    expires: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._LightboxConfig

js.com.jquery.v3._LightboxConfig = function js_com_jquery_v3__LightboxConfig() {
    /// <field name="overlayBgColor" type="String">
    /// </field>
    /// <field name="overlayOpacity" type="Number">
    /// </field>
    /// <field name="imageLoading" type="String">
    /// </field>
    /// <field name="imageBtnClose" type="String">
    /// </field>
    /// <field name="imageBtnPrev" type="String">
    /// </field>
    /// <field name="imageBtnNext" type="String">
    /// </field>
    /// <field name="containerBorderSize" type="Number" integer="true">
    /// </field>
    /// <field name="containerResizeSpeed" type="Number" integer="true">
    /// </field>
    /// <field name="txtImage" type="String">
    /// </field>
    /// <field name="txtOf" type="String">
    /// </field>
    /// <field name="imageBlank" type="String">
    /// </field>
    /// <field name="keyToClose" type="String">
    /// </field>
    /// <field name="keyToPrev" type="String">
    /// </field>
    /// <field name="keyToNext" type="String">
    /// </field>
}
js.com.jquery.v3._LightboxConfig.prototype = {
    overlayBgColor: null,
    overlayOpacity: null,
    imageLoading: null,
    imageBtnClose: null,
    imageBtnPrev: null,
    imageBtnNext: null,
    containerBorderSize: 0,
    containerResizeSpeed: 0,
    txtImage: null,
    txtOf: null,
    imageBlank: null,
    keyToClose: null,
    keyToPrev: null,
    keyToNext: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._DialogOptions

js.com.jquery.v3._DialogOptions = function js_com_jquery_v3__DialogOptions() {
    /// <field name="autoOpen" type="Boolean">
    /// </field>
    /// <field name="bgiframe" type="Boolean">
    /// </field>
    /// <field name="buttons" type="Object">
    /// Specifies which buttons should be displayed on the dialog. The property key is the text of the button. The value is the callback function for when the button is clicked. The context of the callback is the dialog element; if you need access to the button, it is available as the target of the event object.
    /// The key is the label of the button, the value is the callback function
    /// </field>
    /// <field name="closeOnEscape" type="Boolean">
    /// </field>
    /// <field name="closeText" type="String">
    /// </field>
    /// <field name="dialogClass" type="String">
    /// </field>
    /// <field name="draggable" type="Boolean">
    /// </field>
    /// <field name="height" type="Object">
    /// Number or auto
    /// </field>
    /// <field name="width" type="Object">
    /// Number or auto
    /// </field>
    /// <field name="hide" type="String">
    /// e.g. slide, fade
    /// </field>
    /// <field name="maxHeight" type="Number">
    /// </field>
    /// <field name="minHeight" type="Number">
    /// </field>
    /// <field name="maxWidth" type="Number">
    /// </field>
    /// <field name="minWidth" type="Number">
    /// </field>
    /// <field name="modal" type="Boolean">
    /// If set to true, the dialog will have modal behavior; other items on the page will be disabled (i.e. cannot be interacted with). Modal dialogs create an overlay below the dialog but above other page elements.
    /// </field>
    /// <field name="position" type="Object">
    /// Specifies where the dialog should be displayed. Possible values:
    /// 1) a single string representing position within viewport: 'center', 'left', 'right', 'top', 'bottom'.
    /// 2) an array containing an x,y coordinate pair in pixel offset from left, top corner of viewport (e.g. [350,100])
    /// 3) an array containing x,y position string values (e.g. ['right','top'] for top right corner).
    /// </field>
    /// <field name="resizable" type="Boolean">
    /// </field>
    /// <field name="show" type="String">
    /// slide / fade etc...
    /// </field>
    /// <field name="stack" type="Boolean">
    /// Specifies whether the dialog will stack on top of other dialogs. This will cause the dialog to move to the front of other dialogs when it gains focus.
    /// </field>
    /// <field name="title" type="String">
    /// </field>
    /// <field name="zIndex" type="Number" integer="true">
    /// </field>
    /// <field name="beforeclose" type="js.com.jquery.v3.DialogEventHandler">
    /// </field>
    /// <field name="open" type="js.com.jquery.v3.DialogEventHandler">
    /// </field>
    /// <field name="focus" type="js.com.jquery.v3.DialogEventHandler">
    /// </field>
    /// <field name="dragStart" type="js.com.jquery.v3.DialogEventHandler">
    /// </field>
    /// <field name="drag" type="js.com.jquery.v3.DialogEventHandler">
    /// </field>
    /// <field name="dragStop" type="js.com.jquery.v3.DialogEventHandler">
    /// </field>
    /// <field name="resizeStart" type="js.com.jquery.v3.DialogEventHandler">
    /// </field>
    /// <field name="resize" type="js.com.jquery.v3.DialogEventHandler">
    /// </field>
    /// <field name="resizeStop" type="js.com.jquery.v3.DialogEventHandler">
    /// </field>
    /// <field name="close" type="js.com.jquery.v3.DialogEventHandler">
    /// </field>
}
js.com.jquery.v3._DialogOptions.prototype = {
    autoOpen: false,
    bgiframe: false,
    buttons: null,
    closeOnEscape: false,
    closeText: null,
    dialogClass: null,
    draggable: false,
    height: null,
    width: null,
    hide: null,
    maxHeight: null,
    minHeight: null,
    maxWidth: null,
    minWidth: null,
    modal: false,
    position: null,
    resizable: false,
    show: null,
    stack: false,
    title: null,
    zIndex: 0,
    beforeclose: null,
    open: null,
    focus: null,
    dragStart: null,
    drag: null,
    dragStop: null,
    resizeStart: null,
    resize: null,
    resizeStop: null,
    close: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._BrowserOptions

js.com.jquery.v3._BrowserOptions = function js_com_jquery_v3__BrowserOptions() {
    /// <field name="safari" type="Boolean">
    /// </field>
    /// <field name="opera" type="Boolean">
    /// </field>
    /// <field name="msie" type="Boolean">
    /// </field>
    /// <field name="mozilla" type="Boolean">
    /// </field>
    /// <field name="version" type="String">
    /// </field>
}
js.com.jquery.v3._BrowserOptions.prototype = {
    safari: false,
    opera: false,
    msie: false,
    mozilla: false,
    version: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._BrowserSupportOptions

js.com.jquery.v3._BrowserSupportOptions = function js_com_jquery_v3__BrowserSupportOptions() {
    /// <field name="boxModel" type="Boolean">
    /// </field>
    /// <field name="cssFloat" type="Boolean">
    /// </field>
    /// <field name="hrefNormalized" type="Boolean">
    /// </field>
    /// <field name="htmlSerialize" type="Boolean">
    /// </field>
    /// <field name="leadingWhitespace" type="Boolean">
    /// </field>
    /// <field name="noCloneEvent" type="Boolean">
    /// </field>
    /// <field name="objectAll" type="Boolean">
    /// </field>
    /// <field name="opacity" type="Boolean">
    /// </field>
    /// <field name="scriptEval" type="Boolean">
    /// </field>
    /// <field name="style" type="Boolean">
    /// </field>
    /// <field name="tbody" type="Boolean">
    /// </field>
}
js.com.jquery.v3._BrowserSupportOptions.prototype = {
    boxModel: false,
    cssFloat: false,
    hrefNormalized: false,
    htmlSerialize: false,
    leadingWhitespace: false,
    noCloneEvent: false,
    objectAll: false,
    opacity: false,
    scriptEval: false,
    style: false,
    tbody: false
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._JsonNameValue

js.com.jquery.v3._JsonNameValue = function js_com_jquery_v3__JsonNameValue() {
    /// <field name="name" type="String">
    /// </field>
    /// <field name="value" type="String">
    /// </field>
}
js.com.jquery.v3._JsonNameValue.prototype = {
    name: null,
    value: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._AjaxOptions

js.com.jquery.v3._AjaxOptions = function js_com_jquery_v3__AjaxOptions() {
    /// <field name="async" type="Boolean">
    /// </field>
    /// <field name="beforeSend" type="js.com.jquery.v3.XmlHttpRequestHandler">
    /// </field>
    /// <field name="cache" type="Boolean">
    /// </field>
    /// <field name="complete" type="js.com.jquery.v3.XmlHttpRequestResponseHandler">
    /// </field>
    /// <field name="contentType" type="String">
    /// </field>
    /// <field name="data" type="Object">
    /// </field>
    /// <field name="dataFilter" type="js.com.jquery.v3.DataFilterHandler">
    /// A function to be used to handle the raw responsed data of XmlHttpRequest.This is a pre-filtering function to sanitize the response.You should return the sanitized data.The function gets passed two arguments: The raw data returned from the server, and the 'dataType' parameter.
    /// </field>
    /// <field name="dataType" type="String">
    /// xml, html, script, json, jsonp, text - The type of data that you're expecting back from the server. If none is specified, jQuery will intelligently pass either responseXML or responseText to your success callback, based on the MIME type of the response. The available types (and the result passed as the first argument to your success callback) are:        ///
    /// </field>
    /// <field name="error" type="js.com.jquery.v3.XmlHttpRequestErrorHandler">
    /// A function to be called if the request fails. The function is passed three arguments: The XmlHttpRequest object, a string describing the type of error that occurred and an optional exception object, if one occurred. Possible values for the second argument (besides null) are "timeout", "error", "notmodified" and "parsererror". This is an Ajax Event.
    /// </field>
    /// <field name="global" type="Boolean">
    /// Whether to trigger global AJAX event handlers for this request. The default is true. Set to false to prevent the global handlers like ajaxStart or ajaxStop from being triggered. This can be used to control various Ajax Events.
    /// </field>
    /// <field name="ifModified" type="Boolean">
    /// Allow the request to be successful only if the response has changed since the last request. This is done by checking the Last-Modified header. Default value is false, ignoring the header.
    /// </field>
    /// <field name="jsonp" type="String">
    /// </field>
    /// <field name="password" type="String">
    /// A password to be used in response to an HTTP access authentication request.
    /// </field>
    /// <field name="processData" type="Boolean">
    /// By default, data passed in to the data option as an object (technically, anything other than a string) will be processed and transformed into a query string, fitting to the default content-type "application/x-www-form-urlencoded". If you want to send DOMDocuments, or other non-processed data, set this option to false.
    /// </field>
    /// <field name="scriptCharset" type="String">
    /// Only for requests with 'jsonp' or 'script' dataType and GET type. Forces the request to be interpreted as a certain charset. Only needed for charset differences between the remote and local content.
    /// </field>
    /// <field name="success" type="js.com.jquery.v3.XmlHttpRequestSuccessResponseHandler">
    /// A function to be called if the request succeeds. The function gets passed two arguments: The data returned from the server, formatted according to the 'dataType' parameter, and a string describing the status. This is an Ajax Event.
    /// </field>
    /// <field name="timeout" type="Number">
    /// Set a local timeout (in milliseconds) for the request. This will override the global timeout, if one is set via $.ajaxSetup. For example, you could use this property to give a single request a longer timeout than all other requests that you've set to time out in one second. See $.ajaxSetup() for global timeouts.
    /// </field>
    /// <field name="type" type="String">
    /// GET/POST
    /// </field>
    /// <field name="url" type="String">
    /// </field>
    /// <field name="username" type="String">
    /// </field>
    /// <field name="xhr" type="Function">
    /// </field>
}
js.com.jquery.v3._AjaxOptions.prototype = {
    async: false,
    beforeSend: null,
    cache: false,
    complete: null,
    contentType: null,
    data: null,
    dataFilter: null,
    dataType: null,
    error: null,
    global: false,
    ifModified: false,
    jsonp: null,
    password: null,
    processData: false,
    scriptCharset: null,
    success: null,
    timeout: null,
    type: null,
    url: null,
    username: null,
    xhr: null
}


////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3._Position

js.com.jquery.v3._Position = function js_com_jquery_v3__Position() {
    /// <field name="top" type="Number">
    /// </field>
    /// <field name="left" type="Number">
    /// </field>
}
js.com.jquery.v3._Position.prototype = {
    top: null,
    left: null
}


Type.registerNamespace('js.com.jquery.v3.Objects');

////////////////////////////////////////////////////////////////////////////////
// js.com.jquery.v3.Objects._Size

js.com.jquery.v3.Objects._Size = function js_com_jquery_v3_Objects__Size() {
    /// <field name="width" type="Number">
    /// </field>
    /// <field name="height" type="Number">
    /// </field>
}
js.com.jquery.v3.Objects._Size.prototype = {
    width: null,
    height: null
}


js.com.jquery.v3._jQueryScrollToSettings.registerClass('js.com.jquery.v3._jQueryScrollToSettings');
js.com.jquery.v3._jQueryPopupWindowOptions.registerClass('js.com.jquery.v3._jQueryPopupWindowOptions');
js.com.jquery.v3._jQueryUIResizeableOptionsEventUI.registerClass('js.com.jquery.v3._jQueryUIResizeableOptionsEventUI');
js.com.jquery.v3._jQueryUIResizeableOptions.registerClass('js.com.jquery.v3._jQueryUIResizeableOptions');
js.com.jquery.v3._jQueryUIDraggableOptionsEventUI.registerClass('js.com.jquery.v3._jQueryUIDraggableOptionsEventUI');
js.com.jquery.v3._jQueryUIDraggableOptions.registerClass('js.com.jquery.v3._jQueryUIDraggableOptions');
js.com.jquery.v3._jQueryQTipOptionsEvents.registerClass('js.com.jquery.v3._jQueryQTipOptionsEvents');
js.com.jquery.v3._jQueryQTipOptionsStyle.registerClass('js.com.jquery.v3._jQueryQTipOptionsStyle');
js.com.jquery.v3._jQueryQTipOptionsShow.registerClass('js.com.jquery.v3._jQueryQTipOptionsShow');
js.com.jquery.v3._jQueryQTipOptionsHide.registerClass('js.com.jquery.v3._jQueryQTipOptionsHide');
js.com.jquery.v3._jQueryQTipOptionsAdjust.registerClass('js.com.jquery.v3._jQueryQTipOptionsAdjust');
js.com.jquery.v3._jQueryQTipOptionsContent.registerClass('js.com.jquery.v3._jQueryQTipOptionsContent');
js.com.jquery.v3._jQueryQTipOptionsPosition.registerClass('js.com.jquery.v3._jQueryQTipOptionsPosition');
js.com.jquery.v3._jQueryQTipOptionsTitle.registerClass('js.com.jquery.v3._jQueryQTipOptionsTitle');
js.com.jquery.v3._jQueryQTipOptions.registerClass('js.com.jquery.v3._jQueryQTipOptions');
js.com.jquery.v3._jQueryFullCalendarOptionsEventObject.registerClass('js.com.jquery.v3._jQueryFullCalendarOptionsEventObject');
js.com.jquery.v3._jQueryFullCalendarOptionsViewObject.registerClass('js.com.jquery.v3._jQueryFullCalendarOptionsViewObject');
js.com.jquery.v3._jQueryFullCalendarOptionsbuttonIconsObject.registerClass('js.com.jquery.v3._jQueryFullCalendarOptionsbuttonIconsObject');
js.com.jquery.v3._jQueryFullCalendarOptionsHeader.registerClass('js.com.jquery.v3._jQueryFullCalendarOptionsHeader');
js.com.jquery.v3._jQueryFullCalendarOptionsButtonText.registerClass('js.com.jquery.v3._jQueryFullCalendarOptionsButtonText');
js.com.jquery.v3._jQueryFullCalendarOptions.registerClass('js.com.jquery.v3._jQueryFullCalendarOptions');
js.com.jquery.v3._jQueryMultiSelectParams.registerClass('js.com.jquery.v3._jQueryMultiSelectParams');
js.com.jquery.v3._jQueryFX.registerClass('js.com.jquery.v3._jQueryFX');
js.com.jquery.v3._jAnimateExtraParams.registerClass('js.com.jquery.v3._jAnimateExtraParams');
js.com.jquery.v3._jQueryUIDatePickerInstance.registerClass('js.com.jquery.v3._jQueryUIDatePickerInstance');
js.com.jquery.v3._jQueryUIDatePickerOptions.registerClass('js.com.jquery.v3._jQueryUIDatePickerOptions');
js.com.jquery.v3._JScrollPaneOptions.registerClass('js.com.jquery.v3._JScrollPaneOptions');
js.com.jquery.v3._jEditableSettings.registerClass('js.com.jquery.v3._jEditableSettings');
js.com.jquery.v3._jCropAPI.registerClass('js.com.jquery.v3._jCropAPI');
js.com.jquery.v3._JCropCoords.registerClass('js.com.jquery.v3._JCropCoords');
js.com.jquery.v3._JCropOptions.registerClass('js.com.jquery.v3._JCropOptions');
js.com.jquery.v3._ShadowBoxObjectParams.registerClass('js.com.jquery.v3._ShadowBoxObjectParams');
js.com.jquery.v3._ShadowBoxOptions.registerClass('js.com.jquery.v3._ShadowBoxOptions');
js.com.jquery.v3._TableDnDOptions.registerClass('js.com.jquery.v3._TableDnDOptions');
js.com.jquery.v3.JQueryEnums.registerClass('js.com.jquery.v3.JQueryEnums');
js.com.jquery.v3.fx.registerClass('js.com.jquery.v3.fx');
js.com.jquery.v3.jQueryUtil.registerClass('js.com.jquery.v3.jQueryUtil');
js.com.jquery.v3._PrettyPhotoSettings.registerClass('js.com.jquery.v3._PrettyPhotoSettings');
js.com.jquery.v3._jQueryUISortableOptionsEventUI.registerClass('js.com.jquery.v3._jQueryUISortableOptionsEventUI');
js.com.jquery.v3._jQueryUISortableOptions.registerClass('js.com.jquery.v3._jQueryUISortableOptions');
js.com.jquery.v3._UploadifySettings.registerClass('js.com.jquery.v3._UploadifySettings');
js.com.jquery.v3._UploadifyFileObject.registerClass('js.com.jquery.v3._UploadifyFileObject');
js.com.jquery.v3._UploadifyQueueData.registerClass('js.com.jquery.v3._UploadifyQueueData');
js.com.jquery.v3._UploadifySelectOnceData.registerClass('js.com.jquery.v3._UploadifySelectOnceData', js.com.jquery.v3._UploadifyQueueData);
js.com.jquery.v3._UploadifyErrorData.registerClass('js.com.jquery.v3._UploadifyErrorData');
js.com.jquery.v3._UploadifyProgressData.registerClass('js.com.jquery.v3._UploadifyProgressData');
js.com.jquery.v3._UploadifyCompleteData.registerClass('js.com.jquery.v3._UploadifyCompleteData');
js.com.jquery.v3._UploadifyCompleteAllData.registerClass('js.com.jquery.v3._UploadifyCompleteAllData');
js.com.jquery.v3._CookieOptions.registerClass('js.com.jquery.v3._CookieOptions');
js.com.jquery.v3._LightboxConfig.registerClass('js.com.jquery.v3._LightboxConfig');
js.com.jquery.v3._DialogOptions.registerClass('js.com.jquery.v3._DialogOptions');
js.com.jquery.v3._BrowserOptions.registerClass('js.com.jquery.v3._BrowserOptions');
js.com.jquery.v3._BrowserSupportOptions.registerClass('js.com.jquery.v3._BrowserSupportOptions');
js.com.jquery.v3._JsonNameValue.registerClass('js.com.jquery.v3._JsonNameValue');
js.com.jquery.v3._AjaxOptions.registerClass('js.com.jquery.v3._AjaxOptions');
js.com.jquery.v3._Position.registerClass('js.com.jquery.v3._Position');
js.com.jquery.v3.Objects._Size.registerClass('js.com.jquery.v3.Objects._Size');
js.com.jquery.v3.jQueryUtil._onReadyCallbackCalled = false;
js.com.jquery.v3.jQueryUtil._jQueryFactory = null;
js.com.jquery.v3._UploadifySettings.defaulT_CANCEL_IMAGE = '/_common/js/jQuery/plugins/uploadify/2.1.0/images/cancel.jpg';
