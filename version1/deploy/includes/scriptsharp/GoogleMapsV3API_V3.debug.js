// GoogleMapsApi.js
//


Type.registerNamespace('js.com.cs.googlemaps.v3.Classes');

////////////////////////////////////////////////////////////////////////////////
// js.com.cs.googlemaps.v3.Classes.CSGeocoderRequest

js.com.cs.googlemaps.v3.Classes.CSGeocoderRequest = function js_com_cs_googlemaps_v3_Classes_CSGeocoderRequest(address, recursive, countryCode, callback, obj) {
    /// <param name="address" type="String">
    /// </param>
    /// <param name="recursive" type="Boolean">
    /// </param>
    /// <param name="countryCode" type="String">
    /// </param>
    /// <param name="callback" type="js.com.cs.googlemaps.v3.Classes.Function">
    /// </param>
    /// <param name="obj" type="Object">
    /// </param>
    /// <field name="_originalAddress" type="String">
    /// </field>
    /// <field name="_currentAddress" type="String">
    /// </field>
    /// <field name="_countryCode" type="String">
    /// </field>
    /// <field name="_recursive" type="Boolean">
    /// </field>
    /// <field name="_obj" type="Object">
    /// </field>
    /// <field name="_callBack" type="js.com.cs.googlemaps.v3.Classes.Function">
    /// </field>
    /// <field name="_geoCoder" type="google.maps.Geocoder">
    /// </field>
    /// <field name="_cancelled" type="Boolean">
    /// </field>
    this._geoCoder = new google.maps.Geocoder();
    if (ss.isNullOrUndefined(callback)) {
        callback = countryCode;
    }
    this._obj = obj;
    this._originalAddress = address;
    this._recursive = recursive;
    this._callBack = callback;
    this._countryCode = countryCode;
    this._init();
}
js.com.cs.googlemaps.v3.Classes.CSGeocoderRequest.prototype = {
    _originalAddress: null,
    _currentAddress: null,
    _countryCode: null,
    _recursive: false,
    _obj: null,
    
    get_obj: function js_com_cs_googlemaps_v3_Classes_CSGeocoderRequest$get_obj() {
        /// <value type="Object"></value>
        return this._obj;
    },
    set_obj: function js_com_cs_googlemaps_v3_Classes_CSGeocoderRequest$set_obj(value) {
        /// <value type="Object"></value>
        this._obj = value;
        return value;
    },
    
    _callBack: null,
    _cancelled: false,
    
    addressGeocodeComplete: function js_com_cs_googlemaps_v3_Classes_CSGeocoderRequest$addressGeocodeComplete(results, status) {
        /// <param name="results" type="Array" elementType="_GeocoderResponse">
        /// </param>
        /// <param name="status" type="google.maps.GeocoderStatus">
        /// </param>
        if (this._cancelled) {
            return;
        }
        if (status === google.maps.GeocoderStatus.OK) {
            this._callBack(this, results, status);
        }
        else {
            this._checkNextAddress();
        }
    },
    
    _checkNextAddress: function js_com_cs_googlemaps_v3_Classes_CSGeocoderRequest$_checkNextAddress() {
        if (this._cancelled) {
            return;
        }
        if (this._currentAddress == null) {
            this._currentAddress = this._originalAddress;
        }
        else {
            if (this._currentAddress.indexOf(',') !== -1) {
                this._currentAddress = this._currentAddress.substring(this._currentAddress.indexOf(',') + 1, this._currentAddress.length);
                this._currentAddress = this._currentAddress.trim();
            }
            else {
                this._callBack(this, null, google.maps.GeocoderStatus.ZERO_RESULTS);
                return;
            }
        }
        var r = {};
        r.address = this._currentAddress;
        r.country = this._countryCode;
        this._geoCoder.geocode(r, ss.Delegate.create(this, this.addressGeocodeComplete));
    },
    
    _init: function js_com_cs_googlemaps_v3_Classes_CSGeocoderRequest$_init() {
        this._checkNextAddress();
    },
    
    cancel: function js_com_cs_googlemaps_v3_Classes_CSGeocoderRequest$cancel() {
        this._cancelled = true;
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cs.googlemaps.v3.Classes.CSInfoWindowWithMarker

js.com.cs.googlemaps.v3.Classes.CSInfoWindowWithMarker = function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker(marker, infoWindow, toggleOnClick, openOnRollover, hideOnRollout) {
    /// <param name="marker" type="js.com.cs.googlemaps.v3.Classes.CSMarker">
    /// </param>
    /// <param name="infoWindow" type="js.com.cs.googlemaps.v3.Classes.CSInfoWindow">
    /// </param>
    /// <param name="toggleOnClick" type="Boolean">
    /// </param>
    /// <param name="openOnRollover" type="Boolean">
    /// </param>
    /// <param name="hideOnRollout" type="Boolean">
    /// </param>
    /// <field name="_toggleOnClick" type="Boolean">
    /// </field>
    /// <field name="_openOnRollover" type="Boolean">
    /// </field>
    /// <field name="_hideOnRollout" type="Boolean">
    /// </field>
    /// <field name="_marker" type="js.com.cs.googlemaps.v3.Classes.CSMarker">
    /// </field>
    /// <field name="_infoWindow" type="js.com.cs.googlemaps.v3.Classes.CSInfoWindow">
    /// </field>
    this._marker = marker;
    this._infoWindow = infoWindow;
    this._openOnRollover = openOnRollover;
    this._toggleOnClick = toggleOnClick;
    this._hideOnRollout = hideOnRollout;
    this._marker.add_onClick(ss.Delegate.create(this, this._marker_onClick));
    this._marker.add_onMouseOver(ss.Delegate.create(this, this._marker_onMouseOver));
    this._marker.add_onMouseOut(ss.Delegate.create(this, this._marker_onMouseOut));
}
js.com.cs.googlemaps.v3.Classes.CSInfoWindowWithMarker.prototype = {
    _toggleOnClick: false,
    
    get_toggleOnClick: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$get_toggleOnClick() {
        /// <value type="Boolean"></value>
        return this._toggleOnClick;
    },
    set_toggleOnClick: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$set_toggleOnClick(value) {
        /// <value type="Boolean"></value>
        this._toggleOnClick = value;
        return value;
    },
    
    _openOnRollover: false,
    
    get_openOnRollover: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$get_openOnRollover() {
        /// <value type="Boolean"></value>
        return this._openOnRollover;
    },
    set_openOnRollover: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$set_openOnRollover(value) {
        /// <value type="Boolean"></value>
        this._openOnRollover = value;
        return value;
    },
    
    _hideOnRollout: false,
    
    get_hideOnRollout: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$get_hideOnRollout() {
        /// <value type="Boolean"></value>
        return this._hideOnRollout;
    },
    set_hideOnRollout: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$set_hideOnRollout(value) {
        /// <value type="Boolean"></value>
        this._hideOnRollout = value;
        return value;
    },
    
    _marker: null,
    
    get_marker: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$get_marker() {
        /// <value type="js.com.cs.googlemaps.v3.Classes.CSMarker"></value>
        return this._marker;
    },
    set_marker: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$set_marker(value) {
        /// <value type="js.com.cs.googlemaps.v3.Classes.CSMarker"></value>
        this._marker = value;
        return value;
    },
    
    _infoWindow: null,
    
    get_infoWindow: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$get_infoWindow() {
        /// <value type="js.com.cs.googlemaps.v3.Classes.CSInfoWindow"></value>
        return this._infoWindow;
    },
    set_infoWindow: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$set_infoWindow(value) {
        /// <value type="js.com.cs.googlemaps.v3.Classes.CSInfoWindow"></value>
        this._infoWindow = value;
        return value;
    },
    
    _marker_onMouseOut: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$_marker_onMouseOut() {
        if (this._hideOnRollout) {
            this.close();
        }
    },
    
    _marker_onMouseOver: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$_marker_onMouseOver() {
        js.com.cs.v3.Util.Console.log('OVER');
        js.com.cs.v3.Util.Console.log(this._openOnRollover);
        if (this._openOnRollover) {
            this.open();
        }
    },
    
    _marker_onClick: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$_marker_onClick() {
        if (this._toggleOnClick) {
            this.toggle();
        }
    },
    
    close: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$close() {
        this._infoWindow.close();
    },
    
    open: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$open() {
        this._infoWindow.open(this._marker.get_marker().map, this._marker.get_marker());
    },
    
    toggle: function js_com_cs_googlemaps_v3_Classes_CSInfoWindowWithMarker$toggle() {
        this._infoWindow.toggle(this._marker.get_marker().map, this._marker.get_marker());
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cs.googlemaps.v3.Classes.CSEventListenerMapsHandler

js.com.cs.googlemaps.v3.Classes.CSEventListenerMapsHandler = function js_com_cs_googlemaps_v3_Classes_CSEventListenerMapsHandler() {
    /// <field name="_delegates" type="Array" elementType="Delegate">
    /// </field>
    /// <field name="_eventListenerHandlers" type="Array" elementType="EventListenerMaps">
    /// </field>
    this._delegates = [];
    this._eventListenerHandlers = [];
}
js.com.cs.googlemaps.v3.Classes.CSEventListenerMapsHandler.prototype = {
    _delegates: null,
    _eventListenerHandlers: null,
    
    addEventListener: function js_com_cs_googlemaps_v3_Classes_CSEventListenerMapsHandler$addEventListener(handler, eventListenerHandler) {
        /// <summary>
        /// Add a google event handler
        /// </summary>
        /// <param name="handler" type="ss.Delegate">
        /// </param>
        /// <param name="eventListenerHandler" type="google.maps.EventListenerMaps">
        /// </param>
        this._delegates[this._delegates.length] = handler;
        this._eventListenerHandlers[this._eventListenerHandlers.length] = eventListenerHandler;
    },
    
    removeEventListener: function js_com_cs_googlemaps_v3_Classes_CSEventListenerMapsHandler$removeEventListener(handler) {
        /// <summary>
        /// Remove a google event by specifying handler
        /// </summary>
        /// <param name="handler" type="ss.Delegate">
        /// </param>
        for (var i = 0; i < this._delegates.length; i++) {
            var targetsA = handler._targets;
            var targetsB = this._delegates[i]._targets;
            if (js.com.cs.v3.Util.GeneralUtil.compareDelegate(handler, this._delegates[i])) {
                google.maps.GoogleEvent.removeListener(this._eventListenerHandlers[i]);
                var a = this._delegates;
                var b = this._eventListenerHandlers;
                a.removeAt(i);
                b.removeAt(i);
                i--;
            }
        }
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cs.googlemaps.v3.Classes.CSInfoWindow

js.com.cs.googlemaps.v3.Classes.CSInfoWindow = function js_com_cs_googlemaps_v3_Classes_CSInfoWindow(options) {
    /// <param name="options" type="google.maps._InfoWindowOptions">
    /// </param>
    /// <field name="_eventHandlers" type="js.com.cs.googlemaps.v3.Classes.CSEventListenerMapsHandler">
    /// </field>
    /// <field name="_infoWindow" type="google.maps.InfoWindow">
    /// </field>
    /// <field name="_shown" type="Boolean">
    /// </field>
    /// <field name="__onOpen" type="Function">
    /// </field>
    /// <field name="__onClose" type="Function">
    /// </field>
    this._eventHandlers = new js.com.cs.googlemaps.v3.Classes.CSEventListenerMapsHandler();
    if (ss.isNullOrUndefined(options)) {
        throw new Error('Please specify options');
    }
    this._infoWindow = new google.maps.InfoWindow(options);
    this.add_onCloseClick(ss.Delegate.create(this, this._csInfoWindow_onCloseClick));
}
js.com.cs.googlemaps.v3.Classes.CSInfoWindow.prototype = {
    _infoWindow: null,
    _shown: false,
    
    get_shown: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$get_shown() {
        /// <value type="Boolean"></value>
        return this._shown;
    },
    set_shown: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$set_shown(value) {
        /// <value type="Boolean"></value>
        this._shown = value;
        return value;
    },
    
    get_infoWindow: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$get_infoWindow() {
        /// <value type="google.maps.InfoWindow"></value>
        return this._infoWindow;
    },
    
    _csInfoWindow_onCloseClick: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$_csInfoWindow_onCloseClick() {
        this._shown = false;
    },
    
    open: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$open(map, anchor) {
        /// <summary>
        /// Opens this InfoWindow on the given map.
        /// </summary>
        /// <param name="map" type="google.maps.Map">
        /// Map
        /// </param>
        /// <param name="anchor" type="google.maps.IGoogleObject">
        /// Anchor to position window on (usually marker)
        /// </param>
        if (!this._shown) {
            this._shown = true;
            this._infoWindow.open(map, anchor);
            if (this.__onOpen != null) {
                this.__onOpen(this, null);
            }
        }
    },
    
    toggle: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$toggle(map, anchor) {
        /// <summary>
        /// Toggles this InfoWindow
        /// </summary>
        /// <param name="map" type="google.maps.Map">
        /// </param>
        /// <param name="anchor" type="google.maps.IGoogleObject">
        /// </param>
        if (this._shown) {
            this.close();
        }
        else {
            this.open(map, anchor);
        }
    },
    
    close: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$close() {
        /// <summary>
        /// Close the window
        /// </summary>
        if (this._shown) {
            this._shown = false;
            this._infoWindow.close();
            if (this.__onClose != null) {
                this.__onClose(this, null);
            }
        }
    },
    
    add_onOpen: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$add_onOpen(value) {
        /// <param name="value" type="Function" />
        this.__onOpen = ss.Delegate.combine(this.__onOpen, value);
    },
    remove_onOpen: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$remove_onOpen(value) {
        /// <param name="value" type="Function" />
        this.__onOpen = ss.Delegate.remove(this.__onOpen, value);
    },
    
    __onOpen: null,
    
    add_onClose: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$add_onClose(value) {
        /// <param name="value" type="Function" />
        this.__onClose = ss.Delegate.combine(this.__onClose, value);
    },
    remove_onClose: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$remove_onClose(value) {
        /// <param name="value" type="Function" />
        this.__onClose = ss.Delegate.remove(this.__onClose, value);
    },
    
    __onClose: null,
    
    add_onCloseClick: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$add_onCloseClick(value) {
        /// <summary>
        /// This event is fired when the close button was clicked.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_infoWindow(), 'closeclick', value));
    },
    remove_onCloseClick: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$remove_onCloseClick(value) {
        /// <summary>
        /// This event is fired when the close button was clicked.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onContentChanged: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$add_onContentChanged(value) {
        /// <summary>
        /// This event is fired when the content property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_infoWindow(), 'content_changed', value));
    },
    remove_onContentChanged: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$remove_onContentChanged(value) {
        /// <summary>
        /// This event is fired when the content property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onPositionChanged: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$add_onPositionChanged(value) {
        /// <summary>
        /// This event is fired when the position property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_infoWindow(), 'position_changed', value));
    },
    remove_onPositionChanged: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$remove_onPositionChanged(value) {
        /// <summary>
        /// This event is fired when the position property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onSizeChanged: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$add_onSizeChanged(value) {
        /// <summary>
        /// This event is fired when the size property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_infoWindow(), 'size_changed', value));
    },
    remove_onSizeChanged: function js_com_cs_googlemaps_v3_Classes_CSInfoWindow$remove_onSizeChanged(value) {
        /// <summary>
        /// This event is fired when the size property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cs.googlemaps.v3.Classes.CSMap

js.com.cs.googlemaps.v3.Classes.CSMap = function js_com_cs_googlemaps_v3_Classes_CSMap(mapDiv, centerPosition, zoomLevel, mapTypeId, confirmVisible) {
    /// <param name="mapDiv" type="Object" domElement="true">
    /// </param>
    /// <param name="centerPosition" type="google.maps.LatLng">
    /// </param>
    /// <param name="zoomLevel" type="Number">
    /// </param>
    /// <param name="mapTypeId" type="google.maps.MapTypeId">
    /// </param>
    /// <param name="confirmVisible" type="Boolean">
    /// If true, the map will be placed at end of DOM element list to make sure it is visible and then on loaded, replaced at original position
    /// </param>
    /// <field name="_eventHandlers" type="js.com.cs.googlemaps.v3.Classes.CSEventListenerMapsHandler">
    /// </field>
    /// <field name="_map" type="google.maps.Map">
    /// </field>
    /// <field name="_initialInvisibleElements" type="Array" elementType="Object" elementDomElement="true">
    /// </field>
    /// <field name="_markers" type="Array" elementType="CSMarker">
    /// </field>
    /// <field name="_initialTilesLoaded" type="Boolean">
    /// </field>
    /// <field name="_initProperties" type="Object">
    /// </field>
    /// <field name="_initMapDomNodeHolder" type="Object" domElement="true">
    /// </field>
    /// <field name="_ElementMap" type="Object" domElement="true">
    /// </field>
    this._eventHandlers = new js.com.cs.googlemaps.v3.Classes.CSEventListenerMapsHandler();
    this._initialInvisibleElements = [];
    this._markers = [];
    this._initProperties = {};
    js.com.cs.googlemaps.v3.Util.GoogleMapsUtil.INIT();
    if (Type.canCast(mapDiv, String)) {
        mapDiv = document.getElementById(mapDiv);
    }
    if (!ss.isNullOrUndefined(mapDiv)) {
        var opts = {};
        if (Type.canCast(centerPosition, google.maps.LatLng)) {
            opts.center = centerPosition;
            opts.zoom = zoomLevel;
            if (ss.isNullOrUndefined(mapTypeId)) {
                mapTypeId = google.maps.MapTypeId.ROADMAP;
            }
            opts.mapTypeId = mapTypeId;
        }
        else {
            opts = centerPosition;
        }
        this.init(mapDiv, opts, confirmVisible);
    }
}
js.com.cs.googlemaps.v3.Classes.CSMap.prototype = {
    _map: null,
    _initialTilesLoaded: false,
    _initMapDomNodeHolder: null,
    _ElementMap: null,
    
    get_markers: function js_com_cs_googlemaps_v3_Classes_CSMap$get_markers() {
        /// <value type="Array" elementType="CSMarker"></value>
        return this._markers;
    },
    
    get_map: function js_com_cs_googlemaps_v3_Classes_CSMap$get_map() {
        /// <value type="google.maps.Map"></value>
        return this._map;
    },
    
    _checkInitialInv22isibleElement: function js_com_cs_googlemaps_v3_Classes_CSMap$_checkInitialInv22isibleElement(element) {
        /// <summary>
        /// If GoogleMaps is initially, hidden, it does not work.  So show all possible parent elements.
        /// </summary>
        /// <param name="element" type="Object" domElement="true">
        /// </param>
        var parent = element.parentNode;
        while (parent != null) {
            if (parent.style != null && parent.style.display === 'none') {
                this._initialInvisibleElements[this._initialInvisibleElements.length] = parent;
                parent.style.display = '';
            }
            parent = parent.parentNode;
        }
    },
    
    _setAbsoluteHiddenPosition: function js_com_cs_googlemaps_v3_Classes_CSMap$_setAbsoluteHiddenPosition(map) {
        /// <param name="map" type="Object" domElement="true">
        /// </param>
        var span = document.createElement('span');
        var mapParentNode = new js.com.cs.v3.Controls.Common.MyElement(map.parentNode);
        mapParentNode.get_jQueryElement().replaceWith(span);
        this._initMapDomNodeHolder = span;
        document.body.appendChild(map);
        var mapElem = new js.com.cs.v3.Controls.Common.MyElement(map);
        var width = mapElem.get_jQueryElement().outerWidth(false);
        map.style.width = width + 'px';
        map.style.height = mapElem.get_jQueryElement().outerHeight(false) + 'px';
        map.style.display = '';
        this._initProperties['left'] = map.style.left;
        this._initProperties['top'] = map.style.top;
        this._initProperties['display'] = map.style.display;
        map.style.display = '';
        map.style.left = -width + 'px';
        map.style.top = '0px';
    },
    
    _reHideInitialInvisibleElements: function js_com_cs_googlemaps_v3_Classes_CSMap$_reHideInitialInvisibleElements() {
        for (var i = 0; i < this._initialInvisibleElements.length; i++) {
            this._initialInvisibleElements[i].style.display = 'none';
        }
    },
    
    init: function js_com_cs_googlemaps_v3_Classes_CSMap$init(Element, opts, confirmVisible) {
        /// <param name="Element" type="Object" domElement="true">
        /// </param>
        /// <param name="opts" type="google.maps._MapOptions">
        /// </param>
        /// <param name="confirmVisible" type="Boolean">
        /// This will MAKE SURE that map is visible by placing it at end of body tag and then replacing when ready
        /// </param>
        if (ss.isNullOrUndefined(opts)) {
            throw new Error('Please specify options');
        }
        if (opts.center == null) {
            throw new Error('Map Center LatLng is required');
        }
        if (opts.zoom == null) {
            throw new Error('Map zoom level is required');
        }
        if (opts.mapTypeId == null) {
            opts.mapTypeId = google.maps.MapTypeId.ROADMAP;
        }
        if (confirmVisible) {
            this._setAbsoluteHiddenPosition(Element);
        }
        this._map = new google.maps.Map(Element, opts);
        this._ElementMap = Element;
        this.add_onTilesLoaded(ss.Delegate.create(this, this._csMap_onTilesLoaded));
    },
    
    _csMap_onTilesLoaded: function js_com_cs_googlemaps_v3_Classes_CSMap$_csMap_onTilesLoaded() {
        /// <summary>
        /// If it is initially invisible, re hide
        /// </summary>
        if (!this._initialTilesLoaded) {
            this._initialTilesLoaded = true;
            this.remove_onTilesLoaded(ss.Delegate.create(this, this._csMap_onTilesLoaded));
            if (this._initMapDomNodeHolder != null) {
                js.com.cs.v3.Util.DOMUtil.replaceNodeInDom(this._ElementMap, this._initMapDomNodeHolder);
                this._ElementMap.style.left = this._initProperties['left'];
                this._ElementMap.style.top = this._initProperties['top'];
                this._ElementMap.style.display = this._initProperties['display'];
            }
            else {
                this._reHideInitialInvisibleElements();
            }
        }
    },
    
    addMarker: function js_com_cs_googlemaps_v3_Classes_CSMap$addMarker(position, title) {
        /// <param name="position" type="google.maps.LatLng">
        /// </param>
        /// <param name="title" type="String">
        /// </param>
        /// <returns type="js.com.cs.googlemaps.v3.Classes.CSMarker"></returns>
        var options = null;
        if (Type.canCast(position, google.maps.LatLng)) {
            options = {};
            options.position = position;
            options.title = title;
        }
        else {
            options = position;
        }
        options.map = this.get_map();
        var marker = new js.com.cs.googlemaps.v3.Classes.CSMarker(options);
        this._markers[this._markers.length] = marker;
        return marker;
    },
    
    setCenter: function js_com_cs_googlemaps_v3_Classes_CSMap$setCenter(latLng, zoomLevel) {
        /// <param name="latLng" type="google.maps.LatLng">
        /// </param>
        /// <param name="zoomLevel" type="Number">
        /// </param>
        if (ss.isNullOrUndefined(zoomLevel)) {
            zoomLevel = this._map.getZoom();
        }
        var options = {};
        options.center = latLng;
        options.zoom = zoomLevel;
        this._map.setOptions(options);
    },
    
    add_onDragStart: function js_com_cs_googlemaps_v3_Classes_CSMap$add_onDragStart(value) {
        /// <summary>
        /// This event is fired when the user starts dragging the map.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_map(), 'dragstart', value));
    },
    remove_onDragStart: function js_com_cs_googlemaps_v3_Classes_CSMap$remove_onDragStart(value) {
        /// <summary>
        /// This event is fired when the user starts dragging the map.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onDrag: function js_com_cs_googlemaps_v3_Classes_CSMap$add_onDrag(value) {
        /// <summary>
        /// This event is repeatedly fired while the user drags the map.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_map(), 'drag', value));
    },
    remove_onDrag: function js_com_cs_googlemaps_v3_Classes_CSMap$remove_onDrag(value) {
        /// <summary>
        /// This event is repeatedly fired while the user drags the map.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onDragEnd: function js_com_cs_googlemaps_v3_Classes_CSMap$add_onDragEnd(value) {
        /// <summary>
        /// This event is fired when the user stops dragging the map.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_map(), 'dragend', value));
    },
    remove_onDragEnd: function js_com_cs_googlemaps_v3_Classes_CSMap$remove_onDragEnd(value) {
        /// <summary>
        /// This event is fired when the user stops dragging the map.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onClick: function js_com_cs_googlemaps_v3_Classes_CSMap$add_onClick(value) {
        /// <summary>
        /// This event is fired when the user clicks on the map (but not when they click on a marker or infowindow).
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_map(), 'click', value));
    },
    remove_onClick: function js_com_cs_googlemaps_v3_Classes_CSMap$remove_onClick(value) {
        /// <summary>
        /// This event is fired when the user clicks on the map (but not when they click on a marker or infowindow).
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onDblClick: function js_com_cs_googlemaps_v3_Classes_CSMap$add_onDblClick(value) {
        /// <summary>
        /// This event is fired when the user double-clicks on the map. Note that the click event will also fire, right before this one.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_map(), 'dblclick', value));
    },
    remove_onDblClick: function js_com_cs_googlemaps_v3_Classes_CSMap$remove_onDblClick(value) {
        /// <summary>
        /// This event is fired when the user double-clicks on the map. Note that the click event will also fire, right before this one.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onRightClick: function js_com_cs_googlemaps_v3_Classes_CSMap$add_onRightClick(value) {
        /// <summary>
        /// This event is fired when the DOM contextmenu event is fired on the map container.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_map(), 'rightclick', value));
    },
    remove_onRightClick: function js_com_cs_googlemaps_v3_Classes_CSMap$remove_onRightClick(value) {
        /// <summary>
        /// This event is fired when the DOM contextmenu event is fired on the map container.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onTilesLoaded: function js_com_cs_googlemaps_v3_Classes_CSMap$add_onTilesLoaded(value) {
        /// <summary>
        /// This event is fired when the visible tiles have finished loading.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_map(), 'tilesloaded', value));
    },
    remove_onTilesLoaded: function js_com_cs_googlemaps_v3_Classes_CSMap$remove_onTilesLoaded(value) {
        /// <summary>
        /// This event is fired when the visible tiles have finished loading.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onBoundsChanged: function js_com_cs_googlemaps_v3_Classes_CSMap$add_onBoundsChanged(value) {
        /// <summary>
        /// This event is fired when the viewport bounds have changed.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_map(), 'bounds_changed', value));
    },
    remove_onBoundsChanged: function js_com_cs_googlemaps_v3_Classes_CSMap$remove_onBoundsChanged(value) {
        /// <summary>
        /// This event is fired when the viewport bounds have changed.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onCenterChanged: function js_com_cs_googlemaps_v3_Classes_CSMap$add_onCenterChanged(value) {
        /// <summary>
        /// This event is fired when the map center property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_map(), 'center_changed', value));
    },
    remove_onCenterChanged: function js_com_cs_googlemaps_v3_Classes_CSMap$remove_onCenterChanged(value) {
        /// <summary>
        /// This event is fired when the map center property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onZoomChanged: function js_com_cs_googlemaps_v3_Classes_CSMap$add_onZoomChanged(value) {
        /// <summary>
        /// This event is fired when the map zoom property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_map(), 'zoom_changed', value));
    },
    remove_onZoomChanged: function js_com_cs_googlemaps_v3_Classes_CSMap$remove_onZoomChanged(value) {
        /// <summary>
        /// This event is fired when the map zoom property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onMapTypeIdChanged: function js_com_cs_googlemaps_v3_Classes_CSMap$add_onMapTypeIdChanged(value) {
        /// <summary>
        /// This event is fired when the mapTypeId property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_map(), 'mapTypeId_changed', value));
    },
    remove_onMapTypeIdChanged: function js_com_cs_googlemaps_v3_Classes_CSMap$remove_onMapTypeIdChanged(value) {
        /// <summary>
        /// This event is fired when the mapTypeId property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cs.googlemaps.v3.Classes.CSMapTest

js.com.cs.googlemaps.v3.Classes.CSMapTest = function js_com_cs_googlemaps_v3_Classes_CSMapTest(id) {
    /// <param name="id" type="String">
    /// </param>
    /// <field name="_map" type="js.com.cs.googlemaps.v3.Classes.CSMap">
    /// </field>
    var opts = {};
    opts.center = new google.maps.LatLng(35.8268343585429, 14.4801306724548);
    opts.zoom = 13;
    opts.mapTypeId = google.maps.MapTypeId.ROADMAP;
    var map = new js.com.cs.googlemaps.v3.Classes.CSMap(document.getElementById(id), opts);
    var marker = map.addMarker(opts.center, 'Hello There!');
    map.add_onClick(ss.Delegate.create(this, this._map_onClick));
    this._map = map;
    marker.setInfoWindow('Hi My name is mark i am testing!', new google.maps.Size(300, 20), true, true, false);
    var geoCoder = new google.maps.Geocoder();
    var request = {};
    request.address = 'Pieta';
    request.country = 'mt';
    geoCoder.geocode(request, ss.Delegate.create(this, this._geocoderRequestLoaded));
}
js.com.cs.googlemaps.v3.Classes.CSMapTest.prototype = {
    _map: null,
    
    _geocoderRequestLoaded: function js_com_cs_googlemaps_v3_Classes_CSMapTest$_geocoderRequestLoaded(results, status) {
        /// <param name="results" type="Array" elementType="_GeocoderResponse">
        /// </param>
        /// <param name="status" type="google.maps.GeocoderStatus">
        /// </param>
        for (var i = 0; i < results.length; i++) {
            var response = results[i];
            var options = {};
            this._map.addMarker(response.geometry.location, response.formatted_address);
            this._map.setCenter(response.geometry.location, 13);
        }
    },
    
    _map_onClick: function js_com_cs_googlemaps_v3_Classes_CSMapTest$_map_onClick(eventArgs) {
        /// <param name="eventArgs" type="google.maps._MouseEvent">
        /// </param>
        console.log(latLng);
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cs.googlemaps.v3.Classes.CSMarker

js.com.cs.googlemaps.v3.Classes.CSMarker = function js_com_cs_googlemaps_v3_Classes_CSMarker(opts) {
    /// <param name="opts" type="google.maps._MarkerOptions">
    /// </param>
    /// <field name="_eventHandlers" type="js.com.cs.googlemaps.v3.Classes.CSEventListenerMapsHandler">
    /// </field>
    /// <field name="_marker" type="google.maps.Marker">
    /// </field>
    /// <field name="_markerOptions" type="google.maps._MarkerOptions">
    /// </field>
    /// <field name="__onClickRef" type="Function">
    /// </field>
    this._eventHandlers = new js.com.cs.googlemaps.v3.Classes.CSEventListenerMapsHandler();
    js.com.cs.googlemaps.v3.Util.GoogleMapsUtil.INIT();
    if (!ss.isNullOrUndefined(opts)) {
        this.init(opts);
    }
}
js.com.cs.googlemaps.v3.Classes.CSMarker.prototype = {
    _marker: null,
    _markerOptions: null,
    
    get_marker: function js_com_cs_googlemaps_v3_Classes_CSMarker$get_marker() {
        /// <value type="google.maps.Marker"></value>
        return this._marker;
    },
    
    init: function js_com_cs_googlemaps_v3_Classes_CSMarker$init(opts) {
        /// <param name="opts" type="google.maps._MarkerOptions">
        /// </param>
        this._markerOptions = opts;
        if (ss.isNullOrUndefined(opts)) {
            throw new Error('Please specify options');
        }
        if (opts.position == null) {
            throw new Error('Position LatLng for marker is required');
        }
        this._marker = new google.maps.Marker(opts);
        this._initHandlers();
    },
    
    _initHandlers: function js_com_cs_googlemaps_v3_Classes_CSMarker$_initHandlers() {
        this.add_onClick(ss.Delegate.create(this, this._csMarker_onClick));
    },
    
    _csMarker_onClick: function js_com_cs_googlemaps_v3_Classes_CSMarker$_csMarker_onClick() {
        if (this.__onClickRef != null) {
            this.__onClickRef(this, null);
        }
    },
    
    setInfoWindow: function js_com_cs_googlemaps_v3_Classes_CSMarker$setInfoWindow(content, size, toggleOnClick, openOnRollover, hideOnRollout) {
        /// <param name="content" type="String">
        /// </param>
        /// <param name="size" type="google.maps.Size">
        /// </param>
        /// <param name="toggleOnClick" type="Boolean">
        /// </param>
        /// <param name="openOnRollover" type="Boolean">
        /// </param>
        /// <param name="hideOnRollout" type="Boolean">
        /// </param>
        /// <returns type="js.com.cs.googlemaps.v3.Classes.CSInfoWindowWithMarker"></returns>
        var options = {};
        if (Type.canCast(size, google.maps.Size)) {
            options.size = size;
        }
        options.content = content;
        var infoWindow = new js.com.cs.googlemaps.v3.Classes.CSInfoWindow(options);
        var markerAndWindow = new js.com.cs.googlemaps.v3.Classes.CSInfoWindowWithMarker(this, infoWindow, toggleOnClick, openOnRollover, hideOnRollout);
        return markerAndWindow;
    },
    
    add_onClickRef: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onClickRef(value) {
        /// <param name="value" type="Function" />
        this.__onClickRef = ss.Delegate.combine(this.__onClickRef, value);
    },
    remove_onClickRef: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onClickRef(value) {
        /// <param name="value" type="Function" />
        this.__onClickRef = ss.Delegate.remove(this.__onClickRef, value);
    },
    
    __onClickRef: null,
    
    add_onClick: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onClick(value) {
        /// <summary>
        /// This event is fired when the marker icon was clicked.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'click', value));
    },
    remove_onClick: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onClick(value) {
        /// <summary>
        /// This event is fired when the marker icon was clicked.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onMouseUp: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onMouseUp(value) {
        /// <summary>
        /// This event is fired for the DOM mouseup on the marker.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'mouseup', value));
    },
    remove_onMouseUp: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onMouseUp(value) {
        /// <summary>
        /// This event is fired for the DOM mouseup on the marker.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onMouseDown: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onMouseDown(value) {
        /// <summary>
        /// This event is fired when the DOM mousedown event is fired on the marker icon.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'mousedown', value));
    },
    remove_onMouseDown: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onMouseDown(value) {
        /// <summary>
        /// This event is fired when the DOM mousedown event is fired on the marker icon.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onMouseOver: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onMouseOver(value) {
        /// <summary>
        /// This event is fired when the mouse enters the area of the marker icon.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'mouseover', value));
    },
    remove_onMouseOver: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onMouseOver(value) {
        /// <summary>
        /// This event is fired when the mouse enters the area of the marker icon.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onMouseOut: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onMouseOut(value) {
        /// <summary>
        /// This event is fired when the mouse leaves the area of the marker icon.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'mouseout', value));
    },
    remove_onMouseOut: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onMouseOut(value) {
        /// <summary>
        /// This event is fired when the mouse leaves the area of the marker icon.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onRightClick: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onRightClick(value) {
        /// <summary>
        /// This event is fired when the marker is right clicked on.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'rightclick', value));
    },
    remove_onRightClick: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onRightClick(value) {
        /// <summary>
        /// This event is fired when the marker is right clicked on.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onPositionChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onPositionChanged(value) {
        /// <summary>
        /// This event is fired when the marker position property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'position_changed', value));
    },
    remove_onPositionChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onPositionChanged(value) {
        /// <summary>
        /// This event is fired when the marker position property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onTitleChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onTitleChanged(value) {
        /// <summary>
        /// This event is fired when the marker title property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'title_changed', value));
    },
    remove_onTitleChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onTitleChanged(value) {
        /// <summary>
        /// This event is fired when the marker title property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onIconChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onIconChanged(value) {
        /// <summary>
        /// This event is fired when the marker icon property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'icon_changed', value));
    },
    remove_onIconChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onIconChanged(value) {
        /// <summary>
        /// This event is fired when the marker icon property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onShadowChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onShadowChanged(value) {
        /// <summary>
        /// This event is fired when the marker's shadow property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'shadow_changed', value));
    },
    remove_onShadowChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onShadowChanged(value) {
        /// <summary>
        /// This event is fired when the marker's shadow property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onShapeChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onShapeChanged(value) {
        /// <summary>
        /// This event is fired when the marker's shape property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'shape_changed', value));
    },
    remove_onShapeChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onShapeChanged(value) {
        /// <summary>
        /// This event is fired when the marker's shape property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onCursorChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onCursorChanged(value) {
        /// <summary>
        /// This event is fired when the marker's cursor property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'cursor_changed', value));
    },
    remove_onCursorChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onCursorChanged(value) {
        /// <summary>
        /// This event is fired when the marker's cursor property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onClickableChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onClickableChanged(value) {
        /// <summary>
        /// This event is fired when the marker's clickable property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'clickable_changed', value));
    },
    remove_onClickableChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onClickableChanged(value) {
        /// <summary>
        /// This event is fired when the marker's clickable property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onVisibleChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onVisibleChanged(value) {
        /// <summary>
        /// This event is fired when the marker's visible property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'visible_changed', value));
    },
    remove_onVisibleChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onVisibleChanged(value) {
        /// <summary>
        /// This event is fired when the marker's visible property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onFlatChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onFlatChanged(value) {
        /// <summary>
        /// This event is fired when the marker's flat property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'flat_changed', value));
    },
    remove_onFlatChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onFlatChanged(value) {
        /// <summary>
        /// This event is fired when the marker's flat property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onZIndexChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onZIndexChanged(value) {
        /// <summary>
        /// This event is fired when the marker's zIndex property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'zIndex_changed', value));
    },
    remove_onZIndexChanged: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onZIndexChanged(value) {
        /// <summary>
        /// This event is fired when the marker's zIndex property changes.
        /// </summary>
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onDrag: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onDrag(value) {
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'drag', value));
    },
    remove_onDrag: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onDrag(value) {
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onDragEnd: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onDragEnd(value) {
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'dragend', value));
    },
    remove_onDragEnd: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onDragEnd(value) {
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    },
    
    add_onDragStart: function js_com_cs_googlemaps_v3_Classes_CSMarker$add_onDragStart(value) {
        /// <param name="value" type="Function" />
        this._eventHandlers.addEventListener(value, google.maps.GoogleEvent.addListener(this.get_marker(), 'dragstart', value));
    },
    remove_onDragStart: function js_com_cs_googlemaps_v3_Classes_CSMarker$remove_onDragStart(value) {
        /// <param name="value" type="Function" />
        this._eventHandlers.removeEventListener(value);
    }
}


Type.registerNamespace('google.maps');

////////////////////////////////////////////////////////////////////////////////
// google.maps.Test

google.maps.Test = function google_maps_Test() {
    var options = {};
    options.mapTypeId = google.maps.MapTypeId.ROADMAP;
    var m = new google.maps.Map(null, options);
    var type = m.get_mapTypeId();
    var g = new google.maps.Geocoder();
    var obj = {};
    g.geocode(obj, ss.Delegate.create(this, this.geocodeResultsLoaded));
    google.maps.GoogleEvent.addListener(null, null, null);
}
google.maps.Test.prototype = {
    
    geocodeResultsLoaded: function google_maps_Test$geocodeResultsLoaded(resulsts, status) {
        /// <param name="resulsts" type="Array" elementType="_GeocoderResponse">
        /// </param>
        /// <param name="status" type="google.maps.GeocoderStatus">
        /// </param>
    }
}


Type.registerNamespace('js.com.cs.googlemaps.v3.Util');

////////////////////////////////////////////////////////////////////////////////
// js.com.cs.googlemaps.v3.Util.GoogleMapsUtil

js.com.cs.googlemaps.v3.Util.GoogleMapsUtil = function js_com_cs_googlemaps_v3_Util_GoogleMapsUtil() {
    /// <field name="_INIT" type="Boolean" static="true">
    /// </field>
}
js.com.cs.googlemaps.v3.Util.GoogleMapsUtil.INIT = function js_com_cs_googlemaps_v3_Util_GoogleMapsUtil$INIT() {
    if (!js.com.cs.googlemaps.v3.Util.GoogleMapsUtil._INIT) {
        js.com.cs.googlemaps.v3.Util.GoogleMapsUtil._INIT = true;
        google.maps.GoogleEvent = google.maps.event;
    }
}


js.com.cs.googlemaps.v3.Classes.CSGeocoderRequest.registerClass('js.com.cs.googlemaps.v3.Classes.CSGeocoderRequest');
js.com.cs.googlemaps.v3.Classes.CSInfoWindowWithMarker.registerClass('js.com.cs.googlemaps.v3.Classes.CSInfoWindowWithMarker');
js.com.cs.googlemaps.v3.Classes.CSEventListenerMapsHandler.registerClass('js.com.cs.googlemaps.v3.Classes.CSEventListenerMapsHandler');
js.com.cs.googlemaps.v3.Classes.CSInfoWindow.registerClass('js.com.cs.googlemaps.v3.Classes.CSInfoWindow');
js.com.cs.googlemaps.v3.Classes.CSMap.registerClass('js.com.cs.googlemaps.v3.Classes.CSMap');
js.com.cs.googlemaps.v3.Classes.CSMapTest.registerClass('js.com.cs.googlemaps.v3.Classes.CSMapTest');
js.com.cs.googlemaps.v3.Classes.CSMarker.registerClass('js.com.cs.googlemaps.v3.Classes.CSMarker');
google.maps.Test.registerClass('google.maps.Test');
js.com.cs.googlemaps.v3.Util.GoogleMapsUtil.registerClass('js.com.cs.googlemaps.v3.Util.GoogleMapsUtil');
js.com.cs.googlemaps.v3.Util.GoogleMapsUtil._INIT = false;
