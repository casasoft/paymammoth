﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Classes.HelperClasses;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Classes.Pages.Frontend;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using PayMammoth;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Extensions;
using PayMammoth.Modules;
using PayMammoth.Modules.PaymentMethodModule;
using PaymentMethod = PayMammothDeploy.usercontrols.payment.PaymentMethod;
using PayMammoth.Modules.ContentTextModule;
using log4net;


namespace PayMammothDeploy.payment
{
    public partial class _default : BasePaymentPage
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(_default));
        public _default()
        {
            this.RedirectAndShowErrorIfAlreadyPaid = true;
        }

        private void initUpdateTexts()
        {
            MyContentText.AttachContentTextWithControl(h3PaymentMethod, ((ContentText)Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.Payment_Details_PaymentMethodSectionTitle)).ToFrontend());
            MyContentText.AttachContentTextWithControl(divChoosePaymentText, ((ContentText)Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.Payment_Details_ChoosePaymentText)).ToFrontend());
            MyContentText.AttachContentTextWithControl(divPreConfirmationText, ((ContentText)Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.Payment_Details_PreConfirmationPayMethodText)).ToFrontend());
            btnPayment.Text = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.General_Button_Proceed).GetContent();
        }

        private List<PaymentMethod> listOfPaymentMethods;

        private MyRadioButtonList rdList = null;
        //private MyButton btnPayment = null;
        private MyCheckBoxWithLabel chkFakePayment = null;

        private bool checkIfFakePaymentsEnabled()
        {
            bool enabled = false;
            if (this.CurrentPaymentRequest.Data.WebsiteAccount.EnableFakePayments)
                enabled = true;
            else
            {
                if (this.CurrentPaymentRequest.Data.WebsiteAccount.CheckFakePaymentKey(PayMammoth.QuerystringData.FakePayments))
                {
                    enabled = true;
                }

            }
            return enabled;
        }

        private void showPaymentChoices()
        {
            string title = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.Payment_Details_PageTitle).GetContent();
            this.PageTitle = title;
            this.Functionality.UpdateTitle(title, false);

            btnPayment.Click += new EventHandler(btnPayment_Click);
            var request = this.CurrentPaymentRequest;
            if (request != null)
            {
                IPaymentRequestDetails requestDetails = request.Data;
                aCancel.HRef = requestDetails.ReturnUrlFailure;

                var cpCancelText = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        PayMammoth.Enums.CONTENT_TEXT.Payment_Details_PaymentCancelText);
                var tokenRep = cpCancelText.CreateTokenReplacerNew();
                tokenRep["Account_Website"] = request.Data.WebsiteAccount.WebsiteName;
                tokenRep["$Account_Website"] = request.Data.WebsiteAccount.WebsiteName;
                tokenRep["[Account_Website]"] = request.Data.WebsiteAccount.WebsiteName;
                string cancelText =  BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(cpCancelText,
                    tokenRep);
                aCancel.InnerHtml = cancelText;

                //rdList = new CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.MyRadioButtonList();
                //rdList.ID = "rdList";

                listOfPaymentMethods = new List<PaymentMethod>();

                foreach (IPaymentMethodInfo method in request.Data.WebsiteAccount.GetAvailablePaymentMethods())
                {

                    var ctrl = PaymentMethod.LoadControl(this.Page, method.Title);
                    ctrl.Functionality.PaymentMethod = method;
                    listOfPaymentMethods.Add(ctrl);
                    divPaymentChoices.Controls.Add(ctrl);
                    //rdList.Items.Add(new ListItem(method.Title, CS.General_v3.Util.EnumUtils.StringValueOf(method.PaymentMethodType)));
                }

                //divPaymentChoices.Controls.Add(rdList);
                if (checkIfFakePaymentsEnabled())
                {
                    MyDiv divFakePaymentChoice = new MyDiv();
                    divFakePaymentChoice.CssManager.AddClass("fake-payment-choice-container");
                    chkFakePayment = new MyCheckBoxWithLabel();
                    divFakePaymentChoice.Controls.Add(chkFakePayment);
                    chkFakePayment.Label = "Perform fake payment?";
                    chkFakePayment.ID = "chkFakePayment";
                    divPaymentChoices.Controls.Add(divFakePaymentChoice);
                }
            }

            //continue payment choices
        }

        void btnPayment_Click(object sender, EventArgs e)
        {
            bool cont = false;
            if(listOfPaymentMethods != null && listOfPaymentMethods.Count()> 0)
            {
                foreach (var item in listOfPaymentMethods)
                {
                    if(item.Functionality.IsChecked())
                    {
                        cont = true;
                        initProceedPayment(item.Functionality.PaymentMethod);
                        break;
                    }
                }
                if (!cont)
                {
                    this.SetStatusMsg("Please choose a valid payment method", CS.General_v3.Enums.STATUS_MSG_TYPE.Information);
                }
            }
        }

        private void initProceedPayment(IPaymentMethodInfo paymentMethod)
        {
            if (_log.IsDebugEnabled) _log.Debug("initProceedPayment - PaymentMethod: " + paymentMethod.PaymentMethod);
            bool fakePayment = false;
            if (chkFakePayment != null) fakePayment = chkFakePayment.GetFormValueObjectAsBool();

            //var paymentMethodType = CS.General_v3.Util.EnumUtils.StringValueOf(paymentMethod.PaymentMethod);
            PayMammoth.Connector.Enums.PaymentMethodSpecific? method = paymentMethod.PaymentMethod;
                //CS.General_v3.Util.EnumUtils.GetEnumByStringValueNullable<PayMammoth.Connector.Enums.PaymentMethodSpecific>(paymentMethodType);

            if (method.HasValue)
            {
                if (!fakePayment)
                {
                    var redirector = PayMammoth.Util.PaymentUtil.GetRedirectorBasedOnPaymentMethod(method.Value);
                    if (_log.IsDebugEnabled) _log.Debug("Redirecting to payment page, based on redirector: " + redirector);
            
                    var result = redirector.Redirect(this.CurrentPaymentRequest.Data);
                    {
                        var ct = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextForEnumValue(result).ToFrontendBase() ;
                        this.SetStatusMsg(ct, (result == Enums.RedirectionResult.Success ? CS.General_v3.Enums.STATUS_MSG_TYPE.Success : CS.General_v3.Enums.STATUS_MSG_TYPE.Error));
                            
                    }
                    //if redirect is successful, it will NOT return here.  if it returns here, an error has occured most probably
                }
                else
                {

                    this.CurrentPaymentRequest.Data.MakeFakePayment(method.Value, PayMammoth.QuerystringData.FakePayments);
                    this.CurrentPaymentRequest.RedirectToSuccessUrl();

                }
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            initUpdateTexts();
            showPaymentChoices();
            var currentRequest = this.CurrentPaymentRequest;
            if(currentRequest != null)
            {
                YourDetails.Functionality.ClientDetails = currentRequest.Data;
            }
        }
    }
}