﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages.master" AutoEventWireup="true"
    CodeBehind="default.aspx.cs" Inherits="PayMammothDeploy.payment._default" %>

<%@ Register Src="~/usercontrols/payment/YourDetails.ascx" TagName="YourDetails"
    TagPrefix="PayMammoth" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <PayMammoth:YourDetails runat="server" ID="YourDetails" />
    <div class="payment-method-listing-wrapper">
        <div class="payment-method-container clearfix">
            <h3 class="h3-payment-method rounded-border" runat="server" id="h3PaymentMethod">
                Payment Method</h3>
        </div>
        <div class='payment-method-choice-text' runat="server" id="divChoosePaymentText">
        </div>
        <div class='payment-method-selection' id="divPaymentChoices" runat="server">
        </div>
        <div runat="server" id="divPreConfirmationText">
        </div>
        <div class="button-wrapper">
            <CSControls:MyButton runat="server" ID="btnPayment" Text="Proceed" CssClass="button" />
            <a href="#" id="aCancel" runat="server">&laquo; Cancel and return to <span runat="server"
                id="spanWebsite"></span></a>
        </div>
        <div class='fake-payments-container' id="divFakePayments" runat="server" visible="false">
            <h2>
                Fake Payments</h2>
        </div>
        <script type="text/javascript">
            jQuery(function () {
                jQuery(".payment-method-wrapper").click(function () {
                    jQuery(this).find("input[type=radio]").attr("checked", "checked");
                })
            });
        </script>
    </div>
</asp:Content>
