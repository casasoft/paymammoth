﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="response.aspx.cs" Inherits="PayMammothDeploy.payment.gateways.realex.response1" %>

<!-- this is the page that is shown in the template of realex -->

<div id="divSuccess" class="success" visible="false" runat="server">
    <asp:literal runat="server" id="successText">
    </asp:literal>
</div>
<div id="divFailure" class="failure" visible="false" runat="server">
    <span id="spanMsg" runat="server"></span>
    <asp:literal runat="server" id="failureText">
    </asp:literal>
</div>
<script type="text/javascript">
    function redirectPage() {
        window.location = '<%=UrlToRedirectTo %>';
    }
    window.setTimeout('redirectPage();', 250);
    window.setTimeout('redirectPage();', 500);
    window.setTimeout('redirectPage();', 1000);
    window.setTimeout('redirectPage();', 2000);
    window.setTimeout('redirectPage();', 3000);
    window.setTimeout('redirectPage();', 4000);
    window.setTimeout('redirectPage();', 6000);

</script>