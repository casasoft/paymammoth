﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Extensions;
using PayMammoth.Modules;
using PayMammoth.Modules.ContentTextModule;

namespace PayMammothDeploy.payment.gateways.realex
{
    public partial class response1 : PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.RedirectMethod.RealexResponsePage
    {
        /// <summary>
        /// This is the page which is used to receive the response from the Realex Gateway
        /// </summary>

        public response1()
        {
            this.UrlToRedirectTo = "#";
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int k = 5;


        }

        public string UrlToRedirectTo { get; set; }

        protected override void checker_OnTransactionFailed(PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.Classes.RealexResponseCheckResponse response)
        {
            divFailure.Visible = true;
            var t = response.Transaction;
            if (t != null)
            {
                string identifier = t.PaymentRequest.Identifier;
                string retryUrl = PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.RealexUrls.GetPrePaymentUrl(identifier);
                string choicePage = PayMammoth.Urls.GetPaymentChoiceUrl(identifier);

                var cpFailure = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_Realex_FailureMessage);
                var tr= cpFailure.CreateTokenReplacer();
                tr["[failure_message]"]= PayMammoth.Enums.GetMessageFromPaymentStatus(response.Status);
                tr["[failure_link]"]= retryUrl;
                tr["[go_back]"] = choicePage;
                string strFailure = cpFailure.GetContent();
                tr.ReplaceInString(ref strFailure);
                UrlToRedirectTo = retryUrl;
                failureText.Text = strFailure;
            }
            base.checker_OnTransactionFailed(response);
        }
        protected override void checker_OnTransactionSuccessful(PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.Classes.RealexResponseCheckResponse response)
        {

            divSuccess.Visible = true;

            var transaction = response.Transaction;
            var frontendPaymentItem = transaction.PaymentRequest.ToFrontend();

            var successUrl = frontendPaymentItem.GetSuccessUrl();

            var  cpSuccessText =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_Realex_SuccessMessage);
            var tr = cpSuccessText.CreateTokenReplacer();
            tr["[success_link]"] = successUrl;
            string strSuccessText = cpSuccessText.GetContent();
            tr.ReplaceInString(ref strSuccessText);
            successText.Text = strSuccessText;
            UrlToRedirectTo = successUrl;
            CS.General_v3.Util.PageUtil.RedirectPageUsingJavaScript(this, successUrl);
            base.checker_OnTransactionSuccessful(response);
        }



    }
}