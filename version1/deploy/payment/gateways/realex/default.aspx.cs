﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Classes.Pages.Frontend;
using PayMammoth.Frontend.PaymentRequestModule;
using PayMammoth.Modules;
using PayMammoth.PaymentMethods.PaymentGateways.Realex.v1;
using PayMammoth.Modules.ContentTextModule;

namespace PayMammothDeploy.payment.gateways.realex
{
    public partial class _default : BasePaymentPage
    {
        private PaymentRequestFrontend _request;
        public _default()
        {
            //string title = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
            //        PayMammoth.Enums.CONTENT_TEXT.Payment_Method_Realex_PageTitle).GetValue();
            //this.PageTitle = title;

        }

        private void fillInfo()
        {
            if (this.CurrentPaymentRequest != null)
            {
                _request = this.CurrentPaymentRequest;
                redirectText.Text =
                    Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        PayMammoth.Enums.CONTENT_TEXT.Payment_Gateways_Relaex_RedirectText).GetContent();
                //spanTotalAmount.InnerText = request.Data.GetTotalPrice().ToString("#,0.00");
                StagingNote.Functionality.PaymentRequest = _request;
                StagingNote.Functionality.IsStaging = !_request.Data.WebsiteAccount.PaymentGatewayRealexUseLiveEnvironment;
                var realexRequest = RealexUtil.InitialiseRequest(this.CurrentPaymentRequest.Data);
                realexRequest.OutputHiddenFieldsToFormAndChangeAction(this);
                aGoBack.Href = PayMammoth.Urls.GetPaymentChoiceUrl(this.CurrentPaymentRequest.Data.Identifier);
                aGoBack.InnerHtml = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_CreditCard_GoBackText).GetContent();
                btnSubmit.Click += new EventHandler(btnSubmit_Click);
            }
        }

        void btnSubmit_Click(object sender, EventArgs e)
        {
            int k = 5;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            //updateTexts();
            fillInfo();
        }

        //private void updateTexts()
        //{
        //    spanTotal.InnerHtml =
        //        Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.OrderSummaryDetails_TotalLabel)
        //            .GetValue();
        //}
    }
}