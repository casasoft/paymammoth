﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Classes.HelperClasses;
using PayMammoth.Classes.Pages.Frontend.PayPal;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.PaymentMethods.PayPal.v1;
using PayMammoth.Connector.Classes.Interfaces;
using log4net;
using PayMammoth.Util;

namespace PayMammothDeploy.payment.paypal.v1
{
    public partial class confirm : BasePayPalPage
    {
        private ILog _log = LogManager.GetLogger(typeof(confirm));

        protected void fillDetails()
        {
            //divAmount.InnerText = PayMammoth.Connector.Extensions.PaymentRequestDetailsExtensions.GetTotalPrice(this.CurrentPaymentRequest.Data).ToString("#,0.00");
            btnConfirm.Text =
                PayMammoth.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.General_Button_ConfirmAndProceed).GetContent();
            spanConfirmOrderText.InnerHtml = PayMammoth.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_Paypal_ConfirmOrderText).GetContent();
            h3ConfirmOrder.InnerHtml = PayMammoth.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_Paypal_ConfirmOrderTitleText).GetContent();
            string orderLinkClientWebsite = ((IPaymentRequestDetails) CurrentPaymentRequest.Data).OrderLinkOnClientWebsite;
            if (!String.IsNullOrEmpty(orderLinkClientWebsite))
            {
                var ctFullorderDetailsText = PayMammoth.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_Paypal_OrderDetailsText);

                string fullorderDetailsText = ctFullorderDetailsText.GetContent();

                BusinessLogic_v3.Classes.Text.TokenReplacerWithContentTagItem tr = new BusinessLogic_v3.Classes.Text.TokenReplacerWithContentTagItem(ctFullorderDetailsText);
                tr["[more_details_link]"] = orderLinkClientWebsite;
                tr.ReplaceInString(ref fullorderDetailsText);

                //fullOrderLink.Text = fullorderDetailsText;
            }
            else
            {
                //fullOrderLink.Parent.Controls.Remove(fullOrderLink);
            }

            //divTotalAmount.InnerHtml =
            //    Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
            //        PayMammoth.Enums.CONTENT_TEXT.OrderSummaryDetails_TotalAmountLabel).GetValue();
            btnConfirm.Click += new EventHandler(btnConfirm_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            fillDetails();
        }

        void btnConfirm_Click(object sender, EventArgs e)
        {
            _log.Debug("Confirming payment");
            var result = PayPalManager.Instance.ConfirmPayment(CurrentPaymentRequest.Data);
            if (result == PayPalEnums.CONFIRM_RESULT.OK)
            {
                if (_log.IsDebugEnabled)
                {
                    _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(this.CurrentPaymentRequest.Data,"> - Starting redirect to success url"));
                }
                CurrentPaymentRequest.RedirectToSuccessUrl();
                
                //string successUrl = ((IPaymentRequestDetails)CurrentPaymentRequest.Data).ReturnUrlSuccess;
                //CS.General_v3.Util.PageUtil.RedirectPage(successUrl);
            }
            else
            {
                var ct = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextForEnumValue(result).ToFrontendBase();

                this.Functionality.ShowGenericMessageAndRedirectToPage( ct, CS.General_v3.Enums.STATUS_MSG_TYPE.Error, PayMammoth.Urls.Url_Payment_Choice);
                
            }


        }

       
    }
}