﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Pages.Frontend;
using PayMammoth.Frontend.PaymentRequestModule;
using PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.Modules;
using log4net;
namespace PayMammothDeploy.payment.transactium.v1
{
    public partial class _default : BasePaymentPage
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(_default));
             
        public _default()
        {
            
            string title =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.Payment_Method_CreditCard_PageTitle).GetContent();
            this.PageTitle = title;
            this.Functionality.UpdateTitle(title, false);
        }

        private void initTransaction()
        {
            _log.Debug("initTransaction - [START]");
            var requestResponse= TransactiumManager.Instance.CreateRequest(this.CurrentPaymentRequest.Data);
            _log.Debug("initTransaction - RequestResponse Result: " + requestResponse.Result.GetMessageAsString());
            if (requestResponse.Result.IsSuccessful)
            {
                paymentCtrl.Functionality.ResponseUrl = requestResponse.PaymentResponse.RedirectURL;
            }
            else
            {
                
                this.SetStatusMsg(requestResponse.Result.GetMessageAsString(), requestResponse.Result.Status);
            }
            _log.Debug("initTransaction - [Finish]");

        }

        private void fillData()
        {
            PaymentRequestFrontend req = this.CurrentPaymentRequest;
            if (req != null)
            {
                StagingNote.Functionality.PaymentRequest = req;
            }
            var websiteAccount = req.Data.WebsiteAccount;
            if (websiteAccount != null)
            {
                if (!websiteAccount.IsTransactiumEnabled())
                {
                    throw new InvalidOperationException(
                        "Transactium Payment gateway must be switched on in order to use this");
                }
            }
            StagingNote.Functionality.IsStaging = !req.Data.WebsiteAccount.PaymentGatewayTransactiumUseLiveEnvironment;

            //divStagingNote.Visible = (!req.Data.WebsiteAccount.PaymentGatewayTransactiumUseLiveEnvironment);
            //string stagingNoteContent = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(PayMammoth.Enums.CONTENT_TEXT.General_StagingError).GetValue();
            //stagingNoteContent = CS.General_v3.Util.Text.Repl2aceTag(stagingNoteContent, "COMPANY_NAME",
            //                                                        w.ChequePayableTo);
            //divStagingNote.InnerHtml = stagingNoteContent;

            aGoBack.HRef = PayMammoth.Urls.GetPaymentChoiceUrl(this.CurrentPaymentRequest.Data.Identifier);
            aGoBack.InnerHtml =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_CreditCard_GoBackText).GetContent();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            fillData();
            initTransaction();
            updateTexts();
        }

        private void updateTexts()
        {
            SecurityInformation.Functionality.BankStatementText = this.CurrentPaymentRequest.Data.WebsiteAccount.PaymentGatewayTransactiumBankStatementText;
        }
    }
}