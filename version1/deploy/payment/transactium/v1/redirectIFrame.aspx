﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Core.Master" AutoEventWireup="true" CodeBehind="redirectIFrame.aspx.cs" 
Inherits="PayMammothDeploy.payment.transactium.v1.redirectIFrame" %>



<asp:Content ID="Content2" ContentPlaceHolderID="coreContentPlaceHolder" runat="server">

<!-- Note, this page is shown in the payment IFRAME -->

If you are not automatically redirected, click <a id="aRedirectLink" runat="server" href="#" target="_parent">here</a>.

</asp:Content>