﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages.master" AutoEventWireup="true"
    CodeBehind="default.aspx.cs" Inherits="PayMammothDeploy.payment.transactium.v1._default" %>

<%@ Register Src="../../../usercontrols/transactium/v1/paymentCtrl.ascx" TagName="paymentCtrl"
    TagPrefix="uc1" %>
<%@ Register Src="~/usercontrols/payment/SecurityInformation.ascx" TagName="SecurityInformation"
    TagPrefix="PayMammoth" %>
<%@ Register Src="~/usercontrols/payment/StagingNote.ascx" TagName="StagingNote"
    TagPrefix="PayMammoth" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <PayMammoth:StagingNote runat="server" ID="StagingNote" />
    <div class="payment pay-by-credit-card">
        <uc1:paymentCtrl ID="paymentCtrl" runat="server" />
    </div>
    <div class="button-wrapper">
        <a href="" id="aGoBack" runat="server"></a>
    </div>
    <PayMammoth:SecurityInformation runat="server" ID="SecurityInformation" />
</asp:Content>
