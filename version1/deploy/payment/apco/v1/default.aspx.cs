﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Pages.Frontend;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.WebForm;
using PayMammoth.Modules;
using log4net;
namespace PayMammothDeploy.payment.apco.v1
{
    public partial class _default : BasePaymentPage
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(_default));
        private void loadDetails()
        {
            var req = this.CurrentPaymentRequest;

            SecurityInformation.Functionality.BankStatementText =
                req.Data.WebsiteAccount.PaymentGatewayApcoBankStatementText;
            //spanTransactionId.InnerText = req.Data.WebsiteAccount.PaymentGatewayApcoBankStatementText;
            StagingNote.Functionality.IsStaging = !req.Data.WebsiteAccount.PaymentGatewayApcoUseLiveEnvironment;

            var paymentUrl = PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.ApcoUrls.GetPaymentIframeUrl(req.Data.Identifier);
            iframePayment.Attributes["src"] = paymentUrl;
            StagingNote.Functionality.PaymentRequest = req;
            //divStagingNote.Visible = (!req.Data.WebsiteAccount.PaymentGatewayApcoUseLiveEnvironment);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_log.IsDebugEnabled) _log.Debug("Apco/default.aspx - Page_Load [START]");
            loadDetails();


            aGoBack.HRef = PayMammoth.Urls.GetPaymentChoiceUrl(this.CurrentPaymentRequest.Data.Identifier);
            aGoBack.InnerHtml =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    PayMammoth.Enums.CONTENT_TEXT.PaymentMethod_CreditCard_GoBackText).GetContent();
            if (_log.IsDebugEnabled) _log.Debug("Apco/default.aspx - Page_Load [FINISH]");
        }
    }
}