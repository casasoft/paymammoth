﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Pages.Frontend;
using PayMammoth.PaymentMethods.PaymentGateways.Apco.v1;

namespace PayMammothDeploy.payment.apco.v1
{
    public partial class payment_success : BasePaymentPage
    {
        public payment_success()
        {
            this.RedirectAndShowErrorIfAlreadyPaid = false;
        }

        private void checkRequest()
        {
            var req = this.CurrentPaymentRequest;
            bool ok = req.Data.WaitUntilRequestIsMarkedAsPaid();
            if (ok)
            {
                req.RedirectToSuccessUrlUsingJavaScriptInParent(this);
            }
            else
            {
                ApcoManager.Instance.RedirectToFailurePageInParent(this, req.Data);
                
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            checkRequest();

        }
    }
}