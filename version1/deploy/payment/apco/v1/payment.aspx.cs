﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.WebForm;
using PayMammoth.PaymentMethods.PaymentGateways.Apco.v1;

namespace PayMammothDeploy.payment.apco.v1
{
    public partial class payment : CS.WebComponentsGeneralV3.Code.Classes.Pages.BasePageMostBasic
    {
        /*This is the actual IFRAME that outputs the APCO form */
        private void writeForm()
        {
           // BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            var req = PayMammoth.QuerystringData.GetPaymentRequestFromQuerystring();
           // BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
            if (req != null && !req.CheckIfPaid())
            {


                PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.ApcoManager.Instance.CreateInitialRequest(this, req);
                
                //this will output the required form

            }
            else
            {
                throw new InvalidOperationException("Request required and must not be already paid");
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            writeForm();
            
        }
        public override void UpdateTitle(string pageTitle, bool useOpenGraph = true)
        {

        }

        public override void UpdateTitleFromContentPage(BusinessLogic_v3.Frontend.ArticleModule.ArticleBaseFrontend page, bool useOpenGraph = true)
        {

        }

        public override void UpdateTitleAndMetaTags(string title, string metaKeywords, string metaDesc, bool useOpenGraph = true)
        {

        }

        public override void UpdateOpenGraphPageType(CS.General_v3.Enums.OPEN_GRAPH_TYPE type, bool overwrite = true)
        {

        }

        public override void UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH tag, string value, bool overwrite = true)
        {

        }
    }
}