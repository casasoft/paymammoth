﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using PayMammoth.Classes.Pages.Frontend;
using PayMammoth;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Frontend.PaymentRequestModule;

namespace PayMammothDeploy
{
    public partial class pages : BasePagesMasterPage
    {
        private bool _currentPaymentLoaded = false;
        private PaymentRequestFrontend _currentPayment = null;

        public PaymentRequestFrontend CurrentPaymentRequest
        {
            get
            {
                if (!_currentPaymentLoaded)
                {
                    _currentPaymentLoaded = true;
                    var data = QuerystringData.GetPaymentRequestFromQuerystring();
                    _currentPayment = PaymentRequestFrontend.Get(data);
                }
                return _currentPayment;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {

            base.OnPreRender(e);
            init();

        }

        protected override void OnLoad(EventArgs e)
        {
            orderSummary.Functionality.PaymentRequest = this.CurrentPaymentRequest.Data;

            base.OnLoad(e);
        }

        private void init()
        {
            
            string pageTitle = this.PageTitle;
            if (!String.IsNullOrEmpty(pageTitle))
            {
                h2PageTitle.InnerHtml = pageTitle;
            }
            else
            {
                h2PageTitle.Visible = false;
            }
        }

        protected override HtmlGenericControl _divMessage
        {
            get { return divMessage; }
        }


        public override HtmlGenericControl BodyTag
        {
            get { return this.Master.BodyTag; }
        }
    }
}