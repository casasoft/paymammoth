﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LatestNews.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.News.LatestNews" %>
<div class="latest-news-container">
    <ul class="latest-news-list" runat="server" id="ulLatestNews">
    </ul>
    <div class="latest-news-view-more-container clearfix" runat="server" id="divViewMore">
        <div class="latest-news-list-separator"></div>
        <a href="/" runat="server" id="anchorViewMore"></a>
    </div>
</div>
