﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductImagesSlider2.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.ProductImagesSlider2" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Sliders/AnythingSlider/v1/AnythingSliderV1.ascx"
    TagName="AnythingSlider" TagPrefix="Common" %>
<div class="product-description-image-slider clearfix" runat="server" id="divProductImageSlider">
    <Common:AnythingSlider runat="server" ID="productImageSlider" />
    <div class="product-description-image-slider-text" runat="server" id="divProductImageSliderText"></div>
</div>
