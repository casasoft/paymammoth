﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductAddToCart.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.ProductAddToCart" %>
<div class="product-add-to-cart-container">
    <CSControlsFormFields:FormFields ID="formAddToCart" runat="server"></CSControlsFormFields:FormFields>
</div>