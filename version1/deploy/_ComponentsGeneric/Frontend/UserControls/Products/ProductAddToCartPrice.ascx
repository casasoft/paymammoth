﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductAddToCartPrice.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.ProductAddToCartPrice" %>
<span class="product-add-to-cart-price" runat="server" id="spanPrice"></span>
<span class="product-add-to-cart-new-price" runat="server" id="spanNewPrice" visible="false"></span>