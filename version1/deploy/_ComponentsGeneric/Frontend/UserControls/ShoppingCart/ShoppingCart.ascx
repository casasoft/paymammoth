﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCart.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.ShoppingCart" %>
<%@ Register TagPrefix="CommonControls" TagName="ShoppingCartProceedCtrl" Src="~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/ShoppingCartProceedCtrl.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="CheckOutManagementArea" Src="~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/CheckOutManagementArea.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ShoppingCartDiscountsItems" Src="~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/ShoppingCartDiscountsItems.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ShoppingCartPriceDetails" Src="~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/ShoppingCartPriceDetails.ascx" %>
<div class="cart-container" runat="server" id="divCartContainer">
    <!-- Product Listing Table -->
    <div class="listing-content">
        <table class="listing-table" cellpadding="0" cellspacing="0">
            <thead>
                <tr id="trHead" runat="server">
                  
                    <th runat="server" id="thDelete">
                    </th>
                    <th runat="server" id="thItem">
                        Item
                    </th>
                    <th runat="server" id="thQuantity" class='cart-quantity-header'>
                        Quantity
                    </th>
                    <th runat="server" id="thUnitPrice">
                        Unit Price
                    </th>
                    <th runat="server" id="thTotalPrice">
                        Total Price
                    </th>
                </tr>
            </thead>
            <tbody id="tBodyListing" runat="server">
            </tbody>
        </table>
    </div>
    <!-- End Of Product Listing Table -->
    <CommonControls:ShoppingCartDiscountsItems runat="server" ID="discountItems" />
    <div class="cart-management-area" id="divCartManagementArea" runat="server" visible="false">
        <div class="quantities-totals-wrapper clearfix">
            <div class="update-quantities-container" id="divUpdateQuantitiesContainer" runat="server">
                <span class="update-quantities-text" id="spanQuantitiesText" runat="server"></span>
                <CSControls:MyButton runat="server" ID="btnUpdateQuantities" CssClass="button-update-quantities">
                </CSControls:MyButton>
            </div>
            <div class="totals-container" id="divTotalsContainerCart" runat="server">
                <CommonControls:ShoppingCartPriceDetails runat="server" ID="shoppingCartPriceDetails" />
            </div>
        </div>
    </div>
    <CommonControls:CheckOutManagementArea runat="server" ID="checkOutManagementArea"
        Visible="false" />
    <CommonControls:ShoppingCartProceedCtrl runat="server" ID="shoppingCartProceedCtrl" />
</div>

