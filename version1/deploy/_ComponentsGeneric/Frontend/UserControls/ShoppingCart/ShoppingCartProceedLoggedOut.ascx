﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartProceedLoggedOut.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.ShoppingCartProceedLoggedOut" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/MasterPage/Login.ascx"
    TagName="Login" TagPrefix="CommonControls" %>
<div class="shopping-cart-proceed-container clearfix" id="divShoppingCartProceedContainer"
    runat="server">
    <div class="shopping-cart-proceed-login">
        <h2 class="shopping-cart-proceed-login-title" runat="server" id="h2ProceedLoginTitle">
        </h2>
        <div class="shopping-cart-proceed-login-text" runat="server" id="divProceedLoginText">
        </div>
        <div class="login-credentials-content" runat="server" id="divLoginCredentialsContainer">
            <CommonControls:Login runat="server" ID="loginCtrl" />
        </div>
    </div>
    <div class="shopping-cart-proceed-register">
        <h2 class="shopping-cart-proceed-register-title" runat="server" id="h2ProceedRegisterTitle">
        </h2>
        <div class="shopping-cart-proceed-login-text" runat="server" id="divProceedRegisterText">
        </div>
        <CSControls:MyButton runat="server" ID="btnProceedToCheckOut"></CSControls:MyButton>
    </div>
</div>
