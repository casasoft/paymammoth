﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartProceedCtrl.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.ShoppingCartProceedCtrl" %>
<%@ Register TagPrefix="CommonControls" TagName="ShoppingCartProceedLoggedOut" Src="~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/ShoppingCartProceedLoggedOut.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ShoppingCartProceedLoggedIn" Src="~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/ShoppingCartProceedLoggedIn.ascx" %>
    
<CommonControls:ShoppingCartProceedLoggedOut runat="server" ID="loggedOut" />
<CommonControls:ShoppingCartProceedLoggedIn runat="server" ID="loggedIn" />