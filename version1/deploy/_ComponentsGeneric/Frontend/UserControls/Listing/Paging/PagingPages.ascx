﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagingPages.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.Paging.PagingPages" %>
<ul class="clearfix paging-pages-list" id="ulPages" runat="server">
    <li><a href="#">&laquo;</a></li>
    <li><a href="#">‹</a></li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">›</a></li>
    <li><a href="#">&raquo;</a></li>
</ul>
