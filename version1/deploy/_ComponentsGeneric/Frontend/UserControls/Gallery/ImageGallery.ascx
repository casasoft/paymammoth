﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageGallery.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Gallery.ImageGallery" %>
<script type="text/javascript" src="/_common/js/jQuery/plugins/prettyPhoto/3.1.2/js/jquery.prettyPhoto.js"></script>
<link type="text/css" rel="Stylesheet" href="/_common/js/jQuery/plugins/prettyPhoto/3.1.2/css/prettyPhoto.css" />

<div class="gallery-container">
    <span runat="server" id="spanGalleryTitle" class="gallery-title"></span>
    <div class="image-gallery-container clearfix" runat="server" id="divImageGalleryContainer">
    </div>
    <div class="gallery-container-text">
      <span runat="server" id="spanGalleryText" class="gallery-text"></span>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("a[rel^='prettyPhoto']").prettyPhoto();
    });
</script>
