﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentPageMediaItems.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage.ContentPageMediaItems" %>
<%@ Register TagPrefix="CommonControls" TagName="PrettyPhotoImageGallery" Src="~/_ComponentsGeneric/Frontend/UserControls/Gallery/PrettyPhotoImageGallery.ascx" %>
<div class="content-page-media-items-container">
    <CommonControls:PrettyPhotoImageGallery runat="server" ID="articleMediaItems" />
</div>
