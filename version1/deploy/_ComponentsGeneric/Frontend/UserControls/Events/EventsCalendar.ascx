﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventsCalendar.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Events.EventsCalendar" %>
<%@ Register TagPrefix="CommonControls" TagName="jQueryCalendar" Src="~/_ComponentsGeneric/Frontend/UserControls/Calendar/jQueryCalendar.ascx" %>

<CommonControls:jQueryCalendar runat="server" ID="fullCalendar"/>