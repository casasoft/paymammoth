﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventTutorThumbnailItem.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Events.EventTutorThumbnailItem" %>
<div class="tutor-listing-thumbnail-container">
    <div class="tutor-listing-thumbnail">
        <CSControls:MyImage runat="server" ID="imgTutor">
        </CSControls:MyImage>
    </div>
    <CSControls:MyAnchor runat="server" ID="anchorTutor">
    </CSControls:MyAnchor>
</div>
