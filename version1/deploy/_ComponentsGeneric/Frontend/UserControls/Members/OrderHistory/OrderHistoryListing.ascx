﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderHistoryListing.ascx.cs" 
Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.OrderHistory.OrderHistoryListing" %>

<%@ Register TagPrefix="CommonControls" TagName="PagingBar" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/Paging/PagingBar.ascx" %>
<div class="order-history-results-container">
    <CommonControls:PagingBar ID="pagingBarTop" runat="server" />
    <asp:PlaceHolder ID="listingPlaceholder" runat="server">
        <div class="listing-content">
            <table class="listing-table" cellpadding="0" cellspacing="0">
                <thead>
                    <tr id="trHead" runat="server">
                        <th runat="server" id="thDate">
                            Date
                        </th>
                        <th runat="server" id="thDescription">
                            Description
                        </th>
                        <th runat="server" id="thStatus">
                            Status
                        </th>
                        <th runat="server" id="thInvoice">
                            Invoice
                        </th>
                        <th runat="server" id="thTotal">
                            Total
                        </th>
                    </tr>
                </thead>
                <tbody id="tBodyListing" runat="server">
                </tbody>
            </table>
        </div>
    </asp:PlaceHolder>
    <div class="paging-footer-container">
    <CommonControls:PagingBar ID="pagingBarBottom" runat="server" />
    </div>
</div>
