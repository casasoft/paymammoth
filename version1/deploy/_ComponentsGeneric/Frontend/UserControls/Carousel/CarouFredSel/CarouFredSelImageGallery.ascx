﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CarouFredSelImageGallery.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Carousel.CarouFredSel.CarouFredSelImageGallery" %>
<%@ Register TagPrefix="CommonControls" TagName="CarouFredSelControl" Src="CarouFredSelControl.ascx" %>

<CommonControls:CarouFredSelControl ID="carousel" runat="server" />
