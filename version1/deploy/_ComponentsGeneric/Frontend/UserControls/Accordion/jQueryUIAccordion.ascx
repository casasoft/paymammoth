﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="jQueryUIAccordion.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Accordion.jQueryUIAccordion" %>
<div id="divAccordion" class="jqueryui-accordion-container" runat="server">
    <asp:PlaceHolder ID="placeholderAccordionContent" runat="server">
        <h3>
            <a href="#">First header</a></h3>
        <div>
            First content</div>
        <h3>
            <a href="#">Second header</a></h3>
        <div>
            Second content</div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="jsPlaceholder" runat="server"></asp:PlaceHolder>
</div>
