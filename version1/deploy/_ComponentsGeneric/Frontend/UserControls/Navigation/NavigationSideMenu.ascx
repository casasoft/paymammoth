﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationSideMenu.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Navigation.NavigationSideMenu" %>
    <%@ Register Assembly="WebComponentsGeneralV3" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Hierarchy" TagPrefix="Hierarchy" %> 
<div>
    <div class="navigation-side-menu-parent-wrapper">
        <a id="aParentTitle" runat="server"></a>
    </div>
    <div class="navigation-side-menu-content">     
        <Hierarchy:HierarchicalControlNavigation id="hierarchyLinks" runat="server" />
    </div>
</div>
