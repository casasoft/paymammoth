﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MCustomScrollbar.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Scrollbar.MCustomScrollbar.MCustomScrollbar" %>
<div id="divCustomScrollbar" class="custom-scrollbar-container" runat="server">
    <div class="customScrollBox">
        <div class="container">
            <div class="content">
                <asp:PlaceHolder ID="placeHolderContent" runat="server"></asp:PlaceHolder>
            </div>
        </div>
    </div>
    <div class="dragger_container">
        <div class="dragger">
        </div>
    </div>
    <a href="#" class="scrollUpBtn" id="aScrollUpButton" runat="server"></a><a href="#" class="scrollDownBtn" id="aScrollDownButton" runat="server"></a>
    <div class="scrollbar-mask-top">
    </div>
    <div class="scrollbar-mask-bottom">
    </div>
</div>
