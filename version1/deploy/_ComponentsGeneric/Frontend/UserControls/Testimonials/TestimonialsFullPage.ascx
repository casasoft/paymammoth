﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestimonialsFullPage.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Testimonials.TestimonialsFullPage" %>
<%@ Register TagPrefix="CommonControls" TagName="PagingBar" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/Paging/PagingBar.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="TestimonialsItems" Src="~/_ComponentsGeneric/Frontend/UserControls/Testimonials/TestimonialsItems.ascx" %>
<div class="testimonials-page-container">
    <CommonControls:ContentPage runat="server" ID="contentPage" />
    <div class="html-container" runat="server" id="divNoResultsText" visible="false"></div>
    <CommonControls:TestimonialsItems runat="server" ID="testimonials" />
    <div class="listing-page-bottom-section-container clearfix" runat="server" id="divBottomSection">
        <CommonControls:PagingBar runat="server" ID="pagingBarBottom" />
    </div>
</div>
