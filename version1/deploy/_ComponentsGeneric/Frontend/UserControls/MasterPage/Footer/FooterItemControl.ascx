﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterItemControl.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.Footer.FooterItemControl" %>

<div class="footer-item-control" runat="server" id="divFooterItemControl">
    <div class="footer-item-control-title" runat="server" id="footerItemTitle"></div>
    <div class="footer-item-control-content clearfix" runat="server" id="footerItemContent"></div>
</div>