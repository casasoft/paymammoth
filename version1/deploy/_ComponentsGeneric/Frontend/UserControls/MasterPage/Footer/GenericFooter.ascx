﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericFooter.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.Footer.MainFooter" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/MasterPage/Footer/FooterSitemap.ascx"
    TagName="FooterSitemap" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Products/ProductCategoriesListing.ascx"
    TagName="ProductCategoriesListing" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Contact/ContactUsDetails.ascx"
    TagName="ContactUsDetails" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/MasterPage/Footer/CasaSoftFooterLink.ascx"
    TagName="CasaSoftFooterLink" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/MasterPage/Footer/SiteCopyrightText.ascx"
    TagName="SiteCopyrightText" TagPrefix="Common" %>
<footer>
    <div class="main-footer-content clearfix">
        <div class="main-footer-content-sitemap main-footer-content-item" runat="server" id="divFooterSitemap">
            <Common:FooterSitemap runat="server" ID="FooterSitemap" />
        </div>
        <div class="main-footer-content-product-categories main-footer-content-item" runat="server" id="divFooterProductCategories">
            <Common:ProductCategoriesListing runat="server" ID="ProductCategoriesListing" />
        </div>
        <div class="main-footer-content-contact-details main-footer-content-item" runat="server" id="divFooterContactDetails">
            <Common:ContactUsDetails runat="server" ID="ContactUsDetails" />
        </div>
    </div>
    <div class="main-footer-content-text clearfix">
        <Common:SiteCopyrightText runat="server" ID="SiteCopyrightText" />
        <Common:CasaSoftFooterLink runat="server" ID="CasaSoftFooterLink" />
    </div>
</footer>