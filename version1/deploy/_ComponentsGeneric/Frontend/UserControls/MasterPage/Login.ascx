﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.Login" %>
<div class="login-fields-container clearfix">
    <div class="logged-out-fields">
        <CSControls:MyTxtBoxEmail runat="server" ID="txtLoginEmail" Required="true" CssClass="login-section-field login-section-email"
            ValidationGroup="MASTERPAGE_LOGIN">
        </CSControls:MyTxtBoxEmail>
        <CSControls:MyTxtBoxString runat="server" ID="txtLoginUsername" Required="true" CssClass="login-section-field login-section-username"
            ValidationGroup="MASTERPAGE_LOGIN">
        </CSControls:MyTxtBoxString>
        <CSControls:MyTxtBoxStringPassword runat="server" ID="txtLoginPassword" Required="true"
            CssClass="login-section-field login-section-password" ValidationGroup="MASTERPAGE_LOGIN">
        </CSControls:MyTxtBoxStringPassword>
        <CSControls:MyButton runat="server" ID="btnLogin" Text="Login" ValidationGroup="MASTERPAGE_LOGIN"
            CssClass="login-section-button" />
    </div>
    <div class="header-container-login-navigational" runat="server" id="divNavigationalLinks">
        <CSControls:MyCheckBoxWithLabel runat="server" ID="chkKeepMeLoggedIn" />
        <a href="/" class="login-navigational-link-forgot-password" runat="server" id="anchorForgotPassword">Forgot your password?</a>
        <a href="/" class="login-navigational-link-register" runat="server" id="anchorRegister">Register</a>
    </div>
</div>
