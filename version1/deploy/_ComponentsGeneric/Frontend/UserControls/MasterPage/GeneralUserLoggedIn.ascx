﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralUserLoggedIn.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.GeneralUserLoggedIn" %>
<%@ Register TagPrefix="Hierarchy" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Hierarchy"
    Assembly="WebComponentsGeneralV3" %>
<div class="authentication-user-logged-in-container">
    <Hierarchy:HierarchicalControlNavigation ID="myAccountNavigation" runat="server" />
    <CSControls:MyButton runat="server" id="btnLogout" CssClass="link-button"></CSControls:MyButton>
</div>