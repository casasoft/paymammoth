﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainMenu.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.MainMenu" %>
<%@ Register TagPrefix="Hierarchy" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Hierarchy" Assembly="WebComponentsGeneralV3" %>
<div class="main-menu-wrapper">
    <nav>
        <Hierarchy:HierarchicalControlNavigation ID="topMenuNavigation" runat="server" />
    </nav>
</div>
