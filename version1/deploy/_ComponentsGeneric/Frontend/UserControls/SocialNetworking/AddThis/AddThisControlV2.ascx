﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddThisControlV2.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.SocialNetworking.AddThis.AddThisControlV2" %>
<%@ Register TagPrefix="AddThisButtons" TagName="FacebookLikeButton" Src="~/_ComponentsGeneric/Frontend/UserControls/SocialNetworking/AddThis/Buttons/FacebookLikeButton.ascx" %>
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_floating_style addthis_counter_style">
    <AddThisButtons:FacebookLikeButton id="fbButton" runat="server" />
    <a class="addthis_button_tweet" <%="tw:count"%>="vertical"></a>
    <a class="addthis_button_google_plusone" <%="g:plusone:size"%>="tall"></a>
    <a class="addthis_counter"></a>
</div>
<script type="text/javascript">    var addthis_config = { "data_track_addressbar": true };</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=casasoft"></script>
<!-- AddThis Button END -->