﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUsDetails.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Contact.ContactUsDetails" %>
<%@ Register TagPrefix="CommonControls" TagName="SocialNetworksIcons" Src="~/_ComponentsGeneric/Frontend/UserControls/SocialNetworking/SocialNetworksIcons.ascx" %>
<div class="contact-details-content">
    <h2 runat="server" id="h2ContactDetailsTitle" class="contact-details-title">
        Contact Details</h2>
     <span class="contact-details-director-name" runat="server" id="spanDirector"
            visible="false"></span><span class="contact-details-address" runat="server" id="spanAddress"
                visible="false"></span>    
                    <CSControls:MyTable runat="server" ID="tblDetails">
    </CSControls:MyTable>
    <CommonControls:SocialNetworksIcons runat="server" ID="socialNetworksIcons" CssClass="contact-details-socialnetworks-icons-container" />
    <div class="contact-details-map-container" runat="server" id="divMapContainer">

    </div>
</div>
