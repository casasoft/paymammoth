﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileFieldsPersonalDetails.ascx.cs"
    Inherits="CS.WebComponentsGeneralV1._ComponentsGeneric.Frontend.UserControls.Member.ProfileFields.ProfileFieldsPersonalDetails" %>
<%@ Register Assembly="General_v3" Namespace="CS.WebComponentsGeneralV1.Code.Controls.WebControls.FormFieldsOld"
    TagPrefix="CSFormFields" %>
<h2 runat="server" id="h2PersonalDetails">
    Personal Details</h2>
<CSFormFields:FormFields id="personalDetails" runat="server" />
<div class="button-wrapper" id="divButtonWrapper" runat="server" visible="false">
    <CSControls:MyButton runat="server" ID="btnSubmit" Text="Update" />
</div>
