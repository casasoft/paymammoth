﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileFieldsPasswordDetails.ascx.cs" Inherits="CS.WebComponentsGeneralV1._ComponentsGeneric.Frontend.UserControls.Member.ProfileFields.ProfileFieldsPasswordDetails" %>

<%@ Register Assembly="General_v3" Namespace="CS.WebComponentsGeneralV1.Code.Controls.WebControls.FormFieldsOld"
    TagPrefix="CSFormFields" %>

<h2 runat="server" id="h2ChangePassword" visible="false">Change Password</h2>
<CSFormFields:FormFields id="businessPasswordDetails" runat="server" />

<div class="button-wrapper" id="divButtonWrapper" runat="server" visible="false">
    <CSControls:MyButton runat="server" ID="btnSubmit" Text="Update"/> 
</div>