﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileFields.ascx.cs" Inherits="CS.WebComponentsGeneralV1._ComponentsGeneric.Frontend.UserControls.Member.ProfileFields.ProfileFields" %>
<%@ Register Assembly="General_v3" Namespace="CS.WebComponentsGeneralV1.Code.Controls.WebControls.FormFieldsOld"
    TagPrefix="CSFormFields" %>

<div class="profile-fields-content">
    <CSFormFields:FormFields id="profileFields" runat="server" />
</div>
<div class="button-wrapper">
    <CSControls:MyButton ID="btnSubmit" runat="server" />
</div>
 