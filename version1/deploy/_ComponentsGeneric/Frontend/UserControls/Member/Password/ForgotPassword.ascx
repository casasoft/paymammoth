﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.ascx.cs" Inherits="CS.WebComponentsGeneralV1._ComponentsGeneric.Frontend.UserControls.Member.Password.ForgotPassword" %>
<%@ Register Assembly="General_v3" Namespace="CS.WebComponentsGeneralV1.Code.Controls.WebControls.FormFieldsOld"
    TagPrefix="CSFormFields" %>

<div class="forgot-password-wrapper">
    <CSFormFields:FormFields id="formForgotPassword" runat="server" />
</div>
