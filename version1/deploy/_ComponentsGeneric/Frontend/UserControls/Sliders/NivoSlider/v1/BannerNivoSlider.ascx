﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BannerNivoSlider.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Sliders.NivoSlider.v1.BannerNivoSlider" %>
<%@ Register TagPrefix="CommonControls" TagName="NivoSlider" Src="NivoSlider.ascx" %>
<div class="banner-nivo-slider-container">
    <CommonControls:NivoSlider ID="nivoSlider" runat="server" />
</div>
