﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnythingSliderV1.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Sliders.AnythingSlider.v1.AnythingSliderV1" %>
<ul class="cs-anything-slider" id="ulSlider" runat="server">
    <li>
        <img src="demos/images/slide-civil-1.jpg" alt="" />/li>
    <li>
        <img src="demos/images/slide-env-1.jpg" alt=""></li>
    <li>
        <img src="demos/images/slide-civil-2.jpg" alt="" /></li>
    <li>
        <img src="demos/images/slide-env-2.jpg" alt="" /></li>
</ul>
