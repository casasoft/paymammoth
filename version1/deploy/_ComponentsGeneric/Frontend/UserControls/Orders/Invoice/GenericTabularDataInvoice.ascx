﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericTabularDataInvoice.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Orders.Invoice.GenericTabularDataInvoice" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Orders/Invoice/GenericInvoice.ascx" TagName="GenericInvoice" TagPrefix="CommonControls" %>

<CommonControls:GenericInvoice ID="invoice" runat="server">

    <Content>
        <table class="invoice-items-table" cellpadding="0" cellspacing="0">
            <thead>
                <th class="invoice-items-qty-cell" id="tdQtyHeaderCell" runat="server">
                   Qty.
                </th>
                <th class="invoice-items-desc-cell" id="tdDescHeaderCell" runat="server">
                    Description
                </th>
                <th class="invoice-items-unit-price-cell" id="tdUnitPriceHeaderCell" runat="server">
                    Unit Price
                </th>
                <th class="invoice-items-total-price-cell" id="tdTotalPriceHeaderCell" runat="server">
                    Total Price
                </th>
                <th class="invoice-items-tax-cell" id="tdTaxHeaderCell" runat="server">
                    VAT%
                </th>
            </thead>
            <tbody id="tItemsBody" runat="server">
                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        1 year subscription renewal ending on 31/12/2012
                    </td>
                    <td>
                        &euro;1.24
                    </td>
                    <td>
                        &euro;22.34
                    </td>
                    <td>
                        18%
                    </td>
                </tr>
                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        1 year subscription renewal ending on 31/12/2012
                    </td>
                    <td>
                        &euro;1.24
                    </td>
                    <td>
                        &euro;22.34
                    </td>
                    <td>
                        18%
                    </td>
                </tr>
                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        1 year subscription renewal ending on 31/12/2012
                    </td>
                    <td>
                        &euro;1.24
                    </td>
                    <td>
                        &euro;22.34
                    </td>
                    <td>
                        18%
                    </td>
                </tr>
                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        1 year subscription renewal ending on 31/12/2012
                    </td>
                    <td>
                        &euro;1.24
                    </td>
                    <td>
                        &euro;22.34
                    </td>
                    <td>
                        18%
                    </td>
                </tr>
                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        1 year subscription renewal ending on 31/12/2012
                    </td>
                    <td>
                        &euro;1.24
                    </td>
                    <td>
                        &euro;22.34
                    </td>
                    <td>
                        18%
                    </td>
                </tr>
            </tbody>
        </table>
    </Content>
    
</CommonControls:GenericInvoice>
