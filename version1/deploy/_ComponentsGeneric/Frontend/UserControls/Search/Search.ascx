﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Search.Search" %>
<div class="search-container">
    <CSControls:MyTxtBoxString Required="true" runat="server" id="txtSearch" ValidationGroup="search-field" CssClass="search-container-text"></CSControls:MyTxtBoxString>
    <CSControls:MyButton runat="server" id="btnSearch" CssClass="search-container-button" ValidationGroup="search-field"></CSControls:MyButton>
    
</div>