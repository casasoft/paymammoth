﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchField.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Search.SearchField" %>
<div class="search-container">
    <CSControls:MyTxtBoxString Required="true" runat="server" id="txtSearch" ValidationGroup="search-field" CssClass="search-container-text"></CSControls:MyTxtBoxString>
    <CSControls:MyButton runat="server" id="btnSearch" CssClass="search-container-button" ValidationGroup="search-field"></CSControls:MyButton>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.search-container-text').focus(function () {
            $(".search-container-button").addClass("search-container-button-focus");
        });

        $('.search-container-text').blur(function () {
            $(".search-container-button").removeClass("search-container-button-focus");
        });
    });
</script>