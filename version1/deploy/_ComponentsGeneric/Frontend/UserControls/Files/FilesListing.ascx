﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FilesListing.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Files.FilesListing" %>
<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="PagingBar" Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/Paging/PagingBar.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="FileListingItems" Src="~/_ComponentsGeneric/Frontend/UserControls/Files/FileListingItems.ascx" %>

<CommonControls:ContentPage runat="server" ID="contentPage" />
<div class="file-listing-items-container">
    <div class="file-listing-items-description html-container" runat="server" id="divHtmlText">
    </div>
    <div class="file-listing-content" runat="server" id="divFileListing">
        <CommonControls:FileListingItems runat="server" ID="fileListingItems" />
        <div class="file-listing-item-paging-container clearfix" runat="server" id="divBottomSection">
            <CommonControls:PagingBar runat="server" ID="pagingBarBottom" />
        </div>
    </div>
</div>
