﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileListingItem.ascx.cs"
 Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Files.FileListingItem" %>
<tr>
    <td class="file-listing-item-date-uploaded" runat="server" id="tdDateUploaded"></td>
    <td class="file-listing-item-filename" runat="server" id="tdFileName">
        <CSControls:MyButton runat="server" ID="btnFileListingItem" CssClass="link-button"></CSControls:MyButton>
    </td>
    <td class="file-listing-item-course" runat="server" id="tdCourse">
        <CSControls:MyAnchor runat="server" ID="anchorFileListingItemCourse"></CSControls:MyAnchor>
    </td>
</tr>