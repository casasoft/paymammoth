﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileListingItems.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Files.FileListingItems" %>
<table class="file-listing-items-table" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th runat="server" id="thDateUploaded">
            </th>
            <th runat="server" id="thFileName">
            </th>
            <th runat="server" id="thCourse">
            </th>
        </tr>
    </thead>
    <tbody runat="server" id="tBodyListing"></tbody>
</table>