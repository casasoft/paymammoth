﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LanguageSelection.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Languages.LanguageSelection" %>
<div class="language-selection-container" id="divLanguageSelectionContainer" runat="server">
    <div class="language-selection-current-selected-container">
       <span class="language-selection-icon"></span><span class="country-name"
            runat="server" id="spanCountryName"></span>
        <CSControls:MyImage runat="server" ID="imgCountryIcon" />
    </div>
    <div class="language-selection-available-languages" runat="server" id="divAvailableLanguage">
    </div>
</div>
