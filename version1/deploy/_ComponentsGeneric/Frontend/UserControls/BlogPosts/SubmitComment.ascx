﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubmitComment.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.BlogPosts.SubmitComment" %>
<div class="submit-comment-container">
    <div class="submit-comment-title-container">
        <span class="submit-comment-title" runat="server" id="spanPostCommentTitle">Post Comment</span>
    </div>
    <div class="submit-comments-fields-container">
        <CSControlsFormFields:FormFields ID="submitCommentFields" runat="server">
        </CSControlsFormFields:FormFields>
    </div>
</div>
