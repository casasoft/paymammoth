﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommentItem.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.BlogPosts.CommentItem" %>
<div class="comment-item-container">
    <span class="comment-item-author" runat="server" id="spanCommentAuthor"></span>
    <span class="comment-item-date" runat="server" id="spanCommentDate"></span>
    <div class="comment-content"><p runat="server" id="pCommentContent"></p></div>
</div>