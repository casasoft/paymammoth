﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PreviousNextArticle.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.BlogPosts.PreviousNextArticle" %>
<div class="previous-next-article-container clearfix">
    <a runat="server" id="anchorPreviousArticle" class="previous-article-link"></a>
    <a runat="server" id="anchorNextArticle" class="next-article-link"></a>
</div>