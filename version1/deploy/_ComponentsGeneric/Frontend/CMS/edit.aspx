<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/Frontend/CMS/cms.master" AutoEventWireup="true" CodeBehind="edit.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.CMS.editPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">


<asp:PlaceHolder ID="phBeforeForm" runat="server"></asp:PlaceHolder>

<CSControlsFormFields:FormFields ID="formFields" runat="server" />

<asp:PlaceHolder ID="phAfterForm" runat="server"></asp:PlaceHolder>


</asp:Content>
