<%@ Page Language="C#" MasterPageFile="~/_ComponentsGeneric/Frontend/CMS/cms.master" AutoEventWireup="true" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.CMS.login" Codebehind="login.aspx.cs" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/CMS/ucLoginPage.ascx" TagName="LoginPage" TagPrefix="CMS" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" Runat="Server">

<CMS:LoginPage ID="loginPage" runat="server" />

</asp:Content>

