<%@ Page Language="C#" MasterPageFile="~/_ComponentsGeneric/Frontend/CMS/cms.master" AutoEventWireup="true" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.CMS.welcome" Codebehind="welcome.aspx.cs" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/CMS/ucWelcomePage.ascx" TagName="WelcomePage" TagPrefix="CMS" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" Runat="Server">
<CMS:WelcomePage id="welcomePage" runat="server"></CMS:WelcomePage>
    
</asp:Content>

