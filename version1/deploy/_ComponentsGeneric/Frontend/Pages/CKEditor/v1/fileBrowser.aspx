﻿<%@ Page Title="CKEditor - File Browser" Language="C#" MasterPageFile="~/_ComponentsGeneric/Frontend/Pages/MasterPages/Common.Master" AutoEventWireup="true"
    CodeBehind="fileBrowser.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.CKEditor.v1.fileBrowserAspx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link  href="/_common/static/cms/css/cs-cms-v4.css" media="all" rel="stylesheet" type="text/css" />
<link  href="/_common/static/css/filebrowser.css" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/_common/static/js/ckeditor/ckeditor_3.6.3/ckeditor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainPlaceHolder" runat="server">
  <div id="divMain" runat="server" class="file-browser-scroll-div">
    
    </div>
    
</asp:Content>
