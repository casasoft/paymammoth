﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
     CodeBehind="login.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member.login" %>

<%@ Register TagPrefix="CommonControls" TagName="LoginPageContent" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/LoginPageContent.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:LoginPageContent ID="loginPage" runat="server" />
</asp:Content>
