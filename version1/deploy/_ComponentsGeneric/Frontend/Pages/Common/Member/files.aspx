﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="files.aspx.cs"
 Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member.files" %>

<%@ Register TagPrefix="CommonControls" TagName="FilesListing" Src="~/_ComponentsGeneric/Frontend/UserControls/Files/FilesListing.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:FilesListing runat="server" ID="filesListing" />
</asp:Content>
