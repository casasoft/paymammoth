﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="classified.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Classified.classified" %>
<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="General" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Navigation"
    Assembly="WebComponentsGeneralV3" %>
<%@ Register TagPrefix="CommonControls" TagName="ClassifiedContactDetails" Src="~/_ComponentsGeneric/Frontend/UserControls/Classifieds/ClassifiedContactDetails.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ClassifiedSubmitEnquiry" Src="~/_ComponentsGeneric/Frontend/UserControls/Classifieds/ClassifiedSubmitEnquiry.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <div class="content-page-breadcrumbs" runat="server" id="divNavigationBreadcrumbs">
        <General:NavigationBreadcrumbs runat="server" ID="navigationBreadcrumbs" />        
    </div>      
    <CommonControls:ContentPage ID="contentPage" runat="server">
        <TopContent>
            <div class="article-media-gallery-container">
                <CSControls:MyImage ID="imgClassified" runat="server" />
            </div> 
            <div class="classified-info-container clearfix">
                <div class="article-top-date-container">
                     
                </div> 
                <div runat="server" id="divPrice" class="classified-info-price"></div>      
            </div>   
        </TopContent>
    </CommonControls:ContentPage>
    <CommonControls:ClassifiedContactDetails runat="server" id="ctrlClassifiedContactDetails" />
    <CommonControls:ClassifiedSubmitEnquiry runat="server" id="ctrlClassifiedSubmitEnquiry" />
</asp:Content>
