﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="advert.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Advert.advert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" id="htmlTag" runat="server">
<head id="Head1" runat="server">
    <title></title>
    <CSControls:CombinerCtrl ID="CombinerCtrl1" runat="server">
        <script type="text/javascript" src="/_common/static/flash/v1/swfobject/2.2/swfobject.js"></script>
    </CSControls:CombinerCtrl>
    <style type="text/css">
        body
        {
            margin: 0px;
        }
        a img
        {
            border: none;
        }
        .advert-container
        {
            position: relative;
        }
        .media-item-swf-link-mask
        {
            position: absolute;
            left: 0px;
            top: 0px;
        }
    </style>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', '<asp:Literal ID="ltlAnalyticsID" runat="server"></asp:Literal>']);
        _gaq.push(['_setDomainName', '<asp:Literal ID="ltlAnalyticsDomain" runat="server"></asp:Literal>']);
        _gaq.push(['_trackPageview']);
        <asp:Literal id='ltlEventTracking' runat='server'></asp:Literal>

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</head>
<body id="bodyTag" runat="server">
    <form id="form1" runat="server">
    <div class="advert-container" id="divAdvert" runat="server">
        <div class="advert-slot-note-container" id="divAdvertSlotNote" runat="server" visible="false">
            Advert
        </div>
    </div>
    </form>
</body>
</html>
