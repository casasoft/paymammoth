﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
     CodeBehind="checkout.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.ShoppingCart.checkout" %>

<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="ShoppingCart" Src="~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/ShoppingCart.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:ContentPage ID="contentPage" runat="server" />
    <asp:PlaceHolder ID="shoppingCartPlaceholder" runat="server">
    <!-- This gets replaced through 'inversion-of-control' -->
        <CommonControls:ShoppingCart ID="shoppingCart1" runat="server" />
    </asp:PlaceHolder>
</asp:Content>