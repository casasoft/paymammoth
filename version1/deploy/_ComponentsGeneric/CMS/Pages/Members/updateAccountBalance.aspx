﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/CMS/Pages/cms.master"
    AutoEventWireup="true" CodeBehind="updateAccountBalance.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.Members.updateAccountBalance" %>

<%@ Register Src="~/_ComponentsGeneric/CMS/UserControls/DataImport/dataImport.ascx" TagName="dataImport"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">
<div class="update-account-balance">
    <p>
        This section is used to update the account balance of the member by any amount.
    </p>
    <div class="member-details">
        <div class="label-value">
            <span class="label">
                Member:</span>
            <span class="value" id="divMemberDetails" runat="server">
            [details]
            </span>
        </div>
        <div class="label-value">
            <span class="label">
                Current Balance:</span>
            <span class="value" id="divCurrentBalance" runat="server">
            [balance]
            </span>
        </div>
    </div>
    <hr />
    <CSControlsFormFields:FormFields ID="formFields" runat="server" />
    </div>
</asp:Content>
