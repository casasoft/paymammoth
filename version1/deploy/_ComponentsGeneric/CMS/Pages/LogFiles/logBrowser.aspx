﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/CMS/Pages/cms.master" AutoEventWireup="true" CodeBehind="logBrowser.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.LogFiles.logBrowser" %>

<%@ Register TagPrefix="CMSListing" Namespace="CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses" Assembly="WebComponentsGeneralV3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
<CMSListing:Listing ID="listingLogs" runat="server" />
<CSControls:MyButton id="btnViewForAll" runat="server" Text="View for all checked" />
</asp:Content>
