﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImportVoucherCodes.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.SpecialOffers.ImportVoucherCodes" %>
<%@ Register TagPrefix="CmsDataImport" Namespace="CS.WebComponentsGeneralV3.Code.Cms.WebControls.DataImport" %>
<h2>Voucher Code Import</h2>
<p>
Here you can bulk-import voucher codes.  Import should be done from a comma-seperated value file (CSV). Each line in the file represents one voucher code.  All columns in the row will be appended to form the voucher code.
The system will check if the voucher code exists, before adding.  A summary of the results will also be provided.
</p>


<CmsDataImport:DataImportForm ID="dataImportForm" runat="server">
    </CmsDataImport:DataImportForm>

