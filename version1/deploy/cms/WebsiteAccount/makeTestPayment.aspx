﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/CMS/pages/cms.master" AutoEventWireup="true" CodeBehind="makeTestPayment.aspx.cs" Inherits="PayMammothDeploy.cms.WebsiteAccount.makeTestPayment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">

<h2>Make a test payment</h2>
<p>Please fill in the fields below, in order to make a test payment.  When you click submit, you will be redirected to the frontend to perform the payment.</p>
<CSControlsFormFields:FormFields ID="formFields" runat="server" />


</asp:Content>
