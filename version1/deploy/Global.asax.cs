﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using BusinessLogic_v3.Modules.AuditLogModule;
using log4net;

namespace PayMammothDeploy
{
    public class Global : BusinessLogic_v3.Classes.WebApplications.WebApplication
    {
        public const bool AUTO_LOGIN = true;
        public static bool FACTORIES_INIT = false;
        public Global()
        {


        }

        private void initCommonFilesSettings()
        {

        }

        private void test()
        {
            var log1 = LogManager.GetLogger(typeof(System.Action));
            var log2 = LogManager.GetLogger(typeof(AuditLogBaseFactory));
        }

        protected void Application_Start(object sender, EventArgs e)
        {

          
            PayMammoth.Classes.Application.AppInstance.Instance.OnApplicationStart();
            initCommonFilesSettings();
            //PayMammoth.Connector.Constants._loadFromOfficeCasaSoftServer = false;
            test();

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            PayMammoth.Classes.Application.AppInstance.Instance.OnSessionStart();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }
        protected void Application_EndRequest(object sender, EventArgs e)
        {

        }
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}