// JavaScriptCMS_v4_1.js
//


Type.registerNamespace('js.com.cms.v3');

////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.Button

js.com.cms.v3.Button = function js_com_cms_v3_Button(btn) {
    /// <param name="btn" type="js.com.cs.v3.Controls.Common.MyElement">
    /// </param>
    /// <field name="_btn" type="js.com.cs.v3.Controls.Common.MyElement">
    /// </field>
    /// <field name="_grey" type="Boolean">
    /// </field>
    this._btn = btn;
    this._grey = (btn.get_cssClass() === 'button-grey');
    this._init();
}
js.com.cms.v3.Button.prototype = {
    _btn: null,
    _grey: false,
    
    _over: function js_com_cms_v3_Button$_over(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
        this._btn.set_cssClass(((this._grey) ? 'button-grey_over' : 'button_over'));
    },
    
    _up: function js_com_cms_v3_Button$_up(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
        this._btn.set_cssClass(((this._grey) ? 'button-grey' : 'button'));
    },
    
    _initHandlers: function js_com_cms_v3_Button$_initHandlers() {
        this._btn.add_onMouseOver(ss.Delegate.create(this, this._over));
        this._btn.add_onMouseOut(ss.Delegate.create(this, this._up));
    },
    
    _init: function js_com_cms_v3_Button$_init() {
        this._up(null, null);
        this._initHandlers();
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.Listing

js.com.cms.v3.Listing = function js_com_cms_v3_Listing(tableID) {
    /// <param name="tableID" type="String">
    /// </param>
    /// <field name="_table" type="js.com.cs.v3.Controls.Common.MyTableElement">
    /// </field>
    /// <field name="_tableID" type="String">
    /// </field>
    /// <field name="_rows" type="Array" elementType="ListingRow">
    /// </field>
    this._tableID = tableID;
    js.com.jquery.v3.jQueryUtil.addOnReadyCallback(ss.Delegate.create(this, this._onLoad));
}
js.com.cms.v3.Listing.prototype = {
    _table: null,
    _tableID: null,
    _rows: null,
    
    _initRows: function js_com_cms_v3_Listing$_initRows() {
        var elems = js.com.jquery.v3.jQueryUtil.select("tr[class='row_0'],tr[class='row_1']", this._table.get_element()).get();
        this._rows = [];
        for (var i = 0; i < elems.length; i++) {
            this._rows[this._rows.length] = new js.com.cms.v3.ListingRow(new js.com.cs.v3.Controls.Common.MyTableRowElement(elems[i]));
        }
    },
    
    _onLoad: function js_com_cms_v3_Listing$_onLoad() {
        this._table = new js.com.cs.v3.Controls.Common.MyTableElement(this._tableID);
        this._init();
    },
    
    _init: function js_com_cms_v3_Listing$_init() {
        this._initRows();
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.ListingRow

js.com.cms.v3.ListingRow = function js_com_cms_v3_ListingRow(tr) {
    /// <param name="tr" type="js.com.cs.v3.Controls.Common.MyTableRowElement">
    /// </param>
    /// <field name="_tr" type="js.com.cs.v3.Controls.Common.MyTableRowElement">
    /// </field>
    /// <field name="_initClass" type="String">
    /// </field>
    this._tr = tr;
    this._initClass = tr.get_cssClass();
    this._init();
}
js.com.cms.v3.ListingRow.prototype = {
    _tr: null,
    _initClass: null,
    
    _onOut: function js_com_cms_v3_ListingRow$_onOut(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
        this._tr.set_cssClass(this._initClass);
    },
    
    _onOver: function js_com_cms_v3_ListingRow$_onOver(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
        this._tr.set_cssClass(this._initClass + ' over');
    },
    
    _initHandlers: function js_com_cms_v3_ListingRow$_initHandlers() {
        this._tr.add_onMouseOver(ss.Delegate.create(this, this._onOver));
        this._tr.add_onMouseOut(ss.Delegate.create(this, this._onOut));
    },
    
    _init: function js_com_cms_v3_ListingRow$_init() {
        this._initHandlers();
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.SearchButton

js.com.cms.v3.SearchButton = function js_com_cms_v3_SearchButton(btnSearchID, divSearchID, showInitally) {
    /// <param name="btnSearchID" type="String">
    /// </param>
    /// <param name="divSearchID" type="String">
    /// </param>
    /// <param name="showInitally" type="Boolean">
    /// </param>
    /// <field name="_btnSearchID" type="String">
    /// </field>
    /// <field name="_divSearchID" type="String">
    /// </field>
    /// <field name="_showInitially" type="Boolean">
    /// </field>
    /// <field name="_btnSearch" type="js.com.cs.v3.Controls.Form.MyButton">
    /// </field>
    /// <field name="_divSearch" type="js.com.cs.v3.Controls.Common.MyDivElement">
    /// </field>
    /// <field name="_initHeight" type="Number" integer="true">
    /// </field>
    /// <field name="_duration" type="Number" integer="true">
    /// </field>
    /// <field name="_shown" type="Boolean">
    /// </field>
    this._showInitially = showInitally;
    this._btnSearchID = btnSearchID;
    this._divSearchID = divSearchID;
    js.com.jquery.v3.jQueryUtil.addOnReadyCallback(ss.Delegate.create(this, this._onLoad));
}
js.com.cms.v3.SearchButton.prototype = {
    _btnSearchID: null,
    _divSearchID: null,
    _showInitially: false,
    _btnSearch: null,
    _divSearch: null,
    _initHeight: 0,
    _duration: 250,
    _shown: false,
    
    _click: function js_com_cms_v3_SearchButton$_click(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
        this._toggle();
    },
    
    _toggle: function js_com_cms_v3_SearchButton$_toggle() {
        if (this._divSearch.isVisible()) {
            this._hide();
        }
        else {
            this._show();
        }
    },
    
    _show: function js_com_cms_v3_SearchButton$_show() {
        this._shown = true;
        this._divSearch.get_jQueryElement().slideDown(500);
        this._updateClass(true);
    },
    
    _updateClass: function js_com_cms_v3_SearchButton$_updateClass(visible) {
        /// <param name="visible" type="Boolean">
        /// </param>
        this._btnSearch.toggleClass('button-search-panel-toggle-collapse', visible);
    },
    
    _hide: function js_com_cms_v3_SearchButton$_hide() {
        this._divSearch.get_jQueryElement().slideUp(500);
        this._shown = false;
        this._updateClass(false);
    },
    
    _hide_onReady: function js_com_cms_v3_SearchButton$_hide_onReady() {
        this._divSearch.get_style().display = 'none';
    },
    
    _init: function js_com_cms_v3_SearchButton$_init() {
        this._btnSearch.add_onMouseDown(ss.Delegate.create(this, this._click));
    },
    
    _onLoad: function js_com_cms_v3_SearchButton$_onLoad() {
        this._btnSearch = new js.com.cs.v3.Controls.Form.MyButton(this._btnSearchID);
        this._divSearch = new js.com.cs.v3.Controls.Common.MyDivElement(this._divSearchID);
        this._divSearch.get_style().display = 'none';
        this._divSearch.get_style().overflow = 'hidden';
        this._divSearch.get_style().display = '';
        this._initHeight = this._divSearch.get_jQueryElement().innerHeight();
        this._divSearch.get_style().display = 'none';
        if (this._showInitially) {
            this._divSearch.get_style().display = '';
            this._updateClass(true);
        }
        this._init();
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.SearchPanelController

js.com.cms.v3.SearchPanelController = function js_com_cms_v3_SearchPanelController() {
    /// <field name="_btnToggle" type="js.com.cs.v3.Controls.Common.MyElement">
    /// </field>
    /// <field name="_elemPanel" type="js.com.cs.v3.Controls.Common.MyElement">
    /// </field>
    js.com.jquery.v3.jQueryUtil.addOnReadyCallback(ss.Delegate.create(this, this._init));
}
js.com.cms.v3.SearchPanelController.prototype = {
    _btnToggle: null,
    _elemPanel: null,
    
    _initControls: function js_com_cms_v3_SearchPanelController$_initControls() {
        var jButton = js.com.jquery.v3.jQueryUtil.select('.button-search-panel-toggle');
        if (jButton.length > 0) {
            this._btnToggle = new js.com.cs.v3.Controls.Common.MyElement();
            var jElemPanel = js.com.jquery.v3.jQueryUtil.select('.search-form');
            if (jElemPanel.length > 0) {
                this._elemPanel = new js.com.cs.v3.Controls.Common.MyElement(jElemPanel);
                this._initHandlers();
            }
        }
    },
    
    _initHandlers: function js_com_cms_v3_SearchPanelController$_initHandlers() {
        this._btnToggle.add_onClick(ss.Delegate.create(this, this._btnToggle_onClick));
    },
    
    _btnToggle_onClick: function js_com_cms_v3_SearchPanelController$_btnToggle_onClick(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
        this._toggle();
    },
    
    _toggle: function js_com_cms_v3_SearchPanelController$_toggle() {
        if (this._elemPanel.isVisible()) {
            this._elemPanel.get_jQueryElement().slideUp(500);
        }
        else {
            this._elemPanel.get_jQueryElement().slideDown(500);
        }
    },
    
    _init: function js_com_cms_v3_SearchPanelController$_init() {
        this._initControls();
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.ShowAmtComboBox

js.com.cms.v3.ShowAmtComboBox = function js_com_cms_v3_ShowAmtComboBox(cmbID, qsShowAmtVarName, qsPageVarName) {
    /// <param name="cmbID" type="String">
    /// </param>
    /// <param name="qsShowAmtVarName" type="String">
    /// </param>
    /// <param name="qsPageVarName" type="String">
    /// </param>
    /// <field name="_cmbID" type="String">
    /// </field>
    /// <field name="_cmb" type="js.com.cs.v3.Controls.Form.MyDropDownList">
    /// </field>
    /// <field name="_qsShowAmtVarName" type="String">
    /// </field>
    /// <field name="_qsPageVarName" type="String">
    /// </field>
    this._cmbID = cmbID;
    this._qsPageVarName = qsPageVarName;
    this._qsShowAmtVarName = qsShowAmtVarName;
    js.com.jquery.v3.jQueryUtil.addOnReadyCallback(ss.Delegate.create(this, this._onLoad));
}
js.com.cms.v3.ShowAmtComboBox.prototype = {
    _cmbID: null,
    _cmb: null,
    _qsShowAmtVarName: null,
    _qsPageVarName: null,
    
    _change: function js_com_cms_v3_ShowAmtComboBox$_change(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
        var value = this._cmb.get_value();
        var url = new js.com.cs.v3.Classes.URL.URLHandler(window.location.toString());
        url.set_item(this._qsPageVarName, '');
        url.set_item(this._qsShowAmtVarName, value);
        js.com.cs.v3.Util.WindowUtil.changeLocation(url.toString());
    },
    
    _init: function js_com_cms_v3_ShowAmtComboBox$_init() {
        this._cmb.add_onChange(ss.Delegate.create(this, this._change));
    },
    
    _onLoad: function js_com_cms_v3_ShowAmtComboBox$_onLoad() {
        this._cmb = new js.com.cs.v3.Controls.Form.MyDropDownList(this._cmbID);
        this._init();
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.ListingCheckboxSelector

js.com.cms.v3.ListingCheckboxSelector = function js_com_cms_v3_ListingCheckboxSelector(chkSelectorID, prefixID, amtCheckboxes) {
    /// <param name="chkSelectorID" type="String">
    /// </param>
    /// <param name="prefixID" type="String">
    /// </param>
    /// <param name="amtCheckboxes" type="Number" integer="true">
    /// </param>
    /// <field name="_chk" type="js.com.cs.v3.Controls.Form.MyCheckBox">
    /// </field>
    /// <field name="_chkSelectorID" type="String">
    /// </field>
    /// <field name="_prefixID" type="String">
    /// </field>
    /// <field name="_amtCheckboxes" type="Number" integer="true">
    /// </field>
    this._prefixID = prefixID;
    this._amtCheckboxes = amtCheckboxes;
    this._chkSelectorID = chkSelectorID;
    js.com.jquery.v3.jQueryUtil.addOnReadyCallback(ss.Delegate.create(this, this._onLoad));
}
js.com.cms.v3.ListingCheckboxSelector.prototype = {
    _chk: null,
    _chkSelectorID: null,
    _prefixID: null,
    _amtCheckboxes: 0,
    
    _change: function js_com_cms_v3_ListingCheckboxSelector$_change(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
        for (var i = 0; i < this._amtCheckboxes; i++) {
            var chk = new js.com.cs.v3.Controls.Form.MyCheckBox(this._prefixID + '_' + i);
            chk.set_checked(this._chk.get_checked());
        }
    },
    
    _onLoad: function js_com_cms_v3_ListingCheckboxSelector$_onLoad() {
        this._chk = new js.com.cs.v3.Controls.Form.MyCheckBox(this._chkSelectorID);
        this._chk.add_onChange(ss.Delegate.create(this, this._change));
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.MaxWords

js.com.cms.v3.MaxWords = function js_com_cms_v3_MaxWords(item, maxWords) {
    /// <param name="item" type="js.com.cs.v3.Controls.Form.MyTextBox">
    /// </param>
    /// <param name="maxWords" type="Number" integer="true">
    /// </param>
    /// <field name="_t" type="Object">
    /// </field>
    /// <field name="_item" type="js.com.cs.v3.Controls.Form.MyTextBox">
    /// </field>
    this._item = item;
    var wordLimit = new js.com.cs.v3.Classes.Forms.Validation.WordsLimit(item, maxWords, null);
    wordLimit.add_onChange(ss.Delegate.create(this, this._wordLimit_onChange));
}
js.com.cms.v3.MaxWords.initMaxWords = function js_com_cms_v3_MaxWords$initMaxWords() {
    var elems = js.com.jquery.v3.jQueryUtil.select('[maxWords]').get();
    for (var i = 0; i < elems.length; i++) {
        var elem = new js.com.cs.v3.Controls.Common.MyElement(elems[i]);
        var maxWords = parseInt(elem.get_jQueryElement().attr('maxWords'));
        var maxWordsClass = new js.com.cms.v3.MaxWords(elem, maxWords);
    }
}
js.com.cms.v3.MaxWords.prototype = {
    _t: null,
    _item: null,
    
    _wordLimit_onChange: function js_com_cms_v3_MaxWords$_wordLimit_onChange(amtRemaining) {
        /// <param name="amtRemaining" type="Number" integer="true">
        /// </param>
        var divs = js.com.jquery.v3.jQueryUtil.select("[class='dijitTooltipContainer dijitTooltipContents']").get();
        var div = null;
        if (divs.length > 0) {
            div = new js.com.cs.v3.Controls.Common.MyDivElement(divs[0]);
        }
        var str = '';
        if (!ss.isNullOrUndefined(this._item.get_jQueryElement().attr(js.com.cms.v3.Util.General.helP_MESSAGE_ATTRIBUTE))) {
            var helpMessage = this._item.get_jQueryElement().attr(js.com.cms.v3.Util.General.helP_MESSAGE_ATTRIBUTE);
            str += helpMessage + '<br />';
        }
        str += amtRemaining + ' word' + ((amtRemaining === 1) ? '' : 's') + 'remaining.';
        if (!ss.isNullOrUndefined(div)) {
            div.set_innerHTML(str);
        }
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.LoadingMessage

js.com.cms.v3.LoadingMessage = function js_com_cms_v3_LoadingMessage(divLoading, divFader, divLoadingContainer) {
    /// <param name="divLoading" type="js.com.cs.v3.Controls.Common.MyDivElement">
    /// </param>
    /// <param name="divFader" type="js.com.cs.v3.Controls.Common.MyDivElement">
    /// </param>
    /// <param name="divLoadingContainer" type="js.com.cs.v3.Controls.Common.MyDivElement">
    /// </param>
    /// <field name="loadingMessageInstance" type="js.com.cms.v3.LoadingMessage" static="true">
    /// </field>
    /// <field name="_divLoading" type="js.com.cs.v3.Controls.Common.MyDivElement">
    /// </field>
    /// <field name="_divFader" type="js.com.cs.v3.Controls.Common.MyDivElement">
    /// </field>
    /// <field name="_divLoadingContainer" type="js.com.cs.v3.Controls.Common.MyDivElement">
    /// </field>
    /// <field name="_shown" type="Boolean">
    /// </field>
    /// <field name="_resizeHandle" type="js.com.cs.v3.Controls.Common.Function">
    /// </field>
    /// <field name="_scrollHandle" type="js.com.cs.v3.Controls.Common.Function">
    /// </field>
    /// <field name="_animation" type="Object">
    /// </field>
    /// <field name="_loadingWidth" type="Number" integer="true">
    /// </field>
    /// <field name="_loadingHeight" type="Number" integer="true">
    /// </field>
    this._divLoading = divLoading;
    this._divFader = divFader;
    this._divLoadingContainer = divLoadingContainer;
    this._loadingWidth = divLoading.get_jQueryElement().outerWidth();
    this._loadingHeight = divLoading.get_jQueryElement().outerHeight();
    this._divFader.get_style().display = this._divFader.get_style().display = 'none';
    this._resize(null, null);
    this._init();
}
js.com.cms.v3.LoadingMessage.initLoadingMessge = function js_com_cms_v3_LoadingMessage$initLoadingMessge() {
    var divLoading = new js.com.cs.v3.Controls.Common.MyDivElement('divLoading');
    if (ss.isNullOrUndefined(divLoading)) {
        return;
    }
    js.com.cms.v3.LoadingMessage.loadingMessageInstance = new js.com.cms.v3.LoadingMessage(divLoading, new js.com.cs.v3.Controls.Common.MyDivElement('divFader'), new js.com.cs.v3.Controls.Common.MyDivElement('divLoadingContainer'));
    js.com.cms.v3.LoadingMessage.loadingMessageInstance.hide();
}
js.com.cms.v3.LoadingMessage._currentForm_OnSubmitEvent = function js_com_cms_v3_LoadingMessage$_currentForm_OnSubmitEvent(sender, e) {
    /// <param name="sender" type="Object">
    /// </param>
    /// <param name="e" type="ss.EventArgs">
    /// </param>
    js.com.cms.v3.LoadingMessage.loadingMessageInstance.show();
}
js.com.cms.v3.LoadingMessage.prototype = {
    _divLoading: null,
    _divFader: null,
    _divLoadingContainer: null,
    _shown: false,
    _resizeHandle: null,
    _scrollHandle: null,
    _animation: null,
    _loadingWidth: 0,
    _loadingHeight: 0,
    
    _stopCurrentAnimation: function js_com_cms_v3_LoadingMessage$_stopCurrentAnimation() {
        this._divLoadingContainer.get_jQueryElement().stop();
    },
    
    _resize: function js_com_cms_v3_LoadingMessage$_resize(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
        var viewPortW = js.com.cs.v3.Util.WindowUtil.getViewportWidth(true);
        var viewPortH = js.com.cs.v3.Util.WindowUtil.getViewportHeight(true);
        var viewPortTop = js.com.cs.v3.Util.WindowUtil.getScrollTop();
        var left = (viewPortW - this._loadingWidth) / 2;
        var top = (viewPortH - this._loadingHeight) / 2 + viewPortTop;
        this._divLoading.get_style().left = left + 'px';
        this._divLoading.get_style().top = top + 'px';
        this._divFader.get_style().top = viewPortTop + 'px';
    },
    
    show: function js_com_cms_v3_LoadingMessage$show() {
        this._resize(null, null);
        this._resizeHandle = ss.Delegate.create(this, this._resize);
        this._scrollHandle = ss.Delegate.create(this, this._resize);
        js.com.cs.v3.Util.WindowUtil.get_windowElement().add_onResize(this._resizeHandle);
        js.com.cs.v3.Util.WindowUtil.get_windowElement().add_onScroll(this._scrollHandle);
        this._divFader.get_style().display = this._divLoading.get_style().display = '';
        this._stopCurrentAnimation();
        this._divLoadingContainer.get_jQueryElement().animate({ opacity: '1' }, 500);
    },
    
    hide: function js_com_cms_v3_LoadingMessage$hide() {
        js.com.cs.v3.Util.WindowUtil.get_windowElement().remove_onResize(this._resizeHandle);
        js.com.cs.v3.Util.WindowUtil.get_windowElement().remove_onScroll(this._scrollHandle);
        this._stopCurrentAnimation();
        this._divLoadingContainer.get_jQueryElement().animate({ opacity: '0' }, 500, null, ss.Delegate.create(this, this.animationEnd));
    },
    
    animationEnd: function js_com_cms_v3_LoadingMessage$animationEnd() {
        this._divLoadingContainer.get_style().display = 'none';
    },
    
    _init: function js_com_cms_v3_LoadingMessage$_init() {
    }
}


////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.SideBar

js.com.cms.v3.SideBar = function js_com_cms_v3_SideBar(divSideBarID, tdSideBarHolderID, tdDraggerID) {
    /// <param name="divSideBarID" type="String">
    /// </param>
    /// <param name="tdSideBarHolderID" type="String">
    /// </param>
    /// <param name="tdDraggerID" type="String">
    /// </param>
    /// <field name="_divSideBar" type="js.com.cs.v3.Controls.Common.MyDivElement">
    /// </field>
    /// <field name="_tdSideBarHolder" type="js.com.cs.v3.Controls.Common.MyTableCellElement">
    /// </field>
    /// <field name="_tdDragger" type="js.com.cs.v3.Controls.Common.MyTableCellElement">
    /// </field>
    /// <field name="_imgDragger" type="js.com.cs.v3.Controls.Common.MyImageElement">
    /// </field>
    /// <field name="_COOKIE" type="String">
    /// </field>
    /// <field name="_divWidth" type="Number" integer="true">
    /// </field>
    /// <field name="_draggerWidth" type="Number" integer="true">
    /// </field>
    /// <field name="_duration" type="Number" integer="true">
    /// </field>
    /// <field name="_animation" type="Object">
    /// </field>
    /// <field name="_close" type="Boolean">
    /// </field>
    this._divSideBar = new js.com.cs.v3.Controls.Common.MyDivElement(divSideBarID);
    this._tdSideBarHolder = new js.com.cs.v3.Controls.Common.MyTableCellElement(tdSideBarHolderID);
    this._tdDragger = new js.com.cs.v3.Controls.Common.MyTableCellElement(tdDraggerID);
    this._imgDragger = new js.com.cs.v3.Controls.Common.MyImageElement(this._tdDragger.get_jQueryElement().find('*').eq(0));
    this._divWidth = this._divSideBar.get_jQueryElement().outerWidth();
    this._draggerWidth = this._tdDragger.get_jQueryElement().outerWidth() - 1;
    this._init();
    js.com.jquery.v3.jQueryUtil.addOnReadyCallback(ss.Delegate.create(this, this._initCookie));
}
js.com.cms.v3.SideBar.prototype = {
    _divSideBar: null,
    _tdSideBarHolder: null,
    _tdDragger: null,
    _imgDragger: null,
    _COOKIE: 'cs-cms-sidebar-open',
    _divWidth: 0,
    _draggerWidth: 0,
    _duration: 250,
    _animation: null,
    _close: true,
    
    _draggerOver: function js_com_cms_v3_SideBar$_draggerOver(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
    },
    
    _draggerOut: function js_com_cms_v3_SideBar$_draggerOut(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
    },
    
    _draggerClick: function js_com_cms_v3_SideBar$_draggerClick(elem, e) {
        /// <param name="elem" type="js.com.cs.v3.Controls.Common.MyElement">
        /// </param>
        /// <param name="e" type="js.com.jquery.v3.JQueryEvent">
        /// </param>
        this._toggle();
    },
    
    _initHandlers: function js_com_cms_v3_SideBar$_initHandlers() {
        this._tdDragger.add_onMouseOver(ss.Delegate.create(this, this._draggerOver));
        this._tdDragger.add_onMouseOut(ss.Delegate.create(this, this._draggerOut));
        this._tdDragger.add_onMouseDown(ss.Delegate.create(this, this._draggerClick));
    },
    
    _toggle: function js_com_cms_v3_SideBar$_toggle() {
        if (this._close) {
            this._closeSideBar(true);
        }
        else {
            this._openSideBar(true);
        }
    },
    
    _closeSideBar: function js_com_cms_v3_SideBar$_closeSideBar(useAnimations) {
        /// <param name="useAnimations" type="Boolean">
        /// </param>
        this._close = false;
        var endPos = -(this._divWidth - this._draggerWidth);
        var endWidth = this._draggerWidth;
        if (useAnimations) {
            this._divSideBar.get_jQueryElement().animate({ left: endPos + 'px' }, this._duration);
            this._tdSideBarHolder.get_jQueryElement().animate({ width: endWidth + 'px' }, this._duration);
        }
        else {
            this._divSideBar.get_style().left = endPos + 'px';
            this._tdSideBarHolder.get_style().width = endWidth + 'px';
        }
        try {
            jQuery.cookie(this._COOKIE, false);
        }
        catch (ex) {
        }
    },
    
    _openSideBar: function js_com_cms_v3_SideBar$_openSideBar(useAnimations) {
        /// <param name="useAnimations" type="Boolean">
        /// </param>
        this._close = true;
        var endPos = 0;
        var endWidth = this._divWidth;
        if (useAnimations) {
            this._divSideBar.get_jQueryElement().animate({ left: endPos + 'px' }, this._duration);
            this._tdSideBarHolder.get_jQueryElement().animate({ width: endWidth + 'px' }, this._duration);
        }
        else {
            this._divSideBar.get_style().left = endPos + 'px';
            this._tdSideBarHolder.get_style().width = endWidth + 'px';
        }
        try {
            jQuery.cookie(this._COOKIE, true);
        }
        catch (ex) {
        }
    },
    
    _init: function js_com_cms_v3_SideBar$_init() {
        this._initHandlers();
    },
    
    _initCookie: function js_com_cms_v3_SideBar$_initCookie() {
        var val = 'true';
        try {
            val = jQuery.cookie(this._COOKIE);
        }
        catch ($e1) {
        }
        if (ss.isNullOrUndefined(val) || val === 'true') {
            this._openSideBar(false);
        }
        else {
            this._closeSideBar(false);
        }
    }
}


Type.registerNamespace('js.com.cms.v3.Controllers');

////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.Controllers.EditPageShortcutsController

js.com.cms.v3.Controllers.EditPageShortcutsController = function js_com_cms_v3_Controllers_EditPageShortcutsController() {
    /// <field name="_btnSave" type="js.com.cs.v3.Controls.Form.MyButton">
    /// </field>
    /// <field name="_btnSaveAndAddAnother" type="js.com.cs.v3.Controls.Form.MyButton">
    /// </field>
    /// <field name="_btnSaveAndGoBack" type="js.com.cs.v3.Controls.Form.MyButton">
    /// </field>
    /// <field name="_btnDelete" type="js.com.cs.v3.Controls.Form.MyButton">
    /// </field>
    /// <field name="_btnGoBack" type="js.com.cs.v3.Controls.Form.MyButton">
    /// </field>
    js.com.jquery.v3.jQueryUtil.addOnReadyCallback(ss.Delegate.create(this, this._init));
}
js.com.cms.v3.Controllers.EditPageShortcutsController.prototype = {
    _btnSave: null,
    _btnSaveAndAddAnother: null,
    _btnSaveAndGoBack: null,
    _btnDelete: null,
    _btnGoBack: null,
    
    _getButton: function js_com_cms_v3_Controllers_EditPageShortcutsController$_getButton(className) {
        /// <param name="className" type="String">
        /// </param>
        /// <returns type="js.com.cs.v3.Controls.Form.MyButton"></returns>
        var jButton = js.com.jquery.v3.jQueryUtil.select(className);
        if (jButton.length > 0) {
            return new js.com.cs.v3.Controls.Form.MyButton(jButton);
        }
        else {
            return null;
        }
    },
    
    _initButtons: function js_com_cms_v3_Controllers_EditPageShortcutsController$_initButtons() {
        this._btnSave = this._getButton('.button-submit');
        this._btnSaveAndAddAnother = this._getButton('.btn-edit-save-and-add-another');
        this._btnSaveAndGoBack = this._getButton('.btn-edit-save-and-goback');
        this._btnDelete = this._getButton('.btn-edit-delete');
        this._btnGoBack = this._getButton('.btn-edit-goback');
    },
    
    _initKeyHandlers: function js_com_cms_v3_Controllers_EditPageShortcutsController$_initKeyHandlers() {
        js.com.cs.v3.Util.KeyboardUtil.addShortcutHandler('Ctrl+S', ss.Delegate.create(this, this._onSave));
        js.com.cs.v3.Util.KeyboardUtil.addShortcutHandler('Ctrl+N', ss.Delegate.create(this, this._onSaveAndAddAnother));
        js.com.cs.v3.Util.KeyboardUtil.addShortcutHandler('Ctrl+D', ss.Delegate.create(this, this._onDelete));
        js.com.cs.v3.Util.KeyboardUtil.addShortcutHandler('Ctrl+G', ss.Delegate.create(this, this._onSaveAndGoBack));
        js.com.cs.v3.Util.KeyboardUtil.addShortcutHandler('Ctrl+E', ss.Delegate.create(this, this._onGoBack));
    },
    
    _onSave: function js_com_cms_v3_Controllers_EditPageShortcutsController$_onSave() {
        if (this._btnSave != null) {
            this._btnSave.performClick();
        }
    },
    
    _onSaveAndAddAnother: function js_com_cms_v3_Controllers_EditPageShortcutsController$_onSaveAndAddAnother() {
        if (this._btnSaveAndAddAnother != null) {
            this._btnSaveAndAddAnother.performClick();
        }
    },
    
    _onDelete: function js_com_cms_v3_Controllers_EditPageShortcutsController$_onDelete() {
        if (this._btnDelete != null) {
            this._btnDelete.performClick();
        }
    },
    
    _onSaveAndGoBack: function js_com_cms_v3_Controllers_EditPageShortcutsController$_onSaveAndGoBack() {
        if (this._btnSaveAndGoBack != null) {
            this._btnSaveAndGoBack.performClick();
        }
    },
    
    _onGoBack: function js_com_cms_v3_Controllers_EditPageShortcutsController$_onGoBack() {
        if (this._btnGoBack != null) {
            this._btnGoBack.performClick();
        }
    },
    
    _init: function js_com_cms_v3_Controllers_EditPageShortcutsController$_init() {
        this._initButtons();
        this._initKeyHandlers();
    }
}


Type.registerNamespace('js.com.cms.v3.Util');

////////////////////////////////////////////////////////////////////////////////
// js.com.cms.v3.Util.General

js.com.cms.v3.Util.General = function js_com_cms_v3_Util_General() {
    /// <field name="helP_MESSAGE_ATTRIBUTE" type="String" static="true">
    /// </field>
}
js.com.cms.v3.Util.General.InitCMS = function js_com_cms_v3_Util_General$InitCMS() {
    js.com.cms.v3.Util.General.initBody();
    js.com.cms.v3.MaxWords.initMaxWords();
    js.com.cms.v3.Util.General.initHelpMessages();
    js.com.cms.v3.Util.General.initButtons();
    js.com.cms.v3.LoadingMessage.initLoadingMessge();
}
js.com.cms.v3.Util.General.initBody = function js_com_cms_v3_Util_General$initBody() {
    var body = new js.com.cs.v3.Controls.Common.MyElement(js.com.jquery.v3.jQueryUtil.select('body').get()[0]);
    body.get_style().position = 'absolute';
    body.get_style().left = '-2000px';
    js.com.jquery.v3.jQueryUtil.addOnReadyCallback(js.com.cms.v3.Util.General._body_onLoad);
}
js.com.cms.v3.Util.General._body_onLoad = function js_com_cms_v3_Util_General$_body_onLoad() {
    var body = new js.com.cs.v3.Controls.Common.MyElement(js.com.jquery.v3.jQueryUtil.select('body').get()[0]);
    body.get_style().position = '';
    body.get_style().left = '0px';
}
js.com.cms.v3.Util.General.initButtons = function js_com_cms_v3_Util_General$initButtons() {
}
js.com.cms.v3.Util.General.initHelpMessages = function js_com_cms_v3_Util_General$initHelpMessages() {
}


js.com.cms.v3.Button.registerClass('js.com.cms.v3.Button');
js.com.cms.v3.Listing.registerClass('js.com.cms.v3.Listing');
js.com.cms.v3.ListingRow.registerClass('js.com.cms.v3.ListingRow');
js.com.cms.v3.SearchButton.registerClass('js.com.cms.v3.SearchButton');
js.com.cms.v3.SearchPanelController.registerClass('js.com.cms.v3.SearchPanelController');
js.com.cms.v3.ShowAmtComboBox.registerClass('js.com.cms.v3.ShowAmtComboBox');
js.com.cms.v3.ListingCheckboxSelector.registerClass('js.com.cms.v3.ListingCheckboxSelector');
js.com.cms.v3.MaxWords.registerClass('js.com.cms.v3.MaxWords');
js.com.cms.v3.LoadingMessage.registerClass('js.com.cms.v3.LoadingMessage');
js.com.cms.v3.SideBar.registerClass('js.com.cms.v3.SideBar');
js.com.cms.v3.Controllers.EditPageShortcutsController.registerClass('js.com.cms.v3.Controllers.EditPageShortcutsController');
js.com.cms.v3.Util.General.registerClass('js.com.cms.v3.Util.General');
js.com.cms.v3.LoadingMessage.loadingMessageInstance = null;
js.com.cms.v3.Util.General.helP_MESSAGE_ATTRIBUTE = 'helpMessage';
