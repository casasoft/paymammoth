﻿Imports Dovetail.Realex.Realauth

Module Module1

    Sub Main()


        ' Create a quote processor object
        Dim p As New RealexProcessor()
        ' populate the request
        Dim r As RealexRequest = p.Request

        ' Specify the request type.  Usually an "Auth": a request for payment.
        r.RequestType = RequestType.Auth

        ' these supplied by Realex
        r.MerchantId = "ENTER YOURS"
        r.SharedSecret = "ENTER YOURS"
        r.AccountName = "ENTER YOURS"

        ' any unique value. Usually the Primary Key from your Orders table. For this sample we just make a unique string.
        r.OrderId = NewOrderID()

        ' amount must be provided in CENT if using Euro, so multiply the supplied value by 100.
        r.Amount = 12345

        ' currency
        r.Currency = Currency.EUR

        ' the cardholders name
        r.CardHolderName = "Some Cardholder"

        ' the cardnumber
        r.CardNumber = "4263971921001307"

        ' expiry date in MMyy format. e.g. May 2012 is "0512"
        r.CardExpDate = "0512"

        r.CardType = CardType.Visa

        ' three digit verification number
        r.CardVerificationNumber = 123

        ' whether to automatically send the transaction to the bank for settlement in the next batch (tonight)
        r.Autosettle = True

        ' issue the request. This call is synchronous.
        p.SendRequest()

        ' display the response. Based on the ResponseCode you may update a customer's order as paid etc. "00" indicates a successful payment.
        Dim rp As RealexResponse = p.Response
        Console.Out.WriteLine("Result: " + rp.ResultCode)
        Console.Out.WriteLine("Message: " + rp.ResponseMessage)
        Console.Out.WriteLine("AuthCode: " + rp.AuthCode)
        Console.Out.WriteLine("PasRef: " + rp.PasRef)
        Console.Out.WriteLine("OrderID: " + r.OrderId)


        Console.Out.WriteLine("Press any key to finish")
        Console.ReadKey()


    End Sub

    Private Function NewOrderID() As String
        ' Order ID must be unique across all your Realex accounts. Using the time to the second plus
        ' * a random number will pretty much guarantee this for testing purposes.
        ' * In the real world you will probably want to use something more meaningful such
        ' * as the Primary Key column from your Orders table.
        ' *
        Dim timestamp As String = DateTime.Now.ToString("yyyyMMddHHmmss")
        Dim r As New Random()
        Dim rand As Integer = r.[Next](1, 1000)
        Return timestamp + rand
    End Function

End Module
