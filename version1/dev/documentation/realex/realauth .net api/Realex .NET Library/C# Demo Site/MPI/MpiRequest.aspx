<%@ Page Language="c#" CodeBehind="MpiResponse.aspx.cs" AutoEventWireup="True" Inherits="Dovetail.Realex.DemoSite.Mpi.MpiRequest" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head id="head" runat="server">
    <title>Outbound</title>
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <style type="text/css">
        h2
        {
            font-weight: bolder;
            font-size: large;
        }
        #RealexRequest
        {
            border-color: #CCFFCC;
            border-style: solid;
            width: 550px;
            float: left;
            clear: both;
        }
        #RealexResponse
        {
            margin-top: 10px;
            border-style: solid;
            border-color: #FF9999;
            clear: both;
            width: 550px;
        }
        #DevelopersNote
        {
             margin-top: 10px;
             border-color: #FF9900;
             border-style: solid;
             
        }
        #autosettleinfo
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: smaller;
            background-color: Yellow;
            position: absolute;
            width: 280px;
            left: 300px;
            top: 270px;
            visibility: hidden;
        }
        .RequestInfo
        {
            float: left;
            background-color: #99CCFF;
            font-family: Arial, Helvetica, sans-serif;
            font-size: smaller;
            width: 400px;
        }
        .infoText
        {
            font-size: x-small;
        }
    </style>
</head>
<body id="body" runat="server">
    <form id="Form1" method="post" runat="server">

    <h1>Realex 3-D Secure Example</h1>
    <a href="../Default.aspx" style="float:left;">Home Page</a>
    
    <div id="RealexRequest">

    <h2>Request Parameters</h2>

    <table width="500" border="0" cellpadding="5" cellspacing="0">
        <tr>
            <td>Card Type</td>
            <td><asp:DropDownList ID="cboCardType" runat="server"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>Card Number</td>
            <td><asp:TextBox ID="txtCardNumber" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Card Holder Name</td>
            <td><asp:TextBox ID="txtCardHolderName" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Expiry Date<span class="infoText"><br />
                In MMYY format.&nbsp; e.g. enter May 2012 as &quot;0512&quot;</span></td>
            <td><asp:TextBox ID="txtExpiryDate" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Amount<span class="infoText"><br />
                Transaction amount in cent</span></td>
            <td><asp:TextBox ID="txtAmount" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Maximum Liability in Cent <span class="infoText">
                <br />
                This is the largest transaction that the merchant is willing to accept when there is no liability shift.&nbsp; 
                This would not normally be visible to the user and would instead come from your 
                config file.&nbsp; It is just here for your convenience while testing.</span></td>
            <td><asp:TextBox ID="txtMaxLiability" runat="server">100</asp:TextBox></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><asp:Button ID="btnSend" runat="server" Text="Send Request" OnClick="btnSend_Click" ></asp:Button></td>
        </tr>
    </table>
    </div>

    <!--DELETE_FROM_HERE-->
    <!-- This section will be deleted in FinalBuilder before shipping to Realex -->
    <asp:Panel runat="server" ID="pnlRequestInfoAuth" CssClass="RequestInfo">
        <h3>Quick populators</h3>
        <p>For your convenience while testing, these links will populate the fields with the values to run the 3-D Secure test scenarios.
        They use the PIT test credit card numbers which you should have already received.</p>
        <asp:LinkButton ID="btnScenario1" runat="server" onclick="btnScenarioX_Click">1: Cardholder not enrolled (liability shift)</asp:LinkButton><br />
        <asp:LinkButton ID="btnScenario2" runat="server" onclick="btnScenarioX_Click">2: Unable to verify enrollment (no shift)</asp:LinkButton><br />
        <asp:LinkButton ID="btnScenario3" runat="server" onclick="btnScenarioX_Click">3: Invalid response from enrollment server (no shift)</asp:LinkButton><br />
        <asp:LinkButton ID="btnScenario4" runat="server" onclick="btnScenarioX_Click">4: Enrolled but invalid response from  ACS (no shift)</asp:LinkButton><br />
        <asp:LinkButton ID="btnScenario5" runat="server" onclick="btnScenarioX_Click">5: Successful authentication (liability shift)</asp:LinkButton><br />
        <asp:LinkButton ID="btnScenario6" runat="server" onclick="btnScenarioX_Click">6: Successful authentication 13 digit (liability shift)</asp:LinkButton><br />
        <asp:LinkButton ID="btnScenario7" runat="server" onclick="btnScenarioX_Click">7: Issuing bank with Attempt ACS (liability shift)</asp:LinkButton><br />
        <asp:LinkButton ID="btnScenario8" runat="server" onclick="btnScenarioX_Click">8: Incorrect password entered (no shift)</asp:LinkButton><br />
        <asp:LinkButton ID="btnScenario9" runat="server" onclick="btnScenarioX_Click">9: Authentication unavailable (no shift)</asp:LinkButton><br />
        <asp:LinkButton ID="btnScenario10" runat="server" onclick="btnScenarioX_Click">10: Invalid response from ACS (no shift)</asp:LinkButton>
    </asp:Panel>
    <!--DELETE_TO_HERE-->


    <div id="RealexResponse">
        <h2>Response from Realex</h2>
        Time: <asp:Label ID="lblTimestamp" runat="server" Text=""></asp:Label><br />
        Outcome: <asp:Label ID="lblOutcome" runat="server" Text=""></asp:Label><br />
        Response code: <asp:Label ID="lblResult" runat="server" Text=""></asp:Label><br />
        Message: <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label><br />
        Order ID: <asp:Label ID="lblOrderID" runat="server" Text=""></asp:Label><br />
    </div>


    <div id="DevelopersNote">
        <h2>Notes for Developers</h2>
        <ul>
            <li>Be sure to update the web.config file with your Merchant ID, Shared Secret and Account Name.</li>
            <li>You will also need to be enrolled in Visa&#39;s Product Integration Testing 
                facility.&nbsp; Contact Realex about this.</li>
            <li>This Visual Studio Project is configured to run on port 26666.  You can change this by right clicking on the Project in the Solution Explorer.  
            Choose "Properties".  Then select the "Web" tab and edit the value for "Specific Port".  If you do this be sure to update the RealexMpiResponseUrl 
            in the web.config.</li>
            <li>Ensure that Realex have configured their systems to expect requests from your IP address.  By default your IP will be blocked for security reasons.</li>
            
            <li>This sample uses the Realex Payment Processor library, developed by 
                <a href="http://www.dovetail.ie">Dovetail Technologies</a> for Realex.&nbsp; 
                This is provided to you by Realex, and you are free to use the library in your 
                own applications.</li>

            <li>Any queries regarding this sample application should be directed to 
                <a href="http://www.realex.ie">Realex Payments</a>.</li>
        </ul>
        <h2>
            How to implement</h2>
        <p>
            To create your own implemention follow these steps:</p>
        <ul>
            <li>Create a new web application.</li>
            <li>Add a reference to the library Dovetail.Realex.Realauth.dll which you will find 
                in the bin folder of this sample.</li>
            <li>The first page you will need to create is your &quot;Request&quot; page.&nbsp; Call it 
                whatever you like but it must inherit from RealexMpiRequestPageBase.</li>
            <li>Implement the abstract properties Head, Body and ResponseURL.&nbsp; Head and 
                Body return the Head and Body of the new page.&nbsp; ResponseURL will return the 
                URL to your Response page (see below for more on the response page)</li>
            <li>The Request page should capture the user&#39;s credit card details: name, card 
                number, expiry date etc.</li>
            <li>Copy the code from btnSend_Click in this sample to send the request to Realex</li>
            <li>Then make a second page, which will be your &quot;Response&quot; page.&nbsp; You can call 
                this whatever you like, but it must inherit from RealexMpiResponsePageBase.&nbsp; 
                This is the page to which the Payer Authentication Response (PaRes) will be 
                submitted after a user has been asked for their password by their bank.</li>
            <li>Override the abstract SharedSecret property.</li>
            <li>In this page simply call the TryAuthorise method in the page load, and take 
                appropiate actions based on the response.&nbsp; Note that the full details of 
                the transaction will be available to you, such as the amount and the order ID.</li>
        </ul>
    </div>
    </form>
</body>
</html>