using System;
using System.Web.Configuration;
using Dovetail.Realex.Realauth;
using Dovetail.Realex.Realauth.Mpi;

namespace Dovetail.Realex.DemoSite.Mpi
{
    /// <summary>
    /// This page receives the PARes (Payer Authentication Response) from the cardholder's bank's ACS.
    /// With the PARes we are now in a position to go back to Realex and process the payment with any
    /// liability shift that we are now entitled to.
    /// </summary>
    public partial class MpiResponse : RealexMpiResponsePageBase
    {
        readonly string _sharedSecret = WebConfigurationManager.AppSettings["RealexSharedSecret"];

        /// <summary>
        /// Provides access to the shared secret for the underlying super class to
        /// decode information being returned to it from the bank's ACS and for further
        /// communications with the Realex servers
        /// </summary>
        public override string SharedSecret
        {
            get{return _sharedSecret;}
        }
		
        protected void Page_Load(object sender, EventArgs e)
        {
            // calling "TryAuthorise" starts the MPI process which decides
            // which actions to take on the results which are returned from the
            // Realex servers and the clients "Risk" value (i.e, the maximum value
            // the client is willing to risk being charged back when an MPI transaction
            // is not available

            switch(TryAuthorise())
            {
                case MpiResult.Authorised:
                    // the users credit card has been authorised for the amount
                    // specified in the initial request, log the details of the
                    // transaction to database
                    ShowResponse("Authorisation accepted");
                    break;
                case MpiResult.RiskTooHigh:
                    // based on the response from the ACS, an MPI transaction is not possible
                    // and therefore no liability shift is possible. The library has determined that
                    // the value of the transaction is greater than the level of liability the merchant will accept
                    ShowResponse("Risk not acceptable");
                    break;
                case MpiResult.Declined:
                    // the system attempted to authorise the credit card but it was declined
                    // for the amount requested
                    ShowResponse("Authorisation declined");
                    break;
                case MpiResult.Error:
                    // an error has occured from which the system cannot recover
                    ShowResponse("An error has occured: <br />");
                    ShowResponse(Processor.Response.ResponseMessage);
                    break;
            }
            lblTimestamp.Text = DateTime.Now.ToString("d/MM/yyyy HH:mm:ss");
        }

        private void ShowResponse(string outcomeMessage)
        {
            lblTimestamp.Text = DateTime.Now.ToString("d/MM/yyyy HH:mm:ss");
            lblResult.Text = Processor.Response.ResultCode;
            lblMessage.Text = Processor.Response.ResponseMessage;
            lblOrderID.Text = Processor.Request.OrderId;    // note that all your order information is still available in the Request.
            lblOutcome.Text = outcomeMessage;
        }

    }
}