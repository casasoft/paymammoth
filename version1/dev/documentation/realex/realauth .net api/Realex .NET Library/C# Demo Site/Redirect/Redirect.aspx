﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Redirect/Redirect.Master" AutoEventWireup="true" CodeBehind="Redirect.aspx.cs" Inherits="Dovetail.Realex.DemoSite.Redirect.Redirect" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Label ID="lblAmount" runat="server" Text="Label"></asp:Label>

    <asp:TextBox ID="txtAmount" runat="server">12.34</asp:TextBox>

    <asp:Button ID="btnSubmit" runat="server" Text="Submit" />

    <br />

    <a href="Redirect.aspx">Restart</a>    
    
    <!-- These is where we will insert the hidden fields -->
    <asp:Literal ID="litHiddenFields" runat="server" />
    
</asp:Content>
