﻿namespace CS.WebComponentsGeneralV3.Code.Util
{
    using System;
    using System.Collections.Generic;
    using CS.General_v3.JavaScript.Data;
    using System.Text.RegularExpressions;
    using System.Web.UI;
    using General_v3.JavaScript.Data;
    using General_v3.Util;
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;
    using System.Web.UI.WebControls;

    public static class JSUtil
    {

        public static string replaceInnerSemiColons(string text, char quotationMark, string replace)
        {
            int currIndex = 0;
            int semiColonIndex = text.IndexOf(";");
            while (semiColonIndex != -1)
            {
                int quoteIndex = text.IndexOf(quotationMark);
                bool insideQuote = false;
                while (quoteIndex != -1 && quoteIndex < semiColonIndex)
                {
                    insideQuote = !insideQuote;
                    currIndex = quoteIndex + 1;
                    quoteIndex = text.IndexOf(quotationMark, currIndex);
                }
                if (insideQuote)
                {
                    //Relace
                    text = text.Remove(semiColonIndex, 1);
                    text = text.Insert(semiColonIndex, replace);
                }
                semiColonIndex = text.IndexOf(";", semiColonIndex + 1);
            }
            return text;
        }


        public static string MakeJavaScript(string txt, bool addCarriageReturn)
        {
            string tmp = "";
            tmp = "<script type=\"text/javascript\">";
            if (addCarriageReturn) tmp += "\r\n";
            tmp += txt;
            if (addCarriageReturn)
            {
                tmp = replaceInnerSemiColons(tmp, '\'', "#$#");
                tmp = replaceInnerSemiColons(tmp, '"', "#$#");
                tmp = tmp.Replace(";", ";\r\n") + "\r\n";
                tmp = tmp.Replace("#$#", ";");
            }
            tmp += "</script>";
            return tmp;
        }
        /// <summary>
        /// Convert a list of object to a Javascript array according to the types.
        /// </summary>
        /// <example>Mark, John, Karl, 22 will be converted to ['Mark', 'John', 'Karl', 22]</example>
        /// <param name="a">A list of items</param>

        /// <returns>Javascript array</returns>
        public static string ConvertToArray<T>(IList<T> a)
        {
            return ConvertToArray(a, true);
        }

        /// <summary>
        /// Convert a list of object to a Javascript array according to the types.
        /// </summary>
        /// <example>Mark, John, Karl, 22 will be converted to ['Mark', 'John', 'Karl', 22]</example>
        /// <param name="a">A list of items</param>
        /// <param name="addQuotesToStrings">Wheter to add quotes '' to a string</param>
        /// <returns>Javascript array</returns>
        public static string ConvertToArray<T>(IList<T> a, bool addQuotesToStrings)
        {
            if (a == null) return null;
            string js = "[";
            for (int i = 0; i < a.Count; i++)
            {
                T obj = a[i];

                if (i > 0) js += ",";
                if (obj is string)
                {
                    if (addQuotesToStrings) js += "'";
                    js += a[i];
                    if (addQuotesToStrings) js += "'";
                }
                else if (CS.General_v3.Util.NumberUtil.IsNumeric(obj))
                {
                    js += a[i];
                }
            }
            js += "]";
            return js;
        }
        public static JSArrayFlash ConvertToJSArrayFlash<T>(IList<T> a)
        {

            JSArrayFlash arr = new JSArrayFlash();
            for (int i = 0; i < a.Count; i++)
            {

                arr.AddItem(a[i]);
            }
            return arr;
        }
        
        public static JSArray ConvertToJSArray<T>(IList<T> a)
        {

            JSArray arr = new JSArray();
            for (int i = 0; i < a.Count; i++)
            {
                
                arr.AddItem(a[i]);
            }
            return arr;
        }

        /// <summary>
        /// Navigate to an anchor link in the page on load
        /// </summary>
        /// <param name="page"></param>
        /// <param name="anchorLink"></param>
        public static void NavigateToAnchorLink(System.Web.UI.Page page, string anchorLink)
        {
            string js = "var loc = window.location.toString();";
            js += "if (loc.lastIndexOf('#') != -1) loc = loc.substring(0, loc.lastIndexOf('#'));";
            js += "window.location = loc + '#" + anchorLink + "';";
            page.ClientScript.RegisterStartupScript(page.GetType(), "anchor", js, true);

        }
        /// <summary>
        /// Returns either the string value in 'VALUE' format or if null returns 'null';
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetStringOrNull(string value)
        {
            if (value == null) return "null";
            return "\"" + value + "\"";
        }

        /*
        public static class Dojo
        {
            public static string AddOnLoad(string js)
            {
                return "dojo.addO2nLoad(function() { " + js + " });";
            }
        }
        */
        public static string EscapeTextForJS(object data)
        {
            string ret = "";
            if (data is string)
            {
                string tmpData = (string)data;
                tmpData = tmpData.Replace("\\", "\\\\");
                tmpData = tmpData.Replace("'", "\\'");
                tmpData = tmpData.Replace("\"", "\\\"");
                ret = tmpData;
            }
            else if (data is int || data is double || data is float ||
                data is Int32 || data is Int16 || data is Int64 || data is short
                || data is byte || data is long || data is Double || data is Byte)
            {
                ret = data.ToString();
            }
            else if (data is DateTime)
            {
                DateTime tmpData = (DateTime)data;
                ret = tmpData.ToString("yyyy-MM-dd hh:mm:ss");
            }
            else if (data is char || data is Char)
            {
                ret = data.ToString();
            }
            else if (data is bool)
            {
                bool b = false;

                b = (bool)data;
                ret = "false";
                if (b)
                    ret = "true";
            }
            return ret;

        }

        public static void SetFocus(string elementID)
        {
            //commented by karl on 26/01/2011 as this is not used anymore

            

           /* string js = "dojo.add3OnLoad(function() {";
            js += "dojo.byId('" + elementID + "').focus();";
            js += "});";

            CS.General_v3.Util.PageUtil.GetCurrentPage().ClientScript.RegisterStartupScript(CS.General_v3.Util.PageUtil.GetCurrentPage().GetType(), "focus_" + elementID, js, true);
            */

        }
        public static void RedirectTopFrame(string url, System.Web.UI.Page pg)
        {
            string js = "js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".Util.WindowUtil.redirectTopFrame(\"" + url + "\");";
            AddJSScriptToPage(js);
            //pg.ClientScript.RegisterStartupScript(pg.GetType(), CS.General_v3.Util.Date.Now.ToString(), js, true);

        }

        public static void RedirectParentFrame(string url, System.Web.UI.Page pg)
        {
            string js = "js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".Util.WindowUtil.redirectParentFrame(\"" + url + "\");";
            AddJSScriptToPage(js);
            //pg.ClientScript.RegisterStartupScript(pg.GetType(), CS.General_v3.Util.Date.Now.ToString(), js, true);
            
        }
        public static string GetPopupJS(string url, string windowName, string width, string height, bool scrollbars,
            bool locationBar,
            bool directories,
            bool statusBar,
            bool menuBar,
            bool toolBar,
            bool resizeable)
        {

            string js = "js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".Util.WindowUtil.openPopup('" + url + "','" + windowName + "','" + width + "','" + height + "'," + scrollbars.ToString().ToLower() + "," + locationBar.ToString().ToLower() + "," + directories.ToString().ToLower() + "," + statusBar.ToString().ToLower() + "," + menuBar.ToString().ToLower() + "," + toolBar.ToString().ToLower() + "," + resizeable.ToString().ToLower() + ")";
            return "javascript:" + js;
            /*
            public static void OpenPopup(string url, string windowName, string width, string height, bool scrollbars,
            bool locationBar,
            bool directories,
            bool statusBar,
            bool menuBar,
            bool toolBar,
            bool resizeable,
            POPUP_WINDOW_OPEN_POSITION position
           )*/
        }

        /// <summary>
        /// Returns a JSObject[Flash] from a particular object.  It will use reflection to iterate through all the properties
        /// and will create an object having keys the same as property names and the respective value.
        /// </summary>
        /// <param name="obj">The object to convert</param>
        /// <param name="forFlash">returns JSObject if false, JSObjectFlash if true</param>
        /// <param name="loadNonPublicTypes">Load even private properties</param>
        /// <returns></returns>
        /*public static JSObject GetJSObjectFromTypeWithReflection(object obj, bool forFlash = false, bool loadNonPublicTypes = false, bool copyNulls = false, bool allPropertiesStartWithLowerCase = false)
        {
            Type t = obj.GetType();
            JSObject jsObj = (forFlash ? new JSObjectFlash() : new JSObject());
            var props = ReflectionUtil.GetAllPropertiesForType(t, loadNonPublicTypes: loadNonPublicTypes, loadStaticProperties: false);
            for (int i = 0; i < props.Length; i++)
            {
                
                string propName = props[i].Name;
                object val = props[i].GetValue(obj, null);
                if (allPropertiesStartWithLowerCase && propName != null && propName.Length > 0)
                {
                    propName = propName.Substring(0, 1).ToLower() + propName.Substring(1);
                }
                if (val != null)
                {
                    jsObj.addPropertyFromObject(propName, val);
                }
                
            }
            var fields = t.GetFields();
            for (int i = 0; i < fields.Length; i++)
            {

                string fieldName = fields[i].Name;
                
                object val = fields[i].GetValue(obj);
                if (allPropertiesStartWithLowerCase && fieldName != null && fieldName.Length > 0)
                {
                    fieldName = fieldName.Substring(0, 1).ToLower() + fieldName.Substring(1);
                }
                if (val != null)
                {
                    jsObj.addPropertyFromObject(fieldName, val);
                }

            }
            return jsObj;
        }*/
        public static string StripIDsFromHTML(string html)
        {
            Regex r = new Regex("id=(.*?)", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Singleline);
            html = r.Replace(html, "");
            return html;
        }
        public static string GetJSWithScriptTags(string js)
        {
                js = @"<script>
                            " + js + @"
                        </script>";
            return js;
        }
        public static string GetDeferredJSScriptThroughJQuery(string js, bool addScriptTag = false)
        {
            js = "jQuery(function() { " + js + " });" + (CS.General_v3.Util.Other.IsLocalTestingMachine ? "\r\n\r\n" : null); //add line break only if testing
            if (addScriptTag)
            {
                js = GetJSWithScriptTags(js);
            }
            return js;
        }
        public static void AddJSScriptToControl(string js, Control control, bool deferJSExecutionThroughJQuery = true, bool addScriptTags = true)
        {
            if (deferJSExecutionThroughJQuery)
            {
                js = GetDeferredJSScriptThroughJQuery(js, addScriptTags);
            }
            else if (addScriptTags)
            {
                js = GetJSWithScriptTags(js);
            }
            control.Controls.Add(new Literal() { Text = js });
        }
        public static void AddJSScriptToPage(string js, int priority = 0, bool deferJSExecutionThroughJQuery = true)
        {
            JavascriptScriptManager.Instance.AddScript(js, priority, deferJSExecutionThroughJQuery);
        }
        /*
        public static void AddJSScriptToPage(string js, bool positionAtBottomOfHTML = true, bool deferJSThroughJQuery = true, Type type = null, string key = null, Page page = null)
        {

            if (page == null) page = CS.General_v3.Util.PageUtil.GetCurrentPage();
            if (type == null) type = page.GetType();
            if (key == null) key = CS.General_v3.Util.Random.GetGUID();
            
            if (deferJSThroughJQuery)
            {
                js = GetDeferredJSScriptThroughJQuery(js);
            }
            if (positionAtBottomOfHTML)
            {
                page.ClientScript.RegisterStartupScript(type, key, js, true);
            }
            else
            {
                page.ClientScript.RegisterClientScriptBlock(type, key, js, true);
            }
        }*/

    }
}
