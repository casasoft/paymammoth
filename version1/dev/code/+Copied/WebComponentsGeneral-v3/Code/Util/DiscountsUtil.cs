﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Frontend.ContentTextModule;
using log4net;

namespace CS.WebComponentsGeneralV3.Code.Util
{
    public static class DiscountsUtil
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(DiscountsUtil));

        public static AddDiscountCodeResult AddDiscountCodeFromSession(string username)
        {
           
            

            var user = BusinessLogic_v3.Modules.Factories.MemberFactory.GetMembersByUsernameOrEmail(username).FirstOrDefault().ToFrontendBase();
            string discountCode = BusinessLogic_v3.SessionData.MemberDiscountCode;
            if(user != null && discountCode !=null)
            {
                return user.Data.SetDiscountCode(discountCode);
            }
            
            return AddDiscountCodeResult.Invalid;
        }
    }
}