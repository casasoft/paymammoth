﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Frontend.ContentTextModule;

namespace CS.WebComponentsGeneralV3.Code.Util
{
    public static class DataUtil
    {
        public static void FillDropDownListFromEnum<TEnumType>(MyDropDownList cmb, ListItem blankItemOnTop = null, CS.General_v3.Enums.ENUM_SORT_BY sortBy = CS.General_v3.Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false, TEnumType? selectedValue = null) where TEnumType : struct, IConvertible
        {
            List<ContentTextBaseFrontend> contentTexts = null;
            var items = BusinessLogic_v3.Util.EnumUtils.GetListItemCollectionFromEnumAndAddContentTexts<TEnumType>(out contentTexts, blankItemOnTop, sortBy, addSpacesToCamelCasedName, selectedValue);
            for (int i = 0; i < items.Count; i++)
            {
                MyDropDownOption opt = new MyDropDownOption(items[i].Text, items[i].Value) {
                     LabelContentText = contentTexts[i]
                };
                opt.Functionality.Selected = items[i].Selected;
                opt.Functionality.Disabled = !items[i].Enabled;

                cmb.Functionality.Items.Add(opt);
            }
        }
    }
}