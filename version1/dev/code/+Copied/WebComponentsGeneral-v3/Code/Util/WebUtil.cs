﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Util
{
    public static class WebUtil
    {
        public static string UploadFileContentToTempFile(CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.MyFileUpload file)
        {
            string filename = file.FileName;
            string tmpFile = CS.General_v3.Util.IO.GetTempFilename(filename);
            CS.General_v3.Util.IO.SaveStreamToFile(file.UploadedFileStream, tmpFile);
            return tmpFile;

        }
    }
}