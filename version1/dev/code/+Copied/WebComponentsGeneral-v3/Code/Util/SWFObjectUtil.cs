﻿namespace CS.WebComponentsGeneralV3.Code.Util
{
    using CS.General_v3.Controls.WebControls.Common.SWFObject.v2;
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

    public static class SWFObjectUtil
    {

        public static string GetSWFObjectV2JS(string flashURL, string clientID, string width, string height, string flashVersion, string expressInstallURL)
        {
            JSONObjectDictionary flashParams = new JSONObjectDictionary();
            
            flashParams.AddProperty("scale", SWFObject.ScaleToString(SWFObject.SCALE.NoScale));
            flashParams.AddProperty("allowfullscreen", true);

            JSONObjectDictionary flashAttributes = new JSONObjectDictionary();
            flashAttributes.AddProperty("id", clientID);

            return GetSWFObjectV2JS(flashURL, clientID, width, height, flashVersion, expressInstallURL, null, flashParams, flashAttributes); 
        }

        public static string GetSWFObjectV2JS(string flashURL, string clientID, string width, string height, string flashVersion, string expressInstallURL, JSONObjectDictionary flashVars = null, JSONObjectDictionary flashParams = null, JSONObjectDictionary flashAttributes = null)
        {
            if (flashAttributes == null) flashAttributes = new JSONObjectDictionary();
            flashAttributes.AddProperty("name", clientID);

            flashAttributes.AddProperty("id", clientID);

            string jsFlashVars = (flashVars == null ? "{}" : flashVars.GetJSON());

            string js = "jQuery(function() { swfobject.embedSWF('" + flashURL + "', '" + clientID + "', '" + width + "', '" + height + "', '" + flashVersion + "', '" + expressInstallURL + "', " + jsFlashVars + ", " + (flashParams == null ? "{}" : flashParams.GetJSON()) + ", " + (flashAttributes == null ? "{}" : flashAttributes.GetJSON()) + ")});";
            return js;
        }
    }
}
