﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace CS.WebComponentsGeneralV3.Code.Util
{
    using General_v3.Util;

    public static class PrettyPhotoUtil
    {
        

        public static void InitJS()
        {
            const string key = "PrettyPhotoInitialized";
            bool initialized = CS.General_v3.Util.PageUtil.GetContextObject<bool>(key);
            if (!initialized)
            {
                CS.General_v3.Util.PageUtil.SetContextObject(key, true);
                string js = @"$(""a[rel^='prettyPhoto']"").prettyPhoto();";
                JSUtil.AddJSScriptToPage(js);

                string jsScript = "/_common/static/js/jQuery/plugins/prettyPhoto/3.1.4/js/jquery.prettyPhoto.js";
                string css = @"/_common/static/js/jQuery/plugins/prettyPhoto/3.1.3/css/prettyPhoto.css";
                PageUtil.RegisterJavaScriptInHeadOfPage(jsScript);
                PageUtil.RegisterCSSInHeadOfPage(css, 0);

            }
        }
    }
}