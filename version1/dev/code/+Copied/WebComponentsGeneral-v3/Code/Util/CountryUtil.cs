﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Util
{
    public static class CountryUtil
    {
        public static string GetFlagPathOfCountry(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 country)
        {
            string baseCountryPath = CS.General_v3.Settings.GetSettingFromDatabase<string>(General_v3.Enums.SETTINGS_ENUM.BaseRelativeFlagLocationDirectoryPath);
            string countryImageFileType = CS.General_v3.Settings.GetSettingFromDatabase<string>(General_v3.Enums.SETTINGS_ENUM.FlagImagesExtension);

            return baseCountryPath + CS.General_v3.Util.EnumUtils.GetCountry2LetterCode(country) + countryImageFileType;
        }
    }
}