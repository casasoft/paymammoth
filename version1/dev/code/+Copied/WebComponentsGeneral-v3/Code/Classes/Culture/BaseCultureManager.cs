﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using CS.General_v3;
using CS.General_v3.Classes.CultureManager;
using CS.General_v3.Classes.PorterStemmer;


namespace CS.WebComponentsGeneralV3.Code.Classes.Culture
{
    public abstract class BaseCultureManager : ICultureManager
    {
        public abstract CultureInfo GetCurrentCultureFormatProvider();
        public abstract NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForHTML();
        public abstract NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForUnicode();

        public abstract StemmerInterface GetStemmerForCurrentCulture();



        public string FormatNumberWithCurrentCulture(double d, CS.General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE formatType)
        {
            return CS.General_v3.Util.NumberUtil.FormatNumber(d, formatType, GetCurrentCultureFormatProvider());
        }

        public string FormatNumberWithCurrentCulture(double d, string formatType)
        {
            return d.ToString(formatType, GetCurrentCultureFormatProvider());
        }

        public string FormatDateTimeWithCurrentCulture(DateTime date, CS.General_v3.Util.Date.DATETIME_FORMAT formatType)
        {
            return CS.General_v3.Util.Date.FormatDateTime(date, formatType, GetCurrentCultureFormatProvider());

        }

        public string FormatDateTimeWithCurrentCulture(DateTime date, string formatType)
        {
            return date.ToString(formatType, GetCurrentCultureFormatProvider());

        }
        public virtual string FormatPriceAsHTML(double d, string specificCurrencySymbol = null)
        {
            return CS.General_v3.Util.NumberUtil.FormatNumber(d, CS.General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency, GetCurrentCurrencyNumberFormatProviderForHTML(), specificCurrencySymbol);
        }

        #region IBaseSessionCultureManager Members


        public virtual Enums.ISO_ENUMS.LANGUAGE_ISO639 GetCurrentLanguageCode()
        {
            return Enums.ISO_ENUMS.LANGUAGE_ISO639.English;
        }

        public virtual Enums.ISO_ENUMS.CURRENCY_ISO4217 GetCurrentCurrencyCode()
        {
            return Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro;
        }

        #endregion
    }
}
