﻿using System;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.CurrencyModule;

namespace CS.WebComponentsGeneralV3.Code.Classes.Culture
{
    public interface ICultureManager
    {

        string FormatPriceAsHTML(double d, string specificCurrencySymbol = null);
        System.Globalization.CultureInfo GetCurrentCultureFormatProvider();
        System.Globalization.NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForHTML();
        System.Globalization.NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForUnicode();

        CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 GetCurrentLanguageCode();
        CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 GetCurrentCurrencyCode();


        CS.General_v3.Classes.PorterStemmer.StemmerInterface GetStemmerForCurrentCulture();

        string FormatNumberWithCurrentCulture(double d, CS.General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE formatType);
        string FormatNumberWithCurrentCulture(double d, string formatType);
        
        string FormatDateTimeWithCurrentCulture(DateTime date, CS.General_v3.Util.Date.DATETIME_FORMAT formatType);
        string FormatDateTimeWithCurrentCulture(DateTime date, string formatType);

        long? CustomCultureOverrideIDForCurrentContext { get; set; }
        ICultureDetailsBase GetCulture();
        void SetCulture(ICultureDetailsBase culture);
        void SetCultureByCode(string cultureCode);
        long? CustomCurrencyOverrideIDForCurrentContext { get; set; }
        ICurrencyBase GetCurrency();
        void SetCurrency(ICurrencyBase currency);
        void SetCurrencyByCode(string currencyCode);
    }
}
