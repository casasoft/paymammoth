﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;
using BusinessLogic_v3.Frontend.ArticleModule;
using System.Web.UI;
using CS.General_v3.Classes.Factories;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Dialog
{
    public class DialogLinkControllerParameters : JSONObject
    {

        

        public string selectorElement;
        public string title;
        public int width;
        public int height;
        public string url;
        public string dialogCssClass;
        public bool modal = true;
        public bool closeable = true;

        public void AttachToControlPreRender(Control c)
        {
            c.PreRender += new EventHandler(c_PreRender);
            this.selectorElement = "#" + c.ClientID;
        }

        void c_PreRender(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            this.selectorElement = "#" + c.ClientID;
        }

        public void FillValuesFromArticleAndAttachToControl(ArticleBaseFrontend article, Control c, string url = null, bool setStaticID = true)
        {

            if (setStaticID)
            {
                c.ClientIDMode = ClientIDMode.Static;
                string id = "dialogbox-" + CS.General_v3.Util.Random.GetGUID();
                
                c.ID = id;
                this.selectorElement = "#" + id;
            }
            else
            {
                AttachToControlPreRender(c);
            }
            
            FillValuesFromArticle(article);
            if (!string.IsNullOrEmpty(url)) this.url = url;
        }

        public void FillValuesFromArticle(ArticleBaseFrontend article)
        {
            this.title = article.Data.Title;


            if (article.Data.DialogWidth.HasValue)
            {
                this.width = article.Data.DialogWidth.Value;
            }
            else
            {
                int defaultDialogWidth = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Dialog_DefaultWidth);
                this.width = defaultDialogWidth;
            }
            if (article.Data.DialogHeight.HasValue)
            {
                this.height = article.Data.DialogHeight.Value;
            }
            else
            {
                int defaultDialogHeight = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Dialog_DefaultHeight);
                this.height = defaultDialogHeight;
            }
            this.url = article.GetUrl();
            this.dialogCssClass = article.Data.DialogCssClass;
            this.modal = article.Data.DialogModal.GetValueOrDefault(true);
            this.closeable = article.Data.DialogModal.GetValueOrDefault(true);
        }


        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.JQuery.jQueryUI.Dialog.DialogLinkController";
        }

        //public override bool _DoNotOutputJS
        //{
        //    get
        //    {
        //        return !string.IsNullOrWhiteSpace(selectorElement) && !string.IsNullOrWhiteSpace(url) && base._DoNotOutputJS;
        //    }
        //    set
        //    {
        //        base._DoNotOutputJS = value;
        //    }
        //}
    }
}