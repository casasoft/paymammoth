﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CS.General_v3.Util;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data
{
    using Util;

    public class JavascriptScriptManagerItem
    {
        public string JavascriptCode { get; set; }
        public int Priority { get; set; }
        public bool DeferExecutionThroughJQueryOnLoad { get; set; }

        public JavascriptScriptManagerItem()
        {
            DeferExecutionThroughJQueryOnLoad = true;
            Priority = 0;
        }
        public string GetJavascript()
        {
            string js = this.JavascriptCode;
            if (DeferExecutionThroughJQueryOnLoad)
            {
                js = JSUtil.GetDeferredJSScriptThroughJQuery(js);
            }
            return js;
        }
    }

}