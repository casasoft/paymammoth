﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.DatePicker
{
    public class _jQueryUIDatePickerOptions : JSONObject
    {
        ///
        ///      
        ///<summary>Defaults: false 
        ///    Disables (true) or enables (false) the datepicker. Can be set when initialising (first creating) the datepicker.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the disabled option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ disabled: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the disabled option, after init.
        ///
        ///
        /////getter
        ///var disabled = $( ".selector" ).datepicker( "option", "disabled" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "disabled", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? disabled;
        ///
        ///
        ///<summary>Defaults: '' 
        ///    The jQuery selector for another field that is to be updated with the selected date from the datepicker. Use the altFormat setting to change the format of the date within this field. Leave as blank for no alternate field.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the altField option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ altField: '#actualDate' });
        ///
        ///
        ///    
        ///
        ///  Get or set the altField option, after init.
        ///
        ///
        /////getter
        ///var altField = $( ".selector" ).datepicker( "option", "altField" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "altField", '#actualDate' );
        ///
        ///
        ///    
        ///   
        ///</summary>
        public object altField;


        /// <summary>
        ///Defaults: '' 
        ///    The dateFormat to be used for the altField option. This allows one date format to be shown to the user for selection purposes, while a different format is actually sent behind the scenes. For a full list of the possible formats see the formatDate function
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the altFormat option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ altFormat: 'yy-mm-dd' });
        ///
        ///
        ///    
        ///
        ///  Get or set the altFormat option, after init.
        ///
        ///
        /////getter
        ///var altFormat = $( ".selector" ).datepicker( "option", "altFormat" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "altFormat", 'yy-mm-dd' );
        ///
        ///
        ///    
        /// </summary>
        public string altFormat;
        ///
        ///
        ///<summary>Defaults: '' 
        ///    The text to display after each date field, e.g. to show the required format.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the appendText option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ appendText: '(yyyy-mm-dd)' });
        ///
        ///
        ///    
        ///
        ///  Get or set the appendText option, after init.
        ///
        ///
        /////getter
        ///var appendText = $( ".selector" ).datepicker( "option", "appendText" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "appendText", '(yyyy-mm-dd)' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string appendText;
        ///
        ///
        ///<summary>Defaults: false 
        ///    Set to true to automatically resize the input field to accomodate dates in the current dateFormat.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the autoSize option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ autoSize: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the autoSize option, after init.
        ///
        ///
        /////getter
        ///var autoSize = $( ".selector" ).datepicker( "option", "autoSize" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "autoSize", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? autoSize;
        ///
        ///
        ///<summary>Defaults: '' 
        ///    The URL for the popup button image. If set, buttonText becomes the <i>alt</i> value and is not directly displayed.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the buttonImage option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ buttonImage: '/images/datepicker.gif' });
        ///
        ///
        ///    
        ///
        ///  Get or set the buttonImage option, after init.
        ///
        ///
        /////getter
        ///var buttonImage = $( ".selector" ).datepicker( "option", "buttonImage" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "buttonImage", '/images/datepicker.gif' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string buttonImage = "/_common/static/images/calendar-icon-24x24.png";
        ///
        ///
        ///<summary>Defaults: false 
        ///    Set to true to place an image after the field to use as the trigger without it appearing on a button.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the buttonImageOnly option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ buttonImageOnly: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the buttonImageOnly option, after init.
        ///
        ///
        /////getter
        ///var buttonImageOnly = $( ".selector" ).datepicker( "option", "buttonImageOnly" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "buttonImageOnly", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? buttonImageOnly = true;
        ///
        ///
        ///<summary>Defaults: '...' 
        ///    The text to display on the trigger button. Use in conjunction with showOn equal to 'button' or 'both'.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the buttonText option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ buttonText: 'Choose' });
        ///
        ///
        ///    
        ///
        ///  Get or set the buttonText option, after init.
        ///
        ///
        /////getter
        ///var buttonText = $( ".selector" ).datepicker( "option", "buttonText" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "buttonText", 'Choose' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string buttonText;
        ///
        ///
        ///<summary>Defaults: $.datepicker.iso8601Week 
        ///    A function to calculate the week of the year for a given date. The default implementation uses the ISO 8601 definition: weeks start on a Monday; the first week of the year contains the first Thursday of the year.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the calculateWeek option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ calculateWeek: myWeekCalc });
        ///
        ///
        ///    
        ///
        ///  Get or set the calculateWeek option, after init.
        ///
        ///
        /////getter
        ///var calculateWeek = $( ".selector" ).datepicker( "option", "calculateWeek" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "calculateWeek", myWeekCalc );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public object calculateWeek;
        ///
        ///
        ///<summary>Defaults: false 
        ///    Allows you to change the month by selecting from a drop-down list. You can enable this feature by setting the attribute to true.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the changeMonth option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ changeMonth: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the changeMonth option, after init.
        ///
        ///
        /////getter
        ///var changeMonth = $( ".selector" ).datepicker( "option", "changeMonth" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "changeMonth", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? changeMonth;
        ///
        ///
        ///<summary>Defaults: false 
        ///    Allows you to change the year by selecting from a drop-down list. You can enable this feature by setting the attribute to true. Use the yearRange option to control which years are made available for selection.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the changeYear option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ changeYear: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the changeYear option, after init.
        ///
        ///
        /////getter
        ///var changeYear = $( ".selector" ).datepicker( "option", "changeYear" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "changeYear", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? changeYear;
        ///
        ///
        ///<summary>Defaults: 'Done' 
        ///    The text to display for the close link. This attribute is one of the regionalisation attributes. Use the showButtonPanel to display this button.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the closeText option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ closeText: 'X' });
        ///
        ///
        ///    
        ///
        ///  Get or set the closeText option, after init.
        ///
        ///
        /////getter
        ///var closeText = $( ".selector" ).datepicker( "option", "closeText" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "closeText", 'X' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string closeText;
        ///
        ///
        ///<summary>Defaults: true 
        ///    When true entry in the input field is constrained to those characters allowed by the current dateFormat.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the constrainInput option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ constrainInput: false });
        ///
        ///
        ///    
        ///
        ///  Get or set the constrainInput option, after init.
        ///
        ///
        /////getter
        ///var constrainInput = $( ".selector" ).datepicker( "option", "constrainInput" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "constrainInput", false );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? constrainInput;
        ///
        ///
        ///<summary>Defaults: 'Today' 
        ///    The text to display for the current day link. This attribute is one of the regionalisation attributes. Use the showButtonPanel to display this button.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the currentText option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ currentText: 'Now' });
        ///
        ///
        ///    
        ///
        ///  Get or set the currentText option, after init.
        ///
        ///
        /////getter
        ///var currentText = $( ".selector" ).datepicker( "option", "currentText" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "currentText", 'Now' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string currentText;
        ///
        ///
        ///<summary>Defaults: 'mm/dd/yy' 
        ///    The format for parsed and displayed dates. This attribute is one of the regionalisation attributes. For a full list of the possible formats see the formatDate function.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the dateFormat option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ dateFormat: 'yy-mm-dd' });
        ///
        ///
        ///    
        ///
        ///  Get or set the dateFormat option, after init.
        ///
        ///
        /////getter
        ///var dateFormat = $( ".selector" ).datepicker( "option", "dateFormat" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string dateFormat;
        ///
        ///
        ///<summary>Defaults: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'] 
        ///    The list of long day names, starting from Sunday, for use as requested via the dateFormat setting. They also appear as popup hints when hovering over the corresponding column headings. This attribute is one of the regionalisation attributes.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the dayNames option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'] });
        ///
        ///
        ///    
        ///
        ///  Get or set the dayNames option, after init.
        ///
        ///
        /////getter
        ///var dayNames = $( ".selector" ).datepicker( "option", "dayNames" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "dayNames", ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'] );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public List<string> dayNames;
        ///
        ///
        ///<summary>Defaults: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'] 
        ///    The list of minimised day names, starting from Sunday, for use as column headers within the datepicker. This attribute is one of the regionalisation attributes.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the dayNamesMin option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'] });
        ///
        ///
        ///    
        ///
        ///  Get or set the dayNamesMin option, after init.
        ///
        ///
        /////getter
        ///var dayNamesMin = $( ".selector" ).datepicker( "option", "dayNamesMin" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "dayNamesMin", ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'] );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public List<string> dayNamesMin;
        ///
        ///
        ///<summary>Defaults: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'] 
        ///    The list of abbreviated day names, starting from Sunday, for use as requested via the dateFormat setting. This attribute is one of the regionalisation attributes.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the dayNamesShort option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'] });
        ///
        ///
        ///    
        ///
        ///  Get or set the dayNamesShort option, after init.
        ///
        ///
        /////getter
        ///var dayNamesShort = $( ".selector" ).datepicker( "option", "dayNamesShort" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "dayNamesShort", ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'] );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public List<string> dayNamesShort;
        ///
        ///
        ///<summary>Defaults: null 
        ///    Set the date to highlight on first opening if the field is blank. Specify either an actual date via a Date object or as a string in the current dateFormat, or a number of days from today (e.g. +7) or a string of values and periods ('y' for years, 'm' for months, 'w' for weeks, 'd' for days, e.g. '+1m +7d'), or null for today.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the defaultDate option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ defaultDate: +7 });
        ///
        ///
        ///    
        ///
        ///  Get or set the defaultDate option, after init.
        ///
        ///
        /////getter
        ///var defaultDate = $( ".selector" ).datepicker( "option", "defaultDate" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "defaultDate", +7 );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public object defaultDate; ///Date, Number, String
        
        ///
        ///
        ///<summary>Defaults: 0 
        ///    Set the first day of the week: Sunday is 0, Monday is 1, ... This attribute is one of the regionalisation attributes.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the firstDay option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ firstDay: 1 });
        ///
        ///
        ///    
        ///
        ///  Get or set the firstDay option, after init.
        ///
        ///
        /////getter
        ///var firstDay = $( ".selector" ).datepicker( "option", "firstDay" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "firstDay", 1 );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public int? firstDay;
        ///
        ///
        ///<summary>Defaults: false 
        ///    When true the current day link moves to the currently selected date instead of today.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the gotoCurrent option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ gotoCurrent: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the gotoCurrent option, after init.
        ///
        ///
        /////getter
        ///var gotoCurrent = $( ".selector" ).datepicker( "option", "gotoCurrent" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "gotoCurrent", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? gotoCurrent;
        ///
        ///
        ///<summary>Defaults: false 
        ///    Normally the previous and next links are disabled when not applicable (see minDate/maxDate). You can hide them altogether by setting this attribute to true.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the hideIfNoPrevNext option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ hideIfNoPrevNext: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the hideIfNoPrevNext option, after init.
        ///
        ///
        /////getter
        ///var hideIfNoPrevNext = $( ".selector" ).datepicker( "option", "hideIfNoPrevNext" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "hideIfNoPrevNext", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? hideIfNoPrevNext;
        ///
        ///
        ///<summary>Defaults: false 
        ///    True if the current language is drawn from right to left. This attribute is one of the regionalisation attributes.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the isRTL option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ isRTL: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the isRTL option, after init.
        ///
        ///
        /////getter
        ///var isRTL = $( ".selector" ).datepicker( "option", "isRTL" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "isRTL", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? isRTL;

        ///<summary>Defaults: null 
        ///    Set a maximum selectable date via a Date object or as a string in the current dateFormat, or a number of days from today (e.g. +7) or a string of values and periods ('y' for years, 'm' for months, 'w' for weeks, 'd' for days, e.g. '+1m +1w'), or null for no limit.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the maxDate option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ maxDate: '+1m +1w' });
        ///
        ///
        ///    
        ///
        ///  Get or set the maxDate option, after init.
        ///
        ///
        /////getter
        ///var maxDate = $( ".selector" ).datepicker( "option", "maxDate" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "maxDate", '+1m +1w' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public object minDate;/*Date, Number, String*/

        ///
        ///
        ///<summary>Defaults: null 
        ///    Set a maximum selectable date via a Date object or as a string in the current dateFormat, or a number of days from today (e.g. +7) or a string of values and periods ('y' for years, 'm' for months, 'w' for weeks, 'd' for days, e.g. '+1m +1w'), or null for no limit.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the maxDate option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ maxDate: '+1m +1w' });
        ///
        ///
        ///    
        ///
        ///  Get or set the maxDate option, after init.
        ///
        ///
        /////getter
        ///var maxDate = $( ".selector" ).datepicker( "option", "maxDate" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "maxDate", '+1m +1w' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public object maxDate;/*Date, Number, String*/
       
        ///
        ///
        ///<summary>Defaults: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'] 
        ///    The list of full month names, for use as requested via the dateFormat setting. This attribute is one of the regionalisation attributes.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the monthNames option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ monthNames: ['Januar','Februar','Marts','April','Maj','Juni','Juli','August','September','Oktober','November','December'] });
        ///
        ///
        ///    
        ///
        ///  Get or set the monthNames option, after init.
        ///
        ///
        /////getter
        ///var monthNames = $( ".selector" ).datepicker( "option", "monthNames" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "monthNames", ['Januar','Februar','Marts','April','Maj','Juni','Juli','August','September','Oktober','November','December'] );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public List<string> monthNames;
        ///
        ///
        ///<summary>Defaults: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'] 
        ///    The list of abbreviated month names, as used in the month header on each datepicker and as requested via the dateFormat setting. This attribute is one of the regionalisation attributes.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the monthNamesShort option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ monthNamesShort: ['Jan','Feb','Mar','Apr','Maj','Jun','Jul','Aug','Sep','Okt','Nov','Dec'] });
        ///
        ///
        ///    
        ///
        ///  Get or set the monthNamesShort option, after init.
        ///
        ///
        /////getter
        ///var monthNamesShort = $( ".selector" ).datepicker( "option", "monthNamesShort" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "monthNamesShort", ['Jan','Feb','Mar','Apr','Maj','Jun','Jul','Aug','Sep','Okt','Nov','Dec'] );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public List<string> monthNamesShort;
        ///
        ///
        ///<summary>Defaults: false 
        ///    When true the formatDate function is applied to the prevText, nextText, and currentText values before display, allowing them to display the target month names for example.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the navigationAsDateFormat option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ navigationAsDateFormat: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the navigationAsDateFormat option, after init.
        ///
        ///
        /////getter
        ///var navigationAsDateFormat = $( ".selector" ).datepicker( "option", "navigationAsDateFormat" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "navigationAsDateFormat", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? navigationAsDateFormat;
        ///
        ///
        ///<summary>Defaults: 'Next' 
        ///    The text to display for the next month link. This attribute is one of the regionalisation attributes. With the standard ThemeRoller styling, this value is replaced by an icon.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the nextText option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ nextText: 'Later' });
        ///
        ///
        ///    
        ///
        ///  Get or set the nextText option, after init.
        ///
        ///
        /////getter
        ///var nextText = $( ".selector" ).datepicker( "option", "nextText" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "nextText", 'Later' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string nextText;
       
        ///
        ///
        ///<summary>Defaults: 'Prev' 
        ///    The text to display for the previous month link. This attribute is one of the regionalisation attributes. With the standard ThemeRoller styling, this value is replaced by an icon.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the prevText option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ prevText: 'Earlier' });
        ///
        ///
        ///    
        ///
        ///  Get or set the prevText option, after init.
        ///
        ///
        /////getter
        ///var prevText = $( ".selector" ).datepicker( "option", "prevText" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "prevText", 'Earlier' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string prevText;
        ///
        ///
        ///<summary>Defaults: false 
        ///    When true days in other months shown before or after the current month are selectable. This only applies if showOtherMonths is also true.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the selectOtherMonths option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ selectOtherMonths: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the selectOtherMonths option, after init.
        ///
        ///
        /////getter
        ///var selectOtherMonths = $( ".selector" ).datepicker( "option", "selectOtherMonths" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "selectOtherMonths", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? selectOtherMonths;
        ///
        ///
        ///<summary>Defaults: '+10' 
        ///    Set the cutoff year for determining the century for a date (used in conjunction with dateFormat 'y'). If a numeric value (0-99) is provided then this value is used directly. If a string value is provided then it is converted to a number and added to the current year. Once the cutoff year is calculated, any dates entered with a year value less than or equal to it are considered to be in the current century, while those greater than it are deemed to be in the previous century.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the shortYearCutoff option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ shortYearCutoff: 50 });
        ///
        ///
        ///    
        ///
        ///  Get or set the shortYearCutoff option, after init.
        ///
        ///
        /////getter
        ///var shortYearCutoff = $( ".selector" ).datepicker( "option", "shortYearCutoff" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "shortYearCutoff", 50 );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string shortYearCutoff; /*String, Number*/
        ///
        ///
        ///<summary>Defaults: 'show' 
        ///    Set the name of the animation used to show/hide the datepicker. Use 'show' (the default), 'slideDown', 'fadeIn', any of the show/hide jQuery UI effects, or '' for no animation.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the showAnim option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ showAnim: 'fold' });
        ///
        ///
        ///    
        ///
        ///  Get or set the showAnim option, after init.
        ///
        ///
        /////getter
        ///var showAnim = $( ".selector" ).datepicker( "option", "showAnim" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "showAnim", 'fold' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string showAnim;
        ///
        ///
        ///<summary>Defaults: false 
        ///    Whether to show the button panel.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the showButtonPanel option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ showButtonPanel: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the showButtonPanel option, after init.
        ///
        ///
        /////getter
        ///var showButtonPanel = $( ".selector" ).datepicker( "option", "showButtonPanel" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "showButtonPanel", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? showButtonPanel = true;
        ///
        ///
        ///<summary>Defaults: 0 
        ///    Specify where in a multi-month display the current month shows, starting from 0 at the top/left.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the showCurrentAtPos option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ showCurrentAtPos: 3 });
        ///
        ///
        ///    
        ///
        ///  Get or set the showCurrentAtPos option, after init.
        ///
        ///
        /////getter
        ///var showCurrentAtPos = $( ".selector" ).datepicker( "option", "showCurrentAtPos" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "showCurrentAtPos", 3 );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public int? showCurrentAtPos;
        ///
        ///
        ///<summary>Defaults: false 
        ///    Whether to show the month after the year in the header. This attribute is one of the regionalisation attributes.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the showMonthAfterYear option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ showMonthAfterYear: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the showMonthAfterYear option, after init.
        ///
        ///
        /////getter
        ///var showMonthAfterYear = $( ".selector" ).datepicker( "option", "showMonthAfterYear" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "showMonthAfterYear", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? showMonthAfterYear;
        ///
        ///
        ///<summary>Defaults: 'focus' 
        ///    Have the datepicker appear automatically when the field receives focus ('focus'), appear only when a button is clicked ('button'), or appear when either event takes place ('both').
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the showOn option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ showOn: 'both' });
        ///
        ///
        ///    
        ///
        ///  Get or set the showOn option, after init.
        ///
        ///
        /////getter
        ///var showOn = $( ".selector" ).datepicker( "option", "showOn" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "showOn", 'both' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string showOn = "both";
        ///
        ///
        ///<summary>Defaults: {} 
        ///    If using one of the jQuery UI effects for showAnim, you can provide additional settings for that animation via this option.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the showOptions option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ showOptions: {direction: 'up' });
        ///
        ///
        ///    
        ///
        ///  Get or set the showOptions option, after init.
        ///
        ///
        /////getter
        ///var showOptions = $( ".selector" ).datepicker( "option", "showOptions" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "showOptions", {direction: 'up' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public object showOptions;
        ///
        ///
        ///<summary>Defaults: false 
        ///    Display dates in other months (non-selectable) at the start or end of the current month. To make these days selectable use selectOtherMonths.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the showOtherMonths option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ showOtherMonths: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the showOtherMonths option, after init.
        ///
        ///
        /////getter
        ///var showOtherMonths = $( ".selector" ).datepicker( "option", "showOtherMonths" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "showOtherMonths", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? showOtherMonths;
        ///
        ///
        ///<summary>Defaults: false 
        ///    When true a column is added to show the week of the year. The calculateWeek option determines how the week of the year is calculated. You may also want to change the firstDay option.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the showWeek option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ showWeek: true });
        ///
        ///
        ///    
        ///
        ///  Get or set the showWeek option, after init.
        ///
        ///
        /////getter
        ///var showWeek = $( ".selector" ).datepicker( "option", "showWeek" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "showWeek", true );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public bool? showWeek;
        ///
        ///
        ///<summary>Defaults: 1 
        ///    Set how many months to move when clicking the Previous/Next links.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the stepMonths option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ stepMonths: 3 });
        ///
        ///
        ///    
        ///
        ///  Get or set the stepMonths option, after init.
        ///
        ///
        /////getter
        ///var stepMonths = $( ".selector" ).datepicker( "option", "stepMonths" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "stepMonths", 3 );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public int? stepMonths;
        ///
        ///
        ///<summary>Defaults: 'Wk' 
        ///    The text to display for the week of the year column heading. This attribute is one of the regionalisation attributes. Use showWeek to display this column.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the weekHeader option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ weekHeader: 'W' });
        ///
        ///
        ///    
        ///
        ///  Get or set the weekHeader option, after init.
        ///
        ///
        /////getter
        ///var weekHeader = $( ".selector" ).datepicker( "option", "weekHeader" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "weekHeader", 'W' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string weekHeader;
        ///
        ///
        ///<summary>Defaults: 'c-10:c+10' 
        ///    Control the range of years displayed in the year drop-down: either relative to today's year (-nn:+nn), relative to the currently selected year (c-nn:c+nn), absolute (nnnn:nnnn), or combinations of these formats (nnnn:-nn). Note that this option only affects what appears in the drop-down, to restrict which dates may be selected use the minDate and/or maxDate options.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the yearRange option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ yearRange: '2000:2010' });
        ///
        ///
        ///    
        ///
        ///  Get or set the yearRange option, after init.
        ///
        ///
        /////getter
        ///var yearRange = $( ".selector" ).datepicker( "option", "yearRange" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "yearRange", '2000:2010' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string yearRange;
        ///
        ///
        ///<summary>Defaults: '' 
        ///    Additional text to display after the year in the month headers. This attribute is one of the regionalisation attributes.
        ///   
        ///    Code examples
        ///    
        ///    
        ///
        ///  Initialize a datepicker with the yearSuffix option specified.
        ///
        ///
        ///$( ".selector" ).datepicker({ yearSuffix: 'CE' });
        ///
        ///
        ///    
        ///
        ///  Get or set the yearSuffix option, after init.
        ///
        ///
        /////getter
        ///var yearSuffix = $( ".selector" ).datepicker( "option", "yearSuffix" );
        /////setter
        ///$( ".selector" ).datepicker( "option", "yearSuffix", 'CE' );
        ///
        ///
        ///    
        ///   
        ///  </summary>
        public string yearSuffix;
        ///
        ///    



    }
}