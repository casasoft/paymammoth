﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.ClickableItem
{
    public class ClickableItemsParameters : JSONObject
    {
        public List<string> elemsOrSelectors = new List<string>();
        public List<string> urls = new List<string>();
        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.ClickableItem.ClickableItems";
        }
    }
}