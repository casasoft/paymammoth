﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.MultiSelect
{
    public class _jQueryMultiSelectParams : JSONObject
    {
        public bool? sortable;
        public bool? searchable;

        public bool? doubleClickable;
        public string animated;
        public string show;
        public string hide;
        public double? dividerLocation = 0.5d;
    }
}