﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls;
using System.Web.UI.HtmlControls;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Lightbox.PrettyPhoto
{
    /// <summary>
    /// Initialize pretty photo. 
    /// 
    /// “You can also add a title and a description to your picture: To have display a title, add the title into the ALT attribute or your thumbnail image. To have display a description, add the description into the TITLE attribute or your link.”
    /// 
    /// Read more about jQuery lightbox for images, videos, YouTube, iframes, ajax | Stéphane Caron – No Margin For Errors on:
    /// 
    /// http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/?utm_source=INK&utm_medium=copy&utm_campaign=share&
    /// </summary>
    public class PrettyPhotoGalleryController
    {
        public List<Control> Controls { get; set; }
        public string GalleryName { get; set; }

        public PrettyPhotoGalleryController(string galleryName)
        {
            this.Controls = new List<Control>();
            this.GalleryName = galleryName;
            CS.General_v3.Util.PageUtil.GetCurrentPage().PreRender += new EventHandler(PrettyPhotoGalleryController_PreRender);
        }
        private void initRels()
        {
            if (this.Controls != null)
            {
                string prettyPhotoRel = "prettyPhoto[" + this.GalleryName +"]";
                foreach (var control in this.Controls)
                {
                    if (control is MyAnchor)
                    {
                        ((MyAnchor)control).Rel = prettyPhotoRel;
                    } 
                    else if (control is MyImage)
                    {
                        ((MyImage)control).Rel = prettyPhotoRel;
                    }
                    else if (control is HtmlControl)
                    {
                        ((HtmlControl)control).Attributes["rel"] = prettyPhotoRel;
                    }
                    else
                    {
                        throw new NotSupportedException("Control of type " + control + " is not supported");
                    }
                }
            }
        }

        void PrettyPhotoGalleryController_PreRender(object sender, EventArgs e)
        {
            initRels();
        }






    }
}