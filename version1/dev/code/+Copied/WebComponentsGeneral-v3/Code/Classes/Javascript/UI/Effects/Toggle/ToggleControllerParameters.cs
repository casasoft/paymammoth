﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Effects.Toggle
{
    public enum TOGGLE_ANIMATION_TYPE
    {
        Fade = 0,
        SlideUpDown = 10,
        SlideLeftRight = 20
    }
    public enum TOGGLE_EVENT
    {
        Click = 0,
        Hover = 10
    }
    public class ToggleControllerParameters : JSONObject
    {
        //The toggle item
        public string elemToggleSelector;
        /// <summary>
        /// The content which is shown when the toggle button is in state 'on'
        /// </summary>
        public string elemSelectorContentOn;
        /// <summary>
        /// The content which is shown when the toggle button is in state 'off'
        /// </summary>
        public string elemSelectorContentOff;
        public int durationMS = 150;
        public TOGGLE_ANIMATION_TYPE animationType = TOGGLE_ANIMATION_TYPE.SlideUpDown;
        public TOGGLE_EVENT eventType = TOGGLE_EVENT.Click;
        public bool initialStateOff = true;/// <summary>
        /// Whether you would want to hide the toggle elements when state is set on.  I.e. you can't reverse
        /// </summary>
        public bool onStateOnHideToggleSelector = false;

        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.Effects.Toggle.ToggleController";
        }
        protected override bool customDoNotOutputJSFunctionality()
        {
           return  string.IsNullOrWhiteSpace(elemToggleSelector) || (string.IsNullOrWhiteSpace(elemSelectorContentOn) && string.IsNullOrWhiteSpace(elemSelectorContentOff));
        }
        
    }
}