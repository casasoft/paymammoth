﻿namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data
{
    using System;
    using Newtonsoft.Json;

    public class JsonDateISO8601Converter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            DateTime date = (DateTime)value;
            bool includeTime = (date.Hour != 0 && date.Minute != 0 && date.Second != 0);
            string sDate = CS.General_v3.Util.Date.DateToISO8601String(date, includeTime);
            writer.WriteValue(sDate);
            writer.Flush();
        }
    }
}