﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Products
{
    public class ProductVariationItemData : JSONObject
    {
        public string variationID;
        public int imageIndex;
    }
}