﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Calculation.ValueMultiplier
{
    public class TextfieldValueMultiplierParameters : JSONObject
    {
        public string selectorTxt;
        public string selectorElem;
        public string prefix = "&euro;";
        public double multiplierValue;
        public int decimalPoints = 2;
        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.Calculation.ValueMultiplier.TextfieldValueMultiplier";
        }
    }
}