﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Products
{
    public class ProductVariationControllerAnythingSliderParameters : ProductVariationControllerParameters
    {
        public string selectorAnythingSlider;

        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.Products.ProductVariation.ProductVariationControllerAnythingSlider";
        }

    }
}