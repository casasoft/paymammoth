﻿namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Uploadify
{
    using System.Collections.Specialized;
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

    public class UploadifySettings : JSONObject

    {
        /*settings.uploadifySettings.uploader = 
            settings.uploadifySettings.script = "/ajax/tmp/uploadTempImage.ashx";
            settings.uploadifySettings.auto = true;*/
        /// <summary>
        /// The relative path to the uploadify.swf file. For absolute paths prefix the path with either ‘/’ or ‘http’. Default = ‘uploadify.swf’
        /// </summary>
        public string uploader = "/_common/static/js/jQuery/plugins/uploadify/2.1.0/uploadify.swf";
        /// <summary>
        /// The relative path to the backend script that will be processing your uploaded files. For absolute paths prefix the path with either ‘/’ or ‘http’ 
        /// Default = ‘uploadify.php’
        /// 
        /// IMPORTANT: Best is to make a handler which will extend the base handler AjaxMediaGalleryUploadBaseHandler
        /// </summary>
        public string script = "/ajax/tmp/uploadTempImage.ashx";
        /// <summary>
        /// The relative path to the backend script that will check if the file selected already resides on the server.
        /// No Default. ‘check.php’ is provided with core files.
        /// </summary>
        public string checkScript = null;
        /// <summary>
        /// An object containing name/value pairs of additional information you would like sent to the upload script. {‘name’: ‘value’}
        /// </summary>
        public NameValueCollection scriptData = null;
        /// <summary>
        /// The name of your files array in the upload server script. Default = ‘Filedata’
        /// </summary>
        public string fileDataName = null;
        /// <summary>
        /// GET / POST
        /// </summary>
        public string method = null;
        /// <summary>
        /// The access mode for scripts in the flash file. If you are testing locally, set to ‘always’.
        /// Default = ’sameDomain’
        /// </summary>
        public string scriptAccess = null;
        /// <summary>
        /// The path to the folder you would like to save the files to. Do not end the path with a ‘/’.
        /// For absolute paths prefix the path with either ‘/’ or ‘http’. Note server security issues with trying to upload to remote destinations.
        /// </summary>
        public string folder = null;
        /// <summary>
        /// The ID of the element you want to use as your file queue. By default, one is created on the fly below the ‘Browse’ button.
        /// </summary>
        public string queueID = null;
        /// <summary>
        /// The limit of the number of items that can be in the queue at one time. Default = 999.
        /// </summary>
        public int? queueSizeLimit = null;
        /// <summary>
        /// Set to true if you want to allow multiple file uploads.
        /// </summary>
        public bool multi = true;
        /// <summary>
        /// Set to true if you would like the files to be uploaded when they are selected.
        /// </summary>
        public bool auto = true;
        /// <summary>
        /// The text that will appear in the file type drop down at the bottom of the browse dialog box.
        /// </summary>
        public string fileDesc = null;
        /// <summary>
        /// A list of file extensions you would like to allow for upload. Format like ‘*.ext1;*.ext2;*.ext3′.
        /// fileDesc is required when using this option.
        /// </summary>
        public string fileExt = "*.jpg;*.jpeg;*.gif;*.bmp;*.tif;*.tiff";
        /// <summary>
        /// A number representing the limit in bytes for each upload.
        /// </summary>
        public int? sizeLimit = null;
        /// <summary>
        /// A limit to the number of simultaneous uploads you would like to allow.
        /// Default: 1
        /// </summary>
        public int? simUploadLimit = null;
        /// <summary>
        /// The text you would like to appear on the default button. Default = ‘BROWSE’
        /// </summary>
        public string buttonText = null;
        public string buttonImg = null;
        public bool? hideButton = null;
        public bool? rollover = null;
        public int? width = null;
        public int? height = null;
        public string wmode = null;
        public string cancelImg = null;

 

       
    }
}
