﻿namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data
{
    using System;
    using Newtonsoft.Json;

    public class JsonStringLiteralConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            string literal = (string)value;
            writer.WriteRawValue(literal);
            writer.Flush();
        }
    }
}