﻿namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Tooltips.QTip
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public class jQueryQTip
    {

        private Control _control;
        public Control Control
        {
            get { return _control; }
            set
            {
                if (_control != null)
                {
                    _control.PreRender -= _control_PreRender;
                }
                _control = value;
                if (_control != null)
                {
                    _control.PreRender += new EventHandler(_control_PreRender);
                }
            }
        }
        private WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Tooltips.QTip._jQueryQTipOptions _options = new WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Tooltips.QTip._jQueryQTipOptions();
        public Control ControlToAddJavascriptTo { get; set; }
        public WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Tooltips.QTip._jQueryQTipOptions Options
        {
            get { return _options; }
            set { _options = value; }
        }
        public jQueryQTip()
        {

        }

        public jQueryQTip(Control control)
        {
            this.Control = control;
        }

        private string getSelectorSuffix()
        {
            if (_control is MyImage)
            {
                return " img";
            }
            else
            {
                return "";
            }
        }
        private void updateImageDefaultOptions()
        {
            if (_options.content == null)
            {
                _options.content = new WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Tooltips.QTip._jQueryQTipOptionsContent();

            }
            if (string.IsNullOrEmpty(_options.content.text))
            {
                _options.content.attr = "alt";
            }
        }
        private void updateDefaultOptionsAccordingToControl()
        {
            if (_options.position == null)
            {
                _options.position = new WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Tooltips.QTip._jQueryQTipOptionsPosition();
            }
            if (_options.position.adjust == null)
            {
                _options.position.adjust = new WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Tooltips.QTip._jQueryQTipOptionsAdjust();
                _options.position.adjust.x = 15;
                _options.position.adjust.y = 15;
                _options.position.target = "mouse";
            }

            if (_control is MyImage || _control is Image)
            {
                updateImageDefaultOptions();
            }
        }

        private void initJS()
        {
            updateDefaultOptionsAccordingToControl();

            string js = "jQuery('#" + _control.ClientID + getSelectorSuffix() + "').qtip(" + Options.GetJSON() + ");";
            if (ControlToAddJavascriptTo != null)
            {
                CS.WebComponentsGeneralV3.Code.Util.JSUtil.AddJSScriptToControl(js, ControlToAddJavascriptTo);
            }
            else
            {
                CS.WebComponentsGeneralV3.Code.Util.JSUtil.AddJSScriptToPage(js);
            }
        }

        void _control_PreRender(object sender, EventArgs e)
        {
            initJS();
        }

        public string Title
        {
            get
            {
                if (_options.content != null)
                {
                    if (_options.content.title != null)
                    {
                        return _options.content.title.text;
                    }
                }
                return null;
            }
            set
            {
                if (_options.content == null)
                {
                    _options.content = new WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Tooltips.QTip._jQueryQTipOptionsContent();
                }
                if (_options.content.title == null)
                {
                    _options.content.title = new WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Tooltips.QTip._jQueryQTipOptionsTitle();
                }
                _options.content.title.text = value;
            }
        }








    }
}
