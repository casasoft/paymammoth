﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Effects.Toggle
{
    
    public class ToggleControllersParameters : JSONObject
    {
        public List<ToggleControllerParameters> toggleControllers = new List<ToggleControllerParameters>();
        /// <summary>
        /// This will stay selected when no one of them is in focus
        /// </summary>
        public int defaultSelectedIndex = -1;
        public int showDefaultAfterMouseLeaveAllItemsMS = 500;

        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.Effects.Toggle.ToggleControllers";
        }
        protected override bool customDoNotOutputJSFunctionality()
        {
            return toggleControllers == null || toggleControllers.Count == 0;
        }
        
    }
}