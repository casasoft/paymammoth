﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Tabs
{
    public enum TAB_SHOW_METHOD
    {
        Click = 0,
        Hover =10
    }
    public class TabsUIParameters : JSONObject
    {
        public List<string> selectorTabs = new List<string>();
        public List<string> selectorElements = new List<string>();
        public TAB_SHOW_METHOD showMethod = TAB_SHOW_METHOD.Click;
        public int fadeDurationMS = 250;
        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.Tabs.TabsUI";
        }
    }
}