﻿using CS.WebComponentsGeneralV3.Code.Util;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Web.Script.Serialization;
    using Newtonsoft.Json;
    using CS.General_v3.Util;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;
    using System.Web.UI;



    public class JSONObject
    {
        [JsonIgnore]
        public int _OutputPriority { get; set; }
        [JsonIgnore]
        public bool _DoNotOutputJS { get; set; }
        [JsonIgnore]
        public Control _OutputInsideControl { get; set; }

        protected virtual bool customDoNotOutputJSFunctionality()
        {
            return false;
        }

        private bool _jsOutputted;
        public JSONObject()
        {
            var pg = PageUtil.GetCurrentPage();
            if (pg != null)
            {
                pg.PreRender += new EventHandler(pg_PreRender);
            }
        }

        void pg_PreRender(object sender, EventArgs e)
        {
            InitJS();
        }
        protected virtual void onPreGetJson()
        {

        }

        public string GetJSON()
        {
            onPreGetJson();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;



            string json = JSONUtil.Serialize(this, NullValueHandling.Ignore);

            return json;
        }
        protected virtual string getJSClassName()
        {
            //throw new NotImplementedException("Override this method");
            return null;
        }

        /// <summary>
        /// This will be called automatically on page pre render if not called before
        /// </summary>
        public void InitJS()
        {
            bool doNotOutputJS = _DoNotOutputJS || customDoNotOutputJSFunctionality();
            if (!doNotOutputJS)
            {
                string jsClassName = getJSClassName();
                if (!string.IsNullOrEmpty(jsClassName) && !_jsOutputted)
                {
                    string js = "new " + jsClassName + "(" + this.GetJSON() + ");";
                    if (_OutputInsideControl != null)
                    {
                        JSUtil.AddJSScriptToControl(js, _OutputInsideControl);
                    }
                    else
                    {
                        JSUtil.AddJSScriptToPage(js, _OutputPriority);
                    }
                    _jsOutputted = true;
                }
            }
        }

      
    }
}
