﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.SectionToggler
{
    public class SectionTogglerParametersJS : JSONObject
    {


        public bool hideButtonOnClick = true;
        /// <summary>
        /// If ID, prepend with #, if CSS prepend with '.'
        /// </summary>
        public string buttonSelector;
        /// <summary>
        /// If ID, prepend with #, if CSS prepend with '.'
        /// </summary>
        public string sectionSelector;
        public int animationDurationMs = 250;



        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.SectionToggler.SectionToggler";
        }
    }
}