﻿using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;
using Newtonsoft.Json;
using System.Web.UI;
namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Tooltips.QTip
{


    public class _jQueryQTipOptionsStyle : JSONObject
    {
        /// <summary>
        /// A space separated string containing all class names which should be added to the main qTip element. 
        /// There are several base styles included in the CSS file provided, including: 
        /// </summary>
        public string classes;
        /// <summary>
        /// This propery allows you to override all applied CSS width styles for the tooltip. Can be any valid width CSS value. Please note that this does not override max/min width styles! Change those in the CSS file provided. 
        /// </summary>
        public object width;

        /// <summary>
        /// Determines whether or not the ui-widget classes of the Themeroller UI styles are applied to your tooltip 
        /// </summary>
        public bool? widget = false;
    }

    public class _jQueryQTipOptionsShow : JSONObject
    {
        public _jQueryQTipOptionsShow()
        {
            //this._allPropertiesStartWithLowerCase = true;
        }

        /// <summary>
        /// Defines the HTML element(s) which will trigger your specified show.event(s). When set to false, the element the .qtip() method was called upon is used. 
        /// </summary>
        public object target;
        public bool? effect;
        /// <summary>
        /// Event(s) which will trigger the tooltip to be shown. All possible values are documented under jQuery's Event bind() documentation. Multiple, space separated events are supported. 
        /// </summary>
        [JsonProperty(PropertyName = "event")]
        public string Event;
        /// <summary>
        /// Time in milliseconds by which to delay showing of the tooltip when the show.event is triggered on the show.target
        /// </summary>
        public int? delay = 140;
        /// <summary>
        /// Determines whether or not the tooltip will hide all others when the show.event is triggered on the show.target. If a jQuery object is used as its value, only tooltips found within that object will be hidden. 
        /// </summary>
        public bool? solo;
        /// <summary>
        /// Determines whether or not the tooltip is shown once the document is loaded e.g. when the document.ready() event is triggered. 
        /// </summary>
        public bool? ready;
        /// <summary>
        /// Determines the type of effect that takes place when showing the tooltip. A custom method can also be used whose scope is the tooltip element when called. If set to false, no animation takes place. 
        /// </summary>
    }

    public class _jQueryQTipOptionsHide : JSONObject
    {
        public _jQueryQTipOptionsHide()
        {
            //this._allPropertiesStartWithLowerCase = true;
        }
        /// <summary>
        /// Defines the HTML element(s) which will trigger your specified show.event(s). When set to false, the element the .qtip() method was called upon is used. 
        /// </summary>
        public object target;

        public bool? effect;
        /// <summary>
        /// Event(s) which will trigger the tooltip to be shown. All possible values are documented under jQuery's Event bind() documentation. Multiple, space separated events are supported. 
        /// </summary>
        [JsonProperty(PropertyName = "event")]
        public string Event;
        /// <summary>
        /// Time in milliseconds by which to delay showing of the tooltip when the show.event is triggered on the show.target
        /// </summary>
        public int? delay = 0;
        /// <summary>
        /// Time in milliseconds in which the tooltip should be hidden if it remains inactive e.g. isn't interacted with. If set to false, tooltip will not hide when inactive. 
        /// </summary>
        public int? inactive;

        /// <summary>
        /// When set to true, the tooltip will not hide if moused over, allowing the contents to be clicked and interacted with. 
        /// </summary>
        [JsonProperty(PropertyName = "fixed")]
        public bool? Fixed;
        /// <summary>
        /// Additional hide setting that allows you to specify whether the tooltip will hide when leaving the window it's contained within. This option requires you to be using either mouseout or mouseleave as (one of) your hide events. 
        /// </summary>
        public string leave;
        /// <summary>
        /// This setting allows you to determine the distance after which the tooltip hides when the mouse is moved from the point it triggered the tooltip. This is what the regular browser tooltips behave like. 
        /// </summary>
        public int? distance;
        /// <summary>
        /// Determines the type of effect that takes place when showing the tooltip. A custom method can also be used whose scope is the tooltip element when called. If set to false, no animation takes place. 
        /// </summary>
    }

    public class _jQueryQTipOptionsAdjust : JSONObject
    {
        /// <summary>
        /// A positive or negative pixel value by which to offset the tooltip in the horizontal plane e.g. the x-axis. Negative values cause a reduction in the value e.g. moves tooltip to the left. 
        /// </summary>
        public int? x;
        /// <summary>
        /// A positive or negative pixel value by which to offset the tooltip in the vertical plane e.g. the y-axis. Negative values cause a reduction in the value e.g. moves tooltip upwards. 
        /// </summary>
        public int? y;
        /// <summary>
        /// This option determines the kind of viewport positioning that takes place.
        /// The default "flip" type basically flips the tooltip when it goes off-screen i.e. from top-right, to bottom-right etc. The "shift" type attempts to keep the tooltip on screen by adjusting only by the amount needed to keep it within the viewport boundaries.
        /// You can specify the behaviour of each axis (i.e. horizontal and vertical) separately, for example a value of "flip none" will cause the tooltip to flip accross the horizontal axis when it extends out the viewport, but do nothing when it extends out the viewport vertically. There are a number of combinations. 
        /// </summary>
        public string method;
        /// <summary>
        /// When the position.target is set to mouse, this option determines whether the tooltip follows the mouse when hovering over the show.target. 
        /// </summary>
        public bool? mouse;
        /// <summary>
        /// Determines if the tooltips position is adjusted when the window is resized. 
        /// </summary>
        public bool? resize;
    }

    public class _jQueryQTipOptionsContent : JSONObject
    {
        private string _text;

        /// <summary>
        /// Text/HTML which will appear inside the tooltip initially. If set to true the title attribute of the target will be used, if available. Can also specify an anonymous function that returns the content, and whose scope is the target element.
        /// </summary>
        public string text
        {
            get
            {
                if (textControl != null)
                {
                    return CS.General_v3.Util.ControlUtil.RenderControl(textControl);
                }
                else
                {
                    return _text;
                }
            }
            set { _text = value; }
        }

        /// <summary>
        /// Attribute of the target element to use for content if none is provided with the above content.text option, or no valid content can be found.
        /// </summary>
        public string attr;
        public _jQueryQTipOptionsTitle title;

        [JsonIgnore]
        public Control textControl { get; set; }
    }

    public class _jQueryQTipOptionsPosition : JSONObject
    {
        /// <summary>
        /// The corner of the tooltip to position in relation to the position.at. See the Basics section for all possible corner values. 
        /// </summary>
        public string my;
        /// <summary>
        /// The corner of the position.target element to position the tooltips corner at. See the Basics section for all possible corner values. 
        /// </summary>
        public string at;
        /// <summary>
        /// HTML element the tooltip will be positioned in relation to. Can also be set to 'mouse' or the 'event' (position at target that triggered the tooltip), or an array containg an absolute x/y position on the page.
        /// If you also have position.adjust.mouse set to true, the qTip will follow the mouse until a hide event is triggered on the hide target
        /// 
        /// Can be:
        /// - jQuery Element
        /// - 'mouse'
        /// - Array of [x,y]
        /// - 'false' will show in relation to selector
        /// </summary>
        public object target = null;

        /// <summary>
        /// Determines the HTML element which the tooltip is appended to e.g. it's containing element. 
        /// </summary>
        public object container;
        /// <summary>
        /// Determines the viewport used to keep the tooltip visible i.e. the element whose boundaries the tooltip must stay visible within at all times if possible. If true it's value will be inherited from the position.container property. 
        /// </summary>
        public object viewport;

        public _jQueryQTipOptionsAdjust adjust;

    }
    public class _jQueryQTipOptionsTitle : JSONObject
    {
        /// <summary>
        /// Text/HTML which will appear inside the title element of the content. If set to false, no title will be created. An anonymous function can also be used to return the title text, whose scope is the target element.
        /// </summary>
        public string text;

        /// <summary>
        /// Text/HTML which will appear inside the title's button element (e.g. close link) located to the right of the title content. The button will close the tooltip when clicked.
        /// </summary>
        public string button = null;
    }
    public class _jQueryQTipOptions : JSONObject
    {
        /// <summary>
        /// A unique string that determines the value of the qTip's "id" attribute that can be used to easily identify this qTip in the document.The attribute is prepended with 'ui-tooltip-'.
        /// </summary>
        public string id;

        /// <summary>
        /// By default, tooltips are rendered on thier first show event, rather than on page load. Setting this to true will cause tooltips to be created on page load.
        /// </summary>
        public bool? prerender;

        public bool? overwrite;

        public _jQueryQTipOptionsContent content;// = new _jQueryQTipOptionsContent();
        public _jQueryQTipOptionsPosition position;// = new _jQueryQTipOptionsPosition();
        public _jQueryQTipOptionsShow show;//= new _jQueryQTipOptionsShow();
        public _jQueryQTipOptionsHide hide;// = new _jQueryQTipOptionsHide();
        public _jQueryQTipOptionsStyle style;
    }
}
