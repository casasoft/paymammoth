﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;
using Newtonsoft.Json;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Carousel.CarouFredSel.v1
{
    using System.Web.UI.WebControls;
    using Controls.WebControls.Common.Fields.Validators;
    using Util;
    using CS.General_v3.Util;

    public class CarouFredSelV1PaginationParameters : CarouFredSelV1ScrollParameters
    {
        /// <summary>
        /// A jQuery-selector for the HTML element that should contain the pagination-links.
        /// </summary>
        public string container;
        public bool? keys;
        /// <summary>
        /// Add a function literal, write Function code
        /// </summary>
        [JsonConverter(typeof(JsonStringLiteralConverter))]
        public string anchorBuilder;
        
    }
    public class CarouFredSelV1PrevNextParameters : CarouFredSelV1ScrollParameters
    {
        /// <summary>
        /// A jQuery-selector for the HTML element that should scroll the carousel.
        /// </summary>
        public string button;
        /// <summary>
        /// Number	The keyCode of the keyboard-key that should scroll the carousel.
        /// String	Can be: "up", "down", "left" or "right".
        /// </summary>
        public object key;
        
    }
    public class CarouFredSelV1AutoParameters : CarouFredSelV1ScrollParameters
    {
        /// <summary>
        /// Boolean	Determines whether the carousel should scroll automatically or not.
        /// </summary>
        public bool? play;
        /// <summary>
        /// String	A jQuery-selector for the HTML element that should toggle the carousel between playing and paused.
        /// jQuery-object	A jQuery-object of the HTML element that should toggle the carousel between playing and paused.
        /// Function	A function that returns any of the other valid values.
        /// </summary>
        public string button;
        /// <summary>
        /// Number	The amount of milliseconds the carousel will pause.
        /// If auto.duration is less then 10 -to use a speed (in pixels/milliseconds) instead of a duration-, the default value is 2500.
        /// </summary>
        public int? pauseDuration;
        /// <summary>
        /// Additional delay in milliseconds before the carousel starts scrolling the first time.
        /// Hint: This can also be a negative number.
        /// </summary>
        public int? delay;
        /// <summary>
        /// Determines whether the timeout between transitions should be paused when the event on the button is triggered.
        /// See the description for scroll.pauseOnHover.
        /// </summary>
        public object pauseOnEvent;
        /// <summary>
        /// Determines whether the timeout between transitions should be paused when resizing the window.
        /// Note: Only applies on responsive carousels.
        /// </summary>
        public object pauseOnResize;

        /// <summary>
        /// Function that will be called when starting the pausing-timer.
        /// This function receives 2 parameters:
        /// percentage: The percentage the auto.pauseDuration is at.
        /// duration: The remaining time left in milliseconds.
        /// </summary>
        [JsonConverter(typeof(JsonStringLiteralConverter))]
        public string onPauseStart;
        /// <summary>
        /// Function that will be called when ending the pausing-timer.
        /// Functionality is the same as for the onPauseStart-function.
        /// </summary>
        [JsonConverter(typeof(JsonStringLiteralConverter))]
        public string onPauseEnd;
        /// <summary>
        /// Function that will be called when pausing the pausing-timer.
        /// Functionality is the same as for the onPauseStart-function.
        /// </summary>
        [JsonConverter(typeof(JsonStringLiteralConverter))]
        public string onPausePause;
    }
    public class CarouFredSelV1ScrollParameters : JSONObject
    {
        /// <summary>
        /// Number	The number of items to scroll.
        /// If null, the number of visible items is used.
        /// String	Enter "page" to scroll to the first item of the previous/next "page".
        /// String	A string consisting of three sections:
        /// base:"odd", "even" or ""(an empty string, default).
        /// adjustment:"+" or "-".
        /// amount:any number (1 by default)
        /// For example: "odd+2" will decrease the number of visible items to an odd number and increase it by two.
        /// </summary>
        public object items = null;
        /// <summary>
        /// Indicates which effect to use for the transition.
        /// Possible values: "none", "scroll", "directscroll", "fade", "crossfade", "cover" or "uncover".
        /// Note: these effects are considered to be built in workarounds, therefore it is not guaranteed that they will work with all other options.
        /// </summary>
        public string fx;
        /// <summary>
        /// Indicates which easing function to use for the transition. jQuery defaults: "linear" and "swing", built in: "quadratic", "cubic" and "elastic".
        /// </summary>
        public string easing;
        /// <summary>
        /// Determines the duration of the transition in milliseconds.
        /// If less than 10, the number is interpreted as a speed (pixels/millisecond).
        /// This is probably desirable when scrolling items with variable sizes.
        /// </summary>
        public int? duration;
        /// <summary>
        /// 	Boolean	Determines whether the timeout between transitions should be paused "onMouseOver" (only applies when the carousel scrolls automatically).
        /// 	String	Enter "resume" to let the timeout resume instead of restart "onMouseOut".
        /// 	String	Enter "immediate" to immediately stop "onMouseOver" and resume "onMouseOut" a scrolling carousel.
        /// 	String	Enter "immediate-resume" for both the options above.
        /// </summary>
        public object pauseOnHover;
        /// <summary>
        /// Determines whether the scrolling should be queued if the carousel is currently being animated.
        /// </summary>
        public bool? queue;
        /// <summary>
        /// Boolean	Determines whether the carousel can be scrolled via the mousewheel. To enable this feature, you'll need to include the jquery.mousewheel plugin.
        /// Number	Enter a numeric value to specify the amount of items to scroll.
        /// </summary>
        public object mousewheel;
        /// <summary>
        /// Determines whether the carousel can be scrolled via wipe-gestures (for smartphones). To enable this feature, you'll need to include the jquery.touchwipe plugin.
        /// </summary>
        public bool? wipe;
    }
    public class CarouFredSelV1ItemParameters : JSONObject
    {
        /// <summary>
        /// Number	The number of visible items.
        /// If null, the number will be calculated (and set to "variable" if necessary).
        /// String	Enter "variable" to measure the number of visible items
        /// (based on the available size).
        /// Object	A map for min and max.
        /// String	A string consisting of three sections:
        /// base:"odd", "even" or ""(an empty string, default).
        /// adjustment:"+" or "-".
        /// amount:any number (1 by default)
        /// For example: "odd+2" will measure the number of items needed to fill the available size, decrease it to an odd number and increase it by two.
        /// Function	A function that returns the number of visible items.
        /// This function receives 1 parameter:
        /// visibleItems: the number of items that would fit the available size.
        /// </summary>
        public object visible;
        /// <summary>
        /// The minimum number of items needed to create a carousel.
        /// If null, the number for items.visible is inherited and increased by 1.
        /// </summary>
        public int? minimum;
        /// <summary>
        /// Number	The nth item to start the carousel.
        /// Hint: This can also be a negative number.
        /// String	Enter "random" to let the plugin pick a randon item to start the carousel.
        /// String	A jQuery-selector of the item to start the carousel.
        /// For example: "#foo li:first".
        /// jQuery-object	A jQuery-object of the item to start the carousel.
        /// For example: $("#foo li:first").
        /// Boolean	If true, the plugin will search for an item-anchor to start the carousel using the url-hashtag.
        /// For example: http://domain.com#startitem
        /// </summary>
        public object start;
        /// <summary>
        /// Number	The width of the items.
        /// If null, the width will be measured (and set to "variable" if necessary).
        /// String	Enter "variable" to create a carousel that supports variable item-widths.
        /// String	Enter a percentage to automatically resize the width of the items onWindowResize.
        /// Note: Only applies on responsive, vertical carousels.
        /// For example: "50%".
        /// </summary>
        public int? width;
        /// <summary>
        /// Number	The height of the items.
        /// If null, the height will be measured (and set to "variable" if necessary).
        /// String	Enter "variable" to create a carousel that supports variable item-heights.
        /// String	Enter a percentage to automatically resize the height of the items onWindowResize.
        /// Note: Only applies on responsive, horizontal carousels.
        /// For example: "50%".
        /// </summary>
        public int? height;
        /// <summary>
        /// String	The selector elements should match to be considered an item.
        /// If null, all elements inside the carousel will be considered to be an item.
        /// If the carousel contains :hidden-elements, it is set to ":visible".
        /// Note: this option is not (yet) compatible with non-circular carousels.
        /// }
        /// Number	A number for items.visible.
        /// String	Enter "variable" for items.width, items.height and items.visible.
        /// </summary>
        public string filter;
    }
    public class CarouFredSelV1Parameters : JSONObject
    {
        /// <summary>
        /// Determines whether the carousel should be circular.
        /// </summary>
        public bool? circular;
        /// <summary>
        /// Determines whether the carousel should be infinite.
        /// Note: It is possible to create a non-circular, infinite carousel, but it is not possible to create a circular, non-infinite carousel.
        /// </summary>
        public bool? infinite;
        /// <summary>
        /// Determines whether the carousel should be responsive.
        /// If true, the items will be resized to fill the carousel.
        /// </summary>
        public bool? responsive;
        /// <summary>
        /// The direction to scroll the carousel, determines whether the carousel scrolls horizontal or vertical and -when the carousel scrolls automatically- in what direction.
        /// Possible values: "right", "left", "up" or "down".
        /// </summary>
        public string direction;
        /// <summary>
        /// Number	The width of the carousel.
        /// If null, the width will be calculated (and set to "variable" if necessary, depending on the item-widths).
        /// String	Enter "variable" to automatically resize the carousel when scrolling items with variable widths.
        /// String	Enter "auto" to measure the widest item.
        /// String	Enter a percentage to automatically resize (and re-configurate) the carousel onWindowResize.
        /// Note: Only applies on horizontal carousels.
        /// For example: "100%".
        /// </summary>
        public object width;
        /// <summary>
        /// Number	The height of the carousel.
        /// If null, the height will be calculated (and set to "variable" if necessary, depending on the item-heights).
        /// String	Enter "variable" to automatically resize the carousel when scrolling items with variable heights.
        /// String	Enter "auto" to measure the highest item.
        /// String	Enter a percentage to automatically resize (and re-configurate) the carousel onWindowResize.
        /// Note: Only applies on vertical carousels.
        ////For example: "100%".
        /// </summary>
        public object height;
        /// <summary>
        /// Whether and how to align the items inside a fixed width/height.
        /// Possible values: "center", "left", "right" or false.
        /// </summary>
        public string align;
        /// <summary>
        /// Number	Padding around the carousel (top, right, bottom and left).
        /// Array	Padding in an Array.
        /// For example: [10, 20, 30, 40] (top, right, bottom, left)
        /// or [0, 50] (top/bottom, left/right).
        /// </summary>
        public object padding;
        /// <summary>
        /// String	Selector for the carousel to synchronise.
        /// Array	Selector and options for the carousel to synchronise:
        /// [string selector, boolean inheritOptions, boolean sameDirection, number deviation] For example: ["#foo2", true, true, 0]
        /// Array	A collection of arrays.
        /// </summary>
        public object synchronise;
        /// <summary>
        /// Boolean	Determines whether the carousel should start at its last viewed position.
        /// The cookie is stored until the browser is closed.
        /// String	A specific name for the cookie to prevent multiple carousels from using the same cookie.
        /// </summary>
        public object cookie;
        public CarouFredSelV1ItemParameters items = null;
        public CarouFredSelV1ScrollParameters scroll = null;
        public CarouFredSelV1AutoParameters auto = null;
        public CarouFredSelV1PrevNextParameters prev = null;
        public CarouFredSelV1PrevNextParameters next = null;
        public CarouFredSelV1PaginationParameters pagination = null;

    }

    public class CarouFredSelV1
    {

        private IAttributeAccessor _item;
        private string _controlExtraCssClass;
        public CarouFredSelV1Parameters Parameters { get; set; }
        public CarouFredSelV1(IAttributeAccessor item, string controlExtraCssClass, CarouFredSelV1Parameters parameters)
        {
            _item = item;
            _controlExtraCssClass = controlExtraCssClass;
            this.Parameters = parameters;
            AddJS();
            
        }
        public CarouFredSelV1(IAttributeAccessor item, string controlExtraCssClass, int itemsVisible, int itemsToScroll, string direction = "up", string align = "top", int durationScrollMs = 250, bool pauseOnHover = true, bool automaticTransition = true, int automaticPauseDurationMs = 1000, int automaticInitialExtraPauseDurationMS = 1000)

        {
            _item = item;
            _controlExtraCssClass = controlExtraCssClass;
            this.Parameters = new CarouFredSelV1Parameters();
            this.Parameters.direction = direction;
            this.Parameters.align = align;
            this.Parameters.items = new CarouFredSelV1ItemParameters()
            {
                visible = itemsVisible
            };
            this.Parameters.scroll = new CarouFredSelV1ScrollParameters()
            {
                items = itemsToScroll,
                duration = durationScrollMs,
                pauseOnHover = pauseOnHover
            };
            if (automaticTransition)
            {
                this.Parameters.auto = new CarouFredSelV1AutoParameters()
                                       {
                                           pauseDuration = automaticPauseDurationMs,
                                           delay = automaticInitialExtraPauseDurationMS
                                       };
            }
        }
        

        public void AddJS()
        {
            _item.SetAttribute("class", _item.GetAttribute("class") + " " + _controlExtraCssClass);

            string js = "jQuery('." + _controlExtraCssClass + "').carouFredSel(" + Parameters.GetJSON() + ");";
            CS.WebComponentsGeneralV3.Code.Util.JSUtil.AddJSScriptToPage(js);
        }

        public static void RegisterJS()
        {
            string jsFile = "/_common/static/js/jQuery/plugins/carousel/carouFredSel/5.5.5/jquery.carouFredSel-5.5.5-packed.js";
            PageUtil.RegisterJavaScriptInHeadOfPage(jsFile);
        }

    }
}