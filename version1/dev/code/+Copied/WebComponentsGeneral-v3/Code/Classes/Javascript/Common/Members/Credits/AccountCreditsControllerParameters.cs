﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.Common.Members.Credits
{
    public class AccountCreditsControllerParameters : JSONObject
    {
        public string selectorElement;
        public string ajaxHandler = "/_ComponentsGeneric/Frontend/Handlers/AJAX/Credits/getCreditsAvailable.ashx";

        protected override string getJSClassName()
        {
            return "js.com.cs.v3.Common.Members.Credits.AccountCreditsController";
        }
    }
}