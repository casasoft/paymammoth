﻿using CS.WebComponentsGeneralV3.Code.Util;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Web.Script.Serialization;
    using Newtonsoft.Json;
    using CS.General_v3.Util;


    /// <summary>
    /// Add dynamic properties
    /// </summary>
    public class JSONObjectDictionary
    {
        private readonly Dictionary<string, object> _properties = new Dictionary<string, object>();

        public JSONObjectDictionary()
        {
        }

        public string GetJSON(NullValueHandling nullvalueHandlig = NullValueHandling.Ignore, Formatting formatting = Formatting.None)
        {
            string json = JSONUtil.Serialize(_properties, nullvalueHandlig, formatting);

            return json;
        }
        
       public object this[string index]
        {
            get
            {
                return _properties[index];
            }
            set { _properties[index] = value; }
        }
        public void AddProperty(string key, object value)
        {
            _properties[key] = value;
        }
        public object GetProperty(string key)
        {
            return _properties[key];
        }
        public T GetProperty<T>(string key)
        {
            return (T)_properties[key];
        }
        public Dictionary<string,object> GetAllProperties()
        {
            return _properties;
        }
    }
}
