﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Dialog
{
    public class jQueryDialogIFrameControllerParameters : JSONObject
    {
        public string title;
        public int? width;
        public int? height;

        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.JQuery.jQueryUI.Dialog.jQueryDialogIFrameController";
        }
    }
}