﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.Common
{
    using GenericStatusMessage;
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;
    using BusinessLogic_v3.Modules.CmsUserModule;

    public class CommonControllersParameters : JSONObject
    {
        public GenericStatusMessageControllerParameters genericStatusMessageParams = new GenericStatusMessageControllerParameters();
        private bool? _isLoggedInCms;
        public bool isLoggedInCms
        {
            get {
                if (_isLoggedInCms.HasValue)
                {
                    return _isLoggedInCms.Value;
                }
                else
                {
                    return CmsUserSessionLogin.Instance.IsLoggedIn;
                }

            }
            set {
                _isLoggedInCms = value;
            }
        }


            protected override string getJSClassName()
        {
            return "js.com.cs.v3.Common.CommonControllers";
        }
    }
}