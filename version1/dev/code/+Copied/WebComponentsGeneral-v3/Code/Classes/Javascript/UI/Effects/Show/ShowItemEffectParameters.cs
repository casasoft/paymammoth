﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Effects.Show
{
    public enum SHOW_TYPE
    {
        Fade = 0,
        SlideUpDown = 10
    }
    
    public class ShowItemEffectParameters : JSONObject
    {
        public string elemSelector;
        public string elemToShowSelector;
        public int fadeDurationMS = 250;
        public SHOW_TYPE showType = SHOW_TYPE.Fade;

        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.Effects.Show.ShowItemEffect";
        }
    }
}