﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Products
{
    public abstract class ProductVariationControllerParameters : JSONObject
    {
        public string selectorCmb;

        public List<ProductVariationItemData> variationsData = new List<ProductVariationItemData>();


        protected override bool customDoNotOutputJSFunctionality()
        {
            return string.IsNullOrEmpty(selectorCmb) || variationsData == null || variationsData.Count == 0;
        }
        
    }
}