﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Products
{
    public class ProductVariationControllerCarouFredSelParameters : ProductVariationControllerParameters
    {
        public string selectorCarouFredSel;

        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.Products.ProductVariation.ProductVariationControllerCarouFredSel";
        }

    }
}