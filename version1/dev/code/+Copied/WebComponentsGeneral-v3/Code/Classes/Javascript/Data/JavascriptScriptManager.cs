﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data
{
    using Util;

    public class JavascriptScriptManager
    {
        private bool _rendered = false;
        private List<JavascriptScriptManagerItem> _jsItems = new List<JavascriptScriptManagerItem>();
        private JavascriptScriptManager()
        {
            CS.General_v3.Util.PageUtil.GetCurrentPage().PreRenderComplete += new EventHandler(JavascriptScriptManager_PreRender);
        }

        void JavascriptScriptManager_PreRender(object sender, EventArgs e)
        {
            Page pg = (Page)sender;
            OutputScripts(pg);
        }


        private int sortScripts(JavascriptScriptManagerItem a, JavascriptScriptManagerItem b)
        {
            return CS.General_v3.Util.SortingUtil.IntegerComparer(a.Priority, b.Priority);
        }

        public void OutputScripts(Page pg)
        {
            _rendered = true;
            //Sort them in required order
            _jsItems.Sort(sortScripts);

            StringBuilder js = new StringBuilder();
            foreach(var item in _jsItems)
            {
                js.Append(item.GetJavascript());
                js.Append("\r\n");
            }
            //output them


            Page page = CS.General_v3.Util.PageUtil.GetCurrentPage();
            Type type = page.GetType();
            string key = CS.General_v3.Util.Random.GetGUID();
            //page.Controls[page.Controls.Count-1].Controls.Add(new System.Web.UI.WebControls.Literal() { Text = "MARK TEST" });
            page.ClientScript.RegisterStartupScript(type, key, js.ToString(), true);

            //JSUtil.AddJSScriptToPage(js.ToString(), true, false);
            
        }

        public void AddScript(string js, int priority = 0, bool deferExecutionThroughJquery = true)
        {
            this.AddScript(new JavascriptScriptManagerItem()
            {
                JavascriptCode = js,
                Priority = priority,
                DeferExecutionThroughJQueryOnLoad = deferExecutionThroughJquery
            });
        }

        public void AddScript(JavascriptScriptManagerItem scriptItem)
        {
            if (_rendered)
            {
                throw new Exception("Javascript has already been rendered.  Please check life cycle when you are adding script.");
            }
            _jsItems.Add(scriptItem);
        }

        private const string JS_SCRIPT_MANAGER_CONTEXT_KEY = "JS_Script_Manager";
        public static JavascriptScriptManager Instance
        {
            get
            {
                JavascriptScriptManager manager = CS.General_v3.Util.PageUtil.GetContextObject<JavascriptScriptManager>(JS_SCRIPT_MANAGER_CONTEXT_KEY);
                if (manager == null)
                {
                    manager = new JavascriptScriptManager();
                    CS.General_v3.Util.PageUtil.SetContextObject(JS_SCRIPT_MANAGER_CONTEXT_KEY, manager);
                }
                return manager;
            }
        }
    }

}