﻿namespace CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.NavigationMenu.SliderNavigation
{
    using System;
    using CS.General_v3.JavaScript.Data;
    using CS.WebComponentsGeneralV3.Code.Util;
    using General_v3.JavaScript.Data;

    public class SliderNavigationJSDetails : JSONObject
    {
        public int slideDurationMS = 500;
        public string cssClassIconExpand = "navigation-expand";
        public string cssClassIconCollapse = "navigation-collapse";
        public string cssClassLiSectionExpanded = "navigation-section-expanded";
        public string cssClassLiSectionCollapsed = "navigation-section-collapsed";
        public string cssClassLinkExpanded = "navigation-link-expanded";
        public string cssClassLinkCollapsed = "navigation-link-collapsed";
        public bool clickOnNonLeafNodesOpenSection = false;
        public string initialSelectedClass = "selected";
        public bool autoOpenSelectedHierarchy = true;
        public bool doNotAllowMultipleSiblingsOpen = true;

        public string rootULElementOrID;

        public static void Init(SliderNavigationJSDetails parameters)
        {
            if (string.IsNullOrEmpty(parameters.rootULElementOrID))
            {
                throw new Exception("Please specify root UL client ID");
            }
            string js = "new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.NavigationMenu.SliderNavigation.SliderNavigation(" + parameters.GetJSON() + ");";
            JSUtil.AddJSScriptToPage(js);
        }

    }
}
