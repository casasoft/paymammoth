﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Classes.GoogleAnalytics
{
    public class GoogleAnalyticsInfo
    {
        public string GoogleAnalyticsID { get; set; }
        public string GoogleAnalyticsRootDomain { get; set; }
        public CS.General_v3.Enums.GOOGLE_ANALYTICS_TYPE? GoogleAnalyticsType { get; set; }
    }
}