﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Modules.CultureDetailsModule;

namespace CS.WebComponentsGeneralV3.Code.Classes.GoogleAnalytics
{
    public class GoogleAnalyticsManager
    {
        private GoogleAnalyticsManager()
        {

        }

        /// <summary>
        /// Returns the current Google Analytics Info. First it attempts to fetch the
        /// data from the current culture. If not found, it will fall back to the data
        /// from the global settings.
        /// </summary>
        /// <returns></returns>
        public static GoogleAnalyticsInfo GetInfo()
        {
            GoogleAnalyticsInfo result = new GoogleAnalyticsInfo();
            ICultureDetailsBase currentCulture = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture();
            result.GoogleAnalyticsID = getGAID(currentCulture);
            result.GoogleAnalyticsRootDomain = getRootDomain(currentCulture);
            result.GoogleAnalyticsType = getAnalyticsType(currentCulture);
            return result;
        }

        private static General_v3.Enums.GOOGLE_ANALYTICS_TYPE? getAnalyticsType(ICultureDetailsBase culture)
        {
            General_v3.Enums.GOOGLE_ANALYTICS_TYPE? result = null;
            result =
                        BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<General_v3.Enums.GOOGLE_ANALYTICS_TYPE?>(
                            CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_GoogleAnalyticsDomainType);
            return result;
        }

        private static string getRootDomain(ICultureDetailsBase culture)
        {
            string result = null;
            if (culture != null)
            {
                if (!string.IsNullOrWhiteSpace(culture.GoogleAnalyticsRootDomain))
                {
                    result = culture.GoogleAnalyticsRootDomain;
                }
                else
                {
                    result =
                        BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(
                            CS.General_v3.Enums.SETTINGS_ENUM.BaseWebsiteDomain);
                }
            }
            return result;
        }

        private static string getGAID(ICultureDetailsBase culture)
        {
            string result = null;
            if (culture != null)
            {
                if (!string.IsNullOrWhiteSpace(culture.GoogleAnalyticsID))
                {
                    result = culture.GoogleAnalyticsID;
                }
                else
                {
                    result =
                        BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(
                            CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_GoogleAnalyticsUserAccountID);
                }
            }
            return result;
        }
    }
}