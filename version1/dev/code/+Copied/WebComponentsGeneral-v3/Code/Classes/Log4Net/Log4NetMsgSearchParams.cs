﻿using System;
using System.Collections.Generic;

namespace CS.WebComponentsGeneralV3.Code.Classes.Log4Net
{
    public class Log4NetMsgSearchParams : BusinessLogic_v3.Classes.URL.ListingURLParser.ListingURLParser
    {
       // public int PageNo { get; set; }
      //  public int ShowAmt { get; set; }

        public string Delimeter { get; set; }

        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _LevelTypes = null;
        public List<BusinessLogic_v3.Classes.log4netClasses.GeneralLog4Net.LOG4NET_TYPE> LevelTypes { get; set; }


        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _LoggerName = null;
        public List<string> LoggerName { get; set; }
        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _MethodAndClass = null;
        public List<string> MethodAndClass { get; set; }

        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _ThreadID = null;
        public List<string> ThreadID { get; set; }

        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _Message = null;
        public List<string> Message { get; set; }

        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _DateFrom = null;
        public DateTime? DateFrom { get; set; }
        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _DateTo = null;
        public DateTime? DateTo { get; set; }

        public Log4NetMsgSearchParams(string customUrl = null) : base("", customUrl)
        {
            this.Delimeter = "||";
                
            this._LevelTypes = new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "levelTypes");
            this._LoggerName = new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "loggerName");
            this._MethodAndClass= new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "method");
            this._Message = new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "message");
            this._ThreadID= new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "threadID");
            _DateFrom = new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "dateFrom");
            _DateTo= new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "dateTo");
            this.PagingInfo.DefaultShowAmtValue = 250;
            loadInitialValues();
            
        }



        
        protected override string getRouteName()
        {
            return CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsRoutesMapper.Route_LogFiles_ViewLogEntries;
            
        }

        protected override string getPageURL()
        {
            return CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsRoutesMapper.Instance.GetLogFiles_ViewPage();
            
        }
        private void loadInitialValues()
        {
            this.LevelTypes = _LevelTypes.GetValueAsListOfEnums<BusinessLogic_v3.Classes.log4netClasses.GeneralLog4Net.LOG4NET_TYPE>(delimeter: Delimeter);

            this.MethodAndClass = _MethodAndClass.GetValueAsListOfString(delimeter: Delimeter) ;
            this.LoggerName = _LoggerName.GetValueAsListOfString(delimeter: Delimeter);
            this.Message = _Message.GetValueAsListOfString(delimeter: Delimeter);
            this.ThreadID = _ThreadID.GetValueAsListOfString(delimeter: Delimeter);
            this.DateFrom = _DateFrom.GetValueAsDateTimeNullable();
            this.DateTo= _DateFrom.GetValueAsDateTimeNullable();
        }

        protected override void updateURLParameterValues()
        {
            _LevelTypes.SetValueFromListOfEnum < BusinessLogic_v3.Classes.log4netClasses.GeneralLog4Net.LOG4NET_TYPE>(this.LevelTypes,delimeter: this.Delimeter);
            _LoggerName.SetValue(this.LoggerName, delimeter: this.Delimeter);
            _Message.SetValue(this.Message, delimeter: this.Delimeter);
            _MethodAndClass.SetValue(this.MethodAndClass, delimeter: this.Delimeter);
            _ThreadID.SetValue(this.ThreadID, delimeter: this.Delimeter);
            _DateFrom.SetValue(this.DateFrom);
            _DateTo.SetValue(this.DateTo);
           

        }
    }
}
