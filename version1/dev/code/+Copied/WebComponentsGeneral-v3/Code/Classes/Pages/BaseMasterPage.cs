﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    using System.Web.UI.HtmlControls;

    public abstract class BaseMasterPage<TNestedMasterPage> : BaseMasterPageBase where TNestedMasterPage : BaseMasterPageBase
    {
        public new TNestedMasterPage Master { get { return (TNestedMasterPage)base.Master; } }

        public class FUNCTIONALITY : BaseMasterPageBase.FUNCTIONALITY
        {
            protected BaseMasterPageBase _item;
            public FUNCTIONALITY(BaseMasterPageBase masterPage)
                : base(masterPage)
            {
                _item = masterPage;
            }

            public string LastPageURL
            {
                get
                {
                    return (string)CS.General_v3.Util.SessionUtil.GetObject("_LastPageURL");
                }
                set
                {
                    CS.General_v3.Util.SessionUtil.SetObject("_LastPageURL", value);
                }

            }

        }
        protected override BaseMasterPageBase.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }



    }
}