﻿using BusinessLogic_v3.Classes.DbObjects.Objects;
using CS.General_v3.Classes.Interfaces.Hierarchy;
using BusinessLogic_v3.Extensions;
namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    using BusinessLogic_v3.Frontend.CultureDetailsModule;
    using CS.General_v3.Util;
    using System;
    using System.Collections.Generic;
    using System.Web.UI.HtmlControls;
    using System.Web.UI;
    using System.IO;
    using System.Text.RegularExpressions;
    using BusinessLogic_v3.Classes.NHibernateClasses.Session;
    using CS.General_v3;
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Dialog;
    using log4net;
    using BusinessLogic_v3.Classes.NHibernateClasses;
    using CS.General_v3.Classes.Caching;
    using BusinessLogic_v3.Frontend.ArticleModule;
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Common;
    using BusinessLogic_v3.Classes.DbObjects;
    using BusinessLogic_v3.Util;
    using BusinessLogic_v3.Modules;
    using BusinessLogic_v3.Classes.Exceptions;
    using CS.General_v3.Classes.URL;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.WebComponentsGeneralV3.Code.Classes.Exceptions;

    public abstract class BasePageBase : BasePageMostBasic
    {


        private bool _pageTitleUpdated = false;


        private ILog _log = log4net.LogManager.GetLogger(typeof(BasePageBase));
        public delegate void MessageHandler(ContentTextBaseFrontend msg, Enums.STATUS_MSG_TYPE messageType);
        public bool PerformCookieCheck { get; set; }

        protected MyNHSessionBase getCurrentNhibernateSession()
        {
            
            return NHClasses.NhManager.GetCurrentSessionFromContext();
        }


        public class FUNCTIONALITY
        {
            public bool DoNotThrowErrorIfContentPageIsNull { get; set; }
            public PageMetaTagsController MetaTagsController { get; private set; }

            private IContentPageProperties _contentPage;
            public IContentPageProperties ContentPage
            {
                get { return _contentPage; }
                set
                {
                    _contentPage = value;
                    if (value != null)
                    {
                        this.SetEditInCmsButtonObject(value.Data, false);
                    }
                }
            }



            private CommonControllersParameters _commonControllersJSParams;
            public CommonControllersParameters CommonControllersJSParams
            {
                get
                {
                    if (_commonControllersJSParams == null)
                    {
                        _commonControllersJSParams = new CommonControllersParameters();
                    }
                    return _commonControllersJSParams;
                }
            }

            public BasePageContextData PageContextData
            {
                get
                {
                    return BasePageContextData.Instance;
                    
                }
            }

            public bool HasErrorInContext
            {
                get
                {
                    return PageContextData.GenericMessageType == Enums.STATUS_MSG_TYPE.Error && (PageContextData.GenericMessage != null);
                }
            }
            private void setEditInCms(IBaseDbObject dbObject, MasterPage mp, bool overrideIfExists = true)
            {
                if (mp != null)
                {
                    if (mp is BaseCommonMasterPage)
                    {
                        BaseCommonMasterPage commonMP = (BaseCommonMasterPage)mp;
                        if (overrideIfExists || commonMP.Functionality.DbObjectToLinkWithEditInCmsButton == null)
                        {
                            commonMP.Functionality.DbObjectToLinkWithEditInCmsButton = dbObject;
                        }
                    }
                    else
                    {
                        setEditInCms(dbObject, mp.Master, overrideIfExists);
                    }
                }
            }

            /// <summary>
            /// This makes the item editable in CMS very easily through a button at the top right of the page 'Edit in CMS'
            /// </summary>
            /// <param name="dbObject"></param>
            public void SetEditInCmsButtonObject(IBaseDbObject dbObject, bool overrideIfExists = true)
            {
                setEditInCms(dbObject, this._basePage.Master, overrideIfExists);
            }

            public const string PAGE_CONTEXT_DATA_ID = "pageContextDataID";

            public bool DisableServerCachingForAll { get; set; }
            public bool DisableServerCachingForLocalhost { get; set; }
            public event MessageHandler DisplayGenericMessage;



            public bool? PermanentRedirectRootToWWW { get; set; }


            public virtual void UpdateTitleAndMetaTags(string title, string metaKeywords, string metaDesc, bool useOpenGraph = true, bool includeWebsiteTitleAsSuffix = true)
            {
                UpdateTitle(title, useOpenGraph, includeWebsiteTitleAsSuffix);
                MetaTagsController.UpdateMetaTags(metaKeywords, metaDesc, useOpenGraph: useOpenGraph);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="pageTitle"></param>
            /// <param name="includeWebsiteTitle"></param>
            /// <param name="websiteTitle">If left null, it is taken from settings</param>
            public virtual void UpdateTitle(string pageTitle, bool useOpenGraph = true, bool includeWebsiteTitleAsSuffix = true)
            {
                _basePage._pageTitleUpdated = true;
                if (includeWebsiteTitleAsSuffix)
                {
                    string websiteName = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.Others_WebsiteName);
                    if (!string.IsNullOrEmpty(websiteName))
                    {
                        pageTitle += " - " + websiteName;
                    }
                }

                if (useOpenGraph)
                {
                    MetaTagsController.UpdateOpenGraphTag(Enums.OPEN_GRAPH.Title, pageTitle);
                }
               


                _basePage.Title = pageTitle;
            }

            public System.Web.HttpRequest Request
            {
                get
                {
                    return System.Web.HttpContext.Current.Request;
                }
            }
            public System.Web.HttpResponse Response
            {
                get
                {
                    return System.Web.HttpContext.Current.Response;
                }
            }
            protected BasePageBase _basePage = null;
            public FUNCTIONALITY(BasePageBase basePage)
            {
                this.MetaTagsController = new PageMetaTagsController(basePage);
                this.OutputLanguageFromCurrentCulture = true;

                this.DisableServerCachingForLocalhost = true;
                _basePage = basePage; 
                RequiresMemberAuthenticationErrorMessage =
                    BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        BusinessLogic_v3.Enums.CONTENT_TEXT.BasePage_MemberAuthentication_ErrorMessage).ToFrontendBase();
                RequiresMemberAuthenticationLoginRedirectionURL = "/";
            }

            public bool RequiresMemberAuthentication { get; set; }
            public string RequiresMemberAuthenticationLoginRedirectionURL { get; set; }
            public ContentTextBaseFrontend RequiresMemberAuthenticationErrorMessage { get; set; }

            public bool RequiresSSL { get; set; }
            public void CheckSSL()
            {
                if (this.RequiresSSL && !Request.IsSecureConnection)
                {
                    if (!CS.General_v3.Util.Other.IsLocalTestingMachine)
                    //this is on purpose, so that locally it will not redirect to SSL always, else there is no way one can test it in visual studio dev server as
                    //it does not support SSL certificates (Karl, 28 Feb 2012)
                    {
                        var url = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation();
                        url.UseSsl = true;
                        CS.General_v3.Util.PageUtil.PermanentRedirect(url.GetURL(fullyQualified: true));
                    }
                    //CS.General_v3.Util.PageUtil.RedirectPage(CS.General_v3.Util.PageUtil.GetCurrentFullURL(true));
                }
                else if (!this.RequiresSSL && Request.IsSecureConnection)
                {
                    var url = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation();
                    url.UseSsl = false;
                    CS.General_v3.Util.PageUtil.PermanentRedirect(url.GetURL(fullyQualified: true));
                    //CS.General_v3.Util.PageUtil.RedirectPage(CS.General_v3.Util.PageUtil.GetCurrentFullURL(false));
                }
            }


            internal void checkForBasePageMessage()
            {

                if ((PageContextData.GenericMessage != null))
                {
                    if (DisplayGenericMessage != null)
                    {
                        DisplayGenericMessage(PageContextData.GenericMessage, PageContextData.GenericMessageType);
                    }
                    PageContextData.LastGenericMessage = PageContextData.GenericMessage;
                    PageContextData.LastGenericMessageType = PageContextData.GenericMessageType;
                    PageContextData.GenericMessage = null;
                    PageContextData.GenericMessageType = CS.General_v3.Enums.STATUS_MSG_TYPE.Success;
                }
                else
                {
                    PageContextData.LastGenericMessage = null;
                    PageContextData.LastGenericMessageType = CS.General_v3.Enums.STATUS_MSG_TYPE.Success;
                }
                //Session is used from different page to page
                if ((BasePageSessionData.Instance.GenericMessage != null))
                {
                    if (DisplayGenericMessage != null)
                    {
                        DisplayGenericMessage(BasePageSessionData.Instance.GenericMessage, BasePageSessionData.Instance.GenericMessageType);
                    }
                    BasePageSessionData.Instance.LastGenericMessage = BasePageSessionData.Instance.GenericMessage;
                    BasePageSessionData.Instance.LastGenericMessageType = BasePageSessionData.Instance.GenericMessageType;
                    BasePageSessionData.Instance.GenericMessage = null;
                    BasePageSessionData.Instance.GenericMessageType = CS.General_v3.Enums.STATUS_MSG_TYPE.Success;

                }
                else
                {
                    BasePageSessionData.Instance.LastGenericMessage = null;
                    BasePageSessionData.Instance.LastGenericMessageType = CS.General_v3.Enums.STATUS_MSG_TYPE.Success;
                }
            }

            public void Init()
            {
                CheckSSL();

            }


            public void ShowErrorMessage(ContentTextBaseFrontend msg)
            {
                ShowGenericMessage(msg, Enums.STATUS_MSG_TYPE.Error);
            }
            public void ShowGenericMessage(ContentTextBaseFrontend msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType)
            {
                showGenericMessage(msg, msgType, false, null);

            }
            private void showGenericMessage(ContentTextBaseFrontend msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType, bool storeInSession, string redirectURL)
            {
                if (storeInSession)
                {

                    StoreGenericMessageInSession(msg, msgType);
                    if (!string.IsNullOrEmpty(redirectURL))
                    {
                        CS.General_v3.Util.PageUtil.RedirectPage(redirectURL);
                    }


                }
                else
                {
                    if ((this.PageContextData.LastGenericMessage != null && msg != null && this.PageContextData.LastGenericMessage.Data.ID != msg.Data.ID) || this.PageContextData.LastGenericMessageType != msgType)
                    {
                        this.PageContextData.GenericMessage = msg;
                        this.PageContextData.GenericMessageType = msgType;
                    }
                }
            }
            public static void StoreGenericMessageInSession(ContentTextBaseFrontend msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType)
            {
                BasePageSessionData.Instance.GenericMessage = msg;
                BasePageSessionData.Instance.GenericMessageType = msgType;
            }
            public void ShowGenericMessageAndRedirectToPage(ContentTextBaseFrontend msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType, string redirectURL)
            {
                showGenericMessage(msg, msgType, true, redirectURL);

            }
            /// <summary>
            /// This will NOT redirect to page but will wait for next redirect by someone else, used e.g. when need to change window.top
            /// </summary>
            /// <param name="msg"></param>
            public void ShowErrorMessageForNextRedirectToPage(ContentTextBaseFrontend msg)
            {
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Error, true, null);
            }
            public void ShowSuccessMessageForNextRedirectToPage(ContentTextBaseFrontend msg)
            {
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Success, true, null);
            }
            public void ShowInformationMessageForNextRedirectToPage(ContentTextBaseFrontend msg)
            {
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Information, true, null);
            }
            public void ShowErrorMessageAndRedirectToPage(ContentTextBaseFrontend msg, string redirectURL = null)
            {
                if (redirectURL == null) redirectURL = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Error, true, redirectURL);
            }
            public void ShowSuccessMessageAndRedirectToPage(ContentTextBaseFrontend msg, string redirectURL = null)
            {
                if (redirectURL == null) redirectURL = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Success, true, redirectURL);
            }
            public void ShowInformationMessageAndRedirectToPage(ContentTextBaseFrontend msg, string redirectURL = null)
            {
                if (redirectURL == null) redirectURL = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Information, true, redirectURL);
            }
            /* public void ShowErrorMessage(string msg, string pageRedirect) {
                 ShowGenericMessage(msg, CS.General_v3.Enums.STATUS_MSG_TYPE.Error, pageRedirect);
             }*/

            public void ShowSuccessMessage(ContentTextBaseFrontend msg)
            {
                ShowGenericMessage(msg, Enums.STATUS_MSG_TYPE.Success);
            }

            /* public void ShowSuccessMessage(string msg, string pageRedirect) {
                 ShowGenericMessage(msg, CS.General_v3.Enums.STATUS_MSG_TYPE.Success, pageRedirect);
                
             }*/

            public void ShowInformationMessage(ContentTextBaseFrontend msg)
            {
                ShowGenericMessage(msg, Enums.STATUS_MSG_TYPE.Information);
            }





            public bool OutputLanguageFromCurrentCulture { get; set; }


            public virtual void UpdateTitleFromContentPage(IContentPageProperties page, bool useOpenGraph = true)
            {
                string title = page.GetTitleForSEO();
                UpdateTitle(title, useOpenGraph);
            }

            public string GetCultureSpecificURLForThisPage(CultureDetailsBaseFrontend culture)
            {
                URLClass url = new URLClass();
                string baseRedirectionURL = culture.GetBaseRedirectionURL();
                if (!string.IsNullOrEmpty(baseRedirectionURL))
                {
                    if (!baseRedirectionURL.Contains("://"))
                    {
                        //Doesn't contain scheme
                        baseRedirectionURL = "http://" + baseRedirectionURL;
                    }

                    URLClass baseURL = new URLClass(baseRedirectionURL);
                    url.Domain = baseURL.Domain;
                    url.Port = baseURL.Port;
                }
                else
                {
                    url[BusinessLogic_v3.Constants.ParameterNames.LANG_PARAM] = culture.Data.GetLanguage2LetterCode();
                }
                return url.ToString();
            }
        }


        private FUNCTIONALITY _functionality;
        public FUNCTIONALITY Functionality
        {
            get
            {
                if (_functionality == null)
                {
                    _functionality = getFunctionality();
                }
                return _functionality;
            }
        }


        public virtual void InitParameters()
        {


        }


        public bool ProcessFormAutomatically { get; set; }

        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }


        public BasePageBase()
        {
            // this.AutoTransferToItselfIfPostback = true;

            this.PerformCookieCheck = true;

            //if (_log.IsInfoEnabled) _log.Info("ConstructorStart");
            ProcessFormAutomatically = true;
            CS.General_v3.Util.PageUtil.SetCurrentPage(this);
            this.Init += new EventHandler(BasePage_Init);
            //if (_log.IsInfoEnabled) _log.Info("ConstructorEnd");
            this.PreRenderComplete += new EventHandler(BasePage_PreRenderComplete);
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
        }
        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
        }
        protected override void OnError(EventArgs e)
        {

            base.OnError(e);
        }

        void BasePage_PreRenderComplete(object sender, EventArgs e)
        {

        }
        public override void RenderControl(HtmlTextWriter writer)
        {
            base.RenderControl(writer);
        }
        protected override void RenderChildren(HtmlTextWriter writer)
        {
            base.RenderChildren(writer);
            //checkPostback();
        }

        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);

        }


        void BasePage_Init(object sender, EventArgs e)
        {
            this.EnableViewState = false;

        }

        protected override void OnPreInit(EventArgs e)
        {
            if (PerformCookieCheck)
                PageUtil.CheckFromPageWhetherBrowserSupportsCookies();
            checkUpdateLanguageFromQS();
            CS.General_v3.Util.PageUtil.WriteCookieToCheckIfCookiesAreEnabled();

            base.OnPreInit(e);
        }
        protected override void OnPreLoad(EventArgs e)
        {
            //setJavaScriptOverrideValues();

            base.OnPreLoad(e);
        }

        public static bool CheckUrlWhetherItRequiresRedirect(string url)
        {

            Match match = Regex.Match(url, "(https?://)(.*)");
            string urlOnly = url;
            if (match.Success)
                urlOnly = match.Groups[2].Value;

            //here use regexsd
            string regExpPattern = @"^((http|https)://)?(www[0-9]?|test|new|dev|old|staging|[0-9]{1,3})\.";
            bool redirect = false;
            if (!Regex.IsMatch(urlOnly, regExpPattern, RegexOptions.IgnoreCase))
            {//redirect required
                redirect = true;
            }
            return redirect;
        }
        private void checkForRootToWWWPermanentRedirect()
        {
            bool redirectToWWW = false;
            if (this.Functionality.PermanentRedirectRootToWWW.HasValue)
            {
                redirectToWWW = this.Functionality.PermanentRedirectRootToWWW.Value;
            }
            else
            {
                //Load from settings
                redirectToWWW = CS.General_v3.Settings.GetSettingFromDatabase<bool>(CS.General_v3.Enums.SETTINGS_ENUM.Others_PermanentRedirectionUrlsToWWW);
            }

            if (redirectToWWW && !CS.General_v3.Util.Other.IsLocalTestingMachine)
            {

                string url = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: true);

                //Changed by Mark on 2011-08-11 as GetCurrentFullURL uses GetBaseURL which interfaces with settings which is not good for this case
                //string url = CS.General_v3.Util.PageUtil.GetCurrentRequest().Url.AbsoluteUri;
                Match match = Regex.Match(url, "(https?://)(.*)");
                string urlOnly = url;
                if (match.Success)
                    urlOnly = match.Groups[2].Value;
                bool redirect = CheckUrlWhetherItRequiresRedirect(urlOnly);

                bool removeDefaultAspx = CS.General_v3.Settings.GetSettingFromDatabase<bool>(CS.General_v3.Enums.SETTINGS_ENUM.Others_RemoveDefaultPageFromWWWRedirect);

                string newURL = match.Groups[1].Value + "www." + urlOnly;
                string defaultAspxFileName = "default.aspx";
                if (removeDefaultAspx)
                {
                    if (newURL.EndsWith(defaultAspxFileName))
                    {
                        newURL = CS.General_v3.Util.Text.SubStr(newURL, 0, (newURL.Length - defaultAspxFileName.Length) - 1);
                    }
                }

                if (redirect)
                {
                    CS.General_v3.Util.PageUtil.PermanentRedirect(newURL);
                }
            }
        }
        private void checkCaching()
        {
            if (this.Functionality.DisableServerCachingForAll || (CS.General_v3.Util.Other.IsLocalTestingMachine && this.Functionality.DisableServerCachingForLocalhost))
            {
                Response.Cache.SetNoServerCaching();
            }
        }
        /*private void updateHtmlOpenGraphXMLNS()
        {
            /*if (!_usingOpenGraphProtocol)
            {
            
            if (HtmlTag != null)
            {//[2011/Oct/28 - Karl added the null check
                HtmlTag.Attributes.Add("xmlns:og", "http://ogp.me/ns#");
            }
            // }
        }*/
        private void checkUpdateLanguageFromQS()
        {
            string langCode = CS.General_v3.Util.QueryStringUtil.GetStringFromQS(BusinessLogic_v3.Constants.ParameterNames.LANG_PARAM);
            if (!string.IsNullOrWhiteSpace(langCode))
            {
                CultureDetailsBaseFrontend culture = BusinessLogic_v3.Modules.Factories.CultureDetailsFactory.GetCultureByCode(langCode, false).ToFrontendBase();

               BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.SetCulture(culture.Data);
               
            }
        }
        /*
        private void updateHtmlFacebookXMLNS()
        {
            /* if (!_usingFacebookProtocol)
             {
            if (HtmlTag != null)
            { //[2011/Oct/28 - Karl added the null check
                HtmlTag.Attributes.Add("xmlns:fb", "http://www.facebook.com/2008/fbml");
            }
            //}
        }*/
        private void updateHtmlTagNamespaces()
        {
            //updateHtmlFacebookXMLNS();
           // updateHtmlOpenGraphXMLNS();
        }
        /// <summary>
        /// Override this for custom authentication methods.  If you want to update the redirection URL update the RequiresMemberAuthenticationLoginRedirectURL / Error Message
        /// </summary>
        /// <returns></returns>
        protected virtual bool checkMemberAuthentication()
        {

            var user = BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.GetLoggedInUserAlsoFromRememberMe();
            if (user == null)
            {

                //No Access
                return false;
            }
            else
            {
                return true;
            }

        }
        
private void validateMemberAuthentication()
        {
            if (this.Functionality.RequiresMemberAuthentication)
            {
                if (!checkMemberAuthentication())
                {
                    if (this.Functionality.ContentPage != null)
                    {
                        BasePageSessionData.Instance.LastNoAccessContentPage = this.Functionality.ContentPage;
                    }
                    else
                    {
                        BasePageSessionData.Instance.LastNoAccessURL = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: true);
                    }
                    //No Access
                    this.Functionality.ShowErrorMessageAndRedirectToPage(this.Functionality.RequiresMemberAuthenticationErrorMessage, this.Functionality.RequiresMemberAuthenticationLoginRedirectionURL);
                }
                else
                {
                    BasePageSessionData.Instance.LastNoAccessContentPage = null;
                }

            }
        }

        protected override void OnInit(EventArgs e)
        {
            validateMemberAuthentication();
            base.OnInit(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            // updateMultiLingualFromDomain();
            checkCaching();
            checkForRootToWWWPermanentRedirect();
            updateHtmlTagNamespaces();
            checkForErrorGenerationForTest();
            InitParameters();
            //updateEditInCms();
            this.Functionality.Init();
            this.Functionality.CommonControllersJSParams.InitJS();
            base.OnLoad(e);
        }

        //private void updateMultiLingualFromDomain()
        //{
        //    if (BusinessLogic_v3.Modules.ModuleSettings.Generic.MultiLingualChangeDomain)
        //    {
        //        string baseUrl = CS.WebComponentsGeneralV3.Code.Util.PageUtil.GetBaseURL();
        //        var cultureDetails =
        //            BusinessLogic_v3.Modules.Factories.CultureDetailsFactory.GetCultureByBaseUrl(baseUrl);

        //        if (cultureDetails != null)
        //        {
        //            BusinessLogic_v3.Classes.Application.AppInstance.Instance.CurrentCultureManager.SetCulture(cultureDetails);
        //        }
        //    }
        //}

        private void parseHTMLForSSL(HtmlTextWriter writer)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter htmlTW = new HtmlTextWriter(sw);
            base.Render(htmlTW);
            string htmlOutput = sw.GetStringBuilder().ToString();

            //E.g. http://www.bet-at.eu";
            var domain = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl().GetURL(fullyQualified: true, appendQueryString: false);
            //E.g. www.bet-at.eu
            domain = CS.General_v3.Util.PageUtil.GetDomainFromUrl(domain);
            //Replace links which do not start with the domain
            System.Text.RegularExpressions.Regex rExternalLinks = new System.Text.RegularExpressions.Regex("<a(.*?)href=(['\"])(http://)(?!" + domain + ")(.*?)(['\"])", System.Text.RegularExpressions.RegexOptions.Singleline | System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            //Temporarily with this
            string replaceTempWith = "!!#_##_##_##!!";
            htmlOutput = rExternalLinks.Replace(htmlOutput, "<a$1href=$2" + replaceTempWith + "$4$5");
            //Replace all http to https
            htmlOutput = Regex.Replace(htmlOutput, "http://", "https://", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            //Replace external links back
            htmlOutput = htmlOutput.Replace(replaceTempWith, "http://"); // back to where they were

            writer.Write(htmlOutput);

        }
        protected override void Render(HtmlTextWriter writer)
        {
            if (this.Functionality.RequiresSSL)
            {
                parseHTMLForSSL(writer);
            }
            else
            {
                base.Render(writer);
            }
        }



        protected override void OnPreRender(EventArgs e)
        {
            this.Functionality.checkForBasePageMessage();
            if (this.Form != null)
            {
                CS.General_v3.Util.Forms.ProcessForm(this.Form, false);
            }



            updatePageSEOFromContentPage();
            initJSDialogParameters(); // in case this is a dialog page
            base.OnPreRender(e);
        }
        /*private void updateEditInCms()
        {
            if (this.Functionality.ContentPage != null)
            {
                this.Functionality.SetEditInCmsButtonObject(this.Functionality.ContentPage.Data);
            }
        }
        */
        private void updatePageSEOFromContentPage()
        {
            if (this.Functionality.ContentPage != null)
            {
                if (!_pageTitleUpdated)
                {
                    this.Functionality.UpdateTitleFromContentPage(this.Functionality.ContentPage);
                }
                if (!this.Functionality.MetaTagsController.PageMetaTagsUpdatedByUser)
                {
                    this.Functionality.MetaTagsController.UpdateMetaFromContentPage(this.Functionality.ContentPage);
                }
            }
            else if (!this.Functionality.DoNotThrowErrorIfContentPageIsNull && PageUtil.IsLocalhost() && !PageUtil.GetCurrentUrlLocation().GetURL().Contains("/cms/") && ThrowContentPageNotSetExceptionWhenLocalhost)
            {
                throw new ContentPageNotSetException("Content Page cannot be null!");
            }
        }

        public bool ThrowContentPageNotSetExceptionWhenLocalhost
        {
            get
            {
                return
                    Factories.SettingFactory.GetSettingValue<bool>(
                        BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ThrowContentPageNotSetExceptionWhenLocalhost);
            }
        }





        private void checkForErrorGenerationForTest()
        {
            if (CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<bool>("GenerateError"))
            {
                throw new TestErrorException("Test Error Generated at " + CS.General_v3.Util.Date.Now.ToString("dd/MM/yyyy hh:mm tt"));
            }


        }



        private void initJSDialogParameters()
        {
            if (this.Functionality.ContentPage != null && this.Functionality.ContentPage.DialogPage)
            {
                jQueryDialogIFrameControllerParameters p = new jQueryDialogIFrameControllerParameters();
                p.title = ((IHierarchy)this.Functionality.ContentPage.Data).Title;
                this.HtmlTag.Attributes["class"] += " html-dialog-page";
                p.width = this.Functionality.ContentPage.DialogWidth;
                p.height = this.Functionality.ContentPage.DialogHeight;
            }
        }
        public virtual HtmlGenericControl HtmlTag
        {
            get
            {
                if (this.Master != null)

                    return this.Master.HtmlTag;
                else
                    return null;
            }
        }
        public virtual HtmlGenericControl BodyTag
        {
            get
            {
                if (this.Master != null)

                    return this.Master.HtmlTag;
                else
                    return null;
            }
        }
        public new BaseMasterPageBase Master { get { return (BaseMasterPageBase)base.Master; } }

        public override void UpdateTitle(string pageTitle, bool useOpenGraph = true)
        {
            this.Functionality.UpdateTitle(pageTitle, useOpenGraph);
        }

        public override void UpdateOpenGraphPageType(Enums.OPEN_GRAPH_TYPE type, bool overwrite = true)
        {
            this.Functionality.MetaTagsController.UpdateOpenGraphPageType(type, overwrite);
        }

        public override void UpdateTitleFromContentPage(ArticleBaseFrontend page, bool useOpenGraph = true)
        {
            this.Functionality.UpdateTitleFromContentPage(page, useOpenGraph);
        }

        public override void UpdateOpenGraphTag(Enums.OPEN_GRAPH tag, string value, bool overwrite = true)
        {
            this.Functionality.MetaTagsController.UpdateOpenGraphTag(tag, value, overwrite);
        }

        public override void UpdateTitleAndMetaTags(string title, string metaKeywords, string metaDesc, bool useOpenGraph = true)
        {
            this.Functionality.UpdateTitleAndMetaTags(title, metaKeywords, metaDesc, useOpenGraph);
        }
    }
}
