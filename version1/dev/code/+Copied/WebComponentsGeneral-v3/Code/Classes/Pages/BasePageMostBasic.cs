﻿using CS.General_v3.Classes.Interfaces.Hierarchy;

namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    using CS.General_v3.Util;
    using System;
    using System.Collections.Generic;
    using System.Web.UI.HtmlControls;
    using System.Web.UI;
    using System.IO;
    using System.Text.RegularExpressions;
    using BusinessLogic_v3.Classes.NHibernateClasses.Session;
    using CS.General_v3;
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Dialog;
    using log4net;
    using BusinessLogic_v3.Classes.NHibernateClasses;
    using CS.General_v3.Classes.Caching;
    using BusinessLogic_v3.Frontend.ArticleModule;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Common;

    public abstract class BasePageMostBasic : BusinessLogic_v3.Classes.Pages.BasePageBL
    {
        
        private ILog _log = log4net.LogManager.GetLogger(typeof(BasePageBase));

        private void createNhibernateSession()
        {
            
            NHClasses.NhManager.CreateNewSessionForContext();
        }

        private void disposeNhibernateSession(bool throwErrorIfNoSessionExists = true)
        {
            if (NHClasses.NhManager.GetSessionFactory() != null)
            {
                NHClasses.NhManager.DisposeCurrentSessionInContext(throwErrorIfNoSessionExists);
            }
        }


        public BasePageMostBasic()
        {
            createNhibernateSession();
        }
        public override void Dispose()
        {
            disposeNhibernateSession();
            base.Dispose();
        }

        protected override void OnError(EventArgs e)
        {
            //disposeNhibernateSession(throwErrorIfNoSessionExists: false);

            base.OnError(e);
        }

        protected override void OnUnload(EventArgs e)
        {
            BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.OnPageEndRequest();
            base.OnUnload(e);
        }

        private void setThreadCulture()
        {
            
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.OnPagePreInit();
            
        }

    }
}
