﻿namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    using System.Web.UI.HtmlControls;
    using Pages;

    public abstract class BaseMainMasterPage : BaseMasterPage<BaseCoreMasterPage>
    {

        #region BaseCommonMasterPage Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY : BaseMasterPage<BaseCoreMasterPage>.FUNCTIONALITY
        {
            protected new BaseMainMasterPage _item { get { return (BaseMainMasterPage)base._item; } }

            internal FUNCTIONALITY(BaseMainMasterPage item)
                : base(item)
            {

            }

        }

        protected override BaseMasterPageBase.FUNCTIONALITY createFunctionality()
        {
            
            return new FUNCTIONALITY(this);
        }

        #endregion


        public BaseMainMasterPage()
        {
        }

        public override HtmlGenericControl HtmlTag
        {
            get { return this.Master.HtmlTag; }
        }

        public override HtmlGenericControl BodyTag
        {
            get { return this.Master.BodyTag; }
        }
    }
}
