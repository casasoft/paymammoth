﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Extensions;

namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using BusinessLogic_v3.Frontend.ArticleModule;
    using BusinessLogic_v3.Modules.MemberModule;
    using General_v3;
    using General_v3.Classes.Login;
    using CS.WebComponentsGeneralV3.Code.Util;
    using BusinessLogic_v3.Frontend.ContentTextModule;

    public delegate void LoginFieldsSuccessHandler(PageLoginHandler sender);
    public delegate void LoginFieldsErrorHandler(PageLoginHandler sender, General_v3.Enums.LOGIN_STATUS result);
  
    public class PageLoginHandler
    {
        public event LoginFieldsSuccessHandler OnLoginSuccess;
        public event LoginFieldsErrorHandler OnLoginFail;
        public bool OnLoginAutoRedirect { get; set; }
        public bool OnLoginRedirectToLastNoAccessURL { get; set; }
        public string OnLoginRedirectToURL { get; set; }
        private BasePageBase _pg;
        public PageLoginHandler(BasePageBase pg)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(pg, "Page cannot be null");
            _pg = pg; 
            this.OnLoginAutoRedirect = true;
            this.OnLoginRedirectToLastNoAccessURL = true;
            this.OnLoginRedirectToURL = "/";
        }
        public void LoginUser(string username, string pass, bool rememberMe)
        {

            ILoginResult<MemberBase> result = BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.Login(username, pass, fromAdmin: false, rememberMe: rememberMe, autoRedirectToLastURL: false);
            General_v3.Enums.LOGIN_STATUS status = result.LoginStatus;

            switch (status)
            {
                case General_v3.Enums.LOGIN_STATUS.Blocked:
                case General_v3.Enums.LOGIN_STATUS.InvalidPass:
                case General_v3.Enums.LOGIN_STATUS.InvalidUser:
                case General_v3.Enums.LOGIN_STATUS.NotAccepted:
                case General_v3.Enums.LOGIN_STATUS.NotActivated:
                case General_v3.Enums.LOGIN_STATUS.SelfExcluded:
                case General_v3.Enums.LOGIN_STATUS.TerminatedAccount:
                    loginFailure(status, BusinessLogic_v3.Enums.GetLoginStatusMessage(result));
                    break;
                case General_v3.Enums.LOGIN_STATUS.Ok:
                    loginSuccess(username, BusinessLogic_v3.Enums.GetLoginStatusMessage(result));
                    break;
                default:
                    throw new InvalidOperationException("Value <" + status + "> not mapped");
            }
        }


        private void loginFailure(General_v3.Enums.LOGIN_STATUS status, ContentTextBaseFrontend message = null)
        {
            if (this.OnLoginFail != null)
            {
                this.OnLoginFail(this, status);
            }

            _pg.Functionality.ShowErrorMessageAndRedirectToPage(message);
        }


        private void loginSuccess(string username, ContentTextBaseFrontend message = null)
        {
            //ok
            DiscountsUtil.AddDiscountCodeFromSession(username);
            
            if (this.OnLoginAutoRedirect)
            {
                if (this.OnLoginSuccess != null)
                {
                    this.OnLoginSuccess(this);
                }

                string urlToRedirectTo = null;
                //Perform redirection
                if (this.OnLoginRedirectToLastNoAccessURL && (BasePageSessionData.Instance.LastNoAccessContentPage != null))
                {
                    urlToRedirectTo = BasePageSessionData.Instance.GetLastNoAccessURL();
                }
                else if (!string.IsNullOrEmpty(OnLoginRedirectToURL))
                {
                    urlToRedirectTo = OnLoginRedirectToURL;
                }
                if (!string.IsNullOrEmpty(urlToRedirectTo))
                {
                    
                    bool canRedirectTo = BusinessLogic_v3.Modules.Factories.ArticleFactory.CanRedirectToURLOnLogin(urlToRedirectTo);
                    if (canRedirectTo)
                    {
                        _pg.Functionality.ShowSuccessMessageAndRedirectToPage(message, urlToRedirectTo);
                    }
                }
            }
        }

    }
}