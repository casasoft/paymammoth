﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using BusinessLogic_v3.Frontend.ArticleModule;
    using General_v3;

    public class PageMetaTagsController
    {


        public bool PageMetaTagsUpdatedByUser { get; set; }
        private BasePageBase _basePage; 
        private HtmlMeta _metaSkype;
        //<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
        private HtmlMeta _metaLanguage;

        private HtmlMeta _metaGeoRegion;
        private HtmlMeta _metaGeoPosition;
        private HtmlMeta _metaGeoPlaceName;
        private HtmlMeta _metaGeoICBM;

        private HtmlMeta _metaKeywords;
        private HtmlMeta _metaDescription;
        public PageMetaTagsController(BasePageBase pg)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(pg, "Page cannot be null");
            _basePage = pg;
            _basePage.Load += new EventHandler(_basePage_Load);
            _basePage.PreRender += new EventHandler(_basePage_PreRender);
        }

        void _basePage_PreRender(object sender, EventArgs e)
        {
            updateMetaTagsPositionAndRemoveEmpty();
        }

        void _basePage_Load(object sender, EventArgs e)
        {
            init();
        }
        private void initDefaultGoogleVerificationTag()
        {
            string googleVerificationContent = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.MetaTag_GoogleSiteVerification);
            if (!string.IsNullOrEmpty(googleVerificationContent))
            {
                this.AddMetaTag("google-site-verification", googleVerificationContent);
            }
        }

        private void init()
        {
            if (!string.IsNullOrEmpty(MetaKeywords))
            {
                MetaKeywords = MetaKeywords.ToLower();
            }
            if (_basePage.Header != null)
            {
                initDefaultGoogleVerificationTag();
                initDefaultFacebookTags();

                initDefaultOpenGraphTags();
                initDefaultGeoLocationTags();
            }
        }

        public HtmlMeta MetaGeoPlacename
        {
            get
            {
                if (_metaGeoPlaceName == null)
                {
                    _metaGeoPlaceName = getMetaTagFromHeader("geo.placename");
                }
                return _metaGeoPlaceName;
            }
        }
        public HtmlMeta MetaGeoICBM
        {
            get
            {
                if (_metaGeoICBM == null)
                {
                    _metaGeoICBM = getMetaTagFromHeader("ICBM");
                }
                return _metaGeoICBM;
            }
        }
        public HtmlMeta MetaGeoPosition
        {
            get
            {
                if (_metaGeoPosition == null)
                {
                    _metaGeoPosition = getMetaTagFromHeader("geo.position");
                }
                return _metaGeoPosition;
            }
        }
        public HtmlMeta MetaGeoRegion
        {
            get
            {
                if (_metaGeoRegion == null)
                {
                    _metaGeoRegion = getMetaTagFromHeader("geo.region");
                }
                return _metaGeoRegion;
            }
        }
        public HtmlMeta MetaLanguage
        {
            get
            {
                if (_metaLanguage == null)
                {
                    _metaLanguage = GetMetaTagFromHeaderWithHttpEquiv("content-language");
                }
                return _metaLanguage;
            }
        }
        public string MetaKeywords
        {
            get
            {
                if (_metaKeywords != null)
                {
                    return _metaKeywords.Content;
                }
                return null;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    if (_metaKeywords != null)
                    {
                        _basePage.Header.Controls.Remove(_metaKeywords);
                    }
                    _metaKeywords = null;
                }
                else
                {
                    if (_metaKeywords == null)
                    {
                        _metaKeywords = getMetaTagFromHeader("keywords");
                    }
                    _metaKeywords.Content = value;
                }
            }
        }
        public string MetaDesc
        {
            get
            {
                if (_metaDescription != null)
                {
                    return _metaDescription.Content;
                }
                return null;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    if (_metaDescription != null)
                    {
                        _basePage.Header.Controls.Remove(_metaDescription);
                    }
                    _metaDescription = null;
                }
                else
                {
                    if (_metaDescription == null)
                    {
                        _metaDescription = getMetaTagFromHeader("description");
                    }
                    _metaDescription.Content = value;
                }
            }
        }

        public HtmlMeta AddMetaTag(string name, string content)
        {
            HtmlMeta tag = getMetaTagFromHeader(name);
            if (tag != null)
            {
                tag.Content = content;
            }
            return tag;
        }

        protected HtmlMeta getMetaTagFromHeader(string name)
        {
            string origName = name;
            name = name.ToLower();
            for (int i = 0; i < _basePage.Header.Controls.Count; i++)
            {
                if (_basePage.Header.Controls[i] is HtmlMeta)
                {
                    HtmlMeta metaControl = (HtmlMeta)_basePage.Header.Controls[i];

                    if (metaControl.Name.ToLower() == name)
                    {
                        return metaControl;
                    }
                }
            }
            //Still not found so add them
            HtmlMeta meta = new HtmlMeta();
            meta.Name = origName;
            _basePage.Header.Controls.Add(meta);
            return meta;

        }
        public HtmlMeta GetOpenGraphMetaTagFromHeader(Enums.FACEBOOK_FBML_NAMESPACE tag)
        {
            return GetOpenGraphMetaTagFromHeader(CS.General_v3.Util.EnumUtils.StringValueOf(tag));
        }
        public HtmlMeta GetOpenGraphMetaTagFromHeader(string tag)
        {
            HtmlMeta metaPropertyOG = GetMetaTagFromHeaderFromAttribute("property", tag);
            return metaPropertyOG;
        }
        public HtmlMeta GetOpenGraphMetaTagFromHeader(Enums.OPEN_GRAPH tag)
        {
            return GetOpenGraphMetaTagFromHeader(CS.General_v3.Util.EnumUtils.StringValueOf(tag));
        }

        public HtmlMeta GetMetaTagFromHeaderFromAttribute(string attribute, string value)
        {
            attribute = attribute.ToLower();
            HtmlMeta metaTag = null;

            if (_basePage.Header != null)
            {
                for (int i = 0; i < _basePage.Header.Controls.Count; i++)
                {
                    if (_basePage.Header.Controls[i] is HtmlMeta)
                    {
                        HtmlMeta metaControl = (HtmlMeta)_basePage.Header.Controls[i];
                        if (metaControl.Attributes[attribute] == value)
                        {
                            metaTag = metaControl;
                        }
                    }
                }
                if (metaTag == null)
                {
                    //Still not found so add them
                    metaTag = new HtmlMeta();
                    metaTag.Attributes[attribute] = value;
                    //meta.Content = value;
                    _basePage.Header.Controls.AddAt(1, metaTag);
                }
            }
            return metaTag;
        }
        public HtmlMeta GetMetaTagFromHeaderWithHttpEquiv(string name)
        {
            string origName = name;
            name = name.ToLower();
            for (int i = 0; i < _basePage.Header.Controls.Count; i++)
            {
                if (_basePage.Header.Controls[i] is HtmlMeta)
                {
                    HtmlMeta metaControl = (HtmlMeta)_basePage.Header.Controls[i];

                    if (metaControl.HttpEquiv.ToLower() == name)
                    {
                        return metaControl;
                    }
                }
            }

            //Still not found so add them
            HtmlMeta meta = new HtmlMeta();
            meta.HttpEquiv = origName;

            _basePage.Header.Controls.Add(meta);
            return meta;

        }


        private void initDefaultGeoLocationTags()
        {

            double? lat = CS.General_v3.Settings.GetSettingFromDatabase<double?>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Latitude);
            double? lng = CS.General_v3.Settings.GetSettingFromDatabase<double?>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Longtitude);
            if (lat.HasValue && lng.HasValue)
            {
                if (string.IsNullOrWhiteSpace(MetaGeoICBM.Content) && string.IsNullOrWhiteSpace(MetaGeoPosition.Content))
                {
                    MetaGeoICBM.Content = MetaGeoPosition.Content = lat.Value + ", " + lng.Value;
                }
            }
            string countryCode3Letter = CS.General_v3.Settings.GetSettingFromDatabase<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Country3LetterCode);
            var countryCode = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(countryCode3Letter);
            if (countryCode.HasValue && string.IsNullOrWhiteSpace(MetaGeoRegion.Content))
            {
                MetaGeoRegion.Content = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode(countryCode.Value);
                MetaGeoPlacename.Content = CS.General_v3.Util.EnumUtils.StringValueOf(countryCode.Value);
            }
        }
        private void initDefaultOpenGraphTags()
        {
            string url = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: true);
            //og:url
            if (!string.IsNullOrEmpty(url) && string.IsNullOrEmpty(GetOpenGraphMetaTagFromHeader(Enums.OPEN_GRAPH.URL).Content)) UpdateOpenGraphTag(Enums.OPEN_GRAPH.URL, url);

            //og:title
            HtmlTitle titleTag = CS.General_v3.Util.ControlUtil.GetFirstControlFromHtmlTagName<HtmlTitle>(_basePage.Header, "title");
            if (titleTag != null && !string.IsNullOrEmpty(titleTag.Text) && string.IsNullOrEmpty(GetOpenGraphMetaTagFromHeader(Enums.OPEN_GRAPH.Title).Content))
            {
                UpdateOpenGraphTag(Enums.OPEN_GRAPH.Title, titleTag.Text);
            }

            //og:site_name
            string websiteName = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.Others_WebsiteName);
            if (!string.IsNullOrEmpty(websiteName) && string.IsNullOrEmpty(GetOpenGraphMetaTagFromHeader(Enums.OPEN_GRAPH.SiteName).Content))
            {
                UpdateOpenGraphTag(Enums.OPEN_GRAPH.SiteName, websiteName);
            }

            //og:image
            string openGraphImage = CS.General_v3.Settings.GetSettingFromDatabase<string>(CS.General_v3.Enums.SETTINGS_ENUM.OpenGraph_DefaultImage);
            if (!String.IsNullOrEmpty(openGraphImage) && string.IsNullOrEmpty(GetOpenGraphMetaTagFromHeader(Enums.OPEN_GRAPH.Image).Content))
            {
                openGraphImage = CS.General_v3.Util.PageUtil.ConvertRelativeUrlToAbsoluteUrl(openGraphImage);
                UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH.Image, openGraphImage);
            }
        }

        private void initDefaultFacebookTags()
        {
            string pageID = CS.General_v3.Settings.GetSettingFromDatabase<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook_PageID);
            string adminsID = CS.General_v3.Settings.GetSettingFromDatabase<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook_Admins);
            string appID = CS.General_v3.Settings.GetSettingFromDatabase<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook_AppID);
            if (!string.IsNullOrEmpty(pageID) && string.IsNullOrEmpty(GetOpenGraphMetaTagFromHeader(Enums.FACEBOOK_FBML_NAMESPACE.PageID).Content)) UpdateFacebookTag(Enums.FACEBOOK_FBML_NAMESPACE.PageID, pageID);
            if (!string.IsNullOrEmpty(adminsID) && string.IsNullOrEmpty(GetOpenGraphMetaTagFromHeader(Enums.FACEBOOK_FBML_NAMESPACE.Admins).Content)) UpdateFacebookTag(Enums.FACEBOOK_FBML_NAMESPACE.Admins, adminsID);
            if (!string.IsNullOrEmpty(appID) && string.IsNullOrEmpty(GetOpenGraphMetaTagFromHeader(Enums.FACEBOOK_FBML_NAMESPACE.AppID).Content)) UpdateFacebookTag(Enums.FACEBOOK_FBML_NAMESPACE.AppID, appID);
            UpdateOpenGraphPageType(Enums.OPEN_GRAPH_TYPE.Website, false);
        }


        public virtual void UpdateMetaFromContentPage(IContentPageProperties page, bool ifEmptyMetaDescUseHtmlText = true, bool useOpenGraph = true)
        {
            string metaDesc = page.GetMetaDescriptionForSEO(ifEmptyMetaDescUseHtmlText);
            string metaKeywords = page.GetMetaKeywordsForSEO();
            UpdateMetaTags(metaKeywords, metaDesc, useOpenGraph);
        }

        public virtual void UpdateTitleAndMetaFromContentPage(ArticleBaseFrontend page, bool ifEmptyMetaDescUseHtmlText = true, bool useOpenGraph = true)
        {
            string metaDesc = page.Data.MetaDescription;
            if (ifEmptyMetaDescUseHtmlText && string.IsNullOrWhiteSpace(metaDesc) && page.Data.GetContent() != null)
            {
                metaDesc = CS.General_v3.Util.Text.ConvertHTMLToPlainText(page.Data.GetContent(), false);
                int maxLength = 155;
                if (metaDesc.Length > maxLength)
                {
                    metaDesc = metaDesc.Substring(0, maxLength);
                }
            }

            _basePage.Functionality.UpdateTitleFromContentPage(page, useOpenGraph);
            UpdateMetaFromContentPage(page, ifEmptyMetaDescUseHtmlText, useOpenGraph);
        }

        public virtual void UpdateMetaTags(string metaKeywords, string metaDesc, bool useOpenGraph = true)
        {
            PageMetaTagsUpdatedByUser = true;
            MetaKeywords = metaKeywords;
            MetaDesc = metaDesc;
            if (useOpenGraph)
            {
                UpdateOpenGraphTag(Enums.OPEN_GRAPH.Description, metaDesc);
            }
            //MetaKeywords.Visible = !string.IsNullOrEmpty(MetaKeywords.Content);
            //MetaDesc.Visible = !string.IsNullOrEmpty(MetaDesc.Content);
        }
        public void UpdateFacebookTag(CS.General_v3.Enums.FACEBOOK_FBML_NAMESPACE tag, string value)
        {
            HtmlMeta metaPropertyFB = GetOpenGraphMetaTagFromHeader(tag);
            if (!string.IsNullOrWhiteSpace(value))
            {
                /* if (!_usingFacebookProtocol)
                 {
                     updateHtmlFacebookXMLNS();
                 }*/
                metaPropertyFB.Content = value;
            }
            else
            {
                _basePage.Header.Controls.Remove(metaPropertyFB);
            }
        }


        public virtual void UpdateMetaDesc(string desc, bool useOpenGraph = true)
        {
            UpdateMetaDesc(desc, true, useOpenGraph);
        }

        public virtual void UpdateMetaDesc(string desc, bool removeHTML, bool useOpenGraph = true)
        {
            if (removeHTML)
            {
                desc = CS.General_v3.Util.Text.ConvertHTMLToPlainText(desc, false);
            }
            MetaDesc = desc;
            if (useOpenGraph)
            {
                this.UpdateOpenGraphTag(Enums.OPEN_GRAPH.Description, desc);
            }
        }

        public void UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH tag, string value, bool overwrite = true)
        {
            HtmlMeta metaPropertyOG = GetOpenGraphMetaTagFromHeader(tag);
            if (metaPropertyOG != null)
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    /*if (!_usingOpenGraphProtocol)
                    {
                        updateHtmlOpenGraphXMLNS();
                    }*/
                    if (overwrite || string.IsNullOrWhiteSpace(metaPropertyOG.Content))
                    {
                        metaPropertyOG.Content = value;
                    }


                }
                else
                {
                    _basePage.Header.Controls.Remove(metaPropertyOG);
                }
            }
        }
        public void UpdateOpenGraphPageType(CS.General_v3.Enums.OPEN_GRAPH_TYPE type, bool overwrite = true)
        {
            UpdateOpenGraphTag(Enums.OPEN_GRAPH.Type, CS.General_v3.Util.EnumUtils.StringValueOf(type), overwrite);
        }
        private void updateMetaTagsPositionAndRemoveEmpty()
        {
            var metaTags = CS.General_v3.Util.ControlUtil.GetControlsFromHtmlTagName<HtmlMeta>(_basePage.Header, "meta");
            //Place them all at top
            foreach (var metaTag in metaTags)
            {
                _basePage.Header.Controls.Remove(metaTag);
                if (!string.IsNullOrEmpty(metaTag.Content)) //add it only if it is not empty
                {
                    _basePage.Header.Controls.AddAt(0, metaTag);
                }
            }

            //Override description on top
            if (_metaDescription != null && !string.IsNullOrEmpty(_metaDescription.Content))
            {
                _basePage.Header.Controls.Remove(_metaDescription);
                _basePage.Header.Controls.AddAt(0, _metaDescription);
            }

            //Override keywords on top
            if (_metaKeywords != null && !string.IsNullOrEmpty(_metaKeywords.Content))
            {
                _basePage.Header.Controls.Remove(_metaKeywords);
                _basePage.Header.Controls.AddAt(0, _metaKeywords);
            }

            //Override title on top
            HtmlTitle titleTag = CS.General_v3.Util.ControlUtil.GetFirstControlFromHtmlTagName<HtmlTitle>(_basePage.Header, "title");
            if (titleTag != null && !string.IsNullOrEmpty(titleTag.Text))
            {

                _basePage.Header.Controls.Remove(titleTag);
                _basePage.Header.Controls.AddAt(0, titleTag);
            }
        }
            
    }
}