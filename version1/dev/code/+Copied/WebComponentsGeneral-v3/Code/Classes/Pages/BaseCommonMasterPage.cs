﻿using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    using System.Web.UI.HtmlControls;
    using _ComponentsGeneric.CMS.UserControls.EditItem;
    using Pages;
    using BusinessLogic_v3.Classes.DbObjects;

    public abstract class BaseCommonMasterPage : BaseMasterPage<BaseMasterPageBase>
    {
        public bool IsGenericMessageEnabled { get; set; }
        protected abstract CmsEditItemLink _cmsEditItemLink { get; }
        #region BaseCommonMasterPage Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY : BaseMasterPage<BaseMasterPageBase>.FUNCTIONALITY
        {
            protected new BaseCommonMasterPage _item { get { return (BaseCommonMasterPage)base._item; } }
            public bool IsGenericMessageEnabled { get; set; }
            public IBaseDbObject DbObjectToLinkWithEditInCmsButton { get; set; }

            internal FUNCTIONALITY(BaseCommonMasterPage item)
                : base(item)
            {

            }

        }

        protected override BaseMasterPageBase.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }


            #endregion
        private void initEditCMS()
        {
            if (this.Functionality.DbObjectToLinkWithEditInCmsButton != null)
            {
                _cmsEditItemLink.Functionality.DbObject = this.Functionality.DbObjectToLinkWithEditInCmsButton;
            }
        }
			
        public BaseCommonMasterPage()
        {
        }
        protected override void OnPreRender(System.EventArgs e)
        {
            initEditCMS();
            base.OnPreRender(e);
        }
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
        }
    }
}
