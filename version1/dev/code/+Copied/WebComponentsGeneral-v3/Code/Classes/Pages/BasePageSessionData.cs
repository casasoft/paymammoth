﻿namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    using General_v3;
    using BusinessLogic_v3.Frontend.ArticleModule;
    using BusinessLogic_v3.Extensions;
    using General_v3.Classes.Factories;
    using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Classes.Text;

    public class BasePageSessionData
    {
        private BasePageSessionData()
        {

        }


        public static BasePageSessionData Instance
        {
            get {
                const string key = "BasePageSessionData";
                BasePageSessionData data = CS.General_v3.Util.PageUtil.GetSessionObject<BasePageSessionData>(key);
                if (data == null)
                {
                    data = new BasePageSessionData();
                    CS.General_v3.Util.PageUtil.SetSessionObject(key, data);
                }
                return data;
            }
        }



        private long? _GenericMessageContentTextID;
        private long? _LastGenericMessageContentTextID;
        private TokenReplacerNew _genericMessageTokenReplacer;
        public ContentTextBaseFrontend GenericMessage
        {
            get
            {
                if (_GenericMessageContentTextID.HasValue)
                {
                    var ct = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetByPrimaryKey(_GenericMessageContentTextID.Value).ToFrontendBase();
                    ct.UpdateTokenReplacer(_genericMessageTokenReplacer);
                    return ct;
                }
                else
                {
                    return null;
                }
            }
            set {
                if (value != null)
                {
                    _GenericMessageContentTextID = value.Data.ID;
                    _genericMessageTokenReplacer = value.TokenReplacer;
                }
                else
                {
                    _GenericMessageContentTextID = null;
                }
            }
        }

            public Enums.STATUS_MSG_TYPE GenericMessageType { get; set; }


            public ContentTextBaseFrontend LastGenericMessage
            {
                get
                {
                    if (_LastGenericMessageContentTextID.HasValue)
                    {
                        return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetByPrimaryKey(_LastGenericMessageContentTextID.Value).ToFrontendBase();
                    }
                    else
                    {
                        return null;
                    }
                }
                set
                {
                    if (value != null)
                    {
                        _LastGenericMessageContentTextID = value.Data.ID;
                    }
                    else
                    {
                        _LastGenericMessageContentTextID = null;
                    }
                }
            }

        public Enums.STATUS_MSG_TYPE LastGenericMessageType { get; set; }

        private long? _LastNoAccessContentPageID { get; set; }
        public IContentPageProperties LastNoAccessContentPage
        {
            get {
                if (_LastNoAccessContentPageID.HasValue)
                {
                    return BusinessLogic_v3.Modules.Factories.ArticleFactory.GetByPrimaryKey(_LastNoAccessContentPageID.Value).ToFrontendBase();
                }
                else
                {
                    return null;
                }
            }
            
            set {
                if (value != null)
                {
                    _LastNoAccessContentPageID = ((IBaseObject)value.Data).ID;
                }
                else
                {
                    _LastNoAccessContentPageID = null;
                }
            }
        }
        public string LastNoAccessURL { get; set; }

        public string GetLastNoAccessURL()
        {
            if (LastNoAccessContentPage == null)
            {
                return LastNoAccessURL;
            }
            else
            {
                return LastNoAccessContentPage.GetUrl();
            }
        }

    }
}
