﻿namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    using BusinessLogic_v3.Extensions;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using General_v3;

    public class BasePageContextData
    {
        private BasePageContextData()
        {

        }
        public static BasePageContextData Instance
        {
            get
            {
                const string key = "BasePageContextData";
                BasePageContextData data = CS.General_v3.Util.PageUtil.GetContextObject<BasePageContextData>(key);
                if (data == null)
                {
                    data = new BasePageContextData();
                    CS.General_v3.Util.PageUtil.SetContextObject(key, data);
                }
                return data;
            }
        }


        private long? _GenericMessageContentTextID;
        private long? _LastGenericMessageContentTextID;
        public ContentTextBaseFrontend GenericMessage
        {
            get
            {
                if (_GenericMessageContentTextID.HasValue)
                {
                    return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetByPrimaryKey(_GenericMessageContentTextID.Value).ToFrontendBase();
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value != null)
                {
                    _GenericMessageContentTextID = value.Data.ID;
                }
                else
                {
                    _GenericMessageContentTextID = null;
                }
            }
        }

        public Enums.STATUS_MSG_TYPE GenericMessageType { get; set; }


        public ContentTextBaseFrontend LastGenericMessage
        {
            get
            {
                if (_LastGenericMessageContentTextID.HasValue)
                {
                    return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetByPrimaryKey(_LastGenericMessageContentTextID.Value).ToFrontendBase();
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value != null)
                {
                    _LastGenericMessageContentTextID = value.Data.ID;
                }
                else
                {
                    _LastGenericMessageContentTextID = null;
                }
            }
        }

        public Enums.STATUS_MSG_TYPE LastGenericMessageType { get; set; }


    }
}
