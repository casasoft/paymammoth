﻿namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    using System.Web.UI.HtmlControls;
    using Pages;

    public abstract class BaseCoreMasterPage : BaseMasterPage<BaseCommonMasterPage>
    {

        #region BaseCommonMasterPage Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY : BaseMasterPage<BaseCommonMasterPage>.FUNCTIONALITY
        {
            protected new BaseCoreMasterPage _item { get { return (BaseCoreMasterPage)base._item; } }

            internal FUNCTIONALITY(BaseCoreMasterPage item)
                : base(item)
            {

            }

        }

        protected override BaseMasterPageBase.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        #endregion


        public BaseCoreMasterPage()
        {
        }



        public override HtmlGenericControl HtmlTag
        {
            get { return this.Master.HtmlTag; }
        }

        public override HtmlGenericControl BodyTag
        {
            get { return this.Master.BodyTag; }
        }
    }
}
