﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;

namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    public class BasePage<TMasterPage> : BasePageBase where TMasterPage : BaseMasterPageBase
    {
        public new TMasterPage Master
        {
            get { return (TMasterPage)base.Master; }
        }

        protected MyNHSessionBase getCurrentNhibernateSession()
        {
            return NHClasses.NhManager.GetCurrentSessionFromContext();
        }
    }

}