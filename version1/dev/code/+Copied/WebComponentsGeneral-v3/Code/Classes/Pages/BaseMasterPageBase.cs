﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3.Code.Classes.Pages
{
    using System.Web.UI.HtmlControls;

    public abstract class BaseMasterPageBase : MasterPage
    {
        public class FUNCTIONALITY
        {
            protected BaseMasterPageBase _item;
            public FUNCTIONALITY(BaseMasterPageBase masterPage)
            {
                _item = masterPage;
            }
            public string LastPageURL
            {
                get
                {
                    return (string)CS.General_v3.Util.SessionUtil.GetObject("_LastPageURL");
                }
                set
                {
                    CS.General_v3.Util.SessionUtil.SetObject("_LastPageURL", value);
                }

            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        public FUNCTIONALITY Functionality { get; private set; }

        protected virtual void initControls()
        {

        }

        public BaseMasterPageBase()
        {
            this.Functionality = createFunctionality();

        }

        protected override void OnInit(EventArgs e)
        {
            this.Functionality.LastPageURL = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL();
            base.OnInit(e);
        }


        public abstract HtmlGenericControl HtmlTag { get; }
        public abstract HtmlGenericControl BodyTag { get; }

        public new BasePageBase Page
        {
            get
            {
                return
                    (BasePageBase) base.Page;
            }
        }
    }
}