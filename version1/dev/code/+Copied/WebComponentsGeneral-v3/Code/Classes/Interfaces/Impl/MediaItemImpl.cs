﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Classes.Interfaces.Impl
{
    public class MediaItemImpl : IMediaItem
    {
        public string ThumbImageURL { get; set; }
        public string LargeItemURL { get; set; }
        public string GetThumbnailImageURL()
        {
            return ThumbImageURL;
        }

        public string Caption
        {
            get;
            set;
        }

        public string GetLargeItemURL()
        {
            return LargeItemURL;
        }

        public string Title
        {
            get;
            set;
        }
    }
}