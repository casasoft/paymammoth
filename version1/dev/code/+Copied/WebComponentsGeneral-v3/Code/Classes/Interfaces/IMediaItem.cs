﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Classes.Interfaces
{
    public interface IMediaItem
    {
        string GetLargeItemURL();
        string GetThumbnailImageURL();
        string Title { get; }
        string Caption { get; }
    }
}
