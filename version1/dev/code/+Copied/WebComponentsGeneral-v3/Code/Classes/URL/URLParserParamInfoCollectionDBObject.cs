﻿using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace CS.WebComponentsGeneralV3.Code.Classes.URL
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
using BusinessLogic_v3.Classes.DbObjects;

    public abstract class URLParserParamInfoCollectionDBObject<T> : URLParserParamInfoCollection<T> where T : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
        private IBaseDbFactory<T> _itemFactory;

        public URLParserParamInfoCollectionDBObject(URLParserBase searchParams, string identifier, URL_PARAMETER_TYPE parameterType, IBaseDbFactory<T> itemFactory)
            : base(searchParams, identifier, parameterType)
        {
            _itemFactory = itemFactory;
        }
        protected override string convertDataTypeToString(T item)
        {
            
            return item.ID.ToString();
        }
        protected override T convertStringToDataType(string value)
        {
            long ID = 0;
            T item = default(T);
            if (long.TryParse(value, out ID))
            {
                item = _itemFactory.GetByPrimaryKey(ID);
            }
            return item;
        }
        

    }
}
