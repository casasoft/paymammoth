﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Classes.URL
{
    public class URLParserParamInfo<T> : URLParserParamInfoBase
    {
        public T DefaultValue
        {
            get
            {
                return CS.General_v3.Util.Other.ConvertStringToBasicDataType<T>(_defaultValue);
            }
            set
            {
                _defaultValue = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value);
            }
        }

        public URLParserParamInfo(URLParserBase searchParams, string identifier, URL_PARAMETER_TYPE parameterType)
            : base(searchParams, identifier, parameterType)
        {

        }


        /*
        public double? GetValueAsDoubleNullable()
        {
            double? num = null;
            double tmp = 0;
            string s = GetValueAsString();
            if (double.TryParse(s, out tmp))
                num = tmp;
            return num;
        }
        public double GetValueAsDouble(double defaultValue = 0)
        {
            var v = GetValueAsDoubleNullable();
            if (!v.HasValue) v = defaultValue;
            return v.Value;
        }
        public int? GetValueAsIntNullable()
        {
            double? d = GetValueAsDoubleNullable();
            return (d.HasValue ? (int?)d.Value : null);
        }
        public int GetValueAsInt(int defaultValue = 0)
        {
            var v = GetValueAsIntNullable();
            if (!v.HasValue) v = defaultValue;
            return v.Value;
        }
        public long? GetValueAsLongNullable()
        {
            double? d = GetValueAsDoubleNullable();
            return (d.HasValue ? (long?)d.Value : null);
        }
        public long GetValueAsLong(long defaultValue = 0)
        {
            var v = GetValueAsLongNullable();
            if (!v.HasValue) v = defaultValue;
            return v.Value;
        }
        public bool? GetValueAsBoolNullable()
        {
            string s = GetValueAsString();
            bool? b = CS.General_v3.Util.Other.TextToBoolNullable(s);
            return b;
        }
        public bool GetValueAsBool(bool defaultValue = false)
        {
            var v = GetValueAsBoolNullable();
            if (!v.HasValue) v = defaultValue;
            return v.Value;
        }
        public DateTime? GetValueAsDateTimeNullable()
        {
            string s = GetValueAsString();
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<DateTime?>(s);
        }
        public DateTime GetValueAsDateTime(DateTime defaultValue)
        {
            var v = GetValueAsDateTimeNullable();
            if (!v.HasValue) v = defaultValue;
            return v.Value;
        }


        public void SetValue(string value)
        {
            this.setValue(value);
        }
        public void SetValue(int? value)
        {
            this.setValue((value != null ? value.Value.ToString() : null));
        }
        public void SetValue(Enum value)
        {
            int? iVal = null;
            if (value != null)
                iVal = (int)(object)value;

            this.SetValue(iVal);
        }
        public void SetValue(long? value)
        {
            this.setValue((value != null ? CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value.Value) : null));
        }
        public void SetValue(double? value)
        {
            this.setValue((value != null ? CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value.Value) : null));
        }
        public void SetValue(DateTime? value)
        {

            this.setValue((value != null ? CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value.Value) : null));
        }
        public void SetValue(bool? value)
        {
            this.setValue((value != null ? CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value.Value) : null));
        }

        */

        public T GetValue()
        {
            string sVal = GetValueAsString(true);
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<T>(sVal);
        }

        public void SetValue(T value, bool storeInSessionOrCookie = true)
        {
            string sVal = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value);
            setValue(sVal, storeInSessionOrCookie);
        }
    }
}
