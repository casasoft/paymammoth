﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Classes.Routing.Member;
using BusinessLogic_v3.Frontend.ArticleModule;

namespace CS.WebComponentsGeneralV3.Code.Classes.URL
{
    public class URLParserOrderHistory : CS.WebComponentsGeneralV3.Code.Classes.URL.URLParserDefaultParams<BusinessLogic_v3.Enums.ORDER_HISTORY_SORT_BY>
    {
        private ArticleBaseFrontend _url { get; set; }
        public URLParserOrderHistory(ArticleBaseFrontend article)
            : base(BusinessLogic_v3.Enums.ORDER_HISTORY_SORT_BY.DateDesc)
        {
            _url = article;
        }

        protected override string getPageURL()
        {
            if(_url != null)
            {
                return _url.GetUrl();
            }
            return null;
        }

        protected override string getRouteNameAndAddAdditionalRouteVariables(CS.General_v3.Classes.Routing.MyRouteValueDictionary routingVariables)
        {
            throw new NotImplementedException();
        }
        /*
        public override BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase ShowAmtValueToContentText(BusinessLogic_v3.Enums.SHOW_AMOUNT value)
        {
            return BusinessLogic_v3.Enums.GetShowAmountAsContentText(value);
        }

        public override BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase SortByValueToContentText(BusinessLogic_v3.Enums.ORDER_HISTORY_SORT_BY value)
        {
            return BusinessLogic_v3.Enums.OrderHistorySortByToContentText(value);
        }*/
    }
}
