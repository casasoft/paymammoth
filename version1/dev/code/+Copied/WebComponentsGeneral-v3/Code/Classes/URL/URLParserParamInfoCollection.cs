﻿namespace CS.WebComponentsGeneralV3.Code.Classes.URL
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;

    public class URLParserParamInfoCollection<T> : URLParserParamInfoBase
    {
        private List<T> _values = new List<T>();

        public char CollectionValueDelimiter { get; set; }

        public URLParserParamInfoCollection(URLParserBase searchParams, string identifier, URL_PARAMETER_TYPE parameterType)
            : base(searchParams, identifier, parameterType)
        {
            CollectionValueDelimiter = '|';
            parseInitialList();
        }
        protected virtual IEnumerable<string> getAsListOfStrings()
        {
            List<string> vals = new List<string>();

            foreach (T item in _values)
            {
                string sVal = convertDataTypeToString(item);
                if (!string.IsNullOrEmpty(sVal))
                {
                    vals.Add(sVal);
                }
            }
            return vals;

        }
        private string serialize()
        {
            var list = getAsListOfStrings();
            return CS.General_v3.Util.Text.JoinList(list, CollectionValueDelimiter.ToString());
        }
        protected virtual string convertDataTypeToString(T item)
        {
            string s = null;
            try
            {
                s = item.ToString();
            }
            catch (Exception ex)
            {
            }
            return s;
        }

        protected virtual T convertStringToDataType(string value)
        {
            var item = CS.General_v3.Util.Other.ConvertStringToBasicDataType<T>(value);
            return item;
        }

        private void parseInitialList()
        {
            string value = GetValueAsString(true);
            if (!string.IsNullOrEmpty(value))
            {
                string[] sVals = value.Split(this.CollectionValueDelimiter);
                foreach (string val in sVals)
                {
                    var item = convertStringToDataType(val);
                    _values.Add(item);
                }
            }

        }

        public void RemoveValue(T value)
        {
            if (_values.Contains(value))
            {
                _values.Remove(value);
                setValue(serialize());
            }
        }

        public void AddValue(T value)
        {
            if (!_values.Contains(value))
            {
                _values.Add(value);
                setValue(serialize());
            }
        }
        public void UpdateValues(List<T> values)
        {
            
            _values.Clear();
            _values.AddRange(values);
            setValue(serialize());
        }
        public List<T> Values
        {
            get { return _values; }
        }
    }
    
}
