﻿namespace CS.WebComponentsGeneralV3.Code.Classes.URL
{
    using System;
    using System.Collections.Generic;
    using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.CategoryModule;
    using CS.General_v3.Classes.Routing;

    public class URLParserDefaultShopParams : URLParserShopParams<BusinessLogic_v3.Enums.ITEM_GROUP_SORT_BY>
    {

        public URLParserDefaultShopParams()
            : base(BusinessLogic_v3.Enums.ITEM_GROUP_SORT_BY.TitleAscending)
        {
        }



        /*
        public override BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase ShowAmtValueToContentText(BusinessLogic_v3.Enums.SHOW_AMOUNT value)
        {
            return BusinessLogic_v3.Enums.GetShowAmountAsContentText(value);
        }

        public override BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase SortByValueToContentText(BusinessLogic_v3.Enums.ITEM_GROUP_SORT_BY value)
        {
            return BusinessLogic_v3.Enums.ItemGroupSortByToContentText(value);
        }*/
    }
}
