﻿namespace CS.WebComponentsGeneralV3.Code.Classes.URL
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;

    public abstract class URLParserParamInfoBase
    {
        public enum URL_PARAMETER_TYPE
        {
            QueryString,
            Routing
        }

        private string _value;

        public URL_PARAMETER_TYPE Type { get; set; }

        public bool StoreInSession { get; set; }
        public bool StoreInCookies { get; set; }

        private bool _updatedInitialValue = false;

        public string Identifier { get; set; }
        protected URLParserBase _urlParams = null;

        protected string _defaultValue { get; set; }

        public URLParserParamInfoBase(URLParserBase searchParams, string identifier, URL_PARAMETER_TYPE parameterType)
        {
            this._urlParams = searchParams;
            this.Identifier = identifier;
            this.Type = parameterType;
            if (searchParams != null)
            {
                //Add yourself
                
                searchParams.AddParameter(this);
            }
        }
        
        private string getInitialValueFromSession()
        {
            string key = _urlParams.getSessionOrCookieKey(this.Identifier);
            string val = CS.General_v3.Util.PageUtil.GetSessionObject<string>(key);
            
            return val;
        }
        private string getInitialValueFromCookies()
        {
            string key = _urlParams.getSessionOrCookieKey(this.Identifier);
            string val = CS.General_v3.Util.PageUtil.GetCookie<string>(key);
            return val;
        }

        private string getValueFromQS()
        {
            //First time read from QS
            string identifier = _urlParams.getQuerystringVariableName(this.Identifier);
            var obj = _urlParams.Url[identifier];
            string value = null;
            if (obj != null)
            {
                value = obj.ToString();
            }
            else
            {
                value = null;
            }

            return value;
        }
        private string getValueFromRouteData()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteValue<string>(_urlParams.getRoutingVariableName(this.Identifier));
        }
        private string getInitialValue()
        {
            switch (this.Type)
            {
                case URL_PARAMETER_TYPE.QueryString: return getValueFromQS();
                case URL_PARAMETER_TYPE.Routing: return getValueFromRouteData();
            }
            throw new NotImplementedException("This type is not implemented");
        }

        public string GetValueAsString(bool loadDefaultValueIfNull)
        {
            string value = null;
            if (!_updatedInitialValue)
            {
                value = getInitialValue();
                if (string.IsNullOrEmpty(value))
                {
                    if (this.StoreInSession)
                    {
                        value = getInitialValueFromSession();
                    }
                    if (string.IsNullOrEmpty(value) && this.StoreInCookies)
                    {
                        value = getInitialValueFromCookies();
                    }
                }
                else
                {
                    //Value was updated so either from QS or RouteData
                    setValue(value); //To update it if different & store in session / cookie
                }
                
                _updatedInitialValue = true;
            }
            value = _value;
            if (loadDefaultValueIfNull && string.IsNullOrEmpty(value))
            {
                value = _defaultValue;
            }
            return value;
        }
        
        public bool IsQuerystringParameter()
        {
            switch (this.Type)
            {
                case URL_PARAMETER_TYPE.QueryString: return true;
            }
            return false;
        }
        public bool IsRoutingParameter()
        {
            switch (this.Type)
            {
                case URL_PARAMETER_TYPE.Routing: return true;
            }
            return false;
        }

        /// <summary>
        /// Set the most basic value
        /// </summary>
        /// <param name="value"></param>
        protected void setValue(string value, bool storeInSessionOrCookie = true)
        {
            if (IsRoutingParameter()) value = CS.General_v3.Util.RoutingUtil.ConvertStringForRouteUrl(value);

            if (_value != value)
            {
                _value = value;
                if (storeInSessionOrCookie && this.StoreInSession)
                {
                    CS.General_v3.Util.PageUtil.SetSessionObject(_urlParams.getSessionOrCookieKey(this.Identifier), value);
                }
                if (storeInSessionOrCookie && this.StoreInCookies)
                {
                    CS.General_v3.Util.PageUtil.SetCookie(_urlParams.getSessionOrCookieKey(this.Identifier), value);
                }
            }
            _updatedInitialValue = true;
        }
        public void Clear()
        {
            setValue(null, false);

        }

    }
}
