﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Classes.Routing.Member;
using BusinessLogic_v3.Frontend.ArticleModule;

namespace CS.WebComponentsGeneralV3.Code.Classes.URL
{
    public class URLParserGeneralListing : CS.WebComponentsGeneralV3.Code.Classes.URL.URLParserDefaultParams<BusinessLogic_v3.Enums.CONTENT_PAGE_SORT_BY>
    {
        private ArticleBaseFrontend _url { get; set; }
        public URLParserGeneralListing(ArticleBaseFrontend article)
            : base(sortByDefaultValue: BusinessLogic_v3.Enums.CONTENT_PAGE_SORT_BY.DateDescending)
        {
            _url = article;
        }

        protected override string getPageURL()
        {
            if (_url != null)
            {
                return _url.GetUrl();
            }
            return null;
        }

        protected override string getRouteNameAndAddAdditionalRouteVariables(CS.General_v3.Classes.Routing.MyRouteValueDictionary routingVariables)
        {
            throw new NotImplementedException();
        }
    }
}
