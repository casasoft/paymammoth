﻿namespace CS.WebComponentsGeneralV3.Code.Classes.URL
{
    using System;
    using BusinessLogic_v3.Modules.ContentTextModule;

    public interface IURLParserPagingInfo
    {
        string GetDifferentPageURL(int pgNo);
        string GetDifferentShowAmtURL(int showAmtValue);
        string GetDifferentSortByURL(Enum sortByValue);
        int PageNo { get;  }
        int ShowAmount { get;  }
        Enum SortByValue { get;  }
        Type SortByEnumType { get; }

        //IContentTextBase ShowAmtValueToContentText(Enum value);
        //IContentTextBase SortByValueToContentText(Enum value);

    }
}
