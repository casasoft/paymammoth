﻿namespace CS.WebComponentsGeneralV3.Code.Classes.URL
{
    using System;
    using System.Collections.Generic;
using BusinessLogic_v3.Modules.ContentTextModule;

    public abstract class URLParserDefaultParams<TEnumSortBy> : URLParserBase, IURLParserPagingInfo
        where TEnumSortBy : struct, IConvertible 
    {
        public static string PARAM_PAGENO = "pg";
        public static string PARAM_SHOWAMT = "show";
        public static string PARAM_SORTBY = "sort";

        public URLParserParamInfo<int> PageNo { get; private set; }
        public URLParserParamInfo<int> ShowAmt { get; private set; }
        public URLParserParamInfo<TEnumSortBy> SortBy { get; private set; }

        public URLParserDefaultParams(TEnumSortBy sortByDefaultValue, int? showAmtDefaultValue = null, string urlToLoad = null, bool loadCurrentUrlIfNull = true)
            : base(urlToLoad, loadCurrentUrlIfNull)
        {
            if (!showAmtDefaultValue.HasValue)
            {
                showAmtDefaultValue = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Listing_ShowAmountComboValuesDefaultValue);
            }

            initDefaultParams(showAmtDefaultValue.Value, sortByDefaultValue);
            
        }

        //public abstract IContentTextBase ShowAmtValueToContentText(TEnumShowAmt value);
        //public abstract IContentTextBase SortByValueToContentText(TEnumSortBy value);




        private void initDefaultParams(int showAmtDefaultValue, TEnumSortBy sortByDefaultValue)
        {
            this.PageNo = new URLParserParamInfo<int>(this, PARAM_PAGENO, URLParserParamInfoBase.URL_PARAMETER_TYPE.QueryString)
            {
                DefaultValue = 1
            };

            this.ShowAmt = new URLParserParamInfo<int>(this, PARAM_SHOWAMT, URLParserParamInfoBase.URL_PARAMETER_TYPE.QueryString)
            {
                DefaultValue = showAmtDefaultValue,
                StoreInCookies = true,
                StoreInSession = true
            };
            this.SortBy = new URLParserParamInfo<TEnumSortBy>(this, PARAM_SORTBY, URLParserParamInfoBase.URL_PARAMETER_TYPE.QueryString)
            {
                DefaultValue = sortByDefaultValue,
                StoreInCookies = true,
                StoreInSession = true
            };
            this.AddParameter(this.PageNo);
            this.AddParameter(this.ShowAmt);
            this.AddParameter(this.SortBy);
        }

        string IURLParserPagingInfo.GetDifferentPageURL(int pgNo)
        {
            int currPg = this.PageNo.GetValue();
            this.PageNo.SetValue(pgNo, false);
            string url = this.GetUrl();
            this.PageNo.SetValue(currPg, false);
            return url;
        }

        string IURLParserPagingInfo.GetDifferentShowAmtURL(int showAmtValue)
        {
            int currValue = this.ShowAmt.GetValue();
            this.ShowAmt.SetValue((int)(object)showAmtValue, false);
            int currPg = this.PageNo.GetValue();
            this.PageNo.SetValue(1, false);
            string url = this.GetUrl();
            this.PageNo.SetValue(currPg);
            this.ShowAmt.SetValue(currValue, false);
            return url;
        }

        string IURLParserPagingInfo.GetDifferentSortByURL(Enum sortByValue)
        {
            TEnumSortBy currValue = this.SortBy.GetValue();
            this.SortBy.SetValue((TEnumSortBy)(object)sortByValue, false);
            string url = this.GetUrl();
            this.SortBy.SetValue(currValue, false);
            return url;
        }
        int IURLParserPagingInfo.PageNo
        {
            get
            {
                return this.PageNo.GetValue();
            }
            /*set
            {
                this.PageNo.SetValue(value);
            }*/
        }

        int IURLParserPagingInfo.ShowAmount
        {
            get
            {
                return this.ShowAmt.GetValue(); //It is an enumeration
            }
            /*set
            {
                var val = CS.General_v3.Util.EnumUtils.EnumValueOf<TEnumShowAmt>(value.ToString());
                this.ShowAmt.SetValue(val);
            }*/
        }

        Enum IURLParserPagingInfo.SortByValue
        {
            get
            {
                return (Enum)(object)this.SortBy.GetValue(); //It is an enumeration
            }
            /*set
            {
                var val = CS.General_v3.Util.EnumUtils.EnumValueOf<TEnumSortBy>(value.ToString());
                this.SortBy.SetValue(val);
            }*/
        }


        Type IURLParserPagingInfo.SortByEnumType
        {
            get { return typeof(TEnumSortBy); }
        }

     

        /*
        public IContentTextBase ShowAmtValueToContentText(Enum value)
        {
            if (CS.General_v3.Util.EnumUtils.IsValidEnum(typeof(TEnumShowAmt), value))
            {
                return ShowAmtValueToContentText((TEnumShowAmt)(object)value);
            }
            else
            {
                return null;
            }
        }

        public IContentTextBase SortByValueToContentText(Enum value)
        {
            if (CS.General_v3.Util.EnumUtils.IsValidEnum(typeof(TEnumSortBy), value))
            {
                return SortByValueToContentText((TEnumSortBy)(object)value);
            }
            else
            {
                return null;
            }
        }*/
    }
}
