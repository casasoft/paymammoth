﻿namespace CS.WebComponentsGeneralV3.Code.Classes.URL
{
    using System;
    using System.Collections.Generic;
    using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.CategoryModule;
    using CS.General_v3.Classes.Routing;

    public abstract class URLParserShopParams<TEnumSortBy> : URLParserDefaultParams<TEnumSortBy>
        where TEnumSortBy : struct, IConvertible
    {
        public URLParserParamInfo<long?> CategoryID { get; private set; }
        public URLParserParamInfo<string> Keywords { get; private set; }

        public URLParserShopParams(TEnumSortBy sortByDefaultValue, string urlToLoad = null, bool loadCurrentUrlIfNull = true)
            : base(sortByDefaultValue, urlToLoad: urlToLoad, loadCurrentUrlIfNull: loadCurrentUrlIfNull)
        {
            initParams();
        }
        private void initParams()
        {
            this.CategoryID = new URLParserParamInfo<long?>(this, BusinessLogic_v3.Classes.Routing.ShopRoute.ROUTE_PARAM_CATEGORY_ID,URLParserParamInfoBase.URL_PARAMETER_TYPE.Routing);
            this.Keywords = new URLParserParamInfo<string>(this, BusinessLogic_v3.Classes.Routing.ShopRoute.ROUTE_PARAM_KEYWORDS,URLParserParamInfoBase.URL_PARAMETER_TYPE.Routing);
            this.AddParameter(CategoryID);
            this.AddParameter(Keywords);
        }

        protected override string getRouteNameAndAddAdditionalRouteVariables(MyRouteValueDictionary routingVariables)
        {
            var catID = CategoryID.GetValue();
            string keywords = Keywords.GetValue();

            if (catID.HasValue && !string.IsNullOrEmpty(keywords))
            {
                catID = null;
                return BusinessLogic_v3.Classes.Routing.ShopRoute.ROUTE_NAME_KEYWORDS;
                //both filled
            }
            else if (catID.HasValue)
            {
                CategoryBaseFrontend category =
                    BusinessLogic_v3.Modules.Factories.CategoryFactory.GetByPrimaryKey(catID.Value).ToFrontendBase();
                if (category != null)
                {
                    routingVariables[BusinessLogic_v3.Classes.Routing.ShopRoute.ROUTE_PARAM_CATEGORY_TITLE] = category.Data.Title;
                    routingVariables[BusinessLogic_v3.Classes.Routing.ShopRoute.ROUTE_PARAM_CATEGORY_ID] = category.Data.ID;
                }

                //Category filled in
                return BusinessLogic_v3.Classes.Routing.ShopRoute.ROUTE_NAME_CATEGORY;
            }
            else if (!string.IsNullOrEmpty(keywords))
            {
                //keywrods filled in
                return BusinessLogic_v3.Classes.Routing.ShopRoute.ROUTE_NAME_KEYWORDS;
            }
            else
            {
                //no parameters
                return BusinessLogic_v3.Classes.Routing.ShopRoute.ROUTE_NAME_DEFAULT_SEARCH;
            }
        }

        protected override string getPageURL()
        {
            return "/products/";
        }
    }
}
