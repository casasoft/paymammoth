﻿namespace CS.WebComponentsGeneralV3.Code.Classes.URL
{
    using System;
    using System.Collections.Generic;
    using CS.General_v3.Classes.URL;
    using CS.General_v3.Classes.Routing;

    public abstract class URLParserBase
    {
        private Dictionary<string, URLParserParamInfoBase> _paramInfos = null;

        public string DefaultRouteName { get; set; }
        public URLClass Url { get; private set; }

        public string PrefixQuerystringVariable { get; set; }
        public string PrefixSessionCookieKey { get; set; }

        private bool _loadCurrentURLIfNull;
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">An identifier which will be used to get the session keys</param>
        /// <param name="urlToLoad"></param>
        /// <param name="loadCurrentUrlIfNull"></param>
        public URLParserBase(string urlToLoad = null, bool loadCurrentUrlIfNull = true)
        {
            _loadCurrentURLIfNull = loadCurrentUrlIfNull;
            if (urlToLoad == null && loadCurrentUrlIfNull)
            {
                urlToLoad = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
            }

            this.Url = new CS.General_v3.Classes.URL.URLClass(urlToLoad, _loadCurrentURLIfNull);
            this._paramInfos = new Dictionary<string, URLParserParamInfoBase>();

        }

        internal protected string getQuerystringVariableName(string identifier)
        {
            string key = identifier.ToLower();
            if (!string.IsNullOrEmpty(this.PrefixQuerystringVariable))
                key = this.PrefixQuerystringVariable.ToLower() + "_" + identifier;
            return key;
        }
        internal protected string getRoutingVariableName(string identifier)
        {
            return identifier;
        }
        internal protected string getSessionOrCookieKey(string identifier)
        {
            string key = identifier.ToLower();
            if (!string.IsNullOrEmpty(this.PrefixSessionCookieKey))
                key = this.PrefixSessionCookieKey.ToLower() + "_" + identifier;
            return key;
        }
        private bool hasGotRoutingParameters()
        {
            foreach(var key in _paramInfos.Keys)
            {
                if (_paramInfos[key].IsRoutingParameter())
                {
                    return true;
                }
            }
            return false;
        }
        public void AddParameter(URLParserParamInfoBase paramInfo)
        {
            string identifier = paramInfo.Identifier;
            string key = identifier;
            if (_paramInfos.ContainsKey(key))
                _paramInfos.Remove(key);
            _paramInfos.Add(key, paramInfo);
        }
        
        public void RemoveParameter(string identifier)
        {
            string key = identifier;
            if (_paramInfos.ContainsKey(key))
                _paramInfos.Remove(key);
        }
        
        public URLParserParamInfoBase GetParameter(string identifier)
        {
            URLParserParamInfoBase result = null;
            string key = identifier;
            if (_paramInfos.ContainsKey(key))
                result =_paramInfos[key];
            else
                throw new InvalidOperationException("URLParser_Base:: Parameter with identifier <" + identifier + "> does not exist");
            return result;
        }

        public virtual string GetQuerystring()
        {
            return Url.GetQueryString();
        }
        /// <summary>
        /// Certain routes would require more variables such as /articles/CAT_ID/TITLE.. Title is not added by default so it need to be added manually
        /// </summary>
        /// <param name="routeName"></param>
        /// <param name="routingVariables"></param>
        /// <returns></returns>
        protected abstract string getRouteNameAndAddAdditionalRouteVariables(MyRouteValueDictionary routingVariables);
        protected abstract string getPageURL();

        /// <summary>
        /// Override this for custom RouteURL params
        /// </summary>
        /// <param name="fullyQualifiedUrl"></param>
        /// <returns></returns>
        protected string getRouteURL(bool fullyQualifiedUrl = true)
        {
            CS.General_v3.Classes.Routing.MyRouteValueDictionary values = new General_v3.Classes.Routing.MyRouteValueDictionary();
            foreach( string key in _paramInfos.Keys) {
                URLParserParamInfoBase param = _paramInfos[key];
                if (param.IsRoutingParameter()) {
                    string value = param.GetValueAsString(true);
                    values.Add(param.Identifier, value);
                }
            }
            string routeName = getRouteNameAndAddAdditionalRouteVariables(values);
            string url = CS.General_v3.Util.RoutingUtil.GetRouteUrl(routeName, values, fullyQualifiedUrl);
            return url;
        }
        /// <summary>
        /// Update any QS variables
        /// </summary>
        private void updateQuerystringVariables()
        {
            foreach (string key in _paramInfos.Keys)
            {
                var item = _paramInfos[key];
                if (item.IsQuerystringParameter())
                {
                    string value = item.GetValueAsString(false);
                    if (!string.IsNullOrEmpty(value))
                    {
                        this.Url[getQuerystringVariableName(item.Identifier)] = value;
                    }
                }
            }
        }

        public virtual string GetUrl(bool appendQS = true)
        {
             string baseURL = null;
            if (appendQS)
            {
                updateQuerystringVariables(); // update them in URL
            }
            if (hasGotRoutingParameters())
            {
                baseURL = getRouteURL();
            }
            else
            {
                baseURL = getPageURL();
            }
            Url.ParseURL(baseURL, parseQuerystringVariables: false);
            return Url.GetURL(fullyQualified: true,  appendQueryString: appendQS);
        }


        public virtual void Redirect(bool appendQS = true)
        {
            string url = GetUrl(appendQS);
            CS.General_v3.Util.PageUtil.RedirectPage(url);

        }

        public void ClearParameters()
        {
            foreach (string key in _paramInfos.Keys)
            {
                var item = _paramInfos[key];
                item.Clear();
            }
        }
    }
}
