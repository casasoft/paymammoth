﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Classes.Images
{
    public class ImageDimensionCacheItem
    {
        public int? Width { get; set; }
        public int? Height { get; set; }
    }
}