﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Classes.Images
{
    public class ImageDimensionCacheManager
    {
        private const string CACHE_SUFFIX = "ImageDimensionsCache_";
        //const string CACHE_WIDTH_SUFFIX = "_width";
        //const string CACHE_HEIGHT_SUFFIX = "_height";
        public static ImageDimensionCacheManager Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<ImageDimensionCacheManager>(); }
        }
        public string GetImageCacheID(string filePath)
        {
            string cacheID = CACHE_SUFFIX + filePath;
            return cacheID;
        }
        public void SetCachedWidthAndHeight(int? width, int? height, string filePath)
        {
            string cacheID = GetImageCacheID(filePath);
            ImageDimensionCacheItem cacheItem = new ImageDimensionCacheItem();
            cacheItem.Width = width;
            cacheItem.Height = height;
            if (width.HasValue && height.HasValue)
            {
                CS.General_v3.Util.CachingUtil.AddItemToCache(cacheID, cacheItem, TimeSpan.FromDays(30));
            }
            else
            {
                CS.General_v3.Util.CachingUtil.RemoveItemFromCache(cacheID );
            }
            //if (width.HasValue)
            //{
            //    CS.General_v3.Util.CachingUtil.AddItemToCache(cacheID + CACHE_WIDTH_SUFFIX, width.Value, TimeSpan.FromDays(30));
            //}
            //else
            //{
            //    CS.General_v3.Util.CachingUtil.RemoveItemFromCache(cacheID + CACHE_WIDTH_SUFFIX);

            //}
            //if (height.HasValue)
            //{
            //    CS.General_v3.Util.CachingUtil.AddItemToCache(cacheID + CACHE_HEIGHT_SUFFIX, height.Value, TimeSpan.FromDays(30));
            //}
            //else
            //{
            //    CS.General_v3.Util.CachingUtil.RemoveItemFromCache(cacheID + CACHE_HEIGHT_SUFFIX);

            //}
        }
        public ImageDimensionCacheItem GetImageDimensionsFromCache(string localPath)
        {
            string cacheID = GetImageCacheID(localPath);
            return CS.General_v3.Util.CachingUtil.GetItemFromCache<ImageDimensionCacheItem>(cacheID);
        }



        public void WatchFile(string localPath)
        {
            if (File.Exists(localPath))
            {
                CS.General_v3.Util.IO.WatchFileForChanges(localPath, ImageDimensionCacheManager.Instance.OnImageFileSystemChange, false, false);
            }
        }


        public void OnImageFileSystemChange(object sender, FileSystemEventArgs e)
        {

            SetCachedWidthAndHeight(null, null, e.FullPath);
        }
    }
}