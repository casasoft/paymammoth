﻿namespace CS.WebComponentsGeneralV3.Code.Classes.PagesOld 
{
    using System;
    using System.Web.UI.HtmlControls;
    using BasePage = PagesOld.BasePage;

    public abstract class BaseMasterPage : System.Web.UI.MasterPage
    {
        public class FUNCTIONALITY
        {
            protected BaseMasterPage _item;
            public FUNCTIONALITY(BaseMasterPage masterPage)
            {
                _item = masterPage;
            }
            public string LastPageURL
            {
                get
                {
                    return (string)CS.General_v3.Util.SessionUtil.GetObject("_LastPageURL");
                }
                set
                {
                    CS.General_v3.Util.SessionUtil.SetObject("_LastPageURL", value);
                }

            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        public FUNCTIONALITY Functionality {get;private set;}
        
        protected virtual void initControls()
        {
            
        }

        public BaseMasterPage()
        {
            this.Functionality = createFunctionality();
            
        }

        protected override void OnInit(EventArgs e)
        {
            this.Functionality.LastPageURL = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL();
            base.OnInit(e);
        }


        public abstract HtmlGenericControl HtmlTag { get; }

        public new PagesOld.BasePage Page
        {
            get
            {
                return
                    (PagesOld.BasePage)base.Page;
            }
        }
    }
}
