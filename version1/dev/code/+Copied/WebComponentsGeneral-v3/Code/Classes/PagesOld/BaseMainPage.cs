﻿namespace CS.WebComponentsGeneralV3.Code.Classes.PagesOld
{
    using BasePage = PagesOld.BasePage;

    public abstract class BaseMainPage : PagesOld.BasePage
    {


        public new BaseMainMasterPage Master { get { return (BaseMainMasterPage)base.Master; } }


    }
}
