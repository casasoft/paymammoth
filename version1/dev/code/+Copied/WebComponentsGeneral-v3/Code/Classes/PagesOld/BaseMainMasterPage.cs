﻿namespace CS.WebComponentsGeneralV3.Code.Classes.PagesOld 
{
    using System.Web.UI.HtmlControls;
    using Pages;

    public abstract class BaseMainMasterPage : BaseMasterPage
    {
        public HtmlGenericControl BodyTag { get { return this.Master.BodyTag; } }


        #region BaseMainMasterPage Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY : BaseMasterPage.FUNCTIONALITY
        {
            protected new BaseMainMasterPage _item { get { return (BaseMainMasterPage)base._item; } }

            internal FUNCTIONALITY(BaseMainMasterPage item)
                : base(item)
            {

            }

        }

        protected override BaseMasterPage.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        #endregion


        public BaseMainMasterPage()
        {
        }

        public new BaseCoreMasterPage Master { get { return (BaseCoreMasterPage)base.Master; } }


        }
}
