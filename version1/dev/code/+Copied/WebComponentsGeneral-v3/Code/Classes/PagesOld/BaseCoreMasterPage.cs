﻿namespace CS.WebComponentsGeneralV3.Code.Classes.PagesOld 
{
    using System.Web.UI.HtmlControls;
    using Pages;

    public abstract class BaseCoreMasterPage : BaseMasterPage
    {
        public HtmlGenericControl BodyTag { get { return this.Master.BodyTag; } }

            #region BaseCommonMasterPage Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY : BaseMasterPage.FUNCTIONALITY
        {
            protected new BaseCoreMasterPage _item { get { return (BaseCoreMasterPage)base._item; } }

            internal FUNCTIONALITY(BaseCoreMasterPage item)
                : base(item)
            {

            }

        }

        protected override BaseMasterPage.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        #endregion


        public BaseCoreMasterPage()
        {
        }

        public new BaseCommonMasterPage Master { get { return (BaseCommonMasterPage)base.Master; } }


        }
}
