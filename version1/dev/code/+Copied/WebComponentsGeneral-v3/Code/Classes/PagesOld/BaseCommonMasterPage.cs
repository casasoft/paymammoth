﻿namespace CS.WebComponentsGeneralV3.Code.Classes.PagesOld 
{
    using System.Web.UI.HtmlControls;
    using Pages;

    public abstract class BaseCommonMasterPage : BaseMasterPage
    {
        public abstract HtmlGenericControl BodyTag { get; }

        #region BaseCommonMasterPage Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY : BaseMasterPage.FUNCTIONALITY
        {
            protected new BaseCommonMasterPage _item { get { return (BaseCommonMasterPage)base._item; } }

            internal FUNCTIONALITY(BaseCommonMasterPage item)
                : base(item)
            {

            }

        }

        protected override BaseMasterPage.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        #endregion
		
			
        public BaseCommonMasterPage()
        {
        }


    }
}
