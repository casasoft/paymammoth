﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace CS.WebComponentsGeneralV3.Code.Classes.DataImport.Helpers
{
    public class ImageToImport
    {
        public FileInfo File { get; set; }
        public bool ReassignToVariationIfExisting { get; set; }
        
    }
}