﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using BusinessLogic_v3.Modules.ProductVariationModule;
using CS.General_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.DataImport.Helpers;
using log4net;
using BusinessLogic_v3.Classes.DataImport.Helpers;

namespace CS.WebComponentsGeneralV3.Code.Classes.DataImport
{
    public class ProductImageImporter
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ProductImageImporter));
        public ProductsImporter Importer { get; set; }

        public ProductImageImporter(ProductsImporter importer)
        {
            this.Importer = importer;
        }

        private List<ImageToImport> findImagesByBarcode(ProductVariationBase variation)
        {
            List<ImageToImport> list = new List<ImageToImport>();
            string startsWith = variation.Barcode;
            if (!string.IsNullOrWhiteSpace(startsWith))
            {
                IEnumerable<FileInfo> imageFiles = Importer.GetImagesFromFileSystem(startsWith);
                foreach (var f in imageFiles)
                {
                    list.Add(new ImageToImport() { File = f, ReassignToVariationIfExisting= true});
                }
                

                //assignImages(imageFiles, variation, reassignToVariationAnyExistingImages: true);

            }
            return list;

        }
        private List<ImageToImport> findImagesByProductRefcode(ProductVariationBase variation)
        {
            List<ImageToImport> list = new List<ImageToImport>();
           // bool oneFound = false;
            var p = variation.Product;
            if (p.ProductVariations.Count > 0 && variation.ID == p.ProductVariations.FirstOrDefault().ID)
            {

                string startsWith = p.ReferenceCode + @"[\. -/\\_\(#,]";
                if (!string.IsNullOrWhiteSpace(startsWith))
                {
                    IEnumerable<FileInfo> imageFiles = Importer.GetImagesFromFileSystem(startsWith);
                    foreach (var f in imageFiles)
                    {
                        list.Add(new ImageToImport() { File = f, ReassignToVariationIfExisting= false});
                    }
                    //list.AddRange(imageFiles);
                    if (_log.IsDebugEnabled) _log.Debug("findImagesByProductRefcode: Found " + imageFiles.Count() + " images - " + CS.General_v3.Util.Text.AppendStrings(", ", imageFiles.ConvertAll(x => x.Name)));
                    //assignImages(imageFiles, variation, reassignToVariationAnyExistingImages: false);//dont reassign any found images
                    //oneFound = (imageFiles.IsNotNullOrEmpty());
                }
            }
            return list;
            //return oneFound;

        }

        private List<ImageToImport> sortFilesByFileName(IEnumerable<ImageToImport> files)
        {
            var fileList = files.ToList();
            fileList.Sort((f1, f2) => (f1.File.FullName.CompareTo(f2.File.FullName)));
            return fileList;

        }
        private void assignImages(IEnumerable<ImageToImport> images, ProductVariationBase variation,DataImportResultLine currentResultLine)
        {

            var imageFiles = images.ToList();
            if (imageFiles.IsNotNullOrEmpty())
            {
                imageFiles = sortFilesByFileName(imageFiles);
                foreach (var imgFile in imageFiles)
                {
                    var file = imgFile.File;
                    string name = file.Name;

                    ProductVariationMediaItemBase mediaItem = variation.Product.GetMediaItemByReference(name);
                    bool existing = false;
                    if (mediaItem == null)
                    {
                        int priority = (variation.MediaItems.Count + 1) * 100;
                        mediaItem = variation.MediaItems.CreateNewItem();
                        mediaItem.Priority = priority;

                    }
                    else
                    {
                        existing = true;
                        if (imgFile.ReassignToVariationIfExisting && mediaItem.ProductVariation != variation)
                        {
                            mediaItem.ProductVariation.MediaItems.Remove(mediaItem);
                            mediaItem.ProductVariation = variation;

                        }
                    }
                    mediaItem.Reference = name;

                    // var fs = imageFile.OpenRead();

                    try
                    {
                        if (!existing)
                        {
                            mediaItem.Image.UploadFileFromLocalPath(file, autoSave: false);
                            //contiunue test imageFiles insert
                            //cointiue here
                        }
                        mediaItem.Save();

                    }
                    catch (Exception ex)
                    {
                        mediaItem.Delete();
                        currentResultLine.Result.AddException(ex);
                    }


                }
            }


        }


        public void CheckForImages(ProductVariationBase variation, DataImportResultLine currentResultLine)
        {
            List<ImageToImport> imagesToImport = new List<ImageToImport>();

            bool oneFound = false;
            {
                var list = findImagesByBarcode(variation);
                imagesToImport.AddRange(list);
            }
            {
                var list = findImagesByProductRefcode(variation);
                imagesToImport.AddRange(list);
            }



            oneFound = imagesToImport.Count > 0;
            assignImages(imagesToImport, variation, currentResultLine);
            if (!oneFound)
            {
                currentResultLine.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Image not found for item with barcode '" + variation.Barcode + "'");
                if (BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Shop_BulkImport_AssignRandomImageIfNoneFound))
                {
                    assignRandomImageIfNoImageFound(variation, currentResultLine);
                }
            }
        }

        private void assignRandomImageIfNoImageFound(ProductVariationBase variation, DataImportResultLine currentResultLine)
        {
            var files = this.Importer.GetImagesFromFileSystem("");

            if (files.Count > 0)
            {
                List<ImageToImport> list = new List<ImageToImport>();
                foreach (var f in files)
                {
                    list.Add(new ImageToImport() { File = f, ReassignToVariationIfExisting = false });
                }
                var rndIndex = CS.General_v3.Util.Random.GetInt(0, list.Count - 1);
                var file = list[rndIndex];
                
                assignImages(CS.General_v3.Util.ListUtil.GetListFromSingleItem(file), variation, currentResultLine);
            }



        }
    }
}