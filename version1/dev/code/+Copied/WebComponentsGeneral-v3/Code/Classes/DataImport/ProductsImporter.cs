﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using BusinessLogic_v3.Classes.DataImport;
using BusinessLogic_v3.Classes.DataImport.Helpers;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductVariationColourModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using BusinessLogic_v3.Modules.ProductVariationModule;
using CS.General_v3.Extensions;

namespace CS.WebComponentsGeneralV3.Code.Classes.DataImport
{
    public class ProductsImporter : DataImporterBase<ProductsImporter>
    {
        public enum ColumnsInfo
        {
            RefCode,
            BarCode,
            Category,
            SubCategory,
            SubSubCategory,
            Title,
            Description,
            PriceWholesale,
            PriceRetail,
            PriceSpecialOffer,
            VatRate,
            Colour,
            Size,
            Material,
            Warranty,
            Brand,
            Quantity



        }
        public bool DontOverwriteDescriptionsIfAlreadyFilled { get; set; }
        private readonly string[] CATEGORY_DELIMETERS = { ">", "=", };

        protected ProductsImporter()
        {
            

        }
        protected override void initialiseColumns()
        {
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.RefCode, "Ref. Code");

                col.Validation.Required = true;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.BarCode);
                col.Validation.Required = true;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Category);
                //col.Validation.Required = true;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.SubCategory);

                col.Validation.Required = false;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.SubSubCategory);

                col.Validation.Required = false;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Title);

                col.Validation.Required = false;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Description);

                col.Validation.Required = false;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.PriceWholesale, "Wholesale");

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.DoubleNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.PriceRetail, "Price (RRP)", "RRP", "Retail");

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.DoubleNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.PriceSpecialOffer, "Special Offer","Special Offer Price");

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.DoubleNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.VatRate, "VAT");

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.DoubleNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Colour);

                col.Validation.Required = false;

                this.Parameters.ColumnsCollection.Columns.Add(col);
            }

            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Size);

                col.Validation.Required = false;
                
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Material);

                col.Validation.Required = false;

                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Warranty);

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.IntegerNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Brand);

                col.Validation.Required = false;

                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Quantity, "Qty");

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.IntegerNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
        }

        protected override bool preProcessFile(CS.General_v3.Util.Encoding.CSV.CSVFile csvFile)
        {
            return base.preProcessFile(csvFile);
        }

        private ProductBase getProductByReferenceCode(string refCode)
        {
           // isNewItemGroup = false;
            ProductBase item = ProductBaseFactory.Instance.GetProductByReferenceCode(refCode);

            //var itemGroupMatched = _itemGroupList.Where((itemGroup) => itemGroup.Data.ReferenceCode == articleRef).FirstOrDefault();
            if (item == null)
            {
                item = ProductBase.Factory.CreateNewItem();
                item.ReferenceCode = refCode;
               // isNewItemGroup = true;
                
            }
            
            return item;
        }

        private ProductVariationBase getVariationByBarCode(string barCode, ProductBase product)
        {
            ProductVariationBase item = ProductVariationBaseFactory.Instance.GetByBarcode(barCode);
          //  isNew = false;
            if (item == null)
            {
                item = product.ProductVariations.CreateNewItem();
                item.Barcode = barCode;
            //    isNew = true;
                return item;
            }
            item.Product = product;
            product.ProductVariations.Add(item);
            return item;

        }
        private BrandBase getBrandOrCreateNewBrand(string brand)
        {
            return (BrandBase)BrandBase.Factory.GetBrandByTitleOrImportRef(brand);
        }
        



        private double getParsedPrice(string priceValue)
        {
            if (priceValue.IsNotNullOrEmpty())
            {
                return double.Parse(Regex.Replace(priceValue, "[^0-9.]", ""));
            }
            return 0;
        }

        private CategoryBase getCategoryByTitleOrCreateNew(string category)
        {
            
            List<string> cats = new List<string>();
            {
                var tokens = CS.General_v3.Util.Text.Split(category, CATEGORY_DELIMETERS.ToArray());
                for (int j = 0; j < tokens.Count; j++)
                {
                    var t = tokens[j];
                    t = t.Trim();
                    cats.Add(t);
                }
            }

            CategoryBase currentCat = CategoryBaseFactory.Instance.GetRootCategory();
            for (int i = 0; i < cats.Count; i++)
            {
                string cat = cats[i];
                if (!string.IsNullOrWhiteSpace(cat))
                {
                    

                    var nextCat = currentCat.GetChildCategoryByTitleOrImportRef(cat);

                    currentCat = nextCat;
                }
            }
            return currentCat;



        }

        private void verifyRequiredFields(ProductBase product, DataImportResultLine resultLine,
            string category, string title, string description, double? priceRetail)
            
            
        {
            if (string.IsNullOrWhiteSpace(title)) resultLine.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Title is required");
            if (string.IsNullOrWhiteSpace(category)) resultLine.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Category is required");
            if (string.IsNullOrWhiteSpace(description)) resultLine.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Description is not filled");
            if (!priceRetail.HasValue) resultLine.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Retail price is required");


        }

        /// <summary>
        /// This method will parse the string for a refernece code
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        protected virtual string parseRefCode(string s)
        {
            if (s != null) s = s.Trim();
            return s;
        }
        /// <summary>
        /// This method will parse the string for a bar code
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        protected virtual string parseBarcode(string s)
        {
            if (s != null) s = s.Trim();
            return s;
        }
        protected virtual void setCmsLink(ProductBase product, DataImportResultLine resultsLine)
        {
            if (product != null)
            {
                var url = CS.WebComponentsGeneralV3.Cms.ProductModule.ProductBaseCmsFactory.Instance.GetEditUrlForItemWithID(product);
                resultsLine.Link = url.GetURL(fullyQualified: true);
            }
        }

        

        protected override void processLine(CS.General_v3.Util.Encoding.CSV.CSVLine csvLine, int lineIndex, DataImportResultLine resultsLine)
        {
            var prevCulture = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture();
            BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.SetCulture(this.Parameters.Culture);
            string refCode =parseRefCode( getColumnData(ColumnsInfo.RefCode, csvLine));
            string barCode = parseBarcode(getColumnData(ColumnsInfo.BarCode, csvLine));
            string category = getColumnData(ColumnsInfo.Category, csvLine);
            string subCategory = getColumnData(ColumnsInfo.SubCategory, csvLine);
            string subSubCategory = getColumnData(ColumnsInfo.SubSubCategory, csvLine);
            string title = getColumnData(ColumnsInfo.Title, csvLine);
            string description = getColumnData(ColumnsInfo.Description, csvLine);
            double? priceWholesale = getNumericColumnData<double?>(ColumnsInfo.PriceWholesale, csvLine);
            double? priceSpecialOffer = getNumericColumnData<double?>(ColumnsInfo.PriceSpecialOffer, csvLine);
            double? priceRetail = getNumericColumnData<double?>(ColumnsInfo.PriceRetail, csvLine);
            double? vatRate = getNumericColumnData<double?>(ColumnsInfo.VatRate, csvLine);
            string colour = getColumnData(ColumnsInfo.Colour, csvLine);
            string size = getColumnData(ColumnsInfo.Size, csvLine);
            string material = getColumnData(ColumnsInfo.Material, csvLine);
            string warrantyText = getColumnData(ColumnsInfo.Warranty, csvLine);
            string brand = getColumnData(ColumnsInfo.Brand, csvLine);
            int? quantity = getNumericColumnData<int?>(ColumnsInfo.Quantity, csvLine);

            bool isDefaultCulture = (this.Parameters.Culture == null || this.Parameters.Culture.IsDefaultCulture);
           

            if (resultsLine.Result.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Error)
            {
                
                using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                {
                    ProductBase product = null;
                    
                    product = getProductByReferenceCode(refCode); //get product, and if it does not exist, create
                    if (product.IsTransient()) verifyRequiredFields(product, resultsLine, category,title,description,priceRetail); // if this is a new product, verify that all the required fields are filled in
                    if (resultsLine.Result.IsSuccessful)
                    {
                        product.Save();

                        product.ReferenceCode = refCode;
                        //if (this.Parameters.Culture != null) product.SetCurrentCultureInfoDBId(this.Parameters.Culture);

                        //if (!isVariationLine)

                        //product fields - Fill in these fields, if they are filled in
                        if (string.IsNullOrWhiteSpace(product.Description) || //description is empty
                            (!string.IsNullOrWhiteSpace(product.Description) && !DontOverwriteDescriptionsIfAlreadyFilled)) //description is not empty, and dont overwrite descriptions is not flagged as true
                        {
                            product.Description = description;
                        }

                        if (!string.IsNullOrWhiteSpace(title)) product.Title = title;
                        //if (priceWholesale.HasValue) product.PriceWholesale = priceWholesale.Value;
                        if (priceRetail.HasValue) product.PriceRetailIncTaxPerUnit = priceRetail.Value;
                        if (priceRetail.HasValue && priceSpecialOffer.GetValueOrDefault() > 0)
                        {
                            product.PriceRetailIncTaxPerUnit = priceRetail.Value;
                            product.PriceDiscounted = priceSpecialOffer.Value;
                        }
                        //product.PriceRetailTaxPerUnit = CS.General_v3.Util.FinancialUtil.GetTaxOnlyAmountFromPriceIncTax(product.PriceRetailIncTaxPerUnit, vatRate.GetValueOrDefault());

                        if (vatRate.GetValueOrDefault() <= 0) vatRate = CS.General_v3.Settings.GetSettingFromDatabase<double>(CS.General_v3.Enums.SETTINGS_ENUM.Others_VatRate);



                        if (!string.IsNullOrWhiteSpace(brand))
                        {
                            if (isDefaultCulture)
                            {
                                BrandBase itemGroupBrand = getBrandOrCreateNewBrand(brand);
                                if (itemGroupBrand != null)
                                {
                                    product.Brand = itemGroupBrand;
                                }
                            }
                            else
                            {
                                if (product.Brand != null)
                                {
                                    product.Brand.Title = brand;
                                }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(material)) product.Material = material;
                        product.Save();
                        {//categories
                            string sFullCat = CS.General_v3.Util.Text.AppendStrings("=", category, subCategory, subSubCategory);

                            if (!string.IsNullOrWhiteSpace(sFullCat))
                            {
                                //categories only apply for default culture.
                                if (isDefaultCulture)
                                {
                                    product.AddCategory(getCategoryByTitleOrCreateNew(sFullCat));
                                }


                            }
                        }
                        //update the variation
                        ProductVariationBase variation = null;

                        variation = getVariationByBarCode(barCode, product); //get the variation, else create
                        variation.Save();
                        if (this.Parameters.Culture != null) variation.SetCurrentCultureInfoDBId(this.Parameters.Culture);

                        //bool isVariationLine = string.IsNullOrWhiteSpace(title); //checks whether this is just an empty variation line.  This is based on if the title is filled in.  
                        //                                                           //A variation is a line which contains online variation specific info like colour, size, quantity





                        //variation fields

                        variation.Barcode = barCode;
                        {
                            if (isDefaultCulture)
                            {
                                variation.Colour = ProductVariationColourBaseFactory.Instance.GetByIdentifier(colour);
                            }
                            else
                            {
                                if (variation.Colour != null)
                                {
                                    variation.Colour.Title = colour;
                                    variation.Colour.Save();
                                }
                            }
                        }
                        variation.Size = size; // ProductVariationSizeBaseFactory.Instance.GetByTitle(size);

                        if (quantity.HasValue)
                        {
                            variation.Quantity = quantity.Value;
                        }
                        processLine_AdditionalLogic_PreSave(csvLine, lineIndex, resultsLine, variation);

                        variation.Save();

                        //end of variation fields

                        if (this.RemoveAnyExistingImages)
                        {
                            variation.RemoveAllImages(autoSave: false);
                        }



                        //product.MarkAsNotTemporary();

                        if (product._Temporary_Flag)
                        {
                            int k = 5;
                        }
                        {
                            ProductImageImporter imageImporter = new ProductImageImporter(this);
                            imageImporter.CheckForImages(variation, resultsLine);
                        }
                        
                        setCmsLink(product, resultsLine);
                        processLine_AdditionalLogic_PostSave(csvLine, lineIndex, resultsLine, variation);
                        t.Commit();
                    }
                }
            }
            BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.SetCulture(prevCulture);
        }

        protected virtual void processLine_AdditionalLogic_PreSave(CS.General_v3.Util.Encoding.CSV.CSVLine csvLine, int lineIndex, DataImportResultLine resultsLine,ProductVariationBase variation)
        {

        }
        protected virtual void processLine_AdditionalLogic_PostSave(CS.General_v3.Util.Encoding.CSV.CSVLine csvLine, int lineIndex, DataImportResultLine resultsLine, ProductVariationBase variation)
        {

        }
    }
}












