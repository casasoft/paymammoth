﻿
using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.IO.Compression;
using System.Text;
using System.Configuration;
using System.Web;

namespace CS.General_v3.HTTPHandlers.FileBrowser
{
    /// <summary>
    /// Combines files together. Querystring can include:
    /// 
    /// usecache = [yes/no] - Whether you want to use caching or not.  To use caching, it must also be enabled from the
    ///                       'settings.config' file, variable: HTTPCombiner_UseCaching
    ///                       
    /// get=[path], where path can contain multiple locations seperated by a comma ','.
    /// 
    /// Each seperate path can contain multiple files at the same folder, seperated by a |.
    /// 
    /// E.g  'get=/includes/js/karl.js|mark.js' is equivalent to 'get=/includes/js/karl.js,/includes.js/mark.js"
    /// 
    /// For relative paths, (local paths on server, you can even use say '*.js' to get all js files)
    /// 
    /// </summary>
    public class FileUpload : IHttpHandler
    {
        public class SETTINGS
        {
            public string FileUploadPath { get; set; }
            public int? MaxWidth { get; set; }
            public int? MaxHeight { get; set; }
            public SETTINGS()
            {
                
                this.FileUploadPath = "/uploads/filebrowser/";
            }
        }
        
        public static SETTINGS Settings
        {
            get
            {
                if (System.Web.HttpContext.Current.Application["_FileBrowser_FileUploadPath"] == null)
                    Settings = new SETTINGS();
                return (SETTINGS)System.Web.HttpContext.Current.Application["_FileBrowser_FileUploadPath"];
            }
            private set
            {
                System.Web.HttpContext.Current.Application["_FileBrowser_FileUploadPath"] = value;
            }
        }
        public static string GetFileUploadPath_Local()
        {
            string s =CS.General_v3.Util.PageUtil.MapPath(Settings.FileUploadPath);
            if (!s.EndsWith("\\")) s += "\\";
            return s;
        }
        public HttpRequest Request
        {
            get
            {
                return System.Web.HttpContext.Current.Request;
            }
        }
        
        private void saveFile(HttpPostedFile file)
        {
            //string path = FileUploadPath;
            bool ok = false;
            int index = 0;
            string path = GetFileUploadPath_Local() + file.FileName;
            do
            {
                
                ok = true;
                if (System.IO.File.Exists(path))
                {
                    index++;
                    path = GetFileUploadPath_Local() + CS.General_v3.Util.IO.GetFilenameOnly(file.FileName) + "_" + index + "." + CS.General_v3.Util.IO.GetExtension(file.FileName);
                    ok = false;
                }

            } while (!ok);
            file.SaveAs(path);
            if (Settings.MaxHeight != null || Settings.MaxWidth != null)
            {
                int width = Settings.MaxWidth ?? 999999;
                int height = Settings.MaxHeight ?? 999999;
                CS.General_v3.Util.Image.ResizeImage(path,width, height, path);
            }
            string relPath = path.Substring(GetFileUploadPath_Local().Length);
            relPath = Settings.FileUploadPath + "/" + relPath + "/";
            relPath = relPath.Replace("\\", "/");
            relPath = relPath.Replace("//", "/");
            System.Web.HttpContext.Current.Response.Write(relPath);
            System.Web.HttpContext.Current.Response.End();
        }
        private void processFiles()
        {
            if (Request.Files.Count > 0)
            {
                HttpPostedFile f = Request.Files[0];
                saveFile(f);
            }

        }
        private void checkPath()
        {
            string s = Settings.FileUploadPath;
            if (string.IsNullOrEmpty(s))
            {
                throw new InvalidOperationException("CS.General_v3.HTTPHandlers.FileBrowser.FileUpload:: Please specify 'FileUploadPath'");
            }
            string path = CS.General_v3.Util.PageUtil.MapPath(s);
            CS.General_v3.Util.IO.CreateDirectory(path);
            if (!System.IO.Directory.Exists(path))
            {
                throw new InvalidOperationException("CS.General_v3.HTTPHandlers.FileBrowser.FileUpload:: Please make sure 'FileUploadPath' folder is created, or user has permissions to create");
            }
        }
        
        public void ProcessRequest(HttpContext context)
        {
            checkPath();
            processFiles();
            
        }


        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        #endregion
    }
}
