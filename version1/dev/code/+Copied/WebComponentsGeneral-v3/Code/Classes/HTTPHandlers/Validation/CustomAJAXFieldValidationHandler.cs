﻿using CS.General_v3.Util;

namespace CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers
{
    using System;
    using System.Web;
    using System.Web.SessionState;
    using General_v3.HTTPHandlers;
    using CS.WebComponentsGeneralV3.Code.Util;
    
    /// <summary>
    /// Ovverride the Validate method and return an error string message.  Return NULL if OK!
    /// </summary>
    public abstract class CustomAJAXFieldValidationHandler : BaseJSONHandler
    {
        

      

        /// <summary>
        /// Validate the user request
        /// </summary>
        /// <param name="data">Data to validate</param>
        /// <returns>Error message (null if ok)</returns>
        protected abstract string Validate(string data);



        protected override void outputJsonVariables(HttpContext context)
        {
            string data = PageUtil.GetVariableFromQuerystring("data", context != null ? context.Request.QueryString : null);

            this.AddProperty("error", Validate(data));
        }
    }
}
