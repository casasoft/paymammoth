﻿using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;

namespace CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers
{
    using System.Web;
    using System.Web.SessionState;
    using CS.WebComponentsGeneralV3.Code.Util;

    public abstract class BaseHandler : IHttpHandler, IRequiresSessionState
    {
        #region IHttpHandler Members
        public bool RequiresSsl { get; set; }
        public bool RequiresSslEvenOnTestDevelopment { get; set; }
        public bool IsReusable { get { return false; } }

        protected abstract void processRequest(HttpContext ctx);


        private void createNhibernateSession()
        {
            NHClasses.NhManager.CreateNewSessionForContext();
        }

        private void disposeNhibernateSession()
        {
            if (NHClasses.NhManager.GetSessionFactory() != null)
            {
                NHClasses.NhManager.DisposeCurrentSessionInContext();
            }
        }
        public BaseHandler()
        {
            int k = 5;
        

        }

        
        
        private void _processRequest(HttpContext context)
        {
            createNhibernateSession();
            bool ok = true;
            PageUtil.checkFromHandlerWhetherBrowserSupportsCookies();
            if (RequiresSsl && 
                (!context.Request.IsSecureConnection))
            {
                if (!CS.General_v3.Util.Other.IsLocalTestingMachine ||
                    (CS.General_v3.Util.Other.IsLocalTestingMachine && RequiresSslEvenOnTestDevelopment))
                {

                    ok = false;
                }
            }
            if (ok)
            {
                processRequest(context);
            }
            //non SSL requests are not processed if requires ssl
            disposeNhibernateSession();
        }

        #endregion

        #region IHttpHandler Members


        void IHttpHandler.ProcessRequest(HttpContext context)
        {
            this._processRequest(context);

        }

        #endregion
    }
}
