﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CS.General_20090518.JavaScript.Data;
using System.Web.SessionState;
namespace CS.General_20090518.HTTPHandlers
{
    public abstract class BaseAjaxHandler : BaseHandler, IRequiresSessionState
    {
        private JSObject _jsObject;



        public BaseAjaxHandler()
        {
            _jsObject = new JSObject();
        }

        public object this[string index]
        {
            get
            {
                return _jsObject[index];
            }
            set
            {
                _jsObject[index] = value;
            }
        }

        public override void ProcessRequest(HttpContext context)
        {
            context.Response.Write(_jsObject.GetJS());
        }
    }
}
