﻿namespace CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers.Combiner
{
    using System;
    using System.Net;
    using System.IO;
    using System.Collections.Generic;
    using System.IO.Compression;
    using System.Text;
    using System.Web;
    using CS.General_v3;
    using CS.General_v3.Classes.JavascriptMinimizer;
    using CS.General_v3.Util;
    using log4net;
    using BusinessLogic_v3.Classes.NHibernateClasses;

    /// <summary>
    /// Combines files together. Querystring can include:
    /// 
    /// usecache = [yes/no] - Whether you want to use caching or not.  To use caching, it must also be enabled from the
    ///                       'settings.config' file, variable: HTTPCombiner_UseCaching
    ///                       
    /// get=[path], where path can contain multiple locations seperated by a comma ','.
    /// 
    /// Each seperate path can contain multiple files at the same folder, seperated by a |.
    /// 
    /// E.g  'get=/includes/js/karl.js|mark.js' is equivalent to 'get=/includes/js/karl.js,/includes.js/mark.js"
    /// 
    /// For relative paths, (local paths on server, you can even use say '*.js' to get all js files)
    /// 
    /// </summary>
    public class HttpCombiner : IHttpHandler
    {
        private ILog _log = log4net.LogManager.GetLogger(typeof(HttpCombiner));
        public const string VERSION = "1.0";
        private const bool DO_GZIP = true;
        private readonly static TimeSpan CACHE_DURATION = TimeSpan.FromDays(30);
        public bool UseCaching
        {
            get
            {
                bool b = (Enums.SETTINGS_ENUM.Others_HttpCombiner_UseCaching.GetSettingFromDatabase<bool>());
                if (b)
                {
                    bool rqCaching = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<bool?>("usecache").GetValueOrDefault(true);
                    if (!rqCaching)
                        b = rqCaching;
                }
                return b;
            }
        }
        private List<string> processFilePath(string filePaths)
        {
            filePaths = filePaths.Replace("\r", "");
            filePaths = filePaths.Replace("\n", "");
            filePaths = filePaths.Replace("\t", "");
            string[] filesTmp = filePaths.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            List<string> files = new List<string>();
            for (int i = 0; i < filesTmp.Length; i++)
            {
                string f = filesTmp[i];
                f = f.Trim();
                if (!string.IsNullOrEmpty(f))
                {
                    int lastSlash = f.LastIndexOf("/");

                    string fileName = f.Substring(lastSlash + 1);
                    string filePath = "";
                    if (lastSlash > -1)
                    {
                        filePath = f.Substring(0, lastSlash + 1);
                    }
                    string[] fileNames = fileName.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                    for (int j = 0; j < fileNames.Length; j++)
                    {
                        string fName = fileNames[j];
                        if (!string.IsNullOrEmpty(fName))
                        {
                            fName = fName.Replace("\r", "");
                            fName = fName.Replace("\n", "");
                            fName = fName.Replace("\t", "");
                            fName = fName.Trim();

                            int qIndex = fName.IndexOf("?");
                            if (qIndex != -1)
                            {
                                fName = fName.Substring(0, qIndex);
                            }

                            files.Add(filePath + fName);
                        }
                    }
                }

            }
            return files;
        }

        private System.Text.Encoding outputEncoding = null;
        private byte[] loadFiles(HttpContext context, bool isCompressed, List<string> files, out string contentType)
        {

            contentType = null;
            if (files.Count > 0)
            {
                contentType = Data.GetMIMEContentType(files[0]);
            }

            //string version = request["v"] ?? string.Empty;

            // Decide if browser supports compressed response

            //isCompressed = false;
            // Response is written as UTF8 encoding. If you are using languages like
            // Arabic, you should change this to proper encoding 
            System.Text.Encoding encoding = null;

            // If the set has already been cached, write the response directly from
            // cache. Otherwise generate the response and cache it
            byte[] responseBytes = null;
            if (UseCaching)
            {
                responseBytes = this.WriteFromCache(context, files, VERSION, isCompressed, contentType);
            }
            if (responseBytes == null)
            {
                ///{
                ///
                MemoryStream memoryStream = new MemoryStream(8192 * 4);



                // Decide regular stream or GZipStream based on whether the response
                // can be cached or not
                Stream writer = isCompressed ? (Stream)(new GZipStream(memoryStream, CompressionMode.Compress)) : memoryStream;

                StringBuilder sb = new StringBuilder();
                foreach (var file in files)
                {
                    try
                    {
                        System.Text.Encoding fileEnc = null;
                        string fileStr = this.GetFileStr(context, file.Trim(), out fileEnc);
                        sb.AppendLine(fileStr);
                        sb.AppendLine();

                        //byte[] fileBytes = this.GetFileBytes(context, file.Trim(), encoding);
                        //writer.Write(fileBytes, 0, fileBytes.Length);
                    }
                    catch (Exception ex)
                    {
                        _log.Debug("HttpCombiner Error: " + ex.Message);
                    }
                }
                /*
                //pad these to the end as sometimes end characters are being lost
                writer.WriteByte((byte)'\r'); writer.WriteByte((byte)'\n'); writer.WriteByte((byte)'\r'); writer.WriteByte((byte)'\n');
                writer.WriteByte((byte)'\r'); writer.WriteByte((byte)'\n'); writer.WriteByte((byte)'\r'); writer.WriteByte((byte)'\n');
                writer.WriteByte((byte)'\r'); writer.WriteByte((byte)'\n'); writer.WriteByte((byte)'\r'); writer.WriteByte((byte)'\n');
                */

                string js = sb.ToString();

                if (false && Enums.SETTINGS_ENUM.Others_MinimiseJavascript.GetSettingFromDatabase<bool>())
                {
                    CS.General_v3.Classes.JavascriptMinimizer.JsMin jsMin = new JsMin();
                    js = jsMin.MinimiseFromString(js);

                    js += CS.General_v3.Util.Text.RepeatText(" ", 500); // add padding as sometimes they get cuttoff
                }
                //js += "\r\n\r\n/* TEST */\r\n\r\n";
                byte[] by = outputEncoding.GetBytes(js);
                responseBytes = by;
                writer.Write(by, 0, by.Length);
                writer.Flush();


                memoryStream.Seek(0, SeekOrigin.Begin);
                responseBytes = new byte[memoryStream.Length];
                int readBytes = memoryStream.Read(responseBytes, 0, responseBytes.Length);

                writer.Dispose();
                writer.Close();
                memoryStream.Close();
                writer.Dispose();
                memoryStream.Dispose();
                if (UseCaching)
                {
                    saveInCache(responseBytes, files, context, isCompressed);
                }
            }
            return responseBytes;
        }
        private void saveInCache(byte[] responseBytes, List<string> files, HttpContext context, bool isCompressed)
        {
            string cacheKey = GetCacheKey(files, VERSION, isCompressed);

            context.Cache.Insert(cacheKey, responseBytes, null,
                System.Web.Caching.Cache.NoAbsoluteExpiration, CACHE_DURATION);
        }
        public void ProcessRequest(HttpContext context)
        {
            NHClasses.NhManager.CreateNewSessionForContext();

            if (outputEncoding == null)
                outputEncoding = System.Text.Encoding.UTF8;
            HttpRequest request = context.Request;

            // Read setName, contentType and version. All are required. They are
            // used as cache key
            string filePaths = request["get"] ?? string.Empty;
            List<string> files = processFilePath(filePaths);
            // files.Clear();
            // files.Add("/includes/testjs/1.js");
            // files.Add("/includes/testjs/2.js");
            if (files.Count > 0)
            {
                string contentType = "";
                bool isCompressed = DO_GZIP && this.CanGZip(context.Request);
                //isCompressed = false;
                //isCompressed = false;

                byte[] responseBytes = loadFiles(context, isCompressed, files, out contentType);

                // Cache the combined response so that it can be directly written
                // in subsequent calls 
                //StreamReader tmpSR = new StreamReader(memoryStream);
                //string all = tmpSR.ReadToEnd();
                //tmpSR.Close();
                //byte[] responseBytes = memoryStream.ToArray();
                /*  context.Cache.Insert(GetCacheKey(setName, version, isCompressed),
                      responseBytes, null, System.Web.Caching.Cache.NoAbsoluteExpiration,
                      CACHE_DURATION);*/

                // Generate the response
                this.WriteBytes(responseBytes, context, isCompressed, contentType);

                //}
            }

            NHClasses.NhManager.DisposeCurrentSessionInContext();
        }

        private byte[] GetFileBytes(HttpContext context, string virtualPath)
        {
            if (virtualPath.StartsWith("http://", StringComparison.InvariantCultureIgnoreCase) || virtualPath.StartsWith("https://", StringComparison.InvariantCultureIgnoreCase))
            {
                using (WebClient client = new WebClient())
                {
                    byte[] by = new byte[0];
                    try
                    {
                        by = client.DownloadData(virtualPath);
                    }
                    catch
                    {
                        by = new byte[0];
                    }

                    return by;
                }
            }
            else
            {

                string folder = virtualPath.Substring(0, virtualPath.LastIndexOf("/"));
                string physicalFolder = context.Server.MapPath(folder);

                string fileName = virtualPath.Substring(virtualPath.LastIndexOf("/") + 1);
                if (Directory.Exists(physicalFolder))
                {
                    DirectoryInfo dir = new DirectoryInfo(physicalFolder);
                    MemoryStream ms = new MemoryStream(8192 * 4);
                    FileInfo[] files = dir.GetFiles(fileName);
                    foreach (var file in files)
                    {
                        try
                        {
                            byte[] bytes = File.ReadAllBytes(file.FullName);
                            ms.Write(bytes, 0, bytes.Length);

                            /*
                            string physicalPath = context.Server.MapPath(virtualPath);
                            byte[] bytes = File.ReadAllBytes(physicalPath);
                            // Convert unicode files to specified encoding. For now, assuming
                            // files are either ASCII or UTF8
                            return bytes;*/
                        }
                        catch
                        { }
                    }

                    ms.Seek(0, SeekOrigin.Begin);

                    byte[] byAll = new byte[ms.Length];
                    int read = ms.Read(byAll, 0, byAll.Length);
                    ms.Dispose();
                    return byAll;

                }
                else
                {
                    byte[] byAll = new byte[0];
                    return byAll;
                }


            }
        }
        private string GetFileStr(HttpContext context, string virtualPath, out System.Text.Encoding encoding)
        {
            byte[] by = GetFileBytes(context, virtualPath);
            outputEncoding = Encoding.UTF8;

            System.Text.Encoding currEncoding = CS.General_v3.Util.IO.GetFileTextEncoding(by);
            //currEncoding = Encoding.ASCII;
            encoding = outputEncoding;
            if (currEncoding.WebName != encoding.WebName)
            {
                by = System.Text.Encoding.Convert(currEncoding, encoding, by);
            }


            string s = encoding.GetString(by);
            return s;
        }

        private byte[] WriteFromCache(HttpContext context, List<string> files, string version,
            bool isCompressed, string contentType)
        {
            byte[] responseBytes = context.Cache[GetCacheKey(files, version, isCompressed)] as byte[];

            if (null == responseBytes || 0 == responseBytes.Length)
            {
                return null;

            }
            else
            {
                return responseBytes;
            }
        }

        private void WriteBytes(byte[] bytes, HttpContext context,
            bool isCompressed, string contentType)
        {
            if (bytes.Length > 0)
            {
                try
                {
                    HttpResponse response = context.Response;

                    response.AppendHeader("Content-Length", bytes.Length.ToString());

                    response.ContentType = contentType + "; charset=" + outputEncoding.WebName;
                    //response.ContentType = contentType;
                    if (isCompressed)
                        response.AppendHeader("Content-Encoding", "gzip");

                    context.Response.Cache.SetCacheability(HttpCacheability.Public);
                    context.Response.Cache.SetExpires(CS.General_v3.Util.Date.Now.Add(CACHE_DURATION));
                    context.Response.Cache.SetMaxAge(CACHE_DURATION);
                    context.Response.Cache.AppendCacheExtension("must-revalidate, proxy-revalidate");


                    response.OutputStream.Write(bytes, 0, bytes.Length);
                    response.Flush();
                }
                catch (HttpException ex)
                {
                    List<string> allowedMsgs = new List<string>();
                    allowedMsgs.Add("The remote host closed the connection");
                    bool errorOK = false;
                    foreach (var s in allowedMsgs)
                    {
                        if (ex.Message.ToLower().Contains(s.ToLower()))
                        {
                            errorOK = true;
                        }
                    }
                    if (!errorOK)
                        throw ex;
                }
            }

        }

        private bool CanGZip(HttpRequest request)
        {
            string acceptEncoding = request.Headers["Accept-Encoding"];
            string browserType = HttpContext.Current.Request.Browser.Type.ToLower();

            if (!browserType.Contains("ie6") && !string.IsNullOrEmpty(acceptEncoding) &&
                 (acceptEncoding.Contains("gzip") || acceptEncoding.Contains("deflate")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string GetCacheKey(List<string> files, string version, bool isCompressed)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < files.Count; i++)
            {
                if (i > 0)
                    sb.Append("|");
                sb.Append(files[i]);
            }
            string setName = sb.ToString();
            return "HttpCombiner." + setName + "." + version + "." + isCompressed;
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

    }
}
