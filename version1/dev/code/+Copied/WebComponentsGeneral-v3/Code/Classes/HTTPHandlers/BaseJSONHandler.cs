﻿namespace CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers
{
    using System.Collections.Generic;
    using System.Web;
    using System.Web.SessionState;
    using CS.General_v3.Util;

    public abstract class BaseJSONHandler : __BaseJSONHandler
    {
        protected new void processRequest(HttpContext context)
        {

        }


    }
    public abstract class __BaseJSONHandler : BaseHandler, IRequiresSessionState
    {
        private Dictionary<string, object> _dictionary = new Dictionary<string, object>();


        public void AddProperty(string key, object value)
        {
            _dictionary[key] = value;
        }

        public object this[string key]
        {
            get
            {
                return _dictionary[key];
            }
            set { _dictionary[key] = value; }
        }
        /// <summary>
        /// Return the required JSOn variables to be outputted
        /// </summary>
        protected abstract void outputJsonVariables(HttpContext context);

        protected override void processRequest(HttpContext context)
        {
            outputJsonVariables(context);
            string js = JSONUtil.Serialize(_dictionary);

            context.Response.Write(js);
        }

    }

}
