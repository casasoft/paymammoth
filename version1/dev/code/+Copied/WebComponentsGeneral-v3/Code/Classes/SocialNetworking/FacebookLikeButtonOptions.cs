﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Classes.SocialNetworking
{
    public class FacebookLikeButtonOptions
    {
        public string href;
        public bool? send;
        public CS.General_v3.Enums.FACEBOOK_LAYOUT_STYLE layout;
        public bool? show_faces;
        public int? width;
        public CS.General_v3.Enums.FACEBOOK_VERB action;
        public CS.General_v3.Enums.FACEBOOK_FONT font;
        public CS.General_v3.Enums.FACEBOOK_COLOUR_SCHEME colorscheme;
        public string _ref;
    }
}