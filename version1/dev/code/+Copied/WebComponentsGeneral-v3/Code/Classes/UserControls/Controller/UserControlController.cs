﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Classes.UserControls.Controller
{
    public class UserControlController
    {
        public static UserControlController Instance
        {
            get {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstance<UserControlController>();
            }
        }
        private UserControlController()
        {

        }


    }
}