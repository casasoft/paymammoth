﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Classes.Exceptions
{
    public class TestErrorException : Exception
    {
        public TestErrorException(string msg)
            : base(msg)
        {

        }
    }
}