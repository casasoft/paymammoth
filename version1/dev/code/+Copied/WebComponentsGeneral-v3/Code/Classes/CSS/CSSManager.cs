﻿namespace CS.WebComponentsGeneralV3.Code.Classes.CSS
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;

    public class CSSManager
    {

        public CSSManager(Control ctrl)
        {
            if (ctrl is HtmlControl)
            {
                initHtmlControl((HtmlControl)ctrl);
            }
            else if (ctrl is WebControl)
            {
                initWebControl((WebControl)ctrl);
            }
            else
            {
                throw new NotSupportedException("Control cannot be of type  " + ctrl);
            }
        }

        public CSSManager(HtmlControl ctrl)
        {
           initHtmlControl(ctrl);
        }
        public CSSManager(WebControl ctrl)
        {
           initWebControl(ctrl);
        }
        public CSSManager(string cssClass = null)
        {
            
           // init();
            this.AddClass(cssClass);
        }

        private void initHtmlControl(HtmlControl ctrl)
        {
             _htmlControl = ctrl;
            string css = ctrl.Attributes["class"];
            this.AddClass(css);
        }
        private void initWebControl(WebControl ctrl)
        {
             _webControl = ctrl;
            string css = ctrl.CssClass;
            this.AddClass(css);
        }
        private HtmlControl _htmlControl = null;
        private WebControl _webControl = null;



        private void checkClasses()
        {
            if (_htmlControl != null)
                _htmlControl.Attributes["class"] = this.ToString();
            if (_webControl != null)
                _webControl.CssClass = this.ToString();
        }

        private List<string> _cssClasses = new List<string>();

        public virtual void AddClass(params string[] cssClasses)
        {
            for (int i = 0; i < cssClasses.Length; i++)
            {
                if (!string.IsNullOrEmpty(cssClasses[i]))
                {
                    string cssClass = cssClasses[i].Trim();
                    //A class can contain multiple classes
                    string[] classes2 = cssClass.Split(' ');
                    for (int j = 0; j < classes2.Length; j++)
                    {
                        string cssClass2 = classes2[j].Trim();
                        if (!String.IsNullOrEmpty(cssClass2) && !_cssClasses.Contains(cssClass2))
                        {
                            _cssClasses.Add(cssClass2);
                        }
                    }
                }

            }
            checkClasses();
        }

        
        public virtual void RemoveClass(params string[] cssClasses)
        {
            for (int i = 0; i < cssClasses.Length; i++)
            {
                string cssClass = cssClasses[i].Trim();
                //A class can contain multiple classes
                string[] classes2 = cssClass.Split(' ');
                for (int j = 0; j < classes2.Length; j++)
                {
                    string cssClass2 = classes2[j].Trim();
                    if (!String.IsNullOrEmpty(cssClass2))
                    {
                        _cssClasses.Remove(cssClass2);
                    }
                }
            }
            checkClasses();
        }
        public virtual void Clear()
        {
            _cssClasses.Clear();
            checkClasses();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _cssClasses.Count; i++)
            {
                if (i > 0)
                {
                    sb.Append(" ");
                }
                sb.Append(_cssClasses[i]);
            }
            return sb.ToString();
        }

        public bool Contains(string cssClass)
        {
            return _cssClasses.Contains(cssClass);
        }


        public static CSSManager operator +(CSSManager manager, string cssClass)
        {
            string[] cssClasses = cssClass.Split(' ');

            manager.AddClass(cssClasses);
            return manager;
        }
        public static CSSManager operator -(CSSManager manager, string cssClass)
        {
            string[] cssClasses = cssClass.Split(' ');

            manager.RemoveClass(cssClasses);
            return manager;
        }
         public void PrependCssClasses(string prefix)
        {
            for (int i = 0; i < _cssClasses.Count; i++)
            {
                _cssClasses[i] = prefix + _cssClasses[i];
            }
        }
         public void AppendCssClasses(string suffix)
         {
             for (int i = 0; i < _cssClasses.Count; i++)
             {
                 _cssClasses[i] += suffix;
             }
         }
    }
}
