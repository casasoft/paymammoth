﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Modules.SettingModule;

namespace CS.WebComponentsGeneralV3.Code.Classes.Contact
{
    public class ContactDetailsData
    {
        public BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS Identifier { get; set; }
        public ContentTextBaseFrontend Label { get; set; }
        public string CssClass { get; set; }
        public Control Value { get; set; }
        public int Priority { get; set; }
        public ISettingBase SettingData { get; set; }
    }
}