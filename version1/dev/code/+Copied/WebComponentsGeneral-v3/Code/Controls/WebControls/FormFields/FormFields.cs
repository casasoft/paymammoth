﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields
{
    using Common.Fields;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.MultiSelect;
    using System.Web.UI.WebControls;
    using System.Web.UI;
using CS.WebComponentsGeneralV3.Code.Classes.CSS;
    using CS.General_v3;
using BusinessLogic_v3.Frontend.ContentTextModule;

    public class FormFields : MyDiv
    {


        private Dictionary<string, FormFieldsItemGroupData> _itemGroups = new Dictionary<string, FormFieldsItemGroupData>();

        private const string DEFAULT_GROUP_ID = "__default";
        public const string DEFAULT_VALIDATION_GROUP = "main";



        public class FUNCTIONALITY
        {
            private FormFields _control;

            public IFormFieldsLayout LayoutProvider { get; set; }

            public string ValidationGroup { get; set; }

            public event EventHandler ClickSubmit;
            public event EventHandler ClickCancel;
            public string ButtonSubmitText { get; set; }
            public string ButtonCancelText { get; set; }
            public MyButton ButtonSubmit { get; set; }
            public MyButton ButtonCancel { get; set; }

            /// <summary>
            /// Setting classes to this css manager will be set to all groups
            /// </summary>
            public CSSManager CssManager { get; set; }


            public FUNCTIONALITY(FormFields control)
            {
                this.CssManager = new CSSManager();
                _control = control;
                this.ButtonSubmitText = "Submit";
                this.ValidationGroup = DEFAULT_VALIDATION_GROUP;
                this.LayoutProvider = new FormFieldsLayoutTable();
                
                this.ButtonSubmit = new MyButton();
                this.ButtonCancel = new MyButton();
            }

           

            public Dictionary<string, FormFieldsItemGroupData> ItemGroups
            {
                get { return _control._itemGroups; }
            }

            public FormFieldsItemGroupData GetGroup(string group = null)
            {
                if (string.IsNullOrEmpty(group)) group = DEFAULT_GROUP_ID;

                if (!ItemGroups.ContainsKey(group))
                {
                     var g = new FormFieldsItemGroupData();
                    g.Identifier = group;
                    if (string.IsNullOrEmpty(g.Title) && group != DEFAULT_GROUP_ID)
                    {
                        g.Title = group;
                    }
                    
                    
                    g.ValidationGroup = this.ValidationGroup;
                    ItemGroups[group] = g;
                }
                return ItemGroups[group];
            }
            public FormFieldsItemDataImpl AddGenericItem(FormFieldsItemDataImpl item, string group = null)
            {
                AddGenericItem((IFormFieldsItemData)item, group);
                return item;
            }
            public FormFieldsItemDataImpl AddGenericLabelAndControl(string title, bool required, Control c, string group = null)
            {
                FormFieldsItemDataImpl item = null;
                if (c is IMyFormWebControl) {
                    item = new FormFieldsFieldItemDataImpl((IMyFormWebControl)c);
                }
                else {
                    item = new FormFieldsItemDataImpl() { Control = c};
                }
                
                item.Title = title;
                item.Required = required;
                
                return AddGenericItem(item, group);

            }
            public void AddGenericItem(IFormFieldsItemData item, FormFieldsItemGroupData fieldGroup = null)
            {



                fieldGroup.AddItem(item);
            }
            public void AddGenericItem(IFormFieldsItemData item, string group = null)
            {
                var fieldGroup = GetGroup(group);



                AddGenericItem(item, fieldGroup);
            }
            public T AddFieldItem<T>(T control, string group = null, FormFieldsLayoutTypeInformation layoutInfo = null, string subtitle = null) where T : IMyFormWebControl
            {
                IFormFieldsItemData itemData = null;
                return (T)AddFieldItem(control, out itemData, group, layoutInfo, subtitle);
            }

            public T AddFieldItem<T>(T control, out IFormFieldsItemData formFieldItemData, string group = null, FormFieldsLayoutTypeInformation layoutInfo = null, string subtitle = null) where T : IMyFormWebControl
            {
                FormFieldsFieldItemDataImpl item = FormFieldsFieldItemDataImpl.GetFromMyFormWebControl(control, layoutInfo, subtitle);
                
                AddGenericItem(item, group);
                formFieldItemData = item;
                return control;
            }
            private IMyFormWebControl addFormWebControl(IMyFormWebControl control, string id, string title, bool required, string helpMessage = null, string group = null, FormFieldsLayoutTypeInformation layoutInfo = null, object initialValue = null, string subtitle = null, ContentTextBaseFrontend helpMessageContentText = null)
            {
                control.FieldParameters.id = id;
                control.FieldParameters.title = title;
                control.FieldParameters.required = required;
                control.FieldParameters.helpMessage = helpMessage;
                control.FieldParameters.helpMessageContentText = helpMessageContentText;
                control.WebControlFunctionality.InitialValue = initialValue;
                return this.AddFieldItem(control, group, layoutInfo, subtitle);
            }

            public MyFileUpload AddFileUpload(string id, string title, bool required, string helpMessage = null, string group = null, FormFieldsLayoutTypeInformation layoutInfo = null)
            {
                MyFileUpload f = new MyFileUpload(new FieldSingleControlParameters());
                addFormWebControl(f, id, title, required, helpMessage, group, layoutInfo);
                return f;
            }
            public MyTxtBoxString AddString(string id, string title, bool required, string helpMessage = null, string group = null, string initialValue = null, FormFieldsLayoutTypeInformation layoutInfo = null)
            {
                MyTxtBoxString f = new MyTxtBoxString(new FieldSingleControlParameters());
                addFormWebControl(f, id, title, required, helpMessage, group, layoutInfo, initialValue);
                return f;
            }

            public MyTxtBoxDate AddDateTime(string id, string title, bool required, string helpMessage = null, string group = null, DateTime? initialValue = null, FormFieldsLayoutTypeInformation layoutInfo = null, string subtitle = null)
            {
                MyTxtBoxDate f = new MyTxtBoxDate(new FieldSingleControlDateParameters());
                addFormWebControl(f, id, title, required, helpMessage, group, layoutInfo, initialValue, subtitle);
                return f;
            }
            public MyTxtBoxNumber AddInteger(string id, string title, bool required, string helpMessage = null, string group = null, int? initialValue = null, FormFieldsLayoutTypeInformation layoutInfo = null)
            {
                MyTxtBoxNumber f = new MyTxtBoxNumber(new FieldSingleControlParameters());
                f.ValidatorNumericParameters.integersOnly = true;
                addFormWebControl(f, id, title, required, helpMessage, group, layoutInfo, initialValue);
                return f;
            }
            public MyTxtBoxNumber AddDouble(string id, string title, bool required, string helpMessage = null, string group = null, double? initialValue = null, FormFieldsLayoutTypeInformation layoutInfo = null)
            {
                MyTxtBoxNumber f = new MyTxtBoxNumber(new FieldSingleControlParameters());
                addFormWebControl(f, id, title, required, helpMessage, group, layoutInfo, initialValue);
                return f;
            }
            public MyTxtBoxStringMultiLine AddStringMultiline(string id, string title, bool required, string helpMessage = null, string group = null, string initialValue = null, FormFieldsLayoutTypeInformation layoutInfo = null)
            {
                MyTxtBoxStringMultiLine f = new MyTxtBoxStringMultiLine(new FieldSingleControlParameters());
                addFormWebControl(f, id, title, required, helpMessage, group, layoutInfo, initialValue);
                return f;
            }
            public MyTxtBoxEmail AddEmail(string id, string title, bool required, string helpMessage = null, string group = null, string initialValue = null, FormFieldsLayoutTypeInformation layoutInfo = null)
            {
                MyTxtBoxEmail f = new MyTxtBoxEmail(new FieldSingleControlParameters());
                addFormWebControl(f, id, title, required, helpMessage, group, layoutInfo, initialValue);
                return f;
            }
            public MyListBox AddMultipleChoiceList(string id, string title, bool required, MyDropDownListItems items, _jQueryMultiSelectParams jQueryMultiSelectParams = null, string helpMessage = null, string group = null, IEnumerable<string> selectedValues = null, FormFieldsLayoutTypeInformation layoutInfo = null)
            {
                MyListBox listBox = new MyListBox(new FieldSingleControlListBoxParameters());
                addFormWebControl(listBox, id, title, required, helpMessage, group, layoutInfo, selectedValues);
                listBox.Functionality.Items = items;
                listBox.FieldParameters.jQueryMultiSelectParams = jQueryMultiSelectParams;
                return listBox;
               
            }
            public MyDropDownList AddSingleChoiceList(string id, string title, bool required, MyDropDownListItems items, string helpMessage = null, string group = null, string selectedValue = null, FormFieldsLayoutTypeInformation layoutInfo = null)
            {
                MyDropDownList cmb = new MyDropDownList(new FieldSingleControlParameters());

                if (!required)
                {
                    bool found = false;
                    foreach (var item in items)
                    {
                        if (string.IsNullOrEmpty(item.Value))
                        {
                            found = true;
                            break;
                        }

                    }
                    if (!found)
                    {
                        MyDropDownOption opt = new MyDropDownOption();
                        items.Insert(0, opt);
                    }
                }

                addFormWebControl(cmb, id, title, required, helpMessage, group, layoutInfo, selectedValue);
                cmb.Functionality.Items = items;
                return cmb;

            }

            public MyDropDownList AddSingleChoiceListFromEnum<TEnumType>(string id, string title, bool required, string helpMessage = null, string group = null, TEnumType? selectedValue = null, FormFieldsLayoutTypeInformation layoutInfo = null, ListItem blankItemOnTop = null,  Enums.ENUM_SORT_BY sortBy =  Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false) where TEnumType: struct, IConvertible
            {
                if (!required && blankItemOnTop == null)
                {
                    ListItem blankItem = new ListItem();
                    blankItemOnTop = blankItem;
                }

                var items = BusinessLogic_v3.Util.EnumUtils.GetListItemCollectionFromEnumAndAddContentTexts<TEnumType>(blankItemOnTop, sortBy, addSpacesToCamelCasedName, selectedValue);

                return this.AddSingleChoiceList(id, title, required, new MyDropDownListItems(items), helpMessage, group, null, layoutInfo);
            }
            public MyListBox AddMultipleChoiceListFromEnum<TEnum>(
                string id,
                string title,
                bool required,
                
                _jQueryMultiSelectParams jQueryMultiSelectParams = null,
                string helpMessage = null,
                string group = null,
                IEnumerable<TEnum> selectedValues = null,
                FormFieldsLayoutTypeInformation layoutInfo = null,
                CS.General_v3.Enums.ENUM_SORT_BY enumValuesSortBy = General_v3.Enums.ENUM_SORT_BY.NameAscending,
                bool addSpacesToCamelCasedEnums = true,
                bool addContentTexts = false) where TEnum : struct, IConvertible
            {
                string blankValue = null;

                ListItemCollection listColl = null;
                if (addContentTexts)
                {
                    listColl = BusinessLogic_v3.Util.EnumUtils.GetListItemCollectionFromEnumAndAddContentTexts(typeof(TEnum), blankValue, enumValuesSortBy, addSpacesToCamelCasedEnums);
                }
                else
                {
                    listColl = CS.General_v3.Util.Data.GetListItemCollectionFromEnum<TEnum>();
                }

                MyDropDownListItems listItems = new MyDropDownListItems();
                foreach (ListItem item in listColl)
                {
                    var listItem = listItems.AddOption(item);
                    if (selectedValues != null)
                    {
                        var itemValue = CS.General_v3.Util.EnumUtils.GetEnumByStringValueNullable<TEnum>(item.Value);
                        if (itemValue.HasValue && selectedValues.Contains(itemValue.Value))
                        {
                            listItem.Functionality.Selected = true;
                        }
                    }
                }
                return AddMultipleChoiceList(id, title, required, listItems, jQueryMultiSelectParams, helpMessage, group, null, layoutInfo);
            }

            public MyTxtBoxStringPassword AddStringPassword(string id, string title, bool required, string helpMessage = null, string group = null, FormFieldsLayoutTypeInformation layoutInfo = null)
            {
                MyTxtBoxStringPassword f = new MyTxtBoxStringPassword(new FieldSingleControlPasswordParameters());
                addFormWebControl(f, id, title, required, helpMessage, group, layoutInfo);
                return f;
            }
            public MyCheckBox AddBool(string id, string title, bool required, string helpMessage = null, string group = null, FormFieldsLayoutTypeInformation layoutInfo = null, bool initialValue = false)
            {
                MyCheckBox f = new MyCheckBox(new FieldSingleControlParameters());
                addFormWebControl(f, id, title, required, helpMessage, group, layoutInfo);
                f.WebControlFunctionality.InitialValue = initialValue;
                return f;
            }
            public MyDropDownList AddBoolNullable(string id, string title, string helpMessage = null, string group = null, FormFieldsLayoutTypeInformation layoutInfo = null)
            {
                MyDropDownListItems listItems = new MyDropDownListItems();
                listItems.Add(new MyDropDownOption("",""));
                listItems.Add(new MyDropDownOption(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Yes).GetContent(),"Y"));
                listItems.Add(new MyDropDownOption(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_No).GetContent(), "N"));

                

                return AddSingleChoiceList(id, title, false, listItems, helpMessage, group, null, layoutInfo);
                
            }
            public FormFieldsItemGroupData AddGroup(FormFieldsItemGroupData groupData)
            {
                ItemGroups[groupData.Identifier] = groupData;
                return groupData;
            }
            private void updateButtonForGroup(MyButton btn, bool isDefaultButton, FormFieldsItemGroupData group)
            {
                // btn.ID = "btn" + this.ValidationGroup + "_" + fieldGroup.Identifier;
                btn.WebControlFunctionality.ButtonParams.defaultButton = isDefaultButton;
                btn.ValidationGroup = group.ValidationGroup;
                
                btn.CssManager.AddClass("form-fields-button");
                if (btn.WebControlFunctionality.ButtonParams.defaultButton)
                {
                    btn.CssManager.AddClass("form-fields-button-default");
                }
            }
            public MyButton AddButtonForGroup(MyButton btn, FormFieldsItemGroupData fieldGroup)
            {
                updateButtonForGroup(btn, !fieldGroup.HasButton(), fieldGroup);
                FormFieldsItemDataImpl item = new FormFieldsItemDataImpl()
                {
                    Control = btn,
                    LayoutInfo = new FormFieldsLayoutTypeInformation()
                    {
                        //BlockElement = true
                    }
                };
                AddGenericItem(item, fieldGroup);
                return btn;
            }
            public MyButton AddButton(MyButton btn, string group = null)
            {
                var fieldGroup = GetGroup(group);
                updateButtonForGroup(btn, !fieldGroup.HasButton(), fieldGroup);
                FormFieldsItemDataImpl item = new FormFieldsItemDataImpl()
                {
                    Control = btn,
                    LayoutInfo = new FormFieldsLayoutTypeInformation()
                    {
                        //BlockElement = true
                    }
                };
                AddGenericItem(item, group);
                return btn;
            }
            public FormFieldsItemDataImpl AddButtons(List<MyButton> btns, string group = null)
            {
                MyDiv divButtons = new MyDiv();
                divButtons.CssManager.AddClass("form-fields-button-group");
                var fieldGroup = GetGroup(group);
                bool isDefaultButton = !fieldGroup.HasButton();
                foreach (var btn in btns)
                {
                    updateButtonForGroup(btn, isDefaultButton, fieldGroup);
                    divButtons.Controls.Add(btn);
                    isDefaultButton = false;
                }
                FormFieldsItemDataImpl item = new FormFieldsItemDataImpl()
                {
                    Control = divButtons,
                    CssClass = "form-fields-button-group-container",
                    LayoutInfo = new FormFieldsLayoutTypeInformation()
                    {
                        //BlockElement = true
                    }
                };
                AddGenericItem(item, group);
                return item;
            }
            public MyButton AddButton(string title, string group = null)
            {
                
                MyButton btn = new MyButton();
                AddButton(btn, group);
                btn.Text = title;
                return btn;
            }
            public MyButton AddButton(ContentTextBaseFrontend title, string group = null)
            {

                MyButton btn = new MyButton();
                AddButton(btn, group);
                btn.TextContentText = title;
                return btn;
            }
            public MyButton AddButton(string title, FormFieldsItemGroupData group)
            {

                MyButton btn = new MyButton();
                AddButtonForGroup(btn, group);
                btn.Text = title;
                return btn;
            }
            public MyButton AddButton(ContentTextBaseFrontend title, FormFieldsItemGroupData group)
            {

                MyButton btn = new MyButton();
                AddButtonForGroup(btn, group);
                btn.TextContentText = title;
                return btn;
            }
            internal bool hasGotClickSubmitEventAttached()
            {
                return this.ClickSubmit != null;
            }
            internal bool hasGotClickCancelEventAttached()
            {
                return this.ClickCancel != null;
            }
            internal void triggerClickCancel()
            {
                if (this.ClickCancel != null)
                {
                    this.ClickCancel(_control, null);
                }
            }
            internal void triggerClickSubmit()
            {
                if (this.ClickSubmit != null)
                {
                    this.ClickSubmit(_control, null);
                }
            }

            public MyFormWebControl AddFieldByType(Type dataType, string id, string title, string helpMessage = null, string group = null, object initialValue = null)
            {
                MyFormWebControl input = null;
                var enumDataType = CS.General_v3.Enums.GetDataTypeFromType(dataType);
                bool required = (CS.General_v3.Util.ReflectionUtil.CheckIfTypeAllowsNullValue(dataType)) == false;
                switch (enumDataType)
                {
                    case Enums.DATA_TYPE.Bool:
                        {

                            input = this.AddBool(id, title, required, helpMessage, group);
                            break;
                        }
                    case Enums.DATA_TYPE.BoolNullable:
                        {

                            input = this.AddBoolNullable(id, title, helpMessage, group);
                            break;
                        }
                    case Enums.DATA_TYPE.DateTime:
                    case Enums.DATA_TYPE.DateTimeNullable:
                        {

                            input = this.AddDateTime(id, title, required, helpMessage, group, (DateTime?)initialValue);
                            break;
                        }
                    case Enums.DATA_TYPE.Double:
                    case Enums.DATA_TYPE.DoubleNullable:
                        {

                            input = this.AddDouble(id, title, required, helpMessage, group, (double?)initialValue);
                            break;
                        }
                    case Enums.DATA_TYPE.Integer:
                    case Enums.DATA_TYPE.IntegerNullable:
                    case Enums.DATA_TYPE.Long:
                    case Enums.DATA_TYPE.LongNullable:
                        {

                            input = this.AddInteger(id, title, required, helpMessage, group, (int?)initialValue);
                            break;

                        }
                    case Enums.DATA_TYPE.String:
                    
                        {

                            input = this.AddString(id, title, required, helpMessage, group, (string)initialValue);
                            break;
                        }
                    default:
                        throw new InvalidOperationException("Data type not mapped <" + enumDataType + ">");
                }

                return input;
            }
        }

        public FUNCTIONALITY Functionality { get; private set; }

       
        public FormFields()
        {
            this.Functionality = getFunctionality();
            this.CssManager.AddClass("formfields-core-container");
        }
        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        private bool checkUpdateValidationGroupField(IFormFieldsItemData item, FormFieldsItemGroupData group)
        {
            IMyFormWebControl formControl = null;
            if (item is IMyFormWebControl)
            {
                formControl = (IMyFormWebControl)item;
            }
            else if (item.Control is IMyFormWebControl)
            {
                formControl = (IMyFormWebControl)item.Control;
            }

            //Set validation group
            if (formControl != null)
            {
                if (!string.IsNullOrEmpty(group.ValidationGroup))
                {
                    //Set validation group
                    formControl.FieldParameters.validationGroup = group.ValidationGroup;
                }
                else
                {
                    formControl.FieldParameters.validationGroup = this.Functionality.ValidationGroup;
                }
                
            }
            return formControl != null;
        }
        private bool checkUpdateValidationGroupButton(IFormFieldsItemData item, FormFieldsItemGroupData group)
        {
            IBaseButtonWebControl btnControl = null;
            if (item is IBaseButtonWebControl)
            {
                btnControl = (IBaseButtonWebControl)item;
            }
            else if (item.Control is IBaseButtonWebControl)
            {
                btnControl = (IBaseButtonWebControl)item.Control;
            }

            //Set validation group
            if (btnControl != null)
            {
                if (!string.IsNullOrEmpty(group.ValidationGroup))
                {
                    //Set validation group
                    btnControl.WebControlFunctionality.ButtonParams.validationGroup = group.ValidationGroup;
                }
                else
                {
                    btnControl.WebControlFunctionality.ButtonParams.validationGroup = this.Functionality.ValidationGroup;
                }
            }
            return btnControl != null;
        }

        /// <summary>
        /// Update the validation group of each field within the form fields
        /// </summary>
        private void updateValidationGroups()
        {
            foreach (var group in this.Functionality.ItemGroups.Values)
            {
                foreach (var item in group.ChildItems)
                {
                    bool found = checkUpdateValidationGroupField(item, group);
                    if (!found) found = checkUpdateValidationGroupButton(item, group);
                }
            }
            
        }
        protected void addSubmitButtonFromEvent()
        {
            if (this.Functionality.hasGotClickSubmitEventAttached())
            {
                if (!string.IsNullOrEmpty(this.Functionality.ButtonSubmitText))
                {
                    this.Functionality.ButtonSubmit.Text = this.Functionality.ButtonSubmitText;
                }

                string group = null;
                if (this.Functionality.ItemGroups.Keys.Count > 0)
                {
                    group = this.Functionality.ItemGroups[this.Functionality.ItemGroups.Keys.Last()].Identifier;
                }

                MyButton btnSubmit = this.Functionality.AddButton(this.Functionality.ButtonSubmit, group);
                btnSubmit.Click += new EventHandler(btnSubmit_Click);
            }
        }
        protected void addCancelButtonFromEvent()
        {
            if (this.Functionality.hasGotClickCancelEventAttached())
            {
                if (!string.IsNullOrEmpty(this.Functionality.ButtonCancelText))
                {
                    this.Functionality.ButtonCancel.Text = this.Functionality.ButtonCancelText;
                }
                string group = null;
                if (this.Functionality.ItemGroups.Keys.Count > 0)
                {
                    group = this.Functionality.ItemGroups[this.Functionality.ItemGroups.Keys.Last()].Identifier;
                }
                MyButton btnCancel = this.Functionality.AddButton(this.Functionality.ButtonCancel, group);
                btnCancel.Click += new EventHandler(btnCancel_Click);
            }
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.Functionality.triggerClickCancel();
        }
        void btnSubmit_Click(object sender, EventArgs e)
        {
            this.Functionality.triggerClickSubmit();
        }
        protected override void OnLoad(EventArgs e)
        {
            if (this.Functionality.LayoutProvider == null) throw new ArgumentNullException("Please specify layout provider");

            addSubmitButtonFromEvent();
            addCancelButtonFromEvent();
            updateValidationGroups();
            var control = this.Functionality.LayoutProvider.Render(this);
            this.Controls.Add(control);


            base.OnLoad(e);
        }
    }
}
