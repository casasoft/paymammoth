﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Hierarchy
{
    using System.Web.UI;
    using General_v3.Classes.Interfaces.Hierarchy;

    public class HierarchicalControlNavigation : HierarchicalControl
    {
        

        #region HierarchicalControlNavigation Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY : HierarchicalControl.FUNCTIONALITY
        {
            protected new HierarchicalControlNavigation _item { get { return (HierarchicalControlNavigation)base._item; } }

            public delegate void ListItemCreateHandler(MyListItem li, IHierarchy item, int depth, int index, MyAnchor a);

            public new event ListItemCreateHandler OnListItemCreate;

            internal void callOnListItemCreateEvent(MyListItem li, IHierarchy item, int depth, int index, MyAnchor a)
            {
                if (OnListItemCreate != null)
                {
                    OnListItemCreate(li, item, depth, index, a);
                }
            }

            public new IEnumerable<IHierarchyNavigation> Items
            {
                get { return (IEnumerable<IHierarchyNavigation>)base.Items; }
                set
                {
                    base.Items = value;
                }
            }


                internal FUNCTIONALITY(HierarchicalControlNavigation item)
                : base(item)
            {

            }

        }
        protected override BaseWebControl createChildItemContent(IHierarchy level, int levelIndex, MyListItem childItem, int count)
        {
            BaseWebControl result = null;
            IHierarchyNavigation navLevel = (IHierarchyNavigation)level;
            if (!string.IsNullOrEmpty(navLevel.Href))
            {
                MyAnchor a = new MyAnchor();
                a.InnerText = navLevel.Title;
                a.Href = navLevel.Href;
                a.HrefTarget = navLevel.HrefTarget;
                string css = getChildItemContentCssClassName(levelIndex);
                a.WebControlFunctionality.CssManager.AddClass(css);
                if (navLevel.Selected)
                {
                    //Add generic selected class
                    a.WebControlFunctionality.CssManager.AddClass("selected");
                    //Add level specific selected class for custom styling
                    a.WebControlFunctionality.CssManager.AddClass(css + "-selected");
                }
                result = a;
                Functionality.callOnListItemCreateEvent(childItem, level, levelIndex, count, a);
            }
            else
            {
                result = base.createChildItemContent(level, levelIndex, childItem, count);
                Functionality.callOnListItemCreateEvent(childItem, level, levelIndex, count, null);
            }
            return result;
        }
        protected override HierarchicalControl.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        public FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }

            #endregion


      
		
			
    }
}