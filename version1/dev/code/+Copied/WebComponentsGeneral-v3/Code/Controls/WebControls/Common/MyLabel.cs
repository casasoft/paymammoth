﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Web.UI.WebControls;
    using System.Web.UI;

    public class MyLabel : MyInnerHtmlControl
    {

        public Control ForControl { get; set; }
       

        public MyLabel()
            : base( HtmlTextWriterTag.Label)
        {
        }

        public string For { get { return this.Attributes["for"]; } set { this.Attributes["for"] = value; } }
            
        
        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            if (ForControl != null && string.IsNullOrEmpty(For))
            {
                this.For = ForControl.ClientID;
            }
            base.OnPreRender(e);
        }

    }
}
