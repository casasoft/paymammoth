namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using General_v3;
    using General_v3.Controls.WebControls;
    using Iesi.Collections.Generic;
using System.ComponentModel;

    public class MyAnchor : MyInnerHtmlControl
    {
        public MyAnchor()
            : base(HtmlTextWriterTag.A)
        {
            this.HrefTarget = Enums.HREF_TARGET.Self;
          //  this.RelTags = new HashedSet<Enums.ANCHOR_REL_VALUE>();
            
        }
       /* private void checkRelTags()
        {
            StringBuilder sb = new StringBuilder();
            if (RelTags != null && RelTags.Count > 0)
            {
                foreach (var tag in RelTags)
                {
                    if (sb.Length > 0) sb.Append(", ");
                    sb.Append(CS.General_v3.Util.EnumUtils.StringValueOf(tag));
                }
                this.Rel = sb.ToString();
            }
        }*/

       // public HashedSet<CS.General_v3.Enums.ANCHOR_REL_VALUE> RelTags { get; set; }
        public string Rel
        {
            get { return this.getAttributeAsString("rel"); }
            set { this.setAttributeFromString("rel", value); }
        }
        public string Href
        {
            get
            {
                return this.getAttributeAsString("href");
            }
            set
            {
                this.setAttributeFromString("href", value);
            }
        }
        public string Title
        {
            get
            {
                return this.getAttributeAsString("title");
            }
            set
            {
                this.setAttributeFromString("title", value);
            }
        }
        public Enums.HREF_TARGET HrefTarget { get; set; }
        private void initHrefTarget()
        {
            string hrefTarget = CS.General_v3.Util.EnumUtils.StringValueOf(HrefTarget);
            if (!string.IsNullOrEmpty(hrefTarget))
            {
                this.setAttributeFromString("target", hrefTarget);
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            initHrefTarget();
            base.OnLoad(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            
            base.OnPreRender(e);
        }

        public override void ForceRender()
        {
            initHrefTarget();
            base.ForceRender();
        }
    }
}
