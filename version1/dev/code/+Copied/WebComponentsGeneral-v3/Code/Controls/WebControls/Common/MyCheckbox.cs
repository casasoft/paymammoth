namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

    public class MyCheckBox : MyInputBaseWithType<bool>
    {
        public bool Checked
        {
            get
            {
                return getAttributeAsBool("checked");
            } 
            set
            {
                setAttributeFromBool("checked", value);
            }
        }
        public object Tag { get; set; }

            public MyCheckBox(FieldSingleControlParameters parameters = null)
            : base( INPUT_TYPE.Checkbox, parameters)
        {

        }

        public string GroupName { get; set; }
        
        protected override BaseFieldParameters createFieldParameters()
        {
            return new FieldSingleControlParameters();
        }

        public new FieldSingleControlParameters FieldParameters
        {
            get
            {
                return (FieldSingleControlParameters)base.FieldParameters;
            }
            set { base.FieldParameters = value; }
        }



        public string DataValue { get { return getAttributeAsString("data-value"); } set { setAttributeFromString("data-value", value); } }

        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
        }

        public override string JSFieldTypeName
        {
            get
            {
                
                return "FieldSingleControl";
            }
        }

        public bool Multiple { get { return getAttributeAsBool("multiple"); } set { setAttributeFromBool("multiple", value); } }

        private bool objectToBool(object value)
        {
            bool b = false;
            if (value != null)
            {
                string sValue = value.ToString().ToLower();
                b = ((sValue == "on") || (sValue == "true") || (sValue == "1"));
            }

            return b;
        }

        protected override void updateInitialValue(object value)
        {
            this.Checked = objectToBool(value);
            this.WebControlFunctionality.InitialValue = value;
        }



        public bool GetFormValueObjectAsBool()
        {
            return (bool) GetFormValueObject();
        }

        public override object GetFormValueObject()
        {
            return objectToBool(base.GetFormValueObject());
        }

        public override bool InitialValue
        {
            get
            {
                return CS.General_v3.Util.Other.ConvertObjectToBasicDataType<bool>(this.WebControlFunctionality.InitialValue);
            }
            set
            {
                updateInitialValue(value);
            }
        }
        public override string Value
        {
            get
            {
                return this.ClientID;
            }
            set
            {
                //base.Value = value;
            }
        }

        public override object GetInitialTemporaryRandomValue()
        {
            bool val = CS.General_v3.Util.Random.GetBool();
            if (this.FieldParameters.required)
            {
                val = true;
            }
            return val;
        }
    }
}
