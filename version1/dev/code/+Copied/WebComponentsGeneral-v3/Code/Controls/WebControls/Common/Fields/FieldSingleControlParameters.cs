﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields
{

    public class FieldSingleControlParameters : BaseFieldParameters
    {
        /// <summary>
        /// Do not update this.  This is updated internally to update the field clientID
        /// </summary>
        public string elementID;
    }
}