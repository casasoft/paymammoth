﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;


namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.FileBrowser
{
    public class NewFolderPanel : MyDiv
    {
        public NewFolderPanel(string id) 
        {

            this.ID = id;
            this.Functionality = new FUNCTIONALITY(this);
        }
        public class FUNCTIONALITY
        {
#region Events
            public delegate void NewFolderDelegate(NewFolderPanel sender, string folderName);
#endregion
            public FUNCTIONALITY(NewFolderPanel NewFolderPanel)
            {
                _NewFolderPanel = NewFolderPanel;
                
            }

            private MyTxtBoxString _txtFolderName; 

            public event NewFolderDelegate OnNewFolder;

            private NewFolderPanel _NewFolderPanel = null;
            private FormFields.FormFields _form = null;
            
            private void renderHeading()
            {
                MyHeading hTitle = new MyHeading(1, "Create New Folder");
                hTitle.CssManager.AddClass("newFolderPanel-heading");
                _NewFolderPanel.Controls.Add(hTitle);
            }
            private void renderForm()
            {
                _form = new FormFields.FormFields();
                _form.ID = _NewFolderPanel.ID + "_form_newFolder";
                _form.Functionality.ValidationGroup = "newFolder";
                _txtFolderName = _form.Functionality.AddString("txtFolderName", "Folder Name", true, "Folder name", initialValue:"");
                _form.Functionality.ClickSubmit += new EventHandler(_form_ClickSubmit);
                _form.Functionality.ButtonSubmitText = "Create folder";
                _NewFolderPanel.Controls.Add(_form);
                
            }

            void _form_ClickSubmit(object sender, EventArgs e)
            {
                string folderName = _txtFolderName.GetFormValueAsString();
                if (OnNewFolder != null)
                    OnNewFolder(_NewFolderPanel,folderName);
            }
            public void Render()
            {
                renderHeading();
                renderForm();
            }

        }
        public FUNCTIONALITY Functionality { get; set; }
        
        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.Render();
            base.OnLoad(e);
        }
    }
}
