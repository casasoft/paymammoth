namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Fields;
    using General_v3;
    using General_v3.Controls.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

    public class MyTxtBoxString : MyInputBaseWithType<string>
    {


        public MyTxtBoxString(FieldSingleControlParameters fieldParameters = null, INPUT_TYPE type = INPUT_TYPE.Text)
            : base(type, fieldParameters)
        {
            
        }

        public string GetFormValue()
        {
            return this.WebControlFunctionality.GetFormValue();
        }


        public override string InitialValue
        {
            get
            {
                return (string)this.WebControlFunctionality.InitialValue;
            }
            set
            {
                this.WebControlFunctionality.InitialValue = value;
            }
        }

        public override object GetInitialTemporaryRandomValue()
        {
            var stringValidator = this.WebControlFunctionality.GetOrCreateValidator<ValidatorString>();
            if (stringValidator != null)
            {
                return stringValidator.GenerateRandomValidValue();
            }
            else
            {
                return null;
            }
        }
    }
}
