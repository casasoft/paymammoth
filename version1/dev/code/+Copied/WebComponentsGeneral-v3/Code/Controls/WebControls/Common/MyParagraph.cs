﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public class MyParagraph : MyInnerHtmlControl
    {

        public MyParagraph(string htmlContent = null)
            : base(HtmlTextWriterTag.P)
        {
            this.InnerHtml = htmlContent;
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
        

    }
}
