using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Cms.Classes;
namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{

    public class MyTableCellCollection : MyControlCollection<MyTableCell>
    {

        public MyTableCellCollection(MyTableRow tr):base(tr)
        {
            
        }
       
    }
    public class MyTableRow : BaseWebControl
    {

        public MyTableCellCollection Cells { get; private set; }
        public MyTableRow():base( HtmlTextWriterTag.Tr)
        {
            this.Cells = new MyTableCellCollection(this);
        }

        public new MyTableCellCollection Controls
        {
            get
            {
                return Cells;
            }
        }



        public MyTableCell this[int i]
        {
            get {
                return Cells[i];
            }
        }
        private MyTableCell addCell(IEnumerable<Control> controls, string cssClass = null, string innerHtml = null, bool header = false)
        {
            MyTableCell cell = header ? new MyTableHeaderCell() : new MyTableCell();
            this.Cells.Add(cell);
            if (!string.IsNullOrEmpty(cssClass)) cell.CssManager.AddClass(cssClass);
            if (!string.IsNullOrEmpty(innerHtml)) cell.InnerHtml = innerHtml;
            foreach (var control in controls)
            {
                cell.Controls.Add(control);
            }
            return cell;

        }
        public MyTableCell AddCell(string cssClass = null, string innerHtml = null, bool header = false, params Control[] controls)
        {
            return addCell(controls, cssClass, innerHtml, header);

        }
        public MyTableHeaderCell AddHeaderCell(string cssClass = null, string innerHtml = null, params Control[] controls)
        {
            return (MyTableHeaderCell)addCell(controls, cssClass, innerHtml, header: true);

        }

    }
}
