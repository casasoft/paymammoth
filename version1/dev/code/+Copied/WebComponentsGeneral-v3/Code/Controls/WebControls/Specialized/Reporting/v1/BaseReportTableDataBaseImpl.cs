﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public abstract class BaseReportTableDataBaseImpl : IBaseReportTableDataBase
    {
        #region IBaseReportTableData Members
        public BaseReportTableDataBaseImpl()
        {
        }

       

        protected abstract IEnumerable<BaseReportTableHeaderDataBaseImpl> createHeaderRows();
        protected abstract IEnumerable<BaseReportTableRowDataBaseImpl> createDataRows();
        private IEnumerable<BaseReportTableHeaderDataBaseImpl> _headersTop;
        private IEnumerable<BaseReportTableHeaderDataBaseImpl> _headersLeft;
        private IEnumerable<BaseReportTableRowDataBaseImpl> _dataRows;
        public IEnumerable<BaseReportTableRowDataBaseImpl> DataRows
        {
            get { return _dataRows ?? (_dataRows = createDataRows()); }
        }
        public IEnumerable<BaseReportTableHeaderDataBaseImpl> HeadersTop
        {
            get { return _headersTop ?? (_headersTop = createHeaderRows()); }
        }
        public IEnumerable<BaseReportTableHeaderDataBaseImpl> HeadersLeft
        {
            get { return _headersLeft ?? (_headersLeft = createHeaderRows()); }
        }
        #endregion


        #region IBaseReportTableDataBase Members

        IEnumerable<IBaseReportTableHeaderDataBase> IBaseReportTableDataBase.HeadersTop
        {
            get { return this.HeadersTop; }
        }

        IEnumerable<IBaseReportTableHeaderDataBase> IBaseReportTableDataBase.HeadersLeft
        {
            get { return this.HeadersLeft; }
        }

        IEnumerable<IBaseReportTableRowDataBase> IBaseReportTableDataBase.DataRows
        {
            get { return this.DataRows; }
        }

        #endregion
    }
}
