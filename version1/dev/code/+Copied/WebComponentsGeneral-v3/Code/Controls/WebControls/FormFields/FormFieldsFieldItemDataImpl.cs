﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields
{
    public class FormFieldsFieldItemDataImpl : FormFieldsItemDataImpl
    {
        public FormFieldsFieldItemDataImpl(IMyFormWebControl control)
        {
            this.Control = control;
        }

        private IMyFormWebControl _formControl;

        new public IMyFormWebControl Control
        {
            get { return _formControl; }
            set
            {
                _formControl = value;
                if (_formControl != null)
                {
                    base.Control = _formControl.Control;
                }
                else
                {
                    base.Control = null;
                }
            }

        }

        public override bool Required
        {
            get
            {
                return _formControl.FieldParameters.required;
            }
            set
            {
                _formControl.FieldParameters.required = value;
            }
        }
        public override string Title
        {
            get
            {
                return _formControl.FieldParameters.title;
            }
            set
            {
                _formControl.FieldParameters.title = value;
            }
        }

        public static FormFieldsFieldItemDataImpl GetFromMyFormWebControl(IMyFormWebControl control, FormFieldsLayoutTypeInformation layoutInfo = null, string subTitle = null, string rowCssClass = null)
        {
            FormFieldsFieldItemDataImpl item = new FormFieldsFieldItemDataImpl(control)
                                                   {
                                                       
                                                       LayoutInfo = layoutInfo,
                                                       CssClass = rowCssClass,
                                                       Title = control.FieldParameters.title,
                                                       TitleContentText = control.FieldParameters.titleContentText, 
                                                       Required = control.FieldParameters.required,
                                                       SubTitle = subTitle

                                                   };
            return item;
        }
    }
}