﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields
{
    public class FormFieldsItemDataImpl : IFormFieldsItemData
    {
        public FormFieldsItemDataImpl()
        {
            this.LayoutInfo = new FormFieldsLayoutTypeInformation();
        }

        public string CssClass { get; set; }

        private string _title;
        public virtual string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }

            public Control Control { get; set; }

        public FormFieldsLayoutTypeInformation LayoutInfo { get; set; }


        public string SubTitle { get; set; }

        public virtual bool Required { get; set; }

        public BaseFieldParameters FieldParameters { get; set; }





        public BusinessLogic_v3.Frontend.ContentTextModule.ContentTextBaseFrontend TitleContentText
        {
            get;
            set;
        }


        public BusinessLogic_v3.Frontend.ContentTextModule.ContentTextBaseFrontend SubTitleContentText
        {
            get;
            set;
        }
    }
}