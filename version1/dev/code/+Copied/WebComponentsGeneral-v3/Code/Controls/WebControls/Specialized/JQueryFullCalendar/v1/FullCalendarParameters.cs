﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.JQueryFullCalendar.v1
{
    public class FullCalendarParameters : JSONObject
    {
        public string divCalendarID;
        public bool onChangeViewRefreshCufon;

        public _jQueryFullCalendarOptions options = new _jQueryFullCalendarOptions();

        protected override bool customDoNotOutputJSFunctionality()
        {
            return string.IsNullOrEmpty(divCalendarID);
        }

        protected override string getJSClassName()
        {
            return "js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.JQuery.FullCalendar.FullCalendar";
        }
    }
}
