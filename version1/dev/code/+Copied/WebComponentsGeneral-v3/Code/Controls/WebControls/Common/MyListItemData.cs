﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using BusinessLogic_v3.Frontend.ContentTextModule;
using System.Web.UI.WebControls;
    using BusinessLogic_v3.Classes.Text;

    public class MyListItemData
    {
        public MyListItemData(ListItem item = null)
        {
            if (item != null)
            {
                this.Disabled = !item.Enabled;
                this.Label = item.Text;
                this.Value = item.Value;
                this.Selected = item.Selected;
            }
        }

        public bool Disabled { get; set; }
        public bool Selected { get; set; }

        public string Label { get; set; }
        public ContentTextBaseFrontend LabelContentText { get; set; }
        public TokenReplacerNew LabelContentTextReplacementTags { get; set; }

        public string Value { get; set; }

        public string GetLabel()
        {
            if (LabelContentText != null)
            {
                return LabelContentText.GetContent(LabelContentTextReplacementTags);
            }
            else
            {
                return Label;
            }
        }

    }
}