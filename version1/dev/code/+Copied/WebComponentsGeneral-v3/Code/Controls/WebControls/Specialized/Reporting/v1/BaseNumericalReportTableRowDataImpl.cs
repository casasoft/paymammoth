﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public class BaseNumericalReportTableRowDataImpl : BaseReportTableRowDataBaseImpl, IBaseNumericalReportTableRowData
    {

        public BaseNumericalReportTableRowDataImpl()
        {
        }

        protected override IEnumerable<BaseReportTableCellDataBaseImpl> initCells()
        {
            return new List<BaseNumericalReportTableCellDataImpl>();
        }
        
       

        #region IBaseNumericalReportTableRowData Members

        public new List<BaseNumericalReportTableCellDataImpl> Cells
        {
            get { return (List<BaseNumericalReportTableCellDataImpl>)base.Cells; }
        }

        #endregion

        #region IBaseNumericalReportTableRowData Members

        IEnumerable<IBaseNumericalReportTableCellData> IBaseNumericalReportTableRowData.Cells
        {
            get { return this.Cells; }
        }

        #endregion

      
    }
}
