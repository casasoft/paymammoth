﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    internal class BaseNumericalReportSectionTable : BaseReportSectionTableBase
    {


        

        #region BaseNumericalReportSectionTable Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public new class FUNCTIONALITY : BaseReportSectionTableBase.FUNCTIONALITY
        {
            public new IBaseNumericalReportSectionData Section { get { return (IBaseNumericalReportSectionData)base.Section; } set { base.Section = value; } }
            protected new BaseNumericalReportSectionTable _item { get { return (BaseNumericalReportSectionTable)base._item; } }

            internal FUNCTIONALITY(BaseNumericalReportSectionTable item)
                : base(item)
            {
                
            }

        }
        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }

        protected override BaseReportSectionTableBase.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        #endregion
		
			
		
			
        public BaseNumericalReportSectionTable()
        {
            
        }



        protected override bool hasTotalVertical()
        {
            return this.Functionality.Section.ShowTotalsVertical;
        }

        protected override bool hasTotalHorizontal()
        {
            return this.Functionality.Section.ShowTotalsHorizontal;
        }

        protected override bool hasTotalVerticalAndHorizontal()
        {
            return this.Functionality.Section.ShowTotalsHorizontal && this.Functionality.Section.ShowTotalsVertical;
        }

        private readonly Dictionary<int, double> _rowTotals = new Dictionary<int, double>();
        private readonly Dictionary<int, double> _colTotals = new Dictionary<int, double>();


        private Control getControlFromValue(double value)
        {
            return new Literal()
            {
                Text =
                    CS.General_v3.Util.NumberUtil.FormatNumber(value,
                                                                 this.Functionality.Section.
                                                                     ThousandsDelimiter,
                                                                 this.Functionality.Section.DecimalPlaces)
            };
        }

        protected override void parseHeaderTotalContent(IBaseReportTableHeaderDataBase header, bool vertical, int index, out System.Web.UI.Control totalContent, out General_v3.Classes.CSS.CSSManager cssClasses)
        {

            totalContent = null;
            cssClasses = null;
            if ((vertical && this.Functionality.Section.ShowTotalsVertical) || (!vertical && this.Functionality.Section.ShowTotalsHorizontal))
            {
                double total = 0;
                if (vertical)
                {
                    if (_colTotals.ContainsKey(index))
                    {
                        total = _colTotals[index];
                    }
                    else
                    {
                        foreach (var row in this.Functionality.Section.TableData.DataRows)
                        {
                            var col = row.Cells.ElementAt(index);
                            total += col.Value;
                        }
                        _colTotals[index] = total;
                    }
                }
                else
                {
                    if (_rowTotals.ContainsKey(index))
                    {
                        total = _rowTotals[index];
                    }
                    else
                    {
                        var row = this.Functionality.Section.TableData.DataRows.ElementAt(index);
                        foreach (var cell in row.Cells)
                        {
                            total += cell.Value;
                        }
                        _rowTotals[index] = total;
                    }
                }

                totalContent = getControlFromValue(total);
                cssClasses = null;
            }
        }

        protected override void getVerticalAndHorizontalTotalContent(out System.Web.UI.Control content, out General_v3.Classes.CSS.CSSManager cssClasses)
        {
            content = null;
            cssClasses = null;
            if (this.Functionality.Section.ShowTotalHorizontalAndVertical && this.Functionality.Section.ShowTotalsHorizontal && this.Functionality.Section.ShowTotalsVertical)
            {
                double total = 0;
                foreach(var val in _rowTotals.Values) 
                {
                    total += val;
                } 
                foreach (var val in _colTotals.Values)
                {
                    total += val;
                }
                content = getControlFromValue(total);
                cssClasses = null;
            }
        }

        private void updateDecimalPlacesAndThousands()
        {
            if (this.Functionality.Section.DecimalPlaces.HasValue || this.Functionality.Section.ThousandsDelimiter != null)
            {
                foreach (var row in this.Functionality.Section.TableData.DataRows)
                {
                    foreach (var cell in row.Cells)
                    {
                        if (!cell.DecimalPlaces.HasValue && this.Functionality.Section.DecimalPlaces.HasValue)
                        {
                            cell.DecimalPlaces = this.Functionality.Section.DecimalPlaces;
                        }
                        if (cell.ThousandsDelimiter == null && this.Functionality.Section.ThousandsDelimiter != null)
                        {
                            cell.ThousandsDelimiter = this.Functionality.Section.ThousandsDelimiter;
                        }
                    }
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            updateDecimalPlacesAndThousands();
            base.OnLoad(e);
        }


    }
}
