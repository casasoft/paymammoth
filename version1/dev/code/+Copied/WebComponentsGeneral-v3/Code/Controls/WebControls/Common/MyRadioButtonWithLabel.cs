namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using BusinessLogic_v3.Frontend.ContentTextModule;

    public class MyRadioButtonWithLabel : MyFormItemWithLabel<bool>
    {

        private MyRadioButton _radioButton;

        public MyRadioButtonWithLabel(FieldSingleControlParameters parameters = null)
            : base(new MyRadioButton(), parameters)
        {
            _radioButton = (MyRadioButton)this.FormControl;
        }

        public string GroupName { get { return _radioButton.GroupName; } set { _radioButton.GroupName = value; } }
        public object Tag { get { return _radioButton.Tag; } set { _radioButton.Tag = value; } }

        public override string GetDataTypeAsString()
        {
            return "radio-button";
        }

        public bool Checked { get { return _radioButton.Checked; } set { _radioButton.Checked = value; } }
        }
}
