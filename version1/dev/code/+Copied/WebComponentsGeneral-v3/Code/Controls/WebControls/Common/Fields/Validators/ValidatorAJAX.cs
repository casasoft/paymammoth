﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorAJAX : ValidatorBase
    {

        public new ValidatorAJAXParameters Parameters { get { return (ValidatorAJAXParameters)base.Parameters; } set { base.Parameters = value; } }

        protected override ValidatorBaseParameters createParameters()
        {
            return new ValidatorAJAXParameters();
        }

        public override object GenerateRandomValidValue()
        {
            throw new NotImplementedException();
        }
    }
}