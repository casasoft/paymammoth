namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System.Collections.Generic;
    using System.Collections;
    using System.Web.UI.WebControls;


    public class MyTableRowCollection : IList<MyTableRow>
    {
        private TableRowCollection _rows;
        public MyTableRowCollection(TableRowCollection rows)
        {
            
            this._rows = rows;
            
        }







        
        #region IList<MyTableRow> Members

        public int IndexOf(MyTableRow item)
        {
            return _rows.GetRowIndex(item);
            
        }

        public void Insert(int index, MyTableRow item)
        {
            _rows.AddAt(index, item);
            
        }

        public void RemoveAt(int index)
        {
            _rows.RemoveAt(index);
            
        }

        public MyTableRow this[int index]
        {
            get
            {
                return (MyTableRow)_rows[index];
                
            }
            set
            {
                
            }
        }

        #endregion

        #region ICollection<MyTableRow> Members

        public void Add(MyTableRow item)
        {
            _rows.Add(item);
            
        }

        public void Clear()
        {
            _rows.Clear();
            
        }

        public bool Contains(MyTableRow item)
        {
            int index = _rows.GetRowIndex(item);
            return (index != -1);
            
        }

        public void CopyTo(MyTableRow[] array, int arrayIndex)
        {
            _rows.CopyTo(array, arrayIndex);
            
        }

        public int Count
        {
            get { return _rows.Count; }
        }

        public bool IsReadOnly
        {
            get
            {
                return _rows.IsReadOnly;
            }
        }

        public bool Remove(MyTableRow item)
        {
            int index = _rows.GetRowIndex(item);
            if (index > -1)
            {
                _rows.Remove(item);
                return true;
            }
            else
            {
                return false;
            }

            
        }

        #endregion

        #region IEnumerable<MyTableRow> Members

        public IEnumerator<MyTableRow> GetEnumerator()
        {
            return (IEnumerator<MyTableRow>) _rows.GetEnumerator();
            
        }

        #endregion
        
        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _rows.GetEnumerator();
            
        }

        #endregion


    }
}
