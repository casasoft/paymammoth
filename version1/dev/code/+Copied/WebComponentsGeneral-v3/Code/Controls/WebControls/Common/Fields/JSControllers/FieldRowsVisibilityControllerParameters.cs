﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.JSControllers
{
    public class FieldRowsVisibilityControllerParameters : JSONObject
    {
        public string fieldID;
        public List<FieldRowVisibilityValueParameters> values = new List<FieldRowVisibilityValueParameters>();


        protected override string getJSClassName()
        {
            return "js.com.cs.v3.UI.Forms.Fields.Controllers.FieldRowsVisibilityController";
        }
        protected override bool customDoNotOutputJSFunctionality()
        {
            return string.IsNullOrEmpty(fieldID);
        }

    }
}