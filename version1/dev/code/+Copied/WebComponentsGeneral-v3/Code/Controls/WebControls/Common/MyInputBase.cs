namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

    public abstract class MyInputBase : MyFormWebControl
    {

        public enum INPUT_TYPE
        {
            [Description("text")]
            Text,
            [Description("password")]
            Password,
            [Description("checkbox")]
            Checkbox,
            [Description("radio")]
            Radio,
            [Description("button")]
            Button,
            [Description("submit")]
            Submit,
            [Description("file")]
            File,
            [Description("hidden")]
            Hidden,
            [Description("image")]
            Image,
            [Description("datetime")]
            DateTime,
            [Description("datetime-local")]
            DateTimeLocal,
            [Description("date")]
            Date,
            [Description("month")]
            DateMonth,
            [Description("time")]
            TimeOnly,
            [Description("week")]
            DateWeek,
            [Description("number")]
            Number,
            [Description("range")]
            NumberRange,
            [Description("email")]
            Email,
            [Description("url")]
            Website,
            [Description("search")]
            Search,
            [Description("tel")]
            Tel,
            [Description("color")]
            Color
        }

        public INPUT_TYPE InputType { get; set; }

        protected MyInputBase(FieldSingleControlParameters fieldParameters = null)
            : this(INPUT_TYPE.Text, fieldParameters)
        {

        }


        protected MyInputBase(INPUT_TYPE type, FieldSingleControlParameters fieldParameters = null)
            : base(HtmlTextWriterTag.Input, fieldParameters)
        {
            InputType = type;
            this._type = GetType(type);
            this.JSFieldTypeName = "FieldSingleControl";
        }
        

        public static string GetType(INPUT_TYPE type)
        {
            ///You cannot customise the date format ( need to do some workaround )
            switch (type)
            {
                case INPUT_TYPE.Date:
                case INPUT_TYPE.DateMonth:
                case INPUT_TYPE.DateTime:
                case INPUT_TYPE.DateTimeLocal:
                case INPUT_TYPE.DateWeek:
                    type = INPUT_TYPE.Text; break;
            }

            return CS.General_v3.Util.EnumUtils.StringValueOf(type);
        }

        private string _type { get { return this.getAttributeAsString("type"); } set { this.setAttributeFromString("type", value); } }

        protected override BaseFieldParameters createFieldParameters()
        {
            return new FieldSingleControlParameters();
        }

        public new FieldSingleControlParameters FieldParameters
        {
            get
            {
                return (FieldSingleControlParameters)base.FieldParameters;
            }
            set { base.FieldParameters = value; }
        }




        protected override void OnLoad(EventArgs e)
        {
            this._type = GetType(InputType);

            base.OnLoad(e);
        }

        public override string JSFieldTypeName
        {
            get;
            set;
        }


        protected override void updateInitialValue(object value)
        {
            if (value != null)
            {
                this.Value = value.ToString();
            }
            else
            {
                this.Value = null;
            }
        }

        public override string GetDataTypeAsString()
        {
            return CS.General_v3.Util.EnumUtils.StringValueOf(this.InputType);
        }

        public override void SetFormNames()
        {
            this.setAttributeFromString("name", this.ClientID);
        }

        protected override string getElementClientID()
        {
            return this.ClientID;
        }
        }
}
