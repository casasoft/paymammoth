﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorDate : ValidatorBase
    {
        
        public new ValidatorDateParameters Parameters { get { return (ValidatorDateParameters)base.Parameters; } set { base.Parameters = value; } }

        protected override ValidatorBaseParameters createParameters()
        {
            return new ValidatorDateParameters();
        }

        public override object GenerateRandomValidValue()
        {
            DateTime dateFrom = DateTime.MinValue;
            DateTime dateTo = DateTime.MaxValue;
            if (this.Parameters.dateFrom.HasValue)
            {
                dateFrom = this.Parameters.dateFrom.Value;
            }
            if (this.Parameters.dateTo.HasValue)
            {
                dateTo = this.Parameters.dateTo.Value;
            }
            DateTime dt = CS.General_v3.Util.Random.GetDateTime(dateFrom, dateTo);
            return dt;
        }
        public override string GenerateRandomValidValueAsString()
        {
            DateTime dt = (DateTime)GenerateRandomValidValue();
            if (!string.IsNullOrEmpty(this.Parameters.dateFormat))
            {
                return CS.General_v3.Util.Date.FormatDateTime(dt, this.Parameters.dateFormat);
            }
            else
            {
                return base.GenerateRandomValidValueAsString();
            }
        }
    }
}