﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public class BaseReportTableHeaderDataImpl :BaseReportTableHeaderDataBaseImpl,  IBaseReportTableHeaderData
    {
        public BaseReportTableHeaderDataImpl()
        {
            init();
        }
        public BaseReportTableHeaderDataImpl(string title)
        {
            init();
            this.HeaderCellContent = new BaseReportTableCellDataImpl();
            this.HeaderCellContent.InnerHTML = CS.General_v3.Util.PageUtil.HtmlEncode(title);
        }
        private void init()
        {
            this.TotalCellContent = new BaseReportTableCellDataImpl();
        }

        #region IBaseReportTableHeaderData Members

        public BaseReportTableCellDataImpl TotalCellContent
        {
            get;
            set;
        }

        #endregion

        #region IBaseReportTableHeaderData Members

        IBaseReportTableCellData IBaseReportTableHeaderData.TotalCellContent
        {
            get { return this.TotalCellContent; }
        }

        #endregion

    }


}
