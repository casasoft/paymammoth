﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.JQueryFullCalendar.v1
{
    public class FullCalendar : MyDiv
    {
        public class FUNCTIONALITY
        {
            private FullCalendar _control;

            public FullCalendarParameters Parameters { get; private set; }
            public List<_jQueryFullCalendarOptionsEventObject> Events { get; set; }

            public FUNCTIONALITY(FullCalendar control)
            {
                this.Parameters = new FullCalendarParameters();

                _control = control;
            }
        }

        public FUNCTIONALITY Functionality { get; private set; }
        public FullCalendar()
        {
            this.Functionality = getFunctionality();
        }
        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private void initJS()
        {
            this.Functionality.Parameters.divCalendarID = this.ClientID;


            string js = "new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.JQuery.FullCalendar.FullCalendar(" + this.Functionality.Parameters.GetJSON() + ");\r\n";
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "fullCalendar_" + this.ClientID, js, true);

        }
        protected override void OnPreRender(EventArgs e)
        {
            initJS();
            base.OnPreRender(e);
        }
        

    }
}
