﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls
{
    using System;

    public interface IBaseButtonWebControl : IBaseWebControl
    {
        event EventHandler Click;
        new BaseButtonWebControlFunctionality WebControlFunctionality { get; }
        
    }
}
