﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.FileBrowser
{
    public class UploadPanel : MyDiv
    {
        public UploadPanel(string id)
        {
            this.ID = id;
            this.Functionality = new FUNCTIONALITY(this);
        }
        public class FUNCTIONALITY
        {
            private MyFileUpload _fileUpload;
            public FUNCTIONALITY(UploadPanel UploadPanel)
            {
                _UploadPanel = UploadPanel;
                this.ShowImageWidthHeight = true;
                this.FileTypesAllowed = new List<string>();
            }

            public bool ShowImageWidthHeight { get; set; }
            public delegate void UploadEventDelegate(UploadPanel sender,
                string filename, System.IO.Stream fileContent, int? widthPx, int? heightPx, bool fillBoxByCropping);
            public event UploadEventDelegate OnFileUpload;

            private UploadPanel _UploadPanel = null;
            private FormFields.FormFields _form = null;
            public List<string> FileTypesAllowed { get; set; }
            private void renderHeading()
            {
                MyHeading hTitle = new MyHeading(1, "Upload");
                hTitle.CssManager.AddClass("uploadPanel-heading");
                _UploadPanel.Controls.Add(hTitle);
            }

            private MyTxtBoxNumber _txtWidth;
            private MyTxtBoxNumber _txtHeight;
            private MyCheckBox _chkFillBoxByCropping;

            private void renderForm()
            {
                _form = new FormFields.FormFields();
                _form.ID = _UploadPanel.ID + "_formUpload";
                _fileUpload = _form.Functionality.AddFileUpload("txtUpload", "Upload", true, "Select file to upload");
                //_form.ValidationGroup = "fileUploadFiles";

                _fileUpload.ValidatorStringParameters.fileExtensionsAllowed = this.FileTypesAllowed;

                if (ShowImageWidthHeight)
                {
                    _txtWidth = _form.Functionality.AddInteger("txtWidth", "Width (px)", false, "Width in pixels (Applies only to image files)");
                    _txtHeight = _form.Functionality.AddInteger("txtHeight", "Height (px)", false, "Height in pixels (Applies only to image files)");
                    _chkFillBoxByCropping = _form.Functionality.AddBool("chkFillBoxByCropping", "Fill box:", false, "Whether image should fill exactly the specified box, by cropping the middle part");

                }
                _form.Functionality.ButtonSubmitText = "Upload";
                _form.Functionality.ClickSubmit += new EventHandler(_form_ClickSubmit);
                _UploadPanel.Controls.Add(_form);
            }

            void _form_ClickSubmit(object sender, EventArgs e)
            {
                MyFileUpload txt = _fileUpload;
                
                if (txt.UploadedFileStream.Length > 0)
                {
                    int? width = null;
                    int? height = null;
                    bool fillBox = false;
                    if (ShowImageWidthHeight)
                    {
                        width = _txtWidth.GetFormValueAsIntNullable();
                        height = _txtHeight.GetFormValueAsIntNullable();
                        fillBox = width != null && height != null && _chkFillBoxByCropping.GetFormValueObjectAsBool();
                    }

                    if (OnFileUpload != null)
                        OnFileUpload(_UploadPanel, txt.FileName, txt.UploadedFileStream,width,height,fillBox);

                }
            }
            public void Render()
            {
                renderHeading();
                renderForm();
            }

        }
        public FUNCTIONALITY Functionality { get; set; }
        
        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.Render();
            base.OnLoad(e);
        }
    }
}
