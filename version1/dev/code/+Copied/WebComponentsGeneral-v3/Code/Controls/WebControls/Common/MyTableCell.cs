namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;


    public class MyTableCell : MyInnerHtmlControl
    {

        public MyTableCell(bool headerCell = false)
            : base(headerCell ? HtmlTextWriterTag.Th : HtmlTextWriterTag.Td)
        {
        }

        public int ColumnSpan
        {
            get { return (int)this.getAttributeAsNumeric("colspan").GetValueOrDefault(); }
            set { this.setAttributeFromNumeric("colspan", value); }
        }
    }
}
