﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public interface IBaseReportTableRowData : IBaseReportTableRowDataBase
    {
        new IEnumerable<IBaseReportTableCellData> Cells { get; }
    }
}
