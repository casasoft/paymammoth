namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Fields;
    using General_v3;
    using General_v3.Controls.WebControls;

    public class MyHiddenField : MyInputBaseWithType<string>
    {


        public MyHiddenField(FieldSingleControlParameters fieldParameters = null, INPUT_TYPE type = INPUT_TYPE.Hidden)
            : base(type, fieldParameters)
        {
            
        }

        public string GetFormValue()
        {
            return this.WebControlFunctionality.GetFormValue();
        }


        public override string InitialValue
        {
            get
            {
                return (string)this.WebControlFunctionality.InitialValue;
            }
            set
            {
                this.WebControlFunctionality.InitialValue = value;
            }
        }

        public override object GetInitialTemporaryRandomValue()
        {
            return null;
        }
    }
}
