namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Fields;
    using General_v3;
    using General_v3.Controls.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

    public class MyTxtBoxNumber : MyInputBaseWithType<double?>
    {
        public double? Min 
        {
            get
            {
                return getAttributeAsNumeric("min");
            }
            set
            {
                this.WebControlFunctionality.GetOrCreateValidator<ValidatorNumeric>().Parameters.numFrom = value;
                this.setAttributeFromNumeric("min", value);
            }
        }

        public double? Max
        {
            get
            {
                return getAttributeAsNumeric("max");
            }
            set
            {
                this.WebControlFunctionality.GetOrCreateValidator<ValidatorNumeric>().Parameters.numTo = value;
                this.setAttributeFromNumeric("max", value);
            }
        }
        public double? Step
        {
            get
            {

                return getAttributeAsNumeric("step");
            }
            set
            {
                
                this.setAttributeFromNumeric("step", value, "any");
            }
        }
        /*public double? Max { get { return getAttributeValue("max"); } set { this.setAttributeValue("max", value); } }
        public double? Step { get { return getAttributeValue("min"); } set { this.setAttributeValue("min", value); } }
        public double? Min { get { return getAttributeValue("min"); } set { this.setAttributeValue("min", value); } }*/



        public MyTxtBoxNumber(FieldSingleControlParameters fieldParameters = null)
            : base(INPUT_TYPE.Number, fieldParameters)
        {
           // this.InputType = INPUT_TYPE.Text;
        }
        
        public long GetFormValueAsLong()
        {
            return GetFormValueAsLongNullable().GetValueOrDefault();
        }
        public int? GetFormValueAsIntNullable()
        {
            return ((int?)GetFormValueAsLongNullable());
        }
        public double? GetFormValueAsDoubleNullable()
        {
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<double?>(GetFormValueAsString());        
        }

        public override object GetFormValueObject()
        {
            if (ValidatorNumericParameters.integersOnly)
            {
                //Integers
                return GetFormValueAsLongNullable();
            }
            else
            {
                return GetFormValueAsDoubleNullable();
            }
        }
        private void updateAttributesFromValidators()
        {
            var validator = this.WebControlFunctionality.GetOrCreateValidator<ValidatorNumeric>();
            this.Min = validator.Parameters.numFrom;
            this.Max = validator.Parameters.numTo;
            if (validator.Parameters.positiveOnly)
            {
                
                if (!this.Min.HasValue) this.Min = 0;
                if (!this.Max.HasValue) this.Max = double.PositiveInfinity;
            }
            if (validator.Parameters.negativeOnly)
            {
                if (!this.Min.HasValue) this.Min = double.NegativeInfinity;
                if (!this.Max.HasValue) this.Max = 0;
            }
            if (validator.Parameters.integersOnly)
            {
                this.Step = 1;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            updateAttributesFromValidators();
            this.WebControlFunctionality.GetOrCreateValidator<ValidatorNumeric>();
            base.OnLoad(e);
        }
        public ValidatorNumericParameters ValidatorNumericParameters
        {
            get
            {
                ValidatorNumeric dtValidator = this.FieldParameters.GetOrCreateValidator<ValidatorNumeric>();

                return dtValidator.Parameters;
            }
        }



        public override double? InitialValue
        {
            get
            {
                if (this.WebControlFunctionality.InitialValue != null)
                {
                    return CS.General_v3.Util.NumberUtil.ParseDouble(this.WebControlFunctionality.InitialValue);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.WebControlFunctionality.InitialValue = value;
            }
        }

        public override object GetInitialTemporaryRandomValue()
        {
            var numValidator = this.WebControlFunctionality.GetOrCreateValidator<ValidatorNumeric>();
            if (numValidator != null)
            {
                return (double)numValidator.GenerateRandomValidValue();
            }
            else
            {
                return null;
            }
        }
    }
}
