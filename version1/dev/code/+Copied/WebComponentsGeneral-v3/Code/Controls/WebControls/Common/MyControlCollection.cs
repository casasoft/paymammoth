﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    public class MyControlCollection<TControl> : List<TControl> where TControl : Control
    {
        private Control _container;
        public MyControlCollection(Control container)
        {
            _container = container;
            
        }
        new public void Add(TControl td)
        {
            _container.Controls.Add(td);
            base.Add(td);
        }
        new public void AddRange(IEnumerable<TControl> tds)
        {
            foreach (var td in tds)
            {
                Add(td);
            }
        }
        new public void Insert(int index, TControl control)
        {
            _container.Controls.AddAt(index, control);
            base.Insert(index, control);
        }
        new public void InsertRange(int index, IEnumerable<TControl> controls)
        {
            int i = index;
            foreach(var control in controls)
            {
                this.Insert(i, control);
                i++;
            }
        }
        new public void Remove(TControl item)
        {
            _container.Controls.Remove(item);
            base.Remove(item);
        }
        
        new public void RemoveAt(int index)
        {
            _container.Controls.Remove(this[index]);
            base.RemoveAt(index);
        }
        new public void RemoveRange(int index, int count)
        {
            for (int i = index; i < index + count; i++)
            {
                _container.Controls.Remove(this[i]);
            }
            base.RemoveRange(index, count);
        }
    }
}