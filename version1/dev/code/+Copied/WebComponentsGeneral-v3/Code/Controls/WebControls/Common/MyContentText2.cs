using BusinessLogic_v3.Util;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using CSSManagerForControl = Classes.CSS.CSSManagerForControl;
    using CS.WebComponentsGeneralV3.Code.Classes.CSS;
    using BusinessLogic_v3.Modules.ContentTextModule;
    using BusinessLogic_v3.Classes.Text;
    using CS.General_v3.Util;

    public class MyContentText : PlaceHolder
    {


        #region MyContentText Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected MyContentText _item = null;

            private Control _containerControl;
            /// <summary>
            /// If you specify this control, the content text will be updated within this control itself 
            /// </summary>
            public Control ContainerControl
            {
                get { return _containerControl; }
                set
                {
                    _containerControl = value;
                    if (_containerControl != null)
                    {
                        _containerControl.Load += new EventHandler(_containerControl_Load);
                    }
                }
            }

            public Dictionary<string, string> ReplacementTags { get; set; }

            void _containerControl_Load(object sender, EventArgs e)
            {
                //Initialize the item
                _item.init();
            }

            public ContentTextBaseFrontend ContentText { get; set; }
            public string CssClass { get; set; }

            internal FUNCTIONALITY(MyContentText item)
            {
                this.ReplacementTags = new Dictionary<string, string>();
                this._item = item;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion


        private CSSManager _cssManager;

        public MyContentText()
            : base()
        {

        }
        private string replaceTags()
        {
            return this.Functionality.ReplacementTags != null && this.Functionality.ReplacementTags.Count > 0
                       ? BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(
                           this.Functionality.ContentText.Data, this.Functionality.ReplacementTags)
                       : this.Functionality.ContentText.Data.GetContent();


            /*var tokenReplacer = this.Functionality.ContentText.Data.CreateTokenReplacer();
            foreach (string contentTag in this.Functionality.ReplacementTags.Keys) 
            {
                string value = this.Functionality.ReplacementTags[contentTag];
                tokenReplacer[contentTag] = value;
            }

            return tokenReplacer.ReplaceString(content);*/
        }

        private void updateControlText(Control control, string content, bool html)
        {
            if (control is HtmlContainerControl)
            {
                if (html)
                {
                    ((HtmlContainerControl)control).InnerHtml = replaceTags();
                }
                else
                {
                    ((HtmlContainerControl)control).InnerText = replaceTags();
                }
            }
            else if (control is WebControl)
            {
                ((WebControl)control).Controls.Clear();
                if (html)
                {
                    ((WebControl)control).Controls.Add(new Literal() { Text = replaceTags() });
                }
                else
                {
                    ((WebControl)control).Controls.Add(new Literal() { Text = replaceTags(), Mode = LiteralMode.Encode });
                }
            }
            else
            {
                throw new NotSupportedException("Control of type " + control + " is not supported");
            }
        }

        public string CssClass
        {
            get { return this.Functionality.CssClass; }
            set { this.Functionality.CssClass = value; }
        }

        private void initPlainText()
        {
            if (this.Functionality.ContainerControl != null)
            {
                _cssManager = new CSSManager(this.Functionality.ContainerControl);
                updateControlText(this.Functionality.ContainerControl, this.Functionality.ContentText.Data.GetContent(), false);
            }
            else
            {
                MySpan span = new MySpan();
                _cssManager = span.CssManager;
                span.InnerText = replaceTags(); 
                this.Controls.Add(span);
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.CssClass))
            {
                _cssManager.AddClass(this.Functionality.CssClass);
            }
            _cssManager.AddClass("content-text-plain-text");


        }

        private void initHTML()
        {
            if (this.Functionality.ContainerControl != null)
            {
                _cssManager = new CSSManager(this.Functionality.ContainerControl);
                updateControlText(this.Functionality.ContainerControl, this.Functionality.ContentText.Data.GetContent(), true);
            }
            else
            {
                MySpan span = new MySpan();
                _cssManager = span.CssManager;
                span.InnerHtml = replaceTags();
                this.Controls.Add(span);
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.CssClass))
            {
                _cssManager.AddClass(this.Functionality.CssClass);
            }
            _cssManager.AddClass("content-text-html");
        }
        private void initImage()
        {
            MyImage img = new MyImage();
            _cssManager = img.CssManager;
            img.ImageUrl = this.Functionality.ContentText.Data.GetContent();
            img.AlternateText = this.Functionality.ContentText.Data.ImageAlternateText;
            if (this.Functionality.ContainerControl != null)
            {
                this.Functionality.ContainerControl.Controls.Add(img);
            }
            else
            {
                this.Controls.Add(img);
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.CssClass))
            {
                _cssManager.AddClass(this.Functionality.CssClass);
            }
            _cssManager.AddClass("content-text-image");
        }


        private void init()
        {
            if (this.Functionality.ContentText == null) throw new ArgumentNullException("Please specify ContentText");

            switch (this.Functionality.ContentText.Data.ContentType)
            {
                case BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText: initPlainText(); break;
                case BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html: initHTML(); break;
               // case BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Image: initImage(); break;
                default:
                    throw new NotImplementedException("This type is not yet supported '" + this.Functionality.ContentText.Data.ContentType + "'");
                    break;
            }
            _cssManager.AddClass("content-text-item");
        }

        protected override void OnLoad(EventArgs e)
        {
            if (this.Functionality.ContainerControl == null)
            {
                //Item stored in self
                init();
            }
            base.OnLoad(e);
        }

        public static void AttachContentTextWithControl(Control control, ContentTextBaseFrontend contentText, params string[] contentTagsAndValues)
        {
            MyContentText ct = new MyContentText();
            ct.Functionality.ContentText = contentText;
            ct.Functionality.ContainerControl = control;
            control.Controls.Add(ct);
        }
    }
}
