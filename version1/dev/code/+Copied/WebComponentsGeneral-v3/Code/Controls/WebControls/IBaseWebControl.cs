﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls
{
    using CS.General_v3.Classes.CSS;
    using System.Web.UI.WebControls;

    public interface IBaseWebControl : IBaseControl
    {
        new BaseWebControlFunctionality WebControlFunctionality { get; }
        new WebControl Control { get; }

    }
}
