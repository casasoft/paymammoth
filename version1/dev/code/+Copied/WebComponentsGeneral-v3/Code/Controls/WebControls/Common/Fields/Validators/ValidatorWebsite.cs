﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorWebsite : ValidatorBase
    {

        public new ValidatorWebsiteParamters Parameters { get { return (ValidatorWebsiteParamters)base.Parameters; } set { base.Parameters = value; } }

        protected override ValidatorBaseParameters createParameters()
        {
            return new ValidatorWebsiteParamters();
        }

        public override object GenerateRandomValidValue()
        {
            string str = "www." + CS.General_v3.Util.Random.GetString(10, 30) + ".com";
            return str;
        }
    }
}