namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Classes.Text;

    public class MyDropDownListItems : List<IMyDropdownListChildItem>
    {
        public MyDropDownListItems()
        {

        }
        public MyDropDownListItems(ListItemCollection items)
        {
            this.AddRange(items);
        }
        public void AddOption(string label, string value, bool selected = false, bool disabled = false)
        {
            var opt = new MyDropDownOption();
            opt.Functionality.ItemData.Label = label;
            opt.Functionality.ItemData.Value = value;
            opt.Functionality.ItemData.Selected = selected;
            opt.Functionality.ItemData.Disabled = disabled;
            AddOption(opt);
        }
        public void AddOption(ContentTextBaseFrontend label, string value, TokenReplacerNew replacementTags = null, bool selected = false, bool disabled = false)
        {
            var opt = new MyDropDownOption();
            opt.Functionality.ItemData.LabelContentText = label;
            opt.Functionality.ItemData.Value = value;
            opt.Functionality.ItemData.Selected = selected;
            opt.Functionality.ItemData.Disabled = disabled;
            opt.Functionality.ItemData.LabelContentTextReplacementTags = replacementTags;
            AddOption(opt);
        }
        public void AddOption(MyDropDownOption opt)
        {
            this.Add(opt);
        }
        public MyDropDownOption AddOption(ListItem opt)
        {
            var item = new MyDropDownOption();
            item.Functionality.ItemData.Label = opt.Text;
            item.Functionality.ItemData.Value = opt.Value;
            item.Functionality.ItemData.Selected = opt.Selected;
            item.Functionality.ItemData.Disabled = !opt.Enabled;
            AddOption(item);
            return item;
            
        }

        protected string convertObjectToStringValue(object o)
        {

            return CS.General_v3.Util.Other.ConvertBasicDataTypeToString(o);
        }

        public void AddOptGroup(MyDropDownListOptionGroup group, params MyDropDownOption[] options)
        {
            this.Add(group);
            if (options != null)
            {
                group.Functionality.Options.AddRange(options);
            }
        }
        public void SelectItemByValue(object value, bool clearCurrentSelectedItems = false)
        {
            SelectItemsByValue(new List<object>() { value }, clearCurrentSelectedItems);
        }
        public void SelectItemsByValue(IEnumerable<object> values, bool clearCurrentSelectedItems = false)
        {


            bool found = false;
            foreach (var item in this)
            {
                if (item is MyDropDownOption)
                {
                    
                    MyDropDownOption opt = (MyDropDownOption)item;
                    if (clearCurrentSelectedItems) opt.Functionality.Selected = false;
                    if (!found)
                    {
                        foreach (var value in values)
                        {
                            string sValue = convertObjectToStringValue(value);
                            if (opt.Functionality.Value == sValue)
                            {
                                opt.Functionality.Selected = true;
                                found = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        public void AddRange(ListItemCollection items)
        {
            
            foreach(ListItem item in items)
            {
                AddOption(item);
            }
        }
        public void AddRange(IEnumerable<ListItem> items)
        {
            foreach (ListItem item in items)
            {
                AddOption(item);
            }
        }
    }
}
