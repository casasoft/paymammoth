namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.IO;
    using Fields;
    using General_v3;
    using General_v3.Controls.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

    public class MyFileUpload : MyFormWebControl
    {
        private FileUpload _fileUpload = new FileUpload();

        public MyFileUpload(FieldSingleControlParameters fieldParams = null)
            : base(HtmlTextWriterTag.Div, fieldParams)
        {
            this.JSFieldTypeName = "FieldSingleControl";
        }


        protected override BaseFieldParameters createFieldParameters()
        {
            return new FieldSingleControlParameters();
        }

        protected override void updateInitialValue(object value)
        {
            //Can do nothing
        }

        public override string JSFieldTypeName
        {
            get;
            set;
        }

        public override string GetDataTypeAsString()
        {
            return "file-upload";
        }

        public override void SetFormNames()
        {
            this._fileUpload.Attributes["name"] = this.ClientID;
        }

        protected override void OnLoad(EventArgs e)
        {
            _fileUpload.ID = this.ID + "_fileUpload";
            this.Controls.Add(_fileUpload);

            base.OnLoad(e);
        }

        private Stream _uploadedFileStream = null;
        public Stream UploadedFileStream
        {
            get
            {
                //if (false && !CS.General_v3.FileUploader.UploadManager.Instance.Initialised)
                //{
                //    string errMsg = "File Upload Manager has not yet been initialised\r\n\r\n";
                //    errMsg += "It must be initialised at startup, ideally in 'global.asax'. Instructions and Sample code can be found\r\n";
                //    errMsg += "In 'FileUploader.FileSystemProcessor'.";

                //    throw new InvalidOperationException(errMsg);

                //}
                //Instance

                _uploadedFileStream = _fileUpload.FileContent;
                return _uploadedFileStream;
            }
        }

        public byte[] FileBytes
        {
            get { return _fileUpload.FileBytes; }
        }
        public bool HasUploadedContent()
        {
            return FileBytes != null && FileBytes.Length > 0;
        }

        public string FileName
        {
            get
            {
                return _fileUpload.FileName;
            }
        }
        
        protected override string getElementClientID()
        {
            return _fileUpload.ClientID;
        }
        public ValidatorStringParameters ValidatorStringParameters
        {
            get
            {
                ValidatorString validator = this.FieldParameters.GetOrCreateValidator<ValidatorString>();

                return validator.Parameters;
            }
        }

        public override object GetInitialTemporaryRandomValue()
        {
            return null;
        }
    }
        
}
