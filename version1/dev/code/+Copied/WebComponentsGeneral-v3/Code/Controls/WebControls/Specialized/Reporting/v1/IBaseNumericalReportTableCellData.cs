﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using CS.General_v3.Classes.CSS;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public interface IBaseNumericalReportTableCellData : IBaseReportTableCellDataBase
    {
        double Value { get; }
        int? DecimalPlaces { get; set;  }
        string ThousandsDelimiter { get; set; }
    }
}
