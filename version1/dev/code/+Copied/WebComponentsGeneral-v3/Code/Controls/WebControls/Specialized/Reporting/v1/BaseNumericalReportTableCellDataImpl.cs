﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public class BaseNumericalReportTableCellDataImpl : BaseReportTableCellDataBaseImpl, IBaseNumericalReportTableCellData
    {


        #region IBaseNumericalReportTableCellData Members

        public double Value
        {
            get;
            set;
        }
        public int? DecimalPlaces
        {
            get;
            set;
        }

        public string ThousandsDelimiter
        {
            get;
            set;
        }

        #endregion

        public override Control GetCellContent()
        {
            string value = null;
            if (this.DecimalPlaces.HasValue || this.ThousandsDelimiter != null)
            {
                value = CS.General_v3.Util.NumberUtil.FormatNumber(Value, this.ThousandsDelimiter,
                                                                     this.DecimalPlaces);
            }
            else
            {
                value = Value.ToString();
            }
            return new Literal() {Text = value};

        }






    }
}
