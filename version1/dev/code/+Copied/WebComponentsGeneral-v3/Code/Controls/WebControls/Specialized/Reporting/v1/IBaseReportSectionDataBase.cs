﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public interface IBaseReportSectionDataBase
    {
        int SectionNumber { get; set; }
        string Title { get;  }
        string DescriptionHTML { get; }
        string FooterHTML { get; }

        IBaseReportTableDataBase TableData { get; }

    }
}
