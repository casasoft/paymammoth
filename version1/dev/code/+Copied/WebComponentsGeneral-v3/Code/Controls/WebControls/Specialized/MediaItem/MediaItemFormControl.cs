﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.MediaItem
{
    using System;
    using BusinessLogic_v3.Classes.MediaItems;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public class MediaItemFormControl : MyFormWebControl
    {
        public delegate bool OnDeleteHandler(MediaItemFormControl sender, IMediaItem itemToDelete);
        public event OnDeleteHandler OnDelete_Before;
        public event EventHandler OnDelete_After;


        private MyDiv _divImage = null;
        private MyFileUpload _txtUpload = new MyFileUpload();
        private MyImage _imgMediaItem;
        private MyButton _btnDelete;
        public class MediaItemControlFunctionality
        {
            private MediaItemFormControl _ctrl = null;
            public MediaItemControlFunctionality(MediaItemFormControl ctrl)
            {
                this._ctrl = ctrl;
            }
            public bool IsRequired { get; set; }
            public IMediaItem MediaItem { get; set; }
            public MyFileUpload GetFileUploadControl()
            {
                return _ctrl._txtUpload;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            renderTable();
            base.OnLoad(e);
        }
        private void renderTable()
        {
            this.CssManager.AddClass("media-item-control");
            {
                if (this.Functionality.MediaItem != null && !this.Functionality.MediaItem.IsEmpty())
                {
                    MyDiv divImageContainer = new MyDiv();
                    divImageContainer.CssManager.AddClass("media-item-control-image-row clearfix");
                    this.Controls.Add(divImageContainer);
                    _imgMediaItem.ImageUrl = this.Functionality.MediaItem.GetThumbnailImageUrl();
                    _imgMediaItem.HRef = this.Functionality.MediaItem.GetUrl();
                    _imgMediaItem.HRefTarget = CS.General_v3.Enums.HREF_TARGET.Blank;
                    _divImage = new MyDiv();
                    _divImage.CssManager.AddClass("media-item-control-image");
                    _divImage.Controls.Add(_imgMediaItem);
                    divImageContainer.Controls.Add(_divImage);

                    _btnDelete.ValidationGroup = "Delete Image" + _imgMediaItem.ClientID;

                    if (!this.Functionality.IsRequired)
                    {
                        MyDiv divDeleteImage = new MyDiv();
                        divDeleteImage.CssManager.AddClass("media-item-control-delete");
                        divDeleteImage.Controls.Add(_btnDelete);
                        divImageContainer.Controls.Add(divDeleteImage);
                    }
                }
            }
            {
                MyDiv divFileUpload = new MyDiv();
                divFileUpload.CssManager.AddClass("media-item-control-upload");
                divFileUpload.Controls.Add(_txtUpload);
                this.Controls.Add(divFileUpload);
            }
        }

        protected virtual MediaItemControlFunctionality createFunctionality()
        {
            return new MediaItemControlFunctionality(this);
        }
        public MediaItemControlFunctionality Functionality { get; private set; }
        private void initControls()
        {
            _txtUpload.ID = this.ID + "txtFileUpload_MediaItem";
            _imgMediaItem = new MyImage();
            _btnDelete = new MyButton(this.ID + "_btnDelete");
            _btnDelete.Text = "Delete";
            _btnDelete.Click += new EventHandler(btnDelete_Click);

        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            bool delete = true;
            if (this.OnDelete_Before != null)
                delete = OnDelete_Before(this, this.Functionality.MediaItem);
            if (delete)
            {
                this.Functionality.MediaItem.Delete();
                _divImage.Visible = false;
                if (OnDelete_After != null)
                    OnDelete_After(this, e);
            }


        }
        protected MyFormWebControlFunctionality _formWebControlFunctionality = null;

        public MediaItemFormControl()
            : this(null, null)
        {

        }
        public MediaItemFormControl(string id, IMediaItem item)
        {
            this.ID = id;
            _formWebControlFunctionality = new MyFormWebControlFunctionality(this);
            // _formWebControlFunctionality.JSFieldClassName = "FieldMediaItem";
            this.DoNotAddFieldJS = true;
            this.Functionality = createFunctionality();
            this.Functionality.MediaItem = item;
            initControls();
        }



        protected override Common.Fields.BaseFieldParameters createFieldParameters()
        {

            return new Common.Fields.FieldSingleControlParameters();
        }

        protected override void updateInitialValue(object value)
        {
            if (value is IMediaItem)
            {
                this.Functionality.MediaItem = (IMediaItem)value;
            }
            _txtUpload.WebControlFunctionality.InitialValue = value;
        }

        public override string JSFieldTypeName
        {
            get { return this._txtUpload.JSFieldTypeName; }
            set
            {
                this._txtUpload.JSFieldTypeName = value;
            }


        }

        public override string GetDataTypeAsString()
        {
            return "media-item";
        }

        public override void SetFormNames()
        {
            this._txtUpload.SetFormNames();
        }

        protected override string getElementClientID()
        {
            return this._txtUpload.ClientID;
        }

        public override object GetInitialTemporaryRandomValue()
        {
            return null;
        }
    }
}
