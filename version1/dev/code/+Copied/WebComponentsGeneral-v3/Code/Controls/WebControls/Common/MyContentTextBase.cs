using BusinessLogic_v3.Util;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using CSSManagerForControl = Classes.CSS.CSSManagerForControl;
    using CS.WebComponentsGeneralV3.Code.Classes.CSS;
    using BusinessLogic_v3.Modules.ContentTextModule;
    using BusinessLogic_v3.Classes.Text;
    using CS.General_v3.Util;
    using BusinessLogic_v3.Modules.CmsUserModule;
    using CS.WebComponentsGeneralV3.Code.Cms.Classes;

    public class MyContentTextBase
    {
        private bool _init = false;
        public Control Control { get; set; }


        public ContentTextBaseFrontend ContentText { get; set; }
        public bool RequiresAtLeastOneParagraphTag { get; set; }



        public MyContentTextBase(Control control, ContentTextBaseFrontend contentText, TokenReplacerNew replacementTags = null, bool requiresAtLeastOneParagraphTag = false)
        {
            this.Control = control;
            this.RequiresAtLeastOneParagraphTag = requiresAtLeastOneParagraphTag;
            this.ContentText = contentText;
            
            if (this.ContentText == null)
            {
                throw new ArgumentNullException("Argument contentText cannot be null");
            }
            if (replacementTags != null)
            {
                contentText.UpdateTokenReplacer(replacementTags);
            }
            
            this.Control.Load += new EventHandler(Control_Load);
            this.Control.PreRender += new EventHandler(Control_PreRender);
        }
        public void ForceRender()
        {
            if (!_init)
            {
                init();
            }
        }

        void Control_PreRender(object sender, EventArgs e)
        {
            if (!_init)
            {
                init();
            }
        }

        void Control_Load(object sender, EventArgs e)
        {
            if (!_init)
            {
                init();
            }
        }

        private string replaceTags()
        {
            return this.ContentText.GetContent();

            

        }

        private void updateControlText()
        {
            bool html = this.ContentText.Data.ContentType == BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html;
            string value = replaceTags();
            if (!html)
            {
                html = this.ContentText.Data._Computed_IsHtml;
            }

            if (Control is HtmlContainerControl)
            {
                if (html)
                {
                    ((HtmlContainerControl)Control).InnerHtml = value;
                }
                else
                {
                    ((HtmlContainerControl)Control).InnerText = value;
                }
            }
            else if (Control is WebControl)
            {
                ((WebControl)Control).Controls.Clear();
                if (html)
                {
                    ((WebControl)Control).Controls.Add(new Literal() { Text = value });
                }
                else
                {
                    ((WebControl)Control).Controls.Add(new Literal() { Text = value, Mode = LiteralMode.Encode });
                }
            }
            else
            {
                throw new NotSupportedException("Control of type " + Control + " is not supported");
            }
        }

        //private void initPlainText()
        //{
        //    _cssManager = new CSSManager(Control);
        //    updateControlText();
        //    _cssManager.AddClass("content-text-plain-text");


        //}

        //private void initHTML()
        //{
        //    if (this.Functionality.ContainerControl != null)
        //    {
        //        _cssManager = new CSSManager(this.Functionality.ContainerControl);
        //        updateControlText(this.Functionality.ContainerControl, this.Functionality.ContentText.Data.GetContent(), true);
        //    }
        //    else
        //    {
        //        MySpan span = new MySpan();
        //        _cssManager = span.CssManager;
        //        span.InnerHtml = replaceTags();
        //        this.Controls.Add(span);
        //    }
        //    if (!string.IsNullOrWhiteSpace(this.Functionality.CssClass))
        //    {
        //        _cssManager.AddClass(this.Functionality.CssClass);
        //    }
        //    _cssManager.AddClass("content-text-html");
        //}
        //private void initImage()
        //{
        //    MyImage img = new MyImage();
        //    _cssManager = img.CssManager;
        //    img.ImageUrl = this.Functionality.ContentText.Data.GetContent();
        //    img.AlternateText = this.Functionality.ContentText.Data.ImageAlternateText;
        //    if (this.Functionality.ContainerControl != null)
        //    {
        //        this.Functionality.ContainerControl.Controls.Add(img);
        //    }
        //    else
        //    {
        //        this.Controls.Add(img);
        //    }
        //    if (!string.IsNullOrWhiteSpace(this.Functionality.CssClass))
        //    {
        //        _cssManager.AddClass(this.Functionality.CssClass);
        //    }
        //    _cssManager.AddClass("content-text-image");
        //}
        private void initCMSInlineEditing()
        {
            //CMSInlineEditingItem item = new CMSInlineEditingItem((IAttributeAccessor)this.Control, this.ContentText.Data, BusinessLogic_v3.Enums.CMS_INLINE_EDITING_TYPE.ContentText, this.ContentText.Data.GetContent(), this.ReplacementTags);
            /*(BusinessLogic_v3.Enums.TEXT_TYPE textType = BusinessLogic_v3.Enums.TEXT_TYPE.PlainText;
            if (this.ContentText.Data.ContentType == BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html)
            {

            }*/
            BusinessLogic_v3.Enums.INLINE_EDITING_TEXT_TYPE textType = BusinessLogic_v3.Enums.INLINE_EDITING_TEXT_TYPE.PlainText;
            if (this.ContentText.Data.ContentType == BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html)
            {
                textType = BusinessLogic_v3.Enums.INLINE_EDITING_TEXT_TYPE.HtmlWithAtLeastParagraphTag;
                if (this.RequiresAtLeastOneParagraphTag)
                {
                    textType = BusinessLogic_v3.Enums.INLINE_EDITING_TEXT_TYPE.HtmlWithAtLeastParagraphTag;
                }
            }

            CMSInlineEditingItem item = CMSInlineEditingItem.Create(this.ContentText.Data, x => x._Content_ForEditing, (IAttributeAccessor)this.Control,
                this.ContentText.Data._Content_ForEditing, this.ContentText.TokenReplacer, textType);

        }



        private void init()
        {
            _init = true;
            if (this.ContentText == null) throw new ArgumentNullException("Please specify ContentText");
            if (this.Control == null) throw new ArgumentNullException("Please specify Control");
            updateControlText();

            if (CmsUserSessionLogin.Instance.IsLoggedIn)
            {
                CSSManager cssManager = new CSSManager(this.Control);
                switch (this.ContentText.Data.ContentType)
                {
                    case BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText:
                        cssManager.AddClass("content-text-item-plain-text");
                        break;
                    case BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html:
                        cssManager.AddClass("content-text-item-html-text");
                        break;
                        // case BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Image: initImage(); break;
                    default:
                        throw new NotImplementedException("This type is not yet supported '" + this.ContentText.Data.ContentType + "'");
                        break;
                }
                cssManager.AddClass("content-text-item");
                initCMSInlineEditing();
                //initDataAttribute();
            }
        }
    }
}
