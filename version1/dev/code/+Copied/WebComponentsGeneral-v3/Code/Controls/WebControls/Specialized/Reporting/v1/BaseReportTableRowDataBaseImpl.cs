﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public abstract class BaseReportTableRowDataBaseImpl :IBaseReportTableRowDataBase
    {

        public BaseReportTableRowDataBaseImpl()
        {
            this.CustomCss = new CSSManager();
        }


        #region IBaseReportTableRowData Members

        protected abstract IEnumerable<BaseReportTableCellDataBaseImpl> initCells();

        private IEnumerable<BaseReportTableCellDataBaseImpl> _cells;


        public IEnumerable<BaseReportTableCellDataBaseImpl> Cells
        {
            get { return _cells ?? (_cells = initCells()); }
        }

        #endregion

        public CSSManager CustomCss { get; private set; }



        #region IBaseReportTableRowDataBase Members

        IEnumerable<IBaseReportTableCellDataBase> IBaseReportTableRowDataBase.Cells
        {
            get { return this.Cells; }
        }

        #endregion
    }
}
