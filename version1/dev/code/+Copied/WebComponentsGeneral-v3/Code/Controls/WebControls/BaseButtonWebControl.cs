﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls
{
    using System;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public abstract class BaseButtonWebControl : BaseWebControl, IBaseButtonWebControl, IPostBackEventHandler
    {
        public object Tag { get; set; }

        public event EventHandler Click
        {
            add
            {
                this.WebControlFunctionality.Click += value;
            }
            remove
            {
                this.WebControlFunctionality.Click -= value;
            }
        }

        public BaseButtonWebControl(string id)
            : base()
        {
            init(id);
        }
        public BaseButtonWebControl(System.Web.UI.HtmlTextWriterTag tagName, string id)
            : base(tagName)
        {
            init(id);

        }
        public BaseButtonWebControl(string tagName, string id)
            : base(tagName)
        {
            init(id);


        }

        protected override BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new BaseButtonWebControlFunctionality(this);
        }





        private void init(string ID)
        {
            this.ID = ID;



        }



        public void RaisePostBackEvent(string eventArgument)
        {
            switch (eventArgument)
            {
                case "Click":
                    this.WebControlFunctionality.TriggerClickEvent();
                    break;
            }
        }


        public new Control Control
        {
            get { return this; }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get { return (BaseControlFunctionality)base.WebControlFunctionality; }
        }


        public new BaseButtonWebControlFunctionality WebControlFunctionality
        {
            get { return (BaseButtonWebControlFunctionality)base.WebControlFunctionality; }
        }

        public abstract string Text { get; set; }

        public string ValidationGroup { get { return this.WebControlFunctionality.ButtonParams.validationGroup; } set { this.WebControlFunctionality.ButtonParams.validationGroup = value; } }

    }
}
