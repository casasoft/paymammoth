using System.ComponentModel;
using System.Web;
using System.Web.UI;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls;
using System.Web.UI.WebControls;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyEmail runat=server></{0}:MyEmail>")]
    public class MyEmail : MySpan 
    {
        public MyEmail()
        {
            this.ConvertEmailToAntiSpam = true;
            this.MakeAnchorLink = true;
        }

        public bool ConvertEmailToAntiSpam { get; set; }

        public bool MakeAnchorLink { get; set; }
        
        public string EmailSubject { get; set; }

        public string Email { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        
        private void outputEmail()
        {
            this.ConvertEmailToAntiSpam = true;
            string data;
            string txt = this.Text;
            if (txt == "" || txt == null)
                txt = this.Email;
            if (HttpContext.Current != null)
            {
                if (ConvertEmailToAntiSpam)
                {

                    data = CS.General_v3.Util.Text.EncodeEmailForSpam(this.Email, txt, this.EmailSubject,
                        this.MakeAnchorLink, this.CssClass, Title);
                }
                else
                {
                    data = this.Text;
                }
            }
            else
            {
                data = txt;
            }
            this.Controls.Add(new Literal() { Text = data });
        }

        protected override void OnLoad(System.EventArgs e)
        {

            outputEmail();
            base.OnLoad(e);
        }
        
    }
}
