﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public class BaseReportTableDataImpl : BaseReportTableDataBaseImpl, IBaseReportTableData
    {
        public BaseReportTableDataImpl()
        {
        }


        protected override IEnumerable<BaseReportTableRowDataBaseImpl> createDataRows()
        {
            return new List<BaseReportTableRowDataImpl>();
        }


        public new List<BaseReportTableHeaderDataImpl> HeadersTop
        {
            get { return (List<BaseReportTableHeaderDataImpl>)base.HeadersTop; }
        }
        public new List<BaseReportTableHeaderDataImpl> HeadersLeft
        {
            get { return (List<BaseReportTableHeaderDataImpl>)base.HeadersLeft; }
        }
        public void AddHeaderTop(Control headerContent, Control totalContent)
        {
            var h = new BaseReportTableHeaderDataImpl() ;
            h.HeaderCellContent.Control = headerContent;
            h.TotalCellContent.Control = totalContent;

            this.HeadersTop.Add(h);
        }

        public void AddHeaderTop(string s)
        {
            LiteralControl lit = new LiteralControl();
            lit.Text = s;
            AddHeaderTop(lit, null);
        }

        #region IBaseReportTableData Members

        public BaseReportTableCellDataImpl TotalBothHeadersCell
        {
            get;
            set;
        }

        public new List<BaseReportTableRowDataImpl> DataRows
        {
            get { return (List<BaseReportTableRowDataImpl>)base.DataRows; }
        }

        #endregion

        #region IBaseReportTableData Members

        IBaseReportTableCellData IBaseReportTableData.TotalBothHeadersCell
        {
            get { return this.TotalBothHeadersCell; }
        }

        IEnumerable<IBaseReportTableRowData> IBaseReportTableData.DataRows
        {
            get { return this.DataRows; }
        }

        #endregion



        protected override IEnumerable<BaseReportTableHeaderDataBaseImpl> createHeaderRows()
        {
            return new List<BaseReportTableHeaderDataImpl>();
        }

        #region IBaseReportTableData Members

        IEnumerable<IBaseReportTableHeaderData> IBaseReportTableData.HeadersTop
        {
            get { return this.HeadersTop; }
        }

        IEnumerable<IBaseReportTableHeaderData> IBaseReportTableData.HeadersLeft
        {
            get { return this.HeadersLeft; }
        }

        #endregion

    }
}
