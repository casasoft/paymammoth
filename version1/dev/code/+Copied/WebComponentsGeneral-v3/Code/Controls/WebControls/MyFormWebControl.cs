﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Common.Fields;
    using CS.General_v3.Util;
    using JSUtil = Util.JSUtil;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

    public abstract class MyFormWebControl : BaseWebControl, IMyFormWebControl
    {

        public object Tag { get; set; }

        public MyFormWebControl(BaseFieldParameters fieldParameters = null)
            : base()
        {
            init(fieldParameters);
            this.AutoComplete = false;
        }
        public MyFormWebControl(System.Web.UI.HtmlTextWriterTag tagName, BaseFieldParameters fieldParameters = null)
            : base(tagName)
        {
            init(fieldParameters);
        }
        public MyFormWebControl(string tagName, BaseFieldParameters fieldParameters = null)
            : base(tagName)
        {
            init(fieldParameters);
        }
        private void init(BaseFieldParameters fieldParameters)
        {
            this._fieldParameters = fieldParameters;
        }

        protected override BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new MyFormWebControlFunctionality(this);
        }

        protected abstract BaseFieldParameters createFieldParameters();

        protected abstract void updateInitialValue(object value);

        public new MyFormWebControlFunctionality WebControlFunctionality
        {
            get
            {
                return (MyFormWebControlFunctionality)base.WebControlFunctionality;
            }
        }




        private BaseFieldParameters _fieldParameters;
        public BaseFieldParameters FieldParameters
        {
            get
            {
                if (_fieldParameters == null)
                {
                    _fieldParameters = createFieldParameters();
                }
                return _fieldParameters;
            }
            set
            {
                _fieldParameters = value;
            }
        }

        public bool GetFormValueAsBool()
        {
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<bool>(GetFormValueAsString());
        }

        public bool? GetFormValueAsBoolNullable()
        {
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<bool?>(GetFormValueAsString());
        }

        public double? GetFormValueAsDoubleNullable()
        {
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<double?>(GetFormValueAsString());
        }

        public long? GetFormValueAsLongNullable()
        {
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<long?>(GetFormValueAsString());
        }

        public int GetFormValueAsInt()
        {
            return (int)GetFormValueAsLongNullable().GetValueOrDefault();
        }
        public string GetFormValueAsString()
        {
            return this.WebControlFunctionality.GetFormValue();
        }
        public T? GetFormValueAsEnumNullable<T>() where T : struct, IConvertible
        {
            return CS.General_v3.Util.EnumUtils.EnumValueNullableOf<T>(GetFormValueAsString());
        }
        public T GetFormValueAsEnum<T>(T defaultValue = default(T)) where T : struct, IConvertible
        {
            return CS.General_v3.Util.EnumUtils.EnumValueOf<T>(GetFormValueAsString(), defaultValue:defaultValue);
        }
        private void updateAttributesFromFieldParameters()
        {
            //Update it just in case
            this.Required = FieldParameters.required;
            this.PlaceholderText = FieldParameters.placeholderText;

            if (string.IsNullOrEmpty(FieldParameters.id)) FieldParameters.id = this.ClientID; // update it just in case

            this.ID = FieldParameters.id;

            if (this.ReadOnly.GetValueOrDefault())
            {
                this.CssManager.AddClass("field-item-readonly");
            }
            //  this.Form = _fieldParameters.validationGroup;
            //this.Value = this.WebControlFunctionality.InitialValue.ToString();
        }
        private void checkFillInTempRandomValues()
        {
            if (this.WebControlFunctionality.InitialValue == null && !this.WebControlFunctionality.NeverFillWithRandomValue)
            {
                
                bool fillTempValues = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_FillFormsWithRandomValues);
                if (fillTempValues)
                {
                    if (this.FieldParameters.required)
                    {
                        this.WebControlFunctionality.InitialValue = GetInitialTemporaryRandomValue();
                    }
                }

                var validatorGroup = this.WebControlFunctionality.GetValidator<ValidatorFieldGroup>();
                if (validatorGroup != null)
                {
                    this.WebControlFunctionality.InitialValue = validatorGroup.GenerateRandomValidValue();
                }
            }
        }

        protected override void OnLoad(System.EventArgs e)
        {
            if (this.Visible)
            {
                if (!this.AutoComplete.HasValue)
                {
                    this.AutoComplete = false;
                }
                updateAttributesFromFieldParameters();
               
                SetFormNames();
                checkFillInTempRandomValues();
            }

            base.OnLoad(e);
        }

        public override string ID
        {
            get
            {
                return this.FieldParameters.id;
            }
            set
            {
                base.ID = this.FieldParameters.id = value;
            }
        }
        public override string ClientID
        {
            get
            {
                //this.ID = base.ID; //refresh
                return base.ClientID;
            }
        }
        public override string UniqueID
        {
            get
            {
               // this.ID = base.ID; //refresh
                return base.UniqueID;
            }
        }



        private void initJS()
        {
            if (this.Visible && !this.DoNotAddFieldJS)
            {
                if (this.FieldParameters is FieldSingleControlParameters)
                {
                    //update it
                    ((FieldSingleControlParameters)this.FieldParameters).elementID = getElementClientID();
                }

                string parameters = this.FieldParameters.GetJSON();
                
                string js = "new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.Forms.Fields." + JSFieldTypeName + "(" + parameters + ");\r\n";

                JSUtil.AddJSScriptToPage(js);
            }
        }
      

        protected override void OnPreRender(System.EventArgs e)
        {
            if (this.Visible)
            {
                if (this.LoadInitialValueFromPostback)
                {
                    this.WebControlFunctionality.LoadInitialValueFromPostback();
                }

                initJS();
                if (this.WebControlFunctionality.InitialValue != null)
                {
                    updateInitialValue(this.WebControlFunctionality.InitialValue);
                }
            }
            base.OnPreRender(e);
        }

        public bool DoNotAddFieldJS { get; set; }
        public abstract string JSFieldTypeName { get; set; }

        public bool Required
        {
            get
            {
                return getAttributeAsBool("required");
            }
            set
            {
                setAttributeFromBool("required", value);
                FieldParameters.required = value;
            }
        }
        public virtual bool? ReadOnly
        {
            get
            {
                
                return getAttributeAsBoolNullable("readonly");
            }
            set
            {
                setAttributeFromBool("readonly", value);
            }
        }

        public bool? AutoFocus { get { return getAttributeAsBoolNullable("autofocus"); } set { setAttributeFromBool("autofocus", value, "on", "off"); } }
        public bool? AutoComplete { get { return getAttributeAsBoolNullable("autocomplete"); } set { setAttributeFromBool("autocomplete", value, "on", "off"); } }
        public bool? Disabled { get { return getAttributeAsBoolNullable("disabled"); } set { setAttributeFromBool("disabled", value); } }
        public virtual string Value { get { return getAttributeAsString("value"); } set { setAttributeFromString("value", value); } }
        public string Form { get { return getAttributeAsString("form"); } set { setAttributeFromString("form", value); } }

        public string PlaceholderText
        {
            get
            {
                return getAttributeAsString("placeholder");
            }
            set
            {
                _fieldParameters.placeholderText = value;
                setAttributeFromString("placeholder", value);
            }
        }



        public virtual object GetFormValueObject()
        {
            
            return this.GetFormValueAsString();
        }



        public abstract string GetDataTypeAsString();

        /// <summary>
        /// This method is called so that you set the name attribute
        /// </summary>
        public abstract void SetFormNames();

        protected abstract string getElementClientID();

        public string ValidationGroup { get { return FieldParameters.validationGroup; } set { this.FieldParameters.validationGroup = value; } }
          


        public bool LoadInitialValueFromPostback
        {
            get;
            set;
        }

        public abstract object GetInitialTemporaryRandomValue();

        public bool NeverFillWithRandomValue { get { return this.WebControlFunctionality.NeverFillWithRandomValue; } set { this.WebControlFunctionality.NeverFillWithRandomValue = value; } }

        }
}
