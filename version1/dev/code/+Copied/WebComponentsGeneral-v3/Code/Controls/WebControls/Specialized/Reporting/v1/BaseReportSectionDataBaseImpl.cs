﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public abstract class BaseReportSectionDataBaseImpl : IBaseReportSectionDataBase
    {
        public BaseReportSectionDataBaseImpl()
        {
        }

        #region IBaseReportSectionData Members

        public string Title
        {
            get;
            set;
        }

        public string DescriptionHTML
        {
            get;
            set;
        }
        public string FooterHTML
        {
            get;
            set;
        }
        public BaseReportTableDataBaseImpl TableData { get; set; }

        IBaseReportTableDataBase IBaseReportSectionDataBase.TableData
        {
            get { return this.TableData; }
        }

        #endregion



        #region IBaseReportSectionDataBase Members

        public int SectionNumber { get; set; }

        #endregion
    }
}
