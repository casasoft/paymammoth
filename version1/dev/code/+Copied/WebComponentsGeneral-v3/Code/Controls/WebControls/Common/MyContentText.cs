using BusinessLogic_v3.Classes.Text;
using BusinessLogic_v3.Util;
using BusinessLogic_v3.Frontend.ContentTextModule;
using System.Collections.Generic;
using BusinessLogic_v3.Extensions;
namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System.Web.UI;
    using BusinessLogic_v3.Modules.ContentTextModule;


    public class MyContentText : BaseWebControl
    {
        private MyContentTextBase _contentTextBase;
        public string CssClass
        {
            get { return this.CssManager.ToString(); }
            set
            {
                this.CssManager.Clear();
                this.CssManager.AddClass(value);
            }
        }

            #region MyContentText Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected MyContentText _item = null;
            private ContentTextBaseFrontend _contentText;
            public ContentTextBaseFrontend ContentText
            {
                get { return _contentText; }
                set
                {
                    _contentText = value;
                    if (value != null)
                    {
                        if (_contentText.TokenReplacer.Count > 0)
                        {
                            this.ReplacementTags = _contentText.TokenReplacer;
                        }
                        else
                        {
                            this.ReplacementTags.AddItemWithContentTag(value.Data);
                        }
                    }
                }
            }

            public TokenReplacerNew ReplacementTags { get; set; }
            internal FUNCTIONALITY(MyContentText item)
            {
                this._item = item;
                this.ReplacementTags = new TokenReplacerNew();
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        protected override void OnLoad(System.EventArgs e)
        {
            _contentTextBase = new MyContentTextBase(this, this.Functionality.ContentText, this.Functionality.ReplacementTags);
            base.OnLoad(e);
        }


        public static MyContentTextBase AttachContentTextWithControl(Control control, ContentTextBase contentText, TokenReplacerNew replacementTags = null, bool requiresAtLeastOneParagraphTag = false)
        {
            return AttachContentTextWithControl(control, contentText.ToFrontendBase(), replacementTags, requiresAtLeastOneParagraphTag);
        }

        public static MyContentTextBase AttachContentTextWithControl(Control control, ContentTextBaseFrontend contentText, TokenReplacerNew replacementTags = null, bool requiresAtLeastOneParagraphTag = false)
        {
            return new MyContentTextBase(control, contentText, replacementTags, requiresAtLeastOneParagraphTag);
        }
    }
}
