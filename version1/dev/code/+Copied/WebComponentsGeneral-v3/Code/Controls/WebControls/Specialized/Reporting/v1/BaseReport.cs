﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI;
using System.ComponentModel;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:BaseReport runat=server></{0}:BaseReport>")]
    public class BaseReport : MyDiv
    {

        public class FUNCTIONALITY
        {
            public string Title { get; set; }
            public string LogoImageURL { get; set; }
            public string DescriptionHTML { get; set; }
            public string FooterHTML { get; set; }
            public bool ShowGeneratedOn { get; set; }
            private int _lastSectionID = 0;
            private List<IBaseReportSectionDataBase> sections { get; set; }
            public IEnumerable<IBaseReportSectionDataBase> GetSections()
            {
                return sections;
            }

            public void AddSection(IBaseReportSectionDataBase section)
            {
                _lastSectionID ++;
                section.SectionNumber = _lastSectionID;
                sections.Add(section);
            }
            private BaseReport _control;
            public FUNCTIONALITY(BaseReport control)
            {
                _control = control;
                this.sections = new List<IBaseReportSectionDataBase>();
                this.ShowGeneratedOn = true;
            }
        }
        private FUNCTIONALITY _functionality;
        public virtual FUNCTIONALITY Functionality
        {
            get
            {
                if (_functionality == null)
                {
                    _functionality = getFunctionality();
                }
                return _functionality;
            }
        }
        public BaseReport()
        {
            
        }

        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private MyDiv getGeneratedOnDiv(string extraCSS)
        {
            MyDiv divGeneratedOn = new MyDiv();
            divGeneratedOn.CssManager.AddClass("cs-report-generated-on");
            divGeneratedOn.CssManager.AddClass(extraCSS);
            divGeneratedOn.InnerHtml = "generated on " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
            return divGeneratedOn;

        }
        private void initHeader()
        {
            if (this.Functionality.ShowGeneratedOn)
            {
                this.Controls.Add(getGeneratedOnDiv("cs-report-generated-on-top"));
            }
            //header
            MyDiv divHeader = new MyDiv();
            divHeader.CssManager.AddClass("cs-report-header-container");
            if (!string.IsNullOrWhiteSpace(this.Functionality.LogoImageURL))
            {
                MyImage imgLogo = new MyImage();
                imgLogo.ImageUrl = this.Functionality.LogoImageURL;
                imgLogo.CssManager.AddClass("cs-report-logo");
                divHeader.Controls.Add(imgLogo);
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.Title))
            {
                MyHeading hTitle = new MyHeading(1, this.Functionality.Title);
                hTitle.CssManager.AddClass("cs-report-title");
                divHeader.Controls.Add(hTitle);
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.DescriptionHTML))
            {
                MyDiv divDesc = new MyDiv();
                divDesc.CssManager.AddClass("cs-report-main-description-container");
                divDesc.InnerHtml = this.Functionality.DescriptionHTML;
                divHeader.Controls.Add(divDesc);
            }
            this.Controls.Add(divHeader);
        }

        private BaseReportSectionBase getSectionFromSectionData(IBaseReportSectionDataBase data)
        {
            BaseReportSectionBase section = null;
            if (data is IBaseReportSectionData)
            {

                return  new BaseReportSection();
                
            }
            else if (data is IBaseNumericalReportSectionData)
            {
                return new BaseNumericalReportSection();
            }
            else
            {
                throw new NotSupportedException("Please add more options to this method");
            }
            return null;
        }



        private void initSections()
        {
            MyDiv divSections = new MyDiv();
            divSections.CssManager.AddClass("cs-report-sections-container");
            this.Controls.Add(divSections);
            var sections = this.Functionality.GetSections();
            if (sections != null)
            {
                foreach (var sectionItem in sections)
                {
                    BaseReportSectionBase section = getSectionFromSectionData(sectionItem);
                    section.Functionality.Section = sectionItem;
                    divSections.Controls.Add(section);
                }
            }

        }
        private void initFooter()
        {
            
            if (!string.IsNullOrWhiteSpace(this.Functionality.FooterHTML))
            {
                MyDiv divFooter = new MyDiv();
                divFooter.CssManager.AddClass("cs-report-footer-container");
                divFooter.InnerHtml = this.Functionality.FooterHTML;
                this.Controls.Add(divFooter);
            }
            if (this.Functionality.ShowGeneratedOn)
            {
                this.Controls.Add(getGeneratedOnDiv("cs-report-generated-on-bottom"));
            }
        }

        private void init()
        {
            this.CssManager.AddClass("cs-report-container");
            initHeader();
            initSections();
            initFooter();

        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }
    }
}
