﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorNumericParameters : ValidatorBaseParameters
    {
        public bool positiveOnly = false;
        public bool negativeOnly = false;
        public bool integersOnly = false;
        public double? numFrom = null;
        public double? numTo = null;
        
    }
}