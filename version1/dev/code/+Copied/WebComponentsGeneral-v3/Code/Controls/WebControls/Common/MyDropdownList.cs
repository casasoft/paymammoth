namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using System.Collections;
    using CS.WebComponentsGeneralV3.Code.Util;
    using CS.WebComponentsGeneralV3.Code.Cms.Classes;

    public class MyDropDownList : MyFormWebControl
    {

        

        #region MyDropdownList Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {

            public MyDropDownListItems Items { get; set; }
            public bool OnChangeRedirectToValue { get; set; }

            protected MyDropDownList _item = null;
            internal FUNCTIONALITY(MyDropDownList item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "Item of MyDropdownList cannot be null");
                this._item = item;
                this.Items = new MyDropDownListItems();
            }

            public void AddItemFromListItem(ListItem item)
            {
                var opt = new MyDropDownOption();
                opt.Functionality.Label = item.Text;
                opt.Functionality.Selected = item.Selected || (_item.InitialValue != null && item.Value == _item.InitialValue.ToString());
               

                opt.Functionality.Disabled = !item.Enabled;
                opt.Functionality.Value = item.Value;
                this.Items.AddOption(opt);
            }
            public void AddItemsFromListItemCollection(ListItemCollection items)
            {
                foreach (ListItem item in items)
                {
                    AddItemFromListItem(item);
                }
            }
            
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			
       
		
			

        

        public MyDropDownList(FieldSingleControlParameters fieldParameters = null)
            : base(HtmlTextWriterTag.Select, fieldParameters)
        {
            this.JSFieldTypeName = "FieldSingleControl";
        }



        protected override BaseFieldParameters createFieldParameters()
        {
            return new FieldSingleControlParameters();
        }

        public new FieldSingleControlParameters FieldParameters
        {
            get
            {
                return (FieldSingleControlParameters)base.FieldParameters;
            }
            set { base.FieldParameters = value; }
        }

        private void initRedirectJS()
        {
            if (this.Functionality.OnChangeRedirectToValue)
            {
                string jsID = "'#" + this.ClientID + "'";
                string js = @"jQuery(" + jsID + @").change(function() { window.location = jQuery(" + jsID + @").val(); });";
                JSUtil.AddJSScriptToPage(js);
            }
        }

        private void addChildControls(bool forceRender)
        {
            if (this.Functionality.Items != null)
            {
                foreach (var item in this.Functionality.Items)
                {
                    this.Controls.Add(item.Control);
                    if (forceRender)
                    {
                        item.Control.ForceRender();
                    }
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            renderControls(false);
            initCMSEditable();
            base.OnLoad(e);
        }

        public override string JSFieldTypeName
        {
            get;
            set;
        }




        protected override void updateInitialValue(object value)
        {
            if (value != null)
            {
                if (value is IEnumerable<string>)
                {
                    this.Functionality.Items.SelectItemsByValue((IEnumerable<string>)value, true);
                }
                else
                {
                    this.Functionality.Items.SelectItemByValue(value, true);
                }
            }
        }

        public override string GetDataTypeAsString()
        {
            return "dropdownlist";
        }

        public override void SetFormNames()
        {
            this.setAttributeFromString("name", this.ClientID);
        }
        public object InitialValue { get { return this.WebControlFunctionality.InitialValue; } set { this.WebControlFunctionality.InitialValue = value; } }
        public bool OnChangeRedirectToValue { get { return this.Functionality.OnChangeRedirectToValue; } set { this.Functionality.OnChangeRedirectToValue = value; } }

        protected override string getElementClientID()
        {
            return this.ClientID;
        }
        public TEnumType GetFormValueAsEnum<TEnumType>() where TEnumType: struct, IConvertible
        {
            return CS.General_v3.Util.EnumUtils.EnumValueOf<TEnumType>(this.GetFormValueAsString());
        }
        public TEnumType? GetFormValueAsEnumNullable<TEnumType>() where TEnumType : struct, IConvertible
        {
            return CS.General_v3.Util.EnumUtils.EnumValueNullableOf<TEnumType>(this.GetFormValueAsString());
        }
        private void renderControls(bool forceRender)
        {
            addChildControls(forceRender);
            initRedirectJS();
        }

        public override void ForceRender()
        {
            renderControls(true);
            base.ForceRender();
        }
        private void initCMSEditable()
        {
            CMSInlineEditingItemDropdown editableDropdown = CMSInlineEditingItemDropdown.Create(this);
        }

        public override object GetInitialTemporaryRandomValue()
        {
            if (this.Functionality.Items != null)
            {
                int amt = this.Functionality.Items.Count;
                int index = CS.General_v3.Util.Random.GetInt(0, amt - 1);
                return this.Functionality.Items[index].Value;
            }
            else
            {
                return null;
            }
        }
    }
}
