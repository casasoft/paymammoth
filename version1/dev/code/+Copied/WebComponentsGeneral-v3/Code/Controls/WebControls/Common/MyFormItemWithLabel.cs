namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using BusinessLogic_v3.Frontend.ContentTextModule;

    public abstract class MyFormItemWithLabel<TDataType> : BaseWebControl, IMyFormWebControl
    {
        public enum LABEL_POSITION {
            Before,
            After
        }
        public MyFormWebControl FormControl { get; private set; }

        public MyFormItemWithLabel(MyFormWebControl formControl, BaseFieldParameters parameters = null)
            : base(HtmlTextWriterTag.Span)
        {
            this.LabelPosition = LABEL_POSITION.After;
            FormControl = formControl;
            if (parameters != null)
            {
                FormControl.FieldParameters = parameters;
            }
        }

        public BaseFieldParameters FieldParameters
        {
            get { return FormControl.FieldParameters; }
            set { FormControl.FieldParameters = value; }
        }
        BaseFieldParameters IMyFormWebControl.FieldParameters
        {
            get { return FormControl.FieldParameters; }
        }
        public bool GetFormValueObjectAsBool()
        {
            return FormControl.GetFormValueAsBool();
        }
        public object GetFormValueObject()
        {
            return FormControl.GetFormValueObject();
        }
        private MyLabel _labelControl;
        private MySpan _spanControl;

        public string Label { get; set; }
        public ContentTextBaseFrontend LabelContentText { get; set; }
        public LABEL_POSITION LabelPosition { get; set; }

        public bool? ReadOnly { get { return FormControl.ReadOnly; } set { FormControl.ReadOnly = value; } }
        public bool? AutoFocus { get { return FormControl.AutoFocus; } set { FormControl.AutoFocus = value; } }
        public bool? AutoComplete { get { return FormControl.AutoComplete; } set { FormControl.AutoComplete = value; } }
        public bool? Disabled { get { return FormControl.Disabled; } set { FormControl.Disabled = value; } }
        public string PlaceholderText { get { return FormControl.PlaceholderText; } set { FormControl.PlaceholderText = value; } }
      


        public new MyFormWebControlFunctionality WebControlFunctionality
        {
            get { return FormControl.WebControlFunctionality;  }
        }
        private void addLabel(string dataType)
        {
            if (!string.IsNullOrEmpty(Label) || this.LabelContentText != null)
            {
                _labelControl = new MyLabel();
                _labelControl.ForControl = FormControl;
                _labelControl.CssManager.AddClass(dataType + "-with-label-text");
                _labelControl.CssManager.AddClass("form-item-with-label-text");
                //Add the label
                if (this.LabelContentText != null)
                {
                    MyContentText.AttachContentTextWithControl(_labelControl, this.LabelContentText);
                }
                else
                {
                    _labelControl.InnerHtml = this.Label;
                }
                this.Controls.AddAt(this.LabelPosition == LABEL_POSITION.After ? this.Controls.Count : 0, _labelControl);
            }
        }
        private void createControl(string dataType)
        {
            _spanControl = new MySpan();
            _spanControl.CssManager.AddClass(dataType + "-with-label-form-item");
            _spanControl.CssManager.AddClass("form-item-with-label-form-item");
            _spanControl.Controls.Add(FormControl);
            this.Controls.Add(_spanControl);
            this.CssManager.AddClass(dataType + "-with-label-container");
            this.CssManager.AddClass("form-item-with-label-container");
        }

        protected override void OnLoad(EventArgs e)
        {
            string dataType = this.GetDataTypeAsString();
            this.CssManager.AddClass(dataType + "-with-label-" + ((this.LabelPosition == LABEL_POSITION.After) ? "right" : "left"));
            this.CssManager.AddClass("form-item-with-label-" + ((this.LabelPosition == LABEL_POSITION.After) ? "right" : "left"));

            createControl(dataType);

            addLabel(dataType);
            base.OnLoad(e);
        }


        public TDataType InitialValue { get { return  (TDataType)this.WebControlFunctionality.InitialValue; } set { this.WebControlFunctionality.InitialValue = value; } }



        public bool LoadInitialValueFromPostback
        {
            get;
            set;
        }

        public string Value { get { return FormControl.Value; } set { FormControl.Value = value; } }


        public abstract string GetDataTypeAsString();


        #region IMyFormWebControl Members


        public virtual bool NeverFillWithRandomValue { get; set; }

        #endregion


    }
}
