﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public interface IBaseNumericalReportTableRowData : IBaseReportTableRowDataBase
    {
        new IEnumerable<IBaseNumericalReportTableCellData> Cells { get; }

    }
}
