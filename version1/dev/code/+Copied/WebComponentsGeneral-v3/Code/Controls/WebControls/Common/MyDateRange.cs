using System.Text;
using BusinessLogic_v3.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.General_v3;
using System.Linq;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{

    public class MyDateRange : MyFormWebControl
    {
        #region MyRadioButtonList Functionality Class

        private MyTxtBoxDate _txtDateFrom;
        private MyTxtBoxDate _txtDateTo;

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected MyDateRange _item = null;

            public ContentTextBaseFrontend LblFrom { get; set; }
            public ContentTextBaseFrontend LblTo { get; set; }

            public DateTime? InitialValueFrom { get; set; }
            public DateTime? InitialValueTo { get; set; }
            public bool ValidateFrom { get; set; }
            public DateTime? GetDateFrom()
            {
                DateTime? txtDateFrom = null;
                if(!string.IsNullOrEmpty(_item.GetFormValueAsString())&& _item.GetFormValueAsString() != "-")
                {
                    List<string> sList = CS.General_v3.Util.Text.Split(_item.GetFormValueAsString(), "-");
                    if (sList != null)
                    {
                        txtDateFrom = DateTime.Parse(sList.FirstOrDefault());
                    }
                }
                return txtDateFrom;
            }

            public DateTime? GetDateTo()
            {
                DateTime? txtDateTo = null;
                if (!string.IsNullOrEmpty(_item.GetFormValueAsString())&& _item.GetFormValueAsString() != "-")
                {
                    List<string> sList = CS.General_v3.Util.Text.Split(_item.GetFormValueAsString(), "-");
                    if (sList != null)
                    {
                        txtDateTo = DateTime.Parse(sList.LastOrDefault());
                    }
                }
                return txtDateTo;
            }

            internal FUNCTIONALITY(MyDateRange item)
            {
                ContractsUtil.RequiresNotNullable(item, "Item cannot be null");
                this._item = item;
            }
        }

        #region MyDateRange Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class MyDateRangeFunctionality : MyFormWebControlFunctionality
        {
            protected new MyDateRange _item { get { return (MyDateRange)_formWebControl; } }

            internal MyDateRangeFunctionality(MyDateRange item)
                : base(item)
            {

            }
            protected override string getFormValue()
            {

                if(_item._txtDateFrom != null && _item._txtDateTo != null)
                {
                    string dateFrom = _item._txtDateFrom.GetFormValueAsString();
                    string dateTo = _item._txtDateTo.GetFormValueAsString();
                    return dateFrom + "-" + dateTo;
                }
                return null;
            }
        }


        #endregion
        protected override BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new MyDateRangeFunctionality(this);
        }
			

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        public MyDateRange(FieldRadioButtonOrCheckboxListParameters fieldParameters = null)
            : base( HtmlTextWriterTag.Div,  fieldParameters)
        {
            CssManager.AddClass("date-range-container","clearfix");
            //todo: 20-3-12 - DoNotAddFieldJS is set to true since JS has still to be implemented
            this.DoNotAddFieldJS = true;
            updateTexts();
        }

        private void updateTexts()
        {
            if (this.Functionality.LblFrom == null)
            {
                this.Functionality.LblFrom = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Listing_DateFilter_FromText).ToFrontendBase();
            }

            if (this.Functionality.LblTo == null)
            {
                this.Functionality.LblTo = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Listing_DateFilter_ToText).ToFrontendBase();
            }
        }

        protected override BaseFieldParameters createFieldParameters()
        {
            return new FieldSingleControlDateParameters();
        }

        protected override void updateInitialValue(object value)
        {
            string sValue = null;
            if (value != null) sValue = value.ToString();
            //todo: 20-3-12 - Check this out
        }

        public override string GetDataTypeAsString()
        {
            return "date-range";
        }

        public override void SetFormNames()
        {

        }

        protected override string getElementClientID()
        {
            return this.ClientID;
        }
        private void initControls()
        {
            MyDiv divFromContainer = new MyDiv();
            divFromContainer.CssClass = "date-range-from";

            MyDiv divToContainer = new MyDiv();
            divToContainer.CssClass = "date-range-to";

            _txtDateTo = new MyTxtBoxDate();
            if(this.Functionality.InitialValueTo.HasValue)
            {
                _txtDateTo.InitialValue = this.Functionality.InitialValueTo.Value;
            }
            _txtDateFrom = new MyTxtBoxDate();
            if (this.Functionality.InitialValueFrom.HasValue)
            {
                _txtDateFrom.InitialValue = this.Functionality.InitialValueFrom.Value;
            }
            _txtDateTo.ID = "dateRange_txtTo";
            _txtDateFrom.ID = "dateRange_txtFrom";
          
            string txtDateFromID = _txtDateFrom.FieldParameters.id;
            string txtDateToID = _txtDateTo.FieldParameters.id;
            CS.General_v3.Util.ReflectionUtil.CopyPropertiesAndFields(this.FieldParameters, _txtDateFrom.FieldParameters);
            CS.General_v3.Util.ReflectionUtil.CopyPropertiesAndFields(this.FieldParameters, _txtDateTo.FieldParameters);
            _txtDateFrom.FieldParameters.id = txtDateFromID;
            _txtDateTo.FieldParameters.id = txtDateToID;
            _txtDateFrom.FieldParameters.title += " From";
            _txtDateTo.FieldParameters.title += " To";
            _txtDateFrom.FieldParameters.validationGroup = _txtDateTo.FieldParameters.validationGroup = this.FieldParameters.validationGroup;
            if(this.Functionality.ValidateFrom)
            {
                ValidatorDate vdDate = new ValidatorDate();
                vdDate.Parameters.dateFrom = CS.General_v3.Util.Date.Now;
                _txtDateFrom.FieldParameters.validators.Add(vdDate);
            }
            MyLabel lblFrom = new MyLabel();
            MyContentText.AttachContentTextWithControl(lblFrom, this.Functionality.LblFrom);

            divFromContainer.Controls.Add(lblFrom);
            divFromContainer.Controls.Add(_txtDateFrom);

            MyLabel lblTo = new MyLabel();
            MyContentText.AttachContentTextWithControl(lblTo, this.Functionality.LblTo);

            divToContainer.Controls.Add(lblTo);
            divToContainer.Controls.Add(_txtDateTo);

            this.Controls.Add(divFromContainer);
            this.Controls.Add(divToContainer);

        }

        public object InitialValue { get { return this.WebControlFunctionality.InitialValue; } set { this.WebControlFunctionality.InitialValue = value; } }

        protected override void OnLoad(EventArgs e)
        {
            initControls();
            base.OnLoad(e);
        }
        private bool? _readOnly;
        public override bool? ReadOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                _readOnly = value;
                _txtDateFrom.ReadOnly = value;
                _txtDateTo.ReadOnly = value;
            }
        }

        public override string JSFieldTypeName
        {
            get;
            set;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        public override object GetInitialTemporaryRandomValue()
        {
            //Each item should work on its own
            return null;
        }
    }
}
