﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ContentTextModule;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    public interface IMyDropdownListChildItem
    {
        BaseWebControl Control { get; }
        ContentTextBaseFrontend LabelContentText { get; set; }
        string Label { get; set; }
        string Value { get; set; }
    }
}
