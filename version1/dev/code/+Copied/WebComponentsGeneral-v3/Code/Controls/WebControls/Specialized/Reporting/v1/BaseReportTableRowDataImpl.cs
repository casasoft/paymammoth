﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using CS.General_v3;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Classes.CSS;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public class BaseReportTableRowDataImpl :BaseReportTableRowDataBaseImpl, IBaseReportTableRowData
    {

        public BaseReportTableRowDataImpl()
        {
          
        }
        protected override IEnumerable<BaseReportTableCellDataBaseImpl> initCells()
        {
            return new List<BaseReportTableCellDataImpl>();
        }
        
        #region IBaseReportTableRowData Members

        public new List<BaseReportTableCellDataImpl> Cells
        {
            get { return (List<BaseReportTableCellDataImpl>)base.Cells; }
        }

        #endregion
        public void AddCell(Control c, string cssClass = null)
        {
            BaseReportTableCellDataImpl cell = new BaseReportTableCellDataImpl();
            cell.Control = (c);
            cell.CssManager.AddClass(cssClass);
            
            this.Cells.Add(cell);

        }

        /// <summary>
        /// Adds a cell
        /// </summary>
        /// <param name="text">Text to display</param>
        /// <param name="link">If specified, an anchor link is created with the link as it's Href</param>
        public void AddCell(string text, string link = null, string cssClass = null)
        {
            if (link == null)
            {
                LiteralControl lit = new LiteralControl();
                lit.Text = text;
                AddCell(lit, cssClass: cssClass);
            }
            else
            {
                MyAnchor a = new MyAnchor();
                a.Text = text;
                a.Href = link;
                a.HRefTarget = Enums.HREF_TARGET.Blank;
                AddCell(a, cssClass: cssClass);
            }
        }

        #region IBaseReportTableRowData Members

        IEnumerable<IBaseReportTableCellData> IBaseReportTableRowData.Cells
        {
            get { return this.Cells; }
        }

        #endregion

       
    }
}
