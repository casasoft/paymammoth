﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    public class MyTxtBoxTextMultiLine : MyFormWebControl
    {
        protected override Fields.BaseFieldParameters createFieldParameters()
        {
            return new FieldSingleControlParameters();
        }

        public MyTxtBoxTextMultiLine()
            : base("Textarea")
        {
            this.JSFieldTypeName = "FieldSingleControl";
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void updateInitialValue(object value)
        {
            if (value != null)
            {
                this.Value = value.ToString();
            }
            else
            {
                this.Value = null;
            }
        }

        public override string JSFieldTypeName
        {
            get;
            set;
        }

        public override string GetDataTypeAsString()
        {
            return "textarea";
        }

        public override void SetFormNames()
        {
            this.setAttributeFromString("name", this.ClientID);
        }

        protected override string getElementClientID()
        {
            return this.ClientID;
        }
    }
}