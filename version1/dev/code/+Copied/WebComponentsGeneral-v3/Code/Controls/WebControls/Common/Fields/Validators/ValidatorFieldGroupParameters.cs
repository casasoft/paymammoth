﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public enum FIELD_SUBGROUP_TYPE
    {
        /// <summary>
        /// All the fields with the same field group ID must have same values
        /// </summary>
        SameValues = 100,
        /// <summary>
        /// All the fields within the same field group must have at least one required
        /// </summary>
        AtLeastOneRequired = 200,
        /// <summary>
        /// Values must not be the same
        /// </summary>
        NotSameValues = 300,

    }
    public class ValidatorFieldGroupParameters : ValidatorBaseParameters
    {
        public FIELD_SUBGROUP_TYPE groupType = FIELD_SUBGROUP_TYPE.SameValues;
        public List<string> groupElementsIDs = new List<string>();
       
        
    }
}