namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Classes.CSS;

    public class MyTableRowCollection : MyControlCollection<MyTableRow>
    {
        public MyTableRowCollection(MyTable table)
            : base(table)
        {

        }
    }

    public class MyTable : BaseWebControl
    {
        public MyTableRowCollection Rows { get; private set; }

        private void init()
        {
            this.Rows = new MyTableRowCollection(this);
        }
        public MyTable(string cssClass)
            : base(HtmlTextWriterTag.Table)
        {
            if (!string.IsNullOrEmpty(cssClass))
            {
                this.CssManager.AddClass(cssClass);
            }
            init();
        }


        public MyTable()
            : this(null)
        {
            init();

        }
        public MyTableRow AddRow(string cssClass)
        {

            MyTableRow tr = new MyTableRow();

            this.Rows.Add(tr);
            if (!string.IsNullOrEmpty(cssClass))
                tr.CssManager.AddClass(cssClass);
            return tr;
        }
        public MyTableRow AddRow()
        {
            return AddRow("");
        }
        public MyTableRow this[int i]
        {
            get
            {
                return (MyTableRow)Rows[i];
            }
        }
        public new MyTableRowCollection Controls { get { return this.Rows; } }

       
    }
}
