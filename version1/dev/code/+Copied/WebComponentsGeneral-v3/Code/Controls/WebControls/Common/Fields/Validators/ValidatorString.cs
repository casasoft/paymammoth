﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorString : ValidatorBase
    {

        public new ValidatorStringParameters Parameters { get { return (ValidatorStringParameters)base.Parameters; } set { base.Parameters = value; } }

        protected override ValidatorBaseParameters createParameters()
        {
            return new ValidatorStringParameters();
        }

        public override object GenerateRandomValidValue()
        {
            int lenFrom = 10;
            int lenTo = 30;
            if (Parameters.maxLength != 0)
            {
                lenTo = Parameters.maxLength;
            }
            if (Parameters.minLength != 0)
            {
                lenFrom = Parameters.minLength;
            }
            string str = CS.General_v3.Util.Random.GetString(lenFrom, lenTo);
            return str;
        }
    }
}