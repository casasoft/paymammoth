﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.General_v3.JavaScript.jQuery.Plugins.Tooltips.QTip;
    using General_v3.Controls.WebControls;
    using General_v3.Util;
    using CS.General_v3;
    using CS.WebComponentsGeneralV3.Code.Classes.CSS;
using System.Collections.Generic;
using System.ComponentModel;

    [DefaultProperty("ChildControls")]
    [ParseChildren(false, "ChildControls")]
    public class BaseWebControl : System.Web.UI.WebControls.WebControl, IBaseWebControl
    {
        private Literal _ltlContent = new Literal();

        private BaseWebControlFunctionality _webControlFunctionality = null;

        //
        // [PersistenceMode(PersistenceMode.InnerProperty), TemplateContainer(typeof(INamingContainer)), TemplateInstance(TemplateInstance.Multiple)]
        //public ITemplate Content { get; set; }

        private List<object> _childControls = new List<object>();
        public List<object> ChildControls { get { return _childControls; } }

        public BaseWebControl()
            : base()
        {

            init();
        }
        public BaseWebControl(System.Web.UI.HtmlTextWriterTag tagName)
            : base(tagName)
        {
            init();
        }
        public BaseWebControl(string tagName)
            : base(tagName)
        {
            init();
        }

        private void init()
        {
            _webControlFunctionality = getWebControlFunctionality();

        }

        protected virtual BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new BaseWebControlFunctionality(this);
        }





        public override string CssClass
        {
            get
            {
                if (_webControlFunctionality == null)
                    return base.CssClass;
                else
                    return _webControlFunctionality.CssClass;


            }
            set
            {
                _webControlFunctionality.CssClass = value;
                base.CssClass = _webControlFunctionality.CssClass;
            }
        }

        public CSSManager CssManager { get { return _webControlFunctionality.CssManager; } }



        public BaseWebControlFunctionality WebControlFunctionality
        {
            get { return _webControlFunctionality; }
        }





        public WebControl Control
        {
            get { return this; }
        }

        Control IBaseControl.Control
        {
            get { return this; }
        }

        WebComponentsGeneralV3.Code.Controls.WebControls.BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get { return (WebComponentsGeneralV3.Code.Controls.WebControls.BaseControlFunctionality)this.WebControlFunctionality; }
        }

        public bool ContentEditable { get { return getAttributeAsBool("contenteditable", "true"); } set { setAttributeFromBool("contenteditable", value, "true", "false"); } }
        public bool Draggable { get { return getAttributeAsBool("draggable", "true"); } set { setAttributeFromBool("draggable", value, "true", "false"); } }
        public bool SpellCheck { get { return getAttributeAsBool("spellcheck", "true"); } set { setAttributeFromBool("spellcheck", value, "true", "false"); } }
        public bool Hidden { get { return getAttributeAsBool("hidden", "hidden"); } set { setAttributeFromBool("hidden", value, "hidden", ""); } }
        public Enums.ELEMENT_TEXT_DIRECTION? Direction
        {
            get
            {
                return getAttributeAsEnum<Enums.ELEMENT_TEXT_DIRECTION>("dir");
            }
            set { setAttributeFromEnum<Enums.ELEMENT_TEXT_DIRECTION>("dir", value); }
        }
        public string Title { get { return this.Attributes["title"]; } set { this.Attributes["title"] = value; } }

        protected T? getAttributeAsEnum<T>(string attr) where T : struct, IConvertible
        {
            return EnumUtils.GetEnumByStringValueNullable<T>(this.Attributes[attr]);
        }
        protected void setAttributeFromEnum<T>(string attr, T? value) where T : struct, IConvertible
        {
            if (value.HasValue)
            {
                string str = EnumUtils.StringValueOf(value.Value);
                if (!string.IsNullOrEmpty(str))
                {
                    this.Attributes[attr] = str;
                }
            }
            else
            {
                this.Attributes.Remove(attr);
            }
        }
        protected bool? getAttributeAsBoolNullable(string attr, string trueValue = null)
        {
            string val = this.Attributes[attr];
            if (string.IsNullOrEmpty(val))
            {
                return null;
            }
            else
            {
                return val == trueValue;
            }
        }
        protected bool getAttributeAsBool(string attr, string trueValue = null)
        {
            if (string.IsNullOrEmpty(trueValue)) trueValue = attr;

            return getAttributeAsBoolNullable(attr, trueValue).GetValueOrDefault();
        }
       
        protected string getAttributeAsString(string attr)
        {
            return this.Attributes[attr];
        }

        protected void setAttributeFromString(string attr, string value, bool removeAttributeIfEmptyString = true)
        {
            if (removeAttributeIfEmptyString && string.IsNullOrEmpty(value))
            {
                this.Attributes.Remove(attr);
            }
            else
            {
                this.Attributes[attr] = value;
            }
        }

        /// <summary>
        /// If you leave the trueValue/falseValue to null, then it will simply set the attributes without any value (HTML5)
        /// </summary>
        /// <param name="attr"></param>
        /// <param name="value"></param>
        /// <param name="trueValue"></param>
        /// <param name="falseValue"></param>
        protected void setAttributeFromBool(string attr, bool? value, string trueValue = null, string falseValue = null)
        {
            if (trueValue == null) trueValue = attr;

            if (value.HasValue)
            {
                if (value.Value)
                {
                    this.Attributes[attr] = trueValue;
                }
                else
                {
                    if (falseValue == null)
                    {
                        this.Attributes.Remove(attr);
                    }
                    else
                    {
                        this.Attributes[attr] = falseValue;
                    }
                }
            }
            else
            {
                this.Attributes.Remove(attr);
            }
        }
        protected double? getAttributeAsNumeric(string attr)
        {
            double n = 0;
            if (double.TryParse(this.Attributes[attr], out n))
            {
                return n;
            }
            else
            {
                return null;
            }
        }
        protected void setAttributeFromNumeric(string attr, double? value, string nullValue = null)
        {
            if (value.HasValue)
            {
                this.Attributes[attr] = value.Value.ToString();
            }
            else
            {
                this.Attributes[attr] = nullValue;
            }
        }

        public string Rel { get { return getAttributeAsString("rel"); } set { this.setAttributeFromString("rel", value); } }


        protected override void CreateChildControls()
        {
            //if (Content != null)
            //    Content.InstantiateIn(this); //Replace this with container where you want to place such controls

            foreach (object control in this.ChildControls)
            {
                string s = "";
            }

            base.CreateChildControls();
        }
        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    value = CS.General_v3.Util.Text.NormalizeControlID(value);
                }
                base.ID = value;
            }
        }
        public virtual void ForceRender()
        {

        }
    }


}
