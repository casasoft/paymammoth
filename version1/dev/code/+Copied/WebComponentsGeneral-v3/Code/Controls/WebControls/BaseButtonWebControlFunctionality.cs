﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls
{
    using System;
    using System.Text;
    using System.Web.UI.WebControls;
    using CS.General_v3.JavaScript.Data;
    using General_v3.JavaScript.Data;
    using Util;
    using MyLinkButton = Common.MyLinkButton;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

    public class BaseButtonWebControlFunctionality : BaseWebControlFunctionality
    {

        public class FormButtonParametersJS : JSONObject
        {
            public bool validateFormOnClick = true;
            public bool submitFormOnClick = true;
            public string validationGroup = FormFields.FormFields.DEFAULT_VALIDATION_GROUP;
            public bool defaultButton = true;
            public string confirmMessage = null;
            public bool disableButtonOnSubmit = true;
        }

        public event EventHandler Click;

        public FormButtonParametersJS ButtonParams { get; private set; }

        public BaseButtonWebControlFunctionality(BaseWebControl control)
            : base(control) 
        {
            this.ButtonParams = new FormButtonParametersJS();
            init();
            
            
        }
        private void checkID()
        {
            if (string.IsNullOrEmpty(Control.ID))
            {
                throw new InvalidOperationException("A button must always have an ID set to a value");
            }
        }
        private void initHandlers()
        {
            this.Control.PreRender += new EventHandler(Control_PreRender);
            this.Control.Load += new EventHandler(Control_Load);
        }

        void Control_Load(object sender, EventArgs e)
        {
            addJSToPage();
            attachJsClickEvent();
            
        }

        void Control_PreRender(object sender, EventArgs e)
        {
            
            checkID(); 
            checkPostBackUrl();


        }

       

     

        public string Text { get
        {
            string s = (string)this.Control.Attributes["value"]; 
            return s;
        } set
        {
            this.Control.Attributes["value"] = value;
        } 
        }


        

        private void attachASPPostbackJS()
        {
            //this

            string js = Control.Page.ClientScript.GetPostBackEventReference(Control, "Click");
            this.OnClientClick += js;
        }
        private void attachValidationJS()
        {
            if (this.ButtonParams.validateFormOnClick)
            {
                this.OnClientClick += "js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.Forms.FormsCollection.get_instance().validateCurrentForm();";
            }
        }

        private void attachJsClickEvent()
        {
            if (string.IsNullOrEmpty(this.OnClickHref))
            {
                if (this.Click != null)
                {
                    attachASPPostbackJS();
                }
                else
                {
                    attachValidationJS();
                }
            }
            else
            {
                updateOnClickHrefJs();
            }

        }
       
      

        #region IPostBackEventHandler Members

        public void RaisePostBackEvent(string eventArgument)
        {
            switch (eventArgument)
            {
                case "Click":
                    if (this.Click != null)
                    {
                        this.Click(this, null);
                    }
                    break;
            }
        }

        #endregion




        private void init()
        {
          //  this.AddFieldJS = false;
            initHandlers();
            Control.Attributes["type"] = "button";
        }

        public string PostBackUrl { get; set; }
        public bool UseSubmitBehavior { get; set; }
        public bool DisableButtonOnSubmitForm { get; set; }
        public string DisableButtonOnSubmitFormText { get; set; }
        public string ConfirmMessage
        {
            get { return this.ButtonParams.confirmMessage; }
            set { this.ButtonParams.confirmMessage = value; }
        }
        public string ConfirmMessageHighlightRowClass { get; set; }
        public string ConfirmMessageHighlightRowID { get; set; }
        public bool NoValidation { get; set; }
        private StringBuilder _JSAfterControl = new StringBuilder();
        public object Tag2 { get; set; }
        public object Tag3 { get; set; }


        public bool ValidationDefault
        {
            get { return this.ButtonParams.validateFormOnClick; }
            set { this.ButtonParams.validateFormOnClick = value; }
        }

      
       private void addJSToPage()
        {
            if (Control.Visible)
            {
                
               /* _JSAfterControl.Append("new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.Forms.Button('" + Control.ClientID + "',true,'" + this.ValidationGroup +
                    "'," + CS.General_v3.Util.Text.forJS(this.ValidationDefault) + "," + (String.IsNullOrEmpty(ConfirmMessage) ? "null" : "'" + ConfirmMessage + "'") + ");");
                */
                //this.ButtonParams.defaultButton = true;

                string js = "new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.Forms.Buttons.FormButton('" + Control.ClientID + "'," + this.ButtonParams.GetJSON() + ");";


                JSUtil.AddJSScriptToPage(js);
                //writer.Write(js);
            }
        }

        private void checkPostBackUrl()
        {
            if (!string.IsNullOrEmpty(this.PostBackUrl))
            {
                string JS = ";document.forms[0].action = \"" + this.PostBackUrl + "\";";
                JS += "document.forms[0].submit();";
                OnClientClick += JS;

            }
        }


        private void updateOnClickHrefJs()
        {
            if (!string.IsNullOrEmpty(OnClickHref))
            {
                this.OnClientClick += "window.location = '" + OnClickHref + "';";
            }
        }

        internal void TriggerClickEvent(object sender = null, EventArgs e = null)
        {
            if (sender == null) sender = Control;

            if (Click != null) {
                Click(sender, e);
            }
        }
        public bool HasClickEventBeenAttached { get { return this.Click != null; } }

        public string Value { get { return Control.Attributes["value"]; } set { Control.Attributes["value"] = value; } }

        public string OnClickHref { get; set; }

      
    }
}
