namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public class MyLinkButton : BaseWebControl, IBaseButtonWebControl
    {
        private MyButton _btnLinkButton = new MyButton();

        public MyLinkButton()
            : this("")
        {
            //this.WebControlFunctionality.FieldCssClasses.cssClassButton = "link-button";
            
        }
      
        public void AddClassToAnchor(string cssClass)
        {
            
        }

        public MyLinkButton(string ID)
            : base(HtmlTextWriterTag.A)
        {
            _btnLinkButton.ID = ID;
            init();

            
            // _myWebControl = new CS.General_v3.Controls.WebControls.Common.General.MyButtonWebControl(this, ClientIDSeparator);
            //this.WebControlFunctionality.ButtonParams. = true;
            //this.WebControlFunctionality.CssManager.Clear();
           // this.WebControlFunctionality.CssManager.AddClass("link-button");
        }

        private void init()
        {
            _btnLinkButton.CssManager.AddClass("link-button-hidden");
        }

        /*

        void _btnButton_Click(object sender, EventArgs e)
        {
            WebControlFunctionality.TriggerClickEvent();
        }

       
        

        protected override void OnLoad(EventArgs e)
        {
            _btnButton.ID = this.ID + "_button";
            _btnButton.Click += new EventHandler(_btnButton_Click);
            _btnButton.Style.Add("display", "none");
            _btnButton.ConfirmMessage = ConfirmMessage;
            this.Controls.Add(_btnButton);

            _btnLink.Attributes["href"] = "javascript:document.getElementById('" + _btnButton.ClientID + "').click();";
            _btnLink.Text = WebControlFunctionality.Text;
            this.Controls.Add(_btnLink);


            base.OnLoad(e);
        }

        #region IMyButtonWebControl Members


        string IMyButtonWebControl.PostBackUrl
        {
            get
            {
                return WebControlFunctionality.PostBackUrl;
            }
            set
            {
                WebControlFunctionality.PostBackUrl = value;
            }
        }
        */
      

        //#endregion

        event EventHandler IBaseButtonWebControl.Click
        {
            add { _btnLinkButton.WebControlFunctionality.Click += value; }
            remove { _btnLinkButton.WebControlFunctionality.Click += value; }
        }

        BaseButtonWebControlFunctionality IBaseButtonWebControl.WebControlFunctionality
        {
            get { return _btnLinkButton.WebControlFunctionality;  }
        }
        public BaseButtonWebControlFunctionality ButtonWebControlFunctionality { get { return _btnLinkButton.WebControlFunctionality; } }


            protected override void OnLoad(EventArgs e)
        {
            //Add it to the page
            //this.Page.Controls.Add(_btnLinkButton);
            base.OnLoad(e);
        }
    }
}
