﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using General_v3.Controls.WebControls;
    using General_v3.Util;

    public class BaseControl : Control, IBaseControl
    {
        private void addTagComments()
        {
            if (CS.General_v3.Settings.Others.IsInLocalTesting)
            {
                Literal litBefore = new Literal();
                litBefore.Text = "<!-- " + CS.General_v3.Util.ReflectionUtil.GetNameFromType(this.GetType()) + " {Start} -->";
                this.Controls.AddAt(0, litBefore);
                Literal litAfter = new Literal();
                litAfter.Text = "<!-- " + CS.General_v3.Util.ReflectionUtil.GetNameFromType(this.GetType()) + " {End} -->";
                this.Controls.Add(litAfter);


            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            addTagComments();
            base.OnPreRender(e);
        }
        private BaseControlFunctionality _functionality = null;

        public BaseControl() : base() {
            init();
        }
        

        private void init()
        {
            _functionality = getFunctionality();
            
        }

        protected virtual BaseControlFunctionality getFunctionality()
        {
            return new BaseControlFunctionality(this);
        }





        #region IBaseControl Members

        public Control Control
        {
            get { return this; }
        }




        public BaseControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion
    }
}
