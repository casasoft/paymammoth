﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorNumeric : ValidatorBase
    {

        public new ValidatorNumericParameters Parameters { get { return (ValidatorNumericParameters)base.Parameters; } set { base.Parameters = value; } }

        protected override ValidatorBaseParameters createParameters()
        {
            return new ValidatorNumericParameters();
        }

        public override object GenerateRandomValidValue()
        {
            double numFrom = double.MinValue;
            double numTo = double.MaxValue;
            if (Parameters.positiveOnly)
            {
                numFrom = 0;
            }
            if (Parameters.negativeOnly)
            {
                numTo = 0;
            }
            double num = CS.General_v3.Util.Random.GetDouble(numFrom, numTo);
            if (Parameters.integersOnly)
            {
                num = Math.Round(num);
            }
            return num;
        }
    }
}