﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields
{
    /*public enum FORMFIELDS_ITEM_LAYOUT_TYPE
    {
        NoLabel,
        Normal,
        Block
    }*/
    public class FormFieldsLayoutTypeInformation
    {
        /// <summary>
        /// Doesn't show label
        /// </summary>
        public bool DoNotShowLabel { get; set; }
        /// <summary>
        /// Takes an entire line
        /// </summary>
        public bool BlockElement { get; set; }
    }

    public interface IFormFieldsItemData
    {
        string CssClass { get; set;  }
        string Title { get; }
        ContentTextBaseFrontend TitleContentText { get; }
        string SubTitle { get; }
        ContentTextBaseFrontend SubTitleContentText { get; }
        bool Required { get; }
        Control Control { get; }
        FormFieldsLayoutTypeInformation LayoutInfo { get; }

        BaseFieldParameters FieldParameters { get; }
    }
}
