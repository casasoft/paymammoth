using System.Text;
using BusinessLogic_v3.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.General_v3;
using System.Linq;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{

    public class MyCurrencyAndPrice : MyFormWebControl
    {

        private MyDropDownList _cmbCurrency;
        private MyTxtBoxNumber _txtValue;

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected MyCurrencyAndPrice _item = null;

            public  double? InitialValue{ get; set; }
            public CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217? InitialCurrencyValue { get; set; }
            public CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217? GetFormValueCurrency()
            {
                return _item._cmbCurrency.GetFormValueAsEnumNullable<CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217>();
            }

            public double? GetFormValue()
            {
                
                return _item._txtValue.GetFormValueAsDoubleNullable();
            }

            internal FUNCTIONALITY(MyCurrencyAndPrice item)
            {
                ContractsUtil.RequiresNotNullable(item, "Item cannot be null");
                this._item = item;
                InitialCurrencyValue = Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro;
            }
        }

        #region MyDateRange Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class MyCurrencyAndPriceFunctionality : MyFormWebControlFunctionality
        {
            protected new MyCurrencyAndPrice _item { get { return (MyCurrencyAndPrice)_formWebControl; } }

            internal MyCurrencyAndPriceFunctionality(MyCurrencyAndPrice item)
                : base(item)
            {

            }
            protected override string getFormValue()
            {
                string val = "";
                var curr = _item.Functionality.GetFormValueCurrency();
                var value = _item.Functionality.GetFormValue();
                if (curr.HasValue)
                {
                    val += (int)curr;
                }
                if (value.HasValue)
                {
                    val += "-" + value.Value;
                }
                return val;
            }
        }


        #endregion
        protected override BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new MyCurrencyAndPriceFunctionality(this);
        }
			

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }


        public MyCurrencyAndPrice(FieldCurrencyAndPriceParameters fieldParameters = null)
            : base( HtmlTextWriterTag.Div,  fieldParameters)
        {
            CssManager.AddClass("price-and-currency-container","clearfix");
            //todo: 20-3-12 - DoNotAddFieldJS is set to true since JS has still to be implemented
            this.DoNotAddFieldJS = true;
        }

        
        protected override BaseFieldParameters createFieldParameters()
        {
            return new FieldCurrencyAndPriceParameters();
        }

        protected override void updateInitialValue(object value)
        {
            string sValue = null;
            
            if (value != null) sValue = value.ToString();
            //todo: 20-3-12 - Check this out
        }

        public FieldCurrencyAndPriceParameters FieldParameters { 
            get { return (FieldCurrencyAndPriceParameters)base.FieldParameters; }
        }

            public override string GetDataTypeAsString()
        {
            return "price-and-currency";
        }

        public override void SetFormNames()
        {

        }

        protected override string getElementClientID()
        {
            return this.ClientID;
        }
        private void initControls()
        {
            MyDiv divCurrency = new MyDiv();
            divCurrency.CssClass = "form-price-currency-container";

            MyDiv divValue = new MyDiv();
            divValue.CssClass = "form-price-value-container";

            _cmbCurrency = new MyDropDownList();
            _cmbCurrency.Functionality.AddItemsFromListItemCollection(CS.General_v3.Enums.ISO_ENUMS.GetCurrenciesAsListItemCollection());
            if(this.Functionality.InitialCurrencyValue.HasValue)
            {
                _cmbCurrency.InitialValue = this.Functionality.InitialCurrencyValue.Value;
            }

            _txtValue = new MyTxtBoxNumber();

            if (this.Functionality.InitialValue.HasValue)
            {
                _txtValue.InitialValue = this.Functionality.InitialValue.Value;
            }
            
            divCurrency.Controls.Add(_cmbCurrency);
            divValue.Controls.Add(_txtValue);
            _cmbCurrency.FieldParameters.title = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Fields_Currency).ToFrontendBase().GetContent();
            _txtValue.FieldParameters.title = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Fields_Price).ToFrontendBase().GetContent();

            CS.General_v3.Util.ReflectionUtil.CopyPropertiesAndFields<BaseFieldParameters>(this.FieldParameters, _txtValue.FieldParameters);
            CS.General_v3.Util.ReflectionUtil.CopyPropertiesAndFields<BaseFieldParameters>(this.FieldParameters, _cmbCurrency.FieldParameters);
            _cmbCurrency.ID = "cmbPriceAndCurrencyCurrency";
            _txtValue.ID = "txtPriceAndCurrencyValue";
            this.Controls.Add(divCurrency);
            this.Controls.Add(divValue);

        }

        public object InitialValue { get { return this.WebControlFunctionality.InitialValue; } set { this.WebControlFunctionality.InitialValue = value; } }

        protected override void OnLoad(EventArgs e)
        {
            initControls();
            base.OnLoad(e);
        }
        private bool? _readOnly;
        public override bool? ReadOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                _readOnly = value;
                _cmbCurrency.ReadOnly = value;
                _txtValue.ReadOnly = value;
            }
        }

        public override string JSFieldTypeName
        {
            get;
            set;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        public override object GetInitialTemporaryRandomValue()
        {
            //Each item should work on its own
            return null;
        }
    }
}
