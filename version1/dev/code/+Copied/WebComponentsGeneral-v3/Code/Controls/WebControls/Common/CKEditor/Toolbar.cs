/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * This is the FCKeditor Asp.Net control.
 */

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.CKEditor
{
    using System.Collections.Generic;


    public partial class CKEditor : MyTxtBoxStringMultiLine
	{
        public enum DEFAULT_TOOLBARS
        {
            Full,
            Standard,
            Simple
        }
        public static string DefaultToolbarToText(DEFAULT_TOOLBARS toolbar)
        {
            return CS.General_v3.Util.EnumUtils.GetEnumName(typeof(DEFAULT_TOOLBARS), toolbar);
        }
        public enum TOOLBAR_BUTTON
        {
            Source, Toolbar_Separator, Save, NewPage, Preview, Templates, Cut, Copy, Paste, PasteText, PasteFromWord, Print, SpellChecker, Scayt,
            Undo, Redo, Find, Replace, SelectAll, RemoveFormat, Form, Checkbox, Radio, TextField, Textarea, Select, Button, ImageButton,
            HiddenField, Bold, Italic, Underline, Strike, Subscript, Superscript, NumberedList, BulletedList, Outdent, Indent, Blockquote,
            JustifyLeft, JustifyCenter, JustifyRight, JustifyBlock, Link, Unlink, Anchor, Image, Flash, Table, HorizontalRule, Smiley,
            SpecialChar, PageBreak, Styles, Format, Font, FontSize, TextColor, BGColor, Maximize, ShowBlocks, About, MediaEmbed,
            Toolbar_LineBreak
        }
        public static string ToolbarButtonToCode(TOOLBAR_BUTTON btn)
        {
            string s = "";
            switch (btn)
            {
                case TOOLBAR_BUTTON.Toolbar_Separator: s = "-"; break;
                case TOOLBAR_BUTTON.Toolbar_LineBreak: s = "/"; break;
                default:
                    s = CS.General_v3.Util.EnumUtils.GetEnumName(typeof(TOOLBAR_BUTTON), btn);
                    break;
            }
            return s;
        }
        
        public class TOOLBAR
        {

            public class TOOLBAR_LINE : List<TOOLBAR_BUTTON>
            {
                
                public override string ToString()
                {
                    
                    return GetAsJSArray().ToString();
                    
                }
                public List<string> GetAsJSArray()
                {
                    List<string> arr = new List<string>();
                    foreach (var item in this)
                    {
                        arr.Add(CS.General_v3.Util.Text.forJS(ToolbarButtonToCode(item)));
                    }
                    return arr;
                }
            }
            public void RemoveButton(TOOLBAR_BUTTON btn)
            {
                foreach (var line in this.Lines)
                {
                    line.Remove(btn);
                }
            }
            public string Name { get; set; }
            public List<TOOLBAR_LINE> Lines { get; set; }
            public TOOLBAR(string name)
            {
                this.Name = name;
                this.Lines = new List<TOOLBAR_LINE>();
            }
            public void AddLine(params TOOLBAR_BUTTON[] buttons)
            {
                TOOLBAR_LINE line = new TOOLBAR_LINE();
                foreach (var btn in buttons)
                {
                    line.Add(btn);
                }
                this.Lines.Add(line);
            }
            public  List<object> GetAsJSArray()
            {
                List<object> jsArr = new List<object>();
                TOOLBAR_LINE fullLine = new TOOLBAR_LINE();
                for (int i = 0; i < this.Lines.Count; i++)
                {
                    if (i > 0)
                    {
                        jsArr.Add(ToolbarButtonToCode(TOOLBAR_BUTTON.Toolbar_LineBreak));
                    }
                    var lineArr = this.Lines[i].GetAsJSArray();
                    
                    jsArr.Add(lineArr);
                    
                }
                return jsArr;
            }
            public override string ToString()
            {
                return this.GetAsJSArray().ToString();
            }
            

            
        }


        #region IBaseWebControl Members


        //public new BaseWebControlFunctionality WebControlFunctionality
        //{
        //    get { return base.WebControlFunctionality this.WebControlFunctionality; }
        //}

        #endregion

        #region IMyFormWebControl Members


        
        #endregion
    }
}
