﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public interface IBaseNumericalReportTableData : IBaseReportTableDataBase
    {
        new IEnumerable<IBaseNumericalReportTableHeaderData> HeadersTop { get; }
        new IEnumerable<IBaseNumericalReportTableHeaderData> HeadersLeft { get; }
        new IEnumerable<IBaseNumericalReportTableRowData> DataRows { get; }

       

    }
}
