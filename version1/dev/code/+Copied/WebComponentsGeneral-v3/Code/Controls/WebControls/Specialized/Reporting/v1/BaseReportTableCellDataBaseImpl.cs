﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public abstract class BaseReportTableCellDataBaseImpl : IBaseReportTableCellDataBase
    {
        public BaseReportTableCellDataBaseImpl()
        {
            this.CssManager = new CSSManager();
        }

        #region IBaseReportTableCellDataBase Members

        public CSSManager CssManager
        {
            get;
            set;
        }

        public abstract Control GetCellContent();

        #endregion
    }
}
