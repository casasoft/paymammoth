using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.General_v3;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.JWPlayer.v5;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.SWFObject;

    /// <summary>
    /// Supports to include images / swfs / flvs
    /// </summary>
    public class MyMediaItem : General_v3.Controls.WebControls.Common.MyDiv
    {
        public class FUNCTIONALITY
        {
            private MyMediaItem _item;

            public string Rel { get; set; }
            public string ItemURL { get; set; }
            public string Caption { get; set; }
            public string Href { get; set; }
            public string OnMouseDownJS { get; set; }
            public Unit Width { get; set; }
            public Unit Height { get; set; }

            public Enums.HREF_TARGET HrefTarget { get; set; }

            /// <summary>
            /// Used for JW / SWF Object
            /// </summary>
            public string FlashVersion { get; set; }
            /// <summary>
            /// Express instal URL
            /// </summary>
            public string ExpressInstall { get; set; }
            /// <summary>
            /// JW Path
            /// </summary>
            public bool JWPlayerAutoStart { get; set; }
            public JWPlayer.JWPLAYER_REPEAT JWPlayerRepeat { get; set; }
            public JWPlayer.JWPLAYER_CONTROL_BAR_POSITION JWPlayerControlBar { get; set; }
            public SWFObject.WMODE WindowMode { get; set; }
            public string BackgroundColor { get; set; }
            public string SwfBackgroundColor { get; set; }
            public bool SwfHrefAddTransparentMask { get; set; }

            public FUNCTIONALITY(MyMediaItem item)
            {
                this.SwfHrefAddTransparentMask = true;
                this.JWPlayerAutoStart = true;
                this.JWPlayerRepeat = JWPlayer.JWPLAYER_REPEAT.Always;
                this.JWPlayerControlBar = JWPlayer.JWPLAYER_CONTROL_BAR_POSITION.None;
                _item = item;
                WindowMode = SWFObject.WMODE.None;
            }
            private void initItem()
            {
                if (!string.IsNullOrEmpty(ItemURL))
                {
                    string url = ItemURL.ToLower();
                    bool isSWF = false;
                    if (url.EndsWith(".swf"))
                    {
                        isSWF = true;
                        //SWF File
                        _item.CssManager.AddClass("swf-file");
                        _item.Width = Width;
                        _item.Height = Height;

                        SWFObject swf = new SWFObject();
                        swf.Functionality.FlashUrl = ItemURL;
                        
                        swf.Width = Width;
                        swf.Height = Height;
                        _item.Controls.Add(swf);
                        if (WindowMode != SWFObject.WMODE.None) {
                            swf.Functionality.WindowMode = WindowMode;
                        }
                        swf.Functionality.BackgroundColor = SwfBackgroundColor;
                        _item.Attributes["onmousedown"] += this.OnMouseDownJS;
                    }
                    else if (url.EndsWith(".flv"))
                    {
                        isSWF = true;
                        //Flash file

                        _item.CssManager.AddClass("flv-file");

                        _item.Width = Width;
                        _item.Height = Height;

                        JWPlayer jw = new JWPlayer(ItemURL);
                        //if (WindowMode != null) (always true)
                        {
                            jw.Functionality.WindowMode = WindowMode;
                        }
                        jw.AutoStart = JWPlayerAutoStart;
                        jw.Repeat = JWPlayerRepeat;
                        jw.ControlBar = JWPlayerControlBar;
                        jw.Functionality.FlashWidth = Width;
                        jw.Functionality.FlashHeight = Height;
                        jw.Functionality.BackgroundColor = SwfBackgroundColor;
                        _item.Controls.Add(jw);
                        _item.Attributes["onmousedown"] += this.OnMouseDownJS;
                    }
                    else
                    {
                        //Image
                        this._item.CssManager.AddClass("media-item-image");
                        MyImage img = new MyImage();
                        img.HRef = Href;
                        img.ImageUrl = ItemURL;
                        img.HRefTarget = HrefTarget;
                        img.AlternateText = Caption;
                        _item.Controls.Add(img);
                        _item.Attributes["onmousedown"] += this.OnMouseDownJS;
                        img.Rel = Rel;
                    }

                    if (!string.IsNullOrEmpty(this.BackgroundColor))
                    {
                        this._item.Style.Add(HtmlTextWriterStyle.BackgroundColor, this.BackgroundColor);
                    } else if (!string.IsNullOrEmpty(this.SwfBackgroundColor))
                    {
                        this._item.Style.Add(HtmlTextWriterStyle.BackgroundColor, this.SwfBackgroundColor);
                    }
                    if (isSWF && !string.IsNullOrEmpty(Href) && SwfHrefAddTransparentMask)
                    {
                        addTransparentHrefMask();
                    }

                }
            }
            private void addTransparentHrefMask()
            {
                if (!string.IsNullOrWhiteSpace(this.Href))
                {
                    General_v3.Controls.WebControls.Common.MyAnchor aLink = new General_v3.Controls.WebControls.Common.MyAnchor();
                    aLink.Href = this.Href;
                    aLink.HRefTarget = this.HrefTarget;
                    aLink.Title = this.Caption;
                    aLink.Width = this.Width;
                    aLink.Height = this.Height;
                    aLink.CssManager.AddClass("media-item-swf-link-mask");
                    _item.Controls.AddAt(0, aLink);
                }
            }
            public void Init()
            {
                initItem();
            }
        }

        public string ItemURL { get { return Functionality.ItemURL; } set { Functionality.ItemURL = value; } }
        public string Caption { get { return Functionality.Caption; } set { Functionality.Caption = value; } }
        public string Href { get { return Functionality.Href; } set { Functionality.Href = value; } }
        public Enums.HREF_TARGET HrefTarget { get { return Functionality.HrefTarget; } set { Functionality.HrefTarget = value; } }

        public new FUNCTIONALITY Functionality { get; set; }
        public MyMediaItem()
        {
            Functionality = new FUNCTIONALITY(this);
        }
        
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
