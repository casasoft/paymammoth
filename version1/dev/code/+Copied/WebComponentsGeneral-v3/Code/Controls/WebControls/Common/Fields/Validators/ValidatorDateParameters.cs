﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorDateParameters : ValidatorBaseParameters
    {
        public DateTime? dateFrom;
        public DateTime? dateTo;

        public string dateFormat = "dd/MM/yyyy";
    }
}