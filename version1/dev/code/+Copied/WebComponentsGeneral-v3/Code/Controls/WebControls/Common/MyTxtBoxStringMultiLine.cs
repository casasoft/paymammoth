﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using Fields.Validators;

    public class MyTxtBoxStringMultiLine : MyFormWebControl
    {
        protected override Fields.BaseFieldParameters createFieldParameters()
        {
            return new FieldSingleControlParameters();
        }

        public MyTxtBoxStringMultiLine(FieldSingleControlParameters fieldParameters = null)
            : base("textarea", fieldParameters)
        {
            this.JSFieldTypeName = "FieldSingleControl";
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void updateInitialValue(object value)
        {
            if (value != null)
            {
                this.Value = value.ToString();
                this.Controls.Clear();
                this.Controls.Add(new System.Web.UI.WebControls.Literal() { Text = this.Value });
            }
            else
            {
                this.Value = null;
            }
        }

        public override string JSFieldTypeName
        {
            get;
            set;
        }

        public override string GetDataTypeAsString()
        {
            return "textarea";
        }

        public override void SetFormNames()
        {
            this.setAttributeFromString("name", this.ClientID);
        }

        protected override string getElementClientID()
        {
            return this.ClientID;
        }
        public string GetFormValue()
        {
            return this.WebControlFunctionality.GetFormValue();
        }

        public override object GetInitialTemporaryRandomValue()
        {
            var stringValidator = this.WebControlFunctionality.GetOrCreateValidator<ValidatorString>();
            if (stringValidator != null)
            {


                string str = "";
                int amtLines = CS.General_v3.Util.Random.GetInt(1, 5);
                for (int i = 0; i < amtLines; i++)
                {
                    if (i > 0) str += "\r\n";
                    str += (string)stringValidator.GenerateRandomValidValueAsString();
                }

                return str;
            }
            else
            {
                return null;
            }
        }
    }
}