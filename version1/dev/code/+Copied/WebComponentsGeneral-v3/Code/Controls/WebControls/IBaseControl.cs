﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls
{
    using System.Web.UI;

    public interface IBaseControl 
    {
        Control Control { get; }
        BaseControlFunctionality WebControlFunctionality { get; }
    }
}
