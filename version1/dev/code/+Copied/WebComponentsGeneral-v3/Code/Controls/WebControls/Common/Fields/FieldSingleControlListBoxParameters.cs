﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields
{
    using Classes.Javascript.UI.jQuery.DatePicker;
    using Classes.Javascript.UI.jQuery.MultiSelect;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

    public class FieldSingleControlListBoxParameters : FieldSingleControlParameters
    {

        public _jQueryMultiSelectParams jQueryMultiSelectParams;
        /* public bool showJQueryCalendar = true;
         public JQUERY_CALENDAR_SHOW_TYPE showjQueryCalendarType = JQUERY_CALENDAR_SHOW_TYPE.Both;
         public string jQueryCalendarButtonImageIcon;*/
    }
}