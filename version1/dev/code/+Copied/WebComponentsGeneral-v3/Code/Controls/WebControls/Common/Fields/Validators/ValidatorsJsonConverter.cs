﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorsJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(List<IValidator>);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            List<IValidator> validators = (List<IValidator>)value;

            string js = "";
            for(int i = 0;i<validators.Count;i++) 
            {
                var validator = validators[i];
                if (validator != null)
                {
                    string validatorTypeName = validator.GetType().Name;
                    validator.UpdateExtraParameters();
                    string jsonParams = validator.Parameters.GetJSON();
                    if (!string.IsNullOrEmpty(js)) js += ", ";
                    js += "new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".Classes.Validation." + validatorTypeName + "(" + jsonParams + ")";
                }
            }
            js = "[" + js + "]"; //place in array
            writer.WriteRawValue(js);
            //writer.WriteValue((object)js);
            writer.Flush();
        }
    }
}