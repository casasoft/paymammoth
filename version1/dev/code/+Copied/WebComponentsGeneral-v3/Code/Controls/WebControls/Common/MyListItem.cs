﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using General_v3.Controls.WebControls;

    public class MyListItem : MyInnerHtmlControl
    {

        public MyListItem()
            : base(HtmlTextWriterTag.Li)
        {
        }

        private Literal _lit = new Literal();
        public string Text
        {
            get
            {
                return _lit.Text;
            }
            set
            {
                _lit.Text = value;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.Controls.Add(_lit);
            base.OnLoad(e);
        }
        

    }
}
