﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorIPAddress : ValidatorBase
    {

        public new ValidatorIPAddressParameters Parameters { get { return (ValidatorIPAddressParameters)base.Parameters; } set { base.Parameters = value; } }

        protected override ValidatorBaseParameters createParameters()
        {
            return new ValidatorIPAddressParameters();
        }

        public override object GenerateRandomValidValue()
        {

            string ip = "";
            for (int i = 0; i < 4; i++)
            {
                if (i > 0) ip += ".";
                ip += CS.General_v3.Util.Random.GetInt(0, 255).ToString();
            }
            return ip;
        }
    }
}