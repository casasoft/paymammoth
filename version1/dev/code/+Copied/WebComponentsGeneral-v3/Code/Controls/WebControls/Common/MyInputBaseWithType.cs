namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

    public abstract class MyInputBaseWithType<T> : MyInputBase
    {


        public abstract T InitialValue { get; set; }

        protected MyInputBaseWithType(FieldSingleControlParameters fieldParameters = null)
            : this(INPUT_TYPE.Text, fieldParameters)
        {
        }


        protected MyInputBaseWithType(INPUT_TYPE type, FieldSingleControlParameters fieldParameters = null)
            : base(type, fieldParameters)
        {
            
        }
        

    }
}
