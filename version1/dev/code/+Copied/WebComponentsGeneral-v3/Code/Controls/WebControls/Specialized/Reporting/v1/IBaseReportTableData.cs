﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public interface IBaseReportTableData : IBaseReportTableDataBase
    {
        new IEnumerable<IBaseReportTableHeaderData> HeadersTop { get; }
        new IEnumerable<IBaseReportTableHeaderData> HeadersLeft { get; }
        IBaseReportTableCellData TotalBothHeadersCell { get; }
        new IEnumerable<IBaseReportTableRowData> DataRows { get; }


    }
}
