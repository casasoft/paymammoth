﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public abstract class ValidatorBase : IValidator
    {


        private ValidatorBaseParameters _parameters;
        public ValidatorBaseParameters Parameters
        {
            get
            {
                if (_parameters == null)
                {
                    _parameters = createParameters();
                }
                return _parameters;
            }
            set { _parameters = value; }
        }
        protected abstract ValidatorBaseParameters createParameters();








        public virtual void UpdateExtraParameters()
        {
        }


        public abstract object GenerateRandomValidValue();


        public virtual string GenerateRandomValidValueAsString()
        {
            return this.GenerateRandomValidValue().ToString();
        }
    }
}