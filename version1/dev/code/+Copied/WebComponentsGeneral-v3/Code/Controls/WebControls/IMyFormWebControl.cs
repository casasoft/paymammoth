﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls
{
    using Common.Fields;
    using General_v3;
    using General_v3.Controls.WebControls.Common.General;

    public interface IMyFormWebControl : IBaseWebControl
    {
        /// <summary>
        /// Used mainly for design purposes & CSS
        /// </summary>
        /// <returns></returns>
        string GetDataTypeAsString();

        BaseFieldParameters FieldParameters { get;  }

        object GetFormValueObject();

        bool? ReadOnly { get; set; }
        bool? AutoFocus { get; set; }
        bool? AutoComplete { get; set; }
        bool? Disabled { get; set; }
        string PlaceholderText { get; set; }
        bool NeverFillWithRandomValue { get; set; }
        new MyFormWebControlFunctionality WebControlFunctionality { get;  }

        bool LoadInitialValueFromPostback { get; set; }



    }
}
