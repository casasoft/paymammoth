namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.General_v3.JavaScript.Classes.ShadowBox;
    using System.Web.UI.HtmlControls;
    using General_v3;
    using System.Web;
    using CS.WebComponentsGeneralV3.Code.Classes.Images;

    public class MyImage : BaseWebControl, IBaseWebControl
    {
        public bool DoNotCacheWidthAndHeight { get; set; }
        const string CACHE_WIDTH_SUFFIX = "_width";
        const string CACHE_HEIGHT_SUFFIX = "_height";

        private Image _img = new Image();
        private MyAnchor _a = new MyAnchor();

        public string LongDesc { get { return _img.Attributes["longdesc"]; } set { _img.Attributes["longdesc"] = value; } }

        public string Rel { get { return _a.Rel; } set { _a.Rel = value; _img.Attributes["rel"] = value; } }
      
        private BaseWebControlFunctionality _functionality = null;
        
        public MyImage()
            : base( )
        {
            _functionality = new BaseWebControlFunctionality(this);
             
        }

        public string AlternateText { get { return _img.AlternateText; } set { _img.AlternateText = value; } }

        public string ImageUrl { get { return _img.ImageUrl; } set { _img.ImageUrl = value; } }

        public string HRef { get { return _a.Href; } set { _a.Href = value; } }

        public Enums.HREF_TARGET HRefTarget { get { return _a.HrefTarget; } set { _a.HrefTarget = value; } }

        public void ForceRender()
        {
             if (!string.IsNullOrEmpty(_a.Href))
            {
                //Contains link
                _a.Controls.Add(_img);
                this.Controls.Add(_a);
            }
            else
            {
                this.Controls.Add(_img);
            }
             outputWidthAndHeight();
        }
        

        private void outputWidthAndHeight()
        {
            
            if (!DoNotCacheWidthAndHeight && !string.IsNullOrEmpty(this.ImageUrl))
            {
                string localPath = null;
                ImageDimensionCacheItem cachedDimensions = null;
                bool isRelativeUrl = !CS.General_v3.Util.PageUtil.IsAbsoluteURL(this.ImageUrl);
                if (isRelativeUrl)
                {
                    localPath = CS.General_v3.Util.PageUtil.MapPath(this.ImageUrl);
                    cachedDimensions = ImageDimensionCacheManager.Instance.GetImageDimensionsFromCache(localPath);
                }


                //string cacheID = getImageCacheID(localPath);
                int? w = (cachedDimensions != null ? cachedDimensions.Width : null);
                int? h = (cachedDimensions != null ? cachedDimensions.Height : null);
                if (!w.HasValue || !h.HasValue)
                {
                    try
                    {
                        int width, height;
                        string file = CS.General_v3.Util.PageUtil.MapPath(ImageUrl);
                        CS.General_v3.Util.Image.getImageSize(file, out width, out height);
                        w = width;
                        h = height;
                        ImageDimensionCacheManager.Instance.SetCachedWidthAndHeight(w, h, localPath);
                        
                    }
                    catch (HttpException ex)
                    {

                    }
                }
                if (w.HasValue && h.HasValue && w > 0 && h > 0)
                {
                    this._img.Style.Add(HtmlTextWriterStyle.Width, w + "px");
                    this._img.Style.Add(HtmlTextWriterStyle.Height, h + "px");
                }

                if (isRelativeUrl)
                {
                    //Watch for changes in file system

                    ImageDimensionCacheManager.Instance.WatchFile(localPath);
                    
                }
            }
            
        }
        

        protected override void OnLoad(EventArgs e)
        {
            ForceRender();  
            
            base.OnLoad(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            outputWidthAndHeight();
            base.OnPreRender(e);
        }
    }
    
}
