﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields
{
    using JSControllers;
    using Validators;
using Newtonsoft.Json;
    using BusinessLogic_v3.Frontend.ContentTextModule;

    public abstract class BaseFieldParameters : JSONObject
    {

        public string id;


        private string _title;
        public string title
        {
            get
            {
                if (titleContentText != null)
                {
                    return titleContentText.GetContent();
                }
                else
                {
                    return _title;
                }
            }
            set
            {
                _title = value;
            }
        }
        [JsonIgnore]
        public ContentTextBaseFrontend titleContentText;

        [JsonIgnore]
        public ContentTextBaseFrontend helpMessageContentText;

        [JsonConverter(typeof(ValidatorsJsonConverter))]
        public List<IValidator> validators = new List<IValidator>();

        public bool doNotShowMessagesOnFocus;
        public bool doNotValidateOnBlur;
        /// <summary>
        /// When a field loses focus, error message is shown and hidden after this duration
        /// </summary>
        public int validationIconShowErrorMessageAndHideDurationMs = 1500;
        public bool validateIfNotVisible = false;
        public string placeholderText;
        public string helpMessage;
        public string validationGroup = FormFields.FormFields.DEFAULT_VALIDATION_GROUP;
        public bool required;
        public string validationIconContainerID;
        public string helpMessageIconContainerID;

        /// <summary>
        /// To enable show / hide of certain fields/rows
        /// </summary>
        public FieldRowsVisibilityControllerParameters rowsVisiblityParameters;


        /*public TValidator GetValidatorOfType<TValidator>() where TValidator : IValidator
        {
            foreach(var validator in this.validators)
            {
                if (validator is TValidator)
                {
                    return (TValidator)validator;
                }
            }
            return default(TValidator);
        }*/
        public TValidator GetValidator<TValidator>() where TValidator :class, IValidator
        {
            if (validators != null)
            {
                for (int i = 0; i < validators.Count; i++)
                {
                    if (validators[i] is TValidator)
                    {
                        return (TValidator)validators[i];
                    }
                }
            }
            return null;
        }
        public TValidator GetOrCreateValidator<TValidator>() where TValidator : class, IValidator, new()
        {
            var validator = GetValidator<TValidator>();

            if (validator == null)
            {
                //Not found.. so create the validator
                if (validators == null)
                {
                    validators = new List<IValidator>();
                }
                var newValidator = new TValidator();
                validators.Add((IValidator)newValidator);
                return newValidator;
            }
            else
            {
                return validator;
            }

        }
        protected override void onPreGetJson()
        {
            if (helpMessageContentText != null && string.IsNullOrEmpty(helpMessage))
            {
                MyDiv divHelpMessageContentText = new MyDiv();
                string helpMessageID = this.id + "_helpMessage";
                var cntText = MyContentText.AttachContentTextWithControl(divHelpMessageContentText, helpMessageContentText);
                cntText.ForceRender();

                string html = CS.General_v3.Util.ControlUtil.RenderControl(divHelpMessageContentText);
                helpMessage = html;
            }
            base.onPreGetJson();
        }

    }
}