﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorStringParameters : ValidatorBaseParameters
    {
        public int minLength = 0;
        public int maxLength = 0;
        /// <summary>
        /// A comma-seperated string of file extensions allowed, e.g gif, bmp, tif...
        /// </summary>
        public IEnumerable<string> fileExtensionsAllowed;
        public int maxWords = 0;
        public object[] valueIn;
        public object[] valueNotIn;
        public bool valueRangeCaseSensitive = false;
        public Regex regExpPattern;

        public bool isAlphaNumericOnly = false;
    }
}