﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Frontend.ContentTextModule;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    public class MyDropDownOption : BaseWebControl, IMyDropdownListChildItem
    {
        public class FUNCTIONALITY
        {
            private MyDropDownOption _control;

            public MyListItemData ItemData { get; set; }

            public bool Disabled { get { return ItemData.Disabled; } set { ItemData.Disabled = value; } }
            public bool Selected { get { return ItemData.Selected; } set { ItemData.Selected = value; } }
            public string Label { get { return ItemData.Label; } set { ItemData.Label = value; } }
            public string Value { get { return ItemData.Value; } set { ItemData.Value = value; } }
            public ContentTextBaseFrontend LabelContentText { get { return ItemData.LabelContentText; } set { ItemData.LabelContentText = value; } }


                public FUNCTIONALITY(MyDropDownOption control)
            {
                this.ItemData = new MyListItemData();
                _control = control;
            }
        }

        public FUNCTIONALITY Functionality { get; private set; }
        public MyDropDownOption(string label = null, string value = null)
            : base(System.Web.UI.HtmlTextWriterTag.Option)
        {
            this.Functionality = getFunctionality();
            this.Functionality.ItemData.Label = label;
            this.Functionality.ItemData.Value = value;
        }

        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        public new BaseWebControl Control
        {
            get { return this; }
        }
        private void updateAttributes()
        {

            this.setAttributeFromBool("disabled", this.Functionality.ItemData.Disabled);
            this.setAttributeFromBool("selected", this.Functionality.ItemData.Selected);
            this.setAttributeFromString("label", (this.Functionality.ItemData.GetLabel()));
            this.setAttributeFromString("value", this.Functionality.ItemData.Value, false);

            //this.setAttributeFromBool("value", this.Functionality.ItemData.Disabled);

           /* public bool Disabled { get { return _control.getAttributeAsBool("disabled"); } set { _control.setAttributeFromBool("disabled", value); } }
        public bool Selected { get { return _control.getAttributeAsBool("selected"); } set { _control.setAttributeFromBool("selected", value); } }

        public string Label { get { return _control.getAttributeAsString("label"); } set { _control.setAttributeFromString("label", value); } }
        public ContentTextBaseFrontend LabelContentText { get; set; }
        public string Value { get { return _control.getAttributeAsString("value"); } set { _control.setAttributeFromString("value", value); } }*/
            
        }
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
        protected override void OnPreRender(EventArgs e)
        {

            //Update it in the end when user have set such properties
            ForceRender();
            base.OnPreRender(e);
        }


        public override void ForceRender()
        {
            updateAttributes();

            this.Controls.Add(new System.Web.UI.WebControls.Literal() { Text = this.Functionality.ItemData.GetLabel() });
            base.ForceRender();
        }

        public ContentTextBaseFrontend LabelContentText { get { return this.Functionality.LabelContentText; } set { this.Functionality.LabelContentText = value; } }


        public string Label
        {
            get
            {
                return this.Functionality.Value;
            }
            set
            {
                this.Functionality.Value = value;
            }
        }

        public string Value
        {
            get
            {
                return this.Functionality.Value;
            }
            set
            {
                this.Functionality.Value = value;
            }
        }
    }
}
