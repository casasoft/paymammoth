﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public class MySpan : MyInnerHtmlControl
    {

        public MySpan()
            : base(HtmlTextWriterTag.Span)
        {
            
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
        

    }
}
