using System.Text;
using BusinessLogic_v3.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.General_v3;
using System.Linq;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{

    public class MyTime : MyInnerHtmlControl
    {
        public enum DATE_DISPLAY_TYPE
        {
            Literal, //Do nothing but leave middle text as is
            RelevantToToday, //E.g. Today at 08:30
            Static //E.g. 07/12/2011 08:30
        }
        public string DateTimeFormat { get { return this.Functionality.DateTimeFormat; } set { this.Functionality.DateTimeFormat = value; } }
        public string TimeOnlyFormat { get { return this.Functionality.TimeOnlyFormat; } set { this.Functionality.TimeOnlyFormat = value; } }
        public bool IfTodayOrYesterdayIncludeOnlyTime { get { return this.Functionality.IfTodayOrYesterdayIncludeOnlyTime; } set { this.Functionality.IfTodayOrYesterdayIncludeOnlyTime = value; } }
        public DATE_DISPLAY_TYPE DisplayType { get { return this.Functionality.DisplayType; } set { this.Functionality.DisplayType = value; } }
        public DateTime? DateTime { get { return this.Functionality.DateTime; } set { this.Functionality.DateTime = value; } }



        #region MyTime Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected MyTime _item = null;
            public bool IfTodayOrYesterdayIncludeOnlyTime { get; set; }

            public DateTime? DateTime { get; set; }
            public string DateTimeFormat { get; set; }
            public string TimeOnlyFormat { get; set; }
            public DATE_DISPLAY_TYPE DisplayType { get; set; }

            internal FUNCTIONALITY(MyTime item)
            {
                this._item = item;
                this.TimeOnlyFormat = "HH:mm";
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			


        public MyTime()
            : base("time")
        {
            this.DateTimeFormat = CS.General_v3.Enums.SETTINGS_ENUM.Website_LongDateTimeFormat.GetSettingValue<string>();
            this.DisplayType = DATE_DISPLAY_TYPE.RelevantToToday;
        }

        


        /// <summary>
        /// The pubdate attribute indicates that the date/time in the <time> element is the publication date of the document (or the nearest ancestor <article> element).
        /// </summary>
        public bool? IsPublicationDateOfArticle { get { return getAttributeAsBoolNullable("pubdate"); } set { setAttributeFromBool("pubdate", value); } }

        private void initTextRelevantToToday()
        {
            DateTime today = CS.General_v3.Util.Date.Now;
            DateTime todayStart = new System.DateTime(today.Year, today.Month, today.Day);
            DateTime checkDateStart = new System.DateTime(this.DateTime.Value.Year, this.DateTime.Value.Month, this.DateTime.Value.Day);

            double daysDiff = CS.General_v3.Util.Date.GetAmtDaysDifference(checkDateStart, todayStart);
            string dateText = this.DateTime.Value.ToString(this.DateTimeFormat);
            string timeText = this.DateTime.Value.ToString(this.TimeOnlyFormat);
            ContentTextBaseFrontend ct = null;
            if (daysDiff < 1)
            {
                //Today
                ct = BusinessLogic_v3.Enums.CONTENT_TEXT.DateTime_Today.GetContentTextFrontend();
                ct.TokenReplacer[BusinessLogic_v3.Constants.Tokens.TAG_DATETIME] = IfTodayOrYesterdayIncludeOnlyTime ? timeText : dateText;
            }
            else if (daysDiff < 2)
            {
                //Yesterday
                ct = BusinessLogic_v3.Enums.CONTENT_TEXT.DateTime_Yesterday.GetContentTextFrontend();
                ct.TokenReplacer[BusinessLogic_v3.Constants.Tokens.TAG_DATETIME] = IfTodayOrYesterdayIncludeOnlyTime ? timeText : dateText;
            }
            else 
            {
                this.InnerHtml = dateText;
            }
            if (ct != null)
            {
                MyContentText.AttachContentTextWithControl(this, ct);
            }
        }

        private void initText()
        {
            switch (DisplayType)
            {
                case DATE_DISPLAY_TYPE.Literal:
                    //do nothing
                    break;
                case DATE_DISPLAY_TYPE.Static:
                    this.InnerHtml = DateTime.Value.ToString(this.DateTimeFormat);
                    break;
                case DATE_DISPLAY_TYPE.RelevantToToday:
                    initTextRelevantToToday();
                    break;

            }
        
        }

        private void initDate()
        {
            if (DateTime.HasValue)
            {
                //"yyyy'-'MM'-'dd HH':'mm':'ss'Z'".
                string dateAttr = GetDateTimeForAttribute(DateTime.Value);
                this.setAttributeFromString("datetime", dateAttr);

               
                initText();
                    
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            initDate();
            base.OnLoad(e);
        }

        public static string GetDateTimeForAttribute(DateTime value)
        {
            string dateAttr = value.ToString("u");
            return dateAttr;

        }

    }
}
