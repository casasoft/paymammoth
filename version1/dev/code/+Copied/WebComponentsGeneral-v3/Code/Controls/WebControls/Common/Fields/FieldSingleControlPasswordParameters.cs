﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields
{
    using BusinessLogic_v3;
    using Classes.Javascript.UI.jQuery.DatePicker;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;
    

    public class FieldPasswordStrengthMarkerParameters
    {

        public int minLength = 0;

        public string textStrength1 = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Password_TooWeak).GetContent();
        public string textStrength2 = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Password_Normal).GetContent();
        public string textStrength3 = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Password_Strong).GetContent();
        public string textStrength4 = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Password_Perfect).GetContent();

    }
    public class FieldSingleControlPasswordParameters : FieldSingleControlParameters
    {
        public Enums.PASSWORD_STRENGTH_CHARACTERS passwordStrengthType = Enums.PASSWORD_STRENGTH_CHARACTERS.None;
        public bool showPasswordStrengthMeter = false;
        public FieldPasswordStrengthMarkerParameters passwordParameters = new FieldPasswordStrengthMarkerParameters();
    }
}