﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public interface IValidator
    {
        ValidatorBaseParameters Parameters { get; }
        /// <summary>
        /// This is called before serializing the JSON of the validator, so that you can update any required parameters
        /// </summary>
        void UpdateExtraParameters();

        object GenerateRandomValidValue();
        string GenerateRandomValidValueAsString();
    }
}
