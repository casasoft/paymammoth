﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System.Web.UI;
    using General_v3.Controls.WebControls;
    using System;

    public class MyPlaceHolder : System.Web.UI.WebControls.PlaceHolder, IBaseControl
    {
        private BaseWebControlFunctionality _functionality = null;

        public MyPlaceHolder()
            : base()
        {
            init();
        }


        private void init()
        {
            
        }

        public override string ClientID
        {
            get
            {
                throw new NotSupportedException("This is not supported with placeholder as it is not a control - override this");
            }
        }
        public override string ID
        {
            get
            {
                throw new NotSupportedException("This is not supported with placeholder as it is not a control - override this");
            }
            set
            {
                throw new NotSupportedException("This is not supported with placeholder as it is not a control - override this");
            }
        }

        #region IBaseControl Members

        public Control Control
        {
            get { return this; }
        }

        public BaseControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion
    }
}
