﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using BusinessLogic_v3.Frontend.ContentTextModule;

    public class MyDropDownListOptionGroup : BaseWebControl, IMyDropdownListChildItem
    {
        public class FUNCTIONALITY
        {
            private MyDropDownListOptionGroup _control;

            public bool? Disabled { get; set; }
            public string Label { get; set; }
            public ContentTextBaseFrontend LabelContentText { get; set; }

            public List<MyDropDownOption> Options { get; set; }

            public FUNCTIONALITY(MyDropDownListOptionGroup control)
            {
                this.Options = new List<MyDropDownOption>();
                _control = control;
            }
        }

        public FUNCTIONALITY Functionality { get; private set; }
        public MyDropDownListOptionGroup()
            : base("optgroup")
        {
            this.Functionality = getFunctionality();
        }

        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        public new BaseWebControl Control
        {
            get { return this; }
        }
        private void updateAttributes()
        {
            if (this.Functionality.Disabled.HasValue) this.Attributes["disabled"] = this.Functionality.Disabled.Value ? "disabled" : "";
            if (!string.IsNullOrEmpty(this.Functionality.Label)) this.Attributes["label"] = this.Functionality.Label;
        }
        private void addChildOptions()
        {
            if (this.Functionality.Options != null)
            {
                foreach (var option in this.Functionality.Options)
                {
                    this.Controls.Add(option);
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            ForceRender();
            base.OnLoad(e);
        }
        public override void ForceRender()
        {
            updateAttributes();
            addChildOptions();
            base.ForceRender();
        }

        public ContentTextBaseFrontend LabelContentText { get { return this.Functionality.LabelContentText; } set { this.Functionality.LabelContentText = value; } }

            public string Label
        {
            get
            {
                return this.Functionality.Label;
            }
            set
            {
                this.Functionality.Label = value;
            }
        }

        public string Value
        {
            get
            {
                return null;
            }
            set
            {
            }
        }
    }
}
