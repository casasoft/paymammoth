namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Fields;
    using General_v3;
    using General_v3.Controls.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

    public class MyTxtBoxWebsite : MyInputBaseWithType<string>
    {


        public MyTxtBoxWebsite(FieldSingleControlParameters fieldParameters = null)
            : base(INPUT_TYPE.Website, fieldParameters)
        {
            
        }


        protected override void OnLoad(EventArgs e)
        {
            this.WebControlFunctionality.GetOrCreateValidator<ValidatorWebsite>(); // create it if not exists

            base.OnLoad(e);
        }
        public override string InitialValue
        {
            get
            {
                return (string)this.WebControlFunctionality.InitialValue;
            }
            set
            {
                this.WebControlFunctionality.InitialValue = value;
            }
        }

        public override object GetInitialTemporaryRandomValue()
        {
            var stringValidator = this.WebControlFunctionality.GetOrCreateValidator<ValidatorWebsite>();
            if (stringValidator != null)
            {
                return (string)stringValidator.GenerateRandomValidValue();
            }
            else
            {
                return null;
            }
        }
    }
}
