namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.ComponentModel;
    using System.Web.UI;
using System.Web.UI.WebControls;
    using BusinessLogic_v3.Frontend.ContentTextModule;

    public class MyButton : BaseButtonWebControl
    {
        public MyButton()
            : base(HtmlTextWriterTag.Button, "")
        {
        }
        public MyButton(string id)
            : base(HtmlTextWriterTag.Button, id)
        {
        }

        public Literal LiteralText { get; set; }

        /// <summary>
        /// This is used to show the hover text
        /// </summary>
        public string Title { get { return this.getAttributeAsString("title"); } set { this.setAttributeFromString("title", value); } }


        protected override void OnLoad(EventArgs e)
        {
            if(this.LiteralText != null)
            {
                Controls.Add(LiteralText);
            }
            base.OnLoad(e);
        }
        private void outputContentText()
        {
            if (this.TextContentText != null)
            {
                MyContentText.AttachContentTextWithControl(this, this.TextContentText);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            outputContentText();
            if (string.IsNullOrEmpty(this.ID))
            {
                throw new Exception("ID cannot be null for form elements");
            }
            base.OnPreRender(e);
        }

       // public new string Value { get; set; }


        public override string Text
        {
            get
            {
                return getAttributeAsString("value");
            }
            set
            {
                
                this.InnerHTML = string.IsNullOrEmpty(value) ? value :  CS.General_v3.Util.PageUtil.HtmlEncode(value);
            }
        }
        private ContentTextBaseFrontend _textContentText;
        public ContentTextBaseFrontend TextContentText
        {
            get
            {
                return _textContentText;
            }
            set
            {
                _textContentText = value;
                
                if (value != null)
                {
                    this.Text = _textContentText.GetContent();
                    MyContentText.AttachContentTextWithControl(this, value);
                }
            }
        }
        public string InnerHTML
        {
            get
            {
                return getAttributeAsString("value");
            }
            set
            {
                setAttributeFromString("value", value);
                if (LiteralText == null)
                {
                    LiteralText = new Literal();
                }

                LiteralText.Text = value;
            }
        }

        
    }
}
