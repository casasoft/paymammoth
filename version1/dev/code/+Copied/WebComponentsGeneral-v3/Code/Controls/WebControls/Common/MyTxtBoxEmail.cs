namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Fields;
    using General_v3;
    using General_v3.Controls.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

    public class MyTxtBoxEmail : MyInputBaseWithType<string>
    {


        public MyTxtBoxEmail(FieldSingleControlParameters fieldParameters = null)
            : base(INPUT_TYPE.Email, fieldParameters)
        {
        }


        protected override void OnLoad(EventArgs e)
        {
            this.WebControlFunctionality.GetOrCreateValidator<ValidatorEmail>(); // create it if not exists

            base.OnLoad(e);
        }
        public override string InitialValue
        {
            get
            {
                return (string)this.WebControlFunctionality.InitialValue;
            }
            set
            {
                this.WebControlFunctionality.InitialValue = value;
            }
        }

        public override object GetInitialTemporaryRandomValue()
        {
            var validator = this.WebControlFunctionality.GetOrCreateValidator<ValidatorEmail>();
            if (validator != null)
            {
                string value = validator.GenerateRandomValidValueAsString();
                return value.ToString();
            }
            else
            {
                return null;
            }
        }
    }
}
