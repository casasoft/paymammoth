﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;
using Newtonsoft.Json;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.JQueryFullCalendar.v1
{
    public class _jQueryFullCalendarOptions : JSONObject
    {

        public class _jQueryFullCalendarOptionsEventObject : JSONObject
        {
            /// <summary>
            /// String/Integer - Optional
            /// 
            /// Uniquely identifies the given event. Different instances of repeating events should all have the same id.
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// String - Required
            /// 
            /// The text on an event's element
            /// </summary>
            public string title;

            /// <summary>
            /// Boolean - Optional
            /// 
            /// Whether an event occurs at a specific time-of-day. This property affects whether an event's time is shown.
            /// Also, in the agenda views, determines if it is displayed in the "all-day" section.
            /// 
            /// When specifying Event Objects for events or eventSources, omitting this property will make it inherit from
            /// allDayDefault, which is normally true.
            /// </summary>
            public bool? allDay;

            /// <summary>
            /// DateTime - Required
            /// 
            /// The date/time an event begins.
            /// 
            /// When specifying Event Objects for events or eventSources, you may specify a string in IETF format
            /// (ex: "Wed, 18 Oct 2009 13:00:00 EST"), a string in ISO8601 format (ex: "2009-11-05T13:15:30Z") or
            /// a UNIX timestamp.
            /// </summary>
            public DateTime? start;

            /// <summary>
            /// DateTime - Optional
            /// 
            /// The date/time an event ends.
            /// 
            /// As with start, you may specify it in IETF, ISO8601, or UNIX timestamp format.
            /// 
            /// If an event is all-day...
            /// the end date is inclusive. This means an event with start Nov 10 and end Nov 12 will span 3 days on the calendar.
            /// 
            /// If an event is NOT all-day...
            /// the end date is exclusive. This is only a gotcha when your end has time 00:00. It means your event ends on
            /// midnight, and it will not span through the next day.
            /// </summary>
            public DateTime? end;

            /// <summary>
            /// String - Optional
            /// 
            /// A URL that will be visited when this event is clicked by the user. For more information on controlling this
            /// behavior, see the eventClick callback.
            /// </summary>
            public string url;

            /// <summary>
            /// String/Array - Optional
            /// 
            /// A CSS class (or array of classes) that will be attached to this event's element.
            /// </summary>
            public string className;

            /// <summary>
            /// Boolean - Optional
            /// 
            /// Overrides the master editable option for this single event.
            /// </summary>
            public bool? editable;

            /// <summary>
            /// Event Source Object - Automatically populated
            /// 
            /// A reference to the event source that this event came from.
            /// </summary>
            public object source;

            /// <summary>
            /// String - Optional
            /// 
            /// Sets an event's background and border color just like the calendar-wide eventColor option.
            /// </summary>
            public string color;

            /// <summary>
            /// String - Optional
            /// 
            /// Sets an event's background color just like the calendar-wide eventBackgroundColor option.
            /// </summary>
            public string backgroundColor;

            /// <summary>
            /// String - Optional
            /// 
            /// Sets an event's border color just like the the calendar-wide eventBorderColor option.
            /// </summary>
            public string borderColor;

            /// <summary>
            /// String - Optional
            /// 
            /// Sets an event's text color just like the calendar-wide eventTextColor option.
            /// </summary>
            public string textColor;
        }


        public class _jQueryFullCalendarOptionsViewObject : JSONObject
        {
            public string name;
            public string title;
            public DateTime? start;
            public DateTime? end;
            public DateTime? visStart;
            public DateTime? visEnd;
        }

        public class _jQueryFullCalendarOptionsbuttonIconsObject : JSONObject
        {
            public string prev;
            public string next;
        }


        /// <summary>
        /// Each option can contain one or more of these: 'title', 'prev', 'next',
        /// 'prevYear', 'nextYear', 'today', 'month', 'basicWeek', 'basicDay',
        /// 'agendaWeek', 'agendaDay'
        /// </summary>
        public class _jQueryFullCalendarOptionsHeader : JSONObject
        {
            public string left;
            public string center;
            public string right;
        }
        public _jQueryFullCalendarOptionsHeader header;

        public class _jQueryFullCalendarOptionsButtonText : JSONObject
        {
            public string prev;
            public string next;
            public string prevYear;
            public string nextYear;
            public string today;
            public string month;
            public string week;
            public string day;
        }

        public _jQueryFullCalendarOptionsButtonText buttonText;

        /// <summary>
        /// This option only applies to calendars that have jQuery UI theming enabled with the theme option.
        /// 
        /// A hash must be supplied that maps button names (from the header) to icon strings.
        /// The icon strings determine the CSS class that will be used on the button.
        /// For example, the string 'circle-triangle-w' will result in the class 'ui-icon-triangle-w'.
        /// 
        /// If a button does not have an entry, it falls back to using buttonText.
        /// 
        /// If you are using a jQuery UI theme and would prefer not to display any icons and would rather use buttonText instead, you can set
        /// the buttonIcons option to false.
        /// </summary>
        public _jQueryFullCalendarOptionsbuttonIconsObject buttonIcons;


        /// <summary>
        /// Once you enable theming with true, you still need to include the CSS file for the theme you want.
        /// </summary>
        public bool? theme;



        /// <summary>
        /// The value must be a number that represents the day of the week.
        /// 
        /// Sunday=0, Monday=1, Tuesday=2, etc.
        /// 
        /// This option is useful for UK users who need the week to start on Monday (1).
        /// </summary>
        public int? firstDay;

        /// <summary>
        /// Displays the calendar in right-to-left mode.
        /// 
        /// This option is useful for right-to-left languages such as Arabic and Hebrew.
        /// </summary>
        public bool? isRTL;

        /// <summary>
        /// Whether to include Saturday/Sunday columns in any of the calendar views
        /// </summary>
        public bool? weekends;

        /// <summary>
        /// Determines the number of weeks displayed in a month view. Also determines each week's height.
        /// 
        /// 'fixed' - The calendar will always be 6 weeks tall. The height will always be the same, as determined by height, contentHeight, or aspectRatio.
        /// 
        /// 'liquid' - The calendar will have either 4, 5, or 6 weeks, depending on the month. The height of the weeks will stretch to fill the available
        /// height, as determined by height, contentHeight, or aspectRatio.
        /// 
        /// 'variable' - The calendar will have either 4, 5, or 6 weeks, depending on the month. Each week will have the same constant height,
        /// meaning the calendar’s height will change month-to-month.
        /// </summary>
        public string weekMode;

        /// <summary>
        /// Will make the entire calendar (including header) a pixel height.
        /// 
        /// By default, this option is unset and the calendar's height is calculated by aspectRatio.
        /// </summary>
        public int? height;

        /// <summary>
        /// Will make the calendar's content area a pixel height.
        /// 
        /// By default, this option is unset and the calendar's height is calculated by aspectRatio.
        /// </summary>
        public int? contentHeight;

        /// <summary>
        /// Determines the width-to-height aspect ratio of the calendar.
        /// 
        /// A calendar is a block-level element that fills its entire avaiable width. The calendar’s height, however, is determined by
        /// this ratio of width-to-height. (Hint: larger numbers make smaller heights).
        /// </summary>
        public double? aspectRatio;



        /// <summary>
        /// The initial view when the calendar loads.
        /// 
        /// Can take any of these: 'month', 'basicWeek', 'basicDay', 'agendaWeek', 'agendaDay'
        /// </summary>
        public string defaultView;

        /// <summary>
        /// Determines if the "all-day" slot is displayed at the top of the calendar.
        /// 
        /// When hidden with false, all-day events will not be displayed in agenda views.
        /// </summary>
        public bool? allDaySlot;

        /// <summary>
        /// The text titling the "all-day" slot at the top of the calendar.
        /// </summary>
        public string allDayText;

        /// <summary>
        /// Determines the time-text that will be displayed on the vertical axis of the agenda views.
        /// 
        /// The value is a format-string that will be processed by formatDate.
        /// 
        /// The default value will produce times that look like "5pm" and "5:30pm".
        /// </summary>
        public string axisFormat;

        /// <summary>
        /// The frequency for displaying time slots, in minutes.
        /// 
        /// The default will make a slot every half hour.
        /// </summary>
        public int? slotMinutes;

        /// <summary>
        /// Determines the length (in minutes) an event appears to be when it has an unspecified end date.
        /// 
        /// By default, if an Event Object as no end, it will appear to be 2 hours.
        /// 
        /// This option only affects events that appear in the agenda slots, meaning they have allDay set to true.
        /// </summary>
        public int? defaultEventMinutes;

        /// <summary>
        /// Determines the first hour that will be visible in the scroll pane.
        /// 
        /// Values must be from 0-23, where 0=midnight, 1=1am, etc.
        /// 
        /// The user will be able to scroll upwards to see events before this time. If you want to prevent users from doing this, use
        /// the minTime option instead.
        /// </summary>
        public int? firstHour;

        /// <summary>
        /// Determines the first hour/time that will be displayed, even when the scrollbars have been scrolled all the way up.
        /// 
        /// This can be a number like 5 (which means 5am), a string like '5:30' (which means 5:30am) or a string like '5:30am'.
        /// </summary>
        public object minTime;

        /// <summary>
        /// Determines the last hour/time (exclusively) that will be displayed, even when the scrollbars have been scrolled all the way down.
        /// 
        /// This can be a number like 22 (which means 10pm), a string like '22:30' (which means 10:30pm) or a string like '10:30pm'.
        /// </summary>
        public object maxTime;

        /// <summary>
        /// The initial year when the calendar loads.
        /// 
        /// Must be a 4-digit year like 2009.
        /// 
        /// If year is unspecified, the calendar will begin at the current year.
        /// </summary>
        public int? year;

        /// <summary>
        /// The initial month when the calendar loads.
        /// 
        /// IMPORTANT: The value is 0-based, meaning January=0, February=1, etc.
        /// 
        /// If month is unspecified and year is set to the current year, the calendar will start on the current month.
        /// If month is unspecified and year is not set to the current year, the calendar will start on January.
        /// </summary>
        public int? month;

        /// <summary>
        /// The initial date-of-month when the calendar loads.
        /// 
        /// This option only matters for the week & day views. Month view does not need this option, because month view
        /// always displays the entire month from start to finish.
        /// 
        /// If date is unspecified, and year/month are set to the current year/month, then the calendar will start on
        /// the current date. If date is unspecified, and year/month are not set to the current year/month, then the
        /// calendar will start on the first of the month.
        /// </summary>
        public int? date;

        /// <summary>
        /// Determines the time-text that will be displayed on each event.
        /// 
        /// A single format string will change the time-text for events in all
        /// views. A View Option Hash may be provided to target specific views (this is what the default does).
        /// 
        /// Uses formatDate/formatDates formatting rules.
        /// 
        /// Time-text will only be displayed for Event Objects that have allDay equal to false.
        /// </summary>
        public string timeFormat;

        /// <summary>
        /// Determines the text that will be displayed on the calendar's column headings.
        /// 
        /// A single string will set the title format for all views. A View Option Hash may be provided
        /// to target specific views (this is what the default does).
        /// 
        /// Uses formatDate/formatDates formatting rules.
        /// </summary>
        public string columnFormat;

        /// <summary>
        /// Determines the text that will be displayed in the header's title.
        /// 
        /// A single string will set the title format for all views. A View Option Hash may be provided
        /// to target specific views (this is what the default does).
        /// 
        /// Uses formatDate/formatDates formatting rules. 
        /// </summary>
        public string titleFormat;

        /// <summary>
        /// Full names of months. [Array]
        /// 
        /// ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        /// </summary>
        public object monthNames;

        /// <summary>
        /// Abbreviated names of months. [Array]
        /// 
        /// ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        /// </summary>
        public object monthNamesShort;

        /// <summary>
        /// Full names of days-of-week [Array]
        /// 
        /// ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
        /// </summary>
        public object dayNames;

        /// <summary>
        /// Abbreviated names of days-of-week. [Array]
        /// 
        /// ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        /// </summary>
        public object dayNamesShort;


        /// <summary>
        /// Allows a user to highlight multiple days or timeslots by clicking and dragging.
        /// 
        /// To let the user make selections by clicking and dragging, this option must be set to true.
        /// 
        /// The select and unselect callbacks will be useful for monitoring when selections are made and cleared.
        /// 
        /// To learn the ways in which selections can be cleared, read the docs for the unselect callback.
        /// </summary>
        public bool? selectable;

        /// <summary>
        /// Whether to draw a "placeholder" event while the user is dragging.
        /// 
        /// This option only applies to the agenda views.
        /// 
        /// A value of true will draw a "placeholder" event while the user is dragging (similar to what Google
        /// Calendar does for its week and day views). A value of false (the default) will draw the standard
        /// highlighting over each cell.
        /// 
        /// A function can also be specified for drawing custom elements. It will be given 2 arguments: the
        /// selection's start date and end date (Date objects). It must return a DOM element that will be used.
        /// </summary>
        public bool? selectHelper;

        /// <summary>
        /// Whether clicking elsewhere on the page will cause the current selection to be cleared.
        /// 
        /// This option can only take effect when selectable is set to true.
        /// </summary>
        public bool? unselectAuto;

        /// <summary>
        /// A way to specify elements that will ignore the unselectAuto option.
        /// 
        /// Clicking on elements that match this jQuery selector will prevent the current selection from being cleared
        /// (due to the unselectAuto option).
        /// 
        /// This option is useful if you have a "Create an event" form that shows up in response to the user making a
        /// selection. When the user clicks on this form, you probably don't want to the current selection to go away.
        /// Thus, you should add a class to your form such as "my-form", and set the unselectCancel option to ".my-form".
        /// </summary>
        public string unselectCancel;






        /// <summary>
        /// This can be an array of events, a json feed or a function. Refer to documentation for more info.
        /// 
        /// http://arshaw.com/fullcalendar/docs/event_data/events_array/
        /// Can be either a List<_jQueryFullCalendarOptionsEventObject> or else JSON Feed e.g. /ajax/feed.ashx which will be passed parameters start & end The value of the parameters will always be UNIX timestamps (seconds since 1970).
        /// </summary>
        public object events;

        

        /// <summary>
        /// Determines the default value for each Event Object's allDay property, when it is unspecified.
        /// </summary>
        public bool? allDayDefault;

        /// <summary>
        /// When parsing ISO8601 dates, whether UTC offsets should be ignored while processing event source data.
        /// 
        /// The default is true, which means the UTC offset for all ISO8601 dates will be ignored. For example, the
        /// date "2008-11-05T08:15:30-05:00" will be processed as November 5th, 2008 at 8:15am in the local offset of
        /// the browser.
        /// 
        /// If you are using ISO8601 dates with UTC offsets, chances are you want them processed. You must set this
        /// option to false. In the future, the default for this option will probably be changed to false.
        /// </summary>
        public bool? ignoreTimezone;

        /// <summary>
        /// A GET parameter of this name will be inserted into each JSON feed's URL.
        /// 
        /// The value of this GET parameter will be a UNIX timestamp denoting the start of the first visible day (inclusive).
        /// </summary>
        public string startParam;

        /// <summary>
        /// A GET parameter of this name will be inserted into each JSON feed's URL.
        /// 
        /// The value of this GET parameter will be a UNIX timestamp denoting the end of the last visible day (exclusive).
        /// </summary>
        public string endParam;

        /// <summary>
        /// Determines when event fetching should occur
        /// 
        /// When set to true (the default), the calendar will only fetch events when it absolutely needs to, minimizing AJAX
        /// calls. For example, say your calendar starts out in month view, in February. FullCalendar will fetch events for
        /// the entire month of February and store them in its internal cache. Then, say the user switches to week view and
        /// begins browsing the weeks in February. The calendar will avoid fetching events because it already has this
        /// information stored.
        /// 
        /// When set to false, the calendar will fetch events any time the view is switched, or any time the current date
        /// changes (for example, as a result of the user clicking prev/next).
        /// </summary>
        public bool? lazyFetching;



        /// <summary>
        /// Sets the background and border colors for all events on the calendar.
        /// 
        /// You can use any of the CSS color formats such #f00, #ff0000, rgb(255,0,0), or red.
        /// 
        /// This option can be overridden on a per-source basis with the color Event Source Object option or on a per-event
        /// basis with the color Event Object option.
        /// </summary>
        public string eventColor;

        /// <summary>
        /// Sets the background color for all events on the calendar.
        /// 
        /// You can use any of the CSS color formats such #f00, #ff0000, rgb(255,0,0), or red.
        /// 
        /// This option can be overridden on a per-source basis with the backgroundColor Event
        /// Source Object option or on a per-event basis with the backgroundColor Event Object option.
        /// </summary>
        public string eventBackgroundColor;

        /// <summary>
        /// Sets the border color for all events on the calendar.
        /// 
        /// You can use any of the CSS color formats such #f00, #ff0000, rgb(255,0,0), or red.
        /// 
        /// This option can be overridden on a per-source basis with the borderColor Event Source
        /// Object option or on a per-event basis with the borderColor Event Object option.
        /// </summary>
        public string eventBorderColor;

        /// <summary>
        /// Sets the text color for all events on the calendar.
        /// 
        /// You can use any of the CSS color formats such #f00, #ff0000, rgb(255,0,0), or red.
        /// 
        /// This option can be overridden on a per-source basis with the textColor Event Source
        /// Object option or on a per-event basis with the textColor Event Object option.
        /// </summary>
        public string eventTextColor;


        /// <summary>
        /// Determines whether the events on the calendar can be modified.
        /// 
        /// This determines if the events can be dragged and resized. Enables/disables both at the
        /// same time. If you don't want both, use editable in conjunction with disableDragging
        /// and disableResizing.
        /// 
        /// This option can be overridden on a per-event basis with the Event Object editable property.
        /// </summary>
        public bool? editable;

        /// <summary>
        /// Disables all event dragging, even when events are editable.
        /// </summary>
        public bool? disableDragging;

        /// <summary>
        /// Disables all event resizing, even when events are editable.
        /// </summary>
        public bool? disableResizing;

        /// <summary>
        /// Time it takes for an event to revert to its original position after an unsuccessful drag.
        /// 
        /// Time is in milliseconds (1 second = 1000 milliseconds).
        /// </summary>
        public int? dragRevertDuration;

        /// <summary>
        /// The opacity of an event while it is being dragged.
        /// 
        /// Float values range from 0.0 to 1.0.
        /// 
        /// Specify a single number to affect all views, or a View Option Hash to target specific views
        /// (which is what the default does).
        /// </summary>
        public float? dragOpacity;




        /// <summary>
        /// Determines if jQuery UI draggables can be dropped onto the calendar.
        /// </summary>
        public bool? droppable;

        /// <summary>
        /// Type: string/function
        /// 
        /// Provides a way to filter which elements can be dropped onto the calendar.
        /// 
        /// By default, after setting a calendar' droppable option to true, the calendar will accept any draggables
        /// that are dropped onto the calendar. The dropAccept option allows the calendar be more selective about
        /// which elements can/can't be dropped.
        /// 
        /// The value of dropAccept can be a string jQuery selector. It can also be a function that accepts the
        /// draggable item as a single argument, and returns true if the element can be dropped onto the calendar.
        /// </summary>
        public object dropAccept = "*";


        [JsonConverter(typeof(JsonStringLiteralConverter))]
        public string dayClick;

        [JsonConverter(typeof(JsonStringLiteralConverter))]
        public string eventMouseover;
        [JsonConverter(typeof(JsonStringLiteralConverter))]
        public string eventMouseout;


    }
}
