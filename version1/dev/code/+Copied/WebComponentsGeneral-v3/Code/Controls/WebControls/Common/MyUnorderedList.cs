﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System.Web.UI;
    using General_v3.Controls.WebControls;

    public class MyUnorderedList : BaseWebControl
    {


        public MyUnorderedList():base( HtmlTextWriterTag.Ul)
        {
            
        }

        public void AddListItem(string text, string cssClass)
        {
            MyListItem li = new MyListItem();
            li.Text = text;
            li.WebControlFunctionality.CssManager.AddClass(cssClass);
            this.Controls.Add(li);
        }

    }
}
