﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorEmail : ValidatorBase
    {

        public new ValidatorEmailParameters Parameters { get { return (ValidatorEmailParameters)base.Parameters; } set { base.Parameters = value; } }

        protected override ValidatorBaseParameters createParameters()
        {
            return new ValidatorEmailParameters();
        }
        public override object GenerateRandomValidValue()
        {
            string email = "";
            email += CS.General_v3.Util.Random.GetString(5, 20);
            email += "@";
            email += "mailinator.com";
            return email;
        }
    }
}