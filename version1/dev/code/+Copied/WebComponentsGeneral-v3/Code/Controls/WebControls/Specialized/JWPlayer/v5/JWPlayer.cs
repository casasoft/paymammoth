﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.JWPlayer.v5
{

    public class JWPlayer : CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.SWFObject.SWFObject {

        public enum JWPLAYER_CONTROL_BAR_POSITION {
            Bottom,
            Over,
            None
        }
        public enum JWPLAYER_REPEAT
        {
           None, List, Always, Single
        }



        public bool AutoStart { get { return this.Functionality.FlashVars.GetProperty<bool>("autostart") == true; } set { this.Functionality.FlashVars.AddProperty("autostart", value); } }

        private JWPLAYER_CONTROL_BAR_POSITION _controlBar;
        public JWPLAYER_CONTROL_BAR_POSITION ControlBar
        {
            get { return _controlBar; }
            set
            {
                _controlBar = value;
                this.Functionality.FlashVars.AddProperty("controlbar", value.ToString().ToLower());
            }
        }

        private JWPLAYER_REPEAT _repeat;
        public JWPLAYER_REPEAT Repeat
        {
            get { return _repeat; }
            set
            {
                _repeat = value;
                this.Functionality.FlashVars.AddProperty("repeat", value.ToString().ToLower());
            }
        }
        public JWPlayer(string videoFileURL, string jwPlayerPath = "/_common/static/js/flash/jwplayer/v5.4/player.swf")
            : base()
        {
            this.Functionality.FlashUrl = jwPlayerPath;
            this.Functionality.AllowFullscreen = true;
            this.Functionality.AllowScriptAccess = ALLOW_SCRIPT_ACCESS.Always;
            this.Functionality.FlashVars.AddProperty("file", videoFileURL);

        }

      


 
    }
}
