namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using General_v3;
    using General_v3.Controls.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

    public class MyTxtBoxDate : MyInputBaseWithType<DateTime?>
    {




        public MyTxtBoxDate(FieldSingleControlDateParameters fieldParameters = null)
            : base(INPUT_TYPE.Date, fieldParameters)
        {

            this.JSFieldTypeName = "FieldSingleControlDate";
        }

        public string DateFormat { get { return this.WebControlFunctionality.GetOrCreateValidator<ValidatorDate>().Parameters.dateFormat; } set { this.WebControlFunctionality.GetOrCreateValidator<ValidatorDate>().Parameters.dateFormat = value; } }

        public DateTime GetFormValueAsDate()
        {
            var dt = this.GetFormValueAsDateNullable();
            if (dt.HasValue)
            {
                return dt.Value;
            }
            else {
                throw new InvalidCastException("Date '"+this.GetFormValueAsString()+"' must follow the date format '"+this.DateFormat+"'");
            }
        }
        protected override BaseFieldParameters createFieldParameters()
        {
            return new FieldSingleControlDateParameters();
        }
        protected override void updateInitialValue(object value)
        {

            if (value is DateTime)
            {

                var dtValidator = this.WebControlFunctionality.GetOrCreateValidator<ValidatorDate>();

                DateTime dt = (DateTime)value;

                string strDateValue = dt.ToString(DateFormat);
                base.updateInitialValue(strDateValue);
            }
            else
            {
                base.updateInitialValue(value);
            }
        }

        public DateTime? GetFormValueAsDateNullable()
        {
            var date = CS.General_v3.Util.Date.ParseDate(this.GetFormValueAsString(), this.DateFormat);
            return date;
        }
        public override object GetFormValueObject()
        {
            return GetFormValueAsDateNullable();
        }
        protected override void OnLoad(EventArgs e)
        {
            this.WebControlFunctionality.GetOrCreateValidator<ValidatorDate>();// just in case

            base.OnLoad(e);
        }

        public new FieldSingleControlDateParameters FieldParameters
        {
            get
            {
                return (FieldSingleControlDateParameters)base.FieldParameters;
            }
            set { base.FieldParameters = value; }
        }




        public override DateTime? InitialValue
        {
            get
            {
                if (this.WebControlFunctionality.InitialValue != null)
                {
                    return CS.General_v3.Util.Other.ConvertObjectToBasicDataType<DateTime>(this.WebControlFunctionality.InitialValue);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.WebControlFunctionality.InitialValue = value;
            }
        }

        public string InitialValueAsString
        {
            get
            {
                return (string)(this.WebControlFunctionality.InitialValue);
            }
            set
            {
                this.WebControlFunctionality.InitialValue = value;
            }
        }

        public override object GetInitialTemporaryRandomValue()
        {
            var validator = this.WebControlFunctionality.GetOrCreateValidator<ValidatorDate>();
            if (validator != null)
            {
                string value = validator.GenerateRandomValidValueAsString();
                return value.ToString();
            }
            else
            {
                return null;
            }
        }
    }
}
