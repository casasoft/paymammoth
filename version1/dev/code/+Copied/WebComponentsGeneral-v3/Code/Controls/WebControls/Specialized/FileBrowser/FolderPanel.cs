﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructureBasic;
namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.FileBrowser
{
    public class FolderPanel : MyTableCell
    {
        public class FUNCTIONALITY
        {
            private TreeStructureBasicClass _tree = null;
            private FolderPanel _FolderPanel = null;
            public List<ITreeItemBasic> RootFolders { get; set; }
            public FUNCTIONALITY(FolderPanel FolderPanel)
            {
                _FolderPanel = FolderPanel;
                _tree = new TreeStructureBasicClass();
                this.RootFolders = new List<ITreeItemBasic>();
            }
            public void Render()
            {
                _FolderPanel.Controls.Add(_tree);
                _tree.Functionality.ShowPriorityAfterName = false;
                _tree.Functionality.RootItems = this.RootFolders;
                _tree.Functionality.RenderTree();
            }

        }
        public FUNCTIONALITY Functionality { get; set; }
        public FolderPanel()
        {
            this.Functionality = new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.Render();
            base.OnLoad(e);
        }
    }
}
