﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    public class MyListBox : MyDropDownList
    {
        private List<string> _formValues = null;

        #region MyListBox Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY : MyDropDownList.FUNCTIONALITY
        {
            protected new MyListBox _item { get { return (MyListBox)base._item; } }

            internal FUNCTIONALITY(MyListBox item)
                : base(item)
            {

            }

        }

        protected override MyDropDownList.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        #endregion

        public MyListBox(FieldSingleControlListBoxParameters fieldParameters = null)
            : base(fieldParameters)
        {
            this.JSFieldTypeName = "FieldSingleControlListBox";
            this.Multiple = true;
            //this.DoNotAddFieldJS = true;
        }

        public bool Multiple { get { return getAttributeAsBool("multiple"); } set { setAttributeFromBool("multiple", value); } }
        public uint? Size { get { return (uint?)getAttributeAsNumeric("size"); } set { setAttributeFromNumeric("size", value); } }


        new public FieldSingleControlListBoxParameters FieldParameters
        {
            get
            {
                return (FieldSingleControlListBoxParameters)base.FieldParameters;
            }
            set { base.FieldParameters = value; }
        }

        protected override BaseFieldParameters createFieldParameters()
        {
            return new FieldSingleControlListBoxParameters();
        }

        /// <summary>
        /// This must return a list
        /// </summary>
        /// <returns></returns>
        public override object GetFormValueObject()
        {
            if (_formValues == null)
            {
                var val = base.GetFormValueObject();

                if (val != null)
                {
                    string sVal = val.ToString();
                    _formValues = CS.General_v3.Util.Text.Split(sVal, ",");
                }
                else
                {
                    _formValues = new List<string>();
                }

            }
            return _formValues;
        }
        public List<string> GetFormValueAsListOfString()
        {
            return (List<string>)GetFormValueObject();
        }
        public List<TEnum> GetFormValueAsListOfEnum<TEnum>() where TEnum : struct, IConvertible
        {
            return CS.General_v3.Util.EnumUtils.GetListOfEnumsFromListOfStringValues<TEnum>(GetFormValueAsListOfString());
        }

    }
}