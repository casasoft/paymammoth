﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields
{
    using Util;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

    public class FormFieldsLayoutTable : IFormFieldsLayout
    {
        /// <summary>
        /// Used for JS to have correct IDs of table cells
        /// </summary>
        public class FormFieldsIconJSHelper
        {
            public IFormFieldsItemData Field { get; set; }
            public MyTableCell TdValidationIcon { get; set; }
            public MyTableCell TdHelpMessageIcon { get; set; }
        }

        public enum FORM_FIELDS_LAYOUT_TABLE_COLUMNS
        {
            Label,
            Field,
            HelpIcon,
            ValidationIcon
        }
        public string FieldSuffix { get; set; }
        public string RequiredCharacter { get; set; }
        public List<FORM_FIELDS_LAYOUT_TABLE_COLUMNS> ColumnsOrder { get; set; }
        private List<FormFieldsIconJSHelper> _itemsAndCells = new List<FormFieldsIconJSHelper>();

        /*
        public class FormFieldsLayoutTableIconsParametersJS : JSONObject
        {
            public List<string> tableIDs = new List<string>();
            public List<string> fieldIDs = new List<string>();
            public bool showHelpMessageIconAfterField = true;
            public bool showValidationIcons = true;
            public bool showHelpMessageIcons = true;

            
            

            public static void InitJS(FormFieldsLayoutTableIconsParametersJS parameters)
            {
                string js = "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.Forms.Icons.TableLayout.FormFieldsLayoutTableIcons(" + parameters .GetJSON()+ ");\r\n";
                JSUtil.AddJSScriptToPage(js, 1000);
            }
        }
        */
        public FormFieldsLayoutTable()
        {
           // this.ParametersJS = new FormFieldsLayoutTableIconsParametersJS();
            this.FieldSuffix = ":";
            this.RequiredCharacter = " *";
            this.ColumnsOrder = new List<FORM_FIELDS_LAYOUT_TABLE_COLUMNS>() { 
                FORM_FIELDS_LAYOUT_TABLE_COLUMNS.Label, 
                FORM_FIELDS_LAYOUT_TABLE_COLUMNS.Field, 
                FORM_FIELDS_LAYOUT_TABLE_COLUMNS.HelpIcon, 
                FORM_FIELDS_LAYOUT_TABLE_COLUMNS.ValidationIcon 
            };
        }


       // public FormFieldsLayoutTableIconsParametersJS ParametersJS { get; set; }

        private void initControlIcons(IFormFieldsItemData item, MyTableCell tdValidationIcon, MyTableCell tdHelpIcon)
        {
            BaseFieldParameters fieldParameters = item.FieldParameters;
            if (fieldParameters == null)
            {

                IMyFormWebControl formControl = null;

                if (item.Control is IMyFormWebControl)
                {
                    formControl = (IMyFormWebControl) item.Control;
                }
                else if (item is IMyFormWebControl)
                {
                    formControl = (IMyFormWebControl) item;
                }
                if (formControl != null)
                {
                    fieldParameters = formControl.FieldParameters;
                }
            }
            if (fieldParameters != null)
            {
                if (tdValidationIcon != null && !fieldParameters.doNotValidateOnBlur)
                {
                    fieldParameters.validationIconContainerID = tdValidationIcon.ClientID;
                }
                if (tdHelpIcon != null && (!string.IsNullOrEmpty(fieldParameters.helpMessage) || fieldParameters.helpMessageContentText != null))
                {
                    fieldParameters.helpMessageIconContainerID = tdHelpIcon.ClientID;
                }
            }
        }
        
        private MyDiv createGroup(FormFieldsItemGroupData group, FormFields formFields)
        {
            MyDiv divGroup = new MyDiv();
            divGroup.CssManager.AddClass("form-fields-group-container");
            if (group.CssManager != null)
            {
                divGroup.CssManager.AddClass(group.CssManager.ToString());
            }
            if(group.TitleContentText != null)
            {
                MyDiv divHeading = new MyDiv();
                divHeading.CssClass = "form-fields-group-title clearfix";
                MyHeading h2 = new MyHeading(2, null);
                MyContentText.AttachContentTextWithControl(h2, group.TitleContentText);
                divHeading.Controls.Add(h2);
                divGroup.Controls.Add(divHeading);
            }
            else if (!string.IsNullOrEmpty(group.Title))
            {
                divGroup.Controls.Add(new MyHeading(2, group.Title));
            }

            if(group.DescriptionContentText != null)
            {
                MyDiv divDescriptionGroup = new MyDiv();
                MyContentText.AttachContentTextWithControl(divDescriptionGroup, group.DescriptionContentText, group.DescriptionReplacementTags);
                divGroup.Controls.Add(divDescriptionGroup);
            }
            else if (!string.IsNullOrEmpty(group.DescriptionHtml))
            {
                divGroup.Controls.Add(new Literal() { Text = group.DescriptionHtml });
            }

            if (group.ChildItems != null && group.ChildItems.Count> 0)
            {
                MyDiv divContentWrapper = new MyDiv();
                divContentWrapper.CssClass = "form-fields-group-content";
                MyTable tblForm = new MyTable();

                tblForm.ID = "formFieldsGroup_" + formFields.ClientID + "_" + CS.General_v3.Util.Text.NormalizeControlID(group.Identifier);
                tblForm.CssManager.AddClass("form-fields-table");
                divContentWrapper.Controls.Add(tblForm);
                divGroup.Controls.Add(divContentWrapper);
                int count = 0;
                foreach (IFormFieldsItemData fieldItem in group.ChildItems)
                {
                    var row = tblForm.AddRow();
                    MyTableCell tdLabel = new MyTableCell();
                    MyTableCell tdControl = new MyTableCell();
                    MyTableCell tdValidationIcon = new MyTableCell();
                    MyTableCell tdHelpIcon = new MyTableCell();
                    tdValidationIcon.ID = tblForm.ID + "_validationIcon" + count;
                    tdHelpIcon.ID = tblForm.ID + "helpIcon" + count;
                    tdLabel.WebControlFunctionality.CssManager.AddClass("form-fields-item-label");
                    tdControl.WebControlFunctionality.CssManager.AddClass("form-fields-item-control");

                    tdValidationIcon.WebControlFunctionality.CssManager.AddClass("form-fields-item-validation-icon");
                    tdHelpIcon.WebControlFunctionality.CssManager.AddClass("form-fields-item-help-message");
                    
                    //Add them in order required
                    for (int i = 0; i < this.ColumnsOrder.Count; i++)
                    {
                        switch (this.ColumnsOrder[i])
                        {
                            case FORM_FIELDS_LAYOUT_TABLE_COLUMNS.Field: row.Controls.Add(tdControl); break;
                            case FORM_FIELDS_LAYOUT_TABLE_COLUMNS.Label: row.Controls.Add(tdLabel); break;
                            case FORM_FIELDS_LAYOUT_TABLE_COLUMNS.HelpIcon: if (!DoNotShowHelpMessageIcons) row.Controls.Add(tdHelpIcon); break;
                            case FORM_FIELDS_LAYOUT_TABLE_COLUMNS.ValidationIcon: if (!DoNotShowValidationIcons) row.Controls.Add(tdValidationIcon); break;
                        }
                    }

                    //Label
                    MyLabel lblTitle = new MyLabel();
                    
                    if ((!string.IsNullOrEmpty(fieldItem.Title) || fieldItem.TitleContentText != null)   && (fieldItem.LayoutInfo == null || !fieldItem.LayoutInfo.DoNotShowLabel))
                    {
                        MySpan spanLabel = new MySpan();
                        spanLabel.CssManager.AddClass("form-label-text");
                        if (fieldItem.TitleContentText != null)
                        {
                            MyContentText.AttachContentTextWithControl(spanLabel, fieldItem.TitleContentText);
                        }
                        else
                        {
                            spanLabel.InnerText = fieldItem.Title;
                        }
                        lblTitle.Controls.Add(spanLabel);


                        if (!string.IsNullOrEmpty(FieldSuffix))
                        {
                            MySpan spanLabelSuffix = new MySpan();
                            spanLabelSuffix.CssManager.AddClass("form-label-suffix");
                            spanLabelSuffix.InnerHtml = FieldSuffix;
                            lblTitle.Controls.Add(spanLabelSuffix);
                        }
                        if (fieldItem.Required && !string.IsNullOrEmpty(RequiredCharacter))
                        {
                            MySpan spanRequired = new MySpan();
                            spanRequired.CssManager.AddClass("form-label-required-char");
                            spanRequired.InnerHtml = RequiredCharacter;
                            lblTitle.Controls.Add(spanRequired);
                        }
                    }
                    
                    
                    tdLabel.Controls.Add(lblTitle);
                    lblTitle.ForControl = fieldItem.Control;

                    if (!string.IsNullOrEmpty(fieldItem.CssClass))
                    {
                        row.CssManager.AddClass(fieldItem.CssClass);
                    }


                    if (!string.IsNullOrEmpty(fieldItem.SubTitle) || fieldItem.SubTitleContentText != null)
                    {
                        MyDiv divSubtitle = new MyDiv();
                        divSubtitle.CssManager.AddClass("form-fields-item-subtitle");
                        if (fieldItem.SubTitleContentText != null)
                        {
                            MyContentText.AttachContentTextWithControl(divSubtitle, fieldItem.SubTitleContentText);
                        }
                        else
                        {
                            divSubtitle.InnerHtml = fieldItem.SubTitle;

                        }
                        tdLabel.Controls.Add(divSubtitle);
                    }

                    //Control
                    tdControl.Controls.Add(fieldItem.Control);

                    _itemsAndCells.Add(new FormFieldsIconJSHelper()
                    {
                        Field = fieldItem,
                        TdValidationIcon = tdValidationIcon,
                        TdHelpMessageIcon = tdHelpIcon
                    });

                    //BLOCK
                    if (fieldItem.LayoutInfo != null && fieldItem.LayoutInfo.BlockElement)
                    {
                        //This is a block element so remove all the other
                        tdControl.ColumnSpan = 4;
                        if (tdHelpIcon.Parent != null) tdHelpIcon.Parent.Controls.Remove(tdHelpIcon);
                        if (tdValidationIcon.Parent != null) tdValidationIcon.Parent.Controls.Remove(tdValidationIcon);
                        if (tdLabel.Parent != null) tdLabel.Parent.Controls.Remove(tdLabel);
                    }
                    count++;
                }
            }

            if (!string.IsNullOrEmpty(group.FooterHtml))
            {
                divGroup.Controls.Add(new Literal() { Text = group.FooterHtml });
            }
            return divGroup;
        }

        private MyDiv createControls(FormFields formFields)
        {
            MyDiv divContainer = new MyDiv();
            divContainer.CssManager.AddClass("form-fields-core-container");
            if (formFields.Functionality.CssManager != null)
            {
                divContainer.CssManager.AddClass(formFields.Functionality.CssManager.ToString());
            }
            foreach(var kvp in formFields.Functionality.ItemGroups)
            {
                var groupData = kvp.Value;
                var divGroup = createGroup(groupData, formFields);
                if (divGroup != null)
                {
                    divContainer.Controls.Add(divGroup);
                }
            }
            return divContainer;
        }

        private void initJS()
        {
            //FormFieldsLayoutTableIconsParametersJS.InitJS(ParametersJS);
            foreach (var item in _itemsAndCells)
            {
                initControlIcons(item.Field, item.TdValidationIcon, item.TdHelpMessageIcon);

            }
        }

        public System.Web.UI.Control Render(FormFields formFields)
        {
            var control = createControls(formFields);
            control.PreRender += new EventHandler(control_PreRender);
            return control;
            
        }

        /// <summary>
        /// To make sure they are added to the UI and has got correct IDs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void control_PreRender(object sender, EventArgs e)
        {
            initJS();
        }


        public bool DoNotShowValidationIcons { get; set; }

        public bool DoNotShowHelpMessageIcons { get; set; }
    }
}