﻿using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    internal class BaseNumericalReportSection : BaseReportSectionBase
    {





        #region BaseNumericalReportSection Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public new class FUNCTIONALITY : BaseReportSectionBase.FUNCTIONALITY
        {
            public new IBaseNumericalReportSectionData Section { get { return (IBaseNumericalReportSectionData)base.Section; } set { base.Section = value; } }
            protected new BaseNumericalReportSection _item { get { return (BaseNumericalReportSection)base._item; } }

            internal FUNCTIONALITY(BaseNumericalReportSection item)
                : base(item)
            {

            }

        }
        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }

        protected override BaseReportSectionBase.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        #endregion
		
			
			

        public BaseNumericalReportSection()
        {
            
        }
      
        protected override BaseReportSectionTableBase getTableData()
        {
            return new BaseNumericalReportSectionTable();
        }
    }
}
