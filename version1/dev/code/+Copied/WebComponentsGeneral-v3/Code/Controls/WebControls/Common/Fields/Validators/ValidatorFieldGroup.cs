﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    using System.Web.UI;

    public class ValidatorFieldGroup : ValidatorBase
    {
        private bool _updatedFieldGroupItemIDs = false;
        private IMyFormWebControl[] _elements;

        public ValidatorFieldGroup()
        {

        }
        public ValidatorFieldGroup(FIELD_SUBGROUP_TYPE type, params string[] elementIDs)
        {
            this.Parameters.groupType = type;
            this.Parameters.groupElementsIDs = elementIDs.ToList();
        }
        public ValidatorFieldGroup(FIELD_SUBGROUP_TYPE type, params IMyFormWebControl[] elements)
        {
            this.Parameters.groupType = type;
            _elements = elements;

            for (int i = 0; i < elements.Length; i++)
            {
                if (elements[i] == null) throw new ArgumentNullException("Element cannot be null.  Please make sure to instantiate it before");
                if (!elements[i].FieldParameters.validators.Contains(this))
                {
                    elements[i].FieldParameters.validators.Add(this);
                }

               
            }

        }

        public override void UpdateExtraParameters()
        {
            if (!_updatedFieldGroupItemIDs && _elements != null)
            {
                _updatedFieldGroupItemIDs = true;

                for (int i = 0; i < _elements.Length; i++)
                {
                    if (_elements[i] != null)
                    {
                        this.Parameters.groupElementsIDs.Add(_elements[i].Control.ClientID);
                    }
                }
                
            }
            base.UpdateExtraParameters();
        }
       /* void Control_PreRender(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            this.Parameters.groupElementsIDs.Add(c.ClientID);
        }
        */
        
        public new ValidatorFieldGroupParameters Parameters { get { return (ValidatorFieldGroupParameters)base.Parameters; } set { base.Parameters = value; } }

        protected override ValidatorBaseParameters createParameters()
        {
            return new ValidatorFieldGroupParameters();
        }

        public override object GenerateRandomValidValue()
        {
            string validValue = null;
            if (this._elements != null)
            {
                for (int i = 0; i < _elements.Length; i++)
                {
                   
                    if (_elements[i].WebControlFunctionality.InitialValue != null)
                    {
                         validValue = (string) _elements[i].WebControlFunctionality.InitialValue;
                         break;
                    }

                } 
                for (int i = 0; i < _elements.Length; i++)
                {

                    _elements[i].WebControlFunctionality.InitialValue = validValue;
                }
            }
            return validValue;
        }

        
    }
}