﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public abstract class MyInnerHtmlControl : BaseWebControl
    {
        public MyInnerHtmlControl(string tagName):base(tagName)
        {

        }

        public MyInnerHtmlControl(HtmlTextWriterTag tagName)
            : base(tagName)
        {

        }

        public string InnerHtml { get { return this.WebControlFunctionality.InnerHtml; } set { this.WebControlFunctionality.InnerHtml = value; } }
        public string InnerText { get { return this.WebControlFunctionality.InnerText; } set { this.WebControlFunctionality.InnerText = value; } }



    }
}
