﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorAJAXParameters : ValidatorBaseParameters
    {
        public string ajaxValidationURL = null;
        public List<string> omitValues = null;
        public bool caseSensitive = false;
        public bool cacheResults = false;
        public string ajaxParameterName = "data";
       
        
    }
}