﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace CS.General_v3.Controls.WebControls.Common
{
    
    [ToolboxData("<{0}:CombinerCtrl runat=server></{0}:CombinerCtrl>")]
    public class CombinerCtrl : PlaceHolder
    {
        public class FUNCTIONALITY
        {
            private CombinerCtrl _ctrl = null;
            public FUNCTIONALITY(CombinerCtrl ctrl)
            {
                this._ctrl = ctrl;
                //this.CombineControls = true;
                

                this.RemoveDebugFilesExtension = CS.General_v3.Settings.Others.IsInLocalTesting ? false : true; //dont remove .debug on localhost
                
                this.CombinerPath = "/combiner.ashx";
                this.CombinerParam = "get";
                this.Version = 1;
                this.ExtraJSFiles = new List<string>();
                this.ExtraCSSFiles = new List<string>();
            }

            public bool RemoveDebugFilesExtension { get; set; }
            //public bool CombineControls { get; set; }
            public string CombinerPath { get; set; }
            public string CombinerParam { get; set; }
            public List<string> ExtraJSFiles { get; private set; }
            public List<string> ExtraCSSFiles { get; private set; }
            public int Version { get; set; }
            
        }
        
        
        public bool? CombineControls { get; set; }
        public bool DeferLoadingThroughJQuery { get; set; }
        public bool DontCombineIfLocalhost { get; set; }
        public bool LoadMinifiedJQueryFromGoogleCDN { get; set; }
        public bool RemoveDebugFilesExtension { get { return this.Functionality.RemoveDebugFilesExtension; } set { this.Functionality.RemoveDebugFilesExtension = value; } }


        public bool NeverCombine
        {
            get { return CombineControls.HasValue ? !CombineControls.Value : false; }
            set { CombineControls = !NeverCombine; }
        }

        public bool AlwaysCombine
        {
            get { return CombineControls.HasValue ? CombineControls.Value : false; }
            set { CombineControls = AlwaysCombine; }
        }

            private bool getDefaultCombineValue()
        {
            

            bool combine  = true;
            if (CS.General_v3.Util.Other.IsLocalTestingMachine && DontCombineIfLocalhost)
            {
                combine = false;
            }
            return combine;
        }

        private const string TYPE_JAVASCRIPT = "text/javascript";
        private const string TYPE_CSS = "text/css";
        private const string REL_CSS = "stylesheet";
        private const string ATTRIBUTE_TYPE = "type";
        private const string ATTRIBUTE_REL = "rel";
        private const string ATTRIBUTE_SRC = "src";
        private const string ATTRIBUTE_HREF = "href";

        public FUNCTIONALITY Functionality { get; private set; }
        public int Version { get { return this.Functionality.Version; } set { this.Functionality.Version = value; } }
        public string CombinerPath { get { return this.Functionality.CombinerPath; } set { this.Functionality.CombinerPath = value; } }


        public bool? CombinePaths { get; set; }



        public CombinerCtrl()
        {
            this.Functionality = new FUNCTIONALITY(this);
            this.LoadMinifiedJQueryFromGoogleCDN = !CS.General_v3.Util.Other.IsLocalTestingMachine;
            this.DontCombineIfLocalhost = true;
            this.DeferLoadingThroughJQuery = false;
        }
        private WebControl checkIfScriptTag(Control c)
        {
            WebControl result = null;
            if (c is WebControl)
            {
                result  = (WebControl)c;
                if (string.Compare(result.Attributes[ATTRIBUTE_TYPE], TYPE_JAVASCRIPT, 0) == 0)
                {
                    
                }
                else
                {
                    result = null;
                }
            }
            return result;
        }
        private WebControl checkIfCssTag(Control c)
        {
            WebControl result = null;
            if (c is WebControl)
            {
                result = (WebControl)c;
                if (string.Compare(result.Attributes[ATTRIBUTE_TYPE], TYPE_CSS, 0) == 0 &&
                    string.Compare(result.Attributes[ATTRIBUTE_REL], REL_CSS, 0) == 0)
                {

                }
                else
                {
                    result = null; 
                }
            }
            return result;
        }
        private string convertListOfSourceFilesToCombinerStr(List<string> list)
        {
            //list.Sort();
            StringBuilder sb = new StringBuilder();
            string lastPath = null;
            for (int i = 0; i < list.Count; i++)
            {
                var src = list[i];
                string basePath = CS.General_v3.Util.IO.GetDirName(src);
                string name = CS.General_v3.Util.IO.GetFilenameAndExtension(src);

                //if (false && string.Compare(lastPath, basePath, true) == 0)
                //if (false)
                //{
                //    sb.Append("|");
                //    sb.Append(name);

                //}
                //else
                //{
                    if (sb.Length != 0)
                        sb.Append(",");
                    sb.Append(src);
                //}
                lastPath = basePath;
            }
            return sb.ToString();
        }
        private void renderCombinerCssTag()
        {
            string src = null;
            string combinerSrc = convertListOfSourceFilesToCombinerStr(_cssTags);
            if (!string.IsNullOrWhiteSpace(combinerSrc))
            {
                src = this.Functionality.CombinerPath + "?" + this.Functionality.CombinerParam + "=" + convertListOfSourceFilesToCombinerStr(_cssTags) + "&version=" + this.Functionality.Version;
            }
           
            if (!string.IsNullOrWhiteSpace(src))
            {
                renderCssTag(src);
            }
        

        }

        private List<string> _scriptTags = null;
        private List<string> _deferrableScriptTags = null;
        private List<string> _cssTags = null;

        private void parseStringForTags(string s )
        {
            Regex removeComments = new Regex("<!--.*?-->");
            s = removeComments.Replace(s, "");

            //Regex scriptRegExp = new Regex(@"\<script.+?src=(""|')(.*?)(""|').*?/?>", RegexOptions.IgnoreCase);
            {
                Regex scriptRegExp = new Regex(@"\<script.+?" + ATTRIBUTE_SRC + @"=[""']?(.+?)[""' ].*?/?>", RegexOptions.IgnoreCase | RegexOptions.Singleline);
                MatchCollection scriptMatches = scriptRegExp.Matches(s);
                
                foreach (Match match in scriptMatches)
                {
                    if (Regex.IsMatch(match.Value, ATTRIBUTE_TYPE + @"=(""|')?" + TYPE_JAVASCRIPT + @"(""|')", RegexOptions.IgnoreCase | RegexOptions.Singleline))
                    {
                        _scriptTags.Add(match.Groups[1].Value);
                    }
                }
            }
            {
                Regex cssRegExp = new Regex(@"\<link.+?" + ATTRIBUTE_HREF + @"=[""']?(.+?)[""' ].*?/?>", RegexOptions.IgnoreCase | RegexOptions.Singleline);
                MatchCollection cssMatches = cssRegExp.Matches(s);

                foreach (Match match in cssMatches)
                {
                    if (Regex.IsMatch(match.Value, ATTRIBUTE_TYPE + @"=(""|')?" + TYPE_CSS + @"(""|')", RegexOptions.IgnoreCase | RegexOptions.Singleline) &&
                        Regex.IsMatch(match.Value, ATTRIBUTE_REL + @"=(""|')?" + REL_CSS + @"(""|')", RegexOptions.IgnoreCase | RegexOptions.Singleline))
                    {
                            
                        _cssTags.Add(match.Groups[1].Value);
                    }
                }
            }
            // Regex cssRegExp = new Regex(@"\<script.+?src=(""|')(.*?)(""|').*?/?>", RegexOptions.IgnoreCase);


        }
        private string removeScriptSharpDebug(string s)
        {
            const string scriptSharpDebugExtension = ".debug.js";
            if (s.Contains(scriptSharpDebugExtension))
            {
                s = s.Replace(scriptSharpDebugExtension, ".js");
            }
            return s;
        }
        private bool isFromGoogleCDN(string url)
        {
            return url.Contains("ajax.googleapis.com");
        }
        private bool isFromGoogleJQuery(string url)
        {
            return isFromGoogleCDN(url) && url.Contains("/jquery/");
        }
        private bool canDeferScript(string url)
        {
            //Defer all except the jQuery one or cufon
            //JQuery needed to defer
            //Cufon gives error in IE
            if (!isFromGoogleJQuery(url) && !url.Contains("/cufon/"))
            {
                return true;
            }
            return false;

        }

        private string loadMinifiedGoogleScript(string s)
        {
            if (isFromGoogleCDN(s) && !s.Contains(".min.js"))
            {
                s = s.Replace(".js", ".min.js");
            }
            return s;
        }

        private void checkTags()
        {
            _scriptTags = new List<string>();
            _cssTags = new List<string>();
            for (int i = 0; i < this.Controls.Count; i++)
            {
                
                var c = this.Controls[i];
                if (c is System.Web.UI.LiteralControl)
                {
                    LiteralControl lc = (LiteralControl)c;
                    parseStringForTags(lc.Text);
                }
                else
                {
                    WebControl scriptTag = checkIfScriptTag(c);
                    if (scriptTag != null)
                    {
                        string webCtrlSrc = scriptTag.Attributes[ATTRIBUTE_SRC];
                        if (!string.IsNullOrWhiteSpace(webCtrlSrc))
                        {

                            webCtrlSrc = webCtrlSrc.ToLower();
                            
                            _scriptTags.Add(webCtrlSrc);
                        }
                    }
                }
                {
                    WebControl cssTag = checkIfCssTag(c);
                    if (cssTag != null)
                    {
                        string webCtrlSrc = cssTag.Attributes[ATTRIBUTE_HREF];
                        if (!string.IsNullOrWhiteSpace(webCtrlSrc))
                        {

                            webCtrlSrc = webCtrlSrc.ToLower();
                            _cssTags.Add(webCtrlSrc);
                        }
                    }
                }
                
            }
        }

        private void renderScriptTag(string src, bool canDeferLoadingViaJQuery)
        {
            
            Literal lit = new Literal();
            string js = "<script " + ATTRIBUTE_TYPE + "=\"" + TYPE_JAVASCRIPT + "\" " + ATTRIBUTE_SRC + "=\"" + src + "\" ";
            if (canDeferLoadingViaJQuery && this.DeferLoadingThroughJQuery)
            {
                //Self close it as this is how it works for jQuery
                js += "/>";
                js = "<script type=\"text/javascript\"> jQuery(function() {    jQuery('body').append(jQuery('" + js + "')); }); </script>";
            }
            else
            {
                //do not self close it
                js += "></script>";
            }
            js += "\r\n\r\n";
            lit.Text = js;
            this.Controls.Add(lit);
            
        }
        private void renderCssTags(List<string> files)
        {
            foreach (var file in files)
            {
                renderCssTag(file);
            }
        }
        private void renderCssTag(string src)
        {
            Literal lit = new Literal();
            lit.Text = "<link " + ATTRIBUTE_TYPE + "=\"" + TYPE_CSS + "\" " + ATTRIBUTE_HREF + "=\"" + src + "\" " + ATTRIBUTE_REL +"=\"" + REL_CSS + "\" ></link>\r\n";

            this.Controls.Add(lit);
        }
        private void renderNonCombinerScriptTags()
        {
            for (int i = 0; i < _scriptTags.Count; i++)
            {
                var scriptSrc = _scriptTags[i].ToLower();
                bool isExternalLink = scriptSrc.StartsWith("http://") || scriptSrc.StartsWith("https://");
                if (!this.CombineControls.Value || isExternalLink)
                {
                    renderScriptTag(scriptSrc, !isExternalLink);
                    _scriptTags.RemoveAt(i);
                    i--;
                }
                
            }
        }
        private void renderNonCombinerCssTags()
        {
            for (int i = 0; i < _cssTags.Count; i++)
            {
                var src = _cssTags[i].ToLower();
                if (!this.CombineControls.Value || src.StartsWith("http://") || src.StartsWith("https://"))
                {
                    renderCssTag(src);
                    _cssTags.RemoveAt(i);
                    i--;
                }

            }
        }
        private void renderCombinerScriptTag()
        {
           // checkTags();
            //_cssTags.AddRange(this.Functionality.ExtraCSSFiles);

            string src = null;
            {

                string combinerSrc = convertListOfSourceFilesToCombinerStr(_scriptTags);
                if (this.CombineControls.Value && !string.IsNullOrWhiteSpace(combinerSrc))
                {
                    src = this.Functionality.CombinerPath + "?" + this.Functionality.CombinerParam + "=" + convertListOfSourceFilesToCombinerStr(_scriptTags) + "&version=" + this.Functionality.Version;
                }
            }
            {
               if (!string.IsNullOrWhiteSpace(src))
               {
                   renderScriptTag(src, true);
               }
            }
            

        }
        /// <summary>
        /// Update script tag values
        /// </summary>
        private void parseScriptTags()
        {
            if (this.Functionality.RemoveDebugFilesExtension || this.LoadMinifiedJQueryFromGoogleCDN)
            {
                for (int i = 0; i < _scriptTags.Count; i++)
                {
                    //var s = _scriptTags[i];
                    if (this.Functionality.RemoveDebugFilesExtension)
                    {
                        _scriptTags[i] = removeScriptSharpDebug(_scriptTags[i]);
                    }
                    if (this.LoadMinifiedJQueryFromGoogleCDN)
                    {
                        _scriptTags[i] = loadMinifiedGoogleScript(_scriptTags[i]);
                    }
                }
            }

        }
        private void checkDeferrableScriptTags()
        {
            _deferrableScriptTags = new List<string>();
            for (int i = 0; i < _scriptTags.Count; i++)
            {
                if (canDeferScript(_scriptTags[i]))
                {
                    _deferrableScriptTags.Add(_scriptTags[i]);
                    _scriptTags.RemoveAt(i);
                    i--;
                }
            }
        }

        private string getCombinedFilesHandlerPath(List<string> files)
        {
            string handler = this.Functionality.CombinerPath + "?" + this.Functionality.CombinerParam + "=" + convertListOfSourceFilesToCombinerStr(files);
            if (files.Count == 1)
            {
                //Else the ? is not yet added
                handler += "?";
            }
            handler += "&version=" + this.Functionality.Version;
            return handler;
        }

        private void combineFiles()
        {
            if (_scriptTags.Count > 0)
            {
                string combinedScripts = getCombinedFilesHandlerPath(_scriptTags);
                _scriptTags.Clear();
                _scriptTags.Add(combinedScripts);
            }
            if (_deferrableScriptTags.Count > 0)
            {
                string combinedDeferrableScripts = getCombinedFilesHandlerPath(_deferrableScriptTags);
                _deferrableScriptTags.Clear();
                _deferrableScriptTags.Add(combinedDeferrableScripts);
            }
            if (_cssTags.Count > 0)
            {
                string combinedCSSFiles = getCombinedFilesHandlerPath(_cssTags);
                _cssTags.Clear();
                _cssTags.Add(combinedCSSFiles);
            }
        }
        private void renderScriptTags(List<string> files, bool canDeferLoadingViaJQuery)
        {
            for (int i = 0; i < files.Count; i++)
            {
                renderScriptTag(files[i], canDeferLoadingViaJQuery);
            }
        }

        private void renderCombinerCtrls()
        {
            if (!this.CombineControls.HasValue)
            {
                this.CombineControls = getDefaultCombineValue();
            }
            if (this.CombineControls.Value || this.Functionality.RemoveDebugFilesExtension || this.DeferLoadingThroughJQuery)
            {
                checkTags();

                _scriptTags.AddRange(this.Functionality.ExtraJSFiles);
                _cssTags.AddRange(this.Functionality.ExtraCSSFiles);
                
                parseScriptTags();
                checkDeferrableScriptTags();

                //Combine scripts and reduce multiple scripts in _scriptTags & _deferredScriptTags to count=1
                if (this.CombineControls.Value)
                {
                    combineFiles();
                }

                this.Controls.Clear();

                renderScriptTags(_scriptTags, false);
                renderScriptTags(_deferrableScriptTags, true);
                renderCssTags(_cssTags);


                /*
                renderNonCombinerScriptTags(); //If no combine is made, all are output
                if (this.CombineControls.Value)
                {
                    renderCombinerScriptTag();
                }
                
                renderNonCombinerCssTags();  //If no combine is made, all are output
                if (this.CombineControls.Value)
                {
                    renderCombinerCssTag();
                }
                */

            }

        }
        protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer)
        {
            
                renderCombinerCtrls();

            base.RenderChildren(writer);
        }

    }
}
