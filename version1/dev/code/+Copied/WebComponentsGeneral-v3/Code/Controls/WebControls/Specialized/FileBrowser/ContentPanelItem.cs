﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Cms.Classes;
namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.FileBrowser
{
    public class ContentPanelItem : MyDiv
    {
        public class FUNCTIONALITY
        {
            public delegate void OnDeletedItemDelegate(ContentPanelItem sender);
            public string Title { get; set; }
            public string LinkURL { get; set; }
            public string ImageURL { get; set; }
            public int? ImageWidth { get; set; }
            public int? ImageHeight { get; set; }
            public List<ExtraButton> Buttons { get; set; }
            private ContentPanelItem _ContentPanelItem = null;
            public FUNCTIONALITY(ContentPanelItem ContentPanelItem)
            {
                _ContentPanelItem = ContentPanelItem;
                this.chkCheckBox = new MyCheckBox() { ID = _ContentPanelItem.ID + "_chkCheckBox" };
                this.Buttons = new List<ExtraButton>();
                this.Button_Delete_ImageURL = "/_common/static/images/components/v1/filebrowser/delete.gif";
                this.AddDeleteButton = true;
                this.Width = 100;


            }
            public object Tag { get; set; }
            private void addStandardButtons()
            {
                if (this.AddDeleteButton)
                {
                    ExtraButton btn = new ExtraButton();
                    btn.Title = "Delete";
                    btn.CssClass = this.Button_Delete_CssClass;
                    btn.ImageUrl_Up = this.Button_Delete_ImageURL;
                    btn.Click += new EventHandler(btnDelete_Click);
                }
            }

            private void btnDelete_Click(object sender, EventArgs e)
            {
                if (OnDelete != null)
                    OnDelete(_ContentPanelItem);
            }
            private MyCheckBox chkCheckBox { get; set; }
            private MyTable tbContent { get; set; }
            public event OnDeletedItemDelegate OnDelete;
            public int Width { get; set; }
            private int ctrlIndex {get;set;}
            public string Button_Delete_ImageURL { get; set; }
            public string Button_Delete_CssClass { get; set; }
            public bool AddDeleteButton { get; set; }
            public bool ShowCheckbox { get; set; }
            public bool IsChecked()
            {
                return ShowCheckbox && chkCheckBox.GetFormValueObjectAsBool();
            }
            private void renderImage()
            {
                MyTableRow tr = tbContent.AddRow();  tr.CssManager.AddClass("contentItem-image");
                MyTableCell td = new MyTableCell(); tr.Cells.Add(td); td.CssManager.AddClass("contentItem-image");
                MyImage img = new MyImage(); td.Controls.Add(img); img.CssManager.AddClass("contentItem-image");
                int width, height;
                if (this.ImageWidth.HasValue && this.ImageHeight.HasValue)
                {
                    width = this.ImageWidth.Value;
                    height = this.ImageHeight.Value;
                }
                else
                {
                    int imgWidth, imgHeight;
                    CS.General_v3.Util.Image.getImageSize(CS.General_v3.Util.PageUtil.MapPath(this.ImageURL), out imgWidth, out imgHeight);
                    int maxWidth = this.Width;
                    int maxHeight = this.Width;
                    if (imgWidth > maxWidth || imgHeight > maxHeight)
                    {
                        double ratioResizeW = (double)maxWidth / (double)imgWidth;
                        double ratioResizeH = (double)maxHeight / (double)imgHeight;
                        double ratioResize = Math.Min(ratioResizeW, ratioResizeH);
                        imgWidth = ((int)(ratioResize * (double)imgWidth));
                        imgHeight = ((int)(ratioResize * (double)imgHeight));
                    }
                    width = imgWidth;
                    height = imgHeight;
                }

                img.ImageUrl = this.ImageURL;
                img.Width = width;
                img.Height = height;
                img.AlternateText = this.Title;
                if (!string.IsNullOrEmpty(this.LinkURL))
                    img.HRef = this.LinkURL;



            }
            private void renderText()
            {
                MyTableRow tr = tbContent.AddRow();  tr.CssManager.AddClass("contentItem-text");
                MyTableCell td = new MyTableCell(); tr.Cells.Add(td); td.CssManager.AddClass("contentItem-text");
                if (string.IsNullOrEmpty(this.LinkURL))
                    td.InnerHtml = this.Title;
                else
                {
                    MyAnchor link = new MyAnchor();
                    link.InnerText = this.Title;
                    link.Href = LinkURL;
                    td.Controls.Add(link);
                }
            }
            private void renderButtons()
            {



                if (this.Buttons != null && this.Buttons.Count > 0)
                {
                    MyTableRow tr = tbContent.AddRow(); tr.CssManager.AddClass("contentItem-buttons");
                    MyTableCell td = new MyTableCell(); tr.Cells.Add(td); td.CssManager.AddClass("contentItem-buttons");
                    MyTable tbButtons = new MyTable(); td.Controls.Add(tbButtons); tbButtons.CssManager.AddClass("contentItem-buttons");
                    MyTableRow trButtons = new MyTableRow(); tbButtons.Rows.Add(trButtons); trButtons.CssManager.AddClass("contentItem-buttons-row");
                    for (int i = 0; i < this.Buttons.Count; i++)
                    {
                        var btn = this.Buttons[i];
                        MyTableCell tdButton = new MyTableCell(); trButtons.Cells.Add(tdButton); tdButton.CssManager.AddClass("contentItem-buttons-cell");
                        //imgBtn.AlternateText = btn.Title;
                        if (btn.clickHandler == null)
                        {
                            MyAnchor a = new MyAnchor();
                            a.Href = btn.Href;
                            a.Style[HtmlTextWriterStyle.BackgroundImage] = btn.ImageUrl_Up;

                            tdButton.Controls.Add(a);
                        }
                        else
                        {
                            MyButton imgBtn = new MyButton(_ContentPanelItem.ID + "_" + i); tdButton.Controls.Add(imgBtn);
                            imgBtn.Click += btn.clickHandler;
                            imgBtn.WebControlFunctionality.ConfirmMessage = btn.ConfirmMessageOnClick;
                            imgBtn.ValidationGroup = "none2";
                            imgBtn.CssClass = btn.CssClass;

                            // imgBtn.ImageUrl= btn.ImageUrl_Up;
                            //imgBtn.RollOverClass = btn.ImageUrl_Over;
                            imgBtn.Tag = btn.Tag;
                            tdButton.Controls.Add(imgBtn);
                        }


                    }
                }


            }
            private void renderCheckBox()
            {
                if (ShowCheckbox)
                {
                    MyTableRow tr = tbContent.AddRow("contentItem-checkBox");
                    MyTableCell td = tr.AddCell("contentItem-checkBox"); 
                    this.chkCheckBox.ID = _ContentPanelItem.ID + "_checkbox";
                    
                    td.Controls.Add(chkCheckBox);
                        
                }
            }
            public void Render()
            {
                _ContentPanelItem.CssManager.AddClass("filebrowser-contentPanelItem");
                tbContent = new MyTable(); _ContentPanelItem.Controls.Add(tbContent);
                renderImage();
                renderText();
                renderButtons();
                renderCheckBox();
                
            }

        }
        public new FUNCTIONALITY Functionality { get; set; }
        public ContentPanelItem()
        {
            this.Functionality = new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.Render();
            base.OnLoad(e);
        }
    }
}
