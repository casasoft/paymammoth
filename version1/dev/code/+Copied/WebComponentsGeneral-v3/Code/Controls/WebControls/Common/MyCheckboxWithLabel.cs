namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using BusinessLogic_v3.Frontend.ContentTextModule;

    public class MyCheckBoxWithLabel : MyFormItemWithLabel<bool>
    {
        private MyCheckBox _chk;
        public MyCheckBoxWithLabel(FieldSingleControlParameters parameters = null)
            : base(new MyCheckBox(), parameters)
        {
            _chk = (MyCheckBox)FormControl;
            
        }
        public bool Checked { get { return _chk.Checked; } set { _chk.Checked = value; } }

        public string GroupName { get { return _chk.GroupName; } set { _chk.GroupName = value; } }
        public object Tag { get { return _chk.Tag; } set { _chk.Tag = value; } }
        public string DataValue { get { return _chk.DataValue; } set { _chk.DataValue = value; } }

        public override string GetDataTypeAsString()
        {
            return "checkbox";
        }
    }
}
