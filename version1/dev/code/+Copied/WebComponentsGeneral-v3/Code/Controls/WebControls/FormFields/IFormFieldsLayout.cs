﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields
{
    public interface IFormFieldsLayout
    {
        bool DoNotShowValidationIcons { get; set; }
        bool DoNotShowHelpMessageIcons { get; set; }
        Control Render(FormFields formFields);

    }
}
