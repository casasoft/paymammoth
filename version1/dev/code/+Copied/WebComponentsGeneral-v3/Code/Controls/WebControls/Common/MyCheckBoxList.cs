namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using General_v3.Util;
    using System.Collections;

    public class MyCheckBoxList : MyFormWebControl
    {
        public new FieldRadioButtonOrCheckboxListParameters FieldParameters { get { return (FieldRadioButtonOrCheckboxListParameters)base.FieldParameters; } }
        private List<MyCheckBoxWithLabel> _checkBoxes;

        #region MyRadioButtonList Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected MyCheckBoxList _item = null;

            public List<MyListItemData> Items { get; set; }

            public void AddItemsFromListItemCollection(ListItemCollection coll)
            {
                foreach (ListItem item in coll)
                {
                    Items.Add(new MyListItemData(item));
                }
            }

            internal FUNCTIONALITY(MyCheckBoxList item)
            {
                this.Items = new List<MyListItemData>();
                ContractsUtil.RequiresNotNullable(item, "Item cannot be null");
                this._item = item;
            }

        }


        #region MyCheckBoxList Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class MyCheckBoxListFunctionality : MyFormWebControlFunctionality
        {
            protected new MyCheckBoxList _item { get { return (MyCheckBoxList)_formWebControl; } }

            internal MyCheckBoxListFunctionality(MyCheckBoxList item)
                : base(item)
            {

            }
            protected override string getFormValue()
            {
                if (_item._checkBoxes != null)
                {
                    List<String> list = new List<string>();
                    foreach (MyCheckBoxWithLabel rd in _item._checkBoxes)
                    {
                        if (rd.GetFormValueObjectAsBool())
                        {

                            MyListItemData itemData = (MyListItemData) rd.Tag;
                            list.Add(itemData.Value);
                        }
                    }
                    if (list != null && list.Count > 0)
                    {
                        return CS.General_v3.Util.Text.JoinList<string>(list, ",");
                    }
                }
                return null;
            }
        }

        #endregion
        protected override BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new MyCheckBoxListFunctionality(this);
        }
			

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        public MyCheckBoxList(FieldRadioButtonOrCheckboxListParameters fieldParameters = null)
            : base( HtmlTextWriterTag.Div,  fieldParameters)
        {
            CssManager.AddClass("checkbox-list-container");
            this.JSFieldTypeName = "FieldRadioButtonOrCheckboxList";

        }

        protected override BaseFieldParameters createFieldParameters()
        {
            return new FieldRadioButtonOrCheckboxListParameters();
        }

        protected override void updateInitialValue(object value)
        {
            string sValue = null;
            //if (value != null) sValue = value.ToString();

            IEnumerable listValues = null;

            if (value is string && !string.IsNullOrEmpty((string)value))
            {
                listValues = CS.General_v3.Util.Text.Split((string)value, ",", "-", "|");
            }
            else if (value is IEnumerable)
            {
                listValues = (IEnumerable)value;
            }


            if (_checkBoxes != null && listValues != null)
            {
                if (value is IEnumerable && !(value is string))
                {
                    foreach (var item in listValues)
                    {
                        if (item != null)
                        {
                            foreach (var chk in _checkBoxes)
                            {

                                if (item is Enum)
                                {
                                    if (EnumUtils.CompareEnumValue(chk.Value, (Enum)item) || EnumUtils.CompareEnumValue(chk.DataValue, (Enum)item))
                                    {
                                        chk.InitialValue = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (chk.Value == item.ToString() || chk.DataValue == item.ToString())
                                    {
                                        chk.InitialValue = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

            }



        }

        

        public override string GetDataTypeAsString()
        {
            return "checkbox-list";
        }

        public override void SetFormNames()
        {
        }

        protected override string getElementClientID()
        {
            return this.ClientID;
        }
        private void initControls()
        {
            _checkBoxes = new List<MyCheckBoxWithLabel>();
            if (this.Functionality.Items != null)
            {
                
                MyUnorderedList ul = new MyUnorderedList();
                ul.CssManager.AddClass("checkbox-list");
                int count = 0;
                foreach (MyListItemData item in this.Functionality.Items)
                {
                    MyListItem li = new MyListItem();
                    MyCheckBoxWithLabel checkBox = new MyCheckBoxWithLabel();
                    checkBox.GroupName = this.ClientID;
                    checkBox.FormControl.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    li.CssManager.AddClass("checkbox-list-item");
                    checkBox.LabelContentText = item.LabelContentText;
                    checkBox.Label = item.Label;
                    checkBox.DataValue = item.Value;

                    checkBox.Value = item.Value;
                    checkBox.Checked = item.Selected;
                    checkBox.ReadOnly = _readOnly;
                    li.Controls.Add(checkBox);
                    ul.Controls.Add(li);
                    _checkBoxes.Add(checkBox);
                    checkBox.Tag = item;

                    string id = this.ClientID + "_chkBox" + count++;
                    //They have static ID so that it doesn't change depending on when they are added in display list
                    checkBox.FormControl.ID = id;
                }
                this.Controls.Add(ul);
            }
        }

        public object InitialValue { get { return this.WebControlFunctionality.InitialValue; } set { this.WebControlFunctionality.InitialValue = value; } }

        protected override void OnLoad(EventArgs e)
        {
            initControls();
            base.OnLoad(e);
        }
        private bool? _readOnly;
        public override bool? ReadOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                _readOnly = value;
                if (_checkBoxes != null)
                {
                    foreach (var item in _checkBoxes)
                    {
                        item.ReadOnly = value;
                    }
                }
            }
        }

        public override string JSFieldTypeName
        {
            get;
            set;
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.FieldParameters.containerElementID = this.ClientID;
            this.FieldParameters.radioButtonsOrCheckboxes = true;
            base.OnPreRender(e);
        }
        public List<string> GetFormValues()
        {


            var list = new List<string>();

            string strValue = this.WebControlFunctionality.GetFormValue();
            if (!string.IsNullOrEmpty(strValue))
            {

                list.AddRange(this.WebControlFunctionality.GetFormValue().Split(','));
            }
            return list;
        }
        public List<int> GetFormValuesAsInts()
        {
            var values = GetFormValues();
            return ListUtil.ConvertArrayToListOfInts<string>(values);
        }
        public List<long> GetFormValuesAsLongs()
        {
            var values = GetFormValues();
            return ListUtil.ConvertArrayToListOfLongs<string>(values);
        }
        public List<TEnum> GetFormValuesAsEnums<TEnum>() where TEnum : struct, IConvertible
        {
            var values = GetFormValues();
            List<TEnum> enumValues = new List<TEnum>();
            foreach (var value in values)
            {
                var enumValue = EnumUtils.GetEnumByStringValueNullable<TEnum>(value);
                if (enumValue.HasValue)
                {
                    enumValues.Add(enumValue.Value);
                }
                else
                {
                    throw new Exception("Cannot convert '"+value+"' to type " + typeof(TEnum));
                }
            }
            return enumValues;
        }
        public override object GetInitialTemporaryRandomValue()
        {
            //Each item should work on its own
            return null;
        }
    }
}
