﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields
{
    using Classes.Javascript.UI.jQuery.DatePicker;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

    public enum JQUERY_CALENDAR_SHOW_TYPE
    {
        None = 0,
        Focus = 100,
        Button = 200,
        Both = 300
    }
    public class FieldSingleControlDateParameters : FieldSingleControlParameters
    {
        public _jQueryUIDatePickerOptions jQueryDatePickerOptions = new _jQueryUIDatePickerOptions();
        public string jQueryUICalendarCustomContainerCSSClass;


        public ValidatorDateParameters ValidatorDateParameters
        {
            get
            {
                ValidatorDate dtValidator = GetOrCreateValidator<ValidatorDate>();
                
                return dtValidator.Parameters;
            }
        }

        /* public bool showJQueryCalendar = true;
         public JQUERY_CALENDAR_SHOW_TYPE showjQueryCalendarType = JQUERY_CALENDAR_SHOW_TYPE.Both;
         public string jQueryUICalendarCustomContainerCSSClass;
         public string jQueryCalendarButtonImageIcon = "/_common/static/images/calendar-icon-24x24.png";*/
    }
}