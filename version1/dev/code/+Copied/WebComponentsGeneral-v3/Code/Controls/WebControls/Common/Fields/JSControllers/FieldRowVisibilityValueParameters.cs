﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.JSControllers
{
    public class FieldRowVisibilityValueParameters : JSONObject
    {
        public object value;
        public string selectorShowItems;
        public string selectorHideItems;
    }
}