﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using CS.General_v3.JavaScript.Data;
    using General_v3;
    using General_v3.Controls.WebControls.Common.General;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;
    using CS.WebComponentsGeneralV3.Code.Util;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public class MyFormWebControlFunctionality : BaseWebControlFunctionality
    {

        public bool NeverFillWithRandomValue { get; set; }
        public bool AutomaticallyLoadSavedFormValue { get; set; }
        public virtual void Focus()
        {

            string js = "jQuery('#" + Control.ClientID + "').focus();";
            JSUtil.AddJSScriptToPage(js);
        }

        protected IMyFormWebControl _formWebControl;

        public MyFormWebControlFunctionality(IMyFormWebControl control)
            : base(control.Control)
        {

            _formWebControl = control;
            this.AutomaticallyLoadSavedFormValue = true;
            //control.Control.Init += new EventHandler(Control_Init);
            control.Control.Load += new EventHandler(Control_Load);
            control.Control.PreRender += new EventHandler(Control_PreRender);
        }

        void Control_PreRender(object sender, EventArgs e)
        {
            if (this._formWebControl.LoadInitialValueFromPostback)
            {
                LoadInitialValueFromPostback();
            }
        }
        private bool _initialValueFromPostbackLoaded = false;
        public void LoadInitialValueFromPostback()
        {
            if (!_initialValueFromPostbackLoaded)
            {
                _initialValueFromPostbackLoaded = true;
                this.InitialValue = this.GetFormValue();
            }
        }

        void Control_Init(object sender, EventArgs e)
        {
            if (AutomaticallyLoadSavedFormValue && InitialValue == null)
            {
                InitialValue = getFormValue();
            }
        }

        void Control_Load(object sender, EventArgs e)
        {
            this.Control.ID = _formWebControl.FieldParameters.id;

            if (AutomaticallyLoadSavedFormValue && InitialValue == null)
            {
                InitialValue = getFormValue();
            }
            /*if (AutomaticallyLoadSavedFormValue)
            {
                this.InitialValue = getFormValue();
            }
            */
        }
        /// <summary>
        /// Override this if you want to override the base get form value of this control
        /// </summary>
        /// <returns></returns>
        protected virtual string getFormValue()
        {
            string id = null;
            if (this._control is MyRadioButton)
            {
                MyRadioButton webControl = (MyRadioButton)_control;
                id = webControl.GroupName;
            }
            else
            {
                id = this._control.ClientID;
            }

            if (string.IsNullOrEmpty(id))
            {
                throw new InvalidOperationException("ID of " + _control + " cannot be empty");
            }
            string value = CS.General_v3.Util.PageUtil.GetFormVariable(id);


            if (this._control is MyRadioButton)
            {
                //Radio buttons work differently as their value is part of a group
                bool isSelected = value != null && (value == ((MyRadioButton)_control).Value);
                value = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(isSelected);

            }
            else if (this._control is MyCheckBox)
            {
                //Radio buttons work differently as their value is part of a group
                bool isSelected = value != null && (value == "on" || (value == ((MyCheckBox)_control).Value));
                value = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(isSelected);

            }

            return value;
        }

        public string GetFormValue()
        {
            return getFormValue();
        }


        public IMyFormWebControl FormWebControl { get { return _formWebControl; } }

        public TValidator GetValidator<TValidator>() where TValidator : class, IValidator
        {
            if (_formWebControl != null && _formWebControl.FieldParameters != null)
            {
                return _formWebControl.FieldParameters.GetValidator<TValidator>();
            }
            else
            {
                return null;
            }
        }
        public TValidator GetOrCreateValidator<TValidator>() where TValidator :class, IValidator, new()
        {
            if (_formWebControl != null && _formWebControl.FieldParameters != null)
            {
                return _formWebControl.FieldParameters.GetOrCreateValidator<TValidator>();
            }
            else
            {
                return new TValidator();
            }
        }

        public object InitialValue { get; set; }

    }
}
