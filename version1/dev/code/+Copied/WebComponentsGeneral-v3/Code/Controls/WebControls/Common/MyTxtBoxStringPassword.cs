namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Fields;
    using General_v3;
    using General_v3.Controls.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

    public class MyTxtBoxStringPassword : MyTxtBoxString
    {
        

        public MyTxtBoxStringPassword(FieldSingleControlPasswordParameters fieldParameters = null)
            : base(fieldParameters, INPUT_TYPE.Password)
        {
            this.JSFieldTypeName = "FieldSingleControlPassword";


        }

        public void LoadDefaultsFromDatabase()
        {
            int minLength = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Password_MinLength);
            if (minLength > 0)
            {
                var validatorString = this.WebControlFunctionality.GetOrCreateValidator<ValidatorString>();
                this.FieldParameters.GetOrCreateValidator<ValidatorString>().Parameters.minLength = minLength;
            }
            this.FieldParameters.passwordStrengthType = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<BusinessLogic_v3.Enums.PASSWORD_STRENGTH_CHARACTERS>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Password_StrengthLevel);
            this.FieldParameters.showPasswordStrengthMeter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Password_ShowStrengthMeter);
        }


        public string GetFormValue()
        {
            return this.WebControlFunctionality.GetFormValue();
        }
        protected override BaseFieldParameters createFieldParameters()
        {
            return new FieldSingleControlPasswordParameters();
        }
        public new FieldSingleControlPasswordParameters FieldParameters
        {
            get
            {
                return (FieldSingleControlPasswordParameters)base.FieldParameters;
            }
            set { base.FieldParameters = value; }
        }

        public override object GetInitialTemporaryRandomValue()
        {
            base.GetInitialTemporaryRandomValue();
            string pass = this.InitialValue;
            if (this.FieldParameters.passwordStrengthType.HasFlag(BusinessLogic_v3.Enums.PASSWORD_STRENGTH_CHARACTERS.Numerics))
            {
                pass += CS.General_v3.Util.Random.GetInt(0, 9);
            }
            if (this.FieldParameters.passwordStrengthType.HasFlag(BusinessLogic_v3.Enums.PASSWORD_STRENGTH_CHARACTERS.SpecialCharacters))
            {
                pass += "*";
            } 
            if (this.FieldParameters.passwordStrengthType.HasFlag(BusinessLogic_v3.Enums.PASSWORD_STRENGTH_CHARACTERS.BothLetterCase))
            {
                pass += CS.General_v3.Util.Random.GetString(1, 1).ToUpper();
                pass += CS.General_v3.Util.Random.GetString(1, 1).ToLower();
            }
            return pass;
        }
        
    }
}
