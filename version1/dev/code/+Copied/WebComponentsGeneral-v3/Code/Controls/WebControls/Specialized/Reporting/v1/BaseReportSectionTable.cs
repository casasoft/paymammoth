﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    internal class BaseReportSectionTable : BaseReportSectionTableBase
    {
        

        #region BaseReportSectionTable Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public new class FUNCTIONALITY : BaseReportSectionTableBase.FUNCTIONALITY
        {
            public new IBaseReportSectionData Section { get { return (IBaseReportSectionData)base.Section; } set { base.Section = value; } }
            protected new BaseReportSectionTable _item { get { return (BaseReportSectionTable)base._item; } }

            internal FUNCTIONALITY(BaseReportSectionTable item)
                : base(item)
            {

            }

        }
        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY) base.Functionality; }}
            protected override BaseReportSectionTableBase.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        #endregion
		
			
        /*
        public BaseReportSectionTable()
        {
            
        }
       

        private int _totalRows;
        private int _totalCols;

        private int getAmtCols()
        {
            int cols = 0;
            bool addedTotal = false;
            if (this.Functionality.Data.HeadersLeft != null && this.Functionality.Data.HeadersLeft.Count() > 0)
            {
                cols++;
                //Check total of headers
                foreach(var headerCellData in this.Functionality.Data.HeadersLeft) {

                    if (headerCellData.TotalCellContent != null)
                    {
                        cols++;
                        addedTotal = true;
                        break;
                    }
                }
            }
            if (!addedTotal && this.Functionality.Data.TotalBothHeadersCell != null)
            {
                cols++;
                addedTotal = true;
            }
            if (this.Functionality.Data.HeadersTop != null && this.Functionality.Data.HeadersTop.Count() > 0)
            {
                cols += this.Functionality.Data.HeadersTop.Count();
            }
            else
            {
                //No headers top so take it from the data
                int maxDataCols = 0;
                foreach(var dataRow in this.Functionality.Data.DataRows) {
                    if (dataRow.Cells != null)
                    {
                        maxDataCols = Math.Max(maxDataCols, dataRow.Cells.Count());
                    }
                }
                cols += maxDataCols;
            }
            return cols;
        }
        private int getAmtRows()
        {
            int rows = 0;
            bool addedTotal = false;
            if (this.Functionality.Data.HeadersTop != null && this.Functionality.Data.HeadersTop.Count() > 0)
            {
                rows++;
                //Check total of headers
                foreach (var headerCellData in this.Functionality.Data.HeadersTop)
                {

                    if (headerCellData.TotalCellContent != null)
                    {
                        rows++;
                        addedTotal = true;
                        break;
                    }
                }
            }
            if (!addedTotal && this.Functionality.Data.TotalBothHeadersCell != null)
            {
                rows++;
                addedTotal = true;
            }
            if (this.Functionality.Data.HeadersLeft != null && this.Functionality.Data.HeadersLeft.Count() > 0)
            {
                rows += this.Functionality.Data.HeadersLeft.Count();
            }
            else
            {
                //No headers top so take it from the data
                if (this.Functionality.Data.DataRows != null)
                {
                    rows += this.Functionality.Data.DataRows.Count();
                }
            }
            return rows;
        }

        private void initTableData()
        {
            //MyTableRow
            int rows = getAmtRows();
            int cols = getAmtCols();
            bool containsTopHeaders = this.containsTopHeaders();
            bool containsLeftHeaders = this.containsLeftHeaders();
            for (int y = 0; y < rows; y++)
            {
                MyTableRow r = this.AddRow("cs-report-table-row");
                r.CssManager.AddClass((y % 2 == 0) ? "cs-report-table-row-even" : "cs-report-table-row-odd");
                if (containsTopHeaders && y == 0)
                {
                    r.CssManager.AddClass("cs-report-table-row-header");
                }
                for (int x = 0; x < cols; x++)
                {
                    MyTableCell c = r.AddCell("cs-report-table-cell");
                    c.CssManager.AddClass((x % 2 == 0) ? "cs-report-table-cell-even" : "cs-report-table-cell-odd");
                    if (containsTopHeaders && y == 0 || containsLeftHeaders && x == 0)
                    {
                        c.CssManager.AddClass("cs-report-table-header-cell");
                    }
                }
            }
            _totalCols = cols;
            _totalRows = rows;
        }
        private bool containsLeftHeaders()
        {
            return this.Functionality.Data.HeadersLeft != null && this.Functionality.Data.HeadersLeft.Count() > 0;
        }
        private bool containsTopHeaders()
        {
            return this.Functionality.Data.HeadersTop != null && this.Functionality.Data.HeadersTop.Count() > 0;
        }

        private void init()
        {
            initTableData();
            initTopHeadersAndTotals();
            initLeftHeadersAndTotals();
            initDataCells();
            initTotalHeadersBothCell();
        }
        private void initTopHeadersAndTotals()
        {
            if (containsTopHeaders())
            {
                int startIndex = 0;
                if (containsLeftHeaders())
                {
                    startIndex++;
                }
                int i = 0;
                foreach (var headerData in this.Functionality.Data.HeadersTop)
                {
                    int index = startIndex + i;
                    initCell(this[0][index], headerData.HeaderCellContent, "cs-report-table-top-header-cell");
                    if (headerData.TotalCellContent != null)
                    {
                        initCell(this[_totalRows - 1][index], headerData.TotalCellContent, "cs-report-table-bottom-total-cell");
                    }
                    i++;
                }
            }
        }
        private void initLeftHeadersAndTotals()
        {
            if (containsLeftHeaders())
            {
                int startIndex = 0;
                if (containsTopHeaders())
                {
                    startIndex++;
                }
                int i = 0;
                foreach (var headerData in this.Functionality.Data.HeadersLeft)
                {
                    int index = startIndex + i;
                    initCell(this[index][0], headerData.HeaderCellContent, "cs-report-table-left-header-cell");
                    if (headerData.TotalCellContent != null)
                    {
                        initCell(this[index][_totalCols- 1], headerData.TotalCellContent, "cs-report-table-right-total-cell");
                    }
                    i++;
                }
            }
        }
        private void initDataCells()
        {
            if (this.Functionality.Data.DataRows != null && this.Functionality.Data.DataRows.Count() > 0)
            {
                int startX = containsLeftHeaders() ? 1 : 0;
                int startY = containsTopHeaders() ? 1 : 0;
                int y = 0;
                foreach(var rowData in this.Functionality.Data.DataRows) {
                    int x = 0;
                    foreach(var cellData in rowData.Cells) {
                        this.initCell(this[y + startY][x + startX], cellData, "cs-report-table-data-cell");
                        x++;
                    }
                    y++;
                }
            }
        }
        private void initTotalHeadersBothCell()
        {
            if (this.Functionality.Data.TotalBothHeadersCell != null)
            {
                initCell(this[_totalRows - 1][_totalCols - 1], this.Functionality.Data.TotalBothHeadersCell, "cs-report-table-total-cell");
            }
        }

        private void initCell(MyTableCell cell, IBaseReportTableCellData cellData, string extraCSS)
        {
            bool hasContent = false;
            if (!string.IsNullOrWhiteSpace(cellData.InnerHTML))
            {
                hasContent = true;
                cell.InnerHtml = cellData.InnerHTML;
            }
            else if (cellData.Control != null)
            {
                hasContent = true;
                cell.Controls.Add(cellData.Control);
            }
            cell.CssManager.AddClass(hasContent ? "cs-report-table-cell-content" : "cs-report-table-cell-empty");
            if (!string.IsNullOrWhiteSpace(extraCSS))
            {
                cell.CssManager.AddClass(extraCSS);
            }
            if (cellData.CssManager != null)
            {
                cell.CssManager.AddClass(cellData.CssManager.ToString());
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.CssManager.AddClass("cs-report-table");
            init();
            base.OnLoad(e);
        }*/

        protected override bool hasTotalVertical()
        {
            if (this.Functionality.Section.TableData.HeadersTop != null)
            {
                foreach (var header in this.Functionality.Section.TableData.HeadersTop)
                {

                    if (header.TotalCellContent != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        protected override bool hasTotalHorizontal()
        {
            if (this.Functionality.Section.TableData.HeadersLeft != null)
            {
                foreach (var header in this.Functionality.Section.TableData.HeadersLeft)
                {

                    if (header.TotalCellContent != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        

        protected override bool hasTotalVerticalAndHorizontal()
        {
            return this.Functionality.Section.TableData.TotalBothHeadersCell != null;
        }

        protected override void parseHeaderTotalContent(IBaseReportTableHeaderDataBase header, bool vertical, int index, out System.Web.UI.Control totalContent, out General_v3.Classes.CSS.CSSManager cssClasses)
        {
            IBaseReportTableHeaderData headerData = (IBaseReportTableHeaderData)header;
            if (headerData.TotalCellContent != null)
            {
                totalContent = headerData.TotalCellContent.GetCellContent();
                cssClasses = headerData.TotalCellContent.CssManager;
            }
            else
            {
                totalContent = null;
                cssClasses = null;
            }
        }

        protected override void getVerticalAndHorizontalTotalContent(out System.Web.UI.Control content, out General_v3.Classes.CSS.CSSManager cssClasses)
        {
            if (this.Functionality.Section.TableData.TotalBothHeadersCell != null)
            {
                content = this.Functionality.Section.TableData.TotalBothHeadersCell.GetCellContent();
                cssClasses = this.Functionality.Section.TableData.TotalBothHeadersCell.CssManager;
            }
            else
            {
                content = null;
                cssClasses = null;
            }
        }
    }
}
