﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using CS.WebComponentsGeneralV3.Code.Classes.CSS;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Classes.Text;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields
{
    public class FormFieldsItemGroupData
    {
        public string Identifier { get; set; }
        public string Title { get; set; }
        public ContentTextBaseFrontend TitleContentText { get; set; }
        public string DescriptionHtml { get; set; }
        public ContentTextBaseFrontend DescriptionContentText { get; set; }
        public TokenReplacerNew DescriptionReplacementTags { get; set; }
        public string FooterHtml { get; set; }
        public string ValidationGroup { get; set; }
        public CSSManager CssManager { get; set; }

        public List<IFormFieldsItemData> ChildItems { get; private set; }

        public FormFieldsItemGroupData()
        {
            this.CssManager = new CSSManager();
            this.ChildItems = new List<IFormFieldsItemData>();
            this.ValidationGroup = FormFields.DEFAULT_VALIDATION_GROUP;
        }

        public IFormFieldsItemData AddItem(IFormFieldsItemData item)
        {
            this.ChildItems.Add(item);

            if (item.Control != null && item.Control is IMyFormWebControl)
            {
                string cssClass = "form-field-row-" + ((IMyFormWebControl)item.Control).GetDataTypeAsString();
                if(!string.IsNullOrEmpty(item.CssClass))
                {
                    cssClass += " " + item.CssClass;
                }
                item.CssClass = cssClass;
            }

            return item;
        }
        public IMyFormWebControl AddFormWebControl(IMyFormWebControl control, FormFieldsLayoutTypeInformation layoutInfo = null, string subTitle = null, string rowCssClass = null)
        {
            var item = FormFieldsFieldItemDataImpl.GetFromMyFormWebControl(control, layoutInfo, subTitle, rowCssClass);
            
            this.AddItem(item);
            return control;
        }

        public bool HasButton()
        {
            foreach (var item in ChildItems)
            {
                if (item.Control is BaseButtonWebControl)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
