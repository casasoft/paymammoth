namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using General_v3.Util;

    public class MyRadioButtonList : MyFormWebControl
    {
        public new FieldRadioButtonOrCheckboxListParameters FieldParameters { get { return (FieldRadioButtonOrCheckboxListParameters)base.FieldParameters; } }
            private List<MyRadioButtonWithLabel> _radioButtons;

        #region MyRadioButtonList Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected MyRadioButtonList _item = null;

            public List<MyListItemData> Items { get; set; }

            public void AddItemsFromListItemCollection(ListItemCollection coll)
            {
                foreach (ListItem item in coll)
                {
                    Items.Add(new MyListItemData(item));
                }
            }

            internal FUNCTIONALITY(MyRadioButtonList item)
            {
                this.Items = new List<MyListItemData>();
                ContractsUtil.RequiresNotNullable(item, "Item cannot be null");
                this._item = item;
            }

        }


      

        #region MyRadioButtonList Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class MyRadioButtonListFunctionality : MyFormWebControlFunctionality
        {
            protected new MyRadioButtonList _item { get { return (MyRadioButtonList)_formWebControl; } }

            internal MyRadioButtonListFunctionality(MyRadioButtonList item)
                : base(item)
            {

            }
            protected override string getFormValue()
            {
                
                if (_item.Functionality.Items != null && _item._radioButtons == null)
                {
                  //  _item.initControls(); // create radio buttons
                }

                if (_item._radioButtons != null)
                {
                    
                    foreach(MyRadioButtonWithLabel rd in _item._radioButtons)
                    {
                        if (rd.GetFormValueObjectAsBool())
                        {
                            //Selected
                            MyListItemData itemData = (MyListItemData)rd.Tag;
                            return itemData.Value;
                        }
                    }
                }
                return null;
            }

        }

        

        #endregion
        protected override BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new MyRadioButtonListFunctionality(this);
        }
			

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        public MyRadioButtonList(FieldRadioButtonOrCheckboxListParameters fieldParameters = null)
            : base( HtmlTextWriterTag.Div,  fieldParameters)
        {
            CssManager.AddClass("radio-button-list-container");
            this.JSFieldTypeName = "FieldRadioButtonOrCheckboxList";

        }




        protected override BaseFieldParameters createFieldParameters()
        {
            return new FieldRadioButtonOrCheckboxListParameters();
        }

        protected override void updateInitialValue(object value)
        {
            string sValue = null;
            if (value != null) sValue = value.ToString();
            if (_radioButtons != null)
            {
                foreach (var item in _radioButtons)
                {
                    MyListItemData data = (MyListItemData)item.Tag;
                    if (data.Value == sValue)
                    {

                        data.Selected = item.Checked = true;
                        break;
                    }
                }
            }
        }

        

        public override string GetDataTypeAsString()
        {
            return "radio-button-list";
        }

        public override void SetFormNames()
        {
        }

        protected override string getElementClientID()
        {
            return this.ClientID;
        }
        private void initControls()
        {
            _radioButtons = new List<MyRadioButtonWithLabel>();
            if (this.Functionality.Items != null)
            {
                MyUnorderedList ul = new MyUnorderedList();
                ul.CssManager.AddClass("radio-button-list");
                int count = 0;
                foreach (MyListItemData item in this.Functionality.Items)
                {
                    MyListItem li = new MyListItem();
                    MyRadioButtonWithLabel rd = new MyRadioButtonWithLabel();
                    rd.GroupName = this.ClientID;
                    rd.FormControl.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    li.CssManager.AddClass("radio-button-list-item");
                    rd.LabelContentText = item.LabelContentText;
                    rd.Label = item.Label;
                    if (rd.FormControl is MyRadioButton)
                    {
                        //Should be but just in case
                        ((MyRadioButton) rd.FormControl).DataValue = item.Value;
                    }
                    rd.Tag = item;
                    rd.Checked = item.Selected;
                    rd.ReadOnly = _readOnly;
                    li.Controls.Add(rd);
                    ul.Controls.Add(li);
                    _radioButtons.Add(rd);



                    string id = this.ClientID + "_rd" + count++;
                    //They have static ID so that it doesn't change depending on when they are added in display list
                    rd.FormControl.ID = id;
                }
                this.Controls.Add(ul);
            }
        }

        public object InitialValue { get { return this.WebControlFunctionality.InitialValue; } set { this.WebControlFunctionality.InitialValue = value; } }

        protected override void OnLoad(EventArgs e)
        {
            initControls();
            base.OnLoad(e);
        }
        private bool? _readOnly;
        public override bool? ReadOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                _readOnly = value;
                if (_radioButtons != null)
                {
                    foreach (var rd in _radioButtons)
                    {
                        rd.ReadOnly = value;
                    }
                }
            }
        }

        public override string JSFieldTypeName
        {
            get;
            set;
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.FieldParameters.containerElementID = this.ClientID;
            this.FieldParameters.radioButtonsOrCheckboxes = true;
            base.OnPreRender(e);
        }

        public override object GetInitialTemporaryRandomValue()
        {
            if (this.Functionality.Items != null)
            {
                int amt = this.Functionality.Items.Count;
                int index = CS.General_v3.Util.Random.GetInt(0, amt - 1);
                return  this.Functionality.Items[index].Value;
            }
            else
            {
                return null;
            }
        }
    }
}
