﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1
{
    public interface IBaseNumericalReportSectionData : IBaseReportSectionDataBase
    {


        

        bool ShowTotalsVertical { get; }
        bool ShowTotalsHorizontal { get; }
        bool ShowTotalHorizontalAndVertical { get; }
        int? DecimalPlaces { get; }
        string ThousandsDelimiter { get; }

        new IBaseNumericalReportTableData TableData { get; }





    }
}
