﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators
{
    public class ValidatorCreditCards : ValidatorBase
    {

        public new ValidatorCreditCardsParameters Parameters { get { return (ValidatorCreditCardsParameters)base.Parameters; } set { base.Parameters = value; } }

        protected override ValidatorBaseParameters createParameters()
        {
            return new ValidatorCreditCardsParameters();
        }

        public override object GenerateRandomValidValue()
        {
            return "4111111111111111";
        }
    }
}