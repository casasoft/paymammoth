namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

    public class MyRadioButton : MyInputBaseWithType<bool>
    {
        public bool Checked { get { return this.getAttributeAsBool("checked"); } set { this.setAttributeFromBool("checked", value); } }
        public object Tag { get; set; }

        public MyRadioButton()
            : base( INPUT_TYPE.Radio)
        {
        }

        //public string GroupName
        //{
        //    get { return this.getAttributeAsString("name"); }
        //    set { this.setAttributeFromString("name", value); }
        //}
        public string GroupName { get; set; }


        protected override BaseFieldParameters createFieldParameters()
        {

            return new FieldSingleControlParameters();
        }

        private bool objectToBool(object value)
        {
            bool b = false;
            if (value != null)
            {
                string sValue = value.ToString().ToLower();
                b = ((sValue == "on") || (sValue == "true") || (sValue == "1"));
            }

            return b;
        }
        public new FieldSingleControlParameters FieldParameters
        {
            get
            {
                return (FieldSingleControlParameters)base.FieldParameters;
            }
            set { base.FieldParameters = value; }
        }


        public override string Value
        {
            get
            {
                base.Value = this.ClientID;

                return base.Value;
            }
            set
            {
                base.Value = value;
            }
        }
        /// <summary>
        /// This is set to data-value since the value is used with teh client ID for asp.net
        /// </summary>
        public string DataValue { get { return getAttributeAsString("data-value"); } set { setAttributeFromString("data-value", value); } }

        public bool GetFormValueObjectAsBool()
        {
            return (bool)GetFormValueObject();
        }

        public override object GetFormValueObject()
        {
            return objectToBool(base.GetFormValueObject());
        }
        private void updateValue()
        {
            base.Value = this.ClientID;
        }

        protected override void OnLoad(EventArgs e)
        {
            updateValue();
            base.OnLoad(e);
            
        }

        public override bool? ReadOnly
        {
            get
            {
                return base.ReadOnly;
            }
            set
            {
                base.ReadOnly = value;
                if (value.GetValueOrDefault())
                {
                    //Else it will still be clickable
                    this.Disabled = value;
                }
            }
        }

        private void updateGroupName()
        {
            if (!string.IsNullOrWhiteSpace(this.GroupName))
            {
                this.setAttributeFromString("name", this.GroupName);
            }
            
        }
        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
        }
        public override void RenderControl(HtmlTextWriter writer)
        {
            base.RenderControl(writer);
        }
        protected override void RenderContents(HtmlTextWriter writer)
        {
            base.RenderContents(writer);
        }
        protected override void OnPreRender(EventArgs e)
        {

            updateValue();
            base.OnPreRender(e);
            updateGroupName();
            
        }

        public override string JSFieldTypeName
        {
            get
            {
                
                return "FieldSingleControl";
            }
        }

        public bool Multiple { get { return getAttributeAsBool("multiple"); } set { setAttributeFromBool("multiple", value); } }



        protected override void updateInitialValue(object value)
        {
            if (value is bool)
            {
                this.Checked = (bool)value;
            }
        }

        public override bool InitialValue
        {
            get
            {
                return CS.General_v3.Util.Other.ConvertObjectToBasicDataType<bool>(this.WebControlFunctionality.InitialValue);
            }
            set
            {
                updateInitialValue(value);
            }
        }



        public override object GetInitialTemporaryRandomValue()
        {
            return null;
        }
    }
}
