﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.FileBrowser;
namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.FileBrowser.Pages
{
    public abstract class UploadPage : CS.WebComponentsGeneralV3.Code.Classes.Pages.BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseCommonMasterPage>
    {
        public class SETTINGS
        {
            public string FileUploadPath { get; set; }
            public int? MaxWidth { get; set; }
            public int? MaxHeight { get; set; }
            public SETTINGS()
            {

                this.FileUploadPath = "/uploads/filebrowser/";
            }
        }
        public SETTINGS Settings { get; set; }

        private string GetFileUploadPath_Local()
        {
            string s = CS.General_v3.Util.PageUtil.MapPath(Settings.FileUploadPath);
            if (!s.EndsWith("\\")) s += "\\";
            return s;
        }
        private void outputJS(string relPath)
        {
            string js = "";
            js += "uploadedFile = '" + relPath + "';";
            js += "funcNum = " + CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.FileBrowser.FileBrowserCKEditor.FUNCTIONALITY.CKEditor_FunctionName + ";";
            ClientScript.RegisterStartupScript(this.GetType(), "upload", js, true);
        }
        private void saveFile(HttpPostedFile file)
        {
            //string path = FileUploadPath;
            bool ok = false;
            int index = 0;
            string path = GetFileUploadPath_Local() + file.FileName;
            do
            {

                ok = true;
                if (System.IO.File.Exists(path))
                {
                    index++;
                    path = GetFileUploadPath_Local() + CS.General_v3.Util.IO.GetFilenameOnly(file.FileName) + "_" + index + "." + CS.General_v3.Util.IO.GetExtension(file.FileName);
                    ok = false;
                }

            } while (!ok);
            file.SaveAs(path);
            if (Settings.MaxHeight != null || Settings.MaxWidth != null)
            {
                int width = Settings.MaxWidth ?? 999999;
                int height = Settings.MaxHeight ?? 999999;
                CS.General_v3.Util.Image.ResizeImage(path, width, height, path);
            }
            string relPath = path.Substring(GetFileUploadPath_Local().Length);
            relPath = Settings.FileUploadPath + "/" + relPath;
            relPath = relPath.Replace("\\", "/");
            relPath = relPath.Replace("//", "/");
            outputJS(relPath);
        }
        private void processFiles()
        {
            if (Request.Files.Count > 0)
            {
                HttpPostedFile f = Request.Files[0];
                saveFile(f);
            }

        }
        private void checkPath()
        {
            string s = Settings.FileUploadPath;
            if (string.IsNullOrEmpty(s))
            {
                throw new InvalidOperationException("CS.General_v3.HTTPHandlers.FileBrowser.FileUpload:: Please specify 'FileUploadPath'");
            }
            string path = CS.General_v3.Util.PageUtil.MapPath(s);
            CS.General_v3.Util.IO.CreateDirectory(path);
            if (!System.IO.Directory.Exists(path))
            {
                throw new InvalidOperationException("CS.General_v3.HTTPHandlers.FileBrowser.FileUpload:: Please make sure 'FileUploadPath' folder is created, or user has permissions to create");
            }
        }


        public UploadPage()
        {
            this.Settings = new SETTINGS();
        }

        protected override void OnLoad(EventArgs e)
        {
            

            checkPath();
            processFiles();
            base.OnLoad(e);
        }
        

    }
}
