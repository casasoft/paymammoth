﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Hierarchy
{
    using General_v3.Classes.Interfaces.Hierarchy;
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.NavigationMenu.SliderNavigation;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.DropDownMenu;

    public class HierarchicalControl : MyUnorderedList
    {
        

        #region HierarchicalControl Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            public delegate void ListItemCreateHandler(MyListItem li, IHierarchy item, int depth, int index);

            public event ListItemCreateHandler OnListItemCreate;

            protected HierarchicalControl _item = null;

            internal void callOnListItemCreateEvent(MyListItem li, IHierarchy item, int depth, int index)
            {
                if (OnListItemCreate != null)
                {
                    OnListItemCreate(li, item, depth, index);
                }
            }

            public IEnumerable<IHierarchy> Items { get; set; }

            public int? MaxDepth { get; set; }
            public bool ShowNotVisible { get; set; }
            /// <summary>
            /// Tree like structure
            /// </summary>
            public SliderNavigationJSDetails SliderNavigationJSDetails = null;
            /// <summary>
            /// Dropdown menu on rollover
            /// </summary>
            public DropDownMenuParametersJS DropDownMenuParametersJS = null;



            internal FUNCTIONALITY(HierarchicalControl item)
            {
                this._item = item;
                this.MaxDepth = int.MaxValue;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion


        protected virtual string getChildItemContentCssClassName(int levelIndex)
        {
            return "nav-level-" + levelIndex + "-item-content";
        }

        /// <summary>
        /// Create the child item control to place inside the list item
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        protected virtual BaseWebControl createChildItemContent(IHierarchy level, int levelIndex, MyListItem childItem, int count)
        {
            MySpan spanLevel = new MySpan();
            spanLevel.InnerText = level.Title;
            string css = getChildItemContentCssClassName(levelIndex);
            spanLevel.WebControlFunctionality.CssManager.AddClass(css);
            Functionality.callOnListItemCreateEvent(childItem, level, levelIndex, count);
            return spanLevel;
        }


        private void createLevel(MyUnorderedList levelUL, IEnumerable<IHierarchy> children, int currLevel)
        {
            if (children == null) return;
            int nextLevel = currLevel + 1;
            int count = 0;
            int lastIndex = children.Count()-1;
            foreach (IHierarchy child in children)
            {
                if (child != null && (child.Visible || this.Functionality.ShowNotVisible))
                {
                    MyListItem childItem = new MyListItem();
                    string css = "nav-level-" + currLevel;
                    levelUL.WebControlFunctionality.CssManager.AddClass(css);
                    levelUL.Controls.Add(childItem);
                    childItem.WebControlFunctionality.CssManager.AddClass(css + "-item");
                    if (count == 0) childItem.WebControlFunctionality.CssManager.AddClass(css + "-item-first");
                    if (count == lastIndex) childItem.WebControlFunctionality.CssManager.AddClass(css + "-item-last");
                    IHierarchyNavigation navLevel = (IHierarchyNavigation)child;
                    if(navLevel.Selected)
                    {
                        childItem.WebControlFunctionality.CssManager.AddClass("selected");
                    }
                    BaseWebControl childItemContent = createChildItemContent(child, currLevel, childItem, count);
                    childItem.Controls.Add(childItemContent);
                    var subLevels = child.GetChildren();

                    if (subLevels != null && subLevels.Count() > 0 && (this.Functionality.MaxDepth.HasValue && nextLevel < this.Functionality.MaxDepth.Value) && !navLevel.OmitChildrenInNavigation)
                    {
                        //Contains further children
                        MyUnorderedList ulSubLevel = new MyUnorderedList();
                        createLevel(ulSubLevel, subLevels, nextLevel);
                        childItem.Controls.Add(ulSubLevel);
                    }
                    count++;
                }
            }
        }

        private void createControls()
        {
            createLevel(this, this.Functionality.Items, 0);
        }
        private void initJS()
        {
            if (this.Functionality.SliderNavigationJSDetails != null)
            {
                if (string.IsNullOrEmpty(this.ClientID)) throw new Exception("Please specify ID of control");
                this.Functionality.SliderNavigationJSDetails.rootULElementOrID = this.ClientID;
                SliderNavigationJSDetails.Init(this.Functionality.SliderNavigationJSDetails);
            }
            if (this.Functionality.DropDownMenuParametersJS != null)
            {
                if (string.IsNullOrEmpty(this.ClientID)) throw new Exception("Please specify ID of control");
                this.Functionality.DropDownMenuParametersJS.ulElementID = this.ClientID;
                DropDownMenuParametersJS.InitJS(this.Functionality.DropDownMenuParametersJS);
            }
        }
        
        protected override void OnLoad(EventArgs e)
        {
            createControls();
            initJS();
            base.OnLoad(e);
        }

            

       
		
			

    }
}