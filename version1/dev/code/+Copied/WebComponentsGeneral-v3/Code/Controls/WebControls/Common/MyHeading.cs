﻿namespace CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common
{
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using System.Web.UI;
    using General_v3.Controls.WebControls;

    public class MyHeading : MyInnerHtmlControl
    {
        private HtmlGenericControl _heading;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="headingType"></param>
        /// <param name="text"></param>
        /// <param name="cssClass"></param>
        /// <param name="parentControlToAddTo">The parent control.  If specified, this control is added to the Controls collection of the parent</param>
        public MyHeading(int headingType, string text, string cssClass = null, Control parentControlToAddTo = null)
            : base("h" + headingType)
        {
            if (parentControlToAddTo != null)
                parentControlToAddTo.Controls.Add(this);
            this.CssManager.AddClass(cssClass);
            this.InnerText = text;
        }


    }
}
