﻿using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using CS.WebComponentsGeneralV3.Code.Classes.CSS;
using System.Web.UI;
namespace CS.WebComponentsGeneralV3.Code.Controls.UserControls
{

    public class BaseUserControl : BusinessLogic_v3.Controls.UserControls.BaseUserControl
    {
        public new BasePageBase Page { get { return (BasePageBase)base.Page; } }
        public void RemoveControlFromParentContainer()
        {
            CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(this);
        }
        /// <summary>
        /// Returns the master page of this user control of a particular type (if found)
        /// </summary>
        /// <typeparam name="TMasterPage"></typeparam>
        /// <returns></returns>
        public TMasterPage GetMasterPageOfType<TMasterPage>() where TMasterPage : BaseMasterPageBase
        {
            TMasterPage master = null;
            
            MasterPage currMaster = this.Page.Master;
            while (currMaster != null)
            {
                if (currMaster is TMasterPage)
                {
                    master = (TMasterPage)currMaster;
                    break;
                }
                else
                {
                    currMaster = currMaster.Master;
                }
            }
            return master;
        }







    }
}