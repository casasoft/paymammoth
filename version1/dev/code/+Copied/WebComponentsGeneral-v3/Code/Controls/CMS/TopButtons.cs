﻿namespace CS.WebComponentsGeneralV3.Code.Controls.CMS
{
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI;
    using BusinessLogic_v3.Classes.Cms;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public class TopButtons : BaseUserControl
    {
        protected HtmlTable tblButtons;



        private bool _cleared = false;



        private void clear()
        {
            
            initControls();
            HtmlTableRow tr = tblButtons.Rows[0];
            tr.Cells.Clear();
            _cleared = true;
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!_cleared)
            {
                clear();
            }
            base.OnLoad(e);
        }


        public void AddControl(Control ctrl)
        {
            HtmlTableRow tr = tblButtons.Rows[0];
            if (!_cleared)
            {
                clear();
            }

            if (tr.Cells.Count > 0)
            {
                //Add Seperator
                HtmlTableCell tdSeperator = new HtmlTableCell();
                tdSeperator.Attributes["class"] = "seperator";
                tdSeperator.InnerHtml = "|";
                tr.Cells.Add(tdSeperator);
            }

            HtmlTableCell td = new HtmlTableCell();
            td.Controls.Add(ctrl);

            tr.Cells.Add(td);

        }
        public MyButton AddButton(string title, BusinessLogic_v3.Classes.Cms.EnumsCms.IMAGE_ICON imageIcon, string buttonID)
        {

            MyButton btn = new MyButton();
            btn.ValidationGroup = "top_buttons";
            btn.Text = title;
            btn.ID = buttonID;
            btn.CssManager.AddClass("top-button");
            btn.CssManager.AddClass(EnumsCms.GetButtonCssClass(imageIcon));
            string rolloverImageURL;
            /*btn.ImageUrl = BusinessLogic_v3.Classes.Cms.Enums.GetImageFilePath(imageIcon, out rolloverImageURL);
            btn.ButtonMode = buttonMode;
            if (buttonMode != MyImageButtonWithText.BUTTON_MODE.None)
            {
                btn.RolloverImageUrl = rolloverImageURL;
            }*/
            AddControl(btn);

            return btn;

        }
        public MyAnchor AddLink(string title, BusinessLogic_v3.Classes.Cms.EnumsCms.IMAGE_ICON imageIcon, string href)
        {
            MyAnchor a = new MyAnchor();
            a.InnerText = title;
            a.Style.Add(HtmlTextWriterStyle.BackgroundImage, EnumsCms.GetImageFilePath(imageIcon));
            a.Href = href;

            a.CssManager.AddClass("top-button");
            a.CssManager.AddClass(EnumsCms.GetButtonCssClass(imageIcon));
            AddControl(a);
            return a;

        }
    }
}
