﻿namespace CS.WebComponentsGeneralV3.Code.Controls.CMS
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using BusinessLogic_v3.Classes.Cms;
    using BusinessLogic_v3.Classes.Cms.CmsObjects;

    public class MainMenu : BaseUserControl
    {
        protected Repeater repGroups;


        private MainMenuItem _rootMenu = new MainMenuItem("Root", "/", BusinessLogic_v3.Classes.Cms.EnumsCms.IMAGE_ICON.Add);
        //private List<MainMenuItem> _items = new List<MainMenuItem>();




        public void RenderMenu( )
        {
            initControls();
            

            _rootMenu.CheckMenuItemsForAccess();
            _rootMenu.SortChildrenByPriorityAndTitle(false);


            repGroups.DataSource = _rootMenu.Children;
            repGroups.ItemDataBound += new RepeaterItemEventHandler(repGroups_ItemDataBound);
            repGroups.DataBind();
        }

        void repGroups_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater repLinks = (Repeater)e.Item.FindControl("repLinks");

            MainMenuItem item = (MainMenuItem)e.Item.DataItem;

            List<MainMenuItem> links = item.Children;
            links.Insert(0, item);

            repLinks.DataSource = links;
            repLinks.ItemDataBound += new RepeaterItemEventHandler(repLinks_ItemDataBound);
            repLinks.DataBind();

            
        }

        void repLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            MainMenuItem item = (MainMenuItem)e.Item.DataItem;

            Literal ltlLinkTitle = (Literal)e.Item.FindControl("ltlLinkTitle");
            HtmlAnchor aHref = (HtmlAnchor)e.Item.FindControl("aHref");
            HtmlGenericControl spanImageIcon = (HtmlGenericControl)e.Item.FindControl("spanImageIcon");
            HtmlTableCell tdLink = (HtmlTableCell)e.Item.FindControl("tdLink");

            aHref.Title = ltlLinkTitle.Text = item.Title;
            aHref.HRef = item.URL;

            spanImageIcon.Attributes["class"] = EnumsCms.GetButtonCssClass(item.ImageIcon);
            spanImageIcon.Attributes["class"] += " main-menu-icon";
            spanImageIcon.Attributes["style"] = "background-image:url(" + BusinessLogic_v3.Classes.Cms.EnumsCms.GetImageFilePath(item.ImageIcon) + ");";
            //spanImageIcon.Style.Add("background-image", "url(" + BusinessLogic_v3.OldCMS.Enums.GetImageFilePath(item.ImageIcon) + ")");

            if (e.Item.ItemIndex == 0)
            {
                //Main Link
                tdLink.Attributes["class"] = "link";
            }
            else
            {
                tdLink.Attributes["class"] = "sub-link";
            }
            string currentURL = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL();
            string itemURL = item.URL;
            //if (itemURLWithoutQS.IndexOf('?') > -1)
            //    itemURLWithoutQS = itemURLWithoutQS.Substring(0, itemURLWithoutQS.IndexOf('?'));


            if (currentURL.ToLower().StartsWith(itemURL.ToLower()))
                
            {
                //Selected
                aHref.Attributes["class"] = "selected";
            }
            
        }

        //Main menu is initialised in BusinessLogic_v3.Classes.Cms.Pages.BaseMasterPage.initMainMenu()

        public void AddItem(MainMenuItem item)
        {
            _rootMenu.AddChild(item);
            //_items.Add(item);
        }

        protected override void OnLoad(EventArgs e)
        {
           
            base.OnLoad(e);
        }
        
    }
}
