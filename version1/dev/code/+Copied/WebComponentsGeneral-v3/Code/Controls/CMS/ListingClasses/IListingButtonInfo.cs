﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Code.Controls.CMS.ListingClasses
{
    public interface IListingButtonInfo 
    {
       // CS.General_v3.Enums.HREF_TARGET HRefTarget { get; set; }
        bool NoValidation { get; set; }
       // string RollOverImage { get; set; }
        string Text { get; set; }
        string ValidationGroup { get; set; }
        object Tag { get; set; }
        string CssClass { get; set; }
        string IconImageUrl { get; }

        WebControl Control { get; }
        
    }
}
