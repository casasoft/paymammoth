﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Code.Controls.CMS.ListingClasses
{
    public class ListingItemButtonBaseFunctionality
    {
        private IListingButtonInfo _button;
        public ListingItemButtonBaseFunctionality(IListingButtonInfo button)
        {
            _button = button;
            _button.Control.PreRender += new EventHandler(Control_PreRender);
        }


        void Control_PreRender(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_button.IconImageUrl))
            {
                _button.CssClass += " listing-item-button";
            }
            else
            {
                _button.CssClass += " listing-item-button-icon";
                _button.Control.Style.Add(System.Web.UI.HtmlTextWriterStyle.BackgroundImage, "url(" + _button.IconImageUrl + "");
            }
            
        }
    }
    
        
}
