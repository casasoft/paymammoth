﻿using CS.WebComponentsGeneralV3.Code.Classes.Pages;
namespace CS.WebComponentsGeneralV3.Code.Controls.CMS
{
    using _ComponentsGeneric.Frontend.CMS;

    public class BaseUserControl : CS.WebComponentsGeneralV3.Code.Controls.UserControls.BaseUserControl
    {
        public cms Master
        {
            get { return (cms)this.Page.Master; }
        }




        public new BasePageBase Page
        {
            get { return (BasePageBase)base.Page; }
        }
        protected virtual void initControls()
        {
            
        }
    }
}
