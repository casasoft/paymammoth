﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Classes;
namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructureBasic
{
    using Classes;

    public class TreeItemBasic : ITreeItemBasic
    {




        public virtual long ID { get; set;  }

        public virtual string Title { get; set; }

        public virtual int Priority { get; set; }

        public virtual string ImageURL { get; set; }
        public virtual string LinkURL { get; set; }

        public virtual List<ITreeItemBasic> ChildItems { get; set; }

        public virtual List<ExtraButton> ExtraButtons { get; set; }
        public TreeItemBasic()
        {
            this.ExtraButtons = new List<ExtraButton>();
            this.ChildItems = new List<ITreeItemBasic>();
            this.ImageURL = "/_common/static/images/components/v1/tree/folder.gif";
        }




        public virtual IEnumerable<ITreeItemBasic> GetChildTreeItems()
        {
            return this.ChildItems;
        }
        public virtual void AddExtraButtons()
        {

        }



        public virtual string AddNewItemURL { get; set; }
    }
        
}
