﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;


namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses
{
    public class ListingItemGeneralButton : MyButton, IListingButtonInfo
    {
        private ListingItemButtonBaseFunctionality _functionality;
        public ListingItemGeneralButton()
        {
            _functionality = new ListingItemButtonBaseFunctionality(this);
        }

        #region IListingButtonInfo Members


        public string IconImageUrl
        {
            get;
            set;
        }

        #endregion
    }
        
}
