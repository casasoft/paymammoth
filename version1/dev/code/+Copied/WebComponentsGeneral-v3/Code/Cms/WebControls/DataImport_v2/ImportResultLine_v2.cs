﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util.Encoding.CSV;

namespace BusinessLogic_v3.Classes.Cms.WebControls.DataImport_v2
{
    public class ImportResultLine_v2
    {

        public CS.General_v3.Enums.STATUS_MSG_TYPE GetResultType()
        {
            return DataImportUtil.GetResultTypeFromListOfStatusMsgs(this.StatusMsgs);
        }
        public List<StatusMsg> StatusMsgs { get; private set; }
        public CSVLine CsvLine { get; private set; }
        public int LineIndex { get; set; }
       
        public ImportResultLine_v2(CSVLine csvLine, int lineIndex)
        {
            this.StatusMsgs = new List<StatusMsg>();

            this.LineIndex = lineIndex;
        }
        public void ResetCsvLineMarkerPosition()
        {
            this.CsvLine.ResetMarkerPosition();
        }
        public string GetNextToken()
        {
            return this.CsvLine.GetNextToken();
        }


        private string getMsg(string msg)
        {
            return msg + " <" + this.CsvLine.PeekToken() + ">";
        }

        public double? GetNextTokenAsDouble(bool addErrorIfNull, CS.General_v3.Enums.STATUS_MSG_TYPE errorMsgType, string errorMsg, bool mustBePositive = false)
        {
            double? d = this.CsvLine.GetNextTokenAsDouble();

            if ((addErrorIfNull && !d.HasValue) || 
                (d.HasValue && d.Value < 0))
            {
                this.StatusMsgs.Add(new StatusMsg(errorMsgType, getMsg(errorMsg)));
            }
            return d;
        }
        public int? GetNextTokenAsInt(bool addErrorIfNull, CS.General_v3.Enums.STATUS_MSG_TYPE errorMsgType, string errorMsg, bool mustBePositive = false)
        {
            double? d = GetNextTokenAsDouble(addErrorIfNull, errorMsgType, errorMsg, mustBePositive);
            return (int?)d;
        }


    }
}
