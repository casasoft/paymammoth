﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.DataImport.Generic;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;

using System.IO;
using CS.General_v3.Util.Encoding.CSV;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;


namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.DataImport
{
    public class DataImportForm : Panel
    {
        private MyFileUpload _fileUploadCSV;
        private MyFileUpload _fileUploadZIP;
        private MyTxtBoxString _txtZIPPath;

        public delegate ImportResults FormSubmittedEvent(CSVFile csvFileContents, Stream ZIPFile);
        public DataImportForm()
        {
            this.CssClass = "dataimportform";
            this.Functionality = new FUNCTIONALITY(this);
        }
        public class FUNCTIONALITY
        {
            private DataImportForm _form = null;
            public FUNCTIONALITY(DataImportForm form)
            {
                this._form = form;
                this.ShowZIPFile = true;
            }
            internal ImportResults callFormSubmitted(CSVFile csvFileContents, Stream ZIPFile)
            {
                return FormSubmitted(csvFileContents, ZIPFile);
            }
            public event FormSubmittedEvent FormSubmitted;
            public bool ShowZIPFile { get; set; }

        }

        public FUNCTIONALITY Functionality { get; set; }

        public FormFields Form { get; set; }
        public MyHeading hResults { get; set; }
        public MyUnorderedList ulMsgs { get; set; }
        public ResultsTable ResultsTable { get; set; }

        private void initControls()
        {
            this.Form = new FormFields();
            this.Form.ID = this.ID + "_dataImport";
            this.Controls.Add(Form);
            hResults = new MyHeading(2, "Results"); this.Controls.Add(hResults);
            ulMsgs = new MyUnorderedList(); this.Controls.Add(ulMsgs); ulMsgs.CssClass = "dataimportform-msgs";
            ResultsTable = new ResultsTable(); this.Controls.Add(ResultsTable); ResultsTable.CssManager.AddClass("dataimportform-results");
            ulMsgs.Visible = false;
            ResultsTable.Visible = false;
            hResults.Visible = false;

        }
        private void initForm()
        {
            _fileUploadCSV = this.Form.Functionality.AddFileUpload("txtCSV", "Excel File", true, "Excel File to upload with information");
            if (this.Functionality.ShowZIPFile)
            {
                _fileUploadZIP =  this.Form.Functionality.AddFileUpload("txtZIPFileUpload", "ZIP File (Upload)", false, "ZIP File to upload with images");
                _txtZIPPath = this.Form.Functionality.AddString("txtZIPPath", "Zip File (Path)", false, "Path of ZIP file on server to upload", null);
            }
            this.Form.Functionality.ButtonSubmitText = "Import";
            this.Form.Functionality.ClickSubmit += new EventHandler(Form_ClickSubmit);
        }


        private void addErrorMsgToMsgs(string msg)
        {
            hResults.Visible = true;
            ulMsgs.Visible = true;
            ListItem item = new ListItem(msg);
            item.Attributes["class"] = "error";
            MyListItem li = new MyListItem();
            li.Controls.Add(new Literal() { Text = msg });
            ulMsgs.Controls.Add(li);
        }
        private void addWarningMsgToMsgs(string msg)
        {
            hResults.Visible = true;
            ulMsgs.Visible = true;
            ListItem item = new ListItem(msg);
            item.Attributes["class"] = "warning";
            MyListItem li = new MyListItem();
            li.Controls.Add(new Literal() { Text = msg });
            ulMsgs.Controls.Add(li);
        }
        private void addSuccessMsgToMsgs(string msg)
        {
            hResults.Visible = true;
            ulMsgs.Visible = true;
            ListItem item = new ListItem(msg);
            item.Attributes["class"] = "success";
            MyListItem li = new MyListItem();
            li.Controls.Add(new Literal() { Text = msg });
            ulMsgs.Controls.Add(li);
        }
        void Form_ClickSubmit(object sender, EventArgs e)
        {
            bool ok = true;


            var txtCSV = _fileUploadCSV;

            MyFileUpload txtZIPFileUpload = null;
            string zipPath = null;
            string zipFilename = "";
            if (this.Functionality.ShowZIPFile)
            {
                txtZIPFileUpload = _fileUploadZIP;
                zipPath = _txtZIPPath.GetFormValue();
            }
            string csvFilename = txtCSV.FileName;
            Stream zipFile = null;
          //  string csvFileContents = "";
            if (txtCSV.UploadedFileStream.Length > 0)
            {
              //  csvFileContents = CS.General_v3.Util.IO.GetStringFromStream(txtCSV.FileContent);

                if (txtZIPFileUpload != null && txtZIPFileUpload.UploadedFileStream.Length > 0)
                {
                    zipFile = txtZIPFileUpload.UploadedFileStream;
                    zipFilename = txtZIPFileUpload.FileName;
                }
                else if (!string.IsNullOrEmpty(zipPath))
                {
                    string localPath = CS.General_v3.Util.PageUtil.MapPath(zipPath);
                    if (File.Exists(localPath))
                    {
                        zipFile = new MemoryStream(CS.General_v3.Util.IO.LoadFileAsByteArray(localPath));
                        zipFilename = zipPath;
                    }
                    else
                    {
                        ok = false;
                        addErrorMsgToMsgs("ZIP File does not exist at path <" + zipPath + ">");
                    }

                }

            }
            else
            {
                ok = false;
                addErrorMsgToMsgs("Invalid CSV File");
            }
            hResults.InnerHtml = CS.General_v3.Util.Text.HtmlEncode("Results - Excel: " + csvFilename + " | ZIP: " + zipFilename);
            if (ok)
            {
                try
                {
                    CSVFile csvFile = CSVFile.LoadFromStream(txtCSV.UploadedFileStream, txtCSV.FileName);

                    ImportResults results = this.Functionality.callFormSubmitted(csvFile, zipFile);
                    showResults(results);
                }
                catch (Exception ex)
                {
                    Cms.Util.CmsUtil.ShowErrorMessageInCMS("Error occurred while trying to parse data file", ex);
                }
            }


        }
        private void showResults(ImportResults results)
        {
            ResultsTable.Visible = true;
            if (results.Success)
            {
                addSuccessMsgToMsgs("Data imported successfully");
                ResultsTable.FillFromImportResults(results);

            }
            else
            {
                if (results.ErrorMessages.Count > 0)
                {
                    foreach (var errMsg in results.ErrorMessages)
                    {
                        addErrorMsgToMsgs(errMsg);
                    }

                }
                else
                {
                    addErrorMsgToMsgs("Error importing data");
                }
            }
        }
        protected override void OnInit(EventArgs e)
        {
            initControls();
            base.OnInit(e);

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            initForm();
        }
    }
}
