﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses
{
    using WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public class ListingItemTextLinkButton : MyAnchor, IListingButtonInfo
    {
        public Listing.ItemHrefRetriever HrefRetriever { get; set; }
        public ListingItemTextLinkButton(Listing.ItemHrefRetriever HrefRetriever)
        {
            
            this.HrefRetriever = HrefRetriever;
        }

        public string ValidationGroup { get; set; }
        public bool NoValidation
        {
            get
            {
                return true;
            }
            set
            {
               
            }
        }

        public new string Text
        {
            get
            {
                return CS.General_v3.Util.Text.HtmlDecode(this.InnerHtml);
            }
            set
            {
                this.InnerHtml = CS.General_v3.Util.Text.HtmlEncode(value);
                
            }
        }

        public object Tag { get; set; }

        #region IListingButtonInfo Members


        public string IconImageUrl
        {
            get;
            set;
        }

        #endregion
    }
        
}
