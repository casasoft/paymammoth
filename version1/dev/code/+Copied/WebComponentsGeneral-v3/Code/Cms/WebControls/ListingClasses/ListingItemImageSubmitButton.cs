﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses
{
    using WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;

    public class ListingItemImageSubmitButton : ListingItemImageButton
    {
        public Listing.ItemHandler SubmitHandler { get; set; }
        public ListingItemImageSubmitButton(Listing.ItemHandler SubmitHandler)
        {
            this.SubmitHandler = SubmitHandler;
        }
    }

}
