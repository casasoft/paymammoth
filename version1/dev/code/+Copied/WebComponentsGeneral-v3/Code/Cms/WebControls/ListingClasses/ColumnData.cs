﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses
{
    public class ColumnData
    {
        public bool Sortable { get; set; }
        public string Title { get; set; }
        public string Width { get; set; }
        public string CssClass { get; set; }
        public CS.General_v3.Enums.SORT_TYPE DefaultSort { get; set; }
        public ColumnData(string title, string width, CS.General_v3.Enums.SORT_TYPE DefaultSortType, bool sortable)
            : this(title, width, DefaultSortType, sortable, null)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title">Title of column</param>
        /// <param name="width">Width of column (CSS Width e.g. 10px or 10%).  Leave null if you do not want to specify width</param>
        public ColumnData(string title, string width, CS.General_v3.Enums.SORT_TYPE DefaultSortType, bool sortable,
            string cssClass)
        {
            this.CssClass = cssClass;
            this.Title = title;
            this.Width = width;
            this.DefaultSort = DefaultSortType;
            this.Sortable = sortable;

        }

    }
}
