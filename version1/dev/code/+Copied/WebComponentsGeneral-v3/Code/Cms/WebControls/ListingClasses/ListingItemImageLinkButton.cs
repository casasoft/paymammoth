﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses
{
    using WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public class ListingItemImageLinkButton : MyAnchor, IListingButtonInfo
    {
        private ListingItemButtonBaseFunctionality _functionality;
        public Listing.ItemHrefRetriever HrefRetriever { get; set; }
        public ListingItemImageLinkButton(Listing.ItemHrefRetriever HrefRetriever)
        {
            this.HrefRetriever = HrefRetriever;
            _functionality = new ListingItemButtonBaseFunctionality(this);
        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        #region IListingButtonInfo Members

        public bool NoValidation { get; set; }

        public new string Text
        {
            get
            {

                return this.InnerHtml;
            }
            set
            {
                this.InnerHtml = value;
                this.Title = value;
                
            }
        }


        public bool ValidationGroup { get; set; }

        public object Tag { get; set; }

        #endregion

        #region IListingButtonInfo Members


        string IListingButtonInfo.ValidationGroup { get; set; }
        #endregion

        #region IListingButtonInfo Members


        public string IconImageUrl
        {
            get;
            set;
        }

        #endregion
    }
        
}
