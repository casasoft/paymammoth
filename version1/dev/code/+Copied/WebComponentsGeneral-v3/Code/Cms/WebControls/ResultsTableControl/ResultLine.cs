﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.WebControls.ResultsTableControl
{
    public class ResultLine
    {
        public enum RESULT_TYPE
        {
            OK,
            Error,
            Warning
        }
        public RESULT_TYPE ResultType { get; private set; }
        //p/ublic List<string> ErrorMessages { get; set; }
        //  public List<string> WarningMessages { get; set; }
        public List<string> ColumnValues { get; private set; }
        public int LineIndex { get; set; }
    }
}
