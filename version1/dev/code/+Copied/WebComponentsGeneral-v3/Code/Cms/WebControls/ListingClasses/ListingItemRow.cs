﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses
{
    public class ListingItemRow
    {
        public int RowIndex { get; set; }
        /// <summary>
        /// Whether to show an expandable row with this listing item row
        /// </summary>
        public bool ShowExpandableRow { get; set; }
        /// <summary>
        /// Whether initially, the expandable row is visible or not
        /// </summary>
        public bool InitExpandableRowVisible { get; set; }
        /// <summary>
        /// The expandable row.  Is null if not available
        /// </summary>
        public HtmlTableRow ExpandableRow { get; set; }
        public CheckBox CheckBox { get; set; }
        public HtmlTableRow Row { get; private set; }
        public List<HtmlTableCell> DataCells { get; set; }
        public void MoveDataCellsValueForwardByOne(int startIndex)
        {
            for (int i = startIndex + 1; i < DataCells.Count; i++)
            {
                for (int j = 0; j < DataCells[i - 1].Controls.Count; j++)
                {

                    DataCells[i].Controls.Add(DataCells[i - 1].Controls[j]);
                }

            }
            DataCells[startIndex].Controls.Clear();

        }
        public object Object { get; private set; }
        public object ExpandableRowTag { get; set; }
        public object ExpandableRowTag2 { get; set; }
        public object ExpandableRowTag3 { get; set; }

        public ListingItemRow(HtmlTableRow row, object obj, List<HtmlTableCell> DataCells, int rowIndex)
        {
            this.Row = row;
            this.CheckBox = CheckBox;
            this.Object = obj;
            this.DataCells = DataCells;
            this.RowIndex = rowIndex;

        }
        /// <summary>
        /// Set the values of the data columns.  The passed list of items can be strings, objects, controls etc.  If a Control is passed, it is added
        /// to the table cell controls, else if it is an object, the toString() is added to the innerHTML
        /// </summary>
        /// <param name="objects">A list of items</param>
        public void SetDataColumns(params object[] objects)
        {
            if (objects.Length != DataCells.Count)
            {
                throw new InvalidOperationException("The amount of parameters passed must be equal to the data cells.  There are " + this.DataCells.Count + " data cells.");

            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    HtmlTableCell td = DataCells[i];
                    object obj = objects[i];
                    if (obj is System.Web.UI.Control)
                    {
                        td.Controls.Add((System.Web.UI.Control)obj);
                    }
                    else
                    {
                        td.InnerHtml = obj.ToString();
                    }
                }
            }
        }

        public bool Selected
        {
            get
            {
                if (CheckBox != null)
                {
                    return CheckBox.Checked;
                }
                else
                {
                    return false;
                }
            }
        }

        public HtmlTableCell this[int i]
        {
            get
            {
                return DataCells[i];
            }
        }

    }

}
