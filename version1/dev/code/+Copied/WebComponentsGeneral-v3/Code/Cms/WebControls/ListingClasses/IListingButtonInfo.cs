﻿namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses
{
    using System.Web.UI.WebControls;

    public interface IListingButtonInfo 
    {
       // CS.General_v3.Enums.HREF_TARGET HRefTarget { get; set; }
       // string RollOverImage { get; set; }
        string Text { get; set; }
        string ValidationGroup { get; set; }
        object Tag { get; set; }
        string CssClass { get; set; }
        string IconImageUrl { get; }

        WebControl Control { get; }
        
    }
}
