﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.WebControls.DataImport_v2
{
    public class StatusMsg
    {
        
        public string Title { get; set; }
        public CS.General_v3.Enums.STATUS_MSG_TYPE MsgType { get; set; }
        public string MessageHtml { get; set; }
        public int? ColumnIndex { get; set; }
        public StatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE msgType, string title, string message = null, int? columnIndex = null)
        {
            this.Title = title;
            this.MsgType = msgType;
            this.MessageHtml = message;
            this.ColumnIndex = columnIndex;
        }
    }
}
