﻿namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructure
{
    using CS.General_v3.Classes.HelperClasses;
    using CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructureBasic;

    public interface ITreeItem : ITreeItemBasic
    {
        //int ParentID { get; set; }
        
        string EditURL { get;  }
        string AddNewItemURL { get; }
        bool AllowUpdate { get;  }
        bool AllowDelete { get;  }
        //bool AllowAdd { get; }
        bool AllowAddSubItems { get; }
        bool CanRemove(out string errorMsg);
        OperationResult Remove();
        OperationResult Save();
        
        /// <summary>
        /// Delete message to show.  If left null, default message is shown
        /// </summary>
        string Message_DeleteOK { get; }
        /// <summary>
        /// Confirm message to show.  If left null, default message is shown
        /// </summary>
        string Message_ConfirmDelete { get;  }


        bool AddedExtraButtons { get; set; }
    }
        
}
