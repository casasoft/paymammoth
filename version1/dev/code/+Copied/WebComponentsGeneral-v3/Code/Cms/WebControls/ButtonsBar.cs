﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls
{
    [ToolboxData("<{0}:ButtonsBar runat=server></{0}:ButtonsBar>")]    
    
    public class ButtonsBar : BaseCmsWebControl
    {
        private List<WebControl> _buttons = new List<WebControl>();
        public bool HasButtons()
        {
            return _buttons.Count > 0;
        }


        /*
        public MyButton AddButton(string title, Enums.IMAGE_ICON imageIcon)
        {
            return AddButton(title, imageIcon, null,null);
            
            
        }
        public MyButton AddButton(string title, Enums.IMAGE_ICON imageIcon, string href, EventHandler clickHander)
        {

            string rollOverImageURL;
            string imageURL = Enums.GetImageFilePath(imageIcon, out rollOverImageURL);
            return AddButton(title, imageURL, rollOverImageURL,href, clickHander);

        }*/
        public MyButton AddButton(string title, EnumsCms.IMAGE_ICON imageIcon, EventHandler clickHandler )
        {
            MyButton btn = new MyButton();
            btn.Text = title;
            btn.CssClass = EnumsCms.GetButtonCssClass(imageIcon);
            btn.Style.Add(HtmlTextWriterStyle.BackgroundImage, EnumsCms.GetImageFilePath(imageIcon));
            //btn.AlternateText = btn.Text = title;
            //btn.ResolveClientHRef = false;
            //btn.ImageUrl = imageURL;
            //btn.RolloverImageUrl = rolloverImageURL;
            btn.Click += clickHandler;
            this._buttons.Add(btn);
            return btn;
        }
        public MyAnchor AddLink(string title, EnumsCms.IMAGE_ICON imageIcon,    string href )
        {
            MyAnchor a = new MyAnchor();
            a.InnerText = title;
            a.Style.Add(HtmlTextWriterStyle.BackgroundImage, EnumsCms.GetImageFilePath(imageIcon));
            a.CssClass = EnumsCms.GetButtonCssClass(imageIcon);
            //btn.Style.Add(HtmlTextWriterStyle.BackgroundImage, Enums.GetImageFilePath(imageIcon));
            //btn.AlternateText = btn.Text = title;
            //btn.ResolveClientHRef = false;
            //btn.ImageUrl = imageURL;
            //btn.RolloverImageUrl = rolloverImageURL;
                a.Href= href;
            this._buttons.Add(a);
            return a;
        }

        private void initButtons(HtmlTable tbl)
        {
            HtmlTableRow tr = new HtmlTableRow();
            tbl.Rows.Add(tr);
            for (int i = 0; i < this._buttons.Count; i++)
            {
                HtmlTableCell td = new HtmlTableCell();
                tr.Cells.Add(td);
                td.Controls.Add(this._buttons[i]);
            }
        }
        private void render()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "buttons-bar";
            this.Controls.Add(div);
            HtmlTable tbl = new HtmlTable();
            tbl.CellSpacing = 0;
            tbl.CellPadding = 10;
            tbl.Attributes["class"] = "buttons-bar";
            div.Controls.Add(tbl);
            initButtons(tbl);

        }

        protected override void OnLoad(EventArgs e)
        {
            render();
            base.OnLoad(e);
        }
    }
}
