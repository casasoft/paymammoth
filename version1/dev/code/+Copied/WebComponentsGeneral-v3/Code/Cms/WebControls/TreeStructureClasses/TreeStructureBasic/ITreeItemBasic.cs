﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Classes;
namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructureBasic
{
    using Classes;

    public interface ITreeItemBasic
    {
        long ID { get; }
        //int ParentID { get; set; }
        string Title { get;  }
        int Priority { get; set; }
        string ImageURL { get;  }
        string LinkURL { get; }
        
       // string EditURL { get;  }
       // string AddNewItemURL { get;  }
       // bool AllowUpdate { get;  }
      //  bool AllowDelete { get;  }
       // bool AllowAdd { get; }
        //bool AllowAddSubItems { get; }
        //void Remove();
       // void Save();
        IEnumerable<ITreeItemBasic> GetChildTreeItems();
        List<ExtraButton> ExtraButtons { get; }
        void AddExtraButtons();
        /// <summary>
        /// Delete message to show.  If left null, default message is shown
        /// </summary>
       // string Message_Delete { get; }
        /// <summary>
        /// Confirm message to show.  If left null, default message is shown
        /// </summary>
       // string Message_Confirm { get;  }

    }
        
}
