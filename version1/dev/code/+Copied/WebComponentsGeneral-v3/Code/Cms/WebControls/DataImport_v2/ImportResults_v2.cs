﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util.Encoding.CSV;

namespace BusinessLogic_v3.Classes.Cms.WebControls.DataImport_v2
{
    public class ImportResults_v2
    {
       
        public List<ImportResultLine_v2> Lines { get; set; }
        public List<string> ColumnNames { get; set; }
        public string CSVFilename { get; set; }
        public string ZIPFilename { get; set; }
        public CS.General_v3.Enums.STATUS_MSG_TYPE GetResultType()
        {
            List<List<StatusMsg>> statusMsgs = new List<List<StatusMsg>>();
            statusMsgs.Add(this.StatusMsgs);
            statusMsgs.AddRange(Lines.ConvertAll(item=>item.StatusMsgs));

            return DataImportUtil.GetResultTypeFromListOfStatusMsgs(statusMsgs.ToArray());

        }
        public List<StatusMsg> StatusMsgs { get; private set; }
        private int currLineIndex = 0;
        public ImportResultLine_v2 AddNewResultLine(CSVLine csvLine)
        {
            ImportResultLine_v2 line = new ImportResultLine_v2(csvLine, currLineIndex);
            currLineIndex++;
            this.Lines.Add(line);
            return line;
        }
        /*public void AddLine(RESULT_TYPE resultType, params string[] columnValues)
        {
            ImportResultLine line = new ImportResultLine(resultType, columnValues);
            AddLine(line);
        }*/
        public ImportResults_v2(int StartLineIndex )
        {
            //this.Success = true;
            this.Lines = new List<ImportResultLine_v2>();
            this.ColumnNames = new List<string>();
            this.currLineIndex = StartLineIndex;
            this.StatusMsgs = new List<StatusMsg>();
        }
        public ImportResults_v2(int StartLineIndex, IEnumerable<string> resultTableColumnNames)
            : this(StartLineIndex)
        {
            this.ColumnNames.AddRange(resultTableColumnNames);
        }
        public ImportResults_v2(int StartLineIndex, params string[] resultTableColumnNames)
            : this(StartLineIndex)
        {
            this.ColumnNames.AddRange(resultTableColumnNames);
        }

    }
}
