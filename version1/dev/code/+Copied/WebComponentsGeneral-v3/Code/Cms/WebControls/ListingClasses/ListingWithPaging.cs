﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;
using System.Web.UI;
namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses
{
    using WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Paging.Old;

    /// <summary>
    /// This will create a listing for CMS with two paging bars at top and bottom.
    /// Same as Listing except that it hass all properties of the PagingBar control.
    /// 
    /// You have to set the FromIndex of where you are displaying items now.  The amount of total pages in the listing,
    /// the amount of total items in the FULL listing.
    /// 
    /// Then you can take the SelectedPage number, which will give you the selected page number by user and the
    /// SelectedShowAmt which will retrieve the current user selection of how much items he wants to show in listing.
    /// 0 means ALL.
    /// </summary>
    [ToolboxData("<{0}:ListingWithPaging runat=server></{0}:ListingWithPaging>")] 
    public class ListingWithPaging : Listing
    {
        private PagingBar_TypeOne _topPaging = new PagingBar_TypeOne();
        private PagingBar_TypeOne _bottomPaging = new PagingBar_TypeOne();

        public new class FUNCTIONALITY : Listing.FUNCTIONALITY
        {
            protected new ListingWithPaging _listing
            {
                get
                {
                    
                    return (ListingWithPaging)base._listing;
                }
            }
            public FUNCTIONALITY(ListingWithPaging listing) : base(listing)
            {
                
            }

            /// <summary>
            /// Text displayed when no results are added to the listing item.
            /// </summary>
            public string NoResultsText { get { return _listing._topPaging.NoResultsText; } set { _listing._topPaging.NoResultsText = _listing._bottomPaging.NoResultsText = value; } }
            /// <summary>
            /// The list of show amounts to be shown in the combo box.  Defaults to 10, 25, 50, 100, 250.  (ALL is always added at the end of the list)
            /// </summary>
            public List<int> ShowAmts { get { return _listing._topPaging.ShowAmts; } set { _listing._topPaging.ShowAmts = _listing._bottomPaging.ShowAmts = value; } }

            /// <summary>
            /// The key of the QS variable to show for updating show amt.  Defaults to the ClientID of listing with '_showAmt' appended to it.
            /// </summary>
            public string QueryStringVarNameShowAmt { get { return _listing._topPaging.QueryStringVarNameShowAmt; } set { _listing._topPaging.QueryStringVarNameShowAmt = _listing._bottomPaging.QueryStringVarNameShowAmt = value; } }

            /// <summary>
            /// The key of the querystring variable to show for updating page.  By default it will set to the item's client ID with '_pg' appended at the end.
            /// This might be a bit ugly, but that guarantees uniqueness since there can be multiple paging in a page.  Change this to 'pg' if you want it to
            /// show something more nice and you only have one paging bar in the page.
            /// </summary>
            public string QueryStringVarNamePage { get { return _listing._topPaging.QueryStringVarNamePage; } set { _listing._topPaging.QueryStringVarNamePage = _listing._bottomPaging.QueryStringVarNamePage = value; } }

            /// <summary>
            /// The amount of pages to show.  Defaults to 3.   This will show 3 page numbers to the left and to the right.  
            /// Thus if you are for example in page 8, it will show 5 6 7 [8] 9 10 11.  etc..
            /// </summary>
            public int ShowAmtPages { get { return _listing._topPaging.ShowAmtPages; } set { _listing._topPaging.ShowAmtPages = _listing._bottomPaging.ShowAmtPages = value; } }
            /// <summary>
            /// The total amount of pages to show
            /// </summary>
            public int AmtPages { get { return _listing._topPaging.AmtPages; } set { _listing._topPaging.AmtPages = _listing._bottomPaging.AmtPages = value; } }
            /// <summary>
            /// 0 based index.  The amount passed here will be incremented by one.  
            /// </summary>
            //public int GetFromItemIndex { get { return _listing._topPaging.FromItemIndex; } set { _listing._topPaging.FromItemIndex = _listing._bottomPaging.FromItemIndex = value; } }
            public int GetFromItemIndex()
            {
                int from = 0;
                if (TotalItems > 0)
                {
                    from = (_listing._topPaging.SelectedPage - 1) * _listing.Functionality.SelectedShowAmt;
                    if (from > _listing.Functionality.SelectedShowAmt)
                        from = _listing.Functionality.SelectedShowAmt;

                }
                return from;
            }
            /// <summary>
            /// The total amount of items in the FULL listing
            /// </summary>
            public int TotalItems { get { return _listing._topPaging.TotalItems; } set { _listing._topPaging.TotalItems = _listing._bottomPaging.TotalItems = value; } }
            /// <summary>
            /// The noun to which describe the items in plural.  Defaults to 'Items'.  Used to show "Showing Items 1 - 10 of 100"
            /// </summary>
            public string NounPlural { get { return _listing._topPaging.NounPlural; } set { _listing._topPaging.NounPlural = _listing._bottomPaging.NounPlural = value; } }
            /// <summary>
            /// The noun to which describe the items.  Defaults to 'Item'.  Used to show "Showing Items 1 - 10 of 100"
            /// </summary>
            public string NounSingular { get { return _listing._topPaging.NounSingular; } set { _listing._topPaging.NounSingular = _listing._bottomPaging.NounSingular = value; } }
            public int? DefaultShowAmount
            {
                get { return _listing._topPaging.DefaultShowAmount; }
                set { _listing._topPaging.DefaultShowAmount = _listing._bottomPaging.DefaultShowAmount = value; }
            }
            /// <summary>
            /// The user selection of Show Amt from the combo box
            /// </summary>
            public int SelectedShowAmt
            {
                get
                {

                    
                    return _listing._topPaging.SelectedShowAmt;
                }
                set { _listing._topPaging.SelectedShowAmt = value; }
            }
            /// <summary>
            /// The selected page by the paging bar.  Taken either from Querystring according to key set or session
            /// </summary>
            public int SelectedPage
            {
                get
                {

                    return _listing._topPaging.SelectedPage;
                }
            }


            public override void DataBind()
            {
                this._listing._topPaging.AmtItems = this._listing._bottomPaging.AmtItems = this.DataSource.Count();
                base.DataBind();
            }
        }

        public new FUNCTIONALITY Functionality
        {
            get
            {
                return (FUNCTIONALITY)base.Functionality;
            }
        }
        protected override Listing.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        
        public ListingWithPaging()
        {
            
        }

        protected override void initControls()
        {
            this.Controls.Add(_topPaging);
            base.initControls(); //Adds the table
            this.Controls.Add(_bottomPaging);
            this.Functionality.QueryStringVarNamePage = this.Functionality.QueryStringVarNamePage;
            this.Functionality.QueryStringVarNameShowAmt = this.Functionality.QueryStringVarNameShowAmt;
        }

        
    }
}
