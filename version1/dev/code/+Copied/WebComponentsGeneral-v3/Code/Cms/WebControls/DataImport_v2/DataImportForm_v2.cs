﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.DataImport.Generic.v2;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;

using System.IO;
using CS.General_v3.Util.Encoding.CSV;

using CS.WebComponentsGeneralV3.Code.Cms.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.DataImport_v2
{
    public class DataImportForm_v2 : Panel
    {
        private MyFileUpload _fileUploadCSV;
        private MyFileUpload _fileUploadZIP;
        private MyTxtBoxString _txtZIPPath;
        private MyTxtBoxString _txtUploadPath;

        public delegate ImportResults FormSubmittedEvent(CSVFile csvFileContents, Stream ZIPFile, string uploadPath);
        public DataImportForm_v2()
        {
            this.CssClass = "dataimportform";
            this.Functionality = new FUNCTIONALITY(this);
        }
        public class FUNCTIONALITY
        {
            private DataImportForm_v2 _form = null;
            public IGenericDataImporter_v2 Importer { get; set; }
            public FUNCTIONALITY(DataImportForm_v2 form)
            {
                this._form = form;
                this.ShowZIPFile = true;
            }
            //internal ImportResults callFormSubmitted(CSVFile csvFileContents, Stream ZIPFile, string uploadPath)
            //{
            //    return FormSubmitted(csvFileContents, ZIPFile, uploadPath);
            //}
           // public event FormSubmittedEvent FormSubmitted;
            public bool ShowZIPFile { get; set; }
            public bool ShowUploadPath { get; set; }

        }

        public FUNCTIONALITY Functionality { get; set; }

        public FormFields Form { get; set; }
        public MyHeading hResults { get; set; }
        public MyUnorderedList ulMsgs { get; set; }
        public ResultsTable_v2 ResultsTable { get; set; }

        private void initControls()
        {
            this.Form = new FormFields() { ID = this.ID + "_dataImport" };
            
            this.Controls.Add(Form);
            hResults = new MyHeading(2, "Results"); this.Controls.Add(hResults);
            ulMsgs = new MyUnorderedList(); this.Controls.Add(ulMsgs); ulMsgs.CssClass = "dataimportform-msgs";
            ResultsTable = new ResultsTable_v2(); this.Controls.Add(ResultsTable); ResultsTable.CssManager.AddClass("dataimportform-results");
            ulMsgs.Visible = false;
            ResultsTable.Visible = false;
            hResults.Visible = false;

        }
        private void initForm()
        {
            _fileUploadCSV = this.Form.Functionality.AddFileUpload("txtCSV", "Excel File", true, "File to upload with information (*.csv, *.txt, *.xls, *.xlsx)");
            if (this.Functionality.ShowZIPFile)
            {
                _fileUploadZIP = this.Form.Functionality.AddFileUpload("txtZIPFileUpload", "ZIP File (Upload)", false, "ZIP File to upload with images");
                _txtZIPPath = this.Form.Functionality.AddString("txtZIPPath", "Zip File (Path)", false, "Path of ZIP file on server to load information from", null);
            }
            if (this.Functionality.ShowUploadPath)
            {
                _txtUploadPath = this.Form.Functionality.AddString("txtUploadPath", "Uploads Path", false, "Path of a folder (relative from website folder), which contains images/media to be linked with in the excel file uploaded", null);
            }
            this.Form.Functionality.ButtonSubmitText = "Import";
            this.Form.Functionality.ClickSubmit += new EventHandler(Form_ClickSubmit);
        }
       

        private void addErrorMsgToMsgs(string msg)
        {
            MyDiv divError = new MyDiv();
            this.Controls.Add(this);
            divError.CssManager.AddClass("import-error");
            divError.InnerHtml = msg;



            hResults.Visible = true;
            ulMsgs.Visible = true;
            MyListItem li = new MyListItem();
            li.Controls.Add(new Literal() { Text = msg });
            li.CssManager.AddClass("error");

            ulMsgs.Controls.Add(li);
        }
        private void addWarningMsgToMsgs(string msg)
        {
            hResults.Visible = true;
            ulMsgs.Visible = false;
            ListItem item = new ListItem(msg);
            item.Attributes["class"] = "warning";
            MyListItem li = new MyListItem();
            li.Controls.Add(new Literal() { Text = msg });
            ulMsgs.Controls.Add(li);
        }
        private void addSuccessMsgToMsgs(string msg)
        {
            hResults.Visible = true;
            ulMsgs.Visible = false;
            ListItem item = new ListItem(msg);
            item.Attributes["class"] = "success";
            MyListItem li = new MyListItem();
            li.Controls.Add(new Literal() { Text = msg });
            ulMsgs.Controls.Add(li);
        }
        void Form_ClickSubmit(object sender, EventArgs e)
        {
            bool ok = true;


            var txtCSV = _fileUploadCSV;

            MyFileUpload txtZIPFileUpload = null;
            string zipPath = null;
            string zipFilename = "";
            if (this.Functionality.ShowZIPFile)
            {
                txtZIPFileUpload = _fileUploadZIP;
                zipPath = _txtZIPPath.GetFormValue();
            }
            string uploadPath = "";
            if (this.Functionality.ShowUploadPath)
            {
                uploadPath = _txtUploadPath.GetFormValue();
            }
            string csvFilename = txtCSV.FileName;
            Stream zipFile = null;
            if (txtCSV.UploadedFileStream.Length > 0)
            {

                if (txtZIPFileUpload != null && txtZIPFileUpload.UploadedFileStream.Length > 0)
                {
                    zipFile = txtZIPFileUpload.UploadedFileStream;
                    zipFilename = txtZIPFileUpload.FileName;
                }
                else if (!string.IsNullOrEmpty(zipPath))
                {
                    string localPath = CS.General_v3.Util.PageUtil.MapPath(zipPath);
                    if (File.Exists(localPath))
                    {
                        zipFile = new MemoryStream(CS.General_v3.Util.IO.LoadFileAsByteArray(localPath));
                        zipFilename = zipPath;
                    }
                    else
                    {
                        ok = false;
                        addErrorMsgToMsgs("ZIP File does not exist at path <" + zipPath + ">");
                    }

                }

            }
            else
            {
                ok = false;
                addErrorMsgToMsgs("Invalid Data File");
            }
            hResults.InnerHtml = CS.General_v3.Util.Text.HtmlEncode("Results - Excel: " + csvFilename + " | ZIP: " + zipFilename);
            if (ok)
            {
                CSVFile csvFile = CSVFile.LoadFromStream(txtCSV.UploadedFileStream, txtCSV.FileName);
                var importer = this.Functionality.Importer;
                if (zipFile != null && zipFile.Length > 0)
                    importer.ExtractZIPFile(zipFile);
                if (!string.IsNullOrWhiteSpace(uploadPath))
                    importer.AddPathsToLookForFiles(uploadPath);
                {
                    string settingsPath = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.ProductsBulkImportUploadPath);
                    if (!string.IsNullOrWhiteSpace(settingsPath))
                        importer.AddPathsToLookForFiles(settingsPath);
                }
                importer.ProcessFile(csvFile);
                //CmsUtil.ShowStatusMessageInCMS("Import started successfully.  Refresh for progress.", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
                
                showResults();
            }


        }

        private void showResults()
        {
            var results = this.Functionality.Importer.Results;
            if (results != null && ( results.Lines.Count > 0 || (results.Lines.Count == 0 & results.GetResultType() != CS.General_v3.Enums.STATUS_MSG_TYPE.Success)))
            {
                var importer = this.Functionality.Importer;
                if (results.Lines.Count > 0)
                {
                    ResultsTable.Visible = true;
                    ResultsTable.FillFromImportResults(results);
                }
                var resultType = results.GetResultType();


                if (importer.IsFinished())
                    addSuccessMsgToMsgs("Data imported. Status: " + CS.General_v3.Util.EnumUtils.StringValueOf(resultType));
                else
                    addSuccessMsgToMsgs("Import Status: " + importer.CurrentLineIndex + " / " + importer.GetTotalLineCount());
                

                if (results.GeneralImportResult.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
                {
                    foreach (var msg in results.GeneralImportResult.StatusMessages)
                    {
                        addErrorMsgToMsgs(msg.Message);
                    }

                }
                else
                {
                    addErrorMsgToMsgs("Error importing data");
                }
            }

        }
        protected override void OnInit(EventArgs e)
        {
            initControls();
            base.OnInit(e);

        }

        private void checkRequirements()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(this.Functionality.Importer, "Importer must be defined!");
        }

        protected override void OnLoad(EventArgs e)
        {
            checkRequirements();
            base.OnLoad(e);
            initForm();
            showResults();
        }
    }
}
