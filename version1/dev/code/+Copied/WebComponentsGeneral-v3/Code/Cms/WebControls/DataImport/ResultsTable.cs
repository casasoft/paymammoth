﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.DataImport.Generic;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;


namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.DataImport
{
    public class ResultsTable : MyTable
    {
        private void addHeader()
        {
            var header = this.AddRow("results-heading");
            header.AddCell("heading", "Line #");
            for (int i = 0; i < results.ColumnNames.Count; i++)
            {
                var col = results.ColumnNames[i];
                header.AddCell("heading", CS.General_v3.Util.Text.HtmlEncode(col));

            }
        }
        private ImportResults results = null;
        private void addLine(ImportResultLine line, bool alternating)
        {
            var row = this.AddRow("results-line");
            if (alternating)
                row.CssManager.AddClass("alt");
            switch (line.ResultType)
            {
                case ImportResults.RESULT_TYPE.OK: row.CssManager.AddClass("result-ok"); break;
                case ImportResults.RESULT_TYPE.Warning: row.CssManager.AddClass("result-warning"); break;
                case ImportResults.RESULT_TYPE.Error: row.CssManager.AddClass("result-error"); break;
            }
            row.AddCell("results-cell", CS.General_v3.Util.Text.HtmlEncode(line.LineIndex.ToString()));
            for (int i = 0; i < line.ColumnValues.Length; i++)
            {
                var val = line.ColumnValues[i] ?? "";

                row.AddCell("results-cell", CS.General_v3.Util.Text.TxtForHTML(CS.General_v3.Util.Text.HtmlEncode(val)));
            }

        }
        public void FillFromImportResults(ImportResults results)
        {
            this.results = results;
            addHeader();
            bool alt = false;
            foreach (var line in results.Lines)
            {
                addLine(line, alt);
                alt = !alt;
            }


        }
    }
}
