﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.DataImport.Generic.v2;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;



namespace CS.WebComponentsGeneralV3.Code.Cms.WebControls.DataImport_v2
{
    public class ResultsTable_v2 : MyTable
    {
        private void addHeader()
        {
            var header = this.AddRow("results-heading");
            header.AddCell("heading", "Line #");
            for (int i = 0; i < results.ColumnNames.Count; i++)
            {
                var col = results.ColumnNames[i];
                header.AddCell("heading", CS.General_v3.Util.Text.HtmlEncode(col));

            }
        }
        private ImportResults results = null;
        private void addLine(ImportResultLine line, bool alternating)
        {
            {
                var row = this.AddRow("results-line");
                if (alternating)
                    row.CssManager.AddClass("alt");
                row.CssManager.AddClass("result-" + CS.General_v3.Util.EnumUtils.StringValueOf(line.Result.Status).ToLower());
                row.AddCell("results-cell", CS.General_v3.Util.Text.HtmlEncode(line.LineIndex.ToString()));
                for (int i = 0; i < line.CsvLine.Count; i++)
                {
                    var val = line.CsvLine[i] ?? "";

                    row.AddCell("results-cell", CS.General_v3.Util.Text.TxtForHTML(CS.General_v3.Util.Text.HtmlEncode(val)));
                }
            }
            if (line.Result.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
            {

                MyUnorderedList ulStatus = new MyUnorderedList();
                foreach (var msg in line.Result.StatusMessages)
                {
                    
                    WebControl li = new WebControl(System.Web.UI.HtmlTextWriterTag.Li);
                    MyDiv divMsg = new MyDiv(); divMsg.CssManager.AddClass("status-msg");divMsg.CssManager.AddClass("status-msg-" + CS.General_v3.Util.EnumUtils.StringValueOf(msg.Status).ToLower());
                    
                    if (!string.IsNullOrWhiteSpace(msg.GetMessageAsHtml()))
                    {
                        MySpan spanMsg = new MySpan(); spanMsg.CssManager.AddClass("status-msg-message"); divMsg.Controls.Add(spanMsg);
                    
                        //MySpan spanMsg = new MySpan(); spanTitle.CssManager.AddClass("status-msg-message");divMsg.Controls.Add(spanMsg);
                        spanMsg.InnerHtml = msg.GetMessageAsHtml();
                    }
                    li.Controls.Add(divMsg);
                    ulStatus.Controls.Add(li);
                }


                var row = this.AddRow("results-line-info");
                var cell = row.AddCell("results-line-info-cell",null,false, ulStatus);
                cell.ColumnSpan = line.CsvLine.Count;
                if (alternating)
                    row.CssManager.AddClass("alt");
                row.CssManager.AddClass("results-line-info-" + CS.General_v3.Util.EnumUtils.StringValueOf(line.Result.Status).ToLower());
                
                
            }

        }
        public void FillFromImportResults(ImportResults results)
        {
            this.results = results;  
            addHeader();
            bool alt = false;
            for (int i = results.Lines.Count-1; i>=0; i--)
            {
                var line = results.Lines[i];
                addLine(line, alt);
                alt = !alt;
            }
            

        }
    }
}
