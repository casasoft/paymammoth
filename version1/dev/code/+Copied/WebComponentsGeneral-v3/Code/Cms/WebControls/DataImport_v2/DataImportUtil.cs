﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.WebControls.DataImport_v2
{
    public static class DataImportUtil
    {

        public static CS.General_v3.Enums.STATUS_MSG_TYPE GetResultTypeFromListOfStatusMsgs(params IEnumerable<StatusMsg>[] statusMsgsLists)
        {
            List<StatusMsg> list = new List<StatusMsg>();
            foreach (var statusMsgList in statusMsgsLists)
            {
                list.AddRange(statusMsgList);
            }

            int worst = (int)CS.General_v3.Enums.STATUS_MSG_TYPE.Success;
            foreach (var msg in list)
            {
                int val = (int)msg.MsgType;
                if (val > worst)
                    worst = val;
            }
            return (CS.General_v3.Enums.STATUS_MSG_TYPE)worst;
        


        }

    }
}
