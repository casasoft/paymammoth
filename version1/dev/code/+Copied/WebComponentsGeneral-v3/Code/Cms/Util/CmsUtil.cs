﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Controls.WebControls.Common.General;



using System.Web.UI;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

using System.Reflection;

using NHibernate.Criterion;
using System.Web.UI.WebControls;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace CS.WebComponentsGeneralV3.Code.Cms.Util
{
    public static class CmsUtil
    {
        public static bool CheckIPWhiteListingOmitted()
        {
            const string key = "omitIPWhiteListing";
            //Check from QS
            bool? qsOmitIPWhitelisting = CS.General_v3.Util.QueryStringUtil.GetBoolNullableFromQS(key);
            if (qsOmitIPWhitelisting != null)
            {
                //Update session
                CS.General_v3.Util.SessionUtil.SetObject(key, qsOmitIPWhitelisting.Value);
            }
            if (!qsOmitIPWhitelisting.HasValue)
            {
                //If no value, load from Session
                qsOmitIPWhitelisting = CS.General_v3.Util.SessionUtil.GetObject<bool?>(key);
            }
            return qsOmitIPWhitelisting.HasValue ? qsOmitIPWhitelisting.Value : false;
        }

        /// <summary>
        /// Checks whether users IP address is white listed in CMS or not
        /// </summary>
        /// <returns></returns>
        public static bool IsUserIPWhitelistedToAccessCMS()
        {
            string whitelistedIPs = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.CMS_WhiteListedIPs);
            bool ok = true;


            //There is no whitelisted IPs and omitIPWhitelisting must not be true
            if (!string.IsNullOrWhiteSpace(whitelistedIPs) && !CheckIPWhiteListingOmitted())
            {

                ok = false;
                var ips = CS.General_v3.Util.Text.Split(whitelistedIPs, ",", "|", " ");

                string userIP = CS.General_v3.Util.PageUtil.GetUserIP();
                for (int i = 0; i < ips.Count; i++)
                {
                    string ipRegex = ips[i].Trim();
                    ok = CS.General_v3.Util.RegexUtil.MatchWithWildCardPattern(userIP, ipRegex);
                    if (ok)
                    {
                        break;
                    }
                }
            }
            return ok;
        }

        public static void ShowErrorMessageInCMS(string msg, Exception ex)
        {
            CmsItemInfo.ShowErrorMessageInCMS(msg, ex);
        }


        public static void ShowStatusMessageInCMS(OperationResult result)
        {
            CmsItemInfo.ShowStatusMessageInCMS(result);
        }
        public static void ShowStatusMessageInCMS(string message, CS.General_v3.Enums.STATUS_MSG_TYPE type)
        {
            CmsItemInfo.ShowStatusMessageInCMS(message, type);
        }
        public static void ShowStatusMessageInCMSAndRefresh(string message, CS.General_v3.Enums.STATUS_MSG_TYPE type)
        {
            ShowStatusMessageInCMS(message, type);
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
            url.RedirectTo();

        }

        public static CmsFieldBase GetPropertyFromListByLabel(this IEnumerable<CmsFieldBase> list, string name)
        {
            foreach (var p in list)
            {
                if (string.Compare(p.Label, name, true) == 0)
                    return p;
            }
            return null;
        }

        public static List<CmsPropertyInfo> GetListOfCmsPropertyInfosFromGenericList(IEnumerable<CmsFieldBase> baseList)
        {
            List<CmsPropertyInfo> list = new List<CmsPropertyInfo>();
            foreach (var item in baseList)
            {
                if (item is CmsPropertyInfo)
                {
                    list.Add((CmsPropertyInfo)item);
                }
            }
            return list;
        }
        //public static CmsPropertySearchInfo GetSearchPropertyByPropertyInfoType(this IEnumerable<CmsPropertySearchInfo> list, PropertyInfo pInfoToFind)
        //{
        //    foreach (var item in list)
        //    {
        //        if (item.PropertyInfo.PropertyInfo == pInfoToFind)
        //        {
        //            return item;
        //        }
        //    }
        //    return null;
        //}
        private static ICultureDetailsBaseFactory getCultureFactory()
        {

            return CultureDetailsBaseFactory.Instance;

        }
        public static IEnumerable<ICultureDetailsBase> GetLanguages()
        {
            var qParams =new BusinessLogic_v3.Classes.DbObjects.Parameters.GetQueryParams() { LoadNonPublishedItems= true};
            var q = CultureDetailsBase.Factory.GetQuery(qParams);
            q.Cacheable();
            var list = CultureDetailsBase.Factory.FindAll(q);
            return list;

            //return getCultureFactory().GetAllItemsInRepository();
            

        }

        public static IEnumerable<MainMenuItem> SortMenuItemsByPriorityAndTitle(IEnumerable<MainMenuItem> menuList)
        {
            List<MainMenuItem> sortedList = new List<MainMenuItem>();
            sortedList.AddRange(menuList);
            sortedList.Sort((item1, item2) =>
                {
                    int priorityCmp = item1.Priority.CompareTo(item2.Priority);
                    if (priorityCmp == 0)
                    {
                        return item1.Title.CompareTo(item2.Title);
                    }
                    else
                    {
                        return priorityCmp;
                    }
                });
            return sortedList;
        }


        public static IEnumerable<CmsFieldBase> SortPropertyListByPrioritizer(this IEnumerable<CmsFieldBase> list, ColumnPrioritizer prioritizer)
        {
            
            return CS.General_v3.Util.ListUtil.SortByMultipleComparers(list,
                ((item1, item2) => (prioritizer.GetPriorityFor(item1).CompareTo(prioritizer.GetPriorityFor(item2)))),
                ((item1, item2) => (item1.Label.CompareTo(item2.Label)))
                );
                

        }
    

        public static string GetToStringValueForObject(object value)
        {
            string s = null;
            if (value != null)
            {
                if (value is ICmsItemInstance)
                {
                    s = ((ICmsItemInstance)value).TitleForCms;
                }
                else
                {
                    s = value.ToString();
                }
            }
            return s;
        }
        public static ICriterion GetNHibernateCriterion(PropertyInfo pInfo, object value)
        {
            string propertyName = pInfo.Name;
            NHibernate.Criterion.ICriterion crit = null;
            if (pInfo.PropertyType.IsEnum)
            {
                if (!Enum.IsDefined(pInfo.PropertyType, value))
                    value = null;
                else if (value is int || value is long)
                    value = Enum.Parse(pInfo.PropertyType, (Convert.ToInt32(value)).ToString());
            }

            if (value != null)
            {
                if (pInfo.PropertyType.IsAssignableFrom(typeof(string)))
                {
                    string s = (string)value;
                    if (!string.IsNullOrEmpty(s))
                    {
                        crit = Expression.InsensitiveLike(propertyName, s, MatchMode.Anywhere);
                    }
                    
                }
                else if (typeof(BaseDbObject).IsAssignableFrom(pInfo.PropertyType) && value is long)
                {
                    crit = Expression.Eq(propertyName + "." + CS.General_v3.Util.ReflectionUtil<BaseDbObject>.GetPropertyName(x => x.ID), value);
                }
                else
                {

                    crit = Expression.Eq(propertyName, value);
                }
            }
            return crit;
        }


        public static string GetLinkQuerystringValue()
        {
            return null;
        }
        /// <summary>
        /// SPlits the link value into name and value, e.g Product-1234
        /// </summary>
        /// <param name="qsValue"></param>
        /// <param name="itemName"></param>
        /// <param name="itemValue"></param>
        /// <returns></returns>
        public static long? GetLinkItemAndValueFromQsParam(string qsValue)
        {
            bool ok = false;

            long value = 0;
            if (long.TryParse(qsValue, out value))
                return value;
            else
                return null;

        }
        /// <summary>
        /// SPlits the link value into name and value, e.g Product-1234
        /// </summary>
        /// <param name="qsValue"></param>
        /// <param name="itemName"></param>
        /// <param name="itemValue"></param>
        /// <returns></returns>
        public static bool GetLinkItemAndValueFromQsParam(string qsValue, out string itemName, out long itemValue)
        {
            bool ok = false;
            itemName = null;
            itemValue = 0;
            if (!string.IsNullOrWhiteSpace(qsValue))
            {
                string[] tokens = qsValue.Split('-');
                if (tokens.Length == 2)
                {

                    itemName = tokens[0];
                    if (!string.IsNullOrWhiteSpace(itemName))
                    {
                        if (long.TryParse(tokens[1], out itemValue))
                        {
                            ok = true;
                        }
                    }

                }
            }
            return ok;
            
        }

        public static ICmsUserBase GetLoggedInUser()
        {
            
            return CmsUserSessionLogin.Instance.GetLoggedInUser();
            
        }

        public static int GetWidthOrDefault(int? width)
        {
            if (width.GetValueOrDefault() == 0)
                return 400;
            else
                return width.Value;
        }
        public static void AddAuditLog(Enums.AUDITLOG_MSG_TYPE msgType, IBaseDbObject dbItem, string msg)
        {
            BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.AddAuditLog(msgType, dbItem, msg);
            //BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBaseFactory.Instance.AddAuditLog(msgType, objectName,
            //    objectID, shortMsg, furtherInfo);
        }

    }
}