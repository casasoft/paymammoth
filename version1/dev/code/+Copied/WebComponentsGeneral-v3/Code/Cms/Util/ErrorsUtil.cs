﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms.Util
{
    public static class ErrorsUtil
    {
        public static string ConvertListOfErrorsToHTML(string titleText, List<string> errors)
        {
            StringBuilder sb = new StringBuilder();
            if (errors != null && errors.Count > 0)
            {
                if (!string.IsNullOrEmpty(titleText))
                {
                    sb.AppendLine("<li>");
                    sb.AppendLine(titleText);
                    
                }


                sb.AppendLine("<ul>");
                foreach (var err in errors)
                {
                    string errHtml = CS.General_v3.Util.Text.TxtForHTML(err);
                    sb.AppendLine("<li>" + errHtml + "</li>");
                }
                sb.AppendLine("</ul>");
                if (!string.IsNullOrEmpty(titleText))
                {
                    sb.AppendLine("</li>");
                }
            }
            return sb.ToString();
        }

    }
}
