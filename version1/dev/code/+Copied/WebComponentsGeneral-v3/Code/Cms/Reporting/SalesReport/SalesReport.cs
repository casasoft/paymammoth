﻿using System.Collections.Generic;
using System.Linq;
using BusinessLogic_v3.Modules.MemberModule;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting.SalesReport;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1;
using BusinessLogic_v3.Modules.OrderModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;


namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting.SalesReport
{
    public class SalesReport : BaseCmsReport
    {
        public SalesReportParams Params { get; set; }

        public SalesReport()
        {
            this.Params = new SalesReportParams();
        }




        protected override ICmsReportParams getReportParams()
        {
            return this.Params;
            
        }

        public double CurrentRevenueTaxRate
        {
            get { return CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<double>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Taxes_RevenueTaxRate); }
        }

        private List<OrderBase> _dataList = null;
        private BaseReport _report = null;

        private void loadData()
        {
            var factory=BusinessLogic_v3.Modules.OrderModule.OrderBaseFactory.Instance;
            var q = factory.GetQuery();

            q.RootCriteria.SetFetchMode(CS.General_v3.Util.ReflectionUtil<OrderBase>.GetPropertyName(x => x.OrderItems), NHibernate.FetchMode.Select);
            q = q.Where(x =>x.Paid);
            q = q.Where(x => !x.IsTestOrder); //dont load test orders
            if (this.Params.DateFrom.HasValue) q = q.Where(x => x.DateCreated >= this.Params.DateFrom);
            if (this.Params.DateTo.HasValue) q = q.Where(x => x.DateCreated <= this.Params.DateTo);
            q = q.OrderBy(x => x.DateCreated).Asc;

            _dataList = factory.FindAll(q).ToList();

            
        }

        private void addHeaderRow(BaseReportSectionDataImpl section)
        {
            var headers = section.TableData.HeadersTop;
            headers.Add(new BaseReportTableHeaderDataImpl("Date"));
            headers.Add(new BaseReportTableHeaderDataImpl("Reference"));
            headers.Add(new BaseReportTableHeaderDataImpl("Details"));
            headers.Add(new BaseReportTableHeaderDataImpl("Paid On"));
            headers.Add(new BaseReportTableHeaderDataImpl("Payment Method"));
            headers.Add(new BaseReportTableHeaderDataImpl("Auth Code"));
            headers.Add(new BaseReportTableHeaderDataImpl("Customer"));
            headers.Add(new BaseReportTableHeaderDataImpl("Total"));
            headers.Add(new BaseReportTableHeaderDataImpl("Revenue Tax"));

        }

        private double totalSales = 0;
        private double totalTax = 0;

        private void fillSectionDetails()
        {
            BaseReportSectionDataImpl section = new BaseReportSectionDataImpl(); _report.Functionality.AddSection(section);
            addHeaderRow(section);
            foreach (var item in _dataList)
            {
                Helpers.OrderToReportRow orderToReportRow = new Helpers.OrderToReportRow(item);
                orderToReportRow.CreateRow(section);
                totalSales += item.GetTotalPrice();
                totalTax += item.GetTotalRevenueTax();
                
            }
        }
       

        public override System.Web.UI.Control GenerateFooter()
        {

            MyTable tbFooter = new MyTable("sales-report-footer"); _report.Controls.Add(tbFooter);
            {
                var row = tbFooter.AddRow("gross-sales");
                row.AddCell("label", "Gross Sales:");
                row.AddCell("value sales", totalSales.ToString("#,0.00"));
            }
            {
                var row = tbFooter.AddRow("expenses");
                row.AddCell("label", "Expenses:");
                row.AddCell("value expenses", this.Params.TotalExpensesDuringPeriod.ToString("#,0.00"));
            }
            {
                var row = tbFooter.AddRow("net-revenue");
                row.AddCell("label", "Net Revenue:");

                double netRevenue = totalSales - this.Params.TotalExpensesDuringPeriod;

                row.AddCell("value net-revenue", netRevenue.ToString("#,0.00"));
            }
            {
                var row = tbFooter.AddRow("net-revenue-tax");
                row.AddCell("label", "Revenue Tax Due (" + CurrentRevenueTaxRate + "%):");
                double taxRate = CurrentRevenueTaxRate / 100d;
                double revenueTaxOnExpenses = this.Params.TotalExpensesDuringPeriod * taxRate;
                double netRevenueTax = totalTax - revenueTaxOnExpenses;

                row.AddCell("value net-revenue-tax", netRevenueTax.ToString("#,0.00"));
            }
            return tbFooter;
        }

        public override BaseReport Generate()
        {
            _report = new BaseReport();
            _report.Functionality.Title = "Member Balance Report";
            _report.Functionality.ShowGeneratedOn = true;
            
            loadData();
            fillSectionDetails();
            
            return _report;

        }
    }
}
