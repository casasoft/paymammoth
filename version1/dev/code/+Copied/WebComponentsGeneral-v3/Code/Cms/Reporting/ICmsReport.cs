﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting
{
    public interface ICmsReport
    {
        
        ICmsReportParams GetReportParams();
        BaseReport Generate();

        Control GenerateFooter();




    }
}