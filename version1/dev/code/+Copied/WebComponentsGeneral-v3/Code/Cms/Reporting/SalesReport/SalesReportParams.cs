﻿using System;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting.Attributes;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting.SalesReport
{
    public class SalesReportParams : ICmsReportParams
    {
        [CmsReportParam(Priority = 10)]
        public DateTime? DateFrom { get; set; }

        [CmsReportParam(Priority = 20)]
        public DateTime? DateTo { get; set; }

        [CmsReportParam(Priority = 30)]
        public double TotalExpensesDuringPeriod { get; set; }

        //[CmsReportParam(Priority = 20, Title = "Start Date To")]
        //public DateTime? WallStartDateTo { get; set; }

        //[CmsReportParam(Priority = 30, Title = "Finalised Date From")]
        //public DateTime? WallFinalisedDateFrom { get; set; }

        //[CmsReportParam(Priority = 40, Title = "Finalised Date To")]
        //public DateTime? WallFinalisedDateTo { get; set; }

        //[CmsReportParam(Priority = 50, Title = "Lottery Draw Date From")]
        //public DateTime? WallLotteryDrawDateFrom { get; set; }

        //[CmsReportParam(Priority = 60, Title = "Lottery Draw Date To")]
        //public DateTime? WallLotteryDrawDateTo { get; set; }



    }
}
