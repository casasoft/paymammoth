﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting
{
    public abstract class BaseCmsReport : ICmsReport
    {
        protected BaseCmsReport()
        {
            init();
        }
        private void init()
        {
            

        }

        #region ICmsReport Members

      

        protected abstract ICmsReportParams getReportParams();


        public abstract Controls.WebControls.Specialized.Reporting.v1.BaseReport Generate();

        #endregion

        #region ICmsReport Members

        ICmsReportParams ICmsReport.GetReportParams()
        {
            return getReportParams();
            
        }

        #endregion

        #region ICmsReport Members

     

        #endregion

        #region ICmsReport Members


        public virtual System.Web.UI.Control GenerateFooter()
        {
            return null;
            
        }

        #endregion
    }
}