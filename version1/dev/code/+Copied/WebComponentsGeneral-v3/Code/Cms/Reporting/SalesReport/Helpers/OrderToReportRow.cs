﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Modules.OrderModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting.SalesReport.Helpers
{
    public class OrderToReportRow
    {
        private OrderBase _order = null;
        public OrderToReportRow(OrderBase o)
        {
            this._order = o;
        }

        private BaseReportTableRowDataImpl _row = null;


     
        public string GetDetailsOfOrderHtml()
        {
            List<string> list = new List<string>();
            foreach (var item in _order.OrderItems)
            {
                list.Add(" - " + item.Title);
            }
            return CS.General_v3.Util.Text.AppendStrings("<br />", list);
        }

        public void CreateRow(BaseReportSectionDataImpl section)
        {
            _row = new BaseReportTableRowDataImpl();
            _row.AddCell(_order.DateCreated.ToString("dd/MM/yyyy hh:mm:sstt"));
            _row.AddCell(_order.Reference, CS.WebComponentsGeneralV3.Cms.OrderModule.OrderBaseCmsFactory.Instance.GetEditUrlForItemWithID(_order.ID).ToString());
            _row.AddCell(GetDetailsOfOrderHtml());
            _row.AddCell(_order.PaidOn.HasValue ? _order.PaidOn.Value.ToString("dd/MM/yyyy hh:mm:sstt") : "");
            _row.AddCell(CS.General_v3.Util.EnumUtils.StringValueOf(_order.PaymentMethod));
            _row.AddCell(_order.AuthCode);
            {
                string link = null;
                if (_order.Member != null)
                {
                    link = CS.WebComponentsGeneralV3.Cms.MemberModule.MemberBaseCmsFactory.Instance.GetEditUrlForItemWithID(_order.Member.ID).ToString();
                }

                _row.AddCell(_order.GetCustomerFullName(), link);
            }
            _row.AddCell(_order.GetTotalPrice().ToString("#,0.00"));
            _row.AddCell(_order.GetTotalRevenueTax().ToString("#,0.00"));



            section.TableData.DataRows.Add(_row);

        }


        public static void AddToSection(BaseReportSectionDataImpl section, OrderBase m)
        {
            OrderToReportRow row = new OrderToReportRow(m);
            row.CreateRow(section);
            
        }
    }
}