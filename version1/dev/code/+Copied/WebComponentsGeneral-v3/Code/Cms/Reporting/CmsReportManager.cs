﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting
{
    public class CmsReportManager
    {
        public CmsReportManager()
        {
            _reports = new List<ICmsReportGenerator>();
        }

        private List<ICmsReportGenerator> _reports = null;
        public void AddReport(ICmsReportGenerator report)
        {
            _reports.Add(report);
        }
        public void AddReport<T>() 
            where T: ICmsReportGenerator, new()

        {
            T instance = new T();
            AddReport(instance);
        }


        public List<ICmsReportGenerator> GetListOfReportsBySection(string section)
        {
            List<ICmsReportGenerator> list = new List<ICmsReportGenerator>();
            foreach (var r in _reports)
            {
                if (string.Compare(r.ReportSection, section, true) == 0)
                {
                    list.Add(r);
                }
            }



            CS.General_v3.Util.ListUtil.SortByMultipleComparers(list,
                (x1, x2) => x1.ReportPriority.CompareTo(x2.ReportPriority),
                (x1, x2) => x1.ReportTitle.CompareTo(x2.ReportTitle));

            return list;
        }

        public List<string> GetDistinctReportSections()
        {
            Dictionary<string, string> sections = new Dictionary<string, string>();
            foreach (var r in this._reports)
            {
                string s = r.ReportSection ?? "";
                sections[s.ToLower()] = s;
            }
            return sections.Values.ToList();

        }


        public bool CheckIfLoggedInUserHasAccessToAtLeastOneReport()
        {
            var loggedInUser = CmsUtil.GetLoggedInUser();
            bool ok = false;
            if (loggedInUser != null)
            {
                foreach (var r in this._reports)
                {
                    if (r.AccessTypeRequired.CheckIfUserHasEnoughAccess(loggedInUser))
                    {
                        ok = true;
                        break;
                    }
                }
            }
            return ok;
        }

        public ICmsReportGenerator GetReportByIdentifier(string identifier)
        {
            return this._reports.Where(x => string.Compare(x.ReportIdentifier, identifier, true) == 0).FirstOrDefault();
            
        }
    }
}