﻿using System;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;
using System.Reflection;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting.Attributes
{
    public class CmsReportParamAttribute : Attribute
    {
        public int Priority { get; set; }
        public bool Required { get; set; }
        public string Title { get; set; }
        public string HelpMessage { get; set; }
        
        public CmsReportParamAttribute()
        {
            this.Priority = int.MaxValue;
        }

        public string GetFieldId(PropertyInfo pInfo)
        {
            string fieldID = pInfo.Name;
            return fieldID;
        }

        private string getTitle(PropertyInfo pInfo)
        {
            string s = this.Title;
            if (string.IsNullOrWhiteSpace(s))
                s = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(pInfo.Name);
            return s;
        }

        public MyInputBase AddToFormField(FormFields formFields, PropertyInfo pInfo)
        {

            string fieldID = GetFieldId(pInfo);
            string title = getTitle(pInfo);
            string sValue = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(fieldID);

            Type pType = pInfo.PropertyType;

            object initialValue = CS.General_v3.Util.Other.ConvertStringToBasicDataType(sValue, pType);

            var dataType = CS.General_v3.Enums.GetDataTypeFromType(pInfo.PropertyType);

            MyInputBase input = null;
            switch (dataType)
            {
                case General_v3.Enums.DATA_TYPE.DateTime:
                case General_v3.Enums.DATA_TYPE.DateTimeNullable:
                    {
                        input = formFields.Functionality.AddDateTime(fieldID, title, this.Required,
                                                    helpMessage: this.HelpMessage,
                                                    initialValue: (DateTime?)initialValue);
                        MyTxtBoxDate dt = (MyTxtBoxDate)input;
                        dt.FieldParameters.jQueryDatePickerOptions.changeYear = true;
                        dt.FieldParameters.jQueryDatePickerOptions.changeMonth = true;
                        break;
                    }
                case General_v3.Enums.DATA_TYPE.Bool:
                    {
                        input = formFields.Functionality.AddBool(fieldID, title, this.Required,
                                                    helpMessage: this.HelpMessage);
                        break;
                    }
                case General_v3.Enums.DATA_TYPE.Double:
                case General_v3.Enums.DATA_TYPE.DoubleNullable:
                    {
                        input = formFields.Functionality.AddDouble(fieldID, title, this.Required,
                                                    helpMessage: this.HelpMessage,
                                                    initialValue: (double?)initialValue);
                        break;
                    }

                default:
                    throw new InvalidOperationException("Field data type not yet mapped");

            }
            return input;
        }


    }
}
