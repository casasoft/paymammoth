﻿using System;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting.Attributes;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting.MemberBalance
{
    public class MemberBalanceReportParams : ICmsReportParams
    {
        [CmsReportParam(Priority = 20, HelpMessage="This will load the member balances as of the specified date.  If left empty, the current date is taken (today)")]
        public DateTime? AsOfDate { get; set; }
        //[CmsReportParam(Priority = 10, Title = "Start Date From")]
        //public DateTime? WallStartDateFrom { get; set; }

        //[CmsReportParam(Priority = 20, Title = "Start Date To")]
        //public DateTime? WallStartDateTo { get; set; }

        //[CmsReportParam(Priority = 30, Title = "Finalised Date From")]
        //public DateTime? WallFinalisedDateFrom { get; set; }

        //[CmsReportParam(Priority = 40, Title = "Finalised Date To")]
        //public DateTime? WallFinalisedDateTo { get; set; }

        //[CmsReportParam(Priority = 50, Title = "Lottery Draw Date From")]
        //public DateTime? WallLotteryDrawDateFrom { get; set; }

        //[CmsReportParam(Priority = 60, Title = "Lottery Draw Date To")]
        //public DateTime? WallLotteryDrawDateTo { get; set; }



    }
}
