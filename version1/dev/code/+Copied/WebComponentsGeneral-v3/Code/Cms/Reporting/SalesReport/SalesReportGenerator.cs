﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting.SalesReport
{
    public class SalesReportGenerator : BaseCmsReportGenerator<CS.WebComponentsGeneralV3.Code.Cms.Reporting.SalesReport.SalesReport>
    {

        public SalesReportGenerator()
        {
            this.ReportSection = "Sales Report";
            this.ReportDescriptionHtml = @"This report will show you a list of all the sales during this period, together with any accumulated tax";
        }

    }
}
