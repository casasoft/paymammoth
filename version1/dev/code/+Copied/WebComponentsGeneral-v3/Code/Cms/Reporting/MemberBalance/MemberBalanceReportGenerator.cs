﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting.MemberBalance
{
    public class MemberBalanceReportGenerator : BaseCmsReportGenerator<CS.WebComponentsGeneralV3.Code.Cms.Reporting.MemberBalance.MemberBalanceReport>
    {

        public MemberBalanceReportGenerator()
        {
            this.ReportSection = "Members";
            this.ReportDescriptionHtml = @"This report will show you a list of all the members, together with their account balance to date.";
        }

    }
}
