﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Modules.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Cms.MemberModule;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting.MemberBalance.Helpers
{
    public class MemberToReportRow
    {
        private MemberBase _member = null;
        public MemberToReportRow(MemberBase m)
        {
            this._member = m;
        }

        private BaseReportTableRowDataImpl _row = null;
        public void CreateRow(BaseReportSectionDataImpl section, DateTime asOfDate)
        {
            _row = new BaseReportTableRowDataImpl();
            _row.AddCell(_member.DateRegistered.ToString("dd/MM/yyyy hh:mm:sstt"));
            {
                MyAnchor a = new MyAnchor();
                a.InnerText = _member.GetFullName();
                a.Href = MemberBaseCmsFactory.Instance.GetEditUrlForItemWithID(_member.ID).ToString();
                a.HrefTarget = General_v3.Enums.HREF_TARGET.Blank;
                _row.AddCell(a);
            }
            _row.AddCell(_member.GetAddressAsOneLine());
            _row.AddCell(_member.Country.HasValue ? CS.General_v3.Util.EnumUtils.StringValueOf(_member.Country.Value) : "");
            _row.AddCell(_member.LastLoggedIn.HasValue ? _member.LastLoggedIn.Value.ToString("dd/MM/yyyy hh:mm:sstt") : "Never");
            _row.AddCell(CS.General_v3.Util.Text.ShowBool(_member.KYCVerified));
            _row.AddCell(CS.General_v3.Util.Text.ShowBool(_member.Activated));
            _row.AddCell(CS.General_v3.Util.Text.ShowBool(_member.Blocked));

            var balanceHistory = _member.GetAccountBalanceHistoryItemAsOfDate(asOfDate);

            double totalCredits = 0;
            if (balanceHistory != null) totalCredits = balanceHistory.GetTotalBalance();

            _row.AddCell((totalCredits.ToString("0")), "balance");

            section.TableData.DataRows.Add(_row);

        }


        public static void AddToSection(BaseReportSectionDataImpl section, MemberBase m, DateTime asOfDate)
        {
            MemberToReportRow row = new MemberToReportRow(m);
            row.CreateRow(section, asOfDate);
            
        }
    }
}