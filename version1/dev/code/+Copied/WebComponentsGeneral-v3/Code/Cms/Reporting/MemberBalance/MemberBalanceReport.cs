﻿using System.Collections.Generic;
using System.Linq;
using BusinessLogic_v3.Modules.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1;
using System;


namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting.MemberBalance
{
    public class MemberBalanceReport : BaseCmsReport
    {
        public MemberBalanceReportParams Params { get; set; }

        public MemberBalanceReport()
        {
            this.Params = new MemberBalanceReportParams();
        }




        protected override ICmsReportParams getReportParams()
        {
            return this.Params;
            
        }

        private List<MemberBase> _memberList = null;
        private BaseReport _report = null;

        private void loadData()
        {
            var factory=BusinessLogic_v3.Modules.MemberModule.MemberBaseFactory.Instance;
            var q = factory.GetQuery();
            q = q.Where(x => !x.IsTestMember);

            _memberList = factory.FindAll(q).ToList();


        }

        private void addHeaderRow(BaseReportSectionDataImpl section)
        {
            var headers = section.TableData.HeadersTop;
            headers.Add(new BaseReportTableHeaderDataImpl("Reg. Date"));
            headers.Add(new BaseReportTableHeaderDataImpl("Name"));
            headers.Add(new BaseReportTableHeaderDataImpl("Address"));
            headers.Add(new BaseReportTableHeaderDataImpl("Country"));
            headers.Add(new BaseReportTableHeaderDataImpl("Last Logged In"));
            headers.Add(new BaseReportTableHeaderDataImpl("KYC Verified"));
            headers.Add(new BaseReportTableHeaderDataImpl("Activated"));
            headers.Add(new BaseReportTableHeaderDataImpl("Blocked"));
            headers.Add(new BaseReportTableHeaderDataImpl("Balance"));
        }
        private void fillSectionDetails()
        {
            BaseReportSectionDataImpl section = new BaseReportSectionDataImpl(); _report.Functionality.AddSection(section);
            addHeaderRow(section);

            DateTime asOfDate = CS.General_v3.Util.Date.Now;
            if (this.Params.AsOfDate.HasValue)
            {
                asOfDate= this.Params.AsOfDate.Value;
                
            }
            
            asOfDate = CS.General_v3.Util.Date.GetEndOfDay(asOfDate);

            foreach (var m in _memberList)
            {

                Helpers.MemberToReportRow.AddToSection(section, m, asOfDate);
            }
        }


        public override BaseReport Generate()
        {
            _report = new BaseReport();
            _report.Functionality.Title = "Member Balance Report";
            _report.Functionality.ShowGeneratedOn = true;
            loadData();
            fillSectionDetails();
            return _report;

        }
    }
}
