﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting
{
    public abstract class BaseCmsReportGenerator<T> : ICmsReportGenerator
        where T : class, ICmsReport,  new()
    {

        protected BaseCmsReportGenerator()
        {
            init();
        }
        private void init()
        {
            Type t = this.GetType();
            {
                string s = t.Name;
                if (s.EndsWith("Generator")) s = s.Substring(0, s.Length - "Generator".Length);
                this.ReportTitle = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(s);
            }


            this.ReportPriority = int.MaxValue;
            this.ReportIdentifier =  t.Name;
            this.ReportSection = "General";
            this.AccessTypeRequired = new BusinessLogic_v3.Classes.Cms.CmsObjects.Access.CmsAccessType(null);
            this.AccessTypeRequired.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.Normal);

        }

        public string ReportTitle { get; set; }

        public string ReportSection { get; set; }

        public int ReportPriority { get; set; }
        public string ReportIdentifier { get; set; }

        #region ICmsReportGenerator Members

        public ICmsReport CreateNewReport()
        {
            return new T();
            
        }

        #endregion

        #region ICmsReportGenerator Members


        public BusinessLogic_v3.Classes.Cms.CmsObjects.Access.CmsAccessType AccessTypeRequired { get; private set; }

        #endregion

        #region ICmsReportGenerator Members


        public string ReportDescriptionHtml { get; set; }

        #endregion
    }
}