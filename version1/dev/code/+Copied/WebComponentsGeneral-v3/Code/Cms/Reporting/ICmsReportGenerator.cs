﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;

namespace CS.WebComponentsGeneralV3.Code.Cms.Reporting
{
    public interface ICmsReportGenerator
    {
        string ReportTitle { get; set; }
        string ReportSection { get; set; }
        string ReportDescriptionHtml { get; set; }

        int ReportPriority { get; set; }

        CmsAccessType AccessTypeRequired { get; }

        string ReportIdentifier { get; set; }
        ICmsReport CreateNewReport();
    }
}