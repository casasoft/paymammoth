﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.Classes.CMSInfo
{
    public abstract class CMSPropertyInfoBase<TItem>
    {
        public string Title { get; set; }

        public object InitialValueIfEmpty { get; set; }

        public bool ShowInListing { get; set; }
        public bool EditableInListing { get; set; }
        public int ListingColumnPriority {get;set;}


        public bool IsRequired { get; set; }
        public int FieldWidth { get; set; }


    }
}
