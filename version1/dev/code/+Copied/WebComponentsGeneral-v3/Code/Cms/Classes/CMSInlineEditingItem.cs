﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BusinessLogic_v3.Classes.DbObjects;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using CS.General_v3.Util;
using System.Reflection;

using BusinessLogic_v3.Classes.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms.Classes
{
    using BusinessLogic_v3;
    using BusinessLogic_v3.Frontend._Base;


    public class CMSInlineEditingItem 
    {
        
        /// <summary>
        /// Create an inline editable item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item to update inline</param>
        /// <param name="selector">The selector the property of the item, e.g. x => x.Title</param>
        /// <param name="control">The control to attach the inline editing with</param>
        /// <param name="originalValue">The initial original value of the text (without content tags replaced)</param>
        /// <param name="replacementTags">The replacement tags to replace text with</param>
        /// <returns></returns>
        public static CMSInlineEditingItem Create<T>(

          T item,
          System.Linq.Expressions.Expression<Func<T, object>> selector,
             IAttributeAccessor control,
          string originalValue = null,
          TokenReplacerNew replacementTags = null, Enums.INLINE_EDITING_TEXT_TYPE textType = Enums.INLINE_EDITING_TEXT_TYPE.PlainText) where T : IBaseDbObject
        {
            var propInfo = ReflectionUtil.GetPropertyBySelector(selector);
            CMSInlineEditingItem inlineEditing = new CMSInlineEditingItem(control, item, propInfo, originalValue, replacementTags, textType);
            return inlineEditing;

        }

        /// <summary>
        /// Create an inline editable item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item to update inline</param>
        /// <param name="selector">The selector the property of the item, e.g. x => x.Title</param>
        /// <param name="control">The control to attach the inline editing with</param>
        /// <param name="originalValue">The initial original value of the text (without content tags replaced)</param>
        /// <param name="replacementTags">The replacement tags to replace text with</param>
        /// <returns></returns>
        public static CMSInlineEditingItem CreateFromFrontend<T>(

          T item,
          System.Linq.Expressions.Expression<Func<T, object>> selector,
             IAttributeAccessor control,
          string originalValue = null,
          TokenReplacerNew replacementTags = null, Enums.INLINE_EDITING_TEXT_TYPE textType = Enums.INLINE_EDITING_TEXT_TYPE.PlainText) where T : IBaseFrontend
        {
            var propInfo = ReflectionUtil.GetPropertyBySelector(selector);
            CMSInlineEditingItem inlineEditing = new CMSInlineEditingItem(control, item.Data, propInfo, originalValue, replacementTags, textType);
            return inlineEditing;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="item"></param>
        /// <param name="type"></param>
        /// <param name="originalValue"></param>
        /// <param name="replacementTags">If this is present, then original value is passed on</param>
        public CMSInlineEditingItem(IAttributeAccessor control, IBaseDbObject item, PropertyInfo property, string originalValue = null, TokenReplacerNew replacementTags = null, Enums.INLINE_EDITING_TEXT_TYPE textType = Enums.INLINE_EDITING_TEXT_TYPE.PlainText)
        {
            if (BusinessLogic_v3.Modules.CmsUserModule.CmsUserSessionLogin.Instance.DoesLoggedInUserHasAtLeastAccessOfType(General_v3.Enums.CMS_ACCESS_TYPE.Administrator))
            {

                var type = item.GetType();
                string itemFullClassName = type.FullName;
                if (itemFullClassName != null && !itemFullClassName.Contains(".") && type.BaseType != null)
                {
                    itemFullClassName = type.BaseType.FullName;
                }
                string propertyName = property.Name;
                control.SetAttribute("data-cms-edit-id", item.ID.ToString());
                control.SetAttribute("data-cms-edit-type", itemFullClassName);
                control.SetAttribute("data-cms-edit-propName", propertyName);

                if (textType == Enums.INLINE_EDITING_TEXT_TYPE.Html && CS.General_v3.Util.HtmlUtil.IsControlOneOfTheseTags(control, "div"))
                {
                    //They must contain a paragraph tag
                    textType = Enums.INLINE_EDITING_TEXT_TYPE.HtmlWithAtLeastParagraphTag;
                }

                control.SetAttribute("data-cms-edit-texttype", ((int)textType).ToString());

                object objOriginalVal = ReflectionUtil.GetPropertyValueByName(item, property.Name);
                //Replaced the one below with one above, as it was tying the value with the object when property name is
                //enough. Franco - 2012-06-25
                //object objOriginalVal = property.GetValue(item, null);
                if (objOriginalVal != null)
                {
                    originalValue = objOriginalVal.ToString();
                }
                //if (!string.IsNullOrEmpty(originalValue) && replacementTags != null && replacementTags.Count() > 0)
                if (originalValue != null && (originalValue.Contains("[") || originalValue.Contains("]")))
                {
                    //Contains replacement tag
                    control.SetAttribute("data-cms-edit-original", originalValue);
                }
            }
        }
      
    }
}