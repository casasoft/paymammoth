﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BusinessLogic_v3.Classes.DbObjects;
using System.Web.UI.WebControls;
using CS.General_v3.Util;
using System.Reflection;

namespace CS.WebComponentsGeneralV3.Code.Cms.Classes
{

    
    public class CMSInlineEditingItemWithType 
    {
        public static CMSInlineEditingItemWithType Create<T>(

          T item,
          System.Linq.Expressions.Expression<Func<T, object>> selector,
             IAttributeAccessor control,
          string originalValue = null,
          Dictionary<string, string> replacementTags = null) where T : IBaseDbObject
        {
            var propInfo = ReflectionUtil.GetPropertyBySelector(selector);
            CMSInlineEditingItemWithType inlineEditing = new CMSInlineEditingItemWithType(control, item, propInfo, originalValue, replacementTags);
            return inlineEditing;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="item"></param>
        /// <param name="type"></param>
        /// <param name="originalValue"></param>
        /// <param name="replacementTags">If this is present, then original value is passed on</param>
        public CMSInlineEditingItemWithType(IAttributeAccessor control, IBaseDbObject item, PropertyInfo property, string originalValue = null, Dictionary<string, string> replacementTags = null)
        {
            if (BusinessLogic_v3.Modules.CmsUserModule.CmsUserSessionLogin.Instance.IsLoggedIn)
            {

                string itemFullClassName = item.GetType().FullName;
                string propertyName = property.Name;


                control.SetAttribute("data-cms-edit-id", item.ID.ToString());
                control.SetAttribute("data-cms-edit-type", itemFullClassName);
                control.SetAttribute("data-cms-edit-propName", propertyName);
                if (!string.IsNullOrEmpty(originalValue) && replacementTags != null && replacementTags.Count() > 0)
                {
                    control.SetAttribute("data-cms-edit-original", originalValue);
                }
            }
        }
      
    }
}