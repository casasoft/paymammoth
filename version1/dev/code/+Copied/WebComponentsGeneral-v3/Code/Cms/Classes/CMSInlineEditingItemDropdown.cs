﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BusinessLogic_v3.Classes.DbObjects;
using System.Web.UI.WebControls;
using CS.General_v3.Util;
using System.Reflection;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Modules.ContentTextModule;

namespace CS.WebComponentsGeneralV3.Code.Cms.Classes
{

    
    public class CMSInlineEditingItemDropdown 
    {
        /// <summary>
        /// Create an inline editable item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item to update inline</param>
        /// <param name="selector">The selector the property of the item, e.g. x => x.Title</param>
        /// <param name="control">The control to attach the inline editing with</param>
        /// <param name="originalValue">The initial original value of the text (without content tags replaced)</param>
        /// <param name="replacementTags">The replacement tags to replace text with</param>
        /// <returns></returns>
        public static CMSInlineEditingItemDropdown Create(
             MyDropDownList control)
        {
            System.Linq.Expressions.Expression<Func<IContentTextBase, object>> selector = x => x._Content_ForEditing;
            var propInfo = ReflectionUtil.GetPropertyBySelector(selector);
            CMSInlineEditingItemDropdown inlineEditing = new CMSInlineEditingItemDropdown(control, propInfo);
            return inlineEditing;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="item"></param>
        /// <param name="type"></param>
        /// <param name="originalValue"></param>
        /// <param name="replacementTags">If this is present, then original value is passed on</param>
        public CMSInlineEditingItemDropdown(MyDropDownList control, PropertyInfo property)
        {
            if (BusinessLogic_v3.Modules.CmsUserModule.CmsUserSessionLogin.Instance.IsLoggedIn)
            {
                
                foreach (var item in control.Functionality.Items)
                {
                    if (item.LabelContentText != null)
                    {
                        item.Control.Attributes["data-cms-edit-option-id"] = item.LabelContentText.Data.ID.ToString();
                        item.Control.Attributes["data-cms-edit-option-original"] = item.LabelContentText.GetContent();
                    }
                }
                string itemFullClassName = typeof(ContentTextBase).FullName;
                string propertyName = property.Name;
                control.Attributes["data-cms-edit-texttype"] = ((int)BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText).ToString();
                control.Attributes["data-cms-edit-type"] = itemFullClassName;
                control.Attributes["data-cms-edit-propName"] =  propertyName;

                control.Attributes.Add("data-cms-edit-dropdown", "");
                
            }
        }
      
    }
}