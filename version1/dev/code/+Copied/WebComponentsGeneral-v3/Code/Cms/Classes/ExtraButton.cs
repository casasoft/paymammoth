﻿namespace CS.WebComponentsGeneralV3.Code.Cms.Classes
{
    using System;

    public class ExtraButton
    {
        public string Title { get; set; }
        public string ImageUrl_Up { get; set; }
        public string ImageUrl_Over { get; set; }
        public string Href { get; set; }
        public string CssClass { get; set; }
        internal EventHandler clickHandler;
        public string ConfirmMessageOnClick { get; set; }
        public object Tag { get; set; }
        public event EventHandler Click
        {
            add { clickHandler += value; }
            remove { clickHandler -= value; }
        }
        


    }
        
}
