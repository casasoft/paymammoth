﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting.MemberBalance;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using BusinessLogic_v3.Classes.Cms.System;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting.SalesReport;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects
{
  

    public abstract class CmsSystem  : CmsSystemBase
    {

        

        public IEnumerable<MainMenuItem> GetCustomAdditionalMenuItems()
        {
            List<MainMenuItem> list = new List<MainMenuItem>();
            addCustomAdditionalMenuItems(list);
            return list;
        }

        protected virtual void addCustomAdditionalMenuItems(List<MainMenuItem> menuItems)
        {
            var user = CmsUtil.GetLoggedInUser();
            if (user != null && user.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft))
            {
                string url = CmsRoutesMapper.Instance.GetLogFiles_ListingPage();
                menuItems.Add(new MainMenuItem("Logs", url, EnumsCms.IMAGE_ICON.Listing, null, priority: int.MaxValue));
            }

            if (CmsReports.CheckIfLoggedInUserHasAccessToAtLeastOneReport())
            {
                string url = Routing.Reporting.ReportingRoutes.GetListingUrl();
                menuItems.Add(new MainMenuItem("Reports", url, EnumsCms.IMAGE_ICON.Listing, null, priority: int.MaxValue));
            }
        }

        public new static CmsSystem Instance
        {

            get { return (CmsSystem) CmsSystemBase.Instance; }
        }

        protected virtual void initialiseCmsTypes()
        {
           
            //RegisterItemType(ProductFactory.Instance);
            //RegisterItemType(StoreFactory.Instance);
        }


        public bool ShowCasaSoftLogos { get; set; }
        public string CmsTitle { get; set; }

        //private CS.General_v3.Classes.Factories.FactoryController<ICmsItemFactory> _CmsItemTypes = null;
        public List<ICmsItemFactory> GetRegisteredCmsFactories()
        {
            List<ICmsItemFactory> list = new List<ICmsItemFactory>();
            list.AddRange(BusinessLogic_v3.Classes.Cms.Factories.CmsFactoriesController.Instance.GetAllFactories().Cast<ICmsItemFactory>());
            return list;
        }


        ///// <summary>
        ///// Returns all CMS Properites of all registered types, which are linked to an item of the given type of factory
        ///// </summary>
        ///// <param name="itemInfo"></param>
        ///// <returns></returns>
        //public IEnumerable<CmsPropertyInfo> GetCmsPropertiesLinkedToFactory2(ICmsItemFactory itemInfo)
        //{
        //    List<CmsPropertyInfo> list = new List<CmsPropertyInfo>();
        //    foreach (var item in GetRegisteredCmsFactories())
        //    {
        //        foreach (var property in item.GetPropertyInfos(null))
        //        {
        //            if (property is CmsPropertyInfo)
        //            {
        //                CmsPropertyInfo p = (CmsPropertyInfo)property;

        //                if (p.LinkedWithInCms)
        //                {
        //                    if (itemInfo.CheckIfFactoryIsForType(p.PropertyInfo.PropertyType))
        //                    {
        //                        list.Add(p);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return list;
        //}
        /// <summary>
        /// Returns all CMS Properites of all registered types, which are linked to an item of the given type of factory
        /// </summary>
        /// <param name="itemInfo"></param>
        /// <returns></returns>
        public IEnumerable<CmsCollectionInfo> GetCmsPropertiesLinkedToFactory(ICmsItemFactory itemInfo)
        {
            List<CmsCollectionInfo> list = new List<CmsCollectionInfo>();
            var emptyItem = itemInfo.GetEmptyCmsItemInfo();
            foreach (var p in emptyItem.GetCmsProperties())
            {
                if (p is CmsCollectionInfo && p.IsUsedInProject())
                {
                    CmsCollectionInfo cmsCollInfo = (CmsCollectionInfo)p;
                    if (cmsCollInfo.ShowLinkInCms)
                    {
                        list.Add(cmsCollInfo);
                    }
                }
            }
            return list;
            //foreach (var item in GetRegisteredCmsFactories())
            //{
            //    foreach (var property in item.GetPropertyInfos(null))
            //    {
            //        if (property is CmsPropertyInfo)
            //        {
            //            CmsPropertyInfo p = (CmsPropertyInfo)property;

            //            if (p.LinkedWithInCms)
            //            {
            //                if (itemInfo.CheckIfFactoryIsForType( p.PropertyInfo.PropertyType))
            //                {
            //                    list.Add(p);
            //                }
            //            }
            //        }
            //    }
            //}
            //return list;
        }

        public void RemoveAnyQuerystringParametersRelatedToCms2(CS.General_v3.Classes.URL.URLClass url)
        {
            //foreach (var factory in this.GetRegisteredCmsFactories())
            //{
            //    url[factory.QueryStringParamID] = null;
            //    foreach (var p in factory.GetPropertyInfos(null))
            //    {
            //        if (p.LinkedWithInCms)
            //        {
            //            url[p.GetLinkedQuerystringParamName()] = null;
            //        }
            //    }

            //}

        }

        public void RegisterItemType(ICmsItemFactory cmsItemType)
        {
            BusinessLogic_v3.Classes.Cms.Factories.CmsFactoriesController.Instance.AddFactory(cmsItemType);
            
            /*
            if (_InitialiseRegisteredTypes)
            {
                cmsItemType.InitCmsParameters();
                //_CmsItemTypes.AddFactory(cmsItemType);
            }
            else
            {
                throw new InvalidOperationException("You cannot initialise a factory when they have already been initialised");
            }*/
            
        }

        public void FinishedInitialisingFactories()
        {
            BusinessLogic_v3.Classes.Cms.Factories.CmsFactoriesController.Instance.FinishedInitialisingFactories();
            
        }

        /*
        private bool _InitialiseRegisteredTypes = false;
        public void InitialiseRegisteredTypes()
        {
            if (!_InitialiseRegisteredTypes)
            {
                foreach (var t in this.GetRegisteredCmsFactories())
                {
                    t.InitCmsParameters();
                }
                _InitialiseRegisteredTypes = true;
            }
        }
        */
        public string CmsRootPath { get; set; }
        /// <summary>
        /// This ensures that the returend path ends with a slash
        /// </summary>
        /// <returns></returns>
        public string GetCmsRoot()
        {


            string s = CmsRootPath;
            if (!s.EndsWith("/"))
                s += "/";
            return s;
        }


        protected CmsSystem()
        {
            this.ShowCasaSoftLogos = true;
            this.CmsRootPath = "/cms/";
            this.CmsReports = new CmsReportManager();
            
            
        //    this.MainMenu2Items = new List<MainMenuItem>();
        }

        public void OnApplicationStart()
        {
            this.initialiseCmsTypes();
            this.initialiseCmsRoutes();
            this.initialiseCmsReports();
        }

        protected virtual void initialiseCmsReports()
        {
            this.CmsReports.AddReport<MemberBalanceReportGenerator>();
            this.CmsReports.AddReport<SalesReportGenerator>();
        }


        protected virtual void initialiseCmsRoutes()
        {
            
            
            CmsRoutesMapper mapper = CmsRoutesMapper.Instance;
            
            mapper.MapRoutes();
        }

        // public List<MainMenuItem> MainMenu2Items { get; private set; }

        public bool CMS_AutoLoginFromDevMachines
        {
            get
            {
                return true;
                //return CS.General_v3.Settings.getset.GetSetting<bool>(CS.General_v3.Enums.SETTINGS_ENUM.CMS_AutoLoginFromDevMachines);
            }
        }
        public ICmsItemFactory GetFactoryForType(Type t)
        {
            return (ICmsItemFactory) BusinessLogic_v3.Classes.Cms.Factories.CmsFactoriesController.Instance.GetFactoryForType(t);
            

            //foreach (var factory in this.GetRegisteredCmsFactories())
            //{
            //    if (factory.CheckIfFactoryIsForType(t))
            //    {
            //        return factory;
            //    }
            //}
            //return null;
        }
        public ICmsItemFactory GetCmsFactoryByTitle(string title)
        {
            var allFactories = this.GetRegisteredCmsFactories();
            foreach (var f in allFactories)
            {
                if (string.Compare(title, f.Name,true) ==0)
                {
                    return f;
                }
            }
            return null;
        }



    
        public override bool  CheckIfCurrentRequestInCms()
        {
            bool inCms = false;
            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
            {
                string url = CS.General_v3.Util.PageUtil.GetCurrentRequest().Url.PathAndQuery;
                if (url.ToLower().StartsWith(CmsUrls.CMS_FOLDER.ToLower()))
                {
                    inCms = true;
                }
            }
            return inCms;
            
        }

        public CmsReportManager CmsReports { get; set; }
    

    }
}