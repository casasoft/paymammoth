﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3.Code.Cms
{
    public static class CmsConstants
    {
        public const string QUERYSTRING_LASTADDED = "LastAddedItem";

        public const string ROUTING_PARAM_TITLE = "Title";
        public const string ROUTING_PARAM_ID = "ID";

        public const string CSS_FIELD_TINY= "fieldTiny";
        public const string CSS_FIELD_SMALL = "fieldSmall";
        public const string CSS_FIELD_NORMAL = "fieldNormal";
        public const string CSS_FIELD_LARGE = "fieldLarge";
        public const string CSS_FIELD_LARGER = "fieldLarger";
        public const string CSS_FIELD_EXTRALARGE = "fieldExtraLarge";
    }
}