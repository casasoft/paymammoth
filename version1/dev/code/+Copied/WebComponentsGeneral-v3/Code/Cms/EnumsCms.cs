﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using System.Collections;
using System.ComponentModel;
using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace CS.WebComponentsGeneralV3.Code.Cms
{
    

    public static class EnumsCms
    {

        public enum MULTICHOICE_DISPLAY_TYPE
        {
            CheckboxList,
            Listbox

        }
        public enum CMS_DATA_TYPE
        {
            Bool = 50,
            BoolNullable = 100,
            DateTime = 150,
            DateTimeNullable = 200,
            Double = 250,
            DoubleNullable = 300,
            Integer = 350,
            IntegerMultipleChoice = 400,
            IntegerNullable = 450,
            String = 500,
            FileUpload = 550,
            FileUploadWithMediaItem = 600,
            LongNullable = 650,
            Long = 700,
            
            Enumeration = 750,
            LinkedObject = 800,
            Collection = 850,
            MediaItem = 900            

        }

        public enum CMS_CSS_CLASSES
        {
            [Description("fieldSmall")]
            FieldSmall,
            [Description("fieldNormal")]
            FieldNormal,
            [Description("fieldLarge")]
            FieldLarge,
            [Description("fieldEditInPage")]
            FieldEditInPage,
            [Description("fieldEditInSearch")]
            FieldEditInSearch,
            [Description("fieldEditInListing")]
            FieldEditInListing,

            [Description("listingColumn")]
            ListingColumnCell,
            [Description("listingHeader")]
            ListingColumnHeader

        }
        public static string GetCssClass(CMS_CSS_CLASSES cssClass)
        {
            return CS.General_v3.Util.EnumUtils.StringValueOf(cssClass);
        }

        public static CMS_DATA_TYPE GetDataTypeFromType(Type type)
        {
            Type genericType = null;
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                genericType = type.GetGenericArguments()[0];
            }

            if (type.IsAssignableFrom(typeof(bool?)))
                return CMS_DATA_TYPE.BoolNullable;
            else if (type.IsAssignableFrom(typeof(bool)))
                return CMS_DATA_TYPE.Bool;
            else if (type.IsEnum || (genericType != null && genericType.IsEnum))
                return CMS_DATA_TYPE.Enumeration;
            else if (type.IsAssignableFrom(typeof(long?)))
                return CMS_DATA_TYPE.LongNullable;
            else if (type.IsAssignableFrom(typeof(long)))
                return CMS_DATA_TYPE.Long;
            else if (type.IsAssignableFrom(typeof(int?)))
                return CMS_DATA_TYPE.IntegerNullable;
            else if (type.IsAssignableFrom(typeof(int)))
                return CMS_DATA_TYPE.Integer;
            else if (type.IsAssignableFrom(typeof(double?)))
                return CMS_DATA_TYPE.DoubleNullable;
            else if (type.IsAssignableFrom(typeof(double)))
                return CMS_DATA_TYPE.Double;
            else if (type.IsAssignableFrom(typeof(DateTime?)))
                return CMS_DATA_TYPE.DateTimeNullable;
            else if (type.IsAssignableFrom(typeof(DateTime)))
                return CMS_DATA_TYPE.DateTime;
            else if (type.IsAssignableFrom(typeof(string)))
                return CMS_DATA_TYPE.String;
            else if (type.IsAssignableFrom(typeof(IBaseDbObject)) || typeof(IBaseDbObject).IsAssignableFrom(type))
                return CMS_DATA_TYPE.LinkedObject;
            else if (type.IsAssignableFrom(typeof(IEnumerable)) || typeof(IEnumerable).IsAssignableFrom(type))
                return CMS_DATA_TYPE.Collection;
            else
                throw new InvalidOperationException("invalid data type for cms");


        }

        

        public enum SECTION_RENDER_TYPE
        {
            Listing = 50,
            Tree = 100
        }
        public enum SECTION_TYPE
        {
            ListingPage = 100, 
            EditPage = 50,
            SearchInListing = 150
        }
        public enum IMAGE_ICON
        {
            Advert,
            Add,
            Edit,
            Flags,
            Padlock,
            Tools,
            Brand,
            GoBack,
            Delete,
            Track,
            Logout,
            User,
            ReOrder,
            Award,
            Download,
            Search,
            Success,
            Error,
            Member,
            Member2,
            Email,
            Calendar,
            Category,
            MailForward,
            Settings,
            PageEdit,
            Listing,
            Language,
            Music,
            News,
            Import,
            Tools2,
            ShortNotes,
            Book,
            Export,
            Product,
            PDF,
            UnorderedList,
            EuroSign,
            Update,
            Speaker,
            DeeJay,
            Music2,
            News2,
            Photos,
            Link,
            Generate,
            Question,
            PriceTag,
            EmailEdit,
            BagOfMoney,
            Cart,
            Present_Gift,
            Tag,
            Label1,
            Label2,
            Bookshelf,
            FeaturedIcon,
            Star,
            Shipping,
            Ruler,
            House,
            Graph1,
            Graph2,
            BarChart,
            PieChart,
            EditView,
            Audit,
            Save



        }




        

        /// <summary>
        /// Returns the icon image URL according to image Folder
        /// </summary>
        /// <param name="image"></param>
        /// <param name="imageFolder">Specify image folder path in CMS e.g. .http://css-tricks.com/examples/AnythingSlider/images/</param>
        /// <returns></returns>
        public static string GetImageFilePath(IMAGE_ICON image)
        {
            string imgFileName = "";
            switch (image)
            {
                case IMAGE_ICON.Brand: imgFileName = "icon_brand.png"; break;
                case IMAGE_ICON.Present_Gift: imgFileName = "icon_present.png"; break;
                case IMAGE_ICON.BagOfMoney: imgFileName = "icon_money.png"; break;
                case IMAGE_ICON.Cart: imgFileName = "icon_cart.png"; break;
                case IMAGE_ICON.EmailEdit: imgFileName = "icon_email_edit.png"; break;
                case IMAGE_ICON.Add: imgFileName = "icon_add_up.png"; break;
                case IMAGE_ICON.Advert: imgFileName = "icon_advert.png"; break;
                case IMAGE_ICON.Book: imgFileName = "icon_contentPage.png"; break;
                case IMAGE_ICON.ShortNotes: imgFileName = "icon_shortNotes.gif"; break;
                case IMAGE_ICON.PriceTag: imgFileName = "icon_pricetag.png"; break;
                case IMAGE_ICON.Language: imgFileName = "icon_language.png"; break;
                case IMAGE_ICON.Edit: imgFileName = "icon_edit_up.png"; break;
                case IMAGE_ICON.GoBack: imgFileName = "icon_edit_up.png"; break;
                case IMAGE_ICON.Delete: imgFileName = "icon_delete_up.png"; break;
                case IMAGE_ICON.Track: imgFileName = "icon_track_up.png"; break;
                case IMAGE_ICON.Logout: imgFileName = "icon_logout_up.png"; break;
                case IMAGE_ICON.Calendar: imgFileName = "icon_calendar.png"; break;
                case IMAGE_ICON.User: imgFileName = "icon_user_up.png"; break;
                case IMAGE_ICON.ReOrder: imgFileName = "icon_reorder_up.png"; break;
                case IMAGE_ICON.Search: imgFileName = "icon_search_up.png"; break;
                case IMAGE_ICON.Award: imgFileName = "icon_award.png"; break;
                case IMAGE_ICON.Download: imgFileName = "icon_download.png"; break;
                case IMAGE_ICON.Success: imgFileName = "icon_success_up.png"; break;
                case IMAGE_ICON.Error: imgFileName = "icon_error_up.png"; break;
                case IMAGE_ICON.Product: imgFileName = "icon_product.gif"; break;
                case IMAGE_ICON.Category: imgFileName = "icon_category.gif"; break;
                case IMAGE_ICON.Question: imgFileName = "icon_question.png"; break;
                case IMAGE_ICON.Settings: imgFileName = "icon_settings.png"; break;
                case IMAGE_ICON.EuroSign: imgFileName = "icon_euro.png"; break;
                case IMAGE_ICON.UnorderedList: imgFileName = "icon_unorderedlist.gif"; break;
                case IMAGE_ICON.PDF: imgFileName = "icon_pdf.gif"; break;
                case IMAGE_ICON.Flags: imgFileName = "icon_flags.png"; break;
                case IMAGE_ICON.Tools: imgFileName = "icon_settings.png"; break;
                case IMAGE_ICON.Tools2: imgFileName = "icon_settings2.png"; break;
                case IMAGE_ICON.Padlock: imgFileName = "icon_padlock.gif"; break;

                case IMAGE_ICON.Member: imgFileName = "icon_member_up.png"; break;
                case IMAGE_ICON.Member2: imgFileName = "icon_member2_up.png"; break;
                case IMAGE_ICON.Email: imgFileName = "icon_email_up.png"; break;
                case IMAGE_ICON.MailForward: imgFileName = "icon_mailforward_up.png"; break;
                case IMAGE_ICON.PageEdit: imgFileName = "icon_pageedit_up.gif"; break;
                case IMAGE_ICON.Listing: imgFileName = "icon_listing_up.gif"; break;
                case IMAGE_ICON.Music: imgFileName = "icon_music_up.png"; break;
                case IMAGE_ICON.News: imgFileName = "icon_news_up.png"; break;
                case IMAGE_ICON.Import: imgFileName = "icon_import_up.gif"; break;
                case IMAGE_ICON.Export: imgFileName = "icon_export_up.gif"; break;

                case IMAGE_ICON.Update: imgFileName = "icon_update_up.gif"; break;
                case IMAGE_ICON.Speaker: imgFileName = "icon_speaker_up.png"; break;
                case IMAGE_ICON.DeeJay: imgFileName = "icon_deejay_up.png"; break;
                case IMAGE_ICON.Music2: imgFileName = "icon_music2_up.png"; break;
                case IMAGE_ICON.News2: imgFileName = "icon_news2_up.png"; break;
                case IMAGE_ICON.Link: imgFileName = "icon_link_up.gif"; break;
                case IMAGE_ICON.Photos: imgFileName = "icon_photos_up.png"; break;
                case IMAGE_ICON.Generate: imgFileName = "icon_generate_up.gif"; break;
                case IMAGE_ICON.Tag:
                case IMAGE_ICON.Label1:
                case IMAGE_ICON.Label2:
                case IMAGE_ICON.Bookshelf:
                case IMAGE_ICON.FeaturedIcon:
                case IMAGE_ICON.Star:
                case IMAGE_ICON.Shipping:
                case IMAGE_ICON.Ruler:
                case IMAGE_ICON.Graph1:
                case IMAGE_ICON.Graph2:
                case IMAGE_ICON.BarChart:
                case IMAGE_ICON.PieChart:
                case IMAGE_ICON.EditView:
                case IMAGE_ICON.Audit:
                case IMAGE_ICON.House:
                case IMAGE_ICON.Save:
                    {
                        imgFileName = "icon_" + CS.General_v3.Util.EnumUtils.StringValueOf(image) + ".png";
                        break;
                    }
            }


            string rootFolder = Settings.CMSSettings.Images.GetImageRootFolder();
            //rootFolder += "_common/images/";
            string imageURL = rootFolder + imgFileName;


            return imageURL;
        }


        public static string GetImageFilePath(IMAGE_ICON image, out string rolloverImageURL)
        {
            string imgFileName = GetImageFilePath(image);
            rolloverImageURL = imgFileName.Replace("up.png", "over.png");
            return imgFileName;
        }

        public static string GetButtonCssClass(IMAGE_ICON image)
        {
            return "button-" + image.ToString().ToLower();
        }
    }
}
