﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms.Settings
{
    public static class CMSSettings
    {
        public static class Images
        {
            public static string GetImageRootFolder()
            {
                return "/_common/static/images/cms/v4-1/";
            }
        }
        public static class CMS
        {
            public static string GetRootFolder()
            {
                return "/cms/";
            }
        }


    }
}
