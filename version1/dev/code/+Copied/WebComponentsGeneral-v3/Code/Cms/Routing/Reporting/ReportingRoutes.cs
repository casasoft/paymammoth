﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting;

namespace CS.WebComponentsGeneralV3.Code.Cms.Routing.Reporting
{
    public static class ReportingRoutes
    {
        public const string Route_Reporting_Listing = "Cms-Reporting-Listing";
        public const string Route_Reporting_Report = "Cms-Reporting-Report";
        public const string Param_Identifier = "Identifier";
        public static void MapRoute()
        {
            {//reports
                {
                    CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_Reporting_Listing, CmsSystem.Instance.CmsRootPath + "Reporting/",
                        "_ComponentsGeneric/CMS/Pages/Reports/default.aspx", null, null);
                    CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_Reporting_Report, CmsSystem.Instance.CmsRootPath + "Reporting/{" + Param_Identifier + "}/",
                        "_ComponentsGeneric/CMS/Pages/Reports/report.aspx", null, null);

                }
            }
        }
        public static string GetListingUrl()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(Route_Reporting_Listing,null);
        }
        public static string GetReportUrl(ICmsReportGenerator report)
        {
            string identifier = report.ReportIdentifier;
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(Route_Reporting_Report, new General_v3.Classes.Routing.MyRouteValueDictionary(Param_Identifier, identifier));
        }

        public static ICmsReportGenerator GetCmsReportFromRouteParam()
        {
            string identifier = CS.General_v3.Util.PageUtil.GetRouteDataVariable(Param_Identifier);
            return CmsSystem.Instance.CmsReports.GetReportByIdentifier(identifier);

        }

    }
}