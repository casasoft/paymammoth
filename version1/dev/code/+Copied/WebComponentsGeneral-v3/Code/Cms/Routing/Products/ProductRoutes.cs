﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
namespace CS.WebComponentsGeneralV3.Code.Cms.Routing.Products
{
    public static class ProductRoutes
    {
        public const string Route_DataImport= "Cms-Products-DataImport";
        

        public static void MapRoute()
        {
            {
                {
                    CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_DataImport, CmsSystem.Instance.CmsRootPath + "Products/Import",
                        "_ComponentsGeneric/CMS/Pages/Products/import.aspx", null, null);
                    

                }
            }
        }
        public static string GetDataImportUrl()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(Route_DataImport, null);
        }

        
    }
}