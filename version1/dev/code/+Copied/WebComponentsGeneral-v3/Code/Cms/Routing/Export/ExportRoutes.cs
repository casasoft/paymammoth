﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.General_v3.Classes.Routing;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Code.Cms.Routing.Exporting
{
    public static class ExportRoutes
    {
        public const string Route_Reporting_ChooseFields = "Cms-Item-Export-ChooseFields";
        public const string Param_Factory_Title = CmsConstants.ROUTING_PARAM_TITLE;
        public const string Param_Search_SortBy = "_search_SortBy";
        public const string Param_Search_SortType = "_search_SortType";
        public const string Param_Search_FieldPrepend = "_sf_";




        public static void MapRoute()
        {
            {//reports
                {
                    CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_Reporting_ChooseFields, CmsSystem.Instance.CmsRootPath + "{" + Param_Factory_Title + "}/Export",
                        "_ComponentsGeneric/CMS/Pages/Export/chooseFields.aspx", null, null);

                }
            }
            return;
        }

        public static string GetSearchFieldQuerystringId(CmsFieldBase field)
        {
            return Param_Search_FieldPrepend + field.GetPropertyIdentifier();
        }
        public static string GetSearchFieldValue(CmsFieldBase field)
        {
            string id = GetSearchFieldQuerystringId(field);
            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(id);
        }

        public static string GetExportUrl(ICmsItemFactory factory, string sortBy, CS.General_v3.Enums.SORT_TYPE sortType, IEnumerable<CmsPropertySearchInfo> searchFields)
        {
            string title = factory.GetTitleForRoutingFolder();
            string url = CS.General_v3.Util.RoutingUtil.GetRouteUrl(Route_Reporting_ChooseFields, new MyRouteValueDictionary(Param_Factory_Title, title));

            CS.General_v3.Classes.URL.URLClass urlClass = new General_v3.Classes.URL.URLClass(url);
            urlClass[Param_Search_SortBy] = sortBy;
            urlClass[Param_Search_SortType] = sortType;

            foreach (var searchField in searchFields)
            {
                string id = GetSearchFieldQuerystringId(searchField.PropertyInfo);
                if (searchField.HasSearchValue())
                {
                    urlClass[id] = searchField.GetSearchValue();
                }

            }


            return urlClass.ToString();
        }
        


    }
}