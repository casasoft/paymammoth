﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Configuration;
using System.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Controls.WebControls.Common;

using CS.WebComponentsGeneralV3.Code.Cms.UserControls;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls;

using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;


namespace CS.WebComponentsGeneralV3.Code.Cms.Pages
{

    public abstract class BaseMasterPage : BaseMasterPageBase
    {
        public static string QUERYSTRING_PARAM_CULTUREID = "Culture";

        public const string QUERYSTRING_PARAM_CUSTOMACCESS = "_CustomAccess";
        

        public string Title
        {
            get
            {
                return CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Others_WebsiteName);
                
            }
        }



        public new class FUNCTIONALITY : BaseMasterPageBase.FUNCTIONALITY
        {
             public long? CurrentCmsCultureID
             {
                 get
                 {
                     return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long?>(QUERYSTRING_PARAM_CULTUREID);
                 }
             }
             public long GetCurrentCultureID()
             {
                 long id = 0;
                 if (CurrentCmsCultureID.HasValue)
                     id = CurrentCmsCultureID.Value;
                 if (id == 0)
                 {

                     string code = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Culture_DefaultCultureCode);

                     id = CultureDetailsBaseFactory.Instance.GetCultureByCode(code).ID;
                 }
                 return id;
             }
            
            //private string currentLanguageCookie
            //{
            //    get
            //    {
            //        //string s = CS.General_v3.Util.Page.GetBaseURL();
            //        string s = "";
            //        s += "_CMS_CurrentLanguage";
            //        return s;
            //    }
            ////}
            //public ICultureDetails GetCurrentLanguage()
            //{
               
            //        string code = null;

            //        ICultureDetails lang = null;
            //        if (CS.General_v3.Util.PageUtil.CheckSessionExistsAtContext())
            //        {
            //            code = (string)CS.General_v3.Util.PageUtil.GetSessionObject("CMS_Current_Language");
            //        }
            //        if (code == null)
            //        {
                        
            //            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
            //            {
            //                code = CS.General_v3.Util.PageUtil.GetCookie(currentLanguageCookie);
            //            }
                        

            //        }
            //        if (!string.IsNullOrEmpty(code))
            //        {

            //            lang = BusinessLogic_v3.Modules.Factories.CultureDetailsFactory.GetCultureByCode(code);
            //        }
            //        if (lang == null)
            //        {
            //            lang = BusinessLogic_v3.Modules.Factories.CultureDetailsFactory.GetDefaultCulture();
            //            SetCurrentLanguage(lang);
            //        }
            //        return lang;
                
               
            //}

            //public void SetCurrentLanguage(ICultureDetails lang)
            //{
            //    System.Diagnostics.Contracts.Contract.Requires(lang != null);
            //    string code = lang.LanguageISOCode;
            //    if (CS.General_v3.Util.PageUtil.CheckSessionExistsAtContext())
            //    {
            //        CS.General_v3.Util.PageUtil.SetSessionObject("CMS_Current_Language", code);
            //    }
            //    if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext() && CS.General_v3.Util.PageUtil.CheckResponseExistsAtContext())
            //    {
            //        CS.General_v3.Util.PageUtil.SetCookie(currentLanguageCookie, code);
            //    }
            //    //this is updated, but will be re-set once the preRenderComplete of this page is complete
            //    //BusinessLogic.v1.v1.Languages.Language_Base.CurrentLanguage = value;
            //}
            public bool ShowCasaSoftLogos { get; set; }
            //public Hashtable SessionDataResettable
            //{
            //    get
            //    {
            //        Hashtable nv = (Hashtable)S3ession["__SessionDataResettable"];
            //        if (nv == null)
            //        {
            //            nv = new Hashtable();

            //            SessionDataResettable = nv;
            //        }
                    
            //        return nv;
            //    }
            //    private set
            //    {
            //        S2ession["__SessionDataResettable"] = value;
            //    }
            //}

            private BaseMasterPage _baseMasterPage = null;
            //public System.Web.SessionState.HttpSessionState Session
            //{
            //    get { return _baseMasterPage.Session; }
            //}
            public FUNCTIONALITY(BaseMasterPage pg) : base(pg)
            {
                _baseMasterPage = pg;
                this.ShowCasaSoftLogos = true;
                ExtraCSSFile = "/css/cms.css";
                this.GoBackText = "Go Back";
                
            }
            public string DojoLocalURL { get; set; }
            public string ExtraCSSFile { get; set; }
            public bool ShowLanguageSelect { get; set; }

            public CmsUserSessionLogin MemberLogin
            {
                get
                {
                    return CmsUserSessionLogin.Instance;
                }
            }
            public string GoBackText { get; set; }
            public string GoBackURL { get; set; }
            //public IEnumerable<BusinessLogic.v1.v1.Languages.Language_Base> Languages { get; set; }
            public MyImage Logo
            {
                get
                {
                    _baseMasterPage.initControls();
                    return _baseMasterPage._logo;


                }
            }

            public TopButtons TopButtons
            {
                get
                {
                    _baseMasterPage.initControls();
                    return _baseMasterPage._topButtons;
                }
            }
            public MainMenu MainMenu
            {
                get
                {
                    
                    _baseMasterPage.initControls();
                    return _baseMasterPage._mainMenu;
                }
            }



            /// <summary>
            /// Set the page title of the CMS
            /// </summary>
            /// <param name="titles">A list of sections.  Only pass the sections, e.g. Manage Tracks, Add Track</param>
            public void SetPageTitles(IEnumerable<PageTitle> list)
            {
                var arr = list.ToArray();
                SetPageTitle(arr);
            }
            
            
            
            /// <summary>
            /// Set the page title of the CMS
            /// </summary>
            /// <param name="titles">A list of sections.  Only pass the sections, e.g. Manage Tracks, Add Track</param>
            public void SetPageTitle(params PageTitle[] titles)
            {
                _baseMasterPage.initControls();
                StringBuilder title = new StringBuilder();
                title.Append(this._baseMasterPage.Title + " CMS");
                StringBuilder listingTitle = new StringBuilder();
                
                for (int i = 0; i < titles.Length; i++)
                {
                    if (i > 0)
                    {
                        listingTitle.Append(" | ");
                    }

                    title.Insert(0,titles[i].Title + " | ");
                    listingTitle.Append(titles[i].GetHTML());
                }
                string strTitle = title.ToString();
                _baseMasterPage.Page.Master.Page.Functionality.UpdateTitle(strTitle, false);
                _baseMasterPage.Page.Functionality.UpdateTitle(strTitle, false);
                //_ltlCurrentTitle.Text = titles[titles.Length-1];
                _baseMasterPage._ltlCurrentTitle.Text = listingTitle.ToString();
            }

            public void ShowStatusMessageAndRedirectToPage(OperationResult result, string url)
            {
                ShowStatusMessage(result);
                CS.General_v3.Util.PageUtil.RedirectPage(url);
            }
            public void ShowStatusMessageAndRedirectToPage(string message, CS.General_v3.Enums.STATUS_MSG_TYPE type, string url)
            {
                ShowStatusMessage(message, type);
                CS.General_v3.Util.PageUtil.RedirectPage(url);
            }
            
            public void ShowStatusMessageAndRedirectToSamePage(OperationResult result)
            {
                ShowStatusMessage(result);
                CS.General_v3.Classes.URL.URLClass.RedirectToSamePage();
            }
            public void ShowStatusMessageAndRedirectToSamePage(string message, CS.General_v3.Enums.STATUS_MSG_TYPE type)
            {
                ShowStatusMessage(message, type);
                CS.General_v3.Classes.URL.URLClass.RedirectToSamePage();

            }
            public void ShowStatusMessageAndTransferToSamePage(OperationResult result)
            {
                ShowStatusMessage(result);
                CS.General_v3.Classes.URL.URLClass.TransferToSamePage();
            }
            public void ShowStatusMessageAndTransferToSamePage(string message, CS.General_v3.Enums.STATUS_MSG_TYPE type)
            {
                ShowStatusMessage(message, type);
                CS.General_v3.Classes.URL.URLClass.TransferToSamePage();

            }
            public void ShowStatusMessage(OperationResult result)
            {
                

                CmsItemInfo.ShowStatusMessageInCMS(result);
            }
            public void ShowStatusMessage(string message, CS.General_v3.Enums.STATUS_MSG_TYPE type)
            {
                CmsItemInfo.ShowStatusMessageInCMS(message, type);
            }
            public void ShowIPRestricedError()
            {
                this.ShowStatusMessageAndRedirectToPage("Your IP address '" + CS.General_v3.Util.PageUtil.GetUserIP() + 
                    "' is restricted to access CMS.  If you should have access, inform administrators to white list your IP address.", CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "/cms/");
            }
            /*
            /// <summary>
            /// 
            /// </summary>
            /// <param name="username"></param>
            /// <param name="redirectURL">The redirecting URL IF there is no previously stored last logged in URL</param>
            public void Login(CS.General_v3.Classes.Login.IUser user, string redirectURL)
            {
                LoggedMember = user;
                if (!String.IsNullOrEmpty(_lastLoggedInURL))
                {
                    redirectURL = _lastLoggedInURL;
                }
                if (string.Compare(CS.General_v3.Util.Page.getURLLocation(), redirectURL, true) != 0)
                {
                    Res2ponse.R2edirect(redirectURL);
                }
            }
            */
            //public bool NoAuthenticationRequired { get; set; }
            //internal void resetSessionDataResettable()
            //{
            //   // SessionDataResettable = null;
            //}
        }
        public new FUNCTIONALITY Functionality
        {
            get
            {
                return (FUNCTIONALITY)base.Functionality;
            }
        }

        protected override BaseMasterPageBase.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        
        
        public BaseMasterPage()
        {
            
            


            
        }

        public static string SessionStatusMessage
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObject<string>(SESSION_STATUS_MESSAGE); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject(SESSION_STATUS_MESSAGE, value); }
        }
        public static CS.General_v3.Enums.STATUS_MSG_TYPE? SessionStatusMessageType
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObjectEnum<CS.General_v3.Enums.STATUS_MSG_TYPE>(SESSION_STATUS_MESSAGE_TYPE); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject(SESSION_STATUS_MESSAGE_TYPE, value); }
        }
        internal const string SESSION_STATUS_MESSAGE = "cms_statusmsg";
        internal const string SESSION_STATUS_MESSAGE_TYPE = "cms_statusmsgtype";


        public abstract ButtonsBar ButtonsBar { get; }
        protected MainMenu _mainMenu;
        protected TopButtons _topButtons;
        protected Literal _ltlTitle;
        protected Literal _ltlCurrentTitle;
        protected MyImage _logo;
        
        protected HtmlGenericControl _divGoBack;
        protected MyLink _aGoBack;
        protected HtmlGenericControl _divDate;
        protected HtmlGenericControl _divStatusMessage;
        internal string _lastLoggedInURLWithoutAccessForUser
        {
            get
            {
                
                return Functionality.MemberLogin.lastUrlForCurrentSession;

                
            }
            set
            {
                Functionality.MemberLogin.lastUrlForCurrentSession = value;

            }
        }


        

        
        private void initDate()
        {
            initControls();
            _divDate.InnerHtml = DateTime.Now.ToString("ddd dd MMMM yyyy");
        }
        public new BaseCMSPage Page
        {
            get
            {
                return (BaseCMSPage)base.Page;
            }
        }


        protected override void OnInit(EventArgs e)
        {
            checkForCustomAccessParameter();
            base.OnInit(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            this.Page.PreRenderComplete += new EventHandler(Page_PreRenderComplete);
            initDate();
            

            showDefaultTopButtons();
            initCmsMainMenu();
            base.OnLoad(e);
        }
        protected ICmsUserBase getCmsLoggedUser()
        {

            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            return loggedUser;
        }
        public event Action AddAdditionalMenuItems;
        

        private void initCmsMainMenu()
        {
            List<MainMenuItem> menuList = new List<MainMenuItem>();
            var loggedUser = getCmsLoggedUser();
            if (loggedUser != null)
            {
                var cmsFactories = CmsSystem.Instance.GetRegisteredCmsFactories().ToList();
                for (int i=0; i <cmsFactories.Count;i++)
                {
                    if (i == 53)
                    {
                        int k = 5;
                    }
                    var type = cmsFactories[i];

                    //if (type.GetUsedInProject() &&  type.ShowInCmsMainMenu && type.UserSpecificGeneralInfoInContext.CheckIfUserCanView(loggedUser))
                    //cannot know if type is used in project, if cms factory is not even instantiated
                    if (type.ShowInCmsMainMenu && type.UserSpecificGeneralInfoInContext.CheckIfUserCanView(loggedUser))
                    {
                        //add menu item
                        string listingUrl = type.GetListingUrl(removeAnyQuerystringParameters: true).ToString();


                        MainMenuItem menu = new MainMenuItem(type.TitlePlural, listingUrl, type.CmsImageIcon,
                            type.UserSpecificGeneralInfoInContext.GetAccessRequiredToView(), priority : type.CmsMainMenuPriority);
                        menuList.Add(menu);
                        

                    }
            
                }
            }
            if (AddAdditionalMenuItems != null)
                AddAdditionalMenuItems();
            

            foreach (var m in menuList)
            {
                this._mainMenu.AddItem(m);
            }
            

        }
        private void renderControls()
        {
            _ltlTitle.Text = this.Title;
        }
        //private void checkExtraCSS()
        //{
        //    _linkExtraCSS.Visible = false;
        //    if (!string.IsNullOrEmpty(Functionality.ExtraCSSFile))
        //    {
        //        _linkExtraCSS.Visible = true;
        //        _linkExtraCSS.Href = Functionality.ExtraCSSFile;
        //    }
        //}
        protected void checkGoBackButton()
        {
            this._divGoBack.Visible = (!string.IsNullOrEmpty(Functionality.GoBackURL));
            this._aGoBack.Text = Functionality.GoBackText;
            this._aGoBack.ResolveClientHRef = false;
            this._aGoBack.HRef = Functionality.GoBackURL;
        }

        private void checkButtonsBar()
        {
            ButtonsBar.Visible = (ButtonsBar.HasButtons());

        }
        protected override void OnPreRender(EventArgs e)
        {


            initStatusMessage();
            renderControls();
            if (Functionality.MemberLogin.IsLoggedIn)
            {
                Functionality.MainMenu.RenderMenu();
            }
            checkGoBackButton();
            checkButtonsBar();
            
            base.OnPreRender(e);
            
            
        }

        public override void RenderControl(System.Web.UI.HtmlTextWriter writer)
        {
            base.RenderControl(writer);
            
        }
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            base.Render(writer);
        }
        protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer)
        {
            base.RenderChildren(writer);
        }
        void Page_PreRenderComplete(object sender, EventArgs e)
        {
            //this.Functionality.resetSessionDataResettable();
        }

        private void initStatusMessage()
        {
            _divStatusMessage.Visible = false;
            if (SessionStatusMessage != null)
            {
                string msg = SessionStatusMessage;
                SessionStatusMessage = null;
                //Session.Remove(SESSION_STATUS_MESSAGE);
                //Literal litMsg = new Literal();
                //litMsg.Text = msg;
                //litMsg.Text = litMsg.Text.Insert(50, "<ul><li>test1</li></ul>");

                _divStatusMessage.InnerHtml = msg;
                //_pStatusMessage.Controls.Add(litMsg);
                //.InnerHtml = msg;

                if (SessionStatusMessageType != null)
                {
                    CS.General_v3.Enums.STATUS_MSG_TYPE type = (CS.General_v3.Enums.STATUS_MSG_TYPE)SessionStatusMessageType;
                    Session.Remove(SESSION_STATUS_MESSAGE_TYPE);
                    switch (type)
                    {
                        case CS.General_v3.Enums.STATUS_MSG_TYPE.Error: _divStatusMessage.Attributes["class"] = "error"; break;
                        case CS.General_v3.Enums.STATUS_MSG_TYPE.Success: _divStatusMessage.Attributes["class"] = "success"; break;
                        case CS.General_v3.Enums.STATUS_MSG_TYPE.Warning: _divStatusMessage.Attributes["class"] = "warning"; break;
                    }
                    _divStatusMessage.Visible = true;
                }
            }
        }

        /// <summary>
        /// This method checks for the 'custom access parameter', which allows CasaSoft-acccess type users to choose their 'session access', in order to test the CMS
        /// </summary>
        private void checkForCustomAccessParameter()
        {

            string s = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QUERYSTRING_PARAM_CUSTOMACCESS);
            if (!string.IsNullOrEmpty(s))
            {
                CS.General_v3.Enums.CMS_ACCESS_TYPE accessType;
                var loggedUser = Functionality.MemberLogin.GetLoggedInUser();
                if (loggedUser != null)
                {
                    if (Enum.TryParse<CS.General_v3.Enums.CMS_ACCESS_TYPE>(s, out accessType))
                    {
                        loggedUser.SessionCustomAccessType = accessType;
                    }
                    else
                    {
                        loggedUser.SessionCustomAccessType = null;
                    }
                }
            }
        }

        private void showDefaultTopButtons()
        {

            initControls(); //Just in case
            if (Functionality.MemberLogin.IsLoggedIn)
            {

                var loggedUser = Functionality.MemberLogin.GetLoggedInUser();
                {
                    string memberTitle = loggedUser.GetFullName() + " (" + loggedUser.Username + ")" + " - " + CS.General_v3.Util.EnumUtils.StringValueOf(loggedUser.AccessType);

                    var aMemberName = Functionality.TopButtons.AddLink(memberTitle, CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.User, "#");

                }
                {
                    if (loggedUser.AccessType == CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)
                    {
                        MyDiv divCustomAccess = new MyDiv(cssClass: "custom-access");
                        MySpan spanLabel = new MySpan(cssClass: "label", parentToAddTo: divCustomAccess);
                        MyDropDownList cmb = new MyDropDownList("cmbCmsCustomAccess", "Custom Access", false);
                        var accessTypeList = CS.General_v3.Util.EnumUtils.GetListOfEnumValues<CS.General_v3.Enums.CMS_ACCESS_TYPE>(CS.General_v3.Enums.ENUM_SORT_BY.Value);
                        CS.General_v3.Classes.URL.URLClass accessUrl = new CS.General_v3.Classes.URL.URLClass();
                        spanLabel.InnerHtml = "Custom Access:";
                        cmb.CssManager.AddClass("custom-access");
                        accessUrl[QUERYSTRING_PARAM_CUSTOMACCESS] = "--default--";
                        cmb.Items.Add(new ListItem("--Default--", accessUrl.ToString()));
                        for (int i = 0; i < accessTypeList.Count; i++)
                        {
                            var val = accessTypeList[i];
                            accessUrl[QUERYSTRING_PARAM_CUSTOMACCESS] = (int)val;
                            string txt = CS.General_v3.Util.EnumUtils.StringValueOf(val, addSpacesToCamelCasedName: true);
                            ListItem li = new ListItem(txt, accessUrl.ToString());
                            if (val == loggedUser.SessionCustomAccessType)
                                li.Selected = true;
                            cmb.Items.Add(li);

                        }
                        cmb.OnChangeRedirectToValue = true;
                        divCustomAccess.Controls.Add(cmb);
                        Functionality.TopButtons.AddControl(divCustomAccess);

                    }
                }
                {
                    CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.MyButton btnLogout = Functionality.TopButtons.AddButton("Logout", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Logout, "btnLogout");
                    btnLogout.Click += new EventHandler(btnLogout_Click);
                }
                if (Functionality.ShowLanguageSelect)
                {
                    UserControls.LanguageSelect langSelect = (UserControls.LanguageSelect)LoadControl("/_common/vs/cms/v1/usercontrols/ucLanguageSelect.ascx");
                    //  langSelect.Languages = this.Functionality.Languages;
                    Functionality.TopButtons.AddControl(langSelect);
                }
            }
            
        }

        void btnLogout_Click(object sender, EventArgs e)
        {


            this.Functionality.MemberLogin.Logout();
            this.Functionality.ShowStatusMessage("You have been logged out successfully!", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
            string loginUrl = CmsObjects.CmsRoutesMapper.Instance.GetLoginPage();
            CS.General_v3.Util.PageUtil.RedirectPage(loginUrl);
            //Re2sponse.Red2irect(CmsObjects.CmsSystemBase.Instance.GetCmsRoot());
        }
        
    }
}
