﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

namespace CS.WebComponentsGeneralV3.Code.Cms.Pages
{
    public abstract class LoginPage : BaseCMSPage
    {
        public LoginPage()
        {
            this.Functionality.AccessRequired.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Everyone;
            this.Functionality.CheckRestrictedIPAccess = false;
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            Master.Functionality.SetPageTitle(new PageTitle("Login", "default.aspx"));
            
            base.OnLoad(e);
        }
    }
}