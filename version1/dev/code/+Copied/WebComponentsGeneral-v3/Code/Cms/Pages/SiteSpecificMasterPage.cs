﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;


using System.Web.UI;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Code.Cms.Pages
{
    /// <summary>
    /// This is the master page that is directly inherited by the CMS.  This, in turn has 'BaseMasterPage' as its master
    /// </summary>
    public abstract class SiteSpecificMasterPage : BusinessLogic_v3.Classes.Pages.BaseMasterPage
    {
        public SiteSpecificMasterPage()
        {
           
            
        }
        
        
        protected void addAdditionalMenuItems()
        {
            var additionalMenus = CmsSystem.Instance.GetCustomAdditionalMenuItems();
            foreach (var menuItem in additionalMenus)
            {
                this.Master.Functionality.MainMenu.AddItem(menuItem);
            }
        }

        void Master_AddAdditionalMenuItems()
        {
            addAdditionalMenuItems();
        }

/*
        protected BusinessLogic_v3.OldCMS.Collections.MainMenuItem addDefaultMenuItem(IObjectPageInfo pageInfo, CS.WebComponentsGeneralV3.Code.Cms.Enums.IMAGE_ICON icon)
        {
            string folderRoot = CS.General_v3.Classes.Settings.SettingsMain.CMS.GetRoot() + pageInfo.Folder + "/";
            string indexPageName = pageInfo.IndexPageName;
            return this.addDefaultMenuItem(folderRoot, pageInfo.Title_SingleUppercase, pageInfo.Title_PluralUppercase, icon, indexPageName);
        }*/

        /// <summary>
        /// Does not add the 'edit' links
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="titleSingular"></param>
        /// <param name="titlePlural"></param>
        /// <param name="icon"></param>
        /// <param name="indexPageName"></param>
        /// <returns></returns>
        protected MainMenuItem addDefaultMenuItem(string folder, string titleSingular, string titlePlural,
            CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON icon, string indexPageName)
        {
            return addDefaultMenuItem(folder, titleSingular, titlePlural, icon, indexPageName, null);
        }
        protected MainMenuItem addDefaultMenuItem(string folder, string titleSingular, string titlePlural,
           CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON icon, string indexPageName, string editPageName)
        {

            
            string rootPageURL = folder;
            if (!rootPageURL.EndsWith("/")) rootPageURL += "/";
            if (string.IsNullOrEmpty(editPageName))
                rootPageURL += indexPageName;
            MainMenuItem menuItem = new MainMenuItem(titlePlural, rootPageURL , icon);

           /* if (!string.IsNullOrEmpty(editPageName))
            {
                menuItem.AddChild(new BusinessLogic_v3.OldCMS.Collections.MainMenuItem("Manage " + titlePlural,
                     folder + "/" + indexPageName, CS.WebComponentsGeneralV3.Code.Cms.Enums.IMAGE_ICON.Listing));
                menuItem.AddChild(new BusinessLogic_v3.OldCMS.Collections.MainMenuItem("Add new " + titleSingular,
                    folder + "/" + editPageName + "?id=0", CS.WebComponentsGeneralV3.Code.Cms.Enums.IMAGE_ICON.Add));

            }*/
            Master.Functionality.MainMenu.AddItem(menuItem);
            return menuItem;
        }


        public new BaseCMSMasterPage Master
        {
            get
            {
                return (BaseCMSMasterPage)base.Master;
            }
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            
            base.OnPreRender(e);
        }
        protected virtual void createMenu()
        {
            //do nothing
        }
        protected override void OnLoad(EventArgs e)
        {
            this.createMenu();
            base.OnLoad(e);
        }
        

        protected override void OnInit(EventArgs e)
        {

            this.Master.AddAdditionalMenuItems += new Action(Master_AddAdditionalMenuItems);
            base.OnInit(e);
        }

        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get {return this.Master.HtmlTag; }
        }
    }
}
