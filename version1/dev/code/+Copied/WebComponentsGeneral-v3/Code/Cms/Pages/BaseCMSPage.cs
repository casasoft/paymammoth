﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

using CS.WebComponentsGeneralV3.Code.Cms.Util;
using BusinessLogic_v3.Modules.CmsUserModule;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;


namespace CS.WebComponentsGeneralV3.Code.Cms.Pages
{
    public abstract class BaseCMSPage : CS.WebComponentsGeneralV3.Code.Classes.Pages.BasePage<BaseCMSMasterPage>
    {
        private void checkIPAddressRestriction()
        {
            //CmsUtil.CheckIPWhiteListingOmitted();
            if (this.Functionality.CheckRestrictedIPAccess && CmsUserSessionLogin.Instance.IsLoggedIn)
            {
                bool ok = CmsUtil.IsUserIPWhitelistedToAccessCMS();
                if (!ok)
                {
                    CmsUserSessionLogin.Instance.Logout();

                    this.Master.Functionality.ShowIPRestricedError();
                }

            }
        }
        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.CommonControllersJSParams.isLoggedInCms = false; // force it so that it does not show text editing
            checkIPAddressRestriction();
            base.OnLoad(e);
        }

        protected string getFactoryTitleFromRouteData()
        {
            return CS.General_v3.Util.PageUtil.GetRouteDataVariable(CmsConstants.ROUTING_PARAM_TITLE);
        }

        private void setCustomCulture()
        {
            if (this.Master.Functionality.CurrentCmsCultureID.GetValueOrDefault() > 0)
            {

                BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.CustomCultureOverrideForCurrentContext =
                    BusinessLogic_v3.Modules.Factories.CultureDetailsFactory.GetByPrimaryKey(this.Master.Functionality.CurrentCmsCultureID.Value);
            }
            else
            {
                BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.CustomCultureOverrideForCurrentContext = null;
            }
        }
        private void resetCustomCulture()
        {
            BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.CustomCultureOverrideForCurrentContext = null;
        }

        
        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
            //resetCustomCulture();
        }
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            base.Render(writer);
            resetCustomCulture();
        }

        public new BaseCMSMasterPage Master
        {
            get
            {
                return (BaseCMSMasterPage)base.Master;
            }
        }

        public CmsUserBase GetCurrentLoggedInUser()
        {
            return this.Functionality.GetCurrentLoggedInUser();
        }
        
        
        private void checkAccess()
        {
            bool ok = this.Functionality.VerifyAccess();
            if (!ok)
            //if (!Functionality.NoAuthenticationRequired && !Functionality.MemberLogin.CheckAuthentication(Page.Functionality.AccessTypeRequired))
            {
                //Authentication is required and no member is logged in
                this.Master._lastLoggedInURLWithoutAccessForUser = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                this.Master.Functionality.ShowStatusMessage("You do not have access to visit this section.  ", CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
                string loginUrl = CmsObjects.CmsRoutesMapper.Instance.GetLoginPage();
                //string root = CmsObjects.CmsSystemBase.Instance.GetCmsRoot();
                CS.General_v3.Util.PageUtil.RedirectPage(loginUrl);
                
            }
        }

        /// <summary>
        /// This method is used to check further access requirements.  If this returns false, user is not given access.
        /// </summary>
        /// <returns></returns>
        protected virtual bool checkFurtherAccessRequirements()
        {
            return true;
        }

        public new class FUNCTIONALITY : BasePage<BaseCMSMasterPage>.FUNCTIONALITY
        {
            public bool CheckRestrictedIPAccess { get; set; }
            public CmsUserBase GetCurrentLoggedInUser()
            {
                
                return CmsUserSessionLogin.Instance.GetLoggedInUser();
            }

            private BaseCMSPage _page = null;
            /// <summary>
            /// Minium access level type to access this page (also required the roles as well)
            /// </summary>
            public virtual CmsAccessType AccessRequired { get; private set; }
            public virtual void SetAccessRoleRequired(Enum enumValue)
            {
                this.AccessRequired.AccessRolesRequired.Clear();
                this.AccessRequired.AddAccessRole(enumValue);
            }
            


            public FUNCTIONALITY(BaseCMSPage page) : base(page)
            {
                _page = page;
                this.AccessRequired = new CmsAccessType(null);
                this.CheckRestrictedIPAccess = true; // by default always check for restricted IP access
 

 
            }

            public bool VerifyAccess()

            {
                var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
                bool ok = false;

                if (loggedUser != null)
                {
                    ok = this.AccessRequired.CheckIfUserHasEnoughAccess(loggedUser);
                }
                else
                {
                    if (this.AccessRequired.MinimumAccessTypeRequired == CS.General_v3.Enums.CMS_ACCESS_TYPE.Everyone &&
                        this.AccessRequired.AccessRolesRequired.Count == 0)
                    {
                        ok = true;//only allow if no access is required and 

                    }

                }
                if (ok)
                {
                    ok = _page.checkFurtherAccessRequirements();

                }
                return ok;


            }

        }
        public new FUNCTIONALITY Functionality
        {
            get
            {
                return (FUNCTIONALITY)base.Functionality;
            }
            
        }
        
        public BaseCMSPage()
        {

            this.Functionality.OutputLanguageFromCurrentCulture = false;
            this.Functionality.PermanentRedirectRootToWWW = false;
            
            
        }
        protected override BasePageBase.FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
            
        }
        
        protected override void OnPreInit(EventArgs e)
        {
            
            
            setCustomCulture();
            
            base.OnPreInit(e);
        }
        protected override void OnInit(EventArgs e)
        {
            checkAccess(); 
            base.OnInit(e);
        }

        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get { return this.Master.HtmlTag; }
        }
    }
}