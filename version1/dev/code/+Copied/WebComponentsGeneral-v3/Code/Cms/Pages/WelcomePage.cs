﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

namespace CS.WebComponentsGeneralV3.Code.Cms.Pages
{
    public abstract class WelcomePage : BaseCMSPage
    {
        public WelcomePage()
        {
            this.Functionality.AccessRequired.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser;
        }
        protected override void OnLoad(EventArgs e)
        {
            Master.Functionality.SetPageTitle(new PageTitle("Welcome", "welcome.aspx"));
            
            base.OnLoad(e);
        }
    }
}