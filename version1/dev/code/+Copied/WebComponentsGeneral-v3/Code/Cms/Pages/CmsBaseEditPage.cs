﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms;
using CS.WebComponentsGeneralV3.Code.Cms.Util;


using BusinessLogic_v3.Classes.Exceptions;
using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;
using CS.General_v3.Classes.HelperClasses;

using NHibernate;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;


namespace CS.WebComponentsGeneralV3.Code.Cms.Pages
{
    public abstract class CmsBaseEditPage : BaseCMSPage
    {
        protected abstract FormFields _formFields { get; }
        protected abstract PlaceHolder _phBeforeForm { get; }
        protected abstract PlaceHolder _phAfterForm { get; }
         public CmsBaseEditPage()
        {

        }

         public CmsBaseEditPage(ICmsItemFactory factory)
        {
            //the parameter is just to satisfy old requirements
        }

    }
}