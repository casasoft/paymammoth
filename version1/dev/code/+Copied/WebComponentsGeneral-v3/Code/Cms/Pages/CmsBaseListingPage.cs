﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.General_v3.Classes.URL;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;
using CS.General_v3.Controls.WebControls.Common.General;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using System.Web.UI.WebControls;

using CS.WebComponentsGeneralV3.Code.Cms.WebControls;
using CS.General_v3.Classes.CSS;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Extensions;
using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;
using CS.General_v3.Classes.HelperClasses;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;


namespace CS.WebComponentsGeneralV3.Code.Cms.Pages
{
    public abstract class CmsBaseListingPage : BaseCMSPage



    {
        [Obsolete]
        protected abstract PlaceHolder _phListingComponents {get;}
        [Obsolete]
        protected abstract MyButton _btnListingUpdate { get; }
        [Obsolete]
        protected abstract FormFields _formSearch { get; }
        [Obsolete]
        protected abstract PlaceHolder _phAfterListing { get; }
        [Obsolete]
        protected abstract PlaceHolder _phBeforeFormSearch { get; }
        [Obsolete]
        protected abstract PlaceHolder _phBeforeListing { get; }
       
        public CmsBaseListingPage()
        {

        }

        public CmsBaseListingPage(ICmsItemFactory factory)
        {
            //the parameter is just to satisfy old requirements
        }


    }
}