﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;

namespace CS.WebComponentsGeneralV3.Code.Cms.UserControls
{
    public class BaseCmsUserControl : CS.WebComponentsGeneralV3.Code.Controls.UserControls.BaseUserControl
    {
        public BaseCMSMasterPage Master
        {
            get { return this.Page.Master; }
        }




        public new BaseCMSPage Page
        {
            get { return (BaseCMSPage)base.Page; }
        }
        protected virtual void initControls()
        {
            
        }
    }
}
