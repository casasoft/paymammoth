﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms
{
    public static class CmsUrls
    {
        public const string BASE_CMS_COMMON_FOLDER = "/_ComponentsGeneric/CMS/Pages/";
        public const string CMS_FOLDER = "/cms/";

        public static string GetBaseCmsFolder()
        {
            return CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + CMS_FOLDER.Substring(1); //remove the double slash
        }
    }
}
