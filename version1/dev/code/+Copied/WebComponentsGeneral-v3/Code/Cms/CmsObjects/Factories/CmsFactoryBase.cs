﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Util;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Classes.DbObjects;
using CS.General_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using NHibernate.Criterion;
using CS.General_v3.Util;
using NHibernate;
using CS.General_v3.Classes.Factories;
using System.Collections;
using CS.General_v3.Classes.URL;
using BaseDbObject = BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;


namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories
{
    public abstract class CmsFactoryBase<TCMSItemInfo, TDBItem> : CS.General_v3.Classes.Factories.BaseFactory,  ICmsItemFactory
        where TCMSItemInfo : CmsItemInfo
        where TDBItem : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject
       
        
    {
        public static void _initialiseStaticInstance()
        {
            var tmp = Instance;

        }
        public static CmsFactoryBase<TCMSItemInfo, TDBItem> Instance
        {
            get 
            {

                return (CmsFactoryBase<TCMSItemInfo, TDBItem>)CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<CmsFactoryBase<TCMSItemInfo, TDBItem>>(null);;
            }
        }
        public static bool UsedInProject { get; set; }
        

        private IBaseDbFactory<TDBItem> _dbFactory = null;
        protected IBaseDbFactory<TDBItem> dbFactory
        {
            get
            {
                if (_dbFactory == null)
                {
                    _dbFactory = (IBaseDbFactory<TDBItem>)BusinessLogic_v3.Classes.DbObjects.DbFactoryController.Instance.GetFactoryForType(typeof(TDBItem));
                }
                return _dbFactory;
            }
        }
        [Obsolete("Use 'this.UserSpecificGeneralInfoInContext.Name' instead")]
        public string Name
        {
            get { return this.UserSpecificGeneralInfoInContext.Name; }
            set { this.UserSpecificGeneralInfoInContext.Name = value; }

        }

        public CmsFactoryBase( )
        {
           // CS.General_v3.Util.ContractsUtil.RequiresNotNullable(dbFactory);
            this.CmsItemType = typeof(TCMSItemInfo);
            //this.dbFactory = dbFactory;

           // this.LinkedChildren = new List<ICmsItemFactory>();

            this.TypesCreatedByFactory.Add(typeof(TCMSItemInfo));
            this.TypesCreatedByFactory.Add(typeof(TDBItem));


            CmsSystem.Instance.RegisterItemType(this);

            
            

        }

        public override void OnPostInitialisation()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(dbFactory, "Database factory must be initialised for Cms factory to work");
            this.addTypeCreatedByFactory(this.CmsItemType);
            this.addTypeCreatedByFactory(dbFactory.GetTypesCreatedByFactory());
            //initCmsParameters();
            base.OnPostInitialisation();
        }

        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase getLoggedCmsUser()
        {
            return CmsUserSessionLogin.Instance.GetLoggedInUser(); 
        }
        protected abstract IQueryOver _getQueryForSearchResults(GetQueryParams qParams);
        protected virtual void parseQueryForListing(IQueryOver query)
        {

        }

        [Obsolete("Use the 'getUserSpecificGeneralInfo'")]
        protected virtual void initCmsParameters()
        {

        }
        public abstract TCMSItemInfo CreateCmsItemInfo(TDBItem item);
        public virtual TCMSItemInfo CreateNewItem(bool isTemporary)
        {
            var item = _createNewItem(isTemporary);
            
           
            return item;
            
            

        }
        protected abstract TCMSItemInfo _createNewItem(bool isTemporary);
        protected string baseFolder { get; set; }
      //  protected string listingPageName { get; set; }
      //  protected string editPageName { get; set; }

        public CS.General_v3.Classes.URL.URLClass GetAddUrl()
        {
            return GetEditUrlForItemWithID(0);
        }

        //public ICmsItemFactory LinkedParent { get; set; }
        
        public long? GetIDFromCurrentQuerystring()
        {

            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long?>(QueryStringParamID);
        }

        private void addCurrentCultureToUrl(URLClass url)
        {
            url[CS.WebComponentsGeneralV3.Code.Cms.Pages.BaseCMSMasterPage.QUERYSTRING_PARAM_CULTUREID] = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture().ID;
            
        }
        public CS.General_v3.Classes.URL.URLClass GetEditUrlForItemWithID(TCMSItemInfo item)
        {
            if (item != null)
                return GetEditUrlForItemWithID(item.DbItem.ID);
            else
                return null;
        }
        public CS.General_v3.Classes.URL.URLClass GetEditUrlForItemWithID(TDBItem item)
        {
            if (item != null)
                return GetEditUrlForItemWithID(item.ID);
            else
                return null;
        }

        public CS.General_v3.Classes.URL.URLClass GetEditUrlForItemWithID(long id)
        {

            string s = CmsRoutesMapper.Instance.GetEditPage(this.GetTitleForRoutingFolder());
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(s);
            url.ClearQueryString();
            url.LoadFromCurrentQuerystring();
            url[QueryStringParamID] = null;
            if (id > 0)
                url[QueryStringParamID] = id;
            addCurrentCultureToUrl(url);
            resetLastAddedQueryStringParam(url);
            url.Path = s;
            return url;
            
        }
        public bool CheckIfFactoryIsOfType(Type t)
        {
            if (this.CmsItemType.IsAssignableFrom(t) ||
                t.IsAssignableFrom(this.CmsItemType))
                return true;
            else
                return false;
        }





        public IEnumerable<CmsFieldBase> GetPropertyInfos(TCMSItemInfo item)
        {
            TDBItem dbItem = null;
            if (item != null)
                dbItem =(TDBItem)item.DbItem;
            var cmsInfo = CreateCmsItemInfo(dbItem);
            return cmsInfo.GetCmsProperties();

        }

        public virtual PageTitle GetListingPageTitle(CmsCollectionInfo pInfo)
        {
            string title = null;
            long id = GetIDFromCurrentQuerystring().GetValueOrDefault();
            if (pInfo == null)
                title = this.TitlePlural;
            else
                title = pInfo.Label;



           return new PageTitle(title, GetListingUrl().ToString() );

        }
        public virtual PageTitle GetEditPageTitle(long id)
        {
            string title = null;
            ICmsItemInstance item = null;
            title = this.TitleSingular;
            if (id > 0)
            {
                item = GetCmsItemWithId(id);
            }
            if (item != null && !item.CheckIfStillTemporary())
            {
                title = "Edit " + this.TitleSingular + " [" + item.TitleForCms + "]";
            }
            else
            {
                title = "Add new " + this.TitleSingular;
            }
            return new PageTitle(title, GetEditUrlForItemWithID(id).ToString());

        }
        public virtual PageTitle GetEditPageTitle()
        {
            long id = GetIDFromCurrentQuerystring().GetValueOrDefault();
            return GetEditPageTitle(id);
        }

        /// <summary>
        /// Returns the first property being filtered with, e.g SHoing the Payment Requests filtered by WebsiteAccount = 1.  In this case, this returns the 'WebsiteAccount' collection.
        /// </summary>
        /// <returns></returns>
        protected CmsLinkedPropertyInfo getFirstPropertyFilteringWith()
        {
            var list = getPropertiesFilteringWith();
            if (list.Count > 0)
            {
                var collInfo = list[0];
                return collInfo;
            }
            else
                return null;

        }

        public virtual IEnumerable<PageTitle> GetListingPageTitlesForThisAndLinked()
        {
            List<PageTitle> list = new List<PageTitle>();

            CmsLinkedPropertyInfo linkedProperty = getFirstPropertyFilteringWith();
            CmsCollectionInfo collInfo = null;
            if (linkedProperty != null)
            {
                collInfo = linkedProperty.GetLinkedObjectCollectionFromOtherSide();

                if (collInfo.GetLinkedIDFromQuerystring().GetValueOrDefault() > 0)
                {
                    long linkedID = collInfo.GetLinkedIDFromQuerystring().Value;

                    ICmsItemFactory linkedFactory = linkedProperty.GetFactoryForLinkedObject();
                    if (linkedFactory != this)
                    {
                        list.AddRange(linkedFactory.GetEditPageTitlesIncludingLinked(linkedID));
                    }
                }
            }
            list.Add(GetListingPageTitle(collInfo));
            return list;
        }
        public virtual IEnumerable<PageTitle> GetEditPageTitlesIncludingLinked(long itemID)
        {
            List<PageTitle> list = new List<PageTitle>();
            list.AddRange(this.GetListingPageTitlesForThisAndLinked());
            list.Add(this.GetEditPageTitle(itemID));
            return list;
        }
        public virtual IEnumerable<PageTitle> GetEditPageTitlesForThisAndLinked()
        {
            long id = GetIDFromCurrentQuerystring().GetValueOrDefault();
            return GetEditPageTitlesIncludingLinked(id);
        }
        public virtual string GetGoBackUrlForListing()
        {
            string s = null;
            {
                var linkedP = getFirstPropertyFilteringWith();
                if (linkedP != null)
                {
                    var collInfo = linkedP.GetLinkedObjectCollectionFromOtherSide();
                    long id = collInfo.GetLinkedIDFromQuerystring().GetValueOrDefault();
                    if (id > 0)
                    {
                        var linkedFactory = linkedP.GetFactoryForLinkedObject();
                        s = linkedFactory.GetEditUrlForItemWithID(id).GetURL();
                        //s = linkedFactory.GetEditPageTitlesIncludingLinked(id).ToString();
                    }
                }
                
            }
            return s;


        }
        public virtual string GetGoBackUrlForEdit()
        {
            return this.GetListingUrl().ToString();
        }
        private string _QueryStringParamID = null;

        public string QueryStringParamID
        {
            get 
            {
                string s = _QueryStringParamID;
                if (string.IsNullOrEmpty(s)) s = this.Name + "Id";
                return s;
                
            }
            set { _QueryStringParamID = value; }
        }
        

        protected string getBaseFolder()
        {
            string s = this.baseFolder ?? "";
            if (!s.EndsWith("/")) s += "/";
            return s;
        }
        private void resetLastAddedQueryStringParam(CS.General_v3.Classes.URL.URLClass url)
        {
            url[CmsConstants.QUERYSTRING_LASTADDED] = null;
        }

        /// <summary>
        /// Returns the cms folder, e.g /cms/Category/
        /// </summary>
        /// <returns></returns>
        public string GetCmsFolderUrl(bool includeBaseUrl = true)
        {
            string baseFolder = getBaseFolder();
            if (baseFolder.StartsWith("/")) baseFolder = baseFolder.Substring(1);
            string s = "/";
            if (includeBaseUrl)
                s = CS.General_v3.Util.IO.EnsurePathEndsWithSlash(CS.General_v3.Util.PageUtil.GetApplicationBaseUrl().GetURL(fullyQualified: true, includePath: false, appendQueryString: false));
            s += CS.General_v3.Util.IO.EnsurePathDoesNotStartWithSlash(baseFolder);
            //string s = CS.General_v3.Util.PageUtil.GetBaseURL() + baseFolder;
            return s;
        }

        public virtual string GetTitleForRoutingFolder()
        {
            return this.Name;
        }

        
        


        public virtual CS.General_v3.Classes.URL.URLClass GetListingUrl(bool removeAnyQuerystringParams = false)
        {
            string s = CmsRoutesMapper.Instance.GetListingPage(this.GetTitleForRoutingFolder());
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(s);

            url.ClearQueryString();
            url.LoadFromCurrentQuerystring();

            if (removeAnyQuerystringParams)
                url.ClearQueryString();

            resetLastAddedQueryStringParam(url);

            addCurrentCultureToUrl(url);
            
            /*if (removeAnyQuerystringParams)
            {
                CmsInfoBase.Instance.RemoveAnyQuerystringParametersRelatedToCms(url);
            }*/

            url[QueryStringParamID] = null;
            
            return url;

            

        }

        public TCMSItemInfo GetCmsItemWithId(long id)
        {
            var dbItem =dbFactory.GetByPrimaryKey(id);
            ICmsItemInstance cmsItem = null;
            if (dbItem != null)
            {
                cmsItem = CreateCmsItemInfo(dbItem);
                
                
            }

            return (TCMSItemInfo)cmsItem;
            

        }


        public List<TCMSItemInfo> ConvertDataItemsToCmsItems(IEnumerable<TDBItem> dbItems)
        {
            List<TCMSItemInfo> list = new List<TCMSItemInfo>();
            foreach (var dbItem in dbItems)
            {
                var cmsItem = CreateCmsItemInfo(dbItem);
                list.Add(cmsItem);
            }
            return list;

        }
        private List<TCMSItemInfo> convertDataItemsToCmsItems(IEnumerable dbItems)
        {
            List<TDBItem> list = new List<TDBItem>();
            foreach (var dbItem in dbItems)
            {
                list.Add((TDBItem)dbItem);
            }
            return ConvertDataItemsToCmsItems(list);
            

        }


        protected IEnumerable<ICmsItemInstance> getSearchResultsFromCriteria(NHibernate.ICriteria crit, int pageNo, int pageSize, out int totalResults)
        {
            if (pageSize > 500)
            {
                //2011-10-28 Done by Mark to help not hog the server if there is too much items to load
                pageSize = 500;
            }
            IEnumerable<TDBItem> results = nHibernateUtil.LimitCriteriaByPrimaryKeysAndReturnResultAndTotalCount<TDBItem>(crit,  pageNo, pageSize, out totalResults, useFutures: false);

         //   return results.ConvertAll<object, ICmsItemInstance>(item=>(ICmsItemInstance)item);
            //var list = dbFactory.FindAll(crit);
            //CS.General_v3.Util.nHibernateUtil.LimitQueryByPrimaryKeysAndGetTotalCount(

            return convertDataItemsToCmsItems(results);
        }


        /// <summary>
        /// Returns a list of collections, which the current listing/edit page is being filtered with.  In most cases this will only return one collection.
        /// A filtering collection could be say you are viewing Website Accounts, and click on the 'Payment Requests' section.  In the PaymentRequests page, you will be filtering
        /// by the specific Website Account linked, which will be returned by this method.
        /// </summary>
        /// <returns></returns>
        protected List<CmsLinkedPropertyInfo> getPropertiesFilteringWith()
        {
            List<CmsLinkedPropertyInfo> list = new List<CmsLinkedPropertyInfo>();
            foreach (var property in this.GetPropertyInfos(null)) // fill links
            {
                if (property is CmsLinkedPropertyInfo)
                {
                    CmsLinkedPropertyInfo p = (CmsLinkedPropertyInfo)property;

                    CmsCollectionInfo collInfo = p.GetLinkedObjectCollectionFromOtherSide(); //get the collection from the other side

                    if (collInfo != null && collInfo.IsUsedInProject())
                    {
                        if (collInfo.ShowLinkInCms)
                        {
                            var linkedFactory = collInfo.CmsFactory;
                            long? linkedID = collInfo.GetLinkedIDFromQuerystring();
                            if (linkedID.GetValueOrDefault() > 0)
                            {
                                list.Add(p);
                            }
                        }
                    }

                }
            }
            return list;
        }


        public virtual CmsSearchResults GetSearchResultsForListing(IEnumerable<CmsPropertySearchInfo> searchCriteria, bool showDeleted, bool filterByLinkedItems, int pageNo, int pageSize, 
            CmsFieldBase propertyToSortWith, CS.General_v3.Enums.SORT_TYPE sortType, bool showNonPublished = true)
        {
            GetQueryParams qParams = new GetQueryParams();
            qParams.FillDefaultsForCms();
            qParams.LoadDeletedItems = showDeleted;
            qParams.LoadNonPublishedItems = showNonPublished;
            var query = _getQueryForSearchResults(qParams);
            parseQueryForListing(query);
            var crit = query.RootCriteria;
            
            if (searchCriteria != null) // fill search criterias
            {
                foreach (var p in searchCriteria)
                {
                    
                    p.AddCriterionToSearchCriteria(crit);
                    

                }
            }
            if (filterByLinkedItems)
            {
                var filteringProperties = getPropertiesFilteringWith();
                foreach (var p in filteringProperties)
                {
                    CmsCollectionInfo collInfo = p.GetLinkedObjectCollectionFromOtherSide(); //get the collection from the other side

                    var linkedFactory = collInfo.CmsFactory;
                    long? linkedID = collInfo.GetLinkedIDFromQuerystring();
                    if (linkedID.GetValueOrDefault() > 0)
                    {
                        //var item = linkedFactory.GetCmsItemWithId(linkedID.Value);
                        //if (item != null)
                        {
                            var expression = Util.CmsUtil.GetNHibernateCriterion(p.PropertyInfo, linkedID.Value);
                            if (expression != null)
                                crit.Add(expression);
                        }
                    }
                    if (collInfo.PreFetchInListing)
                    {
                        //fetch any items which will be used in the listing
                        //                            if (p.PropertyInfo.PropertyType.IsAssignableFrom(typeof(IEnumerable)))
                        crit.SetFetchMode(p.PropertyInfo.Name, FetchMode.Join); //if it is a collection, use a subselect (this should never be the case, as collections cannot be show in listings)
                        //else
                        //    crit.SetFetchMode(p.PropertyInfo.Name, FetchMode.Join); //if it is a direct link, use a join
                    }
                }
                //foreach (var property in this.GetPropertyInfos(null)) // fill links
                //{
                //    if (property is CmsLinkedPropertyInfo)
                //    {
                //        CmsLinkedPropertyInfo p = (CmsLinkedPropertyInfo)property;
                        
                //        CmsCollectionInfo collInfo = p.GetLinkedObjectCollectionFromOtherSide(); //get the collection from the other side

                //        if (collInfo != null && collInfo.IsUsedInProject())
                //        {


                //            if (collInfo.ShowLinkInCms)
                //            {
                //                var linkedFactory = collInfo.CmsFactory;
                //                long? linkedID = collInfo.GetLinkedIDFromQuerystring();
                //                if (linkedID.GetValueOrDefault() > 0)
                //                {
                //                    //var item = linkedFactory.GetCmsItemWithId(linkedID.Value);
                //                    //if (item != null)
                //                    {
                //                        var expression = Util.CmsUtil.GetNHibernateCriterion(p.PropertyInfo, linkedID.Value);
                //                        if (expression != null)
                //                            crit.Add(expression);
                //                    }
                //                }
                //            }
                //            if (collInfo.PreFetchInListing)
                //            {
                //                //fetch any items which will be used in the listing
                //                //                            if (p.PropertyInfo.PropertyType.IsAssignableFrom(typeof(IEnumerable)))
                //                crit.SetFetchMode(p.PropertyInfo.Name, FetchMode.Join); //if it is a collection, use a subselect (this should never be the case, as collections cannot be show in listings)
                //                //else
                //                //    crit.SetFetchMode(p.PropertyInfo.Name, FetchMode.Join); //if it is a direct link, use a join
                //            }
                //        }
                        
                //    }
                //}
            }
            if (propertyToSortWith != null)
            {
                propertyToSortWith.ApplyOrderByToCriteria(crit, sortType);

                //crit.AddOrder(new Order(sortBy, sortType == CS.General_v3.Enums.SORT_TYPE.Ascending));
            }
            
            
            
            

            
            CmsSearchResults searchResults = new CmsSearchResults();
            int totalResults ;
            var results = getSearchResultsFromCriteria(crit, pageNo, pageSize, out totalResults);

            searchResults.TotalCount = totalResults;
            searchResults.SearchItems = results.ConvertAll(item => (ICmsItemInstance)item);


            return searchResults;

        }

        protected virtual string getPrimaryKeyName()
        {
            return "ID";
            
        }

      


        /*
        public IEnumerable<CmsPropertyBase> GetPropertyInfos(TDBItem item)
        {
            var cmsInfo = CreateCmsItemInfo(item);
            return cmsInfo.GetListOfCMSProperties();

        }*/


        ICmsItemInstance ICmsItemFactory.CreateNewItem(bool isTemporary)
        {
            return this.CreateNewItem(isTemporary);
            
        }

        [ThreadStatic]
        private CmsItemGeneralInfo _threadStatic_userSpecificGeneralInfo = null;


        private CmsItemGeneralInfo _context_userSpecificGeneralInfo
        {
            get
            {


                return CS.General_v3.Util.PageUtil.GetContextObject<CmsItemGeneralInfo>("CMS_" + this.CmsItemType.Name + "_userSpecificGeneralInfo");
            }
            set { CS.General_v3.Util.PageUtil.SetContextObject("CMS_" + this.CmsItemType.Name + "_userSpecificGeneralInfo", value); }
        }
        
        public CmsItemGeneralInfo UserSpecificGeneralInfoInContext
        {
            get 
            {
                CmsItemGeneralInfo info = null;
                if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
                {

                    info = _context_userSpecificGeneralInfo;

                }
                else
                {
                    info = _threadStatic_userSpecificGeneralInfo;
                }

                
                if (info == null)
                {
                    info = new CmsItemGeneralInfo(this);

                    if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
                    {

                        _context_userSpecificGeneralInfo = info;

                    }
                    else
                    {
                        _threadStatic_userSpecificGeneralInfo = info;
                    }


                    getUserSpecificGeneralInfo(info);
                    
                }
                return info;
            }
        }

        private void createDefaultButtons(CmsItemGeneralInfo cmsInfo)
        {
            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            {
                

                if (cmsInfo.GetWhetherToShowAddNewButton())
                {

                    string href = this.GetAddUrl().ToString();
                    var op = new CmsGeneralOperation("Add New " + this.TitleSingular, CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Add, href);
                    
                    this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);

                }

            }
            {
                if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam))
                {
                    var op = new CmsGeneralOperation("Re-Save All", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Save, customOperation_ReSaveAllItems);
                    op.ConfirmMessage = "This will iterate through ALL the items, and re-save them.  This might take quite some time.  This should only be used to update formula properties mainly. Continue?";
                    this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
                    


                }

            }
            {
                {
                    if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam))
                    {
                        var op = new CmsGeneralOperation("Refresh Lucene Index", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.ReOrder, customOperation_RefreshLucene);
                        op.ConfirmMessage = "Refreshing the Lucene Index might take some time! Do only if search results are not working correctly! Also, index is refreshed for this sections only. Other sections must be refreshed separately.";
                        this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
                    }
                }

            }
        }



        private void customOperation_RefreshLucene()
        {
            this.dbFactory.ReCreateLuceneIndex();
            CmsUtil.ShowStatusMessageInCMS("Lucene index recreated successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);

        }
        /// <summary>
        /// This re-saves all items in the database, by iterating through ALL of the items, and calls save on each of them.  This should only be used for forumala properties mainly.
        /// </summary>
        private void customOperation_ReSaveAllItems()
        {
            var dbFactory = this.dbFactory;
            var allItems = dbFactory.FindAll().ToList();
            var session = NHClasses.NhManager.GetCurrentSessionFromContext();
            using (var transaction = session.BeginTransaction())
            {
                for (int i = 0; i < allItems.Count; i++)
                {
                    var item = allItems[i];
                    item.Update();
                }
                transaction.Commit();
            }
            CmsUtil.ShowStatusMessageInCMS("All items have been re-saved successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);

        }
        protected virtual void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            initCmsParameters();
            initCustomCmsOperations();
            createDefaultButtons(cmsInfo);
        }

        //CmsAccessType ICmsItemFactory.AccessTypeRequired_ToAdd
        //{
        //    get 
        //    {
        //        var userInfo = UserSpecificGeneralInfoInContext;
        //        return CmsAccessType.GetMostRestrictiveAccessType(userInfo.AccessTypeRequired_ToAdd, userInfo.AccessTypeRequired_ToView); 
        //    }
        //}
        //CmsAccessType ICmsItemFactory.AccessTypeRequired_ToView
        //{
        //    get 
        //    {
        //        var userInfo = UserSpecificGeneralInfoInContext; 
        //        return CmsAccessType.GetMostRestrictiveAccessType(userInfo.AccessTypeRequired_ToView); 
        //    }
        //}
        
        
        protected virtual CmsUserBase getCurrentLoggedUser()
        {
            return CmsUserSessionLogin.Instance.GetLoggedInUser();
        }

        



        /// <summary>
        /// Returns a list of ListItems
        /// </summary>
        /// <param name="propertyInfo">Property Info that is retrieving this information. Optional</param>
        /// <param name="addNullValue">Whehter to add a null value on top</param>
        /// <param name="excludeID">Id to exclude</param>
        /// <param name="filterByLinkedItems">Whether to filter search results by linked items (based on querystring)</param>
        /// <returns></returns>
        public virtual System.Web.UI.WebControls.ListItem[] GetAllAsListItemsForCms(CmsFieldBase propertyInfo = null, bool addNullValue = false, long excludeID=0, bool filterByLinkedItems= false, 
            bool showNonPublished = true)
        {
            var searchResults = this.GetSearchResultsForListing(null, false, filterByLinkedItems, 1, 0, null, CS.General_v3.Enums.SORT_TYPE.Ascending, showNonPublished:showNonPublished);
            List<System.Web.UI.WebControls.ListItem> list = new List<System.Web.UI.WebControls.ListItem>();
            foreach (var item in searchResults.SearchItems)
            {
                if (!item.CheckIfStillTemporary())
                {
                    if (item.DbItem.ID != excludeID)
                    {
                        list.Add(new System.Web.UI.WebControls.ListItem(item.TitleForCms, item.DbItem.ID.ToString()));
                    }
                }
            }
            list.Sort((item1, item2) => (item1.Text.CompareTo(item2.Text)));
            if (addNullValue)
                list.Insert(0,new System.Web.UI.WebControls.ListItem("", ""));
            return list.ToArray();
            
        }

        public virtual Type CmsItemType { get; protected set; }



        public virtual IEnumerable<ICmsHierarchyItem> GetRootItems(IEnumerable<CmsPropertySearchInfo> searchCriteria)
        {
            throw new InvalidOperationException("CMSFactoryBase<" + this.CmsItemType.ToString() + "> : Please override 'GetRootItems' if you want to list this item as a tree view");
            
            
        }


        
       

        protected virtual void initCustomCmsOperations()
        {
            
        }

        /// <summary>
        /// Returns a list of cms operations
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CmsGeneralOperation> GetCustomCmsOperations()
        {
          

            var info = this.UserSpecificGeneralInfoInContext;
             return info.CustomCmsOperations;


        }


        #region ICmsItemFactory Members

        public ICmsItemInstance GetEmptyCmsItemInfo()
        {
            return CreateCmsItemInfo(null);
        }

        public IEnumerable<CmsFieldBase> GetPropertyInfos(ICmsItemInstance item)
        {
            if (item == null)
            {
                item = GetEmptyCmsItemInfo();
                //item = _createNewItem();
            }

            return item.GetCmsProperties();
            
        }
        public TCMSItemInfo GetCmsItemFromDBObject(TDBItem dbObject)
        {
            var item = CreateCmsItemInfo(dbObject);
            
            return item;
        }
        public IEnumerable<TCMSItemInfo> GetListOfCmsItemFromDBObjects(IEnumerable<TDBItem> dbObjects)
        {
            List<TCMSItemInfo> list = new List<TCMSItemInfo>();
            foreach (var dbItem in dbObjects)
            
            {
                if (dbItem != null)
                {
                    list.Add(GetCmsItemFromDBObject(dbItem));
                }
            }
            return list;
            
        }
        ICmsItemInstance ICmsItemFactory.GetCmsItemFromDBObject(BaseDbObject dbObject)
        {

            return CreateCmsItemInfo((TDBItem)dbObject);
            
        }

      
        

        public virtual CmsFieldBase GetDefaultSortFieldForListing(out CS.General_v3.Enums.SORT_TYPE sortType)
        {
            sortType = CS.General_v3.Enums.SORT_TYPE.None;
            return null;
            
        }

        #endregion





        #region ICmsItemFactory Members

        EnumsCms.SECTION_RENDER_TYPE ICmsItemFactory.RenderType
        {
            get { return this.UserSpecificGeneralInfoInContext.RenderType; }
        }

        int ICmsItemFactory.CmsMainMenuPriority
        {
            get { return this.UserSpecificGeneralInfoInContext.CmsMainMenuPriority; }
        }

        bool ICmsItemFactory.ShowInCmsMainMenu
        {
            get { return this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu; }
        }

        string ICmsItemFactory.Name
        {
            get { return this.UserSpecificGeneralInfoInContext.Name; }
        }

        EnumsCms.IMAGE_ICON ICmsItemFactory.CmsImageIcon
        {
            get { return this.UserSpecificGeneralInfoInContext.CmsImageIcon; }
        }


        [Obsolete("User 'this.UserSpecificGeneralInfoInContext.TitlePlural' instead")]
        public string TitlePlural
        {
            get { return this.UserSpecificGeneralInfoInContext.TitlePlural; }
            set { this.UserSpecificGeneralInfoInContext.TitlePlural = value; }

        }
        [Obsolete("User 'this.UserSpecificGeneralInfoInContext.TitleSingular' instead")]
        public string TitleSingular
        {
            get { return this.UserSpecificGeneralInfoInContext.TitleSingular; }
            set { this.UserSpecificGeneralInfoInContext.TitleSingular = value; }
        }


        protected override IEnumerable<object> __getAllItemsInRepository()
        {
            var dbList = this.dbFactory.GetAllItemsInRepository();
            return convertDataItemsToCmsItems(dbList);
            
        }

        public IEnumerable<TCMSItemInfo> GetAllItemsInRepository()
        {
            var list = __getAllItemsInRepository();
            return convertDataItemsToCmsItems(list);
            
        }


        IEnumerable<ICmsItemInstance> ICmsItemFactory.GetAllItemsInRepository()
        {
            return GetAllItemsInRepository();
            
        }

        #endregion

        
        #region ICmsItemFactory Members


        public void ThrowErrorIfNotUsed()
        {
            var db = dbFactory;
            if (!UsedInProject)
            {
                throw new InvalidOperationException("Cannot use this section of the CMS if it is not used in project!");

            }
        }

        #endregion



      

        #region ICmsItemFactory Members


        public bool GetUsedInProject()
        {
            return UsedInProject;
            
        }

        #endregion

        #region ICmsItemFactory Members




        #endregion

        #region ICmsItemFactoryBL Members

        IBaseDbFactory BusinessLogic_v3.Classes.Cms.CmsObjects.Factories.ICmsItemFactoryBL.GetDbFactory()
        {
            return this.dbFactory;
            
        }

        IEnumerable<BusinessLogic_v3.Classes.Cms.CmsObjects.ICmsItemInstanceBL> BusinessLogic_v3.Classes.Cms.CmsObjects.Factories.ICmsItemFactoryBL.GetAllItemsInRepository()
        {
            return this.GetAllItemsInRepository();
            
        }

        int BusinessLogic_v3.Classes.Cms.CmsObjects.Factories.ICmsItemFactoryBL.CmsMainMenuPriority
        {
            get { return this.UserSpecificGeneralInfoInContext.CmsMainMenuPriority; }
        }

        bool BusinessLogic_v3.Classes.Cms.CmsObjects.Factories.ICmsItemFactoryBL.ShowInCmsMainMenu
        {
            get { return this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu; }
        }

        BusinessLogic_v3.Classes.Cms.CmsObjects.ICmsItemInstanceBL BusinessLogic_v3.Classes.Cms.CmsObjects.Factories.ICmsItemFactoryBL.GetCmsItemFromDBObject(BaseDbObject dbObject)
        {
            return this.GetCmsItemFromDBObject((TDBItem) dbObject);
            
        }

        BusinessLogic_v3.Classes.Cms.CmsObjects.ICmsItemInstanceBL BusinessLogic_v3.Classes.Cms.CmsObjects.Factories.ICmsItemFactoryBL.GetCmsItemWithId(long id)
        {
            return this.GetCmsItemWithId(id);
        }

        BusinessLogic_v3.Classes.Cms.CmsObjects.ICmsItemInstanceBL ICmsItemFactoryBL.CreateNewItem(bool isTemporary)
        {
            return this.CreateNewItem(isTemporary);
            
        }

        BusinessLogic_v3.Classes.Cms.CmsObjects.ICmsItemInstanceBL BusinessLogic_v3.Classes.Cms.CmsObjects.Factories.ICmsItemFactoryBL.GetEmptyCmsItemInfo()
        {
            return this.GetEmptyCmsItemInfo();
            
        }

        #endregion

        #region ICmsItemFactory Members

        IEnumerable<CmsFieldBase> ICmsItemFactory.GetPropertyInfos(BusinessLogic_v3.Classes.Cms.CmsObjects.ICmsItemInstanceBL item)
        {
            return this.GetPropertyInfos((TCMSItemInfo)item);
            
        }

        #endregion

        #region ICmsItemFactory Members

        

        #endregion




        #region ICmsItemFactory Members


        ICmsItemGeneralInfo ICmsItemFactory.UserSpecificGeneralInfoInContext
        {
            get { return this.UserSpecificGeneralInfoInContext; }
        }

        #endregion

        #region ICmsItemFactoryBL Members


        BusinessLogic_v3.Classes.Cms.Factories.ICmsItemGeneralInfoBL ICmsItemFactoryBL.UserSpecificGeneralInfoInContext
        {
            get { return this.UserSpecificGeneralInfoInContext; }
        }

        #endregion

        #region ICmsItemFactory Members


        ICmsItemInstance ICmsItemFactory.GetCmsItemWithId(long id)
        {
            return this.GetCmsItemWithId(id);
            
        }

        #endregion
    }
}