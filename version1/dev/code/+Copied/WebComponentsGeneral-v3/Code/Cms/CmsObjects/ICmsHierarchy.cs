﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public interface ICmsHierarchy : IHierarchy, ITreeItem, ICmsItemInstance
    {
        //new int Priority { get; set; }
        /// <summary>
        /// This is the item that the hierarchy is linked with.  For example, for a hierarchy of categories, this would be the 'parent'
        /// </summary>
        CmsPropertyInfo SubitemLinkedProperty { get; }

    }
}
