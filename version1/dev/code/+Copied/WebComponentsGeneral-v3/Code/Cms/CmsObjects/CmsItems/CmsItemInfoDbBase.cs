﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Linq.Expressions;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.Exceptions;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;


using System.Web.UI.WebControls;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems
{
    public abstract class CmsItemInfoDbBase< TItem> : CmsItemInfo
        
        where TItem: BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject
    {
        public CmsItemInfoDbBase(ICmsItemFactory factory, TItem item)
            : base(factory, item)
        {

        }

        

        public bool InEditMode { get { return this.DbItem != null && !this.DbItem.IsTemporary() && !this.DbItem.IsTransient(); } }
        public new TItem DbItem
        {
            get
            {

                return (TItem) base.DbItem;
            }
        }
        public CmsPropertyInfo ID { get; private set; }
        public CmsPropertyInfo _Temporary_LastUpdOn { get; private set; }

        public CmsPropertyInfo _Temporary_Flag { get; private set; }

        public CmsPropertyInfo LastEditedBy { get; private set; }
        public CmsPropertyInfo LastEditedOn { get; private set; }

        public CmsPropertyInfo Deleted { get; private set; }

        public CmsPropertyInfo DeletedOn { get; private set; }

        public CmsPropertyInfo DeletedBy { get; private set; }

        public CmsPropertyInfo Priority { get; private set; }
        public CmsPropertyInfo Published { get; private set; }
        public CmsPropertyInfo PublishedOn { get; private set; }

        protected override void initBasicFields()
        {

            this.ID = base.AddProperty(

                        CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(item => item.ID),
                        isEditable: false,
                        isVisible: true
                        );


            this.LastEditedOn = base.AddProperty(

                        CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(item => item.LastEditedOn),
                        isEditable: true,
                        isVisible: true
                        );

            this.LastEditedBy = base.AddProperty(

                        CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(item => item.LastEditedBy),
                        isEditable: true,
                        isVisible: true
                        );


            this._Temporary_LastUpdOn = base.AddProperty(

                        CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(item => item._Temporary_LastUpdOn),
                        isEditable: false,
                        isVisible: false
                        );

            this._Temporary_Flag = base.AddProperty(

                        CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(item => item._Temporary_Flag),
                        isEditable: false,
                        isVisible: false
                        );

            this.Deleted = base.AddProperty(

                        CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(item => item.Deleted),
                        isEditable: true,
                        isVisible: true
                        );
            this.Published = base.AddProperty(

                        CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(item => item.Published),
                        isEditable: true,
                        isVisible: true
                        );
            this.PublishedOn = base.AddProperty(

                       CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(item => item.PublishedOn),
                       isEditable: true,
                       isVisible: true
                       );
            
            this.PublishedOn.DateShowTime = true;
            this.Deleted.SetDefaultSearchValue(false);
            this.DeletedOn = base.AddProperty(

                        CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(item => item.DeletedOn),
                        isEditable: true,
                        isVisible: true
                        );

            this.DeletedBy = base.AddProperty(

                        CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(item => item.DeletedBy),
                        isEditable: true,
                        isVisible: true
                        );

            this.Priority = base.AddProperty(

                        CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(item => item.Priority),
                        isEditable: true,
                        isVisible: true
                        );
            this.Priority.WidthInListing = 30;
            this.Priority.EditableInListing = true;
            this.Priority.ShowInListing = false;
            this.Priority.HelpMessage = "This is a numerical value used for sorting.  Use if you want to specify a custom sorting order.  If priorities of two pages have the same numerical value, e.g both are 10, then they are sorted by their Title.";

            this.LastEditedBy.ShowInEdit = true;
            this.LastEditedBy.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam);
            this.LastEditedOn.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam);


            base.initBasicFields();
        }
        /// <summary>
        /// This should be used when the many-to-many link is directly implemented via nhibernate, e.g a Category.Features, where there is no intermediate class in between.  If in the case of
        /// content pages, you have the ContentPageParentChildLink class in between, use the other generic property
        /// </summary>
        /// <typeparam name="TLinkedItem"></typeparam>
        /// <param name="linkedWithFactory"></param>
        /// <param name="property"></param>
        /// <param name="displayType"></param>
        /// <returns></returns>
        public CmsPropertyManyToManyCollection<TCollectionType> AddManyToManyCollectionWithDirectProperty<TCollectionType>
            
            (
            Expression<Func<TItem, ICollectionManager>> selector,
            EnumsCms.MULTICHOICE_DISPLAY_TYPE displayType)
            // where TItem: ICmsItemInstance
            where TCollectionType : ICmsItemInstance
        {

            var p = new CmsPropertyManyToManyCollection<TCollectionType>(this, CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(selector), displayType);
            this.AddProperty(p);
            return p;
        }


        public void SetShowInListingAndOrder(params CmsFieldBase[] properties)
        {
            foreach (var p in properties)
            {
                p.ShowInListing = true;
            }
            this.ListingOrderPriorities.AddColumnsToStart(properties);
        }
    }
}