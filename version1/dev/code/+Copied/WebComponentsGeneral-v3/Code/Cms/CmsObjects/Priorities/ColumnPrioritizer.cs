﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.General_v3.Util;
namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects
{
    public class ColumnPrioritizer
    {
        private List<ICmsFieldBase> _properties;
        public ColumnPrioritizer()
        {
            _properties = new List<ICmsFieldBase>();
        }
        private void addColumns(IEnumerable<ICmsFieldBase> cmsProperties, int index)
        {
            var listCmsProperties = cmsProperties.ToList();
            for (int i = listCmsProperties.Count - 1; i >= 0; i--)
            {
                var p = listCmsProperties[i];

                if (checkIfPropertyAlreadyExists(p))
                {//if existing property, and this property is before the index to add, reduce index by 1
                    int existingIndex = _properties.FindIndex(item => item == p);
                    if (existingIndex < index)
                    {
                        index--;
                    }
                    removePropertyIfExists(p);
                }
                _properties.Insert(index, p);
               // index++;
            }
        }

       
        public void AddColumnsToStart(params ICmsFieldBase[] cmsProperties)
        {
            this.AddColumnsToStart((IEnumerable<ICmsFieldBase>)cmsProperties);
        }
        public void AddColumnsToStart(IEnumerable<ICmsFieldBase> cmsProperties)
        {
            addColumns(cmsProperties, 0);
        }

        public void AddColumnBefore(ICmsFieldBase p, ICmsFieldBase propertyToAddBefore)
        {
            int existingIndex = _properties.FindIndex(item => item == propertyToAddBefore);
            
            if (existingIndex > -1)
            {
                addColumns(CS.General_v3.Util.ListUtil.GetListFromSingleItem(p), existingIndex);
            }
        }
        public void AddColumnAfter(ICmsFieldBase p, ICmsFieldBase propertyToAddAfter)
        {
            int existingIndex = _properties.FindIndex(item => item == propertyToAddAfter);

            if (existingIndex > -1)
            {
                addColumns(CS.General_v3.Util.ListUtil.GetListFromSingleItem(p), existingIndex+1);
            }
        }

        public void AddColumnsToEnd(params ICmsFieldBase[] cmsProperties)
        {
            this.AddColumnsToEnd((IEnumerable<ICmsFieldBase>)cmsProperties);
        }
        public void AddColumnsToEnd(IEnumerable<ICmsFieldBase> cmsProperties)
        {
            addColumns(cmsProperties, _properties.Count);
        }
        private bool checkIfPropertyAlreadyExists(ICmsFieldBase p)
        {
            return _properties.FindElem(item => item == p) != null;
        }
        private void removePropertyIfExists(ICmsFieldBase p)
        {
            _properties.Remove(p);
        }


        public int GetPriorityFor(ICmsFieldBase property)
        {
            int priority = 100;
            bool matched = false;
            for (int i = 0; i < _properties.Count; i++)
            {
                if (_properties[i] == property)
                {
                    matched = true;
                    break;
                }

                priority += 100;
            }
            if (!matched)
                priority = Int32.MaxValue;
            return priority;

        }

    }
}
