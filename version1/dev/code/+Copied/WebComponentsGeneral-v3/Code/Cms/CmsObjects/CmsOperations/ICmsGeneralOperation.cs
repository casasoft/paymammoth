using System;
namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects
{
    public interface ICmsGeneralOperation
    {
        string Title { get; set; }
        EnumsCms.IMAGE_ICON Icon { get; set; }
        string ConfirmMessage { get; set; }
        string Href { get; set; }
        event Action OnClick;
        bool IsLink();
        bool IsButton();
    }
}