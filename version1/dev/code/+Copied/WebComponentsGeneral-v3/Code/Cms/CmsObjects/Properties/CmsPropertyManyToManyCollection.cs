﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes;
using System.Web.UI.WebControls;
using System.Web.UI;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.HelperClasses;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties
{
    public class CmsPropertyManyToManyCollection<TCollectionItem> : CmsPropertyMultiChoice
        
        where TCollectionItem: ICmsItemInstance
    
       
    {

        public ICmsItemFactory LinkedWithFactory { get; private set; }
       // public PropertyInfo PropertyInfo { get; set; }

        public CmsPropertyManyToManyCollection(ICmsItemInstance cmsItem, 
            PropertyInfo property,  
            EnumsCms.MULTICHOICE_DISPLAY_TYPE displayType)

            : base(cmsItem, property, displayType)
        {
            this.LinkedWithFactory = (ICmsItemFactory) BusinessLogic_v3.Classes.Cms.Factories.CmsFactoriesController.Instance.GetFactoryForType(typeof(TCollectionItem));
            if (this.LinkedWithFactory == null)
            {
                //this was removed in order to allow for the many-to-many collections which are not used in project
             //   throw new InvalidOperationException("No cms factory exists for type <" + typeof (TCollectionItem).FullName + ">");
            }

        }
        public override bool IsUsedInProject()
        {
            return base.IsUsedInProject() && this.LinkedWithFactory != null;
        }
        protected ICollectionManager getCollectionManager(ICmsItemInstance item)
        {
            return (ICollectionManager)this.PropertyInfo.GetValue(item.DbItem, null);
        }


        // private Action<IBaseDbObject> clearCollection;
      //  private Action<IBaseDbObject, BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject> addItemToCollection;

        private OperationResult setValueFromString(ICmsItemInstance item, IEnumerable<string> sValues)
        {
            var collManager = getCollectionManager(item);
            OperationResult result = new OperationResult();
            collManager.Clear();
            
            
            
            
            foreach (var token in sValues)
            {
                long id;
                if (long.TryParse(token.Trim(), out id))
                {
                    var linkedItem = LinkedWithFactory.GetCmsItemWithId(id);
                    if (linkedItem != null)
                    {
                        TCollectionItem castedlinkedItem = (TCollectionItem)linkedItem;
                        collManager.Add(castedlinkedItem.DbItem);
                        //addItemToCollection(item.DbItem, castedlinkedItem.DbItem);
                        //list.Add(castedlinkedItem.DbItem);
                    }
                }
            }
            //this.PropertyInfo.SetValue(item, list, null);
            //SetValueForObject(item, currentList);
            
            return result;

        }
        protected override IEnumerable<string> getSelectedValuesForItem(ICmsItemInstance item)
        {
            return GetValueForObject(item).ToList().ConvertAll<string>(c => c.ID.ToString());
                
            
        }

        protected override void setValueForCmsItem(ICmsItemInstance item, IEnumerable<string> values)
        {
            setValueFromString(item, values);
            
        }

        protected override IEnumerable<ListItem> getListItemChoices(ICmsItemInstance item, bool sortByText = true)
        {
            return LinkedWithFactory.GetAllAsListItemsForCms(this, false, 0, false, showNonPublished: false);
            
        }

        public new ICollection<IBaseDbObject> GetValueForObject(ICmsItemInstance item)
        {
            List<IBaseDbObject> list = new List<IBaseDbObject>();
            var dataList = GetValueForObject<IEnumerable<IBaseDbObject>>(item);
            list.AddRange(dataList);
            return list;

        }

        public override TDataType GetValueForObject<TDataType>(ICmsItemInstance o)
        {
            return (TDataType)this.PropertyInfo.GetValue(o.DbItem, null);
        }

        protected override Control getControlNonEditable(ICmsItemInstance item)
        {
            var selectedList = GetValueForObject(item);
            StringBuilder sb = new StringBuilder();
            foreach (var selItem in selectedList)
            {
                if (sb.Length > 0) sb.Append(", ");

                var cmsItem = LinkedWithFactory.GetCmsItemFromDBObject((BaseDbObject)selItem);
                sb.Append(cmsItem.TitleForCms);
            }
            LiteralControl lit = new LiteralControl();
            lit.Text = sb.ToString();
            return lit;
        }
    }
}