﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using BusinessLogic_v3.Classes.DB;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using System.Web.UI;
using BusinessLogic_v3.DB;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Controls.WebControls.Common;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public abstract class CmsPropertyGeneral  : CmsPropertyBase
    {
        public Type propertyType { get; protected set; }
        //public Enums.CMS_DATA_TYPE DataType { get; protected set; }
        
        public CmsPropertyGeneral(ICmsItemFactory itemGeneralInfo)
            : base(itemGeneralInfo)
        {
            
            
            
        }

        
        
      

        /// <summary>
        /// Delegate that returns the list items for the property in question.  Please note that item CAN BE NULL
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public delegate ListItemCollection CustomListItemCollectionDelegate(CmsPropertyGeneral propertyInfo, ICmsItemInstance item, EnumsCms.SECTION_TYPE sectionType);
        public event CustomListItemCollectionDelegate CustomListItemCollectionRetriever;

        public abstract ICmsItemFactory GetFactoryForLinkedObject();

        public override object GetFormValueConvertedToPropertyDataType(object formValue, EnumsCms.SECTION_TYPE sectionType)
        {
            object o = formValue;
            object result = null;
            switch (this.DataType)
            {
                case EnumsCms.CMS_DATA_TYPE.Bool:
                case EnumsCms.CMS_DATA_TYPE.BoolNullable:
                    if (sectionType == EnumsCms.SECTION_TYPE.SearchInListing)
                    {
                        result = CS.General_v3.Util.Other.TextToBoolNullable((string)o);

                    }
                    else
                    {
                        result = o;
                    }
                    break;


                case EnumsCms.CMS_DATA_TYPE.Integer:
                case EnumsCms.CMS_DATA_TYPE.IntegerNullable:
                    {
                        result = o;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.Enumeration:
                    {
                        string s = (o != null ? o.ToString() : null);
                        // result = null;
                        int num = 0;
                        if (int.TryParse(s, out num))
                            result = num;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.LinkedObject:
                    { //this means a linked object
                        string s = (o != null ? o.ToString() : null);
                        // result = null;
                        int num = 0;
                        if (!int.TryParse(s, out num))
                            num = 0;
                        if (num > 0)
                        {
                            var factory = GetFactoryForLinkedObject();
                            var item = factory.GetCmsItemWithId(num).DbItem;
                            result = item;
                        }
                        break;
                        // break;
                    }

                case EnumsCms.CMS_DATA_TYPE.Long:
                case EnumsCms.CMS_DATA_TYPE.LongNullable:
                    {
                        result = o;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.Double:
                case EnumsCms.CMS_DATA_TYPE.DoubleNullable:
                    {
                        result = o;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.String:
                    {
                        string s = (string)o;
                        if (sectionType == EnumsCms.SECTION_TYPE.SearchInListing && string.IsNullOrEmpty(s))
                            s = null;
                        result = s;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.DateTime:
                case EnumsCms.CMS_DATA_TYPE.DateTimeNullable:
                    {
                        result = o;
                        break;
                    }
                default:
                    throw new InvalidOperationException("Invalid data type");
            }
            return result;
        }


        public virtual System.Web.UI.WebControls.ListItemCollection GetListItemCollectionForDataType(EnumsCms.SECTION_TYPE sectionType)
        {
            ListItemCollection listItems = null;
            
            switch (this.DataType)
            {
                case EnumsCms.CMS_DATA_TYPE.Enumeration:
                    {
                        listItems = CS.General_v3.Util.EnumUtils.GetListItemCollectionFromEnum(this.propertyType, (!this.GetRequiredValueBasedOnSectionType(sectionType) ? "" : null), addSpacesToCamelCasedName: true);
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.LinkedObject:
                    {
                        if ((typeof(BusinessLogic_v3.Classes.DB.IBaseDbObject)).IsAssignableFrom(this.propertyType))
                        {
                            //this is an item link
                            var factory = CmsInfoBase.Instance.GetFactoryForType(this.propertyType);
                            if (factory == null)
                                throw new InvalidOperationException("Factory not registered for property with type " + this.propertyType.ToString());
                            else
                            {
                                listItems = new ListItemCollection();
                                var listItemArray = factory.GetAllAsListItemsForCms(this, !this.GetRequiredValueBasedOnSectionType(sectionType), 0, false);
                                listItems.AddRange(listItemArray);
                            }
                        }
                        else
                        {
                            throw new InvalidOperationException("Unsupported data type");
                        }
                        break;
                    }
                default:
                    throw new InvalidOperationException("this cannot be called if data type is not an enumeration or linked object");
            }
            return listItems;

        }
        
        /// <summary>
        /// Returns the list item collection to show in the combo box
        /// </summary>
        /// <returns></returns>
        public virtual System.Web.UI.WebControls.ListItemCollection GetListItemCollectionForDataTypeAndItem(ICmsItemInstance item, EnumsCms.SECTION_TYPE sectionType)
        {
            if (this.DataType != EnumsCms.CMS_DATA_TYPE.Enumeration && this.DataType != EnumsCms.CMS_DATA_TYPE.LinkedObject)
                throw new InvalidOperationException("this cannot be called if data type is not an enumeration or linked object");
            ListItemCollection listItems = null;
            if (this.CustomListItemCollectionRetriever == null)
            {
                listItems = GetListItemCollectionForDataType(sectionType);

            }
            else
                listItems = this.CustomListItemCollectionRetriever(this, item, sectionType);
            return listItems;
        }
        public override FormFieldBaseData GetFormFieldBaseDataForProperty(EnumsCms.SECTION_TYPE sectionType,ICmsItemInstance item ,
         bool loadInitialValueFromPostback = false, object value = null)
        {
            var p = this;

            int width = 0;
            if (sectionType == EnumsCms.SECTION_TYPE.ListingPage)
                width = this.WidthInListing.GetValueOrDefault(100);
            else
                width = this.WidthInListing.GetValueOrDefault(600);

            FormFieldBaseData fieldBase = null;
            switch (p.DataType)
            {
                case EnumsCms.CMS_DATA_TYPE.MediaItem:
                    {

                        break;
                    }

                case EnumsCms.CMS_DATA_TYPE.Bool:
                    {
                        if (sectionType != EnumsCms.SECTION_TYPE.SearchInListing)
                        {
                            FormFieldBoolData field = new FormFieldBoolData(); fieldBase = field;
                        }
                        else
                        {

                            FormFieldListSingleChoiceDropdownData field = p.GetFormFieldForBoolNullable(); fieldBase = field;

                        }
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.BoolNullable:
                    {

                        FormFieldListSingleChoiceDropdownData field = p.GetFormFieldForBoolNullable(); fieldBase = field;
                        break;
                    }

                case EnumsCms.CMS_DATA_TYPE.DateTime:
                case EnumsCms.CMS_DATA_TYPE.DateTimeNullable:
                    {
                        FormFieldDateData field = new FormFieldDateData(); fieldBase = field;
                        field.ShowTime = this.DateShowTime;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.Double:
                case EnumsCms.CMS_DATA_TYPE.DoubleNullable:
                    {
                        FormFieldNumericDoubleData field = new FormFieldNumericDoubleData(); fieldBase = field;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.Integer:
                case EnumsCms.CMS_DATA_TYPE.IntegerNullable:
                case EnumsCms.CMS_DATA_TYPE.Long:
                case EnumsCms.CMS_DATA_TYPE.LongNullable:
                    {
                        FormFieldNumericIntegerData field = new FormFieldNumericIntegerData(); fieldBase = field;

                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.String:
                    {
                        FormFieldStringBaseData field = FormFieldStringBaseData.GetFormFieldStringBaseDataFromStringDataType(p.StringDataType);

                        fieldBase = field;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.Enumeration:
                case EnumsCms.CMS_DATA_TYPE.LinkedObject:
                    {
                        FormFieldListSingleChoiceDropdownData field = new FormFieldListSingleChoiceDropdownData(); fieldBase = field;
                        var listItems = p.GetListItemCollectionForDataTypeAndItem(item, sectionType);
                        field.ListItems = listItems;
                        break;

                    }
                default:
                    throw new InvalidOperationException("Unsupported data type");
            }

            fillFormFieldBaseDataFromProperty(fieldBase, sectionType);

            
            fieldBase.ID = getControlID(sectionType, item);
            if (value is BusinessLogic_v3.Classes.DB.IBaseDbObject )
            {
                BusinessLogic_v3.Classes.DB.IBaseDbObject cmsItem = (BusinessLogic_v3.Classes.DB.IBaseDbObject)value;
                fieldBase.InitialValue = cmsItem.ID;
                
            }
            else
            {
                fieldBase.InitialValue = value;
            }
            fieldBase.Width = width;
            if (sectionType == EnumsCms.SECTION_TYPE.SearchInListing)
                fieldBase.Required = false;
            fieldBase.LoadInitialValueFromPostback = loadInitialValueFromPostback;
            fieldBase.FormFieldCssClass.AddClass(p.GetFieldCssClassForSectionType(sectionType));
            
            return fieldBase;
        }
        public override OperationResult SetValueForObjectFromFormControl(ICmsItemInstance o, CS.General_v3.Controls.WebControls.Common.General.IMyFormWebControl formControl)
        {
            object value = formControl.FormValueObject;
            var convertedValue = this.GetFormValueConvertedToPropertyDataType(value, EnumsCms.SECTION_TYPE.EditPage);
            var result = SetValueForObject(o, convertedValue);
            return result;


        }
        
        protected override System.Web.UI.Control getControlNonEditable(ICmsItemInstance item)
        {
            var p = this;
            var loggedUser = CMSUserSessionLogin.Instance.GetLoggedInUser();
            Control ctrl = null;
            object value = p.GetValueForObject(item);
            LiteralControl lit = new LiteralControl();
            lit.Text = "&nbsp;";
            ctrl = lit;
            if (loggedUser != null && loggedUser.CheckAccess(this.GetAccessTypeRequiredToView(item)))
            {
                string s = Util.CmsUtil.GetToStringValueForObject(value);
                if (this.DataType != EnumsCms.CMS_DATA_TYPE.LinkedObject)
                {
                    lit.Text = CS.General_v3.Util.Text.HtmlEncode(s);
                }
                else
                {
                    
                    

                    BusinessLogic_v3.Classes.DB.IBaseDbObject dbObject = (BusinessLogic_v3.Classes.DB.IBaseDbObject)value;
                    if (dbObject != null)
                    {
                        MyAnchor anchor = new MyAnchor();
                        anchor.Text = CS.General_v3.Util.Text.HtmlEncode(s);
                        var linkedFactory = this.GetFactoryForLinkedObject();

                        anchor.Href = linkedFactory.GetEditUrlForItemWithID(dbObject.PrimaryKey).ToString();
                        ctrl = anchor;
                    }
                }
            }
            
            return ctrl;
        }

     




    }
}