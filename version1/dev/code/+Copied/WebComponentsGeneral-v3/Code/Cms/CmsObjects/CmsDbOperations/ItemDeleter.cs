﻿using System.Text;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Cms;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using CS.General_v3.Classes.HelperClasses;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsDbOperations
{
    public class ItemDeleter
    {
       
        public ItemDeleter(ICmsItemInstance item, EnumsCms.SECTION_TYPE sectionType)
        {
            this.Item = item;
            this.SectionType = sectionType;
            
        }
        public EnumsCms.SECTION_TYPE SectionType { get; set; }
        public ICmsItemInstance Item { get; set; }


        //private void writeToAuditLog()
        //{

        //    string msg = "";


        //    msg = Item.DbItem.GetType().Name + " ";
        //    switch (_result.Status)
        //    {
        //        case CS.General_v3.Enums.STATUS_MSG_TYPE.Success: msg += " deleted successfully";break;
        //        case CS.General_v3.Enums.STATUS_MSG_TYPE.Error: msg += " could not be deleted";break;
        //        case CS.General_v3.Enums.STATUS_MSG_TYPE.Warning: msg += " deleted with warning";break;
        //    }
        //    StringBuilder furtherInfo = new StringBuilder();
        //    furtherInfo.AppendLine("Status: " + CS.General_v3.Util.EnumUtils.StringValueOf(_result.Status));
        //    if (_result.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
        //    {
        //        furtherInfo.AppendLine();
        //        furtherInfo.AppendLine(_result.GetMessageAsString());
        //    }
            
        //    CmsUtil.AddAuditLog(Enums.CMS_AUDIT_MESSAGE_TYPE.Delete,
        //                                 Item.DbItem.GetType().Name, Item.DbItem.ID,
        //                                 msg, furtherInfo.ToString());
            


        //}

        private OperationResult _result = null;
        public OperationResult DeleteDbItem()
        {
            _result = new OperationResult();

            ICmsItemInstance item = this.Item;
            var deleteResult = item.DeleteFromCms();
            _result.AddFromOperationResult(deleteResult);


            //writeToAuditLog();
            return _result;
        }

    }
}
