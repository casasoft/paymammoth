﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects
{
    public class CmsItemSpecificOperation : CmsGeneralOperation
    {
        

        public CmsItemSpecificOperation(ICmsItemInstance item, string title, EnumsCms.IMAGE_ICON icon, string href) : base(title,icon,href)
        {
            this.Item = item;
        }

        public CmsItemSpecificOperation(ICmsItemInstance item, string title, EnumsCms.IMAGE_ICON icon, Action delegateToCall)
            : base(title, icon, delegateToCall)
        {
            this.Item = item;

        }



        public ICmsItemInstance Item { get; private set; }


    }
}
