﻿using System;
using System.Collections.Generic;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Classes.HelperClasses;


namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects
{
    public interface ICmsItemInstance : BusinessLogic_v3.Classes.Cms.CmsObjects.ICmsItemInstanceBL
    {
        OperationResult DeleteFromCms();
        ColumnPrioritizer GetListingOrderPrioritizer();
        ColumnPrioritizer GetEditOrderPrioritizer();
        //ICMSPropertySpecificInfo GetPropertySpecificInfoForProperty(CMSPropertyGeneralInfo propertyInfo);
        IEnumerable<CmsFieldBase> GetCmsProperties(bool getOnlyUsedInProject = true);
        
        //int Priority { get; set; }
        OperationResult SaveFromCms(SaveParams saveParams = null);
        string TitleForCms { get;  }

        //CmsAccessType AccessTypeRequired_ToDelete {get;}
        //CmsAccessType AccessTypeRequired_ToView { get; }
        //CmsAccessType AccessTypeRequired_ToEdit { get; }

        IEnumerable<CmsItemSpecificOperation> GetCustomCmsOperations();




        CmsFieldBase GetDefaultSortField(out General_v3.Enums.SORT_TYPE sortType);
        bool IsMultilingual();
        bool ContainsUntranslatedMultilingualText();
    }
}
