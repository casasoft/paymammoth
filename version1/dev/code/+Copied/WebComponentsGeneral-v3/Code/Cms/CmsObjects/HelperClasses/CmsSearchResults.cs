﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects
{
    public class CmsSearchResults
    {
        public IEnumerable<ICmsItemInstance> SearchItems { get; set; }
        public int TotalCount { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }


    }
}
