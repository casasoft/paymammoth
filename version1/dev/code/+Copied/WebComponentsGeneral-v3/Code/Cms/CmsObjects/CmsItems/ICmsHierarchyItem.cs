﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems
{
    public interface ICmsHierarchyItem 
    {
        ICmsHierarchyInfo GetCmsHierarchyInfo();

    }
}
