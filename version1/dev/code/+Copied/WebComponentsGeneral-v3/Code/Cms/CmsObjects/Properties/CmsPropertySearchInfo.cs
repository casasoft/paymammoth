﻿using CS.WebComponentsGeneralV3.Code.Cms.Util;
using NHibernate.Criterion;
using CS.General_v3.Classes.URL;
using NHibernate;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties
{
    public class CmsPropertySearchInfo
    {
        public CmsPropertySearchInfo(CmsFieldBase pInfo, FormFieldsFieldItemDataImpl formField)
        {


            this.PropertyInfo = pInfo;
            this.FormField = formField;
          //  initValue();

            object o = GetSearchValue();
            if (o != null && HasFormField)
            {
                object value = o;
                if (o is ICmsItemInstance)
                {
                    ICmsItemInstance cmsItem = (ICmsItemInstance)o;
                    value = cmsItem.DbItem.ID;
                }
                else if (o is BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject)
                {
                    BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject dbObj = (BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject)o;
                    value = dbObj.ID;
                }
                this.FormField.Control.WebControlFunctionality.InitialValue = value;
            }
        }
        public bool HasFormField
        {
            get { return this.FormField != null; }
        }
        public bool ShowInSearch
        {
            get
            {
                var loggedInUser = CmsUtil.GetLoggedInUser();
                return this.PropertyInfo.CheckIfCanSearch() && loggedInUser != null && loggedInUser.CheckAccess(PropertyInfo.AccessTypeRequired_View);
            }
        }
        private object getFormValueConvertedToPropertyDataType()
        {
            object result = null;
            if (HasFormField)
            {
                object formValue = this.FormField.Control.GetFormValueObject();

                result = this.PropertyInfo.GetFormValueConvertedToPropertyDataType(formValue, EnumsCms.SECTION_TYPE.SearchInListing);

            }

            return result;
        }
      

        public void UpdateValueFromForm(URLClass url)
        {
            var o = getFormValueConvertedToPropertyDataType();
            string s = null;
            if (o is BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject)
            {
                BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject item = (BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject)o;
                s = item.ID.ToString();
            }
            else
            {
                s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(o);
            }
            if (s != null)
            {
                int k = 5;
            }
            updateUrl(url, s);
            
        }


        public CmsFieldBase PropertyInfo { get; set; }
        public FormFieldsFieldItemDataImpl FormField { get; set; }

        private string getQueryStringKey()
        {
            string key =  "Search_"+ this.PropertyInfo.GetPropertyIdentifier();
            return key;

        }

        private void updateUrl(URLClass url, string value)
        {
            string key = getQueryStringKey();
            if (value == null)
                url.Remove(key);
            else
                url[key] = value;

        }

        //private void saveInSession(object value)
        //{
        //    string key = this.PropertyInfo.GetPropertyIdentifier();
        //    CS.General_v3.Util.PageUtil.SetSessionObject("cmsSearch_" + key, value);

        //}
       /* private object loadSearchValueFromSession()
        {
            string key = this.PropertyInfo.GetPropertyIdentifier();
            return CS.General_v3.Util.PageUtil.GetSessionObject("cmsSearch_" + key);
            
        }*/


        public bool HasSearchValue()
        {
            string s = GetSearchValue(returnDefaultIfEmpty:false);
            return !string.IsNullOrEmpty(s);

        }
        /// <summary>
        /// This is used to specify a custom-search value for this property.
        /// </summary>
        public string CustomSearchValue { get; set; }

        /// <summary>
        /// Returns the search value
        /// </summary>
        /// <param name="returnDefaultIfEmpty">Whether to return the default search value, if empty</param>
        /// <returns></returns>
        public string GetSearchValue(bool returnDefaultIfEmpty = true)
        {
            if (CustomSearchValue == null)
            {
                string key = getQueryStringKey();
                string s = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(key);

                if (returnDefaultIfEmpty && s == null && this.PropertyInfo.DefaultSearchValueStr != null)
                    s = this.PropertyInfo.DefaultSearchValueStr;
                //object o =   loadSearchValueFromSession();
                return s;
            }
            else
            {
                return this.CustomSearchValue;
            }
        }


        public void ClearSearchValue(URLClass url)
        {
            string key = getQueryStringKey();
            url.Remove(key);
            // saveInSession(null);
        }

        public void AddCriterionToSearchCriteria(ICriteria crit)
        {
            string searchValue = this.GetSearchValue();
            if (!string.IsNullOrWhiteSpace(searchValue ))
            {
                this.PropertyInfo.AddCriterionToSearchCriteria(crit, searchValue);
            }


        }
        public override string ToString()
        {
            string s = "";
            if (this.PropertyInfo != null)
                s += this.PropertyInfo.ToString() + ": ";
            s += this.GetSearchValue();
            return s;
        }
    }
}