﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.General_v3.Classes.Interfaces.Hierarchy;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructure;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems
{
    public interface ICmsHierarchyInfo : IHierarchy, ITreeItem, ICmsItemInstance
    {
        //new int Priority { get; set; }
        /// <summary>
        /// This is the item that the hierarchy is linked with.  For example, for a hierarchy of categories, this would be the 'parent'
        /// </summary>
        ICmsCollectionInfo SubitemLinkedCollection { get; }
        string GetTitleInHierarchy();
    }
}
