﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using System.Web.UI.WebControls;
using CS.General_v3.Classes.HelperClasses;
using NHibernate.Criterion;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes
{
    using Controls.WebControls.Common;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls;
    using System.Collections;

    public abstract class CmsPropertyMultiChoice : CmsFieldBase
        
       
    {



      //  public ICmsItemFactory LinkedWithFactory { get; private set; }
        public PropertyInfo PropertyInfo { get; set; }

        public EnumsCms.MULTICHOICE_DISPLAY_TYPE DisplayType { get; set; }

        public CmsPropertyMultiChoice(ICmsItemInstance cmsItem, PropertyInfo propertyInfo, EnumsCms.MULTICHOICE_DISPLAY_TYPE displayType)
            : base(cmsItem)

        {
            this.DisplayType = displayType;
            //CS.General_v3.Util.ContractsUtil.RequiresNotNullable(propertyInfo, "Property info must be filled in");
          //  this.LinkedWithFactory = factoryLinkedWith;

            this.IsSearchable = false;
            this.TotalColumns = 3;
            this.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this.ItemsToShowInList = 10;
            setProperty(propertyInfo);
            
        }
        protected void setProperty(PropertyInfo p)
        {
            this.PropertyInfo = p;
            if (this.PropertyInfo != null)
            {
                this.Label = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(PropertyInfo.Name);
            }
        }

        /// <summary>
        /// Only applies for checkboxes
        /// </summary>
        public int TotalColumns { get; set; }
        /// <summary>
        /// Only applies for checkboxes
        /// </summary>
        public RepeatDirection RepeatDirection { get; set; }

        public override bool EditableInListing
        {
            get
            {
                return false;
            }
            set
            {

            }
        }
        public override bool IsSearchable
        {
            get
            {
                return base.IsSearchable;
            }
            set
            {
                base.IsSearchable = value;
            }
        }
        /*
        public override bool ShowInListing
        {
            get
            {
                return false;
            }
            set
            {
                base.ShowInListing = false;
            }
        }*/

        protected abstract IEnumerable<string> getSelectedValuesForItem(ICmsItemInstance item);

        public new IEnumerable<string> GetValueForObject(ICmsItemInstance item)
        {
            return getSelectedValuesForItem(item);
            
        }

        public override TDataType GetValueForObject<TDataType>(ICmsItemInstance o)
        {
            return (TDataType)this.PropertyInfo.GetValue(o.DbItem, null);
        }


        protected abstract void setValueForCmsItem(ICmsItemInstance item, IEnumerable<string> values);
        

        private OperationResult setValueFromString(ICmsItemInstance item, string sValues)
        {
            OperationResult result = new OperationResult();

          //  var currentList = GetValueForObject(item);
          //  currentList.Clear();
            sValues = sValues?? "";
            string[] sTokens = sValues.Split(new string[] { ","},  StringSplitOptions.RemoveEmptyEntries);
            setValueForCmsItem(item, sTokens);
            return result;

        }
        public override OperationResult SetValueForObjectFromFormControl(ICmsItemInstance o, IMyFormWebControl formControl)
        {
            var obj = formControl.GetFormValueObject();
            string s = null;
            if (obj is string)
            {
                s = (string) obj;
            }
            else if (obj is IEnumerable)
            {
                IEnumerable list = (IEnumerable)obj;
                s = CS.General_v3.Util.ListUtil.JoinList(list, ",");
            }

            return setValueFromString(o, s);

            
        }
        public override OperationResult SetValueForObject(ICmsItemInstance o, object value)
        {
            if (value is string)
                return setValueFromString(o, (string)value);
            else
            {
                throw new NotImplementedException("This can only be called with string values");
            }
            
        }


        public int ItemsToShowInList { get; set; }

        public override object GetFormValueConvertedToPropertyDataType(object formValue, EnumsCms.SECTION_TYPE sectionType)
        {
            

            throw new NotImplementedException();
        }
        protected abstract IEnumerable<ListItem> getListItemChoices(ICmsItemInstance item, bool sortByText = true);
       

        /*protected override Control getControlNonEditable(ICmsItemInstance item)
        {
            var selectedList = GetValueForObject(item);
            StringBuilder sb = new StringBuilder();
            foreach (var selItem in selectedList)
            {
                if (sb.Length > 0) sb.Append(", ");

                var cmsItem = LinkedWithFactory.GetCmsItemFromDBObject((BaseDbObject) selItem);
                sb.Append(cmsItem.TitleForCms);
            }
            LiteralControl lit = new LiteralControl();
            lit.Text = sb.ToString();
            return lit;
        }*/

        
        public override void AddCriterionToSearchCriteria(NHibernate.ICriteria criteria, string searchValue)
        {
            NHibernate.Criterion.ICriterion crit = null;

            List<long> ids = new List<long>();
            List<string> sIDs = CS.General_v3.Util.Text.Split(searchValue, ",", "|");
            foreach (var sID in sIDs)
            {
                long id;
                if (long.TryParse(sID, out id))
                {
                    ids.Add(id);
                }
            }
            if (ids.Count > 0)
            {
                crit = NHibernate.Criterion.Expression.Eq(this.PropertyInfo.Name, ids[0]);
                //crit = Restrictions.InG<long>(this.PropertyInfo.Name, ids);
            }
            if (crit != null)
                criteria.Add(crit);
            
            

        }
        public override void ApplyOrderByToCriteria(NHibernate.ICriteria crit, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            throw new NotImplementedException("Should never be called");
        }

        private FormFieldsFieldItemDataImpl getFormFieldForSearch(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {


            
            MyDropDownList cmb = new MyDropDownList();
            FormFieldsFieldItemDataImpl field = new FormFieldsFieldItemDataImpl(cmb);


            //FormFieldListSingleChoiceDropdownData field = new FormFieldListSingleChoiceDropdownData();
            var listItems = this.getListItemChoices(null, sortByText: true).ToList();
            listItems.Insert(0, new ListItem("", "")); // add blank option on top

            cmb.Functionality.Items.AddRange(listItems);

            return field;
        }
        private FormFieldsFieldItemDataImpl getFormFieldAsCheckboxList(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {

            MyListBox multiList = new MyListBox();


            FormFieldsFieldItemDataImpl field = new FormFieldsFieldItemDataImpl(multiList);
            if (item != null)
            {

                var allListItems = getListItemChoicesAndMarkSelected(item);

                multiList.FieldParameters.jQueryMultiSelectParams = new Code.Classes.Javascript.UI.jQuery.MultiSelect._jQueryMultiSelectParams();
                multiList.Functionality.Items.AddRange(allListItems.ToArray());
            }
            return field;
            /*

            FormFieldMulipleChoiceDataAsChkBoxes field = new FormFieldMulipleChoiceDataAsChkBoxes();
            field.RepeatDirection = this.RepeatDirection;
            field.TotalColumns = this.TotalColumns;
            //field.HelpMessage = "To select multiple items, hold CTRL and select items.";
            var chkBoxList = field.GetField();

            if (item != null)
            {
                
                var allListItems = getListItemChoicesAndMarkSelected(item);


                chkBoxList.Items.AddRange(allListItems.ToArray());
            }
            return field;*/
        }

        private IEnumerable<ListItem> getListItemChoicesAndMarkSelected(ICmsItemInstance item)
        {
            var currentSelectedList = GetValueForObject(item).ToList();
            List<ListItem> selectedItems = new List<ListItem>();
            List<ListItem> nonSelectedItems = new List<ListItem>();

            var allListItems = getListItemChoices(item).ToList();
            //  listBox.SelectedItem = null;
            for (int i=0; i < allListItems.Count;i++)
            
            {
                var listItem = allListItems[i];
                listItem.Attributes.Remove("selected");
                listItem.Selected = false;
                foreach (var selItem in currentSelectedList)
                {
                    if (string.Compare(listItem.Value, selItem, true) == 0)
                    {
                        selectedItems.Add(listItem);
                        listItem.Attributes["selected"] = "selected";
                        listItem.Selected = true;
                        allListItems.Remove(listItem);
                        i--;
                        break;
                    }

                }
            }
            nonSelectedItems.AddRange(allListItems);
            allListItems.Clear();


            for (int i = currentSelectedList.Count-1; i >= 0; i--)
            {
                var selItem = currentSelectedList[i];
                for (int j = 0; j < selectedItems.Count; j++)
                {
                    var listItem = selectedItems[j];
                    if (string.Compare(listItem.Value, selItem, true) == 0)
                    {
                        //allListItems.Remove(listItem);
                        allListItems.Insert(0, listItem);
                        break;
                    }
                }
            }
           
            nonSelectedItems.Sort((x1, x2) => (x1.Text.CompareTo(x2.Text)));
            
            allListItems.AddRange(nonSelectedItems);

            return allListItems;
        }

        private FormFieldsFieldItemDataImpl getFormFieldAsListbox(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {
            MyListBox listBox = new MyListBox();
            FormFieldsFieldItemDataImpl fieldItem = new FormFieldsFieldItemDataImpl(listBox)
            {
                CssClass = "cms-many-to-many",
                Title = this.PropertyInfo.Name
            };
            fieldItem.Control = listBox;
            
           // FormFieldMulipleChoiceListBox field = new FormFieldMulipleChoiceListBox();
            listBox.ID = "cmsProperty_" + this.PropertyInfo.Name;
            listBox.Size = (uint)this.ItemsToShowInList;
            //listBox.Width = this.WidthInEdit.GetValueOrDefault(600);
            //listBox.Height = this.HeightInEdit.GetValueOrDefault(400); ;
            listBox.FieldParameters.jQueryMultiSelectParams = new Code.Classes.Javascript.UI.jQuery.MultiSelect._jQueryMultiSelectParams();

            fieldItem.CssClass = "cms-many-to-many";
            listBox.Title = this.PropertyInfo.Name;
            listBox.FieldParameters.helpMessage = "To select multiple items, hold CTRL and select items.";
            
           // var listBox = field.GetField();

            if (item != null)
            {
               // listBox.SelectedIndex = -1;

                
                var allListItems = getListItemChoicesAndMarkSelected(item);
                //field.ListItems = new ListItemCollection();
                listBox.Functionality.Items.Clear();
                listBox.Functionality.Items.AddRange(allListItems);

                //listBox.Items.AddRange(allListItems.ToArray());
            }
            

            return fieldItem;


        }


        public override FormFieldsFieldItemDataImpl GetFormFieldBaseDataForProperty(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {
            FormFieldsFieldItemDataImpl field = null;
            if (sectionType == EnumsCms.SECTION_TYPE.EditPage)
            {
                switch (this.DisplayType)
                {
                    case EnumsCms.MULTICHOICE_DISPLAY_TYPE.CheckboxList:
                        {
                            field = getFormFieldAsCheckboxList(sectionType, item, loadInitialValueFromPostback, value);
                            break;
                        }
                    case EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox:
                        {
                            field = getFormFieldAsListbox(sectionType, item, loadInitialValueFromPostback, value);
                            break;
                        }
                    default:
                        throw new InvalidOperationException("Invalid display type");

                }
            }
            else if (sectionType == EnumsCms.SECTION_TYPE.SearchInListing)
            {
                field = getFormFieldForSearch(sectionType, item, loadInitialValueFromPostback, value);
            }
            field.Control.Control.ID = "cmsProperty_" + this.PropertyInfo.Name;
            field.CssClass = "cms-many-to-many";
            field.Title = this.Label;
            
            return field;


        }

    }
}