﻿using System;
namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects
{
    public interface ICmsItemSpecificOperation
    {
        CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.ICmsItemInstance Item { get; }
    }
}
