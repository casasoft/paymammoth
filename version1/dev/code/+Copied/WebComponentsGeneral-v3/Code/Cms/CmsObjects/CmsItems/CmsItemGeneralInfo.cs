﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Classes.Cms.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;

using BusinessLogic_v3.Modules.CmsUserModule;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems
{
    public class CmsItemGeneralInfo : ICmsItemGeneralInfo
    {
        public CmsItemGeneralInfo(ICmsItemFactory factory )
        {
            this._factory = factory;
            CmsImageIcon = EnumsCms.IMAGE_ICON.Listing;
            this.ShowAddNewButton = true;
            this.CmsMainMenuPriority = int.MaxValue;
           
            this.RenderType = EnumsCms.SECTION_RENDER_TYPE.Listing;
            this.AccessTypeRequired_ToAdd = new CmsAccessType(null);
            this.AccessTypeRequired_ToDelete = new CmsAccessType(null);
            this.AccessTypeRequired_ToView = new CmsAccessType(null);
            this.AccessTypeRequired_ToExport = new CmsAccessType(null);
            this.CustomCmsOperations = new List<CmsGeneralOperation>();
        }
        private ICmsItemFactory _factory;

        public string TitlePlural { get; set; }
        public string TitleSingular { get; set; }
        public List<CmsGeneralOperation> CustomCmsOperations { get; private set; }

        protected virtual CmsUserBase getCurrentLoggedUser()
        {
            return CmsUserSessionLogin.Instance.GetLoggedInUser();
            
        }
        public virtual CmsAccessType AccessTypeRequired_ToDelete { get; set; }
        public virtual CmsAccessType AccessTypeRequired_ToAdd { get; set; }
        public virtual CmsAccessType AccessTypeRequired_ToExport { get; set; }


        public bool CheckIfUserCanView(ICmsUserBase user)
        {
            if (user == null) user = getCurrentLoggedUser();
            bool ok = AccessTypeRequired_ToView.CheckIfUserHasEnoughAccess(user);
            return ok;
        }
        public bool CheckIfUserCanAdd(ICmsUserBase user)
        {
            if (user == null) user = getCurrentLoggedUser();
            bool ok = CheckIfUserCanView(user);
            if (ok) ok = AccessTypeRequired_ToAdd.CheckIfUserHasEnoughAccess(user);
            return ok;
        }
        public bool CheckIfUserCanDelete(ICmsUserBase user)
        {
            if (user == null) user = getCurrentLoggedUser();
            bool ok = CheckIfUserCanView(user);
            if (ok) ok = AccessTypeRequired_ToDelete.CheckIfUserHasEnoughAccess(user);
            return ok;
        }

        public CmsAccessType AccessTypeRequired_ToView { get; set; }
        private bool _showAddNewButton;
        public bool ShowAddNewButton
        {
            get { return _showAddNewButton; }
            set { _showAddNewButton = value; }
        }
        public bool GetWhetherToShowAddNewButton()
        {
            bool b =ShowAddNewButton;
            if (b)
            {
                var user = getCurrentLoggedUser();
                if (user == null || (user != null && !this.CheckIfUserCanAdd(user)))
                {
                    b = false;
                }

            }
            return b;
        }

        public string Name { get; set; }



        public void AddAccessRoleRequiredToAll(Enum accessRole)
        {
            CmsAccessRole role = new CmsAccessRole(accessRole);
            this.AddAccessRoleRequiredToAll(role);

        }
        public void AddAccessRoleRequiredToAll(ICmsAccessRole accessRole)
        {
            this.AccessTypeRequired_ToAdd.AccessRolesRequired.Add(accessRole);
            this.AccessTypeRequired_ToView.AccessRolesRequired.Add(accessRole);

        }
        public void SetAllMinimumAccessRequiredAs(CS.General_v3.Enums.CMS_ACCESS_TYPE accessType)
        {
            this.AccessTypeRequired_ToAdd.MinimumAccessTypeRequired = accessType;
            this.AccessTypeRequired_ToView.MinimumAccessTypeRequired = accessType;

        }
        public void SetAllAccessLevelRequiredToOverrideRolesAs(CS.General_v3.Enums.CMS_ACCESS_TYPE accessType)
        {
            this.AccessTypeRequired_ToAdd.AccessLevelRequiredToOverrideRoles = accessType;
            this.AccessTypeRequired_ToView.AccessLevelRequiredToOverrideRoles = accessType;


        }
        public int CmsMainMenuPriority { get; set; }

        public EnumsCms.IMAGE_ICON CmsImageIcon { get; set; }



        public virtual bool ShowInCmsMainMenu { get; set; }

        public virtual EnumsCms.SECTION_RENDER_TYPE RenderType { get; set; }




        #region ICmsItemGeneralInfo Members


        public bool GetWhetherToShowInCmsMainMenu()
        {
            bool ok = this.ShowInCmsMainMenu;
            if (ok)
            {
                ok = this.CheckIfUserCanView(null);
            }
            return ok;
            
        }

        #endregion


        #region ICmsItemGeneralInfo Members

        IEnumerable<PageTitle> ICmsItemGeneralInfo.GetListingPageTitlesForThisAndLinked()
        {
            return this._factory.GetListingPageTitlesForThisAndLinked();
            
        }

        #endregion

        #region ICmsItemGeneralInfo Members

        IEnumerable<PageTitle> ICmsItemGeneralInfo.GetEditPageTitlesForThisAndLinked(long id)
        {
            return this._factory.GetEditPageTitlesIncludingLinked(id);
            
        }

        #endregion

        CmsAccessType ICmsItemGeneralInfoBL.GetAccessRequiredToView()
        {
            return this.AccessTypeRequired_ToView;

        }

        CmsAccessType ICmsItemGeneralInfoBL.GetAccessRequiredToDelete()
        {
            return this.AccessTypeRequired_ToDelete;

        }

        CmsAccessType ICmsItemGeneralInfoBL.GetAccessRequiredToAdd()
        {
            return this.AccessTypeRequired_ToAdd;

        }

        #region ICmsItemGeneralInfoBL Members


        public bool CheckIfUserCanExport(ICmsUserBase user)
        {
            return AccessTypeRequired_ToView.CheckIfUserHasEnoughAccess(user) && AccessTypeRequired_ToExport.CheckIfUserHasEnoughAccess(user);
            
        }

        #endregion
    }
}
