﻿using System;
using System.Linq;
using System.Reflection;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Modules.CmsUserModule;

using System.Web.UI.WebControls;
using System.Web.UI;


using CS.General_v3.Classes.HelperClasses;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.MediaItem;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls;
using CS.General_v3;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.CKEditor;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties
{
    public abstract class CmsPropertyGeneral  : CmsFieldBase
    {
        public Type propertyType { get; protected set; }
        //public Enums.CMS_DATA_TYPE DataType { get; protected set; }

        public CmsPropertyGeneral( ICmsItemInstance cmsItem)
            : base(cmsItem)
        {
            
            
            
        }

        
        
      

        /// <summary>
        /// Delegate that returns the list items for the property in question.  Please note that item CAN BE NULL
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public delegate ListItemCollection CustomListItemCollectionDelegate(CmsPropertyGeneral propertyInfo, ICmsItemInstance item, EnumsCms.SECTION_TYPE sectionType);
        public event CustomListItemCollectionDelegate CustomListItemCollectionRetriever;

        public virtual ICmsItemFactory GetFactoryForLinkedObject()
        {
            throw new InvalidOperationException("if this is needed, it must be overriden!");
        }

        /// <summary>
        /// This field should be set mainly for linked objects, so that it can search using this field, rather than by ID
        /// </summary>
        public PropertyInfo CustomSearchField { get; set; }

        public override object GetFormValueConvertedToPropertyDataType(object formValue, EnumsCms.SECTION_TYPE sectionType)
        {
            
            object o = formValue;
            object result = null;
            switch (this.DataType)
            {
                case EnumsCms.CMS_DATA_TYPE.Bool:
                case EnumsCms.CMS_DATA_TYPE.BoolNullable:
                    //Commented by Mark 2012-01-03
                    //if (sectionType == EnumsCms.SECTION_TYPE.SearchInListing)
                    //{
                    if (this.DataType == EnumsCms.CMS_DATA_TYPE.Bool && sectionType != EnumsCms.SECTION_TYPE.SearchInListing)
                    {
                        result = CS.General_v3.Util.Other.TextToBoolNullable(o == null ? null : o.ToString()).GetValueOrDefault();
                    }
                    else
                    {
                        result = CS.General_v3.Util.Other.TextToBoolNullable(o == null ? null : o.ToString());
                    }

                    /*}
                    else
                    {
                        var val = CS.General_v3.Util.Other.ConvertStringToBasicDataType<bool?>(formValue.ToString());
                        result = o;
                    }*/
                    break;


                case EnumsCms.CMS_DATA_TYPE.Integer:
                case EnumsCms.CMS_DATA_TYPE.IntegerNullable:
                    {
                        if (this.DataType == EnumsCms.CMS_DATA_TYPE.Integer && sectionType != EnumsCms.SECTION_TYPE.SearchInListing)
                        {
                            result = CS.General_v3.Util.Other.ConvertStringToBasicDataType<int>(o == null ? null : o.ToString());
                        }
                        else
                        {
                            result = CS.General_v3.Util.Other.ConvertStringToBasicDataType<int?>(o == null ? null : o.ToString());
                        }
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.Enumeration:
                    {
                        string s = (o != null ? o.ToString() : null);
                        result = null;

                        if (!string.IsNullOrWhiteSpace(s))
                        {
                            try
                            {
                                var t = this.propertyType;
                                if (CS.General_v3.Util.ReflectionUtil.CheckIfTypeIsNullable(t))
                                {
                                    t = this.propertyType.GetGenericArguments()[0];
                                }

                                result = Enum.Parse(t, s);
                            }
                            catch (ArgumentException ex)
                            {
                                result = null;
                            }

                        }
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.LinkedObject:
                    { //this means a linked object
                        if (this.CustomSearchField == null)
                        {
                            string s = (o != null ? o.ToString() : null);
                            // result = null;
                            int num = 0;
                            if (!int.TryParse(s, out num))
                                num = 0;
                            if (num > 0)
                            {
                                var factory = GetFactoryForLinkedObject();
                                var item = factory.GetCmsItemWithId(num).DbItem;
                                result = item;
                            }
                        }
                        else
                        {
                            result = (string)o;
                        }
                        break;
                        // break;
                    }

                case EnumsCms.CMS_DATA_TYPE.Long:
                case EnumsCms.CMS_DATA_TYPE.LongNullable:
                    {
                        if (this.DataType == EnumsCms.CMS_DATA_TYPE.Long && sectionType != EnumsCms.SECTION_TYPE.SearchInListing)
                        {
                            result = CS.General_v3.Util.Other.ConvertStringToBasicDataType<long>(o == null ? null : o.ToString());
                            
                        }
                        else
                        {
                            result = CS.General_v3.Util.Other.ConvertStringToBasicDataType<long?>(o == null ? null : o.ToString());
                            
                        }
                    }
                    break;
                case EnumsCms.CMS_DATA_TYPE.Double:
                case EnumsCms.CMS_DATA_TYPE.DoubleNullable:
                    {
                        if (this.DataType == EnumsCms.CMS_DATA_TYPE.Double && sectionType != EnumsCms.SECTION_TYPE.SearchInListing)
                        {
                            result = CS.General_v3.Util.Other.ConvertStringToBasicDataType<double>(o == null ? null : o.ToString());
                        }
                        else
                        {
                            result = CS.General_v3.Util.Other.ConvertStringToBasicDataType<double?>(o == null ? null : o.ToString());
                        }
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.String:
                    {
                        string s = (string)o;
                        if (sectionType == EnumsCms.SECTION_TYPE.SearchInListing && string.IsNullOrEmpty(s))
                            s = null;
                        result = s;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.DateTime:
                case EnumsCms.CMS_DATA_TYPE.DateTimeNullable:
                    {
                        result = o;
                        break;
                    }
                default:
                    throw new InvalidOperationException("Invalid data type");
            }
            return result;
        }


        public virtual System.Web.UI.WebControls.ListItemCollection GetListItemCollectionForDataType(EnumsCms.SECTION_TYPE sectionType)
        {
            ListItemCollection listItems = null;
            
            switch (this.DataType)
            {
                case EnumsCms.CMS_DATA_TYPE.Enumeration:
                    {
                        listItems = CS.General_v3.Util.EnumUtils.GetListItemCollectionFromEnum(this.propertyType, 
                            (!this.GetRequiredValueBasedOnSectionType(sectionType) ? "" : null), addSpacesToCamelCasedName: true);
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.LinkedObject:
                    {
                        if ((typeof(BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject)).IsAssignableFrom(this.propertyType))
                        {
                            //this is an item link
                            var factory = CmsSystem.Instance.GetFactoryForType(this.propertyType);
                            if (factory == null)
                            {
                                //it can mean this is not used
                                //throw new InvalidOperationException("Factory not registered for property with type " + this.propertyType.ToString());
                            }
                            else
                            {
                                listItems = new ListItemCollection();
                                var listItemArray = factory.GetAllAsListItemsForCms(this, !this.GetRequiredValueBasedOnSectionType(sectionType), 0, false);
                                listItems.AddRange(listItemArray);
                            }
                        }
                        else
                        {
                            throw new InvalidOperationException("Unsupported data type");
                        }
                        break;
                    }
                default:
                    throw new InvalidOperationException("this cannot be called if data type is not an enumeration or linked object");
            }
            return listItems;

        }
        
        /// <summary>
        /// Returns the list item collection to show in the combo box
        /// </summary>
        /// <returns></returns>
        public virtual System.Web.UI.WebControls.ListItemCollection GetListItemCollectionForDataTypeAndItem(ICmsItemInstance item, EnumsCms.SECTION_TYPE sectionType)
        {
            if (this.DataType != EnumsCms.CMS_DATA_TYPE.Enumeration && this.DataType != EnumsCms.CMS_DATA_TYPE.LinkedObject)
                throw new InvalidOperationException("this cannot be called if data type is not an enumeration or linked object");
            ListItemCollection listItems = null;
            if (this.CustomListItemCollectionRetriever == null)
            {
                listItems = GetListItemCollectionForDataType(sectionType);

            }
            else
                listItems = this.CustomListItemCollectionRetriever(this, item, sectionType);
            return listItems;
        }
        private FormFieldsFieldItemDataImpl _getFormFieldBaseDataForProperty(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item,
              EnumsCms.CMS_DATA_TYPE dataType,bool loadInitialValueFromPostback = false, object value = null)
        {
            var p = this;
            object initialValue = value;

            int width = 0;
            if (sectionType == EnumsCms.SECTION_TYPE.ListingPage)
                width = this.WidthInListing.GetValueOrDefault(100);
            else
                width = this.WidthInListing.GetValueOrDefault(600);

            IMyFormWebControl formControl = null;

            switch (dataType)
            {
                case EnumsCms.CMS_DATA_TYPE.MediaItem:
                    {
                        formControl = new MediaItemFormControl();
                        break;
                    }

                case EnumsCms.CMS_DATA_TYPE.Bool:
                    {
                        if (sectionType != EnumsCms.SECTION_TYPE.SearchInListing)
                        {
                            formControl = new MyCheckBox();
                            //FormFieldBoolData field = new FormFieldBoolData(); fieldBase = field;
                        }
                        else
                        {

                            formControl = p.GetControlForBoolNullable();

                        }
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.BoolNullable:
                    {
                        bool? bValue = (bool?)value;
                        formControl = p.GetControlForBoolNullable();
                        if (bValue == null)
                            initialValue = null;
                        else if (bValue.Value)
                            initialValue = "1";
                        else
                            initialValue = "0";
                        break;
                    }

                case EnumsCms.CMS_DATA_TYPE.DateTime:
                case EnumsCms.CMS_DATA_TYPE.DateTimeNullable:
                    {

                        var txtDate= new  MyTxtBoxDate();

                        formControl = txtDate;
                        //FormFieldDateData field = new FormFieldDateData(); fieldBase = field;
                       // field.ShowTime = this.DateShowTime;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.Double:
                case EnumsCms.CMS_DATA_TYPE.DoubleNullable:
                    {
                        formControl = new MyTxtBoxNumber();
                        //FormFieldNumericDoubleData field = new FormFieldNumericDoubleData(); fieldBase = field;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.Integer:
                case EnumsCms.CMS_DATA_TYPE.IntegerNullable:
                case EnumsCms.CMS_DATA_TYPE.Long:
                case EnumsCms.CMS_DATA_TYPE.LongNullable:
                    {
                        var txtNumeric = new MyTxtBoxNumber();
                        txtNumeric.ValidatorNumericParameters.integersOnly = true;
                        formControl = txtNumeric;
                        //FormFieldNumericIntegerData field = new FormFieldNumericIntegerData(); fieldBase = field;

                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.String:
                    {
                        switch (p.StringDataType)
                        {
                            case Enums.STRING_DATA_TYPE.Email:
                                formControl = new MyTxtBoxEmail();
                                break;
                            case Enums.STRING_DATA_TYPE.MultiLine:
                                formControl = new MyTxtBoxStringMultiLine();
                                break;
                            case Enums.STRING_DATA_TYPE.Password:
                                formControl = new MyTxtBoxStringPassword();
                                break;
                            case Enums.STRING_DATA_TYPE.SingleLine:
                                formControl = new MyTxtBoxString();
                                break;
                            case Enums.STRING_DATA_TYPE.Website:
                                formControl = new MyTxtBoxWebsite();
                                break;
                            case Enums.STRING_DATA_TYPE.Html:
                                formControl = new CKEditor();
                                break;
                            default:
                                throw new InvalidOperationException("Invalid string data type");
                        }

                       // formControl = txt;
                       // FormFieldStringBaseData field = FormFieldStringBaseData.GetFormFieldStringBaseDataFromStringDataType(p.StringDataType);

                       // fieldBase = field;
                        break;
                    }
                case EnumsCms.CMS_DATA_TYPE.Enumeration:
                    {
                        
                        var cmb = new MyDropDownList();
                        formControl = cmb;
                        //FormFieldListSingleChoiceDropdownData field = new FormFieldListSingleChoiceDropdownData(); fieldBase = field;
                        var listItems = p.GetListItemCollectionForDataTypeAndItem(item, sectionType);
                        cmb.Functionality.Items.AddRange(listItems);
                        //field.ListItems = listItems.Cast<ListItem>().ToList();
                        break;

                        
                    }
                case EnumsCms.CMS_DATA_TYPE.LinkedObject:
                    {
                        if (this.CustomSearchField == null)
                        {
                            var f = this.GetFactoryForLinkedObject();
                            if (f != null)
                            {
                                var cmb = new MyDropDownList();

                                //FormFieldListSingleChoiceDropdownData field = new FormFieldListSingleChoiceDropdownData();
                                //fieldBase = field;
                                var listItems = p.GetListItemCollectionForDataTypeAndItem(item, sectionType);
                                //field.ListItems = listItems.Cast<ListItem>().ToList();
                                cmb.Functionality.Items.AddRange(listItems);
                                formControl = cmb;
                            }
                        }
                        else
                        {
                            //return the default string one
                            return _getFormFieldBaseDataForProperty(sectionType, item, EnumsCms.CMS_DATA_TYPE.String);
                                
                        }
                        break;

                    }
                default:
                    throw new InvalidOperationException("Unsupported data type");
            }
            if (formControl == null) throw new InvalidOperationException("Unsupported data type " + dataType);

            FormFieldsFieldItemDataImpl fieldBase = new FormFieldsFieldItemDataImpl(formControl);
            fillFormFieldBaseDataFromProperty(fieldBase, sectionType);
            
            fieldBase.Control.FieldParameters.id = getControlID(sectionType, item);
            if (initialValue is BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject)
            {
                BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject cmsItem = (BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject)value;
                fieldBase.Control.WebControlFunctionality.InitialValue = cmsItem.ID;
                
            }
            else
            {
                fieldBase.Control.WebControlFunctionality.InitialValue = initialValue;
            }



            //fieldBase.Control.Width = width;
            
            if (sectionType == EnumsCms.SECTION_TYPE.SearchInListing)
                fieldBase.Required = false;
            fieldBase.Control.LoadInitialValueFromPostback = loadInitialValueFromPostback;
            fieldBase.Control.WebControlFunctionality.CssManager.AddClass(p.GetFieldCssClassForSectionType(sectionType));
            
            return fieldBase;
        }
        public override FormFieldsFieldItemDataImpl GetFormFieldBaseDataForProperty(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item,
         bool loadInitialValueFromPostback = false, object value = null)
        {
            
            var p = this;
            return _getFormFieldBaseDataForProperty(sectionType, item, p.DataType, loadInitialValueFromPostback, value);

        }
        public override OperationResult SetValueForObjectFromFormControl(ICmsItemInstance o, IMyFormWebControl formControl)
        {
            object value = formControl.GetFormValueObject();
            var convertedValue = this.GetFormValueConvertedToPropertyDataType(value, EnumsCms.SECTION_TYPE.EditPage);
            var result = SetValueForObject(o, convertedValue);
            return result;


        }
        
        protected override System.Web.UI.Control getControlNonEditable(ICmsItemInstance item)
        {

            var p = this;
            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            Control ctrl = null;
            object value = p.GetValueForObject(item);



            LiteralControl lit = new LiteralControl();

            lit.Text = "&nbsp;";
            ctrl = lit;
            if (loggedUser != null && item.CheckIfUserCanView(loggedUser))
            {
                string s = Util.CmsUtil.GetToStringValueForObject(value);
                if (this.DataType != EnumsCms.CMS_DATA_TYPE.LinkedObject)
                {
                    if (this.StringDataType == CS.General_v3.Enums.STRING_DATA_TYPE.Html)
                    {
                        MyDiv div = new MyDiv();
                        div.CssManager.AddClass("literalHtmlContainer");

                        div.InnerHtml = s;

                        ctrl = div;

                    }
                    else if (s != null && (s.Contains("\r") || s.Contains("\n")))
                    {
                        MyTxtBoxStringMultiLine txtMultiLine = new MyTxtBoxStringMultiLine();
                        txtMultiLine.CssManager.AddClass("readonly");
                        txtMultiLine.ReadOnly = true;
                        if (this.WidthInEdit.HasValue)
                            txtMultiLine.Width = this.WidthInEdit.Value;
                        else
                            txtMultiLine.Width = 600;
                        txtMultiLine.Height = 250;
                        txtMultiLine.WebControlFunctionality.InitialValue = s;
                        ctrl = txtMultiLine;
                    }
                    else
                    {
                        lit.Text = CS.General_v3.Util.Text.HtmlEncode(s);
                    }
                }
                else
                {



                    BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject dbObject = (BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject)value;
                    if (dbObject != null)
                    {
                        MyAnchor anchor = new MyAnchor();
                        anchor.InnerText = CS.General_v3.Util.Text.HtmlEncode(s);
                        var linkedFactory = this.GetFactoryForLinkedObject();

                        anchor.Href = linkedFactory.GetEditUrlForItemWithID(dbObject.ID).ToString();
                        ctrl = anchor;
                    }
                }
            }

            return ctrl;
        }


        public override bool IsUsedInProject()
        {
            bool ok = base.IsUsedInProject();
            if (ok)
            {
                if (this.DataType == EnumsCms.CMS_DATA_TYPE.LinkedObject)
                {
                    var factory = GetFactoryForLinkedObject();
                    ok = factory != null;
                }
            }
            if (!ok)
            {
                int k = 5;
            }
            return ok;
            
        }



    }
}