﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public class CmsInfoTemp : CmsInfoBase
    {

    }

    public abstract class CmsInfoBase
    {

        
        
        public static CmsInfoBase Instance
        {

            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<CmsInfoBase>(CS.General_v3.Classes.Settings.SettingsMain.GetProjectAssemblies()); }
        }

        protected virtual void initialiseCmsTypes()
        {
           
            //RegisterItemType(ProductFactory.Instance);
            //RegisterItemType(StoreFactory.Instance);
        }


        public bool ShowCasaSoftLogos { get; set; }
        public string CmsTitle { get; set; }

        private CS.General_v3.Classes.Factories.FactoryController<ICmsItemFactory> _CmsItemTypes = null;
        public List<ICmsItemFactory> GetRegisteredCmsFactories()
        {
            List<ICmsItemFactory> list = new List<ICmsItemFactory>();
            list.AddRange(_CmsItemTypes.GetAllFactories());
            return list;
        }
        
        

        /// <summary>
        /// Returns all CMS Properites of all registered types, which are linked to an item of the given type of factory
        /// </summary>
        /// <param name="itemInfo"></param>
        /// <returns></returns>
        public IEnumerable<CmsPropertyInfo> GetCmsPropertiesLinkedToFactory(ICmsItemFactory itemInfo)
        {
            List<CmsPropertyInfo> list = new List<CmsPropertyInfo>();
            foreach (var item in _CmsItemTypes.GetAllFactories())
            {
                foreach (var property in item.GetPropertyInfos(null))
                {
                    if (property is CmsPropertyInfo)
                    {
                        CmsPropertyInfo p = (CmsPropertyInfo)property;

                        if (p.LinkedWithInCms)
                        {
                            if (itemInfo.CheckIfFactoryIsForType( p.PropertyInfo.PropertyType))
                            {
                                list.Add(p);
                            }
                        }
                    }
                }
            }
            return list;
        }

        public void RemoveAnyQuerystringParametersRelatedToCms(CS.General_v3.Classes.URL.URLClass url)
        {
            foreach (var factory in this.GetRegisteredCmsFactories())
            {
                url[factory.QueryStringParamID] = null;
                foreach (var p in factory.GetPropertyInfos(null))
                {
                    if (p.LinkedWithInCms)
                    {
                        url[p.GetLinkedQuerystringParamName()] = null;
                    }
                }

            }

        }

        public void RegisterItemType(ICmsItemFactory cmsItemType)
        {
            _CmsItemTypes.AddFactory(cmsItemType);
            /*
            if (_InitialiseRegisteredTypes)
            {
                cmsItemType.InitCmsParameters();
                //_CmsItemTypes.AddFactory(cmsItemType);
            }
            else
            {
                throw new InvalidOperationException("You cannot initialise a factory when they have already been initialised");
            }*/
            
        }

        public void FinishedInitialisingFactories()
        {

            _CmsItemTypes.FinishedInitialisingFactories();
        }

        /*
        private bool _InitialiseRegisteredTypes = false;
        public void InitialiseRegisteredTypes()
        {
            if (!_InitialiseRegisteredTypes)
            {
                foreach (var t in this.GetRegisteredCmsFactories())
                {
                    t.InitCmsParameters();
                }
                _InitialiseRegisteredTypes = true;
            }
        }
        */
        public string CmsRootPath { get; set; }
        /// <summary>
        /// This ensures that the returend path ends with a slash
        /// </summary>
        /// <returns></returns>
        public string GetCmsRoot()
        {
            string s = CmsRootPath;
            if (!s.EndsWith("/"))
                s += "/";
            return s;
        }


        protected CmsInfoBase()
        {
            this.ShowCasaSoftLogos = true;
            this.CmsRootPath = "/cms/";
            _CmsItemTypes = new CS.General_v3.Classes.Factories.FactoryController<ICmsItemFactory>();
            this.initialiseCmsTypes();
        //    this.MainMenu2Items = new List<MainMenuItem>();
        }

       // public List<MainMenuItem> MainMenu2Items { get; private set; }

        public bool CMS_AutoLoginFromDevMachines
        {
            get
            {
                return CS.General_v3.Settings.GetSetting<bool>(CS.General_v3.Enums.SETTINGS_ENUM.CMS_AutoLoginFromDevMachines);
            }
        }
        public ICmsItemFactory GetFactoryForType(Type t)
        {
            return _CmsItemTypes.GetFactoryForType(t);

            //foreach (var factory in this.GetRegisteredCmsFactories())
            //{
            //    if (factory.CheckIfFactoryIsForType(t))
            //    {
            //        return factory;
            //    }
            //}
            //return null;
        }
    }
}