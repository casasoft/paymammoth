using System.Collections.Generic;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Modules.CmsUserModule;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects 
{
    public interface ICmsItemGeneralInfo : BusinessLogic_v3.Classes.Cms.Factories.ICmsItemGeneralInfoBL
    {
        List<CmsGeneralOperation> CustomCmsOperations { get; }
        EnumsCms.IMAGE_ICON CmsImageIcon { get; set; }
        EnumsCms.SECTION_RENDER_TYPE RenderType { get; set; }
        
        
        IEnumerable<PageTitle> GetListingPageTitlesForThisAndLinked();
        IEnumerable<PageTitle> GetEditPageTitlesForThisAndLinked(long id);
        

        
    }
}