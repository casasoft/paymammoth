﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
using System.Web.UI;
using BusinessLogic_v3.DB;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
using CS.General_v3.Classes.HelperClasses;
using System.Text;
using BusinessLogic_v3.Classes.DB;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public abstract class CmsPropertyMultipleCheckBoxes : CmsPropertyMultiChoice
        
       
    {


      //  public ICmsItemFactory LinkedWithFactory { get; private set; }
        
        public CmsPropertyMultipleCheckBoxes(ICmsItemFactory itemFactory, PropertyInfo propertyInfo)
            : base(itemFactory, propertyInfo)

        {
          //  this.LinkedWithFactory = factoryLinkedWith;

            this.TotalColumns = 3;
            this.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
        }




        public int TotalColumns { get; set; }
        public RepeatDirection RepeatDirection { get; set; }

        public override FormFieldBaseData GetFormFieldBaseDataForProperty(Enums.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {


            FormFieldMulipleChoiceDataAsChkBoxes field = new FormFieldMulipleChoiceDataAsChkBoxes();
            field.ID = "cmsProperty_" + this.PropertyInfo.Name;
            field.RepeatDirection = this.RepeatDirection;
            field.TotalColumns = this.TotalColumns;
            field.FormFieldCssClass.AddClass("cms-many-to-many-checkboxes");
            field.Title = this.Label;
            //field.HelpMessage = "To select multiple items, hold CTRL and select items.";
            var listBox = field.GetField();

            if (item != null)
            {
                var currentSelectedList = GetValueForObject(item);

                var allListItems = getListItemChoices(item);
                //listBox.SelectedIndex = -1;
              //  listBox.SelectedItem = null;
                foreach (ListItem listItem in allListItems)
                {
                    listItem.Attributes.Remove("selected");
                    listItem.Selected = false;
                    foreach (var selItem in currentSelectedList)
                    {
                        if (string.Compare(listItem.Value, selItem,true) ==0)
                        {
                            listItem.Attributes["selected"] = "selected";
                            listItem.Selected = true;
                            break;
                        }
                        
                    }
                   // listItem.Selected = true;
                }
                listBox.Items.AddRange(allListItems.ToArray());
            }
            return field;

            
        }


        
    }
}