﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using System.Web.UI;
using BusinessLogic_v3.DB;
using CS.General_v3.Classes.HelperClasses;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public class CmsPropertyInfo  : CmsPropertyGeneral
    {
        

        public CmsPropertyInfo(ICmsItemFactory itemGeneralInfo, PropertyInfo pInfo) : base(itemGeneralInfo)
        {
            System.Diagnostics.Contracts.Contract.Requires(pInfo != null);
            this.PropertyInfo = pInfo;
            this.SortableInListing = true;
            
            initInfoFromProperty();
        }

        
        public PropertyInfo PropertyInfo { get; set; }
        public override bool IsRequired
        {
            get
            {
                bool r = base.IsRequired;
                if (!r)
                {
                    if (this.PropertyInfo != null)
                    {
                        var pType = this.PropertyInfo.PropertyType;

                        if (pType.IsPrimitive &&
                            (pType != typeof(string) && pType != typeof(bool)))
                        {
                            r = true;
                        }
                    }

                }
                return r;
                
            }
            set
            {
                base.IsRequired = value;
            }
        }
       
        /// <summary>
        /// Returns the factory for the 'LinkedItem' in CMS.  In order for this to return a value, this property must be marked as 'LinkedWithInCms'.  For example, if this property represenets 'Product.Store',
        /// the returned factory will be the factory for type 'Store'.
        /// </summary>
        /// <returns></returns>
        public ICmsItemFactory GetLinkedCmsFactory()
        {
            if (LinkedWithInCms)
                return CmsInfoBase.Instance.GetFactoryForType(this.PropertyInfo.PropertyType);
            else
                return null;
        }
        public override OperationResult SetValueForObject(ICmsItemInstance o, object value)
        {
            this.PropertyInfo.SetValue(o.DbItem, value, null);
            OperationResult result = new OperationResult();
            return result;
        }

        public override TDataType GetValueForObject<TDataType>(ICmsItemInstance o)
        {
            return (TDataType)this.PropertyInfo.GetValue(o.DbItem, null);
        }
        /// <summary>
        /// This will only work if the type is registered as a factory
        /// </summary>
        /// <returns></returns>
        public override ICmsItemFactory GetFactoryForLinkedObject()
        {
            return CmsInfoBase.Instance.GetFactoryForType(this.PropertyInfo.PropertyType);
        }


        private void initInfoFromProperty()
        {
            this.Label = CS.General_v3.Util.Text.AddSpacesToCamelCasedText( this.PropertyInfo.Name);
            this.DataType = EnumsCms.GetDataTypeFromType(this.PropertyInfo.PropertyType);

            this.propertyType = this.PropertyInfo.PropertyType;
            // this.EditableInListing = true;


            this.IsRequired = (this.DataType == EnumsCms.CMS_DATA_TYPE.Integer || this.DataType == EnumsCms.CMS_DATA_TYPE.DateTime || this.DataType == EnumsCms.CMS_DATA_TYPE.Long);

            if (this.DataType == EnumsCms.CMS_DATA_TYPE.LinkedObject)
            {
                this.ShowInEdit = false;
                this.IsSearchable = false;
            }
            if (this.PropertyInfo.Name.ToLower().Contains("html"))
                this.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
        }

        /// <summary>
        /// Delegate that returns the list items for the property in question.  Please note that item CAN BE NULL
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public delegate ListItemCollection CustomListItemCollectionDelegate(CmsPropertyInfo propertyInfo, ICmsItemInstance item, EnumsCms.SECTION_TYPE sectionType);
        public event CustomListItemCollectionDelegate CustomListItemCollectionRetriever;

    
      
      
        //public override FormFieldBaseData GetFormFieldBaseDataForProperty(Enums.SECTION_TYPE sectionType,ICmsItemInstance item ,
        // bool loadInitialValueFromPostback = false, object value = null)
        //{
        //    var p = this;

        //    int width = 0;
        //    if (sectionType == Enums.SECTION_TYPE.ListingPage)
        //        width = this.WidthInListing.GetValueOrDefault(100);
        //    else
        //        width = this.WidthInListing.GetValueOrDefault(400);

        //    FormFieldBaseData fieldBase = null;
        //    switch (p.DataType)
        //    {
        //        case Enums.CMS_DATA_TYPE.MediaItem:
        //            {

        //                break;
        //            }

        //        case Enums.CMS_DATA_TYPE.Bool:
        //            {
        //                if (sectionType != Enums.SECTION_TYPE.SearchInListing)
        //                {
        //                    FormFieldBoolData field = new FormFieldBoolData(); fieldBase = field;
        //                }
        //                else
        //                {

        //                    FormFieldListSingleChoiceDropdownData field = p.GetFormFieldForBoolNullable(); fieldBase = field;

        //                }
        //                break;
        //            }
        //        case Enums.CMS_DATA_TYPE.BoolNullable:
        //            {

        //                FormFieldListSingleChoiceDropdownData field = p.GetFormFieldForBoolNullable(); fieldBase = field;
        //                break;
        //            }

        //        case Enums.CMS_DATA_TYPE.DateTime:
        //        case Enums.CMS_DATA_TYPE.DateTimeNullable:
        //            {
        //                FormFieldDateData field = new FormFieldDateData(); fieldBase = field;
        //                break;
        //            }
        //        case Enums.CMS_DATA_TYPE.Double:
        //        case Enums.CMS_DATA_TYPE.DoubleNullable:
        //            {
        //                FormFieldNumericDoubleData field = new FormFieldNumericDoubleData(); fieldBase = field;
        //                break;
        //            }
        //        case Enums.CMS_DATA_TYPE.Integer:
        //        case Enums.CMS_DATA_TYPE.IntegerNullable:
        //        case Enums.CMS_DATA_TYPE.Long:
        //        case Enums.CMS_DATA_TYPE.LongNullable:
        //            {
        //                FormFieldNumericIntegerData field = new FormFieldNumericIntegerData(); fieldBase = field;

        //                break;
        //            }
        //        case Enums.CMS_DATA_TYPE.String:
        //            {
        //                FormFieldStringBaseData field = FormFieldStringBaseData.GetFormFieldStringBaseDataFromStringDataType(p.StringDataType);

        //                fieldBase = field;
        //                break;
        //            }
        //        case Enums.CMS_DATA_TYPE.Enumeration:
        //        case Enums.CMS_DATA_TYPE.LinkedObject:
        //            {
        //                FormFieldListSingleChoiceDropdownData field = new FormFieldListSingleChoiceDropdownData(); fieldBase = field;
        //                var listItems = p.GetListItemCollectionForDataTypeAndItem(item, sectionType);
        //                field.ListItems = listItems;
        //                break;

        //            }
        //        default:
        //            throw new InvalidOperationException("Unsupported data type");
        //    }

        //    fillFormFieldBaseDataFromProperty(fieldBase, sectionType);
        //    fieldBase.ID = "cmsProperty_" + p.PropertyInfo.Name;
        //    if (value is BusinessLogic_v3.Classes.DB.IBaseDbObject )
        //    {
        //        BusinessLogic_v3.Classes.DB.IBaseDbObject cmsItem = (BusinessLogic_v3.Classes.DB.IBaseDbObject)value;
        //        fieldBase.InitialValue = cmsItem.ID;
                
        //    }
        //    else
        //    {
        //        fieldBase.InitialValue = value;
        //    }
        //    fieldBase.Width = width;
        //    if (sectionType == Enums.SECTION_TYPE.SearchInListing)
        //        fieldBase.Required = false;
        //    fieldBase.LoadInitialValueFromPostback = loadInitialValueFromPostback;
        //    fieldBase.FormFieldCssClass.AddClass(p.GetFieldCssClassForSectionType(sectionType));
            
        //    return fieldBase;
        //}
        //public override OperationResult SetValueForObjectFromFormControl(ICmsItemInstance o, CS.General_v3.Controls.WebControls.Common.General.IMyFormWebControl formControl)
        //{
        //    object value = formControl.FormValueObject;
        //    var convertedValue = this.GetFormValueConvertedToPropertyDataType(value, Enums.SECTION_TYPE.EditPage);
        //    var result = SetValueForObject(o, convertedValue);
        //    return result;


        //}
        
        //protected override System.Web.UI.Control getControlNonEditable(ICmsItemInstance item)
        //{
        //    var p = this;
        //    var loggedUser = CMSUserSessionLogin.Instance.GetLoggedInUser();
        //    object value = p.GetValueForObject(item);
        //    LiteralControl lit = new LiteralControl();
        //    lit.Text = "&nbsp;";
        //    if (loggedUser != null && loggedUser.CheckAccess(this.GetAccessTypeRequiredToView(item)))
        //    {
        //        string s = Util.CmsUtil.GetToStringValueForObject(value);
        //        lit.Text = CS.General_v3.Util.Text.HtmlEncode(s);
        //    }
        //    return lit;
        //}

        public override NHibernate.Criterion.ICriterion GetNHibernateCriterionForSearch(string searchValue)
        {
            object nhValue = null;
            if (this.DataType == EnumsCms.CMS_DATA_TYPE.LinkedObject)
            {
                nhValue = CS.General_v3.Util.Other.ConvertStringToBasicDataType<long?>(searchValue);
            }
            else
            {
                nhValue = CS.General_v3.Util.Other.ConvertStringToBasicDataType(searchValue, this.propertyType);    

            }
            
            return Util.CmsUtil.GetNHibernateCriterion(this.PropertyInfo, nhValue);
            
        }




        public override void ApplyOrderByToCriteria(NHibernate.ICriteria crit, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            crit.AddOrder(new NHibernate.Criterion.Order(this.PropertyInfo.Name, (sortType == CS.General_v3.Enums.SORT_TYPE.Ascending)));

            
        }
    }
}