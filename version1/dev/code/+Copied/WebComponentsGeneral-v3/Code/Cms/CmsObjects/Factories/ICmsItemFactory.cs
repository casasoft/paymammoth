﻿using System;
using System.Collections.Generic;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;



namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories
{
    public interface ICmsItemFactory : BusinessLogic_v3.Classes.Cms.CmsObjects.Factories.ICmsItemFactoryBL
    {
        new IEnumerable<ICmsItemInstance> GetAllItemsInRepository();
        int CmsMainMenuPriority { get; }
        bool ShowInCmsMainMenu { get; }
        IEnumerable<CmsGeneralOperation> GetCustomCmsOperations();
       // void InitCmsParameters();

        CS.General_v3.Classes.URL.URLClass GetEditUrlForItemWithID(long id);
        /// <summary>
        /// Type of Render for listing - E.g Tree or Listing.  Default is listing
        /// </summary>
        EnumsCms.SECTION_RENDER_TYPE RenderType { get; }
        string Name { get;  }

        
        /// <summary>
        /// Returns the list of properties for the item
        /// </summary>
        /// <returns></returns>
        System.Collections.Generic.IEnumerable<CmsFieldBase> GetPropertyInfos(ICmsItemInstanceBL item);

        new ICmsItemInstance GetCmsItemFromDBObject(BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject dbObject);

      //  ICmsItemFactory LinkedParent { get; }
        //IEnumerable<ICmsItemFactory> GetLinkedChildren();
        //IEnumerable<CmsPropertyInfo> GetLinkedProperties();
        new ICmsItemInstance GetCmsItemWithId(long id);
        /// <summary>
        /// This must take care about the links, making sure that if the item has linked items, it is automatically linked e.g, if you create a new product and there is currently the store id in the querystring, it is updated
        /// </summary>
        /// <returns></returns>
        new ICmsItemInstance CreateNewItem(bool isTemporary);
        new ICmsItemInstance GetEmptyCmsItemInfo();
        
        //PageTitle GetListingPageTitle();
        /// <summary>
        /// Returns the page titles for listing, for this item and any 'linked' items
        /// </summary>
        /// <returns></returns>
        IEnumerable<PageTitle> GetListingPageTitlesForThisAndLinked();
        /// <summary>
        /// Returns the page titles for an edit page, for this item and any 'linked' items.
        /// </summary>
        /// <returns></returns>
        IEnumerable<PageTitle> GetEditPageTitlesIncludingLinked(long id);
        
        
        EnumsCms.IMAGE_ICON CmsImageIcon { get; }
        new ICmsItemGeneralInfo UserSpecificGeneralInfoInContext { get; }

        System.Web.UI.WebControls.ListItem[] GetAllAsListItemsForCms(CmsFieldBase propertyInfo = null, bool addNullValue = false, long excludeID = 0, bool filterByLinkedItems = false,
            bool showNonPublished = true);

        /// <summary>
        /// Returns general search results for a 'listing' type
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="sortType"></param>
        /// <returns></returns>
        CmsSearchResults GetSearchResultsForListing(IEnumerable<CmsPropertySearchInfo> searchCriteria, bool showDeletedItems, 
            bool filterByLinkedItems, int pageNo, int pageSize, CmsFieldBase sortBy, CS.General_v3.Enums.SORT_TYPE sortType, bool showNonPublished = true);
        /// <summary>
        /// Returns search results for a 'hierarchical' listing
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        IEnumerable<ICmsHierarchyItem> GetRootItems(IEnumerable<CmsPropertySearchInfo> searchCriteria);
        string GetTitleForRoutingFolder();
    }
}
