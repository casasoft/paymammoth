﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Web.UI.WebControls;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.CustomPropertyTypes
{
    public class CmsCountryProperty : CmsPropertyMultiChoice
    {
        public CmsCountryProperty(ICmsItemFactory factory, PropertyInfo property ) : base(factory, property, EnumsCms.MULTICHOICE_DISPLAY_TYPE.CheckboxList)
        {
            this.TotalColumns = 5;
        }

        protected override void setValueForCmsItem(ICmsItemInstance item, IEnumerable<string> values)
        {
            string s = CS.General_v3.Util.Text.AppendStrings(",", values);
            PropertyInfo.SetValue(item.DbItem, s, null);
        }

        protected override IEnumerable<System.Web.UI.WebControls.ListItem> getListItemChoices(ICmsItemInstance item)
        {
            var enumValues = CS.General_v3.Util.EnumUtils.GetListOfEnumValues<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166>();
            List<ListItem> listItems = new List<ListItem>();
            foreach (var enumValue in enumValues)
            {
                string countryName = CS.General_v3.Util.EnumUtils.StringValueOf(enumValue);
                string countryCode = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode(enumValue);
                listItems.Add(new ListItem(countryName, countryCode));

            }
            listItems.Sort((c1, c2) => (c1.Text.CompareTo(c2.Text)));
            if (!IsRequired)
            {
                listItems.Insert(0, new ListItem("", ""));
            }
            return listItems;
        }

        protected override System.Web.UI.Control getControlNonEditable(ICmsItemInstance item)
        {
            var selectedCountries = getCountriesAsListOfEnumsForItem(item).ToList().ConvertAll<string>(c => CS.General_v3.Util.EnumUtils.StringValueOf(c));
            selectedCountries.Sort();

            Literal lit = new Literal();
            lit.Text = CS.General_v3.Util.Text.AppendStrings(", ", selectedCountries);
            return lit;
        }

        protected IEnumerable<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166> getCountriesAsListOfEnumsForItem(ICmsItemInstance item)
        {
            string s = getCountriesAsStringForItem(item);
            return CS.General_v3.Enums.ISO_ENUMS.GetListOfCountriesFromCommaSeperatedString(s);
        }

        protected string getCountriesAsStringForItem(ICmsItemInstance item)
        {
            string s = (string)PropertyInfo.GetValue(item.DbItem, null) ?? "";
            return s;
        }


        protected override IEnumerable<string> getSelectedValuesForItem(ICmsItemInstance item)
        {

            var selList = getCountriesAsStringForItem(item).Split(new string[] {","}, StringSplitOptions.RemoveEmptyEntries);
            List<string> list = new List<string>();
            foreach (var selItem in selList)
            {
                string s = selItem.Trim();
                list.Add(selItem);
            }
            return list;

        }
    }
}
