﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
using System.Web.UI;
using BusinessLogic_v3.DB;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
using CS.General_v3.Classes.HelperClasses;
using System.Text;
using BusinessLogic_v3.Classes.DB;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    /// <summary>
    /// Creates a link between a class and another, directly in the edit page.  This should be a many-collection which allows multiple values
    /// </summary>
    /// <typeparam name="TMainItem">The main item, e.g ItemGroup</typeparam>
    /// <typeparam name="TLinkedItem">The linked item, e.g ItemGroupCategoryFeatureValue</typeparam>
    public class CmsPropertyMultipleCheckBoxesGeneric<TMainItem, TLinkedItem> : CmsPropertyMultipleCheckBoxes
        where TMainItem : BaseDbObject
        where TLinkedItem: BaseDbObject
        
       
    {
        private Func<TMainItem, IEnumerable<ListItem>> _listItemChoicesRetriever = null;
        private Func<TLinkedItem, string> _itemToUniqueIDConverter = null;
        private Action<TMainItem, IEnumerable<string>> _itemValueSetter = null;

        /// <summary>
        ///Creates a link between a class and another, directly in the edit page.  This should be a many-collection which allows multiple values
        /// </summary>
        /// <param name="itemFactory">CMS factory</param>
        /// <param name="selector">Selector to retrieve the property info for the collection, from TMainItem to a list of TLInkedItem</param>
        /// <param name="listItemChoicesRetriever">A function that gets the selected item, and returns a list of ListItem</param>
        /// <param name="itemToUniqueIDConverter">A function that converts the linked item, to the Unique ID. This must be the same ID, which would be returned as value of the 'ListItemChoicesRetriever' </param>
        /// <param name="itemValueSetter">A function that sets the values, based from a list of strings</param>
        public CmsPropertyMultipleCheckBoxesGeneric(ICmsItemFactory itemFactory, System.Linq.Expressions.Expression<Func<TMainItem, IEnumerable<TLinkedItem>>> selector,
            Func<TMainItem, IEnumerable<ListItem>> listItemChoicesRetriever, Func<TLinkedItem, string> itemToUniqueIDConverter, Action<TMainItem, IEnumerable<string>> itemValueSetter)
            : base(itemFactory, null)
        {
            var property = CS.General_v3.Util.ReflectionUtil<TMainItem>.GetPropertyBySelector<IEnumerable<TLinkedItem>>(selector);
            _listItemChoicesRetriever = listItemChoicesRetriever;
            this._itemToUniqueIDConverter = itemToUniqueIDConverter;
            this._itemValueSetter = itemValueSetter;
            setProperty(property);
        }

        protected override IEnumerable<string> getSelectedValuesForItem(ICmsItemInstance item)
        {

            IEnumerable<TLinkedItem> values = (IEnumerable<TLinkedItem>)this.PropertyInfo.GetValue(item.DbItem, null);
            List<string> list = new List<string>();
            foreach (var value in values)
            {
                list.Add(_itemToUniqueIDConverter(value));
            }
            return list;
        }

        protected override void setValueForCmsItem(ICmsItemInstance item, IEnumerable<string> values)
        {
            TMainItem dbItem = (TMainItem)item.DbItem;
            _itemValueSetter(dbItem, values);
            
        }

        protected override IEnumerable<ListItem> getListItemChoices(ICmsItemInstance item)
        {
            return _listItemChoicesRetriever((TMainItem)item.DbItem);
            
        }

        

        protected override Control getControlNonEditable(ICmsItemInstance item)
        {

            Literal lit = new Literal();
            lit.Text = CS.General_v3.Util.Text.AppendStrings(", ", GetValueForObject(item));
            return lit;

        }
    }
}