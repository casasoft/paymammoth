﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
using System.Web.UI;
using BusinessLogic_v3.DB;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
using CS.General_v3.Classes.HelperClasses;
using System.Text;
using BusinessLogic_v3.Classes.DB;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public abstract class CmsPropertyMultiChoice : CmsPropertyBase
        
       
    {



      //  public ICmsItemFactory LinkedWithFactory { get; private set; }
        public PropertyInfo PropertyInfo { get; set; }

        public EnumsCms.MULTICHOICE_DISPLAY_TYPE DisplayType { get; set; }

        public CmsPropertyMultiChoice(ICmsItemFactory itemFactory, PropertyInfo propertyInfo, EnumsCms.MULTICHOICE_DISPLAY_TYPE displayType)
            : base(itemFactory)

        {
            this.DisplayType = displayType;
            //CS.General_v3.Util.ContractsUtil.RequiresNotNullable(propertyInfo, "Property info must be filled in");
          //  this.LinkedWithFactory = factoryLinkedWith;


            this.TotalColumns = 3;
            this.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this.ItemsToShowInList = 10;
            setProperty(propertyInfo);
            
        }
        protected void setProperty(PropertyInfo p)
        {
            this.PropertyInfo = p;
            if (this.PropertyInfo != null)
            {
                this.Label = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(PropertyInfo.Name);
            }
        }

        /// <summary>
        /// Only applies for checkboxes
        /// </summary>
        public int TotalColumns { get; set; }
        /// <summary>
        /// Only applies for checkboxes
        /// </summary>
        public RepeatDirection RepeatDirection { get; set; }

        public override bool EditableInListing
        {
            get
            {
                return false;
            }
            set
            {

            }
        }
        public override bool IsSearchable
        {
            get
            {
                return false;
            }
            set
            {
                
            }
        }
        /*
        public override bool ShowInListing
        {
            get
            {
                return false;
            }
            set
            {
                base.ShowInListing = false;
            }
        }*/

        protected abstract IEnumerable<string> getSelectedValuesForItem(ICmsItemInstance item);

        public new IEnumerable<string> GetValueForObject(ICmsItemInstance item)
        {
            return getSelectedValuesForItem(item);
            
        }

        public override TDataType GetValueForObject<TDataType>(ICmsItemInstance o)
        {
            return (TDataType)this.PropertyInfo.GetValue(o.DbItem, null);
        }


        protected abstract void setValueForCmsItem(ICmsItemInstance item, IEnumerable<string> values);
        

        private OperationResult setValueFromString(ICmsItemInstance item, string sValues)
        {
            OperationResult result = new OperationResult();

          //  var currentList = GetValueForObject(item);
          //  currentList.Clear();
            sValues = sValues?? "";
            string[] sTokens = sValues.Split(new string[] { ","},  StringSplitOptions.RemoveEmptyEntries);
            setValueForCmsItem(item, sTokens);
            return result;

        }
        public override OperationResult SetValueForObjectFromFormControl(ICmsItemInstance o, IMyFormWebControl formControl)
        {
            string s = formControl.GetFormValueAsStr();
            return setValueFromString(o, s);

            
        }
        public override OperationResult SetValueForObject(ICmsItemInstance o, object value)
        {
            if (value is string)
                return setValueFromString(o, (string)value);
            else
            {
                throw new NotImplementedException("This can only be called with string values");
            }
            
        }


        public int ItemsToShowInList { get; set; }

        public override object GetFormValueConvertedToPropertyDataType(object formValue, EnumsCms.SECTION_TYPE sectionType)
        {
            throw new NotImplementedException();
        }
        protected abstract IEnumerable<ListItem> getListItemChoices(ICmsItemInstance item);
       

        /*protected override Control getControlNonEditable(ICmsItemInstance item)
        {
            var selectedList = GetValueForObject(item);
            StringBuilder sb = new StringBuilder();
            foreach (var selItem in selectedList)
            {
                if (sb.Length > 0) sb.Append(", ");

                var cmsItem = LinkedWithFactory.GetCmsItemFromDBObject((BaseDbObject) selItem);
                sb.Append(cmsItem.TitleForCms);
            }
            LiteralControl lit = new LiteralControl();
            lit.Text = sb.ToString();
            return lit;
        }*/

        public override NHibernate.Criterion.ICriterion GetNHibernateCriterionForSearch(string s)
        {
            throw new NotImplementedException("Such a control can never be shown in a listing");
        }

        public override void ApplyOrderByToCriteria(NHibernate.ICriteria crit, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            throw new NotImplementedException("Should never be called");
        }


        private FormFieldBaseData getFormFieldAsCheckboxList(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {


            FormFieldMulipleChoiceDataAsChkBoxes field = new FormFieldMulipleChoiceDataAsChkBoxes();
            field.RepeatDirection = this.RepeatDirection;
            field.TotalColumns = this.TotalColumns;
            //field.HelpMessage = "To select multiple items, hold CTRL and select items.";
            var chkBoxList = field.GetField();

            if (item != null)
            {
                
                var allListItems = getListItemChoicesAndMarkSelected(item);


                chkBoxList.Items.AddRange(allListItems.ToArray());
            }
            return field;
        }

        private IEnumerable<ListItem> getListItemChoicesAndMarkSelected(ICmsItemInstance item)
        {
            var currentSelectedList = GetValueForObject(item);

            var allListItems = getListItemChoices(item);
            //  listBox.SelectedItem = null;
            foreach (ListItem listItem in allListItems)
            {
                listItem.Attributes.Remove("selected");
                listItem.Selected = false;
                foreach (var selItem in currentSelectedList)
                {
                    if (string.Compare(listItem.Value, selItem, true) == 0)
                    {
                        listItem.Attributes["selected"] = "selected";
                        listItem.Selected = true;
                        break;
                    }

                }
            }
            return allListItems;
        }

        private FormFieldBaseData getFormFieldAsListbox(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {


            FormFieldMulipleChoiceListBox field = new FormFieldMulipleChoiceListBox();
            field.ID = "cmsProperty_" + this.PropertyInfo.Name;
            field.Size = this.ItemsToShowInList;
            field.FormFieldCssClass.AddClass("cms-many-to-many");
            field.Title = this.PropertyInfo.Name;
            field.HelpMessage = "To select multiple items, hold CTRL and select items.";
            var listBox = field.GetField();

            if (item != null)
            {
                listBox.SelectedIndex = -1;

                
                var allListItems = getListItemChoicesAndMarkSelected(item);

                listBox.Items.AddRange(allListItems.ToArray());
            }
            return field;


        }

       
        public override FormFieldBaseData GetFormFieldBaseDataForProperty(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {
            FormFieldBaseData field = null;
            switch (this.DisplayType)
            {
                case EnumsCms.MULTICHOICE_DISPLAY_TYPE.CheckboxList:
                    {
                        field = getFormFieldAsCheckboxList(sectionType, item, loadInitialValueFromPostback, value);
                        break;
                    }
                case EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox:
                    {
                        field = getFormFieldAsListbox(sectionType, item, loadInitialValueFromPostback, value);
                        break;
                    }
                default:
                    throw new InvalidOperationException("Invalid display type");

            }
            field.ID = "cmsProperty_" + this.PropertyInfo.Name;
            field.FormFieldCssClass.AddClass("cms-many-to-many");
            field.Title = this.Label;
            
            return field;


        }

    }
}