﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects
{
    public class LinkButtonHelperClass
    {
        public LinkButtonHelperClass(ICmsItemFactory parentItem, ICmsCollectionInfo linkedProperty)
        {
            this.LinkedProperty = linkedProperty;
            this.ParentItem = parentItem;
        }
        public ICmsItemFactory ParentItem { get; set; }
        public ICmsCollectionInfo LinkedProperty { get; set; }
        public string ItemHrefRetriever(object obj)
        {
            ICmsItemInstance item = (ICmsItemInstance)obj;
            return this.LinkedProperty.GetListingUrlForLinkedType(item);
        }

    }
}
