﻿using System;
using BusinessLogic_v3.Controls.WebControls.Specialised.MediaItems;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls;


namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties
{
    using Controls.WebControls.Specialized.MediaItem;

    public class CmsPropertyFormData
    {
        public CmsFieldBase PropertyInfo { get; set; }
        public IMyFormWebControl FormControl { get; set; }
        private CS.WebComponentsGeneralV3.Code.Cms.Pages.BaseCMSPage pg = null;
        public CmsPropertyFormData(CS.WebComponentsGeneralV3.Code.Cms.Pages.BaseCMSPage pg, CmsFieldBase pInfo, IMyFormWebControl formControl)
        {
            this.pg = pg;
            this.PropertyInfo = pInfo;
            this.FormControl = formControl;
            attachForAnyStatusMessages();
        }
        public object GetFormValue(EnumsCms.SECTION_TYPE sectionType )
        {

            object formValue = this.FormControl.GetFormValueObject();
            return this.PropertyInfo.GetFormValueConvertedToPropertyDataType(formValue, sectionType);
        }

        public override string ToString()
        {
            return PropertyInfo.ToString() + ": " + this.FormControl.GetFormValueObject();
            
        }

        private void attachForAnyStatusMessages()
        {
            if (FormControl is MediaItemFormControl)
            {
                MediaItemFormControl ctrl = (MediaItemFormControl)FormControl;
                ctrl.OnDelete_After += new EventHandler(ctrl_OnDelete_After);
                
            }

        }

        void ctrl_OnDelete_After(object sender, EventArgs e)
        {
            pg.Master.Functionality.ShowStatusMessageAndRedirectToSamePage(PropertyInfo.Label + " deleted successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

    }
}