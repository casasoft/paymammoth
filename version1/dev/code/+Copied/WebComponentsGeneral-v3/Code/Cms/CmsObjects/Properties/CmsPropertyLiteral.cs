﻿using System;
using System.Web.UI;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;

namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties
{
    public class CmsPropertyLiteral<TDataItem> : CmsFieldBase
        where TDataItem: BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject
    {
        private Func<TDataItem, Control> valueRetriever;

        public CmsPropertyLiteral(ICmsItemInstance cmsItem, string label, Func<TDataItem, Control> valueRetriever)
            : base(cmsItem)
        {
            this.Label = label;
            this.valueRetriever = valueRetriever;
            this.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne, CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            
        }

        protected Control getValueFor(ICmsItemInstance o)
        {
            Control ctrl = valueRetriever((TDataItem)o.DbItem);
            return ctrl;
            
        }

        public override bool IsReadOnly
        {
            get
            {
                return true;
            }
            protected set
            {
                base.IsReadOnly = value;
            }
        }

        public override bool IsSearchable
        {
            get
            {
                return false;
            }
            set
            {
                base.IsSearchable = value;
            }
        }

        public override TDataType GetValueForObject<TDataType>(ICmsItemInstance o)
        {
            throw new NotImplementedException(this.Label + " - This is a read-only field");
            //string s = getValueFor(o);
            //return CS.General_v3.Util.Other.ConvertStringToBasicDataType<TDataType>(s);
            
            
        }

        public override CS.General_v3.Classes.HelperClasses.OperationResult SetValueForObject(ICmsItemInstance o, object value)
        {

            throw new NotImplementedException(this.Label + " -This is a read-only field");
        }

        public override object GetFormValueConvertedToPropertyDataType(object formValue, EnumsCms.SECTION_TYPE sectionType)
        {
            throw new NotImplementedException(this.Label + " -This is a read-only field");
        }

        public override Controls.WebControls.FormFields.FormFieldsFieldItemDataImpl GetFormFieldBaseDataForProperty(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {
            throw new NotImplementedException(this.Label + " -This is a read-only field");
        }

        protected override System.Web.UI.Control getControlNonEditable(ICmsItemInstance item)
        {
            return getValueFor(item);
            //MySpan span = new MySpan();
            
            //string s = getValueFor(item);
            //span.InnerHtml = s;
            //span.CssManager.AddClass(this.cssClass);

            //return span;
            
        }

        public override void AddCriterionToSearchCriteria(NHibernate.ICriteria crit, string searchValue)
        {
            throw new NotImplementedException("This is a read-only field");
        }


        public override void ApplyOrderByToCriteria(NHibernate.ICriteria crit, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            throw new NotImplementedException("This is a read-only field");
        }

        
    }
}
