﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
using System.Web.UI;
using BusinessLogic_v3.DB;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
using CS.General_v3.Classes.HelperClasses;
using System.Text;
using BusinessLogic_v3.Classes.DB;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public class CmsPropertyManyToManyCollection<TLinkedCmsObject> : CmsPropertyMultiChoice
        where TLinkedCmsObject: ICmsItemInstance
    
       
    {

        public ICmsItemFactory LinkedWithFactory { get; private set; }
       // public PropertyInfo PropertyInfo { get; set; }

        public CmsPropertyManyToManyCollection(ICmsItemFactory itemFactory, ICmsItemFactory factoryLinkedWith, PropertyInfo propertyInfo, 
            Action<IBaseDbObject> clearCollection,
            Action<IBaseDbObject, BusinessLogic_v3.Classes.DB.IBaseDbObject> addItemToCollection,
            EnumsCms.MULTICHOICE_DISPLAY_TYPE displayType)
            : base(itemFactory, propertyInfo, displayType)
        {

            this.LinkedWithFactory = factoryLinkedWith;
            this.clearCollection = clearCollection;
            this.addItemToCollection = addItemToCollection;

        }

        private Action<IBaseDbObject> clearCollection;
        private Action<IBaseDbObject, BusinessLogic_v3.Classes.DB.IBaseDbObject> addItemToCollection;

        private OperationResult setValueFromString(ICmsItemInstance item, IEnumerable<string> sValues)
        {
            OperationResult result = new OperationResult();
            clearCollection(item.DbItem);
            
            
            
            foreach (var token in sValues)
            {
                long id;
                if (long.TryParse(token.Trim(), out id))
                {
                    var linkedItem = LinkedWithFactory.GetCmsItemWithId(id);
                    if (linkedItem != null)
                    {
                        TLinkedCmsObject castedlinkedItem = (TLinkedCmsObject)linkedItem;
                        addItemToCollection(item.DbItem, castedlinkedItem.DbItem);
                        //list.Add(castedlinkedItem.DbItem);
                    }
                }
            }
            //this.PropertyInfo.SetValue(item, list, null);
            //SetValueForObject(item, currentList);
            
            return result;

        }
        protected override IEnumerable<string> getSelectedValuesForItem(ICmsItemInstance item)
        {
            return GetValueForObject(item).ToList().ConvertAll<string>(c => c.ID.ToString());
                
            
        }

        protected override void setValueForCmsItem(ICmsItemInstance item, IEnumerable<string> values)
        {
            setValueFromString(item, values);
            
        }

        protected override IEnumerable<ListItem> getListItemChoices(ICmsItemInstance item)
        {
            return LinkedWithFactory.GetAllAsListItemsForCms(this, false, 0, false);
            
        }

        public new ICollection<IBaseDbObject> GetValueForObject(ICmsItemInstance item)
        {
            List<IBaseDbObject> list = new List<IBaseDbObject>();
            var dataList = GetValueForObject<IEnumerable<IBaseDbObject>>(item);
            list.AddRange(dataList);
            return list;

        }

        public override TDataType GetValueForObject<TDataType>(ICmsItemInstance o)
        {
            return (TDataType)this.PropertyInfo.GetValue(o.DbItem, null);
        }

        protected override Control getControlNonEditable(ICmsItemInstance item)
        {
            var selectedList = GetValueForObject(item);
            StringBuilder sb = new StringBuilder();
            foreach (var selItem in selectedList)
            {
                if (sb.Length > 0) sb.Append(", ");

                var cmsItem = LinkedWithFactory.GetCmsItemFromDBObject((BaseDbObject)selItem);
                sb.Append(cmsItem.TitleForCms);
            }
            LiteralControl lit = new LiteralControl();
            lit.Text = sb.ToString();
            return lit;
        }
    }
}