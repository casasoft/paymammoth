﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;


namespace CS.WebComponentsGeneralV3.Code.Cms.CmsObjects
{
    public class CmsGeneralOperation : ICmsGeneralOperation
    {
        public delegate void NullHandler();

        public CmsGeneralOperation(string title, EnumsCms.IMAGE_ICON icon, string href)
        {
            this.Title = title;
            this.Icon = icon;
            this.Href = href;
        }

        public CmsGeneralOperation(string title, EnumsCms.IMAGE_ICON icon, Action delegateToCall)
        {
            this.Title = title;
            this.Icon = icon;
            this.OnClick += delegateToCall;


        }

        private void _eventHandler(object sender, EventArgs e)
        {
            this.OnClick();
        }

        public EventHandler GetButtonClickAsEventHandler()
        {
            return _eventHandler;
        }

        public Action GetButtonClickAsNullHandler()
        {
            return OnClick;
        }


        public string Title { get; set; }
        public EnumsCms.IMAGE_ICON Icon { get; set; }
        public string ConfirmMessage { get; set; }
        public string Href { get; set; }
        public event Action OnClick;

        
        public bool IsLink()
        {
            return !string.IsNullOrWhiteSpace(Href);
        }
        public bool IsButton()
        {
            return this.OnClick != null;
        }

        public MyButton AddToButtonsBarAsButton(ButtonsBar buttonsBar)
        {
            if (!IsButton())
                throw new InvalidOperationException("Cannot add to buttons bar if this is not a button");
            var btn =  buttonsBar.AddButton(this.Title, this.Icon, this.GetButtonClickAsEventHandler());
                        btn.WebControlFunctionality.ConfirmMessage = this.ConfirmMessage;
                        btn.ValidationGroup = "buttonsBar";
            return btn;
              
        }
        public MyAnchor AddToButtonsBarAsLink(ButtonsBar buttonsBar)
        {
            if (!IsLink())
                throw new InvalidOperationException("Cannot add to buttons bar if this is not a link");
            var btn = buttonsBar.AddLink(this.Title, this.Icon, this.Href);
            return btn;
        }
        public CS.WebComponentsGeneralV3.Code.Controls.WebControls.BaseWebControl AddToButtonsBar(ButtonsBar buttonsBar)
        {
            if (IsButton())
                return AddToButtonsBarAsButton(buttonsBar);
            else
                return AddToButtonsBarAsLink(buttonsBar);
        }
    }
}
