﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms.TestCMSItems
{
    public class Store
    {
        public string Title { get; set; }
        public List<Product> Products { get; set; }

    }
}
