﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3.Code.Cms.TestCMSItems
{
    public class Product
    {
        public Store Store { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        

    }
}
