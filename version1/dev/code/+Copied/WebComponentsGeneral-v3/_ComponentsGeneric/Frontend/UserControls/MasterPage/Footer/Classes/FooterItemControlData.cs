﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.Footer.Classes
{
    public class FooterItemControlData
    {
        public Control Control { get; set; }
        public int Priority { get; set; }
    }
}