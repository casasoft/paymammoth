﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductFullDetails.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.ProductFullDetails" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Products/ProductItemSpecifications.ascx"
    TagName="ProductItemSpecifications" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Gallery/PrettyPhotoImageGallery.ascx"
    TagName="Gallery" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Products/ProductImagesSlider.ascx"
    TagName="ProductImagesSlider" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Products/ProductDescription.ascx"
    TagName="ProductDescription" TagPrefix="Common" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Products/ProductAddToCart.ascx"
    TagName="ProductAddToCart" TagPrefix="Common" %>
<article>
    <div class="product-description-top-container clearfix">
        <h1 runat="server" id="h1ProductTitle">
        </h1>
        <span class="product-description-item-code" runat="server" id="spanItemCode"></span>
    </div>
    <div class="product-description html-container clearfix" runat="server" id="divProductDescription">
        <Common:ProductImagesSlider runat="server" ID="productImagesSlider" />
        <div class="product-main-container clearfix">
                <Common:ProductDescription runat="server" ID="productDescription" />
            <div class="product-add-to-cart" runat="server" id="divProductAddToCart" visible="false" >
                <Common:ProductAddToCart runat="server" ID="productAddToCart" />
            </div>
        </div>
        <div class="product-item-image" runat="server" id="divProductMainImage">
            <CSControls:MyImage runat="server" ID="imgProductMainImage" />
        </div>
    </div>
    <Common:ProductItemSpecifications runat="server" ID="productSpecifications" />
    <Common:Gallery runat="server" ID="galleryCtrl" />
    <div class="product-terms" id="divProductTerms" runat="server">
        <a href="#" target="_blank" class="product-terms-link" id="aTermsLink" runat="server">
            View Terms &amp; Conditions</a>
    </div>
</article>
