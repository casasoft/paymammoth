﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArticleComments.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage.ArticleComments" %>
<%@ Register TagPrefix="CommonControls" TagName="ViewComments" Src="~/_ComponentsGeneric/Frontend/UserControls/Comments/ViewComments.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="SubmitComment" Src="~/_ComponentsGeneric/Frontend/UserControls/Comments/SubmitComment.ascx" %>
<div class="article-comments-wrapper" runat="server" id="divArticleCommentsWrapper">
    <div class="article-comment-title-wrapper">
        <h2 class="article-comments-count" runat="server" id="h2CommentCount">
            2 Comments</h2>
        <a href="#article-post-comment" class="article-post-comment-button">Post Comment</a>
    </div>
    <CommonControls:ViewComments ID="viewComments" runat="server" />
    <a id="article-post-comment"></a>
    <CommonControls:SubmitComment ID="submitComment" runat="server" />
</div>
