﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.SpecialOfferModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ShoppingCartModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart
{
    public partial class ShoppingCartDiscountItems : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private ShoppingCartDiscountItems _control;
            public FUNCTIONALITY(ShoppingCartDiscountItems control)
            {
                _control = control;
            }
            public ShoppingCartBaseFrontend ShoppingCart { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ShoppingCartDiscountItems()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if(this.Functionality.ShoppingCart != null)
            {
                updateTexts();
                loadDiscounts();
            }
            else
            {
                throw new Exception("Shopping Cart cannot be null");
            }
        }

        private void updateTexts()
        {
            

            MyContentText.AttachContentTextWithControl(h2ApplicableDiscounts, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Member_ShoppingCart_Discounts_ApplicableDiscounts).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(divDiscountsText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Member_ShoppingCart_Discounts_ApplicableDiscountsText).ToFrontendBase());
        }

        private void loadDiscounts()
        {
            //var offers =this.Functionality.ShoppingCart.Data.SpecialOfferCalculator.GetSpecialOffersApplicableOnlyToCart().ToFrontendBaseList();
            var offers = this.Functionality.ShoppingCart.Data.GetAppliedSpecialOffers().ToFrontendBaseList();
            if (offers != null && offers.Count() > 0)
            {
                var price = this.Functionality.ShoppingCart.Data.GetTotalPrice(true, false);

                foreach (SpecialOfferBaseFrontend offer in offers)
                {
                    createListItem(offer, price);
                }
            }
            else
            {
                this.Visible = false;
            }
        }

        private void createListItem(SpecialOfferBaseFrontend offer, double price)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            string title = offer.Data.Title;

            double totalDiscount = this.Functionality.ShoppingCart.Data.SpecialOfferCalculator.GetTotalDiscountForAllCartForSpecialOffer(offer.Data);
            string totalPrice = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(totalDiscount, General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
            
            MyListItem li = new MyListItem();
            li.InnerHtml = title;

            ulDiscounts.Controls.Add(li);
        }

        public static ShoppingCartDiscountItems LoadControl(Page pg)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/CheckOutDiscountsItem.ascx") as ShoppingCartDiscountItems;
            return ctrl;
        }
    }
}