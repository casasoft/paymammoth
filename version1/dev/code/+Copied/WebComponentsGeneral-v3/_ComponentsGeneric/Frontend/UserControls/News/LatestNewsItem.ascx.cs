﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ArticleModule;
using CS.General_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.News
{
    public partial class LatestNewsItem : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private LatestNewsItem _control;
            public FUNCTIONALITY(LatestNewsItem control)
            {
                _control = control;
            }
            public ArticleBaseFrontend Article { get; set; }
            public bool IsEven { get; set; }
            public bool IsLastItem { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public LatestNewsItem()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
        }

        private void updateTexts()
        {
            if(this.Functionality.Article != null)
            {
                if(this.Functionality.IsEven)
                {
                    liItem.Attributes.Add("class", " even");
                }

                if (this.Functionality.IsLastItem)
                {
                    liItem.Attributes.Add("class", " last-item");
                }

                var dayDateFormat = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.LatestNews_Day_DateFormat);
                var monthDateFormat = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.LatestNews_Month_DateFormat);
                var yearDateFormat = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.LatestNews_Year_DateFormat);

                if(this.Functionality.Article.Data.LastEditedOn.HasValue)
                {
                    spanDay.InnerHtml = this.Functionality.Article.Data.LastEditedOn.Value.ToString(dayDateFormat);
                    spanMonth.InnerHtml = this.Functionality.Article.Data.LastEditedOn.Value.ToString(monthDateFormat);
                    spanYear.InnerHtml = this.Functionality.Article.Data.LastEditedOn.Value.ToString(yearDateFormat);
                }
                else
                {
                    CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(spanNewsDate);
                }
                anchorNews.HRef = this.Functionality.Article.GetUrl();
                spanNewsTitle.InnerHtml = this.Functionality.Article.Data.PageTitle;
            }
            else
            {
                throw new Exception("Artic  le - News cannot be null");
            }
        }

        public static LatestNewsItem LoadControl(Page pg)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/News/LatestNewsItem.ascx") as LatestNewsItem;
            return ctrl;
        }
    }
}