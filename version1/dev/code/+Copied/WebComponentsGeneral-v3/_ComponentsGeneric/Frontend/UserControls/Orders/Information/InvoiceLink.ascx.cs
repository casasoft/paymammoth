﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules.ContentTextModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Orders.Information
{
    public partial class InvoiceLink : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private InvoiceLink _control;
            public IOrderBase Order { get; set; }
            public FUNCTIONALITY(InvoiceLink control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public InvoiceLink()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if (Functionality.Order != null)
            {
                loadInvoiceLink();
            }
        }

        private void loadInvoiceLink()
        {
            IContentTextBase ct =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Orders_Invoice_Link);

            var replacer = ct.CreateTokenReplacerNew();
            replacer["[INVOICE_URL]"] = Functionality.Order.ToFrontendBase().GetURL();
            replacer["[INVOICE_NUMBER]"] = Functionality.Order.Reference;

            MyContentText.AttachContentTextWithControl(spanCTInvoiceLink, ct.ToFrontendBase(), replacer);
        }

        public static InvoiceLink LoadControl (Page page)
        {
            return
                (InvoiceLink)
                page.LoadControl("/_ComponentsGeneric/Frontend/UserControls/Orders/Information/InvoiceLink.ascx");
        }
    }
}