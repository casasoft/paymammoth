﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Carousel.CarouFredSel.v1;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    public partial class BrandsLogosScroller : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private BrandsLogosScroller _control;
            public FUNCTIONALITY(BrandsLogosScroller control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public BrandsLogosScroller()
        {
            this.Functionality = createFunctionality();
            this.CacheParams.SetCacheDependencyFromFactories(true, BusinessLogic_v3.Modules.Factories.BrandFactory);
            this.CacheParams.CacheControl = false;
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadBrandsLogos();
            initJSCarousel();
        }

        private void loadBrandsLogos()
        {
            var brands = BusinessLogic_v3.Modules.Factories.BrandFactory.GetAllBrandsForListing().ToFrontendBaseList();
            if(brands != null && brands.Count()> 0)
            {
                foreach (var brand in brands)
                {
                    //for (int i = 0; i < 10; i++)
                    //{
                        var _ctrlItem = BrandsLogosScrollerItem.LoadControl(this.Page);
                        _ctrlItem.Functionality.Brand = brand;
                        ulBrandsLogos.Controls.Add(_ctrlItem);
                    //}
                }
            }
        }

        private void initJSCarousel()
        {
            bool useCarouselForBrandsScroller = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.Products_BrandsCarouselSlider_Enabled);
            if (useCarouselForBrandsScroller)
            {
                CarouFredSelV1Parameters carouselParams = new CarouFredSelV1Parameters();

                int itemWidth = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(Enums.BusinessLogicSettingsEnum.Products_BrandsCarouselSlider_ItemWidth);
                int itemHeight = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(Enums.BusinessLogicSettingsEnum.Products_BrandsCarouselSlider_ItemHeight);
                int items = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(Enums.BusinessLogicSettingsEnum.Products_BrandsCarouselSlider_Items);
                int carouselDuration = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(Enums.BusinessLogicSettingsEnum.Products_BrandsCarouselSlider_Duration);
                //carouselParams.width = carouselWidth;
                //carouselParams.height = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(Enums.BusinessLogicSettingsEnum.Products_BrandsCarouselSlider_Height);
                if (itemWidth > 0 || itemHeight > 0)
                {
                    carouselParams.items = new CarouFredSelV1ItemParameters();
                    if (itemWidth > 0) carouselParams.items.width = itemWidth;
                    if (itemHeight > 0) carouselParams.items.height = itemHeight;
                }
                carouselParams.align = "center";
                CarouFredSelV1AutoParameters auto = new CarouFredSelV1AutoParameters();
                carouselParams.auto = auto;
                carouselParams.auto.pauseOnHover = "immediate";
                if (items > 0) carouselParams.auto.items = 1;
                carouselParams.auto.pauseDuration = 2000;
                // auto.easing = "linear";
                auto.duration = carouselDuration;
                CarouFredSelV1 carousel = new CarouFredSelV1(ulBrandsLogos, "brands-logos-carousel", carouselParams);
            }
        }
    }
}
