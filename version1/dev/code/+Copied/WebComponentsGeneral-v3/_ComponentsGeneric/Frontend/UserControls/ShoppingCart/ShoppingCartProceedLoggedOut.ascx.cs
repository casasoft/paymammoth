﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart
{
    public partial class ShoppingCartProceedLoggedOut : BaseUserControl
    {
        private const string LOGIN_VALIDATION_GROUP = "cartLoginFields";
        public class FUNCTIONALITY
        {
            private ShoppingCartProceedLoggedOut _control;
            public FUNCTIONALITY(ShoppingCartProceedLoggedOut control)
            {
                _control = control;
            }
            public string RegisterUrl { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ShoppingCartProceedLoggedOut()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initHandlers();
            initLogin();
            updateTexts();
        }

        private void initHandlers()
        {
            btnProceedToCheckOut.Click += new EventHandler(btnProceedToCheckOut_Click);
        }

        void btnProceedToCheckOut_Click(object sender, EventArgs e)
        {
            Session.Add(BusinessLogic_v3.Constants.SessionIdentifiers.CART_REGISTRATION, true);
            CS.General_v3.Util.PageUtil.RedirectPage(this.Functionality.RegisterUrl);
        }

        private void initLogin()
        {
            var articleCheckOut = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.ShoppingCart_DeliveryDetails);
            if(articleCheckOut != null)
            {
                loginCtrl.Functionality.OnLoginRedirectToURL = articleCheckOut.GetUrl();
                loginCtrl.Functionality.DoNoShowNavigationLinksContainer = true;
                loginCtrl.Functionality.ValidationGroup = LOGIN_VALIDATION_GROUP;
            }
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(h2ProceedLoginTitle, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_ProceedToCheckOut_LoggedOut_Login_Title).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(divProceedLoginText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_ProceedToCheckOut_LoggedOut_Login_Text).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(h2ProceedRegisterTitle, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_ProceedToCheckOut_LoggedOut_Register_Title).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(divProceedRegisterText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_ProceedToCheckOut_LoggedOut_Register_Text).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(btnProceedToCheckOut, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_MyCart_ProceedToCheckout).ToFrontendBase());
        }
    }
}