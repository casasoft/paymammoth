﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckOutManagementArea.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.CheckOutManagementArea" %>
<%@ Register TagPrefix="CommonControls" TagName="ShoppingCartPriceDetails" Src="~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/ShoppingCartPriceDetails.ascx" %>
<div class="checkout-management-area clearfix" id="divCheckoutManagementArea" runat="server">
    <div class="discount-coupon-wrapper">
        <span class="discount-coupon-input-text" id="spanDiscountCouponInputText" runat="server">
        </span>
        <div class="discount-coupon-input-fields" id="divDiscountCouponInputFields" runat="server">
            <CSControls:MyTxtBoxString runat="server" ID="txtBoxCoupon">
            </CSControls:MyTxtBoxString>
            <CSControls:MyButton runat="server" ID="btnApply">
            </CSControls:MyButton>
        </div>
    </div>
    <CommonControls:ShoppingCartPriceDetails runat="server" ID="shoppingCartPriceDetails" />
</div>
