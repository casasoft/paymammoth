﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.SocialNetworking.Facebook
{
    using System;
    using System.Web.UI;
    using Code.Controls.UserControls;
    using CS.General_v3;

    public partial class FacebookActivityFeed : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private FacebookActivityFeed _control;
            public FUNCTIONALITY(FacebookActivityFeed control)
            {
                _control = control;
                this.Width = 300;
                this.Height = 300;
                this.ColourScheme = Enums.FACEBOOK_COLOUR_SCHEME.Light;
            }
            public string URL { get; set; }
            public bool ShowRecommendations { get; set; }
            public bool ShowHeader { get; set; }
            public int Width { get; set; }
            public Enums.FACEBOOK_COLOUR_SCHEME ColourScheme { get; set; }
            public Enums.FACEBOOK_FONT Font { get; set; }
            public Enums.FACEBOOK_LINK_TARGET LinkTarget { get; set; }
            public int Height { get; set; }
        }
       
        public FUNCTIONALITY Functionality { get; private set; }

        public int Width { get { return this.Functionality.Width; } set { this.Functionality.Width = value; } }
        public int Height { get { return this.Functionality.Height; } set { this.Functionality.Height = value; } }
        public bool ShowHeader { get { return this.Functionality.ShowHeader; } set { this.Functionality.ShowHeader = value; } }
        public bool ShowRecommendations { get { return this.Functionality.ShowRecommendations; } set { this.Functionality.ShowRecommendations = value; } }
        public string URL { get { return this.Functionality.URL; } set { this.Functionality.URL = value; } }
        public Enums.FACEBOOK_COLOUR_SCHEME ColourScheme { get { return this.Functionality.ColourScheme; } set { this.Functionality.ColourScheme = value; } }
        public Enums.FACEBOOK_FONT Font { get { return this.Functionality.Font; } set { this.Functionality.Font = value; } }
        public Enums.FACEBOOK_LINK_TARGET LinkTarget { get { return this.Functionality.LinkTarget; } set { this.Functionality.LinkTarget = value; } }
     
        public FacebookActivityFeed()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initFacebookActivityFeed();
        }

        private void initFacebookActivityFeed()
        {

            string src = "//www.facebook.com/plugins/activity.php?site";
            if (!string.IsNullOrWhiteSpace(this.Functionality.URL))
            {
                src += "=" + Server.UrlEncode(this.Functionality.URL);
            }

            src += "&width=" + this.Width;
            src += "&colorscheme=" + CS.General_v3.Util.EnumUtils.StringValueOf(this.Functionality.ColourScheme);
            src += "&font=" + CS.General_v3.Util.EnumUtils.StringValueOf(this.Functionality.Font);
            src += "&height=" + this.Height;
            src += "&header=" + this.Functionality.ShowHeader.ToString().ToLower();
            src += "&recommendations=" + this.Functionality.ShowRecommendations.ToString().ToLower();
            src += "&linktarget=" + CS.General_v3.Util.EnumUtils.StringValueOf(this.Functionality.LinkTarget);

            iFrameLikeButton.Attributes["src"] = src;
            iFrameLikeButton.Style.Add(HtmlTextWriterStyle.Width, this.Functionality.Width + "px");
            iFrameLikeButton.Style.Add(HtmlTextWriterStyle.Height, this.Functionality.Height + "px");

        }
    }
}