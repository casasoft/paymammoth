﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.FileItemModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Files
{
    public partial class FileListingItems : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private FileListingItems _control;
            public FUNCTIONALITY(FileListingItems control)
            {
                _control = control;
            }
            public IEnumerable<FileItemBaseFrontend> Files { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public FileListingItems()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if (this.Functionality.Files != null && this.Functionality.Files.Count() > 0)
            {
                updateTexts();
                loadFiles();
            }
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(thDateUploaded, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Members_FileListing_Listing_DateUploaded).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thFileName, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Members_FileListing_Listing_Filename).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thCourse, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Members_FileListing_Listing_Course).ToFrontendBase());
        }

        private void loadFiles()
        {
            foreach (var file in this.Functionality.Files)
            {
                var _ctrl = FileListingItem.LoadControl(this.Page);
                _ctrl.Functionality.FileItem = file;
                tBodyListing.Controls.Add(_ctrl);
            }
        }
    }
}