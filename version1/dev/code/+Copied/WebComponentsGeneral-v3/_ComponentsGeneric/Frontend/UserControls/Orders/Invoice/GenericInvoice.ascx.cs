﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.General_v3.Classes.Login;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.OrderModule;
using System.Web.UI.HtmlControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.OrderItemModule;
using BusinessLogic_v3.Classes.Text;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Orders.Invoice
{


    public partial class GenericInvoice : BaseUserControlWithInnerContent
    {
        public class TaxesSummaryItem
        {
            public double Rate { get; set; }
            public double ExcTaxes { get; set; }
            public double TaxAmount { get; set; }
            public double IncTaxes { get; set; }
        }
        public class TaxesSummary
        {
            public CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 CurrencySymbol { get; set; }
            public Dictionary<double, TaxesSummaryItem> Taxes { get; private set; }

            public TaxesSummary()
            {
                this.Taxes = new Dictionary<double, TaxesSummaryItem>();
                this.CurrencySymbol = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217FromCode(BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Ecommerce_DefaultCurrencyCode)).GetValueOrDefault( General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro);
            }

            public void AddTax(OrderItemBaseFrontend item)
            {
                //TODO: 2012-06-05 Karl make sure this is connected well
                AddTax(item.Data.TaxRate, item.Data.GetTotalPriceExcludingTaxes(reduceDiscount: true), item.Data.GetTaxAmountPerUnit() * item.Data.Quantity, item.Data.GetTotalPrice(reduceDiscount: true));
            }

            public void AddTax(double rate, double totalExcTaxes, double taxAmount, double totalIncTaxes)
            {
                if (!this.Taxes.ContainsKey(rate))
                {
                    this.Taxes[rate] = new TaxesSummaryItem();
                    this.Taxes[rate].Rate = rate;
                }
                this.Taxes[rate].ExcTaxes += totalExcTaxes;
                this.Taxes[rate].TaxAmount += taxAmount;
                this.Taxes[rate].IncTaxes += totalIncTaxes;
            }
        }

        public class FUNCTIONALITY
        {
            private GenericInvoice _control;

            public string LogoImageURL { get; set; }
            public string BusinessAddress { get; set; }
            public string BusinessTel { get; set; }
            public string BusinessMob { get; set; }
            public string BusinessFax { get; set; }
            public string BusinessEmail { get; set; }
            public bool Paid { get; set; }
            public DateTime? PaidOn { get; set; }
            public string PaymentMethod { get; set; }
            public string OnlinePaymentURL { get; set; }

            public string InvoiceNo { get; set; }
            public string InvoiceDate { get; set; }
            public string ClientFullName { get; set; }
            public string ClientAddress { get; set; }
            public string ClientVATNumber { get; set; }

            public string PaymentTerms { get; set; }
            public string SubtotalValue { get; set; }
            public string DiscountValue { get; set; }
            public string ShippingValue { get; set; }
            public string TotalValue { get; set; }
            public TaxesSummary TaxesAmounts { get; private set; }

            public ContentTextBaseFrontend BusinessTelLabel { get; set; }
            public ContentTextBaseFrontend BusinessMobLabel { get; set; }
            public ContentTextBaseFrontend BusinessFaxLabel { get; set; }
            public ContentTextBaseFrontend BusinessEmailLabel { get; set; }
            public ContentTextBaseFrontend InvoiceHeadingTitle { get; set; }
            public ContentTextBaseFrontend ClientDetailsTitle { get; set; }
            public ContentTextBaseFrontend InvoiceDetailsTitle { get; set; }
            public ContentTextBaseFrontend InvoiceItemsTitle { get; set; }
            public ContentTextBaseFrontend PaymentTermsTitle { get; set; }
            public ContentTextBaseFrontend SubtotalLabel { get; set; }
            public ContentTextBaseFrontend DiscountLabel { get; set; }
            public ContentTextBaseFrontend ShippingLabel { get; set; }
            public ContentTextBaseFrontend TotalLabel { get; set; }

            public ContentTextBaseFrontend InvoiceNoLabel { get; set; }
            public ContentTextBaseFrontend DateLabel { get; set; }
            public string InvoiceHeadingSubTitle { get; set; }

            public bool ShowDiscount { get; set; }
            public bool ShowShipping { get; set; }


            public FUNCTIONALITY(GenericInvoice control)
            {
                _control = control;
                this.TaxesAmounts = new TaxesSummary();
                initDefaultValues();

            }

            private void initDefaultValues()
            {
                this.BusinessAddress = CS.General_v3.Settings.CompanyInfo.GetAddressAsOneLine(", <br />", true);
                this.BusinessEmail = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Email);
                this.BusinessFax = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Fax);
                this.BusinessMob = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Mobile);
                this.BusinessTel = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Telephone);
                this.LogoImageURL = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_InvoiceLogoURL);
                this.ShowDiscount = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(CS.General_v3.Enums.SETTINGS_ENUM.MembersArea_Invoice_ShowDiscount);
              
                this.ShowShipping = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(CS.General_v3.Enums.SETTINGS_ENUM.MembersArea_Invoice_ShowShipping);
            }
            public void FillValuesFromOrder(OrderBaseFrontend order)
            {
                ClientAddress = order.Data.GetAddressAsOneLine(", <br />");
                this.OnlinePaymentURL = order.GetPayOnlineUrl();
                var member = order.Data.Member;
                if (member != null)
                {
                    ClientAddress = member.Email + " (ID: " + member.ID + ")" + "<br />" + ClientAddress ;
                    ClientFullName = ((IUser)order.Data.Member).FullName;    
                }
                this.Paid = order.Data.Paid;
                this.PaidOn = order.Data.PaidOn;
                if (order.Data.PaymentMethod.HasValue)
                {
                    this.PaymentMethod = CS.General_v3.Util.EnumUtils.StringValueOf(order.Data.PaymentMethod.Value);
                }
                InvoiceHeadingSubTitle = InvoiceNo = order.Data.Reference;
                InvoiceDate = order.Data.LastUpdatedOn.ToString("dd/MM/yyyy HH:mm");
            }

        }
        public FUNCTIONALITY Functionality { get; private set; }
        public GenericInvoice()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        
        private void updateContactDetailsItem(Control container, HtmlGenericControl ctrlLabel, HtmlGenericControl ctrlValue, ContentTextBaseFrontend label, Control value)
        {
            if (value != null)
            {
                MyContentText.AttachContentTextWithControl(ctrlLabel, label);
                ctrlValue.Controls.Clear();
                ctrlValue.Controls.Add(value);
                
            }
            else
            {
                container.Visible = false;
            }
        }
        private void initOnlinePayment()
        {
            if (!this.Functionality.Paid && !string.IsNullOrEmpty(this.Functionality.OnlinePaymentURL))
            {
                TokenReplacerNew tr = new TokenReplacerNew();
                tr[BusinessLogic_v3.Constants.Tokens.INVOICE_PAYMENT_URL] = this.Functionality.OnlinePaymentURL;
                MyContentText.AttachContentTextWithControl(divOnlinePayment, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_OnlinePaymentFacility).ToFrontendBase(), tr);
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divOnlinePayment);
            }
        }

        private void initPaid()
        {
            MyContentText.AttachContentTextWithControl(thPaid, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_PaidTitle).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(tdPaid, this.Functionality.Paid ?
                BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_PaidYes).ToFrontendBase() :
                BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_PaidNo).ToFrontendBase());
            initOnlinePayment();
            if (this.Functionality.Paid)
            {
                MyContentText.AttachContentTextWithControl(thPaidOn, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_PaidOnTitle).ToFrontendBase());
                MyContentText.AttachContentTextWithControl(thPaymentMethod, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_PaymentMethodTitle).ToFrontendBase());
                if (this.Functionality.PaidOn.HasValue)
                {
                    tdPaidOn.InnerText = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatDateTimeWithCurrentCulture(this.Functionality.PaidOn.Value, General_v3.Util.Date.DATETIME_FORMAT.ShortDateShortTime);
                }
                else
                {
                    CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(trPaidOn);
                }

                if (!string.IsNullOrEmpty(this.Functionality.PaymentMethod))
                {
                    tdPaymentMethod.InnerText = this.Functionality.PaymentMethod;
                }
                else
                {
                    CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(trPaymentMethod);
                }

            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(trPaidOn);
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(trPaymentMethod);
            }

        }

        private void initTexts()
        {
            imgLogo.ImageUrl = this.Functionality.LogoImageURL;
            businessAddress.InnerHtml = this.Functionality.BusinessAddress;
            updateContactDetailsItem(businessTelContainer, businessTelLabel, businessTel, this.Functionality.BusinessTelLabel,
                (!string.IsNullOrEmpty(this.Functionality.BusinessTel))? new Literal() { Text = this.Functionality.BusinessTel } : null);

            updateContactDetailsItem(businessEmailContainer, businessEmailLabel, businessEmail, this.Functionality.BusinessEmailLabel, 
                (!string.IsNullOrEmpty(this.Functionality.BusinessEmail)) ? new  MyEmail() { Email = this.Functionality.BusinessEmail }: null);
            
            updateContactDetailsItem(businessFaxContainer, businessFaxLabel, businessFax, this.Functionality.BusinessFaxLabel, 
                (!string.IsNullOrEmpty(this.Functionality.BusinessFax)) ? new Literal() { Text = this.Functionality.BusinessFax } : null);

            updateContactDetailsItem(businessMobContainer, businessMobLabel, businessMob, this.Functionality.BusinessMobLabel, 
                (!string.IsNullOrEmpty(this.Functionality.BusinessMob)) ?new Literal() { Text = this.Functionality.BusinessMob }: null);

            MyContentText.AttachContentTextWithControl(invoiceHeadingTitle, this.Functionality.InvoiceHeadingTitle);
            MyContentText.AttachContentTextWithControl(clientDetailsTitle, this.Functionality.ClientDetailsTitle);
            MyContentText.AttachContentTextWithControl(invoiceDetailsTitle, this.Functionality.InvoiceDetailsTitle);
            MyContentText.AttachContentTextWithControl(invoiceItemsTitle, this.Functionality.InvoiceItemsTitle);

            invoiceHeadingSubtitle.InnerHtml = this.Functionality.InvoiceHeadingSubTitle;
            MyContentText.AttachContentTextWithControl(invoiceNoHeaderCell, this.Functionality.InvoiceNoLabel);
            MyContentText.AttachContentTextWithControl(invoiceDateHeaderCell, this.Functionality.DateLabel);

            invoiceNoCell.InnerHtml = this.Functionality.InvoiceNo;
            invoiceDateCell.InnerHtml = this.Functionality.InvoiceDate;
            clientName.InnerHtml = this.Functionality.ClientFullName;
            clientAddress.InnerHtml = this.Functionality.ClientAddress;

            if (!string.IsNullOrEmpty(this.Functionality.PaymentTerms))
            {
                MyContentText.AttachContentTextWithControl(paymentTermsTitle, this.Functionality.PaymentTermsTitle);
                paymentTermsContent.InnerHtml = this.Functionality.PaymentTerms;
            }
            else
            {
                paymentTermsContainer.Visible = false;
            }

            MyContentText.AttachContentTextWithControl(subtotalHeaderCell, this.Functionality.SubtotalLabel);
            //subtotalHeaderCell.InnerHtml = this.Functionality.SubtotalLabel +": ";

            subtotalCell.InnerHtml = this.Functionality.SubtotalValue;
            showTaxes();
            if (this.Functionality.ShowDiscount)
            {
                MyContentText.AttachContentTextWithControl(discountHeaderCell, this.Functionality.DiscountLabel);
                this.discountCell.InnerHtml = this.Functionality.DiscountValue;
                //this.discountHeaderCell.InnerHtml = this.Functionality.DiscountLabel + ": ";
            }
            else
            {
                this.trDiscount.Visible = false;
            }
            if (this.Functionality.ShowShipping)
            {
                MyContentText.AttachContentTextWithControl(shippingHeaderCell, this.Functionality.ShippingLabel);
                this.shippingCell.InnerHtml = this.Functionality.ShippingValue;
                //this.discountHeaderCell.InnerHtml = this.Functionality.DiscountLabel + ": ";
            }
            else
            {
                this.trShipping.Visible = false;
            }
            this.totalCell.InnerHtml = this.Functionality.TotalValue;

            MyContentText.AttachContentTextWithControl(totalHeader, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_TotalHeaderPriceText).ToFrontendBase());
            //this.totalHeader.InnerHtml = this.Functionality.TotalLabel;

        }
        private void showTaxes()
        {
            string currCode = null;

            currCode = CS.General_v3.Enums.ISO_ENUMS.GetCurrencySymbol(this.Functionality.TaxesAmounts.CurrencySymbol, true);

            MyContentText.AttachContentTextWithControl(thVatSummaryExcVAT, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_VATSummary_HeaderExcVAT).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thVatSummaryIncVAT, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_VATSummary_HeaderIncVAT).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thVatSummaryVatAmount, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_VATSummary_HeaderVATAmount).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thVatSummaryVatRate, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_VATSummary_HeaderVATRate).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(tdVatSummaryTotalsLabel, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_VATSummary_TotalsLabel).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(hVatSummaryTitle, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_VATSummary_Title).ToFrontendBase());

            tbodyVatSummary.Controls.Clear();
            double totalNet = 0, taxes = 0, total = 0;
            foreach (double vatRate in this.Functionality.TaxesAmounts.Taxes.Keys)
            {
                var taxValues = this.Functionality.TaxesAmounts.Taxes[vatRate];
                MyTableRow tr = new MyTableRow();
                var tdTaxRate = tr.AddCell("vat-summary-item-tax-rate");
                var tdNet = tr.AddCell("vat-summary-item-net");
                var tdTax = tr.AddCell("vat-summary-item-tax");
                var tdTotal = tr.AddCell("vat-summary-item-total");
                tdTaxRate.InnerHtml = CS.General_v3.Util.NumberUtil.FormatNumber(vatRate) + "%";
                tdNet.InnerHtml = CS.General_v3.Util.NumberUtil.FormatCurrency(taxValues.ExcTaxes, currencySymbol: currCode);
                tdTax.InnerHtml = CS.General_v3.Util.NumberUtil.FormatCurrency(taxValues.TaxAmount, currencySymbol: currCode);
                tdTotal.InnerHtml = CS.General_v3.Util.NumberUtil.FormatCurrency(taxValues.IncTaxes, currencySymbol: currCode);
                tbodyVatSummary.Controls.Add(tr);
                totalNet += taxValues.ExcTaxes;
                taxes += taxValues.TaxAmount;
                total += taxValues.IncTaxes;
            }
            tdVatSummaryTotalsNet.InnerHtml = CS.General_v3.Util.NumberUtil.FormatCurrency(totalNet, currencySymbol: currCode);
            tdVatSummaryTotalsTaxes.InnerHtml = CS.General_v3.Util.NumberUtil.FormatCurrency(taxes, currencySymbol: currCode);
            tdVatSummaryTotalsTotal.InnerHtml = CS.General_v3.Util.NumberUtil.FormatCurrency(total, currencySymbol: currCode);

            /*
            foreach (var item in this.Functionality.Taxes)
            {
                HtmlTableRow trTax = new HtmlTableRow();
                HtmlTableCell tdLabel = new HtmlTableCell("th");
                HtmlTableCell tdValue = new HtmlTableCell();
                trTax.Controls.Add(tdLabel);
                tdLabel.Controls.Add(item.Key);
                tdValue.InnerHtml = item.Value.ToString();
                trTax.Controls.Add(tdValue);
                placeholderTaxes.Controls.Add(trTax);
            }*/
        }


        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initValues();
            initTexts();
            initPaid();
        }

        private void initValues()
        {
            this.Functionality.BusinessTelLabel = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_TelephoneLabel).ToFrontendBase();
            this.Functionality.BusinessMobLabel = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_MobileLabel).ToFrontendBase();
            this.Functionality.BusinessFaxLabel = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Fax).ToFrontendBase();
            this.Functionality.BusinessEmailLabel = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_EmailLabel).ToFrontendBase();
            this.Functionality.InvoiceHeadingTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_HeadingTitle).ToFrontendBase();
            this.Functionality.ClientDetailsTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_ClientDetailsTitle).ToFrontendBase();
            this.Functionality.InvoiceDetailsTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_DetailsTitle).ToFrontendBase();
            this.Functionality.InvoiceItemsTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_DetailsTitle).ToFrontendBase();
            this.Functionality.PaymentTermsTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_PaymentTermsTitle).ToFrontendBase();
            this.Functionality.SubtotalLabel = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_SubTotal).ToFrontendBase();
            this.Functionality.DiscountLabel = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_DiscountText).ToFrontendBase();
            this.Functionality.ShippingLabel = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_ShippingText).ToFrontendBase();
            this.Functionality.TotalLabel = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_TotalPriceText).ToFrontendBase();
            this.Functionality.InvoiceNoLabel = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_InvoiceNoLabel).ToFrontendBase();
            this.Functionality.DateLabel = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Invoice_InvoiceDateText).ToFrontendBase();
        }

        protected override Control _container
        {
            get { return divItemsContentContainer; }
        }
    }
}