﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Extensions;
namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login
{
    public partial class LoginPageContent : BaseUserControl
    {
        private ArticleBaseFrontend _article;



        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadPage();
        }

        private void loadPage()
        {
            _article =
                BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                    BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_LoginPage).ToFrontendBase();
            if (_article != null)
            {
                contentPage.Functionality.ContentPage = _article;
            }
        }

        public LoginFields LoginFields { get { return loginFields; } }

    }
}