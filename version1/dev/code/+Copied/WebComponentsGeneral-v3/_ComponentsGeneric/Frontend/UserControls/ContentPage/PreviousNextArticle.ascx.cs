﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ArticleModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage
{
    public partial class PreviousNextArticle : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private PreviousNextArticle _control;
            public FUNCTIONALITY(PreviousNextArticle control)
            {
                _control = control;
            }

            public ArticleBaseFrontend Article { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public PreviousNextArticle()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if(this.Functionality.Article != null)
            {
                initPreviousArticle();
                initNextArticle();
                updateTexts();
            }
        }

        private void updateTexts()
        {
            //MyContentText.AttachContentTextWithControl(anchorNextArticle, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_PreviousArticle).ToFrontendBase());
            //MyContentText.AttachContentTextWithControl(anchorPreviousArticle, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_NextArticle).ToFrontendBase());
        }

        private void initPreviousArticle()
        {
            ArticleBaseFrontend previousArticle = this.Functionality.Article.Data.GetPreviousArticle().ToFrontendBase();
            if (previousArticle != null)
            {
                anchorPreviousArticle.HRef = previousArticle.GetUrl();
                anchorPreviousArticle.InnerHtml = "&laquo; " + previousArticle.Data.Title;
            }
            else
            {
                anchorPreviousArticle.Parent.Controls.Remove(anchorPreviousArticle);
            }
        }

        private void initNextArticle()
        {
            ArticleBaseFrontend nextArticle = this.Functionality.Article.Data.GetNextArticle().ToFrontendBase();
            if (nextArticle != null)
            {
                anchorNextArticle.HRef = nextArticle.GetUrl();
                anchorNextArticle.InnerHtml = nextArticle.Data.Title + " &raquo;";
            }
            else
            {
                anchorNextArticle.Parent.Controls.Remove(anchorNextArticle);
            }
        }
    }
}