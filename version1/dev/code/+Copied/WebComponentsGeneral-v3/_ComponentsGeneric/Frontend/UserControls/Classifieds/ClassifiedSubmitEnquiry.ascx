﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClassifiedSubmitEnquiry.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Classifieds.ClassifiedSubmitEnquiry" %>
<div class="submit-enquiry-container">
    <div class="submit-enquiry-title-container">
        <h3 class="submit-enquiry-title" runat="server" id="spanPostEnquiryTitle">
            Post Comment</h3>
    </div>
    <div class="submit-enquiry-please-login" id="divNeedLogin" runat="server">
        <div class="submit-enquiry-please-login-text" id="divNeedLoginText" runat="server">
            <p>
                You must be logged in to your account in order to submit an enquiry. Choose one of
                the below options:</p>
        </div>
        <ul class="submit-enquiry-choose-options">
            <li class="submit-enquiry-login-facebook"><a id="aCommentsLoginWithFb"
                runat="server">Connect with Facebook</a></li>
            <li class="submit-enquiry-register" id="liRegisterAccount" runat="server"><a href="{Article:Register}">
                Register an account</a></li>
            <li class="submit-enquiry-login" id="liLogin" runat="server"><a href="{Article:Login}">
                Login using your account</a></li>
        </ul>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#<asp:Literal ID="ltlFbLinkId" runat="server"></asp:Literal>').click(function () {
                    showLoginOverlay('<asp:Literal ID="ltlOverlayTitleText" runat="server"></asp:Literal>');
                });
            });
        </script>
    </div>
    <div class="submit-enquiry-fields-container" id="divFields" runat="server">
        <div class="submit-enquiry-description-text" id="divEnquiryDescription" runat="server">
            <p>
                You can also send a direct only enquiry online by filling in the form below and the email will be delivered directly to the owner.</p>
        </div>
        <CSControlsFormFields:FormFields ID="submitEnquiryFields" runat="server">
        </CSControlsFormFields:FormFields>
    </div>
</div>