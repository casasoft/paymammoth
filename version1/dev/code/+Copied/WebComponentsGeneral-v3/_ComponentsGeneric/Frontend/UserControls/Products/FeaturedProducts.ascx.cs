﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    using System;
    using System.Linq;
    using BusinessLogic_v3;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using BusinessLogic_v3.Extensions;

    public partial class FeaturedProducts : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private FeaturedProducts _control;
            public FUNCTIONALITY(FeaturedProducts control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public FeaturedProducts()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initFeaturedProducts();
            updateTexts();
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(h2FeaturedProducts,BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Products_FeaturedProducts).ToFrontendBase());
        }

        private void initFeaturedProducts()
        {
            var featuredProductsAmt = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_ShowAmtFeaturedProducts);
            var loadRandomIfNothingIsFeatured = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_LoadRandomIfNothingIsFeatured);

            var featuredProductsList = BusinessLogic_v3.Modules.Factories.ProductFactory.GetRandomFeaturedItems(featuredProductsAmt, loadRandomIfNothingIsFeatured).ToFrontendBaseList();
            if (featuredProductsList != null && featuredProductsList.Count() > 0)
            {
                featuredProducts.Functionality.Items = featuredProductsList;
            }
            else
            {
                this.Visible = false;
            }
        }
    }
}