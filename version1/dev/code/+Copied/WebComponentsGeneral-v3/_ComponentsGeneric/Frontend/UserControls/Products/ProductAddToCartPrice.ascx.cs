﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ProductModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    public partial class ProductAddToCartPrice : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private ProductAddToCartPrice _control;
            public FUNCTIONALITY(ProductAddToCartPrice control)
            {
                _control = control;
            }
            public ProductBaseFrontend Product { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ProductAddToCartPrice()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateContent();
        }

        private void updateContent()
        {
            if (this.Functionality.Product.HasDiscount)
            {
                spanPrice.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture
                    (this.Functionality.Product.Data.PriceInfo.GetUnitPrice(reduceDiscount: false), General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
                spanNewPrice.Visible = true;
                spanNewPrice.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture
                   (this.Functionality.Product.Data.PriceInfo.GetTotalPrice(), General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
                spanPrice.Attributes["class"] += " product-add-to-cart-old-price";
            }
            else
            {
                spanPrice.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture
                    (this.Functionality.Product.Data.PriceInfo.GetTotalPrice(), General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
            }
        }

        public static ProductAddToCartPrice LoadControl(Page pg)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/Products/ProductAddToCartPrice.ascx") as ProductAddToCartPrice;
            return ctrl;
        }
    }
}