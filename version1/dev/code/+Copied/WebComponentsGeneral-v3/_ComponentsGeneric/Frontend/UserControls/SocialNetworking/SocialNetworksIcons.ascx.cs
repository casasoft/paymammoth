﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.General_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.SocialNetworking
{
    public partial class SocialNetworksIcons : BaseUserControl
    {
        public string CssClass { get { return this.Functionality.CssClass; } set { this.Functionality.CssClass = value; } }
        public class FUNCTIONALITY
        {
            private SocialNetworksIcons _control;
            public FUNCTIONALITY(SocialNetworksIcons control)
            {
                _control = control;
            }
            public string CssClass { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public SocialNetworksIcons()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            
            base.OnLoad(e);
        }

        private void init()
        {
            updateContent();
            updateCss();
        }

        private void updateCss()
        {
            if(!string.IsNullOrEmpty(this.Functionality.CssClass))
            {
                divSocialNetworksContainer.Attributes["class"] += " " + this.Functionality.CssClass;
            }
        }

        private void updateContent()
        {
            string facebookLink = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook);
            if(!string.IsNullOrEmpty(facebookLink))
            {
                anchorFacebook.HRef = facebookLink;
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(anchorFacebook);
            }

            string twitterLink = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Twitter);
            if (!string.IsNullOrEmpty(twitterLink))
            {
                anchorTwitter.HRef = twitterLink;
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(anchorTwitter);
            }
            
            string linkedInLink = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_LinkedIn);
            if (!string.IsNullOrEmpty(linkedInLink))
            {
                anchorLinkedIn.HRef = linkedInLink;
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(anchorLinkedIn);
            }
        }
    }
}