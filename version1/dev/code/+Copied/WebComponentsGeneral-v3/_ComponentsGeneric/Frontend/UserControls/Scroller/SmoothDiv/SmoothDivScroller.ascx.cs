﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Scroller.SmoothDiv
{
    public partial class SmoothDivScroller : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private SmoothDivScroller _control;
            public SmoothDivScrollerParams Parameters { get; set; }

            public List<Control> Items { get; set; }
            public FUNCTIONALITY(SmoothDivScroller control)
            {
                _control = control;
                this.Items = new List<Control>();

                this.Parameters = new SmoothDivScrollerParams() {
                     scrollableArea = ".scrollableArea"
                };
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public SmoothDivScroller()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        private void initItems()
        {
            ulScrollableArea.Controls.Clear();
            foreach (var item in this.Functionality.Items)
            {
                MyListItem li = new MyListItem();
                li.CssManager.AddClass("scrollable-area-item");
                li.Controls.Add(item);
                ulScrollableArea.Controls.Add(li);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            initItems();
            
            base.OnLoad(e);
        }
        private void initJS()
        {
            string id = this.divScroller.ClientID;
           // this.Functionality.Parameters.hiddenOnStart = true;
            //this.Functionality.Parameters.visibleHotSpots = "onstart";
            //this.Functionality.Parameters.hotSpotsVisibleTime = "3";
            string js = "jQuery('#"+id+"').smoothDivScroll(" + this.Functionality.Parameters.GetJSON() + ");";
            //This is called to refresh the left / right arrows displayed initially
            js += "jQuery('#" + id + "').smoothDivScroll('"+(string.IsNullOrEmpty(this.Functionality.Parameters.autoScroll) ? "stopAutoScroll" : "startAutoScroll") +"');";
            JSUtil.AddJSScriptToPage(js);
        }

        protected override void OnPreRender(EventArgs e)
        {
            initJS();
            base.OnPreRender(e);
        }
    }
}