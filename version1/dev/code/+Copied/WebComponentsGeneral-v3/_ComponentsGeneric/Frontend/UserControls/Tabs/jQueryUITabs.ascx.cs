﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Tabs
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using Newtonsoft.Json;
    using CS.WebComponentsGeneralV3.Code.Util;


    public partial class jQueryUITabs : BaseUserControl
    {
        public class JQueryUITabOptions : JSONObject
        {
            public object disabled;
            public bool? cache;
            public bool? collapsible;
            public object cookie;
            public bool? deselectable;
            [JsonProperty("event")]
            public string Event;
            public string idPrefix;
            public string panelTemplate;
            public int? selected;
            public string spinner;
            public string tabTemplate;
        }
        public class JQueryUITabsData
        {
            public ContentTextBaseFrontend Title { get; set; }
            public Control Content { get; set; }
            public string CssClass { get; set; }
            public string TabIconURL { get; set; }
        }


        #region jQueryUITabs Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected jQueryUITabs _item = null;

            public List<JQueryUITabsData> Data { get; private set; }
            public JQueryUITabOptions Options { get; private set; }
            internal FUNCTIONALITY(jQueryUITabs item)
            {
                this.Data = new List<JQueryUITabsData>();
                this.Options = new JQueryUITabOptions();
                this._item = item;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			


        private void init()
        {
            ulTabs.Controls.Clear();
            placeholderTabsContent.Controls.Clear();
            foreach (var item in this.Functionality.Data)
            {
                MyListItem li = new MyListItem();
                li.CssManager.AddClass("jqueryui-tab");

                if (!string.IsNullOrEmpty(item.CssClass))
                {
                    li.CssManager.AddClass(item.CssClass + "-tab");
                }

                MyAnchor aTab = new MyAnchor();
                aTab.CssManager.AddClass("jqueryui-tab-link");
                MyContentText.AttachContentTextWithControl(aTab, item.Title);
                if (!string.IsNullOrEmpty(item.TabIconURL))
                {
                    MyImage imgIcon = new MyImage();
                    imgIcon.CssManager.AddClass("jqueryui-tab-icon");
                    imgIcon.ImageUrl = item.TabIconURL;
                    imgIcon.AlternateText = item.Title.GetContent();
                    li.Controls.Add(imgIcon);
                }
                li.Controls.Add(aTab);
                ulTabs.Controls.Add(li);

                MyDiv divTabContent = new MyDiv();
                divTabContent.CssManager.AddClass("jqueryui-tab-link");
                placeholderTabsContent.Controls.Add(divTabContent);
                    li.CssManager.AddClass(item.CssClass + "-tab");
                if (item.Content != null)
                {
                    divTabContent.Controls.Add(item.Content);
                }
                if (!string.IsNullOrEmpty(item.CssClass))
                {
                    divTabContent.CssManager.AddClass(item.CssClass);
                }

                //<li><a href="#tabs-1">Nunc tincidunt</a></li>


                aTab.Href = "#" + divTabContent.ClientID;
            }
        }
        private void initJS()
        {
            string js = "window.setTimeout(function() { jQuery('#"+divTabsContainer.ClientID+"').tabs("+this.Functionality.Options.GetJSON()+"); }, 50);";
            js = JSUtil.GetDeferredJSScriptThroughJQuery(js, true);
            placeholderTabsContent.Controls.Add(new Literal() { Text = js });
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            initJS();
            base.OnLoad(e);
        }
        public override string ClientID
        {
            get
            {
                return this.divTabsContainer.ClientID;
            }
        }
    }
}