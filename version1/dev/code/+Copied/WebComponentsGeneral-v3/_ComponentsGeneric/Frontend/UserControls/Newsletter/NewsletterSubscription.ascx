﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsletterSubscription.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Newsletter.NewsletterSubscription" %>
<div class="subscribe-form-container">
    <div class="subscribe-text" runat="server" id="divSubscribeText">
    </div>
    <div class="subscribe-form-content">
        <div class="subscribe-form">
            <CSControls:MyTxtBoxString runat="server" ID="txtName">
            </CSControls:MyTxtBoxString>
            <CSControls:MyTxtBoxEmail runat="server" ID="txtEmail">
            </CSControls:MyTxtBoxEmail>
        </div>
        <div class="subscribe-form-footer clearfix">
            <span runat="server" id="spanAssuranceText" class="subscribe-form-footer-assurance-text">
            </span>
            <CSControls:MyButton runat="server" ID="btnSubmit" CssClass="subscribe-button">
            </CSControls:MyButton>
        </div>
    </div>
</div>
