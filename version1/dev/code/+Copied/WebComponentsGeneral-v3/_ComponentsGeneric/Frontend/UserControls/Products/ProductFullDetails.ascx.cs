﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Constants;
using BusinessLogic_v3.Frontend.ProductVariationMediaItemModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
    using Code.Classes.Javascript.UI.Products;
    using Code.Cms.Classes;
    using Code.Controls.UserControls;
    using Code.Controls.WebControls.Common;
    using General_v3.Classes.Interfaces.ImageItem;
    using General_v3.Util;
using BusinessLogic_v3.Frontend.ProductModule;
    using BusinessLogic_v3.Extensions;
    using CS.WebComponentsGeneralV3.Code.Util;
    using BusinessLogic_v3;

    public partial class ProductFullDetails : BaseUserControl
    {


        #region ProductFullDetails Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected ProductFullDetails _item = null;

            public ProductBaseFrontend Product { get; set; }
            public ProductAddToCart ProductAddToCart { get { return _item.productAddToCart; } set { _item.productAddToCart = value; } }

            internal FUNCTIONALITY(ProductFullDetails item)
            {
                ContractsUtil.RequiresNotNullable(item, item + "cannot be null");
                this._item = item;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion


        private void initAddToCart()
        {
            var shopEnabled = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Shop_Ecommerce_Enabled);
            if (!shopEnabled)
            {
                divProductAddToCart.Parent.Controls.Remove(divProductAddToCart);
            }
            else
            {
                productAddToCart.Functionality.Product = this.Functionality.Product;
                initProductVariationsController();
                divProductAddToCart.Visible = true;
            }
        }

        private void initTermsLink()
        {
            bool showTerms = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_ShowViewTerms);
            if (showTerms)
            {
                var articleTerms = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.TermsConditions).ToFrontendBase();
                aTermsLink.HRef = articleTerms.GetUrl();
                var ctTerms = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ProductPage_ViewTermsAndConditions).ToFrontendBase();
                MyContentText.AttachContentTextWithControl(aTermsLink, ctTerms);
            }
            else
            {
                divProductTerms.Visible = false;
            }
        }

        private void initProductVariationsController()
        {
            ProductVariationControllerCarouFredSelParameters jsParams = new ProductVariationControllerCarouFredSelParameters();
            productAddToCart.Functionality.ProductVariationControllerParameters = jsParams;
            productImagesSlider.Functionality.ProductVariationControllerCarouFredSelParameters = jsParams;
        }

        private void updateContent()
        {
            if (this.Functionality.Product != null)
            {
                productImagesSlider.Functionality.Product = productDescription.Functionality.Product = this.Functionality.Product;
                this.Page.Functionality.SetEditInCmsButtonObject(this.Functionality.Product.Data);
                h1ProductTitle.InnerHtml = this.Functionality.Product.GetTitle();

                productSpecifications.Functionality.Product = this.Functionality.Product;
                if(!string.IsNullOrEmpty(this.Functionality.Product.Data.ReferenceCode))
                {
                    var cntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_ItemCode).ToFrontendBase();
                    var dict = cntTxt.CreateTokenReplacer();
                    dict.Add(Tokens.PRODUCT_ITEM_CODE, this.Functionality.Product.Data.ReferenceCode);
                    MyContentText.AttachContentTextWithControl(spanItemCode, cntTxt, dict);
                }
                else
                {
                    spanItemCode.Visible = false;
                }
                
            }
        }
        private void updateOpenGraphImage()
        {
            var img = this.Functionality.Product.Data.GetMainImage();
            if (img != null)
            {
                string url = img.Image.GetSpecificSizeUrl(ProductVariationMediaItemBase.ImageSizingEnum.Normal);
                if (!string.IsNullOrWhiteSpace(url))
                {
                    url = CS.General_v3.Util.PageUtil.ConvertRelativeUrlToAbsoluteUrl(url);
                    this.Page.Functionality.MetaTagsController.UpdateOpenGraphTag(General_v3.Enums.OPEN_GRAPH.Image, url);
                }
            }
        }

        private void updateGallery()
        {
            bool showGallery = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_FullPageShowGallery);
            bool showMainImage = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_FullPageShowMainTopImage);

            if (showGallery)
            {
                IEnumerable<ProductVariationMediaItemBaseFrontend> images =
                    this.Functionality.Product.Data.GetAllImages(!showMainImage).ToFrontendBaseList();
                if (images != null && images.Count() > 0)
                {
                    galleryCtrl.Functionality.Gallery = images;
                    galleryCtrl.Functionality.LastItemPerRow = 3;
                }
                else
                {
                    galleryCtrl.Parent.Controls.Remove(galleryCtrl);
                }
            }
            else
            {
                galleryCtrl.Visible = false;
            }

            if (showMainImage)
            {
                updateMainImages(showMainImage);
            }
            else
            {
                divProductMainImage.Visible = false;
            }
        }

        private void updateMainImages(bool showMainImage)
        {

            if (showMainImage)
            {
                var img = this.Functionality.Product.Data.GetMainImage();
                if (img != null)
                {
                    imgProductMainImage.ImageUrl = img.Image.GetSpecificSizeUrl(ProductVariationMediaItemBase.ImageSizingEnum.Thumbnail);

                    imgProductMainImage.AlternateText = img.GetAlternateText();
                    imgProductMainImage.HRef = img.Image.GetSpecificSizeUrl(ProductVariationMediaItemBase.ImageSizingEnum.Normal);
                    imgProductMainImage.Rel = "prettyPhoto[fullPageGallery]";
                    PrettyPhotoUtil.InitJS();
                }
                else
                {
                    showMainImage = false;
                }
            }

            imgProductMainImage.Visible = showMainImage;
        }

        protected override void OnLoad(EventArgs e)
        {
            initTermsLink();
            updateContent();
            initAddToCart();
            updateGallery();
            updateOpenGraphImage();
            base.OnLoad(e);
        }
    }
}