﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.SocialNetworking.AddThis.Buttons
{
    using Code.Classes.SocialNetworking;

    public partial class FacebookLikeButton : AddThisButtonBase
    {

				
		#region FacebookLikeButton Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected FacebookLikeButton _item = null;
            public FacebookLikeButtonOptions Options { get; private set; }
            internal FUNCTIONALITY(FacebookLikeButton item)
            {
                this._item = item;
                this.Options = new FacebookLikeButtonOptions();
                this.Options.layout = General_v3.Enums.FACEBOOK_LAYOUT_STYLE.BoxCount;
            }
            
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			

        public FacebookLikeButton()
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CopyAttributesFromClassToButton(aButton, this.Functionality.Options, "fb:like:");
        }
    }
}