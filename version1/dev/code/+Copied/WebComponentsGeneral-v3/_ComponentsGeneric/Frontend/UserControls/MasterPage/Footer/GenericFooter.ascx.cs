﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.Footer.Classes;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3;
using BusinessLogic_v3.Modules;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.Footer
{
    public partial class MainFooter : BaseUserControl
    {
        private List<FooterItemControlData> _items;
        public class FUNCTIONALITY
        {
            private MainFooter _control;
            public FUNCTIONALITY(MainFooter control)
            {
                _control = control;
            }
            public List<FooterItemControlData> FooterItems { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public MainFooter()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initTable();
            updateTexts();
        }

        private void initTable()
        {
            if(!ModuleSettings.Shop.IsCurrentWebsiteAShop)
            {
                divFooterProductCategories.Parent.Controls.Remove(divFooterProductCategories);
            }
            else
            {
                initProductCategories();
            }
        }

        private void initProductCategories()
        {
            var cntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Footer_FooterProductCategories_Title).ToFrontendBase();
            ProductCategoriesListing.Functionality.CssClass = "footer-content-title";
            ProductCategoriesListing.Functionality.Title = cntTxt;
        }

        private void updateTexts()
        {
            initContactDetails();
        }


        private void initContactDetails()
        {
            ContactUsDetails.Functionality.DoNotShowTitleAsHeading = true;
            ContactUsDetails.Functionality.ContactDetailsTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Footer_FooterContact_Title).ToFrontendBase();
            ContactUsDetails.Functionality.IsVisibleInFooter = BusinessLogic_v3.Modules.ModuleSettings.Contact.IsContactDetailsVisibleInFooter;
        }
    }
}