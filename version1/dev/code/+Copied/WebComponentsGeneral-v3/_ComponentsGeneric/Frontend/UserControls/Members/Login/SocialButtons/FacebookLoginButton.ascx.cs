﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3;
using Brickred.SocialAuth.NET.Core.BusinessObjects;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Constants;
using BusinessLogic_v3.Modules.ArticleModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login.SocialButtons
{
    public partial class FacebookLoginButton : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private FacebookLoginButton _control;
            public string SuccessUrl { get { return getSuccessUrl(); } set { _successUrl = value; } }
            private string _successUrl { get; set; }
            public MyCheckBoxWithLabel ControlRememberMeCheckBox;
            public FUNCTIONALITY(FacebookLoginButton control)
            {
                _control = control;
            }

            private string getSuccessUrl()
            {
                if (_successUrl == null)
                {
                    string currentUrlFullyQualified = string.Empty;
                    string currentUrlRelative = string.Empty;
                    var cp = this._control.Page.Functionality.ContentPage;
                    if (cp == null || (((IArticleBase)cp.Data).DoNotRedirectOnLogin.HasValue && ((IArticleBase)cp.Data).DoNotRedirectOnLogin.Value))
                    {
                        currentUrlRelative =
                            BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                                BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Home).GetUrl();
                    }
                    else
                    {
                        currentUrlRelative = PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                    }
                    currentUrlFullyQualified = PageUtil.ConvertRelativeUrlToAbsoluteUrl(currentUrlRelative);
                    if (currentUrlRelative.StartsWith("/"))
                    {
                        currentUrlRelative = currentUrlRelative.Substring(1);
                    }
                    _successUrl = currentUrlFullyQualified;
                }
                return _successUrl;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public FacebookLoginButton()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initFacebookLogin();
            initCSSClass();
        }

        private void initCSSClass()
        {
            btnFacebookLogin.CssClass = "social-login-facebook social-button";
        }

        private void initFacebookLogin()
        {
            ContentTextBaseFrontend ctFacebookLoginBtnText =
                Factories.ContentTextFactory.GetContentTextForEnumValue(
                    Enums.CONTENT_TEXT.General_LoginFields_FacebookLoginButtonText).ToFrontendBase();
            btnFacebookLogin.Text = ctFacebookLoginBtnText.GetContent();
            btnFacebookLogin.Click += new EventHandler(btnFacebookLogin_Click);
        }

        void btnFacebookLogin_Click(object sender, EventArgs e)
        {
            generateFacebookLogin();
        }

        private void generateFacebookLogin()
        {
            checkRememberMe();
            SocialAuthUser oUser = new SocialAuthUser(PROVIDER_TYPE.FACEBOOK);
            oUser.Login(returnUrl: Functionality.SuccessUrl, errorRedirectURL: "error.aspx");
            //todo: match return urls with settings or other non hardcoded value
        }

        private void checkRememberMe()
        {
            if (this.Functionality.ControlRememberMeCheckBox != null)
            {
                bool rememberMe = Functionality.ControlRememberMeCheckBox.GetFormValueObjectAsBool();
                CS.General_v3.Util.SessionUtil.SetObject(CS.General_v3.Constants.SESSION_SOCIAL_LOGIN_REMEMBERME, rememberMe);
            }
            else
            {
                CS.General_v3.Util.SessionUtil.RemoveObject(CS.General_v3.Constants.SESSION_SOCIAL_LOGIN_REMEMBERME);
            }
        }

        public static FacebookLoginButton LoadControl(Page page)
        {
            return
                (FacebookLoginButton)
                page.LoadControl(
                    "/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/FacebookLoginButton.ascx");
        }
    }
}