﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Modules.BrandModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.BrandModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    public partial class BrandsLogosScrollerItem : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private BrandsLogosScrollerItem _control;
            public FUNCTIONALITY(BrandsLogosScrollerItem control)
            {
                _control = control;
            }

            public BrandBaseFrontend Brand { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public BrandsLogosScrollerItem()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            updateContent();
            IEnumerable<IBrandBase> x = BusinessLogic_v3.Modules.Factories.BrandFactory.GetAllBrandsForListing();
            base.OnLoad(e);
        }

        private void updateContent()
        {
            if (this.Functionality.Brand != null)
            {
                imgBrandLogo.ImageUrl = this.Functionality.Brand.Data.BrandLogo.GetSpecificSizeUrl(BrandBase.BrandLogoSizingEnum.Thumbnail);
                imgBrandLogo.HRef = CS.General_v3.Util.Text.FormatUrlToIncludeHttp(this.Functionality.Brand.Data.Url);
                imgBrandLogo.HRefTarget = this.Functionality.Brand.Data.HrefTarget;
            }
        }

        public static BrandsLogosScrollerItem LoadControl(Page pg)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/Products/BrandsLogosScrollerItem.ascx") as BrandsLogosScrollerItem;
            return ctrl;
        }
    }
}