﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ShippingMethodModule;
using BusinessLogic_v3.Modules.ShoppingCartModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ShoppingCartModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart
{
    public partial class ShoppingCartPriceDetails : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private ShoppingCartPriceDetails _control;
            public FUNCTIONALITY(ShoppingCartPriceDetails control)
            {

                _control = control;
            }
            public bool IsInCart { get; set; }
            public ShoppingCartDeliveryDetailsHelper DeliveryDetails { get;set;}
            public ShoppingCartBaseFrontend ShoppingCart { get; set; }
            public string CurrentDeliveryMethodUrlLocation { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ShoppingCartPriceDetails()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initHandlers();
            initFields();
            updateTexts();
            updateValues();
        }

        private void updateValues()
        {
            var discount = this.Functionality.ShoppingCart.Data.GetTotalDiscount();
            if (discount == 0)
            {
                trDetailsDiscount.Visible = false;
            }
            else
            {
                tdDiscountValue.InnerHtml = "-" + BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(discount, General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
            }
            tdSubTotalValue.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(this.Functionality.ShoppingCart.Data.GetTotalPrice(true, false), General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
           
            var totalPrice = this.Functionality.ShoppingCart.Data.GetTotalPrice(includeShipping: !this.Functionality.IsInCart);
            tdTotalValue.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(totalPrice, General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
        }

        private void initFields()
        {
            IEnumerable<ShippingMethodBaseFrontend> deliveryMethods = null;

            if(this.Functionality.IsInCart)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(trDetailsDelivery);
            }

            if (this.Functionality.DeliveryDetails != null && !this.Functionality.IsInCart)
            {
                deliveryMethods = BusinessLogic_v3.Modules.Factories.ShippingMethodFactory.GetApplicableDeliveryMethodsForCountry(this.Functionality.DeliveryDetails.Country).ToFrontendBaseList();
                if (deliveryMethods != null && deliveryMethods.Count() > 0)
                {
                    foreach (var deliveryMethod in deliveryMethods)
                    {
                        double cost = Functionality.ShoppingCart.Data.GetDeliveryCostsForShippingMethod(
                            deliveryMethod.Data, this.Functionality.DeliveryDetails.Country);
                        string title = deliveryMethod.Data.Title + " (" + BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(cost, General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency) + ")";

                        CS.General_v3.Classes.URL.URLClass urlClass = new General_v3.Classes.URL.URLClass();
                        urlClass.QS.Clear();
                        urlClass.QS.Add(BusinessLogic_v3.Constants.ParameterNames.ID, deliveryMethod.Data.ID.ToString());

                        var listItem = new ListItem()
                                           {
                                               Text = title,
                                               Value = urlClass.GetURL()
                                           };
                        if (this.Functionality.DeliveryDetails.ShippingMethod == null)
                        {
                            listItem.Selected = deliveryMethod.Data.IsDefaultMethod;
                        }
                        else if (this.Functionality.DeliveryDetails.ShippingMethod.ID == deliveryMethod.Data.ID)
                        {
                            listItem.Selected = true;
                        }
                        dlDeliveryMethods.Functionality.AddItemFromListItem(listItem);
                    }
                }
            }
        }

        private void initHandlers()
        {
            dlDeliveryMethods.OnChangeRedirectToValue = true;
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(tdDeliveryLbl, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_Details_Delivery).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(tdDiscountLbl, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_Details_Discount).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(tdTotal, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_MyCart_Total).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(tdSubTotalLbl, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_MyCart_SubTotal).ToFrontendBase());
        }
    }
}