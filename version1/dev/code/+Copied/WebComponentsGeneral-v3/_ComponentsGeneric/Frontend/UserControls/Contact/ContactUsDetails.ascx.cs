﻿using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Contact
{
    using System;
    using System.Web.UI;
    using System.Text;
    using BusinessLogic_v3.Modules;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using CS.General_v3;
    using System.Collections.Generic;
    using CS.WebComponentsGeneralV3.Code.Classes.Contact;
    using System.Web.UI.HtmlControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.General_v3.Util;
    using CS.WebComponentsGeneralV3.Code.Cms.Classes;
    using BusinessLogic_v3.Modules.SettingModule;

    public partial class ContactUsDetails : BaseUserControl
    {

        private ContactUsDetailsController _contactUsDetailsController;

        private const string PRETTYPHOTO_POSTFIX_HREF = "?iframe=true";
        private const string PRETTYPHOTO_WIDTH = "&width=";
        private const string PRETTYPHOTO_HEIGHT = "&height=";

        public bool DoNotShowHeading { get { return this.Functionality.DoNotShowHeading; } set { this.Functionality.DoNotShowHeading = value; } }


        public class FUNCTIONALITY
        {
            private ContactUsDetails _control;
            public FUNCTIONALITY(ContactUsDetails control)
            {
                _control = control;
                this.LoadDetailsFromSettings = true;
                this.LabelSuffix = ": ";
            }
            public List<ContactDetailsData> ContactDetailsItems { get; set; }
            public void RemoveContactDetailsItemByIdentifiers(params BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS[] identifiers)
            {
                List<BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS> listIdentifiers = new List<BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS>();
                listIdentifiers.AddRange(identifiers);
                if (this.ContactDetailsItems != null)
                {
                    for (int i = 0; i < this.ContactDetailsItems.Count; i++)
                    {
                        if (listIdentifiers.Contains(this.ContactDetailsItems[i].Identifier))
                        {
                            this.ContactDetailsItems.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }

            public bool LoadDetailsFromSettings { get; set; }
            public ContentTextBaseFrontend ContactDetailsTitle { get; set; }
            public bool IsAddressInTable { get; set; }
            public bool DoNotShowHeading { get; set; }
        
            // Map - Pretty Photo
            public ContentTextBaseFrontend MapAnchorText { get; set; }
            public string MapAnchorURL { get; set; }
            public string PrettyPhotoMapHeight { get; set; }
            public string PrettyPhotoMapWidth { get; set; }
            public bool UsePrettyPhoto { get; set; }

            public string LabelSuffix { get; set; }

            //Labels
            public ContentTextBaseFrontend LblTelepohone { get; set; }
            public ContentTextBaseFrontend LblFax { get; set; }
            public ContentTextBaseFrontend LblEmail { get; set; }
            public ContentTextBaseFrontend LblWebsite { get; set; }
            public ContentTextBaseFrontend LblAddress { get; set; }
            public ContentTextBaseFrontend LblMap { get; set; }
            public ContentTextBaseFrontend LblTwitter { get; set; }
            public ContentTextBaseFrontend LblFacebook { get; set; }
            public ContentTextBaseFrontend LblLinkedin { get; set; }
            public ContentTextBaseFrontend LblSkype { get; set; }
            
            //Fields
            public string DirectorName { get; set; }
            public string Mobile { get; set; }
            public string Telephone { get; set; }
            public string Fax { get; set; }
            public string Email { get; set; }
            public string Website { get; set; }
            public string Map { get; set; }
            public string Address { get; set; }
            public string TwitterURL { get; set; }
            public string FacebookURL { get; set; }
            public string LinkedinURL { get; set; }
            public string SkypeURL { get; set; }
            public string SkypeName { get; set; }

            public bool DoNotShowTitleAsHeading { get; set; }
            public bool IsVisibleInFooter { get; set; }
            public bool ShowSocialNetworksAsIcons { get; set; }
        }
    
        public FUNCTIONALITY Functionality { get; private set; }
        public ContactUsDetails()
        {
            this.Functionality = createFunctionality();
            
            this.Functionality.Map = "enlarge map";
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        private void initController()
        {
            _contactUsDetailsController = ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<ContactUsDetailsController>();
            _contactUsDetailsController.UserControl = this;
            _contactUsDetailsController.OnLoad();

        }

        protected override void OnLoad(EventArgs e)
        {
            
            init();
            initController();
            initTable();
            base.OnLoad(e);
        }

        private void init()
        {
            initLoadContent();
        }

        private void initLoadContent()
        {
            updateTexts();
            updateHeading();

            if (this.Functionality.LoadDetailsFromSettings)
            {
                loadSettings();
            }
            initContactDetailsItems();
        }

        private void updateTexts()
        {
            this.Functionality.LblEmail = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Email).ToFrontendBase();
            this.Functionality.LblAddress = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Address).ToFrontendBase();
            this.Functionality.LblFax = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Fax).ToFrontendBase();
            this.Functionality.LblMap = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Map).ToFrontendBase();
            this.Functionality.LblTelepohone = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Telephone).ToFrontendBase();
            this.Functionality.LblWebsite = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Website).ToFrontendBase();
            this.Functionality.MapAnchorText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_MapText).ToFrontendBase();
            this.Functionality.LblTwitter = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Twitter).ToFrontendBase();
            this.Functionality.LblFacebook = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Facebook).ToFrontendBase();
            this.Functionality.LblLinkedin = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Linkedin).ToFrontendBase();
            this.Functionality.LblSkype = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Skype).ToFrontendBase();
            
            if(this.Functionality.ContactDetailsTitle == null)
            {
                this.Functionality.ContactDetailsTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Title).ToFrontendBase();  
            }
        }
        private IEnumerable<ContactDetailsData> sortList(IEnumerable<ContactDetailsData> list)
        {
            return CS.General_v3.Util.ListUtil.SortByMultipleComparers(list, (x1, x2) => x1.Priority.CompareTo(x2.Priority));

        }

        private void initTable()
        {
            if (this.Functionality.ContactDetailsItems != null && this.Functionality.ContactDetailsItems.Count > 0)
            {
                var list = sortList(this.Functionality.ContactDetailsItems);
                foreach (ContactDetailsData item in list)
                {
                    MyTableRow trRow = new MyTableRow();
                    MyTableCell tdValue = new MyTableCell();

                    if(!BusinessLogic_v3.Modules.ModuleSettings.Contact.DoNotShowLabelWithContactDetails)
                    {
                        MyTableCell tdLabel = new MyTableCell();
                        tdLabel.CssClass = "label";



                        if (item.Label != null)
                        {
                            MySpan spanLabelText = new MySpan();
                            spanLabelText.CssManager.AddClass("contact-details-label-text");

                            MyContentText.AttachContentTextWithControl(spanLabelText, item.Label);
                            tdLabel.Controls.Add(spanLabelText);

                            if (!string.IsNullOrEmpty(this.Functionality.LabelSuffix))
                            {
                                MySpan suffix = new MySpan();
                                suffix.CssManager.AddClass("contact-details-label-suffix");
                                suffix.InnerText = this.Functionality.LabelSuffix;
                                tdLabel.Controls.Add(suffix);
                            }
                            trRow.Controls.Add(tdLabel);
                        }
                        else
                        {
                            tdValue.ColumnSpan = 2;
                        }
                    }


                    tdValue.CssClass = "field";
                    if(!string.IsNullOrEmpty(item.CssClass))
                    {
                        tdValue.WebControlFunctionality.CssManager.AddClass(item.CssClass);
                    }
                    tdValue.Controls.Add(item.Value);
                    if (item.Value is IAttributeAccessor && item.SettingData != null)
                    {
                        CMSInlineEditingItem.Create<ISettingBase>(item.SettingData, (x => x.Value), (IAttributeAccessor)(item.Value));
                    }

                    trRow.Controls.Add(tdValue);

                    tblDetails.Controls.Add(trRow);
                }
            }
        }

        private void initContactDetailsItems()
        {
            this.Functionality.ContactDetailsItems = Functionality.ContactDetailsItems;
            if (this.Functionality.ContactDetailsItems == null)
            {
                this.Functionality.ContactDetailsItems = new List<ContactDetailsData>();
            }

            //Telephone
            {
                if (!String.IsNullOrEmpty(this.Functionality.Telephone))
                {
                    ContactDetailsData data = new ContactDetailsData
                                                  {
                                                      Label = this.Functionality.LblTelepohone,
                                                      CssClass = "field-contact-telephone",
                                                      Priority = 10,
                                                      Value = new MySpan() { InnerText = this.Functionality.Telephone, CssClass="field-contact-telephone-content"},
                                                       Identifier = BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS.Telephone

                                                  };

                    this.Functionality.ContactDetailsItems.Add(data);
                    if(this.Functionality.IsVisibleInFooter)
                    {
                        var showInFooter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowTelephoneInFooter);
                        if (!showInFooter)
                        {
                            this.Functionality.ContactDetailsItems.Remove(data);
                        }
                    }
                }
            }
            //Mobile
            {
                if (!String.IsNullOrEmpty(this.Functionality.Mobile))
                {
                    ContactDetailsData data = new ContactDetailsData
                    {
                        Label = BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Mobile.GetContentTextFrontend(),
                        CssClass = "field-contact-mobile",
                        Priority = 10,
                        Value = new MySpan() { InnerText = this.Functionality.Mobile, CssClass = "field-contact-mobile-content" },
                        Identifier = BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS.Mobile

                    };

                    this.Functionality.ContactDetailsItems.Add(data);
                    if (this.Functionality.IsVisibleInFooter)
                    {
                        var showInFooter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowMobileInFooter);
                        if (!showInFooter)
                        {
                            this.Functionality.ContactDetailsItems.Remove(data);
                        }
                    }
                }
            }
            //Skype
            {
                if (!String.IsNullOrEmpty(this.Functionality.SkypeURL))
                {
                    ContactDetailsData data = new ContactDetailsData
                                                  {
                                                      Label = this.Functionality.LblSkype,
                                                      CssClass = "field-contact-skype",
                                                      Priority = 15,
                                                      Value =
                                                          new MyAnchor
                                                              {
                                                                  Href = this.Functionality.SkypeURL,
                                                                  InnerText = this.Functionality.SkypeName
                                                              }, Identifier = BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS.SkypeURL
                                                  };

                    this.Functionality.ContactDetailsItems.Add(data);
                    if (this.Functionality.IsVisibleInFooter)
                    {
                        var showInFooter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowSkypeInFooter);
                        if (!showInFooter)
                        {
                            this.Functionality.ContactDetailsItems.Remove(data);
                        }
                    }

                    
                }
            }

            //Fax
            {
                if (!String.IsNullOrEmpty(this.Functionality.Fax))
                {
                    ContactDetailsData data = new ContactDetailsData
                                                  {
                                                      Label = this.Functionality.LblFax,
                                                      CssClass = "field-contact-fax",
                                                      Priority = 20,
                                                      Value = new MySpan() { InnerText = this.Functionality.Fax , CssClass="field-contact-fax-content" } , Identifier = BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS.Fax
                                                  };

                    this.Functionality.ContactDetailsItems.Add(data);
                    if (this.Functionality.IsVisibleInFooter)
                    {
                        var showInFooter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowFaxInFooter);
                        if(!showInFooter)
                        {
                            this.Functionality.ContactDetailsItems.Remove(data);
                        }
                    }
                }
            }

            //Twitter
            {
                if (!String.IsNullOrEmpty(this.Functionality.TwitterURL) && !this.Functionality.ShowSocialNetworksAsIcons)
                {

                    MyAnchor anchorTwitter = new MyAnchor();
                    anchorTwitter.Href = this.Functionality.TwitterURL;
                    anchorTwitter.HrefTarget = Enums.HREF_TARGET.Blank;
                    MyContentText.AttachContentTextWithControl(anchorTwitter,BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_TwitterText).ToFrontendBase());
                    ContactDetailsData data = new ContactDetailsData
                    {
                        Label = this.Functionality.LblTwitter,
                        CssClass = "field-contact-twitter",
                        Priority = 40,
                        Value = anchorTwitter, Identifier = BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS.TwitterURL
                    };

                    this.Functionality.ContactDetailsItems.Add(data);
                    if (this.Functionality.IsVisibleInFooter)
                    {
                        var showInFooter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowTwitterInFooter);
                        if (!showInFooter)
                        {
                            this.Functionality.ContactDetailsItems.Remove(data);
                        }
                    }
                }
            }

            //Facebook
            {
                if (!String.IsNullOrEmpty(this.Functionality.FacebookURL) && !this.Functionality.ShowSocialNetworksAsIcons)
                {

                    MyAnchor anchorFacebook = new MyAnchor();
                    anchorFacebook.Href = this.Functionality.FacebookURL;
                    anchorFacebook.HrefTarget = Enums.HREF_TARGET.Blank;
                    MyContentText.AttachContentTextWithControl(anchorFacebook, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_FacebookText).ToFrontendBase());
                    ContactDetailsData data = new ContactDetailsData
                    {
                        Label = this.Functionality.LblFacebook,
                        CssClass = "field-contact-facebook",
                        Priority = 30,
                        Value = anchorFacebook, Identifier = BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS.FacebookURL
                        
                    };

                    this.Functionality.ContactDetailsItems.Add(data);
                    if (this.Functionality.IsVisibleInFooter)
                    {
                        var showInFooter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowFacebookInFooter);
                        if (!showInFooter && !this.Functionality.ShowSocialNetworksAsIcons)
                        {
                            this.Functionality.ContactDetailsItems.Remove(data);
                        }
                    }
                }
            }

            //Linkedin
            {
                if (!String.IsNullOrEmpty(this.Functionality.LinkedinURL) && !this.Functionality.ShowSocialNetworksAsIcons)
                {

                    MyAnchor anchorLinkedin = new MyAnchor();
                    anchorLinkedin.Href = this.Functionality.LinkedinURL;
                    anchorLinkedin.HrefTarget = Enums.HREF_TARGET.Blank;
                    MyContentText.AttachContentTextWithControl(anchorLinkedin, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_Linkedin).ToFrontendBase());
                    ContactDetailsData data = new ContactDetailsData
                    {
                        Label = this.Functionality.LblLinkedin,
                        CssClass = "field-contact-linkedin",
                        Priority = 30,
                        Value = anchorLinkedin, Identifier = BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS.LinkedinURL

                    };

                    this.Functionality.ContactDetailsItems.Add(data);
                    if (this.Functionality.IsVisibleInFooter)
                    {
                        var showInFooter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowLinkedinInFooter);
                        if (!showInFooter)
                        {
                            this.Functionality.ContactDetailsItems.Remove(data);
                        }
                    }
                }
            }

            //Email
            {
                if (!String.IsNullOrEmpty(this.Functionality.Email))
                {
                    ContactDetailsData data = new ContactDetailsData
                                                  {
                                                      Label = this.Functionality.LblEmail,
                                                      CssClass = "field-contact-email",
                                                      Priority = 5,
                                                      Value =
                                                          new MyEmail
                                                              {
                                                                  Email = this.Functionality.Email,
                                                                  Text = this.Functionality.Email
                                                              }, Identifier = BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS.Email
                                                  };

                    if (this.Functionality.IsVisibleInFooter)
                    {
                        var showInFooter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowEmailInFooter);
                        if(showInFooter)
                        {
                            string contactUsNowTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Footer_FooterContact_ContactUsNow).ToFrontendBase().GetContent();
                            string contactUsUrl = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.ContactUs).ToFrontendBase().GetUrl();
                            data.Value = new MyAnchor() { InnerHtml = contactUsNowTxt, Href = contactUsUrl };   
                        }
                    }
                    this.Functionality.ContactDetailsItems.Add(data);
                }
            }

            //Website
            {
                if (!String.IsNullOrEmpty(this.Functionality.Website))
                {
                    ContactDetailsData data = new ContactDetailsData
                                                  {
                                                      Label = this.Functionality.LblWebsite,
                                                      CssClass = "field-contact-website",
                                                      Priority = 9999,
                                                      Value =
                                                          new MyAnchor
                                                              {
                                                                  Href = this.Functionality.Website,
                                                                  InnerText = this.Functionality.Website,
                                                                  CssClass = "field-contact-website-content"
                                                              }, Identifier = BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS.Website
                                                  };
                    this.Functionality.ContactDetailsItems.Add(data);
                    if (this.Functionality.IsVisibleInFooter)
                    {
                        var showInFooter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowWebsiteInFooter);
                        if (!showInFooter)
                        {
                            this.Functionality.ContactDetailsItems.Remove(data);
                        }
                    }
                }
            }


            //Address
            if (!String.IsNullOrEmpty(this.Functionality.Address))
            {
                bool show = true;
                if (this.Functionality.IsVisibleInFooter)
                {
                    var showInFooter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowAddressInFooter);
                    if (!showInFooter)
                    {
                        show = false;
                    }
                }

                if (this.Functionality.IsAddressInTable)
                {
                    ContactDetailsData data = new ContactDetailsData
                                                  {
                                                      Label = this.Functionality.LblAddress,
                                                      CssClass = "field-contact-address",
                                                      Priority = 50,
                                                      Value = new MySpan() { InnerHtml = this.Functionality.Address, CssClass="field-contact-address-content" },
                                                       Identifier = BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS.Address
                                                  };
                    this.Functionality.ContactDetailsItems.Add(data);
                    if (this.Functionality.IsVisibleInFooter)
                    {
                        if (!show)
                        {
                            this.Functionality.ContactDetailsItems.Remove(data);
                        }
                    }
                }
                else
                {
                    if(show)
                    {
                        spanAddress.Visible = true;
                        spanAddress.InnerHtml = this.Functionality.Address;
                    }
                    else
                    {
                        spanAddress.Parent.Controls.Remove(spanAddress);
                    }
                }
            }

            //Address
            var anchorMap = initMap();
            if (anchorMap != null)
            {
                ContactDetailsData data = new ContactDetailsData
                                              {
                                                  Label = this.Functionality.LblMap,
                                                  CssClass = "field-contact-map",
                                                  Priority = 9999,
                                                  Value = anchorMap, Identifier = BusinessLogic_v3.Enums.CONTACT_DETAILS_FIELDS.Map
                                              };
                this.Functionality.ContactDetailsItems.Add(data);
                if (this.Functionality.IsVisibleInFooter)
                {
                    var showInFooter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowMapInFooter);
                    if (!showInFooter)
                    {
                        this.Functionality.ContactDetailsItems.Remove(data);
                    }
                }
            }

            //Director Name
            if (!string.IsNullOrEmpty(this.Functionality.DirectorName) && !this.Functionality.IsVisibleInFooter)
            {
                spanDirector.Visible = true;
                var ct = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_Details_ManagingDirectorText).ToFrontendBase();
                var dict = ct.CreateTokenReplacer();
                dict.Add(BusinessLogic_v3.Constants.Tokens.MANAGING_DIRECTOR, this.Functionality.DirectorName);
                MyContentText.AttachContentTextWithControl(spanDirector, ct, dict);
            }
        }


        private void loadSettings()
        {
            this.Functionality.Mobile = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Mobile);
            this.Functionality.Telephone = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Telephone);
            this.Functionality.Fax = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Fax);
            this.Functionality.Email = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Email);
            this.Functionality.Website = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Website);
            this.Functionality.Address = BusinessLogic_v3.Modules.Factories.SettingFactory.GetAddressAsOneLine(",<br/>");
            this.Functionality.DirectorName = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_DirectorName);
            this.Functionality.IsAddressInTable = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowAddressInTable);
     
            this.Functionality.TwitterURL = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Twitter);
            this.Functionality.FacebookURL = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook);
            this.Functionality.LinkedinURL = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Linkedin);
            this.Functionality.SkypeURL = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Skype);
            this.Functionality.SkypeName = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_SkypeName);
            
            this.Functionality.ShowSocialNetworksAsIcons = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_ShowSocialNetworksAsIcons);
            if(!this.Functionality.ShowSocialNetworksAsIcons)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(socialNetworksIcons);
            }
        }

        private MyAnchor initMap()
        {
            string latitude = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Latitude);
            string longitude = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Longtitude);
            string zoomLevel = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_ZoomLevel);

            if (!(string.IsNullOrWhiteSpace(latitude) || string.IsNullOrWhiteSpace(longitude) || string.IsNullOrWhiteSpace(zoomLevel)))
            {
                if (Functionality.UsePrettyPhoto)
                {
                    MyAnchor anchorMap = new MyAnchor();
                    StringBuilder sb = new StringBuilder();
                    var hrefSring = Functionality.MapAnchorURL;
                    sb.Append(hrefSring);
                    sb.Append(PRETTYPHOTO_POSTFIX_HREF);

                    if (!String.IsNullOrEmpty(Functionality.PrettyPhotoMapHeight))
                    {
                        sb.Append(PRETTYPHOTO_HEIGHT);
                        sb.Append(Functionality.PrettyPhotoMapHeight);
                    }
                    if (!String.IsNullOrEmpty(Functionality.PrettyPhotoMapWidth))
                    {
                        sb.Append(PRETTYPHOTO_WIDTH);
                        sb.Append(Functionality.PrettyPhotoMapWidth);
                    }
                    anchorMap.Href = sb.ToString();
                    anchorMap.InnerText = Functionality.Map;
                    return anchorMap;
                }
            }
            return null;
        }

        private void updateHeading()
        {
            MyContentText.AttachContentTextWithControl(h2ContactDetailsTitle, this.Functionality.ContactDetailsTitle);

            if (this.Functionality.DoNotShowHeading || this.Functionality.DoNotShowTitleAsHeading)
            {
                h2ContactDetailsTitle.Parent.Controls.Remove(h2ContactDetailsTitle);
            }
        }

        public static ContactUsDetails LoadControl(Page pg, string id)
        {
            var ctrl = pg.LoadControl("_common/vs/usercontrols/v1/Contact/ContactUsDetails.ascx") as ContactUsDetails;
            if (!string.IsNullOrEmpty(id))
            {
                ctrl.ID = id;
            }
            return ctrl;
        }
    }
}