﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.CultureDetailsModule;
using BusinessLogic_v3.Frontend.ShoppingCartItemModule;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.MemberModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart
{
    public partial class ShoppingCartItem : BaseUserControl
    {
        private CultureDetailsBaseFrontend _culture;
        public class FUNCTIONALITY
        {
            private ShoppingCartItem _control;
            public FUNCTIONALITY(ShoppingCartItem control)
            {
                _control = control;
            }
            public bool IsEven { get; set; }
            public ShoppingCart.SHOPPING_CART_TYPE CartType { get; set; }
            public ShoppingCartItemBaseFrontend ShoppingCartItem { get; set; }

            public int GetQuantity()
            {
                if (CartType == ShoppingCart.SHOPPING_CART_TYPE.Cart)
                {
                    return _control.txtQuantity.GetFormValueAsInt();
                }
                return ShoppingCartItem.Data.GetQuantity();
            }
        }


        public virtual bool OnUpdateQuantityClicked(ShoppingCartItemBaseFrontend item)
        {
            return true;
        }

        public FUNCTIONALITY Functionality { get; private set; }

        public ShoppingCartItem()
        {
            this.Functionality = createFunctionality();
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            
            updateContent();
        }

        

        private void updateContent()
        {
            _culture = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture().ToFrontendBase();
            setRowCssClass();
            setCells();
        }
        
        private void updateProductCell()
        {
            //Product Cell
            var productVariation = this.Functionality.ShoppingCartItem.Data.GetLinkedProductVariation().ToFrontendBase();
            var product = productVariation.Data.Product.ToFrontendBase();
            if (productVariation != null)
            {
                imgProduct.HRef = aProductTitle.Href = product.Data.GetFrontendUrl();
                var mainImage = productVariation.Data.GetMainImage();
                
                string imageUrl;
                if (mainImage != null)
                {
                    var image = mainImage.Image;
                    imageUrl = image.GetSpecificSizeUrl(ProductVariationMediaItemBase.ImageSizingEnum.ShoppingCartThumbnail);
                }
                else
                {
                    imageUrl = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_DefaultShoppingCartImageUrl);
                }
                imgProduct.ImageUrl = imageUrl;

                aProductTitle.InnerHtml = product.GetTitle();
                spanProductExtraInfo.InnerHtml = this.Functionality.ShoppingCartItem.Data.GetSubtitle();
            }
        }
       

        private void updateDeleteCell()
        {
            //Delete Cell
            if (this.Functionality.CartType == ShoppingCart.SHOPPING_CART_TYPE.Cart)
            {
                var confirmMessage = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_Delete_ConfirmMessage);
                btnDelete.WebControlFunctionality.ConfirmMessage = confirmMessage.GetContent();
                btnDelete.Click += new EventHandler(btnDelete_Click);
                tdDelete.Controls.Add(btnDelete);
            }
            else
            {
                tdDelete.Visible = false;
            }
        }

        private void setCells()
        {
            updateDeleteCell();
            updateProductCell();
            updateQuantityCell();
            updateUnitPriceCell();
            updateTotalPriceCell();
        }

        private void updateUnitPriceCell()
        {
            if (hasLowerPrice(this.Functionality.ShoppingCartItem))
            {
                spanUnitOldPrice.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture
                    (this.Functionality.ShoppingCartItem.Data.PriceInfo.GetUnitPrice(reduceDiscount:false), General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
                spanUnitPrice.Attributes["class"] += " shopping-cart-item-unit-price-discounted-price";
                spanPrice.Attributes["class"] += " shopping-cart-item-total-price-discounted-price";
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(spanUnitOldPrice);
                //spanOldPrice.Visible = false;

            }
            var unitPrice = this.Functionality.ShoppingCartItem.Data.PriceInfo.GetUnitPrice(reduceDiscount: true);

            spanUnitPrice.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture
                (unitPrice, General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
        }

        private void updateQuantityCell()
        {
            tdQuantity.Attributes["class"] += " shopping-cart-quantity-cell";
            if (this.Functionality.CartType == ShoppingCart.SHOPPING_CART_TYPE.Cart)
            {
                tdQuantity.Attributes["class"] += " shopping-cart-quantity-editable";
                divQuantityContainer.Attributes["class"] += " shopping-cart-item-quantity-container-editable";
                txtQuantity.InitialValue = this.Functionality.ShoppingCartItem.Data.GetQuantity();
            }
            else
            {
                tdQuantity.Attributes["class"] += " shopping-cart-quantity-readonly";
                divQuantityContainer.Attributes["class"] += " shopping-cart-item-quantity-container-readonly";
                txtQuantity.Visible = false;
                divQuantityContainer.InnerHtml = this.Functionality.ShoppingCartItem.Data.GetQuantity().ToString();
            }
        }

        private bool hasLowerPrice(ShoppingCartItemBaseFrontend shoppingCartItem)
        {
            return shoppingCartItem.Data.GetLinkedProduct().HasDiscountPrice();
        }

        private void updateTotalPriceCell()
        {
            var culture = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture();
            if (hasLowerPrice(this.Functionality.ShoppingCartItem))
            {
                var oldPrice = this.Functionality.ShoppingCartItem.Data.PriceInfo.GetTotalPrice(reduceDiscount: false);
                
                spanOldPrice.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCulture(
                    oldPrice, General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency, culture);
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(spanOldPrice);
                //spanOldPrice.Visible = false;
                
            }
            spanPrice.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCulture(
                    this.Functionality.ShoppingCartItem.GetPrice(), General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency, culture);
        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.Functionality.ShoppingCartItem != null)
            {
                this.Functionality.ShoppingCartItem.DeleteItem();
                this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_DeleteItem_Success).ToFrontendBase());
            }
        }

        private void setRowCssClass()
        {
            if(this.Functionality.IsEven)
            {
                trShoppingCartItem.Attributes["class"] += " even";
            }
        }

        public static ShoppingCartItem LoadControl(Page pg)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/ShoppingCartItem.ascx") as ShoppingCartItem;
            return ctrl;
        }
    }
}