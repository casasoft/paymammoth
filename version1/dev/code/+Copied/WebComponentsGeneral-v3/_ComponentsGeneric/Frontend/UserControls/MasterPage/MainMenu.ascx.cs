﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage
{
    public partial class MainMenu : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private MainMenu _control;
            public FUNCTIONALITY(MainMenu control)
            {
                _control = control;
            }
            public int? MaxDepth { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public MainMenu()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initMenu();
        }

        private void initMenu()
        {
            var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.MainMenu).ToFrontendBase();
            var articleChildren = article.Data.GetChildArticles().ToFrontendBaseList();
            topMenuNavigation.Functionality.Items = articleChildren;
            if (this.Functionality.MaxDepth.HasValue)
            {
                topMenuNavigation.Functionality.MaxDepth = this.Functionality.MaxDepth;
            }
            topMenuNavigation.Functionality.DropDownMenuParametersJS = new CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.DropDownMenu.DropDownMenuParametersJS();
        }
    }
}