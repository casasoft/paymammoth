﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing
{
    public abstract class ListingItemControlBase : BaseUserControl
    {

        public abstract void PopulateControl(object item);

        public static ListingItemControlBase LoadControl(string controlPath, Page pg)
        {
            return (ListingItemControlBase) pg.LoadControl(controlPath);
        }
    }
}