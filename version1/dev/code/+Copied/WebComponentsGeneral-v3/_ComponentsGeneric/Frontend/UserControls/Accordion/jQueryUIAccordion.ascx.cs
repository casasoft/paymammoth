﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Accordion
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using Newtonsoft.Json;
    using CS.WebComponentsGeneralV3.Code.Util;


    public partial class jQueryUIAccordion : BaseUserControl
    {
        public class JQueryUITabOptions : JSONObject
        {
            public bool? disabled;
            public object active;
            public string animated;
            public bool? autoHeight;
            public bool? clearStyle;
            public bool? collapsible;
            [JsonProperty(PropertyName="event")]
            public string Event;
            public bool? fillSpace;
            public string header;
            public object icons;
            public bool? navigation;
            public object navigationFilter;



        }
        public class JQueryUIAccordionData
        {
            public ContentTextBaseFrontend Title { get; set; }
            public Control Content { get; set; }
            public string CssClass { get; set; }
            public string AccordionIconURL { get; set; }
        }


        #region jQueryUITabs Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected jQueryUIAccordion _item = null;

            public List<JQueryUIAccordionData> Data { get; private set; }
            public JQueryUITabOptions Options { get; private set; }
            internal FUNCTIONALITY(jQueryUIAccordion item)
            {
                this.Data = new List<JQueryUIAccordionData>();
                this.Options = new JQueryUITabOptions();
                this._item = item;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			


        private void init()
        {
            placeholderAccordionContent.Controls.Clear();
            foreach (var item in this.Functionality.Data)
            {
                /* <h3>
                        <a href="#">First header</a></h3>
                    <div>
                        First content</div>
                    <h3>
                        <a href="#">Second header</a></h3>
                    <div>
                        Second content</div>*/
                MyHeading h3 = new MyHeading(3, null);

                MyDiv divContent = new MyDiv();
                divContent.Controls.Add(item.Content);
                h3.CssManager.AddClass("jquery-ui-accordion-heading");
                divContent.CssManager.AddClass("jquery-ui-accordion-content-container");
                if (!string.IsNullOrEmpty(item.CssClass))
                {
                    divContent.CssManager.AddClass(item.CssClass);
                    h3.CssManager.AddClass(item.CssClass + "-heading");
                }

                placeholderAccordionContent.Controls.Add(h3);
                placeholderAccordionContent.Controls.Add(divContent);


                MySpan spanHeadingText = new MySpan();
                spanHeadingText.CssManager.AddClass("jquery-ui-accordion-heading-text");
                MyContentText.AttachContentTextWithControl(spanHeadingText, item.Title);

                if (!string.IsNullOrEmpty(item.AccordionIconURL))
                {
                    MyImage imgIcon = new MyImage();
                    imgIcon.CssManager.AddClass("jquery-ui-accordion-heading-icon");
                    imgIcon.ImageUrl = item.AccordionIconURL;
                    imgIcon.AlternateText = item.Title.GetContent();
                    h3.Controls.Add(imgIcon);
                }
                h3.Controls.Add(spanHeadingText);

                /*MyListItem li = new MyListItem();
                li.CssManager.AddClass("jqueryui-tab");

                if (!string.IsNullOrEmpty(item.CssClass))
                {
                    li.CssManager.AddClass(item.CssClass + "-tab");
                }

                MyAnchor aTab = new MyAnchor();
                aTab.CssManager.AddClass("jqueryui-tab-link");
                MyContentText.AttachContentTextWithControl(aTab, item.Title);
                if (!string.IsNullOrEmpty(item.TabIconURL))
                {
                    MyImage imgIcon = new MyImage();
                    imgIcon.CssManager.AddClass("jqueryui-tab-icon");
                    imgIcon.ImageUrl = item.TabIconURL;
                    imgIcon.AlternateText = item.Title.GetContent();
                    li.Controls.Add(imgIcon);
                }
                li.Controls.Add(aTab);
                ulTabs.Controls.Add(li);

                MyDiv divTabContent = new MyDiv();
                divTabContent.CssManager.AddClass("jqueryui-tab-link");
                placeholderTabsContent.Controls.Add(divTabContent);
                    li.CssManager.AddClass(item.CssClass + "-tab");
                if (item.Content != null)
                {
                    divTabContent.Controls.Add(item.Content);
                }
                if (!string.IsNullOrEmpty(item.CssClass))
                {
                    divTabContent.CssManager.AddClass(item.CssClass);
                }

                //<li><a href="#tabs-1">Nunc tincidunt</a></li>


                aTab.Href = "#" + divTabContent.ClientID;*/
            }
        }
        private void initJS()
        {
            string js = "window.setTimeout(function() { jQuery('#"+divAccordion.ClientID+"').accordion("+this.Functionality.Options.GetJSON()+"); }, 50);";
            js = JSUtil.GetDeferredJSScriptThroughJQuery(js, true);
            jsPlaceholder.Controls.Add(new Literal() { Text = js });
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            initJS();
            base.OnLoad(e);
        }
        public override string ClientID
        {
            get
            {
                return this.divAccordion.ClientID;
            }
        }
    }
}