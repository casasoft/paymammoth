﻿using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Extensions;
namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage
{
    using System;
    using System.Web.UI;
    using BusinessLogic_v3.Modules.ArticleModule;
    using BusinessLogic_v3.Modules.CmsUserModule;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using System.Collections.Generic;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using BusinessLogic_v3.Classes.DbObjects;
    using CS.WebComponentsGeneralV3.Code.Cms.Classes;
    using CS.WebComponentsGeneralV3.Code.Classes.Pages;
    using BusinessLogic_v3.Classes.Text;
    using BusinessLogic_v3;
    using BusinessLogic_v3.Modules;

    public partial class ContentPage : BaseUserControl
    {

        

        /// <summary>
        /// The Template property for the Content area.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(INamingContainer))]
        [TemplateInstance(TemplateInstance.Single)] //This is so that inner child controls can be accessed.  If you can create multiple controls, then you cannot access this
        public ITemplate BottomContent { get; set; }
        /// <summary>
        /// The Template property for the Content area.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(INamingContainer))]
        [TemplateInstance(TemplateInstance.Single)] //This is so that inner child controls can be accessed.  If you can create multiple controls, then you cannot access this
        public ITemplate TopContent { get; set; }
        protected override void OnInit(EventArgs e)
        {
            if (BottomContent != null)
            {
                divExtraBottomContent.Visible = true;
                BottomContent.InstantiateIn(this.divExtraBottomContent); //Replace this with container where you want to place such controls
            } 
            if (TopContent != null)
            {
                divExtraTopContent.Visible = true;
                TopContent.InstantiateIn(this.divExtraTopContent); //Replace this with container where you want to place such controls
            }
            base.OnInit(e);
        }
        
			

        public bool IsTitleVisible
        {
            get { return this.Functionality.IsTitleVisible; }
            set { this.Functionality.IsTitleVisible = value; }
        }

        public class FUNCTIONALITY
        {
            private ContentPage _control;
            public FUNCTIONALITY(ContentPage control)
            {
                _control = control;
                this.ContentTagReplacementValues = new TokenReplacerNew();
                this.HeadingReplacementValues = new TokenReplacerNew();
                //defaults
                DoNotShowNavigationBreadcrumbs = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_DoNotShowNavigationBreadcrumbs);
            }
            private ArticleBaseFrontend _contentPage;
            public ArticleBaseFrontend ContentPage
            {
                get { return _contentPage; }
                set
                {
                    _contentPage = value;
                    if (value != null)
                    {
                        DoNotShowNavigationBreadcrumbs = value.Data.DoNotShowNavigationBreadcrumbs.GetValueOrDefault();
                        DoNotShowFooter = value.Data.DoNotShowFooter.GetValueOrDefault();
                        this._control.CacheParams.SetCacheDependencyFromDBObjects(value.Data, true);
                        if (this._control.Page.Functionality.ContentPage == null)
                        {
                            this._control.Page.Functionality.ContentPage = value;
                        }
                    }

                }
            }

            public TokenReplacerNew ContentTagReplacementValues { get; set; }
            public TokenReplacerNew HeadingReplacementValues { get; set; }

            public bool IsTitleVisible { get; set; }
            public bool IsWebsiteNameIncluded { get; set; }
            public bool DoNotUpdateForSEO { get; set; }
            public bool DoNotShowNavigationBreadcrumbs { get; set; }
            public bool DoNotShowFooter { get; set; }
        }

        public FUNCTIONALITY Functionality { get; private set; }
        public ContentPage()
        {
            this.Functionality = createFunctionality();
            this.IsTitleVisible = true;

            this.CacheParams.IsMultilingual = true;
            this.CacheParams.CacheControl = false;
            
            
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        

        private void init()
        {
            initLoadContentPage();
            updateForSEO();
            initNavigationBreadcrumbs();
        }

        private void initNavigationBreadcrumbs()
        {
            bool showNavBreadcrumbs = !this.Functionality.DoNotShowNavigationBreadcrumbs;
            if (showNavBreadcrumbs)
            {
                if (!BusinessLogic_v3.Modules.ModuleSettings.Articles.DoNotShowNavigationBreadcrumbs && !this.Functionality.ContentPage.Data.DoNotShowNavigationBreadcrumbs.GetValueOrDefault())
                {
                    navigationBreadcrumbs.Functionality.AddFromHierarchy(this.Functionality.ContentPage.Data, true);
                    if (BusinessLogic_v3.Modules.ModuleSettings.Articles.ShowRootNodeInNavigationBreadcrumbs)
                    {
                        var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Home);
                        if (article != null) { navigationBreadcrumbs.Functionality.AddItem(article.PageTitle, article.GetUrl(), true); }
                    }
                }
                else
                {
                    showNavBreadcrumbs = false;
                }
            }
            if (!showNavBreadcrumbs)
            {
                divNavigationBreadcrumbs.Parent.Controls.Remove(divNavigationBreadcrumbs);

            }
        }

        private void initInlineEditing()
        {
            CMSInlineEditingItem.Create(this.Functionality.ContentPage.Data, x => x.PageTitle, h1PageTitle, this.Functionality.ContentPage.Data.Title, this.Functionality.HeadingReplacementValues, Enums.INLINE_EDITING_TEXT_TYPE.Html);
            CMSInlineEditingItem.Create(this.Functionality.ContentPage.Data, x => x._HtmlText_ForEditing, divHtmlContent, this.Functionality.ContentPage.Data._HtmlText_ForEditing, this.Functionality.ContentTagReplacementValues, Enums.INLINE_EDITING_TEXT_TYPE.HtmlWithAtLeastParagraphTag);
                   
        }

        private void initLoadContentPage()
        {
            if (this.Visible)
            {
                if (Functionality.ContentPage != null)
                {
                    if (this.Page.Functionality.ContentPage == null)
                    {
                        this.Page.Functionality.ContentPage = this.Functionality.ContentPage;
                    }
                    
                    updateTexts();
                    h1PageTitle.Visible = IsTitleVisible;
                    if (!this.Functionality.DoNotShowFooter && this.Functionality.ContentPage.Data.ArticleType != Enums.ARTICLE_TYPE.Events)
                    {
                        contentPageFooter.Functionality.ContentPage = Functionality.ContentPage;
                    }
                    else
                    {
                        contentPageFooter.RemoveControlFromParentContainer();
                    }
                    
                    divHtmlContent.InnerHtml = BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(this.Functionality.ContentPage.Data, this.Functionality.ContentTagReplacementValues);

                    var commonMasterPage = this.GetMasterPageOfType<BaseCommonMasterPage>();
                    if (commonMasterPage != null && commonMasterPage.Functionality.DbObjectToLinkWithEditInCmsButton == null)
                    {
                        commonMasterPage.Functionality.DbObjectToLinkWithEditInCmsButton = this.Functionality.ContentPage.Data;
                    }
                    initInlineEditing();
                     //new CMSInlineEditingItem(h1PageTitle, this.Functionality.ContentPage.Data, BusinessLogic_v3.Enums.CMS_INLINE_EDITING_TYPE.ArticleTitle, this.Functionality.ContentPage.Data.Title, this.Functionality.HeadingReplacementValues);
                   // new CMSInlineEditingItem(divHtmlContent, this.Functionality.ContentPage.Data, BusinessLogic_v3.Enums.CMS_INLINE_EDITING_TYPE.ArticleHtml, this.Functionality.ContentPage.Data.HtmlText, this.Functionality.ContentTagReplacementValues);

                    var showAddThisInHeading = ModuleSettings.Articles.ShowAddThisInHeading;
                    if(showAddThisInHeading)
                    {
                        headingAddThis.Functionality.URL = this.Functionality.ContentPage.GetUrl();
                        headingAddThis.Functionality.Style = General_v3.Enums.ADDTHIS_STYLE.DefaultStyleSmall;
                        divHeadingAddThisWrapper.Visible = true;
                    }
                    else
                    {
                        CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divHeadingAddThisWrapper);
                    }
                }
                else
                {
                    throw new Exception("Content Page not found");
                }
            }
        }

        private void updateTexts()
        {
            h1PageTitle.InnerHtml = Functionality.ContentPage.Data.PageTitle;
            if(!string.IsNullOrEmpty(Functionality.ContentPage.Data.SubTitle))
            {
                string s = this.Functionality.ContentTagReplacementValues.ReplaceString(Functionality.ContentPage.Data.SubTitle);
                spanArticleSubtitle.InnerHtml =s;
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(spanArticleSubtitle);
            }

            var showMediaItemsInArticle = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_ShowImageGalleryForContentPages);
            if(!showMediaItemsInArticle)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(contentPageMediaItems);
            }
            else
            {
                contentPageMediaItems.Functionality.Article = this.Functionality.ContentPage;
            }

            var showMainImageInContentPage = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_ShowMainImageInContentPage);
            if (!showMainImageInContentPage)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divContentPageImage);
            }
            else
            {
                var imageUrl = this.Functionality.ContentPage.GetMainImageUrl();
                if (!string.IsNullOrEmpty(imageUrl))
                {
                    divContentPageContent.Attributes["class"] += " content-page-main-image-available";
                    imgItem.ImageUrl = imageUrl;
                }
                else
                {
                    CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divContentPageImage);
                }
            }
        }

        private void updateOpenGraphImage()
        {
            var imageURL = CS.General_v3.Util.PageUtil.GetAbsoluteImageURLFromHTML(Functionality.ContentPage.Data.GetContent());
            if (imageURL != null)
            {
                this.Page.Functionality.MetaTagsController.UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH.Image, imageURL);
            }
        }
        private void updateForSEO()
        {
            if (!this.Functionality.DoNotUpdateForSEO)
            {
                string websiteName = CS.General_v3.Settings.GetSettingFromDatabase<string>(
                    CS.General_v3.Enums.SETTINGS_ENUM.Others_WebsiteName);
                if (Functionality.ContentPage != null)
                {


                    this.Page.Functionality.UpdateTitleFromContentPage(Functionality.ContentPage);

                    updateOpenGraphImage();
                    this.Page.Functionality.MetaTagsController.UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH.Type,
                                                               CS.General_v3.Util.EnumUtils.StringValueOf(
                                                                   CS.General_v3.Enums.OPEN_GRAPH_TYPE.Company));
                    this.Page.Functionality.MetaTagsController.UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH.SiteName, websiteName);
                }
            }
        }
    }
}