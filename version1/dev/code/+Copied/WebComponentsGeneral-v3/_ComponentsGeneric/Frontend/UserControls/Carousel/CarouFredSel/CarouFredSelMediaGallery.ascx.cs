﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Carousel.CarouFredSel.v1;
using System.Web.UI.HtmlControls;
using CS.WebComponentsGeneralV3.Code.Util;
using CS.General_v3.Classes.Interfaces.ImageItem;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Carousel.CarouFredSel
{
    using CS.General_v3.Util;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using CS.General_v3.Classes.Interfaces.Gallery;

    public partial class CarouFredSelMediaGallery : BaseUserControl
    {


        #region CarouFredSelImageGallery Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {

            public List<IMediaItemData> MediaItems { get; set; }

            public CarouFredSelControl CarouselFredSel { get { return this._item.carousel; } }
            public CarouFredSelV1Parameters CarouselFredSelParameters { get { return this._item.carousel.Functionality.Parameters; } }

            public string PrettyPhotoImageGalleryID { get; set; }
            public bool ShowPagination { get; set; }
            public bool ShowPaginationThumbnails { get; set; }

            protected CarouFredSelMediaGallery _item = null;
            internal FUNCTIONALITY(CarouFredSelMediaGallery item)
            {
                this._item = item;
                this.PrettyPhotoImageGalleryID = "carouFredSelMediaGallery";
                this.MediaItems = new List<IMediaItemData>();
                this.ShowPagination = this.ShowPaginationThumbnails = true;
            }
            public void AddImages(IEnumerable<IMediaItemData> mediaItems)
            {
                this.MediaItems.AddRange(mediaItems);
            }
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        private Control generateItem(IMediaItemData item)
        {
            MyDiv divItem = new MyDiv();
            divItem.CssManager.AddClass("caroufred-sel-media-gallery-item");
            MyMediaItem mediaItem = new MyMediaItem();
            mediaItem.Functionality.ItemURL = item.NormalImageUrl;

            mediaItem.Functionality.Caption = item.Caption;
            mediaItem.Functionality.Href = item.LargeImageUrl;
            mediaItem.Functionality.HrefTarget = General_v3.Enums.HREF_TARGET.Blank;
            if (!string.IsNullOrEmpty(this.Functionality.PrettyPhotoImageGalleryID))
            {
                mediaItem.Functionality.Rel = "prettyPhoto[" + this.Functionality.PrettyPhotoImageGalleryID + "]";
            }

            divItem.Controls.Add(mediaItem);

            if (!string.IsNullOrEmpty(item.Caption))
            {
                MyDiv divCaption = new MyDiv();
                divCaption.CssManager.AddClass("caroufred-sel-media-gallery-item-caption");
                divCaption.InnerHtml = item.Caption;
                divItem.Controls.Add(divCaption);
            }
            return divItem;
        }

        private void initImages()
        {
            if (this.Functionality.MediaItems != null)
            {
                foreach (var item in this.Functionality.MediaItems)
                {
                    {
                        var ctrlItem = generateItem(item);
                        carousel.Functionality.Controls.Add(ctrlItem);
                    }
                }
                carousel.Functionality.ItemsVisible = 1;
                if (this.Functionality.ShowPaginationThumbnails)
                {
                    initThumbnails();
                }
                else if (this.Functionality.ShowPagination)
                {
                    carousel.Functionality.Parameters.pagination = new CarouFredSelV1PaginationParameters();

                }
            }
        }
        private void initThumbnails()
        {
            List<string> thumbnailImages = new List<string>();
            List<string> thumbnailAlts = new List<string>();
            foreach (var image in this.Functionality.MediaItems)
            {
                thumbnailImages.Add(image.ThumbnailImageUrl);
                thumbnailAlts.Add(image.Caption);
            } 
            
            string varName = "thumbnailImages";
            //Register thumbnails
            string jsArray = varName + " = " + JSONUtil.Serialize(thumbnailImages) + ";";
            JSUtil.AddJSScriptToPage(jsArray);


            carousel.Functionality.Parameters.pagination = new CarouFredSelV1PaginationParameters()
            {
                anchorBuilder = @"function(nr, item) {
                                        var thumbnailImages = " + JSONUtil.Serialize(thumbnailImages) + @";
                                        var altTexts = " + JSONUtil.Serialize(thumbnailAlts) + @";
                                        return '<div class=""carousel-thumbnail-image""><a href=""#'+nr+'""><img alt=""'+altTexts[nr-1]+'"" src=""'+thumbnailImages[nr-1]+'"" /></a></div>'; 
                                    }"
            };
        }
        protected override void OnInit(EventArgs e)
        {
            CS.General_v3.Util.PageUtil.GetCurrentPage().Load += new EventHandler(CarouFredSelImageGallery_Load);
            base.OnInit(e);
        }

        void CarouFredSelImageGallery_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Functionality.PrettyPhotoImageGalleryID))
            {
                CS.WebComponentsGeneralV3.Code.Util.PrettyPhotoUtil.InitJS();
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            initImages();
            
            base.OnLoad(e);
        }
        public override string ClientID
        {
            get
            {
                return this.carousel.ClientID;
            }
        }
			
		
    }
}