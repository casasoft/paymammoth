﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Modules.MemberModule;
using CS.General_v3.Classes.Login;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using BusinessLogic_v3.Modules;
using CS.WebComponentsGeneralV3.Code.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Password
{
    public partial class ForgotPasswordFields : BaseUserControl
    {

        private const string FORGOT_PASSWORD_VALIDATION_GROUP = "forgotPasswordFields";

        private MyTxtBoxEmail _txtEmail;
        private MyButton _btnSubmit;
        public class FUNCTIONALITY
        {
            private ForgotPasswordFields _control;

            public ContentTextBaseFrontend LblEmail { get; set; }
            public ContentTextBaseFrontend BtnSubmitText { get; set; }

            public string OnSuccessUrl { get; set; }
            public string OnErrorUrl { get; set; }
            public ContentTextBaseFrontend InvalidEmail { get; set; }

            public FUNCTIONALITY(ForgotPasswordFields control)
            {
                _control = control;

                //Labels
                this.BtnSubmitText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Button_Submit).ToFrontendBase();
                this.LblEmail = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Email).ToFrontendBase();
                this.InvalidEmail = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ForgotPassword_InvalidEmail).ToFrontendBase();
                //On Success Url 
                var articleSuccess = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_ForgotPassword_Success).ToFrontendBase();
                if(articleSuccess != null)
                {
                    OnSuccessUrl = articleSuccess.Data.GetUrl();
                }

                // Message
                this.InvalidEmail = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ForgotPassword_InvalidEmail).ToFrontendBase();
            }

        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ForgotPasswordFields()
        {
            this.Functionality = createFunctionality();
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        private void initLoginFields()
        {
            formForgotPassword.Functionality.ValidationGroup = FORGOT_PASSWORD_VALIDATION_GROUP;

            _txtEmail = (MyTxtBoxEmail)formForgotPassword.Functionality.AddFieldItem(new MyTxtBoxEmail()
            {
                FieldParameters = new FieldSingleControlParameters()
                    {
                        titleContentText = this.Functionality.LblEmail,
                        required = true,
                        id = "forgotPasswordFields_txtEmail"
                    }, AutoFocus = true
            });
            
            _btnSubmit = formForgotPassword.Functionality.AddButton(this.Functionality.BtnSubmitText);
            
            _btnSubmit.Click += new EventHandler(_btnSubmit_Click);
        }

        void _btnSubmit_Click(object sender, EventArgs e)
        {
            string email = _txtEmail.GetFormValueAsString();

            if(!String.IsNullOrEmpty(email))
            {
                var members = Factories.MemberFactory.GetMembersByUsernameOrEmail(email);
                if (members!= null && members.Count() > 0)
                {
                    bool result = Factories.MemberFactory.SubmitForgotPasswordEmail(email);
                    PageUtil.RedirectPage(result
                                              ? Functionality.OnSuccessUrl
                                              : BusinessLogic_v3.Classes.Routing.ErrorsRoute.GetGenericErrorURL());
                }
                this.Page.Functionality.ShowErrorMessageAndRedirectToPage(this.Functionality.InvalidEmail);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            initLoginFields();
            base.OnLoad(e);
        }

        
    }
}