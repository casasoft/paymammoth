﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Cms.Classes;
namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing
{
    using BusinessLogic_v3.Classes.Interfaces;
    using BusinessLogic_v3;
using BusinessLogic_v3.Frontend.ContentTextModule;

    public partial class GenericListingItem : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private GenericListingItem _control;
            public FUNCTIONALITY(GenericListingItem control)
            {
                _control = control;
            }
            public IListingItemData Item { get; set; }
            public ContentTextBaseFrontend ViewMoreText { get; set; }
            public bool DoNotShowSeparator { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public GenericListingItem()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateContent();
            updateTexts();
        }

        private void updateTexts()
        {
            if(this.Functionality.ViewMoreText == null)
            {
                this.Functionality.ViewMoreText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_GenericListingItem_ReadMore).ToFrontendBase();
            }
            MyContentText.AttachContentTextWithControl(anchorReadMore, this.Functionality.ViewMoreText);
        }

        private void initInlineEditing()
        {

        }

        private void updateContent()
        {
            if(this.Functionality.Item != null)
            {
                aItemTitle.InnerHtml = this.Functionality.Item.GetTitle();

                var showCreatedOnDate = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_ShowDateCreatedInGenericListing);
                var createdOnDateDateFormat = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_DateFormatDateCreatedInGenericListing);
                if(showCreatedOnDate)
                {
                    spanDate.InnerHtml = this.Functionality.Item.CreatedOnDate.ToString(createdOnDateDateFormat);
                }
                else
                {
                    CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(spanDate);
                }

                initInlineEditing();
                string desc = this.Functionality.Item.GetShortDescription(true, true);
                if (!string.IsNullOrEmpty(desc))
                {
                    divItemDescription.InnerHtml = desc;
                }
                else
                {
                    MyContentText.AttachContentTextWithControl(divItemDescription, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Products_NoDescriptionAvailable).ToFrontendBase());
                }

                aItemTitle.HRef= anchorReadMore.HRef = this.Functionality.Item.Url;
                imgItem.ImageUrl = this.Functionality.Item.ImageUrl;
                imgItem.HRef = this.Functionality.Item.Url;
                if (this.Functionality.DoNotShowSeparator)
                {
                    divItemSeparator.Parent.Controls.Remove(divItemSeparator);
                }

                var freeText = this.Functionality.Item.GetFreeText();
                if(freeText != null)
                {
                    divFreeText.Visible = true;
                    divFreeText.Controls.Add(freeText);
                }
            }
        }

        public static GenericListingItem LoadControl(Page pg, string id)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/Listing/GenericListingItem.ascx") as GenericListingItem;
            if (!string.IsNullOrEmpty(id))
            {
                ctrl.ID = id;
            }
            return ctrl;
        }
    }
}