﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDescription.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.ProductDescription" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/SocialNetworking/AddThis/AddThisControl.ascx"
    TagName="AddThisControl" TagPrefix="Common" %>
<div class="product-description-content">
    <div class="product-description-container" runat="server" id="divProductDescriptionContainer">
        <div class="product-description-title" runat="server" id="divProductDescriptionTitle"></div>
        <div class="product-description-content-text html-container" id="divProductDesc" runat="server">
        </div>
    </div>
    <div class="product-material-container" runat="server" id="divProductMaterialContainer">
        <div class="product-material-title" runat="server" id="divProductMaterialTitle"></div>
        <div class="product-description-content-text html-container" id="divProductMaterial" runat="server">
        </div>
    </div>
    <div class="product-available-colours-container" runat="server" id="divProductAvailableColoursContainer">
        <div class="product-available-colours-title" runat="server" id="divProductAvailableColoursTitle"></div>
        <div class="product-description-content-text html-container" id="divProductAvailableColours" runat="server">
        </div>
    </div>
    <div class="product-description-content-addthis" runat="server" id="divDescriptionAddThis">
        <Common:AddThisControl runat="server" ID="addThisCtrl" />
    </div>
</div>
