﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Text;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Modules.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Classes.Routing;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage
{
    public partial class ShoppingCartTotalItems : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private ShoppingCartTotalItems _control;
            public FUNCTIONALITY(ShoppingCartTotalItems control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ShoppingCartTotalItems()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            
            var shoppingCart = BusinessLogic_v3.Modules.ModulesUtil.GetCurrentShoppingCart(createIfNotExists:false);
            if (shoppingCart != null && !shoppingCart.IsCartEmpty())
            {
                anchorShoppingCart.HRef = shoppingCart.ToFrontendBase().GetUrl();



                anchorShoppingCart.InnerText = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(
                        shoppingCart.GetTotalPrice(includeTax: true, reduceDiscount: true),
                        General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);

                int totalItemsInCart = shoppingCart.GetTotalItemsInCart();
                ContentTextBaseFrontend cntTextTotalItems;
                if (totalItemsInCart == 1)
                {
                    cntTextTotalItems = Factories.ContentTextFactory.GetContentTextForEnumValue(
                        Enums.CONTENT_TEXT.Member_ShoppingCart_MasterPage_ItemInCart).ToFrontendBase();
                }
                else
                {
                    cntTextTotalItems = Factories.ContentTextFactory.GetContentTextForEnumValue(
                        Enums.CONTENT_TEXT.Member_ShoppingCart_MasterPage_ItemsInCart).ToFrontendBase();
                }
                TokenReplacerNew dict = new TokenReplacerNew(cntTextTotalItems.Data);
                dict.Add(BusinessLogic_v3.Constants.Tokens.TAG_QUANTITY,
                            totalItemsInCart.ToString());

                MyContentText.AttachContentTextWithControl(spanShoppingCartItems, cntTextTotalItems, dict);
            }
            else
            {
                spanSeparator.Visible = false;
                spanShoppingCartItems.Visible = false;
                anchorShoppingCart.HRef = BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBaseFactory.Instance.GetUrlForGeneralRoute(Enums.RouteUrlsEnum.ShoppingCart);
                ContentTextBaseFrontend cntTextNoItemsInCart = Factories.ContentTextFactory.GetContentTextForEnumValue(
                        Enums.CONTENT_TEXT.Member_ShoppingCart_MasterPage_NoItemsInCart).ToFrontendBase();
                MyContentText.AttachContentTextWithControl(anchorShoppingCart, cntTextNoItemsInCart);
            }
            
        }
    }
}