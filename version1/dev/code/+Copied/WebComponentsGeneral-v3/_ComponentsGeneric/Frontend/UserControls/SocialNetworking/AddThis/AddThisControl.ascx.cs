﻿using System.Collections.Generic;
using System.Text;
using BusinessLogic_v3.Frontend.ArticleModule;
using CS.General_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.SocialNetworking.AddThis
{
    using System;
    using System.Web.UI;
    using BusinessLogic_v3.Modules.ArticleModule;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;

    public partial class AddThisControl : BaseUserControl
    {
        private Dictionary<string, string> _dict;
        private string _style;
        public class FUNCTIONALITY
        {
            private AddThisControl _control;
            public string URL { get; set; }
            public string PublisherId { get; set; }
            public Enums.ADDTHIS_STYLE? Style { get; set; }
            public FUNCTIONALITY(AddThisControl control)
            {
                this.PublisherId = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Others_AddThis_PublisherId) ?? "";
                var styleStr = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Others_AddThis_Style) ?? "";
                if (!string.IsNullOrEmpty(styleStr) && this.Style == null)
                {
                    Enums.ADDTHIS_STYLE style;
                    if (Enum.TryParse<Enums.ADDTHIS_STYLE>(styleStr, out style))
                    {
                        this.Style = style;
                    }
                }
                _control = control;
            }
            public IContentPageProperties ContentPage { get; set; }
        }

        public FUNCTIONALITY Functionality { get; private set; }
        public AddThisControl()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        public string GetAddThisPubId()
        {
            string s = this.Functionality.PublisherId;
            s = s.Trim();
            return s;
        }

        private string initContent()
        {
            StringBuilder sb = new StringBuilder();
            if (_dict != null && _dict.Count > 0)
            {
                foreach (var item in _dict)
                {
                    sb.Append(item.Key + "=" + item.Value);
                }
            }

            string cssClass = null;
            if(this.Functionality.Style == Enums.ADDTHIS_STYLE.DefaultStyleLarge)
            {
                cssClass = "addthis_32x32_style";
            }

            switch(this.Functionality.Style)
            {
                case Enums.ADDTHIS_STYLE.DefaultStyleSmall:
                case Enums.ADDTHIS_STYLE.DefaultStyleLarge:
                    return @"<div class='addthis_toolbox addthis_default_style " + cssClass + "'" + sb +@">
                                    <a class='addthis_button_preferred_1'></a>
                                    <a class'=addthis_button_preferred_2'></a>
                                    <a class='addthis_button_preferred_3'></a>
                                    <a class='addthis_button_preferred_4'></a>
                                    <a class='addthis_button_compact'></a>
                                    <a class='addthis_counter addthis_bubble_style'></a>
                             </div>";
                case Enums.ADDTHIS_STYLE.ExtraCountStyle:
                    return @"<div class='addthis_toolbox addthis_default_style'>
                                <a class='addthis_button_facebook_like' fb:like:layout='button_count'></a>
                                <a class='addthis_button_tweet'></a>
                                <a class='addthis_button_google_plusone' g:plusone:size='medium'></a>
                                <a class='addthis_counter addthis_pill_style'></a>
                             </div>";
            }
            return null;
        }


        private void init()
        {
            _dict = new Dictionary<string, string>();

            if(Functionality.ContentPage != null && Functionality.ContentPage.Data.IsAvailableToFrontend())
            {
                var cp = Functionality.ContentPage;
                _dict.Add("addthis:url", cp.GetUrl());
                _dict.Add("addthis:title", cp.PageTitle);
                _dict.Add("addthis:description", cp.MetaDescription);
            }
            else
            {
                this.Visible = false;
            }

            if(!String.IsNullOrEmpty(Functionality.URL))
            {
                _dict.Add("addthis:url", Functionality.URL);
                this.Visible = true;
            }

            ltlAddThisContent.Text = initContent();
        }

        public static AddThisControl LoadControl(Page pg, string id)
        {
            var ctrl = pg.LoadControl("/_ComponentsGeneric/Frontend/UserControls/SocialNetworking/AddThis/AddThisControl.ascx") as AddThisControl;
            if (!string.IsNullOrEmpty(id))
            {
                ctrl.ID = id;
            }
            return ctrl;
        }
    }
}