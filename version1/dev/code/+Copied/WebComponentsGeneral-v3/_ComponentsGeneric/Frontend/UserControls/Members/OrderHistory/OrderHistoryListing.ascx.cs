﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.OrderModule;
using BusinessLogic_v3.Modules.OrderItemModule;
using BusinessLogic_v3.Modules.OrderModule;
using CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.Paging;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.General_v3.Classes.HelperClasses;
using CS.WebComponentsGeneralV3.Code.Classes.URL;
using BusinessLogic_v3.Frontend.MemberModule;
using CS.General_v3;
using BusinessLogic_v3.Frontend.ContentTextModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.OrderHistory
{
    public partial class OrderHistoryListing : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private OrderHistoryListing _control;
            public FUNCTIONALITY(OrderHistoryListing control)
            {
                _control = control;
            }
 
            public URLParserOrderHistory Url { get; set; }
            public MemberBaseFrontend Member { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public OrderHistoryListing()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
            if (Functionality.Member != null)
            {
                initUrl();
            }
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(thDate, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_Heading_Date).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thDescription, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_Heading_Description).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thStatus, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_Heading_Status).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thInvoice, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_Heading_Invoice).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thTotal, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_Heading_Total).ToFrontendBase());
            
            pagingBarTop.Functionality.ResultsText.Functionality.ResultsTextPlural = pagingBarBottom.Functionality.ResultsText.Functionality.ResultsTextPlural = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_ResultsTextPlural).ToFrontendBase();
            pagingBarTop.Functionality.ResultsText.Functionality.ResultsTextSingular = pagingBarBottom.Functionality.ResultsText.Functionality.ResultsTextSingular = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_ResultsTextSingular).ToFrontendBase();
        }

        private void initOrderHistory(URLParserOrderHistory url)
        {
            int showAmt = (int) url.ShowAmt.GetValue();
            SearchPagedResults<IOrderBase> orderHistory = this.Functionality.Member.Data.GetOrderHistoryWithPaging(url.PageNo.GetValue(), showAmt, url.SortBy.GetValue(), includeNonPaid:true);
            
            int totalResults = orderHistory.TotalResults;
            pagingBarTop.Functionality.UpdateValues(url, totalResults, false, true);
            pagingBarBottom.Functionality.UpdateValues(url, totalResults, true, false);

            if (orderHistory.TotalResults > 0)
            {
                IEnumerable<OrderBaseFrontend> orders = orderHistory.GetResults().ToFrontendBaseList();
                int count = 0;
                foreach (var order in orders)
                {
                    count++;
                    var row = getRow(order, count % 2 == 0);
                    tBodyListing.Controls.Add(row);
                }
            }
            else
            {
                listingPlaceholder.Visible = false;
                pagingBarTop.Visible = false;
            }
        }

        private TableRow getRow(OrderBaseFrontend order, bool isEven)
        {
            TableRow tr = new TableRow();

            if(isEven)
            {
                tr.CssClass += " even";
            }
            TableCell tdDate = new TableCell();
            tdDate.Text = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatDateTimeWithCurrentCulture(order.Data.DateCreated, 
                CS.General_v3.Util.Date.DATETIME_FORMAT.FullDateShortTime);
            
            TableCell tdInvoice = new TableCell();
            tdInvoice.Controls.Add(createOrderRefAnchor(order));
            tdInvoice.CssClass = "listing-table-orderitem-invoice";

            TableCell tdDescription = new TableCell();

            string desc;
            ContentTextBaseFrontend ctDesc;
            order.GetDescription(out desc, out ctDesc);
            if (ctDesc != null)
            {
                MyContentText.AttachContentTextWithControl(tdDescription, ctDesc);
            }
            else {

                tdDescription.Text = desc;
            }

            TableCell tdStatus = new TableCell();
            MySpan statusText = new MySpan();
            MyContentText.AttachContentTextWithControl(statusText, order.Data.Paid
                                                           ? BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_Status_Paid).ToFrontendBase()
                                                           : BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_Status_NotPaid).ToFrontendBase());
            statusText.CssManager.AddClass("order-history-listing-status-text");
            tdDate.CssClass = "order-history-listing-date";
            tdDescription.CssClass = "order-history-listing-description";
            

            tdStatus.Controls.Add(statusText);
            tdStatus.CssClass = "order-history-listing-status";
            if (!order.Data.Paid)
            { 
                MyAnchor aPaid = new MyAnchor();
                MyContentText.AttachContentTextWithControl(aPaid, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_PayOnlineButtonText).ToFrontendBase());
                aPaid.CssManager.AddClass("button order-history-listing-pay-online");
                aPaid.Href = order.GetPayOnlineUrl();
                aPaid.HrefTarget = Enums.HREF_TARGET.Blank;
                tdStatus.Controls.Add(aPaid);
                tdStatus.CssClass += " order-history-listing-status-not-paid";
            }
            else
            {
                tdStatus.CssClass += " order-history-listing-status-paid";

            }

            TableCell tdTotal = new TableCell();
            tdTotal.CssClass += "listing-table-orderitem-total";

            tdTotal.Text =
                BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(
                    order.Data.GetTotalPrice(), CS.General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
 
            tr.Controls.Add(tdDate);
            tr.Controls.Add(tdDescription);
            tr.Controls.Add(tdStatus);
            tr.Controls.Add(tdInvoice);
            tr.Controls.Add(tdTotal);
            return tr;
        }

        private MyAnchor createOrderRefAnchor(OrderBaseFrontend order)
        {
            MyAnchor anchor = new MyAnchor();
            anchor.InnerHtml = "view";
            anchor.HrefTarget = CS.General_v3.Enums.HREF_TARGET.Blank;
            anchor.Href = order.GetURL();
            return anchor;
        }

        private void initUrl()
        {
            var url = Functionality.Url;
            if (url != null)
            {
                initOrderHistory(url);
            }
        }
    }
}