﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.URL;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ContentTextModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Search
{
    public partial class SearchField : BaseUserControl
    {
        private URLParserDefaultShopParams _urlParser;
        public class FUNCTIONALITY
        {
            private SearchField _control;
            public FUNCTIONALITY(SearchField control)
            {
                _control = control;
            }
            public ContentTextBaseFrontend PlaceHolderText { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public SearchField()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        public void ForceRender()
        {
            init();
        }

        protected override void OnLoad(EventArgs e)
        {
            ForceRender();
            base.OnLoad(e);
        }

        private void init()
        {
            var searchText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Search_FieldParameter).ToFrontendBase();
            txtSearch.FieldParameters.titleContentText = searchText;
            initUrlParser();
            initButton();
            
            txtSearch.InitialValue = CS.General_v3.Util.RoutingUtil.ConvertRouteValueToString(_urlParser.Keywords.GetValueAsString(loadDefaultValueIfNull:false));
        }

        private void initButton()
        {
            if (this.Functionality.PlaceHolderText != null)
            {
                txtSearch.PlaceholderText = this.Functionality.PlaceHolderText.GetContent();
            }
            btnSearch.Click += new EventHandler(btnSearch_Click);
        }

        private void initUrlParser()
        {
            _urlParser = new URLParserDefaultShopParams();
        }

        void btnSearch_Click(object sender, EventArgs e)
        {
            string keywords = txtSearch.GetFormValueAsString();

            if(!string.IsNullOrEmpty(keywords))
            {
                _urlParser.Keywords.SetValue(keywords);
                _urlParser.Redirect();
            }
        }

        public static SearchField LoadControl(Page pg)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/Search/SearchField.ascx") as SearchField;
            return ctrl;
        }
    }
}