﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Text;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Extensions;
namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing
{
    using BusinessLogic_v3;
    using Code.Classes.URL;
using BusinessLogic_v3.Classes.Interfaces;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public partial class GenericListingItemsFullPage : BaseUserControl
    {

        #region GenericListingItemsFullPage Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected GenericListingItemsFullPage _item = null;

            public ArticleBaseFrontend ContentPage { get; set; }
            public IURLParserPagingInfo UrlParserWithPaging { get; set; }
            public IEnumerable<IListingItemData> Items { get; set; }
            public int TotalResults { get; set; }
            public string PageTitle { get; set; }
            public ContentTextBaseFrontend PageTitleContentText { get; set; }
            public TokenReplacerNew PageTitleContentTextReplacementTags { get; set; }
            public Control PageTitleCtrl { get; set; }
            public ContentTextBaseFrontend ResultTextPlural { get; set; }
            public ContentTextBaseFrontend ResultTextSingular { get; set; }
            public ContentTextBaseFrontend NoResultsText { get;set; }
            public GenericListingItems GenericListingItems { get { return _item.listingItems; } }

            internal FUNCTIONALITY(GenericListingItemsFullPage item)
            {
                this._item = item;
                ResultTextPlural = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Paging_ResultsTextPlural).ToFrontendBase();
                ResultTextSingular = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Paging_ResultsTextSingular).ToFrontendBase();
                NoResultsText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Search_NoResultsText).ToFrontendBase();
            }
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }
        private void initContentPageOrTitle()
        {
            contentPage.Visible = false;
            if (this.Functionality.PageTitleContentText != null)
            {
                hTitle.Visible = true;
                MyContentText.AttachContentTextWithControl(hTitle, this.Functionality.PageTitleContentText, this.Functionality.PageTitleContentTextReplacementTags);
            }
            else if (!string.IsNullOrEmpty(this.Functionality.PageTitle))
            {
                hTitle.Visible = true;
                hTitle.InnerText = this.Functionality.PageTitle;
            }
            else if(this.Functionality.PageTitleCtrl != null)
            {
                headingContainer.Controls.Add(this.Functionality.PageTitleCtrl);
            }
            else if(this.Functionality.ContentPage != null)
            {
                contentPage.Visible = true;
                contentPage.Functionality.ContentPage = this.Functionality.ContentPage;
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(headingContainer);
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(hTitle);
            }
            if (contentPage.Visible == false)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(contentPage);
            }
            
        }

        private void init()
        {
            initContentPageOrTitle();
            initItems();
        }

        private void initItems()
        {
            if (this.Functionality.Items != null && this.Functionality.Items.Count() > 0)
            {
                listingItems.Functionality.Items = this.Functionality.Items;
                initPagingBar(this.Functionality.TotalResults);
            }
            else
            {
                divNoResultsText.Visible = true;
                MyContentText.AttachContentTextWithControl(divNoResultsText, this.Functionality.NoResultsText);

                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(pagingBarBottom);
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(pagingBarTop);
            }
        }

        private void initPagingBar(int totalResults)
        {
            var showPagingBarTop = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_ShowPagingBarInTopInGeneralListing);
            if(showPagingBarTop)
            {
                pagingBarTop.Functionality.UpdateValues(this.Functionality.UrlParserWithPaging, totalResults, false, BusinessLogic_v3.Modules.ModuleSettings.Generic.ShowCombosInGeneralListing);
                pagingBarTop.Functionality.ResultsText.Functionality.UpdateFromPagingInfo(this.Functionality.UrlParserWithPaging, totalResults);
                pagingBarTop.Functionality.ResultsText.Functionality.ResultsTextPlural = pagingBarBottom.Functionality.ResultsText.Functionality.ResultsTextPlural = this.Functionality.ResultTextPlural;
                pagingBarTop.Functionality.ResultsText.Functionality.ResultsTextSingular = pagingBarBottom.Functionality.ResultsText.Functionality.ResultsTextSingular = this.Functionality.ResultTextSingular;
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(pagingBarTop);
            }
            
            pagingBarBottom.Functionality.UpdateValues(this.Functionality.UrlParserWithPaging, totalResults, true);
            pagingBarBottom.Functionality.ResultsText.Functionality.UpdateFromPagingInfo(this.Functionality.UrlParserWithPaging, totalResults);
        }
    }
}