﻿using System;
using System.Collections.Generic;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.TestimonialModule;
using System.Linq;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Testimonials
{
    public partial class TestimonialsItems : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private TestimonialsItems _control;
            public FUNCTIONALITY(TestimonialsItems control)
            {
                _control = control;
            }
            public IEnumerable<TestimonialBaseFrontend> Testimonials { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public TestimonialsItems()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadTestimonials();
        }

        private void loadTestimonials()
        {
            int maximumItems = this.Functionality.Testimonials.Count();
            if(this.Functionality.Testimonials != null && this.Functionality.Testimonials.Count() > 0)
            {
                int count = 0;
                foreach (var testimonial in this.Functionality.Testimonials)
                {
                    count++;
                    var _ctrl = TestimonialsItem.LoadControl(this.Page);
                    _ctrl.Functionality.Testimonial = testimonial;
                    if (count % maximumItems == 0)
                    {
                        _ctrl.Functionality.IsLastItem = true;
                    }
                    divTestimonialsWrapper.Controls.Add(_ctrl);
                }
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(this);
            }
        }
    }
}