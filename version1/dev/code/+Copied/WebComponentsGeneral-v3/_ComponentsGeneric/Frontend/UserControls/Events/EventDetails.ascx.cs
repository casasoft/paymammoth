﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Text;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.Contact;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.EventModule;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Constants;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Events
{
    public partial class EventDetails : BaseUserControl
    {

        public class EventDetailsData
        {
            public ContentTextBaseFrontend Label { get; set; }
            public string CssClass { get; set; }
            public Control Value { get; set; }
            public int Priority { get; set; }
        }

        public class FUNCTIONALITY
        {
            private EventDetails _control;
            public FUNCTIONALITY(EventDetails control)
            {
                _control = control;
            }
            public List<EventDetailsData> EventDetailsItems { get; set; }
            public bool DoNotShowIcon { get; set; }
            public EventBaseFrontend Event { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public EventDetails()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
            initIcon();
            loadEventDetails();
            initEventDetailsTable();
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(h2EventDetailsTitle, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_EventDetails_Title).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(divEventDetailsText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_EventDetails_Description).ToFrontendBase());
        }

        private void initEventDetailsTable()
        {
            if (this.Functionality.EventDetailsItems != null && this.Functionality.EventDetailsItems.Count > 0)
            {
                var list = sortList(this.Functionality.EventDetailsItems);
                foreach (EventDetailsData item in list)
                {
                    MyTableRow trRow = new MyTableRow();
                    MyTableCell tdLabel = new MyTableCell();
                    MyTableCell tdValue = new MyTableCell();

                    tdLabel.CssClass = "field-event-details-label";
                    MySpan spanLabelText = new MySpan();
                    spanLabelText.CssManager.AddClass("event-details-label-text");

                    MyContentText.AttachContentTextWithControl(spanLabelText, item.Label);
                    tdLabel.Controls.Add(spanLabelText);             
                    trRow.Controls.Add(tdLabel);

                    tdValue.CssClass = "field-event-details-content";
                    if (!string.IsNullOrEmpty(item.CssClass))
                    {
                        tdValue.WebControlFunctionality.CssManager.AddClass(item.CssClass);
                    }

                    var category = this.Functionality.Event.Data.Category.ToFrontendBase();
                    if(category != null && !string.IsNullOrEmpty(category.Data.Identifier))
                    {
                        imgIcon.ImageUrl = "/images/sprites/symbol-icon-" + category.Data.Identifier.ToLower() + ".png"; 
                    }

                    tdValue.Controls.Add(item.Value);
                    trRow.Controls.Add(tdValue);
                    tblEventDetails.Controls.Add(trRow);
                }
            }
        }

        private IEnumerable<EventDetailsData> sortList(IEnumerable<EventDetailsData> list)
        {
            return CS.General_v3.Util.ListUtil.SortByMultipleComparers(list, (x1, x2) => x1.Priority.CompareTo(x2.Priority));

        }

        private void loadEventDetails()
        {
            if(this.Functionality.EventDetailsItems == null)
            {
                this.Functionality.EventDetailsItems = new List<EventDetailsData>();
            }

            //Tutors
            {
                var tutors = this.Functionality.Event.Data.GetEventTutors().ToFrontendBaseList();
                if (tutors != null && tutors.Count() > 0)
                {
                    var _ctrl = EventTutorThumbnailItems.LoadControl(this.Page);
                    _ctrl.Functionality.Members = tutors;

                    EventDetailsData data = new EventDetailsData
                    {
                        Label = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_EventDetails_Label_Tutors).ToFrontendBase(),
                        CssClass = "field-event-details-tutors",
                        Priority = 0,
                        Value = _ctrl
                    };
                    this.Functionality.EventDetailsItems.Add(data);
                }
            }

            //Sessions
            {
                var eventSessions = this.Functionality.Event.Data.Sessions;
                if(!string.IsNullOrEmpty(eventSessions))
                {
                    EventDetailsData data = new EventDetailsData
                    {
                        Label = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_EventDetails_Label_Sessions).ToFrontendBase(),
                        CssClass = "field-event-details-sessions",
                        Priority = 10,
                        Value = new MySpan(){InnerHtml = eventSessions}
                    };
                    this.Functionality.EventDetailsItems.Add(data);
                }
            }

            //Duration
            {
                var eventDuration = this.Functionality.Event.Data.Duration;
                if (!string.IsNullOrEmpty(eventDuration))
                {
                    EventDetailsData data = new EventDetailsData
                    {
                        Label = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_EventDetails_Label_Duration).ToFrontendBase(),
                        CssClass = "field-event-details-duration",
                        Priority = 20,
                        Value = new MySpan() { InnerHtml = eventDuration }
                    };
                    this.Functionality.EventDetailsItems.Add(data);
                }
            }

            //Dates
            {
                var eventDates = this.Functionality.Event.Data.Dates;
                if (!string.IsNullOrEmpty(eventDates))
                {
                    EventDetailsData data = new EventDetailsData
                    {
                        Label = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_EventDetails_Label_Dates).ToFrontendBase(),
                        CssClass = "field-event-details-dates",
                        Priority = 40,
                        Value = new MySpan() { InnerHtml = eventDates }
                    };
                    this.Functionality.EventDetailsItems.Add(data);
                }
            }

            //Class Size
            {
                var eventSize = this.Functionality.Event.Data.ClassSize;
                if (eventSize != 0)
                {
                    MySpan spanClassSize = new MySpan();

                    var classSizeCntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_EventDetails_ClassSize).ToFrontendBase();
                    TokenReplacerNew dict = classSizeCntTxt.CreateTokenReplacer();
                    dict.Add(Tokens.EVENT_MAXIMUM_SLOTS, eventSize.ToString());
                    
                    EventDetailsData data = new EventDetailsData
                    {
                        Label = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_EventDetails_Label_ClassSize).ToFrontendBase(),
                        CssClass = "field-event-details-class-size",
                        Priority = 50,
                        Value = new MySpan() { InnerHtml = classSizeCntTxt.GetContent(dict) }
                    };
                    this.Functionality.EventDetailsItems.Add(data);
                }
            }

            //Price
            {
                var eventPrice = this.Functionality.Event.Data.Price;
                if (!string.IsNullOrEmpty(eventPrice))
                {
                    EventDetailsData data = new EventDetailsData
                    {
                        Label = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_EventDetails_Label_Price).ToFrontendBase(),
                        CssClass = "field-event-details-class-price",
                        Priority = 60,
                        Value = new MySpan(){ InnerHtml = eventPrice}
                    };
                    this.Functionality.EventDetailsItems.Add(data);
                }
            }
        }

        private void initIcon()
        {
            if (this.Functionality.DoNotShowIcon)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divEventDetailsIcon);
            }
        }
    }
}