﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ProductCategorySpecificationValueModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using System.Web.UI.HtmlControls;
using BusinessLogic_v3.Frontend.ProductModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    public partial class ProductItemSpecifications : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private ProductItemSpecifications _control;
            public FUNCTIONALITY(ProductItemSpecifications control)
            {
                _control = control;
            }
            public ProductBaseFrontend Product { get; set;}
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ProductItemSpecifications()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
            loadSpecifications();
        }

        private void loadSpecifications()
        {
            if(this.Functionality.Product != null)
            {
                IEnumerable<ProductCategorySpecificationValueBaseFrontend> specificationValues = this.Functionality.Product.Data.ProductCategorySpecificationValues.ToFrontendBaseList();
                if(specificationValues != null && specificationValues.Count() > 0)
                {
                    foreach (var specificationValue in specificationValues)
                    {
                        createRow(specificationValue);
                    }
                }
                else
                {
                    removeControl();
                    
                }
            }
            else
            {
                removeControl();
            }
        }

        private void createRow(ProductCategorySpecificationValueBaseFrontend specificationValue)
        {
           if(specificationValue != null)
           {
               MyTableRow tr = new MyTableRow();
               MyTableCell tdLabel = new MyTableCell();
               tdLabel.InnerHtml = specificationValue.Data.Title;
               tdLabel.CssManager.AddClass("product-specification-value-title");

               MyTableCell tdValue = new MyTableCell();
               tdValue.InnerHtml = specificationValue.Data.Value;

               tr.Controls.Add(tdLabel);
               tr.Controls.Add(tdValue);
               tblProductSpecifications.Controls.Add(tr);
           }
        }

        private void removeControl()
        {
            this.Visible = false;
        }

        private void updateTexts()
        {
            var cntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ProductPage_Specifications_Title).ToFrontendBase();
            MyContentText.AttachContentTextWithControl(spanProductSpecifications,cntTxt);
        }
    }
}