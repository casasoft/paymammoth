﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckOutProceedToPayment.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.CheckOutProceedToPayment" %>
<div class="checkout-proceed-to-payment-container clearfix">
    <div class="checkout-proceed-to-payment-text" runat="server" id="divProceedToPaymentText"></div>
    <CSControls:MyButton runat="server" ID="btnProceedToPayment" />
</div>