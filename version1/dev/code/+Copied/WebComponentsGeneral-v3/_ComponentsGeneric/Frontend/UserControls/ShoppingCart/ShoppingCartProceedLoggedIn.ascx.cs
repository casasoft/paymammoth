﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules.ShoppingCartModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ShoppingCartModule;
using CS.General_v3.Classes.URL;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart
{
    public partial class ShoppingCartProceedLoggedIn : BaseUserControl
    {
        public delegate ShoppingCartDeliveryDetailsHelper ShoppingCartSubmitHandler(ShoppingCartProceedLoggedIn sender);
        public class FUNCTIONALITY
        {
            public event ShoppingCartSubmitHandler OnSubmit;
            private ShoppingCartProceedLoggedIn _control;
            public FUNCTIONALITY(ShoppingCartProceedLoggedIn control)
            {
                _control = control;
            }

            internal ShoppingCartDeliveryDetailsHelper triggerOnSubmit()
            {
                if (this.OnSubmit != null)
                {
                    return this.OnSubmit(_control);
                }
                return null;
            }
            public string DeliveryDetailsUrl { get; set; }
            public bool IsInCheckOut { get; set; }
            public ShoppingCartBaseFrontend ShoppingCart { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ShoppingCartProceedLoggedIn()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateContent();
        }

        private void updateContent()
        {
            divShoppingCartProceedContainer.Attributes["class"] += " shopping-cart-"+(this.Functionality.IsInCheckOut ? "checkout" : "cart")+"-proceed-container";
            if (this.Functionality.IsInCheckOut)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divCartContent);
                anchorProceedToCheckOut.Visible = false;
                divProceedPaymentText.Visible = true;
                MyContentText.AttachContentTextWithControl(hOnlinePayment, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_OnlinePaymentTitle).ToFrontendBase());
                MyContentText.AttachContentTextWithControl(hPayLater, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_PayLaterTitle).ToFrontendBase());

                MyContentText.AttachContentTextWithControl(divProceedPaymentText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_ProceedToPayment_Text).ToFrontendBase());
                MyContentText.AttachContentTextWithControl(btnProceedToPayment, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_ProceedToPayment).ToFrontendBase());
                
                btnProceedToPayment.Visible = true;
                btnProceedToPayment.Click += new EventHandler(btnProceedToPayment_Click);
                if (BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Shop_Orders_AllowPayLater))
                {
                    MyContentText.AttachContentTextWithControl(btnPayLater, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_OrderAndPayLater).ToFrontendBase());
                    MyContentText.AttachContentTextWithControl(divPayLaterText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_OrderAndPayLater_Text).ToFrontendBase());
                    btnPayLater.Click +=new EventHandler(btnPayLater_Click);
                
                    
                    
                }
                else
                {
                    CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divProceedToPayLater);
                }

            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divCheckoutContent);
                MyContentText.AttachContentTextWithControl(divProceedToCheckoutText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_MyCart_ProceedToCheckoutText).ToFrontendBase());
                MyContentText.AttachContentTextWithControl(anchorProceedToCheckOut, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_MyCart_ProceedToCheckout).ToFrontendBase());
                anchorProceedToCheckOut.HRef = this.Functionality.DeliveryDetailsUrl;
            }
        }
        private void goBackToFillInDeliveryDetails()
        {
            var cp = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.ShoppingCart_DeliveryDetails).ToFrontendBase();
            this.Page.Functionality.ShowErrorMessageAndRedirectToPage(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ShoppingCart_CheckOut_DeliveryDetailsRequired).ToFrontendBase(), cp.GetUrl());

        }

        void btnPayLater_Click(object sender, EventArgs e)
        {
            ShoppingCartDeliveryDetailsHelper deliveryDetails = this.Functionality.triggerOnSubmit();
            if (deliveryDetails != null)
            {
                var order = this.Functionality.ShoppingCart.Data.ConvertToOrder(deliveryDetails).ToFrontendBase();
                var cpOrderAndPayLater = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_Order_OrderAndPayLater).ToFrontendBase();
                URLClass url = new URLClass(cpOrderAndPayLater.GetUrl());
                url[BusinessLogic_v3.Constants.ParameterNames.ID] = order.Data.ID.ToString();
                Response.Redirect(url.GetURL());
            }
            else
            {
                goBackToFillInDeliveryDetails();
            }
        }

        void btnProceedToPayment_Click(object sender, EventArgs e)
        {
            ShoppingCartDeliveryDetailsHelper deliveryDetails = this.Functionality.triggerOnSubmit();
            if(deliveryDetails != null)
            {
                var order = this.Functionality.ShoppingCart.Data.ConvertToOrder(deliveryDetails).ToFrontendBase();
                var payOnlineUrl = order.GetPayOnlineUrl();
                CS.General_v3.Util.PageUtil.RedirectPage(payOnlineUrl);
                //PayMammothConnectorBL.v3.Util.FrontendGeneral.GeneratePayMammothRequestFromOrderAndRedirect(order.Data);
            }
            else
            {
                goBackToFillInDeliveryDetails();
            }
        }
    }
}