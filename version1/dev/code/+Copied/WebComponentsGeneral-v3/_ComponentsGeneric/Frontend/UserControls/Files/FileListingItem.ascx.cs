﻿using System;
using System.Web.UI;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.EventModule;
using BusinessLogic_v3.Frontend.FileItemModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Files
{
    public partial class FileListingItem : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private FileListingItem _control;
            public FUNCTIONALITY(FileListingItem control)
            {
                _control = control;
            }
            public FileItemBaseFrontend FileItem { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public FileListingItem()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateContent();
        }

        private void updateContent()
        {
            if(this.Functionality.FileItem != null)
            {
                tdDateUploaded.InnerHtml = this.Functionality.FileItem.Data.UploadedOn.ToShortDateString();
                EventBaseFrontend eventItem = this.Functionality.FileItem.Data.Event.ToFrontendBase();
                if(eventItem != null)
                {
                    anchorFileListingItemCourse.InnerHtml = eventItem.Data.Title;
                    anchorFileListingItemCourse.Href = eventItem.GetUrl();
                }
                
                btnFileListingItem.Text = this.Functionality.FileItem.Data.Title;
                var enumFileType = CS.General_v3.Util.IO.GetFileExtensionType(this.Functionality.FileItem.Data.File.GetFilenameExtension());
                btnFileListingItem.CssManager.AddClass(CS.General_v3.Util.EnumUtils.StringValueOf(enumFileType).ToLower() + "-file");
                btnFileListingItem.Click += new EventHandler(btnFileListingItem_Click);
            }
            else
            {
                throw new Exception("File Item cannot be null");
            }
        }

        void btnFileListingItem_Click(object sender, EventArgs e)
        {
            string path = this.Functionality.FileItem.Data.File.GetLocalPath();
            var result = CS.General_v3.Util.Web.ForceDownload(path);
            if (!result)
            {
                var fileNotFoundCntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Members_FileListing_FileNotFound).ToFrontendBase();
                this.Page.Functionality.ShowErrorMessageAndRedirectToPage(fileNotFoundCntTxt);
            }
        }

        public static FileListingItem LoadControl(Page pg)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/Files/FileListingItem.ascx") as FileListingItem;
            return ctrl;
        }
    }
}