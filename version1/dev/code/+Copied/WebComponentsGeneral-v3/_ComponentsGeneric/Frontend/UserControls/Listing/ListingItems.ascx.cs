﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing
{
    using BusinessLogic_v3;
    using BusinessLogic_v3.Classes.Text;
    using BusinessLogic_v3.Extensions;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using Code.Classes.URL;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public partial class ListingItems : BaseUserControl
    {


        #region ListingItems Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            public bool DoNotShopTopPagingBar { get; set; }
            public bool DoNotShopBottomPagingBar { get; set; }
            public bool ShowCombos { get; set; }

            protected ListingItems _item = null; 
            public IEnumerable<object> Items { get; set; }
            public string ListingItemUserControlPath { get; set; }
            public IURLParserPagingInfo UrlParserWithPaging { get; set; }
            public int? TotalResults { get; set; }
            public string PageTitle { get; set; }
            public ContentTextBaseFrontend PageTitleContentText { get; set; }
            public ContentTextBaseFrontend ResultTextPlural { get; set; }
            public ContentTextBaseFrontend ResultTextSingular { get; set; }
            public ContentTextBaseFrontend NoResultsText { get; set; }
            internal FUNCTIONALITY(ListingItems item)
            {
                this.Items = new List<object>();
                this._item = item;
                this.ShowCombos = true;
                ResultTextPlural = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Paging_ResultsTextPlural).ToFrontendBase();
                ResultTextSingular = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Paging_ResultsTextSingular).ToFrontendBase();
                NoResultsText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Search_NoResultsText).ToFrontendBase();
            }
            

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
        private void validate()
        {
            if (string.IsNullOrEmpty(this.Functionality.ListingItemUserControlPath))
            {
                throw new Exception("Please specify user control to iterate");
            }
            if (!this.Functionality.TotalResults.HasValue)
            {
                throw new Exception("Please specify TotalResults");
            }
            if (this.Functionality.UrlParserWithPaging == null)
            {
                throw new Exception("Please specify URLParserWithPaging");
            }
        }

        private void initItems()
        {
            bool hasResults = false;
            if (this.Functionality.Items != null)
            {
                
                
                foreach (object item in this.Functionality.Items)
                {
                    hasResults = true;
                    var ctrl = ListingItemControlBase.LoadControl(this.Functionality.ListingItemUserControlPath, this.Page);
                    ctrl.PopulateControl(item);
                    divResults.Controls.Add(ctrl);
                }
            }
            if (!hasResults)
            {
                MyContentText.AttachContentTextWithControl(divResults, this.Functionality.NoResultsText);
            }
        }
        private void initPagingBar(Paging.PagingBar pagingBar, bool showCombos)
        {
            pagingBar.Functionality.UpdateValues(this.Functionality.UrlParserWithPaging, this.Functionality.TotalResults.GetValueOrDefault(), true, showCombos);
            pagingBar.Functionality.ResultsText.Functionality.UpdateFromPagingInfo(this.Functionality.UrlParserWithPaging, this.Functionality.TotalResults.GetValueOrDefault());
            pagingBar.Functionality.ResultsText.Functionality.ResultsTextPlural = this.Functionality.ResultTextPlural;
            pagingBar.Functionality.ResultsText.Functionality.ResultsTextSingular = this.Functionality.ResultTextSingular;

            if(this.Functionality.TotalResults.GetValueOrDefault() == 0)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divBottomSection);
                //divBottomSection.Parent.Controls.Remove(divBottomSection);
            }
        }

        private void initPagingBars()
        {
            if (this.Functionality.DoNotShopTopPagingBar)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(this.pagingBarTop);
            }
            else
            {
                initPagingBar(this.pagingBarTop, this.Functionality.ShowCombos);
            }
            if (this.Functionality.DoNotShopBottomPagingBar)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(this.pagingBarBottom);
            }
            else
            {
                initPagingBar(this.pagingBarBottom, false);
            }
            
        }

        protected override void OnLoad(EventArgs e)
        {
            validate();
            initItems();
            initPagingBars();
            base.OnLoad(e);
        }
    }
}