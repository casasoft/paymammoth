﻿using BusinessLogic_v3.Constants;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Modules.ContentTextModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login
{
    using System;
    using Code.Classes.Pages;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using BusinessLogic_v3.Classes.Interfaces;
    using CS.General_v3.Classes.Login;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using BusinessLogic_v3.Modules.MemberModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;
    using BusinessLogic_v3;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using Brickred.SocialAuth.NET.Core.BusinessObjects;
    using CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login.SocialButtons;
    using CS.General_v3.Util;
    using BusinessLogic_v3.Modules.ArticleModule;
    using System.Web;

    public partial class LoginFields : BaseUserControl
    {
        private bool _rememberMeEnabled = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_RememberMeEnabled);
        private bool _loginWithUsername;

        private MyTxtBoxString _txtUsername;
        private MyTxtBoxEmail _txtEmail;

        private MyTxtBoxString _txtPassword;
        private MyCheckBoxWithLabel _chkRememberMe;
        private MyButton _btnSubmit;
        private MyAnchor _anchorForgotPassword;
        private MySpan _spanRegisterAccount;
        private ContentTextBaseFrontend _LblEmail;

        private PageLoginHandler __loginHandler;
        private PageLoginHandler _loginHandler
        {
            get
            {
                if (__loginHandler == null)
                {
                    __loginHandler = new PageLoginHandler(Page);
                }
                return __loginHandler;
            }
        }

        public class FUNCTIONALITY
        {
            private LoginFields _control;

            public event LoginFieldsSuccessHandler OnLoginSuccess { add { _control._loginHandler.OnLoginSuccess += value; } remove { _control._loginHandler.OnLoginSuccess -= value; } }
            public event LoginFieldsErrorHandler OnLoginFail { add { _control._loginHandler.OnLoginFail += value; } remove { _control._loginHandler.OnLoginFail -= value; } }

            public bool OnLoginAutoRedirect { get { return _control._loginHandler.OnLoginAutoRedirect; } set { _control._loginHandler.OnLoginAutoRedirect = value; } }
            public bool OnLoginRedirectToLastNoAccessURL { get { return _control._loginHandler.OnLoginRedirectToLastNoAccessURL; } set { _control._loginHandler.OnLoginRedirectToLastNoAccessURL = value; } }
            public string OnLoginRedirectToURL { get { return _control._loginHandler.OnLoginRedirectToURL; } set { _control._loginHandler.OnLoginRedirectToURL = value; } }
            public string RegisterUrl { get; set; }
            
            public bool DoNotAllowAutoComplete { get; set; }

            public string OnLoginMessage { get; set; }

            public string ValidationGroup { get; set; }
        

            public ContentTextBaseFrontend LblUsername { get; set; }
            public ContentTextBaseFrontend LblPassword { get; set; }
            public ContentTextBaseFrontend RegisterText { get; set; }

            public ContentTextBaseFrontend BtnSubmitText { get; set; }
            public ContentTextBaseFrontend ChkRememberMeText { get; set; }
            public ContentTextBaseFrontend ForgotPasswordText { get; set; }
            public string ForgotPasswordURL { get; set; }

            public FUNCTIONALITY(LoginFields control)
            {
                _control = control;

                this.DoNotAllowAutoComplete = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_LoginDoNotAllowAutoComplete);
                this.ValidationGroup = "login";

                //Forgot Password URL
                var article =
                    BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                        BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_ForgotPassword);
                if(article != null)
                {
                    this.ForgotPasswordURL = article.GetUrl();
                }

                //Register URL
                if (string.IsNullOrEmpty(this.RegisterUrl))
                {
                    var articleRegister = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_Register);
                    if (articleRegister != null)
                    {
                        this.RegisterUrl = articleRegister.GetUrl();
                        if (this.RegisterUrl == null)
                        {
                            int k = 5;
                        }
                    }
                }
            }
        }

        public FUNCTIONALITY Functionality { get; private set; }

        public LoginFields()
        {
            this.Functionality = createFunctionality();
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            
            return new FUNCTIONALITY(this);

        }

        private void updateTexts()
        {
            // UPDATE TEXTS FROM CONTENT TEXT HERE
            
            //Labels
            this.Functionality.LblUsername = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Username).ToFrontendBase();
            _LblEmail = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Email).ToFrontendBase();
           
            this.Functionality.LblPassword = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Password).ToFrontendBase();
            this.Functionality.BtnSubmitText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_LoginFields_LoginButtonText).ToFrontendBase();
            this.Functionality.ChkRememberMeText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_RememberMe).ToFrontendBase();
            this.Functionality.ForgotPasswordText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_ForgotPassword).ToFrontendBase();
            
            if (!string.IsNullOrEmpty(this.Functionality.RegisterUrl))
            {
                var cntxt = BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Register.GetContentTextFrontend();
                cntxt.TokenReplacer[Tokens.REGISTER_URL] = this.Functionality.RegisterUrl;
                this.Functionality.RegisterText = cntxt;

            }
        }

        private void initLoginFields()
        {
            formLogin.Functionality.ValidationGroup = this.Functionality.ValidationGroup;

            if(BusinessLogic_v3.Modules.ModuleSettings.Members.LoginWithUsername)
            {
                _loginWithUsername = true;
                _txtUsername = (MyTxtBoxString)formLogin.Functionality.AddFieldItem(new MyTxtBoxString()
                {
                    FieldParameters = new FieldSingleControlParameters()
                    {
                        titleContentText = this.Functionality.LblUsername,
                        required = true,
                        id = "loginFields_txtUser"
                    },
                    AutoComplete = !this.Functionality.DoNotAllowAutoComplete,
                    AutoFocus = true
                });
                _txtUsername.NeverFillWithRandomValue = true;
            }
            else
            {
                _txtEmail = (MyTxtBoxEmail)formLogin.Functionality.AddFieldItem(new MyTxtBoxEmail()
                {
                    FieldParameters = new FieldSingleControlParameters()
                    {
                        titleContentText =_LblEmail,
                        required = true,
                        id = "loginFields_txtEmail"
                    },
                    AutoComplete = !this.Functionality.DoNotAllowAutoComplete,
                    AutoFocus = true
                });
                _txtEmail.NeverFillWithRandomValue = true;
            }
            
            _txtPassword = (MyTxtBoxStringPassword)formLogin.Functionality.AddFieldItem(new MyTxtBoxStringPassword()
            {
                FieldParameters = new FieldSingleControlPasswordParameters()
                {
                    titleContentText = this.Functionality.LblPassword,
                    required = true,
                    id = "loginFields_txtPass"
                },
                AutoComplete = !this.Functionality.DoNotAllowAutoComplete
            });
            _txtPassword.NeverFillWithRandomValue = true;
            
            _btnSubmit = formLogin.Functionality.AddButton(this.Functionality.BtnSubmitText);

            if (_rememberMeEnabled)
            {
                // Remember Me
                _chkRememberMe = (MyCheckBoxWithLabel) formLogin.Functionality.AddFieldItem(new MyCheckBoxWithLabel()
                                                                                            {
                                                                                                FieldParameters = new FieldSingleControlParameters()
                                                                                                                  {
                                                                                                                      id = "loginFields_chkRememberMe",
                                                                                                                      titleContentText = this.Functionality.ChkRememberMeText,
                                                                                                                      doNotValidateOnBlur = true
                                                                                                                  }, 
                                                                                                LabelContentText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_RememberMe).ToFrontendBase()
                                                                                            }, layoutInfo: new FormFieldsLayoutTypeInformation()
                                                                                                           {
                                                                                                               DoNotShowLabel = true
                                                                                                           });
                _chkRememberMe.WebControlFunctionality.NeverFillWithRandomValue = true;
            }
            
            // Forgot Password
            _anchorForgotPassword = new MyAnchor()
                                        {
                                            ID = "loginFields_anchorForgotPassword",
                                            Href = this.Functionality.ForgotPasswordURL
                                        };
            MyContentText.AttachContentTextWithControl(_anchorForgotPassword, this.Functionality.ForgotPasswordText);

            
            // Register Account
            
            if (!string.IsNullOrEmpty(this.Functionality.RegisterUrl))
            {
                _spanRegisterAccount = new MySpan()
                {
                    ID = "loginFields_spanRegisterAccount"
                };
                MyContentText.AttachContentTextWithControl(_spanRegisterAccount, this.Functionality.RegisterText);
            }

            //List
            MyUnorderedList ul = new MyUnorderedList();
            ul.CssClass = "login-links-items";

            MyListItem liForgotPassword = new MyListItem();
            liForgotPassword.CssClass = "login-links-item-forgot-password";
            liForgotPassword.Controls.Add(_anchorForgotPassword);

            MyListItem liRegisterAccount = new MyListItem();
            liRegisterAccount.CssClass = "login-links-item-register-account";
            liRegisterAccount.Controls.Add(_spanRegisterAccount);

            ul.Controls.Add(liForgotPassword);
            ul.Controls.Add(liRegisterAccount);

            FormFieldsItemDataImpl ulLinks = new FormFieldsItemDataImpl();
            ulLinks.Control = ul;

            formLogin.Functionality.AddGenericItem(ulLinks);

            _btnSubmit.Click += new EventHandler(_btnSubmit_Click);
        }

        void _btnSubmit_Click(object sender, EventArgs e)
        {
            string userCredential =null;
            userCredential = _loginWithUsername ? _txtUsername.GetFormValueAsString() : _txtEmail.GetFormValueAsString();
            string pass = _txtPassword.GetFormValueAsString();
            bool rememberMe = _chkRememberMe != null && _rememberMeEnabled ? _chkRememberMe.GetFormValueObjectAsBool() : false;
            _loginHandler.LoginUser(userCredential, pass, rememberMe);
        }


        protected override void OnLoad(EventArgs e)
        {
            updateTexts();
            initLoginFields();
            initSocialLoginButtons();
            base.OnLoad(e);
        }

        private void initSocialLoginButtons()
        {
            bool systemUsingSocialLogin = Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Login_SocialLoginEnabled);
            if (systemUsingSocialLogin)
            {
                ContentTextBaseFrontend cntxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_SocialLogin_Message).ToFrontendBase();
                MyContentText.AttachContentTextWithControl(pSocialLoginMessage, cntxt);
                FacebookLoginButton facebookLoginBtn = FacebookLoginButton.LoadControl(this.Page);
                string currentUrlFullyQualified = string.Empty;
                string currentUrlRelative = string.Empty;
                var cp = this.Page.Functionality.ContentPage;
                if (cp == null || (((IArticleBase)cp.Data).DoNotRedirectOnLogin.HasValue && ((IArticleBase)cp.Data).DoNotRedirectOnLogin.Value))
                {
                    currentUrlRelative =
                        BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                            BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Home).GetUrl();
                }
                else
                {
                    currentUrlRelative = PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                }
                currentUrlFullyQualified = PageUtil.ConvertRelativeUrlToAbsoluteUrl(currentUrlRelative);
                if(currentUrlRelative.StartsWith("/"))
                {
                    currentUrlRelative = currentUrlRelative.Substring(1);
                }
                //currentUrl = HttpUtility.UrlEncode(currentUrl);
                facebookLoginBtn.Functionality.SuccessUrl = currentUrlFullyQualified;
                facebookLoginBtn.Functionality.ControlRememberMeCheckBox = _chkRememberMe;
                divSocialLoginButtonsContainer.Controls.Add(facebookLoginBtn);

                GoogleLoginButton googleLoginBtn = GoogleLoginButton.LoadControl(this.Page);
                googleLoginBtn.Functionality.SuccessUrl = currentUrlFullyQualified;
                googleLoginBtn.Functionality.ControlRememberMeCheckBox = _chkRememberMe;
                divSocialLoginButtonsContainer.Controls.Add(googleLoginBtn);

                TwitterLoginButton twitterLoginBtn = TwitterLoginButton.LoadControl(this.Page);
                twitterLoginBtn.Functionality.SuccessUrl = currentUrlRelative;
                twitterLoginBtn.Functionality.ControlRememberMeCheckBox = _chkRememberMe;
                divSocialLoginButtonsContainer.Controls.Add(twitterLoginBtn);
            }
            else
            {
                divSocialLoginButtonsContainer.Visible = false;
            }
        }
    }
}