﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.MemberModule;
using BusinessLogic_v3.Modules.MemberModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Events
{
    public partial class EventTutorThumbnailItem : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private EventTutorThumbnailItem _control;
            public FUNCTIONALITY(EventTutorThumbnailItem control)
            {
                _control = control;
            }
            public MemberBaseFrontend Member { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public EventTutorThumbnailItem()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateContent();
        }

        private void updateContent()
        {
            if(this.Functionality.Member != null)
            {
                imgTutor.ImageUrl = this.Functionality.Member.Data.Image.GetSpecificSizeUrl(MemberBase.ImageSizingEnum.Thumbnail);
                anchorTutor.InnerText = this.Functionality.Member.Data.GetFullName();
                anchorTutor.Href = this.Functionality.Member.GetUrl();
                imgTutor.HRef = anchorTutor.Href;
            }
        }

        public static EventTutorThumbnailItem LoadControl(Page pg)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/Events/EventTutorThumbnailItem.ascx") as EventTutorThumbnailItem;
            return ctrl;
        }
    }
}