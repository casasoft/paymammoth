﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HierarchyNavigation.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage.HierarchyNavigation" %>
<%@ Register TagPrefix="Hierarchy" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Hierarchy" Assembly="WebComponentsGeneralV3" %>
<div class="hierarchy-navigation-wrapper"> 
     <Hierarchy:HierarchicalControlNavigation ID="hierarchyLinks" runat="server" />
</div>
