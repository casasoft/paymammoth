﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.CultureDetailsModule;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Languages
{
    using Code.Classes.Javascript.UI.Effects.Toggle;

    public partial class LanguageSelection : BaseUserControl
    {
        public bool ShowJSAnimations { get { return this.Functionality.ShowJSAnimations; } set { this.Functionality.ShowJSAnimations = value; } }

            public class FUNCTIONALITY
        {
            private LanguageSelection _control;
            public bool ShowJSAnimations { get; set; }
            public FUNCTIONALITY(LanguageSelection control)
            {
                _control = control;
                this.ShowJSAnimations = true;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public LanguageSelection()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            initJS();
            base.OnLoad(e);
        }

        private void init()
        {
            var currentCulture = BusinessLogic_v3.Modules.Factories.CultureDetailsFactory.GetCurrentCultureInSession().ToFrontendBase();
            if(currentCulture != null)
            {
                var country = currentCulture.Data.GetCountryISOEnumValue();
                if(country.HasValue)
                {
                    var language = CS.General_v3.Util.EnumUtils.StringValueOf(currentCulture.Data.GetLanguageISOEnumValue());
                    spanCountryName.InnerHtml = language;
                    imgCountryIcon.ImageUrl = CountryUtil.GetFlagPathOfCountry(country.Value);
                    imgCountryIcon.AlternateText = language;
                }

                var availableLanguages = BusinessLogic_v3.Modules.Factories.CultureDetailsFactory.GetAvailableLanguages(true).ToFrontendBaseList();
                if(availableLanguages != null && availableLanguages.Count() > 0)
                {
                    foreach (CultureDetailsBaseFrontend availableLanguage in availableLanguages)
                    {
                        if (availableLanguage.Data.ID != currentCulture.Data.ID)
                        {
                            divAvailableLanguage.Controls.Add(createAvailableLanguageItem(availableLanguage));
                        }
                    }
                }
            }
        }
        private void initJS()
        {
            if (this.Functionality.ShowJSAnimations)
            {
                ToggleControllerParameters jsParams = new ToggleControllerParameters();
                jsParams.elemToggleSelector = "." + divLanguageSelectionContainer.Attributes["class"];
                jsParams.elemSelectorContentOn = "." + divAvailableLanguage.Attributes["class"];
                jsParams.eventType = TOGGLE_EVENT.Hover;
                jsParams.animationType = TOGGLE_ANIMATION_TYPE.SlideLeftRight;
            }
        }

        private MyDiv createAvailableLanguageItem(CultureDetailsBaseFrontend availableLanguage)
        {
            MyDiv divAvailableLanguageItem = new MyDiv();
            divAvailableLanguageItem.CssManager.AddClass("available-language-item");
            MyImage imgCountryFlag = new MyImage();

            var country = availableLanguage.Data.GetCountryISOEnumValue();
            if (country.HasValue)
            {
                imgCountryFlag.ImageUrl = CountryUtil.GetFlagPathOfCountry(country.Value);
                imgCountryFlag.AlternateText = CS.General_v3.Util.EnumUtils.StringValueOf(availableLanguage.Data.GetLanguageISOEnumValue());
                

                divAvailableLanguageItem.Controls.Add(imgCountryFlag);
                imgCountryFlag.HRef = this.Page.Functionality.GetCultureSpecificURLForThisPage(availableLanguage);
            }
            return divAvailableLanguageItem;
        }
    }
}