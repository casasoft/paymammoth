﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Util;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.JQueryFullCalendar.v1;
using CS.WebComponentsGeneralV3.Code.Util;
using CS.General_v3.Util;
using BusinessLogic_v3.Modules;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Calendar
{
    public partial class jQueryCalendar : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private jQueryCalendar _control;
            
            public FUNCTIONALITY(jQueryCalendar control)
            {
                this.Parameters = new FullCalendarParameters();
                _control = control;
            }
          
            public FullCalendarParameters Parameters { get; private set; }

            public void UpdateParameters(FullCalendarParameters parameters)
            {

                this.Parameters = parameters;
            }

            public List<_jQueryFullCalendarOptionsEventObject> Events { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public jQueryCalendar()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.Parameters.divCalendarID = this.divJqueryCalendar.ClientID;
            string jsScript = @"/_common/static/js/jQuery/plugins/fullcalendar/1.5.3/fullcalendar.js";
            string css = @"/_common/static/js/jQuery/plugins/fullcalendar/1.5.3/fullcalendar.css";
            PageUtil.RegisterJavaScriptInHeadOfPage(jsScript);
            PageUtil.RegisterCSSInHeadOfPage(css, 0);
            this.Functionality.Parameters._OutputInsideControl = jsHolder;
            base.OnLoad(e);
        }

        private void initJS()
        {
            
            /*string js = "new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION +
                        ".UI.JQuery.FullCalendar.FullCalendar(" + this.Functionality.Parameters.GetJSON() + ");\r\n";
            JSUtil.AddJSScriptToControl(js, jsHolder);*/
        }

        protected override void OnPreRender(EventArgs e)
        {
            initJS();
            updateCalendar();
            base.OnPreRender(e);
        }

        private void updateCalendar()
        {
            this.Functionality.Parameters.onChangeViewRefreshCufon = ModuleSettings.Generic.IsSiteUsingCufon;
            this.Functionality.Parameters.options.monthNames = DateUtil.GetMultilingualFullMonthNames();
            this.Functionality.Parameters.options.monthNamesShort = DateUtil.GetMultilingualShort3LetterMonthNames();
            this.Functionality.Parameters.options.buttonText = new _jQueryFullCalendarOptions._jQueryFullCalendarOptionsButtonText();
            this.Functionality.Parameters.options.buttonText.today = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Text_Today).ToFrontendBase().GetContent();
            this.Functionality.Parameters.options.buttonText.month = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Text_Month).ToFrontendBase().GetContent();
            this.Functionality.Parameters.options.buttonText.week = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Text_Week).ToFrontendBase().GetContent();
            this.Functionality.Parameters.options.buttonText.day = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Text_Day).ToFrontendBase().GetContent();
            this.Functionality.Parameters.options.dayNames = DateUtil.GetMultilingualFullDayNames();
            this.Functionality.Parameters.options.dayNamesShort = DateUtil.GetMultilingualShort3LetterDayNames();
        }

    }
}