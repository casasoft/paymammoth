﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.BannerModule;
using BusinessLogic_v3.Modules.BannerModule;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Carousel.CarouFredSel.v1;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Carousel.CarouFredSel
{
    public partial class CarouFredSelBannerSlider : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private CarouFredSelBannerSlider _control;
            public FUNCTIONALITY(CarouFredSelBannerSlider control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public CarouFredSelBannerSlider()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadImages();
        }

        private void loadImages()
        {
            IEnumerable<BannerBaseFrontend> listOfBanners = BannerBaseFactory.Instance.GetAllBannersSortedBy(BusinessLogic_v3.Enums.BANNERS_SORT_BY.PriorityAscending).ToFrontendBaseList();

            if (listOfBanners != null && listOfBanners.Count() > 0)
            {
                foreach (var banner in listOfBanners)
                {
                    var bannerUrl = banner.Data.GetBannerURL();
                    if (!String.IsNullOrEmpty(bannerUrl))
                    {
                        MyImage img = new MyImage();

                        img.Title = img.AlternateText = banner.Data.Title;
                        img.ImageUrl = bannerUrl;
                        bannerSlider.Functionality.Controls.Add(img);
                    }
                }
                bannerSlider.Functionality.ItemsVisible = 1;
                bannerSlider.Functionality.Parameters.pagination = new CarouFredSelV1PaginationParameters();
            }
        }
    }
}