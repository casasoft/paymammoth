﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Carousel.CarouFredSel.v1;
using System.Web.UI.HtmlControls;
using CS.WebComponentsGeneralV3.Code.Util;
using CS.General_v3.Classes.Interfaces.ImageItem;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Carousel.CarouFredSel
{
    using CS.General_v3.Util;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public partial class CarouFredSelImageGallery : BaseUserControl
    {


        #region CarouFredSelImageGallery Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {

            public List<IImageItem> Images { get; set; }

            public CarouFredSelControl CarouselFredSel { get { return this._item.carousel; } }
            public CarouFredSelV1Parameters CarouselFredSelParameters { get { return this._item.carousel.Functionality.Parameters; } }

            public string PrettyPhotoImageGalleryID { get; set; }
            public bool ShowPagination { get; set; }
            public bool ShowPaginationThumbnails { get; set; }

            protected CarouFredSelImageGallery _item = null;
            internal FUNCTIONALITY(CarouFredSelImageGallery item)
            {
                this._item = item;
                this.PrettyPhotoImageGalleryID = "carouFredSelImageGallery";
                this.Images = new List<IImageItem>();
                this.ShowPagination = this.ShowPaginationThumbnails = true;
            }
            public void AddImages(IEnumerable<IImageItem> images)
            {
                this.Images.AddRange(images);
            }
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        private void initImages()
        {
            if (this.Functionality.Images != null)
            {
                foreach (var item in this.Functionality.Images)
                {
                    {
                        MyImage img = new MyImage();
                        img.ImageUrl = item.GetNormalImageURL();
                        
                        img.AlternateText = item.GetAlternateText();
                        img.HRef = item.GetLargeImageURL();
                        img.HRefTarget = General_v3.Enums.HREF_TARGET.Blank;
                        if (!string.IsNullOrEmpty(this.Functionality.PrettyPhotoImageGalleryID))
                        {
                            img.Rel = "prettyPhoto[" + this.Functionality.PrettyPhotoImageGalleryID + "]";
                        }

                        carousel.Functionality.Controls.Add(img);
                    }
                }
                carousel.Functionality.ItemsVisible = 1;
                if (this.Functionality.ShowPaginationThumbnails)
                {
                    initThumbnails();
                }
                else if (this.Functionality.ShowPagination)
                {
                    carousel.Functionality.Parameters.pagination = new CarouFredSelV1PaginationParameters();

                }
            }
        }
        private void initThumbnails()
        {
            List<string> thumbnailImages = new List<string>();
            List<string> thumbnailAlts = new List<string>();
            foreach (var image in this.Functionality.Images)
            {
                thumbnailImages.Add(image.GetThumbnailImageURL());
                thumbnailAlts.Add(image.GetAlternateText());
            } 
            
            string varName = "thumbnailImages";
            //Register thumbnails
            string jsArray = varName + " = " + JSONUtil.Serialize(thumbnailImages) + ";";
            JSUtil.AddJSScriptToPage(jsArray);


            carousel.Functionality.Parameters.pagination = new CarouFredSelV1PaginationParameters()
            {
                anchorBuilder = @"function(nr, item) {
                                        var thumbnailImages = " + JSONUtil.Serialize(thumbnailImages) + @";
                                        var altTexts = " + JSONUtil.Serialize(thumbnailAlts) + @";
                                        return '<div class=""carousel-thumbnail-image""><a href=""#'+nr+'""><img alt=""'+altTexts[nr-1]+'"" src=""'+thumbnailImages[nr-1]+'"" /></a></div>'; 
                                    }"
            };
        }
        protected override void OnInit(EventArgs e)
        {
            CS.General_v3.Util.PageUtil.GetCurrentPage().Load += new EventHandler(CarouFredSelImageGallery_Load);
            base.OnInit(e);
        }

        void CarouFredSelImageGallery_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Functionality.PrettyPhotoImageGalleryID))
            {
                CS.WebComponentsGeneralV3.Code.Util.PrettyPhotoUtil.InitJS();
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            initImages();
            
            base.OnLoad(e);
        }
        public override string ClientID
        {
            get
            {
                return this.carousel.ClientID;
            }
        }
			
		
    }
}