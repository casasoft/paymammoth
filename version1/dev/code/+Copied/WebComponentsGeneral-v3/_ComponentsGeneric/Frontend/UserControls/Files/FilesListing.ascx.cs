﻿using System;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Frontend.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Classes.URL;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Files
{
    public partial class FilesListing : BaseUserControl
    {
        private MemberBaseFrontend _member;
        private URLParserGeneralListing _urlParser;
        private ArticleBaseFrontend _article;

        public class FUNCTIONALITY
        {
            private FilesListing _control;
            public FUNCTIONALITY(FilesListing control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public FilesListing()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadArticle();
            loadMember();
            urlParser();
            loadFiles();
        }

        private void loadFiles()
        {
            int totalResults = 0;
            var files = BusinessLogic_v3.Modules.Factories.FileItemFactory.GetFiles(_urlParser.PageNo.GetValue(), _urlParser.ShowAmt.GetValue(), Enums.FILE_ITEM_SORT_BY.DateDesc, out totalResults).ToFrontendBaseList();
            if(totalResults == 0)
            {
                MyContentText.AttachContentTextWithControl(divHtmlText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Members_FileListing_NoItems).ToFrontendBase());
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divFileListing);
            }
            else
            {
                MyContentText.AttachContentTextWithControl(divHtmlText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Members_FileListing_Items).ToFrontendBase());
                fileListingItems.Functionality.Files = files;
                pagingBarBottom.Functionality.UpdateValues(_urlParser, totalResults, true);
            }
        }

        private void loadArticle()
        {
            _article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.Members_FilesListing).ToFrontendBase();
            contentPage.Functionality.ContentPage = _article;
        }

        private void urlParser()
        {
            _urlParser = new URLParserGeneralListing(_article);
        }

        private void loadMember()
        {
            _member = BusinessLogic_v3.Modules.Factories.MemberFactory.GetLoggedInUserForCurrentSession().ToFrontendBase();
            if(_member != null)
            {
                var dict = new BusinessLogic_v3.Classes.Text.TokenReplacerNew();
                dict.Add(BusinessLogic_v3.Constants.Tokens.USER_FIRST_NAME, ((IMemberAddressDetails)_member.Data).FirstName);
                contentPage.Functionality.ContentTagReplacementValues = dict;
            }
        }
    }
}