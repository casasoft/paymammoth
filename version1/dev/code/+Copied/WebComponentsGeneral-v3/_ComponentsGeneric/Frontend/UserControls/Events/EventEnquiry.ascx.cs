﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Events
{
    public partial class EventEnquiry : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private EventEnquiry _control;
            public FUNCTIONALITY(EventEnquiry control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public EventEnquiry()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
        }

        private void updateTexts()
        {
            this.eventEnquiry.Functionality.TextEnquiryFormTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_Enquiry_Title).ToFrontendBase();
            this.eventEnquiry.Functionality.EnquiryDescription = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_Enquiry_Description).ToFrontendBase();
            this.eventEnquiry.Functionality.EnquiryFormSubject = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Event_Enquiry_Subject);
        }
    }
}