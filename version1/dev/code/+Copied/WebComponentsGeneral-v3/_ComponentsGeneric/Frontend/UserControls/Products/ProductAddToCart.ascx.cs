﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ProductVariationModule;
using BusinessLogic_v3.Frontend.ShoppingCartModule;
using BusinessLogic_v3.Modules.ShoppingCartModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using BusinessLogic_v3.Frontend.ProductModule;
using BusinessLogic_v3.Frontend.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;
using BusinessLogic_v3.Constants;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    using Code.Classes.Javascript.UI.Products;

    public partial class ProductAddToCart : BaseUserControl
    {
        public delegate bool ProductAddToCartSpecificSubmitHandler(ProductAddToCart sender, ProductAddToCartData data);
        public delegate void ProductAddToCartSpecificFieldsHandler(ProductAddToCart sender, FormFieldsItemGroupData formFieldsGroup);

        
        private const string ADD_TO_CART_FIELDS = "addToCartFields";
        private MyTxtBoxNumber _txtQuantity;
        private MyDropDownList _dlMaterial;
        private MyButton _btnAddToCart;

        public class ProductAddToCartData
        {
            public int Quantity { get; set; }

            public ProductVariationBaseFrontend ProductVariation { get; set; }
        }

        public class FUNCTIONALITY
        {
            private ProductAddToCart _control;
            public FUNCTIONALITY(ProductAddToCart control)
            {
                _control = control;
            }
            public ProductVariationControllerParameters ProductVariationControllerParameters { get; set; }
            public event ProductAddToCartSpecificSubmitHandler OnSpecificSubmit;
            public event ProductAddToCartSpecificFieldsHandler OnSpecificFields;

            public ProductBaseFrontend Product { get; set; }
            public ContentTextBaseFrontend LblQuantity { get; set; }
            public ContentTextBaseFrontend BtnAddToCart { get; set; }
            public ContentTextBaseFrontend LblMaterial { get; set; }

            internal bool triggerOnProductAddToCartSpecificSubmitHandler(ProductAddToCartData data)
            {
                if (this.OnSpecificSubmit != null)
                {
                    return this.OnSpecificSubmit(_control, data);
                }
                return false;
            }


            internal void triggerOnProductAddToCartSpecificFieldsHandler(FormFieldsItemGroupData group)
            {
                if (this.OnSpecificFields != null)
                {
                    this.OnSpecificFields(_control, group);
                }
            }
            
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ProductAddToCart()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
           

            if (this.Functionality.Product != null )
            {
                updateTexts();
                initFormFields();
            }
            else
            {
                if (CS.General_v3.Util.Other.IsLocalTestingMachine)
                {
                    throw new InvalidOperationException("Product and cart are required");
                }
            }
        }

        private ShoppingCartBaseFrontend getCart(bool createIfNotExists)
        {
            var _cart = BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartManager.Instance.GetCurrentCartForContext(createIfNotExists: createIfNotExists).ToFrontendBase();
            return _cart;
            
        }

        private void initFormFields()
        {
            FormFieldsItemGroupData group = formAddToCart.Functionality.AddGroup(new CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields.FormFieldsItemGroupData()
            {
                Identifier = "PRODUCT_ADD_TO_CART",
                TitleContentText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_AddToCart_Title).ToFrontendBase(),
                ValidationGroup = ADD_TO_CART_FIELDS
            });

            
            formAddToCart.Functionality.LayoutProvider.DoNotShowValidationIcons = true;
            //var cart = getCart(createIfNotExists: false);


            var productPrice = ProductAddToCartPrice.LoadControl(this.Page);
            productPrice.Functionality.Product = this.Functionality.Product;

            FormFieldsItemDataImpl s = formAddToCart.Functionality.AddGenericItem(new FormFieldsItemDataImpl()
            {
                TitleContentText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_AddToCart_Price).ToFrontendBase(),
                Control = productPrice,
                CssClass="product-add-to-cart-price-row"
            }, group.Identifier);

            _txtQuantity = (MyTxtBoxNumber)group.AddFormWebControl(new MyTxtBoxNumber()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    titleContentText = this.Functionality.LblQuantity,
                    required = true,
                    id = "productAddToCart_txtQuantity"
                }, InitialValue = 1
            });
            _txtQuantity.FieldParameters.doNotValidateOnBlur = true;
            ValidatorNumeric vnNumeric = new ValidatorNumeric();
            vnNumeric.Parameters.positiveOnly = true;
            _txtQuantity.FieldParameters.validators.Add(vnNumeric);

            IEnumerable<ProductVariationBaseFrontend> productVariations = this.Functionality.Product.Data.ProductVariations.ToFrontendBaseList();
            if (productVariations != null && productVariations.Count() > 0)
            {
                _dlMaterial = (MyDropDownList)group.AddFormWebControl(new MyDropDownList()
                {
                    FieldParameters = new FieldSingleControlParameters() { titleContentText = this.Functionality.LblMaterial, id = "productAddToCart_material" }
                });
                string cssClass = "product-add-to-cart-varation-dropdown";
                _dlMaterial.CssManager.AddClass(cssClass);
                if (this.Functionality.ProductVariationControllerParameters != null)
                {
                    this.Functionality.ProductVariationControllerParameters.selectorCmb = "." + cssClass;
                }

                {
                    var variationList = this.Functionality.Product.Data.ProductVariations.ToList();
                    var variationListFrontend =variationList.SortProductVariationsByColorTitle().ToFrontendBaseList();

                    foreach (var productVariation in variationListFrontend)
                    {
                        string text = productVariation.Data.Colour != null ? productVariation.Data.Colour.Title : null;
                        if (string.IsNullOrEmpty(text))
                        {
                            var noColoursCntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_Description_NoAvailableColours);
                            text = noColoursCntTxt.GetContent();
                        }
                        string dlVal = productVariation.Data.ID.ToString();

                        _dlMaterial.Functionality.AddItemFromListItem(new ListItem() {Text = text, Value = dlVal});
                    }
                }
            }

            this.Functionality.triggerOnProductAddToCartSpecificFieldsHandler(group);
            _btnAddToCart = formAddToCart.Functionality.AddButton(this.Functionality.BtnAddToCart.GetContent(), group.Identifier);
            _btnAddToCart.Click += new EventHandler(_btnAddToCart_Click);
        }

        void _btnAddToCart_Click(object sender, EventArgs e)
        {
            ProductAddToCartData data = new ProductAddToCartData();
            var quantity = _txtQuantity != null ? _txtQuantity.GetFormValueAsIntNullable() : null;
            if (quantity.HasValue)
            {
                data.Quantity = quantity.Value;
            }

            var productVariationId = _dlMaterial.GetFormValueAsLongNullable();
            if (productVariationId.HasValue)
            {
                data.ProductVariation = BusinessLogic_v3.Modules.Factories.ProductVariationFactory.GetByPrimaryKey(productVariationId.Value).ToFrontendBase();
            }

            if(!this.Functionality.triggerOnProductAddToCartSpecificSubmitHandler(data))
            {
                if(data.Quantity != 0)
                {
                    addItemToCart(data);

                }
                else
                {
                    this.Page.Functionality.ShowErrorMessage(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Products_AddToCart_QuantityMustBeGreaterThanZero).ToFrontendBase());
                }
            }

        }

        private void addItemToCart(ProductAddToCartData data)
        {
            //var cart = _member.Data.GetShoppingCart(createIfNotExists: true);
            var cart = getCart(createIfNotExists: true);
            ShoppingCartResultHelperBase s = cart.Data.AddItemToShoppingCart(data.ProductVariation.Data, data.Quantity);
            Dictionary<string, string> dict = new Dictionary<string, string>();
            int requestedQuantity = s.RequestedQuantity;
            int availableQuantity = s.AvailableQuantity;
            dict.Add(Tokens.ITEM_TEXT,
            requestedQuantity == 1
                ? BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Item).GetContent()
                 : BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Items).GetContent());
            switch(s.UpdateCartItemResult)
            {
                case Enums.SHOPPING_CART_ITEM_UPDATE_QTY_RESULT.OK:
                    dict.Add(Tokens.PRODUCT_REQUESTED_QUANTITY, requestedQuantity.ToString());
                    this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ProductPage_AddToCart_Success).ToFrontendBase());
                    break;
                case Enums.SHOPPING_CART_ITEM_UPDATE_QTY_RESULT.NotEnoughItemsAvailableInStock:
                    if (availableQuantity != 0)
                    {
                        dict.Add(Tokens.PRODUCT_AVAILABLE_QUANTITY, availableQuantity.ToString());
                        this.Page.Functionality.ShowErrorMessageAndRedirectToPage(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ProductPage_AddToCart_NotEnoughQuantity_1).ToFrontendBase());
                    }
                    else
                    {
                        this.Page.Functionality.ShowErrorMessageAndRedirectToPage(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ProductPage_AddToCart_NotEnoughQuantity_2).ToFrontendBase());
                    }
                    break;
            }
        }

        private void updateTexts()
        {
            this.Functionality.LblQuantity = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_AddToCart_Quantity).ToFrontendBase();
            this.Functionality.LblMaterial = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_AddToCart_Material).ToFrontendBase();
            this.Functionality.BtnAddToCart = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_AddToCart_ButtonText).ToFrontendBase();
        }
    }
}