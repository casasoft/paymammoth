﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ArticleCommentModule;
using CS.General_v3.Classes.Interfaces.BlogPosts;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Comments
{
    public partial class ViewComments : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private ViewComments _control;
            public FUNCTIONALITY(ViewComments control)
            {
                _control = control;
            }
            public IEnumerable<ICommentableItemComment> Comments { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ViewComments()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initComments();
        }

        private void initComments()
        {
            var comments = Functionality.Comments;
            if (comments != null && comments.Count() > 0)
            {
                int count = 0;
                foreach (ICommentableItemComment comment in comments)
                {
                    count++;
                    var _ctrl = CommentItem.LoadControl(this.Page, "ViewComment_" + count);
                    if (_ctrl != null)
                    {
                        _ctrl.Functionality.Comment = comment;
                        divComments.Controls.Add(_ctrl);
                    }
                }
            }
        }
    }
}