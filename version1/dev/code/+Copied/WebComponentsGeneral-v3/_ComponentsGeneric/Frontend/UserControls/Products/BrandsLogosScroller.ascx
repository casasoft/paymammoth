﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BrandsLogosScroller.ascx.cs"
 Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.BrandsLogosScroller" %>
 <div class="brands-logos-scroller-container">
 <ul runat="server" id="ulBrandsLogos" class="brands-logos-list clearfix">
</ul>
<div class="brands-logos-scroller-left-mask"></div>
<div class="brands-logos-scroller-right-mask"></div>
</div>
