﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage
{
    using BusinessLogic_v3.Extensions;
    using BusinessLogic_v3.Frontend.ArticleModule;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using Code.Controls.UserControls;
    using Code.Controls.WebControls.Common;

    public partial class ContentPageFooter : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private ContentPageFooter _control;
            public FUNCTIONALITY(ContentPageFooter control)
            {
                _control = control;
            }
            public IContentPageProperties ContentPage { get; set; }
            public ContentTextBaseFrontend LastUpdatedText { get; set; }
            public bool? ShowFooter { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ContentPageFooter()
        {
            this.Functionality = createFunctionality();
            this.Functionality.LastUpdatedText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_LastEdited).ToFrontendBase();

        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if (initFooter())
            {
                initAddThis();
                initLastEditedOn();
            }
        }

        private bool initFooter()
        {
            bool result = false;
            bool showFooter = !BusinessLogic_v3.Modules.ModuleSettings.Articles.DoNotShowFooter;

            if (showFooter)
            {
                if (this.Functionality.ContentPage != null)
                {
                    if (this.Functionality.ContentPage.DoNotShowFooter.GetValueOrDefault(false))
                    {
                        showFooter = false;
                    }
                }
                if (showFooter && !this.Functionality.ShowFooter.GetValueOrDefault(true))
                {
                    showFooter = false;
                }
            }



            divFooterWrapper.Visible = showFooter;
            return showFooter;

        }


        private void initAddThis()
        {

            if (Functionality.ContentPage != null &&  Functionality.ContentPage.DoNotShowAddThis.GetValueOrDefault())
            {
                divAddThisWrapper.Parent.Controls.Remove(divAddThisWrapper);
            }
            else
            {
                divAddThisWrapper.Parent.Controls.Add(divAddThisWrapper);
                AddThisControl.Functionality.ContentPage = Functionality.ContentPage;
            }
        }

        private void initLastEditedOn()
        {
            bool show = Functionality.ContentPage != null && !Functionality.ContentPage.DoNotShowLastEditedOn.GetValueOrDefault();
            if (show)
            {
                var replacementTags = this.Functionality.LastUpdatedText.CreateTokenReplacer();
                replacementTags[BusinessLogic_v3.Constants.Tokens.DATE] = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatDateTimeWithCurrentCulture(Functionality.ContentPage.Data.LastEditedOn.GetValueOrDefault(), General_v3.Util.Date.DATETIME_FORMAT.LongDate);
                MyContentText.AttachContentTextWithControl(spanLastEditedOn, this.Functionality.LastUpdatedText, replacementTags);
            }
            else
            {
                spanLastEditedOn.Parent.Controls.Remove(spanLastEditedOn);
            }
        }
    }
}