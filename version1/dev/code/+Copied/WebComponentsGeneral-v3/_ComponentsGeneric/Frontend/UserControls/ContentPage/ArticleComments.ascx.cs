﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage
{
    using BusinessLogic_v3.Classes.Text;
    using BusinessLogic_v3.Extensions;
    using BusinessLogic_v3.Frontend.ArticleModule;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using BusinessLogic_v3.Modules.ArticleCommentModule;
    using Code.Controls.WebControls.Common;
    using General_v3.Classes.Interfaces.BlogPosts;

    public partial class ArticleComments : BaseUserControl
    {


        #region ArticleComments Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected ArticleComments _item = null;

            public ArticleBaseFrontend Article { get; set; }
            internal FUNCTIONALITY(ArticleComments item)
            {
                this._item = item;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			
        private void initComments(ArticleBaseFrontend article)
        {
            if (article.IsCommentable())
            {
                divArticleCommentsWrapper.Visible = true;
                submitComment.Functionality.CommentableItem = (ICommentableItem)article.Data;
                IEnumerable<IArticleCommentBase> comments = article.Data.GetComments();
                ContentTextBaseFrontend cntTxt;
                TokenReplacerNew dctReplacer = null;

                if (comments != null && comments.Count() > 0)
                {
                    int totalCommentsAndReplies = article.Data.GetCommentsAndRepliesCount();
                    viewComments.Functionality.Comments = (IEnumerable<ICommentableItemComment>)comments;

                    cntTxt = totalCommentsAndReplies == 1
                                 ? BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_ViewComments_SingularCount).ToFrontendBase()
                                 : BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_ViewComments_PluralCount).ToFrontendBase();

                    dctReplacer = cntTxt.CreateTokenReplacer();
                    dctReplacer.Add(BusinessLogic_v3.Constants.Tokens.COMMENTS_COUNT, totalCommentsAndReplies.ToString());
                }
                else
                {
                    //hrSeparator.Visible = false;
                    cntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_ViewComments_NoComments).ToFrontendBase();

                }
                MyContentText.AttachContentTextWithControl(h2CommentCount, cntTxt, dctReplacer);
            }
            else
            {
                divArticleCommentsWrapper.Visible = false;
            }
        }
        
        protected override void OnLoad(EventArgs e)
        {
            if (this.Functionality.Article != null)
            {
                initComments(this.Functionality.Article);
            }
            else
            {
                throw new Exception("Article cannot be empty");
            }
            base.OnLoad(e);
        }
    }
}