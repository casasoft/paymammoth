﻿using BusinessLogic_v3.Frontend.ContentTextModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BusinessLogic_v3.Classes.Interfaces;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using CS.General_v3;
    using System.Web.UI;
    using CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products;

    public partial class GenericListingItems : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private GenericListingItems _control;
            public FUNCTIONALITY(GenericListingItems control)
            {
                _control = control;
            }
            public ContentTextBaseFrontend ViewMoreText { get; set; }
            public IEnumerable<IListingItemData> Items { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public GenericListingItems()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initItems();
        }

        private void initItems()
        {
            var items = this.Functionality.Items;
            if(items !=null && items.Count() > 0)
            {
                var maximumItems = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(Enums.SETTINGS_ENUM.General_Listing_MaximumItems);
                var listingType = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<Enums.LISTING_CONTROl_TYPE>(Enums.SETTINGS_ENUM.General_Listing_ListingControlType);

                int count = 0;
                foreach (IListingItemData item in items)
                {
                    count++;
                    switch(listingType)
                    {
                        case Enums.LISTING_CONTROl_TYPE.GenericListingItem:
                            var _genericListingCtrl = GenericListingItem.LoadControl(this.Page, item.ID.ToString());
                            if (_genericListingCtrl != null)
                            {
                                _genericListingCtrl.Functionality.Item = item;
                                if (count == items.Count())
                                {
                                    _genericListingCtrl.Functionality.DoNotShowSeparator = true;
                                }
                                _genericListingCtrl.Functionality.ViewMoreText = this.Functionality.ViewMoreText;
                                divItemsListing.Controls.Add(_genericListingCtrl);
                            }
                            break;
                        case Enums.LISTING_CONTROl_TYPE.GenericListingFloatedItem:

                            var _genericListingFloatedCtrl = GenericListingFloatedItem.LoadControl(this.Page, item.ID.ToString());
                            if (_genericListingFloatedCtrl != null)
                            {
                                if (count % maximumItems == 0)
                                {
                                    _genericListingFloatedCtrl.Functionality.IsLastItem = true;
                                }
                                _genericListingFloatedCtrl.Functionality.Item = item;
                                divItemsListing.Controls.Add(_genericListingFloatedCtrl);
                            }
                            break;
                    }
                }
            }
        }
    }
}