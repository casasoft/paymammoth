﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.OrderModule;
using System.Web.UI.HtmlControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Frontend.OrderItemModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Orders.Invoice
{
    public partial class GenericTabularDataInvoice : BaseUserControl
    {
        public class GenericTabularDataInvoiceRowData
        {
            public string Qty { get; set; }
            public string Description { get; set; } 
            public string UnitPriceHtml { get; set; }
            public string TotalPriceHtml { get; set; }
            public string Tax { get; set; }
        }


        public class FUNCTIONALITY
        {
            private GenericTabularDataInvoice _control;


            public ContentTextBaseFrontend QtyLabel { get; set; }
            public ContentTextBaseFrontend DescriptionLabel { get; set; }
            public ContentTextBaseFrontend UnitPriceLabel { get; set; }
            public ContentTextBaseFrontend TotalPriceLabel { get; set; }
            public ContentTextBaseFrontend TaxLabel { get; set; }

            public bool ShowQty { get; set; }
            public bool ShowUnitPrice { get; set; }
            public bool ShowTax { get; set; }

            public List<GenericTabularDataInvoiceRowData> Data { get; set; }




            public FUNCTIONALITY(GenericTabularDataInvoice control)
            {
                _control = control;
                //this.QtyLabel = "Qty.";
                //this.DescriptionLabel = "Description";
                //this.UnitPriceLabel = "Unit Price";
                //this.TotalPriceLabel = "Total";
                //this.TaxLabel = "VAT %";

                this.ShowQty = true;
                this.ShowUnitPrice = true;
                this.ShowTax = true;
            }
            private string getOrderItemUnitPrice(OrderItemBaseFrontend item, CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 currCode)
            {
                return null;
            }
            /*private void getPricesForItem(OrderItemBaseFrontend item, out double unitPriceWithTaxes, out double? unitDiscountPriceWithTaxes, out double totalPriceWithTaxes, out double? totalDiscountedPriceWithTaxes)
            {
                unitPriceWithTaxes = item.Data.GetRetailUnitPrice(false);
                totalPriceWithTaxes = item.Data.GetTotalPrice(false);
                if (item.Data.HasDiscount())
                {
                    unitDiscountPriceWithTaxes = item.Data.GetRetailUnitPrice(true);
                    totalPriceWithTaxes = 
                }
            }*/
            private string getDiscountedPriceHTML(double price, double oldPrice, string currCode, string cssSuffix)
            {
                string html = "";
                MySpan spanPrice = new MySpan();
                spanPrice.CssManager.AddClass("invoice-item-"+cssSuffix+"-price-");
                spanPrice.InnerHtml = CS.General_v3.Util.NumberUtil.FormatCurrency(price, currencySymbol: currCode);

                if (oldPrice != 0 && oldPrice != price)
                {
                    MySpan spanOldPrice = new MySpan();
                    spanOldPrice.CssManager.AddClass("invoice-item-" + cssSuffix + "-old-price");
                    spanOldPrice.InnerHtml = CS.General_v3.Util.NumberUtil.FormatCurrency(oldPrice, currencySymbol: currCode);
                    spanPrice.CssManager.AddClass("invoice-item-" + cssSuffix + "-discount-price");

                    html += CS.General_v3.Util.ControlUtil.RenderControl(spanOldPrice) + " ";
                }
                html += CS.General_v3.Util.ControlUtil.RenderControl(spanPrice);
                return html;
            }
            private string getUnitPriceHTML(OrderItemBaseFrontend item, string currCode)
            {
                return getDiscountedPriceHTML(item.Data.GetRetailUnitPrice(true), item.Data.GetRetailUnitPrice(false), currCode, "unit");
                
            }
            private string getTotalPriceHTML(OrderItemBaseFrontend item, string currCode)
            {
                return getDiscountedPriceHTML(item.Data.GetTotalPrice(true), item.Data.GetTotalPrice(reduceDiscount:false), currCode, "total");
                
            }
            public void FillFromOrder(OrderBaseFrontend order)
            {
                _control.invoice.Functionality.FillValuesFromOrder(order);
                this.Data = new List<GenericTabularDataInvoiceRowData>();
                var orderItems = order.Data.OrderItems.ToFrontendBaseList();

                string currCode = null;
                
                var orderCurrency = order.Data.OrderCurrency;
                if (orderCurrency != null)
                {
                    _control.invoice.Functionality.TaxesAmounts.CurrencySymbol = orderCurrency.GetCurrencyISOAsEnumValue();
                }
                currCode = CS.General_v3.Enums.ISO_ENUMS.GetCurrencySymbol(_control.invoice.Functionality.TaxesAmounts.CurrencySymbol, true);

                double subTotal = 0;
                double total = 0;
                double discount = 0;

                bool showDiscount = false;
                foreach (var orderItem in orderItems)
                {
                    GenericTabularDataInvoiceRowData rowData = new GenericTabularDataInvoiceRowData();
                    this.Data.Add(rowData);
                    rowData.Qty = orderItem.Data.Quantity.ToString();
                    string title = orderItem.Data.GetTitle();
                    rowData.Description = (!string.IsNullOrWhiteSpace(title) ? title + " " : null) + (!string.IsNullOrWhiteSpace(orderItem.Data.Description) && !string.IsNullOrWhiteSpace(title) ? "- " : null) + orderItem.Data.Description;
                    
                    double totalPrice = orderItem.Data.GetTotalPrice(reduceDiscount:true);
                    rowData.UnitPriceHtml = getUnitPriceHTML(orderItem, currCode);
                    rowData.TotalPriceHtml = getTotalPriceHTML(orderItem, currCode);
                    double? taxAmt = null;
                    _control.invoice.Functionality.TaxesAmounts.AddTax(orderItem);

                    double priceExcTaxPerUnit = orderItem.Data.GetPriceExcTaxPerUnit();
                    double taxAmountPerUnit = orderItem.Data.GetTaxAmountPerUnit();
                    double discountPerUnit = orderItem.Data.GetDiscountPerUnit();

                    taxAmt = orderItem.Data.TaxRate;
                    if (discountPerUnit != 0)
                    {
                        discount += (orderItem.Data.Quantity * discountPerUnit);
                        showDiscount = true;
                    }

                    //subTotal += (priceExcTaxPerUnit > 0) ? priceExcTaxPerUnit * orderItem.Data.Quantity : totalPrice; // add subtotal
                    subTotal += orderItem.Data.GetTotalPriceExcludingTaxes(reduceDiscount: true);
                    total += orderItem.Data.GetTotalPrice(reduceDiscount: true);
                    discount += orderItem.Data.GetTotalDiscount();

                    rowData.Tax = taxAmt.Value + "%";
                    double totalTax = orderItem.Data.Quantity * taxAmountPerUnit;
                        

                }

                double? shippingAmt = order.Data.TotalShippingCost;
                double shippingCostVal = 0;
                if (shippingAmt.HasValue && shippingAmt.Value > 0)
                {
                    shippingCostVal = shippingAmt.Value;
                    _control.invoice.Functionality.ShippingValue = CS.General_v3.Util.NumberUtil.FormatCurrency(shippingAmt.Value, currencySymbol: currCode);
                    _control.invoice.Functionality.ShowShipping = true;
                }

                _control.invoice.Functionality.ShowDiscount = showDiscount;

                //subTotal = order.Data.GetTotalPrice(includeTax: false, includeShipping: false, reduceDiscount: false);
                subTotal = order.Data.GetTotalPrice(includeTax: true, includeShipping: false, reduceDiscount: false);
                
                _control.invoice.Functionality.SubtotalValue = CS.General_v3.Util.NumberUtil.FormatCurrency(subTotal, currencySymbol: currCode);

                double totalTaxValue = 0;
                totalTaxValue = order.Data.GetTotalTaxAmount();
                //taxes
                //foreach (int taxAmt in taxesTotal.Keys)
                if (totalTaxValue > 0)
                {
                    double taxValue = totalTaxValue;// taxesTotal[taxAmt];
                    totalTaxValue += taxValue;
                    var vatCntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_OrderHistory_Invoice_VAT).ToFrontendBase();
                    
                }
                if(discount >= 0)
                {
                    discount = discount*-1;
                }
               // total = subTotal + discount + totalTaxValue + shippingCostVal;
                discount = order.Data.GetTotalDiscount() * -1;
                _control.invoice.Functionality.DiscountValue = CS.General_v3.Util.NumberUtil.FormatCurrency(discount, currencySymbol: currCode);
                total = order.Data.GetTotalPrice();
                _control.invoice.Functionality.TotalValue = CS.General_v3.Util.NumberUtil.FormatCurrency(total, currencySymbol: currCode);


            }

        }
        public FUNCTIONALITY Functionality { get; private set; }
        public GenericTabularDataInvoice()
        {
            this.Functionality = createFunctionality();
        }
        private void createTableCell(HtmlTableRow tr, string cssClass, string value)
        {
            HtmlTableCell td = new HtmlTableCell();
            td.Attributes["class"] = cssClass;
            td.InnerHtml = value;
            tr.Controls.Add(td);
        }

        private void fillInTabularData()
        {
            MyContentText.AttachContentTextWithControl(tdQtyHeaderCell, this.Functionality.QtyLabel);
            MyContentText.AttachContentTextWithControl(tdDescHeaderCell, this.Functionality.DescriptionLabel);
            MyContentText.AttachContentTextWithControl(tdTotalPriceHeaderCell, this.Functionality.TotalPriceLabel);
            MyContentText.AttachContentTextWithControl(tdUnitPriceHeaderCell, this.Functionality.UnitPriceLabel);
            MyContentText.AttachContentTextWithControl(tdTaxHeaderCell, this.Functionality.TaxLabel);

            tItemsBody.Controls.Clear();

            tdQtyHeaderCell.Visible = this.Functionality.ShowQty;
            tdTaxHeaderCell.Visible = this.Functionality.ShowTax;
            tdUnitPriceHeaderCell.Visible = this.Functionality.ShowUnitPrice;

            if (this.Functionality.Data != null)
            {
                for (int i = 0; i < this.Functionality.Data.Count; i++)
                {
                    var dataItem = this.Functionality.Data[i];
                    HtmlTableRow tr = new HtmlTableRow();
                    tItemsBody.Controls.Add(tr);

                    if (this.Functionality.ShowQty)
                    {
                        createTableCell(tr, "invoice-item-qty-cell", dataItem.Qty);
                    }

                    createTableCell(tr, "invoice-item-desc-cell", dataItem.Description);

                    if (this.Functionality.ShowUnitPrice)
                    {



                        createTableCell(tr, "invoice-item-price-cell", dataItem.UnitPriceHtml);
                    }

                    createTableCell(tr, "invoice-item-total-cell", dataItem.TotalPriceHtml);

                    if (this.Functionality.ShowTax)
                    {
                        createTableCell(tr, "invoice-item-tax-cell", dataItem.Tax);
                    }

                }
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }


        protected override void OnLoad(EventArgs e)
        {
            init();
            invoice.PreRender += new EventHandler(invoice_PreRender);
            //invoice.Load += new EventHandler(invoice_PreRender);
            //fillInTabularData();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
            updateValues();
        }

        private void updateValues()
        {
            this.Functionality.ShowQty =  BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(CS.General_v3.Enums.SETTINGS_ENUM.MembersArea_Invoice_ShowQuantity);
            this.Functionality.ShowTax =  BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(CS.General_v3.Enums.SETTINGS_ENUM.MembersArea_Invoice_ShowTax);
            this.Functionality.ShowUnitPrice =  BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(CS.General_v3.Enums.SETTINGS_ENUM.MembersArea_Invoice_ShowUnitPrice);
        }

        private void updateTexts()
        {
            //UPDATE TEXTS FROM CONTENT TEXT HERE
            this.Functionality.QtyLabel =
                BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    Enums.CONTENT_TEXT.MembersArea_Invoice_Quantity).ToFrontendBase();

            this.Functionality.DescriptionLabel =
                BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    Enums.CONTENT_TEXT.MembersArea_Invoice_DescriptionText).ToFrontendBase();

            this.Functionality.UnitPriceLabel =
                BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    Enums.CONTENT_TEXT.MembersArea_Invoice_UnitPriceText).ToFrontendBase();

            this.Functionality.TotalPriceLabel =
                BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    Enums.CONTENT_TEXT.MembersArea_Invoice_TotalPriceText).ToFrontendBase();

            this.Functionality.TaxLabel =
                BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    Enums.CONTENT_TEXT.MembersArea_Invoice_TaxText).ToFrontendBase();

        }


        protected override void OnPreRender(EventArgs e)
        {
            //fillInTabularData();
            base.OnPreRender(e);
        }

        void invoice_PreRender(object sender, EventArgs e)
        {
            fillInTabularData();
        }
    }
}