﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LatestNewsItem.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.News.LatestNewsItem" %>
<li runat="server" id="liItem"><a href="/" runat="server" id="anchorNews" class="latest-news-item clearfix">
    <span runat="server" id="spanNewsTitle" class="latest-news-item-title"></span>
    <div runat="server" id="spanNewsDate" class="latest-news-item-date-container clearfix">
        <div class="clearfix">
            <span runat="server" id="spanDay" class="latest-news-item-date-day"></span><span
                runat="server" id="spanMonth" class="latest-news-item-date-month"></span>
        </div>
        <span runat="server" id="spanYear" class="latest-news-item-date-year"></span>
    </div>
</a><span class="latest-news-item-separator"></span></li>
