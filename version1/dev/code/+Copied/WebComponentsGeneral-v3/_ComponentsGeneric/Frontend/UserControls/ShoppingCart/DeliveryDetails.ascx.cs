﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3;
using BusinessLogic_v3.Frontend.MemberModule;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using BusinessLogic_v3.Frontend.ArticleModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart
{
    public partial class DeliveryDetails : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private DeliveryDetails _control;
            
            public FUNCTIONALITY(DeliveryDetails control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public DeliveryDetails()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadContentPage();
        }

        private void loadContentPage()
        {
            ArticleBaseFrontend article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.ShoppingCart_DeliveryDetails).ToFrontendBase();
            if (article != null)
            {
                contentPage.Functionality.ContentPage = article;
            }
        }
    }
}