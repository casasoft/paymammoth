﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Classes.Interfaces;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    using BusinessLogic_v3.Classes.Interfaces;
    using BusinessLogic_v3.Extensions;
    using Code.Controls.WebControls.Common;

    public partial class GenericListingFloatedItem : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private GenericListingFloatedItem _control;
            public FUNCTIONALITY(GenericListingFloatedItem control)
            {
                _control = control;
            }
            private IListingItemData _item;
            public IListingItemData Item
            {
                get { return _item; }
                set
                {
                    _item = value;
                    /*if (value != null && value.DbObject != null)
                    {
                        _control.CacheParams.SetCacheDependencyFromDBObjects(_item.DbObject, true);
                    }*/
                }
            }
            private bool _isLastItem;
            public bool IsLastItem
            {
                get { return _isLastItem; }
                set
                {
                    _isLastItem = value;
                    /*if (value)
                    {
                        _control.CacheParams.CacheKeySuffices.Add("last-item");
                    }*/
                }
            }
        }
            
        public FUNCTIONALITY Functionality { get; private set; }
        public GenericListingFloatedItem()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateContent();
        }

        private void updateContent()
        {
            if(this.Functionality.Item != null)
            {
                if(this.Functionality.IsLastItem)
                {
                    divListingItem.Attributes["class"] += " last-item";
                }
                
                if (!string.IsNullOrEmpty(this.Functionality.Item.ReferenceCode))
                {
                    MyContentText.AttachContentTextWithControl(listingItemCodeLabel, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ProductPage_ItemCodeLabel).ToFrontendBase());
                    listingItemCodeValue.InnerText = this.Functionality.Item.ReferenceCode;
                }
                else
                {
                    listingItemCodeContainer.Visible = false;
                }

                anchorListingTitle.InnerHtml = Functionality.Item.GetTitle();

                string desc = this.Functionality.Item.GetShortDescription(true, true);
                if (!string.IsNullOrEmpty(desc))
                {
                    divProductItemDescription.InnerHtml = desc;
                }
                else
                {
                    MyContentText.AttachContentTextWithControl(divProductItemDescription, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Products_NoDescriptionAvailable).ToFrontendBase());
                }
                imgProduct.ImageUrl = Functionality.Item.ImageUrl;
                if (this.Functionality.Item.HasDiscount)
                {
                    spanNewPrice.Visible = true;
                    spanNewPrice.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(this.Functionality.Item.PriceActual, General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
                    spanPrice.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(this.Functionality.Item.PriceBeforeDiscount, General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
                    spanPrice.Attributes["class"] += " listing-item-old-price";
                }
                else
                {
                    spanPrice.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(this.Functionality.Item.PriceActual, General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
                }
                anchorViewMore.HRef = anchorListingTitle.HRef = imgProduct.HRef = Functionality.Item.Url;
            }


        }

        public static GenericListingFloatedItem LoadControl(Page pg, string id)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/Products/GenericListingFloatedItem.ascx") as GenericListingFloatedItem;
            if (!string.IsNullOrEmpty(id))
            {
                ctrl.ID = id;
            }
            return ctrl;
        }
    }
}