﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.BannerModule;
using BusinessLogic_v3.Modules.BannerModule;
using CS.General_v3.JavaScript.Data;
using CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Sliders.NivoSlider.Classes;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Sliders.NivoSlider.v1
{
    using General_v3.JavaScript.Data;

    public partial class NivoSlider : BaseUserControl
    {
        public class NivoSliderV1Options : JavaScriptObject
        {
            public string effect;

            public int? width;
            public int? height;

            public int? slices;
            public int? boxCols;
            public int? boxRows;
            public int? animSpeed;
            public int? pauseTime;
            public int? startSlide;
           
            public bool? directionNav;
            public bool? directionNavHide;
            public bool? controlNav;

            public bool? controlNavThumbs;
            public bool? controlNavThumbsFromRel;
            public string controlNavThumbsSearch;
            public string controlNavThumbsReplace;
           
            public bool? keyboardNav;
            public bool? pauseOnHover;
            public bool? manualAdvance;
          
            public double? captionOpacity;
            public string prevText;
            public string nextText;

            public Object beforeChange;
            public Object afterChange;
            public Object slideshowEnd;
            public Object lastSlide;
            public Object afterLoad;
        }

        private NivoSliderV1Options _options = new NivoSliderV1Options();

        public int? Width { get { return _options.width; } set { _options.width = value; } }
        public int? Height { get { return _options.height; } set { _options.height = value; } }


        private CS.General_v3.Enums.NIVO_SLIDER_EFFECTS _effect;

        public CS.General_v3.Enums.NIVO_SLIDER_EFFECTS effect
        {

            get
            {
                return _effect;
            }
            set
            {
                _effect = value;
                _options.effect = CS.General_v3.Util.EnumUtils.StringValueOf(value);
            }

        }

        public int? Slices { get { return _options.slices; } set { _options.slices = value; } }
        public int? BoxCols { get { return _options.boxCols; } set { _options.boxCols = value; } }
        public int? BoxRows { get { return _options.boxRows; } set { _options.boxRows = value; } }
        public int? AnimSpeed { get { return _options.animSpeed; } set { _options.animSpeed = value; } }
        public int? PauseTime { get { return _options.pauseTime; } set { _options.pauseTime = value; } }
        public int? StartSlide { get { return _options.startSlide; } set { _options.startSlide = value; } }

        public bool? DirectionNav { get { return _options.directionNav; } set { _options.directionNav = value; } }
        public bool? DirectionNavHide { get { return _options.directionNavHide; } set { _options.directionNavHide = value; } }
        public bool? ControlNav { get { return _options.controlNav; } set { _options.controlNav = value; } }
        public bool? ControlNavThumbs { get { return _options.controlNavThumbs; } set { _options.controlNavThumbs = value; } }
        public bool? ControlNavThumbsFromRel { get { return _options.controlNavThumbsFromRel; } set { _options.controlNavThumbsFromRel = value; } }

        public string ControlNavThumbsSearch { get { return _options.controlNavThumbsSearch; } set { _options.controlNavThumbsSearch = value; } }
        public string ControlNavThumbsReplace { get { return _options.controlNavThumbsReplace; } set { _options.controlNavThumbsReplace = value; } }

        public bool? KeyboardNav { get { return _options.keyboardNav; } set { _options.keyboardNav = value; } }
        public bool? PauseOnHover { get { return _options.pauseOnHover; } set { _options.pauseOnHover = value; } }
        public bool? ManualAdvance { get { return _options.manualAdvance; } set { _options.manualAdvance = value; } }

        public double? CaptionOpacity { get { return _options.captionOpacity; } set { _options.captionOpacity = value; } }
        public string PrevText { get { return _options.prevText; } set { _options.prevText = value; } }
        public string NextText { get { return _options.nextText; } set { _options.nextText = value; } }

        public class FUNCTIONALITY
        {
            private NivoSlider _control;

            public List<INivoSliderImage> Images { get; set; }


            public FUNCTIONALITY(NivoSlider control)
            {
                _control = control;
                this.Images = new List<INivoSliderImage>();
                
            }

            public string CssClass { get; set; }
            public bool DoNotShowCaption { get; set; }
        }

        public FUNCTIONALITY Functionality { get; private set; }
        public NivoSlider()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initScriptsInHeader();
            initSlider();
        }

        

        

        private void initScriptsInHeader()
        {
            bool initializedNivoSliderScripts = CS.General_v3.Util.PageUtil.GetContextObject<bool>("initNivoSliderScripts");
            if (!initializedNivoSliderScripts)
            {
                CS.General_v3.Util.PageUtil.SetContextObject("initNivoSliderScripts", true);
                if (placeholderScripts != null)
                {
                    this.Page.Header.Controls.Add(placeholderScripts);
                }
            }
            else
            {
                //Do not show it, once is enough
                placeholderScripts.Visible = false;
            }
        }

        private void initSlider()
        {
            var images = Functionality.Images;
            if (images != null && images.Count() > 0)
            {
                slider.Controls.Clear();
                foreach (var image in images)
                {
                    Image img = new Image();
                    MyAnchor anch = new MyAnchor();

                    img.AlternateText = image.AlternateText;
                    img.ImageUrl = image.URL;
                    img.ToolTip = image.Title;

                    anch.Href = image.Href;
                    anch.Controls.Add(img);
                    slider.Controls.Add(anch);
                }
                _options.controlNavThumbs = true;
                string js = "jQuery('#" + this.slider.ClientID + "').nivoSlider(" + _options.GetJsObject().GetJS() + ");\r\n";
                JSUtil.AddJSScriptToPage(js);
            }

            if (!String.IsNullOrEmpty(Functionality.CssClass))
            {
                slider.Attributes["class"] += " " + Functionality.CssClass;
            }

            if (this.Width != 0)
            {
                slider.Style.Add(HtmlTextWriterStyle.Width, this.Width + "px");
            }

            if (this.Height != 0)
            {
                slider.Style.Add(HtmlTextWriterStyle.Height, this.Height + "px");
            }
        }
    }
}