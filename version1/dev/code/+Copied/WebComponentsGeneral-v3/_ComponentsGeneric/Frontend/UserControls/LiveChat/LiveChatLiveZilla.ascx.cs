﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.LiveChat
{
    public partial class LiveChatLiveZilla : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private LiveChatLiveZilla _control;
            public FUNCTIONALITY(LiveChatLiveZilla control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public LiveChatLiveZilla()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
    }
}