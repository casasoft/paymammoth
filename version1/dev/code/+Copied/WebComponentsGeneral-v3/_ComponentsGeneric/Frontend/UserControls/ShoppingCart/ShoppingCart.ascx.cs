﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ProductVariationMediaItemModule;
using BusinessLogic_v3.Frontend.ShoppingCartItemModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using CS.General_v3.Classes.HelperClasses;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3;
using BusinessLogic_v3.Frontend.ShoppingCartModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart
{
    public partial class ShoppingCart : BaseUserControl
    {
        public enum SHOPPING_CART_TYPE
        {
            Cart,
            Checkout
        }

        private ShoppingCartBaseFrontend _shoppingCart;
        private List<ShoppingCartItem> _shoppingCartItemsCtrl;

        public class FUNCTIONALITY
        {
            private ShoppingCart _control;
            public FUNCTIONALITY(ShoppingCart control)
            {
                _control = control;
            }
            public SHOPPING_CART_TYPE CartType { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ShoppingCart()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
            initListing();

            if (_shoppingCart != null)
            {
                initCartManagementArea();
                initCheckoutManagementArea();
            }
        }

        private void initCheckoutManagementArea()
        {
            if (Functionality.CartType == SHOPPING_CART_TYPE.Checkout)
            {
                shoppingCartProceedCtrl.Functionality.LoggedInProceedPayment.Functionality.OnSubmit += new ShoppingCartProceedLoggedIn.ShoppingCartSubmitHandler(Functionality_OnSubmit);
                shoppingCartProceedCtrl.Functionality.IsInCheckOut = true;
                checkOutManagementArea.Functionality.ShoppingCart =shoppingCartProceedCtrl.Functionality.ShoppingCart = _shoppingCart;
                checkOutManagementArea.Visible = true;
            }
        }

        BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartDeliveryDetailsHelper Functionality_OnSubmit(ShoppingCartProceedLoggedIn sender)
        {
            var details = checkOutManagementArea.Functionality.GetDeliveryDetails();
            if(details != null)
            {
                return details;
            }
            return null;
        }

        

        private void initCartManagementArea()
        {
            if (Functionality.CartType== SHOPPING_CART_TYPE.Cart)
            {
                shoppingCartPriceDetails.Functionality.IsInCart = true;
                shoppingCartPriceDetails.Functionality.ShoppingCart = _shoppingCart;
                divCartManagementArea.Visible = true;
                setUpdateQuantitiesArea();
              //  updateTotalAreaCart();
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(shoppingCartPriceDetails);
            }
        }

        private void updateTotalAreaCart()
        {
            ContentTextBaseFrontend cntTextTotal = Factories.ContentTextFactory.GetContentTextForEnumValue(Enums.CONTENT_TEXT.ShoppingCart_MyCart_Total).ToFrontendBase();
            
            MySpan totalText = new MySpan();
            totalText.CssManager.AddClass("totals-text");
            MyContentText.AttachContentTextWithControl(totalText, cntTextTotal);
            divTotalsContainerCart.Controls.Add(totalText);

            MySpan totalValue = new MySpan();
            totalValue.CssManager.AddClass("totals-value");
            totalValue.InnerText = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(_shoppingCart.Data.GetTotalPrice(),
                General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency);
            divTotalsContainerCart.Controls.Add(totalValue);
        }

        private void setUpdateQuantitiesArea()
        {
            ContentTextBaseFrontend cntTextUpdateQuantitiesQuestion = Factories.ContentTextFactory.GetContentTextForEnumValue(Enums.CONTENT_TEXT.ShoppingCart_MyCart_DidYouMakeChangesToQuantities).ToFrontendBase();
            ContentTextBaseFrontend cntTextUpdateQuantitiesButton = Factories.ContentTextFactory.GetContentTextForEnumValue(Enums.CONTENT_TEXT.ShoppingCart_MyCart_UpdateQuantities).ToFrontendBase();
            MyContentText.AttachContentTextWithControl(spanQuantitiesText, cntTextUpdateQuantitiesQuestion);

            MyContentText.AttachContentTextWithControl(btnUpdateQuantities, cntTextUpdateQuantitiesButton);
            divUpdateQuantitiesContainer.Controls.Add(btnUpdateQuantities);
            btnUpdateQuantities.Click += new EventHandler(btnUpdateQuantities_Click);
        }

        

        void btnUpdateQuantities_Click(object sender, EventArgs e)
        {
            List<ShoppingCartItemBaseFrontend> updateFail = new List<ShoppingCartItemBaseFrontend>();

            foreach (var item in _shoppingCartItemsCtrl)
            {
                var quantity = item.Functionality.GetQuantity();
                var result = item.Functionality.ShoppingCartItem.Data.UpdateQuantity(quantity);
                if(result.UpdateQuantityResult == Enums.SHOPPING_CART_ITEM_UPDATE_QTY_RESULT.NotEnoughItemsAvailableInStock)
                {
                    updateFail.Add(item.Functionality.ShoppingCartItem);
                }
                bool ok = item.OnUpdateQuantityClicked(item.Functionality.ShoppingCartItem);
                if (!ok)
                {
                    updateFail.Add(item.Functionality.ShoppingCartItem);
                }
            }

            if (updateFail.Count() == 0)
            {
                ContentTextBaseFrontend cntTextUpdateSuccess = Factories.ContentTextFactory.GetContentTextForEnumValue(Enums.CONTENT_TEXT.ShoppingCart_MyCart_QuantitiesUpdatedSuccessfully).ToFrontendBase();
                this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(cntTextUpdateSuccess);
            }
        }

        private void initListing()
        {
            //string shoppingCartUserControlPath = "/Controls/ShoppingCart/ShoppingCart.ascx";
            string shoppingCartItemUserControlPath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_ShoppingCart_ShoppingCartItemUserControlPath);
            //UserControlController.SetControl("/Controls/ShoppingCart/ShoppingCartJadaJade.ascx");
            //_cart = UserControlController.GetControl("/Controls/ShoppingCart/ShoppingCartJadaJade.ascx");

            

            _shoppingCart = BusinessLogic_v3.Modules.ModulesUtil.GetCurrentShoppingCart(createIfNotExists: false).ToFrontendBase();
            _shoppingCartItemsCtrl = new List<ShoppingCartItem>();
            if (_shoppingCart != null)
            {
                discountItems.Functionality.ShoppingCart = _shoppingCart;
                IEnumerable<ShoppingCartItemBaseFrontend> shoppingCartItems = _shoppingCart.Data.GetAllShoppingCartItems().ToFrontendBaseList();
                int count = 0;
                if (shoppingCartItems != null && shoppingCartItems.Count() > 0)
                {
                    foreach (ShoppingCartItemBaseFrontend shoppingCartItem in shoppingCartItems)
                    {
                        count++;


                        var _ctrl = (ShoppingCartItem)Page.LoadControl(shoppingCartItemUserControlPath);
                        _ctrl.Functionality.IsEven = count%2 == 0;
                        _ctrl.Functionality.ShoppingCartItem = shoppingCartItem;
                        _ctrl.Functionality.CartType = this.Functionality.CartType;
                        tBodyListing.Controls.Add(_ctrl);

                        //Delete
                        if (this.Functionality.CartType != SHOPPING_CART_TYPE.Cart )
                        {
                            thDelete.Visible = false;
                        }
                       
                        _shoppingCartItemsCtrl.Add(_ctrl);
                    }
                }
                else
                {
                    showEmptyCartError();
                }
            }
            else
            {
                showEmptyCartError();
            }
        }

      
        private void showEmptyCartError()
        {
            divCartContainer.Controls.Clear();
            MyContentText.AttachContentTextWithControl(divCartContainer, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_NoItemsInCart).ToFrontendBase());
        }
        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(thItem, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ShoppingCart_MyCart_Item).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thQuantity, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ShoppingCart_MyCart_Quantity).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thUnitPrice, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ShoppingCart_MyCart_UnitPrice).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(thTotalPrice, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ShoppingCart_MyCart_TotalPrice).ToFrontendBase());
        }
    }
}