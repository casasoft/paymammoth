﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Modules.ArticleModule;
using CS.General_v3.Util;
using BusinessLogic_v3.Modules.MemberModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage
{
    public partial class GeneralOverlayLoginFields : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private GeneralOverlayLoginFields _control;
            public FUNCTIONALITY(GeneralOverlayLoginFields control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public GeneralOverlayLoginFields()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            init();
        }

        private void init()
        {
            initLoginFieldsCheck();
        }

        private void initLoginFieldsCheck()
        {
            bool hide = true;
            if (MemberBaseFactory.UsedInProject)
            {
                var currentUser = BusinessLogic_v3.Modules.Factories.MemberFactory.GetLoggedInUserForCurrentSession();
                if (currentUser != null)
                {
                    hide = true;
                }
                else
                {
                    hide = false;
                    string loginRedirectUrl = null;
                    var cp = Page.Functionality.ContentPage;
                    if (cp == null ||
                        (((IArticleBase)cp.Data).DoNotRedirectOnLogin.HasValue &&
                         ((IArticleBase)cp.Data).DoNotRedirectOnLogin.Value))
                    {
                        loginRedirectUrl = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                            BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Home).GetUrl();
                    }
                    else
                    {
                        loginRedirectUrl = PageUtil.GetCurrentUrlLocation().GetURL();
                    }
                    loginCtrl.Functionality.OnLoginRedirectToURL = loginRedirectUrl;
                }
            }
            if(hide)
            {
                phLoginFieldsLoggedOut.Visible = false;
            }
        }
    }
}