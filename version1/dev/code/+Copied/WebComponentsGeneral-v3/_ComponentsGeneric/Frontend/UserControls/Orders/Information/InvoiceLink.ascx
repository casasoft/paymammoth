﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvoiceLink.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Orders.Information.InvoiceLink" %>
<div class="payment-success-invoice-text-wrapper">
    <span class="payment-success-invoice-text-link" id="spanCTInvoiceLink" runat="server"></span>
</div>
