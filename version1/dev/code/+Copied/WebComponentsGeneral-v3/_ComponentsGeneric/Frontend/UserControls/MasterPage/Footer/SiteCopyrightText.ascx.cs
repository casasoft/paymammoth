﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.General_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.Footer
{
    public partial class SiteCopyrightText : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private SiteCopyrightText _control;
            public FUNCTIONALITY(SiteCopyrightText control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public SiteCopyrightText()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
        }

        private void updateTexts()
        {
            var ct = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Footer_SiteCopyrightText).ToFrontendBase();
            var dict = ct.CreateTokenReplacer();
            dict.Add(BusinessLogic_v3.Constants.Tokens.SITE_COPYRIGHT_YEAR, CS.General_v3.Util.Date.Now.Year.ToString());
            dict.Add(BusinessLogic_v3.Constants.Tokens.SITE_COPYRIGHT_SITE_NAME, BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.SETTINGS_ENUM.Others_WebsiteName));
            MyContentText.AttachContentTextWithControl(spanSiteCopyright, 
                ct, dict);
        }
    }
}