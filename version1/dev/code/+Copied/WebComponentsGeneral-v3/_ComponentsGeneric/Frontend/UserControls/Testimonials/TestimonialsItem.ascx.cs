﻿using System;
using System.Web.UI;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.TestimonialModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Testimonials
{
    public partial class TestimonialsItem : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private TestimonialsItem _control;
            public FUNCTIONALITY(TestimonialsItem control)
            {
                _control = control;
            }
            public TestimonialBaseFrontend Testimonial { get; set; }
            public bool DoNotShowCourseIcon { get; set; }
            public bool IsLastItem { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }

        public TestimonialsItem()
        {
            this.Functionality = createFunctionality();
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateContent();
        }

        private void updateContent()
        {
            if(this.Functionality.Testimonial != null)
            {
                updateTexts();
            }
        }

        private void updateTexts()
        {
            ltTestimonialContent.Text = this.Functionality.Testimonial.Data.Testimonial;
            spanTestimonialUser.InnerHtml = this.Functionality.Testimonial.Data.FullName;

            var course = this.Functionality.Testimonial.Data.GetEvent().ToFrontendBase();
            if(course != null)
            {
                var userEventCntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Testimonials_User_Event).ToFrontendBase();
                userEventCntTxt.TokenReplacer[BusinessLogic_v3.Constants.Tokens.EVENT_TITLE] = course.Data.Title;
                MyContentText.AttachContentTextWithControl(spanTestimonialCourse, userEventCntTxt);
                
                if (!this.Functionality.DoNotShowCourseIcon)
                {
                    var category = course.Data.Category.ToFrontendBase();
                    if (category != null)
                    {
                        imgIcon.ImageUrl = "/images/sprites/symbol-icon-" + category.Data.Identifier.ToLower() + "-blue.png";
                    }
                }
                else
                {
                    CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divCourseIcon);
                    CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divSeparator);
                }
            }
            if (this.Functionality.IsLastItem)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divSeparator);
            }
        }

        public static TestimonialsItem LoadControl(Page pg)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/Testimonials/TestimonialsItem.ascx") as TestimonialsItem;
            return ctrl;
        }
    }
}