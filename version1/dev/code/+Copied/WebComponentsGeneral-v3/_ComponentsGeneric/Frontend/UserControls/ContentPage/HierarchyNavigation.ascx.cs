﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.General_v3.Classes.Interfaces.Hierarchy;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage
{
    public partial class HierarchyNavigation : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private HierarchyNavigation _control;
            public FUNCTIONALITY(HierarchyNavigation control)
            {
                _control = control;
            }
            public IHierarchyNavigation Item { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public HierarchyNavigation()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if(Functionality.Item != null)
            {
                initHierarchy();
            }
        }

        private void initHierarchy()
        {
            IHierarchyNavigation parent = Functionality.Item.GetMainParent();
            while (parent != null && !parent.ConsiderAsRootNode)
            {
                parent = parent.GetMainParent();
            }
            initSiblings(parent);
        }

        private void initSiblings(IHierarchyNavigation item)
        {
            IEnumerable<IHierarchyNavigation> links = createLinks(item);
            hierarchyLinks.Functionality.Items = links;
        }

        private IEnumerable<IHierarchyNavigation> createLinks(IHierarchyNavigation item)
        {
            List<IHierarchyNavigation> list = new List<IHierarchyNavigation>();
            IEnumerable<IHierarchyNavigation> children = item.GetChildren();
            if (children != null && children.Count() > 0)
            {
                foreach (IHierarchyNavigation child in children)
                {
                    if (child.Visible)
                    {
                        HierarchyNavigationImpl hd = CreateHierarchicalDataFromContentPage(child);
                        var parent = this.Functionality.Item.GetMainParent();
                        if (parent != null && !parent.ConsiderAsRootNode)
                        {
                            if (hd.ID == parent.ID)
                            {
                                var listOfChildren = createChildren(parent);
                                if (listOfChildren != null)
                                {
                                    foreach (var element in listOfChildren)
                                    {
                                        hd.AddChild(element);
                                    }
                                }
                            }
                        }
                        else if (hd.ID == this.Functionality.Item.ID)
                        {
                            hd.Selected = true;
                            var listOfChildren = createChildren(this.Functionality.Item);
                            if (listOfChildren != null)
                            {
                                foreach (var element in listOfChildren)
                                {
                                    hd.AddChild(element);
                                }
                            }
                        }
                        list.Add(hd);
                    }
                }
            }
            return list;
        }

        private List<HierarchyNavigationImpl> createChildren(IHierarchyNavigation item)
        {
            List<HierarchyNavigationImpl> list = new List<HierarchyNavigationImpl>();
            var hdChildren = item.GetChildren();
            if (hdChildren != null && hdChildren.Count() > 0)
            {
                foreach (IHierarchyNavigation hdChild in hdChildren)
                {
                    if (hdChild.Visible)
                    {
                        var hd2 = CreateHierarchicalDataFromContentPage(hdChild);
                        if (hd2.ID == this.Functionality.Item.ID)
                        {
                            hd2.Selected = true;
                        }
                        list.Add(hd2);
                    }
                }
                return list;
            }
            return null;
        }

        private HierarchyNavigationImpl CreateHierarchicalDataFromContentPage(IHierarchyNavigation hierarchy)
        {
            HierarchyNavigationImpl hd = new HierarchyNavigationImpl();
            hd.ID = hierarchy.ID;
            hd.Href = hierarchy.Href;
            hd.Title = hierarchy.Title;
            hd.HrefTarget = hierarchy.HrefTarget;
            hd.Visible = true;
            return hd;
        }
    }
}