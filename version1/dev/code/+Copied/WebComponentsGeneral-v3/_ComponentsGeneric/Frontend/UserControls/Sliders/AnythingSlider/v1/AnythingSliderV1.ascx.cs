﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.BannerModule;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Sliders.AnythingSlider.v1
{
    using System.ComponentModel;
    using System.Web.UI.HtmlControls;
    using CS.General_v3.JavaScript.Data;
    using BusinessLogic_v3.Modules.BannerModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using BusinessLogic_v3;
    using General_v3.JavaScript.Data;

    public partial class AnythingSliderV1 : BaseUserControl
    {
        

        public class AnythingSliderV1Options : JavaScriptObject
        {
            public int? width;
            public int? height;
            public bool resizeContents = true;
            public int startPanel = 1;
            public bool hashTags = true;
            public bool buildArrows = true;
            public bool buildNavigation = true;
            public string forwardText = "&raquo;";
            public string backText = "&laquo;";

            public bool autoPlay = true;
            public bool startStopped = false;
            public bool pauseOnHover = true;
            public bool resumeVideoOnEnd = true;
            public bool stopAtEnd = false;
            public bool playRtl = false;
            public string startText = "Start";
            public string stopText = "Stop";
            public int delay = 3000;
            public int? animationTime;
            public string easing = "easeInOutExpo";
        }

        private AnythingSliderV1Options _options = new AnythingSliderV1Options();

        public int? Width { get { return _options.width; } set { _options.width = value; } }
        public int? Height { get { return _options.height; } set { _options.height = value; } }

        public bool ResizeContents { get { return _options.resizeContents; } set { _options.resizeContents = value; } }
        public int StartPanel { get { return _options.startPanel; } set { _options.startPanel = value; } }
        public bool HashTags { get { return _options.hashTags; } set { _options.hashTags = value; } }
        public bool BuildArrows { get { return _options.buildArrows; } set { _options.buildArrows = value; } }
        public bool BuildNavigation { get { return _options.buildNavigation; } set { _options.buildNavigation = value; } }
        public bool AutoPlay { get { return _options.autoPlay; } set { _options.autoPlay = value; } }
        public bool StartStopped { get { return _options.startStopped; } set { _options.startStopped = value; } }
        public bool PauseOnHover { get { return _options.pauseOnHover; } set { _options.pauseOnHover = value; } }
        public bool ResumeVideoOnEnd { get { return _options.resumeVideoOnEnd; } set { _options.resumeVideoOnEnd = value; } }
        public bool StopAtEnd { get { return _options.stopAtEnd; } set { _options.stopAtEnd = value; } }
        public bool PlayRtl { get { return _options.playRtl; } set { _options.playRtl = value; } }
        public string ForwardText { get { return _options.forwardText; } set { _options.forwardText = value; } }
        public string BackText { get { return _options.backText; } set { _options.backText = value; } }
        public string StartText { get { return _options.startText; } set { _options.startText = value; } }
        public string StopText { get { return _options.stopText; } set { _options.stopText = value; } }
        public int DelayMS { get { return _options.delay; } set { _options.delay = value; } }
        public int? AnimationTime { get { return _options.animationTime; } set { _options.animationTime = value; } }
        public string Easing { get { return _options.easing; } set { _options.easing = value; } }


        /*
        public bool  = true;
        public bool  = true;
        public string forwardText = "&raquo;";
        public string backText = "&laquo;";

        public bool  = true;
        public bool  = false;
        public bool  = true;
        public bool  = true;
        public bool  = false;
        public bool  = false;
        public string  = "Start";
        public string  = "Stop";
        public int  = 3000;
        public int  = 600;
        public string  = "swing";
        |*/

        public class FUNCTIONALITY
        {
            private AnythingSliderV1 _control;

            public List<Control> Controls { get; set; }
            public bool PopulateFromBannerFactory { get; set; }

            public FUNCTIONALITY(AnythingSliderV1 control)
            {
                _control = control;
                this.Controls = new List<Control>();
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public AnythingSliderV1()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        private void initSlider()
        {
            if(BusinessLogic_v3.Modules.ModuleSettings.Generic.MultiLingual)
            {
                updateTexts();
            }

            if (Functionality.PopulateFromBannerFactory)
            {
                initPopulateFromBannerFactory();
            }

            ulSlider.Controls.Clear();
            for (int i = 0; i < this.Functionality.Controls.Count; i++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                li.Controls.Add(this.Functionality.Controls[i]);
                ulSlider.Controls.Add(li);
            }

            string js = "jQuery('#" + this.ulSlider.ClientID + "').anythingSlider(" + _options.GetJsObject().GetJS() +
                        ");\r\n";

            CS.WebComponentsGeneralV3.Code.Util.JSUtil.AddJSScriptToPage(js);
        }

        private void updateTexts()
        {
            // UPDATE TEXTS FROM CONTENT TEXT HERE
            this.StartText =
                BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    BusinessLogic_v3.Enums.CONTENT_TEXT.AnythingSlider_StartText).GetContent();
            this.StopText =
                BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    BusinessLogic_v3.Enums.CONTENT_TEXT.AnythingSlider_StopText).GetContent();
        }

        private void initPopulateFromBannerFactory()
        {

            IEnumerable<BannerBaseFrontend> listOfBanners;
            if(BusinessLogic_v3.Modules.ModuleSettings.Generic.MultiLingual)
            {
                var culture = BusinessLogic_v3.Modules.Factories.CultureDetailsFactory.GetCurrentCultureInSession().ToFrontendBase();
                listOfBanners = BannerBaseFactory.Instance.GetBannersByCulture(Enums.BANNERS_SORT_BY.PriorityDescending, culture.Data).ToFrontendBaseList();
            }
            else
            {
                listOfBanners = BannerBaseFactory.Instance.GetAllBannersSortedBy(BusinessLogic_v3.Enums.BANNERS_SORT_BY.PriorityAscending).ToFrontendBaseList();
            }

            if (listOfBanners != null && listOfBanners.Count() > 0)
            {
                List<Control> listOfControls = new List<Control>();
                foreach(var banner in listOfBanners)
                {
                    listOfControls.Add(initBanner(banner));
                }
                Functionality.Controls = listOfControls;
            }
        }

        private Control initBanner(BannerBaseFrontend banner)
        {
            var bannerUrl = banner.Data.GetBannerURL();
            if (!String.IsNullOrEmpty(bannerUrl))
            {
                if (CS.General_v3.Util.UrlUtil.CheckForYoutubeLinks(bannerUrl))
                {
                    MyDiv divYoutubeObject = new MyDiv();
                    var youtubeLink = CS.General_v3.Util.UrlUtil.GetYoutubeURLForEmbedding(bannerUrl);
                    //string objectMediaText = @"<object width='" + this.Width + "' height='" +
                    //                         this.Height + "'><param name='movie' value='" + youtubeLink +
                    //                         "'><param name='allowFullScreen' value='true'><param name='allowscriptaccess' value='always'><embed src='" +
                    //                         youtubeLink +
                    //                         "' type='application/x-shockwave-flash' allowscriptaccess='always' allowfullscreen='true' width='" +
                    //                         this.Width + "' height='" + this.Height +
                    //                         "'></object>";
                    string objectMediaText = @"<iframe width='" + this.Width + "' height='" + this.Height + "' src='" +
                                             youtubeLink + "' frameborder='0' allowfullscreen></iframe>";
                    divYoutubeObject.InnerHtml = objectMediaText;
                    return divYoutubeObject;
                }
                else
                {
                    MyImage imgSlider = new MyImage();
                    imgSlider.ImageUrl = bannerUrl;
                    imgSlider.AlternateText = banner.Data.Title;
                    return imgSlider;
                }
            }
            return null;
        }

        protected override void OnLoad(EventArgs e)
        {
            initSlider();
            base.OnLoad(e);
        }
        public override string ClientID
        {
            get
            {
                return ulSlider.ClientID;
            }
        }
    }
}