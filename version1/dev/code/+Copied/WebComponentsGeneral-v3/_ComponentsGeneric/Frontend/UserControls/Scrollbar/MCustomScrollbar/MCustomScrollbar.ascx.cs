﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Scrollbar.MCustomScrollbar
{
    /// <summary>
    /// To use this one, just add the content and set variables
    /// You must add CSS classes to your CSS file.  Copy the CSS classes from /_common/static/js/jQuery/plugins/scrollbar/mCustomScrollbar/3.0.6/jquery.mCustomScrollbar2.css 
    /// and rename them to match your scrollbar
    /// 
    /// </summary>
    public partial class MCustomScrollbar : BaseUserControl
    {
        public enum SCROLL_TYPE
        {
            Vertical,
            Horizontal
        }
        public enum SCROLLBAR_ADJUSTMENT
        {
            Auto,
            Fixed
        }

        #region MCustomScrollbar Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected MCustomScrollbar _item = null;

            public bool AttachJsFiles { get; set; }

            public string CssClass { get; set; }

            public SCROLL_TYPE ScrollDirection { get; set; }
            public int EasingAmount { get; set; }
            public string EasingType { get; set; }
            public int ExtraBottomScrollingSpace { get; set; }
            /// <summary>
            /// Set the scrollbar height/width adjustment (“auto” for adjustable scrollbar height/width analogous to content length or “fixed” for fixed dimensions)
            /// </summary>
            public SCROLLBAR_ADJUSTMENT ScrollbarAdjustment { get; set; }
            public bool MouseWheelSupport { get; set; }
            public bool ScrollViaButtons { get; set; }
            /// <summary>
            /// Buttons scrolling speed (an integer between 1 and 20, with 1 indicating the slowest scroll speed)
            /// </summary>
            public int ButtonScrollingSpeed { get; set; }

            internal FUNCTIONALITY(MCustomScrollbar item)
            {
                this._item = item;

                this.EasingAmount = 0;
                this.EasingType = null;
                this.ScrollDirection = SCROLL_TYPE.Vertical;
                this.ExtraBottomScrollingSpace = 0;
                this.ScrollbarAdjustment = SCROLLBAR_ADJUSTMENT.Auto;
                this.MouseWheelSupport = true;
                this.ScrollViaButtons = true;
                this.ButtonScrollingSpeed = 10;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion



        /// <summary>
        /// The Template property for the Content area.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(INamingContainer))]
        [TemplateInstance(TemplateInstance.Single)] //This is so that inner child controls can be accessed.  If you can create multiple controls, then you cannot access this
        public ITemplate Content { get; set; }

        protected override void OnInit(EventArgs e)
        {
            if (Content != null)
            {
                Content.InstantiateIn(this.placeHolderContent);
            }
            base.OnInit(e);
        }

        const string CONTEXT_JS_FILES_ATTACH_ID = "MCustomScrollbarJSFilesAttach";

        private bool hasJsFilesBeenAttached
        {
            get
            {
                return CS.General_v3.Util.PageUtil.GetContextObject<bool>(CONTEXT_JS_FILES_ATTACH_ID);
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetContextObject(CONTEXT_JS_FILES_ATTACH_ID, value);
            }

        }
        private void attachJSFiles()
        {
            if (!this.Functionality.AttachJsFiles && !hasJsFilesBeenAttached)
            {
                hasJsFilesBeenAttached = true;

                string literalHTML = @"<script src='/_common/static/js/jQuery/plugins/scrollbar/mCustomScrollbar/3.0.6/jquery.mCustomScrollbar.js'></script>";
                if (this.Functionality.MouseWheelSupport)
                {
                    literalHTML += "<script src='/_common/static/js/jQuery/plugins/scrollbar/mCustomScrollbar/3.0.6/jquery.mousewheel.min.js'></script>";
                }
                this.Page.Header.Controls.Add(new Literal() { Text = literalHTML});
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Functionality.CssClass))
            {
                this.divCustomScrollbar.Attributes["class"] += " " + this.Functionality.CssClass;
            }
            if (!this.Functionality.ScrollViaButtons)
            {
                this.aScrollDownButton.Visible = false;
                this.aScrollUpButton.Visible = false;
            }
            attachJSFiles();
            base.OnLoad(e);
        }
        public string GetJSCode()
        {
            string call = "jQuery('#" + divCustomScrollbar.ClientID + "').mCustomScrollbar(";
            call += "'" + CS.General_v3.Util.EnumUtils.StringValueOf(this.Functionality.ScrollDirection).ToLower() + "', ";
            call += this.Functionality.EasingAmount + ", ";
            if (string.IsNullOrEmpty(this.Functionality.EasingType))
            {
                call += "null, ";
            }
            else
            {
                call += "'" + this.Functionality.EasingType + "', ";
            }
            call += this.Functionality.ExtraBottomScrollingSpace + ", ";
            call += "'" + CS.General_v3.Util.EnumUtils.StringValueOf(this.Functionality.ScrollbarAdjustment).ToLower() + "', ";
            call += "'" + (this.Functionality.MouseWheelSupport ? "yes" : "no") + "', ";
            call += "'" + (this.Functionality.ScrollViaButtons ? "yes" : "no") + "', ";
            call += this.Functionality.ButtonScrollingSpeed + "";
            call += ");";
            return call;
        }

        private void initJS()
        {
            /*<script>
                $(window).load(function() {
                $("#mcs_container").mCustomScrollbar("vertical",400,"easeOutCirc",1.05,"auto","yes","yes",10);
            });
            </script>
            <script src="jquery.mCustomScrollbar.js" type="text/javascript"></script>
            You can configure each content block scrollbar by setting the parameters of the function call:

            The scroll type (“vertical” or “horizontal”)
            The scroll easing amount (e.g. 400 for normal easing, 0 for no easing etc.)
            The scroll easing type
            The extra bottom scrolling space (applies to vertical scroll type only)
            Set the scrollbar height/width adjustment (“auto” for adjustable scrollbar height/width analogous to content length or “fixed” for fixed dimensions)
            Set mouse-wheel support (“yes” or “no”)
            Scrolling via buttons support (“yes” or “no”)
            Buttons scrolling speed (an integer between 1 and 20, with 1 indicating the slowest scroll speed)*/

            string call = GetJSCode();
                
                

            string js = @"
                        <script>
                            jQuery(window).load(function () {
                                "+call+@"
                            });
                        </script>";

            this.Controls.Add(new Literal() { Text = js });

        }

        protected override void OnPreRender(EventArgs e)
        {
            initJS();
            base.OnPreRender(e);
        }

        public Control ContentControl { get { return placeHolderContent; } }
            public string CssClass { get { return this.Functionality.CssClass; } set { this.Functionality.CssClass = value; } }

            public Control ScrollbarContainer { get { return divCustomScrollbar; } }

        }
}