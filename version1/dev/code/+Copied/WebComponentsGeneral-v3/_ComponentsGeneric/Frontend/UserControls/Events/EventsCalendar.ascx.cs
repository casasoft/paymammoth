﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.EventSessionPeriodDayModule;
using BusinessLogic_v3.Modules.EventSessionModule;
using BusinessLogic_v3.Modules.EventSessionPeriodDayModule;
using BusinessLogic_v3.Util;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.JQueryFullCalendar.v1;
using BusinessLogic_v3.Frontend.EventSessionModule;
using BusinessLogic_v3.Frontend.EventModule;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Events
{
    public partial class EventsCalendar : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private EventsCalendar _control;
            public FUNCTIONALITY(EventsCalendar control)
            {
                _control = control;
            }

            public EventBaseFrontend Event { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public EventsCalendar()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initCalendarWebControl();
        }

        private void AddSessionToCalendarArray(EventSessionPeriodDayBaseFrontend sessionDay)
        {
            var session = sessionDay.Data.GetEventSession().ToFrontendBase();
            if(session != null)
            {


                _calendarJSEvents.Add(new _jQueryFullCalendarOptions.
                                                                                 _jQueryFullCalendarOptionsEventObject()
                {
                    id = sessionDay.Data.ID.ToString(),
                    title = session.Data.Title,
                    allDay = true,
                    start = sessionDay.Data.Day,
                    end = sessionDay.Data.Day,
                    url = session.GetUrl()
                });
            }
        }
        private List<_jQueryFullCalendarOptions._jQueryFullCalendarOptionsEventObject> _calendarJSEvents = new List<_jQueryFullCalendarOptions._jQueryFullCalendarOptionsEventObject>();
        private void initCalendarWebControl()
        {
            IEnumerable<EventSessionPeriodDayBaseFrontend> sessionDays;
            if (Functionality.Event == null)
            {
                sessionDays = ((IEnumerable<IEventSessionPeriodDayBase>)BusinessLogic_v3.Modules.Factories.EventSessionPeriodDayFactory.GetAllItemsInRepository()).ToList().ConvertAll(item => item.ToFrontendBase());
            }
            else
            {
                sessionDays = Functionality.Event.Data.GetEventSessionDays().ToList().ConvertAll(item => item.ToFrontendBase());
            }
            fullCalendar.Functionality.Parameters.options.header = new _jQueryFullCalendarOptions._jQueryFullCalendarOptionsHeader();
            fullCalendar.Functionality.Parameters.options.header.right = "month agendaWeek agendaDay";
            fullCalendar.Functionality.Parameters.options.header.left = "prev,next";
            fullCalendar.Functionality.Parameters.options.header.center = "title";
            fullCalendar.Functionality.Parameters.options.events = _calendarJSEvents;


            foreach (EventSessionPeriodDayBaseFrontend sessionDay in sessionDays)
            {
                var showOnlyAvailableSessions = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.Calendar_EventsCalendar_ShowOnlyAvailableSessions);
                if (showOnlyAvailableSessions)
                {
                    var session = sessionDay.Data.GetEventSession().ToFrontendBase();
                    if (((session.Data.SlotsAvailable - session.Data.SlotsTaken) > 0))
                    {
                        AddSessionToCalendarArray(sessionDay);
                    }
                }
                else
                {
                    AddSessionToCalendarArray(sessionDay);
                }

            }
        }
    }
}