﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Hierarchy;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.Footer
{
    public partial class FooterSitemap : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private FooterSitemap _control;
            public FUNCTIONALITY(FooterSitemap control)
            {
                _control = control;
            }
            public int MaxDepth { get; set; }
            public bool DoNotShowTitle { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public FooterSitemap()
        {
            this.Functionality = createFunctionality();
            this.Functionality.MaxDepth = 1;
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initNavigation();
            updateTexts();
        }

        private void updateTexts()
        {
            if(!this.Functionality.DoNotShowTitle)
            {
                var cntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Footer_FooterSitemap_Sitename).ToFrontendBase();
                footerItemControl.Functionality.FooterItemTitle = cntTxt;
            }
        }

        private void initNavigation()
        {
            HierarchicalControlNavigation siteMapNavigation = new HierarchicalControlNavigation();
            var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.MainFooterMenu).ToFrontendBase();
            var articleChildren = article.Data.GetChildArticles().ToFrontendBaseList();
            siteMapNavigation.Functionality.Items = articleChildren;
            siteMapNavigation.Functionality.MaxDepth = this.Functionality.MaxDepth;

            footerItemControl.Functionality.FooterItem = siteMapNavigation;
        }
    }
}