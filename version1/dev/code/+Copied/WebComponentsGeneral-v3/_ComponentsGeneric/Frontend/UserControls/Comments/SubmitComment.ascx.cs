﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Frontend.ArticleCommentModule;
using BusinessLogic_v3.Modules.ArticleModule;
using CS.General_v3.Classes.Interfaces.BlogPosts;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Comments
{
    using BusinessLogic_v3.Modules.MemberModule;
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Dialog;

    public partial class SubmitComment : BaseUserControl
    {
        private static string SUBMIT_COMMENT_IDENTIFIER = "SubmitComment_postComment";
        private MemberBaseFrontend _member;
        public bool _requiresLogin;
        private MyTxtBoxString _txtFullName;
        private MyTxtBoxEmail _txtEmail;
        private MyTxtBoxStringMultiLine _txtComment;

        public class FUNCTIONALITY
        {
            private SubmitComment _control;


            public FUNCTIONALITY(SubmitComment control)
            {
                _control = control;
            }
            public ICommentableItemComment ReplyToComment { get; set; }
            public ContentTextBaseFrontend LblFullName { get; set; }
            public ContentTextBaseFrontend LblEmailAddress { get; set; }
            public ContentTextBaseFrontend LblComment { get; set; }
            public ContentTextBaseFrontend TxtSubmitBtn { get; set; }
            public ContentTextBaseFrontend SuccessMsg { get; set; }
            public ContentTextBaseFrontend ErrorMsg { get; set; }
            public ICommentableItem CommentableItem { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public SubmitComment()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initMember();
            updateTexts();


            if (!_requiresLogin || _member != null)
            {
                initFormFields(_requiresLogin, _member);
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divNeedLogin);
            }
            else
            {
                initNeedLogin();
            }
        }
        private void initNeedLogin()
        {
            CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divFields);
            if (this.Functionality.ReplyToComment == null)
            {
                var ct = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_NeedLoginToComment).ToFrontendBase();
                MyContentText.AttachContentTextWithControl(divNeedLoginText, ct);
            }
            else
            {
                var ct = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_NeedLoginToReply).ToFrontendBase();
                MyContentText.AttachContentTextWithControl(divNeedLoginText, ct);
            }

            var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_LoginPage).ToFrontendBase();
            ltlLoginLinkId.Text = liLogin.ClientID;
            ltlOverlayTitleText.Text = article.Data.PageTitle;

            MyContentText.AttachContentTextWithControl(liLogin, Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_LoginLink).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(liRegisterAccount, Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_RegisterLink).ToFrontendBase());
        }

        private void initMember()
        {
            if (BusinessLogic_v3.Modules.MemberModule.MemberBaseFactory.UsedInProject)
            {
                _requiresLogin = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_CommentingRequireLogin);
                //If it is used in project, else do nothing
                MemberBaseFrontend member =
                    BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.
                        GetLoggedInUserAlsoFromRememberMe().ToFrontendBase();
                if (member != null)
                {
                    _member = member;
                }
            }
        }

        private void initFormFields(bool requiresLogin, MemberBaseFrontend loggedUser)
        {
            var group = submitCommentFields.Functionality.AddGroup(new CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields.FormFieldsItemGroupData()
            {
                Identifier = SUBMIT_COMMENT_IDENTIFIER
            });
            if (!requiresLogin)
            {
                //If login is required, then do not show these but take them from member

                //Full Name
                _txtFullName = (MyTxtBoxString) group.AddFormWebControl(new MyTxtBoxString()
                                                                        {
                                                                            FieldParameters = new FieldSingleControlParameters()
                                                                                              {
                                                                                                  id = "ContactUsForm_txtFullNme",
                                                                                                  required = true,
                                                                                                  titleContentText = Functionality.LblFullName
                                                                                              },
                                                                            InitialValue = (_member != null ? ((IMemberAddressDetails) _member.Data).FirstName : null)
                                                                        });

                //Email
                _txtEmail = (MyTxtBoxEmail) group.AddFormWebControl(new MyTxtBoxEmail()
                                                                    {
                                                                        FieldParameters = new FieldSingleControlParameters()
                                                                                          {
                                                                                              id = "ContactUsForm_txtEmail",
                                                                                              required = true,
                                                                                              titleContentText = Functionality.LblEmailAddress
                                                                                          },
                                                                        InitialValue = (_member != null) ? _member.Data.Email : null
                                                                    });
            }

            _txtComment = (MyTxtBoxStringMultiLine)group.AddFormWebControl(new MyTxtBoxStringMultiLine()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ContactUsForm_txtComment",
                    required = true,
                    titleContentText = Functionality.LblComment
                }
            });

            MyButton btnSubmit = submitCommentFields.Functionality.AddButton(this.Functionality.TxtSubmitBtn, SUBMIT_COMMENT_IDENTIFIER);
            btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }

        void btnSubmit_Click(object sender, EventArgs e)
        {
            ICommentData commentData = Factories.ArticleCommentFactory.CreateNewItem();
            commentData.PostedByIP = CS.General_v3.Util.PageUtil.GetUserIP();

            if (!_requiresLogin)
            {
                commentData.FullName = _txtFullName != null ? _txtFullName.GetFormValueAsString() : null;
                commentData.Email = _txtEmail != null ? _txtEmail.GetFormValueAsString() : null;
            }
            else
            {
                //Take them from logged user
                if (_member != null)
                {
                    commentData.FullName = _member.Data.FullName;
                    commentData.Email = _member.Data.Email;
                }
            }
            commentData.Comment = _txtComment != null ? _txtComment.GetFormValueAsString() : null;
            commentData.PostedOn = CS.General_v3.Util.Date.Now;


            CS.General_v3.Enums.COMMENT_POST_RESULT result;
            if (this.Functionality.ReplyToComment == null)
            {
                //add comment
                result =
                    this.Functionality.CommentableItem.PostComment(commentData);
            }
            else
            {
                //reply to comment
                result = this.Functionality.ReplyToComment.PostReply(commentData);
            }
            if (result == General_v3.Enums.COMMENT_POST_RESULT.SubmittedAndApproved)
            {

                this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(this.Functionality.SuccessMsg);
            } //todo: error messages should reflect submitted and approved, submitted and awaiting moderation, failure 20120710 (franco)
            else if (result == General_v3.Enums.COMMENT_POST_RESULT.SubmittedAwaitingModeration)
            {

                this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(this.Functionality.SuccessMsg);
            }
            else
            {
                this.Page.Functionality.ShowErrorMessageForNextRedirectToPage(this.Functionality.ErrorMsg);
            }
        }

        private void updateTexts()
        {
            if (this.Functionality.ReplyToComment == null)
            {
                MyContentText.AttachContentTextWithControl(spanPostCommentTitle, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_PostCommentTitle).ToFrontendBase());
            }
            else
            {
                MyContentText.AttachContentTextWithControl(spanPostCommentTitle, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_PostReplyTitle).ToFrontendBase());
            }
            this.Functionality.LblFullName = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_FullName).ToFrontendBase();
            this.Functionality.LblEmailAddress = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_EmailAddress).ToFrontendBase();
            this.Functionality.LblComment = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_Comment).ToFrontendBase();
            this.Functionality.TxtSubmitBtn = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_PostComment).ToFrontendBase();

            this.Functionality.SuccessMsg = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_SuccessMsg).ToFrontendBase();
            this.Functionality.ErrorMsg = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_ErrorMsg).ToFrontendBase();


        }

    }
}