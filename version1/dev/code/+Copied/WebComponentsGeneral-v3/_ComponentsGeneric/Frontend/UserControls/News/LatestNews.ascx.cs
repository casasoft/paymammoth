﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.General_v3;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ArticleModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.News
{
    public partial class LatestNews : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private LatestNews _control;
            public FUNCTIONALITY(LatestNews control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public LatestNews()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
            loadNews();
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(anchorViewMore, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_LatestNews_ViewMore).ToFrontendBase());
        }

        private void loadNews()
        {
            int latestNewsAmt = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(Enums.SETTINGS_ENUM.LatestNews_LimitAmt);
            var articleNews = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.NewsAndUpdates);

            if (articleNews != null)
            {

                int totalResults = 0;
                var articles = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticlesForParent(articleNews, 1, latestNewsAmt, BusinessLogic_v3.Enums.CONTENT_PAGE_SORT_BY.DateDescending, out totalResults).ToFrontendBaseList();
                if (articles != null && articles.Count() > 0)
                {
                    int articlesCount = articles.Count();
                    var showMoreNewsAnchor = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(CS.General_v3.Enums.SETTINGS_ENUM.LatestNews_ShowViewMoreAnchor);
                    if(showMoreNewsAnchor)
                    {
                        var parentCount = articleNews.GetChildArticles().Count();
                        if (parentCount > latestNewsAmt) { anchorViewMore.HRef = articleNews.GetUrl(); }
                        else
                        {
                            CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divViewMore);
                        }
                    }
                    else
                    {
                        CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divViewMore);
                    }

                    int count = 0;
                    foreach (var article in articles)
                    {
                        count++;
                        createListItem(article, count%articlesCount == 0, articles.Count() == count);
                    }
                }
                else
                {
                    this.Visible = false;
                }
            }
        }

        private void createListItem(ArticleBaseFrontend article, bool isEven, bool isLastItem)
        {
            var ctrl = LatestNewsItem.LoadControl(this.Page);
            ctrl.Functionality.Article = article;
            ctrl.Functionality.IsLastItem = isLastItem;
            ctrl.Functionality.IsEven = isEven;
            ulLatestNews.Controls.Add(ctrl);
        }
    }
}