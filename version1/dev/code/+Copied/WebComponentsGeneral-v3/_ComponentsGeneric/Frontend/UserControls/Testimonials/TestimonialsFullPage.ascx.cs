﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Classes.URL;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Testimonials
{
    public partial class TestimonialsFullPage : BaseUserControl
    {
        private ArticleBaseFrontend _article;
        private URLParserTestimonialListing _urlParser;
        public class FUNCTIONALITY
        {
            private TestimonialsFullPage _control;
            public FUNCTIONALITY(TestimonialsFullPage control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public TestimonialsFullPage()
        {
            this.Functionality = createFunctionality();
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadArticle();
            if(_article != null)
            {
                initUrlParser();
                initItems();
            }
        }

        private void initUrlParser()
        {
            _urlParser = new URLParserTestimonialListing(_article);
        }
        private void initItems()
        {
            contentPage.Functionality.ContentPage = _article;

            var items = BusinessLogic_v3.Modules.Factories.TestimonialFactory.GetTestimonials(_urlParser.PageNo.GetValue(), (int)_urlParser.ShowAmt.GetValue(), _urlParser.SortBy.GetValue());
            if (items != null && items.Count() > 0)
            {
                testimonials.Functionality.Testimonials = items.ToFrontendBaseList();
                initPagingBar(items.TotalResults);
            }
            else
            {
                divNoResultsText.Visible = true;
                var noResultsTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Tesimonials_NoResultsText).ToFrontendBase();
                MyContentText.AttachContentTextWithControl(divNoResultsText, noResultsTxt);

                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(pagingBarBottom);
            }
        }

        private void initPagingBar(int totalResults)
        {
            pagingBarBottom.Functionality.UpdateValues(_urlParser, totalResults, true);
            pagingBarBottom.Functionality.ResultsText.Functionality.UpdateFromPagingInfo(_urlParser, totalResults);
        }

        private void loadArticle()
        {
            _article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.Testimonials).ToFrontendBase();
        }
    }
}