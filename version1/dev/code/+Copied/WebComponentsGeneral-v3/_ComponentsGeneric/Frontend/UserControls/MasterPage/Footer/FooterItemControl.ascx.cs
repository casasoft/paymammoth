﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Modules.ContentTextModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.Footer
{
    public partial class FooterItemControl : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private FooterItemControl _control;
            public FUNCTIONALITY(FooterItemControl control)
            {
                _control = control;
            }

            public ContentTextBaseFrontend FooterItemTitle { get; set; }
            public Control FooterItem { get; set; }
            public string CssClass { get; set; }
            public bool DoNotShow { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public FooterItemControl()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if (this.Functionality.FooterItemTitle != null)
            {
                MyContentText.AttachContentTextWithControl(footerItemTitle, this.Functionality.FooterItemTitle);
                footerItemTitle.Attributes["class"] += " footer-content-title";
            }
            else
            {
                footerItemTitle.Visible = false;
            }

            if (this.Functionality.FooterItem != null)
            {
                footerItemContent.Controls.Add(this.Functionality.FooterItem);
            }

            if(string.IsNullOrEmpty(this.Functionality.CssClass))
            {
                footerItemTitle.Attributes["class"] += " " + this.Functionality.CssClass;
            }
        }
    }
}