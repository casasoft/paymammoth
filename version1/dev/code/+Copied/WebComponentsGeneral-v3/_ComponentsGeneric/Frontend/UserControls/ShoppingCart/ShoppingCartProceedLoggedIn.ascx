﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartProceedLoggedIn.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.ShoppingCartProceedLoggedIn" %>
<div class="shopping-cart-proceed-container shopping-cart-proceed-logged-in-container clearfix"
    id="divShoppingCartProceedContainer" runat="server">
    <div class="proceed-checkout-container" id="divCartContent" runat="server">
        <div class="proceed-checkout-text" runat="server" id="divProceedToCheckoutText">
        </div>
        <div class="proceed-checkout-buttons-container">
            <a href="/" class="button proceed-checkout-button" runat="server" id="anchorProceedToCheckOut">Proceed to Checkout</a>
        </div>
    </div>
    <div class="proceed-to-payment-checkout" id="divCheckoutContent" runat="server">
        <div class="proceed-to-online-payment-container">
            <h2 id="hOnlinePayment" runat="server">
                Online Payment</h2>
            <div class="proceed-to-online-payment-text" runat="server" id="divProceedPaymentText">
            </div>
            <div class="proceed-to-online-payment-buttons-container">
                <CSControls:MyButton runat="server" ID="btnProceedToPayment" Visible="false">
                </CSControls:MyButton>
            </div>
        </div>
        <div class="proceed-to-paylater-container" id="divProceedToPayLater" runat="server">
            <h2 id="hPayLater" runat="server">
                Pay Later</h2>
            <div class="proceed-to-paylater-text" runat="server" id="divPayLaterText">
            </div>
            <div class="proceed-to-paylater-buttons-container">
                <CSControls:MyButton runat="server" ID="btnPayLater">
                </CSControls:MyButton>
            </div>
        </div>
    </div>
</div>
