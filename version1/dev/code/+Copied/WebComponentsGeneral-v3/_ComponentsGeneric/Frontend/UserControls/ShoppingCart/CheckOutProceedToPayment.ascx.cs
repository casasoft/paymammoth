﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart
{
    public partial class CheckOutProceedToPayment : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private CheckOutProceedToPayment _control;
            public FUNCTIONALITY(CheckOutProceedToPayment control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public CheckOutProceedToPayment()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initHandlers();
            updateTexts();
        }

        private void initHandlers()
        {
            btnProceedToPayment.Click += new EventHandler(btnProceedToPayment_Click);
        }

        void btnProceedToPayment_Click(object sender, EventArgs e)
        {

        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(divProceedToPaymentText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_ProceedToPaymentText).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(btnProceedToPayment, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_ProceedToPayment).ToFrontendBase());
        }
    }
}