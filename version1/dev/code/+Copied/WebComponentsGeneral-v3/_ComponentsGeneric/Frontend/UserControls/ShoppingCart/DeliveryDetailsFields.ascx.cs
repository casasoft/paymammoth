﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3;
using BusinessLogic_v3.Frontend.MemberModule;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules.ShoppingCartModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart
{
    public partial class DeliveryDetailsFields : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private DeliveryDetailsFields _control;
            public ContentTextBaseFrontend LblAddress1;
            public ContentTextBaseFrontend LblAddress2;
            public ContentTextBaseFrontend LblLocality;
            public ContentTextBaseFrontend LblPostcode;
            public ContentTextBaseFrontend LblCountry;
            public ContentTextBaseFrontend LblBtnProceed;
            public List<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166> ExcludedCountries { get; private set; }
            public MemberBaseFrontend Member;
            public string lblBtnProceedText;
            public FUNCTIONALITY(DeliveryDetailsFields control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public DeliveryDetailsFields()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadFormFieldsLabelsContentText();
            loadMemberIfNull();
            loadFormFields();
        }

        private void loadMemberIfNull()
        {
            if (Functionality.Member == null)
            {
                IMemberBase m = Factories.MemberFactory.GetLoggedInUserForCurrentSession();
                if (m != null)
                {
                    Functionality.Member = m.ToFrontendBase();
                }
            }
        }

        private void loadFormFieldsLabelsContentText()
        {
            if (Functionality.LblAddress1 == null)
            {
                Functionality.LblAddress1 =
                    Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.ShoppingCart_DeliveryPage_Address1).ToFrontendBase();
            }

            if (Functionality.LblAddress2 == null)
            {
                Functionality.LblAddress2 =
                    Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.ShoppingCart_DeliveryPage_Address2).ToFrontendBase();
            }

            if (Functionality.LblLocality == null)
            {
                Functionality.LblLocality =
                    Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.ShoppingCart_DeliveryPage_Locality).ToFrontendBase();
            }

            if (Functionality.LblPostcode == null)
            {
                Functionality.LblPostcode =
                    Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.ShoppingCart_DeliveryPage_PostCode).ToFrontendBase();
            }

            if (Functionality.LblCountry == null)
            {
                Functionality.LblCountry =
                    Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.ShoppingCart_DeliveryPage_Country).ToFrontendBase();
            }

            if (string.IsNullOrEmpty(Functionality.lblBtnProceedText) && Functionality.LblBtnProceed == null)
            {
                Functionality.LblBtnProceed =
                    Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.ShoppingCart_DeliveryPage_ButtonProceed).ToFrontendBase();
                Functionality.lblBtnProceedText = Functionality.LblBtnProceed.GetContent();
            }
            if (string.IsNullOrEmpty(Functionality.lblBtnProceedText) && Functionality.LblBtnProceed != null)
            {
                Functionality.lblBtnProceedText = Functionality.LblBtnProceed.GetContent();
            }
        }

        private MyTxtBoxString _txtAddress1;
        private MyTxtBoxString _txtAddress2;
        private MyTxtBoxString _txtLocality;
        private MyTxtBoxString _txtPostcode;
        private MyDropDownList _cmbCountry;
        private MyButton _btnSubmit;

        private void loadFormFields()
        {
            formFields.Functionality.ValidationGroup = "deliveryDetails";
            _txtAddress1 = (MyTxtBoxString)formFields.Functionality.AddFieldItem(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    titleContentText = Functionality.LblAddress1,
                    required = true,
                    id = "deliveryDetails_Address1"
                },
                InitialValue = Functionality.Member != null ? Functionality.Member.Data.Address1 : null,
                AutoFocus = true
            });

            _txtAddress2 = (MyTxtBoxString)formFields.Functionality.AddFieldItem(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    titleContentText = Functionality.LblAddress2,
                    id = "deliveryDetails_Address2"
                },
                InitialValue = Functionality.Member != null ? Functionality.Member.Data.Address2 : null,
            });

            _txtLocality = (MyTxtBoxString)formFields.Functionality.AddFieldItem(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    titleContentText = Functionality.LblLocality,
                    required = true,
                    id = "deliveryDetails_Locality"
                },
                InitialValue = Functionality.Member != null ? Functionality.Member.Data.Locality : null,
            });

            _txtPostcode = (MyTxtBoxString)formFields.Functionality.AddFieldItem(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    titleContentText = Functionality.LblPostcode,
                    required = true,
                    id = "deliveryDetails_Postcode"
                },
                InitialValue = Functionality.Member != null ? Functionality.Member.Data.PostCode : null,
            });

            string initialCountryCode = null;

            //Country 
            if (this.Functionality.Member != null && this.Functionality.Member.Data.Country.HasValue)
            {
                initialCountryCode = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To3LetterCode(this.Functionality.Member.Data.Country.Value);
            }
            if (string.IsNullOrEmpty(initialCountryCode))
            {
                var countryGeoIPCode = CS.General_v3.Util.GeoIpUtil.GetIPLocationCountryCode();
                if (countryGeoIPCode.HasValue)
                {
                    initialCountryCode = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To3LetterCode(countryGeoIPCode.Value);
                }
            }
            _cmbCountry = (MyDropDownList)formFields.Functionality.AddFieldItem(new MyDropDownList()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "deliveryDetails_Country",
                    required = true,
                    titleContentText = Functionality.LblCountry
                },
                InitialValue = initialCountryCode
            });

            var countriesList = CS.General_v3.Util.Data.GetCountriesAsListWith3LetterCodes(this.Functionality.ExcludedCountries);
            _cmbCountry.Functionality.AddItemsFromListItemCollection(countriesList);

            //SUBMIT
            _btnSubmit = formFields.Functionality.AddButton(Functionality.lblBtnProceedText);
            _btnSubmit.Click += new EventHandler(_btnSubmit_Click);
        }

        void _btnSubmit_Click(object sender, EventArgs e)
        {
            ShoppingCartDeliveryDetailsHelper deliveryDetails = new ShoppingCartDeliveryDetailsHelper();
            deliveryDetails.Address1 = _txtAddress1 != null && !_txtAddress1.ReadOnly.GetValueOrDefault() ? _txtAddress1.GetFormValueAsString() : null;
            deliveryDetails.Address2 = _txtAddress2 != null && !_txtAddress2.ReadOnly.GetValueOrDefault() ? _txtAddress2.GetFormValueAsString() : null;
            deliveryDetails.Postcode = _txtPostcode != null && !_txtPostcode.ReadOnly.GetValueOrDefault() ? _txtPostcode.GetFormValueAsString() : null;
            deliveryDetails.Locality = _txtLocality != null && !_txtLocality.ReadOnly.GetValueOrDefault() ? _txtLocality.GetFormValueAsString() : null;

            CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? countryCode = null;
            if (_cmbCountry != null)
            {
                countryCode = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(_cmbCountry.GetFormValueAsString());
                if(countryCode.HasValue)
                {
                    deliveryDetails.Country = countryCode.Value;
                }
            }
            
            // Create Session
            Session.Add(BusinessLogic_v3.Constants.SessionIdentifiers.CHECKOUT_DELIVERY_DETAILS, deliveryDetails);
            
            // Redirect to Check Out
            var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.CheckOut).ToFrontendBase();
            if(article != null)
            {
                CS.General_v3.Util.PageUtil.RedirectPage(article.GetUrl());
            }
        }
    }
}