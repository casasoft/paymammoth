﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.AdvertSlotModule;
using CS.General_v3;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Advert
{
    public partial class AdvertSlot : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private AdvertSlot _control;

            public int Width { get; set; }
            public int Height { get; set; }
            public Enum Identifier { get; set; }
            public bool DisableAdvertNote { get; set; }
            public string AnalyticsID { get; set; }
            public string AnalyticsSiteDomain { get; set; }
            public FUNCTIONALITY(AdvertSlot control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public AdvertSlot()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initAdvert();
        }

        private void initAdvert()
        {
            if(this.Functionality.Identifier != null)
            {

            AdvertSlotBaseFrontend adSlot = BusinessLogic_v3.Modules.Factories.AdvertSlotFactory.GetAdvertSlotByGeneralIdentifier(Functionality.Identifier).ToFrontendBase();
            
            if (adSlot == null || (!adSlot.Data.HasAdvertsToShow()))
            {
                iFrame.Visible = false;
            }
            else
            {
                iFrame.Style.Add(HtmlTextWriterStyle.Width, this.Functionality.Width + "px");
                iFrame.Style.Add(HtmlTextWriterStyle.Height, this.Functionality.Height + "px");
                iFrame.Attributes["src"] = BusinessLogic_v3.Constants.Urls.ADVERT_SLOT_PAGE_COMMON_RELATIVE_PATH + "?" + Constants.QS_IDENTIFIER_PARAM + "=" +
                                           this.Functionality.Identifier + "&" + Constants.QS_ADVERT_DISABLE_AD_NOTE_PARAM + "=" +
                                           this.Functionality.DisableAdvertNote.ToString().ToLower() + "&" + Constants.QS_GOOGLE_ANALYTICS_ID_PARAM + "=" +
                                           Functionality.AnalyticsID + "&" + Constants.QS_GOOGLE_ANALYTICS_DOMAIN_PARAM + "=" + Functionality.AnalyticsSiteDomain;
            }
            }
        }

        public bool DisableAdvertNote { get { return this.Functionality.DisableAdvertNote; } set { this.Functionality.DisableAdvertNote = value; } }
        public int Width { get { return this.Functionality.Width; } set { this.Functionality.Width = value; } }
        public int Height { get { return this.Functionality.Height; } set { this.Functionality.Height = value; } }
        public Enum Identifier { get { return this.Functionality.Identifier; } set { this.Functionality.Identifier = value; } }
        public string AnalyticsID { get { return this.Functionality.AnalyticsID; } set { this.Functionality.AnalyticsID = value; } }
        public string AnalyticsSiteDomain { get { return this.Functionality.AnalyticsSiteDomain; } set { this.Functionality.AnalyticsSiteDomain = value; } }
    }
}