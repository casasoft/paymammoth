﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductImagesSlider.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.ProductImagesSlider" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Carousel/CarouFredSel/CarouFredSelImageGallery.ascx"
    TagName="CarouFredSelImageGallery" TagPrefix="Common" %>
<div class="product-description-image-slider clearfix" runat="server" id="divProductImageSlider">
    <Common:CarouFredSelImageGallery ID="productImageSlider" runat="server" />
    <div class="product-description-image-slider-text" runat="server" id="divProductImageSliderText"></div>
    <asp:PlaceHolder ID="placeholderJS" runat="server"></asp:PlaceHolder>
</div>
