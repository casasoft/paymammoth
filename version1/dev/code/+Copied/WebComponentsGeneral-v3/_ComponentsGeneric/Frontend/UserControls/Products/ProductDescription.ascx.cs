﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Cms.Classes;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ProductModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    public partial class ProductDescription : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private ProductDescription _control;
            public FUNCTIONALITY(ProductDescription control)
            {
                _control = control;
            }

            private ProductBaseFrontend _product;
            public ProductBaseFrontend Product
            {
                get { return _product; }
                set
                {
                    _product = value;
                    _control.CacheParams.SetCacheDependencyFromDBObjects(_product.Data, true);
                }
            }
        }

        public FUNCTIONALITY Functionality { get; private set; }
        public ProductDescription()
        {
            this.Functionality = createFunctionality();
            this.CacheParams.CacheControl = false;
            this.CacheParams.IsMultilingual = true;
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateContent();
            showAddThis();
        }

        private void showAddThis()
        {
            bool showDescriptionAddThis = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_ShowDescriptionAddThis);
            if (showDescriptionAddThis)
            {
                addThisCtrl.Functionality.URL = this.Functionality.Product.Data.GetFrontendUrl();
            }
            else
            {
                divDescriptionAddThis.Visible = false;
            }
        }

        private void updateContent()
        {
            if(this.Functionality.Product != null)
            {
                updateDescription();
                updateMaterial();
                updateColours();
            }
        }

        private void updateColours()
        {
            bool showColoursInFullProductPage = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Products_FullPage_ShowAvailableColours);
            if (showColoursInFullProductPage)
            {
                var cntTxtTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_Description_AvailableColours).ToFrontendBase();
                MyContentText.AttachContentTextWithControl(divProductAvailableColoursTitle, cntTxtTitle);

                var productVariationList = this.Functionality.Product.Data.ProductVariations.ToList();
                if (productVariationList != null && productVariationList.Count() > 0)
                {
                    var sortedList = productVariationList.SortProductVariationsByColorTitle();
                
                    MyUnorderedList ulColors = new MyUnorderedList();
                    ulColors.CssManager.AddClass("product-colours-list");
                    foreach (var item in sortedList)
                    {

                        string colour = item.Colour != null ? item.Colour.Title : null;
                        if (string.IsNullOrEmpty(colour))
                        {
                            var noColoursCntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_Description_NoAvailableColours);
                            colour = noColoursCntTxt.GetContent();
                        }
                        MyListItem li = new MyListItem();
                        li.CssManager.AddClass("product-colour-item");
                        MySpan spanColourText = new MySpan();
                        spanColourText.InnerText = colour;
                        spanColourText.CssManager.AddClass("product-colour-item-text");
                        MySpan spanColourIcon = new MySpan();
                        spanColourIcon.CssManager.AddClass("product-colour-item-icon");
                            spanColourIcon.Style.Add(HtmlTextWriterStyle.BackgroundColor, item.Colour != null && !string.IsNullOrWhiteSpace(item.Colour.HexColor) ? item.Colour.HexColor : colour);
                        li.Controls.Add(spanColourText);
                        li.Controls.Add(spanColourIcon);
                        ulColors.Controls.Add(li);
                    }
                    divProductAvailableColours.Controls.Add(ulColors);
                    /*var strColours = CS.General_v3.Util.ListUtil.JoinList(colours, ", ");
                
                    strColours = strColours.Trim();
                    if (!CS.General_v3.Util.HtmlUtil.IsHtml(strColours))
                    {
                        strColours = "<p>" + strColours + "</p>";
                    }
                    divProductAvailableColours.InnerHtml = strColours;*/
                }
                else
                {
                    divProductAvailableColoursContainer.Visible = false;
                }
            }
            else
            {
                divProductAvailableColoursContainer.Visible = false;
            }
        }

        private void updateMaterial()
        {
            var cntTxtTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_Description_MaterialTitle).ToFrontendBase();
            MyContentText.AttachContentTextWithControl(divProductMaterialTitle, cntTxtTitle);

            string material = this.Functionality.Product.Data.Material;
            if(!string.IsNullOrEmpty(material))
            {
                if (!CS.General_v3.Util.HtmlUtil.IsHtml(material))
                {
                    material = "<p>" + material + "</p>";
                }
                divProductMaterial.InnerHtml = material;
                CMSInlineEditingItem productMaterial = CMSInlineEditingItem.Create(this.Functionality.Product.Data, x => x.Material, (IAttributeAccessor)this.divProductMaterial, textType: Enums.INLINE_EDITING_TEXT_TYPE.HtmlWithAtLeastParagraphTag);
            }
            else
            {
                divProductMaterialContainer.Visible = false;
            }
        }

        private void updateDescription()
        {
            bool showDescTitle = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Products_FullPage_ShowDescriptionTitle);
            if (showDescTitle)
            {
                var cntTxtTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_Description_DescriptionTitle).ToFrontendBase();
                MyContentText.AttachContentTextWithControl(divProductDescriptionTitle, cntTxtTitle);
            }
            else
            {
                divProductDescriptionTitle.Visible = false;
            }

            string desc = this.Functionality.Product.Data.GetDescription(false);
            if (desc != null)
            {
                desc = desc.Trim();
                if (!CS.General_v3.Util.HtmlUtil.IsHtml(desc))
                {
                    desc = "<p>" + desc + "</p>";
                }
                CMSInlineEditingItem productDesc = CMSInlineEditingItem.Create(this.Functionality.Product.Data, x => x.Description, (IAttributeAccessor)this.divProductDesc, textType: Enums.INLINE_EDITING_TEXT_TYPE.HtmlWithAtLeastParagraphTag);
                divProductDesc.InnerHtml = desc;
            }
        }
    }
}