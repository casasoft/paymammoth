﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericInvoice.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Orders.Invoice.GenericInvoice" %>
<%@ Register TagPrefix="CSControls" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common" %>
<div class="invoice-container">
    <div class="invoice-online-payment-container" id="divOnlinePayment" runat="server">
        <p>
            This invoice has not yet been paid. You can <a href='#' id="aPayOnline" runat="server">
                pay your invoice online</a>.</p>
    </div>
    <div class="invoice-heading" id="divHeading" runat="server">
        <div class="invoice-heading-left">
            <CSControls:MyImage ImageUrl="/images/logo.png" ID="imgLogo" runat="server" />
        </div>
        <div class="invoice-heading-right">
            <div class="invoice-address">
                <address id="businessAddress" runat="server">
                    6, Spencer Flats, Flat 4<br />
                    Triq Dun Gorg Preca,<br />
                    Hamrun, HMR 1605,<br />
                    Malta
                </address>
            </div>
            <div class="invoice-tel" id="businessTelContainer" runat="server">
                <span class="invoice-tel-label" id="businessTelLabel" runat="server">Tel</span>:
                <address id="businessTel" runat="server">
                    +356 21662204</address>
            </div>
            <div class="invoice-mob" id="businessMobContainer" runat="server">
                <span class="invoice-mob-label" id="businessMobLabel" runat="server">Mob</span>:
                <address id="businessMob" runat="server">
                    +356 21662204</address>
            </div>
            <div class="invoice-fax" id="businessFaxContainer" runat="server">
                <span class="invoice-fax-label" id="businessFaxLabel" runat="server">Fax</span>:
                <address id="businessFax" runat="server">
                    +356 21662204</address>
            </div>
            <div class="invoice-email" id="businessEmailContainer" runat="server">
                <span class="invoice-email-label" id="businessEmailLabel" runat="server">Email</span>:
                <address id="businessEmail" runat="server">
                    <CSControls:MyEmail ID="contactEmail" runat="server" Email="info@discountpages.com.mt">
                    </CSControls:MyEmail>
                </address>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="invoice-details-container">
        <div class="invoice-details-top-container">
            <h1 class="invoice-heading-title" id="invoiceHeadingTitle" runat="server">
                Invoice</h1>
            <h2 class="invoice-heading-subtitle" id="invoiceHeadingSubtitle" runat="server">
                0002222</h2>
            <div class="clear">
            </div>
        </div>
        <div class="invoice-details-middle-container">
            <div class="invoice-details-client-details-container">
                <div class="client-details-title" id="clientDetailsTitle" runat="server">
                    Client Details</div>
                <div class="client-details-container">
                    <p class="client-details-name" id="clientName" runat="server">
                        Joe Borg
                    </p>
                    <p class="client-details-address" id="clientAddress" runat="server">
                        Rainbow Flat,<br />
                        Dun Gorg Street,<br />
                        Hamrun, HMR 1234,<br />
                        Malta
                    </p>
                </div>
            </div>
            <div class="invoice-details-invoice-details-container">
                <div class="invoice-details-title" id="invoiceDetailsTitle" runat="server">
                    Invoice Details</div>
                <div class="invoice-details-table-container">
                    <table class="invoice-details-table" cellpadding="0" cellspacing="0">
                        <tr>
                            <th id="invoiceNoHeaderCell" runat="server">
                                Invoice No.:
                            </th>
                            <td id="invoiceNoCell" runat="server">
                                0002222
                            </td>
                        </tr>
                        <tr>
                            <th id="invoiceDateHeaderCell" runat="server">
                                Date:
                            </th>
                            <td id="invoiceDateCell" runat="server">
                                11/10/2011 03:34
                            </td>
                        </tr>
                        <tr class='invoice-row-paid'>
                            <th id="thPaid" runat="server">
                                Paid:
                            </th>
                            <td id="tdPaid" runat="server">
                                Yes
                            </td>
                        </tr>
                        <tr class='invoice-row-paid-on' id="trPaidOn" runat="server">
                            <th id="thPaidOn" runat="server">
                                Paid On:
                            </th>
                            <td id="tdPaidOn" runat="server">
                                11/10/2011 03:34
                            </td>
                        </tr>
                        <tr class='invoice-row-payment-method' id="trPaymentMethod" runat="server">
                            <th id="thPaymentMethod" runat="server">
                                Payment Method:
                            </th>
                            <td id="tdPaymentMethod" runat="server">
                                Online
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <div class="invoice-items-container">
        <div class="invoice-items-title-container">
            <h2 class="invoice-items-title" id="invoiceItemsTitle" runat="server">
                Invoice Details</h2>
        </div>
        <div class="invoice-items-content-container" id="divItemsContentContainer" runat="server">
        </div>
        <div class="invoice-items-bottom-container">
            <div class="invoice-items-terms-container" id="paymentTermsContainer" runat="server">
                <div class="invoice-items-terms-title" id="paymentTermsTitle" runat="server">
                    Payment Terms</div>
                <div class="invoice-itmes-terms-content-container" id="paymentTermsContent" runat="server">
                    Payment due within 30 days.
                </div>
            </div>
            <div class="invoice-items-total-container">
                <table cellpadding="0" cellspacing="0" class="invoice-items-total-table">
                    <tr>
                        <th id="subtotalHeaderCell" runat="server">
                            Subtotal:
                        </th>
                        <td id="subtotalCell" runat="server" class="invoice-items-total-value-cell">
                            &euro;22.34
                        </td>
                    </tr>
                    <tr id="trDiscount" runat="server">
                        <th id="discountHeaderCell" runat="server">
                            Discount:
                        </th>
                        <td id="discountCell" runat="server" class="invoice-items-total-value-cell">
                            &euro;22.34
                        </td>
                    </tr>
                    <tr id="trShipping" runat="server">
                        <th id="shippingHeaderCell" runat="server">
                            Shipping:
                        </th>
                        <td id="shippingCell" runat="server" class="invoice-items-total-value-cell">
                            &euro;22.34
                        </td>
                    </tr>
                    <tr class="invoice-items-total-row">
                        <th id="totalHeader" runat="server">
                            Total:
                        </th>
                        <td id="totalCell" runat="server" class="invoice-items-total-value-cell">
                            &euro;22.34
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="invoice-vat-summary" id="divVatSummary" runat="server">
        <h3 id="hVatSummaryTitle" runat="server">
            Vat Summary</h3>
        <table class="invoice-vat-summary-table">
            <thead>
                <tr>
                    <th id="thVatSummaryVatRate" runat="server" class='invoice-vat-summary-header-tax-rate'>
                        VAT Rate
                    </th>
                    <th id="thVatSummaryExcVAT" runat="server" class='invoice-vat-summary-header-net'>
                        Exc. VAT
                    </th>
                    <th id="thVatSummaryVatAmount" runat="server" class='invoice-vat-summary-header-taxes'>
                        VAT Amount
                    </th>
                    <th id="thVatSummaryIncVAT" runat="server" class='invoice-vat-summary-header-total'>
                        Inc. VAT
                    </th>
                </tr>
            </thead>
            <tbody id="tbodyVatSummary" runat="server">
                <tr>
                    <td>
                        VAT Rate
                    </td>
                    <td>
                        Exc. VAT
                    </td>
                    <td>
                        VAT Amount
                    </td>
                    <td>
                        Inc. VAT
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr class='invoice-vat-summary-totals'>
                    <td class='invoice-vat-summary-totals-label' id="tdVatSummaryTotalsLabel" runat="server">
                        Totals:
                    </td>
                    <td class='invoice-vat-summary-totals-net' id="tdVatSummaryTotalsNet" runat="server">
                        &euro;147.68
                    </td>
                    <td class='invoice-vat-summary-totals-taxes' id="tdVatSummaryTotalsTaxes" runat="server">
                        &euro;147.68
                    </td>
                    <td class='invoice-vat-summary-totals-total' id="tdVatSummaryTotalsTotal" runat="server">
                        &euro;147.68
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class='footer' id="divFooter" runat="server">
    </div>
</div>
