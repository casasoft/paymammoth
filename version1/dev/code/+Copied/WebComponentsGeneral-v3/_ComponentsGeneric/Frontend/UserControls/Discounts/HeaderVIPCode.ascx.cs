﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using CS.WebComponentsGeneralV3.Code.Util;
using log4net;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Discounts
{
    public partial class HeaderVIPCode : BaseUserControl
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(HeaderVIPCode));
        private MyTxtBoxString _txtVIPCode;
        private MyButton _btnDiscountApply;
        private const string VIP_CODE_VALIDATION_GROUP = "vipCodeValidationGroup";

        public class FUNCTIONALITY
        {
            private HeaderVIPCode _control;
            public FUNCTIONALITY(HeaderVIPCode control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public HeaderVIPCode()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            createFields();
        }

        private void createFields()
        {
            var g = vipCodeForm.Functionality.AddGroup(new CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields.FormFieldsItemGroupData()
            {
                Identifier = VIP_CODE_VALIDATION_GROUP,
                ValidationGroup = VIP_CODE_VALIDATION_GROUP
            });
            vipCodeForm.Functionality.LayoutProvider.DoNotShowValidationIcons = true;
            g.CssManager.AddClass("form-fields-vip-code");

            //VIPCode
            _txtVIPCode = (MyTxtBoxString)g.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "nHeading_VIPCode",
                    required = true,
                    titleContentText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MasterPage_AuthenticationUser_VIPCode_VIPCodeTitle).ToFrontendBase()
                },
            });
            var loggedInMember = BusinessLogic_v3.Modules.Factories.MemberFactory.GetLoggedInUserForCurrentSession();
            if (loggedInMember != null)
            {
                var disc = loggedInMember.GetCurrentDiscountCode();
                if (disc != null)
                {
                    _txtVIPCode.InitialValue = disc.VoucherCode;
                }
            }

            btnApplyDiscount.ValidationGroup = VIP_CODE_VALIDATION_GROUP;
            btnApplyDiscount.Click += new EventHandler(btnApplyDiscount_Click);
        }

        void btnApplyDiscount_Click(object sender, EventArgs e)
        {
            _log.Debug("btnApplyDiscount_Click() [START]");
            string discountCode = _txtVIPCode.GetFormValueAsString();
            _log.Debug("Discount Code <" + discountCode + ">");
            if (!string.IsNullOrEmpty(discountCode))
            {
                var voucherCode = BusinessLogic_v3.Modules.Factories.SpecialOfferVoucherCodeFactory.GetByVoucherCode(discountCode);
                _log.Debug("Voucher Code: " + voucherCode);

                if (voucherCode != null)
                {
                    _log.Debug("Setting in session");
                    BusinessLogic_v3.SessionData.MemberDiscountCode = discountCode;
                    _log.Debug("Set in session <" + BusinessLogic_v3.SessionData.MemberDiscountCode + ">");
                    
                    var loggedInMember = BusinessLogic_v3.Modules.Factories.MemberFactory.GetLoggedInUserForCurrentSession();
                    _log.Debug("Logged in member: " + loggedInMember);
                    if (loggedInMember != null)
                    {
                        var result = DiscountsUtil.AddDiscountCodeFromSession(loggedInMember.Email);
                        _log.Debug("Result: " + result);
                        switch (result)
                        {
                            case AddDiscountCodeResult.OK:
                                var successDiscountCode = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_ProfileFields_Discounts_Success);
                                this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(successDiscountCode.ToFrontendBase());
                                break;
                            case AddDiscountCodeResult.Invalid:
                                var errorDiscountCode = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_ProfileFields_Discounts_Error);
                                this.Page.Functionality.ShowErrorMessageAndRedirectToPage(errorDiscountCode.ToFrontendBase());
                                break;
                        }
                    }
                    else
                    {
                        var articleLogin = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_LoginPage);
                        var loginOrRegisterText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MasterPage_AuthenticationUser_Discount_NotLoggedIn);
                        this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(loginOrRegisterText.ToFrontendBase(), articleLogin.GetUrl());
                    }
                }
                else
                {
                    var errorDiscountCode = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Discounts_Error);
                    this.Page.Functionality.ShowErrorMessageAndRedirectToPage(errorDiscountCode.ToFrontendBase());
                }
            }
            else
            {
                var noDiscountCode = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Discounts_PleaseEnterADiscountCode);
                this.Page.Functionality.ShowErrorMessageAndRedirectToPage(noDiscountCode.ToFrontendBase());
            }
            _log.Debug("btnApplyDiscount_Click() [FINISH]");
        }
    }
}