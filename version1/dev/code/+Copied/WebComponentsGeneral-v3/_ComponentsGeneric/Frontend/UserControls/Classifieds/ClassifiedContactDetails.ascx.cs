﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ClassifiedModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using MyEmail = CS.General_v3.Controls.WebControls.Common.MyEmail;
using MyTable = CS.General_v3.Controls.WebControls.Common.MyTable;
using MyTableCell = CS.General_v3.Controls.WebControls.Common.MyTableCell;
using MyTableHeaderCell = CS.General_v3.Controls.WebControls.Common.MyTableHeaderCell;
using MyTableRow = CS.General_v3.Controls.WebControls.Common.MyTableRow;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Classifieds
{
    public partial class ClassifiedContactDetails : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private ClassifiedContactDetails _control;
            public FUNCTIONALITY(ClassifiedContactDetails control)
            {
                _control = control;
            }
            public ClassifiedBaseFrontend Classified { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ClassifiedContactDetails()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            updateTexts();
            base.OnLoad(e);
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(h2Title,
                                                       BusinessLogic_v3.Enums.CONTENT_TEXT.
                                                           Classifieds_ContactDetailsTitle.GetContentTextFrontend());
        }

        private void init()
        {
            if (this.Functionality.Classified != null)
            {
                initLoggedInMember();
            }
        }

        private void initLoggedInMember()
        {
            if(this.Functionality.Classified.Data.CreatedBy != null)
            {
                initContactInfo(this.Functionality.Classified.Data.CreatedBy);
            }
        }

        private void initContactInfo(BusinessLogic_v3.Modules.MemberModule.IMemberBase member)
        {
            MyTable tblInfo = new MyTable();

            MyTableRow trEmailRow = new MyTableRow();

            MyTableHeaderCell tdHeaderEmailCell = new MyTableHeaderCell();
            MyContentText.AttachContentTextWithControl(tdHeaderEmailCell,
                                                       BusinessLogic_v3.Enums.CONTENT_TEXT.
                                                           Classifieds_Comment_EmailAddress.GetContentTextFrontend());
            
            MyTableCell tdValueEmailCell = new MyTableCell();

            MyEmail emailAddress = new MyEmail
                                       {
                                           Email = member.Email
                                       };
            
            tdValueEmailCell.Controls.Add(emailAddress);
            trEmailRow.Controls.Add(tdHeaderEmailCell);
            trEmailRow.Controls.Add(tdValueEmailCell);
            tblInfo.Controls.Add(trEmailRow);

            if (!String.IsNullOrEmpty(member.Telephone))
            {
                MyTableRow trContactNumberRow = new MyTableRow();

                MyTableHeaderCell tdHeaderContactNumberCell = new MyTableHeaderCell();
                MyContentText.AttachContentTextWithControl(tdHeaderContactNumberCell,
                                                           BusinessLogic_v3.Enums.CONTENT_TEXT.
                                                               Classifieds_Comment_ContactNo.GetContentTextFrontend());
                
                MyTableCell tdValueContactNumberCell = new MyTableCell();
                tdValueContactNumberCell.InnerText = member.Telephone;
                trContactNumberRow.Controls.Add(tdHeaderContactNumberCell);
                trContactNumberRow.Controls.Add(tdValueContactNumberCell);
                tblInfo.Controls.Add(trContactNumberRow);
            }
            divTblWrapper.Controls.Add(tblInfo);
        }
    }
}