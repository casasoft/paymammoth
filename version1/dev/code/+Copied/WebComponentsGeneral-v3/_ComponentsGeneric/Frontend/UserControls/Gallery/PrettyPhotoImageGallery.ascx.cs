﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Classes.Interfaces.Gallery;
using CS.WebComponentsGeneralV3.Code.Cms.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Gallery
{
    public partial class PrettyPhotoImageGallery : BaseCmsUserControl
    {
        private const string PRETTY_PHOTO_IDENTIFER = "fullPageGallery";
        public class FUNCTIONALITY
        {
            private PrettyPhotoImageGallery _control;
            public bool IncludePrettyPhotoScripts { get; set; }
            public FUNCTIONALITY(PrettyPhotoImageGallery control)
            {
                _control = control;
            }
            public bool DoNotShowTitle { get; set; }
            public IEnumerable<IMediaItemData> Gallery { get; set;}
            public int LastItemPerRow { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public PrettyPhotoImageGallery()
        {
            this.Functionality = createFunctionality();
            this.Functionality.LastItemPerRow = 2;
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if(this.Functionality.Gallery != null && this.Functionality.Gallery.Count() > 0)
            {
                updateTexts();
                IEnumerable<IMediaItemData> gallery = this.Functionality.Gallery;
                int count = 0;
                foreach (IMediaItemData item in gallery)
                {
                    count++;
                    if ((count%this.Functionality.LastItemPerRow) == 0)
                    {
                        createImage(item, true);
                    }
                    else
                    {
                        createImage(item);
                    }
                }
                initJS();
            }
            else
            {
                this.Visible = false;
            }
        }

        private void createImage(IMediaItemData item, bool isLastItemInRow = false)
        {
            MyImage img = new MyImage();
            if(!string.IsNullOrEmpty(item.VideoLink) && CS.General_v3.Util.UrlUtil.CheckForYoutubeLinks(item.VideoLink))
            {
                var videoMediaItemDefaultThumbnail = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_DefaultThumbnailVideoMediaItem);
                img.ImageUrl = videoMediaItemDefaultThumbnail;
                img.HRef = item.VideoLink;
            }
            else
            {
                img.ImageUrl = item.ThumbnailImageUrl;
                img.HRef = item.LargeImageUrl;
            }
            img.AlternateText = item.Caption;
            img.Rel = "prettyPhoto[" + PRETTY_PHOTO_IDENTIFER + "]";
            if (isLastItemInRow)
            {
                img.CssManager.AddClass("image-gallery-last-item");
            }
            divImageGalleryContainer.Controls.Add(img);
        }

        private void updateTexts()
        {
            if(!this.Functionality.DoNotShowTitle)
            {
                var cntTxtTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ImageGallery_Title).ToFrontendBase();
                var cntTxtDesc = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ImageGallery_Desc).ToFrontendBase();
                MyContentText.AttachContentTextWithControl(spanGalleryTitle, cntTxtTitle);
                MyContentText.AttachContentTextWithControl(spanGalleryText, cntTxtDesc); 
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divGalleryText);
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(spanGalleryTitle);

            }
        
        }

        private void initJS()
        {
            PrettyPhotoUtil.InitJS();
        }
    }
}