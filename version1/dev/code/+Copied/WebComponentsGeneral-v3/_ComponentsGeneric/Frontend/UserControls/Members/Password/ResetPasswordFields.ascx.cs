﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Frontend.MemberModule;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;
using CS.WebComponentsGeneralV3.Code.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Password
{
    using Profile;

    public partial class ResetPasswordFields : BaseUserControl
    {
        private const string RESET_PASSWORD_VALIDATION_GROUP = "resetPasswordFields";

        private MyTxtBoxStringPassword _txtNewPassword;
        private MyTxtBoxStringPassword _txtConfirmpassword;
        private MyButton _btnSubmit;

        public class FUNCTIONALITY
        {
            private ResetPasswordFields _control;

            public ContentTextBaseFrontend LblNewPassword { get; set; }
            public ContentTextBaseFrontend LblConfirmpassword { get; set; }
            public string BtnSubmitText { get; set; }

            public string OnSuccessUrl { get; set; }

            public MemberBaseFrontend Member { get; set; }
            public string Code { get; set; }

            public FUNCTIONALITY(ResetPasswordFields control)
            {
                _control = control;

                //Labels
                this.BtnSubmitText = "Submit";

                //On Success Url 
                var articleSuccess =
                    BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                        BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_ResetPassword_Success).ToFrontendBase();
                if (articleSuccess != null)
                {
                    OnSuccessUrl = articleSuccess.Data.GetUrl();
                }
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }

        public ResetPasswordFields()
        {
            this.Functionality = createFunctionality();
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private void initResetPasswordFields()
        {
            formResetPassword.Functionality.ValidationGroup = RESET_PASSWORD_VALIDATION_GROUP;

            //Password
            _txtNewPassword = ProfileFields.GetPasswordField(Functionality.LblNewPassword, true);
            _txtNewPassword = (MyTxtBoxStringPassword)formResetPassword.Functionality.AddFieldItem(_txtNewPassword);

            _txtConfirmpassword = (MyTxtBoxStringPassword)formResetPassword.Functionality.AddFieldItem(new MyTxtBoxStringPassword()
            {
                FieldParameters = new FieldSingleControlPasswordParameters()
                {
                    titleContentText = this.Functionality.LblConfirmpassword,
                    required = true, showPasswordStrengthMeter = false,
                    id = "resetPasswordFields_txtConfirmPassword"
                },
                AutoFocus = true
            });

            ValidatorFieldGroup groupPassValidation = new ValidatorFieldGroup(FIELD_SUBGROUP_TYPE.SameValues, _txtNewPassword, _txtConfirmpassword);

            _btnSubmit = formResetPassword.Functionality.AddButton(this.Functionality.BtnSubmitText);
            _btnSubmit.Click += new EventHandler(_btnSubmit_Click);
        }

        void _btnSubmit_Click(object sender, EventArgs e)
        {
            string newPassword = _txtNewPassword.GetFormValueAsString();
            string confirmNewPassword = _txtConfirmpassword.GetFormValueAsString();

            if ((newPassword == confirmNewPassword) && (!string.IsNullOrWhiteSpace(newPassword)))
            {
                if (Functionality.Member != null && !String.IsNullOrEmpty(Functionality.Code))
                {
                    var result = Functionality.Member.Data.VerifyForgotPasswordUserCodeAndChange(Functionality.Code, newPassword);
                    if(result)
                    {
                        //OK
                        PageUtil.RedirectPage(Functionality.OnSuccessUrl);
                    }
                    else
                    {
                        PageUtil.RedirectPage(BusinessLogic_v3.Classes.Routing.ErrorsRoute.GetGenericErrorURL());
                    }
                }
                else
                {
                    PageUtil.RedirectPage(BusinessLogic_v3.Classes.Routing.ErrorsRoute.GetPageNotFoundErrorURL());
                }
            } 
        }

        protected override void OnLoad(EventArgs e)
        {
            updateTexts();
            initResetPasswordFields();
            base.OnLoad(e);
        }

        private void updateTexts()
        {
            this.Functionality.BtnSubmitText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Button_Submit).GetContent();
            this.Functionality.LblNewPassword = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ResetPassword_NewPassword).ToFrontendBase();
            this.Functionality.LblConfirmpassword = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ResetPassword_ConfirmPassword).ToFrontendBase();
        }
    }
}