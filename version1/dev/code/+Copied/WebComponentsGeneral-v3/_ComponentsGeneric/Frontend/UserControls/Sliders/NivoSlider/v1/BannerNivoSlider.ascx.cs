﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.BannerModule;
using BusinessLogic_v3.Modules.BannerModule;
using CS.General_v3.JavaScript.Data;
using CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Sliders.NivoSlider.Classes;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Sliders.NivoSlider.v1
{
    public partial class BannerNivoSlider : BaseUserControl
    {



        #region BannerNivoSlider Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected BannerNivoSlider _item = null;
            public NivoSlider NivoSlider { get { return _item.nivoSlider; } }
                internal FUNCTIONALITY(BannerNivoSlider item)
            {
                this._item = item;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion


        public BannerNivoSlider()
        {
            this.CacheParams.SetCacheDependencyFromFactories(true, BusinessLogic_v3.Modules.Factories.BannerFactory);
            this.CacheParams.CacheControl = false;
        }

        private NivoSliderImage initBanner(BannerBaseFrontend banner)
        {
            NivoSliderImage nivoSliderImg = new NivoSliderImage();
            var bannerUrl = banner.Data.GetBannerURL();
            if (!String.IsNullOrEmpty(bannerUrl))
            {
                if (!nivoSlider.Functionality.DoNotShowCaption)
                {
                    nivoSliderImg.Title = banner.Data.Title;
                }

                nivoSliderImg.AlternateText = banner.Data.Title;
                nivoSliderImg.URL = bannerUrl;

                if (!string.IsNullOrEmpty(banner.Data.Link))
                {
                    nivoSliderImg.Href = banner.Data.Link;
                }
                return nivoSliderImg;
            }
            return null;
        }
        private void populateImages()
        {
            var animEffect = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<General_v3.Enums.NIVO_SLIDER_EFFECTS?>(Enums.BusinessLogicSettingsEnum.General_Bannner_AnimationEffect);
            if(animEffect.HasValue)
            {
                nivoSlider.effect = animEffect.Value;
            }

            nivoSlider.Functionality.DoNotShowCaption = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.General_Bannner_DoNotShowCaption);

            var animSpeed = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(Enums.BusinessLogicSettingsEnum.General_Bannner_AnimationSpeedInMS);
            if (animSpeed != 0)
            {

                nivoSlider.AnimSpeed = animSpeed;
            }
            var pauseDuration = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(Enums.BusinessLogicSettingsEnum.General_Bannner_PauseDurationBetweenSlidesInMS);
            if (animSpeed != 0)
            {
                nivoSlider.PauseTime = pauseDuration;
            }
            IEnumerable<BannerBaseFrontend> listOfBanners = BannerBaseFactory.Instance.GetAllBannersSortedBy(BusinessLogic_v3.Enums.BANNERS_SORT_BY.PriorityAscending).ToFrontendBaseList();

            if (listOfBanners != null && listOfBanners.Count() > 0)
            {
                List<INivoSliderImage> list = new List<INivoSliderImage>();
                foreach (var banner in listOfBanners)
                {
                    var image = initBanner(banner);
                    if (image != null)
                    {
                        list.Add(image);
                    }
                }
                nivoSlider.Functionality.Images = list;
            }
        }
			

        protected override void OnLoad(EventArgs e)
        {
            populateImages();
            base.OnLoad(e);
        }
    }
}