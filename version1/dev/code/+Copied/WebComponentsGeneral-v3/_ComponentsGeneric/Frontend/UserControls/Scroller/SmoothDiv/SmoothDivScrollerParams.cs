﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Scroller.SmoothDiv
{
    using CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data;

    public class SmoothDivScrollerParams : JSONObject
    {
      

        /// <summary>
        /// The CSS class or id for the hotspot that triggers scrolling left.
        /// Default: "div.scrollingHotSpotLeft"
        /// </summary>
        public string scrollingHotSpotLeft;
        /// <summary>
        /// The CSS class or id for the hotspot that triggers scrolling right.
        /// Default: "div.scrollingHotSpotRight"
        /// </summary>
        public string scrollingHotSpotRight;
        /// <summary>
        /// The CSS class or id of the wrapper element that surrounds the scrollable area.
        /// Default: "div.scrollWrapper"
        /// </summary>
        public string scrollWrapper;	
        /// <summary>
        /// The CSS class or id of the actual element that is scrolled left or right.
        /// Default: "div.scrollableArea"
        /// </summary>
        public string scrollableArea;
        /// <summary>
        /// True or false. Determines whether the element should be visible or hidden on start. This can be useful if you combine Smooth Div Scroll with some other jQuery plugin that shows or hides areas, like an accordion.
        /// Default: "false"
        /// </summary>
        public bool? hiddenOnStart;
        /// <summary>
        /// Optional. If supplied, the content of scrollableArea is fetched via AJAX using the supplied URL.Default: ""
        /// Default: ""
        /// </summary>
        public string ajaxContentURL;
        /// <summary>
        ///Optional. If supplied, the function that calculates the total width of the scrollable area will only count the width of the elements that have this class. This can be useful if you have content grouped in columns and only the columns should be made scrollable. Otherwise Smooth Div Scroll will sum up the total width of all the elements in the columns and in this case you only want the width of the actual columns.
        /// Default: ""
        /// </summary>
        public string countOnlyClass;
        /// <summary>
        /// Optional. Determines how many pixels the scrollable area should move in each scrolling cycle when scrolling manually. High value means bigger steps which means faster scrolling. But always make sure you balance this value with the scrollInterval since bigger steps also means "choppier" scrolling.
        /// Default: "15"
        /// </summary>
        public int? scrollStep;
        /// <summary>
        /// Optional. Determines the elapsed time between each call to the function that does the manual scrolling. If you'd compare scrolling with an animated movie, this option would be the equivalent of the number of frames per second in the movie. For really slow manual scrolling you'd set scrollStep to 1 and scrollInterval to 100 (100 milliseconds = ten calls/frames per second). So a lower value means more calls per second and thus faster scrolling. A higher value means fewer calls per second and slower scrolling. Balance this option together with the scrollStep option to get the speed you want and at the same time a scrolling that is as smooth as possible.
        /// Default: "10"
        /// </summary>
        public string scrollInterval;
        /// <summary>
        /// Use this if you want a speed boost when the user presses the mouse button while hovering over one of the hotspots.
        /// 1 = normal speed (no boost)
        /// 2 is twice as fast as normal
        /// 3 is three times as fast 
        /// and so on. The default value is three times as fast.
        /// Default: "3"
        /// </summary>
        public string mouseDownSpeedBooster;
        /// <summary>
        /// Optional. Leave it out if you don't want any autoscrolling. Otherwise use the values "onstart" or "always". Using onstart the scrolling will start automatically after the page has loaded and scroll according to the method you've selected using the autoScrollDirection option. When the user moves the mouse over the left or right hotspot the autoscroll will stop. After that the scrolling will only be triggered by the hotspots. Using always - the hotspots are disabled alltogether and the scrollable area will only scroll automatically.
        /// Default: ""
        /// </summary>
        public string autoScroll;
        /// <summary>
        /// Optional. Determines how many pixels the scrollable area should move in each scrolling cycle. High value means bigger steps which means faster scrolling. But always make sure you balance this value with the autoScrollInterval since bigger steps also means "choppier" scrolling.
        /// Default: "5"
        /// </summary>
        public string autoScrollDirection;
        /// <summary>
        /// Optional. Leave it out if you don't want any autoscrolling. Otherwise use the values "onstart" or "always". Using onstart the scrolling will start automatically after the page has loaded and scroll according to the method you've selected using the autoScrollDirection option. When the user moves the mouse over the left or right hotspot the autoscroll will stop. After that the scrolling will only be triggered by the hotspots. Using always - the hotspots are disabled alltogether and the scrollable area will only scroll automatically.
        /// Default: ""
        /// </summary>
        public string autoScrollStep;
        /// <summary>
        /// Optional. Determines the elapsed time between each call to the autoscrolling function. If you'd compare scrolling with an animated movie, this option would be the equivalent of the number of frames per second in the movie. For really slow autoscrolling you'd set autoScrollStep to 1 and autoScrollInterval to 100 (100 milliseconds = ten calls/frames per second). So a lower value means more calls per second and thus faster scrolling. A higher value means fewer calls per second and slower scrolling. Again, try to balance this option together with the autoScrollStep option to get the speed you want and still get the scrolling as smooth as possible.
        /// Default: "10"
        /// </summary>
        public string autoScrollInterval;
        /// <summary>
        /// Optional. Leave it blank for invisible hotspots. Otherwise use the values "onstart" or "always". Onstart makes the hotspots visible for X-number of seconds (controlled via hotSpotsVisibleTime) directly after the page has loaded, then they become invisible. Always is for making them visible all the time. Feel free to alter the graphics and CSS for the visible hotspots. The CSS and graphics supplied with the plugin can serve as a good starting point.
        /// Default: ""
        /// </summary>
        public string visibleHotSpots;
        /// <summary>
        /// Optional. If you have set "onstart" as the value for the option visibleHotSpots, you set the number of seconds that you want the hotspots to be visible. After this time they will become invisible again.
        /// Default: "5"
        /// </summary>
        public string hotSpotsVisibleTime;
        /// <summary>
        /// Optional. Use this option if you want the offset of the scrollable area to be positioned at a specific element directly after the page has loaded. First give your element an ID in the HTML code and then provide this ID as a option.
        /// Default: ""
        /// </summary>
        public string startAtElementId;

    }
}