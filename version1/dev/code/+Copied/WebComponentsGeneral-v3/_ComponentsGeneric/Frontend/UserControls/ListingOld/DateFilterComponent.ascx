﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateFilterComponent.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.DateFilterComponent" %>
<div class="date-filter-formfields-wrapper clearfix">
    <CSControlsFormFields:FormFields ID="formFieldsFilterDateFrom" runat="server">
    </CSControlsFormFields:FormFields>
    <CSControlsFormFields:FormFields ID="formFieldsFilterDateTo" runat="server">
    </CSControlsFormFields:FormFields>
    <div class="date-filter-button-wrapper">
        <CSControls:MyButton runat="server" ID="btnSubmit" />
        <CSControls:MyButton CssClass="clear-button" runat="server" ID="btnClear" />
    </div>
</div>
