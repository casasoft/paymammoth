﻿using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Extensions;
namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage
{
    using System;
    using System.Web.UI;
    using BusinessLogic_v3.Modules.ArticleModule;
    using BusinessLogic_v3.Modules.CmsUserModule;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using System.Collections.Generic;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using BusinessLogic_v3.Classes.DbObjects;
    using CS.WebComponentsGeneralV3.Code.Cms.Classes;
    using CS.WebComponentsGeneralV3.Code.Classes.Pages;
    using BusinessLogic_v3.Classes.Text;
    using BusinessLogic_v3;
    using BusinessLogic_v3.Modules;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Navigation;

    public partial class ContentPageBase : BaseUserControl
    {
        /// <summary>
        /// The Template property for the Content area.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(INamingContainer))]
        [TemplateInstance(TemplateInstance.Single)] //This is so that inner child controls can be accessed.  If you can create multiple controls, then you cannot access this
        public ITemplate BottomContent { get; set; }
        /// <summary>
        /// The Template property for the Content area.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(INamingContainer))]
        [TemplateInstance(TemplateInstance.Single)] //This is so that inner child controls can be accessed.  If you can create multiple controls, then you cannot access this
        public ITemplate TopContent { get; set; }
        protected override void OnInit(EventArgs e)
        {
            if (BottomContent != null)
            {
                divExtraBottomContent.Visible = true;
                BottomContent.InstantiateIn(this.divExtraBottomContent); //Replace this with container where you want to place such controls
            } 
            if (TopContent != null)
            {
                divExtraTopContent.Visible = true;
                TopContent.InstantiateIn(this.divExtraTopContent); //Replace this with container where you want to place such controls
            }
            base.OnInit(e);
        }

        public bool IsTitleVisible
        {
            get { return this.Functionality.IsTitleVisible; }
            set { this.Functionality.IsTitleVisible = value; }
        }

        public class FUNCTIONALITY
        {
            private ContentPageBase _control;

            public NavigationBreadcrumbs NavigationBreadcrumbs { get { return _control.navigationBreadcrumbs; } }

            public FUNCTIONALITY(ContentPageBase control)
            {
                _control = control;
                this.ContentTagReplacementValues = new TokenReplacerNew();
                this.HeadingReplacementValues = new TokenReplacerNew();
                //Defaults
                DoNotShowNavigationBreadcrumbs = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_DoNotShowNavigationBreadcrumbs);
            }

            public string Title { get; set; }
            public ContentTextBaseFrontend TitleContentText { get; set; }

            public string HtmlText_ForEditing { get; set; }
            public ContentTextBaseFrontend HtmlText_ForEditingContentText { get; set; }

            public TokenReplacerNew ContentTagReplacementValues { get; set; }
            public TokenReplacerNew HeadingReplacementValues { get; set; }

            public System.Web.UI.HtmlControls.HtmlGenericControl H1PageTitle { get { return _control.h1PageTitle; } }
            public System.Web.UI.HtmlControls.HtmlGenericControl divHtmlContent { get { return _control.divHtmlContent; } }
            public Enums.ARTICLE_TYPE ArticleType { get; set; }

            public bool IsTitleVisible { get; set; }
            public bool IsWebsiteNameIncluded { get; set; }
            public bool DoNotUpdateForSEO { get; set; }
            public bool DoNotShowNavigationBreadcrumbs { get; set; }
            public bool DoNotShowFooter { get; set; }
            
        }

        public FUNCTIONALITY Functionality { get; private set; }
        public ContentPageBase()
        {
            this.Functionality = createFunctionality();
            this.IsTitleVisible = true;

            this.CacheParams.IsMultilingual = true;
            this.CacheParams.CacheControl = false;
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initLoadContentPage();
            updateForSEO();
            initNavigationBreadcrumbs();
        }

        private void initNavigationBreadcrumbs()
        {
            bool showNavBreadcrumbs = !this.Functionality.DoNotShowNavigationBreadcrumbs;
            if (!showNavBreadcrumbs)
            {
                divNavigationBreadcrumbs.Parent.Controls.Remove(divNavigationBreadcrumbs);
            }
        }

        private bool allRequirementsOk()
        {
            return !string.IsNullOrWhiteSpace(Functionality.Title) &&
                   !string.IsNullOrWhiteSpace(Functionality.HtmlText_ForEditing);
        }

        protected virtual void initLoadContentPage()
        {
            if (this.Visible)
            {
                if (allRequirementsOk())
                {
                    updateTexts();
                    h1PageTitle.Visible = IsTitleVisible;
                    if (!this.Functionality.DoNotShowFooter && this.Functionality.ArticleType != Enums.ARTICLE_TYPE.Events)
                    {
                        contentPageFooter.Functionality.ContentPage = Functionality.ContentPage;
                    }
                    else
                    {
                        contentPageFooter.RemoveControlFromParentContainer();
                    }
                    
                    divHtmlContent.InnerHtml = BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(this.Functionality.ContentPage.Data, this.Functionality.ContentTagReplacementValues);

                    var commonMasterPage = this.GetMasterPageOfType<BaseCommonMasterPage>();
                    if (commonMasterPage != null && commonMasterPage.Functionality.DbObjectToLinkWithEditInCmsButton == null)
                    {
                        commonMasterPage.Functionality.DbObjectToLinkWithEditInCmsButton = this.Functionality.ContentPage.Data;
                    }
                    initInlineEditing();
                    
                    var showAddThisInHeading = ModuleSettings.Articles.ShowAddThisInHeading;
                    if(showAddThisInHeading)
                    {
                        headingAddThis.Functionality.URL = this.Functionality.ContentPage.GetUrl();
                        headingAddThis.Functionality.Style = General_v3.Enums.ADDTHIS_STYLE.DefaultStyleSmall;
                        divHeadingAddThisWrapper.Visible = true;
                    }
                    else
                    {
                        CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divHeadingAddThisWrapper);
                    }
                }
                else
                {
                    throw new Exception("Content Page not found");
                }
            }
        }

        protected virtual void initInlineEditing()
        {
            new CMSInlineEditingItem(h1PageTitle, this.Functionality.ContentPage.Data, BusinessLogic_v3.Enums.CMS_INLINE_EDITING_TYPE.ArticleTitle, this.Functionality.ContentPage.Data.Title, this.Functionality.HeadingReplacementValues);
            new CMSInlineEditingItem(divHtmlContent, this.Functionality.ContentPage.Data, BusinessLogic_v3.Enums.CMS_INLINE_EDITING_TYPE.ArticleHtml, this.Functionality.ContentPage.Data.HtmlText, this.Functionality.ContentTagReplacementValues);
        }

        private void updateTexts()
        {
            h1PageTitle.InnerHtml = Functionality.ContentPage.Data.PageTitle;
            if(!string.IsNullOrEmpty(Functionality.ContentPage.Data.SubTitle))
            {
                string s = this.Functionality.ContentTagReplacementValues.ReplaceString(Functionality.ContentPage.Data.SubTitle);
                spanArticleSubtitle.InnerHtml =s;
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(spanArticleSubtitle);
            }

            var showMediaItemsInArticle = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_ShowImageGalleryForContentPages);
            if(!showMediaItemsInArticle)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(contentPageMediaItems);
            }
            else
            {
                contentPageMediaItems.Functionality.Article = this.Functionality.ContentPage;
            }

            var showMainImageInContentPage = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_ShowMainImageInContentPage);
            if (!showMainImageInContentPage)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divContentPageImage);
            }
            else
            {
                var imageUrl = this.Functionality.ContentPage.GetMainImageUrl();
                if (!string.IsNullOrEmpty(imageUrl))
                {
                    divContentPageContent.Attributes["class"] += " content-page-main-image-available";
                    imgItem.ImageUrl = imageUrl;
                }
                else
                {
                    CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divContentPageImage);
                }
            }
        }

        private void updateOpenGraphImage()
        {
            var imageURL = CS.General_v3.Util.PageUtil.GetAbsoluteImageURLFromHTML(Functionality.ContentPage.Data.GetContent());
            if (imageURL != null)
            {
                this.Page.Functionality.MetaTagsController.UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH.Image, imageURL);
            }
        }
        private void updateForSEO()
        {
            if (!this.Functionality.DoNotUpdateForSEO)
            {
                string websiteName = CS.General_v3.Settings.GetSettingFromDatabase<string>(
                    CS.General_v3.Enums.SETTINGS_ENUM.Others_WebsiteName);
                if (Functionality.ContentPage != null)
                {


                    this.Page.Functionality.UpdateTitleFromContentPage(Functionality.ContentPage);

                    updateOpenGraphImage();
                    this.Page.Functionality.MetaTagsController.UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH.Type,
                                                               CS.General_v3.Util.EnumUtils.StringValueOf(
                                                                   CS.General_v3.Enums.OPEN_GRAPH_TYPE.Company));
                    this.Page.Functionality.MetaTagsController.UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH.SiteName, websiteName);
                }
            }
        }
    }
}