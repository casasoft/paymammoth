﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductCategoriesListing.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.ProductCategoriesListing" %>

<%@ Register TagPrefix="Hierarchy" Namespace="CS.WebComponentsGeneralV3.Code.Controls.WebControls.Hierarchy" Assembly="WebComponentsGeneralV3" %>
<div class="product-categories-listing-container">
    <div class="product-categories-listing-container-title" runat="server" id="divItemTitle"></div>
    <Hierarchy:HierarchicalControlNavigation runat="server" ID="productCategories" CssClass="product-categories-listing"/>
</div>
<asp:PlaceHolder ID="ltlJavascript" runat="server">

</asp:PlaceHolder>