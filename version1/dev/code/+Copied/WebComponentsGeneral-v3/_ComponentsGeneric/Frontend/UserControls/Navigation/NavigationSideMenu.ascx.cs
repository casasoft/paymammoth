﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Classes.Interfaces.Hierarchy;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using IHierarchy = CS.General_v3.Classes.Interfaces.Hierarchy.IHierarchy;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Navigation
{
    public partial class NavigationSideMenu : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            public IHierarchyNavigation Item { get; set; }

            private NavigationSideMenu _control;
            public FUNCTIONALITY(NavigationSideMenu control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public NavigationSideMenu()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            initHierarchy();
            base.OnLoad(e);
        }

        private HierarchyNavigationImpl CreateHierarchicalDataFromContentPage(IHierarchyNavigation hierarchy)
        {
            HierarchyNavigationImpl hd = new HierarchyNavigationImpl();
            hd.Href = hierarchy.Href;
            hd.Title = hierarchy.Title;
            hd.Visible = true;
            return hd;
        }

        private IEnumerable<IHierarchyNavigation> createHierarchyLinks(IEnumerable<IHierarchyNavigation> items)
        {
            List<IHierarchyNavigation> list = new List<IHierarchyNavigation>();
            foreach (var item in items)
            {
                if (item.Visible)
                {
                    var hd = CreateHierarchicalDataFromContentPage(item);
                    if (item.ID == this.Functionality.Item.ID)
                    {
                        hd.Selected = true;
                        if (item.GetChildren().Count() > 0)
                        {
                            var subChildren = item.GetChildren();
                            foreach (var subChild in subChildren)
                            {
                                var hd2 = CreateHierarchicalDataFromContentPage(subChild);
                                hd.AddChild(hd2);
                            }
                        }
                    }
                    list.Add(hd);
                }
            }
            return list;
        }

        private void initSiblings(IHierarchyNavigation parent)
        {
            var children = parent.GetChildren();
            if (children != null)
            {
                var list = createHierarchyLinks(children);
                hierarchyLinks.Functionality.Items = list;
            }
        }

        private void initParent(IHierarchyNavigation parent)
        {
            if (parent.GetParents().FirstOrDefault() != null)
            {
                aParentTitle.HRef = parent.Href;
            }
            aParentTitle.InnerHtml = parent.Title;
        }

        private void initHierarchy()
        {
            //hierarchyLinks.Functionality.LastItemCssClass = "last-item";
            IHierarchyNavigation parent = null;
            if (this.Functionality.Item != null)
            {
                parent = this.Functionality.Item.GetMainParent();
            }
            if (parent != null)
            {
                initParent(parent);
                initSiblings(parent);
            }
        }
    }
}