﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.Paging
{
    using Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Classes.URL;

    public partial class PagingBar : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private PagingBar _control;

            public PagingPages PagesControl { get { return _control.pagingPages; } }
            public ListingResultsText ResultsText { get { return _control.resultsText; } }
            public PagingBarCombos Combos { get { return _control.pagingCombos; } }


            /*public void UpdatePagingValues(int pgSize, int pgNum, int totalResults, bool hideCombos = true)
            {
                this.PagesControl.Functionality.UpdateValues(pgNum, pgSize, totalResults);
                this.ResultsText.Functionality.UpdateValues(pgNum, pgSize, totalResults);
                this.Combos.Visible = !hideCombos;
                this.PagesControl.Visible = true;
            }
            public void UpdateComboValues<TEnumSortBy, TEnumShowAmt>(TEnumSortBy selectedSortByValue, TEnumShowAmt selectedShowAmtValue, bool hidePaging = true)
            {
                this.Combos.Functionality.UpdateValues<TEnumSortBy, TEnumShowAmt>(selectedSortByValue, selectedShowAmtValue);
                this.PagesControl.Visible = !hidePaging;
                this.Combos.Visible = true;
            }*/
            public void UpdateValues(IURLParserPagingInfo pagingInfo, int totalResults, bool showPages, bool showCombos = false)
            {
                this.PagesControl.Functionality.PagingInfo = pagingInfo;
                this.Combos.Functionality.PagingInfo = pagingInfo;
                this.PagesControl.Functionality.TotalPages = (int)Math.Ceiling((double)totalResults / (int)(object)pagingInfo.ShowAmount);
                this.PagesControl.Visible = showPages;
                this.Combos.Visible = showCombos;
                this.ResultsText.Functionality.UpdateFromPagingInfo(pagingInfo, totalResults);
            }
           
            public FUNCTIONALITY(PagingBar control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public PagingBar()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
    }
}