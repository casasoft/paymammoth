﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginFields.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login.LoginFields" %>
<div class="login-fields-container">
    <CSControlsFormFields:FormFields ID="formLogin" runat="server">
    </CSControlsFormFields:FormFields>    
    <div class="social-login-container html-container">
        <p runat="server" ID="pSocialLoginMessage"></p>
        <div class="social-login-buttons-container clearfix" id="divSocialLoginButtonsContainer" runat="server"></div>
    </div>
</div>
