﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using System.Reflection;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.MemberModule;
using CS.General_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage
{
    public partial class GeneralUserLoggedIn : BaseUserControl
    {
        private MemberBaseFrontend _member;

        public class FUNCTIONALITY
        {
            private GeneralUserLoggedIn _control;
            public FUNCTIONALITY(GeneralUserLoggedIn control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public GeneralUserLoggedIn()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            _member = BusinessLogic_v3.Modules.Factories.MemberFactory.GetLoggedInUserForCurrentSession().ToFrontendBase();
            if(_member != null)
            {
                initHandlers();
                initMenu();
                updateTexts();
            }
        }

        private void initMenu()
        {
            var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_MyAccount);
            if(article != null)
            {
                myAccountNavigation.Functionality.Items = article.GetChildArticles();
            }
        }

        private void initHandlers()
        {
            btnLogout.Click += new EventHandler(ButtonWebControlFunctionality_Click);
        }

        void ButtonWebControlFunctionality_Click(object sender, EventArgs e)
        {
            _member.Data.Logout();
            var logoutSuccess =  BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Logout_Success).ToFrontendBase();
            this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(logoutSuccess, "/");
        }

        private void updateTexts()
        {
            var cntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Logout).ToFrontendBase();
            MyContentText.AttachContentTextWithControl(btnLogout, cntTxt);
        }
    }
}