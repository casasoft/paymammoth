﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ProductModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Util;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
    using Code.Classes.Javascript.UI.Products;
    using General_v3.Classes.Interfaces.ImageItem;

    public partial class ProductImagesSlider2 : BaseUserControl
    {
        private List<Control> _ctrls;

        public class FUNCTIONALITY
        {
            private ProductImagesSlider2 _control;
            public ProductVariationControllerAnythingSliderParameters ProductVariationControllerAnythingSliderParameters { get; set; }
            public FUNCTIONALITY(ProductImagesSlider2 control)
            {
                _control = control;
            }

            public ProductBaseFrontend Product { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ProductImagesSlider2()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }
        private void sortImagesByProductVariation(List<IProductVariationMediaItemBase> images)
        {
            images.Sort((a, b) => a.ProductVariation.Colour.Title.CompareTo(b.ProductVariation.Colour.Title));

        }

        private void init()
        {
            updateTexts();
            bool showProductSlider = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_ShowProductImageSlider);
            if (showProductSlider)
            {
                if (this.Functionality.Product != null)
                {
                    _ctrls = new List<Control>();
                    var includeMainImageInSlider = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_IncludeMainImageInSlider);
                    List<IProductVariationMediaItemBase> images = this.Functionality.Product.Data.GetAllImages(includeMainImageInSlider).ToList();
                    if (images != null && images.Count() > 0)
                    {
                       // sortImagesByProductVariation(images);
                        string currVariationColour = null;
                        int imageIndex = 0;
                        foreach (var image in images)
                        {

                           // _ctrls.Add(new Literal() { Text =  "<div class='temp'></div>" });
                            _ctrls.Add(createImage(image));
                            productImageSlider.Functionality.Controls = _ctrls;
                            if (this.Functionality.ProductVariationControllerAnythingSliderParameters != null && image.ProductVariation.Colour!= null && image.ProductVariation.Colour.Title != currVariationColour)
                            {
                                
                                currVariationColour = image.ProductVariation.Colour.Title;
                                this.Functionality.ProductVariationControllerAnythingSliderParameters.variationsData.Add(new ProductVariationItemData()
                                {
                                    imageIndex = imageIndex,
                                    variationID = image.ProductVariation.ID.ToString()
                                });
                            }
                            imageIndex++;
                        }
                        this.Functionality.ProductVariationControllerAnythingSliderParameters.selectorAnythingSlider = "#" + productImageSlider.ClientID;

                        PrettyPhotoUtil.InitJS();
                    }
                    else
                    {
                        this.Visible = false;
                    }
                }
            }
            else
            {
                divProductImageSlider.Parent.Controls.Remove(divProductImageSlider);
            }
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(divProductImageSliderText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_Slider_ClikImageToEnlarge).ToFrontendBase());
        }

        private MyImage createImage(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBase image)
        {
            MyImage imgProduct = new MyImage();
            var imageUrl = image.Image.GetSpecificSizeUrl(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase.ImageSizingEnum.ImageSlider);
            imgProduct.ImageUrl = imageUrl;
            string caption = image.GetCaption();
            imgProduct.AlternateText = (string.IsNullOrEmpty(caption) ? this.Functionality.Product.GetTitle() : caption);
            imgProduct.HRef = image.Image.GetSpecificSizeUrl(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase.ImageSizingEnum.Large);
            imgProduct.Rel = "prettyPhoto[fullPageSlider]";
            return imgProduct;
        }
    }
}