﻿using System;
using System.Collections.Generic;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.TestimonialModule;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Carousel.CarouFredSel.v1;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3;
using System.Linq;
using BusinessLogic_v3.Frontend.EventModule;
using BusinessLogic_v3.Modules;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Testimonials
{
    public partial class TestimonialsScroller : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private TestimonialsScroller _control;
            public FUNCTIONALITY(TestimonialsScroller control)
            {
                _control = control;
            }
            public EventBaseFrontend Event { get; set; }
            public int ShowAmount { get; set; }
            public int Width { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public TestimonialsScroller()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            testimonialsScroller.Functionality.ItemsVisible = 1;
            testimonialsScroller.Functionality.Parameters.width = this.Functionality.Width;
            testimonialsScroller.Functionality.Parameters.pagination = new CarouFredSelV1PaginationParameters();
            loadItems();
        }

        private void loadItems()
        {
            if(this.Functionality.ShowAmount == 0)
            {
                this.Functionality.ShowAmount = Factories.SettingFactory.GetSettingValue<int>(Enums.BusinessLogicSettingsEnum.Testimonials_Scroller_ItemsToScroll);
            }

            IEnumerable<TestimonialBaseFrontend> testimonials = this.Functionality.Event != null ? 
                this.Functionality.Event.Data.GetTestimonials(this.Functionality.ShowAmount).ToFrontendBaseList() : 
                Factories.TestimonialFactory.GetTestimonials(this.Functionality.ShowAmount).ToFrontendBaseList();
            
            if(testimonials != null && testimonials.Count() > 0)
            {
                foreach (var testimonial in testimonials)
                {
                    var ctrl = TestimonialsItem.LoadControl(this.Page);
                    ctrl.Functionality.Testimonial = testimonial;
                    testimonialsScroller.Functionality.Controls.Add(ctrl);
                }
            }
        }
    }
}