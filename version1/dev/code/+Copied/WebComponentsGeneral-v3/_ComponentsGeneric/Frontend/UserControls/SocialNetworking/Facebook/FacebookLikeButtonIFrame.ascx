﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacebookLikeButtonIFrame.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.SocialNetworking.Facebook.FacebookLikeButtonIFrame" %>
<div class="facebook-like-button-container">
    <iframe id="iFrameLikeButton" runat="server" src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.google.com&amp;send=false&amp;layout=button_count&amp;width=50&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=tahoma&amp;height=21"
        scrolling="no" frameborder="0" style="border: none; overflow: hidden;" allowtransparency="true">
    </iframe>
</div>
