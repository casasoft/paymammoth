﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartTotalItems.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.ShoppingCartTotalItems" %>
<div class="header-shoppingcart-total-container">
    <a href="/" runat="server" id="anchorShoppingCart" class="shopping-cart-total">9.35</a><span id="spanSeparator" runat="server"> / </span>
    <span class="shopping-cart-items" runat="server" id="spanShoppingCartItems">2 items</span>
</div>