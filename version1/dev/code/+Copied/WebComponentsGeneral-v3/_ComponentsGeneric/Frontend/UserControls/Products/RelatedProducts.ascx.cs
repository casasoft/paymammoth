﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BusinessLogic_v3;
    using BusinessLogic_v3.Frontend.ProductModule;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using BusinessLogic_v3.Extensions;
    public partial class RelatedProducts : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private RelatedProducts _control;
            public FUNCTIONALITY(RelatedProducts control)
            {
                _control = control;
            }
            public ProductBaseFrontend Product { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public RelatedProducts()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
         protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initFeaturedProducts();
            updateTexts();
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(spanRelatedProducts,BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Products_RelatedProducts).ToFrontendBase());
        }

        private void initFeaturedProducts()
        {
            var relatedProductsAmt = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_ShowAmtRelatedProducts);
            if (this.Functionality.Product != null)
            {
                IEnumerable<ProductBaseFrontend> relatedProducts = this.Functionality.Product.Data.RelatedProductsManager.GetRelatedProducts(relatedProductsAmt).ToFrontendBaseList();
                if (relatedProducts != null && relatedProducts.Count() > 0)
                {
                    relatedProductsListing.Functionality.Items = relatedProducts;
                }
                else
                {
                    this.Visible = false;
                }
            }
            else
            {
                this.Visible = false;
            }
        }
    }
}