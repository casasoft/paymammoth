﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ContentTextModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Newsletter
{
    public partial class NewsletterSubscription : BaseUserControl
    {
        private const string SUBSCRIBE_FORM_VALIDATION_GROUP = "newsletterSubscriptionForm";

        public class FUNCTIONALITY
        {
            private NewsletterSubscription _control;
            public FUNCTIONALITY(NewsletterSubscription control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public NewsletterSubscription()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }
        private void init()
        {
            initHandlers();
            initFields();
            updateTexts();
        }

        private void initHandlers()
        {
            btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }

        void btnSubmit_Click(object sender, EventArgs e)
        {
            string name = txtName.GetFormValueAsString();
            string email = txtEmail.GetFormValueAsString();

            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(email))
            {
                var result = BusinessLogic_v3.Modules.Factories.NewsletterSubscriptionFactory.SubscribeNewsletterSubscription(name, email, null);
                ContentTextBaseFrontend resultMsgCntTxt = null;
                switch(result)
                {
                    case Enums.NEWSLETTER_SUBSCRIBE_RESULT.Success:
                        resultMsgCntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.NewsletterSubscription_Subscribe_Success).ToFrontendBase();
                        this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(resultMsgCntTxt);
                        break;
                    case Enums.NEWSLETTER_SUBSCRIBE_RESULT.AlreadyExists:
                        resultMsgCntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.NewsletterSubscription_Subscribe_EmailAlreadyExists).ToFrontendBase();
                        this.Page.Functionality.ShowErrorMessageForNextRedirectToPage(resultMsgCntTxt);
                        break;
                }
            }
        }

        private void initFields()
        {
            txtName.Required = txtEmail.Required = true;
            txtEmail.ValidationGroup = txtName.ValidationGroup = btnSubmit.ValidationGroup = SUBSCRIBE_FORM_VALIDATION_GROUP;
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(divSubscribeText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.NewsletterSubscription_DescriptionText).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(spanAssuranceText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.NewsletterSubscription_AssuranceText).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(btnSubmit, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.NewsletterSubscription_SubmitBtnTxt).ToFrontendBase());

            var emailPlaceHolderText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.NewsletterSubscription_Email_PlaceHolderTxt).ToFrontendBase();
            var namePlaceHolderText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.NewsletterSubscription_Name_PlaceHolderTxt).ToFrontendBase();

            txtEmail.PlaceholderText = txtEmail.FieldParameters.title = emailPlaceHolderText.GetContent();
            txtName.PlaceholderText = txtName.FieldParameters.title = namePlaceHolderText.GetContent();
        }
    }
}