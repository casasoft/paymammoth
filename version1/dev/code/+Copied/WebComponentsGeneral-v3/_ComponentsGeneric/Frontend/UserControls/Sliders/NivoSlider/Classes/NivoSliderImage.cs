﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Sliders.NivoSlider.Classes
{
    public class NivoSliderImage : INivoSliderImage
    {

        #region INivoSliderImage Members

        public string Title
        {
            get;
            set;
        }

        public string URL
        {
            get;
            set;
        }

        public string AlternateText
        {
            get;
            set;
        }

        #endregion

        #region INivoSliderImage Members


        public string Href
        {
            get;
            set;
        }

        #endregion
    }
}