﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ContactFormModule;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Contact
{
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using BusinessLogic_v3.Frontend.MemberModule;
    using CS.General_v3.Classes.Login;
    using BusinessLogic_v3.Frontend.ArticleModule;
    using BusinessLogic_v3.Classes.Routing;
    using BusinessLogic_v3.Modules.ContactFormModule;
    using BusinessLogic_v3.Modules;
    using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Frontend.ProductModule;
    using BusinessLogic_v3;

    public partial class ContactUsForm  : BaseUserControl
    {
        public delegate void ContactUsFieldsSubmitHandler(ContactUsForm sender, ContactFormBaseFrontend contactForm, ContactUsFormFieldsData data);
        public delegate void ContactUsFieldsOnSubmittedSuccessHandler(ContactUsForm sender, ContactUsFormFieldsData data);
        public delegate void ContactUsFieldsOnSubmittedFailureHandler(ContactUsForm sender, ContactUsFormFieldsData data);

        private ContentTextBaseFrontend _lblSubject;

        public class ContactUsFormFieldsData
        {
            public string Fullname;
            public string Username;
            public string Email;
            public string Enquiry;
            public string ContactNo;
            public string Company;
            public string HowDidYouFindUs;
            public string Subject;

            public void FillContactForm(ContactFormBaseFrontend contactForm)
            {
                if (!String.IsNullOrEmpty(Fullname))
                {
                    contactForm.Data.Name = Fullname;
                }
                if (!String.IsNullOrEmpty(Username))
                {
                    contactForm.Data.Username = Username;
                }
                if (!String.IsNullOrEmpty(Enquiry))
                {
                    contactForm.Data.Enquiry = Enquiry;
                }
                if (!String.IsNullOrEmpty(Email))
                {
                    contactForm.Data.Email = Email;
                }
                if (!String.IsNullOrEmpty(ContactNo))
                {
                    contactForm.Data.Telephone = ContactNo;
                }
                if(!String.IsNullOrEmpty(Company))
                {
                    contactForm.Data.Company = Company;
                }
                if(!String.IsNullOrEmpty(HowDidYouFindUs))
                {
                    contactForm.Data.HowDidYouFindUs = HowDidYouFindUs;
                }
                if(!string.IsNullOrEmpty(Subject))
                {
                    contactForm.Data.Subject = Subject;
                }
            }
        }

        private MyTxtBoxString _txtName;
        private MyTxtBoxString _txtUsername;
        private MyTxtBoxString _txtCompany;
        private MyTxtBoxString _txtContactNo;
        private MyTxtBoxString _txtHowDidYouFindUs;
        private MyTxtBoxEmail _txtEmail;
        private MyDropDownList _dlSubject;
        private MyTxtBoxStringMultiLine _txtEnquiry;

        private MemberBaseFrontend _member;

        private static string ENQURIY_FORM_IDENTIFIER = "ContactUsForm_enquiryForm";
        private static string ENQURIY_FORM_VALIDATION_GROUP = "ContactUsForm_enquiryForm_Vg";

        public class FUNCTIONALITY
        {
            private ContactUsForm _control;
            public FUNCTIONALITY(ContactUsForm control)
            {
                _control = control;
            }
            public MemberBaseFrontend Member { get; set; }
            public event ContactUsFieldsSubmitHandler OnPreSubmit;
            public event ContactUsFieldsOnSubmittedSuccessHandler OnSubmittedSuccess;
            public event ContactUsFieldsOnSubmittedFailureHandler OnSubmittedFailure;

            public ContentTextBaseFrontend LblName { get; set; }
            public ContentTextBaseFrontend LblUsername { get; set; }
            public ContentTextBaseFrontend LblEnquiry { get; set; }
            public ContentTextBaseFrontend LblEmail { get; set; }
            public ContentTextBaseFrontend LblContactNo { get; set; }
            public ContentTextBaseFrontend LblCompany { get; set; }
            public ContentTextBaseFrontend LblHowdidYouFindUs { get; set; }
            public string EnquiryFormSubject { get; set; }
            public ContentTextBaseFrontend TextEnquiryFormTitle { get; set; }
            public string TextSubmitEnquiryBtn { get; set; }
            public ContentTextBaseFrontend EnquiryDescription { get; set; }
 
            public bool FillDataFromCurrentLoggedInUser { get; set; }

            public ProductBaseFrontend Product { get; set; }

            internal void triggerOnPreSubmit(ContactUsFormFieldsData data, ContactFormBaseFrontend contactForm)
            {
                if (this.OnPreSubmit != null)
                {
                    this.OnPreSubmit(_control, contactForm, data);
                }
            }

            internal bool triggerOnSubmittedSuccess(ContactUsFormFieldsData data)
            {
                if (this.OnSubmittedSuccess != null)
                {
                    this.OnSubmittedSuccess(_control, data);
                    return true;
                }
                return false;
            }

            internal bool triggerOnSubmittedFailure(ContactUsFormFieldsData data)
            {
                if (this.OnSubmittedFailure != null)
                {
                    this.OnSubmittedFailure(_control, data);
                    return true;
                }
                return false;
            }

        }

        public FUNCTIONALITY Functionality { get; private set; }
        public ContactUsForm()
        {
            this.Functionality = createFunctionality();

            // Button
            this.Functionality.TextSubmitEnquiryBtn = "Submit Enquiry";
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
            initMember();
            initFormFields();
        }

        private void updateTexts()
        {
            this.Functionality.LblEmail = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Email).ToFrontendBase();
            this.Functionality.LblName = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_FullName).ToFrontendBase(); ;
            this.Functionality.LblEnquiry = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Enquiry).ToFrontendBase(); ;
            this.Functionality.LblContactNo = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_EnquiryForm_ContactNo).ToFrontendBase(); ;
            this.Functionality.LblCompany = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_EnquiryForm_Company).ToFrontendBase();
            this.Functionality.LblHowdidYouFindUs = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_EnquiryForm_HowDidYouFindUs).ToFrontendBase();
            _lblSubject = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_EnquiryForm_Subject).ToFrontendBase();
            if(this.Functionality.TextEnquiryFormTitle == null)
            {
                this.Functionality.TextEnquiryFormTitle = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_EnquiryForm_Title).ToFrontendBase();
            }
            this.Functionality.TextSubmitEnquiryBtn = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_EnquiryForm_SubmitBtnText).GetContent();
           
            if(this.Functionality.EnquiryDescription == null)
            {
                this.Functionality.EnquiryDescription = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Contact_EnquiryForm_Description).ToFrontendBase();
            }
        }

        private void initMember()
        {
            var usedInProject = BusinessLogic_v3.Modules.MemberModule.MemberBaseFactory.UsedInProject;
            if (usedInProject)
            {
                var member =
                    BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.
                        GetLoggedInUserAlsoFromRememberMe().ToFrontendBase();
                if (member != null)
                {
                    _member = member;
                }
            }
        }

        private void initFormFields()
        {
            var group =
                formContactUs.Functionality.AddGroup(
                    new Code.Controls.WebControls.FormFields.FormFieldsItemGroupData()
                        {Identifier = ENQURIY_FORM_IDENTIFIER, ValidationGroup = ENQURIY_FORM_VALIDATION_GROUP });
            
            MyContentText.AttachContentTextWithControl(h2EnquiryFormTitle, this.Functionality.TextEnquiryFormTitle);

            if(!string.IsNullOrEmpty(this.Functionality.EnquiryDescription.GetContent()))
            {
                MyContentText.AttachContentTextWithControl(EnquiryFormDescription, this.Functionality.EnquiryDescription);
            }
            else
            {
                EnquiryFormDescription.Visible = false;
            }

            //Name
            _txtName = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ContactUsForm_txtFullName",
                    required = true,
                    titleContentText = Functionality.LblName
                },
                InitialValue = (_member != null ? ((IMemberAddressDetails)_member.Data).GetFullName() : null)
            });

            //Email
            _txtEmail = (MyTxtBoxEmail)group.AddFormWebControl(new MyTxtBoxEmail()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ContactUsForm_txtEmail",
                    required = true,
                    titleContentText = Functionality.LblEmail
                },

                InitialValue = (_member != null ? (_member.Data.Email) : null)
            });

            //Contact No
            if (BusinessLogic_v3.Modules.ModuleSettings.Contact.IsContactNoInEnquiryFormVisible)
            {
                _txtContactNo = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
                {
                    FieldParameters = new FieldSingleControlParameters()
                    {
                        id = "ContactUsForm_txtContactNo",
                        titleContentText = Functionality.LblContactNo
                    },
                    InitialValue = (_member != null ? _member.Data.Telephone : null)
                });
            }

            //Company
            if (BusinessLogic_v3.Modules.ModuleSettings.Contact.IsCompanyInEnquiryFormVisible)
            {
                _txtCompany = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
                {
                    FieldParameters = new FieldSingleControlParameters()
                    {
                        id = "ContactUsForm_txtCompany",
                        titleContentText = Functionality.LblCompany
                    },
                    InitialValue = (_member != null ? _member.Data.CompanyName : null)
                });
            }

            //How Did You Find Us
            if (BusinessLogic_v3.Modules.ModuleSettings.Contact.IsHowDidYouFindUsInEnquiryFormVisible)
            {
                _txtHowDidYouFindUs = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
                {
                    FieldParameters = new FieldSingleControlParameters()
                    {
                        id = "ContactUsForm_txtHowDidYouFindUs",
                        titleContentText = Functionality.LblHowdidYouFindUs
                    },
                    InitialValue = (_member != null ? _member.Data.HowDidYouFindUs : null)
                });
            }

            //How Did You Find Us
            if (BusinessLogic_v3.Modules.ModuleSettings.Contact.IsSubjectInEnquiryFormVisible)
            {
                if(string.IsNullOrEmpty(this.Functionality.EnquiryFormSubject))
                {
                    this.Functionality.EnquiryFormSubject = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Subject);
                }

                
                List<string> fieldsList = new List<string>();
                string[] fields = this.Functionality.EnquiryFormSubject.Split('|');
                foreach (string strField in fields)
                {
                    fieldsList.Add(strField);
                }

                _dlSubject = (MyDropDownList)group.AddFormWebControl(new MyDropDownList()
                {
                    FieldParameters = new FieldSingleControlParameters()
                    {
                        id = "ContactUsForm_dlSubject",
                        titleContentText = _lblSubject
                    },
                });

                if(fieldsList != null && fieldsList.Count() > 0)
                {
                    foreach (var field in fields)
                    {
                        _dlSubject.Functionality.AddItemFromListItem(new ListItem() {Text = field, Value = field});
                    }
                }
            }

            //Enquiry
            _txtEnquiry = (MyTxtBoxStringMultiLine)group.AddFormWebControl(new MyTxtBoxStringMultiLine()
            { 
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ContactUsForm_txtEnquiry",
                    required = true,
                    titleContentText = Functionality.LblEnquiry
                }
            });

            MyButton btnSubmit = formContactUs.Functionality.AddButton(this.Functionality.TextSubmitEnquiryBtn, ENQURIY_FORM_IDENTIFIER);
            btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }

        void btnSubmit_Click(object sender, EventArgs e)
        {
            sendContactForm();
        }

        private void sendContactForm()
        {
            ContactUsFormFieldsData data = new ContactUsFormFieldsData();
            data.Fullname = _txtName != null ? _txtName.GetFormValueAsString() : null;
            data.Username = (_member != null ? ((IUser) _member.Data).UsernameHashed : null);
            data.Email = _txtEmail != null ? _txtEmail.GetFormValueAsString() : null;
            data.ContactNo = _txtContactNo != null ? _txtContactNo.GetFormValueAsString() : null;
            data.Company = _txtCompany != null ? _txtCompany.GetFormValueAsString() : null;
            data.HowDidYouFindUs = _txtHowDidYouFindUs != null ? _txtHowDidYouFindUs.GetFormValueAsString() : null;
            data.Subject = _dlSubject != null ? _dlSubject.GetFormValueAsString() : null;
            data.Enquiry = _txtEnquiry != null ? _txtEnquiry.GetFormValueAsString() : null;

            var contactForm = BusinessLogic_v3.Modules.Factories.ContactFormFactory.CreateNewItem().ToFrontendBase();
            data.FillContactForm(contactForm);

            if (Functionality.Product != null)
            {
                contactForm.Data.Product = Functionality.Product.Data;
            }

            this.Functionality.triggerOnPreSubmit(data, contactForm);
            var sendContactForm = contactForm.Data.SendContactForm();

            if (sendContactForm.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
            {
                //Specific Success
                if(!this.Functionality.triggerOnSubmittedSuccess(data))
                {
                    //Success
                    ArticleBaseFrontend cp = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier
                        (BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.ContactUs_EnquiryFormSentSuccessfully).
                        ToFrontendBase();

                    if (cp != null)
                    {
                        PageUtil.RedirectPage(cp.Data.GetUrl());
                    }
                }
            }
            else
            {
                // Specific Error
                if (!this.Functionality.triggerOnSubmittedFailure(data))
                {
                    //Error
                    PageUtil.RedirectPage(ErrorsRoute.GetGenericErrorURL());
                }
            }
        }

        public static ContactUsForm LoadControl(Page pg, string id)
        {
            var ctrl = pg.LoadControl("/_ComponentsGeneric/Frontend/UserControls/Contact/ContactUsForm") as ContactUsForm;
            if (!string.IsNullOrEmpty(id))
            {
                ctrl.ID = id;
            }
            return ctrl;
        }
    }
}