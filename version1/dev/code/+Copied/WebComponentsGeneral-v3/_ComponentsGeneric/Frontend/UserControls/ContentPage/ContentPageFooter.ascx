﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentPageFooter.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage.ContentPageFooter" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/SocialNetworking/AddThis/AddThisControl.ascx" TagName="AddThisControl" TagPrefix="Control" %>
    <div class="clearfix content-page-footer" runat="server" id="divFooterWrapper" visible="false">
        <div class="add-this-wrapper" runat="server" id="divAddThisWrapper">
            <Control:AddThisControl runat="server" ID="AddThisControl" />
        </div>
        <span class="content-page-last-edited" runat="server" id="spanLastEditedOn"></span>
    </div>