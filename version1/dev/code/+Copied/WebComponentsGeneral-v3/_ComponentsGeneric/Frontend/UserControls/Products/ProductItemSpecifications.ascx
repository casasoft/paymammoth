﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductItemSpecifications.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.ProductItemSpecifications" %>
<div class="product-item-specifications-container">
    <span runat="server" id="spanProductSpecifications" class="product-item-specifications-title">Specifications</span>
    <CSControls:MyTable runat="server" ID="tblProductSpecifications"></CSControls:MyTable>
</div>

