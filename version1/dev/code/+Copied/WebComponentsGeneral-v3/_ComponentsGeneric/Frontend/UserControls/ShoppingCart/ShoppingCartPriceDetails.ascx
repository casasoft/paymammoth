﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartPriceDetails.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.ShoppingCartPriceDetails" %>
    <div class="shipping-total-details-container">
        <table class="shipping-total-details">
            <tr class="shipping-total-details-subtotal" runat="server" id="trDetailsSubTotal">
                <td runat="server" id="tdSubTotalLbl" class="label">
                </td>
                <td runat="server" id="tdSubTotalValue">
                </td>
            </tr>
            <tr class="shipping-total-details-delivery" runat="server" id="trDetailsDelivery">
                <td runat="server" id="tdDeliveryLbl" class="label">
                </td>
                <td>
                    <CSControls:MyDropDownList runat="server" id="dlDeliveryMethods">
                    </CSControls:MyDropDownList>
                </td>
            </tr>
            <tr class="shipping-total-details-discount" runat="server" id="trDetailsDiscount">
                <td runat="server" id="tdDiscountLbl" class="label">
                </td>
                <td runat="server" id="tdDiscountValue">
                </td>
            </tr>
            <tr class="shipping-total-details-total" runat="server" id="trDetailsTotal">
                <td runat="server" id="tdTotal" class="label">
                </td>
                <td runat="server" id="tdTotalValue">
                </td>
            </tr>
        </table>
    </div>