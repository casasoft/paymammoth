﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing
{
    public abstract class ListingItemControlBaseWithType<T> : ListingItemControlBase
    {
        protected abstract void populateControlFromItem(T item);

        public override void PopulateControl(object item)
        {
            populateControlFromItem((T)item);
        }
        
    }
}