﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Brickred.SocialAuth.NET.Core.BusinessObjects;
using BusinessLogic_v3;
using BusinessLogic_v3.Constants;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Modules.ArticleModule;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Login.SocialButtons
{
    public partial class GoogleLoginButton : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private GoogleLoginButton _control;
            public string SuccessUrl { get { return getSuccessUrl(); } set { _successUrl = value; } }
            private string _successUrl { get; set; }
            public MyCheckBoxWithLabel ControlRememberMeCheckBox;
            public FUNCTIONALITY(GoogleLoginButton control)
            {
                _control = control;
            }
            private string getSuccessUrl()
            {
                if (_successUrl == null)
                {
                    string currentUrlFullyQualified = string.Empty;
                    string currentUrlRelative = string.Empty;
                    var cp = this._control.Page.Functionality.ContentPage;
                    if (cp == null || (((IArticleBase)cp.Data).DoNotRedirectOnLogin.HasValue && ((IArticleBase)cp.Data).DoNotRedirectOnLogin.Value))
                    {
                        currentUrlRelative =
                            BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                                BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Home).GetUrl();
                    }
                    else
                    {
                        currentUrlRelative = PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                    }
                    currentUrlFullyQualified = PageUtil.ConvertRelativeUrlToAbsoluteUrl(currentUrlRelative);
                    if (currentUrlRelative.StartsWith("/"))
                    {
                        currentUrlRelative = currentUrlRelative.Substring(1);
                    }
                    _successUrl = currentUrlFullyQualified;
                }
                return _successUrl;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public GoogleLoginButton()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initCSSClass();
            initGoogleLogin();
        }

        private void initCSSClass()
        {
            btnGoogleLogin.CssClass = "social-login-google social-button";
        }

        private void initGoogleLogin()
        {
            ContentTextBaseFrontend ctGoogleLoginBtnText =
                Factories.ContentTextFactory.GetContentTextForEnumValue(
                    Enums.CONTENT_TEXT.General_LoginFields_GoogleLoginButtonText).ToFrontendBase();
            btnGoogleLogin.Text = ctGoogleLoginBtnText.GetContent();
            btnGoogleLogin.Click += new EventHandler(btnGoogleLogin_Click);
        }

        void btnGoogleLogin_Click(object sender, EventArgs e)
        {
            generateGoogleLogin();
        }

        private void generateGoogleLogin()
        {
            checkRememberMe();
            SocialAuthUser oUser = new SocialAuthUser(PROVIDER_TYPE.GOOGLE);
            oUser.Login(returnUrl: Functionality.SuccessUrl, errorRedirectURL: "error.aspx");
            //todo: match return urls with settings or other non hardcoded value
        }

        private void checkRememberMe()
        {
            if (this.Functionality.ControlRememberMeCheckBox != null)
            {
                bool rememberMe = Functionality.ControlRememberMeCheckBox.GetFormValueObjectAsBool();
                CS.General_v3.Util.SessionUtil.SetObject(CS.General_v3.Constants.SESSION_SOCIAL_LOGIN_REMEMBERME, rememberMe);
            }
            else
            {
                CS.General_v3.Util.SessionUtil.RemoveObject(CS.General_v3.Constants.SESSION_SOCIAL_LOGIN_REMEMBERME);
            }
        }

        public static GoogleLoginButton LoadControl(Page page)
        {
            return
                (GoogleLoginButton)
                page.LoadControl(
                    "/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/GoogleLoginButton.ascx");
        }
    }
}