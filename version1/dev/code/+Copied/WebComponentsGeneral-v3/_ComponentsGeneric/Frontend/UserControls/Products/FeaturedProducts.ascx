﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeaturedProducts.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products.FeaturedProducts" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Listing/GenericListingItems.ascx" TagName="GenericListingItems" TagPrefix="Common" %>
<div class="featured-products-container">
    <h2 class="h1" runat="server" id="h2FeaturedProducts">Featured Products</h2>
    <Common:GenericListingItems runat="server" ID="featuredProducts" />
</div>
