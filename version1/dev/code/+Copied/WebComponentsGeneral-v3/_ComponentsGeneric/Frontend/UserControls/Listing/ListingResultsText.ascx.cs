﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Classes.URL;
using BusinessLogic_v3.Modules.ContentTextModule;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Classes.Text;
namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing
{
    public partial class ListingResultsText : BaseUserControl
    {

        public const string TAG_SHOWING_RESULTS_FROM = "[FROM]";
        public const string TAG_SHOWING_RESULTS_TO = "[TO]";
        public const string TAG_SHOWING_RESULTS_TOTAL = "[TOTAL]";
        public const string TAG_SHOWING_RESULTS_ITEMS = "[ITEMS]";
        public class FUNCTIONALITY
        {
            /// <summary>
            /// This will be something of the form Showing [FROM] - [TO] of [ITEMS].
            /// 
            /// The tags are taken from the constants in this PagingBar.TAG_SHOWING_RESULTS_FROM etc...
            /// </summary>
            public ContentTextBaseFrontend ShowingResultsFullText { get; set; }
            public ContentTextBaseFrontend ResultsTextSingular { get; set; }
            public ContentTextBaseFrontend ResultsTextPlural { get; set; }
            public ContentTextBaseFrontend NoResultsText { get; set; }
            public ContentTextBaseFrontend ItemsText { get; set; }

            private ListingResultsText _control;

            public int? TotalResults { get; set; }
            public int? PageSize { get; set; }
            public int? PageNum { get; set; }

            public void UpdateValues(int pgNum, int pgSize, int totalResults)
            {
                this.TotalResults = totalResults;
                this.PageNum = pgNum;
                this.PageSize = pgSize;
            }
            public void UpdateFromPagingInfo(IURLParserPagingInfo pgInfo, int totalResults)
            {
                this.TotalResults = totalResults;
                this.PageSize = pgInfo.ShowAmount;
                this.PageNum = pgInfo.PageNo;
            }


            public FUNCTIONALITY(ListingResultsText control)
            {
                _control = control;
            }
        }

        public FUNCTIONALITY Functionality { get; private set; }
        public ListingResultsText()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private void updateTexts()
        {
            if (!this.Functionality.TotalResults.HasValue) throw new InvalidOperationException("Please set AmtItems");
            if (!this.Functionality.PageSize.HasValue) throw new InvalidOperationException("Please set PageSize");
            if (!this.Functionality.PageNum.HasValue) throw new InvalidOperationException("Please set PageNum");

            string text = null;
            if (this.Functionality.TotalResults.Value > 0)
            {
                int from, to;
                CS.General_v3.Util.Paging.GetFromToIndices(this.Functionality.TotalResults.Value, this.Functionality.PageNum.Value, this.Functionality.PageSize.Value, out from, out to);

                TokenReplacerNew replacementTags = new TokenReplacerNew(this.Functionality.ShowingResultsFullText.Data);
                replacementTags[TAG_SHOWING_RESULTS_FROM] = CS.General_v3.Util.NumberUtil.FormatNumber(from, decimalPlaces: 0);
                replacementTags[TAG_SHOWING_RESULTS_TO] = CS.General_v3.Util.NumberUtil.FormatNumber(to, decimalPlaces: 0);
                replacementTags[TAG_SHOWING_RESULTS_TOTAL] = CS.General_v3.Util.NumberUtil.FormatNumber(this.Functionality.TotalResults.Value, decimalPlaces: 0);
                /*text = text.Replace(TAG_SHOWING_RESULTS_FROM, CS.General_v3.Util.NumberUtil.FormatNumber(from, decimalPlaces: 0));
                text = text.Replace(TAG_SHOWING_RESULTS_TO, CS.General_v3.Util.NumberUtil.FormatNumber(to, decimalPlaces: 0));
                text = text.Replace(TAG_SHOWING_RESULTS_TOTAL, CS.General_v3.Util.NumberUtil.FormatNumber(this.Functionality.TotalResults.Value, decimalPlaces: 0));*/
                if (this.Functionality.TotalResults.Value == 1)
                {
                    replacementTags[TAG_SHOWING_RESULTS_ITEMS] = this.Functionality.ResultsTextSingular.GetContent();
                }
                else
                {
                    replacementTags[TAG_SHOWING_RESULTS_ITEMS] = this.Functionality.ResultsTextPlural.GetContent();
                }
                MyContentText.AttachContentTextWithControl(divText, this.Functionality.ShowingResultsFullText, replacementTags);
            }
            else
            {
                MyContentText.AttachContentTextWithControl(divText, this.Functionality.NoResultsText);
                //text = this.Functionality.NoResultsText;
            }
            //divText.InnerHtml = text;

        }

        protected override void OnLoad(EventArgs e)
        {
            if (this.Visible)
            {
                updateTextsFromContentText();
                updateTexts();
            }
            base.OnLoad(e);
        }

        private void updateTextsFromContentText()
        {
            //if (BusinessLogic_v3.Modules.ModuleSettings.Generic.MultiLingual)
            //{
            if (Functionality.ItemsText == null) Functionality.ItemsText = (BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Paging_ItemsText).ToFrontendBase());
            if (Functionality.NoResultsText == null) Functionality.NoResultsText = (BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Paging_NoResultsText).ToFrontendBase());
            if (Functionality.ResultsTextPlural == null) Functionality.ResultsTextPlural = (BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Paging_ResultsTextPlural).ToFrontendBase());
            if (Functionality.ResultsTextSingular == null) Functionality.ResultsTextSingular = (BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Paging_ResultsTextSingular).ToFrontendBase());
            if (Functionality.ShowingResultsFullText == null) Functionality.ShowingResultsFullText = (BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Paging_ShowingResultsFullText).ToFrontendBase());
                /*
                Functionality.NoResultsText = string.IsNullOrWhiteSpace(Functionality.NoResultsText) ?
                    ((ContentTextBase)BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.General_Paging_NoResultsText)).GetHTMLOrPlainText() : Functionality.NoResultsText;
                Functionality.ResultsTextPlural = string.IsNullOrWhiteSpace(Functionality.ResultsTextPlural) ? ((ContentTextBase)BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    Enums.CONTENT_TEXT.General_Paging_ResultsTextPlural)).GetHTMLOrPlainText() : Functionality.ResultsTextPlural;
                Functionality.ResultsTextSingular = string.IsNullOrWhiteSpace(Functionality.ResultsTextSingular) ? ((ContentTextBase)BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.General_Paging_ResultsTextSingular)).GetHTMLOrPlainText() : Functionality.ResultsTextSingular;
                Functionality.ShowingResultsFullText = string.IsNullOrWhiteSpace(Functionality.ShowingResultsFullText) ? ((ContentTextBase)BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.General_Paging_ShowingResultsFullText)).GetHTMLOrPlainText() : Functionality.ShowingResultsFullText;*/
            //}
            //else
            //{
            //    this.Functionality.ResultsTextPlural = "items";
            //    this.Functionality.ResultsTextSingular = "item";
            //    this.Functionality.ShowingResultsFullText = "Showing <span class='strong'>[FROM] - [TO]</span> of <span class='strong'>[TOTAL]</span> [ITEMS]";
            //    this.Functionality.NoResultsText = "No results found.";
            //}
        }
    }
}