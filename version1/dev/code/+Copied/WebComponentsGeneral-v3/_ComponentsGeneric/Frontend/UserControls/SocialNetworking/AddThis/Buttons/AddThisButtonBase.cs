﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.SocialNetworking.AddThis.Buttons
{
    using System.Reflection;

    public abstract class AddThisButtonBase : BaseUserControl
    {
        public static void CopyAttributesFromClassToButton(IAttributeAccessor ctrl, object properties, string prefix)
        {
            List<MemberInfo> props = CS.General_v3.Util.ReflectionUtil.GetAllPropertiesAndFieldsForType(properties.GetType(), false, false, false);
            foreach (MemberInfo prop in props)
            {
                string propName = prop.Name;
                if (!string.IsNullOrEmpty(propName))
                {
                    propName = propName.Replace("_", ""); // since certain keywords cannot be used
                }
                object value = null;
                if (prop is PropertyInfo)
                {
                    value = ((PropertyInfo)prop).GetValue(properties, null);
                }
                else if (prop is FieldInfo)
                {
                    value = ((FieldInfo)prop).GetValue(properties);
                }
                if (value != null)
                {
                    string sValue = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value, ifEnumConvertToIntValue:false, useDescriptionAttributeIfEnum:true);
                    ctrl.SetAttribute(prefix + propName, sValue);
                }
            }
        }


    }
}