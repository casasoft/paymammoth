﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ArticleCommentModule;
using CS.General_v3.Classes.Interfaces.BlogPosts;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Comments
{
    using BusinessLogic_v3.Extensions;
    using Code.Controls.WebControls.Common;

    public partial class CommentItem : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private CommentItem _control;
            public FUNCTIONALITY(CommentItem control)
            {
                _control = control;
            }

            public ICommentableItemComment Comment { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public CommentItem()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initComment();
        }
        

        private void initReplies()
        {
            bool allowCommentReplies = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_CommentingReplyEnabled);
            if (allowCommentReplies)
            {
                var replies = this.Functionality.Comment.GetReplies();
                if (replies != null && replies.Count() > 0)
                {
                    foreach (var reply in replies)
                    {
                        CommentItem item = LoadControl(this.Page);
                        item.Functionality.Comment = reply;
                        divCommentReplies.Controls.Add(item);
                    }
                }
                else
                {
                    CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divCommentReplies);
                }

                CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Effects.Toggle.ToggleControllerParameters p = new Code.Classes.Javascript.UI.Effects.Toggle.ToggleControllerParameters();
                p.elemToggleSelector = "#" + aReply.ClientID;
                p.eventType = Code.Classes.Javascript.UI.Effects.Toggle.TOGGLE_EVENT.Click;
                p.elemSelectorContentOn = "#" + divSubmitReply.ClientID;
                p._OutputInsideControl = jsSubmitReply;
            }
        }

        private void initComment()
        {
            var comment = Functionality.Comment;
            if (comment != null)
            {
                this.submitReply.Functionality.ReplyToComment = this.Functionality.Comment;
                var ct = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_Author).ToFrontendBase();
                ct.TokenReplacer[BusinessLogic_v3.Constants.Tokens.TAG_AUTHOR] = comment.Author;
                MyContentText.AttachContentTextWithControl(spanCommentAuthor, ct);
                spanCommentDate.InnerText = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatDateTimeWithCurrentCulture(comment.PostedOn, CS.General_v3.Util.Date.DATETIME_FORMAT.FullDateShortTime);
                pCommentContent.InnerHtml = CS.General_v3.Util.Text.ConvertPlainTextToHTML(comment.Comment);

                initReplies();
            }
        }

        public static CommentItem LoadControl(Page pg, string id = null)
        {
            var ctrl = (CommentItem)pg.LoadControl("/_ComponentsGeneric/Frontend/UserControls/Comments/CommentItem.ascx");
            if (!string.IsNullOrEmpty(id)) ctrl.ID = id;
            return ctrl;
        }
    }
}