﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Extensions;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing
{
    public partial class DateFilterComponent : BaseUserControl
    {
        public delegate void ProfileFieldsSubmitHandler(DateFilterComponent sender, DateFilterData data);
        public delegate void ProfileFieldsClearHandler(DateFilterComponent sender);
        private MyTxtBoxDate _txtDateFrom;
        private MyTxtBoxDate _txtDateTo;
        private const string VALIDATION_GROUP = "showStatementFilterVG";
        public class DateFilterData
        {
            public DateTime? From;
            public DateTime? To;
        }

        public class FUNCTIONALITY
        {
            private DateFilterComponent _control;
            public event ProfileFieldsSubmitHandler OnSubmit;
            public event ProfileFieldsClearHandler OnClear;

            public FUNCTIONALITY(DateFilterComponent control)
            {
                _control = control;
            }
            internal void triggerOnSubmit(DateFilterData data)
            {
                if (this.OnSubmit != null)
                {
                    this.OnSubmit(_control, data);
                }
            }
            internal void triggerOnClear()
            {
                if (this.OnClear != null)
                {
                    this.OnClear(_control);
                }
            }
            public DateTime? DateFromInitialValue { get; set; }
            public DateTime? DateToInitialValue { get; set; }
            public ContentTextBaseFrontend DateFromText { get; set; }
            public ContentTextBaseFrontend DateToText { get; set; }
            public ContentTextBaseFrontend BtnSubmitText { get; set; }
            public ContentTextBaseFrontend BtnClearText { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public DateFilterComponent()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadContentTexts();
            initFormFields();
        }

        private void initFormFields()
        {
            initDateFrom();
            initDateTo();
            
            btnSubmit.Text = this.Functionality.BtnSubmitText.GetContent();
            btnSubmit.ValidationGroup = VALIDATION_GROUP;

            btnClear.Text = this.Functionality.BtnClearText.GetContent();
            

            btnSubmit.Click += new EventHandler(btnSubmit_Click);
            btnClear.Click += new EventHandler(btnClear_Click);
        }

        void btnClear_Click(object sender, EventArgs e)
        {
            this.Functionality.triggerOnClear();
        }

        private void initDateFrom()
        {
            _txtDateFrom = (MyTxtBoxDate)formFieldsFilterDateFrom.Functionality.AddFieldItem(new MyTxtBoxDate()
            {
                FieldParameters = new FieldSingleControlDateParameters()
                {
                    id = "txtDateFrom",
                    required = true,
                    title = this.Functionality.DateFromText.GetContent(),
                    doNotValidateOnBlur = true,
                    validationGroup = VALIDATION_GROUP
                }
            });

            _txtDateFrom.FieldParameters.ValidatorDateParameters.dateTo = CS.General_v3.Util.Date.Now;
            if (Functionality.DateFromInitialValue.HasValue)
            {
                _txtDateFrom.InitialValue = Functionality.DateFromInitialValue.Value.Date;
            }
        }

        private void initDateTo()
        {
            _txtDateTo = (MyTxtBoxDate)formFieldsFilterDateTo.Functionality.AddFieldItem(new MyTxtBoxDate()
            {
                FieldParameters = new FieldSingleControlDateParameters()
                {
                    id = "txtDateTo",
                    required = true,
                    title = this.Functionality.DateToText.GetContent(),
                    doNotValidateOnBlur = true,
                    validationGroup = VALIDATION_GROUP
                }
            });
            _txtDateTo.FieldParameters.ValidatorDateParameters.dateTo = CS.General_v3.Util.Date.Now;
            if (Functionality.DateToInitialValue.HasValue)
            {
                _txtDateTo.InitialValue = Functionality.DateToInitialValue.Value.Date;
            }

            /*CS.WebComponentsGeneralV3.Code.Classes.Javascript.Data.JSONObjectDictionary dict = new Code.Classes.Javascript.Data.JSONObjectDictionary();
            dict["test"] = DateTime.Now;
            string js = dict.GetJSON();*/



        }

        void btnSubmit_Click(object sender, EventArgs e)
        {
            DateFilterData data = new DateFilterData();
            data.From =  _txtDateFrom.GetFormValueAsDateNullable();
            data.To =  _txtDateTo.GetFormValueAsDateNullable();
         
            this.Functionality.triggerOnSubmit(data);
        }

        private void loadContentTexts()
        {

            this.Functionality.DateFromText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    Enums.CONTENT_TEXT.Listing_DateFilter_FromText).ToFrontendBase();
            this.Functionality.DateToText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    Enums.CONTENT_TEXT.Listing_DateFilter_ToText).ToFrontendBase();
            this.Functionality.BtnSubmitText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    Enums.CONTENT_TEXT.General_Button_Submit).ToFrontendBase();
            this.Functionality.BtnClearText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    Enums.CONTENT_TEXT.General_Button_Clear).ToFrontendBase();
        }
    }
}
