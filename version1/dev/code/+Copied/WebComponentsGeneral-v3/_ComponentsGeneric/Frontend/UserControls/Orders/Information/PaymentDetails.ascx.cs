﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules.ContentTextModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.OrderModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Orders.Information
{
    public partial class PaymentDetails : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private PaymentDetails _control;
            public OrderBaseFrontend Order { get; set; }
            public FUNCTIONALITY(PaymentDetails control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public PaymentDetails()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if (Functionality.Order != null)
            {
                loadInfoAccordingToPaymentType();
            }
        }

        private void loadInfoAccordingToPaymentType()
        {
            if (Functionality.Order.Data.PendingManualPayment)
            {
                switch (Functionality.Order.Data.PaymentMethod)
                {
                    case PayMammoth.Connector.Enums.PaymentMethodSpecific.Cheque:
                        showChequeAddressDetails();
                        break;
                    default:
                        showOtherManualPaymentDetails();
                        break;
                }
            }
            else
            {
                showImmediatePaymentDetails();
            }
        }

        private void showImmediatePaymentDetails()
        {
            IContentTextBase ct =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Orders_Purchase_Successful_Text);

            MyContentText.AttachContentTextWithControl(spanCTImmediatePayment, ct.ToFrontendBase());
            divImmediatePaymentContainer.Visible = true;
        }

        private void showOtherManualPaymentDetails()
        {
            IContentTextBase ct =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Orders_Purchase_PendingOtherManualPayment_Text);

            MyContentText.AttachContentTextWithControl(spanCTImmediatePayment, ct.ToFrontendBase());
            divOtherManualPaymentDetailsContainer.Visible = true;
        }

        private void showChequeAddressDetails()
        {
            IContentTextBase ct =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Orders_Purchase_With_Cheque_Text);

            MyContentText.AttachContentTextWithControl(spanChequeText, ct.ToFrontendBase());

            string address = Factories.SettingFactory.GetAddressAsOneLine();
            spanChequeAddress.InnerText = address;

            divChequePaymentContainer.Visible = true;
        }

        public static PaymentDetails LoadControls(Page page)
        {
            return
                (PaymentDetails)
                page.LoadControl("/_ComponentsGeneric/Frontend/UserControls/Orders/Information/PaymentDetails.ascx");
        }
    }
}