﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;
using CS.General_v3.Classes.HelperClasses;
using CS.WebComponentsGeneralV3.Code.Classes.URL;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule;
using BusinessLogic_v3.Frontend.ArticleModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.CreditStatement
{
    public partial class CreditStatementListing : BaseUserControl
    {
        private URLParserOrderHistory _pagingURl { get; set; }
        private SearchPagedResults<IMemberAccountBalanceHistoryBase> balanceHistoryItems { get; set; }
        private MemberBaseFrontend _member { get; set; }

        private bool _showMonthlyCredits;
        private bool _showAdditionalCredits;
        private bool _showCreditDiffference;

        public class FUNCTIONALITY
        {
            private CreditStatementListing _control;
            public FUNCTIONALITY(CreditStatementListing control)
            {
                _control = control;
            }

            public ArticleBaseFrontend Article { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public CreditStatementListing()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            //updateContentTexts();
            loadSettingValues();
            initMember();
            initPagingUrl();
            initListing();
        }

        private void loadSettingValues()
        {
            _showAdditionalCredits = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.MembersArea_CreditStatement_ShowAdditionalCredits);
            _showCreditDiffference = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.MembersArea_CreditStatement_ShowCreditDifference);
            _showMonthlyCredits = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.MembersArea_CreditStatement_ShowMonthlyCredits);

        }

        private void initMember()
        {
            _member = BusinessLogic_v3.Modules.Factories.MemberFactory.GetLoggedInUserForCurrentSession().ToFrontendBase();
            if (_member == null)
            {
                var errorMsg = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_YouMustBeLoggedInToAccessPage).ToFrontendBase();
                this.Page.Functionality.ShowErrorMessageAndRedirectToPage(errorMsg, "/");
            }
        }


        private void initListing()
        {
            var results = _member.Data.GetAccountBalanceHistoryItems(_pagingURl.PageNo.GetValue(),
                                                (int)_pagingURl.ShowAmt.GetValue(),
                                                BusinessLogic_v3.Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_SORT_BY.DateDesc);
            balanceHistoryItems = new SearchPagedResults<IMemberAccountBalanceHistoryBase>(results.GetResults().Cast<IMemberAccountBalanceHistoryBase>(), results.TotalResults);
            initPagingBars(balanceHistoryItems.TotalResults);
            initTable();

        }

        private void initTable()
        {
            if (balanceHistoryItems != null && balanceHistoryItems.Count() > 0)
            {
                //string txtPriceText = OneOutOf100.Modules.Factories.ContentTextFactory.GetContentTextByIdentifier(Enums.CONTENT_TEXT.MembersArea_AccountStatement_Results_Table_PriceText).GetContent();

                MyTable tblStatementResults = new MyTable();

                tblStatementResults.CssClass = "listing-table";
                MyTableRow headerRow = new MyTableRow();
                headerRow.CssClass = "trHead";

                MyTableHeaderCell headerCellDateAndTime = new MyTableHeaderCell();
                MyTableHeaderCell headerCellDescription = new MyTableHeaderCell();
                MyTableHeaderCell headerCellMonthlyCredits = new MyTableHeaderCell();
                MyTableHeaderCell headerCellAdditionalCredits = new MyTableHeaderCell();
                MyTableHeaderCell headerCellTotalCredits = new MyTableHeaderCell();
                MyTableHeaderCell headerCellCreditDifference = new MyTableHeaderCell();

                MyContentText.AttachContentTextWithControl(headerCellDateAndTime, Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_CreditStatement_Heading_Date).ToFrontendBase());
                MyContentText.AttachContentTextWithControl(headerCellDescription, Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_CreditStatement_Heading_Description).ToFrontendBase());
                MyContentText.AttachContentTextWithControl(headerCellMonthlyCredits, Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_CreditStatement_Heading_MonthlyCredits).ToFrontendBase());
                MyContentText.AttachContentTextWithControl(headerCellAdditionalCredits, Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_CreditStatement_Heading_AdditionalCredits).ToFrontendBase());
                MyContentText.AttachContentTextWithControl(headerCellCreditDifference, Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_CreditStatement_Heading_CreditDifference).ToFrontendBase());
                MyContentText.AttachContentTextWithControl(headerCellTotalCredits, Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_CreditStatement_Heading_TotalCredits).ToFrontendBase());

                headerRow.Controls.Add(headerCellDateAndTime);
                headerRow.Controls.Add(headerCellDescription);
                
                if(_showMonthlyCredits)
                {
                    headerRow.Controls.Add(headerCellMonthlyCredits);
                }
                
                if(_showCreditDiffference)
                {
                    headerRow.Controls.Add(headerCellCreditDifference);
                }

                if(_showAdditionalCredits)
                {
                    headerRow.Controls.Add(headerCellAdditionalCredits);
                }

                headerRow.Controls.Add(headerCellTotalCredits);
                tblStatementResults.Controls.Add(headerRow);

                foreach (IMemberAccountBalanceHistoryBase accountBalHistoryItem in balanceHistoryItems)
                {
                    MemberAccountBalanceHistoryBaseFrontend item = accountBalHistoryItem.ToFrontendBase();

                    MyTableRow row = new MyTableRow();
                    MyTableCell cellDateAndTime = new MyTableCell();
                    MyTableCell cellDescription = new MyTableCell();
                    MyTableCell cellMonthlyCredits = new MyTableCell();
                    MyTableCell cellAdditionalCredits = new MyTableCell();
                    MyTableCell cellCreditDifference = new MyTableCell();
                    MyTableCell cellTotal = new MyTableCell();

                    cellDateAndTime.InnerHtml = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatDateTimeWithCurrentCulture(item.Data.Date,CS.General_v3.Util.Date.DATETIME_FORMAT.FullDateShortTime);
                    row.Controls.Add(cellDateAndTime);

                    cellDescription.Controls.Add(item.GetDescription());
                    row.Controls.Add(cellDescription);


                    if (_showMonthlyCredits)
                    {
                        cellMonthlyCredits.InnerHtml = item.GetMonthlyCredits().ToString();
                        row.Controls.Add(cellMonthlyCredits);
                    }

                    if(_showAdditionalCredits)
                    {
                        cellAdditionalCredits.InnerHtml = item.GetAdditionalCredits().ToString();
                        row.Controls.Add(cellAdditionalCredits);
                    }

                    if (_showCreditDiffference)
                    {
                        cellCreditDifference.InnerHtml = item.GetCreditDifference().ToString();
                        row.Controls.Add(cellCreditDifference);
                    }

                    cellTotal.InnerHtml = item.GetTotal().ToString();
                    row.Controls.Add(cellTotal);

                    tblStatementResults.Controls.Add(row);
                }
                phTableContainer.Controls.Add(tblStatementResults);
            }
            else
            {
                pagingBarTop.Visible = false;
                phTableContainer.Visible = false;
            }
        }

        private void initPagingUrl()
        {
            if(this.Functionality.Article != null)
            {
                _pagingURl = new URLParserOrderHistory(this.Functionality.Article);
            }
        }


        private void initPagingBars(int count)
        {
            pagingBarTop.Functionality.UpdateValues(_pagingURl, count, false, true/*, Enums.ACCOUNT_STATEMENT_SORT_BY_PREFIX_IDENTIFIER.AccountStatement_Combo_Prefix*/);
            pagingBarBottom.Functionality.UpdateValues(_pagingURl, count, true);

            pagingBarTop.Functionality.ResultsText.Functionality.ResultsTextPlural = pagingBarBottom.Functionality.ResultsText.Functionality.ResultsTextPlural = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_CreditStatement_ResultsTextPlural).ToFrontendBase();
            pagingBarTop.Functionality.ResultsText.Functionality.ResultsTextSingular = pagingBarBottom.Functionality.ResultsText.Functionality.ResultsTextSingular = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_CreditStatement_ResultsTextSingular).ToFrontendBase();
            //updatePagingBarsFromContentText(pagingBarTop, pagingBarBottom);
        }

        //private void updatePagingBarsFromContentText(params PagingBar[] pagingBars)
        //{
        //    string _itemsText =
        //        OneOutOf100.Modules.Factories.ContentTextFactory.GetContentTextByIdentifier(
        //            Enums.CONTENT_TEXT.MembersArea_AccountStatement_Results_Listing_ItemsText).GetContent();
        //    string _noResultsText = OneOutOf100.Modules.Factories.ContentTextFactory.GetContentTextByIdentifier(
        //            Enums.CONTENT_TEXT.MembersArea_AccountStatement_Results_Listing_NoResultsText).GetContent();
        //    string _resultsTextPlural = OneOutOf100.Modules.Factories.ContentTextFactory.GetContentTextByIdentifier(
        //            Enums.CONTENT_TEXT.MembersArea_AccountStatement_Results_Listing_ResultsTextPlural).GetContent();
        //    string _resultsTextSingular = OneOutOf100.Modules.Factories.ContentTextFactory.GetContentTextByIdentifier(
        //            Enums.CONTENT_TEXT.MembersArea_AccountStatement_Results_Listing_ResultsTextSingular).GetContent();
        //    string _showingResultsFullText = OneOutOf100.Modules.Factories.ContentTextFactory.GetContentTextByIdentifier(
        //            Enums.CONTENT_TEXT.MembersArea_AccountStatement_Results_Listing_ShowingResultsFullText).GetContent();

        //    foreach (PagingBar pagingBar in pagingBars)
        //    {
        //        pagingBar.Functionality.ResultsText.Functionality.ItemsText = _itemsText;
        //        pagingBar.Functionality.ResultsText.Functionality.NoResultsText = _noResultsText;
        //        pagingBar.Functionality.ResultsText.Functionality.ResultsTextPlural = _resultsTextPlural;
        //        pagingBar.Functionality.ResultsText.Functionality.ResultsTextSingular = _resultsTextSingular;
        //        pagingBar.Functionality.ResultsText.Functionality.ShowingResultsFullText = _showingResultsFullText;
        //    }
        //}

        //private void updateContentTexts()
        //{
        //    ltlResultsHeading.Text =
        //        OneOutOf100.Modules.Factories.ContentTextFactory.GetContentTextByIdentifier(
        //            Enums.CONTENT_TEXT.MembersArea_AccountStatement_Results_ResultsHeaderText).GetContent();
        //}
    }
}