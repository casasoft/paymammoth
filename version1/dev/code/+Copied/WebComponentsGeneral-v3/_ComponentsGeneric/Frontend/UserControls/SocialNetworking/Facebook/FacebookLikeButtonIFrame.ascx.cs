﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.SocialNetworking.Facebook
{
    using System;
    using System.Web.UI;
    using Code.Controls.UserControls;
    using CS.General_v3;

    public partial class FacebookLikeButtonIFrame : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private FacebookLikeButtonIFrame _control;
            public FUNCTIONALITY(FacebookLikeButtonIFrame control)
            {
                _control = control;
                this.Width = 80;
                this.Height = 21;
            }

            public string AppID { get; set; }
            public string URL { get; set; }
            public Enums.FACEBOOK_LAYOUT_STYLE LayoutStyle { get; set; }
            public int Width { get; set; }
            public bool ShowFaces { get; set; }
            public Enums.FACEBOOK_VERB VerbToDisplay { get; set; }
            public Enums.FACEBOOK_COLOUR_SCHEME ColourScheme { get; set; }
            public Enums.FACEBOOK_FONT Font { get; set; }
            public int Height { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }


        public string URL { get { return this.Functionality.URL; } set { this.Functionality.URL = value; } }
        public Enums.FACEBOOK_LAYOUT_STYLE LayoutStyle { get { return this.Functionality.LayoutStyle; } set { this.Functionality.LayoutStyle = value; } }
        public int Width { get { return this.Functionality.Width; } set { this.Functionality.Width = value; } }
        public int Height { get { return this.Functionality.Height; } set { this.Functionality.Height = value; } }
        public bool ShowFaces { get { return this.Functionality.ShowFaces; } set { this.Functionality.ShowFaces = value; } }
        public Enums.FACEBOOK_VERB VerbToDisplay { get { return this.Functionality.VerbToDisplay; } set { this.Functionality.VerbToDisplay = value; } }
        public Enums.FACEBOOK_COLOUR_SCHEME ColourScheme { get { return this.Functionality.ColourScheme; } set { this.Functionality.ColourScheme = value; } }
        public Enums.FACEBOOK_FONT Font { get { return this.Functionality.Font; } set { this.Functionality.Font = value; } }



        public FacebookLikeButtonIFrame()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initFacebookLikeHref();
        }



        private void initFacebookLikeHref()
        {

            string src = "//www.facebook.com/plugins/like.php?href";
            if (!string.IsNullOrWhiteSpace(this.Functionality.URL))
            {
                src += "=" + Server.UrlEncode(this.Functionality.URL);
            }
            src += "&send=false" ; //Iframe does not support send button
            src += "&layout=" + CS.General_v3.Util.EnumUtils.StringValueOf(this.Functionality.LayoutStyle);
            src += "&width=" + this.Width;
            src += "&show_faces=" + this.Functionality.ShowFaces.ToString().ToLower();
            src += "&action=" + CS.General_v3.Util.EnumUtils.StringValueOf(this.Functionality.VerbToDisplay);
            src += "&colorscheme=" + CS.General_v3.Util.EnumUtils.StringValueOf(this.Functionality.ColourScheme);
            src += "&font=" + CS.General_v3.Util.EnumUtils.StringValueOf(this.Functionality.Font);
            src += "&height=" + this.Height;
            if (!string.IsNullOrEmpty(this.Functionality.AppID))
            {
                src += "&appId=" + this.Functionality.AppID;
            }

            iFrameLikeButton.Attributes["src"] = src;
            iFrameLikeButton.Style.Add(HtmlTextWriterStyle.Width, this.Functionality.Width + "px");
            iFrameLikeButton.Style.Add(HtmlTextWriterStyle.Height, this.Functionality.Height + "px");

        }

    }

}