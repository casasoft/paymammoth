﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.General_v3;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Classes.Text;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage
{
    public partial class WelcomeMessage : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private WelcomeMessage _control;
            public FUNCTIONALITY(WelcomeMessage control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public WelcomeMessage()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
        }

        private void updateTexts()
        {
            ContentTextBaseFrontend cntTxt = null;
            TokenReplacerNew dict = null;
            
            var user = BusinessLogic_v3.Modules.Factories.MemberFactory.GetLoggedInUserForCurrentSession().ToFrontendBase();
            if(user != null)
            {
                cntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MasterPage_AuthenticationUser_WelcomeMessageLoggedIn).ToFrontendBase();
                dict = cntTxt.CreateTokenReplacer();
                dict.Add(BusinessLogic_v3.Constants.Tokens.USER_FIRST_NAME, ((IMemberAddressDetails)user.Data).FirstName);
                
            }
            else
            {
                cntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MasterPage_AuthenticationUser_WelcomeMessageLoggedOut).ToFrontendBase();
            }
            MyContentText.AttachContentTextWithControl(divUserMessage, cntTxt, dict);
        }
    }
}