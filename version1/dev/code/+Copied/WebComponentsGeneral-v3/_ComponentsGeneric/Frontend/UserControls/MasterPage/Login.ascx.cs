﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Modules.ContentTextModule;
using CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Article;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage
{
    using System;
    using Code.Classes.Pages;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using BusinessLogic_v3.Classes.Interfaces;
    using CS.General_v3.Classes.Login;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using BusinessLogic_v3.Modules.MemberModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;
    using BusinessLogic_v3;
    using BusinessLogic_v3.Frontend.ContentTextModule;

    public partial class Login : BaseUserControl
    {
        private bool _rememberMeEnabled = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_RememberMeEnabled);
        private bool _usingUsernameForLogin;
        private const string LOGIN_FIELDS_VALIDATION_GROUP = "loginFields";

        private PageLoginHandler __loginHandler;
        private PageLoginHandler _loginHandler
        {
            get
            {
                if (__loginHandler == null)
                {
                    __loginHandler = new PageLoginHandler(this.Page);
                }
                return __loginHandler;
            }
        }
        public class FUNCTIONALITY
        {
            private Login _control;




            public event LoginFieldsSuccessHandler OnLoginSuccess { add { _control._loginHandler.OnLoginSuccess += value; } remove { _control._loginHandler.OnLoginSuccess -= value; } }
            public event LoginFieldsErrorHandler OnLoginFail { add { _control._loginHandler.OnLoginFail += value; } remove { _control._loginHandler.OnLoginFail -= value; } }

            public bool OnLoginAutoRedirect { get { return _control._loginHandler.OnLoginAutoRedirect; } set { _control._loginHandler.OnLoginAutoRedirect = value; } }
            public bool OnLoginRedirectToLastNoAccessURL { get { return _control._loginHandler.OnLoginRedirectToLastNoAccessURL; } set { _control._loginHandler.OnLoginRedirectToLastNoAccessURL = value; } }
            public string OnLoginRedirectToURL { get { return _control._loginHandler.OnLoginRedirectToURL; } set { _control._loginHandler.OnLoginRedirectToURL = value; } }

            public ContentTextBaseFrontend LblUsername { get; set; }
            public ContentTextBaseFrontend LblEmail { get; set; }
            public ContentTextBaseFrontend LblPassword { get; set; }
            public ContentTextBaseFrontend BtnSubmitText { get; set; }
            public ContentTextBaseFrontend ChkRememberMeText { get; set; }
            public ContentTextBaseFrontend ForgotPasswordText { get; set; }
            public ContentTextBaseFrontend RegisterText { get; set; }
            public bool DoNoShowNavigationLinksContainer { get; set; }
            public string RegisterUrl { get; set; }
            public string ForgotPasswordURL { get; set; }
            public string ValidationGroup { get; set; }

            public FUNCTIONALITY(Login control)
            {
                _control = control;

                //Forgot Password URL
                if (string.IsNullOrEmpty(this.ForgotPasswordURL))
                {
                    var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_ForgotPassword);
                    if (article != null)
                    {
                        this.ForgotPasswordURL = article.GetUrl();
                    }
                }

                //Register URL
                if(string.IsNullOrEmpty(this.RegisterUrl))
                {
                    var articleRegister = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_Register);
                    if (articleRegister != null)
                    {
                        this.RegisterUrl = articleRegister.GetUrl();
                    }  
                }
            }


        }
        public FUNCTIONALITY Functionality { get; private set; }
        public Login()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            
            return new FUNCTIONALITY(this);

        }

        private void updateTexts()
        {
            // UPDATE TEXTS FROM CONTENT TEXT HERE
            anchorForgotPassword.HRef = this.Functionality.ForgotPasswordURL;
            anchorRegister.HRef = this.Functionality.RegisterUrl;

            //Labels
            this.Functionality.LblUsername = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Username).ToFrontendBase();
            this.Functionality.LblEmail = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Email).ToFrontendBase();
            this.Functionality.LblPassword = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Password).ToFrontendBase();
            this.Functionality.ChkRememberMeText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_RememberMe).ToFrontendBase();
           
            this.Functionality.ForgotPasswordText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_ForgotPassword).ToFrontendBase();
            MyContentText.AttachContentTextWithControl(anchorForgotPassword, this.Functionality.ForgotPasswordText);

            this.Functionality.BtnSubmitText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_LoginFields_LoginButtonText).ToFrontendBase();
            MyContentText.AttachContentTextWithControl(btnLogin, this.Functionality.BtnSubmitText);

            this.Functionality.RegisterText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_LoginFields_RegisterAccount).ToFrontendBase();
            MyContentText.AttachContentTextWithControl(anchorRegister, this.Functionality.RegisterText);
        }

        private void initLoginFields()
        {

            chkKeepMeLoggedIn.Visible = _rememberMeEnabled;

            txtLoginEmail.FieldParameters.title = this.Functionality.LblEmail.GetContent();
            txtLoginPassword.FieldParameters.title = this.Functionality.LblPassword.GetContent();
            txtLoginUsername.FieldParameters.title = this.Functionality.LblUsername.GetContent();
            txtLoginEmail.NeverFillWithRandomValue = true;
            txtLoginPassword.NeverFillWithRandomValue = true;
            txtLoginUsername.NeverFillWithRandomValue = true;
            txtLoginEmail.AutoComplete = false;
            txtLoginPassword.AutoComplete = false;
            if(!string.IsNullOrEmpty(this.Functionality.ValidationGroup))
            {
                txtLoginEmail.ValidationGroup = txtLoginPassword.ValidationGroup = txtLoginUsername.ValidationGroup = btnLogin.ValidationGroup = this.Functionality.ValidationGroup;
            }

            if(BusinessLogic_v3.Modules.ModuleSettings.Members.LoginWithUsername)
            {
                txtLoginUsername.PlaceholderText = this.Functionality.LblUsername.GetContent();
                _usingUsernameForLogin = true;
                txtLoginEmail.Visible = false;
            }
            else
            {
                txtLoginEmail.PlaceholderText = this.Functionality.LblEmail.GetContent();
                txtLoginUsername.Visible = false;
            }
            txtLoginPassword.PlaceholderText = "*******";
            chkKeepMeLoggedIn.LabelContentText = this.Functionality.ChkRememberMeText;
            btnLogin.Click +=new EventHandler(btnLogin_Click);

            if(this.Functionality.DoNoShowNavigationLinksContainer)
            {
                divNavigationalLinks.Visible = false;
            }

        }

        void  btnLogin_Click(object sender, EventArgs e)
        {
            string credential = null;
            credential = _usingUsernameForLogin ? txtLoginUsername.GetFormValueAsString() : txtLoginEmail.GetFormValueAsString();

            string pass = txtLoginPassword.GetFormValueAsString();
            bool rememberMe = _rememberMeEnabled ? chkKeepMeLoggedIn.GetFormValueObjectAsBool() : false;
            _loginHandler.LoginUser(credential, pass, rememberMe);
        }

        protected override void OnLoad(EventArgs e)
        {
            updateTexts();
            initLoginFields();
            base.OnLoad(e);
        }
    }
}