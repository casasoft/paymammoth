﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.Effects.Toggle;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.ProductModule;
using BusinessLogic_v3.Extensions;
namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    using BusinessLogic_v3.Classes.Routing;
    using CS.General_v3;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public partial class ProductFullPage : BaseUserControl
    {


        #region ProductFullPage Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected ProductFullPage _item = null;

            public ProductBaseFrontend Product { get; set; }
            internal FUNCTIONALITY(ProductFullPage item)
            {
                this._item = item;
            }
         
            public ProductAddToCart AddToCart
            {
                get { return _item.productDetails.Functionality.ProductAddToCart; }
                set { _item.productDetails.Functionality.ProductAddToCart = value; }
            }
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			
        protected void Page_Load(object sender, EventArgs e)
        {
            init();
        }

        private void init()
        {
            initProduct();
        }

        private void updateContent()
        {
            initEnquiry();
            loadPage();
        }

        private void loadPage()
        {
            var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Product);
            this.Page.Functionality.ContentPage = article.ToFrontendBase();
        }

        private void initEnquiry()
        {
            bool showContactForm = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_FullPageShowContactForm);
            if(showContactForm)
            {
                contactForm.Functionality.OnSubmittedSuccess += new CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Contact.ContactUsForm.ContactUsFieldsOnSubmittedSuccessHandler(Functionality_OnSubmittedSuccess);
                bool toggleContactForm = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_FullPageToggleContactForm);
                if(toggleContactForm)
                {
                    MyContentText.AttachContentTextWithControl(anchorToggleEnquiry, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ProductPage_ContactForm_ToggleButtonText).ToFrontendBase());
                   
                    ToggleControllerParameters p = new ToggleControllerParameters();
                    p.elemToggleSelector = "#" + anchorToggleEnquiry.ClientID;
                    p.elemSelectorContentOn = ".contact-us-online-enquiry-form";
                    p.onStateOnHideToggleSelector = true;
                }
                else
                {
                    anchorToggleEnquiry.Visible = false;
                }
            }
            else
            {
                divContactFormContainer.Visible = false;
            }

        }

        void Functionality_OnSubmittedSuccess(CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Contact.ContactUsForm sender, CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Contact.ContactUsForm.ContactUsFormFieldsData data)
        {
            this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ProductPage_ContactForm_EnquirySentSuccessfully).ToFrontendBase());
        }


        private void initProduct()
        {
            
            if (this.Functionality.Product != null)
            {
                initAddThis();
                initSEO();
                initCMSEdit();
                updateContent();
                initRelatedProducts();
                initNavigationBreadcrumbs(this.Functionality.Product);
                initContactForm();
            }
            else
            {
                CS.General_v3.Util.PageUtil.RedirectPage(ErrorsRoute.GetPageNotFoundErrorURL());
            }
        }

        private void initContactForm()
        {
            contactForm.Functionality.Product = this.Functionality.Product;
            contactForm.Functionality.EnquiryDescription = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.ProductPage_ContactForm_EnquiryDescription).ToFrontendBase();
        }

        private void initCMSEdit()
        {
            this.Page.Functionality.SetEditInCmsButtonObject(this.Functionality.Product.Data);
            productDetails.Functionality.Product = this.Functionality.Product;
            this.cmsEditItem.Functionality.DbObject = this.Functionality.Product.Data;
        }
        private void initSEO()
        {

            var addCategoryToTitle = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_FullPageAddCategoryToMetaTitleForSEO);
            

            this.Page.Functionality.UpdateTitleAndMetaTags(this.Functionality.Product.GetMetaTitle(addCategoryToTitle), this.Functionality.Product.GetMetaKeywords(), this.Functionality.Product.GetMetaDescription());
        }
        

        private void initAddThis()
        {
            var showTopAddThis = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_ShowTopAddThisInFullPage);
            if (showTopAddThis)
            {
                addThisCtrl.Functionality.URL = this.Functionality.Product.Data.GetFrontendUrl();
            }
            else
            {
                divProductTopAddThis.Visible = false;
            }
        }

        private void initNavigationBreadcrumbs(ProductBaseFrontend product)
        {
            navigationBreadcrumbs.Functionality.AddFromHierarchy(product.Data.GetCategories().FirstOrDefault().ToFrontendBase(), true);
            navigationBreadcrumbs.Functionality.AddItem(product.GetTitle(), product.Data.GetFrontendUrl());
            var includeHome = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_IncludeHomeInBreadcrumbs);
            if(includeHome)
            {
                navigationBreadcrumbs.Functionality.AddItem(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Home).GetContent(), ",",true);
            }
        }

        private void initRelatedProducts()
        {
            var showRelatedProducts = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_ShowRelatedProductsInFullpage);
            if(showRelatedProducts)
            {
                RelatedProducts.Functionality.Product = this.Functionality.Product;

            }
            else
            {
                RelatedProducts.Visible = false;
            }
        }
    }
}