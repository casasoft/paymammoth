﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.Paging
{
    using General_v3.Classes.SearchParams;
    using General_v3.Controls.WebControls.Specialized.Paging;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Classes.URL;
    using BusinessLogic_v3.Modules.ContentTextModule;
    using BusinessLogic_v3;

    public partial class PagingPages : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private PagingPages _control;

            /// <summary>
            /// Pages start from 1, NOT 0
            /// </summary>
            //public int? SelectedPageNum { get; set; }
            public int? TotalPages { get; set; }
            
            /// <summary>
            /// The marker to be added where it should indicate that other pages exist
            /// </summary>
            public string ContinuationMarkerHtml { get; set; }

            /// <summary>
            /// The amount of pages to show to the left/right of the selected page.  For example, if 2 is set
            /// and the selected page is 6, then the pages shown are ...4, 5, [6], 7, 8...
            /// </summary>
            public int AmtNearestPagesShown { get; set; }
            public string ButtonNextText { get; set; }
            public string ButtonLastText { get; set; }
            public string ButtonFirstText { get; set; }
            public string ButtonPrevText { get; set; }

            public IURLParserPagingInfo PagingInfo { get; set; }

            //public ListItemCollection SortByComboBoxValues { get; set; }
            //public ListItemCollection ShowAmtComboBoxValues { get; set; }

            /// <summary>
            /// The Url to use in the links.  If set to null, it will take the URL of the current page
            /// </summary>
            public string Url { get; set; }
            /// <summary>
            /// The parameter to use in the querystring
            /// </summary>
            public string QuerystringParamPage { get; set; }


            /*public void UpdateValuesFromPagingInfo(IURLParserPagingInfo pgInfo)
            {
                this.PagingInfo = pgInfo;
                //this.SelectedPageNum = pgInfo.PageNo;
                
            }
            public void UpdateValues(int pgNum, int pgSize, int totalResults)
            {
                this.TotalPages = (int)Math.Ceiling((double)totalResults / (double)pgSize);
                this.SelectedPageNum = pgNum;
            }

            */

            public FUNCTIONALITY(PagingPages control)
            {
                _control = control;
                //this.SelectedPageNum = 1;

                AmtNearestPagesShown = 2;
                ButtonFirstText = "«";
                ButtonPrevText = "‹";
                ButtonNextText = "›";
                ButtonLastText = "»";
                QuerystringParamPage = "pg";


            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public PagingPages()
        {
            this.Functionality = createFunctionality();

        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            if (this.Visible)
            {
                updateFromContentText();
                initControls();
            }

            base.OnLoad(e);
        }

        private void updateFromContentText()
        {
            if(BusinessLogic_v3.Modules.ModuleSettings.Generic.MultiLingual)
            {
                Functionality.ButtonFirstText = ((ContentTextBase) BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.General_Paging_Navigation_ButtonFirstText)).GetContent();
                Functionality.ButtonLastText = ((ContentTextBase) BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.General_Paging_Navigation_ButtonLastText)).GetContent();
                Functionality.ButtonNextText = ((ContentTextBase) BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.General_Paging_Navigation_ButtonNextText)).GetContent();
                Functionality.ButtonPrevText = ((ContentTextBase) BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        Enums.CONTENT_TEXT.General_Paging_Navigation_ButtonPrevText)).GetContent();
            }
        }
    
    

        private ListItemCollection generatePages()
        {
            ListItemCollection pages = new ListItemCollection();

            int amtPages = this.Functionality.TotalPages.Value;
            int selectedPgNum = this.Functionality.PagingInfo.PageNo;
            if (selectedPgNum > 1)
            {
                //Can have prev
                pages.Add(new ListItem(Functionality.ButtonFirstText, "1"));
                pages.Add(new ListItem(Functionality.ButtonPrevText, (selectedPgNum - 1).ToString()));
                if (!String.IsNullOrEmpty(Functionality.ContinuationMarkerHtml))
                {
                    pages.Add((string)null); // Add marker
                }
            }

            int fromIndex = Math.Max(1, selectedPgNum - Functionality.AmtNearestPagesShown);
            int toIndex = Math.Min(amtPages, selectedPgNum + Functionality.AmtNearestPagesShown);
            for (int i = fromIndex; i <= toIndex; i++)
            {
                pages.Add(new ListItem(i.ToString(), i.ToString()) { Selected = i == selectedPgNum });
            }
            if (selectedPgNum < amtPages)
            {
                if (Functionality.ContinuationMarkerHtml != null)
                {
                    pages.Add((string)null); // Add marker
                }
                pages.Add(new ListItem(Functionality.ButtonNextText, (selectedPgNum + 1).ToString()));
                pages.Add(new ListItem(Functionality.ButtonLastText, amtPages.ToString()));
            }


            return pages;
        }

        private void initPages()
        {
            ulPages.Controls.Clear();

            ListItemCollection pages = generatePages();

            

            //Create items
            for (int i = 0; i < pages.Count; i++)
            {
                bool isContinuationDots = String.IsNullOrEmpty(pages[i].Text) && String.IsNullOrEmpty(pages[i].Value);
                if (isContinuationDots)
                {
                    //Show continuation dots
                    addPage(ulPages, new Literal() { Text = Functionality.ContinuationMarkerHtml });
                }
                else
                {
                    MyAnchor a = new MyAnchor();
                    a.InnerText = pages[i].Text;
                    if (pages[i].Selected)
                    {
                        a.CssManager.AddClass("selected");
                    }
                    int pgNo = 0;
                    int.TryParse(pages[i].Value, out pgNo);
                    string url = this.Functionality.PagingInfo.GetDifferentPageURL(pgNo);
                    //url.QS.Set(Functionality.QuerystringParamPage, pages[i].Value);
                    a.Href = url;
                    addPage(ulPages, a);

                    int temp;
                    if (!int.TryParse(pages[i].Text, out temp))
                    {
                        a.CssManager.AddClass("pages-continuation-marker");
                    }
                }

            }

        }
        private void addPage(Control ulPages, Control pg)
        {
            MyListItem li = new MyListItem();
            li.Controls.Add(pg);
            ulPages.Controls.Add(li);
        }

        private void initComboboxes()
        {

        }
        private void checkRequirements()
        {
            if (this.Functionality.PagingInfo == null) throw new ArgumentNullException("PagingInfo");
            if (!this.Functionality.TotalPages.HasValue) throw new InvalidOperationException("Please specify TotalPages");
        }
        protected void initControls()
        {
            initPages();
            initComboboxes();
        }

        
    }

}