﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Purchase
{
    using BusinessLogic_v3.Classes.Routing;
    using BusinessLogic_v3.Extensions;
    using BusinessLogic_v3.Modules;
    using BusinessLogic_v3.Modules.MemberModule;
    using BusinessLogic_v3.Modules.OrderModule;
    using General_v3.Util;
    using BusinessLogic_v3;
    using BusinessLogic_v3.Frontend.OrderModule;

    public partial class PayMammothPurchaseSuccess : BaseUserControl
    {

        private OrderBaseFrontend _successOrder;
        /// <summary>
        /// This is triggered on init so that if there are sections showing account balance, it would show updated balance
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            getOrderFromSession();
            loadPage();
            loadPaymentDetails();
        }

        private void loadPaymentDetails()
        {
            var showPaymentDetailsInSuccessPage = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Payment_SucesPage_ShowPaymentDetails);
            if (showPaymentDetailsInSuccessPage)
            {
                paymentDetails.Functionality.Order = _successOrder;
            }
            else
            {
                paymentDetails.Visible = false;
            }
        }

        private void getOrderFromSession()
        {
            long? orderId = CS.General_v3.Util.SessionUtil.GetObject<long?>(PayMammothConnectorBL.v3.Constants.SESSION_ORDER_DETAILS);
            if (orderId.HasValue)
            {
                IOrderBase order = BusinessLogic_v3.Modules.Factories.OrderFactory.GetByPrimaryKey(orderId.Value);
                if (order != null)
                {
                    _successOrder = order.ToFrontendBase();
                }
            }
            else
            {
                throw new Exception("Order session cannot be null. This session is being set in the succesHandler");
            }
        }


        private void loadPage()
        {
            var article = Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.PayPippa_FinalSuccessPage).ToFrontendBase();
            if (article != null)
            {
                contentPage.Functionality.ContentPage = article;
                var replacementValues = article.CreateTokenReplacer();
                replacementValues.Add(BusinessLogic_v3.Constants.Tokens.INVOICE_LINK, _successOrder.GetURL());
                replacementValues.Add(BusinessLogic_v3.Constants.Tokens.INVOICE_NUMBER, _successOrder.Data.Reference);
                contentPage.Functionality.ContentTagReplacementValues = replacementValues;
            }
        }
    }
}