﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.MasterPage.Footer
{
    public partial class CasaSoftFooterLink : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private CasaSoftFooterLink _control;
            public FUNCTIONALITY(CasaSoftFooterLink control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public CasaSoftFooterLink()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
        }

        private void updateTexts()
        {
            var cntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Footer_CasaSoft).ToFrontendBase();
            var siteUrl = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.Others_CasaSoftWebsiteURL);
            anchorCasasoftLink.Href = siteUrl;
            anchorCasasoftLink.Title = "Web design and development from Malta and Europe - Web Applications, Rich Interface Applications (RIA), Flash Development, Content Management Systems (CMS)";
            MyContentText.AttachContentTextWithControl(anchorCasasoftLink, cntTxt);
        }
    }
}