﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.CategoryImageModule;
using BusinessLogic_v3.Frontend.CategoryModule;
using BusinessLogic_v3.Modules.CategoryImageModule;
using BusinessLogic_v3.Modules.CategoryModule;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Tooltips.QTip;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.General_v3.Classes.Interfaces.Hierarchy;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Hierarchy;
using CS.WebComponentsGeneralV3.Code.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    public partial class ProductCategoriesListing : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private ProductCategoriesListing _control;
            public FUNCTIONALITY(ProductCategoriesListing control)
            {
                _control = control;
                this.OutputIconIdentifierAsSpan = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Categories_MenuListingOutputIconIdentifierAsSpan);
                this.OutputIconImage = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Categories_MenuListingOutputIconImage);
            }
            public ContentTextBaseFrontend Title { get; set; }
            public bool DoNotShowTitle { get; set; }
            private bool _outputIconImage;
            public bool OutputIconImage
            {
                get { return _outputIconImage; }
                set { 
                    _outputIconImage = value;
                    this._control.CacheParams.CacheControl = !value;
                }
            }
                public bool OutputIconIdentifierAsSpan { get; set; }
            public int MaxDepth { get; set; }
            public string CssClass { get; set; } 
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ProductCategoriesListing()
        {
            this.Functionality = createFunctionality();

            //TODO:2012-05-25 Mark enable cache
            this.CacheParams.SetCacheDependencyFromFactories(true, BusinessLogic_v3.Modules.Factories.CategoryFactory);
            

        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            ForceRender();
            base.OnLoad(e);
        }

        public void ForceRender()
        {
            init();
        }

        private void init()
        {
            var root = BusinessLogic_v3.Modules.Factories.CategoryFactory.GetRootCategory();

            if (root != null)
            {
                if(this.Functionality.MaxDepth != 0)
                {
                    productCategories.Functionality.MaxDepth = this.Functionality.MaxDepth;
                }
                if (!IsPostBack)
                {//only show categories if postback (Karl / 2012-jun-08)
                    IEnumerable<ICategoryBase> items = root.GetChildCategories();
                    if (items != null && items.Count() > 0)
                    {
                        productCategories.Functionality.Items = items.ToFrontendBaseList();
                        productCategories.Functionality.OnListItemCreate += new HierarchicalControlNavigation.FUNCTIONALITY.ListItemCreateHandler(Functionality_OnListItemCreate);
                    }
                }
            }

            updateTexts();
        }
        private void initImageQTip(CategoryBaseFrontend category, MyAnchor a)
        {
            bool showQTipWithProductCategoryListing = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Categories_ShowQTipWithProductCategoryListing);
            if (showQTipWithProductCategoryListing)
            {
                var image = category.Data.ProductImagesManager.GetRandomProductImage().ToFrontendBase();
                if (image != null)
                {
                    var imageurl = image.Data.Image.GetSpecificSizeUrl(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase.ImageSizingEnum.ProductCategoriesListingImage);
                    if (!string.IsNullOrWhiteSpace(imageurl))
                    {

                        jQueryQTip t = new jQueryQTip();
                        t.ControlToAddJavascriptTo = ltlJavascript;
                        _jQueryQTipOptions o = new _jQueryQTipOptions();
                        string alterateText;
                        alterateText = !string.IsNullOrEmpty(image.Caption) ? image.Caption : category.Title;
                        var productImage = createProductImage(imageurl, alterateText);
                        o.content = new _jQueryQTipOptionsContent();
                        var s = o.style = new _jQueryQTipOptionsStyle();
                        o.hide = new _jQueryQTipOptionsHide();
                        o.hide.delay = 0;
                       // o.hide.effect = false;
                        o.position = new _jQueryQTipOptionsPosition();
                        o.position.target = "right center";
                        o.position.my = "left center";
                        o.position.at = "left center";
                        o.show = new _jQueryQTipOptionsShow();
                        o.show.delay = 0;
                        //o.show.effect = false;
                        s.classes = "product-categories-listing-qtip";
                        o.content.text = CS.General_v3.Util.ControlUtil.RenderControl(productImage);
                        t.Options = o;

                        string js = @"var img = new Image(); img.src = '" + imageurl.Replace("'", "\\'") + "';";
                        JSUtil.AddJSScriptToControl(js, ltlJavascript, true);
                        
                        a.WebControlFunctionality.Tooltip = t;
                    }
                }
            }
        }

        void Functionality_OnListItemCreate(CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.MyListItem li,
            CS.General_v3.Classes.Interfaces.Hierarchy.IHierarchy item, int depth, int index, CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.MyAnchor a)
        {
            var category = (CategoryBaseFrontend) item;
                if (this.Functionality.OutputIconIdentifierAsSpan)
                {
                    MySpan spanIcon = new MySpan();
                    spanIcon.CssManager.AddClass("product-category-icon", "product-category-" + category.Data.Identifier);
                    li.Controls.Add(spanIcon);
                }
                
                if (this.Functionality.OutputIconImage && category.Data.Icon != null)
                {
                    string categoryIconURL = category.Data.Icon.GetSpecificSizeUrl(CategoryBase.IconSizingEnum.Thumbnail);
                    if (!string.IsNullOrEmpty(categoryIconURL))
                    {
                        a.Style.Add(HtmlTextWriterStyle.BackgroundImage, "url(" + categoryIconURL + ")");
                        MySpan spanIcon = new MySpan();
                        spanIcon.CssManager.AddClass("product-category-icon-image");
                        MyImage imgIcon = new MyImage();
                        imgIcon.ImageUrl = categoryIconURL;
                        imgIcon.AlternateText = category.GetCategoryTitleForSEO();
                        spanIcon.Controls.Add(imgIcon);
                        //li.Controls.Add(spanIcon);
                    }
                }

                initImageQTip(category, a);
        }

        private MyDiv createProductImage(string imageUrl, string alternateText)
        {
            MyDiv div = new MyDiv();
            div.CssManager.AddClass("product-listing-tooltip-image-container");
            MyImage img = new MyImage();
            img.ImageUrl = imageUrl;
            img.AlternateText = alternateText;
            img.ForceRender();
            div.Style.Add(HtmlTextWriterStyle.BackgroundImage, "url('"+imageUrl+"')");
            //div.Controls.Add(new Literal() { Text = "<img src='"+imageUrl+"' />" });
           // div.Controls.Add(img);
            return div;
        }

        private void updateTexts()
        {
            if(!this.Functionality.DoNotShowTitle && this.Functionality.Title != null)
            {
                MyContentText.AttachContentTextWithControl(divItemTitle, this.Functionality.Title);
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divItemTitle);
            }

            if(!string.IsNullOrEmpty(this.Functionality.CssClass))
            {
                divItemTitle.Attributes["class"] += " " + this.Functionality.CssClass;
            }
        }

        public static ProductCategoriesListing LoadControl(Page pg)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/Products/ProductCategoriesListing.ascx") as ProductCategoriesListing;
            return ctrl;
        }
    }
}