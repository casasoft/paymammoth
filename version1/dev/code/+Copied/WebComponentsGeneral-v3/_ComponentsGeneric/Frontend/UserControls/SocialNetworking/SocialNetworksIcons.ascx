﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SocialNetworksIcons.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.SocialNetworking.SocialNetworksIcons" %>
<div class="socialnetworks-links-container clearfix" runat="server" id="divSocialNetworksContainer">
    <a href="/" runat="server" class="socialnetworks-link socialnetworks-fb" id="anchorFacebook" target="_blank"></a>
    <a href="/" runat="server" class="socialnetworks-link socialnetworks-twitter" id="anchorTwitter" target="_blank"></a>
    <a href="/" runat="server" class="socialnetworks-link socialnetworks-linkedin" id="anchorLinkedIn" target="_blank"></a>
</div>
