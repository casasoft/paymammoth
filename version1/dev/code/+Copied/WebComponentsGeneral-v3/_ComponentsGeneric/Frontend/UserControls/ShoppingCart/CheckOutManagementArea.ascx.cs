﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ShippingMethodModule;
using BusinessLogic_v3.Frontend.ShoppingCartModule;
using BusinessLogic_v3.Modules.ShoppingCartModule;
using CS.General_v3.Controls.WebControls.Specialized.GoogleMaps.v3;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.MemberModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart
{
    public partial class CheckOutManagementArea : BaseUserControl
    {
        private const string APPLY_VOUCER_FIELDS = "applyVoucherFields";
        private ShoppingCartDeliveryDetailsHelper _deliveryDetails;
        public class FUNCTIONALITY
        {
            private CheckOutManagementArea _control;
            public ShoppingCartBaseFrontend ShoppingCart;
            public FUNCTIONALITY(CheckOutManagementArea control)
            {
                _control = control;
            }

            public ShoppingCartDeliveryDetailsHelper GetDeliveryDetails()
            {
                if (_control._deliveryDetails != null)
                {
                    _control.setDeliveryDetails();
                    return _control._deliveryDetails;
                }
                return null;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public CheckOutManagementArea()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void setDeliveryDetails(long? specifyId = null, bool setDefaultFromSystem = false)
        {
            if (_deliveryDetails != null)
            {
                if (specifyId.HasValue)
                {
                    ShippingMethodBaseFrontend shippingMethod = BusinessLogic_v3.Modules.Factories.ShippingMethodFactory.GetByPrimaryKey(specifyId.Value).ToFrontendBase();
                    _deliveryDetails.ShippingMethod = shippingMethod != null ? shippingMethod.Data : BusinessLogic_v3.Modules.Factories.ShippingMethodFactory.GetDefaultShippingMethod();
                }
                else if (setDefaultFromSystem)
                {
                    ShippingMethodBaseFrontend shippingMethod =
                        BusinessLogic_v3.Modules.Factories.ShippingMethodFactory.GetDefaultShippingMethod().
                            ToFrontendBase();
                    _deliveryDetails.ShippingMethod = shippingMethod.Data;
                }
                Session[BusinessLogic_v3.Constants.SessionIdentifiers.CHECKOUT_DELIVERY_DETAILS] = _deliveryDetails;
            }
        }

        private void initDeliveryDetails()
        {

            _deliveryDetails =
                (ShoppingCartDeliveryDetailsHelper)
                Session[BusinessLogic_v3.Constants.SessionIdentifiers.CHECKOUT_DELIVERY_DETAILS];

            if (_deliveryDetails != null)
            {
                shoppingCartPriceDetails.Functionality.DeliveryDetails = _deliveryDetails;
                var deliveryMethodId = CS.General_v3.Util.QueryStringUtil.GetLongFromQS(BusinessLogic_v3.Constants.ParameterNames.ID);
                if (deliveryMethodId.HasValue)
                {
                    shoppingCartPriceDetails.Functionality.CurrentDeliveryMethodUrlLocation = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL();
                    setDeliveryDetails(deliveryMethodId.Value);
                }
                else if (_deliveryDetails.ShippingMethod == null)
                {
                    setDeliveryDetails(setDefaultFromSystem: true);
                }
            }
        }

        private void init()
        {
            if (this.Functionality.ShoppingCart != null)
            {
                shoppingCartPriceDetails.Functionality.ShoppingCart = this.Functionality.ShoppingCart;
                initDeliveryDetails();
                initUpdateTexts();
                initHandlers();
                initFields();
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(shoppingCartPriceDetails);
            }
        }

        private void initFields()
        {
            txtBoxCoupon.FieldParameters.required = true;
            txtBoxCoupon.FieldParameters.validationGroup = btnApply.ValidationGroup = APPLY_VOUCER_FIELDS;
       }

        private void initUpdateTexts()
        {
            MyContentText.AttachContentTextWithControl(spanDiscountCouponInputText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_Discount_Text).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(btnApply, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_Discount_Button).ToFrontendBase());
        }

        private void initHandlers()
        {
            btnApply.Click += new EventHandler(btnApply_Click);
        }

        void btnApply_Click(object sender, EventArgs e)
        {
            string strVoucherCode = txtBoxCoupon.GetFormValueAsString();
            if (!string.IsNullOrEmpty(strVoucherCode))
            {
                Enums.SET_VOUCHERCODE_RESULT result = this.Functionality.ShoppingCart.Data.SetSpecialOfferVoucherCode(strVoucherCode);
                switch (result)
                {
                    case Enums.SET_VOUCHERCODE_RESULT.DoesNotExist:
                        this.Page.Functionality.ShowErrorMessageAndRedirectToPage(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_Discount_NotExists).ToFrontendBase());
                        break;
                    case Enums.SET_VOUCHERCODE_RESULT.ExpiredOrNotActiveYet:
                        this.Page.Functionality.ShowErrorMessageAndRedirectToPage(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_Discount_ExpiredOrNotActiveYet).ToFrontendBase());
                        break;
                    case Enums.SET_VOUCHERCODE_RESULT.OK:
                        this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ShoppingCart_CheckOut_Discount_Success).ToFrontendBase());
                        break;
                    default:
                        throw new InvalidOperationException("Result type <" + result + "> not yet implemented!");
                }
            }
        }
    }
}