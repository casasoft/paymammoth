﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ProductModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Util;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Products
{
    using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;
    using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
    using Code.Classes.Javascript.UI.jQuery.Carousel.CarouFredSel.v1;
    using Code.Classes.Javascript.UI.Products;
    using CS.General_v3.Classes.Interfaces.ImageItem;

    public partial class ProductImagesSlider : BaseUserControl
    {
        public class FUNCTIONALITY
        {
            private ProductImagesSlider _control;
            public ProductVariationControllerCarouFredSelParameters ProductVariationControllerCarouFredSelParameters { get; set; }
            public CarouFredSelV1Parameters CarouFredSelParameters { get { return _control.productImageSlider.Functionality.CarouselFredSelParameters; } }
                public FUNCTIONALITY(ProductImagesSlider control)
            {
                _control = control;
            }
                private void updateCachedDependencyForProduct(ProductBaseFrontend product)
                {
                    if (product != null)
                    {
                        List<IBaseDbObject> dbObjects = new List<IBaseDbObject>();
                        dbObjects.Add(_product.Data);
                        List<IProductVariationMediaItemBase> images = _control.getAllImagesForProduct();
                        foreach (var image in images)
                        {
                            dbObjects.Add(image);

                        }
                        _control.CacheParams.SetCacheDependencyFromDBObjects(dbObjects, true);
                    }

                }

            private ProductBaseFrontend _product;
                public ProductBaseFrontend Product
                {
                    get { return _product; }
                    set {
                        _product = value;
                        updateCachedDependencyForProduct(value);
                    }
                }
            }
        public FUNCTIONALITY Functionality { get; private set; }
        public ProductImagesSlider()
        {
            this.Functionality = createFunctionality();
            this.CacheParams.CacheControl = true;
            this.CacheParams.IsMultilingual = true;
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }
        private List<IProductVariationMediaItemBase> getAllImagesForProduct()
        {
            var includeMainImageInSlider = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_IncludeMainImageInSlider);
            List<IProductVariationMediaItemBase> images = this.Functionality.Product.Data.GetAllImages(includeMainImageInSlider).ToList();

            return images;
        }

        private void init()
        {
            if (this.Functionality.ProductVariationControllerCarouFredSelParameters != null)
            {
                this.Functionality.ProductVariationControllerCarouFredSelParameters._OutputInsideControl = placeholderJS;
            }

            updateTexts();
            productImageSlider.Functionality.CarouselFredSel.Functionality.Align = Carousel.CarouFredSel.CarouFredSelControl.CAROUFREDSEL_ALIGN.Left;
           // productImageSlider.Functionality.CarouselFredSelParameters.width = 710;
            bool showProductSlider = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_ShowProductImageSlider);
            if (showProductSlider)
            {
                if (this.Functionality.Product != null)
                {
                    List<IProductVariationMediaItemBase> images = getAllImagesForProduct();
                    productImageSlider.Functionality.AddImages(images);
                    if (images != null && images.Count() > 0)
                    {
                       // sortImagesByProductVariation(images);
                        string currVariationColour = null;
                        int imageIndex = 0;
                        foreach (var image in images)
                        {

                           // _ctrls.Add(new Literal() { Text =  "<div class='temp'></div>" });
                            //_ctrls.Add(createImage(image));
                            //productImageSlider.Functionality.Images = _ctrls;
                            if (this.Functionality.ProductVariationControllerCarouFredSelParameters != null && image.ProductVariation.Colour !=null && image.ProductVariation.Colour.Title != currVariationColour)
                            {
                                currVariationColour = image.ProductVariation.Colour.Title;
                                this.Functionality.ProductVariationControllerCarouFredSelParameters.variationsData.Add(new ProductVariationItemData()
                                {
                                    imageIndex = imageIndex,
                                    variationID = image.ProductVariation.ID.ToString()
                                });
                            }
                            imageIndex++;
                        }
                        if (this.Functionality.ProductVariationControllerCarouFredSelParameters != null)
                        {
                            this.Functionality.ProductVariationControllerCarouFredSelParameters.selectorCarouFredSel = "#" + productImageSlider.ClientID;
                        }

                        PrettyPhotoUtil.InitJS();
                    }
                    else
                    {
                        this.Visible = false;
                    }
                }
            }
            else
            {
                divProductImageSlider.Parent.Controls.Remove(divProductImageSlider);
            }
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(divProductImageSliderText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_Slider_ClikImageToEnlarge).ToFrontendBase());
        }

        private MyImage createImage(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBase image)
        {
            MyImage imgProduct = new MyImage();
            var imageUrl = image.Image.GetSpecificSizeUrl(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase.ImageSizingEnum.ImageSlider);
            imgProduct.ImageUrl = imageUrl;
            imgProduct.AlternateText = image.GetAlternateText();
            imgProduct.HRef = image.Image.GetSpecificSizeUrl(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase.ImageSizingEnum.Large);
            imgProduct.Rel = "prettyPhoto[fullPageSlider]";
            return imgProduct;
        }
    }
}