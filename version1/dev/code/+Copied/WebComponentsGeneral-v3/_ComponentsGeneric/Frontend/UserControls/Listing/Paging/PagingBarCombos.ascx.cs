﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.Paging
{
    using Code.Controls.UserControls;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using CS.WebComponentsGeneralV3.Code.Classes.URL;
    using BusinessLogic_v3;
    using BusinessLogic_v3.Modules.ContentTextModule;
using BusinessLogic_v3.Frontend.ContentTextModule;
    using BusinessLogic_v3.Classes.Text;


    public partial class PagingBarCombos : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private PagingBarCombos _control;

            //public EnumToContentTextHandler MultilingualSortByEnumToContentTextHandler { get; set; }
            //public EnumToContentTextHandler MultilingualShowAmtEnumToContentTextHandler { get; set; }

            private IURLParserPagingInfo _pagingInfo;
            public IURLParserPagingInfo PagingInfo
            {
                get { return _pagingInfo; }
                set
                {
                    _pagingInfo = value;
                }
            }
            public IEnumerable<int> ShowAmountValues { get; set; }
                //public Type SortByEnumType { get; set; }
            //public Type ShowAmtEnumType { get; set; }
            public General_v3.Enums.ENUM_SORT_BY SortByEnumTypeValueSorting { get; set; }

            //public Enum SelectedSortBy { get; set; }
            //public Enum SelectedShowAmt { get; set; }

            public FUNCTIONALITY(PagingBarCombos control)
            {
                _control = control;
                this.SortByEnumTypeValueSorting = General_v3.Enums.ENUM_SORT_BY.PriorityAttributeValue;
                initShowAmountValuesFromDB();
            }

            private void initShowAmountValuesFromDB()
            {
                const string  cachedShowAmountKey = "viewAmountComboBoxDefaultValues";
                List<int> valueList = CS.General_v3.Util.CachingUtil.GetItemFromCache<List<int>>(cachedShowAmountKey);
                //this.DefaultSelectedShowAmount = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Listing_ShowAmountComboValuesDefaultValue);
                if (valueList == null)
                {
                    valueList = new List<int>();
                    var setting = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSetting(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Listing_ShowAmountComboValues);
                    
                    string showAmountValues = setting.GetValue<string>();
                    string[] values = showAmountValues.Split(',');

                    foreach (var value in values)
                    {
                        string sValue = value.Trim();
                        int nValue = 0;
                        if (int.TryParse(sValue, out nValue))
                        {
                            valueList.Add(nValue);
                        }
                    }
                    CS.General_v3.Util.CachingUtil.AddItemToCache(cachedShowAmountKey, valueList, TimeSpan.FromDays(30), BusinessLogic_v3.Util.CachingUtil.GetOutputCacheDependencyForDBObjects(setting));
                }
                this.ShowAmountValues = valueList;
            }

        }
        public FUNCTIONALITY Functionality { get; private set; }
        public PagingBarCombos()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        private void initSortByValues(MyDropDownList cmb, Type enumType, Enum selectedValue, CS.General_v3.Enums.ENUM_SORT_BY sortByValues)
        {
            var values = CS.General_v3.Util.EnumUtils.GetListOfEnumValues(enumType, sortByValues);

            int nSelectedValue = (int)(object)selectedValue;

            for (int i = 0; i < values.Count; i++)
            {
                Enum eVal = values[i];

                int nVal = (int)(object)eVal;

                var labelContentText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextForEnumValue(enumType, eVal).ToFrontendBase();
                string value = null;
                
                value = this.Functionality.PagingInfo.GetDifferentSortByURL(eVal);

                cmb.Functionality.Items.AddOption(labelContentText, value, null, nSelectedValue == nVal);
            }

        }

        private void initShowAmtValues(MyDropDownList cmb, int selectedValue, CS.General_v3.Enums.ENUM_SORT_BY sortByValues)
        {
            int nSelectedValue = (int)(object)selectedValue;

            var cnt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Listing_ShowAmountText).ToFrontendBase();
            foreach(int value in this.Functionality.ShowAmountValues)
            {
                
                TokenReplacerNew tokenReplacer = new TokenReplacerNew(cnt.Data);
                tokenReplacer[BusinessLogic_v3.Constants.Tokens.TAG_AMOUNT] = value.ToString();

                string URL = this.Functionality.PagingInfo.GetDifferentShowAmtURL(value);

                cmb.Functionality.Items.AddOption(cnt, URL, tokenReplacer, selectedValue == value);
            }

        }

        private void initializeValues()
        {
       
            initShowAmtValues(cmbViewAmt, this.Functionality.PagingInfo.ShowAmount, this.Functionality.SortByEnumTypeValueSorting);
            initSortByValues(cmbSortBy, this.Functionality.PagingInfo.SortByEnumType, this.Functionality.PagingInfo.SortByValue, this.Functionality.SortByEnumTypeValueSorting);
        }
        private void checkRequirements()
        {
            if (this.Functionality.PagingInfo == null) throw new ArgumentNullException("PagingInfo cannot be null");

        }

        protected override void OnLoad(EventArgs e)
        {
            if (this.Visible)
            {
                updateFromContentText();
                checkRequirements();
                initializeValues();
            }
            base.OnLoad(e);
        }

        private void updateFromContentText()
        {
            MyContentText.AttachContentTextWithControl(lblSortBy, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Paging_SortByText).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(lblViewAmt, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Paging_ViewAmountText).ToFrontendBase());
        }
    }
}