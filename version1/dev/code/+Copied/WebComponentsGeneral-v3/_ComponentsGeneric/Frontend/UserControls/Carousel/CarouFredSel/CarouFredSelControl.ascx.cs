﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Classes.Javascript.UI.jQuery.Carousel.CarouFredSel.v1;
using System.Web.UI.HtmlControls;
using CS.WebComponentsGeneralV3.Code.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Extensions;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Carousel.CarouFredSel
{
    public partial class CarouFredSelControl : BaseUserControl
    {
        
        protected override void OnInit(EventArgs e)
        {
            CS.General_v3.Util.PageUtil.GetCurrentPage().Load += new EventHandler(CarouFredSelControl_Load);
            
            base.OnInit(e);
        }

        void CarouFredSelControl_Load(object sender, EventArgs e)
        {
            CarouFredSelV1.RegisterJS();
            
        }
        
			

        public enum CAROUFREDSEL_DIRECTION
        {
            Right,
            Left,
            Up,
            Down
        }
        public enum CAROUFREDSEL_ALIGN
        {
            Center,
            Left,
            Right,
            False
        }
        #region CarouFredSelControl Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            public CarouFredSelV1Parameters Parameters { get; set; }
            public List<Control> Controls { get; private set; }
            public bool ShowStartStopButton { get; set; }

            public int? ItemsVisible
            {
                get
                {
                    if (Parameters.items != null)
                    {
                        return (int?)Parameters.items.visible;
                    }
                    return null;

                }
                set
                {
                    if (value.HasValue && Parameters.items == null)
                    {
                        Parameters.items = new CarouFredSelV1ItemParameters();
                    }
                    if (Parameters.items != null)
                    {
                        Parameters.items.visible = value;
                    }
                }
            }
            public int? ItemsToScroll
            {
                get
                {
                    if (Parameters.scroll != null)
                    {
                        return (int?)Parameters.scroll.items;
                    }
                    return null;

                }
                set
                {
                    if (value.HasValue && Parameters.scroll == null)
                    {
                        Parameters.scroll = new CarouFredSelV1ScrollParameters();
                    }
                    if (Parameters.scroll != null)
                    {
                        Parameters.scroll.items = value;
                    }
                }
            }
            public int? ItemsScrollDurationMS
            {
                get
                {
                    if (Parameters.scroll != null)
                    {
                        return (int?)Parameters.scroll.duration;
                    }
                    return null;

                }
                set
                {
                    if (value.HasValue && Parameters.scroll == null)
                    {
                        Parameters.scroll = new CarouFredSelV1ScrollParameters();
                    }
                    if (Parameters.scroll != null)
                    {
                        Parameters.scroll.duration = value;
                    }
                }
            }
            public CAROUFREDSEL_DIRECTION? Direction
            {
                get
                {
                    if (!string.IsNullOrWhiteSpace(Parameters.direction))
                    {
                        return CS.General_v3.Util.EnumUtils.EnumValueNullableOf<CAROUFREDSEL_DIRECTION>(Parameters.direction);
                    }
                    else
                    {
                        return null;
                    }
                }
                set
                {
                    if (value.HasValue)
                    {
                        Parameters.direction = value.ToString().ToLower();
                    }
                    else
                    {
                        Parameters.direction = null;
                    }
                }
            }
            public CAROUFREDSEL_ALIGN? Align
            {
                get
                {
                    if (!string.IsNullOrWhiteSpace(Parameters.align))
                    {
                        return CS.General_v3.Util.EnumUtils.EnumValueNullableOf<CAROUFREDSEL_ALIGN>(Parameters.align);
                    }
                    else
                    {
                        return null;
                    }
                }
                set
                {
                    if (value.HasValue)
                    {
                        Parameters.align = value.ToString().ToLower();
                    }
                    else
                    {
                        Parameters.align = null;
                    }
                }
            }
            public bool PauseOnHover
            {
                get
                {
                    if (Parameters.scroll != null && Parameters.scroll.pauseOnHover is bool)
                    {
                        return (bool)Parameters.scroll.pauseOnHover;
                    }
                    return false;
                }
                set
                {
                    if (Parameters.scroll == null)
                    {
                        Parameters.scroll = new CarouFredSelV1ScrollParameters();
                    }
                    Parameters.scroll.pauseOnHover = value;
                }
            }
            public int? AutomaticPauseDurationMs
            {
                get
                {
                    if (Parameters.auto != null)
                    {
                        return (int?)Parameters.auto.pauseDuration;
                    }
                    return null;

                }
                set
                {
                    if (value.HasValue && Parameters.auto == null)
                    {
                        Parameters.auto = new CarouFredSelV1AutoParameters();
                    }
                    if (Parameters.auto != null)
                    {
                        Parameters.auto.pauseDuration = value;
                    }
                }
            }
            public int? AutomaticInitialExtraPauseDurationMs
            {
                get
                {
                    if (Parameters.auto != null)
                    {
                        return (int?)Parameters.auto.delay;
                    }
                    return null;

                }
                set
                {
                    if (value.HasValue && Parameters.auto == null)
                    {
                        Parameters.auto = new CarouFredSelV1AutoParameters();
                    }
                    if (Parameters.auto != null)
                    {
                        Parameters.auto.delay = value;
                    }
                }
            }
           
            protected CarouFredSelControl _item = null;
            internal FUNCTIONALITY(CarouFredSelControl item)
            {
                this._item = item;
                this.Controls = new List<Control>();
                this.ShowStartStopButton = true;
                this.Parameters = new CarouFredSelV1Parameters();
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        private void initControls()
        {
            
            foreach (var control in this.Functionality.Controls)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                li.Attributes["class"] = "carousel-fred-item";
                li.Controls.Add(control);
                ulCarouselFred.Controls.Add(li);
            }
        }
        private void initJS()
        {

            if (this.Functionality.Parameters.pagination != null)
            {
                this.containerPagination.Visible = true;
                this.Functionality.Parameters.pagination.container = "#" + this.containerPagination.ClientID;
                if (this.Functionality.Parameters.auto == null)
                {
                    this.Functionality.Parameters.auto = new CarouFredSelV1AutoParameters();
                }

                this.Functionality.Parameters.auto.pauseOnEvent = true;
                this.Functionality.Parameters.auto.button = "#" + this.divCarouselContainer.ClientID +" .carousel-fred-toggle-button";
                MyContentText.AttachContentTextWithControl(buttonStopText, BusinessLogic_v3.Enums.CONTENT_TEXT.AnythingSlider_StopText.GetContentTextFrontend());
                MyContentText.AttachContentTextWithControl(buttonStartText, BusinessLogic_v3.Enums.CONTENT_TEXT.AnythingSlider_StartText.GetContentTextFrontend());
                string jsUpdateToggleButtonFunction = @"function() { 
                                                                        jQuery('#" + ulCarouselFred.ClientID + @"').trigger('isPaused', function(isPaused) { 
                                                                            
                                                                            var jButtonStart = jQuery('#" + this.buttonStartText.ClientID + @"'); 
                                                                            var jButtonStop = jQuery('#" + this.buttonStopText.ClientID + @"'); 
                                                                            if (isPaused) {
                                                                                jButtonStart.show();
                                                                                jButtonStop.hide();
                                                                            }
                                                                            else {
                                                                                jButtonStart.hide();
                                                                                jButtonStop.show();
                                                                            }
                                                                        });
                                                                        
                                                                    }";
                this.Functionality.Parameters.auto.onPausePause = jsUpdateToggleButtonFunction;
                this.Functionality.Parameters.auto.onPauseStart = jsUpdateToggleButtonFunction;
                buttonStartText.Style.Add(HtmlTextWriterStyle.Display, "none");


            }
            if (!this.Functionality.ShowStartStopButton)
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divToggleButtonContainer);
            }
            string jsParams = this.Functionality.Parameters.GetJSON();
            string js = "jQuery('#"+ulCarouselFred.ClientID+"').carouFredSel("+jsParams+");";
            js = JSUtil.GetDeferredJSScriptThroughJQuery(js, true);

            this.Controls.Add(new Literal() { Text = js });
        }
        
        
        protected override void OnLoad(EventArgs e)
        {

            initControls();
            initJS();
            
            base.OnLoad(e);
        }

        public override string ClientID
        {
            get
            {
                return ulCarouselFred.ClientID;
            }
        }
		
    }
}