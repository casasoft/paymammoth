﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ArticleMediaItemModule;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Modules.ArticleMediaItemModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage
{
    public partial class ContentPageMediaItems : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private ContentPageMediaItems _control;
            public FUNCTIONALITY(ContentPageMediaItems control)
            {
                _control = control;
            }
            public IContentPageProperties Article { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ContentPageMediaItems()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadGallery();
        }

        private void loadGallery()
        {
            if(this.Functionality.Article != null)
            {
                IEnumerable<ArticleMediaItemBaseFrontend> mediaItems =
                    this.Functionality.Article.GetMediaItems(true).Cast<ArticleMediaItemBaseFrontend>();
                articleMediaItems.Functionality.Gallery = mediaItems;
                articleMediaItems.Functionality.IncludePrettyPhotoScripts = true;
            }
        }
    }
}