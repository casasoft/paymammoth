﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderVIPCode.ascx.cs"
 Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Discounts.HeaderVIPCode" %>
<div class="heading-vip-code-container clearfix">
    <CSControlsFormFields:FormFields runat="server" id="vipCodeForm"></CSControlsFormFields:FormFields> 
    <div class="heading-vip-code-button-container">
        <CSControls:MyButton runat="server" ID="btnApplyDiscount" CssClass="heading-vip-code-button"></CSControls:MyButton>
    </div>
</div>
