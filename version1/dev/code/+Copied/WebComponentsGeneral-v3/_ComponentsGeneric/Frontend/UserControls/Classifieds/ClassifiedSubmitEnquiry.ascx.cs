﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ClassifiedModule;
using BusinessLogic_v3.Frontend.MemberModule;
using BusinessLogic_v3.Modules;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Classifieds
{
    public partial class ClassifiedSubmitEnquiry : BaseUserControl
    {
        private static string SUBMIT_ENQUIRY_IDENTIFIER = "SUBMIT_ENQUIRY_IDENTIFIER";
        private MyTxtBoxStringMultiLine _txtComment;

        public class FUNCTIONALITY
        {
            private ClassifiedSubmitEnquiry _control;
            public FUNCTIONALITY(ClassifiedSubmitEnquiry control)
            {
                _control = control;
            }
            public MemberBaseFrontend Member { get; set; }
            public ClassifiedBaseFrontend Classified { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ClassifiedSubmitEnquiry()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
            initMember();
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(spanPostEnquiryTitle,
                                               BusinessLogic_v3.Enums.CONTENT_TEXT.Classifieds_Enquiry_Title.
                                                   GetContentTextFrontend());
        }

        private void initMember()
        {
            var member = this.Functionality.Member;
            var classified = this.Functionality.Classified;
            if(member != null && classified != null)
            {
                initLoggedInMember();
                initFields();
            }
            else
            {
                initNeedLogin();
            }
        }

        private void initLoggedInMember()
        {
            CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divNeedLogin);
            MyContentText.AttachContentTextWithControl(divEnquiryDescription,
                                                       BusinessLogic_v3.Enums.CONTENT_TEXT.
                                                           Classifieds_Enquiry_Description.GetContentTextFrontend());
        }

        private void initNeedLogin()
        {
            CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divFields);
            MyContentText.AttachContentTextWithControl(divNeedLoginText, BusinessLogic_v3.Enums.CONTENT_TEXT.Classified_Enquiry_NeedLoginToEnquire.GetContentTextFrontend());

            var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_LoginPage).ToFrontendBase();
            ltlFbLinkId.Text = aCommentsLoginWithFb.ClientID;
            ltlOverlayTitleText.Text = article.Data.PageTitle;

            MyContentText.AttachContentTextWithControl(liLogin, Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_LoginLink).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(liRegisterAccount, Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Article_Comment_RegisterLink).ToFrontendBase());
        }

        private void initFields()
        {
            var group = submitEnquiryFields.Functionality.AddGroup(new CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields.FormFieldsItemGroupData()
            {
                Identifier = SUBMIT_ENQUIRY_IDENTIFIER
            });

            _txtComment = (MyTxtBoxStringMultiLine)group.AddFormWebControl(new MyTxtBoxStringMultiLine()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ClassifiedEnquiryForm_txtEnquiry",
                    required = true,
                    titleContentText = BusinessLogic_v3.Enums.CONTENT_TEXT.Classifieds_Enquiry_EnquiryFieldTitle.GetContentTextFrontend()
                }
            });

            MyButton btnSubmit = submitEnquiryFields.Functionality.AddButton(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Button_Submit.GetContentTextFrontend(), SUBMIT_ENQUIRY_IDENTIFIER);
            btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }

        void btnSubmit_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(this.Functionality.Member.Data.Email))
            {
                this.Functionality.Classified.Data.SubmitEnquiryForm(_txtComment.GetFormValueAsString(),
                                                                     this.Functionality.Member.Data.FullName,
                                                                     this.Functionality.Member.Data.Email);
                
            }
            this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(
                BusinessLogic_v3.Enums.CONTENT_TEXT.Classified_Enquiry_SuccessMessage.GetContentTextFrontend(),
                CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL());
            
        }
    }
}