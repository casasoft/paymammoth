﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Testimonials {
    
    
    public partial class TestimonialsFullPage {
        
        /// <summary>
        /// contentPage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage.ContentPage contentPage;
        
        /// <summary>
        /// divNoResultsText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divNoResultsText;
        
        /// <summary>
        /// testimonials control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Testimonials.TestimonialsItems testimonials;
        
        /// <summary>
        /// divBottomSection control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divBottomSection;
        
        /// <summary>
        /// pagingBarBottom control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Listing.Paging.PagingBar pagingBarBottom;
    }
}
