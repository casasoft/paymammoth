﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.MemberModule;
using BusinessLogic_v3;
using BusinessLogic_v3.Frontend.ShoppingCartModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart
{
    public partial class ShoppingCartProceedCtrl : BaseUserControl
    {
        private MemberBaseFrontend _member;

        public class FUNCTIONALITY
        {
            private ShoppingCartProceedCtrl _control;
            public FUNCTIONALITY(ShoppingCartProceedCtrl control)
            {
                _control = control;
            }
            public string RegisterUrl { get; set; }
            public string DeliveryDetailsUrl { get; set; }
            public bool IsInCheckOut
            {
                get { return _control.loggedIn.Functionality.IsInCheckOut; }
                set { _control.loggedIn.Functionality.IsInCheckOut = value; }
            }

            public ShoppingCartBaseFrontend ShoppingCart { get; set; }

            public ShoppingCartProceedLoggedIn LoggedInProceedPayment
            {
                get { return _control.loggedIn; }
                set { _control.loggedIn = value; }
            }

        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ShoppingCartProceedCtrl()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            updateTexts();
            initMember();
            initControls();
        }

        private void updateTexts()
        {
            if(string.IsNullOrEmpty(this.Functionality.RegisterUrl))
            {
                this.Functionality.RegisterUrl = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_Register).GetUrl();
            }
            loggedOut.Functionality.RegisterUrl = this.Functionality.RegisterUrl;

            if (string.IsNullOrEmpty(this.Functionality.DeliveryDetailsUrl))
            {
                this.Functionality.DeliveryDetailsUrl = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.ShoppingCart_DeliveryDetails).GetUrl();
            }
            loggedIn.Functionality.DeliveryDetailsUrl = this.Functionality.DeliveryDetailsUrl;
        }

        private void initControls()
        {
            if(_member != null)
            {
                loggedOut.Visible = false;
                loggedIn.Functionality.ShoppingCart = this.Functionality.ShoppingCart;
            }
            else
            {
                loggedIn.Visible = false;
            }
        }

        private void initMember()
        {
            _member = BusinessLogic_v3.Modules.Factories.MemberFactory.GetLoggedInUserForCurrentSession().ToFrontendBase();
        }
    }
}