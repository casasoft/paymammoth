﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubmitComment.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Comments.SubmitComment" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/FacebookLoginButton.ascx"
    TagPrefix="SocialLoginButtons" TagName="Facebook" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/GoogleLoginButton.ascx"
    TagPrefix="SocialLoginButtons" TagName="Google" %>
<%@ Register Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Login/SocialButtons/TwitterLoginButton.ascx"
    TagPrefix="SocialLoginButtons" TagName="Twitter" %>
<div class="submit-comment-container">
    <div class="submit-comment-title-container">
        <h3 class="submit-comment-title" runat="server" id="spanPostCommentTitle">
            Post Comment</h3>
    </div>
    <div class="submit-comments-please-login" id="divNeedLogin" runat="server">
        <div class="submit-comments-please-login-text" id="divNeedLoginText" runat="server">
            <p>
                You must be logged in to your account in order to submit a comment. Choose one of
                the below options:</p>
        </div>
        <ul class="submit-comments-choose-options">
            <li class="submit-comments-login-all clearfix">
                <SocialLoginButtons:Facebook ID="ucFacebookLoginButton" runat="server" />
                <SocialLoginButtons:Google ID="ucGoogleLoginButton" runat="server" />
                <SocialLoginButtons:Twitter ID="ucTwitterLoginButton" runat="server" />
            </li>
            <li class="submit-comments-register" id="liRegisterAccount" runat="server"><a href="{Article:Register}">
                Register an account</a></li>
            <li class="submit-comments-login" id="liLogin" runat="server"><a>Login using your account</a></li>
        </ul>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#<asp:Literal ID="ltlLoginLinkId" runat="server"></asp:Literal>').click(function (e) {
                    e.preventDefault();
                    showLoginOverlay('<asp:Literal ID="ltlOverlayTitleText" runat="server"></asp:Literal>');
                });
            });
        </script>
    </div>
    <div class="submit-comments-fields-container" id="divFields" runat="server">
        <CSControlsFormFields:FormFields ID="submitCommentFields" runat="server">
        </CSControlsFormFields:FormFields>
    </div>
</div>
