﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Sliders.NivoSlider.Classes
{
    public interface INivoSliderImage
    {
        string Title { get; set; }
        string URL { get; set; }
        string AlternateText { get; set; }
        string Href { get; set; }
    }
}
