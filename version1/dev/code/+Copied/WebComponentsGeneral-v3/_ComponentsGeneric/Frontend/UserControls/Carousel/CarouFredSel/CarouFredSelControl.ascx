﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CarouFredSelControl.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Carousel.CarouFredSel.CarouFredSelControl" %>
<div class="carousel-fred-core-container clearfix" id="divCarouselContainer" runat="server">
    <ul class='carousel-fred-container' id="ulCarouselFred" runat="server">
    </ul>
    <div class="carousel-fred-bottom-container clearfix">
        <div class="carousel-fred-pagination-container" id="containerPagination" runat="server"
            visible="false">
        </div>
        <div class="carousel-fred-toggle-container" id="divToggleButtonContainer" runat="server">
            <div class="carousel-fred-toggle-button" id="divToggleButton" runat="server">
                <span class='carousel-fred-toggle-button-stop' id="buttonStopText" runat="server">stop</span>
                <span class='carousel-fred-toggle-button-start' id="buttonStartText" runat="server">
                    start</span>
            </div>
        </div>
    </div>
</div>
