﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.MemberModule;
using BusinessLogic_v3.Modules.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Modules.MemberModule.SessionManager;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Logout
{
    public partial class LogoutControl : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private LogoutControl _control;
            public FUNCTIONALITY(LogoutControl control)
            {
                _control = control;
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public LogoutControl()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        private void logoutUser()
        {

            MemberBaseFrontend loggedUser =
                MemberSessionLoginManager.Instance.GetLoggedInUserAlsoFromRememberMe().ToFrontendBase();


            if (loggedUser != null)
            {
                MemberSessionLoginManager.Instance.Logout(); //just in case
                CS.General_v3.Util.PageUtil.RedirectPage(CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: true));
            }
            else
            {
                var cpLogout =
                    BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                        BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_Logout).ToFrontendBase();

                contentPage.Functionality.ContentPage = cpLogout;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            logoutUser();
            base.OnLoad(e);
        }
    }
}