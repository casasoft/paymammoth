﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgotPasswordFields.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Password.ForgotPasswordFields" %>
<div class="forgot-password-fields-container">
    <CSControlsFormFields:FormFields ID="formForgotPassword" runat="server"></CSControlsFormFields:FormFields>
</div>