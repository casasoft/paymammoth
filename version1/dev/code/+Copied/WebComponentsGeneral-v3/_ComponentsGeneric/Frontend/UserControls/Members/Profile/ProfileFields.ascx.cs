﻿using System.Collections;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Modules.ContentTextModule;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Util;
using System.Linq;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Members.Profile
{
    using System;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using BusinessLogic_v3.Classes.Interfaces;
    using CS.General_v3.Classes.Login;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.Validators;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using BusinessLogic_v3.Modules.MemberModule;
    using BusinessLogic_v3.Frontend.MemberModule;
    using System.Collections.Generic;
    using BusinessLogic_v3.Frontend.ArticleModule;
    using BusinessLogic_v3.Modules.MemberModule.SessionManager;
    using BusinessLogic_v3.Classes.Routing;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;
    using BusinessLogic_v3.Util;
    using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;
    using BusinessLogic_v3.Classes.Text;
    using CS.WebComponentsGeneralV3.Code.Util;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields.JSControllers;

    public partial class ProfileFields : BaseUserControl
    {
        private static string PERSONAL_INFORMATION_IDENTIFIER = "personalInfo";
        private static string ACCOUNT_INFORMATION_IDENTIFIER = "accountInfo";
        private static string DISCOUNT_INFORMATION_IDENTIFIER = "discountCodeInfo";

        private const string ROW_CSS_COMPANY = "row-company";
        private const string ROW_CSS_VAT = "row-vat-number";
        private const string ROW_CSS_ID = "row-id-number";

        private ContentTextBaseFrontend _textDiscountInformation;

        public enum PROFILE_FIELDS_DISPLAY_TYPE
        {
            Register,
            UpdateProfile
        }

        public delegate void ProfileFieldsPreSubmitHandler(ProfileFields sender, ProfileFieldsData data);
        public delegate void ProfileFieldsSubmitHandler(ProfileFields sender, ProfileFieldsData data);

        public delegate void ProfileFieldsSpecificRegistrationSuccessHandler(ProfileFields sender, ProfileFieldsData data);
        public delegate void ProfileFieldsSpecificRegistrationFailureHandler(ProfileFields sender, ProfileFieldsData data);
        public delegate void ProfileFieldsSpecificProfileUpdateFailureHandler(ProfileFields sender, ProfileFieldsData data);
        public delegate void ProfileFieldsSpecificProfileUpdateSuccessHandler(ProfileFields sender, ProfileFieldsData data);

        public class MinimumAgeRequirementData
        {
            public List<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166> Countries { get; set; }
            public int MinimumAgeYears { get; set; }
        }
        public class ProfileFieldItemData
        {
            public Enums.PROFILE_FIELDS Field { get; set; }
            public bool Required { get; set; }
        }

        public class ProfileFieldsData
        {
            public PROFILE_FIELDS_DISPLAY_TYPE DisplayType { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public string IdCard { get; set; }
            public string VatNumber { get; set; }
            public CS.General_v3.Enums.PERSONAL_ACCOUNT_TYPE AccountType { get; set; }
            public TimeZoneInfo TimeZone { get; set; }
            public string Street { get; set; }
            public string Street2 { get; set; }
            public string City { get; set; }
            public string PostcodeOrZip { get; set; }
           
            public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? CountryCode { get; set; }
            public CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? LanguageCode { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public string Email { get; set; }
            public string Mobile { get; set; }
            public string Tel { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public CS.General_v3.Enums.GENDER? Gender { get; set; }
            public bool? ChkConfirm { get; set; }
            public string Company { get; set; }
            public bool AllowPromotions { get; set; }

            public bool FillMember(MemberBaseFrontend member)
            {
                if (!String.IsNullOrEmpty(Name))
                {
                    ((IMemberAddressDetails)member.Data).FirstName = Name;
                }
                if (Gender.HasValue)
                {
                    member.Data.Gender = Gender.Value;
                }

                if (!String.IsNullOrEmpty(IdCard))
                {
                    member.Data.IDCard = IdCard;
                }

                if(!String.IsNullOrEmpty(VatNumber))
                {
                    member.Data.VAT = VatNumber;
                }
                
                if (!String.IsNullOrEmpty(Surname))
                {
                    ((IMemberAddressDetails)member.Data).LastName = Surname;
                }
                if (TimeZone != null)
                {
                    member.Data.PreferredTimezoneID = TimeZone.Id;
                }

                if (LanguageCode.HasValue)
                {
                    var language =
                        BusinessLogic_v3.Modules.Factories.CultureDetailsFactory.GetCultureByCode(LanguageCode.Value);
                    if (language != null)
                    {
                        member.Data.PreferredCultureInfo = language;
                    }
                }

                if (!string.IsNullOrWhiteSpace(Company))
                {
                    member.Data.CompanyName = Company;
                }

                if (!String.IsNullOrEmpty(Street))
                {
                    member.Data.Address1 = Street;
                }

                if (!String.IsNullOrEmpty(Street2))
                {
                    member.Data.Address2 = Street2;
                }

                if (!String.IsNullOrEmpty(City))
                {
                    member.Data.Locality = City;
                }

                if (!String.IsNullOrEmpty(PostcodeOrZip))
                {
                    member.Data.PostCode = PostcodeOrZip;
                }

                member.Data.PersonalAccountType = AccountType;

                if (CountryCode.HasValue)
                {
                    member.Data.Country = CountryCode;
                }

                if (DateOfBirth.HasValue)
                {
                    member.Data.DateOfBirth = DateOfBirth;
                }

                if (!String.IsNullOrEmpty(Mobile))
                {
                    member.Data.Mobile = Mobile;
                }

                if (!String.IsNullOrEmpty(Tel))
                {
                    member.Data.Telephone = Tel;
                }

                if (!String.IsNullOrEmpty(Password))
                {
                    ((IUser)member.Data).SetPassword(Password);
                }
                if (!String.IsNullOrEmpty(Username))
                {
                    ((IUser)member.Data).SetUsername(Username);
                }
                if (!String.IsNullOrEmpty(Email))
                {
                    member.Data.Email = Email;
                }

                member.Data.AllowPromotions = AllowPromotions;

                if (this.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.Register)
                {
                    Enums.USER_REGISTRATION_REQUIRED_KEY requiredType = Factories.SettingFactory.GetSettingValue<Enums.USER_REGISTRATION_REQUIRED_KEY>(Enums.BusinessLogicSettingsEnum.UserRegistrationRequiredKey);

                    switch (requiredType)
                    {
                        case Enums.USER_REGISTRATION_REQUIRED_KEY.UsernameAndEmail:
                            if (String.IsNullOrEmpty(Username) || String.IsNullOrEmpty(Email))
                            {
                                return false;
                            }
                            break;
                        case Enums.USER_REGISTRATION_REQUIRED_KEY.Username:
                            if (String.IsNullOrEmpty(Username))
                            {
                                return false;
                            }
                            break;
                        case Enums.USER_REGISTRATION_REQUIRED_KEY.Email:
                            if (String.IsNullOrEmpty(Email))
                            {
                                return false;
                            }
                            break;
                        default:
                            throw new InvalidOperationException("Required Type <" + requiredType +"> not yet implemented!");
                    }
                }

                return true;
            }
        }

        private MyTxtBoxString _txtName;
        private MyTxtBoxString _txtSurname;
        private MyTxtBoxString _txtIdCard;
        private MyTxtBoxString _txtVatNumber;
        private MyRadioButtonList _rdListGender;
        private MyTxtBoxString _txtStreet;
        private MyTxtBoxString _txtStreet2;
        private MyTxtBoxString _txtCity;
        private MyTxtBoxString _txtZIP;
        private MyDropDownList _cmbCountry;
        private MyDropDownList _cmbPersonalAccountType;
        private MyDropDownList _cmbLanguages;
        private MyDropDownList _cmbTimeZone;
        private MyTxtBoxDate _txtDateOfBirth;
        private MyTxtBoxEmail _txtEmail;
        private MyTxtBoxEmail _txtConfEmail;
        private MyTxtBoxString _txtMobile;
        private MyTxtBoxString _txtTelephone;
        private MyTxtBoxString _txtUsername;
        private MyTxtBoxString _txtCompany;
        private MyTxtBoxStringPassword _txtPass;
        private MyTxtBoxStringPassword _txtConfPass;
        private MyTxtBoxStringPassword _txtOldPass;
        private MyTxtBoxString _txtDiscountCode;
        private MyButton _btnDiscountApply;

        private MyCheckBoxWithLabel _chkTerms;
        private MyCheckBoxWithLabel _chkAllowPromotions;

        private string _redirectToURL;

        public class FUNCTIONALITY
        {
            private ProfileFields _control;
            public string ValidationGroupPersonalInformation { get; set; }
            public string ValidationGroupAccountInformation { get; set; }
            public string ValidationGroupDiscountInformation { get; set; }
            public PROFILE_FIELDS_DISPLAY_TYPE DisplayType { get; set; }

            public event ProfileFieldsPreSubmitHandler OnPreSubmit;
            public event ProfileFieldsSubmitHandler OnSubmitSuccess;

            public event ProfileFieldsSpecificRegistrationSuccessHandler OnSpecificRegistrationSuccess;
            public event ProfileFieldsSpecificRegistrationFailureHandler OnSpecificRegistrationFailure;
            public event ProfileFieldsSpecificProfileUpdateFailureHandler OnSpecificProfileUpdateFailure;
            public event ProfileFieldsSpecificProfileUpdateSuccessHandler OnSpecificProfileUpdateSuccess;
            public bool DoNotRedirectOnSubmitSuccess { get; set; }

            public MemberBaseFrontend Member { get; set; }

            public FUNCTIONALITY(ProfileFields control)
            {
                this.ValidationGroupAccountInformation = ACCOUNT_INFORMATION_IDENTIFIER;
                this.ValidationGroupPersonalInformation = PERSONAL_INFORMATION_IDENTIFIER;
                this.ValidationGroupDiscountInformation = DISCOUNT_INFORMATION_IDENTIFIER;

                _control = control;
                MinimumAgeRequiredData = new List<MinimumAgeRequirementData>();
                PersonalInfoFieldsToInclude = new List<ProfileFieldItemData>();
                AccountInfoFieldsToInclude = new List<ProfileFieldItemData>();
                this.ExcludedCountries = new List<General_v3.Enums.ISO_ENUMS.Country_ISO3166>();

                initDefaultPersonalInfoFieldColumnsFromSettings();
                initDefaultAccountInfoFieldColumnsFromSettings();
                parseInitialMinimumAgeRequirements();
                parseExcludedCountriesFromSettings();
            }
            private void parseExcludedCountriesFromSettings()
            {
                string sExcludedCountries = Factories.SettingFactory.GetSettingValue<string>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_ExcludedCountries);
                if (!string.IsNullOrWhiteSpace(sExcludedCountries))
                {
                    string[] sExcludedCountriesList = sExcludedCountries.Split(',');
                    foreach (string sExcludedCountry in sExcludedCountriesList)
                    {
                        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? excludedCountry = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(sExcludedCountry.Trim());
                        if (excludedCountry.HasValue)
                        {
                            this.ExcludedCountries.Add(excludedCountry.Value);
                        }
                    }
                }

            }

            private void parseInitialMinimumAgeRequirements()
            {

                string sAgeRequirements = Factories.SettingFactory.GetSettingValue<string>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_ProfileFieldsMinimumMemberAgeRequired);
                string[] ageRequirements = sAgeRequirements.Split(',');
                foreach (var sAgeRequirement in ageRequirements)
                {
                    string[] ageParts = sAgeRequirement.Split(':');
                    int? minAge = null;
                    List<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166> countries = new List<General_v3.Enums.ISO_ENUMS.Country_ISO3166>();
                    if (ageParts.Length >= 1)
                    {
                        //Age only
                        minAge = CS.General_v3.Util.Other.ConvertStringToBasicDataType<int?>(ageParts[0]);
                    }
                    if (ageParts.Length >= 2)
                    {
                        //country
                        string[] countryCodes = ageParts[1].Split('|');
                        foreach (var countryCode in countryCodes)
                        {
                            CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? countryCodeEnum = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(countryCode);
                            if (countryCodeEnum.HasValue)
                            {
                                countries.Add(countryCodeEnum.Value);
                            }
                        }
                    }
                    if (minAge.HasValue && minAge > 0)
                    {
                        this.MinimumAgeRequiredData.Add(new MinimumAgeRequirementData() { Countries = countries, MinimumAgeYears = minAge.Value });
                    }
                }
            }

            private List<ProfileFieldItemData> parseFieldsFromString(string strFields)
            {
                List<ProfileFieldItemData> fieldsList = new List<ProfileFieldItemData>();
                string[] fields = strFields.Split(',');
                foreach (string strField in fields)
                {
                    bool required = false;
                    string sField = strField.Trim();
                    if (sField.EndsWith("*"))
                    {
                        //Required
                        required = true;
                        sField = sField.Remove(sField.Length - 1, 1);
                        sField = sField.Trim();
                    }

                    var fieldEnum = CS.General_v3.Util.EnumUtils.GetEnumByStringValueNullable<Enums.PROFILE_FIELDS>(sField);
                    if (fieldEnum.HasValue)
                    {
                        fieldsList.Add(new ProfileFieldItemData()
                        {
                            Field = fieldEnum.Value,
                            Required = required
                        });
                    }
                }
                return fieldsList;
            }

            private void initDefaultPersonalInfoFieldColumnsFromSettings()
            {
                var strFields = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_ProfileFieldsPersonalInfoFields);
                this.PersonalInfoFieldsToInclude.AddRange(parseFieldsFromString(strFields));
               
            }
            private void initDefaultAccountInfoFieldColumnsFromSettings()
            {
                var strFields = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_ProfileFieldsAccountInfoFields);
                this.AccountInfoFieldsToInclude.AddRange(parseFieldsFromString(strFields));
            }
            internal void triggerOnSubmitSuccess(ProfileFieldsData data)
            {
                if (this.OnSubmitSuccess != null)
                {
                    this.OnSubmitSuccess(_control, data);
                }
            }

            internal void triggerOnPreSubmitSuccess(ProfileFieldsData data)
            {
                if (this.OnPreSubmit != null)
                {
                    this.OnPreSubmit(_control, data);
                }
            }

            internal bool triggerOnSpecificRegistrationSuccess(ProfileFieldsData data)
            {
                if (this.OnSpecificRegistrationSuccess != null)
                {
                    this.OnSpecificRegistrationSuccess(_control, data);
                    return true;
                }
                return false;
            }

            internal bool triggerOnSpecificRegistrationFailure(ProfileFieldsData data)
            {
                if (this.OnSpecificRegistrationFailure != null)
                {
                    this.OnSpecificRegistrationFailure(_control, data);
                    return true;
                }
                return false;
            }

            internal bool triggerOnSpecificProfileUpdateFailure(ProfileFieldsData data)
            {
                if (this.OnSpecificProfileUpdateFailure != null)
                {
                    this.OnSpecificProfileUpdateFailure(_control, data);
                    return true;
                }
                return false;
            }

            internal bool triggerOnSpecificProfileUpdateSuccess(ProfileFieldsData data)
            {
                if (this.OnSpecificProfileUpdateSuccess != null)
                {
                    this.OnSpecificProfileUpdateSuccess(_control, data);
                    return true;
                }
                return false;
            }

            public ContentTextBaseFrontend LblName { get; set; }
            public ContentTextBaseFrontend LblSurname { get; set; }
            public ContentTextBaseFrontend LblIdCard { get; set; }
            public ContentTextBaseFrontend LblVatNumber { get; set; }
            public ContentTextBaseFrontend LblGender { get; set; }
            public ContentTextBaseFrontend LblStreet { get; set; }
            public ContentTextBaseFrontend LblCity { get; set; }
            public ContentTextBaseFrontend LblPostCode { get; set; }
            public ContentTextBaseFrontend LblCountry { get; set; }
            public ContentTextBaseFrontend LblTimezone { get; set; }
            public ContentTextBaseFrontend LblBirthDate { get; set; }
            public ContentTextBaseFrontend LblPersonalAccountType { get; set; }
            public ContentTextBaseFrontend LblEmail { get; set; }
            public ContentTextBaseFrontend LblConfirmEmail { get; set; }
            public ContentTextBaseFrontend LblMobilePhone { get; set; }
            public ContentTextBaseFrontend LblUsername { get; set; }
            public ContentTextBaseFrontend LblPassword { get; set; }
            public ContentTextBaseFrontend LblOldPassword { get; set; }
            public ContentTextBaseFrontend LblConfirmPassword { get; set; }
            public ContentTextBaseFrontend LblAllowPromotions { get; set; }
            public ContentTextBaseFrontend LblTermsConfirmation { get; set; }
            public ContentTextBaseFrontend LblLanguage { get; set; }
            public ContentTextBaseFrontend LblCompany { get; set; }
            public ContentTextBaseFrontend LblTelephone { get; set; }
            public ContentTextBaseFrontend LblStreet2 { get; set; }
            public ContentTextBaseFrontend LblDiscountCode { get; set; }

            public ContentTextBaseFrontend TextAccountInformation { get; set; }
            public ContentTextBaseFrontend TextPersonalInformation { get; set; }

            public ContentTextBaseFrontend UsernameInformationMessage { get; set; }
            public ContentTextBaseFrontend EmailInformationMessage { get; set; }

            public bool IncludeTermsConditions { get; set; }

            public string TermsConditionsURL { get; set; }
            public ContentTextBaseFrontend TermsConditionsConfirmationText { get; set; }
            public ContentTextBaseFrontend PromotionsConfirmationText { get; set; }
            public ContentTextBaseFrontend IncorrectOldPasswordMessage { get; set; }
            public ContentTextBaseFrontend IncorrectConfirmPasswordMessage { get; set; }
            public ContentTextBaseFrontend BtnSubmitText { get; set; }

            public List<MinimumAgeRequirementData> MinimumAgeRequiredData { get; private set; }
            public List<ProfileFieldItemData> PersonalInfoFieldsToInclude { get; private set; }
            public List<ProfileFieldItemData> AccountInfoFieldsToInclude { get; private set; }
            public List<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166> ExcludedCountries { get; private set; }
            public void SetFieldsToShow(params ProfileFieldItemData[] fields)
            {
                PersonalInfoFieldsToInclude.Clear();
                foreach (var field in fields)
                {
                    PersonalInfoFieldsToInclude.Add(field);
                }
            }


        }

        public FUNCTIONALITY Functionality { get; private set; }
        public ProfileFields()
        {
            this.Functionality = createFunctionality();
            
            this.Functionality.IncludeTermsConditions = Factories.SettingFactory.GetSettingValue<bool>(CS.General_v3.Enums.SETTINGS_ENUM.MembersArea_Register_IncludeTermsAndConditions);
 
            // Information messages
            //this.Functionality.EmailInformationMessage = "You cannot update your email address.  Please contact us if you need to update your email address";
           // this.Functionality.UsernameInformationMessage = "You cannot update your username.  Please contact us if you need to update your email address";
            //this.Functionality.TermsConditionsConfirmationText = "";
            this.Functionality.IncorrectConfirmPasswordMessage = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_IncorrectConfirmPassword).ToFrontendBase();
            this.Functionality.IncorrectOldPasswordMessage = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_IncorrectOldPassword).ToFrontendBase();
        }
        

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private void showFirstOrFullNameField(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            _txtName = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ProfileFields_txtName",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblName
                },
                InitialValue = (this.Functionality.Member != null ? ((IMemberAddressDetails)this.Functionality.Member.Data).FirstName : null)
            });
        }

        private void showLastNameField(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //Surname
            _txtSurname = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "nProfileFields_txtSurname",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblSurname
                },
                InitialValue = (this.Functionality.Member != null ? ((IMemberAddressDetails)this.Functionality.Member.Data).LastName : null)
            });
        }

        private void showIdCardField(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //ID Card
            _txtIdCard = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "nProfileFields_txtIDCard",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblIdCard
                },
                InitialValue = (this.Functionality.Member != null ? this.Functionality.Member.Data.IDCard : null)
            }, rowCssClass: ROW_CSS_ID);
        }

        private void showVatNumberField(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //VAT Number
            _txtVatNumber = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "nProfileFields_txtVatNumber",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblVatNumber
                },
                InitialValue = (this.Functionality.Member != null ? this.Functionality.Member.Data.VAT : null)
            }, rowCssClass: ROW_CSS_VAT);
        }

        private void showCompanyField(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //Company
            _txtCompany = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ProfileFields_txtCompany",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblCompany
                },
                InitialValue = (this.Functionality.Member != null ? this.Functionality.Member.Data.CompanyName : null)
            }, rowCssClass:ROW_CSS_COMPANY);
        }

        private void showStreetField(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //Street
            _txtStreet = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ProfileFields_txtStreet",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblStreet
                },
                InitialValue = (this.Functionality.Member != null ? ((IMemberAddressDetails)this.Functionality.Member.Data).AddressLine1 : null)
            });
        }

        private void showStreet2Field(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //Street 2
            _txtStreet2 = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ProfileFields_txtStreet2",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblStreet2
                },
                InitialValue = (this.Functionality.Member != null ? ((IMemberAddressDetails)this.Functionality.Member.Data).AddressLine2 : null)
            });
        }

        private void showCityField(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //City
            _txtCity = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ProfileFields_txtCity",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblCity
                },
                InitialValue = (this.Functionality.Member != null ? this.Functionality.Member.Data.Locality : null)
            });
        }

        private void showZIPPostCodeField(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //ZIP 
            _txtZIP = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ProfileFields_txtZIP",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblPostCode
                },
                InitialValue = (this.Functionality.Member != null ? this.Functionality.Member.Data.PostCode : null)
            });
        }

        private void showTimeZone(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //Country 
            string initialTimeZoneID = null;
            var timeZones = TimeZoneInfo.GetSystemTimeZones();

            if (this.Functionality.Member != null && !string.IsNullOrEmpty(this.Functionality.Member.Data.PreferredTimezoneID))
            {
                initialTimeZoneID = this.Functionality.Member.Data.PreferredTimezoneID;
            }
            else if (this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.Register)
            {
                var country = CS.General_v3.Util.GeoIpUtil.GetIPLocationCountryCode();
                if (country.HasValue)
                {
                    var tz = CS.General_v3.Util.Data.GetTimeZoneFromCountry(country.Value);
                    initialTimeZoneID = tz.Id;
                }
            }
          
            _cmbTimeZone = new MyDropDownList()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ProfileFields_cmbTimezone",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblTimezone
                },
                InitialValue = initialTimeZoneID
            };
           _cmbTimeZone.Functionality.Items.AddRange(CS.General_v3.Util.Data.GetTimeZonesListAsListItemCollection());
            group.AddFormWebControl(_cmbTimeZone);
        }

        private void showPersonalAccountType(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            var initialValue = CS.General_v3.Enums.PERSONAL_ACCOUNT_TYPE.PersonalAccount;
            
            if (this.Functionality.Member.Data.PersonalAccountType != null)
            {
                initialValue = this.Functionality.Member.Data.PersonalAccountType;
            }            
            //Personal Account Type
            _cmbPersonalAccountType = new MyDropDownList()
                                          {
                                              FieldParameters = new FieldSingleControlParameters()
                                                                    {
                                                                        id = "ProfileFields_cmbPersonalAccountType",
                                                                        required = fieldData.Required,
                                                                        titleContentText = Functionality.LblPersonalAccountType
                                                                         
                                                                    },
                                              InitialValue = initialValue
                                          };

            _cmbPersonalAccountType.FieldParameters.rowsVisiblityParameters = new FieldRowsVisibilityControllerParameters();
            _cmbPersonalAccountType.FieldParameters.rowsVisiblityParameters.values.Add(new FieldRowVisibilityValueParameters()
            {
                value = ((int)CS.General_v3.Enums.PERSONAL_ACCOUNT_TYPE.PersonalAccount).ToString(),
                selectorHideItems = "." + ROW_CSS_VAT +", ." + ROW_CSS_COMPANY,
                selectorShowItems = "." + ROW_CSS_ID
            });
            _cmbPersonalAccountType.FieldParameters.rowsVisiblityParameters.values.Add(new FieldRowVisibilityValueParameters()
            {
                value = ((int)CS.General_v3.Enums.PERSONAL_ACCOUNT_TYPE.BusinessEntity).ToString(),
                selectorShowItems = "." + ROW_CSS_VAT + ", ." + ROW_CSS_COMPANY,
                selectorHideItems = "." + ROW_CSS_ID
            });
            var accountTypeList = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextListItemCollectionByEnumType<CS.General_v3.Enums.PERSONAL_ACCOUNT_TYPE>();
            _cmbPersonalAccountType.Functionality.AddItemsFromListItemCollection(accountTypeList);
            group.AddFormWebControl(_cmbPersonalAccountType);
        }

        private void showCountryField(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //Country 
            string initialCountryCode = null;

            if (this.Functionality.Member != null && this.Functionality.Member.Data.Country.HasValue)
            {
                initialCountryCode =
                    CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To3LetterCode(
                        this.Functionality.Member.Data.Country.Value);
            }
            if (string.IsNullOrEmpty(initialCountryCode))
            {
                var countryGeoIPCode = CS.General_v3.Util.GeoIpUtil.GetIPLocationCountryCode();
                if (countryGeoIPCode.HasValue)
                {
                    initialCountryCode =
                        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To3LetterCode(countryGeoIPCode.Value);
                }
            }
            _cmbCountry = new MyDropDownList()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ProfileFields_cmbCountry",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblCountry
                },
                InitialValue = initialCountryCode
            };
            var countriesList = CS.General_v3.Util.Data.GetCountriesAsListWith3LetterCodes(this.Functionality.ExcludedCountries);
            _cmbCountry.Functionality.AddItemsFromListItemCollection(countriesList);
            group.AddFormWebControl(_cmbCountry);
        }
        private void showGender(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            string initialValue = null;
            if (this.Functionality.Member != null && CS.General_v3.Util.EnumUtils.IsValidEnum(typeof(CS.General_v3.Enums.GENDER), this.Functionality.Member.Data.Gender))
            {
                initialValue = ((long)this.Functionality.Member.Data.Gender).ToString();
            }
            else
            {
                //initialValue = ((long)CS.General_v3.Enums.GENDER.Male).ToString();

            }

            _rdListGender = new MyRadioButtonList(new FieldRadioButtonOrCheckboxListParameters()
            {
                titleContentText = this.Functionality.LblGender,
                required = fieldData.Required,
                id = "ProfileFields_rdGender"
            });
            _rdListGender.ReadOnly = this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile;
            _rdListGender.InitialValue = initialValue;

            var items = BusinessLogic_v3.Util.EnumUtils.GetListItemCollectionFromEnumAndAddContentTexts<CS.General_v3.Enums.GENDER>(sortBy: General_v3.Enums.ENUM_SORT_BY.PriorityAttributeValue);
            _rdListGender.Functionality.AddItemsFromListItemCollection(items);

            group.AddFormWebControl(_rdListGender);


        }

        private void showLanguageField(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //Language
            IEnumerable<ICultureDetailsBase> languages = Factories.CultureDetailsFactory.GetAvailableLanguages();
            if (languages != null && languages.Count() > 0)
            {
                string initialLanguageValue = null;
                if (this.Functionality.Member != null && this.Functionality.Member.Data.PreferredCultureInfo != null)
                {
                    initialLanguageValue =
                        this.Functionality.Member.Data.PreferredCultureInfo.GetLanguage2LetterCode();
                }
                if (string.IsNullOrEmpty(initialLanguageValue))
                {
                    initialLanguageValue =
                        Factories.CultureDetailsFactory.GetCurrentCultureInSession().GetLanguage2LetterCode();
                }

                _cmbLanguages = new MyDropDownList()
                {
                    FieldParameters = new FieldSingleControlParameters()
                    {
                        id = "ProfileFields_cmbLanguages",
                        required = fieldData.Required,
                        titleContentText = Functionality.LblLanguage
                    },
                    InitialValue = initialLanguageValue
                };

                foreach (var language in languages)
                {
                    _cmbLanguages.Functionality.AddItemFromListItem(new System.Web.UI.WebControls.ListItem()
                    {
                        Text =
                            language.GetLanguageISOEnumValue().
                            ToString(),
                        Value =
                            language.GetLanguage2LetterCode()
                    });
                }
                group.AddFormWebControl(_cmbLanguages);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dob"></param>
        /// <param name="country"></param>
        /// <returns>The invalid requirement date</returns>
        private MinimumAgeRequirementData isDOBValid(DateTime dob, CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? country)
        {
            MinimumAgeRequirementData applicableRequirement = null;
            MinimumAgeRequirementData invalidAgeRequirement = null;
            if (this.Functionality.MinimumAgeRequiredData != null)
            {
                bool found = false;
                foreach (var minAge in this.Functionality.MinimumAgeRequiredData)
                {
                    if (minAge.Countries != null && minAge.Countries.Count > 0)
                    {
                        if (country.HasValue)
                        {
                            foreach (var ageCountry in minAge.Countries)
                            {
                                if (country.Value == ageCountry)
                                {
                                    applicableRequirement = minAge;
                                    found = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (applicableRequirement == null)
                        {
                            //set this as default
                            applicableRequirement = minAge;
                        }
                    }
                    if (found) break;
                }
            }
            if (applicableRequirement != null)
            {
                var maxDOB = getMaxDOBToRegisterForYearLimit(applicableRequirement.MinimumAgeYears);
                if (dob > maxDOB)
                {
                    //Invalid DOB
                    invalidAgeRequirement = applicableRequirement;
                }
            }
            return invalidAgeRequirement;
        }

        private DateTime getMaxDOBToRegisterForYearLimit(int yearLimit)
        {
            return Date.Now.AddYears(-yearLimit).GetStartOfDay();
        }
        private DateTime? getMaxDOBToRegister()
        {
            var minReq = getMinAgeToRegister();
            if (minReq != null)
            {
                return getMaxDOBToRegisterForYearLimit(minReq.MinimumAgeYears);
            }
            else
            {
                return null;
            }
        }

        private MinimumAgeRequirementData getMinAgeToRegister() 
        {
            MinimumAgeRequirementData minReq = null;
            if (Functionality.MinimumAgeRequiredData.Count > 0)
            {
                int? minAgeYrs = null;
                foreach (var minAge in Functionality.MinimumAgeRequiredData)
                {
                    if (minReq == null || minReq.MinimumAgeYears > minAge.MinimumAgeYears)
                    {
                        minReq = minAge;
                    }
                }
                
            }
            return minReq;

        }

        private void showBirthDateField(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //DOB 
            string dateFormat = "dd/MM/yyyy";
            string initialDOB = null;
            if (this.Functionality.Member != null && this.Functionality.Member.Data.DateOfBirth.HasValue)
            {
                initialDOB = this.Functionality.Member.Data.DateOfBirth.Value.ToString(dateFormat);
            }
            _txtDateOfBirth = (MyTxtBoxDate)group.AddFormWebControl(new MyTxtBoxDate()
            {
                FieldParameters =
                    new FieldSingleControlDateParameters()
                    {
                        id = "ProfileFields_txtDOB",
                        required = fieldData.Required,
                        titleContentText =
                            Functionality.LblBirthDate
                    },
                InitialValueAsString = initialDOB
            }, subTitle: "(" + dateFormat.ToLower() + ")");
            _txtDateOfBirth.FieldParameters.jQueryDatePickerOptions.changeMonth = true;
            _txtDateOfBirth.FieldParameters.jQueryDatePickerOptions.changeYear = true;

            _txtDateOfBirth.FieldParameters.jQueryDatePickerOptions.yearRange = "1900:" +
                                                                                DateTime.Now.Year.ToString();
            

            ValidatorDateParameters dateParams = new ValidatorDateParameters();
            dateParams.dateFormat = dateFormat;

            dateParams.dateTo = getMaxDOBToRegister();
            _txtDateOfBirth.FieldParameters.validators.Add(new ValidatorDate() { Parameters = dateParams });
            if (this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile)
            {
                _txtDateOfBirth.ReadOnly = true;
                _txtDateOfBirth.FieldParameters.jQueryDatePickerOptions = null;
            }
        }

        private void showEmailField(FormFieldsItemGroupData group, ProfileFieldItemData field)
        {
            //Email 
            _txtEmail = (MyTxtBoxEmail)group.AddFormWebControl(new MyTxtBoxEmail()
            {
                FieldParameters =
                    new FieldSingleControlParameters()
                    {
                        id = "ProfileFields_txtEmail",
                        required = field.Required,
                        titleContentText =
                            Functionality.LblEmail,
                        helpMessageContentText =
                            (this.Functionality.DisplayType ==
                             PROFILE_FIELDS_DISPLAY_TYPE.
                                 UpdateProfile
                                 ? Functionality.
                                       EmailInformationMessage
                                 : null)
                    },
                ReadOnly =
                    this.Functionality.DisplayType ==
                    PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile,
                InitialValue =
                    (this.Functionality.Member != null
                         ? this.Functionality.Member.Data.Email
                         : null),
            });
            //Validate Email
            if (!_txtEmail.ReadOnly.GetValueOrDefault())
            {
                _txtEmail.FieldParameters.validators.Add(new ValidatorAJAX()
                {
                    Parameters = new ValidatorAJAXParameters()
                    {
                        ajaxValidationURL =
                            "/_ComponentsGeneric/Frontend/Handlers/AJAX/memberCheckEmailExists.ashx",
                        omitValues = this.Functionality.Member == null || this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.Register ? null : new List<string>() { this.Functionality.Member.Data.Email }
                    }
                });
            }
        }

        private void showConfirmEmailField(FormFieldsItemGroupData group, ProfileFieldItemData field)
        {
            if (this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.Register)
            {

                //Confirm Email 
                _txtConfEmail = (MyTxtBoxEmail)group.AddFormWebControl(new MyTxtBoxEmail()
                {
                    FieldParameters =
                        new FieldSingleControlParameters()
                        {
                            id =
                                "ProfileFields_txtConfirmEmail",
                            required = field.Required,
                            titleContentText =
                                Functionality.
                                LblConfirmEmail
                        },
                    InitialValue =
                        (this.Functionality.Member != null
                             ? this.Functionality.Member.
                                   Data.Email
                             : null),
                });
                ValidatorFieldGroup groupEmailValidator = new ValidatorFieldGroup(FIELD_SUBGROUP_TYPE.SameValues,_txtEmail, _txtConfEmail);
                _txtConfEmail.FieldParameters.validators.Add(groupEmailValidator);
            }
        }

        private void showMobileField(FormFieldsItemGroupData group, ProfileFieldItemData field)
        {
            //Mobile 
            _txtMobile = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ProfileFields_txtMob",
                    titleContentText = Functionality.LblMobilePhone
                },
                InitialValue = (this.Functionality.Member != null ? this.Functionality.Member.Data.Mobile : null)
            });
        }

        private void showTelephoneField(FormFieldsItemGroupData group, ProfileFieldItemData field)
        {
            //Telephone
            _txtTelephone = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ProfileFields_txtTelephone",
                    required = field.Required,
                    titleContentText = Functionality.LblTelephone
                },
                InitialValue = (this.Functionality.Member != null ? this.Functionality.Member.Data.Telephone : null)
            });
        }
        private void addFieldsToFormGroup(FormFieldsItemGroupData group, IEnumerable<ProfileFieldItemData> fields)
        {
            foreach (var field in fields)
            {
                switch (field.Field)
                {
                    case Enums.PROFILE_FIELDS.BirthDate:
                        showBirthDateField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.TimeZone:
                        showTimeZone(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.City:
                        showCityField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Company:
                        showCompanyField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.AccountType:
                        showPersonalAccountType(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.ConfirmEmail:
                        showConfirmEmailField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Country:
                        showCountryField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Email:
                        showEmailField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.FullName:
                        showFirstOrFullNameField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.IdCard:
                        showIdCardField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.VatNumber:
                        showVatNumberField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Language:
                        showLanguageField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Gender:
                        showGender(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Mobile:
                        showMobileField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Name:
                        showFirstOrFullNameField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Street:
                        showStreetField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Street2:
                        showStreet2Field(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Surname:
                        showLastNameField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Telephone:
                        showTelephoneField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Username:
                        showUsername(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.ZIP_PostCode:
                        showZIPPostCodeField(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.ConfirmPassword:
                        showConfirmPassword(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.Password:
                        showPassword(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.AllowPromotions:
                        showPromotionsCheckbox(group, field);
                        break;
                    case Enums.PROFILE_FIELDS.AcceptTerms:
                        showTermsCheckbox(group, field);
                        break;
                    default:
                        throw new NotImplementedException("Profile Field Type <" + field + "> not implemented!");
                }
            }

        }

        private void initPersonalInformation()
        {
            var g = formProfile.Functionality.AddGroup(new CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields.FormFieldsItemGroupData()
            {
                Identifier = PERSONAL_INFORMATION_IDENTIFIER,
                TitleContentText = Functionality.TextPersonalInformation,
                ValidationGroup = this.Functionality.ValidationGroupPersonalInformation,
            });

            g.CssManager.AddClass("personal-information");

            addFieldsToFormGroup(g, this.Functionality.PersonalInfoFieldsToInclude);


            initPersonalInformationButton();
        }

        private void showUsername(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            _txtUsername = (MyTxtBoxString)group.AddFormWebControl(new MyTxtBoxString()
            {
                FieldParameters = new FieldSingleControlParameters()
                {
                    id = "ProfileFields_txtUsername",
                    required = fieldData.Required,
                    titleContentText = Functionality.LblUsername,
                    helpMessageContentText = (this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile ? Functionality.UsernameInformationMessage : null)
                },
                AutoComplete = false,
                ReadOnly = this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile,
                InitialValue = (this.Functionality.Member != null ? ((IUser)this.Functionality.Member.Data).UsernameHashed : null)

            });

            _txtUsername.FieldParameters.validators.Add(new ValidatorAJAX()
            {
                Parameters = new ValidatorAJAXParameters()
                {
                    ajaxValidationURL = "/_ComponentsGeneric/Frontend/Handlers/AJAX/memberCheckUsernameExists.ashx",
                    omitValues = this.Functionality.Member == null ? null : new List<string>() { ((IUser)this.Functionality.Member.Data).UsernameHashed }
                }
            });
        }

        private void showPassword(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            if (this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile)
            {
                //Old Pass
                _txtOldPass = (MyTxtBoxStringPassword)group.AddFormWebControl(new MyTxtBoxStringPassword()
                {
                    FieldParameters = new FieldSingleControlPasswordParameters()
                    {
                        id = "ProfileFields_txtOldPassword",
                        titleContentText = Functionality.LblOldPassword,
                        required = fieldData.Required
                    },
                    AutoComplete = false
                });
            }

            //Password
            _txtPass = ProfileFields.GetPasswordField(Functionality.LblPassword, fieldData.Required);
            _txtPass = (MyTxtBoxStringPassword)group.AddFormWebControl(_txtPass);
        }

        private void showConfirmPassword(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            //Conf Pass
            _txtConfPass = (MyTxtBoxStringPassword) group.AddFormWebControl(new MyTxtBoxStringPassword()
                                                                            {
                                                                                FieldParameters = new FieldSingleControlPasswordParameters()
                                                                                                  {
                                                                                                      id = "ProfileFields_txtConfPassword",
                                                                                                      required = fieldData.Required, showPasswordStrengthMeter = false,
                                                                                                      titleContentText = Functionality.LblConfirmPassword

                                                                                                  },
                                                                                AutoComplete = false
                                                                            });
            if (_txtPass != null && _txtConfPass != null)
            {
                ValidatorFieldGroup groupPassValidation = new ValidatorFieldGroup(FIELD_SUBGROUP_TYPE.SameValues, _txtPass, _txtConfPass);
                _txtPass.FieldParameters.validators.Add(groupPassValidation);
                _txtConfPass.FieldParameters.validators.Add(groupPassValidation);
            }
        }

        private void showPromotionsCheckbox(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            if (this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.Register)
            {
                _chkAllowPromotions = (MyCheckBoxWithLabel)group.AddFormWebControl(new MyCheckBoxWithLabel()
                {
                    FieldParameters = new FieldSingleControlParameters()
                    {
                        id = "ProfileFields_chkAllowPromotions",
                        titleContentText = Functionality.LblAllowPromotions
                    },
                    CssClass = "clearfix",
                    LabelContentText = Functionality.PromotionsConfirmationText
                }, layoutInfo: new Code.Controls.WebControls.FormFields.FormFieldsLayoutTypeInformation()
                {
                    DoNotShowLabel = true
                });
            }

        }

        private void showTermsCheckbox(FormFieldsItemGroupData group, ProfileFieldItemData fieldData)
        {
            if (this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.Register && this.Functionality.IncludeTermsConditions)
            {
                var minReq = getMinAgeToRegister();
                _chkTerms = (MyCheckBoxWithLabel)group.AddFormWebControl(new MyCheckBoxWithLabel()
                {
                    FieldParameters = new FieldSingleControlParameters()
                    {
                        id = "ProfileFields_chkTerms",
                        required = fieldData.Required,
                        titleContentText = Functionality.LblTermsConfirmation
                    },
                    CssClass = "clearfix",
                    LabelContentText = Functionality.TermsConditionsConfirmationText
                }, layoutInfo: new Code.Controls.WebControls.FormFields.FormFieldsLayoutTypeInformation()
                {
                    DoNotShowLabel = true
                });
            }
            
        }
        
        
        private void initAccountInformation()
        {
            var g = formProfile.Functionality.AddGroup(new CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields.FormFieldsItemGroupData()
            {
                Identifier = ACCOUNT_INFORMATION_IDENTIFIER,
                //To link both with same button if register
                ValidationGroup = this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.Register ? this.Functionality.ValidationGroupPersonalInformation : this.Functionality.ValidationGroupAccountInformation,
                TitleContentText = Functionality.TextAccountInformation,
            });
            g.CssManager.AddClass("form-fields-account-information");


            addFieldsToFormGroup(g, this.Functionality.AccountInfoFieldsToInclude);

            
            initAccountInformationButton();
        }

        private void initDiscountInformation()
        {
            bool isSiteUsingMemberDiscounts = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_IsSiteUsingMemberDiscounts);
            if (isSiteUsingMemberDiscounts)
            {
                var showDiscountInformationInProfile = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_ProfileFieldsShowDiscountCode);
                if (this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile && showDiscountInformationInProfile)
                {
                    var g = formProfile.Functionality.AddGroup(new CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields.FormFieldsItemGroupData()
                                                               {
                                                                   Identifier = DISCOUNT_INFORMATION_IDENTIFIER,
                                                                   ValidationGroup = this.Functionality.ValidationGroupDiscountInformation,
                                                                   TitleContentText = _textDiscountInformation,
                                                               });
                    g.CssManager.AddClass("form-fields-discount-information");

                    ContentTextBaseFrontend descriptionCntTxt = null;
                    TokenReplacerNew dict = null;
                    if (this.Functionality.Member.Data.HasDiscountCode())
                    {
                        var currentDiscountCode = this.Functionality.Member.Data.GetCurrentDiscountCode();
                        if (currentDiscountCode != null)
                        {
                            descriptionCntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_ProfileFields_Discounts_Available_DescriptionText).ToFrontendBase();
                            dict = descriptionCntTxt.CreateTokenReplacer();
                            dict.Add(BusinessLogic_v3.Constants.Tokens.DISCOUNT_COUPON_CODE, currentDiscountCode.VoucherCode);
                            dict.Add(BusinessLogic_v3.Constants.Tokens.DISCOUNT_COUPON_TITLE, CS.General_v3.Util.Text.ToCamelCase(currentDiscountCode.GetSpecialOfferTitle()));

                            dict.Add(BusinessLogic_v3.Constants.Tokens.DISCOUNT_COUPON_HAS_EXPIRY, currentDiscountCode.DateTo.HasValue);
                            dict.Add(BusinessLogic_v3.Constants.Tokens.DISCOUNT_COUPON_EXPIRES_ON, currentDiscountCode.DateTo.GetValueOrDefault().ToShortDateString());
                        }
                    }
                    else
                    {
                        descriptionCntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_ProfileFields_Discounts_NotAvailable_DescriptionText).ToFrontendBase();
                    }
                    g.DescriptionContentText = descriptionCntTxt;
                    g.DescriptionReplacementTags = dict;

                    //Discount Code
                    _txtDiscountCode = (MyTxtBoxString) g.AddFormWebControl(new MyTxtBoxString()
                                                                            {
                                                                                FieldParameters = new FieldSingleControlPasswordParameters()
                                                                                                  {
                                                                                                      id = "ProfileFields_txtDiscountCode",
                                                                                                      titleContentText = Functionality.LblDiscountCode,
                                                                                                      required = true
                                                                                                  },
                                                                                AutoComplete = false
                                                                            });

                    var buttonApplyText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_ProfileFields_ButtonApplyText).ToFrontendBase();
                    _btnDiscountApply = formProfile.Functionality.AddButton(buttonApplyText.GetContent(), DISCOUNT_INFORMATION_IDENTIFIER);
                    _btnDiscountApply.Click += new EventHandler(_btnDiscountApply_Click);
                }
            }
        }

        void _btnDiscountApply_Click(object sender, EventArgs e)
        {
            string discountCode = _txtDiscountCode.GetFormValueAsString();
            if (!string.IsNullOrEmpty(discountCode))
            {

                var result = this.Functionality.Member.Data.SetDiscountCode(discountCode);
                switch(result)
                {
                    case AddDiscountCodeResult.OK:
                        var successDiscountCode = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_ProfileFields_Discounts_Success);
                        this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(successDiscountCode.ToFrontendBase());
                        break;
                    case AddDiscountCodeResult.Invalid:
                        var errorDiscountCode = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_ProfileFields_Discounts_Error);
                        this.Page.Functionality.ShowErrorMessageAndRedirectToPage(errorDiscountCode.ToFrontendBase());
                        break;
                }
            }
            else
            {
                var noDiscountCode = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.MembersArea_ProfileFields_Discounts_PleaseEnterADiscountCode);
                this.Page.Functionality.ShowErrorMessageAndRedirectToPage(noDiscountCode.ToFrontendBase());
            }
        }

        private void setUsernameAndPasswordNotTheSame()
        {
            if (_txtUsername != null && _txtPass != null)
            {
                ValidatorFieldGroup validatorGroup = new ValidatorFieldGroup( FIELD_SUBGROUP_TYPE.NotSameValues, _txtUsername, _txtPass);
                _txtUsername.FieldParameters.validators.Add(validatorGroup);
                _txtPass.FieldParameters.validators.Add(validatorGroup);
            }
        }

        private void setGroupValidations()
        {
            setUsernameAndPasswordNotTheSame();
        }

        private void initFields()
        {
            initMember();
            updateTexts();
            initPersonalInformation();
            initAccountInformation();
            initDiscountInformation();
            setGroupValidations();
            initButton();
        }

        private void initMember()
        {
            if (Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.Register)
            {
                var _member = BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.GetTemporaryUser().ToFrontendBase();
                if (_member != null)
                {
                    this.Functionality.Member = _member;
                }
            }
            else if (Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile)
            {
                var _member = BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.GetLoggedInUserAlsoFromRememberMe().ToFrontendBase();
                if (_member != null)
                {
                    this.Functionality.Member = _member;
                }
            }
        }

        private void updateTexts()
        {
            //Labels
            this.Functionality.LblName = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Name).ToFrontendBase();
            this.Functionality.LblIdCard = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_IDCard).ToFrontendBase();
            this.Functionality.LblVatNumber = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_VatNumber).ToFrontendBase();
            this.Functionality.LblSurname = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Surname).ToFrontendBase();
            this.Functionality.LblStreet = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Street).ToFrontendBase();
            this.Functionality.LblStreet2 = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Street2).ToFrontendBase();
            this.Functionality.LblGender = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Gender).ToFrontendBase();
            this.Functionality.LblCity = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_City).ToFrontendBase();
            this.Functionality.LblPostCode = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_PostalCode).ToFrontendBase();
            this.Functionality.LblCountry = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Country).ToFrontendBase();
            this.Functionality.LblTimezone = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Timezone).ToFrontendBase();
            this.Functionality.LblBirthDate = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_DOB).ToFrontendBase();
            this.Functionality.LblConfirmEmail = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_ConfirmEmail).ToFrontendBase();
            this.Functionality.LblEmail = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Email).ToFrontendBase();
            this.Functionality.LblConfirmEmail = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_ConfirmEmail).ToFrontendBase();
            this.Functionality.LblLanguage = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_PreferredLanguage).ToFrontendBase();
            this.Functionality.LblMobilePhone = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_MobilePhone).ToFrontendBase();
            this.Functionality.LblUsername = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Username).ToFrontendBase();
            this.Functionality.LblOldPassword = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_OldPassword).ToFrontendBase();
            this.Functionality.LblPassword = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Password).ToFrontendBase();
            this.Functionality.LblConfirmPassword = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_ConfirmPassword).ToFrontendBase();
            this.Functionality.LblTermsConfirmation = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_AgeTermsConditions).ToFrontendBase();
            this.Functionality.LblCompany = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Company).ToFrontendBase();
            this.Functionality.LblTelephone = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Telephone).ToFrontendBase();
            this.Functionality.LblDiscountCode = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_DiscountCode).ToFrontendBase();
            this.Functionality.LblPersonalAccountType = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_AccountType).ToFrontendBase();
            this.Functionality.LblAllowPromotions = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Promotions).ToFrontendBase();
            
            //Messages
            this.Functionality.EmailInformationMessage = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_EmailInformationMessage).ToFrontendBase();
            this.Functionality.UsernameInformationMessage = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_UsernameInformationMessage).ToFrontendBase();
            this.Functionality.TermsConditionsConfirmationText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_TermsAndConditionText).ToFrontendBase();
            this.Functionality.PromotionsConfirmationText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_Promotions).ToFrontendBase();
            
            string url = Functionality.TermsConditionsURL;
            if (String.IsNullOrEmpty(url))
            {
                var cp =
                    BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                        BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.TermsConditions);
                if (cp != null)
                {
                    url = cp.GetUrl();
                }
            }
            var cntTxt =
                BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_ConfirmationText).ToFrontendBase();
            this.Functionality.TermsConditionsConfirmationText.TokenReplacer[BusinessLogic_v3.Constants.Tokens.TAG_TERMS_LINK] = url;

           
            //Button
            if(this.Functionality.BtnSubmitText == null)
            {
                this.Functionality.BtnSubmitText = this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.Register ? 
                    BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_RegistrationButtonText).ToFrontendBase() :
                    BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_UpdateProfileButtonText).ToFrontendBase();
            }
         
            // Sections
            _textDiscountInformation = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_TextDiscountInformation).ToFrontendBase();
            this.Functionality.TextPersonalInformation = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_TextPersonalInformation).ToFrontendBase();
            this.Functionality.TextAccountInformation = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_TextAccountInformation).ToFrontendBase();
        }

        private void initPersonalInformationButton()
        {
            if (Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile)
            {
                MyButton btnPersonalInfo = formProfile.Functionality.AddButton(Functionality.BtnSubmitText, PERSONAL_INFORMATION_IDENTIFIER);
                btnPersonalInfo.ID = "btnProfileFields_PersonalInfo";
                btnPersonalInfo.Click += new EventHandler(btnPersonalInformationSubmit_Click);
            }
        }

        private void initAccountInformationButton()
        {
            if (Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile)
            {
                var btnAccountInfo = formProfile.Functionality.AddButton(Functionality.BtnSubmitText, ACCOUNT_INFORMATION_IDENTIFIER);
                btnAccountInfo.ID = "btnProfileFields_AccountInfo";
                btnAccountInfo.Click += new EventHandler(btnAccountInfo_Click);
            }
        }

        void btnAccountInfo_Click(object sender, EventArgs e)
        {
            ProfileFieldsData data = new ProfileFieldsData();

            // Specific Member Update
            data = submitAccountInfo(data);

            // General Member Update 
            updateMember(data);

            this.Functionality.triggerOnSubmitSuccess(data);
        }

        void btnPersonalInformationSubmit_Click(object sender, EventArgs e)
        {
            ProfileFieldsData data = new ProfileFieldsData();

            data = submitPersonalInfo(data);
            /// Specific Member Update

            this.Functionality.triggerOnPreSubmitSuccess(data);
            // General Member Update
            updateMember(data);
            this.Functionality.triggerOnSubmitSuccess(data);
        }

        private ProfileFieldsData submitPersonalInfo(ProfileFieldsData data)
        {
            CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? countryCode = null;
            if (_cmbCountry != null)
            {
                countryCode = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(_cmbCountry.GetFormValueAsString());
            }
            if (_cmbTimeZone != null)
            {
                string timeZoneID = _cmbTimeZone.GetFormValueAsString();
                if (!string.IsNullOrEmpty(timeZoneID))
                {
                    var tz = CS.General_v3.Util.Date.GetTimeZoneFromID(timeZoneID);
                    data.TimeZone = tz;
                }
            }
            CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? languageCode = null;
            if (_cmbLanguages != null)
            {
                languageCode = CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639FromCode(_cmbLanguages.GetFormValueAsString());
            }
           
            DateTime? dob = _txtDateOfBirth != null ? _txtDateOfBirth.GetFormValueAsDateNullable() : Date.Now.GetEndOfDay();
            {
                data.DisplayType = this.Functionality.DisplayType;
                data.Name = _txtName != null ? _txtName.GetFormValueAsString() : null;
                data.IdCard = _txtIdCard != null ? _txtIdCard.GetFormValueAsString() : null;
                data.VatNumber = _txtVatNumber != null ? _txtVatNumber.GetFormValueAsString() : null;
                data.Surname = _txtSurname != null ? _txtSurname.GetFormValueAsString() : null;
                data.Gender = _rdListGender != null && !_rdListGender.ReadOnly.GetValueOrDefault() ? _rdListGender.GetFormValueAsEnumNullable<CS.General_v3.Enums.GENDER>() : null;
                if (_txtDateOfBirth != null && !_txtDateOfBirth.ReadOnly.GetValueOrDefault())
                {
                    data.DateOfBirth = dob;
                }
                if (_chkAllowPromotions != null)
                {
                    data.AllowPromotions = _chkAllowPromotions.GetFormValueObjectAsBool();
                }
                data.City = _txtCity != null && !_txtCity.ReadOnly.GetValueOrDefault() ? _txtCity.GetFormValueAsString() : null;
                data.ChkConfirm = _chkTerms != null ? _chkTerms.GetFormValueObjectAsBool() : false;
                data.Email = _txtEmail != null && !_txtEmail.ReadOnly.GetValueOrDefault() ? _txtEmail.GetFormValueAsString() : null;
                data.Mobile = _txtMobile != null && !_txtMobile.ReadOnly.GetValueOrDefault() ? _txtMobile.GetFormValueAsString() : null;
                data.PostcodeOrZip = _txtZIP != null && !_txtZIP.ReadOnly.GetValueOrDefault() ? _txtZIP.GetFormValueAsString() : null;
                data.CountryCode = countryCode;
                data.LanguageCode = languageCode;
                data.AccountType = (_cmbPersonalAccountType != null ? _cmbPersonalAccountType.GetFormValueAsEnum<CS.General_v3.Enums.PERSONAL_ACCOUNT_TYPE>(): General_v3.Enums.PERSONAL_ACCOUNT_TYPE.PersonalAccount);
                data.Street = _txtStreet != null && !_txtStreet.ReadOnly.GetValueOrDefault() ? _txtStreet.GetFormValueAsString() : null;
                data.Street2 = _txtStreet2 != null && !_txtStreet2.ReadOnly.GetValueOrDefault() ? _txtStreet2.GetFormValueAsString() : null;
                data.Company = _txtCompany != null && !_txtCompany.ReadOnly.GetValueOrDefault() ? _txtCompany.GetFormValueAsString() : null;
                data.Tel = _txtTelephone != null && !_txtTelephone.ReadOnly.GetValueOrDefault() ? _txtTelephone.GetFormValueAsString() : null;
            }
            //There is an error

            if (dob.HasValue) 
            {
                var invalidRequirement = isDOBValid(dob.Value, data.CountryCode);

                if (invalidRequirement != null)
                {
                    //DOB is over the minimum allowed, i.e. user is younger
                    ContentTextBaseFrontend errorMsg = null;
                    if (data.CountryCode.HasValue&& invalidRequirement.Countries != null && invalidRequirement.Countries.Count > 0)
                    {
                        //Country specific
                        errorMsg = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Profile_Fields_MinimumAgeErrorMsgForCountry).ToFrontendBase();
                        errorMsg.TokenReplacer[BusinessLogic_v3.Constants.Tokens.COUNTRY] = CS.General_v3.Util.EnumUtils.StringValueOf(data.CountryCode.Value);
                    }
                    else
                    {
                        //Generic
                        errorMsg = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Profile_Fields_MinimumAgeErrorMsg).ToFrontendBase();
                    }
                    errorMsg.TokenReplacer[BusinessLogic_v3.Constants.Tokens.MINIMUM_MEMBER_AGE_ALLOWED] = invalidRequirement.MinimumAgeYears.ToString();

                    this.Page.Functionality.ShowErrorMessageAndRedirectToPage(errorMsg);
                }
            }
            return data;
        }

        private ProfileFieldsData submitAccountInfo(ProfileFieldsData data)
        {
            data.Username = _txtUsername != null ? _txtUsername.GetFormValueAsString() : null;
            
            //if (Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile)
            //{

            string oldPassword = _txtOldPass != null ? _txtOldPass.GetFormValueAsString() : null;
            string password = _txtPass != null ? _txtPass.GetFormValueAsString() : null;
            string confirmPassword = _txtConfPass != null ? _txtConfPass.GetFormValueAsString() : null;

            if (password == confirmPassword)
            {
                if (Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile)
                {
                    if (Functionality.Member.Data.CheckPassword(oldPassword))
                    {
                        data.Password = password;
                    }
                    else
                    {
                        //pass incorrect
                        this.Page.Functionality.ShowErrorMessageAndRedirectToPage(Functionality.IncorrectOldPasswordMessage);
                    }
                }
                else
                {
                    data.Password = password;
                }
            }
            else
            {
                this.Page.Functionality.ShowErrorMessageAndRedirectToPage(Functionality.IncorrectConfirmPasswordMessage);
            }
            //udatePassword(oldPassword, password, confirmPassword);
            //}
            return data;
        }

        private void initButton()
        {
            if (this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.Register)
            {
                MyButton btnMainSubmit = formProfile.Functionality.AddButton(Functionality.BtnSubmitText, ACCOUNT_INFORMATION_IDENTIFIER);
                btnMainSubmit.Click += new EventHandler(btnMainSubmit_Click);
            }
        }

        private void updateMember(ProfileFieldsData data)
        {
            var _member = Functionality.Member;
            if (_member != null)
            {
                OperationResult result;
                using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                {
                    data.FillMember(_member);
                    result = _member.Data.Update();
                    t.Commit();
                }
                if (result.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
                {
                    // Specific Update - Success
                    if (!this.Functionality.triggerOnSpecificProfileUpdateSuccess(data))
                    {
                        // Success
                        var text = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                            BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Profile_UpdatedSuccessful).ToFrontendBase();

                        if (!this.Functionality.DoNotRedirectOnSubmitSuccess)
                        {
                            
                            this.Page.Functionality.ShowSuccessMessageAndRedirectToPage(text);
                        }
                    }
                }
                else
                {
                    // Specific Update - Failure
                    if (!this.Functionality.triggerOnSpecificProfileUpdateFailure(data))
                    {
                        // Failure
                        PageUtil.RedirectPage(ErrorsRoute.GetGenericErrorURL());
                    }
                }
            }
            else
            {
                var text =
                    BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_General_YouMustBeLoggedInToAccessThisPage).ToFrontendBase();
                this.Page.Functionality.ShowErrorMessageAndRedirectToPage(text, "/");
            }
        }

        private void registerMember(ProfileFieldsData data)
        {
            if (data.FillMember(Functionality.Member))
            {
                var t = ((MemberBase)Functionality.Member.Data).Username;
                Enums.MEMBER_REGISTRATION_RESULT register = Functionality.Member.Data.RegisterMember();
                switch (register)
                {
                    case Enums.MEMBER_REGISTRATION_RESULT.EmailAlreadyExists:
                    case Enums.MEMBER_REGISTRATION_RESULT.UsernameAlreadyExists:
                        registrationFailure(data, getContentTextOfEnum(register));
                        break;
                    case Enums.MEMBER_REGISTRATION_RESULT.Success:
                        registrationSuccess(data);
                        break;
                    default:
                        throw new InvalidOperationException("Value <" + register + "> not mapped");
                }
            }
        }

        private ContentTextBaseFrontend getContentTextOfEnum(Enums.MEMBER_REGISTRATION_RESULT register)
        {
            switch (register)
            {
                case Enums.MEMBER_REGISTRATION_RESULT.EmailAlreadyExists:
                    return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Members_Registration_Result_EmailAlreadyExists).ToFrontendBase();
                case Enums.MEMBER_REGISTRATION_RESULT.Success:
                    return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Members_Registration_Result_Success).ToFrontendBase();
                case Enums.MEMBER_REGISTRATION_RESULT.UsernameAlreadyExists:
                    return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Members_Registration_Result_UsernameAlreadyExists).ToFrontendBase();
                default:
                    throw new NotImplementedException("Value <"+register+"> not mapped");
            }
        }

        private void registrationSuccess(ProfileFieldsData data)
        {
            // Specific Registration - Success
            if (!this.Functionality.triggerOnSpecificRegistrationSuccess(data))
            {
                string userCredential = null;
                userCredential = !string.IsNullOrEmpty(data.Username) ? data.Username : data.Email;
                DiscountsUtil.AddDiscountCodeFromSession(userCredential);

                //Success
                
                
                if ((((IUser)this.Functionality.Member.Data).Activated)) //if activated ok, then redirect to success page
                {
                    ArticleBaseFrontend cp =
                BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier
                    (BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_Register_RegistrationSuccessful).
                    ToFrontendBase();

                    var result = MemberSessionLoginManager.Instance.Login(Functionality.Member.Data, false);
                    if (result.LoginStatus != CS.General_v3.Enums.LOGIN_STATUS.Ok)
                    {
                        //Some error encountered logging him in which should be very strange so throw error
                        throw new Exception("Error logging you in");
                    }
                    if (!this.Functionality.DoNotRedirectOnSubmitSuccess)
                    {
                        PageUtil.RedirectPage(cp.Data.GetUrl());
                    }
                }
                else
                {//redirect to activation page
                    ArticleBaseFrontend cp =
                BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier
                    (BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_Register_RequiresActivation).
                    ToFrontendBase();
                    var url = cp.GetUrl();
                    PageUtil.RedirectPage(url);
                }
                
            }
        }

        private void registrationFailure(ProfileFieldsData data, ContentTextBaseFrontend message = null)
        {
            // Specific Registration - Failure
            if (!this.Functionality.triggerOnSpecificRegistrationFailure(data))
            {
                // General Registration - Failure
                this.Page.Functionality.ShowErrorMessageAndRedirectToPage(message);
            }
        }

        void btnMainSubmit_Click(object sender, EventArgs e)
        {
            ProfileFieldsData data = new ProfileFieldsData();
            data = submitAccountInfo(data);
            data = submitPersonalInfo(data);

            this.Functionality.triggerOnPreSubmitSuccess(data);

            if (this.Functionality.DisplayType == PROFILE_FIELDS_DISPLAY_TYPE.Register)
            {
                registerMember(data);
            }
            else
            {
                updateMember(data);
            }
            this.Functionality.triggerOnSubmitSuccess(data);
        }

        protected override void OnLoad(EventArgs e)
        {
            initFields();
            base.OnLoad(e);
        }

        public static MyTxtBoxStringPassword GetPasswordField(ContentTextBaseFrontend labelPassword, bool required)
        {
            //Password
            var txtPass = new MyTxtBoxStringPassword()
            {
                FieldParameters = new FieldSingleControlPasswordParameters()
                {
                    id = "ProfileFields_txtPassword",
                    required = required,
                    passwordStrengthType = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<Enums.PASSWORD_STRENGTH_CHARACTERS>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Password_StrengthLevel),
                    showPasswordStrengthMeter = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Password_ShowStrengthMeter),
                    titleContentText = labelPassword
                },
                AutoComplete = false
            };

            int minLength = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Password_MinLength);
            if (minLength > 0)
            {
                txtPass.FieldParameters.GetOrCreateValidator<ValidatorString>().Parameters.minLength = minLength;
            }
            return txtPass;
        }

    }
}