﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
using BusinessLogic_v3.Frontend.MemberModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Events
{
    public partial class EventTutorThumbnailItems : BaseUserControl
    {

        public class FUNCTIONALITY
        {
            private EventTutorThumbnailItems _control;
            public FUNCTIONALITY(EventTutorThumbnailItems control)
            {
                _control = control;
            }
            public IEnumerable<MemberBaseFrontend> Members { get; set; }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public EventTutorThumbnailItems()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initListing();
        }

        private void initListing()
        {
            if(this.Functionality.Members != null && this.Functionality.Members.Count() > 0)
            {
                foreach (var m in Functionality.Members)
                {
                    var _ctrl = EventTutorThumbnailItem.LoadControl(this.Page);
                    _ctrl.Functionality.Member = m;
                    divTutorisListingItems.Controls.Add(_ctrl);
                }
            }
        }
        public static EventTutorThumbnailItems LoadControl(Page pg)
        {
            var ctrl = pg.LoadControl("~/_ComponentsGeneric/Frontend/UserControls/Events/EventTutorThumbnailItems.ascx") as EventTutorThumbnailItems;
            return ctrl;
        }
    }
}