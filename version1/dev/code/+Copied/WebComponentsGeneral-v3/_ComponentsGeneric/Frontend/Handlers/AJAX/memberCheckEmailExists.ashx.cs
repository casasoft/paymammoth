﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Handlers.AJAX
{
    using Code.Classes.HTTPHandlers;

    /// <summary>
    /// Summary description for memberCheckEmailExists
    /// </summary>
    public class memberCheckEmailExists : CustomAJAXFieldValidationHandler
    {

        protected override string Validate(string data)
        {
            //Check for email exists

            var loggedUser = BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.GetLoggedInUserAlsoFromRememberMe();
            if (CS.General_v3.Util.PageUtil.IsLocalhost())
            {
                System.Threading.Thread.Sleep(1000);
            }

            var emailExists = BusinessLogic_v3.Modules.Factories.MemberFactory.CheckIfEmailExists(data, loggedUser != null ? (long?)loggedUser.ID : null);
            return emailExists
                       ? BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                           BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_EmailAlreadyExists).GetContent()
                       : null;
        }
    }
}