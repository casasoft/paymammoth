﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Handlers.AJAX.Credits
{
    using BusinessLogic_v3.Extensions;
    using BusinessLogic_v3.Frontend.MemberModule;
    using Code.Classes.HTTPHandlers;

    /// <summary>
    /// Summary description for memberCheckEmailExists
    /// </summary>
    public class getCreditsAvailable : CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers.BaseJSONHandler
    {


        protected override void outputJsonVariables(HttpContext context)
        {
            MemberBaseFrontend member = BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.GetLoggedInUser().ToFrontendBase();
            if (member != null)
            {
                double credits = member.Data.GetTotalCredits();
                this["credits"] = credits.ToString();
            }
            else
            {
                throw new NotSupportedException("User must be logged in to access this method");
            }
        }
    }
}