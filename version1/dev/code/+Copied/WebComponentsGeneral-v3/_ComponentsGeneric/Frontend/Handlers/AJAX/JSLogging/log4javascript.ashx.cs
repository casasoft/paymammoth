﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Handlers.AJAX.JSLogging
{
    using Code.Classes.HTTPHandlers;

    /// <summary>
    /// Summary description for log4javascript
    /// </summary>
    public class log4javascript : BaseHandler
    {
        protected override void processRequest(HttpContext ctx)
        {
            string jsonData = ctx.Request.Form["data"];

            List<JsonLayoutData> messages = CS.General_v3.Util.JSONUtil.Deserialise <List< JsonLayoutData> > (jsonData);
                 
            string s = "";
            for (int i = 0;i<messages.Count;i++)
            {
                var msg = messages[i];
                var type = msg.GetLevelType();
                if (type == General_v3.Enums.LOG4NET_MSG_TYPE.Error || type == General_v3.Enums.LOG4NET_MSG_TYPE.Fatal)
                {
                    CS.General_v3.Error.Reporter.ReportError(type, msg.message);
                }
                else
                {
                    CS.General_v3.Util.Log4NetUtil.LogMsgByObject(this, type, msg.message, null);

                }
            }

        }
    }
    public class JsonLayoutData
    {
        public string logger;
        public long timeStamp;
        public string level;
        public string url;
        public string message;
        public string exception;

        public TimeSpan Date
        {
            get
            {
                return TimeSpan.FromMilliseconds(timeStamp);
            }
        }

        public General_v3.Enums.LOG4NET_MSG_TYPE GetLevelType()
        {
            switch (level)
            {
                    
                case "TRACE":
                case "INFO":
                    return General_v3.Enums.LOG4NET_MSG_TYPE.Info;
                case "DEBUG":
                    return General_v3.Enums.LOG4NET_MSG_TYPE.Debug;
                case "WARN":
                    return General_v3.Enums.LOG4NET_MSG_TYPE.Warn;
                case "ERROR":
                    return General_v3.Enums.LOG4NET_MSG_TYPE.Error;
                case "FATAL":
                    return General_v3.Enums.LOG4NET_MSG_TYPE.Fatal;
            }
            return General_v3.Enums.LOG4NET_MSG_TYPE.Info;
        }

    }
}