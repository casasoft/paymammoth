﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Handlers.AJAX
{
    /// <summary>
    /// Summary description for memberCheckEmailExists
    /// </summary>
    public class memberCheckUsernameExists : CustomAJAXFieldValidationHandler
    {
        protected override string Validate(string data)
        {
            //Check for email exists

            var loggedUser = BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.GetLoggedInUserAlsoFromRememberMe();
            if (CS.General_v3.Util.PageUtil.IsLocalhost())
            {
                System.Threading.Thread.Sleep(1000);
            }

            var usernameExists = BusinessLogic_v3.Modules.Factories.MemberFactory.CheckIfUserExists(data);
            return usernameExists
                       ? BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                           BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ProfileFields_UsernameAlreadyExists).
                             GetContent()
                       : null;
        }
    }
}