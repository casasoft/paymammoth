﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Modules.ContentTextModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Handlers.AJAX.CMS
{
    /// <summary>
    /// Summary description for updateContentText
    /// </summary>
    public class updateContentText : CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers.BaseJSONHandler
    {
       

        protected override void outputJsonVariables(HttpContext context)
        {
            long? id = CS.General_v3.Util.QueryStringUtil.GetLongFromQS("id", null, context.Request.Form);
            string value = CS.General_v3.Util.QueryStringUtil.GetStringFromQS("value", context.Request.Form);
            if (id.HasValue)
            {
                if (CmsUserSessionLogin.Instance.IsLoggedIn)
                {
                    //Only if user is logged in
                    IContentTextBase ct = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetByPrimaryKey(id.Value);
                    if (ct != null)
                    {
                        using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                        {
                            if (ct.ContentType == BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.PlainText)
                            {
                                value = CS.General_v3.Util.Text.ConvertHTMLToPlainText(value);
                            }
                            ct._Content_ForEditing = value;
                            ct.Update();
                            t.Commit();
                        }
                    }
                    else
                    {
                        throw new Exception("Content text with id '" + id.Value + "' not found");
                    }

                }
            }
        }
    }
}