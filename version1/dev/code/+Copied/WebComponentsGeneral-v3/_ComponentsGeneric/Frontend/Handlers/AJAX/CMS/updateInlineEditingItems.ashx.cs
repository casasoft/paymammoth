﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Modules.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Handlers.AJAX.CMS
{
    using BusinessLogic_v3;
    using BusinessLogic_v3.Util;

    /// <summary>
    /// Summary description for updateContentText
    /// </summary>
    public class updateInlineEditingItems : BaseJSONHandler
    {
        /// <summary>
        /// To use this handler, you have to initiate a control with:
        ///            CMSInlineEditingItem.Create(this.Functionality.Prize.Data, (x => x.Title), divPrizeTitle, this.Functionality.Prize.Data.Title);
        /// 
        /// </summary>
        /// <param name="context"></param>
        protected override void outputJsonVariables(HttpContext context)
        {
            bool ok = CmsUserSessionLogin.Instance.IsLoggedInMemberASuperAdministrator();
            if (ok)
            {

                string ids = CS.General_v3.Util.QueryStringUtil.GetStringFromQS("id", context.Request.Form);
                string type = CS.General_v3.Util.QueryStringUtil.GetStringFromQS("type", context.Request.Form);
                string propName = CS.General_v3.Util.QueryStringUtil.GetStringFromQS("propName", context.Request.Form);
                string values = CS.General_v3.Util.QueryStringUtil.GetStringFromQS("value", context.Request.Form);
                string textTypes = CS.General_v3.Util.QueryStringUtil.GetStringFromQS("texttype", context.Request.Form);

                string[] listIDs = ids.Split('|');
                string[] listValues = values.Split('|');
                string[] listTextTypes = textTypes.Split('|');
                if (listIDs.Length != listValues.Length || listIDs.Length != listTextTypes.Length)
                {
                    throw new NotSupportedException("Items & Values & text types must match length");
                }
                for (int i = 0; i < listIDs.Length; i++)
                {
                    long id = 0;
                    BusinessLogic_v3.Enums.INLINE_EDITING_TEXT_TYPE? textType = CS.General_v3.Util.EnumUtils.GetEnumByStringValueNullable<BusinessLogic_v3.Enums.INLINE_EDITING_TEXT_TYPE>(listTextTypes[i]);
                    if (textType.HasValue)
                    {
                        if (long.TryParse(listIDs[i], out id))
                        {
                            string newValue = listValues[i];
                            bool isPlainText = textType == Enums.INLINE_EDITING_TEXT_TYPE.PlainText;
                            bool isHTMLText = textType == Enums.INLINE_EDITING_TEXT_TYPE.Html || textType == Enums.INLINE_EDITING_TEXT_TYPE.HtmlWithAtLeastParagraphTag;

                            if (isPlainText)
                            {
                                newValue = CS.General_v3.Util.Text.ConvertHTMLToPlainText(newValue);
                            }
                            if (isHTMLText)
                            {
                                if (!CS.General_v3.Util.HtmlUtil.IsHtml(newValue))
                                {
                                    newValue = CS.General_v3.Util.Text.ConvertPlainTextToHTML(newValue);
                                }
                                if (textType == Enums.INLINE_EDITING_TEXT_TYPE.HtmlWithAtLeastParagraphTag && !CS.General_v3.Util.HtmlUtil.IsHtml(newValue))
                                {
                                    newValue = "<p>" + newValue + "</p>"; // Add tag to make it html
                                }
                            }

                            DataUtil.UpdateDBItemFieldByReflection(id, type, propName, newValue);
                        }
                    }
                }
               
            }

        }


        
    }
}