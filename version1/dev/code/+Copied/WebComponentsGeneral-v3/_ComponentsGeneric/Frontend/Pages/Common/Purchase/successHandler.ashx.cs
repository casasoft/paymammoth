﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Classes.Routing;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers;
using BusinessLogic_v3.Frontend.MemberModule;
using BusinessLogic_v3.Frontend.OrderModule;
using BusinessLogic_v3.Modules.OrderModule;
using PayMammothConnectorBL.v3;
using BusinessLogic_v3;
using log4net;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Purchase
{
    /// <summary>
    /// Summary description for successHandler
    /// </summary>
    public class successHandler : BaseHandler
    {
        //NOTE: This is not the background notification handler for PayMammoth, but the url PayMammoth redirects to.  That handler is in /Frontend/Handers/Payment/status.ashx
        private readonly static ILog _log = LogManager.GetLogger(typeof(successHandler));
        public delegate void VerificationSuccessHandler(OrderBaseFrontend order);
        private MemberBaseFrontend _member;
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    
        public event VerificationSuccessHandler OnVerificationSuccess;
        private OrderBaseFrontend _successOrder;
        public successHandler()
        {
            _log.Debug("successHandler()");
        }

        protected override void processRequest(HttpContext ctx)
        {
            _log.Debug("successHandler() - processRequest");
            if (CS.General_v3.Util.PageUtil.CheckUserAgentForKnownSearchEngineBots()) return; //dont execute if search engine
            
            if (this.OnVerificationSuccess != null && _successOrder != null)
            {
                _log.Debug("successHandler() - onVerificationSuccess - Before");
                this.OnVerificationSuccess(_successOrder);
                _log.Debug("successHandler() - onVerificationSuccess - After");
            }

            init();
        }

        private void init()
        {
            _log.Debug("successHandler() - init()");
            getLoggedInMember();
            verifyPayment();
            
        }

        private void verifyPayment()
        {
            string identifier = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(PayMammoth.Connector.Constants.PARAM_IDENTIFIER);
            IOrderBase order = null;
            if(PayMammothConnectorBL.v3.Util.PurchaseLogic.VerifyPurchaseAndConfirmAfterPayPippa(identifier, out order))
            {
                _log.Info("successHandler() - purchase confirmed and verified, saving order details");
                saveOrderDetailsInSession(order.ToFrontendBase());
                _log.Info("successHandler() - order details saved");
            }
        }

        private void getLoggedInMember()
        {
            _member = Factories.MemberFactory.GetLoggedInUserForCurrentSession().ToFrontendBase();
            if (_member == null)
            {
                _log.Warn("successHandler() - no member in session()");
                throw new InvalidOperationException("No member in session.");
                
            }
        }

        private void saveOrderDetailsInSession(OrderBaseFrontend order)
        {
            if (order != null)
            {
                CS.General_v3.Util.SessionUtil.SetObject(Constants.SESSION_ORDER_DETAILS, order.Data.ID);
                var articleSuccessPage = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.PayPippa_FinalSuccessPage);
                string url = articleSuccessPage.GetUrl();
                CS.General_v3.Util.PageUtil.RedirectPage(url);
            }
            else
            {
                throw new Exception("Order cannot be null");
            }
        }
    }
}