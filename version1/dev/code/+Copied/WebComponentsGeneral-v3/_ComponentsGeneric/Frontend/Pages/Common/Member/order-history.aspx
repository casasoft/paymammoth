﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
     CodeBehind="order-history.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member.order_history" %>

<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="OrderHistoryListing" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/OrderHistory/OrderHistoryListing.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:ContentPage ID="contentPage" runat="server" />
    <CommonControls:OrderHistoryListing ID="orderHistoryListing" runat="server" />
</asp:Content>
