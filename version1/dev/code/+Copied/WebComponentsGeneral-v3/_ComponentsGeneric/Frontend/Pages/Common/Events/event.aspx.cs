﻿using System;
using System.Collections.Generic;
using BusinessLogic_v3;
using BusinessLogic_v3.Classes.Routing;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.EventMediaItemModule;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Frontend.EventModule;
using BusinessLogic_v3.Constants;
using System.Linq;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Events
{

    public partial class _event : BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage>
    {
        private EventBaseFrontend _eventItem;
        private ArticleBaseFrontend _article;

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadEvent();

            if (_eventItem != null)
            {
                updateTexts();
                updateContent();
            }
            else if (PageUtil.IsLocalhost())
            {
                throw new Exception("Event cannot be null");
            }
            else
            {
                PageUtil.RedirectPage(ErrorsRoute.GetPageNotFoundErrorURL());
            }
        }

        private void updateTexts()
        {
            MyContentText.AttachContentTextWithControl(h2CalendarTitle, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_Calendar_Title).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(h2TestimonialsTitle, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_Testimonials_Title).ToFrontendBase());
            MyContentText.AttachContentTextWithControl(divCalendarText, BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.EventPage_Calendar_Text).ToFrontendBase());
        }

        private void loadEvent()
        {
            _article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.Event).ToFrontendBase();
            this.Functionality.ContentPage = _article;

            var eventId = CS.General_v3.Util.RoutingUtil.GetRouteValueAsLongNullable(Tokens.ROUTE_PARAM_ID);
            if (eventId.HasValue)
            {
                _eventItem = BusinessLogic_v3.Modules.Factories.EventFactory.GetByPrimaryKey(eventId.Value).ToFrontendBase();
                if (_eventItem != null)
                {

                    this.eventDetails.Functionality.Event = this.testimonialsScroller.Functionality.Event = this.eventsCalendar.Functionality.Event = _eventItem;

                    var mediaItems = _eventItem.Data.GetMediaItems().ToFrontendBaseList();
                    if (mediaItems != null && mediaItems.Count() > 0)
                    {
                        eventGallery.Functionality.Gallery = mediaItems;
                    }
                    else
                    {
                        CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divMediaGallery);
                    }

                    var showAddThisInHeading = ModuleSettings.Articles.ShowAddThisInHeading;
                    if (showAddThisInHeading)
                    {
                        headingAddThis.Functionality.URL = _eventItem.GetUrl();
                        headingAddThis.Functionality.Style = General_v3.Enums.ADDTHIS_STYLE.DefaultStyleSmall;
                        divHeadingAddThisWrapper.Visible = true;
                    }
                    else
                    {
                        CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divHeadingAddThisWrapper);
                    }
                }
                else
                {
                    CS.General_v3.Util.PageUtil.RedirectPage(ErrorsRoute.GetPageNotFoundErrorURL());
                }
            }
        }

        private void updateContent()
        {
            h1PageTitle.InnerHtml = _eventItem.Data.Title;
            spanCourseSubTitle.InnerHtml = _eventItem.Data.SubTitle;
            divCourseDescription.InnerHtml = _eventItem.Data.Description;

            var mainImage = _eventItem.Data.Image;
            string defaultImageUrl = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.BusinessLogicSettingsEnum.Event_DefaultImageUrl);
            imgMainImage.ImageUrl = mainImage != null ? _eventItem.Data.Image.GetSpecificSizeUrl(BusinessLogic_v3.Modules.EventModule.EventBase.ImageSizingEnum.Thumbnail) : defaultImageUrl;

        }
    }
}