﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
 CodeBehind="testimonials.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Testimonials.testimonials" %>
<%@ Register TagPrefix="CommonControls" TagName="TestimonialsFullPage" Src="~/_ComponentsGeneric/Frontend/UserControls/Testimonials/TestimonialsFullPage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:TestimonialsFullPage runat="server" ID="testimonialsPage" />
</asp:Content>
