﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Classified {
    
    
    public partial class classified {
        
        /// <summary>
        /// divNavigationBreadcrumbs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divNavigationBreadcrumbs;
        
        /// <summary>
        /// navigationBreadcrumbs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Navigation.NavigationBreadcrumbs navigationBreadcrumbs;
        
        /// <summary>
        /// contentPage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage.ContentPage contentPage;
        
        /// <summary>
        /// imgClassified control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.MyImage imgClassified;
        
        /// <summary>
        /// divPrice control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divPrice;
        
        /// <summary>
        /// ctrlClassifiedContactDetails control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Classifieds.ClassifiedContactDetails ctrlClassifiedContactDetails;
        
        /// <summary>
        /// ctrlClassifiedSubmitEnquiry control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Classifieds.ClassifiedSubmitEnquiry ctrlClassifiedSubmitEnquiry;
    }
}
