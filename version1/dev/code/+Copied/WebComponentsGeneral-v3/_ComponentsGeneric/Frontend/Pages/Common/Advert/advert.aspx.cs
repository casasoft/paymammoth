﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Advert
{
    using System;
    using BusinessLogic_v3.Modules.AdvertModule;
    using BusinessLogic_v3.Modules.AdvertSlotModule;
    using Code.Controls.WebControls.Common;
    using Code.Controls.WebControls.Specialized.SWFObject;
    using CS.General_v3;
    using CS.General_v3.Controls.WebControls.Common;
    using BusinessLogic_v3.Modules;

    public partial class advert : CS.WebComponentsGeneralV3.Code.Classes.Pages.BasePageBase
    {
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }
        private static readonly object _lock = new object();
        private void initAdvert()
        {
            bool disableAdvertNote =
                CS.General_v3.Util.QueryStringUtil.GetBoolFromQS(Constants.QS_ADVERT_DISABLE_AD_NOTE_PARAM,
                                                                       false);
            string stringAdIdentifier =
                CS.General_v3.Util.QueryStringUtil.GetStringFromQS(Constants.QS_IDENTIFIER_PARAM);
           

            MyMediaItem mediaItem = new MyMediaItem();
            bool showAdvertSlotNoteText = CS.General_v3.Settings.GetSettingFromDatabase<bool>(CS.General_v3.Enums.SETTINGS_ENUM.AdvertsShowTopRightAdvertTag);
            if (!disableAdvertNote)
            {
                if (showAdvertSlotNoteText)
                {
                    divAdvertSlotNote.Visible = true;
                }
            }
            if (!string.IsNullOrWhiteSpace(stringAdIdentifier))
            {
                IAdvertSlotBase adSlot = Factories.AdvertSlotFactory.GetAdvertSlotByStringIdentifier(stringAdIdentifier);
                    
                if (adSlot == null) throw new Exception("Please specify Advert Slot '" + stringAdIdentifier + "'");
                IAdvertBase ad = adSlot.GetNextAdvertToShow();
                if (ad != null)
                {
                    mediaItem.Functionality.ItemURL = ad.MediaItem.GetSpecificSizeUrl(AdvertBase.ImageSizingEnum.Normal);
                    mediaItem.Functionality.WindowMode =
                        SWFObject.WMODE.Transparent;
                    mediaItem.Functionality.Width = adSlot.Width;
                    mediaItem.Functionality.SwfBackgroundColor = ad.BackgroundColor;
                    mediaItem.Functionality.Height = adSlot.Height;
                    if (!string.IsNullOrWhiteSpace(ad.Link))
                    {
                        mediaItem.Functionality.Href =
                            BusinessLogic_v3.Classes.Routing.AdvertRoute.GetAdvertLinkURL(ad.Title, ad.ID);
                        mediaItem.Functionality.HrefTarget = ad.HrefTarget;
                    }
                    divAdvert.Controls.Add(mediaItem);
                    attachGoogleAnalytics(ad, mediaItem);
                }
            }
        }

        private void attachGoogleAnalytics(IAdvertBase ad, MyMediaItem mediaItem)
        {
            string js = CS.General_v3.Util.GoogleAnalyticsUtil.GetGoogleAnalyticsCodeForImpression("_gaq", ad.ID.ToString(), ad.Title,
                "Adverts", false);
            ltlEventTracking.Text += js + "\r\n";
            string hitJS = CS.General_v3.Util.GoogleAnalyticsUtil.GetGoogleAnalyticsCodeForHit("_gaq", ad.ID.ToString(), ad.Title, "Adverts", false);
            mediaItem.OnClientClick = hitJS;
        }

        private void init()
        {
            initAdvert();
        }
        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get
            {
                return this.htmlTag;
            }
        }
        public override System.Web.UI.HtmlControls.HtmlGenericControl BodyTag
        {
            get
            {
                return this.bodyTag;
            }
        }
    }
}