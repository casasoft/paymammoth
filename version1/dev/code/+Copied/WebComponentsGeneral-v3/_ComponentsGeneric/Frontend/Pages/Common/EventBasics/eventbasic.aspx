﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="eventbasic.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.EventBasics.eventbasic" %>

<%@ Register TagPrefix="CommonControls" TagName="PrettyPhotoImageGallery" Src="~/_ComponentsGeneric/Frontend/UserControls/Gallery/PrettyPhotoImageGallery.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="TestimonialsScroller" Src="~/_ComponentsGeneric/Frontend/UserControls/Testimonials/TestimonialsScroller.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="EventsCalendar" Src="~/_ComponentsGeneric/Frontend/UserControls/Events/EventsCalendar.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="EventDetails" Src="~/_ComponentsGeneric/Frontend/UserControls/Events/EventDetails.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="EventEnquiry" Src="~/_ComponentsGeneric/Frontend/UserControls/Events/EventEnquiry.ascx" %>
<%@ Register TagPrefix="Control" TagName="AddThisControl" Src="~/_ComponentsGeneric/Frontend/UserControls/SocialNetworking/AddThis/AddThisControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <div class="event-page-container">
        <div class="event-page-heading-container clearfix">
            <div class="content-page-heading-content">
                <h1 runat="server" id="h1PageTitle">
                </h1>
                <span class="event-page-heading-subtitle" runat="server" id="spanCourseSubTitle">
                </span>
            </div>
            <div class="content-page-heading-add-this-wrapper" runat="server" id="divHeadingAddThisWrapper"
                visible="false">
                <Control:AddThisControl runat="server" ID="headingAddThis" />
            </div>
        </div>
        <div class="event-page-description-container clearfix">
            <div class="event-page-description-content html-container" runat="server" id="divCourseDescription">
            </div>
            <div class="event-page-description-image">
                <CSControls:MyImage runat="server" ID="imgMainImage">
                </CSControls:MyImage>
            </div>
        </div>
        <div class="event-page-media-gallery-container" runat="server" id="divMediaGallery">
            <CommonControls:PrettyPhotoImageGallery runat="server" ID="eventGallery" />
        </div>
    </div>
</asp:Content>