﻿using System;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ArticleModule;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using CS.WebComponentsGeneralV3.Code.Util;
using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Classes.Routing;
namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Errors
{

    public partial class error : BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage>
    {
        private void loadPage()
        {
            bool ok = false;

            BusinessLogic_v3.Enums.ERROR_PAGE_TYPE? errorPageType = ErrorsRoute.GetErrorIdentifierFromRoute();
            ArticleBaseFrontend article = null;
            if (errorPageType.HasValue)
            {
                switch (errorPageType)
                {
                    case Enums.ERROR_PAGE_TYPE.Generic:
                        article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Error_Generic).ToFrontendBase();
                        break;
                    case Enums.ERROR_PAGE_TYPE.PageNotFound:
                        article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Error_PageNotFound).ToFrontendBase();
                        break;
                }
            }

            if (article != null)
            {
                contentPage.Functionality.ContentPage = article;

                var cpContact =
                    BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                        BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.ContactUs);
                contentPage.Functionality.ContentTagReplacementValues[Enums.TAG_ERROR_PAGE_CONTACT_US_LINK] =
                    cpContact.GetUrl();


                ok = true;
            }
            if (!ok)
            {
                //Show a default error page
                throw new Exception("Error page of identifier '" + CS.General_v3.Util.RoutingUtil.GetRouteValue<string>(BusinessLogic_v3.Constants.Tokens.ROUTE_PARAM_ID) + "' not found in DB");
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            loadPage();

            base.OnLoad(e);
        }

        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get { return this.Master.HtmlTag; }
        }
    }
}