﻿using CS.General_v3.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.MasterPages
{
    using System;
    using System.Web.UI.HtmlControls;
    using CMS.UserControls.EditItem;
    using Code.Classes.Pages;
    using CS.General_v3;
    using CS.WebComponentsGeneralV3.Code.Classes.GoogleAnalytics;
    using BusinessLogic_v3.Constants;
    using BusinessLogic_v3.Modules;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using BusinessLogic_v3.Modules.ArticleModule;

    public partial class Common : BaseCommonMasterPage
    {
        public Common()
        {
            

        }
        
        private void initJS()
        {
            if (CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                placeHolderJSNonLocalhost.Parent.Controls.Remove(placeHolderJSNonLocalhost);
            }
            else
            {
                placeHolderJSLocalhost.Parent.Controls.Remove(placeHolderJSLocalhost);
            }
        }

        protected override void OnLoad(EventArgs e)
        {

            initJS();

            initGenericMessage();

            initGoogleAnalytics();

            initOfflineModeCheck();

            base.OnLoad(e);

        }

        private void initOfflineModeCheck()
        {
            bool isOffline =
                Factories.SettingFactory.GetSettingValue<bool>(Enums.SETTINGS_ENUM.MakeSystemOfflineCompatible);
            if (isOffline)
            {
                placeHolderOnline.Visible = false;
                placeholderGoogleAnalytics.Visible = false;
                placeHolderOffline.Visible = true;
            }
        }

        private void initGenericMessage()
        {
            IsGenericMessageEnabled = BusinessLogic_v3.Modules.ModuleSettings.Generic.IsGenericMessageEnabled;
            if (IsGenericMessageEnabled)
            {
                this.Page.Functionality.DisplayGenericMessage += new Code.Classes.Pages.BasePageBase.MessageHandler(Functionality_DisplayGenericMessage);
            }
        }

        void Functionality_DisplayGenericMessage(ContentTextBaseFrontend msg, General_v3.Enums.STATUS_MSG_TYPE messageType)
        {
            divGenericMsgContainer.Visible = true;
            MyContentText.AttachContentTextWithControl(divGenericMsgContainerText, msg);
            divGenericMsgContainerText.InnerText = msg.GetContent();
            divGenericMsgContainer.Attributes["class"] += " " + messageType.ToString().ToLower() + "-notification";
        }

        private void initGoogleAnalytics()
        {
            GoogleAnalyticsInfo ga = GoogleAnalyticsManager.GetInfo();
            if (!string.IsNullOrEmpty(ga.GoogleAnalyticsID))
            {
                string jsCode = "_gaq.push(['_setAccount', '" + ga.GoogleAnalyticsID + "']);\r\n";
                if (ga.GoogleAnalyticsType.HasValue)
                {
                    if (ga.GoogleAnalyticsType.Value != Enums.GOOGLE_ANALYTICS_TYPE.SingleDomain)
                    {
                        jsCode += "_gaq.push(['_setDomainName', '" + ga.GoogleAnalyticsRootDomain + "']);\r\n";
                    }
                    if (ga.GoogleAnalyticsType.Value == Enums.GOOGLE_ANALYTICS_TYPE.MultipleTLD)
                    {
                        jsCode += "_gaq.push(['_setAllowLinker', true]);\r\n";
                    }
                }
                placeholderGoogleAnalytics.Visible = true;
                ltlGoogleAnalyticsUAID.Text = jsCode;
            }

        }

        public override HtmlGenericControl HtmlTag
        {
            get { return this.htmlTag; }
        }

        public override HtmlGenericControl BodyTag
        {
            get { return bodyTag; }
        }
        private void initHtmlIECompatibility()
        {

            string langCode = CS.General_v3.Enums.ISO_ENUMS.Language_ISO639_2letter_ToCode(BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCurrentLanguageCode());


            /*<!--[if lt IE 7]> <html class="<asp:Literal ID='htmlTagTestClass' runat='server'></asp:Literal> ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="<asp:Literal id='htmlTag2Class' runat='server' /> ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="<asp:Literal id='htmlTag3Class' runat='server' /> ie8 oldie" lang="en"> <![endif]-->*/

            /*<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->*/

            string htmlCSS = htmlTag.Attributes["class"];

            string html =
                @"<!--[if lt IE 7]> <html class='" + htmlCSS + @" lt-ie9 lt-ie8 lt-ie7' lang='" + langCode + @"' xmlns:fb='http://ogp.me/ns/fb#' xmlns:og='http://ogp.me/ns#' xmlns:addthis='http://www.addthis.com/help/api-spec'> <![endif]-->
<!--[if IE 7]>    <html class='" + htmlCSS + @" lt-ie9 lt-ie8' lang='" + langCode + @"' xmlns:fb='http://ogp.me/ns/fb#' xmlns:og='http://ogp.me/ns#' xmlns:addthis='http://www.addthis.com/help/api-spec' > <![endif]-->
<!--[if IE 8]>    <html class='" + htmlCSS + @" lt-ie9' lang='" + langCode + @"' xmlns:fb='http://ogp.me/ns/fb#' xmlns:og='http://ogp.me/ns#' xmlns:addthis='http://www.addthis.com/help/api-spec'> <![endif]-->";
            htmlTag.Attributes["lang"] = langCode;
            ltlHtmlIECompatibilityTags.Text = html;
        }
        protected override void OnPreRender(EventArgs e)
        {

            initHtmlIECompatibility();
            base.OnPreRender(e);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);
        }

       
        protected override CmsEditItemLink _cmsEditItemLink
        {
            get { return cmsEditItem; }
        }
    }
}