﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Modules;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member
{

    public partial class credit_statement : BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage>
    {
        public credit_statement()
        {
            this.Functionality.RequiresMemberAuthentication = true;
        }

        private ArticleBaseFrontend _article;
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initLoadPage();
        }

        private void initLoadPage()
        {
            _article = Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_CreditStatement).ToFrontendBase();
            if (_article != null)
            {
                contentPage.Functionality.ContentPage = _article;
                creditStatementListing.Functionality.Article = _article;
            }
        }
    }
}