﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Modules;
using CS.WebComponentsGeneralV3.Code.Classes.URL;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member
{
    public partial class order_history : BasePage<BaseMainMasterPage>
    {
        public order_history()
        {
            this.Functionality.RequiresMemberAuthentication = true;
        }
        
        private URLParserOrderHistory _url;
        private ArticleBaseFrontend _article;
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadPage();
            initUrl();
            initListing();
        }

        private void initUrl()
        {
            _url = new URLParserOrderHistory(_article);
        }

        private void initListing()
        {
            var member = BusinessLogic_v3.Modules.Factories.MemberFactory.GetLoggedInUserForCurrentSession().ToFrontendBase();
            if (member != null)
            {
                orderHistoryListing.Functionality.Member = member;
            }
            orderHistoryListing.Functionality.Url = _url;
        }

        private void loadPage()
        {
            _article = Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.Members_OrderHistory).ToFrontendBase();
            if (_article != null)
            {
                contentPage.Functionality.ContentPage = _article;
            }
        }
    }
}