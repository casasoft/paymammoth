﻿using System;
using System.Collections.Generic;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Modules.ArticleCommentModule;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Classes.Routing;
using CS.WebComponentsGeneralV3.Code.Util;
using System.Linq;
using CS.General_v3.Classes.Interfaces.BlogPosts;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.General_v3;
using BusinessLogic_v3.Classes.Text;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Article
{

    public partial class article : CommonArticleBasePage
    {

        protected override UserControls.ContentPage.ContentPage _contentPage
        {
            get { return contentPage; }
        }

        protected override UserControls.ContentPage.PreviousNextArticle _previousNextEntry
        {
            get { return previousNextEntry; }
        }

        protected override UserControls.ContentPage.ArticleComments _contentPageComments
        {
            get { return contentPageComments; }
        }
    }
}