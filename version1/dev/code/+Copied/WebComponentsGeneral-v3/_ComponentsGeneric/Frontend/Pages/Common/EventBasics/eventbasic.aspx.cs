﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3;
using BusinessLogic_v3.Constants;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.EventBasicModule;
using BusinessLogic_v3.Frontend.ArticleModule;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.Routing;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Modules;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.EventBasics
{
    public partial class eventbasic : BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage>
    {
        private EventBasicBaseFrontend _eventBasicItem;
        private ArticleBaseFrontend _article;

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadEvent();

            if (_eventBasicItem != null)
            {
                updateContent();
            }
            else if (PageUtil.IsLocalhost())
            {
                throw new Exception("Event cannot be null");
            }
            else
            {
                PageUtil.RedirectPage(ErrorsRoute.GetPageNotFoundErrorURL());
            }
        }

        private void loadEvent()
        {
            _article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.Event).ToFrontendBase();
            this.Functionality.ContentPage = _article;

            var eventId = CS.General_v3.Util.RoutingUtil.GetRouteValueAsLongNullable(Tokens.ROUTE_PARAM_ID);
            if (eventId.HasValue)
            {
                _eventBasicItem = BusinessLogic_v3.Modules.Factories.EventBasicFactory.GetByPrimaryKey(eventId.Value).ToFrontendBase();
                if (_eventBasicItem != null)
                {

                    var mediaItems = _eventBasicItem.Data.GetMediaItems().ToFrontendBaseList();
                    if (mediaItems != null && mediaItems.Count() > 0)
                    {
                        eventGallery.Functionality.Gallery = mediaItems;
                    }
                    else
                    {
                        CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divMediaGallery);
                    }

                    var showAddThisInHeading = ModuleSettings.Articles.ShowAddThisInHeading;
                    if (showAddThisInHeading)
                    {
                        headingAddThis.Functionality.URL = _eventBasicItem.GetUrl();
                        headingAddThis.Functionality.Style = General_v3.Enums.ADDTHIS_STYLE.DefaultStyleSmall;
                        divHeadingAddThisWrapper.Visible = true;
                    }
                    else
                    {
                        CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(divHeadingAddThisWrapper);
                    }
                }
                else
                {
                    CS.General_v3.Util.PageUtil.RedirectPage(ErrorsRoute.GetPageNotFoundErrorURL());
                }
            }
        }

        private void updateContent()
        {
            h1PageTitle.InnerHtml = _eventBasicItem.Data.Title;
            divCourseDescription.InnerHtml = _eventBasicItem.Data.DescriptionHtml;

            var mainImage = _eventBasicItem.Data.Image;
            string defaultImageUrl = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.BusinessLogicSettingsEnum.Event_DefaultImageUrl);
            imgMainImage.ImageUrl = mainImage != null
                                        ? mainImage.GetSpecificSizeUrl(
                                            BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase.ImageSizingEnum.
                                                Normal)
                                        : defaultImageUrl;
        }
    }
}