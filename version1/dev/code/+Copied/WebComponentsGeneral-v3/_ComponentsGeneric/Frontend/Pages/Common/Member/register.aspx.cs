﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Modules.MemberModule.SessionManager;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Util;
using CS.General_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member
{

    public partial class register : BasePage<BaseMainMasterPage>
    {
        protected override void OnLoad(EventArgs e)
        {
            loadPage();
            initProfileFields();
            base.OnLoad(e);
        }

        private void initProfileFields()
        {
            registerFields.Functionality.DisplayType =
                UserControls.Members.Profile.ProfileFields.PROFILE_FIELDS_DISPLAY_TYPE.Register;
        }

        private void loadPage()
        {
            if (MemberSessionLoginManager.Instance.GetLoggedInUser() == null)
            {
                ArticleBaseFrontend article =
                    BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                        BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_Register).ToFrontendBase();

                if (article != null)
                {
                    contentPage.Functionality.ContentPage = article;
                }
            }
            else
            {
                this.Functionality.ShowErrorMessageAndRedirectToPage(
                    Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_General_YouMustNotBeLoggedInToAccessThisPage).ToFrontendBase(), "/");
            }
        }

    }
}