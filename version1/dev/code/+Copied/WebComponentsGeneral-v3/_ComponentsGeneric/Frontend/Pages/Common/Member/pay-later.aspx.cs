﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Modules;
using CS.WebComponentsGeneralV3.Code.Classes.URL;
using BusinessLogic_v3.Modules.OrderModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member
{
    public partial class payLater : BasePage<BaseMainMasterPage>
    {
        public payLater()
        {
            this.Functionality.RequiresMemberAuthentication = true;
        }

        public long Querystring_InvoiceId
        {
            get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long>(BusinessLogic_v3.Constants.ParameterNames.ID); }
        }
       


        private ArticleBaseFrontend _article;
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadInvoice();
            loadPage();
        }
        

        private OrderBase _order = null;
        private void loadInvoice()
        {
            if (Querystring_InvoiceId > 0)
            {
                _order = OrderBase.Factory.GetByPrimaryKey(Querystring_InvoiceId);
            }
            
        }

        private void loadPage()
        {
            _article = Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.Members_Order_OrderAndPayLater).ToFrontendBase();
            if (_article != null)
            {
                contentPage.Functionality.ContentPage = _article;
                var order = _order.ToFrontendBase();
                contentPage.Functionality.ContentTagReplacementValues[BusinessLogic_v3.Constants.Tokens.INVOICE_NUMBER] = _order.Reference;
                contentPage.Functionality.ContentTagReplacementValues[BusinessLogic_v3.Constants.Tokens.INVOICE_LINK] = order.GetURL();
                contentPage.Functionality.ContentTagReplacementValues[BusinessLogic_v3.Constants.Tokens.INVOICE_PAYMENT_URL] = order.GetPayOnlineUrl();
                

            }
        }
    }
}