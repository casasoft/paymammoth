﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeBehind="success.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Purchase.success" %>
<%@ Register TagPrefix="CommonControls" TagName="PayMammothPurchaseSuccess" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Purchase/PayMammothPurchaseSuccess.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:PayMammothPurchaseSuccess runat="server" ID="paymammothPurchaseSuccess" />
</asp:Content>
