﻿using System;
using System.Web.UI;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.CKEditor.v1
{
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.FileBrowser.Pages;

    public partial class fileBrowserAspx : FileBrowserPage
    {
        private void initSettings()
        {
            fileBrowser.Functionality.RootFolder = "/uploads/filebrowser/";
        }
        protected override void OnLoad(EventArgs e)
        {
            initSettings();
            this.Functionality.DoNotThrowErrorIfContentPageIsNull = true;
            base.OnLoad(e);
        }

        protected override Control mainDiv
        {
            get
            {
                return divMain;
            }
        }
    }
}

