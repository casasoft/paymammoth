﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Frontend.CategoryModule;
using BusinessLogic_v3.Modules.ProductModule;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.URL;
using CS.WebComponentsGeneralV3.Code.Classes.URL;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Util;
using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Classes.Routing;
namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Categories
{
    using Code.Classes.Pages;

    public partial class search : BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage>
    {
        private URLParserDefaultShopParams _urlParser;
        private CategoryBaseFrontend _category;
        private string _keywords;
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initUrlParser();
            initCategory();
            initKeywords();
            initNavigationBreadcrumbs();
            initListing();
            initLoadPage();
            updateTexts();
            initSEO();
        }

        private void initLoadPage()
        {
            var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.Search).ToFrontendBase();
            this.Functionality.ContentPage = article;
            searchFullPage.Functionality.ContentPage = article;

        }

        private void initNavigationBreadcrumbs()
        {
            if(BusinessLogic_v3.Modules.ModuleSettings.Generic.ShowBreadcrumbsInGeneralListing)
            {
                if (_category != null)
                {
                    navigationBreadcrumbs.Functionality.AddFromHierarchy(_category, true);
                }
                else
                {
                    URLClass url = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation();
                    navigationBreadcrumbs.Functionality.AddItem(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Search).GetContent(), url.GetURL());
                }
                navigationBreadcrumbs.Functionality.AddItem(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.General_Home).GetContent(), "/", true);
                divNavigationBreadcrumbs.Visible = true;
            }
        }

        private void updateTexts()
        {
            if(_category == null && string.IsNullOrEmpty(_keywords))
            {
                //UPDATE TO CONTENT TEXT
                this.Functionality.ShowErrorMessageAndRedirectToPage(BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Search_NoSearchCriteriaFound).ToFrontendBase(), "/");
            }
            else
            {
                string title = (string.IsNullOrEmpty(_keywords) && _category != null) ? _category.Data.TitlePlural : null;

                if(string.IsNullOrEmpty(title))
                {
                    var cntTxt = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Search_KeywordsTitle).ToFrontendBase();
                    var dict = cntTxt.CreateTokenReplacer();
                    
                    dict.Add(BusinessLogic_v3.Constants.Tokens.SEARCH_KEYWORDS, _keywords);
                    searchFullPage.Functionality.PageTitleContentText = cntTxt;
                    searchFullPage.Functionality.PageTitleContentTextReplacementTags = dict;
                    this.Functionality.UpdateTitle(_keywords);
                }
                else
                {
                    MyDiv divHeadingWrapper = new MyDiv();
                    divHeadingWrapper.CssManager.AddClass("listing-heading-container clearfix");
                    if(!string.IsNullOrEmpty(_category.Data.Identifier))
                    {
                        MySpan span = new MySpan();
                        var productIconCssClass = "product-category-" + _category.Data.Identifier;
                        span.CssManager.AddClass("product-category-icon", productIconCssClass);
                        divHeadingWrapper.Controls.Add(span);
                    }
                    MyHeading h1 = new MyHeading(1, title);
                    divHeadingWrapper.Controls.Add(h1);
                    this.searchFullPage.Functionality.PageTitleCtrl = divHeadingWrapper;
                    this.Functionality.UpdateTitle(title);
                }
            }
        }

        private void initKeywords()
        {
            _keywords = CS.General_v3.Util.RoutingUtil.ConvertRouteValueToString(_urlParser.Keywords.GetValueAsString(false));
            if (!string.IsNullOrEmpty(_keywords))
            {
                jsSearchHighlightKeywords.Text = _keywords;
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(placeholderSearchHighlightJS);
            }
        }

        private void initSEO()
        {
            //var addCategoryToTitle = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_FullPageAddParentCategoryToMetaTitleForSEO);
            if (_category != null)
            {
                _category.UpdateSEOForPage(this);
            }
        }
        
        private void initListing()
        {
            bool showPagingBar = false;
            SearchPagedResults<IProductBase> list = null;
            if (!IsPostBack) //this 
            {
                if (_category != null)
                {
                    list = BusinessLogic_v3.Modules.Factories.ProductFactory.GetItemsByCategory(_category.Data, _urlParser.PageNo.GetValue(), ((int)_urlParser.ShowAmt.GetValue()), _urlParser.SortBy.GetValue());
                }
                else if (!string.IsNullOrEmpty(_keywords))
                {
                    list = BusinessLogic_v3.Modules.Factories.ProductFactory.GetItemsByKeywords(_keywords, _urlParser.PageNo.GetValue(), ((int) _urlParser.ShowAmt.GetValue()), _urlParser.SortBy.GetValue());

                }
            }
            if (list != null && list.Count() > 0)
            {
                searchFullPage.Functionality.Items = list.ToFrontendBaseList();
                searchFullPage.Functionality.TotalResults = list.TotalResults;
            }
            
        }
        
        private void initCategory()
        {
            var id = _urlParser.CategoryID.GetValue();
            if (id.HasValue)
            {
                CategoryBaseFrontend category = BusinessLogic_v3.Modules.Factories.CategoryFactory.GetByPrimaryKey(id.Value).ToFrontendBase();
                
                if (category != null)
                {
                    _category = category;
                    this.Functionality.SetEditInCmsButtonObject(_category.Data);
                }
            }
        }

        private void initUrlParser()
        {
            _urlParser = new URLParserDefaultShopParams();
            searchFullPage.Functionality.UrlParserWithPaging = _urlParser;
        }
    }
}
