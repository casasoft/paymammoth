﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Core.master"
    AutoEventWireup="true" CodeBehind="invoice.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Orders.invoice" %>

<%@ Register Src="/_ComponentsGeneric/Frontend/UserControls/Orders/Invoice/GenericTabularDataInvoice.ascx"
    TagName="GenericInvoice" TagPrefix="CommonControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="coreHeadPlaceholder" runat="server">
    <link href="/_common/static/css/invoice/GenericInvoice.css" rel="stylesheet" type="text/css" />
    <link href="/css/invoice.css"
        rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="coreContentPlaceHolder" runat="server">
    <CommonControls:GenericInvoice id="controlInvoice" runat="server" />
</asp:Content>
