﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Other
{
    using System;
    using log4net;

    public partial class keepAlive : CS.WebComponentsGeneralV3.Code.Classes.Pages.BasePageBase
    {
        private readonly static ILog _log = LogManager.GetLogger(typeof(keepAlive));

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_log.IsDebugEnabled) _log.Debug("[Application Keep Alive]");
        }
        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get
            {
                return new System.Web.UI.HtmlControls.HtmlGenericControl("html");
            }
        }
        public override System.Web.UI.HtmlControls.HtmlGenericControl BodyTag
        {
            get
            {
                return new System.Web.UI.HtmlControls.HtmlGenericControl("body");
            }
        }
    }
}