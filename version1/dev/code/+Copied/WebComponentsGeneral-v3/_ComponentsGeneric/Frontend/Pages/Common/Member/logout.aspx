﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
     CodeBehind="logout.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member.logout" %>

<%@ Register TagPrefix="CommonControls" TagName="LogoutControl" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/Logout/LogoutControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:LogoutControl ID="logoutControl" runat="server" />
</asp:Content>
