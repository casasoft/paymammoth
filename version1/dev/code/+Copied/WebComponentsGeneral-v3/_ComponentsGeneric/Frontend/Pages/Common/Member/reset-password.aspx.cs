﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Classes.URL;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.ArticleModule;
using CS.WebComponentsGeneralV3.Code.Util;
using BusinessLogic_v3.Frontend.ContentTextModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member
{

    public partial class reset_password : BasePage<BaseMainMasterPage>
    {
        private ArticleBaseFrontend _article;
        private string PARAM_ID = BusinessLogic_v3.Constants.ParameterNames.ID;
        private string PARAM_CODE = BusinessLogic_v3.Constants.ParameterNames.Code;
        public class FUNCTIONALITY
        {
            private reset_password _control;

            public FUNCTIONALITY(reset_password control)
            {
                _control = control;
            }
            public ContentTextBaseFrontend InvalidCode { get; set; }
        }

        public FUNCTIONALITY Functionality { get; private set; }
        public reset_password()
        {
            this.Functionality = createFunctionality();
            this.Functionality.InvalidCode =
                BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ResetPassword_InvalidCodeText).ToFrontendBase();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

 
        private void init()
        {
            initLoadPage();
            
            initResetPassword();
        }

        private void initLoadPage()
        {
            _article =
                BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                    BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_ResetPassword).ToFrontendBase();
            if (_article != null)
            {
                contentPage.Functionality.ContentPage = _article;
            }
        }

        private void initResetPassword()
        {
            URLClass urlClass = new URLClass();
            var id = urlClass.QS.Get(PARAM_ID);
            var code = urlClass.QS.Get(PARAM_CODE);

            if (id != null && code != null)
            {
                long idLong;
                if (long.TryParse(id, out idLong))
                {
                    var member =
                        BusinessLogic_v3.Modules.Factories.MemberFactory.GetByPrimaryKey(idLong).ToFrontendBase();
                    if (member != null)
                    {
                        if (member.Data.ForgottenPassCode == code)
                        {
                            resetPasswordFields.Functionality.Member = member;
                            resetPasswordFields.Functionality.Code = code;
                        }
                        else
                        {
                            base.Functionality.ShowErrorMessageAndRedirectToPage(this.Functionality.InvalidCode, "/");
                        }
                    }
                    else
                    {
                        PageUtil.RedirectPage(BusinessLogic_v3.Classes.Routing.ErrorsRoute.GetPageNotFoundErrorURL());
                    }
                }
            }
            else
            {
                PageUtil.RedirectPage(BusinessLogic_v3.Classes.Routing.ErrorsRoute.GetPageNotFoundErrorURL());
            }
        }


    }
}