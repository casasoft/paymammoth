﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BusinessLogic_v3;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Modules;
using CS.WebComponentsGeneralV3.Code.Classes.URL;
using BusinessLogic_v3.Modules.OrderModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member
{
    public partial class pay_online : BasePage<BaseMainMasterPage>
    {
        public pay_online()
        {
            this.Functionality.RequiresMemberAuthentication = true;
        }

        public long Querystring_InvoiceId
        {
            get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long>(BusinessLogic_v3.Constants.ParameterNames.ID); }
        }
        public string Querystring_InvoiceGuid
        {
            get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<string>(BusinessLogic_v3.Constants.ParameterNames.GUID); }
        }


        private ArticleBaseFrontend _article;
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            loadPage();
           
            loadInvoice();
            checkInvoice();
        }

       

        private void checkInvoice()
        {
            if (_order == null)
            {

                //order does not exist / guid not valid
                var ct = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Orders_PayOnline_OrderDoesNotExist).ToFrontendBase();
                MyContentText.AttachContentTextWithControl(divMessage, ct);
            }
            else
            {
                if (_order.IsPaid())
                {
                    //order already paid
                    var ct = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Orders_PayOnline_OrderAlreadyPaid).ToFrontendBase();
                    MyContentText.AttachContentTextWithControl(divMessage, ct);
                }
                else
                {
                    PayMammothConnectorBL.v3.Util.FrontendGeneral.GeneratePayMammothRequestFromOrderAndRedirect(_order);
                }
            }
        }

        private OrderBase _order = null;
        private void loadInvoice()
        {
            if (Querystring_InvoiceId > 0)
            {
                _order = OrderBase.Factory.GetByPrimaryKey(Querystring_InvoiceId);
                if (_order != null && !_order.VerifyGUID(Querystring_InvoiceGuid)) //if the guid is not verified correctly, reset to null
                {
                    _order = null;
                }
            }
            
        }

        private void loadPage()
        {
            _article = Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.Members_Order_PayOnline).ToFrontendBase();
            if (_article != null)
            {
                contentPage.Functionality.ContentPage = _article;
            }
        }
    }
}