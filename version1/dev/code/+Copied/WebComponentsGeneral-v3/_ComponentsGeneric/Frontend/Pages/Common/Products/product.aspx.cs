﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.Routing;
using CS.General_v3;
using BusinessLogic_v3.Modules.OrderModule;
using CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.Orders.Information;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ProductModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Products
{
    public partial class product : BasePage<BaseMainMasterPage>
    {
        private ProductBaseFrontend _product;
        protected void Page_Load(object sender, EventArgs e)
        {
            init();
        }

        private void init()
        {
            initProduct();
        }

        private void initProduct()
        {
            long? id = ProductRoute.GetProductIDFromRouteData();
            if (id.HasValue)
            {
                ProductBaseFrontend product = BusinessLogic_v3.Modules.Factories.ProductFactory.GetByPrimaryKey(id.Value).ToFrontendBase();
                if (product != null)
                {
                    productFullPage.Functionality.Product = product;
                }
            }
            else
            {
                CS.General_v3.Util.PageUtil.RedirectPage(ErrorsRoute.GetPageNotFoundErrorURL());
            }
        }
    }
}