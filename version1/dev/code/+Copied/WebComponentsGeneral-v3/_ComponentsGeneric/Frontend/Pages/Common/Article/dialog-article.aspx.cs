﻿using System;
using System.Collections.Generic;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Modules.ArticleCommentModule;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Classes.Routing;
using CS.WebComponentsGeneralV3.Code.Util;
using System.Linq;
using CS.General_v3.Classes.Interfaces.BlogPosts;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.General_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Article
{

    public partial class dialogArticle : BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage>
    {
        private void loadPage()
        {
            bool ok = false;
            var cpID = BusinessLogic_v3.Classes.Routing.ArticlesRoute.GetIDFromRouteData();
            if (cpID.HasValue)
            {
                ArticleBaseFrontend article =
                    BusinessLogic_v3.Modules.Factories.ArticleFactory.GetByPrimaryKey(cpID.Value).ToFrontendBase();
                if (article != null)
                {
                    contentPage.Functionality.ContentPage = article;
                    contentPage.Functionality.IsTitleVisible = false;
                    contentPage.Functionality.DoNotShowNavigationBreadcrumbs = article.Data.DoNotShowNavigationBreadcrumbs.GetValueOrDefault(true);
                    contentPage.Functionality.DoNotShowFooter = article.Data.DoNotShowFooter.GetValueOrDefault(true);
                    this.Functionality.ContentPage = article;
                    ok = true;
                }
            }
            if (!ok)
            {
                CS.General_v3.Util.Log4NetUtil.LogMsgByObject(this, General_v3.Enums.LOG4NET_MSG_TYPE.Error, "Page with ID '" + cpID + "' not found in DB", null);
                //Show a default error page
                string pageNotFoundURL = ErrorsRoute.GetPageNotFoundErrorURL();
                PageUtil.RedirectPage(pageNotFoundURL);

            }
        }


        protected override void OnLoad(EventArgs e)
        {
            loadPage();
            base.OnLoad(e);
        }
    }
}