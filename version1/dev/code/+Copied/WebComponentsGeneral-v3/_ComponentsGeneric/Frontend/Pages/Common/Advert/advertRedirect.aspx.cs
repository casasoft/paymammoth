﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Advert
{
    using System;
    using BusinessLogic_v3.Modules.AdvertModule;
    using BusinessLogic_v3.Classes.Pages;
    using General_v3.Controls.WebControls.Common;

    public partial class advertRedirect : CS.WebComponentsGeneralV3.Code.Classes.Pages.BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseCommonMasterPage>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            redirectLink();
        }

        private void redirectLink()
        {
            long? id = BusinessLogic_v3.Classes.Routing.AdvertRoute.GetIdFromCurrentRoute();
            if (id != null)
            {
                AdvertBase advert = AdvertBaseFactory.Instance.GetByPrimaryKey(id.Value);
                if (advert != null)
                {
                    string linkUrl = advert.Link;
                    advert.IncrementClickCounter();
                    JSRedirectControllerParameters p = new JSRedirectControllerParameters();
                    p.url = linkUrl;
                    JSRedirectControllerParameters.InitJSinPage(p);
                }
            }
        }

        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get { return this.Master.HtmlTag; }
        }
    }
}