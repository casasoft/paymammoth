﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Other
{
    /// <summary>
    /// Summary description for keepAlive1
    /// </summary>
    public class keepAlive1 : CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers.BaseHandler, IRequiresSessionState
    {

        protected override void processRequest(HttpContext ctx)
        {
            ctx.Response.Clear();
            ctx.Response.Write("KEEP-ALIVE");
            ctx.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}