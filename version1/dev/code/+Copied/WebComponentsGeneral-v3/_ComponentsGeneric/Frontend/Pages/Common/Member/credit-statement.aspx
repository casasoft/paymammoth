﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
     CodeBehind="credit-statement.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member.credit_statement" %>

<%@ Register TagPrefix="CommonControls" TagName="ContentPage" Src="~/_ComponentsGeneric/Frontend/UserControls/ContentPage/ContentPage.ascx" %>
<%@ Register TagPrefix="CommonControls" TagName="CreditStatementListing" Src="~/_ComponentsGeneric/Frontend/UserControls/Members/CreditStatement/CreditStatementListing.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:ContentPage ID="contentPage" runat="server" />
    <CommonControls:CreditStatementListing ID="creditStatementListing" runat="server" />
</asp:Content>
