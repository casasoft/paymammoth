﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Modules.MemberModule;
using CS.General_v3.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member
{

    public partial class activate : BasePage<BaseMainMasterPage>
    {
        private ArticleBaseFrontend _article;

        protected override void OnLoad(EventArgs e)
        {
            
            init();
            base.OnLoad(e);
        }
        private void init()
        {
            loadPage();
            tryToActivateMemberFromQs();
         
        }

       

        public string GetActivationCodeFromQs()
        {
            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(BusinessLogic_v3.Modules.ModuleSettings.Members.QS_PARAM_ACTIVATION_CODE);
        }


        private void tryToActivateMemberFromQs()
        {
            
            string code = GetActivationCodeFromQs();
            if (!string.IsNullOrWhiteSpace(code))
            {
                IMemberBase m = BusinessLogic_v3.Modules.Factories.MemberFactory.ActivateMemberByCode(code);
                if (m != null)
                {//this means that user is activated
                    
                    var cp =BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_ActivateAccount_Success).ToFrontendBase();
                    var url = cp.GetUrl();
                    PageUtil.RedirectPage(url);
                    
                }
            }
        }
     


        private void loadPage()
        {
            _article =BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_ActivateAccount).ToFrontendBase();

            

            if (_article != null)
            {
               
                contentPage.Functionality.ContentPage = _article;
            }
        }
        
    }
}