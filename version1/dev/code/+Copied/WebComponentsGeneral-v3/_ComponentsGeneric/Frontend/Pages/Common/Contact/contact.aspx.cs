﻿using System;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ArticleModule;
using CS.WebComponentsGeneralV3.Code.Util;
using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Classes.Routing;
namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Contact
{
    using Code.Classes.Pages;

    public partial class contact : BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage>
    {
        private void loadPage()
        {
            ArticleBaseFrontend article =
                BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                    BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.ContactUs).ToFrontendBase();

            if (article != null)
            {
                contentPage.Functionality.ContentPage = article;
                this.Functionality.ContentPage = article;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            loadPage();
            initOnlineEnquiryForm();
            base.OnLoad(e);
        }

        private void initOnlineEnquiryForm()
        {
            contactUsForm.Visible = BusinessLogic_v3.Modules.ModuleSettings.Contact.IsOnlineEnquiryFormEnabled;
            
        }

    }
}