﻿using System;
using System.Collections.Generic;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Modules.ArticleCommentModule;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Classes.Routing;
using CS.WebComponentsGeneralV3.Code.Util;
using System.Linq;
using CS.General_v3.Classes.Interfaces.BlogPosts;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.General_v3;
using BusinessLogic_v3.Classes.Text;
using CS.WebComponentsGeneralV3.Code.Classes.URL;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.News
{

    public partial class news : BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage>
    {
       private URLParserGeneralListing _urlParserGeneralListing;
        private ArticleBaseFrontend _article;
       protected override void OnLoad(EventArgs e)
       {
            init();
            base.OnLoad(e);
        }

       private void init()
       {
           loadContent();
       }

       private void initUrlParser()
       {
           _urlParserGeneralListing = new URLParserGeneralListing(_article);
       }

       private void loadContent()
       {
           loadArticle();
            if(_article != null)
            {
                initUrlParser();
                loadListing();
            }
       }

       private void loadListing()
       {
           newsListing.Functionality.UrlParserWithPaging = _urlParserGeneralListing;
           newsListing.Functionality.ContentPage = _article;
           int totalResults = 0;
           var childArticles = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticlesForParent(_article.Data, _urlParserGeneralListing.PageNo.GetValue(), ((int)_urlParserGeneralListing.ShowAmt.GetValue()), _urlParserGeneralListing.SortBy.GetValue(), out totalResults).ToFrontendBaseList();
           newsListing.Functionality.Items = childArticles;
           newsListing.Functionality.TotalResults = totalResults;
       }

       private void loadArticle()
       {
            _article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.NewsAndUpdates).ToFrontendBase();
       }
    }
}