﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="delivery-details.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.ShoppingCart.delivery_details" %>

<%@ Register TagPrefix="CommonControls" TagName="DeliveryDetails" Src="~/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/DeliveryDetails.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainHeadPlaceholder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <CommonControls:DeliveryDetails ID="ucDeliveryDetails" runat="server" />
</asp:Content>
