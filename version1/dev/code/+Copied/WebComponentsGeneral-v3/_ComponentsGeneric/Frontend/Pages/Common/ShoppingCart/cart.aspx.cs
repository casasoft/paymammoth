﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.ArticleModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Frontend.MemberModule;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.ShoppingCart
{
    public partial class cart : BasePage<BaseMainMasterPage>
    {
        CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.ShoppingCart _cart;

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initCart();
            loadPage();
            loadCart();
        }

        private void loadCart()
        {
            shoppingCart.Functionality.CartType = UserControls.ShoppingCart.ShoppingCart.SHOPPING_CART_TYPE.Cart;
        }
        private void initCart()
        {
            shoppingCartPlaceholder.Controls.Clear();
            //string shoppingCartUserControlPath = "/Controls/ShoppingCart/ShoppingCart.ascx";
            string shoppingCartUserControlPath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_ShoppingCart_CartUserControlPath); 
            //UserControlController.SetControl("/Controls/ShoppingCart/ShoppingCartJadaJade.ascx");
            //_cart = UserControlController.GetControl("/Controls/ShoppingCart/ShoppingCartJadaJade.ascx");

            _cart = (CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ShoppingCart.ShoppingCart)Page.LoadControl(shoppingCartUserControlPath);
            shoppingCartPlaceholder.Controls.Add(_cart);
        }

        private void loadPage()
        {
            var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.ShoppingCart).ToFrontendBase();
            if(article != null)
            {
                contentPage.Functionality.ContentPage = article;
            }
        }



    }
}