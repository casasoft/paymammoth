namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Other
{
    using System;

    public partial class renewSession : CS.WebComponentsGeneralV3.Code.Classes.Pages.BasePageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CS.General_v3.Util.PageUtil.SendRenewSessionImage();

        }
        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get
            {
                return this.htmlTag;
            }
        }
        public override System.Web.UI.HtmlControls.HtmlGenericControl BodyTag
        {
            get
            {
                return this.bodyTag;
            }
        }
    }
}