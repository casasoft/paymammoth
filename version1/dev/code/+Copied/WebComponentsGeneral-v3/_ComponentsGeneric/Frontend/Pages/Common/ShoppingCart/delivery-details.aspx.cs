﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.ArticleModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Frontend.MemberModule;
using BusinessLogic_v3;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.ShoppingCart
{
    public partial class delivery_details : BasePage<BaseMainMasterPage>
    {
        public delivery_details()
        {
            this.Functionality.RequiresMemberAuthentication = true;
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
        }
    }
}