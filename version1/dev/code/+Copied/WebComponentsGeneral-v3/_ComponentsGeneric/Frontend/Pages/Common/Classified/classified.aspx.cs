﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Culture;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.MemberModule;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.ClassifiedModule;
using BusinessLogic_v3.Modules;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Classified
{
    public partial class classified : BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            init();
            initPage();
        }

        private void initPage()
        {
            this.Functionality.ContentPage =BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Classified.GetArticleFrontend();
            navigationBreadcrumbs.Functionality.AddFromHierarchy(
                BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Classifieds.GetArticleFrontend(), false);
        }

        private ClassifiedBaseFrontend _classified { get; set; }

        private void init()
        {
            loadFromRoute();
        }

        private void loadFromRoute()
        {
            _classified = Factories.ClassifiedFactory.GetClassifiedFromRoute().ToFrontendBase();
            if (_classified != null)
            {
                initClassifiedItem(_classified);
                navigationBreadcrumbs.Functionality.AddItem(_classified.Data.Title, _classified.GetURL());
                ctrlClassifiedContactDetails.Functionality.Classified = _classified;
                var member = BusinessLogic_v3.Modules.Factories.MemberFactory.GetLoggedInUserForCurrentSession();
                if(member != null)
                {
                    ctrlClassifiedSubmitEnquiry.Functionality.Member = member.ToFrontendBase();
                    ctrlClassifiedSubmitEnquiry.Functionality.Classified = _classified;
                }
            }
            else
            {
                this.Functionality.ShowGenericMessageAndRedirectToPage(BusinessLogic_v3.Enums.CONTENT_TEXT.General_ErrorEncountered.GetContentTextFrontend(), CS.General_v3.Enums.STATUS_MSG_TYPE.Error, BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Home.GetArticleFrontend().GetUrl());
            }
        }

        private void initClassifiedItem(ClassifiedBaseFrontend _classified)
        {
            this.contentPage.Functionality.ContentPage = _classified;
            this.contentPage.Functionality.DisableInlineEditing = true;
            if (_classified.Data.Image != null)
            {
                imgClassified.ImageUrl = _classified.Data.Image.GetSpecificSizeUrl(BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase.ImageSizingEnum.Normal);
            }
            else
            {
                CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(imgClassified);
            }
            //classifiedDate.Functionality.date = _classified.Data.PostedOn;
            if (_classified.Data.Price != 0)
            {
                divPrice.InnerText =
                    DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(_classified.Data.Price,
                                                                                  CS.General_v3.Util.
                                                                                      NumberUtil.
                                                                                      NUMBER_FORMAT_TYPE.
                                                                                      Currency);
            }
            else
            {
                MyContentText.AttachContentTextWithControl(divPrice,
                                                           BusinessLogic_v3.Enums.CONTENT_TEXT.Classifieds_MessageNoPrice.
                                                               GetContentTextFrontend());
            }

            
        }


    }
}