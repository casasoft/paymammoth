﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Article
{
    using BusinessLogic_v3.Classes.Routing;
    using BusinessLogic_v3.Extensions;
    using BusinessLogic_v3.Frontend.ArticleModule;
    using Code.Classes.Pages;
    using General_v3.Util;

    public abstract class CommonArticleBasePage : BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage>
    {
        private ArticleBaseFrontend __article;

        protected ArticleBaseFrontend _article
        {
            get
            {
                if (__article == null)
                {
                    var cpID = BusinessLogic_v3.Classes.Routing.ArticlesRoute.GetIDFromRouteData();
                    if (cpID.HasValue)
                    {
                        __article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetByPrimaryKey(cpID.Value).ToFrontendBase();
                    }
                }
                return __article;
            }
        }

        protected abstract CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage.ContentPage _contentPage { get; }
        protected abstract CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage.PreviousNextArticle _previousNextEntry { get; }
        protected abstract CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.UserControls.ContentPage.ArticleComments _contentPageComments { get; }

        private void loadPage()
        {
            bool ok = false;
            if (_article != null)
            {
                _contentPage.Functionality.ContentPage = _article;
                this._contentPageComments.Functionality.Article = _article;

                initPreviousNextArticle(_article);
                this.Functionality.ContentPage = _article;
                ok = true;
            }
            if (!ok)
            {
                CS.General_v3.Util.Log4NetUtil.LogMsgByObject(this, General_v3.Enums.LOG4NET_MSG_TYPE.Error, "Page with ID '" + BusinessLogic_v3.Classes.Routing.ArticlesRoute.GetIDFromRouteData() + "' not found in DB", null);
                //Show a default error page
                string pageNotFoundURL = ErrorsRoute.GetPageNotFoundErrorURL();
                PageUtil.RedirectPage(pageNotFoundURL);

            }
        }


        private void initPreviousNextArticle(ArticleBaseFrontend article)
        {
            bool showPreviousNextArticle = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_ShowPreviousNextArticle);
            if (article.Data.ArticleType == BusinessLogic_v3.Enums.ARTICLE_TYPE.BlogPosts || showPreviousNextArticle)
            {
                _previousNextEntry.Functionality.Article = article;
            }
            else
            {
                _previousNextEntry.Parent.Controls.Remove(_previousNextEntry);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            loadPage();
            base.OnLoad(e);
        }
    }
}