﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Frontend.MemberModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Modules.ArticleModule;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using CS.WebComponentsGeneralV3.Code.Util;
using BusinessLogic_v3.Classes.Routing;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member
{

    public partial class profile : BasePage<BaseMainMasterPage>
    {
        public profile()
        {
            this.Functionality.RequiresMemberAuthentication = true;
        }
        private MemberBaseFrontend _member;
        protected override void OnLoad(EventArgs e)
        {
            init();

            base.OnLoad(e);
        }

        private void init()
        {
            _member =
                BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.
                    GetLoggedInUserAlsoFromRememberMe().ToFrontendBase();
            if (_member != null)
            {
                loadPage();
                initProfileFields();
            }
            else
            {
                this.Functionality.ShowErrorMessageAndRedirectToPage(
                    BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                        BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_General_YouMustBeLoggedInToAccessThisPage).ToFrontendBase(), "/");
            }
        }
        private void initProfileFields()
        {
            profileFields.Functionality.DisplayType =
                UserControls.Members.Profile.ProfileFields.PROFILE_FIELDS_DISPLAY_TYPE.UpdateProfile;
        }

        private void loadPage()
        {
            ArticleBaseFrontend article =
                BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(
                    BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.MembersArea_Profile).ToFrontendBase();

            if (article != null)
            {
                contentPage.Functionality.ContentPage = article;
            }
        }

    }
}