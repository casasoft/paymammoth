﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Member
{
    public partial class files  : BasePage<CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage>
    {
        public files()
        {
            this.Functionality.RequiresMemberAuthentication = true;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
    }
}