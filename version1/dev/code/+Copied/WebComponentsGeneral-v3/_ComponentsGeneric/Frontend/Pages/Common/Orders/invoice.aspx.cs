﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Routing;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3;
using BusinessLogic_v3.Modules.ContentTextModule;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.Frontend.Pages.Common.Orders
{

    public partial class invoice : BasePage<BaseCoreMasterPage>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.HtmlTag.Attributes["class"] += " html-invoice";
            init();
        }

        private void init()
        {
            loadFromRoute();
        }

        private void loadFromRoute()
        {
            long? orderID = OrderRoute.GetIdFromCurrentRoute();
            string orderGUID = CS.General_v3.Util.QueryStringUtil.GetStringFromQS(BusinessLogic_v3.Constants.ParameterNames.GUID);
            bool result = false;
            if (orderID.HasValue)
            {
                var cp = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.Members_Order_InvoicePage).ToFrontendBase();
                this.Functionality.ContentPage = cp;
                
                var order = BusinessLogic_v3.Modules.Factories.OrderFactory.GetByPrimaryKey(orderID.Value);



                if (order != null && !string.IsNullOrWhiteSpace(orderGUID))
                {
                    string title = cp.GetTitleForSEO();
                    title = CS.General_v3.Util.Text.ReplaceTag(title, BusinessLogic_v3.Constants.Tokens.INVOICE_NUMBER, order.Reference);
                    this.Functionality.UpdateTitle(title);
                    if (order.GUID == orderGUID)
                    {
                        result = true;
                        controlInvoice.Functionality.FillFromOrder(order.ToFrontendBase());
                    }
                }
            }
            if (!result)
            {
                var articlePageNotFound =Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Error_PageNotFound).ToFrontendBase();
                CS.General_v3.Util.PageUtil.RedirectPage(articlePageNotFound.Data.GetUrl());
            }
        }
    }
}