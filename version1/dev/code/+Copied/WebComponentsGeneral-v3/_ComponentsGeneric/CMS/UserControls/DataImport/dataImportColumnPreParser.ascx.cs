﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.DataImport.Helpers;
using CS.WebComponentsGeneralV3.Code.Cms.UserControls;
using BusinessLogic_v3.Classes.DataImport;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.DataImport
{
    public partial class dataImportColumnPreParser : BaseCmsUserControl
    {

        private class ColumnFieldInfo
        {
            public DataImportColumnInfo Column { get; set; }
            public MyDropDownList ListBox { get; set; }
        }

        #region dataImportColumnPreParser Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            public IDataImporterBase DataImporter { get; set; }

            protected dataImportColumnPreParser _item = null;
            internal FUNCTIONALITY(dataImportColumnPreParser item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, null);
                this._item = item;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        private List<ColumnFieldInfo> columnFieldInfos = null;
        private void initForm()
        {
            columnFieldInfos = new List<ColumnFieldInfo>();
            var csvHeaders = this.Functionality.DataImporter.GetHeadersFromCsvFile();
           

            for (int i = 0; i < this.Functionality.DataImporter.Parameters.ColumnsCollection.Columns.Count; i++)
            {
                var col = this.Functionality.DataImporter.Parameters.ColumnsCollection.Columns[i];
                if (!col.NotUsed)
                {
                    MyDropDownListItems list = new MyDropDownListItems();
                    for (int j = 0; j < csvHeaders.Count; j++)
                    {
                        var h = csvHeaders[j];
                        list.AddOption(h, j.ToString());
                    }
                    string helpMessage = "Data Type: " + CS.General_v3.Util.EnumUtils.StringValueOf(col.Validation.DataType) + "\r\n" +
                                         col.Description;
                    MyDropDownList cmb = formColumns.Functionality.AddSingleChoiceList("txtColumn_" + i,
                                                                                       col.GetMainTitle(), col.Validation.Required, list, helpMessage, null,
                                                                                       selectedValue: (col.LinkedColumnIndexInCsv.HasValue ? col.LinkedColumnIndexInCsv.Value.ToString() : null));
                    columnFieldInfos.Add(new ColumnFieldInfo() {Column = col, ListBox = cmb});
                }
            }

            formColumns.Functionality.ButtonSubmitText = "Start Processing";
            formColumns.Functionality.ButtonCancelText = "Go Back";
            formColumns.Functionality.ClickSubmit += new EventHandler(formColumns_ClickSubmit);
            formColumns.Functionality.ClickCancel += new EventHandler(formColumns_ClickCancel);
        }

        void formColumns_ClickCancel(object sender, EventArgs e)
        {
            
            this.Functionality.DataImporter.TerminateImportJob();
            
        }

        void formColumns_ClickSubmit(object sender, EventArgs e)
        {
            foreach (var colField in this.columnFieldInfos)
            {
                
                var col = colField.Column;
                if (!col.NotUsed)
                {
                    var cmb = colField.ListBox;
                    col.LinkedColumnIndexInCsv = (int?) cmb.GetFormValueAsLongNullable();
                }
            }

            var result = this.Functionality.DataImporter.Parameters.ColumnsCollection.CheckIfAllCsvColumnIndexAreDefined();
            if (result.IsSuccessful)
            {
                result = this.Functionality.DataImporter.StartProcessing();
                
            }
           
            if (result.IsSuccessful)
            {
                System.Threading.Thread.Sleep(2000);
                CmsUtil.ShowStatusMessageInCMSAndRefresh("Started import.  Periodically refresh this page to view status", General_v3.Enums.STATUS_MSG_TYPE.Success);
            }
            else
            {
                CmsUtil.ShowStatusMessageInCMSAndRefresh(result.GetMessageAsString(), result.Status);
            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            initForm();
        }
    }
}