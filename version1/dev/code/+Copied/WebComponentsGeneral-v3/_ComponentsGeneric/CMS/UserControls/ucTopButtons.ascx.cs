﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls
{
    using Code.Cms.UserControls;

    public partial class ucTopButtons : TopButtons
    {

        private bool _controlsInit = false;

        protected override void initControls()
        {
            if (!_controlsInit)
            {
                _controlsInit = true;
                base.tblButtons = tblButtons;
                base.initControls();
            }

        }





    }
}