﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="dataImport.ascx.cs"
    Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.DataImport.dataImport" %>
<div class="data-import">
    <h2>
        Data Importer</h2>
    <p>
        Here you can bulk-import items. Import should be done from an excel file (*.xls,
        *.xlsx) or a comma-seperated file (*.csv).
    </p>
    <CSControlsFormFields:FormFields ID="formImport" runat="server" />
    <div id="divImportJobRunning" runat="server" class="import-job-running" visible="false">
        <p>
            An import job is currently running. You cannot run more than one import job at once.
            Refresh to view the status. If you wish, you can choose to terminate the job by
            clicking on the below button</p>
        <CSControls:MyButton ID="btnTerminateJob" runat="server" Text="Terminate Job" ConfirmMessage="Are you sure you want to terminate the current job?" />
    </div>
    <div id="divResults" runat="server" class="results" visible="false">
        <h3>
            Results</h3>
        <table class="import-info">
            <tr>
                <td class="label">
                    Filename:
                </td>
                <td id="tdFilename" runat="server">
                    N / A
                </td>
            </tr>
            <tr id="trZipFile" runat="server">
                <td class="label">
                    ZIP File:
                </td>
                <td id="tdZIPFile" runat="server">
                    N / A
                </td>
            </tr>
            <tr>
                <td class="label">
                    Started On:
                </td>
                <td id="tdStartedOn" runat="server">
                    N / A
                </td>
            </tr>
            <tr>
                <td class="label">
                    Finished On:
                </td>
                <td id="tdFinishedOn" runat="server">
                    N / A
                </td>
            </tr>
        </table>
        <asp:PlaceHolder ID="phMessages" runat="server"></asp:PlaceHolder>
        <CSControls:MyUnorderedList ID="ulMessages" runat="server" Visible="false" />
        <asp:PlaceHolder ID="phResultsTable" runat="server"></asp:PlaceHolder>
    </div>
</div>
