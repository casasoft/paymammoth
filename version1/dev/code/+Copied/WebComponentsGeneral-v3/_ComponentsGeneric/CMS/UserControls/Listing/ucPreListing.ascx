﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucPreListing.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.Listing.ucPreListing" %>
<p id="pUpdateMsg" runat="server">
* Important:  If you update fields in the listing, kindly note that you must press <strong><em>Update Listing Items</em></strong> button found at the bottom of the screen in order for the values to be saved.
</p>