﻿using BusinessLogic_v3.Classes.DataImport.Generic;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.SpecialOffers
{
    using System;
    using BusinessLogic_v3.Classes.DataImport;
    using BusinessLogic_v3.Modules;
    using BusinessLogic_v3.Modules.SpecialOfferModule;
    using Cms.SpecialOfferModule;
    using Code.Cms.Util;
    using Code.Cms.WebControls.DataImport;
    using CS.WebComponentsGeneralV3.Code.Cms.UserControls;

    public partial class ImportVoucherCodes : CS.WebComponentsGeneralV3.Code.Cms.UserControls.BaseCmsUserControl
    {
        private long getSpecialOfferID()
        {
            return CS.WebComponentsGeneralV3.Code.Cms.Util.QueryStringUtil.GetSpecialOfferID();
        }
        private ISpecialOfferBase specialOffer = null;
        private void loadSpecialOffer()
        {
            specialOffer = Factories.SpecialOfferFactory.GetByPrimaryKey(getSpecialOfferID());

            if (specialOffer == null)
            {
                string url = SpecialOfferBaseCmsFactory.Instance.GetListingUrl().ToString();
                CS.General_v3.Util.PageUtil.RedirectPage(url);
            }


        }
        private void initForm()
        {
            dataImportForm.Functionality.FormSubmitted += new CS.WebComponentsGeneralV3.Code.Cms.WebControls.DataImport.DataImportForm.FormSubmittedEvent(Functionality_FormSubmitted);
            dataImportForm.Functionality.ShowZIPFile = false;
        }

        ImportResults Functionality_FormSubmitted(CS.General_v3.Util.Encoding.CSV.CSVFile csvFileContents, System.IO.Stream ZIPFile)
        {
            VoucherCodeImporter importer = new VoucherCodeImporter(specialOffer);
            var results = importer.ProcessFile(csvFileContents);
            return results;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            loadSpecialOffer();
            initForm();

        }
    }
}