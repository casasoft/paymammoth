﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls
{
    using System;
    using Code.Cms.UserControls;
    using Code.Controls.WebControls.Common;

    public partial class ucLanguageSelect : LanguageSelect
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cmbLanguage.FieldParameters.validationGroup = "languageSelect";
            testButton.Click += new EventHandler(testButton_Click);
        }

        void testButton_Click(object sender, EventArgs e)
        {
            base.buttonClick();
        }

        public override MyDropDownList cmbLanguage
        {
            get
            {
                return _cmbLanguage;
            }
            
        }
    }
}