﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.SpecialOffers
{
    using System;
    using System.Collections.Generic;
    using BusinessLogic_v3.Modules.SpecialOfferModule;
    using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;
    using BusinessLogic_v3.Modules;
    using Cms.SpecialOfferModule;
    using Code.Cms.Util;
    using Code.Controls.WebControls.Common.Fields.Validators;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common.Fields;
    using CS.WebComponentsGeneralV3.Code.Cms.UserControls;

    public partial class GenerateVoucherCodesControl : BaseCmsUserControl
    {
        private MyTxtBoxNumber _txtLengthVoucherCode;
        private MyTxtBoxNumber _txtAmtToGenerate;

        private long getSpecialOfferID()
        {
            return QueryStringUtil.GetSpecialOfferID();
        }
        private ISpecialOfferBase specialOffer = null;
        private void loadSpecialOffer()
        {
            specialOffer = Factories.SpecialOfferFactory.GetByPrimaryKey(getSpecialOfferID());

            if (specialOffer == null)
            {
                string url = SpecialOfferBaseCmsFactory.Instance.GetListingUrl().ToString();
                CS.General_v3.Util.PageUtil.RedirectPage(url);
            }


        }
        private void initForm()
        {
            //public IMyFormWebControl AddInteger(string id, string title, bool required, string helpMessage, int? width, int? initValue, IEnumerable<ListItem> coll)

            // formVoucherCodes.AddInteger("txtLength", "Length of voucher code", true, null, null, 8);

            _txtLengthVoucherCode = (MyTxtBoxNumber)formVoucherCodes.Functionality.AddFieldItem(new MyTxtBoxNumber(new FieldSingleControlParameters()
            {
                title = "Length of voucher code",
                id = "txtLength",
                required = true,
                validators = new List<IValidator>()
                {
                     new ValidatorNumeric() { 
                          Parameters = new ValidatorNumericParameters() {
                               integersOnly = true
                          }
                     }
                }
            }) { InitialValue = 8 });

            //formVoucherCodes.AddInteger("txtAmt", "Amount to generate", true, null, null, 100);

            _txtAmtToGenerate = (MyTxtBoxNumber)formVoucherCodes.Functionality.AddFieldItem(new MyTxtBoxNumber(new FieldSingleControlParameters()
            {
                title = "Amount to generate",
                id = "txtAmt",
                required = true,
                validators = new List<IValidator>()
                {
                     new ValidatorNumeric() { 
                          Parameters = new ValidatorNumericParameters() {
                               integersOnly = true
                          }
                     }
                }
            }) { InitialValue = 8 });

            formVoucherCodes.Functionality.ButtonSubmit.WebControlFunctionality.ConfirmMessage = "Are you sure you want to create the voucher codes?";
            formVoucherCodes.Functionality.ClickSubmit += new EventHandler(formVoucherCodes_ClickSubmit);

        }
        private void showResults(IEnumerable<ISpecialOfferVoucherCodeBase> list)
        {

            phResults.Visible = true;
            foreach (var item in list)
            {
                ulResults.AddListItem(item.VoucherCode, "voucher-code");
            }
        }
        void formVoucherCodes_ClickSubmit(object sender, EventArgs e)
        {

            int amt = _txtAmtToGenerate.GetFormValueAsInt();
            int len = _txtLengthVoucherCode.GetFormValueAsInt();

            var list = specialOffer.GenerateVoucherCodes(len, amt);
            showResults(list);
            this.Master.Functionality.ShowStatusMessage("Voucher codes generated successfully", General_v3.Enums.STATUS_MSG_TYPE.Success);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            loadSpecialOffer();
            initForm();

        }
    }
}