﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenerateVoucherCodesControl.ascx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.SpecialOffers.GenerateVoucherCodesControl" %>
<CSControlsFormFields:FormFields id="formVoucherCodes" runat="server"></CSControlsFormFields:FormFields>
<asp:PlaceHolder Visible="false" ID="phResults" runat="server">
<h2>Results</h2>
<p>Below is a list of all the generated voucher codes</p>
<CSControls:MyUnorderedList ID="ulResults" runat="server" />

</asp:PlaceHolder>