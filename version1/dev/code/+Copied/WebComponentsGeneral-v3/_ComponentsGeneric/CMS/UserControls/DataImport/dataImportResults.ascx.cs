﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.DataImport;
using CS.WebComponentsGeneralV3.Code.Cms.UserControls;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.DataImport_v2;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.DataImport
{
    public partial class dataImportResults : BaseCmsUserControl
    {


        #region dataImportResults Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected dataImportResults _item = null;
            public IDataImporterBase DataImporter { get; set; }

            internal FUNCTIONALITY(dataImportResults item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, null);
                this._item = item;
            }
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			
        protected void Page_Load(object sender, EventArgs e)
        {
            init();
        }

        private void init()
        {
            initControls();
            initHandlers();
            showResults();
        }
        private void initHandlers()
        {
            btnTerminate.WebControlFunctionality.ConfirmMessage = "Are you sure you want to terminate this job?";
            btnTerminate.Click += new EventHandler(btnTerminate_Click);
            btnRefresh.Click += new EventHandler(btnRefresh_Click);
        }

        void btnRefresh_Click(object sender, EventArgs e)
        {
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Refreshed successfully", General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

        void btnTerminate_Click(object sender, EventArgs e)
        {
            this.Functionality.DataImporter.TerminateImportJob();
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Job terminated successfully", General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

        public ResultsTable ResultsTable { get; set; }

       

        private void initControls()
        {

            ResultsTable = new ResultsTable(); phResultsTable.Controls.Add(ResultsTable); ResultsTable.CssManager.AddClass("dataimportform-results");
            ResultsTable.Visible = false;

        }

        private void addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE msgType, string msg)
        {
            ulMessages.Visible = true;
            MyListItem li = new MyListItem();
            li.CssManager.AddClass(CS.General_v3.Util.EnumUtils.StringValueOf(msgType));
            Literal lit = new Literal();
            lit.Text = CS.General_v3.Util.Text.HtmlEncode(msg);

            li.Controls.Add(lit);
            ulMessages.Controls.Add(li);

            // MyDiv spanMsg = new MyDiv();
            //// spanMsg.CssManager.AddClass(CS.General_v3.Util.EnumUtils.StringValueOf(msgType));
            // spanMsg.InnerHtml = CS.General_v3.Util.Text.HtmlEncode(msg);
            // phMessages.Controls.Add(spanMsg);

        }
        private void showResults()
        {
            

            var results = this.Functionality.DataImporter.Results;
            if (results != null && (results.Lines.Count > 0 || (results.Lines.Count == 0 & results.GetResultType() != CS.General_v3.Enums.STATUS_MSG_TYPE.Success)))
            {
                divResults.Visible = true;
                IDataImporterBase importer = this.Functionality.DataImporter;
                if (results.Lines.Count > 0)
                {
                    ResultsTable.Visible = true;
                    ResultsTable.FillFromImportResults(results);
                }
                var resultType = results.GetResultType();
                tdFilename.InnerText = importer.Parameters.UploadedCsvFilename;
                string zipFilename = null;//importer.Parameters.UploadedCsvFilename.CurrentZipFilename;
                if (!string.IsNullOrWhiteSpace(zipFilename))
                {
                    trZipFile.Visible = true;
                    tdZIPFile.InnerText = zipFilename;
                }
                if (importer.StartedOn.HasValue)
                {
                    tdStartedOn.InnerText = importer.StartedOn.Value.ToString("ddd d MMM yyyy hh:mmtt");
                }
                if (importer.FinishedOn.HasValue)
                {
                    tdFinishedOn.InnerText = importer.FinishedOn.Value.ToString("ddd d MMM yyyy hh:mmtt");
                }

                if (importer.IsFinished())
                {
                    addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Success, "Data imported. Status: " + CS.General_v3.Util.EnumUtils.StringValueOf(resultType));
                }
                else
                {

                    
                    addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Success, "Import Status: " + importer.GetCurrentLineIndex() + " / " + importer.GetTotalLineCount());
                    if (importer.StartedOn.HasValue)
                    {
                        TimeSpan span = CS.General_v3.Util.Date.Now.Subtract(importer.StartedOn.Value);
                        double rowsPerSecond = (double)(importer.GetCurrentLineIndex()) / (double)span.TotalSeconds;
                        addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Success, "Import Speed: "+ rowsPerSecond.ToString("0.00") + " rows / second");

                        double secondsLeft = (double)((double)importer.GetTotalLineCount() - importer.GetCurrentLineIndex()) / (double)rowsPerSecond;

                        TimeSpan timeLeft = new TimeSpan((long)(TimeSpan.TicksPerSecond * secondsLeft));
                        DateTime completionEstimate = CS.General_v3.Util.Date.Now.AddSeconds(secondsLeft);
                        addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Success, "Estimated time left: " + timeLeft.Hours.ToString("00") + ":" + timeLeft.Minutes.ToString("00") + ":" + timeLeft.Seconds.ToString("00")
                            + "    |    Completion On: " + completionEstimate.ToString("dd/MM/yyyy hh:mm:ss tt"));
                        


                    }

                }


                if (results.GeneralImportResult.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
                {
                    foreach (var msg in results.GeneralImportResult.StatusMessages)
                    {
                        addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, msg.Message);
                    }

                }
                else
                {
                    addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Error importing data");
                }
            }

        }
    }
}