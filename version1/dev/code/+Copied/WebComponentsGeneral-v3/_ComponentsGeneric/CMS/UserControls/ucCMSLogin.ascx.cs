﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls
{
    using System;
    using CS.WebComponentsGeneralV3.Code.Cms.Util;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using CS.WebComponentsGeneralV3.Code.Cms.UserControls;

    public partial class ucCMSLogin : BaseCmsUserControl
    {
        private MyTxtBoxString _txtUser;
        private MyTxtBoxStringPassword _txtPass;
       


        private void initForm()
        {
            _txtUser = formLogin.Functionality.AddString("txtUser", "Username", true, null, null);
            _txtUser.AutoComplete = true;
            _txtPass = formLogin.Functionality.AddStringPassword("txtPass", "Password", true, null, null);
            _txtPass.AutoComplete = true;
            _txtUser.NeverFillWithRandomValue = _txtPass.NeverFillWithRandomValue = true;
            formLogin.Functionality.ClickSubmit += new EventHandler(formLogin_ClickSubmit);
            formLogin.Functionality.ButtonSubmitText = "Login";
        }

        void formLogin_ClickSubmit(object sender, EventArgs e)
        {
            string user = _txtUser.GetFormValue();
            string pass = _txtPass.GetFormValue();

            //string whiteListedIPs = BusinessLogic_v3.Modules.v2.Setting.SettingBaseFrontendFactory.Instance.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.CMS_WhiteListedIPs);
            if (CmsUtil.IsUserIPWhitelistedToAccessCMS())
            {
                string welcomeUrl = CS.General_v3.Util.RoutingUtil.GetRouteUrl(CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsRoutesMapper.Route_CmsGeneral_Welcome, null);

                BusinessLogic_v3.Modules.CmsUserModule.CmsUserSessionLogin.Instance.WelcomeUrl = welcomeUrl;
                var result = BusinessLogic_v3.Modules.CmsUserModule.CmsUserSessionLogin.Instance.Login(user, pass, false, true, false);
                if (result.LoginStatus != CS.General_v3.Enums.LOGIN_STATUS.Ok)
                {
                    this.Master.Functionality.ShowStatusMessageAndRedirectToSamePage("Invalid username/password", CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
                }
            }
            else
            {
                this.Master.Functionality.ShowIPRestricedError();

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            initForm();

        }
    }
}