﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls
{
    using System;
    using Code.Cms.UserControls;

    public partial class ucWelcomePage : BaseCmsUserControl
    {
        protected override void OnLoad(EventArgs e)
        {
            this.litTitle.Text = this.Master.Title;
            base.OnLoad(e);
        }

    }
}