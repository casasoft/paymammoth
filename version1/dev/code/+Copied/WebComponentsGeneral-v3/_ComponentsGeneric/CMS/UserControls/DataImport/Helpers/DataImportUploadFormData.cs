﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.DataImport.Helpers
{
    public class DataImportUploadFormData
    {

        public bool RemoveAnyExistingImages { get; set; }

        public string CustomUploadPath { get; set; }
        public string CsvFilePath { get; set; }
        public string ZipFilePath { get; set; }

        public void SetCsvFile(MyFileUpload fileUpload)
        {
            string uploadedFile = fileUpload.FileName;
            string tmpFilename = CS.General_v3.Util.IO.GetTempFilename(uploadedFile);
            CS.General_v3.Util.IO.SaveStreamToFile(fileUpload.UploadedFileStream, tmpFilename);
            this.CsvFilePath = tmpFilename;
        }

        public void SetZipFile(Code.Controls.WebControls.Common.MyFileUpload fileUpload)
        {
            string uploadedFile = fileUpload.FileName;
            string tmpFilename = CS.General_v3.Util.IO.GetTempFilename(uploadedFile);
            CS.General_v3.Util.IO.SaveStreamToFile(fileUpload.UploadedFileStream, tmpFilename);
            this.ZipFilePath = tmpFilename;
        }
    }
}