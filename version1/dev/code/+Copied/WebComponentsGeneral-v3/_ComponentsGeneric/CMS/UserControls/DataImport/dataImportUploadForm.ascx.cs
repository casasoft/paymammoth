﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Cms.UserControls;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Classes.DataImport.Helpers;
using BusinessLogic_v3.Classes.DataImport;
using CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.DataImport.Helpers;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.DataImport
{
    public partial class dataImportUploadForm : BaseCmsUserControl
    {
            

        #region dataImportUploadForm Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected dataImportUploadForm _item = null;
            public IDataImporterBase DataImporter { get; set; }
            
            internal FUNCTIONALITY(dataImportUploadForm item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, null);
                this._item = item;
            }


            
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        private MyFileUpload _fileUploadCSV;
        private MyDropDownList _txtCulture;
        private MyFileUpload _fileUploadZIP;
        private MyTxtBoxString _txtZIPPath;
        private MyTxtBoxString _txtUploadPath;
        private MyCheckBox _chkRemoveAnyExistingImages;

        private void initForm()
        {
            var f = this.formImport;
            _fileUploadCSV = f.Functionality.AddFileUpload("txtCSV", "Excel File", true, "File to upload with information (*.csv, *.txt, *.xls, *.xlsx)");
            {
                var cultures = BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.FindAll();
                MyDropDownListItems listItems = new MyDropDownListItems();
                foreach (var c in cultures)
                {
                    listItems.AddOption(c.Title, c.ID.ToString(), c.IsDefaultCulture);
                }
                _txtCulture = f.Functionality.AddSingleChoiceList("txtCulture", "Language", true, listItems, "Language / culture of the import sheet");
            }
            if (this.Functionality.DataImporter.Parameters.FormParams.ShowZipFile)
            {
                _fileUploadZIP = f.Functionality.AddFileUpload("txtZIPFileUpload", "ZIP File (Upload)", false, "ZIP File to upload with images");
                _txtZIPPath = f.Functionality.AddString("txtZIPPath", "Zip File (Path)", false, "Path of ZIP file on server to load information from");
            }

            if (this.Functionality.DataImporter.Parameters.FormParams.ShowCustomUploadPath)
            {
                _txtUploadPath = f.Functionality.AddString("txtUploadPath", "Uploads Path", false, "Path of a folder (relative from website folder), which contains images/media to be linked with in the excel file uploaded");
            }
            if (this.Functionality.DataImporter.Parameters.FormParams.ShowRemoveExistingImagesCheckbox)
            {
                _chkRemoveAnyExistingImages = f.Functionality.AddBool("chkRemoveAnyExistingImages", "Remove any existing images?", false, "Removes any existing images from any product in the excel file");
                _chkRemoveAnyExistingImages.WebControlFunctionality.InitialValue = false;
            }
            f.Functionality.ButtonSubmitText = "Import";
            f.Functionality.ButtonSubmit.WebControlFunctionality.ConfirmMessage = "Are you sure you want to start the import?";
            f.Functionality.ClickSubmit += new EventHandler(formImport_ClickSubmit);
        }

        public event Action<DataImportUploadFormData> FormSubmitted;

        void formImport_ClickSubmit(object sender, EventArgs e)
        {
            StringBuilder sbError = new StringBuilder();
            if (_fileUploadCSV.HasUploadedContent())
            {
                this.Functionality.DataImporter.Parameters.CsvFilePath = WebUtil.UploadFileContentToTempFile(_fileUploadCSV);
                this.Functionality.DataImporter.Parameters.Culture = BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetByPrimaryKey(_txtCulture.GetFormValueAsLongNullable().Value);
                this.Functionality.DataImporter.Parameters.UploadedCsvFilename = _fileUploadCSV.FileName;
            }
            else
            {
                sbError.AppendLine("Please upload a CSV / data file");
            }
            if (_fileUploadZIP != null)
            {
                this.Functionality.DataImporter.Parameters.ZipFilePath = WebUtil.UploadFileContentToTempFile(_fileUploadZIP);
            }
            if (_chkRemoveAnyExistingImages != null)
            {
                this.Functionality.DataImporter.Parameters.RemoveAnyExistingImages = _chkRemoveAnyExistingImages.GetFormValueAsBool();
            }
            if (_txtUploadPath != null)
            {
                this.Functionality.DataImporter.Parameters.CustomFilePathsToCheckForUploads.Add(_txtUploadPath.GetFormValueAsString());
            }

            if (sbError.Length == 0)
            {

                this.Functionality.DataImporter.LoadCsvFile();
                CmsUtil.ShowStatusMessageInCMSAndRefresh("File uploaded successfully. Kindly configure the columns", General_v3.Enums.STATUS_MSG_TYPE.Success);
            }
            else
            {
                CmsUtil.ShowStatusMessageInCMSAndRefresh(sbError.ToString(), General_v3.Enums.STATUS_MSG_TYPE.Error);
            }

        }



        private void checkRequirements()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(this.Functionality.DataImporter, "Importer must be defined!");
        }
        private void checkIfRunning()
        {
            if (this.Functionality.DataImporter.IsRunning())
            {
                formImport.Visible = false;
                divImportJobRunning.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            checkRequirements();
            checkIfRunning();
            init();
        }
        private void init()
        {
            initHandlers();
            initForm();
            
        }

        private void initHandlers()
        {
            btnTerminateJob.Click += new EventHandler(btnTerminateJob_Click);
        }

        void btnTerminateJob_Click(object sender, EventArgs e)
        {
            this.Functionality.DataImporter.TerminateImportJob();
            CmsUtil.ShowStatusMessageInCMS("Job terminated", CS.General_v3.Enums.STATUS_MSG_TYPE.Information);
        }
    }
}