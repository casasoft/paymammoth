﻿using BusinessLogic_v3.Classes.DataImport.Generic.v2;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.DataImport
{
    using System;
    using System.IO;
    using System.Web.UI.WebControls;
    
    using CS.General_v3.Util.Encoding.CSV;
    using ImportResults = BusinessLogic_v3.Classes.DataImport.Generic.ImportResults;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using CS.WebComponentsGeneralV3.Code.Cms.UserControls;
    using CS.WebComponentsGeneralV3.Code.Cms.WebControls.DataImport_v2;
    using CS.WebComponentsGeneralV3.Code.Cms.Util;

    public partial class dataImport : BaseCmsUserControl
    {

        private MyFileUpload _fileUploadCSV;
        private MyFileUpload _fileUploadZIP;
        private MyTxtBoxString _txtZIPPath;
        private MyTxtBoxString _txtUploadPath;
        private MyCheckBox _chkRemoveAnyExistingImages;

        public delegate ImportResults FormSubmittedEvent(CSVFile csvFileContents, Stream ZIPFile, string uploadPath);
        public dataImport()
        {
            
            this.Functionality = new FUNCTIONALITY(this);
        }
        public class FUNCTIONALITY
        {
            private dataImport _form = null;
            public IGenericDataImporter_v2 Importer { get; set; }
            public FUNCTIONALITY(dataImport form)
            {
                this._form = form;
                this.ShowZIPFile = true;
                
            }
            //internal ImportResults callFormSubmitted(CSVFile csvFileContents, Stream ZIPFile, string uploadPath)
            //{
            //    return FormSubmitted(csvFileContents, ZIPFile, uploadPath);
            //}
           // public event FormSubmittedEvent FormSubmitted;
            public bool ShowZIPFile { get; set; }
            public bool ShowUploadPath { get; set; }
            public bool ShowRemoveAnyExistingImages { get; set; }

        }

        public FUNCTIONALITY Functionality { get; set; }

        
        public ResultsTable_v2 ResultsTable { get; set; }
        private void initHandlers()
        {
            btnTerminateJob.Click += new EventHandler(btnTerminateJob_Click);
        }

        void btnTerminateJob_Click(object sender, EventArgs e)
        {
            this.Functionality.Importer.TerminateImportJob();
            CmsUtil.ShowStatusMessageInCMS("Job terminated", CS.General_v3.Enums.STATUS_MSG_TYPE.Information);
        }

        private void initForm()
        {
            var f = this.formImport;
            _fileUploadCSV = f.Functionality.AddFileUpload("txtCSV", "Excel File", true, "File to upload with information (*.csv, *.txt, *.xls, *.xlsx)");
            if (this.Functionality.ShowZIPFile)
            {
                _fileUploadZIP =  f.Functionality.AddFileUpload("txtZIPFileUpload", "ZIP File (Upload)", false, "ZIP File to upload with images");
                _txtZIPPath = f.Functionality.AddString("txtZIPPath", "Zip File (Path)", false, "Path of ZIP file on server to load information from");
            }
            
            if (this.Functionality.ShowUploadPath)
            {
               _txtUploadPath = f.Functionality.AddString("txtUploadPath", "Uploads Path", false, "Path of a folder (relative from website folder), which contains images/media to be linked with in the excel file uploaded");
            }
            if (this.Functionality.ShowRemoveAnyExistingImages)
            {
                 _chkRemoveAnyExistingImages = f.Functionality.AddBool("chkRemoveAnyExistingImages", "Remove any existing images?", false, "Removes any existing images from any product in the excel file");
                 _chkRemoveAnyExistingImages.WebControlFunctionality.InitialValue = false;
            }
            f.Functionality.ButtonSubmitText = "Import";
            f.Functionality.ButtonSubmit.WebControlFunctionality.ConfirmMessage = "Are you sure you want to start the import?";
            f.Functionality.ClickSubmit += new EventHandler(Form_ClickSubmit);
        }



        private void addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE msgType, string msg)
        {
            ulMessages.Visible = true;
            MyListItem li = new MyListItem();
            li.CssManager.AddClass(CS.General_v3.Util.EnumUtils.StringValueOf(msgType));
            Literal lit = new Literal();
            lit.Text = CS.General_v3.Util.Text.HtmlEncode(msg);

            li.Controls.Add(lit);
            ulMessages.Controls.Add(li);
            
           // MyDiv spanMsg = new MyDiv();
           //// spanMsg.CssManager.AddClass(CS.General_v3.Util.EnumUtils.StringValueOf(msgType));
           // spanMsg.InnerHtml = CS.General_v3.Util.Text.HtmlEncode(msg);
           // phMessages.Controls.Add(spanMsg);
            
        }

        

        void Form_ClickSubmit(object sender, EventArgs e)
        {
            bool ok = true;


            var txtCSV = _fileUploadCSV;

            MyFileUpload txtZIPFileUpload = null;
            string zipPath = null;
            string zipFilename = "";
            if (this.Functionality.ShowZIPFile)
            {
                txtZIPFileUpload = _fileUploadZIP;
                zipPath = _txtZIPPath.GetFormValueAsString();
            }
            string uploadPath = "";
            if (this.Functionality.ShowUploadPath)
            {
                uploadPath = _txtUploadPath.GetFormValueAsString();
            }
            bool removeAnyExistingImages = false;
            if (this.Functionality.ShowRemoveAnyExistingImages)
            {
                removeAnyExistingImages = _chkRemoveAnyExistingImages.GetFormValueObjectAsBool();
            }
            string csvFilename = _fileUploadCSV.FileName;
            Stream zipFile = null;
            if (txtCSV.UploadedFileStream.Length > 0)
            {

                if (txtZIPFileUpload != null && txtZIPFileUpload.UploadedFileStream.Length > 0)
                {
                    zipFile = txtZIPFileUpload.UploadedFileStream;
                    zipFilename = txtZIPFileUpload.FileName;
                }
                else if (!string.IsNullOrEmpty(zipPath))
                {
                    string localPath = CS.General_v3.Util.PageUtil.MapPath(zipPath);
                    if (File.Exists(localPath))
                    {
                        zipFile = new MemoryStream(CS.General_v3.Util.IO.LoadFileAsByteArray(localPath));
                        zipFilename = zipPath;
                    }
                    else
                    {
                        ok = false;
                        addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "ZIP File does not exist at path <" + zipPath + ">");
                    }

                }

            }
            else
            {
                ok = false;
                addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Invalid data file");
                
            }

            
            
            if (ok)
            {

                

                CSVFile csvFile = CSVFile.LoadFromStream(txtCSV.UploadedFileStream, txtCSV.FileName);
                var importer = this.Functionality.Importer;
                importer.CurrentCsvFilename = csvFilename;
                importer.CurrentZipFilename = zipFilename;
                importer.RemoveAnyExistingImages = removeAnyExistingImages;
                if (zipFile != null && zipFile.Length > 0)
                {
                    importer.ExtractZIPFile(zipFile);
                }
                if (!string.IsNullOrWhiteSpace(uploadPath))
                    importer.AddPathsToLookForFiles(uploadPath);
                {
                    string settingsPath = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.ProductsBulkImportUploadPath);
                    if (!string.IsNullOrWhiteSpace(settingsPath))
                    {
                        importer.AddPathsToLookForFiles(settingsPath);
                    }
                }
                importer.ProcessFile(csvFile);
                CmsUtil.ShowStatusMessageInCMS("Import started successfully.  Refresh for progress.", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
                System.Threading.Thread.Sleep(1000); //so that some results might be available
                showResults();
            }


        }

        private void showResults()
        {
            checkIfRunning();
            var results = this.Functionality.Importer.Results;
            if (results != null && ( results.Lines.Count > 0 || (results.Lines.Count == 0 & results.GetResultType() != CS.General_v3.Enums.STATUS_MSG_TYPE.Success)))
            {
                divResults.Visible = true;
                var importer = this.Functionality.Importer;
                if (results.Lines.Count > 0)
                {
                    ResultsTable.Visible = true;
                    ResultsTable.FillFromImportResults(results);
                }
                var resultType = results.GetResultType();
                tdFilename.InnerText = importer.CurrentCsvFilename;
                string zipFilename = importer.CurrentZipFilename;
                if (!string.IsNullOrWhiteSpace(zipFilename))
                {
                    trZipFile.Visible = true;
                    tdZIPFile.InnerText = zipFilename;
                }
                if (importer.StartedOn.HasValue)
                {
                    tdStartedOn.InnerText = importer.StartedOn.Value.ToString("ddd d MMM yyyy hh:mmtt");
                }
                if (importer.FinishedOn.HasValue)
                {
                    tdFinishedOn.InnerText = importer.FinishedOn.Value.ToString("ddd d MMM yyyy hh:mmtt");
                }

                if (importer.IsFinished())
                    addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Success, "Data imported. Status: " + CS.General_v3.Util.EnumUtils.StringValueOf(resultType));
                else
                    addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Success, "Import Status: " + importer.CurrentLineIndex + " / " + importer.GetTotalLineCount());
                

                if (results.GeneralImportResult.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
                {
                    foreach (var msg in results.GeneralImportResult.StatusMessages)
                    {
                        addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, msg.Message);
                    }

                }
                else
                {
                    addStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Error importing data");
                }
            }

        }
        protected override void OnInit(EventArgs e)
        {
            initControls();
            base.OnInit(e);

        }

        private void checkRequirements()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(this.Functionality.Importer, "Importer must be defined!");
        }

        private void checkIfRunning()
        {
            if (this.Functionality.Importer.IsRunning())
            {
                formImport.Visible = false;
                divImportJobRunning.Visible = true;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            checkRequirements();
            base.OnLoad(e);
            initHandlers();
            checkIfRunning();
            initForm();
            showResults();
        }


    }
}