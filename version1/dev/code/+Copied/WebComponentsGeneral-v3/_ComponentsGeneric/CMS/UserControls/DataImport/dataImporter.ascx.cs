﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Cms.UserControls;
using BusinessLogic_v3.Classes.DataImport;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.DataImport
{
    public partial class dataImporter : BaseCmsUserControl
    {
        public enum DataImportSteps
        {
            UploadForm,
            ColumnChoice,
            Processing
        }



        #region dataImporter Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected dataImporter _item = null;
            public IDataImporterBase DataImporter { get; set; }

            internal FUNCTIONALITY(dataImporter item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, null);
                this._item = item;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion


        //public DataImportSteps GetCurrentStep()
        //{
        //    if (!this.Functionality.DataImporter.IsRunning())
        //    {
        //        var cols = this.Functionality.DataImporter.Parameters.ColumnsCollection;
        //        if (this.Functionality.DataImporter.Results ==null && this.Functionality.DataImporter.IsCsvFileLoaded())
        //            //if (this.Functionality.DataImporter.IsCsvFileLoaded() && cols != null && cols.Columns.Count > 0 && !cols.CheckIfAllCsvColumnIndexAreDefined().IsSuccessful)
        //        {// if columns are filled in, but not all of the column indexes for the CSV are defined, then
        //            return DataImportSteps.ColumnChoice;
        //        }
        //        else
        //        {
                    
        //            return DataImportSteps.UploadForm;
        //        }
        //    }
        //    else
        //    {
        //        return DataImportSteps.Processing;

        //    }
        //}

        private void initForm()
        {
            dataImportUploadForm.Functionality.DataImporter = this.Functionality.DataImporter;
                
            if (this.Functionality.DataImporter.CurrentStatus == ImportStatus.Idle)
            {
                
            }
            else
            {
                dataImportUploadForm.RemoveControlFromParentContainer();
            }
        }

        private void initColumnChoice()
        {
            
            dataImportColumnPreParser.Functionality.DataImporter = this.Functionality.DataImporter;

            if (this.Functionality.DataImporter.CurrentStatus == ImportStatus.LoadedCsv)
            {
                
                
            }
            else
            {
                dataImportColumnPreParser.RemoveControlFromParentContainer();
            }
        }
        private void initResults()
        {
            
            dataImportResults.Functionality.DataImporter = this.Functionality.DataImporter;
            if (this.Functionality.DataImporter.Results != null && this.Functionality.DataImporter.CurrentStatus != ImportStatus.LoadedCsv)
            {

                
            }
            else
            {
                dataImportResults.RemoveControlFromParentContainer();
            }
        }



        private void init()
        {
            initHandlers();
            initForm();
            initColumnChoice();
            initResults();
        }
        private void initHandlers()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            init();
        }
       

    }
}