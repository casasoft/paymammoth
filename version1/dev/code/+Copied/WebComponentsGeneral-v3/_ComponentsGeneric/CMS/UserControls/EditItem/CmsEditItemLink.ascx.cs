﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.UserControls.EditItem
{
    using System;
    using CS.WebComponentsGeneralV3.Code.Controls.UserControls;
    using BusinessLogic_v3.Classes.DbObjects; using BusinessLogic_v3.Classes.DbObjects.Factories; using BusinessLogic_v3.Classes.DbObjects.Objects;
    using BusinessLogic_v3.Frontend._Base;
    using CS.WebComponentsGeneralV3.Code.Cms.Util;
    using CS.General_v3.Util;
    using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
    using IBaseDbObject = BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject;

    public partial class CmsEditItemLink : BaseUserControl
    {

        public CmsEditItemLink()
        {
            this.Text = "Edit in CMS";
        }

        #region CmsEditItemLink Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected CmsEditItemLink _item = null;
            internal FUNCTIONALITY(CmsEditItemLink item)
            {
                ContractsUtil.RequiresNotNullable(item, "item");
                this._item = item;
            }

            public IBaseDbObject DbObject { get; set; }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion

        public string Text { get; set; }

        protected void showLink()
        {
            this.Visible = false;
            if (this.Parent.Visible)
            {
                IBaseDbObject dbItem = this.Functionality.DbObject;
                if (dbItem is BaseFrontend)
                {
                    dbItem = ((BaseFrontend)dbItem).Data;
                }
                if (this.Functionality.DbObject != null)
                {
                    var loggedInCmsUser = CmsUtil.GetLoggedInUser();
                    if (loggedInCmsUser != null)
                    {

                        var factory = CmsSystem.Instance.GetFactoryForType(dbItem.GetType());
                        if (factory != null)
                        {
                            var cmsItem = factory.GetCmsItemFromDBObject((BaseDbObject)dbItem);
                            if (cmsItem.CheckIfUserCanEdit(loggedInCmsUser))
                            {
                                string editLink = factory.GetEditUrlForItemWithID(dbItem.ID).ToString();
                                if (!string.IsNullOrWhiteSpace(editLink))
                                {
                                    aLink.InnerText = aLink.Title = this.Text;
                                    aLink.HRef = editLink;
                                    aLink.Target = "_blank";
                                        
                                    this.Visible = true;
                                }
                            }
                        }
                    }

                }

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected override void OnPreRender(EventArgs e)
        {
            
                
                this.Visible = this.Functionality.DbObject != null;
            
                showLink();
            
            base.OnPreRender(e);
        }
    }
}