﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.Members
{
    using System;
    using System.Collections.Generic;
    using CS.General_v3.Classes.HelperClasses;
    using CS.WebComponentsGeneralV3.Code.Cms.Pages;
    using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
    using CS.WebComponentsGeneralV3.Cms.MemberModule;
    using CS.WebComponentsGeneralV3.Code.Cms.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public partial class updateAccountBalance : BaseCMSPage
    {
        private MyDropDownList _cmbUpdateType;
        private MyTxtBoxString _txtComment;
        private MyTxtBoxNumber _txtAmount;

        private void setAccess()
        {
            this.Functionality.AccessRequired.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
        }
        public updateAccountBalance()
        {
            
            setAccess();
        }

        private MemberBaseCmsInfo _member = null;

        private void checkMember()
        {
            long? id = MemberBaseCmsFactory.Instance.GetIDFromCurrentQuerystring();
            if (id.HasValue)
                _member = MemberBaseCmsFactory.Instance.GetCmsItemWithId(id.Value);
            if (_member == null)
            {
                CmsUtil.ShowStatusMessageInCMS("No member specified", CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
                MemberBaseCmsFactory.Instance.GetListingUrl().RedirectTo();
            }
        }

        private void setPageTitle()
        {
            
            List<PageTitle> list = new List<PageTitle>();

            var pgTitles = MemberBaseCmsFactory.Instance.GetEditPageTitlesForThisAndLinked();
            list.AddRange(pgTitles);
            list.Add(new PageTitle("Update Account Balance"));
            this.Master.Functionality.SetPageTitles(list);

        }
        private void updateMemberDetails()
        {
            divMemberDetails.InnerText = _member.DbItem.GetFullName() + " [" + _member.ID + "]";
            divCurrentBalance.InnerText = _member.DbItem.AccountBalance.ToString("0.00");
        }

        private void initForm()
        {
            

            _cmbUpdateType = this.formFields.Functionality.AddSingleChoiceListFromEnum<BusinessLogic_v3.Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE>("txtUpdateType", "Update Type", true, "The type of update, e.g Deposit, Adjustment, etc.  Normally, leave it as 'adjustment'.",null, 
                BusinessLogic_v3.Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE.Adjustment);
            _txtComment = this.formFields.Functionality.AddString("txtComment","Comment", true, "Comment describing the update",null);
            _txtAmount = this.formFields.Functionality.AddDouble("txtAmount", "Amount", true, "Amount of update (can also be negative)", null);
            this.formFields.Functionality.ButtonSubmit.Text = "Update Balance";
            this.formFields.Functionality.ButtonSubmit.WebControlFunctionality.ConfirmMessage = "Are you sure you want to update this member's balance?";
            this.formFields.Functionality.ClickSubmit += new EventHandler(Functionality_ClickSubmit);
        }

        void Functionality_ClickSubmit(object sender, EventArgs e)
        {
            var updateType = _cmbUpdateType.GetFormValueAsEnum<BusinessLogic_v3.Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE>();
            double amount = _txtAmount.GetFormValueAsDoubleNullable().GetValueOrDefault();
            string comment = _txtComment.GetFormValueAsString();
            OperationResult result = new OperationResult();
            try
            {
                _member.DbItem.UpdateAccountBalance(amount, updateType, comment, BusinessLogic_v3.Enums.MEMBER_ACCOUNT_BALANCE_UPDATE_TYPE_BALANCE_CHANGE_TYPE.AccountBalance);
            }
            catch (Exception ex)
            {
                result.AddException(ex);
            }

            this.Master.Functionality.ShowStatusMessageAndRedirectToSamePage(result);
        }

        

        protected void Page_Load(object sender, EventArgs e)
        {
            checkMember();
            setPageTitle();
            initForm();
            updateMemberDetails();
            


        }
    }
}