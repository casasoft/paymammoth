﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting;
using CS.WebComponentsGeneralV3.Code.Cms.Reporting.Attributes;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.Reports
{
    public partial class report : BaseCMSPage
    {

        private const string Param_ReportGenerated = "Generate";
        public List<PageTitle> GetCmsPageTitles()
        {
            List<PageTitle> list = new List<PageTitle>();
            list.AddRange(CMS.Pages.Reports._default.GetCmsPageTitles());
            list.Add(new PageTitle(_reportGenerator.ReportTitle + " (" + _reportGenerator.ReportSection + ")", CS.WebComponentsGeneralV3.Code.Cms.Routing.Reporting.ReportingRoutes.GetReportUrl(_reportGenerator)));
            return list;
        }

        public bool GenerateReport
        {
            get 
            {
                return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<bool>(Param_ReportGenerated);
            }
        }


        private ICmsReportGenerator _reportGenerator = null;
        private ICmsReport _report= null;

        private Dictionary<string, MyInputBase> _formFieldList = null;

        public report()
        {
            _formFieldList = new Dictionary<string, MyInputBase>();
        }

        private void loadReport()
        {
            _reportGenerator = CS.WebComponentsGeneralV3.Code.Cms.Routing.Reporting.ReportingRoutes.GetCmsReportFromRouteParam();
            if (_reportGenerator == null)
            {
                CS.General_v3.Util.PageUtil.RedirectPage(CS.WebComponentsGeneralV3.Code.Cms.Routing.Reporting.ReportingRoutes.GetListingUrl());
            }
            _report = _reportGenerator.CreateNewReport();

            if (!string.IsNullOrWhiteSpace(_reportGenerator.ReportDescriptionHtml))
            {
                pDescription.InnerHtml = _reportGenerator.ReportDescriptionHtml;
            }
            else
            {
                pDescription.Visible = false;
            }

        }

        private void generateForm()
        {
            var reportParams = _report.GetReportParams();

            var propertiesList = CS.General_v3.Util.ReflectionUtil.GetAllPropertiesWithAttributes(reportParams.GetType(), typeof(CmsReportParamAttribute));
            bool atLeastOne = false;
            foreach (var p in propertiesList)
            {
                
                CmsReportParamAttribute reportParamAttrib = (CmsReportParamAttribute) p.Attributes.FirstOrDefault();
                string fieldID = reportParamAttrib.GetFieldId(p.Property);
                var input = reportParamAttrib.AddToFormField(this.formReportParams, p.Property);

                _formFieldList[fieldID] = input;
                atLeastOne = true;
            }

            this.formReportParams.Functionality.ClickSubmit += new EventHandler(formReportParams_ClickSubmit);
            this.formReportParams.Functionality.ButtonSubmitText = "Generate";
            if (atLeastOne)
            {
                this.formReportParams.Functionality.ClickCancel += new EventHandler(formReportParams_ClickCancel);
                this.formReportParams.Functionality.ButtonCancelText = "Clear";
            }

        }

        void formReportParams_ClickCancel(object sender, EventArgs e)
        {
            CS.General_v3.Classes.URL.URLClass url = new General_v3.Classes.URL.URLClass();
            url[Param_ReportGenerated] = null;
            var reportParams = _report.GetReportParams();

            var propertiesList = CS.General_v3.Util.ReflectionUtil.GetAllPropertiesWithAttributes(reportParams.GetType(), typeof(CmsReportParamAttribute));
            foreach (var p in propertiesList)
            {


                CmsReportParamAttribute reportParamAttrib = (CmsReportParamAttribute)p.Attributes.FirstOrDefault();
                string fieldID = reportParamAttrib.GetFieldId(p.Property);

                
                url[fieldID] = null;




            }

            CmsUtil.ShowStatusMessageInCMS("Report cleared", General_v3.Enums.STATUS_MSG_TYPE.Information);
            url.RedirectTo();
            
        }

        void formReportParams_ClickSubmit(object sender, EventArgs e)
        {

            var reportParams = _report.GetReportParams();


            CS.General_v3.Classes.URL.URLClass url = new General_v3.Classes.URL.URLClass();

            var propertiesList = CS.General_v3.Util.ReflectionUtil.GetAllPropertiesWithAttributes(reportParams.GetType(), typeof(CmsReportParamAttribute));
            foreach (var p in propertiesList)
            {

                
                CmsReportParamAttribute reportParamAttrib = (CmsReportParamAttribute)p.Attributes.FirstOrDefault();
                string fieldID = reportParamAttrib.GetFieldId(p.Property);

                var input = _formFieldList[fieldID];
                var formValue = input.GetFormValueObject();

                url[fieldID] = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(formValue);

               

                
            }

            url[Param_ReportGenerated] = "Y";
            CmsUtil.ShowStatusMessageInCMS("Report generated successfully", General_v3.Enums.STATUS_MSG_TYPE.Success);
            url.RedirectTo();

        }
        private void fillReportParams()
        {
             var reportParams = _report.GetReportParams();


            
            var propertiesList = CS.General_v3.Util.ReflectionUtil.GetAllPropertiesWithAttributes(reportParams.GetType(), typeof(CmsReportParamAttribute));
            foreach (var p in propertiesList)
            {


                CmsReportParamAttribute reportParamAttrib = (CmsReportParamAttribute)p.Attributes.FirstOrDefault();

                string fieldID = reportParamAttrib.GetFieldId(p.Property);

                string sValue = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(fieldID);

                Type pType = p.Property.PropertyType;

                object o = CS.General_v3.Util.Other.ConvertStringToBasicDataType(sValue, pType);

                
                p.Property.SetValue(reportParams, o, null);


            }


        }

        private void checkToGenerateReport()
        {
            if (GenerateReport)
            {
                
                fillReportParams();
                generateReport();
                generateReportFooter();
            }
        }

        private void generateReport()
        {
            divReport.Visible = true;
            var reportCtrl = _report.Generate();
            divReport.Controls.Add(reportCtrl);
            
        }



        private void generateReportFooter()
        {
            var footer = _report.GenerateFooter();
            if (footer != null)
            {
                phFooter.Controls.Add(footer);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            loadReport();
            generateForm();
            checkToGenerateReport();

            this.Master.Functionality.SetPageTitle(GetCmsPageTitles().ToArray());
        }
    }
}