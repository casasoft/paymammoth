﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using System.Reflection;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.Export
{
    public partial class chooseFields : BaseCMSPage
    {
        private ICmsItemFactory _factory = null;
        private const string Param_Export = "Export";
        private const string QueryStringFieldPrepend = "field_";
        private void setFactory()
        {
            string title = getFactoryTitleFromRouteData();
            var cmsFactory = CmsSystem.Instance.GetCmsFactoryByTitle(title);
            if (cmsFactory != null)
            {


                _factory = cmsFactory;
            }
            else
            {
                CS.General_v3.Util.PageUtil.RedirectPage("/");

            }
        }

        public static string GetQsId(string id)
        {
            return QueryStringFieldPrepend + id;
        }


        private Dictionary<string, MyFormWebControl> _formFieldsInfo = new Dictionary<string, MyFormWebControl>();


        private void fillForm()
        {
            var loggedUser = CmsUtil.GetLoggedInUser();
            var emptyItem = _factory.GetEmptyCmsItemInfo();

            var properties = emptyItem.GetCmsProperties().ToList();
            properties.SortPropertyListByPrioritizer(emptyItem.GetEditOrderPrioritizer());


            foreach (var p in properties)
            {

                if (p.CheckIfUserCanView(loggedUser))
                {
                    string id = p.GetPropertyIdentifier();
                    string title = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(p.Label);
                    _formFieldsInfo[id] = formFieldList.Functionality.AddBool(id, title, false, p.HelpMessage, initialValue: true);
                }


            }

            formFieldList.Functionality.ButtonSubmitText = "Export";
            formFieldList.Functionality.ClickSubmit += new EventHandler(formFieldList_ClickSubmit);
        }

        void formFieldList_ClickSubmit(object sender, EventArgs e)
        {
            CS.General_v3.Classes.URL.URLClass url = new General_v3.Classes.URL.URLClass();

            foreach (var key in _formFieldsInfo.Keys)
            {
                var field = _formFieldsInfo[key];
                string qsId = GetQsId(key);
                bool formValue= field.GetFormValueAsBool();
                if (formValue)
                    url[qsId] = formValue;
                else
                    url.Remove(qsId);


            }
            url[Param_Export] = "Y";
            CmsUtil.ShowStatusMessageInCMS("Data exported successfully", General_v3.Enums.STATUS_MSG_TYPE.Success);
            url.RedirectTo();

            
        }



        private void checkToExportData()
        {
            if (CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<bool>(Param_Export))
            {
                DataExporter exporter = new DataExporter(_factory);
                var report = exporter.CreateReport();
                divExport.Visible = true;
                divExport.Controls.Add(report);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            setFactory();
            fillForm();
            checkToExportData();

            List<PageTitle> pageTitles = new List<PageTitle>();
            pageTitles.AddRange(_factory.GetListingPageTitlesForThisAndLinked());
            pageTitles.Add(new PageTitle("Export"));


            Master.Functionality.SetPageTitles(pageTitles);
             
        }
    }
}