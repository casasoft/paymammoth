<%@ Page Language="C#" MasterPageFile="~/_ComponentsGeneric/CMS/Pages/cms.master" AutoEventWireup="true" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.welcome" Codebehind="welcome.aspx.cs" %>
<%@ Register Src="/_ComponentsGeneric/CMS/usercontrols/ucWelcomePage.ascx" TagName="WelcomePage" TagPrefix="CMS" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" Runat="Server">
<CMS:WelcomePage id="welcomePage" runat="server"></CMS:WelcomePage>
    
</asp:Content>

