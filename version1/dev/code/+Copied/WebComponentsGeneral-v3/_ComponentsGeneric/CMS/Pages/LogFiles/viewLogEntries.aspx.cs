﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using BusinessLogic_v3.Classes.log4netClasses;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;


using CS.General_v3.Classes.CSS;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Classes.Log4Net;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.LogFiles
{
    using Code.Cms.WebControls.ListingClasses;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public partial class viewLogEntries : BaseCMSPage
    {
        private MyTxtBoxString _txtThread;
        private MyTxtBoxString _txtMessage;
        private MyTxtBoxString _txtLogger;
        private MyListBox _listBoxLevelTypes;

        public viewLogEntries()
        {
            initAccess();
        }
        private void initAccess()
        {
            this.Functionality.AccessRequired.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
        }
        private void initSearchForm()
        {
            var form = this.formSearch;
            Log4NetMsgSearchParams searchParams = new Log4NetMsgSearchParams();
            string thread = (searchParams.ThreadID.Count > 0 ? searchParams.ThreadID[0] : null);
            string message = (searchParams.Message.Count > 0 ? searchParams.Message[0] : null);
            string logger = (searchParams.LoggerName.Count > 0 ? searchParams.LoggerName[0] : null);
            //string className = (searchParams.className.Count > 0 ? searchParams.className[0] : null);
            form.Functionality.ButtonCancel.Text = "Clear";
            form.Functionality.ButtonSubmit.Text = "Search";
            form.Functionality.ButtonSubmitText = "Search";
            form.Functionality.ButtonCancelText = "Clear";
            _txtThread = form.Functionality.AddString("txtThread", "Thread", false, null, null, thread);
            _txtMessage = form.Functionality.AddString("txtMessage", "Message", false, null, null, message);
            _txtLogger = form.Functionality.AddString("txtLogger", "Logger", false, null, null, logger);
           // form.AddString("txtClassName", "Class Name", false, null, null, null);

            _listBoxLevelTypes = form.Functionality.AddMultipleChoiceListFromEnum<GeneralLog4Net.LOG4NET_TYPE>(id: "txtLevelTypes", title: "Level Types", required: false, helpMessage: null, selectedValues: searchParams.LevelTypes, jQueryMultiSelectParams: new Code.Classes.Javascript.UI.jQuery.MultiSelect._jQueryMultiSelectParams()
            {
                
            });
            //form.AddEnumMultipleChoice < GeneralLog4Net.LOG4NET_TYPE>("txtLevelTypes", "Level Types", false, null, initValues: searchParams.LevelTypes);
        }

        private void initHandlers()
        {
            this.btnRefresh.Click += new EventHandler(btnRefresh_Click);
            this.formSearch.Functionality.ClickSubmit += new EventHandler(formSearch_ClickSubmit);
            this.formSearch.Functionality.ClickCancel += new EventHandler(formSearch_ClickCancel);
            this.listing.Functionality.ItemDataBound += new Listing.ItemHandler(Functionality_ItemDataBound);

        }

        void btnRefresh_Click(object sender, EventArgs e)
        {
            Log4NetXmlReader loader = Log4NetXmlReader.InstanceInSession;
            loader.Refresh();
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Log entries refreshed successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);

        }

        void formSearch_ClickCancel(object sender, EventArgs e)
        {
            Log4NetMsgSearchParams searchParams = new Log4NetMsgSearchParams(customUrl: "");
            CmsUtil.ShowStatusMessageInCMS("Searched cleared", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
            searchParams.Redirect();

        }

        void formSearch_ClickSubmit(object sender, EventArgs e)
        {
            string thread = _txtThread.GetFormValue();
            string message = _txtMessage.GetFormValue();
            string logger = _txtLogger.GetFormValue();
            var levelTypes =  _listBoxLevelTypes.GetFormValueAsListOfEnum<GeneralLog4Net.LOG4NET_TYPE>();// _listBoxLevelTypes.GetFormValueAsInt formSearch.GetFormValueAsListOfEnums<GeneralLog4Net.LOG4NET_TYPE>("txtLevelTypes");
          //  string className = formSearch.GetFormValueStr("txtClassName");
            Log4NetMsgSearchParams searchParams = new Log4NetMsgSearchParams(customUrl: "");
            searchParams.ThreadID.Add(thread);
            searchParams.Message.Add(message);
            searchParams.LoggerName.Add(logger);
            searchParams.LevelTypes.AddRange(levelTypes);
         //   searchParams.Message.Add(className);
            CmsUtil.ShowStatusMessageInCMS("Searched successfully!", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
            searchParams.Redirect();
        }






        private void viewLogFiles(IEnumerable<string> paths)
        {
            Log4NetXmlReader loader = Log4NetXmlReader.InstanceInSession;
            loader.ReadXmlFiles(paths.ToArray());

            string viewUrl = CmsRoutesMapper.Instance.GetLogFiles_ViewPage();
            CS.General_v3.Util.PageUtil.RedirectPage(viewUrl);

            
        }

        private void initColumns()
        {

            this.listing.Functionality.AddColumn(new ColumnData("Index", null, CS.General_v3.Enums.SORT_TYPE.None, false, "logresults-index"));
            this.listing.Functionality.AddColumn(new ColumnData("Date/Time", null, CS.General_v3.Enums.SORT_TYPE.None, false, "logresults-datetime"));
            this.listing.Functionality.AddColumn(new ColumnData("Level", null, CS.General_v3.Enums.SORT_TYPE.None, false, "logresults-level"));
            this.listing.Functionality.AddColumn(new ColumnData("Thread", null, CS.General_v3.Enums.SORT_TYPE.None, false, "logresults-thread"));
            this.listing.Functionality.AddColumn(new ColumnData("Logger", null, CS.General_v3.Enums.SORT_TYPE.None, false, "logresults-logger"));
            this.listing.Functionality.AddColumn(new ColumnData("Message", null, CS.General_v3.Enums.SORT_TYPE.None, false, "logresults-message"));
            this.listing.Functionality.AddColumn(new ColumnData("Class", null, CS.General_v3.Enums.SORT_TYPE.None, false, "logresults-class"));
            this.listing.Functionality.AddColumn(new ColumnData("Method", null, CS.General_v3.Enums.SORT_TYPE.None, false, "logresults-method"));

            this.listing.Functionality.AddItemLinkButton("View Detailed", "", item =>
            {
                Log4NetMsg msg = (Log4NetMsg)item;
                return CmsRoutesMapper.Instance.GetLogFiles_ViewLogItem_Detailed(msg.Index);

            });
            
        }

        private void loadListing()
        {
            var data = Log4NetXmlReader.InstanceInSession;



            Log4NetMsgSearcher searcher = new Log4NetMsgSearcher(data.GetLastResult());
            int totalSearchResults = 0;
            var searchResults = searcher.Search(null, out totalSearchResults);


            
            int totalPages, fromIndex, toIndex, fromPageIndex, toPageIndex;
            CS.General_v3.Util.Paging.GetPagingInfo(searcher.SearchParams.PagingInfo.ShowAmount,
                searcher.SearchParams.PagingInfo.PageNo, totalSearchResults, 999999, out fromIndex, out toIndex, out totalPages, out fromPageIndex, out toPageIndex);

            this.listing.Functionality.QueryStringVarNameShowAmt = searcher.SearchParams.PagingInfo.GetShowAmtKey();
            this.listing.Functionality.QueryStringVarNamePage = searcher.SearchParams.PagingInfo.GetPageNo_QuerystringVar();
            this.listing.Functionality.AmtPages = totalPages;
            this.listing.Functionality.TotalItems = totalSearchResults;

            this.listing.Functionality.DataSource = searchResults;
            this.listing.Functionality.DataBind();

        }

        private List<MyCheckBox> chkBoxes = new List<MyCheckBox>();

        

        bool Functionality_ItemDataBound(CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses.ListingItemRow row)
        {
            Log4NetMsg msg= (Log4NetMsg) row.Object;
            

            
            int index = 0;

            CSSManager css = new CSSManager(row.Row);
            css.AddClass(msg.Level);
            {
                Literal lit = new Literal();
                lit.Text = CS.General_v3.Util.Text.HtmlEncode(msg.Index.ToString());
                row.DataCells[index].Controls.Add(lit);
                index++;
            }
            {
                Literal lit = new Literal();
                lit.Text = CS.General_v3.Util.Text.HtmlEncode(msg.TimeStamp.ToString("dd/MMM/yy hh:mm:ss.fff tt"));
                row.DataCells[index].Controls.Add(lit);
                index++;
            }
            {
                Literal lit = new Literal();
                lit.Text = CS.General_v3.Util.Text.HtmlEncode(msg.Level);
                row.DataCells[index].Controls.Add(lit);
                index++;
            }
            {
                Literal lit = new Literal();
                lit.Text = CS.General_v3.Util.Text.HtmlEncode(msg.Thread);
                row.DataCells[index].Controls.Add(lit);
                index++;
            }
            {
                Literal lit = new Literal();
                lit.Text = CS.General_v3.Util.Text.HtmlEncode(msg.LoggerName);
                row.DataCells[index].Controls.Add(lit);
                index++;
            }
            {
                Literal lit = new Literal();

                
                string msgTrim = msg.Message;
                
                
                msgTrim = CS.General_v3.Util.Text.LimitText(msgTrim, 300);
                msgTrim = CS.General_v3.Util.Text.HtmlEncode(msgTrim); 
                msgTrim = CS.General_v3.Util.Text.TxtForHTML(msgTrim);
                lit.Text = msgTrim;

                row.DataCells[index].Controls.Add(lit);
                index++;
            }
            
            {
                Literal lit = new Literal();
                lit.Text = CS.General_v3.Util.Text.HtmlEncode(msg.Class);
                row.DataCells[index].Controls.Add(lit);
                index++;
            }
            {
                Literal lit = new Literal();
                lit.Text = CS.General_v3.Util.Text.HtmlEncode(msg.Method);
                row.DataCells[index].Controls.Add(lit);
                index++;
            }
            
            
            return true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.Functionality.SetPageTitle(new PageTitle("Log Browser",CmsRoutesMapper.Instance.GetLogFiles_ListingPage()),
                new PageTitle("View log entries",null));
            this.Master.Functionality.GoBackURL = CmsRoutesMapper.Instance.GetLogFiles_ListingPage();
            

            initHandlers();

            initColumns();
            initSearchForm();
            loadListing();
        }
    }
}