﻿namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages
{
    using System;
    using CS.WebComponentsGeneralV3.Code.Cms;
    using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
    using CS.WebComponentsGeneralV3.Code.Cms.WebControls;

    public partial class cms_master : CS.WebComponentsGeneralV3.Code.Cms.Pages.BaseCMSMasterPage
    {
        private bool _controlsInit = false;
        protected override void initControls()
        {

            if (!_controlsInit)
            {
                _controlsInit = true;
                base._ltlTitle = ltlTitle;
                base._topButtons = topButtons;
                base._logo = imgLogo;
                base._ltlCurrentTitle = ltlCurrentTitle;
                base._mainMenu = mainMenu;
                base._divDate = divDate;
                base._divStatusMessage = divMessage;

                base._divGoBack = divGoBack;
                base._aGoBack = aGoBack;

                base.initControls();

            }

        }


        private void checkSubLogo()
        {
            var cmsUser = getCmsLoggedUser();
            var cmsUserFrontend = cmsUser;

            if (cmsUserFrontend != null && !cmsUserFrontend.CmsCustomLogo.IsEmpty())
            {
                divSubLogo.Visible = true;
                imgSubLogo.ImageUrl = cmsUserFrontend.CmsCustomLogo.GetSpecificSizeUrl(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase.CmsCustomLogoSizingEnum.Normal);
                imgSubLogo.HRef = cmsUser.CmsCustomLogoLinkUrl;
                imgSubLogo.HRefTarget = CS.General_v3.Enums.HREF_TARGET.Blank;

            }

        }
    

        protected void addAdditionalMenuItems()
        {
            var additionalMenus = CmsSystem.Instance.GetCustomAdditionalMenuItems();
            foreach (var menuItem in additionalMenus)
            {
                this.Functionality.MainMenu.AddItem(menuItem);
            }
        }

        void Master_AddAdditionalMenuItems()
        {
            addAdditionalMenuItems();
        }

        /*
                protected BusinessLogic_v3.OldCMS.Collections.MainMenuItem addDefaultMenuItem(IObjectPageInfo pageInfo, BusinessLogic_v3.Classes.Cms.Enums.IMAGE_ICON icon)
                {
                    string folderRoot = CS.General_v3.Classes.Settings.SettingsMain.CMS.GetRoot() + pageInfo.Folder + "/";
                    string indexPageName = pageInfo.IndexPageName;
                    return this.addDefaultMenuItem(folderRoot, pageInfo.Title_SingleUppercase, pageInfo.Title_PluralUppercase, icon, indexPageName);
                }*/

        /// <summary>
        /// Does not add the 'edit' links
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="titleSingular"></param>
        /// <param name="titlePlural"></param>
        /// <param name="icon"></param>
        /// <param name="indexPageName"></param>
        /// <returns></returns>
        protected MainMenuItem addDefaultMenuItem(string folder, string titleSingular, string titlePlural,
            EnumsCms.IMAGE_ICON icon, string indexPageName)
        {
            return addDefaultMenuItem(folder, titleSingular, titlePlural, icon, indexPageName, null);
        }
        protected MainMenuItem addDefaultMenuItem(string folder, string titleSingular, string titlePlural,
           EnumsCms.IMAGE_ICON icon, string indexPageName, string editPageName)
        {


            string rootPageURL = folder;
            if (!rootPageURL.EndsWith("/")) rootPageURL += "/";
            if (string.IsNullOrEmpty(editPageName))
                rootPageURL += indexPageName;
            MainMenuItem menuItem = new MainMenuItem(titlePlural, rootPageURL, icon);

            /* if (!string.IsNullOrEmpty(editPageName))
             {
                 menuItem.AddChild(new BusinessLogic_v3.OldCMS.Collections.MainMenuItem("Manage " + titlePlural,
                      folder + "/" + indexPageName, BusinessLogic_v3.Classes.Cms.Enums.IMAGE_ICON.Listing));
                 menuItem.AddChild(new BusinessLogic_v3.OldCMS.Collections.MainMenuItem("Add new " + titleSingular,
                     folder + "/" + editPageName + "?id=0", BusinessLogic_v3.Classes.Cms.Enums.IMAGE_ICON.Add));

             }*/
            Functionality.MainMenu.AddItem(menuItem);
            return menuItem;
        }


       
        protected override void OnPreRender(EventArgs e)
        {

            base.OnPreRender(e);
        }
        
     



        private void initCopyrightYears()
        {
            copyrightYear2.Text = copyrightYear1.Text = CS.General_v3.Util.Date.Now.Year.ToString();
        }
        private void checkCasaSoftLogos()
        {
            aCasaSoftLink.Visible = spanPoweredByCasaSoft.Visible = (this.Functionality.ShowCasaSoftLogos);



        }
        private void updateJS()
        {
            jsCMSVersion.Text = CS.General_v3.Settings.Others.JAVASCRIPT_VERSION;
            jsCMSVersion2.Text = CS.General_v3.Settings.Others.JAVASCRIPT_VERSION;
        }
        protected override void OnLoad(EventArgs e)
        {
            //this.renewSession.RenewURL = "/_common/aspx/renewSession.aspx";
           

            this.renewSession.RenewInterval = 1 * 60;//1 minutes
            this.renewSession.ResolveURL = false;
            initCopyrightYears();
            checkCasaSoftLogos();
            updateJS();
            checkSubLogo();
            base.OnLoad(e);

        }



        public override ButtonsBar ButtonsBar
        {
            get { return this.buttonsBar; }
        }

       

        protected override void OnInit(EventArgs e)
        {

            this.AddAdditionalMenuItems += new Action(Master_AddAdditionalMenuItems);
            
           // BusinessLogic.v1.CMS.Pages.BaseMasterPage.FUNCTIONALITY.Languages = GentesGame.DB.Languages.Language.GetList();
            this.Functionality.ShowLanguageSelect = true;
           
            base.OnInit(e);
        }

        

        

    }
}