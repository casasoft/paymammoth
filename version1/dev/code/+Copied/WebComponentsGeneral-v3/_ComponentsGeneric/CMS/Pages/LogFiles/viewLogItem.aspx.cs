﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using BusinessLogic_v3.Classes.log4netClasses;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;



using CS.General_v3.Classes.CSS;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.LogFiles
{
    public partial class viewLogItem : BaseCMSPage
    {
       
        public int? GetLogIDFromQuerystring()
        {
            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<int?>(CmsRoutesMapper.QUERYSTRING_ID);
        }

        public viewLogItem()
        {
            initAccess();
        }
        private void initAccess()
        {
            this.Functionality.AccessRequired.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
        }
        private Log4NetMsg loadItem()
        {
            Log4NetMsg result = null;
            var data = Log4NetXmlReader.InstanceInSession;
            if (data != null)
            {


                var items = data.GetLastResult();
                result = items.Find(item => item.Index == GetLogIDFromQuerystring());
                
            }
            return result;
        }

        private void loadLogData()
        {
            var item = loadItem();
            if (item != null)
            {
                this.formLog.Functionality.AddGenericLabelAndControl("Date/Time:", false, new Literal {Text = CS.General_v3.Util.Text.HtmlEncode(item.TimeStamp.ToString("dd/MMM/yyyy hh:mm:sstt"))});
                this.formLog.Functionality.AddGenericLabelAndControl("Index:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.Index.ToString()) });
                this.formLog.Functionality.AddGenericLabelAndControl("Item:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.Item.ToString()) });
                this.formLog.Functionality.AddGenericLabelAndControl("Level:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.Level) });
                this.formLog.Functionality.AddGenericLabelAndControl("Logger Name:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.LoggerName) });
                this.formLog.Functionality.AddGenericLabelAndControl("Message:", false, new Literal { Text = CS.General_v3.Util.Text.TxtForHTML(item.Message) });
                this.formLog.Functionality.AddGenericLabelAndControl("Throwable:", false, new Literal { Text = CS.General_v3.Util.Text.TxtForHTML(item.Throwable) });
                this.formLog.Functionality.AddGenericLabelAndControl("Thread:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.Thread) });
                this.formLog.Functionality.AddGenericLabelAndControl("Class:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.Class) });
                this.formLog.Functionality.AddGenericLabelAndControl("Method:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.Method) });
                this.formLog.Functionality.AddGenericLabelAndControl("Line:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.Line) });
                this.formLog.Functionality.AddGenericLabelAndControl("File:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.File) });
                this.formLog.Functionality.AddGenericLabelAndControl("App:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.App) });
                this.formLog.Functionality.AddGenericLabelAndControl("Machine Name:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.MachineName) });
                this.formLog.Functionality.AddGenericLabelAndControl("Username:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.UserName) });
                this.formLog.Functionality.AddGenericLabelAndControl("Hostname:", false, new Literal { Text = CS.General_v3.Util.Text.HtmlEncode(item.HostName) });
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.Functionality.SetPageTitle(
                new PageTitle("Log Browser", CmsRoutesMapper.Instance.GetLogFiles_ListingPage()),
                new PageTitle("View log entries", CmsRoutesMapper.Instance.GetLogFiles_ViewPage()),
                    new PageTitle("Log Entry (Detailed)", CmsRoutesMapper.Instance.GetLogFiles_ViewPage())
                    ) ;
            this.Master.Functionality.GoBackURL = CmsRoutesMapper.Instance.GetLogFiles_ViewPage();
            loadLogData();
            
        }
    }
}