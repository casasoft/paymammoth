using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Cms;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Classes.CSS;
using CS.General_v3.Classes.HelperClasses;

using CS.General_v3.Classes.URL;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;
using CS.WebComponentsGeneralV3.Code.Cms;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsDbOperations;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using CS.WebComponentsGeneralV3.Code.Cms.Routing.Exporting;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages
{
    public partial class listingPage : CS.WebComponentsGeneralV3.Code.Cms.Pages.CmsBaseListingPage
    {

        public bool QuerystringParam_CheckForUntranslatedText
        {
            get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<bool>(BusinessLogic_v3.Constants.ParameterNames.Cms_CheckForUntranslatedText); }
        }

        public listingPage()
        {
           

        }
        private void setFactory()
        {
            string title = getFactoryTitleFromRouteData();
            var cmsFactory = CmsSystem.Instance.GetCmsFactoryByTitle(title);
            if (cmsFactory != null)
            {
               

                this.Functionality.CmsFactory = cmsFactory;
            }
            else
            {
                CS.General_v3.Util.PageUtil.RedirectPage("/");
                
            }
        }

        protected override void OnPreInit(EventArgs e)
        {
            setFactory();
            base.OnPreInit(e);
        }
        

          protected string getQsParam_ShowDeletedItem()
        {
            return this.factory.Name + "_ShowDel";
        }

        
        public class CmsBaseListingPageFunctionality : BaseCMSPage.FUNCTIONALITY
        {
            public override CmsAccessType AccessRequired
            {
                get
                {
                    return this.CmsFactory.UserSpecificGeneralInfoInContext.GetAccessRequiredToView();
                }
            }
            public CmsBaseListingPageFunctionality(CmsBaseListingPage pg)
                : base(pg)
            {

            }
            public ICmsItemFactory CmsFactory { get; set; }
        
        }
        public new CmsBaseListingPageFunctionality Functionality
        {
            get
            {
                return (CmsBaseListingPageFunctionality)base.Functionality;
            }
        }
        protected override BasePageBase.FUNCTIONALITY getFunctionality()
        {
            return new CmsBaseListingPageFunctionality(this);
        }
        protected ICmsItemFactory factory
        {
            get
            {
                return this.Functionality.CmsFactory;
            }
        }

        
        protected List<CmsItemFormData> itemsFormData = null;
       
        private void checkCustomCmsOperations()
        {
            var customOperations = this.factory.GetCustomCmsOperations();
            if (customOperations != null)
            {
                foreach (var customOp in customOperations)
                {
                    customOp.AddToButtonsBar(this.Master.ButtonsBar);
                }
            }
        }
        private void addExportLinkToButtonsBar()
        {
            {
                var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
                var userSpecificInfo = this.Functionality.CmsFactory.UserSpecificGeneralInfoInContext;
                if (userSpecificInfo.CheckIfUserCanExport(loggedUser))
                {

                    string url = ExportRoutes.GetExportUrl(factory, this.CurrentSortBy, this.CurrentSortType, searchFields);

                    var btn = this.Master.ButtonsBar.AddLink("Export", EnumsCms.IMAGE_ICON.Export,url);
                }

            }
        }

        private void initButtonsBar()
        {
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
            var userSpecificInfo = this.Functionality.CmsFactory.UserSpecificGeneralInfoInContext;
            
            addExportLinkToButtonsBar();
            checkCustomCmsOperations();

            /*  {
            MyImageButtonWithText btnSearch = this.Master.ButtonsBar.AddButton("Search",  BusinessLogic.v1.CMS.Enums.IMAGE_ICON.Search);
            btnSearch.ButtonMode = MyImageButtonWithText.BUTTON_MODE.Link; 
            searchFields.ShowWithButton = btnSearch;
        }*/
            {
                //MyImageButtonWithText btnExportListing = this.Master.ButtonsBar.AddButton("Export" BusinessLogic.v1.CMS.Enums.IMAGE_ICON.Export);
                //btnExportListing.Click += new EventHandler(btnExportListing_Click);

            }
            //initButtonsAfter();
        }
        


        void btnExportListing_Click(object sender, EventArgs e)
        {
            //GentesGameDeploy.cms.Advert_CMS.StandardExportPage.LastSearchCriteria = this.Criteria;

           // CS.General_v3.Util.Page.Respo2nse.Redirec2t("standardExport.aspx");
        }
        
        private void initHandlers()
        {

            
        }

        void btnListingUpdate_Click(object sender, EventArgs e)
        {
            OperationResult result = new OperationResult();


          
         
            for (int i = 0; i < this.itemsFormData.Count; i++)
            {
                var item = this.itemsFormData[i];
                ItemUpdater updater = new ItemUpdater(item.CMSItem, false, EnumsCms.SECTION_TYPE.ListingPage);
                updater.PropertiesFromForm.AddRange(item.Properties);
                var itemResult = updater.UpdateDbItem();

                if (itemResult.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Success ||
                    (itemResult.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success && updater.ChangesWereDone))
                {
                    result.AddFromOperationResult(itemResult);
                }
                bool ok = (itemResult.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
               
                
            }
            

            //this.Master.Functionality.ShowStatusMessageAndTransferToSamePage(result);
            this.Master.Functionality.ShowStatusMessageAndRedirectToSamePage(result);
            
        }
        

       
        void listing_DeleteClick(List<ListingItemRow> selectedItems)
        {
            OperationResult statusResult = new OperationResult();
        	//	System.Text.StringBuilder sbErrors = new System.Text.StringBuilder();
             bool oneOK = false;
             int totalDeleted = 0;
             int totalWarning = 0;
            foreach (ListingItemRow row in selectedItems)
            {
                ICmsItemInstance item = (ICmsItemInstance)row.Object;
                ItemDeleter deleter = new ItemDeleter(item, EnumsCms.SECTION_TYPE.ListingPage);

                var result = deleter.DeleteDbItem();
                statusResult.AddFromOperationResult(result);
                if (result.IsSuccessful)
                {
                    oneOK = true;
                    totalDeleted++;
                }
                else
                {
                    totalWarning++;
                }

            }

            string msg = "";
            if (totalWarning == 0)
            {
                msg = string.Format("{0} item(s) deleted successfully", totalDeleted);
                statusResult.AddStatusMsg(General_v3.Enums.STATUS_MSG_TYPE.Success, msg);
            }
            else
            {
                msg = string.Format("Delete successful with warnings.  {0} item(s) deleted successfully, while {1} item(s) had warnings / could not delete", totalDeleted, totalWarning);
                statusResult.AddStatusMsg(General_v3.Enums.STATUS_MSG_TYPE.Warning, msg);
            }

            

            Master.Functionality.ShowStatusMessageAndRedirectToSamePage(statusResult);
        }
        
        
        
        private IEnumerable<object> dataList = null;
        
        
		    


        string listing_EditHrefRetriever(object obj)
        {
            ICmsItemInstance cmsItem = (ICmsItemInstance)obj;

            return factory.GetEditUrlForItemWithID(cmsItem.DbItem.ID).ToString();
        }
        private List<CmsPropertySearchInfo> searchFields = null;
        private bool hasSearch = false;
        private ICmsItemInstance _emptyItem = null;
        private void initSearch()
        {
            


             _emptyItem = factory.GetEmptyCmsItemInfo();
             var emptyItem = _emptyItem;
          

             var properties = emptyItem.GetCmsProperties();
            searchFields = new List<CmsPropertySearchInfo>();
            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            formSearch.Functionality.LayoutProvider.DoNotShowValidationIcons = true;
            formSearch.Functionality.ValidationGroup = "search";
            properties = CmsUtil.SortPropertyListByPrioritizer(properties, emptyItem.GetEditOrderPrioritizer());

            foreach (var p in properties)
            {
                bool showInSearch = p.CheckIfLoggedUserCanSearch();

                FormFieldsFieldItemDataImpl formField = null;
                if (showInSearch)
                {
                    formField = p.AddPropertyToFormFieldForCurrentCmsUser(formSearch, EnumsCms.SECTION_TYPE.SearchInListing, null, true, p.DefaultSearchValueStr);
                    formField.Control.NeverFillWithRandomValue = true;
                    
                }
                CmsPropertySearchInfo pSearchInfo = new CmsPropertySearchInfo(p, formField);
                searchFields.Add(pSearchInfo);



            }

           
            this.checkForSearch();
            formSearch.Functionality.ButtonSubmitText = "Search";
            formSearch.Functionality.ButtonCancelText = "Clear";
            formSearch.Functionality.ClickCancel += new EventHandler(formSearch_ClickCancel);
            formSearch.Functionality.ClickSubmit += new EventHandler(formSearch_ClickSubmit);
            formSearch.Functionality.CssManager.AddClass("search-form");
            

        }
        private void initSearchPanelToggleJS()
        {
            string js = "new js.com.cms." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".SearchButton('" + aSearchPanelToggle.ClientID + "','" + formSearch.ClientID + "', " + this.hasSearch.ToString().ToLower() + ");";
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "search-panel-toggle-btn", js, true);
        }
        void formSearch_ClickCancel(object sender, EventArgs e)
        {
            URLClass url = new URLClass();
            foreach (var f in this.searchFields)
            {
                f.ClearSearchValue(url);
            }
            hasSearch = false;
            this.Master.Functionality.ShowStatusMessage("Search cleared. Showing all items", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
            url.RedirectTo();
        }

        public bool ShowDeletedItems
        {
            get 
            {
                return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<bool>(getQsParam_ShowDeletedItem());
            }
        }

        

        void formSearch_ClickSubmit(object sender, EventArgs e)
        {
            CS.General_v3.Classes.URL.URLClass url = new URLClass();
            
            foreach (var searchField in this.searchFields)
            {
                searchField.UpdateValueFromForm(url);
            }


            this.Master.Functionality.ShowStatusMessage("Search successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
            url.RedirectTo();

        }


        public string CurrentSortBy
        {
            get
            {
                InitParameters();
                return listing.Functionality.SortBy;

            }
        }
        public CS.General_v3.Enums.SORT_TYPE CurrentSortType
        {
            get
            {
                InitParameters();
                return listing.Functionality.SortType;

            }
        }
        public int CurrentPageNo
        {
            get
            {
                InitParameters();
                return listing.Functionality.SelectedPage;

            }
        }
        public int CurrentPageSize
        {
            get
            {
                InitParameters();
                return listing.Functionality.SelectedShowAmt;
            }
        }


        private void showCurrentSearchAsDescription()
        {
            List<string> list = new List<string>();
            foreach (var f in searchFields)
            {
                object value = f.GetSearchValue();
                if (value != null)
                {
                    string s = value.ToString();
                    if (!string.IsNullOrEmpty(s))
                    {
                        list.Add(f.PropertyInfo.Label + ": " + s);
                    }
                }
                
            }
            if (list.Count > 0)
            {
                pSearchDescription.Visible = true;
                string msg = "You are currently searching with the criteria - " + CS.General_v3.Util.Text.AppendStrings(", ", list);
                pSearchDescription.InnerHtml = CS.General_v3.Util.Text.HtmlEncode(msg);
                pSearchDescription.Visible = false;
            }


        }

        private CmsFieldBase getSortByProperty(out CS.General_v3.Enums.SORT_TYPE sortType)
        {
            var p = columnPropertiesList.GetPropertyFromListByLabel(CurrentSortBy);
            sortType = CurrentSortType;
            
            if (p == null)
            {
                p = _emptyItem.GetDefaultSortField(out sortType);
                
            }

            if (p != null)
            {
                listing.Functionality.SortBy = p.Label;
                listing.Functionality.SortType = sortType;
            }

            return p;
            
        }

        
        protected void fillListingData()
        {
            this.itemsFormData = new List<CmsItemFormData>();

            
            showCurrentSearchAsDescription();
            CS.General_v3.Enums.SORT_TYPE sortType;
            var sortBy = getSortByProperty(out sortType);

          //  bool showDeleted = _formSearch.GetFormValueBool("chkShowDeleted");
            int pageNo = CurrentPageNo;
            bool showDelItems = ShowDeletedItems;
            int pageSize = CurrentPageSize;
            CmsSearchResults searchResults = factory.GetSearchResultsForListing(searchFields, showDelItems, true, pageNo, pageSize, sortBy, sortType);
            listing.Functionality.TotalItems = searchResults.TotalCount;

            
            

            dataList = searchResults.SearchItems;
            listing.Functionality.DataSource = dataList;

            listing.Functionality.DataBind();
        }

        private IEnumerable<CmsFieldBase> columnPropertiesList = null;

        private void initListingColumnHeaders()
        {
            var cmsItem = this.factory.GetEmptyCmsItemInfo();
            var properties = cmsItem.GetCmsProperties();
            columnPropertiesList = CmsUtil.SortPropertyListByPrioritizer(properties, cmsItem.GetListingOrderPrioritizer());
            
            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();

            foreach (var p in columnPropertiesList)
            {
                
                if (p.ShowInListing && loggedUser.CheckAccess(p.AccessTypeRequired_View))
                {
                    var col = p.GetColumnData();  
                    listing.Functionality.AddColumn(col);
                }

            }

            
           // initColumns_After();

        }

        private void checkForSearch()
        {
            hasSearch = false;
            if (this.searchFields != null)
            {
                foreach (var f in this.searchFields)
                {
                    //Karl fix this to return real has search = true or false   - this is the real search, as this is not getting them from the
                    //form value but from the session - Values are stored in session only if they are filled!
                    if (f.HasSearchValue())
                    {
                        hasSearch = true;
                        break;
                    }
                }
            }

        }

        bool listing_ItemDataBound(ListingItemRow row)
        {
            bool ok = true;
            ICmsItemInstance cmsItem = (ICmsItemInstance)row.Object;
            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            CSSManager rowCss = new CSSManager(row.Row);
            
            if (cmsItem.DbItem.Deleted)
                rowCss.AddClass("deleted");
                        
            List<CmsFieldBase> propertyList = CmsUtil.SortPropertyListByPrioritizer(cmsItem.GetCmsProperties(), cmsItem.GetListingOrderPrioritizer()).ToList();
            if (cmsItem.CheckIfUserCanView(loggedUser))
            {
                CmsItemFormData itemFormData = new CmsItemFormData(cmsItem);
                this.itemsFormData.Add(itemFormData);
                int cellIndex = 0;
                bool firstTmp = false;
                for (int i = 0; i < propertyList.Count; i++)
                {
                    var p = propertyList[i];
                    if (p.IsUsedInProject() &&p.ShowInListing && p.AccessTypeRequired_View.CheckIfCurrentLoggedInCmsUserHasEnoughAccess())
                    {
                        
                        var currentCell = row.DataCells[cellIndex];
                        var ctrl = p.GetControlForCmsPropertyForListingAndCurrentUser(cmsItem);
                        IMyFormWebControl formCtrl = null;
                        if (ctrl is IMyFormWebControl)
                        {
                            formCtrl = (IMyFormWebControl)ctrl;
                            formCtrl.FieldParameters.validationGroup = "listingUpdate";
                            formCtrl.NeverFillWithRandomValue = true;
                        }
                        CmsPropertyFormData pForm = new CmsPropertyFormData(this, p, formCtrl); 
                        itemFormData.Properties.Add(pForm);
                        pForm.FormControl = formCtrl;
                        CSSManagerForControl cssManager = new CSSManagerForControl(currentCell);
                        cssManager.AddClass(p.CssClassForListingColumn.ToString());

                        currentCell.Controls.Add(ctrl);
                        //if (!firstTmp && QuerystringParam_CheckForUntranslatedText)
                        //{
                        //    if (cmsItem.ContainsUntranslatedMultilingualText())
                        //    {
                        //        MySpan lit = new MySpan();
                        //        lit.InnerText = "[NOT TRANSLATED!]";
                        //        currentCell.Controls.Add(lit);
                        //    }
                        //    firstTmp = true;
                        //}

                        cellIndex++;
                    }




                }
            }
            else
            {
                ok = false;
            }
            return ok;  
        }
        private List<LinkButtonHelperClass> _linkedButtons = null;
        private void initListingLinkButtons()
        {
            var loggedUser = GetCurrentLoggedInUser();
            _linkedButtons = new List<LinkButtonHelperClass>();
            var linkedProperties = CmsSystem.Instance.GetCmsPropertiesLinkedToFactory(this.factory);
            foreach (var linkProperty in linkedProperties)
            {
                var linkedFactory = linkProperty.GetFactoryForLinkedObject();

                if (linkedFactory != null &&  linkedFactory.UserSpecificGeneralInfoInContext.CheckIfUserCanView(loggedUser))
                {
                    LinkButtonHelperClass link = new LinkButtonHelperClass(this.factory, linkProperty);
                    _linkedButtons.Add(link);
                    listing.Functionality.AddItemLinkButtonText(linkProperty.Label, link.ItemHrefRetriever);
                }

            }

        }

        private void initPageTitle()
        {
            Master.Functionality.SetPageTitles(factory.GetListingPageTitlesForThisAndLinked());
        }

        private void setListingImages()
        {
            listing.Functionality.DefaultImage_EditOver = EnumsCms.GetImageFilePath(EnumsCms.IMAGE_ICON.EditView);
            listing.Functionality.DefaultImage_EditUp = listing.Functionality.DefaultImage_EditOver;

        }

        private void initDefaultParams()
        {
            listing.Functionality.DefaultShowAmount = 25;
        }

        private void initListing()
        {
            var loggedUser = GetCurrentLoggedInUser();
            phListing.Visible = true;
            initDefaultParams();
            setListingImages();
            //_phListingComponents.Controls.Add(listing);
            listing.Functionality.ItemDataBound += listing_ItemDataBound;
            //if (itemInfo.AllowCurrentUserToDelete())
            if (this.factory.UserSpecificGeneralInfoInContext.CheckIfUserCanDelete(loggedUser))
            {
                listing.Functionality.DeleteClick += listing_DeleteClick;
            }


            //if ( itemInfo.AllowCurrentUserToEdit())
            if (true)
            {

                listing.Functionality.EditHrefRetriever += listing_EditHrefRetriever;
                btnListingUpdateTop.ValidationGroup= btnListingUpdateBottom.ValidationGroup = "listingUpdate";
                btnListingUpdateBottom.Click += new EventHandler(btnListingUpdate_Click);
                btnListingUpdateTop.Click += new EventHandler(btnListingUpdate_Click);
                pUpdateNote.Visible = true;

            }
            else
            {
                btnListingUpdateTop.Visible = btnListingUpdateBottom.Visible = false;
            }
            initListingLinkButtons();
            initListingColumnHeaders();
            fillListingData();         
        }
        private void initTree()
        {
            phTree.Visible = true;
            var items = this.factory.GetRootItems(searchFields);
            //_formSearch.Visible = false;
            
            List<ICmsHierarchyInfo> listOfInfos = new List<ICmsHierarchyInfo>();
            foreach (var item in items)
            {
                var hierarchyInfo = item.GetCmsHierarchyInfo();
                if (!hierarchyInfo.CheckIfStillTemporary())
                {
                    listOfInfos.Add(item.GetCmsHierarchyInfo());
                }
            }

            cmsTreeStructure.Functionality.RootItems = listOfInfos;

            
            
        }
        private void initRenderType()
        {
            cmsTreeStructure.Functionality.RootItems = new List<ICmsHierarchyInfo>();
            checkForSearch();
            if (this.factory.RenderType == EnumsCms.SECTION_RENDER_TYPE.Listing ||
                (this.factory.RenderType == EnumsCms.SECTION_RENDER_TYPE.Tree && hasSearch))
            {
                initListing();
            }
            else if (this.factory.RenderType == EnumsCms.SECTION_RENDER_TYPE.Tree)
            {
                initTree();
            }
            else
            {
                throw new InvalidOperationException("Invalid render type");
            }
        }
        private void initCommonItems()
        {
            
            initPageTitle();
            
            initHandlers();
            
            
            initSearch();
            initButtonsBar();
            initRenderType();

        }
        private void initGoBackUrl()
        {
            string goBackUrl = factory.GetGoBackUrlForListing();
            this.Master.Functionality.GoBackURL = factory.GetGoBackUrlForListing();


            
            
            
        }
        private void checkDeletedMsg()
        {
            pDeletedMsg.Visible = (this.ShowDeletedItems);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            initGoBackUrl();
    
            initCommonItems();
            checkDeletedMsg();
            
        }
        protected override void OnPreRender(EventArgs e)
        {
            initSearchPanelToggleJS();
            base.OnPreRender(e);
        }
        #region only to satisfy old requirements


        [Obsolete]
        protected override PlaceHolder _phListingComponents
        {
            get { throw new NotImplementedException(); }
        }

        [Obsolete]
        protected override MyButton _btnListingUpdate
        {
            get { throw new NotImplementedException(); }
        }

        [Obsolete]
        protected override FormFields _formSearch
        {
            get { throw new NotImplementedException(); }
        }

        [Obsolete]
        protected override PlaceHolder _phAfterListing
        {
            get { throw new NotImplementedException(); }
        }

        [Obsolete]
        protected override PlaceHolder _phBeforeFormSearch
        {
            get { throw new NotImplementedException(); }
        }

        [Obsolete]
        protected override PlaceHolder _phBeforeListing
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}