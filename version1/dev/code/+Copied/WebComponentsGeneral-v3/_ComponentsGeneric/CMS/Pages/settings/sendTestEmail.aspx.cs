﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.Email;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Util;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.settings
{
    public partial class sendTestEmail : BaseCMSPage
    {
        private MyTxtBoxEmail _txtFromEmail;
        private MyTxtBoxString _txtFromName;
        private MyTxtBoxEmail _txtToEmail;
        private MyTxtBoxString _txtToName;
        private MyTxtBoxString _txtSubject;
        private MyTxtBoxStringMultiLine _txtMessage;

        private void initForm()
        {

            

            formFields.Functionality.AddGenericLabelAndControl("SMTP Host", false, new Literal() { Text = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.Email_SMTP_Host)});
            formFields.Functionality.AddGenericLabelAndControl("SMTP Port", false, new Literal() { Text = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(CS.General_v3.Enums.SETTINGS_ENUM.Email_SMTP_Port).ToString() });
            formFields.Functionality.AddGenericLabelAndControl("SMTP User", false, new Literal() { Text = CS.General_v3.Settings.Emails.SMTPUser });
            formFields.Functionality.AddGenericLabelAndControl("SMTP Pass", false, new Literal() { Text = CS.General_v3.Settings.Emails.SMTPPass });


            _txtFromEmail =formFields.Functionality.AddEmail(id: "txtFromEmail", title: "From (Email)", required: true, helpMessage: "The email address which appears in the 'From' field of the recipients mail client", initialValue: CS.General_v3.Settings.Emails.SentEmailsFromEmail);
            _txtFromName = formFields.Functionality.AddString(id: "txtFromName", title: "From (Name)", required: true, helpMessage: "The name which appears in the 'From' field of the recipients mail client", initialValue: CS.General_v3.Settings.Emails.SentEmailsFromEmail);
            _txtToEmail = formFields.Functionality.AddEmail("txtToEmail", "To (Email)", true, null);
            _txtToName = formFields.Functionality.AddString("txtToName", "To (Name)", false, null);
            _txtSubject = formFields.Functionality.AddString("txtSubject", "Subject", true, null, initialValue: "Testing email <" + CS.General_v3.Util.Date.Now.ToString("dd/MMM/yyyy hh:mmtt") + ">");
            string testMsg = 
@"Hi, This is a testing email sent to test out the CMS functionality.

Email sent on " + CS.General_v3.Util.Date.Now.ToString("dddd, dd MMMM yyyy") + " at " + CS.General_v3.Util.Date.Now.ToString("hh:mmtt") + 
@"

Regards,
 ---
 Auto Mailer";
            _txtMessage = formFields.Functionality.AddStringMultiline("txtMessage", "Message", true, initialValue:testMsg);
            formFields.Functionality.ButtonSubmitText = "Send test email";
            formFields.Functionality.ButtonSubmit.WebControlFunctionality.ConfirmMessage = "Are you sure you want to send the test email?";
            formFields.Functionality.ClickSubmit += new EventHandler(formFields_ClickSubmit);
        }

        void formFields_ClickSubmit(object sender, EventArgs e)
        {
            string fromEmail = _txtFromEmail.GetFormValueAsString();
            string fromName = _txtFromName.GetFormValueAsString();
            string toEmail = _txtToEmail.GetFormValueAsString();
            string toName = _txtToName.GetFormValueAsString();
            string subject = _txtSubject.GetFormValue();
            string msg = _txtMessage.GetFormValue();
            bool success = false;
            OperationResult result = new OperationResult();
                
            try
            {

                msg = CS.General_v3.Util.Text.ConvertPlainTextToHTML(msg);
                var email = EmailUtil.CreateEmailMessage(subject, msg, fromEmail, fromName, toEmail, toName);
                /*
                EmailMessage email = new EmailMessage();
                email.SetFromEmail(fromEmail, fromName);
                email.AddToEmails(toEmail, toName);
                email.Subject = subject;
                email.BodyHtml = msg;
                email.BodyPlain = msg;
                BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase c;
                */
                result = email.SendSynchronously();
                if (result.IsSuccessful)
                {
                    success = true;
                    result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Success, "Email sent successfully to <" + toEmail + ">");
                }
            }
            catch (Exception ex)
            {
                
                result.AddException(ex);
                
            }
            this.Master.Functionality.ShowStatusMessageAndRedirectToSamePage(result);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            initForm();
        }
    }
}