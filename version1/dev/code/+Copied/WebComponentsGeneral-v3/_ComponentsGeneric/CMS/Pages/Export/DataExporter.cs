﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.Routing.Exporting;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Specialized.Reporting.v1;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.Routing.Reporting;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.Export
{
    public class DataExporter
    {
        private ICmsItemFactory _factory = null;
        public DataExporter(ICmsItemFactory factory)
        {

            _factory = factory;
        }

        private bool checkForExport(CmsFieldBase field)
        {
            var loggedUser = CmsUtil.GetLoggedInUser();
            string qsId = chooseFields.GetQsId(field.GetPropertyIdentifier());
            bool ok = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<bool>(qsId);
            if (ok)
            {
                ok = field.CheckIfUserCanView(loggedUser);
            }
            return ok;
        }

        private ICmsItemInstance _emptyItem = null;
        private CmsSearchResults _data = null;
        private void loadData()
        {
            ICmsItemInstance emptyItem = _factory.GetEmptyCmsItemInfo();
            _emptyItem = emptyItem;
            IEnumerable<CmsFieldBase> properties = emptyItem.GetCmsProperties();
            List<CmsPropertySearchInfo> searchFields = new List<CmsPropertySearchInfo>();
            foreach (var p in properties)
            {
                if (checkForExport(p))
                {
                    var searchField = new CmsPropertySearchInfo(p, null);


                    searchField.CustomSearchValue = ExportRoutes.GetSearchFieldValue(p);
                    searchFields.Add(searchField);

                }
            }

            string sortByStr = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(ExportRoutes.Param_Search_SortBy);
            string sortTypeStr = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(ExportRoutes.Param_Search_SortType);
            var sortBy = emptyItem.GetCmsProperties().GetPropertyFromListByLabel(sortByStr);
            var sortType =  CS.General_v3.Util.EnumUtils.GetEnumByStringValue<CS.General_v3.Enums.SORT_TYPE>(sortTypeStr, General_v3.Enums.SORT_TYPE.Ascending);

            _data = _factory.GetSearchResultsForListing(searchFields, false, true, 1, int.MaxValue, sortBy, sortType);
        }

        private BaseReport _report = null;
        private BaseReportSectionDataImpl _section = null;

        private void addHeaderRow()
        {
            var headers = _section.TableData.HeadersTop;
            ICmsItemInstance emptyItem = _factory.GetEmptyCmsItemInfo();
            _emptyItem = emptyItem;
            IEnumerable<CmsFieldBase> properties = emptyItem.GetCmsProperties();
            foreach (var p in properties)
            {
                if (checkForExport(p))
                {
                    headers.Add(new BaseReportTableHeaderDataImpl(p.Label));
                    
                }
            }
        }

        private void fillReportData()
        {
            
            foreach (var item in _data.SearchItems)
            {
                BaseReportTableRowDataImpl row = new BaseReportTableRowDataImpl();
                _section.TableData.DataRows.Add(row);
                IEnumerable<CmsFieldBase> properties = item.GetCmsProperties();
                foreach (var p in properties)
                {
                    if (checkForExport(p))
                    {
                        string s = p.GetValueForObjectAsString(item);
                        row.AddCell(s);
                    }
                }
                
            }
        }

        public BaseReport CreateReport()
        {
            _report = new BaseReport();
            _report.Functionality.Title = _factory.TitlePlural;
            _report.Functionality.ShowGeneratedOn = true;
            _section = new BaseReportSectionDataImpl(); _report.Functionality.AddSection(_section);
            loadData();
            addHeaderRow();
            fillReportData();
            return _report;

        }

    }
}