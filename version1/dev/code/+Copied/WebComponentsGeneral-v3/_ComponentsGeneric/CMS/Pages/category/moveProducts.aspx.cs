﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Modules.CategoryModule;


using BusinessLogic_v3.Modules;
using CS.WebComponentsGeneralV3.Cms.CategoryModule;
using CS.WebComponentsGeneralV3.Cms.ProductModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;


namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.category
{
    public partial class moveProducts : CS.WebComponentsGeneralV3.Code.Cms.Pages.BaseCMSPage
    {
        private MyDropDownList _cmbTo;

        public moveProducts()
        {
            this.initAccess();
        }

        public static long? GetCategoryIdFromQuerystring()
        {
            return CategoryBaseCmsFactory.Instance.GetIDFromCurrentQuerystring();
        }
        private ICategoryBase category = null;
        private void loadCategory()
        {
            var id = GetCategoryIdFromQuerystring();
            if (id.GetValueOrDefault() > 0)
            {
                category = CategoryBaseFactory.Instance.GetByPrimaryKey(id.Value);
                
            }
            if (category == null)
            {
                CS.General_v3.Util.PageUtil.RedirectPage(CategoryBaseCmsFactory.Instance.GetListingUrl().ToString());
            }
        }
        private void initAccess()
        {
            this.Functionality.AccessRequired.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner);
        }

        private void initForm()
        {
            this.formMove.Functionality.AddGenericLabelAndControl("From:", false, new Literal() { Text = category.GetFullCategoryTitleFromRoot(" > ", true) });

            var listItemsToMoveTo = CategoryBaseCmsFactory.Instance.GetAllAsListItemsForCms(null,false,0,false);
            ListItemCollection liCollMoveTo = new ListItemCollection();
            liCollMoveTo.AddRange(listItemsToMoveTo);
            _cmbTo = this.formMove.Functionality.AddSingleChoiceList("txtTo", "To:", true, new Code.Controls.WebControls.Common.MyDropDownListItems(liCollMoveTo), "Choose the category you want to move all " + ProductBaseCmsFactory.Instance.TitlePlural + " to", null);
            this.formMove.Functionality.ButtonSubmit.WebControlFunctionality.ConfirmMessage = "Are you sure you want to move all " + ProductBaseCmsFactory.Instance.TitlePlural + "?";
            this.formMove.Functionality.ClickSubmit += new EventHandler(formMove_ClickSubmit);

        }

        void formMove_ClickSubmit(object sender, EventArgs e)
        {
            long? moveTo = _cmbTo.GetFormValueAsLongNullable();
            ICategoryBase moveToCategory = Factories.CategoryFactory.GetByPrimaryKey(moveTo.GetValueOrDefault());
            if (moveToCategory != null)
            {
                CategoryProductsMover mover = new CategoryProductsMover();
                mover.Move(this.category, moveToCategory);
                CmsUtil.ShowStatusMessageInCMS(mover.MovedItems.Count + " " + ProductBaseCmsFactory.Instance.TitlePlural + "  moved successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
            }
            else
            {
                CmsUtil.ShowStatusMessageInCMS("Category could not be found", CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            loadCategory();
            initForm();
        }
    }
}