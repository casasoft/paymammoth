﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Classes.HelperClasses;
using CS.WebComponentsGeneralV3.Code.Classes.DataImport;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.Products
{
    public partial class import : BaseCMSPage
    {
       

        private void setAccess()
        {
            this.Functionality.AccessRequired.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
        }
        public import()
        {
            
            setAccess();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.dataImporter.Functionality.DataImporter = ProductsImporter.Instance;
            //this.dataImport.Functionality.Importer = BusinessLogic_v3.Classes.DataImport.ProductDataImporter.Instance;
            //this.dataImport.Functionality.ShowUploadPath = BusinessLogic_v3.Modules.ModuleSettings.Shop.DataImport.ShowUploadPath;
            //this.dataImport.Functionality.ShowZIPFile = BusinessLogic_v3.Modules.ModuleSettings.Shop.DataImport.ShowZIPFile;
            //this.dataImport.Functionality.ShowRemoveAnyExistingImages = BusinessLogic_v3.Modules.ModuleSettings.Shop.DataImport.ShowRemoveAnyExistingImages;


        }
    }
}