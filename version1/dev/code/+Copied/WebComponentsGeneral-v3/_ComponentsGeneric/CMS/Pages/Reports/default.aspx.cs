﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.Reports
{
    public partial class _default : BaseCMSPage
    {

        public static List<PageTitle> GetCmsPageTitles()
        {
            List<PageTitle> list = new List<PageTitle>();
            list.Add(new PageTitle("Reports", CS.WebComponentsGeneralV3.Code.Cms.Routing.Reporting.ReportingRoutes.GetListingUrl()));
            return list;
        }

        private void generateReportSection(string section)
        {
            MyDiv divSection = new MyDiv(); phReportList.Controls.Add(divSection);
            var reports = CmsSystem.Instance.CmsReports.GetListOfReportsBySection(section);
            divSection.CssManager.AddClass("report-section");
            MyHeading header = new MyHeading(2, section, "report-section-header", divSection);
            MyUnorderedList ul = new MyUnorderedList(); divSection.Controls.Add(ul);
            ul.CssManager.AddClass("report-section-list");
            foreach (var report in reports)
            {
                MyListItem li = new MyListItem(); ul.Controls.Add(li); li.CssManager.AddClass("report");
                MyDiv divReport = new MyDiv(); divReport.CssManager.AddClass("report");

                MyAnchor a = new MyAnchor(); divReport.Controls.Add(a);
                a.CssManager.AddClass("report");
                a.InnerText = report.ReportTitle;
                a.Href = CS.WebComponentsGeneralV3.Code.Cms.Routing.Reporting.ReportingRoutes.GetReportUrl(report);

                MyDiv divDescription = new MyDiv(); divReport.Controls.Add(divDescription); divDescription.CssManager.AddClass("report-desc");
                divDescription.InnerHtml = report.ReportDescriptionHtml;


                
                li.Controls.Add(divReport);
                
            }

        }

        private void generateReportList()
        {
            var sections = CmsSystem.Instance.CmsReports.GetDistinctReportSections();

            foreach (var s in sections)
            {
                generateReportSection(s);

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            generateReportList();

            this.Master.Functionality.SetPageTitle(GetCmsPageTitles().ToArray());
        }
    }
}