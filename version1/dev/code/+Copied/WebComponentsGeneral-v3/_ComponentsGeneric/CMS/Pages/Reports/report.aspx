﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_ComponentsGeneric/CMS/Pages/cms.master" AutoEventWireup="true" CodeBehind="report.aspx.cs" Inherits="CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.Reports.report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
<p id="pDescription" runat="server" class="report-desc">
</p>
<p class="report-note">
Fill in the criteria below (if any), and click on <strong>Generate</strong> in order to generate the report.  <strong>Clear</strong> removes any selected criteria.
</p>

<CSControlsFormFields:FormFields CssClass="form-report" ID="formReportParams" runat="server">

</CSControlsFormFields:FormFields>

<div id="divReport" runat="server" class="report-module" visible="false">


</div>

<asp:PlaceHolder ID="phFooter" runat="server"></asp:PlaceHolder>

</asp:Content>