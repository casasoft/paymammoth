namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
    using BusinessLogic_v3.Classes.DbObjects.Parameters;
    using BusinessLogic_v3.Modules.CmsUserModule;
    using BusinessLogic_v3.Util;
    using CS.General_v3.Classes.HelperClasses;
    using CS.WebComponentsGeneralV3.Code.Cms;
    using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
    using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsDbOperations;
    using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
    using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
    using CS.WebComponentsGeneralV3.Code.Cms.Pages;
    using CS.WebComponentsGeneralV3.Code.Cms.Util;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.FormFields;
    using CS.WebComponentsGeneralV3.Code.Classes.Pages;

    public partial class editPage : CS.WebComponentsGeneralV3.Code.Cms.Pages.CmsBaseEditPage
    {
        public editPage()
        {



        }

        private void setFactory()
        {
            string title = getFactoryTitleFromRouteData();
            var cmsFactory = CmsSystem.Instance.GetCmsFactoryByTitle(title);
            if (cmsFactory != null)
            {
                this.Functionality.CmsFactory = cmsFactory;
            }
            else
            {
                CS.General_v3.Util.PageUtil.RedirectPage("/");

            }
        }
        protected override void OnPreInit(EventArgs e)
        {
            setFactory();
            loadItem();
            base.OnPreInit(e);

        }


        public class CmsBaseEditPageFunctionality : BaseCMSPage.FUNCTIONALITY
        {
            public override CmsAccessType AccessRequired
            {
                get
                {
                    return this._basePage._item.GetAccessRequiredToView();
                }
            }

            protected editPage _basePage
            {
                get
                {
                    return (editPage)base._basePage;
                }
            }


            public CmsBaseEditPageFunctionality(CmsBaseEditPage pg)
                : base(pg)
            {

            }
            public ICmsItemFactory CmsFactory { get; set; }

        }
        public new CmsBaseEditPageFunctionality Functionality
        {
            get
            {
                return (CmsBaseEditPageFunctionality)base.Functionality;
            }
        }
        protected override BasePageBase.FUNCTIONALITY getFunctionality()
        {
            return new CmsBaseEditPageFunctionality(this);
        }





        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }


        protected bool redirectPageOnAddEdit = true;




        private void initPageTitle()
        {

            this.Master.Functionality.SetPageTitles(this.itemInfo.GetEditPageTitlesIncludingLinked(this._item.DbItem.ID));
        }

        private void initTexts()
        {

            this.Master.Functionality.GoBackURL = itemInfo.GetGoBackUrlForEdit();

        }
        protected ICmsItemInstance _item = null;
        protected ICmsItemFactory itemInfo
        {
            get
            {
                return this.Functionality.CmsFactory;
            }
        }
        private void createNewItem()
        {

            _item = this.itemInfo.CreateNewItem(true);

            using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                foreach (var property in _item.GetCmsProperties())
                {
                    if (property is CmsLinkedPropertyInfo)
                    {
                        CmsLinkedPropertyInfo p = (CmsLinkedPropertyInfo)property;
                        if (p.DataType == EnumsCms.CMS_DATA_TYPE.LinkedObject)
                        {
                            var coll = p.GetLinkedObjectCollectionFromOtherSide();
                            if (coll != null)
                            {

                                long? id = coll.GetLinkedIDFromQuerystring();
                                if (id.GetValueOrDefault() > 0)
                                {
                                    var linkedItem = p.GetLinkedCmsFactory().GetCmsItemWithId(id.Value);
                                    p.SetValueForObject(_item, linkedItem.DbItem);
                                }
                            }
                        }
                    }
                }
                _item.SaveFromCms(new SaveParams(savePermanently: false));
                t.CommitIfActiveElseFlush();
            }
        }
        private void loadItem()
        {
            // System.Threading.Thread.Sleep(500);
            long id = this.itemInfo.GetIDFromCurrentQuerystring().GetValueOrDefault();
            if (id > 0)
            {
                _item = this.itemInfo.GetCmsItemWithId(id);


            }
            if (_item == null)
            {
                createNewItem();

            }
            if (_item.DbItem.ID != id)
            {
                var url = this.itemInfo.GetEditUrlForItemWithID(_item.DbItem.ID);
                //R2esponse.Red2irect("/"); 
                url.RedirectTo();


            }
        }
        public bool IsAdd
        {
            get
            {
                return !IsEdit;
            }
        }
        public bool IsEdit
        {
            get
            {
                return !this._item.CheckIfStillTemporary();
            }
        }
        private void initShortcutsJS()
        {
            string js = "new js.com.cms." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".Controllers.EditPageShortcutsController();\r\n";
            this.ClientScript.RegisterStartupScript(this.GetType(), "editPageShortcuts", js, true);
        }

        private void initFormButtons()
        {
            if (true)
            {
                var loggedUser = GetCurrentLoggedInUser();
                var userSpecificGeneralInfo = this.Functionality.CmsFactory.UserSpecificGeneralInfoInContext;
                List<MyButton> buttons = new List<MyButton>();
                if ((IsAdd && userSpecificGeneralInfo.CheckIfUserCanAdd(loggedUser)) ||
                    (IsEdit && _item.CheckIfUserCanEdit(loggedUser)))
                {

                    MyButton btnSave = new MyButton("btnSave");
                    btnSave.CssManager.AddClass("btn-edit-save");
                    btnSave.Text = "Save (S)";
                    btnSave.Click += _formFields_ClickSubmit_Save;
                    buttons.Add(btnSave);
                    /*
                    formFields.Functionality.ButtonSubmitText = "Save (S)";

                    formFields.Functionality.ClickSubmit += new EventHandler(_formFields_ClickSubmit_Save);*/
                }
                if (userSpecificGeneralInfo.CheckIfUserCanAdd(loggedUser))
                {
                    MyButton btnSaveAndAddAnother = new MyButton("btnSaveAndAddAnother");
                    btnSaveAndAddAnother.CssManager.AddClass("btn-edit-save-and-add-another");
                    btnSaveAndAddAnother.Text = "Save & Add Another (N)";
                    btnSaveAndAddAnother.Click += _formFields_ClickSubmit_SaveAndAddAnother;
                    buttons.Add(btnSaveAndAddAnother);
                }
                if ((IsAdd && userSpecificGeneralInfo.CheckIfUserCanAdd(loggedUser)) ||
                    (IsEdit && _item.CheckIfUserCanEdit(loggedUser)))
                {
                    MyButton btnSaveAndGoBack = new MyButton("btnSaveAndGoBack");
                    btnSaveAndGoBack.CssManager.AddClass("btn-edit-save-and-goback");
                    btnSaveAndGoBack.Text = "Save & Go Back (G)";
                    btnSaveAndGoBack.Click += _formFields_ClickSubmit_SaveAndGoBack;
                    buttons.Add(btnSaveAndGoBack);
                }
                if (IsEdit && _item.CheckIfUserCanDelete(loggedUser))
                {
                    MyButton btnDelete = new MyButton();
                    btnDelete.CssManager.AddClass("btn-edit-delete");
                    btnDelete.ValidationGroup = "no-delete-validation-group";
                    btnDelete.Text = "Delete (D)";
                    btnDelete.WebControlFunctionality.ConfirmMessage = "Are you sure you want to delete?  There is no way to un-delete this item once deleted, and any linked items may also be deleted!";
                    btnDelete.Click += new EventHandler(btnDelete_Click);
                    buttons.Add(btnDelete);
                }
                {
                    MyButton btnGoBack = new MyButton("btnGoBack");
                    btnGoBack.CssManager.AddClass("btn-edit-goback");
                    btnGoBack.ValidationGroup = "no--validation-group";
                    btnGoBack.Text = "Go Back without saving (E)";
                    btnGoBack.WebControlFunctionality.ButtonParams.validateFormOnClick = false;
                    btnGoBack.Click += new EventHandler(btnGoBack_Click);
                    btnGoBack.WebControlFunctionality.ConfirmMessage = "Any changes will NOT be saved.  Continue?";
                    buttons.Add(btnGoBack);
                }

                formFields.Functionality.AddButtons(buttons);

            }
        }

        void btnGoBack_Click(object sender, EventArgs e)
        {

            string url = this.itemInfo.GetListingUrl().ToString();
            CS.General_v3.Util.PageUtil.RedirectPage(url);

        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            var loggedUser = this.GetCurrentLoggedInUser();
            if (IsEdit && _item.CheckIfUserCanDelete(loggedUser))
            {


                ItemDeleter deleter = new ItemDeleter(this._item, EnumsCms.SECTION_TYPE.EditPage);
                var result = deleter.DeleteDbItem();
                if (result.IsSuccessful)
                {
                    var listingUrl = this.itemInfo.GetListingUrl().ToString();
                    this.Master.Functionality.ShowStatusMessageAndRedirectToPage(result, listingUrl);


                }
                else
                {
                    this.Master.Functionality.ShowStatusMessageAndRedirectToSamePage(result);

                }
            }
        }

        private void checkLinkedToButtons()
        {


            foreach (var linkedProperty in CmsSystem.Instance.GetCmsPropertiesLinkedToFactory(this.itemInfo))
            {
                if (linkedProperty.IsUsedInProject())
                {
                    var linkedFactory = linkedProperty.GetFactoryForLinkedObject();
                    string linkedToHref = "#";
                    if (this._item.DbItem.ID > 0)
                    {
                        linkedToHref = linkedProperty.GetListingUrlForLinkedType(_item);
                    }

                    var aLink = this.Master.ButtonsBar.AddLink("View " + linkedProperty.Label, linkedFactory.CmsImageIcon, linkedToHref);
                    if (this._item.DbItem.ID <= 0)
                    {
                        aLink.WebControlFunctionality.OnClientClick = "alert('You need to save before you can access this link'); return false;";
                    }
                }
            }




        }
        private void checkCustomCmsOperations()
        {
            var customOperations = this._item.GetCustomCmsOperations();
            if (customOperations != null)
            {
                foreach (var customOp in customOperations)
                {
                    customOp.AddToButtonsBar(this.Master.ButtonsBar);
                }
            }
            //this.Master.Functionality.ShowStatusMessageAndRedirectToSamePage
        }


        private List<CmsPropertyFormData> __propertiesFormDatas = null;
        protected List<CmsPropertyFormData> _propertiesFormDatas { get { return __propertiesFormDatas; } }



        private void initForm()
        {
            __propertiesFormDatas = new List<CmsPropertyFormData>();


            //linked with fields



            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();

            //int FIELD_WIDTH = 400;
            var properties = this._item.GetCmsProperties().ToList();
            properties = CmsUtil.SortPropertyListByPrioritizer(properties, this._item.GetEditOrderPrioritizer()).ToList();
            for (int i = 0; i < properties.Count; i++)
            {
                if (i == 13)
                {
                    int k = 5;
                }
                var p = properties[i];
                if (loggedUser != null && p != null && p.ShowInEdit && p.IsUsedInProject())
                {
                    if (p.Label == "Published On")
                    {
                        int k = 5;
                    }
                    if (p.CheckIfUserCanEdit(loggedUser) && !p.IsReadOnly)
                    {


                        object value = p.GetValueForObject(_item);
                        

                        var formFieldRowItem = p.AddPropertyToFormFieldForCurrentCmsUser(formFields, EnumsCms.SECTION_TYPE.EditPage, _item, false, value);
                        CmsPropertyFormData propertyFormData = new CmsPropertyFormData(this, p, formFieldRowItem.Control);
                        if (p.IsMultilingual && p is CmsPropertyInfo)
                        {
                            CmsPropertyInfo cmsPInfo = (CmsPropertyInfo)p;
                            formFieldRowItem.SubTitle += "<div class='multilingual-icon'></div>";
                            var currCulture= BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture();
                            if (!currCulture.IsDefaultCulture)
                            {
                                object englishTxt = BusinessLogic_v3.Util.MultilingualUtil.GetValueForDefaultCultureForMultilingualProperty(
                                    (BusinessLogic_v3.Classes.Multilingual.IMultilingualItem)_item.DbItem, cmsPInfo.PropertyInfo);
                            }
                            //todo: [MULTILINGUAL] [MARK] <- mark put this somewhere!
                        }
                        propertyFormData.FormControl.NeverFillWithRandomValue = true;
                        _propertiesFormDatas.Add(propertyFormData);
                    }
                    else if (p.CheckIfUserCanView(loggedUser))
                    {
                        Control c = p.GetControlNonEditable(_item);

                        //string s = CmsUtil.GetToStringValueForObject(value);
                        //LiteralControl lit = new LiteralControl();
                        //lit.Text = CS.General_v3.Util.Text.HtmlEncode(s);

                        formFields.Functionality.AddGenericLabelAndControl(p.Label, p.IsRequired, c);
                    }
                }

            }



            checkLinkedToButtons();
            initForm_After();

        }

        protected virtual void initForm_After()
        {

        }




        protected virtual void saveDetails()
        {
            OperationResult saveStatusResult = null;

            ItemUpdater updater = new ItemUpdater(_item, (!IsEdit), EnumsCms.SECTION_TYPE.EditPage);
            updater.PropertiesFromForm.AddRange(_propertiesFormDatas);
            saveStatusResult = updater.UpdateDbItem();



            if (IsEdit)
            {
                if (saveStatusResult.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
                {
                    saveStatusResult.SetStatusInfo(CS.General_v3.Enums.STATUS_MSG_TYPE.Success, this.itemInfo.TitleSingular + " edited successfully", null, true);
                    Master.Functionality.ShowStatusMessage(saveStatusResult);
                }
                else
                {
                    string msg = itemInfo.TitleSingular + " updated with errors.  Listed below is a summary of the errors:";
                    saveStatusResult.SetStatusInfo(saveStatusResult.Status, msg, null, false);
                    Master.Functionality.ShowStatusMessage(saveStatusResult);
                }
            }
            else
            {
                if (saveStatusResult.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
                {
                    saveStatusResult.SetStatusInfo(CS.General_v3.Enums.STATUS_MSG_TYPE.Success, this.itemInfo.TitleSingular + " added successfully", null, true);
                }
                else
                {
                    string msg = this.itemInfo.TitleSingular + " added with errors.  Listed below is a summary of the errors:";
                    saveStatusResult.SetStatusInfo(saveStatusResult.Status, msg, null, false);
                    Master.Functionality.ShowStatusMessage(saveStatusResult);
                }

            }
        }


        private void _formFields_ClickSubmit_SaveAndGoBack(object sender, EventArgs e)
        {
            onSaveAndGoBackClicked();
        }



        private void _formFields_ClickSubmit_SaveAndAddAnother(object sender, EventArgs e)
        {
            onSaveAndAddAnotherClicked();
        }

        protected void onSaveClicked()
        {
            saveDetails();

            var url = this.itemInfo.GetEditUrlForItemWithID(_item.DbItem.ID);
            url.RedirectTo();

        }

        protected virtual ICmsItemInstance getLastAddedItem()
        {
            long lastAddedID = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long>(CmsConstants.QUERYSTRING_LASTADDED);
            ICmsItemInstance item = null;
            if (lastAddedID > 0)
                item = itemInfo.GetCmsItemWithId(lastAddedID);
            return item;
        }

        protected void onSaveAndAddAnotherClicked()
        {
            saveDetails();
            var url = this.itemInfo.GetAddUrl();
            url[CmsConstants.QUERYSTRING_LASTADDED] = _item.DbItem.ID;
            url.RedirectTo();
        }
        protected virtual void onSaveAndGoBackClicked()
        {
            saveDetails();
            CS.General_v3.Classes.URL.URLClass url = this.itemInfo.GetListingUrl();

            url[CmsConstants.QUERYSTRING_LASTADDED] = _item.DbItem.ID;
            url.RedirectTo();
        }
        private void _formFields_ClickSubmit_Save(object sender, EventArgs e)
        {
            onSaveClicked();

        }
        private CmsUserBase loggedUser = null;
        private void redirectBackToListing()
        {
            this.itemInfo.GetListingUrl().RedirectTo();
        }
        private void checkAccess()
        {
            loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            if (loggedUser == null ||
                (loggedUser != null && IsAdd && !this.itemInfo.UserSpecificGeneralInfoInContext.CheckIfUserCanAdd(loggedUser)) ||
                (loggedUser != null && IsEdit && (!this._item.CheckIfUserCanView(loggedUser) && !_item.CheckIfUserCanDelete(loggedUser))))
            {
                redirectBackToListing();
            }
        }
        protected override void OnLoad(EventArgs e)
        {

            checkAccess();

            initTexts();
            initPageTitle();
            checkCustomCmsOperations();
            initForm();
            initFormButtons();
            initShortcutsJS();
            base.OnLoad(e);


        }




        protected override FormFields _formFields
        {
            get { throw new NotImplementedException(); }
        }

        protected override PlaceHolder _phBeforeForm
        {
            get { throw new NotImplementedException(); }
        }

        protected override PlaceHolder _phAfterForm
        {
            get { throw new NotImplementedException(); }
        }
    }
}