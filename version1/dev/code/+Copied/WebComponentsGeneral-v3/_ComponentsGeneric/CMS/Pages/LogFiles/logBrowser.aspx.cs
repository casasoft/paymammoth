﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using BusinessLogic_v3.Classes.log4netClasses;
using CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.Pages;


namespace CS.WebComponentsGeneralV3._ComponentsGeneric.CMS.Pages.LogFiles
{
    using Code.Cms.WebControls.ListingClasses;
    using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

    public partial class logBrowser : BaseCMSPage
    {
        public logBrowser()
        {
            initAccess();
        }
        private void initAccess()
        {
            this.Functionality.AccessRequired.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
        }

        private void initHandlers()
        {
            this.listingLogs.Functionality.ItemDataBound += new Listing.ItemHandler(Functionality_ItemDataBound);
            btnViewForAll.Click += new EventHandler(btnViewForAll_Click);

        }

        void btnViewForAll_Click(object sender, EventArgs e)
        {
            List<string> paths = new List<string>();
            foreach (var chk in this.chkBoxes)
            {
                if (chk.GetFormValueObjectAsBool())
                {
                    paths.Add((string)chk.Tag);
                }
            }

            //do something;
            viewLogFiles(paths);
        }

        protected const string QUERYSTRING_FILE = "file";
        public string QsFile { get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QUERYSTRING_FILE); } }

        protected void checkQueryStringForSearch()
        {
            if (!string.IsNullOrWhiteSpace(QsFile))
            {
                viewLogFiles(CS.General_v3.Util.ListUtil.GetListFromSingleItem(QsFile));
            }

        }


        private void viewLogFiles(IEnumerable<string> paths)
        {
            Log4NetXmlReader loader = Log4NetXmlReader.InstanceInSession;
            loader.ReadXmlFiles(paths.ToArray());

            string viewUrl = CmsRoutesMapper.Instance.GetLogFiles_ViewPage();
            CS.General_v3.Util.PageUtil.RedirectPage(viewUrl);

            
        }

        private void initColumns()
        {
            this.listingLogs.Functionality.AddColumn(new ColumnData("&nbsp;", null, CS.General_v3.Enums.SORT_TYPE.None, false));
            this.listingLogs.Functionality.AddColumn(new ColumnData("Folder", null, CS.General_v3.Enums.SORT_TYPE.None, false));
            this.listingLogs.Functionality.AddColumn(new ColumnData("Filename", null, CS.General_v3.Enums.SORT_TYPE.None, false));
            this.listingLogs.Functionality.AddColumn(new ColumnData("Date", null, CS.General_v3.Enums.SORT_TYPE.None, false));
            this.listingLogs.Functionality.AddColumn(new ColumnData("Size (KB)", null, CS.General_v3.Enums.SORT_TYPE.None, false));
            
        }

        protected List<FileInfo> sortFiles(IEnumerable<FileInfo> files)
        {
            List<FileInfo> result = new List<FileInfo>();
            result.AddRange(files);
            result.Sort((f1, f2) =>
                            {
                                string folder1 = CS.General_v3.Util.IO.GetDirName(f1.FullName).ToLower();
                                string folder2 = CS.General_v3.Util.IO.GetDirName(f2.FullName).ToLower();
                                int folderCmp = folder1.CompareTo(folder2);
                                if (folderCmp == 0)
                                {
                                    DateTime dateMod1 = f1.LastAccessTime;
                                    DateTime dateMod2 = f2.LastAccessTime;
                                    return -dateMod1.CompareTo(dateMod2);
                                }
                                else
                                {
                                    return folderCmp;
                                }
                            });
            return result;
        }

        private void loadListing()
        {
            string path = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Others_LogFilesFolder);
            if (path.StartsWith("/")) path = CS.General_v3.Util.PageUtil.MapPath(path);
            List<FileInfo> files = CS.General_v3.Util.IO.FindFilesInFolder(path, "*", true, true);
            files = sortFiles(files);


            this.listingLogs.Functionality.DataSource = files;
            this.listingLogs.Functionality.DataBind();

        }

        private List<MyCheckBox> chkBoxes = new List<MyCheckBox>();

        bool Functionality_ItemDataBound(CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses.ListingItemRow row)
        {
            string path = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Others_LogFilesFolder);
            if (path.StartsWith("/")) path = CS.General_v3.Util.PageUtil.MapPath(path);
            
            FileInfo file = (FileInfo) row.Object;
            string fullPath = file.FullName;
            string dir = CS.General_v3.Util.IO.GetDirName(fullPath);
            string relDir = CS.General_v3.Util.IO.ConvertAbsolutePathToRelativePath(dir, path);
            string filename = CS.General_v3.Util.IO.GetFilenameAndExtension(fullPath);
            string sizeInKb = (file.Length / 1024).ToString("#,0") + " KB";


            string dateModified = file.LastAccessTime.ToString("dd/MMM/yy hh:mmtt");
            int index = 0;
            {
                MyCheckBox chk = new MyCheckBox();
                chk.Tag = file.FullName;
                chk.ID = "chkFile_" + row.RowIndex;
                chkBoxes.Add(chk);

                row.DataCells[index].Controls.Add(chk);
                index++;
            }
            {
                Literal lit = new Literal();
                lit.Text = CS.General_v3.Util.Text.HtmlEncode(relDir);
                row.DataCells[index].Controls.Add(lit);
                index++;
            }
            {
                MyAnchor a = new MyAnchor();
                a.Title = a.InnerText = filename;
                string url = CmsRoutesMapper.Instance.GetLogFiles_ListingPage();
                CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass();
                urlClass[QUERYSTRING_FILE] = fullPath;
                a.Href = urlClass.ToString();
                    
                row.DataCells[index].Controls.Add(a);
                index++;
            }
            {
                Literal lit = new Literal();
                lit.Text = CS.General_v3.Util.Text.HtmlEncode(dateModified);
                row.DataCells[index].Controls.Add(lit);
                index++;
            }
            {
                Literal lit = new Literal();
                lit.Text = CS.General_v3.Util.Text.HtmlEncode(sizeInKb);
                row.DataCells[index].Controls.Add(lit);
                index++;
            }
            

            return true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.Functionality.SetPageTitle(new PageTitle("Log Browser", null));
            
            checkQueryStringForSearch();
            initHandlers();
            initColumns();
            loadListing();
        }
    }
}