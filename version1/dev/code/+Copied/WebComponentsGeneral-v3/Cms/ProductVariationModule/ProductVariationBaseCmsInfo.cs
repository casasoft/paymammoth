using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ProductVariationModule;

namespace CS.WebComponentsGeneralV3.Cms.ProductVariationModule
{
    public abstract class ProductVariationBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ProductVariationBaseCmsInfo_AutoGen
    {
        public ProductVariationBaseCmsInfo(ProductVariationBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();
            initListingItems();
            initRequired();
            initListingPriority();
            initLinksInCMS();
            initEditPriorities();
            this.MediaItems.ShowLinkInCms = true;
        }

        private void initLinksInCMS()
        {
            
        }

        private void initListingItems()
        {
            this.Colour.ShowInListing = true;
            this.Size.ShowInListing = true;
            this.Quantity.ShowInListing = true;
            this.Barcode.ShowInListing = true;
            this.Colour.ShowInEdit = true;
        }

        private void initRequired()
        {
            //this.Colour.IsRequired = true;
        }

        private void initListingPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Barcode, this.Colour, this.Size, this.Quantity);
        }

        private void initEditPriorities()
        {
            this.EditOrderPriorities.AddColumnsToStart(Barcode, SupplierRefCode, Size, Colour, this.Quantity);
        }
	}
}
