using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ProductVariationModule;

namespace CS.WebComponentsGeneralV3.Cms.ProductVariationModule
{
    public abstract class ProductVariationBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.ProductVariationBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            this.TitlePlural = "Product Variations";
            this.TitleSingular = "Product Variation";
            base.initCmsParameters();
        }
        
    }
}
