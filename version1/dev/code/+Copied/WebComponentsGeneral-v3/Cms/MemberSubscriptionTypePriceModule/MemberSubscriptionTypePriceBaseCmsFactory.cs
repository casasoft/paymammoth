using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;

namespace CS.WebComponentsGeneralV3.Cms.MemberSubscriptionTypePriceModule
{
    public abstract class MemberSubscriptionTypePriceBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.MemberSubscriptionTypePriceBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.
            
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

    }
}
