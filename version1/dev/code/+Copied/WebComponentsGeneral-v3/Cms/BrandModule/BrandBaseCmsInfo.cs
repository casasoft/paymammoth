using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.BannerModule;
using BusinessLogic_v3.Frontend.BrandModule;
using BusinessLogic_v3.Modules.BrandModule;

namespace CS.WebComponentsGeneralV3.Cms.BrandModule
{
    public abstract class BrandBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.BrandBaseCmsInfo_AutoGen
    {
        public BrandBaseCmsInfo(BrandBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();
            this.Title.ShowInListing = true;
            initListingItems();
            initListingPriority();
            initMediaItem();
            this.ImportReference.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);

        }

        private void initMediaItem()
        {
            this.Image = this.AddPropertyForMediaItem<BrandBase>(item => item.BrandLogo, true, true);
        }
        public CmsPropertyMediaItem<BrandBase> Image { get; set; }

        private void initListingItems()
        {
            this.Priority.EditableInListing = true;
            Title.ShowInListing = true;
            BrandLogoFilename.ShowInEdit = false;
            BrandLogoFilename.ShowInListing = false;
            Description.ShowInListing = true;

            Url.IsRequired = false;
        }

        private void initListingPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Priority, Title, Description);
            this.EditOrderPriorities.AddColumnsToStart(this.Title, this.Priority, this.Image, this.Description, this.Url, this.ImportReference);
        }


	}
}
