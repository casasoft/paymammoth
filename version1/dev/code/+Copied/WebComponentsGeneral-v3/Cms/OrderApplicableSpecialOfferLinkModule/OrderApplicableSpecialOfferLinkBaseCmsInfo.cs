using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.OrderApplicableSpecialOfferLinkModule
{

//BaseCmsInfo-Class

    public abstract class OrderApplicableSpecialOfferLinkBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.OrderApplicableSpecialOfferLinkBaseCmsInfo_AutoGen
    {
    	public OrderApplicableSpecialOfferLinkBaseCmsInfo(OrderApplicableSpecialOfferLinkBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
