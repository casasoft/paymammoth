using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.EventModule;

namespace CS.WebComponentsGeneralV3.Cms.EventModule
{
    public abstract class EventBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.EventBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
        
        protected override void initCustomCmsOperations()
        {    //here you should initialise any custom cms operations.  The main reason being that if this is
        //called in the initCmsParameters, you may not yet know the base url
            
            base.initCustomCmsOperations();
        }

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            initRefreshLucene();
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

        private void initRefreshLucene()
        {

            var loggedUser = getLoggedCmsUser();
            {
                if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator))
                {
                    var op = new CmsGeneralOperation("Refresh Lucene Index", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.ReOrder, refreshLucene);
                    op.ConfirmMessage = "Refreshing the Lucene Index might take some time! Do only if search results are not working correctly!\n\nAlso, index is refreshed for this sections only. Other sections must be refreshed separately.";
                    this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
                }
            }
        }
        private void refreshLucene()
        {
            EventBaseFactory.Instance.ReCreateLuceneIndex();
        }
    }
}
