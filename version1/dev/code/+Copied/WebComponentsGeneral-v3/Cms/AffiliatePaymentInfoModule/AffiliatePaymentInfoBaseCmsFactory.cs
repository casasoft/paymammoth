using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.AffiliatePaymentInfoModule;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.AffiliatePaymentInfoModule
{
    public abstract class AffiliatePaymentInfoBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.AffiliatePaymentInfoBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.BagOfMoney;
            var user = getLoggedCmsUser();
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToAdd.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, null);
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToView.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, null);
            if (user != null && user.CheckAccessIsLessThan(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator))
            {
                this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
            }

            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }


        protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
            var q = AffiliatePaymentInfoBase.Factory.GetQuery(qParams);
            
            var loggedInUser = (CmsUserBase)getCurrentLoggedUser();

            if (loggedInUser.GetUserAccessType() == CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser)
            {
                q.Left.JoinQueryOver(item => item.Affiliate).Where(item => item.LinkedCmsUser.ID == loggedInUser.ID);
                //CS.General_v3.Util.nHibernateUtil.GetAssociationLinkCriteriaByPath<AffiliatePaymentInfoBase>(q.RootCriteria, item => item.Affiliate);
                //q.Where(item => item.Affiliate.LinkedCmsUser ==  loggedInUser);
            }
            return q;
        }

        protected override void initCustomCmsOperations()
        {    //here you should initialise any custom cms operations.  The main reason being that if this is
        //called in the initCmsParameters, you may not yet know the base url
            
            base.initCustomCmsOperations();
        }


        
    }
}
