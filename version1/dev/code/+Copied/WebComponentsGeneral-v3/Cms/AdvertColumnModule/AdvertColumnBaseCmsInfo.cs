using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.AdvertColumnModule;

namespace CS.WebComponentsGeneralV3.Cms.AdvertColumnModule
{
    public abstract class AdvertColumnBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.AdvertColumnBaseCmsInfo_AutoGen
    {
        public AdvertColumnBaseCmsInfo(AdvertColumnBase item)
            : base(item)
        {
            this.AccessTypeRequired_ToDelete.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            
        }

        protected override void customInitFields()
        {
            base.customInitFields();
            this.AdvertSlots.ShowLinkInCms = true;
            this.Priority.EditableInListing = true;
            this.Title.ShowInListing = true;
            this.Identifier.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
        }

	}
}
