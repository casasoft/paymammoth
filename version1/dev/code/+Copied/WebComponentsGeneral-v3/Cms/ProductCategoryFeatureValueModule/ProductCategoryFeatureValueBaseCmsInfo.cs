using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule;

namespace CS.WebComponentsGeneralV3.Cms.ProductCategoryFeatureValueModule
{
    public abstract class ProductCategoryFeatureValueBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ProductCategoryFeatureValueBaseCmsInfo_AutoGen
    {
    	public ProductCategoryFeatureValueBaseCmsInfo(ProductCategoryFeatureValueBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
