using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes;


using BusinessLogic_v3.Modules.ShippingRatesGroupModule;

namespace CS.WebComponentsGeneralV3.Cms.ShippingRatesGroupModule
{
    public abstract class ShippingRatesGroupBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ShippingRatesGroupBaseCmsInfo_AutoGen
    {
    	public ShippingRatesGroupBaseCmsInfo(ShippingRatesGroupBase item)
            : base(item)
        {
            
        }
        private void addCountriesApplicable()
        {
            {
                this.CountriesApplicableNew = this.AddProperty(new CmsCountryProperty(this, CS.General_v3.Util.ReflectionUtil<ShippingRatesGroupBase>.GetPropertyBySelector(item => item.CountriesApplicable)));
                this.CountriesApplicableNew.ShowInListing = true;
            }
        }

        protected override void customInitFields()
        {
            //custom init field logic here
            this.ShippingRatePrices.ShowLinkInCms = true;
            base.customInitFields();
            
            this.Title.IsRequired = true;
            this.Title.ShowInListing = true;
            this.ShippingMethod.ShowInEdit = true;
            this.AppliesToRestOfTheWorld.HelpMessage = 
@"If this is ticked, and no country match is found for the selected method, this rate group will be used. If more than one are ticked as rest of the world
 the last one is saved, and all others are resetted";

            

            this.RemoveProperty(base.CountriesApplicable);

            addCountriesApplicable();

            this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.CountriesApplicableNew);
            this.EditOrderPriorities.AddColumnsToStart(this.Title, this.CountriesApplicableNew);

        }
        public new CmsCountryProperty CountriesApplicableNew { get; private set; }
	}
}
