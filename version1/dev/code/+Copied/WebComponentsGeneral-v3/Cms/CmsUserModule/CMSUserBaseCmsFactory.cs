using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.CmsUserModule
{
    public abstract class CmsUserBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.CmsUserBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Member2;
            
            this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
            base.initCmsParameters();
        }


        protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
            var q = CmsUserBase.Factory.GetQuery(qParams);

            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            CS.General_v3.Enums.CMS_ACCESS_TYPE maxAccessTypeToShow = CS.General_v3.Enums.CMS_ACCESS_TYPE.Everyone;
            if (loggedUser != null)
            {
                maxAccessTypeToShow = loggedUser.GetUserAccessType();
            }
            if (!loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft))
            {
                q = q.Where(item => !item.HiddenFromNonCasaSoft);// dont show users marked as 'HiddenFromNonCasaSoft' if logged in is not casasoft.
            }
            q = q.Where(item => item.AccessType <= maxAccessTypeToShow);
            return q;
        }
    }
}
