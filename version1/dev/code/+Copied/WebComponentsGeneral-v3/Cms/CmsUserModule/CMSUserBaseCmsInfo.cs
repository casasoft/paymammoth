using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.CmsUserModule;
using BusinessLogic_v3.Modules.CmsUserModule;

namespace CS.WebComponentsGeneralV3.Cms.CmsUserModule
{
    public abstract class CmsUserBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.CmsUserBaseCmsInfo_AutoGen
    {
        public CmsUserBaseCmsInfo(CmsUserBase item)
            : base(item)
        {

            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            if (loggedUser == null || (item != null && (int)loggedUser.GetUserAccessType() < (int)item.AccessType))
            {
                this.AccessTypeRequired_ToView.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
                this.AccessTypeRequired_ToDelete.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
                this.AccessTypeRequired_ToEdit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            }
        }


        private CmsPropertyMediaItem<CmsUserBase> CmsCustomLogo { get; set; }
        private void initCmsCustomLogo()
        {
            CmsCustomLogo = this.AddPropertyForMediaItem<CmsUserBase>(item => item.CmsCustomLogo, false);

        }


        private CmsAccessType checkAccessTypeForCms(CmsAccessType accessType)
        {
            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            if (loggedUser == null || ((int)loggedUser.GetUserAccessType() < (int)this.DbItem.AccessType))
            {
                accessType.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne;
            }
            return accessType;
        }

        private void checkAccess(CmsAccessType accessType)
        {
            if (this.DbItem != null)
            {
                var userAccessType = this.checkAccessTypeForCms(accessType);

            }
        }
        protected override void customInitFields()
        {
            base.customInitFields();
            initCmsCustomLogo();


            this.AccessType.CustomListItemCollectionRetriever += new CmsPropertyInfo.CustomListItemCollectionDelegate(AccessType_CustomEnumToListItemCollectionRetriever);
            this.CmsCustomLogo.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.CmsCustomLogo.HelpMessage = "If specified, this logo will appear underneath the main logo. This can be used for customisability purposes";
            this.CmsCustomLogoLinkUrl.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.CmsCustomLogoLinkUrl.HelpMessage = "The link the custom logo will link to, if specified";
            this.CmsCustomLogoFilename.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);

            this.Name.ShowInListing = true;
            this.Surname.ShowInListing = true;
            this.Username.ShowInListing = true;
            this.AccessType.ShowInListing = true;
            this.Disabled.ShowInListing = true;
            this.AccessType.IsRequired= true;
            this.Name.IsRequired = true;
            this.Username.IsRequired = true;
            this.Password.IsRequired = true;
            this.HiddenFromNonCasaSoft.AccessTypeRequired_View.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.HiddenFromNonCasaSoft.ShowInListing = true;
            this.HiddenFromNonCasaSoft.HelpMessage = "This flag is only visible for 'CasaSoft' access.  If ticked, this user will not be shown for any non-CasaSoft users, even if it is of less access type";

            this.LastLoggedIn.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne;
            this._LastLoggedIn_New.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.Password.HelpMessage = "Please note that the password is 'hashed', so you cannot know what the password is.  However, if you enter a new password, it will be saved and the hashed value will be visible in this field";
            this.SessionGUID.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne;
            this.PasswordIterations.ShowInEdit = false;
            //this.LinkedToAffiliate.ShowInEdit = false;
            //this.LinkedToAffiliate.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal, CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.EditOrderPriorities.AddColumnsToStart(this.ID, this.Name, this.Surname, this.Username, this.Password, this.AccessType, this.AccessType, this.CmsUserRoles);
            this.ListingOrderPriorities.AddColumnsToStart(this.Name, this.Surname, this.Username, this.AccessType, this.AccessType);
            this.Disabled.EditableInListing = true;
            this.PasswordSalt.ShowInEdit = false;
            this.PasswordEncryptionType.ShowInEdit = false;
        }

        ListItemCollection AccessType_CustomEnumToListItemCollectionRetriever(CmsPropertyInfo propertyInfo, ICmsItemInstance item, CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.SECTION_TYPE sectionType )
        {
            ListItemCollection listItems = new ListItemCollection();
            var accessTypes = CS.General_v3.Util.EnumUtils.GetListOfEnumValues(typeof(CS.General_v3.Enums.CMS_ACCESS_TYPE));

            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            if (loggedUser != null)
            {
                int currentAccessType = (int)loggedUser.GetUserAccessType();
                List<int> allowedAccessAsInts = new List<int>();
                foreach (Enum accessType in accessTypes)
                {
                    int val = (int)(CS.General_v3.Enums.CMS_ACCESS_TYPE)accessType;
                    if (val <= currentAccessType)
                    {
                        allowedAccessAsInts.Add(val);
                    }
                }
                if (!propertyInfo.IsRequired)
                    listItems.Add(new ListItem("", "0"));
                allowedAccessAsInts.Sort();
                foreach (var access in allowedAccessAsInts)
                {
                    CS.General_v3.Enums.CMS_ACCESS_TYPE cmsAccessType =   (CS.General_v3.Enums.CMS_ACCESS_TYPE)access;
                    listItems.Add(new ListItem(CS.General_v3.Util.EnumUtils.StringValueOf(cmsAccessType), access.ToString()));
                }
            }
            return listItems;

        }
        //public override CmsAccessType AccessTypeRequired_ToEdit
        //{
        //    get
        //    {
        //        return this.DbItem.checkAccessTypeForCms(base.AccessTypeRequired_ToEdit);
        //    }
        //}
        //public override CmsAccessType AccessTypeRequired_ToDelete
        //{
        //    get
        //    {
        //        return this.DbItem.checkAccessTypeForCms(base.AccessTypeRequired_ToDelete);
        //    }
        //}
        //public override CmsAccessType AccessTypeRequired_ToView
        //{
        //    get
        //    {
        //        return this.DbItem.checkAccessTypeForCms(base.AccessTypeRequired_ToView);
        //    }
        //}

        public override string TitleForCms
        {
            get
            {
                return this.DbItem.Username;
                
            }
        }
	}
}
