using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.LocationModule;

namespace CS.WebComponentsGeneralV3.Cms.LocationModule
{
    public abstract class LocationBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.LocationBaseCmsInfo_AutoGen
    {
    	public LocationBaseCmsInfo(LocationBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
