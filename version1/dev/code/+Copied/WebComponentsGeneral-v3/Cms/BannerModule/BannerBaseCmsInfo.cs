using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.BannerModule;
using BusinessLogic_v3.Modules.BannerModule;

namespace CS.WebComponentsGeneralV3.Cms.BannerModule
{
    public abstract class BannerBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.BannerBaseCmsInfo_AutoGen
    {
    	public BannerBaseCmsInfo(BannerBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();

            initRequired();
            initListingItems();
            initListingItemsPriority();
            initUpload();
            MediaItemFilename.ShowInEdit = MediaItemFilename.ShowInListing = false;
            Identifier.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.EditOrderPriorities.AddColumnsToStart(this.Title, this.Identifier, this.Link, this.Image, this.HrefTarget, this.Priority, this.Published, this.SlideshowDelaySec, this.DurationMS);
            this.Image.SubTitle = "(1000 x 270px)";
        }

        private void initUpload()
        {
            this.Image = this.AddPropertyForMediaItem<BannerBase>(item => item.MediaItem, true, true);
        }
        public CmsPropertyMediaItem<BannerBase> Image { get; set; }

        private void initListingItemsPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Identifier, this.Title, this.Link, this.DurationMS);

            //this.Identifier.ListingColumnPriority = 1000;
            //this.Title.ListingColumnPriority = 2000;
            //this.MediaItemURL.ListingColumnPriority = 3000;
            //this.Link.ListingColumnPriority = 4000;
            //this.DurationMS.ListingColumnPriority = 5000;
        }

        private void initRequired()
        {
            //Identifier.IsRequired = true;
            Title.IsRequired = true; 
            
            DurationMS.IsRequired = true;
        }

        private void initListingItems()
        {
            DurationMS.ShowInListing = true; 
            Link.ShowInListing = true; 
            

            Title.ShowInListing = true; 
            Identifier.ShowInListing = true;
        }
	}
}
