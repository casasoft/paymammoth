using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.AdvertModule;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

namespace CS.WebComponentsGeneralV3.Cms.AdvertModule
{
    public abstract class AdvertBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.AdvertBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            //custom CMS defaults




            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Advert;
        }
    }
}
