using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;


using BusinessLogic_v3.Modules.AdvertModule;

namespace CS.WebComponentsGeneralV3.Cms.AdvertModule
{
    public abstract class AdvertBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.AdvertBaseCmsInfo_AutoGen
    {
        public AdvertBaseCmsInfo(AdvertBase item)
            : base(item)
        {

        }

        private void initImages()
        {
            this.Image = this.AddPropertyForMediaItem<AdvertBase>(item => item.MediaItem, isRequired: true);
            
        }
        protected override void customInitFields()
        {
            initImages();
            //custom init field logic here
            
            base.customInitFields();
            this.AdvertShownPerRound.ShowInListing = true;
            this.AdvertShownPerRound.HelpMessage = @"This is the 'ratio' by which this advert is shown, in relation to other adverts.  If there are 2 adverts, one of them has a ratio of 2, while another has a ratio of 1
 it means that the first advert is shown twice as much as the second advert";

            this.Published.EditableInListing = true;
            this.Link.ShowInListing = true;
            this.ShowDateFrom.ShowInListing = true;
            this.ShowDateTo.ShowInListing = true;
            this.Title.ShowInListing = true;
            this.Filename.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            //this.AdvertShownPerRound.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.RoundCounter.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.VideoDuration.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.Image.ShowInListing = true;
            this.Title.IsRequired = true;
            this.HrefTarget.IsRequired = true;

            this.EditOrderPriorities.AddColumnsToStart(this.ID, this.Title, this.Link, this.HrefTarget, this.Image, this.Published, this.ShowDateFrom, this.ShowDateTo, this.AdvertShownPerRound);

            

            

        }
        public CmsPropertyMediaItem<AdvertBase> Image { get; private set; }
	}
}
