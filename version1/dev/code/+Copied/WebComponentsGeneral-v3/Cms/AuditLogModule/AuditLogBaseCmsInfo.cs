using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.AuditLogModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3.Cms.AuditLogModule
{

//BaseCmsInfo-Class

    public abstract class AuditLogBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.AuditLogBaseCmsInfo_AutoGen
    {
    	public AuditLogBaseCmsInfo(AuditLogBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            addButtonsForCustomOperations();
            initCustomProperties();

            this.AccessTypeRequired_ToDelete.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.AccessTypeRequired_ToEdit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.CmsUser.ShowInEdit = true;
            this.DateTime.ShowInListing = true;
            this.ItemID.ShowInListing = true;
            this.ItemType.ShowInListing = true;
            this.CmsUser.ShowInListing = true;
            this.UpdateType.ShowInListing = true;
            this.Message.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(this.DateTime, this.ItemType, this.ItemID, this.CmsUser, this.UpdateType, this.Message);
            this.AccessTypeRequired_ToEdit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.SetDefaultSortField(this.DateTime, General_v3.Enums.SORT_TYPE.Descending);
            base.Changes.ShowInEdit = false;
            this.ChangesTable.ShowInEdit = true;
            this.StackTrace.StringDataType = General_v3.Enums.STRING_DATA_TYPE.MultiLine;
            this.EditOrderPriorities.AddColumnsToStart(this.DateTime, this.UpdateType, this.ItemType, this.ItemTypeFull, this.ItemID, this.Message, this.Method, this.StackTrace, this.ChangesTable);
            base.customInitFields();
            
        }

        private void customOperation_RevertChanges()
        {
            var changeResults = this.DbItem.RevertChange();
            bool success = (changeResults.CheckIfAllWereSuccessful());
            if (success)
                CmsUtil.ShowStatusMessageInCMSAndRefresh("Changes reverted successfully", General_v3.Enums.STATUS_MSG_TYPE.Success);
            else
                CmsUtil.ShowStatusMessageInCMSAndRefresh("Changes reverted but included some errors/warnings", General_v3.Enums.STATUS_MSG_TYPE.Warning);

        }

        private void addButtonsForCustomOperations()
        {
            if (this.DbItem != null)
            {
                {
                    var customOp = new CmsItemSpecificOperation(this, "Revert Changes", Code.Cms.EnumsCms.IMAGE_ICON.GoBack, customOperation_RevertChanges);
                    customOp.ConfirmMessage = "Are you sure you want to revert these changes?";
                    this.CustomCmsOperations.Add(customOp);
                }
                {
                    Type t = this.DbItem.GetItemType();
                    if (t != null)
                    {
                        var cmsFactory =  BusinessLogic_v3.Classes.Cms.Factories.CmsFactoriesController.Instance.GetFactoryForType(t);
                        string url = cmsFactory.GetEditUrlForItemWithID(this.DbItem.ItemID).GetURL(fullyQualified: true);

                        var customOp = new CmsItemSpecificOperation(this, "View Related Item", Code.Cms.EnumsCms.IMAGE_ICON.Listing, url);
                        
                        this.CustomCmsOperations.Add(customOp);
                    }
                }
            }

        }
        private void initCustomProperties()
        {
            initProperty_Changes();
        }

        private void initProperty_Changes()
        {
            this.ChangesTable = new CmsPropertyLiteral<AuditLogBase>(this, "Changes", x => 
            {
                CustomProperties.ChangesControl changesCtrl = new CustomProperties.ChangesControl();
                changesCtrl.Functionality.AuditLog = x;
                return changesCtrl;
            });

            this.AddProperty(this.ChangesTable);
        }

        public CmsPropertyLiteral<AuditLogBase> ChangesTable { get; set; }
    }
}
