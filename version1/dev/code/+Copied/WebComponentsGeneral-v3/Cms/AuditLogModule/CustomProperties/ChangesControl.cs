﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;
using BusinessLogic_v3.Modules.AuditLogModule;

namespace CS.WebComponentsGeneralV3.Cms.AuditLogModule.CustomProperties
{
    public class ChangesControl : MyTable
    {


        #region ChangesControl Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected ChangesControl _item = null;
            internal FUNCTIONALITY(ChangesControl item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "item is required");
                this._item = item;
            }
            public AuditLogBase AuditLog { get; set; }
        }

        private void initHeader()
        {
            var tr = this.AddRow(cssClass:"header");

            tr.AddCell(cssClass: "header", innerHtml: "Field");
            tr.AddCell(cssClass: "header", innerHtml: "From");
            tr.AddCell(cssClass: "header", innerHtml: "To");

        }

        private string parseValue(string s)
        {
            string result = null;
            if (s != null)
            {
                if (s.Contains("<p>") || s.Contains("/>") || s.Contains("<a>") || s.Contains("<div>"))
                {
                    result = s;
                }
                else
                {
                    result = CS.General_v3.Util.Text.HtmlEncode(s);
                }
            }
            return result;
        }
        private void renderChanges()
        {
            var changes = this.Functionality.AuditLog.ParseChanges();
            foreach (var change in changes.Changes)
            {
                addChangeRow(change);
            }
        }

        private void addChangeRow(BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogChange change)
        {
            var tr = this.AddRow(cssClass: "change");



            tr.AddCell(cssClass: "field", innerHtml: parseValue(change.PropertyName));
            tr.AddCell(cssClass: "from", innerHtml: parseValue(change.From));
            tr.AddCell(cssClass: "to", innerHtml: parseValue(change.To));
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			


        public ChangesControl()
        {
            this.CssManager.AddClass("changes-control");

        }
        private void renderTable()
        {
            initHeader();
            renderChanges();
            
        }

        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
            renderTable();
        }

    }
}