using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.AuditLogModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using NHibernate;

namespace CS.WebComponentsGeneralV3.Cms.AuditLogModule
{

//BaseCmsFactory-Class

    public abstract class AuditLogBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.AuditLogBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {

            cmsInfo.AccessTypeRequired_ToDelete.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            cmsInfo.AccessTypeRequired_ToAdd.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            


        	//any user-specific information, add them to 'cmsInfo'.
            cmsInfo.ShowInCmsMainMenu = true;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        protected override void parseQueryForListing(NHibernate.IQueryOver query)
        {
            NHibernate.IQueryOver<AuditLogBase,AuditLogBase> q = (IQueryOver<AuditLogBase, AuditLogBase>) query;
            var loggedUser = getLoggedCmsUser();
            if (!loggedUser.CheckAccessTypeAtLeast(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam))
            { //if not casasoft, show only those that where done from the cms
                q = q.Where(x => x.CmsUser != null);
            }

            base.parseQueryForListing(query);
        }
    }
}
