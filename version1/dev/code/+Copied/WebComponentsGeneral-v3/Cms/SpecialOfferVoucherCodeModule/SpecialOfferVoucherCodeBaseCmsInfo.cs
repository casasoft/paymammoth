using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;

namespace CS.WebComponentsGeneralV3.Cms.SpecialOfferVoucherCodeModule
{
    public abstract class SpecialOfferVoucherCodeBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.SpecialOfferVoucherCodeBaseCmsInfo_AutoGen
    {
        public SpecialOfferVoucherCodeBaseCmsInfo(SpecialOfferVoucherCodeBase item)
            : base(item)
        {

        }

		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            
            this.VoucherCode.IsRequired = true;
            this.VoucherCode.ShowInListing = true;
            this.QuantityLeft.SetShowInListing(true);
            this.CreatedOn.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(CreatedOn, VoucherCode, QuantityLeft);
            this.EditOrderPriorities.AddColumnsToStart(Priority, VoucherCode, QuantityLeft);
        }
	}
}
