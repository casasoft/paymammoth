using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Cms.SpecialOfferModule;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;

namespace CS.WebComponentsGeneralV3.Cms.SpecialOfferVoucherCodeModule
{
    public abstract class SpecialOfferVoucherCodeBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.SpecialOfferVoucherCodeBaseCmsFactory_AutoGen
    {


        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
        public string GetGenerateVoucherCodesUrl(SpecialOfferBaseCmsInfo specialOffer)
        {
            return this.GetCmsFolderUrl() + "generateCodes.aspx?" + CS.WebComponentsGeneralV3.Code.Cms.Util.QueryStringUtil.ParamSpecialOfferID + "=" + specialOffer.ID;
        }
        public string GetImportVoucherCodesUrl(SpecialOfferBaseCmsInfo specialOffer)
        {
            return this.GetCmsFolderUrl() + "import.aspx?" + CS.WebComponentsGeneralV3.Code.Cms.Util.QueryStringUtil.ParamSpecialOfferID + "=" + specialOffer.ID;
        }
    }
}
