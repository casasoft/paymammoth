using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.ProductVariationColour_CultureInfoModule
{

//BaseCmsInfo-Class

    public abstract class ProductVariationColour_CultureInfoBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ProductVariationColour_CultureInfoBaseCmsInfo_AutoGen
    {
    	public ProductVariationColour_CultureInfoBaseCmsInfo(ProductVariationColour_CultureInfoBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
