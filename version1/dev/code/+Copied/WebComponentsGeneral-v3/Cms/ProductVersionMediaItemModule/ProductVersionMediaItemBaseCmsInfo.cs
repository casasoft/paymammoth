using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductVersionMediaItemModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.ProductVersionMediaItemModule
{

    //BaseCmsInfo-Class

    public abstract class ProductVersionMediaItemBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ProductVersionMediaItemBaseCmsInfo_AutoGen
    {
        public ProductVersionMediaItemBaseCmsInfo(ProductVersionMediaItemBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            //custom init field logic here
            initMediaItem();
            this.Image.ShowInListing = true;
            this.IsMainImage.ShowInListing = true;
            this.IsMainImage.EditableInListing = true;
            this.ID.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(ID, Image, IsMainImage);
            this.EditOrderPriorities.AddColumnsToStart(ID, IsMainImage, Image);
            base.customInitFields();
        }

        private void initMediaItem()
        {
            this.Image = this.AddPropertyForMediaItem<ProductVersionMediaItemBase>(item => item.Image, true, true);
        }
        public CmsPropertyMediaItem<ProductVersionMediaItemBase> Image { get; set; }
    }
}
