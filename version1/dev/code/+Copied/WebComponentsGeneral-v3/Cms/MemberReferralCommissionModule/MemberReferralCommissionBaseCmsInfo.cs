using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.MemberReferralCommissionModule;

namespace CS.WebComponentsGeneralV3.Cms.MemberReferralCommissionModule
{
    public abstract class MemberReferralCommissionBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.MemberReferralCommissionBaseCmsInfo_AutoGen
    {
    	public MemberReferralCommissionBaseCmsInfo(MemberReferralCommissionBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
