using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.PopularSearchModule;

namespace CS.WebComponentsGeneralV3.Cms.PopularSearchModule
{
    public abstract class PopularSearchBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.PopularSearchBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
            this.UserSpecificGeneralInfoInContext.TitlePlural = "Popular Searches";
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Photos;
        }
    }
}
