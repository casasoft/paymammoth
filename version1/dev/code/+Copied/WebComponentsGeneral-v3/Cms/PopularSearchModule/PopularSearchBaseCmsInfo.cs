using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;



using BusinessLogic_v3.Modules.PopularSearchModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Cms.PopularSearchModule
{
    public abstract class PopularSearchBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.PopularSearchBaseCmsInfo_AutoGen
    {
    	public PopularSearchBaseCmsInfo(PopularSearchBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            initSeoUrl();
            base.customInitFields();
            initRequired();
            initListingItems();
            initListingItemsPriority();
            this.SeoUrl.HelpMessage = "This is the search url that would be visible for the frontend";
        }
        private void initSeoUrl()
        {
            this.SeoUrl = this.AddProperty(new CmsPropertyLiteral<PopularSearchBase>(this,
                "Seo Url", 
            dbItem =>
                {
                    MyAnchor a = new MyAnchor();

                    
                    a.Href = dbItem.GetURL();
                    a.HrefTarget = CS.General_v3.Enums.HREF_TARGET.Blank;
                    a.InnerText = a.Title = a.Href;
                    return a;
                }));


                   
        }

        public CmsPropertyLiteral<PopularSearchBase> SeoUrl { get; set; }

        private void initListingItemsPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.SearchURL, this.Published, this.HideFromFrontend, this.Priority);
            this.EditOrderPriorities.AddColumnsToStart(this.Title, this.SearchURL, this.Published, this.Priority, this.SeoUrl);


            this.Published.EditableInListing = true;
            this.HideFromFrontend.EditableInListing = true;

            //this.Title.ListingColumnPriority = 1000;
            //this.SearchURL.ListingColumnPriority = 2000;
            //this.Activated.ListingColumnPriority = 3000;
            //this.Priority.ListingColumnPriority = 4000;
        }

        private void initRequired()
        {
            Title.IsRequired = true;
            SearchURL.IsRequired = true;
            Published.IsRequired = true;
            Priority.IsRequired = true;
        }

        private void initListingItems()
        {
            Title.ShowInListing = true;
            SearchURL.ShowInListing = true;
            Published.ShowInListing = true;
            Priority.ShowInListing = true;
        }
    }
}
