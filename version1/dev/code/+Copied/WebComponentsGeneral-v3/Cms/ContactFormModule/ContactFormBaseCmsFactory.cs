using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ContactFormModule;

namespace CS.WebComponentsGeneralV3.Cms.ContactFormModule
{
    public abstract class ContactFormBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.ContactFormBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Email;
            this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
            base.initCmsParameters();
        }

    }
}
