using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ArticleMediaItemModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms.ArticleMediaItemModule
{
    public abstract class ArticleMediaItemBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ArticleMediaItemBaseCmsInfo_AutoGen
    {
        public ArticleMediaItemBaseCmsInfo(ArticleMediaItemBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            initUpload();
            this.ProductImage.ShowInEdit = true;
            base.customInitFields();
        }

        private void initUpload()
        {
            this.ProductImage = this.AddPropertyForMediaItem<ArticleMediaItemBase>(item => item.Image, true, true);
        }
        public CmsPropertyMediaItem<ArticleMediaItemBase> ProductImage { get; set; }
	}
}
