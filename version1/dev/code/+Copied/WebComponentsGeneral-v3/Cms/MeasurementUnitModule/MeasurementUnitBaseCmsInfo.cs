using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.MeasurementUnitModule;

namespace CS.WebComponentsGeneralV3.Cms.MeasurementUnitModule
{
    public abstract class MeasurementUnitBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.MeasurementUnitBaseCmsInfo_AutoGen
    {
    	public MeasurementUnitBaseCmsInfo(MeasurementUnitBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            initListingItems();
            initListingPriority();
		    initRequiredFields();
        }

        private void initRequiredFields()
        {
            Title.IsRequired = true;
            Description.IsRequired = false;
            Description.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;
        }
        
        private void initListingItems()
        {
            Title.ShowInListing = true;
            Description.ShowInListing = true;
        }

        private void initListingPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.Description);
            this.EditOrderPriorities.AddColumnsToStart(this.Title, this.Description);
        }
	}
}
