using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.MeasurementUnitModule;

namespace CS.WebComponentsGeneralV3.Cms.MeasurementUnitModule
{
    public abstract class MeasurementUnitBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.MeasurementUnitBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Ruler;
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
    }
}
