using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using BusinessLogic_v3.Util;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using NHibernate;
using NHibernate.Criterion;
using CS.General_v3.Util;

using BusinessLogic_v3.Modules.ProductModule;

namespace CS.WebComponentsGeneralV3.Cms.ProductModule
{
    public abstract class ProductBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.ProductBaseCmsFactory_AutoGen
    {
        public ProductBaseCmsFactory()
        {
            
        }

        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.TitlePlural = "Products";
            this.UserSpecificGeneralInfoInContext.TitleSingular = "Product";
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Bookshelf;
            addUpdateProductsButton();
            addImportButton();
            addCleanUpDbButton();
            addCleanUpMissingImagesButton();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }


        private void addCleanUpMissingImagesButton()
        {
            var loggedUser = getLoggedCmsUser();
            if (ProductBaseCmsFactory.UsedInProject && loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft))
            {

                CmsGeneralOperation op = new CmsGeneralOperation("Clean up missing images", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Delete, cleanUpMissingImages);
                op.ConfirmMessage = "Are you sure you want to clean up any missing images? This action is irreversible!";
                this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
            }
        }
        private void cleanUpMissingImages()
        {
            ProductVariationMediaItemCleaner cleaner = new ProductVariationMediaItemCleaner();
            var deletedItems = cleaner.CleanUpMissingFiles();
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Cleaned up missing images.  Removed " + deletedItems.Count() + " images in all", General_v3.Enums.STATUS_MSG_TYPE.Success);
            
        }

        private void addCleanUpDbButton()
        {
            var loggedUser = getLoggedCmsUser();
            if (ProductBaseCmsFactory.UsedInProject && loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft))
            {

                CmsGeneralOperation op = new CmsGeneralOperation("Clean up Db", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Delete, cleanUpProducts);
                op.ConfirmMessage = "Are you sure you want to clean up ALL products? This action is irreversible!";
                this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
            }
        }
        private void cleanUpProducts()
        {
            ProductDbCleaner cleaner = new ProductDbCleaner();
            cleaner.CleanUpDb();
            CmsUtil.ShowStatusMessageInCMSAndRefresh("All products cleaned up!", General_v3.Enums.STATUS_MSG_TYPE.Success);

        }

        private void addImportButton()
        {
            var loggedUser = getLoggedCmsUser();
            if (ProductBaseCmsFactory.UsedInProject && BusinessLogic_v3.Modules.ModuleSettings.Shop.CanBulkImportProducts && loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator))
            {
                string url = Code.Cms.Routing.Products.ProductRoutes.GetDataImportUrl();
                this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(
                    new CmsGeneralOperation("Import", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Import, url));
            }
        }

        private void addUpdateProductsButton()
        {
            var loggedUser = getLoggedCmsUser();
            if ( loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft))
            {
                this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(
                    new CmsGeneralOperation("Update Item Groups Data", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Update, updateProductsData));

            }

        }
        private void updateProductsData()
        {
            BusinessLogic_v3.Classes.DataUpdates.ProductUpdater.Instance.UpdateAll();
            CmsUtil.ShowStatusMessageInCMS("Item groups data updated successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            cmsInfo.TitlePlural = "Products";
            cmsInfo.TitleSingular = "Product";
            if (UsedInProject)
                cmsInfo.ShowInCmsMainMenu = true;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

        public const string QUERYSTRING_FILTERCATEGORYID = "CategoryFilterId";

        public long? GetFilterCategoryIdFromQuerystring()
        {
            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long?>(QUERYSTRING_FILTERCATEGORYID);
        }



        private void checkFilterCategoryId(IQueryOver query)
        {
            var id = GetFilterCategoryIdFromQuerystring();
            if (id.GetValueOrDefault() > 0)
            {
                CategoryBase c = CategoryBase.Factory.GetByPrimaryKey(id.Value);
                if (c != null)
                {
                    var allIds = c.GetAllCategoryIDsUnderThis(includeThis: true);

                    var crit = query.RootCriteria.GetAssociationLinkCriteriaByPath<ProductBase>(item => item.CategoryLinks);
                    crit.Add(Restrictions.On<ProductCategoryBase>(link => link.Category).IsInG(allIds));

                }
            }
        }

        protected override void parseQueryForListing(NHibernate.IQueryOver query)
        {
            checkFilterCategoryId(query);

            base.parseQueryForListing(query);
        }

        

    }
}
