﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;

using BusinessLogic_v3.Modules.CategoryFeatureModule;
using BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes;


namespace CS.WebComponentsGeneralV3.Cms.ProductModule
{
    public class ProductCategoryFeaturesManyToManyParams : CmsPropertyManyToManyWithCustomClassParamsBase<ProductBase, ProductCategoryFeatureValueBase, CategoryFeatureBase>
    {
        /*

         this.CategoryFeatures = new CmsPropertyMultiChoiceGeneric<ProductBase, ProductCategoryFeatureValueBase>(
                this.factory,
                (item => item.ProductCategoryFeatureValues),
                (itemGroup => itemGroup.GetAllCategoryFeaturesAvailableForProductAsListItems(true)),
                (featureValue => featureValue.CategoryFeature.ID.ToString()),
                ((item, values) => item.SetCategoryFeaturesForProductFromStrings(values)), 
                EnumsCms.MULTICHOICE_DISPLAY_TYPE.CheckboxList);
        */


        protected override IEnumerable<System.Web.UI.WebControls.ListItem> getListItemsFromItem(ProductBase item, bool sortByText = true)
        {
            return item.GetAllCategoryFeaturesAvailableForProductAsListItems(true, sortByText: sortByText);
        }

        

        protected override System.Linq.Expressions.Expression<Func<ProductBase, ICollectionManager<ProductCategoryFeatureValueBase>>> getPropertySelector()
        {
            return item => item.ProductCategoryFeatureValues;
            
        }

        protected override System.Linq.Expressions.Expression<Func<ProductCategoryFeatureValueBase, CategoryFeatureBase>> getChildPropertyFromLinkedItemSelector()
        {
            return item => item.CategoryFeature;
            
        }

        public override NHibernate.Criterion.ICriterion GetCustomCriterionForSearch(NHibernate.ICriteria criteria, List<long> ids)
        {
            return null;
            
        }

        protected override System.Linq.Expressions.Expression<Func<ProductCategoryFeatureValueBase, ProductBase>> getParentPropertyFromLinkedItemSelector()
        {
            return item => item.Product;
        }
    }
}
