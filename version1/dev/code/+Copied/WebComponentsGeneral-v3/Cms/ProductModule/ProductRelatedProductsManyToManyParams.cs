﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductRelatedProductLinkModule;
using System.Web.UI.WebControls;

namespace CS.WebComponentsGeneralV3.Cms.ProductModule
{
    public class ProductRelatedProductsManyToManyParams : CmsPropertyManyToManyWithCustomClassParamsBase<ProductBase, ProductRelatedProductLinkBase, ProductBase>
    {
        protected override IEnumerable<System.Web.UI.WebControls.ListItem> getListItemsFromItem(ProductBase item, bool sortByText = true)
        {
            var list = ProductBase.Factory.FindAll().Where(t => t.ID != item.ID).OrderBy(t => t.GetTitle()).ToList().ConvertAll<ListItem>(
                   p => new ListItem(p.GetTitle() + " (Ref.: " + p.ReferenceCode + ")", p.ID.ToString()));
            if (sortByText)
                list.Sort((c1, c2) => (c1.Text.CompareTo(c2.Text)));
            return list;
        }

        protected override System.Linq.Expressions.Expression<Func<ProductBase, BusinessLogic_v3.Classes.DbObjects.Collections.ICollectionManager<ProductRelatedProductLinkBase>>> getPropertySelector()
        {
            return item => item.RelatedProductLinks;
        }

        protected override System.Linq.Expressions.Expression<Func<ProductRelatedProductLinkBase, ProductBase>> getChildPropertyFromLinkedItemSelector()
        {
            return item => item.Product;
        }

        protected override System.Linq.Expressions.Expression<Func<ProductRelatedProductLinkBase, ProductBase>> getParentPropertyFromLinkedItemSelector()
        {
            return item => item.Product;
        }
    }
}