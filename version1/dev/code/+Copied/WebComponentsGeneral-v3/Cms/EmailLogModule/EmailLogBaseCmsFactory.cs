using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

using BusinessLogic_v3.Modules.EmailLogModule;

namespace CS.WebComponentsGeneralV3.Cms.EmailLogModule
{
    public abstract class EmailLogBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.EmailLogBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.
            cmsInfo.AccessTypeRequired_ToAdd.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            
            cmsInfo.AccessTypeRequired_ToDelete.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            cmsInfo.AccessTypeRequired_ToView.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator);
            var loggedUser = getLoggedCmsUser();
            //if (cmsInfo.AccessTypeRequired_ToView.CheckIfUserHasEnoughAccess(loggedUser))
            {
                cmsInfo.ShowInCmsMainMenu = true;
            }
            cmsInfo.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Email;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

    }
}
