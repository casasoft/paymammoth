using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;


using BusinessLogic_v3.Modules.ContentTextModule;

namespace CS.WebComponentsGeneralV3.Cms.ContentTextModule
{
    public abstract class ContentTextBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.ContentTextBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            base.initCmsParameters();
            
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            cmsInfo.RenderType = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.SECTION_RENDER_TYPE.Tree;
            cmsInfo.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.ShortNotes;
            cmsInfo.ShowInCmsMainMenu = true;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

        public override IEnumerable<ICmsHierarchyItem> GetRootItems(IEnumerable<CmsPropertySearchInfo> searchCriteria)
        {

            List<ICmsHierarchyItem> list = new List<ICmsHierarchyItem>();
            var rootNodes = ContentTextBase.Factory.GetRootNodes(true).ToList();
            list.AddRange(rootNodes.ConvertAll(x=>GetCmsItemFromDBObject(x)));
            return list;

        }
        
    }
}
