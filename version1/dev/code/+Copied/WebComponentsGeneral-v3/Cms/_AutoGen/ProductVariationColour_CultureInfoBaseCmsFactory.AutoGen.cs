using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationColour_CultureInfoModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductVariationColour_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<ProductVariationColour_CultureInfoBaseCmsInfo, ProductVariationColour_CultureInfoBase>
    {
       
       public new static ProductVariationColour_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductVariationColour_CultureInfoBaseCmsFactory)CmsFactoryBase<ProductVariationColour_CultureInfoBaseCmsInfo, ProductVariationColour_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ProductVariationColour_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductVariationColour_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductVariationColour_CultureInfo";

            this.QueryStringParamID = "ProductVariationColour_CultureInfoId";

            cmsInfo.TitlePlural = "Product Variation Colour_ Culture Infos";

            cmsInfo.TitleSingular =  "Product Variation Colour_ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductVariationColour_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductVariationColour_CultureInfo/";
			UsedInProject = ProductVariationColour_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
