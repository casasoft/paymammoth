using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.NewsItem_CultureInfoModule;
using BusinessLogic_v3.Cms.NewsItem_CultureInfoModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class NewsItem_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.NewsItem_CultureInfoModule.NewsItem_CultureInfoBase>
    {
		public NewsItem_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.NewsItem_CultureInfoModule.NewsItem_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.NewsItem_CultureInfoModule.NewsItem_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new NewsItem_CultureInfoBase DbItem
        {
            get
            {
                return (NewsItem_CultureInfoBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; private set; }

        public CmsPropertyInfo NewsItem { get; private set; }

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo ShortDesc { get; private set; }

        public CmsPropertyInfo FullDesc { get; private set; }

        public CmsPropertyInfo FullDesc_Search { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.CultureInfo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItem_CultureInfoModule.NewsItem_CultureInfoBase>.GetPropertyBySelector( item => item.CultureInfo),
        			isEditable: true,
        			isVisible: true
        			);

        this.NewsItem = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItem_CultureInfoModule.NewsItem_CultureInfoBase>.GetPropertyBySelector( item => item.NewsItem),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItem_CultureInfoModule.NewsItem_CultureInfoBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShortDesc = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItem_CultureInfoModule.NewsItem_CultureInfoBase>.GetPropertyBySelector( item => item.ShortDesc),
        			isEditable: true,
        			isVisible: true
        			);

        this.FullDesc = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItem_CultureInfoModule.NewsItem_CultureInfoBase>.GetPropertyBySelector( item => item.FullDesc),
        			isEditable: true,
        			isVisible: true
        			);

        this.FullDesc_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItem_CultureInfoModule.NewsItem_CultureInfoBase>.GetPropertyBySelector( item => item.FullDesc_Search),
        			isEditable: false,
        			isVisible: false
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
