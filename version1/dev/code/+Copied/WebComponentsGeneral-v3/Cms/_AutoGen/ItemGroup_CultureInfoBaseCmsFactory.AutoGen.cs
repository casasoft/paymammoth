using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ItemGroup_CultureInfoModule;
using BusinessLogic_v3.Cms.ItemGroup_CultureInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ItemGroup_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<ItemGroup_CultureInfoBaseCmsInfo, ItemGroup_CultureInfoBase>
    {
       
       public new static ItemGroup_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ItemGroup_CultureInfoBaseCmsFactory)CmsFactoryBase<ItemGroup_CultureInfoBaseCmsInfo, ItemGroup_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ItemGroup_CultureInfoBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ItemGroup_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ItemGroup_CultureInfo";

            this.QueryStringParamID = "ItemGroup_CultureInfoId";

            cmsInfo.TitlePlural = "Item Group _ Culture Infos";

            cmsInfo.TitleSingular =  "Item Group _ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ItemGroup_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ItemGroup_CultureInfo/";


        }
       
    }

}
