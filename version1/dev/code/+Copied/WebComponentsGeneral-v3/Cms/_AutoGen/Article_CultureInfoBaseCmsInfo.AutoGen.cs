using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.Article_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Article_CultureInfoModule;
using BusinessLogic_v3.Frontend.Article_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class Article_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase>
    {
		public Article_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.Article_CultureInfoModule.Article_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new Article_CultureInfoBaseFrontend FrontendItem
        {
            get { return (Article_CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new Article_CultureInfoBase DbItem
        {
            get
            {
                return (Article_CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; protected set; }

        public CmsPropertyInfo Article { get; protected set; }

        public CmsPropertyInfo HtmlText { get; protected set; }

        public CmsPropertyInfo HtmlText_Search { get; protected set; }

        public CmsPropertyInfo MetaKeywords { get; protected set; }

        public CmsPropertyInfo MetaKeywords_Search { get; protected set; }

        public CmsPropertyInfo MetaDescription { get; protected set; }

        public CmsPropertyInfo MetaDescription_Search { get; protected set; }

        public CmsPropertyInfo PageTitle { get; protected set; }

        public CmsPropertyInfo PageTitle_Search { get; protected set; }

        public CmsPropertyInfo DialogWidth { get; protected set; }

        public CmsPropertyInfo DialogHeight { get; protected set; }

        public CmsPropertyInfo SubTitle { get; protected set; }

        public CmsPropertyInfo _ComputedURL { get; protected set; }

        public CmsPropertyInfo _Computed_ParentArticleNames { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
