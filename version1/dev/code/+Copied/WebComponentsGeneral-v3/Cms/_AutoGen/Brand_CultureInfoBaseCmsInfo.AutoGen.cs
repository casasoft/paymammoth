using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.Brand_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Brand_CultureInfoModule;
using BusinessLogic_v3.Frontend.Brand_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class Brand_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase>
    {
		public Brand_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.Brand_CultureInfoModule.Brand_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new Brand_CultureInfoBaseFrontend FrontendItem
        {
            get { return (Brand_CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new Brand_CultureInfoBase DbItem
        {
            get
            {
                return (Brand_CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; protected set; }

        public CmsPropertyInfo Brand { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
