using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.TicketModule;
using CS.WebComponentsGeneralV3.Cms.TicketModule;
using BusinessLogic_v3.Frontend.TicketModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class TicketBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.TicketModule.TicketBase>
    {
		public TicketBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.TicketModule.TicketBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.TicketModule.TicketBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new TicketBaseFrontend FrontendItem
        {
            get { return (TicketBaseFrontend)base.FrontendItem; }

        }
        public new TicketBase DbItem
        {
            get
            {
                return (TicketBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo IPAddress { get; protected set; }

        public CmsPropertyInfo CreatedOn { get; protected set; }

        public CmsPropertyInfo AssignedTo { get; protected set; }

        public CmsPropertyInfo Member { get; protected set; }

        public CmsPropertyInfo ClosedOn { get; protected set; }

        public CmsPropertyInfo ClosedBy { get; protected set; }

        public CmsPropertyInfo ClosedByIP { get; protected set; }

        public CmsPropertyInfo ReOpenedOn { get; protected set; }

        public CmsPropertyInfo ReOpenedByCmsUser { get; protected set; }

        public CmsPropertyInfo ReOpenedByIP { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo TicketPriority { get; protected set; }

        public CmsPropertyInfo TicketState { get; protected set; }

        public CmsPropertyInfo ReOpenedByMember { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo TicketComments { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.TicketComments = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TicketModule.TicketBase>.GetPropertyBySelector(item => item.TicketComments),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase>.GetPropertyBySelector(item => item.Ticket)));


			base.initBasicFields();
          
        }

    }
}
