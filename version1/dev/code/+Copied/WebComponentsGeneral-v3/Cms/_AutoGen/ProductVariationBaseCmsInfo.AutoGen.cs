using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ProductVariationModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationModule;
using BusinessLogic_v3.Frontend.ProductVariationModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ProductVariationBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>
    {
		public ProductVariationBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ProductVariationModule.ProductVariationBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductVariationBaseFrontend FrontendItem
        {
            get { return (ProductVariationBaseFrontend)base.FrontendItem; }

        }
        public new ProductVariationBase DbItem
        {
            get
            {
                return (ProductVariationBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo SupplierRefCode { get; protected set; }

        public CmsPropertyInfo Product { get; protected set; }

        public CmsPropertyInfo Barcode { get; protected set; }

        public CmsPropertyInfo Colour { get; protected set; }

        public CmsPropertyInfo Size { get; protected set; }

        public CmsPropertyInfo Quantity { get; protected set; }

        public CmsPropertyInfo ReorderAmount { get; protected set; }

        public CmsPropertyInfo ImportReference { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo OrderItems { get; protected set; }
        

        public CmsCollectionInfo ProductVariation_CultureInfos { get; protected set; }
        

        public CmsCollectionInfo MediaItems { get; protected set; }
        

        public CmsCollectionInfo ShoppingCartItems { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.OrderItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.OrderItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector(item => item.LinkedProductVariation)));
		
		//InitCollectionBaseOneToMany
		this.ProductVariation_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.ProductVariation_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase>.GetPropertyBySelector(item => item.ProductVariation)));
		
		//InitCollectionBaseOneToMany
		this.MediaItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.MediaItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase>.GetPropertyBySelector(item => item.ProductVariation)));
		
		//InitCollectionBaseOneToMany
		this.ShoppingCartItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.ShoppingCartItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>.GetPropertyBySelector(item => item.LinkedProductVariation)));


			base.initBasicFields();
          
        }

    }
}
