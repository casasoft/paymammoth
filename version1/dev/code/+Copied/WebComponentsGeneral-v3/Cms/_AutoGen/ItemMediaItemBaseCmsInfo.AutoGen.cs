using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ItemMediaItemModule;
using BusinessLogic_v3.Cms.ItemMediaItemModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ItemMediaItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ItemMediaItemModule.ItemMediaItemBase>
    {
		public ItemMediaItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ItemMediaItemModule.ItemMediaItemBase dbItem)
            : base(BusinessLogic_v3.Cms.ItemMediaItemModule.ItemMediaItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ItemMediaItemBase DbItem
        {
            get
            {
                return (ItemMediaItemBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Item { get; private set; }

        public CmsPropertyInfo ImageFilename { get; private set; }

        public CmsPropertyInfo Caption { get; private set; }

        public CmsPropertyInfo ExtraValueChoice { get; private set; }

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Reference { get; private set; }

        public CmsPropertyInfo ImportReference { get; private set; }

        public CmsPropertyInfo ItemGroup { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Item = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemMediaItemModule.ItemMediaItemBase>.GetPropertyBySelector( item => item.Item),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImageFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemMediaItemModule.ItemMediaItemBase>.GetPropertyBySelector( item => item.ImageFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.Caption = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemMediaItemModule.ItemMediaItemBase>.GetPropertyBySelector( item => item.Caption),
        			isEditable: true,
        			isVisible: true
        			);

        this.ExtraValueChoice = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemMediaItemModule.ItemMediaItemBase>.GetPropertyBySelector( item => item.ExtraValueChoice),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemMediaItemModule.ItemMediaItemBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Reference = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemMediaItemModule.ItemMediaItemBase>.GetPropertyBySelector( item => item.Reference),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImportReference = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemMediaItemModule.ItemMediaItemBase>.GetPropertyBySelector( item => item.ImportReference),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemGroup = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemMediaItemModule.ItemMediaItemBase>.GetPropertyBySelector( item => item.ItemGroup),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
