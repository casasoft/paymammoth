using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ShippingRatesGroupModule;
using CS.WebComponentsGeneralV3.Cms.ShippingRatesGroupModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ShippingRatesGroupBaseCmsFactory_AutoGen : CmsFactoryBase<ShippingRatesGroupBaseCmsInfo, ShippingRatesGroupBase>
    {
       
       public new static ShippingRatesGroupBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ShippingRatesGroupBaseCmsFactory)CmsFactoryBase<ShippingRatesGroupBaseCmsInfo, ShippingRatesGroupBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ShippingRatesGroupBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ShippingRatesGroup.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ShippingRatesGroup";

            this.QueryStringParamID = "ShippingRatesGroupId";

            cmsInfo.TitlePlural = "Shipping Rates Groups";

            cmsInfo.TitleSingular =  "Shipping Rates Group";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ShippingRatesGroupBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ShippingRatesGroup/";
			UsedInProject = ShippingRatesGroupBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
