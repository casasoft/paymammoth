using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.NewsItemModule;
using BusinessLogic_v3.Cms.NewsItemModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class NewsItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.NewsItemModule.NewsItemBase>
    {
		public NewsItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.NewsItemModule.NewsItemBase dbItem)
            : base(BusinessLogic_v3.Cms.NewsItemModule.NewsItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new NewsItemBase DbItem
        {
            get
            {
                return (NewsItemBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Date { get; private set; }

        public CmsPropertyInfo ShortDesc { get; private set; }

        public CmsPropertyInfo Archived { get; private set; }

        public CmsPropertyInfo FullDesc { get; private set; }

        public CmsPropertyInfo FullDesc_Search { get; private set; }

        public CmsPropertyInfo ImageFilename { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItemModule.NewsItemBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Date = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItemModule.NewsItemBase>.GetPropertyBySelector( item => item.Date),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShortDesc = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItemModule.NewsItemBase>.GetPropertyBySelector( item => item.ShortDesc),
        			isEditable: true,
        			isVisible: true
        			);

        this.Archived = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItemModule.NewsItemBase>.GetPropertyBySelector( item => item.Archived),
        			isEditable: true,
        			isVisible: true
        			);

        this.FullDesc = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItemModule.NewsItemBase>.GetPropertyBySelector( item => item.FullDesc),
        			isEditable: true,
        			isVisible: true
        			);

        this.FullDesc_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItemModule.NewsItemBase>.GetPropertyBySelector( item => item.FullDesc_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.ImageFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsItemModule.NewsItemBase>.GetPropertyBySelector( item => item.ImageFilename),
        			isEditable: false,
        			isVisible: false
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
