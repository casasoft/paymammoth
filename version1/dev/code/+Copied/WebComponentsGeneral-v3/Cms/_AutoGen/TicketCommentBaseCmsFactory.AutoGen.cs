using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.TicketCommentModule;
using CS.WebComponentsGeneralV3.Cms.TicketCommentModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class TicketCommentBaseCmsFactory_AutoGen : CmsFactoryBase<TicketCommentBaseCmsInfo, TicketCommentBase>
    {
       
       public new static TicketCommentBaseCmsFactory Instance
	    {
	         get
	         {
                 return (TicketCommentBaseCmsFactory)CmsFactoryBase<TicketCommentBaseCmsInfo, TicketCommentBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = TicketCommentBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "TicketComment.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "TicketComment";

            this.QueryStringParamID = "TicketCommentId";

            cmsInfo.TitlePlural = "Ticket Comments";

            cmsInfo.TitleSingular =  "Ticket Comment";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public TicketCommentBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "TicketComment/";
			UsedInProject = TicketCommentBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
