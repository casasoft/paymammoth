using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.RoutingInfoModule;
using CS.WebComponentsGeneralV3.Cms.RoutingInfoModule;
using BusinessLogic_v3.Frontend.RoutingInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class RoutingInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBase>
    {
		public RoutingInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.RoutingInfoModule.RoutingInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new RoutingInfoBaseFrontend FrontendItem
        {
            get { return (RoutingInfoBaseFrontend)base.FrontendItem; }

        }
        public new RoutingInfoBase DbItem
        {
            get
            {
                return (RoutingInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo RouteName { get; protected set; }

        public CmsPropertyInfo PhysicalPath { get; protected set; }

        public CmsPropertyInfo VirtualPath { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
