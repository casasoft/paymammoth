using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ContentTextModule;
using CS.WebComponentsGeneralV3.Cms.ContentTextModule;
using BusinessLogic_v3.Frontend.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ContentTextBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>
    {
		public ContentTextBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ContentTextModule.ContentTextBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentTextBaseFrontend FrontendItem
        {
            get { return (ContentTextBaseFrontend)base.FrontendItem; }

        }
        public new ContentTextBase DbItem
        {
            get
            {
                return (ContentTextBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Name { get; protected set; }

        public CmsPropertyInfo Name_Search { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo Parent { get; protected set; }

        public CmsPropertyInfo AllowAddSubItems_AccessRequired { get; protected set; }

        public CmsPropertyInfo VisibleInCMS_AccessRequired { get; protected set; }

        public CmsPropertyInfo Content { get; protected set; }

        public CmsPropertyInfo ContentType { get; protected set; }

        public CmsPropertyInfo UsedInProject { get; protected set; }

        public CmsPropertyInfo ImageAlternateText { get; protected set; }

        public CmsPropertyInfo CustomContentTags { get; protected set; }

        public CmsPropertyInfo ProcessContentUsingNVelocity { get; protected set; }

        public CmsPropertyInfo _Computed_IsHtml { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ChildContentTexts { get; protected set; }
        

        public CmsCollectionInfo ContentText_CultureInfos { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ChildContentTexts = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector(item => item.ChildContentTexts),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector(item => item.Parent)));
		
		//InitCollectionBaseOneToMany
		this.ContentText_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector(item => item.ContentText_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase>.GetPropertyBySelector(item => item.ContentText)));


			base.initBasicFields();
          
        }

    }
}
