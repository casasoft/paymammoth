using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.LocationModule;
using CS.WebComponentsGeneralV3.Cms.LocationModule;
using BusinessLogic_v3.Frontend.LocationModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class LocationBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.LocationModule.LocationBase>
    {
		public LocationBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.LocationModule.LocationBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.LocationModule.LocationBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new LocationBaseFrontend FrontendItem
        {
            get { return (LocationBaseFrontend)base.FrontendItem; }

        }
        public new LocationBase DbItem
        {
            get
            {
                return (LocationBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ReferenceID { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo LocationType { get; protected set; }

        public CmsPropertyInfo ParentLocation { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ChildLocations { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ChildLocations = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.LocationModule.LocationBase>.GetPropertyBySelector(item => item.ChildLocations),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.LocationModule.LocationBase>.GetPropertyBySelector(item => item.ParentLocation)));


			base.initBasicFields();
          
        }

    }
}
