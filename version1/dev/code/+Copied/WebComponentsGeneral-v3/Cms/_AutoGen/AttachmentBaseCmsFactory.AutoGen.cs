using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.AttachmentModule;
using CS.WebComponentsGeneralV3.Cms.AttachmentModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class AttachmentBaseCmsFactory_AutoGen : CmsFactoryBase<AttachmentBaseCmsInfo, AttachmentBase>
    {
       
       public new static AttachmentBaseCmsFactory Instance
	    {
	         get
	         {
                 return (AttachmentBaseCmsFactory)CmsFactoryBase<AttachmentBaseCmsInfo, AttachmentBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = AttachmentBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Attachment.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Attachment";

            this.QueryStringParamID = "AttachmentId";

            cmsInfo.TitlePlural = "Attachments";

            cmsInfo.TitleSingular =  "Attachment";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public AttachmentBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Attachment/";
			UsedInProject = AttachmentBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
