using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ArticleCommentModule;
using CS.WebComponentsGeneralV3.Cms.ArticleCommentModule;
using BusinessLogic_v3.Frontend.ArticleCommentModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ArticleCommentBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase>
    {
		public ArticleCommentBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ArticleCommentModule.ArticleCommentBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ArticleCommentBaseFrontend FrontendItem
        {
            get { return (ArticleCommentBaseFrontend)base.FrontendItem; }

        }
        public new ArticleCommentBase DbItem
        {
            get
            {
                return (ArticleCommentBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Email { get; protected set; }

        public CmsPropertyInfo Comment { get; protected set; }

        public CmsPropertyInfo IPAddress { get; protected set; }

        public CmsPropertyInfo PostedOn { get; protected set; }

        public CmsPropertyInfo Article { get; protected set; }

        public CmsPropertyInfo Author { get; protected set; }

        public CmsPropertyInfo Company { get; protected set; }

        public CmsPropertyInfo IsApproved { get; protected set; }

        public CmsPropertyInfo ReplyTo { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo Replies { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.Replies = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase>.GetPropertyBySelector(item => item.Replies),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase>.GetPropertyBySelector(item => item.ReplyTo)));


			base.initBasicFields();
          
        }

    }
}
