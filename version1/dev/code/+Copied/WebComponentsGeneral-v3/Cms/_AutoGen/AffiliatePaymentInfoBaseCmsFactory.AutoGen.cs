using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.AffiliatePaymentInfoModule;
using CS.WebComponentsGeneralV3.Cms.AffiliatePaymentInfoModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class AffiliatePaymentInfoBaseCmsFactory_AutoGen : CmsFactoryBase<AffiliatePaymentInfoBaseCmsInfo, AffiliatePaymentInfoBase>
    {
       
       public new static AffiliatePaymentInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (AffiliatePaymentInfoBaseCmsFactory)CmsFactoryBase<AffiliatePaymentInfoBaseCmsInfo, AffiliatePaymentInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = AffiliatePaymentInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "AffiliatePaymentInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "AffiliatePaymentInfo";

            this.QueryStringParamID = "AffiliatePaymentInfoId";

            cmsInfo.TitlePlural = "Affiliate Payment Infos";

            cmsInfo.TitleSingular =  "Affiliate Payment Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public AffiliatePaymentInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "AffiliatePaymentInfo/";
			UsedInProject = AffiliatePaymentInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
