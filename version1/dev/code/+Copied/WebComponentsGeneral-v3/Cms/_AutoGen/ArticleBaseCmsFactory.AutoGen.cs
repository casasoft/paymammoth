using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ArticleModule;
using CS.WebComponentsGeneralV3.Cms.ArticleModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ArticleBaseCmsFactory_AutoGen : CmsFactoryBase<ArticleBaseCmsInfo, ArticleBase>
    {
       
       public new static ArticleBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ArticleBaseCmsFactory)CmsFactoryBase<ArticleBaseCmsInfo, ArticleBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ArticleBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Article.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Article";

            this.QueryStringParamID = "ArticleId";

            cmsInfo.TitlePlural = "Articles";

            cmsInfo.TitleSingular =  "Article";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ArticleBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Article/";
			UsedInProject = ArticleBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
