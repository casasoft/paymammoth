using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ItemGroup_CultureInfoModule;
using BusinessLogic_v3.Cms.ItemGroup_CultureInfoModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ItemGroup_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ItemGroup_CultureInfoModule.ItemGroup_CultureInfoBase>
    {
		public ItemGroup_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ItemGroup_CultureInfoModule.ItemGroup_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.ItemGroup_CultureInfoModule.ItemGroup_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ItemGroup_CultureInfoBase DbItem
        {
            get
            {
                return (ItemGroup_CultureInfoBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; private set; }

        public CmsPropertyInfo ItemGroup { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo Title { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.CultureInfo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroup_CultureInfoModule.ItemGroup_CultureInfoBase>.GetPropertyBySelector( item => item.CultureInfo),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemGroup = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroup_CultureInfoModule.ItemGroup_CultureInfoBase>.GetPropertyBySelector( item => item.ItemGroup),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroup_CultureInfoModule.ItemGroup_CultureInfoBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroup_CultureInfoModule.ItemGroup_CultureInfoBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
