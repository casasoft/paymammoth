using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.CultureInfoModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<CultureInfoBaseCmsInfo, CultureInfoBase>
    {
       
       public new static CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CultureInfoBaseCmsFactory)CmsFactoryBase<CultureInfoBaseCmsInfo, CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	qParams.FillDefaultsForCms();
        	
            var q = CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CultureInfo";

            this.QueryStringParamID = "CultureInfoId";

            cmsInfo.TitlePlural = "Culture Infos";

            cmsInfo.TitleSingular =  "Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "CultureInfo/";
			UsedInProject = CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
