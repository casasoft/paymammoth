using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule;
using CS.WebComponentsGeneralV3.Cms.ImageSpecificSizeInfoModule;
using BusinessLogic_v3.Frontend.ImageSpecificSizeInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ImageSpecificSizeInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase>
    {
		public ImageSpecificSizeInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ImageSpecificSizeInfoBaseFrontend FrontendItem
        {
            get { return (ImageSpecificSizeInfoBaseFrontend)base.FrontendItem; }

        }
        public new ImageSpecificSizeInfoBase DbItem
        {
            get
            {
                return (ImageSpecificSizeInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ImageSizingInfo { get; protected set; }

        public CmsPropertyInfo Width { get; protected set; }

        public CmsPropertyInfo Height { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo CropIdentifier { get; protected set; }

        public CmsPropertyInfo ImageFormat { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
