using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;
using CS.WebComponentsGeneralV3.Cms.Category_ParentChildLinkModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class Category_ParentChildLinkBaseCmsFactory_AutoGen : CmsFactoryBase<Category_ParentChildLinkBaseCmsInfo, Category_ParentChildLinkBase>
    {
       
       public new static Category_ParentChildLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Category_ParentChildLinkBaseCmsFactory)CmsFactoryBase<Category_ParentChildLinkBaseCmsInfo, Category_ParentChildLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = Category_ParentChildLinkBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Category_ParentChildLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Category_ParentChildLink";

            this.QueryStringParamID = "Category_ParentChildLinkId";

            cmsInfo.TitlePlural = "Category_ Parent Child Links";

            cmsInfo.TitleSingular =  "Category_ Parent Child Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Category_ParentChildLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Category_ParentChildLink/";
			UsedInProject = Category_ParentChildLinkBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
