using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductVersionModule;
using CS.WebComponentsGeneralV3.Cms.ProductVersionModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductVersionBaseCmsFactory_AutoGen : CmsFactoryBase<ProductVersionBaseCmsInfo, ProductVersionBase>
    {
       
       public new static ProductVersionBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductVersionBaseCmsFactory)CmsFactoryBase<ProductVersionBaseCmsInfo, ProductVersionBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ProductVersionBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductVersion.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductVersion";

            this.QueryStringParamID = "ProductVersionId";

            cmsInfo.TitlePlural = "Product Versions";

            cmsInfo.TitleSingular =  "Product Version";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductVersionBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductVersion/";
			UsedInProject = ProductVersionBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
