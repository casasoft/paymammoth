using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductVersionMediaItemModule;
using CS.WebComponentsGeneralV3.Cms.ProductVersionMediaItemModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductVersionMediaItemBaseCmsFactory_AutoGen : CmsFactoryBase<ProductVersionMediaItemBaseCmsInfo, ProductVersionMediaItemBase>
    {
       
       public new static ProductVersionMediaItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductVersionMediaItemBaseCmsFactory)CmsFactoryBase<ProductVersionMediaItemBaseCmsInfo, ProductVersionMediaItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ProductVersionMediaItemBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductVersionMediaItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductVersionMediaItem";

            this.QueryStringParamID = "ProductVersionMediaItemId";

            cmsInfo.TitlePlural = "Product Version Media Items";

            cmsInfo.TitleSingular =  "Product Version Media Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductVersionMediaItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductVersionMediaItem/";
			UsedInProject = ProductVersionMediaItemBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
