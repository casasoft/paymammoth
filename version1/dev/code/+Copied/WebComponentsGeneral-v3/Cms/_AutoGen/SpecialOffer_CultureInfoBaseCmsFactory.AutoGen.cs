using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.SpecialOffer_CultureInfoModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class SpecialOffer_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<SpecialOffer_CultureInfoBaseCmsInfo, SpecialOffer_CultureInfoBase>
    {
       
       public new static SpecialOffer_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (SpecialOffer_CultureInfoBaseCmsFactory)CmsFactoryBase<SpecialOffer_CultureInfoBaseCmsInfo, SpecialOffer_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = SpecialOffer_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "SpecialOffer_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "SpecialOffer_CultureInfo";

            this.QueryStringParamID = "SpecialOffer_CultureInfoId";

            cmsInfo.TitlePlural = "Special Offer_ Culture Infos";

            cmsInfo.TitleSingular =  "Special Offer_ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public SpecialOffer_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "SpecialOffer_CultureInfo/";
			UsedInProject = SpecialOffer_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
