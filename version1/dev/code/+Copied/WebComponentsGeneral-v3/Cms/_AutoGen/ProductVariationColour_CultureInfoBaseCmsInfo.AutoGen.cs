using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationColour_CultureInfoModule;
using BusinessLogic_v3.Frontend.ProductVariationColour_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ProductVariationColour_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase>
    {
		public ProductVariationColour_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductVariationColour_CultureInfoBaseFrontend FrontendItem
        {
            get { return (ProductVariationColour_CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new ProductVariationColour_CultureInfoBase DbItem
        {
            get
            {
                return (ProductVariationColour_CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; protected set; }

        public CmsPropertyInfo ProductVariationColour { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
