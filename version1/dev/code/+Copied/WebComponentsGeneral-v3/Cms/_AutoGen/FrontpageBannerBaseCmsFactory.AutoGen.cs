using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.FrontpageBannerModule;
using BusinessLogic_v3.Cms.FrontpageBannerModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class FrontpageBannerBaseCmsFactory_AutoGen : CmsFactoryBase<FrontpageBannerBaseCmsInfo, FrontpageBannerBase>
    {
       
       public new static FrontpageBannerBaseCmsFactory Instance
	    {
	         get
	         {
                 return (FrontpageBannerBaseCmsFactory)CmsFactoryBase<FrontpageBannerBaseCmsInfo, FrontpageBannerBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = FrontpageBannerBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "FrontpageBanner.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "FrontpageBanner";

            this.QueryStringParamID = "FrontpageBannerId";

            cmsInfo.TitlePlural = "Frontpage Banners";

            cmsInfo.TitleSingular =  "Frontpage Banner";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public FrontpageBannerBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "FrontpageBanner/";


        }
       
    }

}
