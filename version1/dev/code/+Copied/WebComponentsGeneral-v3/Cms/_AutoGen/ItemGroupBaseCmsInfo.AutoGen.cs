using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ItemGroupModule;
using BusinessLogic_v3.Cms.ItemGroupModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ItemGroupBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>
    {
		public ItemGroupBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase dbItem)
            : base(BusinessLogic_v3.Cms.ItemGroupModule.ItemGroupBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ItemGroupBase DbItem
        {
            get
            {
                return (ItemGroupBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo ReferenceCode { get; private set; }

        public CmsPropertyInfo SupplierReferenceCode { get; private set; }

        public CmsPropertyInfo Activated { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo PriceRetailIncTaxPerUnit { get; private set; }

        public CmsPropertyInfo PriceWholesale { get; private set; }

        public CmsPropertyInfo Brand { get; private set; }

        public CmsPropertyInfo ExtraInfo { get; private set; }

        public CmsPropertyInfo PriceRetailBefore { get; private set; }

        public CmsPropertyInfo TaxRatePercentage { get; private set; }

        public CmsPropertyInfo PDFFilename { get; private set; }

        public CmsPropertyInfo IsSpecialOffer { get; private set; }

        public CmsPropertyInfo ShowPrice { get; private set; }

        public CmsPropertyInfo WeightInKg { get; private set; }

        public CmsPropertyInfo WarrantyInMonths { get; private set; }

        public CmsPropertyInfo DateAdded { get; private set; }

        public CmsPropertyInfo IsFeatured { get; private set; }

        public CmsPropertyInfo ImportReference { get; private set; }

        public CmsPropertyInfo WarrantyText { get; private set; }

        public CmsPropertyInfo MetaKeywords { get; private set; }

        public CmsPropertyInfo CategoryFeatureValues_ForSearch { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.ReferenceCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.ReferenceCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.SupplierReferenceCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.SupplierReferenceCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.Activated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.Activated),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.PriceRetailIncTaxPerUnit = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.PriceRetailIncTaxPerUnit),
        			isEditable: true,
        			isVisible: true
        			);

        this.PriceWholesale = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.PriceWholesale),
        			isEditable: true,
        			isVisible: true
        			);

        this.Brand = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.Brand),
        			isEditable: true,
        			isVisible: true
        			);

        this.ExtraInfo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.ExtraInfo),
        			isEditable: true,
        			isVisible: true
        			);

        this.PriceRetailBefore = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.PriceRetailBefore),
        			isEditable: true,
        			isVisible: true
        			);

        this.TaxRatePercentage = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.TaxRatePercentage),
        			isEditable: true,
        			isVisible: true
        			);

        this.PDFFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.PDFFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.IsSpecialOffer = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.IsSpecialOffer),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShowPrice = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.ShowPrice),
        			isEditable: true,
        			isVisible: true
        			);

        this.WeightInKg = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.WeightInKg),
        			isEditable: true,
        			isVisible: true
        			);

        this.WarrantyInMonths = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.WarrantyInMonths),
        			isEditable: true,
        			isVisible: true
        			);

        this.DateAdded = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.DateAdded),
        			isEditable: true,
        			isVisible: true
        			);

        this.IsFeatured = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.IsFeatured),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImportReference = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.ImportReference),
        			isEditable: true,
        			isVisible: true
        			);

        this.WarrantyText = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.WarrantyText),
        			isEditable: true,
        			isVisible: true
        			);

        this.MetaKeywords = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.MetaKeywords),
        			isEditable: true,
        			isVisible: true
        			);

        this.CategoryFeatureValues_ForSearch = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupModule.ItemGroupBase>.GetPropertyBySelector( item => item.CategoryFeatureValues_ForSearch),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
