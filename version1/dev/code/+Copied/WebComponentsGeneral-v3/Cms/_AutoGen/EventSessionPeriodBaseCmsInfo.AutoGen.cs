using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.EventSessionPeriodModule;
using CS.WebComponentsGeneralV3.Cms.EventSessionPeriodModule;
using BusinessLogic_v3.Frontend.EventSessionPeriodModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class EventSessionPeriodBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase>
    {
		public EventSessionPeriodBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.EventSessionPeriodModule.EventSessionPeriodBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventSessionPeriodBaseFrontend FrontendItem
        {
            get { return (EventSessionPeriodBaseFrontend)base.FrontendItem; }

        }
        public new EventSessionPeriodBase DbItem
        {
            get
            {
                return (EventSessionPeriodBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo EventSession { get; protected set; }

        public CmsPropertyInfo StartDate { get; protected set; }

        public CmsPropertyInfo EndDate { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo EventSessionPeriodDays { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.EventSessionPeriodDays = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase>.GetPropertyBySelector(item => item.EventSessionPeriodDays),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionPeriodDayModule.EventSessionPeriodDayBase>.GetPropertyBySelector(item => item.EventSessionPeriod)));


			base.initBasicFields();
          
        }

    }
}
