using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentPage_CultureInfoModule;
using BusinessLogic_v3.Cms.ContentPage_CultureInfoModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentPage_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase>
    {
		public ContentPage_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentPage_CultureInfoModule.ContentPage_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentPage_CultureInfoBase DbItem
        {
            get
            {
                return (ContentPage_CultureInfoBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; private set; }

        public CmsPropertyInfo ContentPage { get; private set; }

        public CmsPropertyInfo HtmlText { get; private set; }

        public CmsPropertyInfo HtmlText_Search { get; private set; }

        public CmsPropertyInfo MetaKeywords { get; private set; }

        public CmsPropertyInfo MetaKeywords_Search { get; private set; }

        public CmsPropertyInfo MetaDescription { get; private set; }

        public CmsPropertyInfo MetaDescription_Search { get; private set; }

        public CmsPropertyInfo PageTitle { get; private set; }

        public CmsPropertyInfo PageTitle_Search { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.CultureInfo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase>.GetPropertyBySelector( item => item.CultureInfo),
        			isEditable: true,
        			isVisible: true
        			);

        this.ContentPage = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase>.GetPropertyBySelector( item => item.ContentPage),
        			isEditable: true,
        			isVisible: true
        			);

        this.HtmlText = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase>.GetPropertyBySelector( item => item.HtmlText),
        			isEditable: true,
        			isVisible: true
        			);

        this.HtmlText_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase>.GetPropertyBySelector( item => item.HtmlText_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.MetaKeywords = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase>.GetPropertyBySelector( item => item.MetaKeywords),
        			isEditable: true,
        			isVisible: true
        			);

        this.MetaKeywords_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase>.GetPropertyBySelector( item => item.MetaKeywords_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.MetaDescription = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase>.GetPropertyBySelector( item => item.MetaDescription),
        			isEditable: true,
        			isVisible: true
        			);

        this.MetaDescription_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase>.GetPropertyBySelector( item => item.MetaDescription_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.PageTitle = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase>.GetPropertyBySelector( item => item.PageTitle),
        			isEditable: true,
        			isVisible: true
        			);

        this.PageTitle_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_CultureInfoModule.ContentPage_CultureInfoBase>.GetPropertyBySelector( item => item.PageTitle_Search),
        			isEditable: false,
        			isVisible: false
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
