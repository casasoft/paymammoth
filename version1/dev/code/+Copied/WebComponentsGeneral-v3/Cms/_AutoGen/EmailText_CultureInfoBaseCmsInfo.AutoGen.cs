using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.EmailText_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.EmailText_CultureInfoModule;
using BusinessLogic_v3.Frontend.EmailText_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class EmailText_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase>
    {
		public EmailText_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.EmailText_CultureInfoModule.EmailText_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EmailText_CultureInfoBaseFrontend FrontendItem
        {
            get { return (EmailText_CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new EmailText_CultureInfoBase DbItem
        {
            get
            {
                return (EmailText_CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; protected set; }

        public CmsPropertyInfo EmailText { get; protected set; }

        public CmsPropertyInfo Subject { get; protected set; }

        public CmsPropertyInfo Subject_Search { get; protected set; }

        public CmsPropertyInfo Body { get; protected set; }

        public CmsPropertyInfo Body_Search { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
