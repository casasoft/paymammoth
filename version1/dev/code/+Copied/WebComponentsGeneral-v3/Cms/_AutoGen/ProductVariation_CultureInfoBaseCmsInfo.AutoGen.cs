using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariation_CultureInfoModule;
using BusinessLogic_v3.Frontend.ProductVariation_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ProductVariation_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase>
    {
		public ProductVariation_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductVariation_CultureInfoBaseFrontend FrontendItem
        {
            get { return (ProductVariation_CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new ProductVariation_CultureInfoBase DbItem
        {
            get
            {
                return (ProductVariation_CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; protected set; }

        public CmsPropertyInfo ProductVariation { get; protected set; }

        public CmsPropertyInfo Size { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
