using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.PaymentTransactionModule;
using CS.WebComponentsGeneralV3.Cms.PaymentTransactionModule;
using BusinessLogic_v3.Frontend.PaymentTransactionModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class PaymentTransactionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.PaymentTransactionModule.PaymentTransactionBase>
    {
		public PaymentTransactionBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.PaymentTransactionModule.PaymentTransactionBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.PaymentTransactionModule.PaymentTransactionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new PaymentTransactionBaseFrontend FrontendItem
        {
            get { return (PaymentTransactionBaseFrontend)base.FrontendItem; }

        }
        public new PaymentTransactionBase DbItem
        {
            get
            {
                return (PaymentTransactionBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Date { get; protected set; }

        public CmsPropertyInfo Order { get; protected set; }

        public CmsPropertyInfo Details { get; protected set; }

        public CmsPropertyInfo Amount { get; protected set; }

        public CmsPropertyInfo Success { get; protected set; }

        public CmsPropertyInfo AuthCode { get; protected set; }

        public CmsPropertyInfo Remarks { get; protected set; }

        public CmsPropertyInfo ItemID { get; protected set; }

        public CmsPropertyInfo ItemType { get; protected set; }

        public CmsPropertyInfo LinkedItem { get; protected set; }

        public CmsPropertyInfo Member { get; protected set; }

        public CmsPropertyInfo ExternalReference1 { get; protected set; }

        public CmsPropertyInfo ExternalReference2 { get; protected set; }

        public CmsPropertyInfo LastUpdated { get; protected set; }

        public CmsPropertyInfo ErrorMsg { get; protected set; }

        public CmsPropertyInfo InternalReference { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
