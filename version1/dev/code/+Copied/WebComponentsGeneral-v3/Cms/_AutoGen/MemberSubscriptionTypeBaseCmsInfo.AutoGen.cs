using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.MemberSubscriptionTypeModule;
using CS.WebComponentsGeneralV3.Cms.MemberSubscriptionTypeModule;
using BusinessLogic_v3.Frontend.MemberSubscriptionTypeModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class MemberSubscriptionTypeBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase>
    {
		public MemberSubscriptionTypeBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.MemberSubscriptionTypeModule.MemberSubscriptionTypeBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberSubscriptionTypeBaseFrontend FrontendItem
        {
            get { return (MemberSubscriptionTypeBaseFrontend)base.FrontendItem; }

        }
        public new MemberSubscriptionTypeBase DbItem
        {
            get
            {
                return (MemberSubscriptionTypeBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo MemberSubscriptionType { get; protected set; }

        public CmsPropertyInfo RenewableBalanceValue { get; protected set; }

        public CmsPropertyInfo RenewableBalanceRefreshPeriodInDays { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo MemberSubscriptionTypePrices { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.MemberSubscriptionTypePrices = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase>.GetPropertyBySelector(item => item.MemberSubscriptionTypePrices),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.GetPropertyBySelector(item => item.MemberSubscriptionType)));


			base.initBasicFields();
          
        }

    }
}
