using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule;
using CS.WebComponentsGeneralV3.Cms.OrderApplicableSpecialOfferLinkModule;
using BusinessLogic_v3.Frontend.OrderApplicableSpecialOfferLinkModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class OrderApplicableSpecialOfferLinkBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBase>
    {
		public OrderApplicableSpecialOfferLinkBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new OrderApplicableSpecialOfferLinkBaseFrontend FrontendItem
        {
            get { return (OrderApplicableSpecialOfferLinkBaseFrontend)base.FrontendItem; }

        }
        public new OrderApplicableSpecialOfferLinkBase DbItem
        {
            get
            {
                return (OrderApplicableSpecialOfferLinkBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Order { get; protected set; }

        public CmsPropertyInfo SpecialOfferLink { get; protected set; }

        public CmsPropertyInfo SpecialOfferVoucherCodeLink { get; protected set; }

        public CmsPropertyInfo VoucherCode { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
