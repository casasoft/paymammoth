using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using CS.WebComponentsGeneralV3.Cms.SpecialOfferModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class SpecialOfferBaseCmsFactory_AutoGen : CmsFactoryBase<SpecialOfferBaseCmsInfo, SpecialOfferBase>
    {
       
       public new static SpecialOfferBaseCmsFactory Instance
	    {
	         get
	         {
                 return (SpecialOfferBaseCmsFactory)CmsFactoryBase<SpecialOfferBaseCmsInfo, SpecialOfferBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = SpecialOfferBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "SpecialOffer.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "SpecialOffer";

            this.QueryStringParamID = "SpecialOfferId";

            cmsInfo.TitlePlural = "Special Offers";

            cmsInfo.TitleSingular =  "Special Offer";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public SpecialOfferBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "SpecialOffer/";
			UsedInProject = SpecialOfferBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
