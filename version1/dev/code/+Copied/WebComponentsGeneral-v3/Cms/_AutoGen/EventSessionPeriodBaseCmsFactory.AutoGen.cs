using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.EventSessionPeriodModule;
using CS.WebComponentsGeneralV3.Cms.EventSessionPeriodModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class EventSessionPeriodBaseCmsFactory_AutoGen : CmsFactoryBase<EventSessionPeriodBaseCmsInfo, EventSessionPeriodBase>
    {
       
       public new static EventSessionPeriodBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventSessionPeriodBaseCmsFactory)CmsFactoryBase<EventSessionPeriodBaseCmsInfo, EventSessionPeriodBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = EventSessionPeriodBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EventSessionPeriod.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EventSessionPeriod";

            this.QueryStringParamID = "EventSessionPeriodId";

            cmsInfo.TitlePlural = "Event Session Periods";

            cmsInfo.TitleSingular =  "Event Session Period";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventSessionPeriodBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "EventSessionPeriod/";
			UsedInProject = EventSessionPeriodBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
