using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.EventMediaItemModule;
using CS.WebComponentsGeneralV3.Cms.EventMediaItemModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class EventMediaItemBaseCmsFactory_AutoGen : CmsFactoryBase<EventMediaItemBaseCmsInfo, EventMediaItemBase>
    {
       
       public new static EventMediaItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventMediaItemBaseCmsFactory)CmsFactoryBase<EventMediaItemBaseCmsInfo, EventMediaItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = EventMediaItemBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EventMediaItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EventMediaItem";

            this.QueryStringParamID = "EventMediaItemId";

            cmsInfo.TitlePlural = "Event Media Items";

            cmsInfo.TitleSingular =  "Event Media Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventMediaItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "EventMediaItem/";
			UsedInProject = EventMediaItemBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
