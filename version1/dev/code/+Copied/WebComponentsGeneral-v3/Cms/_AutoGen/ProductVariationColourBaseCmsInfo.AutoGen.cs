using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ProductVariationColourModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationColourModule;
using BusinessLogic_v3.Frontend.ProductVariationColourModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ProductVariationColourBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase>
    {
		public ProductVariationColourBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ProductVariationColourModule.ProductVariationColourBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductVariationColourBaseFrontend FrontendItem
        {
            get { return (ProductVariationColourBaseFrontend)base.FrontendItem; }

        }
        public new ProductVariationColourBase DbItem
        {
            get
            {
                return (ProductVariationColourBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo HexColor { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ProductVariations { get; protected set; }
        

        public CmsCollectionInfo ProductVariationColour_CultureInfos { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ProductVariations = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase>.GetPropertyBySelector(item => item.ProductVariations),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.Colour)));
		
		//InitCollectionBaseOneToMany
		this.ProductVariationColour_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase>.GetPropertyBySelector(item => item.ProductVariationColour_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase>.GetPropertyBySelector(item => item.ProductVariationColour)));


			base.initBasicFields();
          
        }

    }
}
