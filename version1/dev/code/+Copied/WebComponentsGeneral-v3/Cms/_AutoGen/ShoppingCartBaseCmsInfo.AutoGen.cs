using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ShoppingCartModule;
using CS.WebComponentsGeneralV3.Cms.ShoppingCartModule;
using BusinessLogic_v3.Frontend.ShoppingCartModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ShoppingCartBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase>
    {
		public ShoppingCartBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ShoppingCartModule.ShoppingCartBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ShoppingCartBaseFrontend FrontendItem
        {
            get { return (ShoppingCartBaseFrontend)base.FrontendItem; }

        }
        public new ShoppingCartBase DbItem
        {
            get
            {
                return (ShoppingCartBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateCreated { get; protected set; }

        public CmsPropertyInfo Member { get; protected set; }

        public CmsPropertyInfo Cancelled { get; protected set; }

        public CmsPropertyInfo CancelledOn { get; protected set; }

        public CmsPropertyInfo LastUpdatedOn { get; protected set; }

        public CmsPropertyInfo Currency { get; protected set; }

        public CmsPropertyInfo SpecialOfferVoucherCode { get; protected set; }

        public CmsPropertyInfo GUID { get; protected set; }

        public CmsPropertyInfo IsTemporaryCart { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ShoppingCartItems { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ShoppingCartItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase>.GetPropertyBySelector(item => item.ShoppingCartItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>.GetPropertyBySelector(item => item.ShoppingCart)));


			base.initBasicFields();
          
        }

    }
}
