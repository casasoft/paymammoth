using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.Brand_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Brand_CultureInfoModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class Brand_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<Brand_CultureInfoBaseCmsInfo, Brand_CultureInfoBase>
    {
       
       public new static Brand_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Brand_CultureInfoBaseCmsFactory)CmsFactoryBase<Brand_CultureInfoBaseCmsInfo, Brand_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = Brand_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Brand_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Brand_CultureInfo";

            this.QueryStringParamID = "Brand_CultureInfoId";

            cmsInfo.TitlePlural = "Brand_ Culture Infos";

            cmsInfo.TitleSingular =  "Brand_ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Brand_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Brand_CultureInfo/";
			UsedInProject = Brand_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
