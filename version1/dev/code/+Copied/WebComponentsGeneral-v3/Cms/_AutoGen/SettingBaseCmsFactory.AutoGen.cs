using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.SettingModule;
using CS.WebComponentsGeneralV3.Cms.SettingModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class SettingBaseCmsFactory_AutoGen : CmsFactoryBase<SettingBaseCmsInfo, SettingBase>
    {
       
       public new static SettingBaseCmsFactory Instance
	    {
	         get
	         {
                 return (SettingBaseCmsFactory)CmsFactoryBase<SettingBaseCmsInfo, SettingBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = SettingBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Setting.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Setting";

            this.QueryStringParamID = "SettingId";

            cmsInfo.TitlePlural = "Settings";

            cmsInfo.TitleSingular =  "Setting";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public SettingBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Setting/";
			UsedInProject = SettingBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
