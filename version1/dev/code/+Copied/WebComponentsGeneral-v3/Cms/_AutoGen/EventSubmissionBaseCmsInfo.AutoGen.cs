using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.EventSubmissionModule;
using CS.WebComponentsGeneralV3.Cms.EventSubmissionModule;
using BusinessLogic_v3.Frontend.EventSubmissionModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class EventSubmissionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase>
    {
		public EventSubmissionBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.EventSubmissionModule.EventSubmissionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventSubmissionBaseFrontend FrontendItem
        {
            get { return (EventSubmissionBaseFrontend)base.FrontendItem; }

        }
        public new EventSubmissionBase DbItem
        {
            get
            {
                return (EventSubmissionBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo EventName { get; protected set; }

        public CmsPropertyInfo UserFullName { get; protected set; }

        public CmsPropertyInfo UserEmail { get; protected set; }

        public CmsPropertyInfo EventStartDate { get; protected set; }

        public CmsPropertyInfo EventEndDate { get; protected set; }

        public CmsPropertyInfo UserIpAddress { get; protected set; }

        public CmsPropertyInfo Timestamp { get; protected set; }

        public CmsPropertyInfo Category { get; protected set; }

        public CmsPropertyInfo IsConvertedToRealEvent { get; protected set; }

        public CmsPropertyInfo Approved { get; protected set; }

        public CmsPropertyInfo ApprovedOn { get; protected set; }

        public CmsPropertyInfo ApprovedBy { get; protected set; }

        public CmsPropertyInfo EventDescription { get; protected set; }

        public CmsPropertyInfo EventSummary { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo EventBasics { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.EventBasics = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase>.GetPropertyBySelector(item => item.EventBasics),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase>.GetPropertyBySelector(item => item.EventSubmission)));


			base.initBasicFields();
          
        }

    }
}
