using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.EventBasicModule;
using CS.WebComponentsGeneralV3.Cms.EventBasicModule;
using BusinessLogic_v3.Frontend.EventBasicModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class EventBasicBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase>
    {
		public EventBasicBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.EventBasicModule.EventBasicBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventBasicBaseFrontend FrontendItem
        {
            get { return (EventBasicBaseFrontend)base.FrontendItem; }

        }
        public new EventBasicBase DbItem
        {
            get
            {
                return (EventBasicBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo DescriptionHtml { get; protected set; }

        public CmsPropertyInfo Summary { get; protected set; }

        public CmsPropertyInfo StartDate { get; protected set; }

        public CmsPropertyInfo EndDate { get; protected set; }

        public CmsPropertyInfo Time { get; protected set; }

        public CmsPropertyInfo Location { get; protected set; }

        public CmsPropertyInfo Category { get; protected set; }

        public CmsPropertyInfo EventSubmission { get; protected set; }

        public CmsPropertyInfo ImageFilename { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo EventMediaItems { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.EventMediaItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase>.GetPropertyBySelector(item => item.EventMediaItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>.GetPropertyBySelector(item => item.EventBasic)));


			base.initBasicFields();
          
        }

    }
}
