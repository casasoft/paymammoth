using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using CS.WebComponentsGeneralV3.Cms.CultureDetailsModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class CultureDetailsBaseCmsFactory_AutoGen : CmsFactoryBase<CultureDetailsBaseCmsInfo, CultureDetailsBase>
    {
       
       public new static CultureDetailsBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CultureDetailsBaseCmsFactory)CmsFactoryBase<CultureDetailsBaseCmsInfo, CultureDetailsBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = CultureDetailsBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CultureDetails.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CultureDetails";

            this.QueryStringParamID = "CultureDetailsId";

            cmsInfo.TitlePlural = "Culture Details";

            cmsInfo.TitleSingular =  "Culture Details";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CultureDetailsBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "CultureDetails/";
			UsedInProject = CultureDetailsBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
