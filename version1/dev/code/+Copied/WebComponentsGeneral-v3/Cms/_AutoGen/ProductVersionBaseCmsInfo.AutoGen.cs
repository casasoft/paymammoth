using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ProductVersionModule;
using CS.WebComponentsGeneralV3.Cms.ProductVersionModule;
using BusinessLogic_v3.Frontend.ProductVersionModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ProductVersionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase>
    {
		public ProductVersionBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ProductVersionModule.ProductVersionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductVersionBaseFrontend FrontendItem
        {
            get { return (ProductVersionBaseFrontend)base.FrontendItem; }

        }
        public new ProductVersionBase DbItem
        {
            get
            {
                return (ProductVersionBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ReleaseDate { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo DownloadLinkFilename { get; protected set; }

        public CmsPropertyInfo TrialLinkFilename { get; protected set; }

        public CmsPropertyInfo Product { get; protected set; }

        public CmsPropertyInfo ReleaseDetails { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ProductVersionMediaItems { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ProductVersionMediaItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase>.GetPropertyBySelector(item => item.ProductVersionMediaItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVersionMediaItemModule.ProductVersionMediaItemBase>.GetPropertyBySelector(item => item.ProductVersion)));


			base.initBasicFields();
          
        }

    }
}
