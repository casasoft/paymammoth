using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.EventSessionPeriodDayModule;
using CS.WebComponentsGeneralV3.Cms.EventSessionPeriodDayModule;
using BusinessLogic_v3.Frontend.EventSessionPeriodDayModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class EventSessionPeriodDayBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventSessionPeriodDayModule.EventSessionPeriodDayBase>
    {
		public EventSessionPeriodDayBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventSessionPeriodDayModule.EventSessionPeriodDayBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.EventSessionPeriodDayModule.EventSessionPeriodDayBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventSessionPeriodDayBaseFrontend FrontendItem
        {
            get { return (EventSessionPeriodDayBaseFrontend)base.FrontendItem; }

        }
        public new EventSessionPeriodDayBase DbItem
        {
            get
            {
                return (EventSessionPeriodDayBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Day { get; protected set; }

        public CmsPropertyInfo EventSessionPeriod { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
