using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.SettingModule;
using CS.WebComponentsGeneralV3.Cms.SettingModule;
using BusinessLogic_v3.Frontend.SettingModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class SettingBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.SettingModule.SettingBase>
    {
		public SettingBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.SettingModule.SettingBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.SettingModule.SettingBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new SettingBaseFrontend FrontendItem
        {
            get { return (SettingBaseFrontend)base.FrontendItem; }

        }
        public new SettingBase DbItem
        {
            get
            {
                return (SettingBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Name { get; protected set; }

        public CmsPropertyInfo Value { get; protected set; }

        public CmsPropertyInfo VisibleInCMS_AccessRequired { get; protected set; }

        public CmsPropertyInfo DataType { get; protected set; }

        public CmsPropertyInfo Parent { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo UsedInProject { get; protected set; }

        public CmsPropertyInfo LocalhostValue { get; protected set; }

        public CmsPropertyInfo EnumType { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ChildSettings { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ChildSettings = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SettingModule.SettingBase>.GetPropertyBySelector(item => item.ChildSettings),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SettingModule.SettingBase>.GetPropertyBySelector(item => item.Parent)));


			base.initBasicFields();
          
        }

    }
}
