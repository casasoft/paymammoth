using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.MemberLoginInfoModule;
using CS.WebComponentsGeneralV3.Cms.MemberLoginInfoModule;
using BusinessLogic_v3.Frontend.MemberLoginInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class MemberLoginInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase>
    {
		public MemberLoginInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.MemberLoginInfoModule.MemberLoginInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberLoginInfoBaseFrontend FrontendItem
        {
            get { return (MemberLoginInfoBaseFrontend)base.FrontendItem; }

        }
        public new MemberLoginInfoBase DbItem
        {
            get
            {
                return (MemberLoginInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Member { get; protected set; }

        public CmsPropertyInfo DateTime { get; protected set; }

        public CmsPropertyInfo IP { get; protected set; }

        public CmsPropertyInfo LoginInfoType { get; protected set; }

        public CmsPropertyInfo AdditionalMsg { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
