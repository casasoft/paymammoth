using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ClassifiedModule;
using CS.WebComponentsGeneralV3.Cms.ClassifiedModule;
using BusinessLogic_v3.Frontend.ClassifiedModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ClassifiedBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase>
    {
		public ClassifiedBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ClassifiedModule.ClassifiedBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ClassifiedBaseFrontend FrontendItem
        {
            get { return (ClassifiedBaseFrontend)base.FrontendItem; }

        }
        public new ClassifiedBase DbItem
        {
            get
            {
                return (ClassifiedBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Category { get; protected set; }

        public CmsPropertyInfo PhotoFilename { get; protected set; }

        public CmsPropertyInfo Price { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo PostedOn { get; protected set; }

        public CmsPropertyInfo ApprovedOn { get; protected set; }

        public CmsPropertyInfo ApprovedBy { get; protected set; }

        public CmsPropertyInfo Approved { get; protected set; }

        public CmsPropertyInfo IsPriceNegotiable { get; protected set; }

        public CmsPropertyInfo CreatedBy { get; protected set; }

        public CmsPropertyInfo Submitted { get; protected set; }

        public CmsPropertyInfo CustomContentTags { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
