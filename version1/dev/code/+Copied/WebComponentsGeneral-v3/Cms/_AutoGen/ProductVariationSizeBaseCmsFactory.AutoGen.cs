using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductVariationSizeModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationSizeModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductVariationSizeBaseCmsFactory_AutoGen : CmsFactoryBase<ProductVariationSizeBaseCmsInfo, ProductVariationSizeBase>
    {
       
       public new static ProductVariationSizeBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductVariationSizeBaseCmsFactory)CmsFactoryBase<ProductVariationSizeBaseCmsInfo, ProductVariationSizeBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ProductVariationSizeBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductVariationSize.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductVariationSize";

            this.QueryStringParamID = "ProductVariationSizeId";

            cmsInfo.TitlePlural = "Product Variation Sizes";

            cmsInfo.TitleSingular =  "Product Variation Size";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductVariationSizeBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductVariationSize/";
			UsedInProject = ProductVariationSizeBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
