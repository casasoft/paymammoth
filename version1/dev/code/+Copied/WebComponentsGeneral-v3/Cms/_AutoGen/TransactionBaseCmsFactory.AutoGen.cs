using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.TransactionModule;
using BusinessLogic_v3.Cms.TransactionModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class TransactionBaseCmsFactory_AutoGen : CmsFactoryBase<TransactionBaseCmsInfo, TransactionBase>
    {
       
       public new static TransactionBaseCmsFactory Instance
	    {
	         get
	         {
                 return (TransactionBaseCmsFactory)CmsFactoryBase<TransactionBaseCmsInfo, TransactionBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = TransactionBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Transaction.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Transaction";

            this.QueryStringParamID = "TransactionId";

            cmsInfo.TitlePlural = "Transactions";

            cmsInfo.TitleSingular =  "Transaction";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public TransactionBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Transaction/";


        }
       
    }

}
