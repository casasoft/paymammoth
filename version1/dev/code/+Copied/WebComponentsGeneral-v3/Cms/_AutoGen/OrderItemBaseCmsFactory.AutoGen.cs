using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.OrderItemModule;
using CS.WebComponentsGeneralV3.Cms.OrderItemModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class OrderItemBaseCmsFactory_AutoGen : CmsFactoryBase<OrderItemBaseCmsInfo, OrderItemBase>
    {
       
       public new static OrderItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (OrderItemBaseCmsFactory)CmsFactoryBase<OrderItemBaseCmsInfo, OrderItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = OrderItemBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "OrderItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "OrderItem";

            this.QueryStringParamID = "OrderItemId";

            cmsInfo.TitlePlural = "Order Items";

            cmsInfo.TitleSingular =  "Order Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public OrderItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "OrderItem/";
			UsedInProject = OrderItemBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
