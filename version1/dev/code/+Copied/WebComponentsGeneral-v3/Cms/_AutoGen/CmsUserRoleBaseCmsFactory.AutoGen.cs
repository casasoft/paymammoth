using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.CmsUserRoleModule;
using CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class CmsUserRoleBaseCmsFactory_AutoGen : CmsFactoryBase<CmsUserRoleBaseCmsInfo, CmsUserRoleBase>
    {
       
       public new static CmsUserRoleBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CmsUserRoleBaseCmsFactory)CmsFactoryBase<CmsUserRoleBaseCmsInfo, CmsUserRoleBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = CmsUserRoleBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CmsUserRole.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CmsUserRole";

            this.QueryStringParamID = "CmsUserRoleId";

            cmsInfo.TitlePlural = "Cms User Roles";

            cmsInfo.TitleSingular =  "Cms User Role";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CmsUserRoleBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "CmsUserRole/";
			UsedInProject = CmsUserRoleBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
