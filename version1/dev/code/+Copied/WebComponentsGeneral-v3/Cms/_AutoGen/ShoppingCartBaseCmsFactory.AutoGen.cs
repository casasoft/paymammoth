using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ShoppingCartModule;
using CS.WebComponentsGeneralV3.Cms.ShoppingCartModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ShoppingCartBaseCmsFactory_AutoGen : CmsFactoryBase<ShoppingCartBaseCmsInfo, ShoppingCartBase>
    {
       
       public new static ShoppingCartBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ShoppingCartBaseCmsFactory)CmsFactoryBase<ShoppingCartBaseCmsInfo, ShoppingCartBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ShoppingCartBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ShoppingCart.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ShoppingCart";

            this.QueryStringParamID = "ShoppingCartId";

            cmsInfo.TitlePlural = "Shopping Carts";

            cmsInfo.TitleSingular =  "Shopping Cart";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ShoppingCartBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ShoppingCart/";
			UsedInProject = ShoppingCartBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
