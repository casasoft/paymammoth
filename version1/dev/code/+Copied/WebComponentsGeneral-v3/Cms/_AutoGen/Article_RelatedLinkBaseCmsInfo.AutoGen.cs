using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.Article_RelatedLinkModule;
using CS.WebComponentsGeneralV3.Cms.Article_RelatedLinkModule;
using BusinessLogic_v3.Frontend.Article_RelatedLinkModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class Article_RelatedLinkBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase>
    {
		public Article_RelatedLinkBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.Article_RelatedLinkModule.Article_RelatedLinkBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new Article_RelatedLinkBaseFrontend FrontendItem
        {
            get { return (Article_RelatedLinkBaseFrontend)base.FrontendItem; }

        }
        public new Article_RelatedLinkBase DbItem
        {
            get
            {
                return (Article_RelatedLinkBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ParentPage { get; protected set; }

        public CmsPropertyInfo RelatedPage { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
