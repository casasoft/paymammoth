using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule;
using CS.WebComponentsGeneralV3.Cms.MemberSelfBarringPeriodModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class MemberSelfBarringPeriodBaseCmsFactory_AutoGen : CmsFactoryBase<MemberSelfBarringPeriodBaseCmsInfo, MemberSelfBarringPeriodBase>
    {
       
       public new static MemberSelfBarringPeriodBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberSelfBarringPeriodBaseCmsFactory)CmsFactoryBase<MemberSelfBarringPeriodBaseCmsInfo, MemberSelfBarringPeriodBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = MemberSelfBarringPeriodBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberSelfBarringPeriod.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberSelfBarringPeriod";

            this.QueryStringParamID = "MemberSelfBarringPeriodId";

            cmsInfo.TitlePlural = "Member Self Barring Periods";

            cmsInfo.TitleSingular =  "Member Self Barring Period";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberSelfBarringPeriodBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "MemberSelfBarringPeriod/";
			UsedInProject = MemberSelfBarringPeriodBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
