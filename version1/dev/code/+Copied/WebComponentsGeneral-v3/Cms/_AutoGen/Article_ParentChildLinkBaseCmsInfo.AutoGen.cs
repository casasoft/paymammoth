using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using CS.WebComponentsGeneralV3.Cms.Article_ParentChildLinkModule;
using BusinessLogic_v3.Frontend.Article_ParentChildLinkModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class Article_ParentChildLinkBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase>
    {
		public Article_ParentChildLinkBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.Article_ParentChildLinkModule.Article_ParentChildLinkBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new Article_ParentChildLinkBaseFrontend FrontendItem
        {
            get { return (Article_ParentChildLinkBaseFrontend)base.FrontendItem; }

        }
        public new Article_ParentChildLinkBase DbItem
        {
            get
            {
                return (Article_ParentChildLinkBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Parent { get; protected set; }

        public CmsPropertyInfo Child { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
