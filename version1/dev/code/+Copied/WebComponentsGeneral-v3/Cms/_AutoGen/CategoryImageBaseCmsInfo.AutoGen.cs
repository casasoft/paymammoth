using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.CategoryImageModule;
using CS.WebComponentsGeneralV3.Cms.CategoryImageModule;
using BusinessLogic_v3.Frontend.CategoryImageModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class CategoryImageBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase>
    {
		public CategoryImageBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.CategoryImageModule.CategoryImageBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CategoryImageBaseFrontend FrontendItem
        {
            get { return (CategoryImageBaseFrontend)base.FrontendItem; }

        }
        public new CategoryImageBase DbItem
        {
            get
            {
                return (CategoryImageBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Category { get; protected set; }

        public CmsPropertyInfo ImageFilename { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
