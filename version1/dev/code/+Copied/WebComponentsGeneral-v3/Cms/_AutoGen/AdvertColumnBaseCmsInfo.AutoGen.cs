using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.AdvertColumnModule;
using CS.WebComponentsGeneralV3.Cms.AdvertColumnModule;
using BusinessLogic_v3.Frontend.AdvertColumnModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class AdvertColumnBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase>
    {
		public AdvertColumnBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.AdvertColumnModule.AdvertColumnBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AdvertColumnBaseFrontend FrontendItem
        {
            get { return (AdvertColumnBaseFrontend)base.FrontendItem; }

        }
        public new AdvertColumnBase DbItem
        {
            get
            {
                return (AdvertColumnBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo AdvertSlots { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.AdvertSlots = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase>.GetPropertyBySelector(item => item.AdvertSlots),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>.GetPropertyBySelector(item => item.AdvertColumn)));


			base.initBasicFields();
          
        }

    }
}
