using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.TestimonialModule;
using CS.WebComponentsGeneralV3.Cms.TestimonialModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class TestimonialBaseCmsFactory_AutoGen : CmsFactoryBase<TestimonialBaseCmsInfo, TestimonialBase>
    {
       
       public new static TestimonialBaseCmsFactory Instance
	    {
	         get
	         {
                 return (TestimonialBaseCmsFactory)CmsFactoryBase<TestimonialBaseCmsInfo, TestimonialBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = TestimonialBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Testimonial.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Testimonial";

            this.QueryStringParamID = "TestimonialId";

            cmsInfo.TitlePlural = "Testimonials";

            cmsInfo.TitleSingular =  "Testimonial";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public TestimonialBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Testimonial/";
			UsedInProject = TestimonialBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
