using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ContentPageMediaItemModule;
using BusinessLogic_v3.Cms.ContentPageMediaItemModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ContentPageMediaItemBaseCmsFactory_AutoGen : CmsFactoryBase<ContentPageMediaItemBaseCmsInfo, ContentPageMediaItemBase>
    {
       
       public new static ContentPageMediaItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContentPageMediaItemBaseCmsFactory)CmsFactoryBase<ContentPageMediaItemBaseCmsInfo, ContentPageMediaItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ContentPageMediaItemBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContentPageMediaItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContentPageMediaItem";

            this.QueryStringParamID = "ContentPageMediaItemId";

            cmsInfo.TitlePlural = "Content Page Media Items";

            cmsInfo.TitleSingular =  "Content Page Media Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContentPageMediaItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ContentPageMediaItem/";


        }
       
    }

}
