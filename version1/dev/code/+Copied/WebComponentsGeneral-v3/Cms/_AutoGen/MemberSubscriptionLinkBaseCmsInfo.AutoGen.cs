using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.MemberSubscriptionLinkModule;
using CS.WebComponentsGeneralV3.Cms.MemberSubscriptionLinkModule;
using BusinessLogic_v3.Frontend.MemberSubscriptionLinkModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class MemberSubscriptionLinkBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase>
    {
		public MemberSubscriptionLinkBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.MemberSubscriptionLinkModule.MemberSubscriptionLinkBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberSubscriptionLinkBaseFrontend FrontendItem
        {
            get { return (MemberSubscriptionLinkBaseFrontend)base.FrontendItem; }

        }
        public new MemberSubscriptionLinkBase DbItem
        {
            get
            {
                return (MemberSubscriptionLinkBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo SubscriptionTypePrice { get; protected set; }

        public CmsPropertyInfo CurrentSubscriptionEndDate { get; protected set; }

        public CmsPropertyInfo CurrentSubscriptionStartDate { get; protected set; }

        public CmsPropertyInfo IsSubscriptionExpired { get; protected set; }

        public CmsPropertyInfo SubscriptionExpiryNotificationSent { get; protected set; }

        public CmsPropertyInfo SubscriptionNotification1Sent { get; protected set; }

        public CmsPropertyInfo SubscriptionNotification2Sent { get; protected set; }

        public CmsPropertyInfo Member { get; protected set; }

        public CmsPropertyInfo Order { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
