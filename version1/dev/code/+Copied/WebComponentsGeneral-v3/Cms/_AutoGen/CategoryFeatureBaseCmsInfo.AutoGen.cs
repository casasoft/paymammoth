using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.CategoryFeatureModule;
using CS.WebComponentsGeneralV3.Cms.CategoryFeatureModule;
using BusinessLogic_v3.Frontend.CategoryFeatureModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class CategoryFeatureBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>
    {
		public CategoryFeatureBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.CategoryFeatureModule.CategoryFeatureBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CategoryFeatureBaseFrontend FrontendItem
        {
            get { return (CategoryFeatureBaseFrontend)base.FrontendItem; }

        }
        public new CategoryFeatureBase DbItem
        {
            get
            {
                return (CategoryFeatureBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo IconFilename { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo Link { get; protected set; }

        public CmsPropertyInfo Category { get; protected set; }

        public CmsPropertyInfo ItemGroup { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ProductCategoryFeatureValues { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ProductCategoryFeatureValues = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.GetPropertyBySelector(item => item.ProductCategoryFeatureValues),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase>.GetPropertyBySelector(item => item.CategoryFeature)));


			base.initBasicFields();
          
        }

    }
}
