using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.MemberLoginInfoModule;
using CS.WebComponentsGeneralV3.Cms.MemberLoginInfoModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class MemberLoginInfoBaseCmsFactory_AutoGen : CmsFactoryBase<MemberLoginInfoBaseCmsInfo, MemberLoginInfoBase>
    {
       
       public new static MemberLoginInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberLoginInfoBaseCmsFactory)CmsFactoryBase<MemberLoginInfoBaseCmsInfo, MemberLoginInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = MemberLoginInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberLoginInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberLoginInfo";

            this.QueryStringParamID = "MemberLoginInfoId";

            cmsInfo.TitlePlural = "Member Login Infos";

            cmsInfo.TitleSingular =  "Member Login Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberLoginInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "MemberLoginInfo/";
			UsedInProject = MemberLoginInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
