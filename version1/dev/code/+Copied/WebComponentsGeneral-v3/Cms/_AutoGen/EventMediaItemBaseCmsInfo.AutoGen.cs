using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.EventMediaItemModule;
using CS.WebComponentsGeneralV3.Cms.EventMediaItemModule;
using BusinessLogic_v3.Frontend.EventMediaItemModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class EventMediaItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>
    {
		public EventMediaItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.EventMediaItemModule.EventMediaItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventMediaItemBaseFrontend FrontendItem
        {
            get { return (EventMediaItemBaseFrontend)base.FrontendItem; }

        }
        public new EventMediaItemBase DbItem
        {
            get
            {
                return (EventMediaItemBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo ImageFilename { get; protected set; }

        public CmsPropertyInfo Event { get; protected set; }

        public CmsPropertyInfo VideoLink { get; protected set; }

        public CmsPropertyInfo EventBasic { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
