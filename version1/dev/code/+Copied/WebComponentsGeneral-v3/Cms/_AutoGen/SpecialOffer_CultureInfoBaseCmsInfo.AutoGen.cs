using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.SpecialOffer_CultureInfoModule;
using BusinessLogic_v3.Frontend.SpecialOffer_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class SpecialOffer_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase>
    {
		public SpecialOffer_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new SpecialOffer_CultureInfoBaseFrontend FrontendItem
        {
            get { return (SpecialOffer_CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new SpecialOffer_CultureInfoBase DbItem
        {
            get
            {
                return (SpecialOffer_CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; protected set; }

        public CmsPropertyInfo SpecialOffer { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
