using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductVariationModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductVariationBaseCmsFactory_AutoGen : CmsFactoryBase<ProductVariationBaseCmsInfo, ProductVariationBase>
    {
       
       public new static ProductVariationBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductVariationBaseCmsFactory)CmsFactoryBase<ProductVariationBaseCmsInfo, ProductVariationBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ProductVariationBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductVariation.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductVariation";

            this.QueryStringParamID = "ProductVariationId";

            cmsInfo.TitlePlural = "Product Variations";

            cmsInfo.TitleSingular =  "Product Variation";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductVariationBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductVariation/";
			UsedInProject = ProductVariationBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
