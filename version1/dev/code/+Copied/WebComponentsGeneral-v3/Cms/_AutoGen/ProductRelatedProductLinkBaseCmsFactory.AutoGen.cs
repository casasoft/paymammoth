using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductRelatedProductLinkModule;
using CS.WebComponentsGeneralV3.Cms.ProductRelatedProductLinkModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductRelatedProductLinkBaseCmsFactory_AutoGen : CmsFactoryBase<ProductRelatedProductLinkBaseCmsInfo, ProductRelatedProductLinkBase>
    {
       
       public new static ProductRelatedProductLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductRelatedProductLinkBaseCmsFactory)CmsFactoryBase<ProductRelatedProductLinkBaseCmsInfo, ProductRelatedProductLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ProductRelatedProductLinkBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductRelatedProductLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductRelatedProductLink";

            this.QueryStringParamID = "ProductRelatedProductLinkId";

            cmsInfo.TitlePlural = "Product Related Product Links";

            cmsInfo.TitleSingular =  "Product Related Product Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductRelatedProductLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductRelatedProductLink/";
			UsedInProject = ProductRelatedProductLinkBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
