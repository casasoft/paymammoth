using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.CmsAuditLogModule;
using CS.WebComponentsGeneralV3.Cms.CmsAuditLogModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class CmsAuditLogBaseCmsFactory_AutoGen : CmsFactoryBase<CmsAuditLogBaseCmsInfo, CmsAuditLogBase>
    {
       
       public new static CmsAuditLogBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CmsAuditLogBaseCmsFactory)CmsFactoryBase<CmsAuditLogBaseCmsInfo, CmsAuditLogBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	qParams.FillDefaultsForCms();
        	
            var q = CmsAuditLogBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CmsAuditLog.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CmsAuditLog";

            this.QueryStringParamID = "CmsAuditLogId";

            cmsInfo.TitlePlural = "Cms Audit Logs";

            cmsInfo.TitleSingular =  "Cms Audit Log";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CmsAuditLogBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "CmsAuditLog/";
			UsedInProject = CmsAuditLogBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
