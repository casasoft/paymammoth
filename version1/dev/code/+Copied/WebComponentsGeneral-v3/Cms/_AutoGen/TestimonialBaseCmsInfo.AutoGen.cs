using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.TestimonialModule;
using CS.WebComponentsGeneralV3.Cms.TestimonialModule;
using BusinessLogic_v3.Frontend.TestimonialModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class TestimonialBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.TestimonialModule.TestimonialBase>
    {
		public TestimonialBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.TestimonialModule.TestimonialBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.TestimonialModule.TestimonialBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new TestimonialBaseFrontend FrontendItem
        {
            get { return (TestimonialBaseFrontend)base.FrontendItem; }

        }
        public new TestimonialBase DbItem
        {
            get
            {
                return (TestimonialBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo FullName { get; protected set; }

        public CmsPropertyInfo Testimonial { get; protected set; }

        public CmsPropertyInfo Event { get; protected set; }

        public CmsPropertyInfo DateSubmitted { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
