using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.CategorySpecificationModule;
using CS.WebComponentsGeneralV3.Cms.CategorySpecificationModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class CategorySpecificationBaseCmsFactory_AutoGen : CmsFactoryBase<CategorySpecificationBaseCmsInfo, CategorySpecificationBase>
    {
       
       public new static CategorySpecificationBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CategorySpecificationBaseCmsFactory)CmsFactoryBase<CategorySpecificationBaseCmsInfo, CategorySpecificationBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = CategorySpecificationBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CategorySpecification.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CategorySpecification";

            this.QueryStringParamID = "CategorySpecificationId";

            cmsInfo.TitlePlural = "Category Specifications";

            cmsInfo.TitleSingular =  "Category Specification";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CategorySpecificationBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "CategorySpecification/";
			UsedInProject = CategorySpecificationBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
