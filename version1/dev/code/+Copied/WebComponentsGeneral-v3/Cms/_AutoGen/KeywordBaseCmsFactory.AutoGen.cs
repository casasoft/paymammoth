using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.KeywordModule;
using CS.WebComponentsGeneralV3.Cms.KeywordModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class KeywordBaseCmsFactory_AutoGen : CmsFactoryBase<KeywordBaseCmsInfo, KeywordBase>
    {
       
       public new static KeywordBaseCmsFactory Instance
	    {
	         get
	         {
                 return (KeywordBaseCmsFactory)CmsFactoryBase<KeywordBaseCmsInfo, KeywordBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = KeywordBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Keyword.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Keyword";

            this.QueryStringParamID = "KeywordId";

            cmsInfo.TitlePlural = "Keywords";

            cmsInfo.TitleSingular =  "Keyword";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public KeywordBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Keyword/";
			UsedInProject = KeywordBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
