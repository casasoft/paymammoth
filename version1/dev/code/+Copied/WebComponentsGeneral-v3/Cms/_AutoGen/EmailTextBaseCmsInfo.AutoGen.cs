using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.EmailTextModule;
using CS.WebComponentsGeneralV3.Cms.EmailTextModule;
using BusinessLogic_v3.Frontend.EmailTextModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class EmailTextBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>
    {
		public EmailTextBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.EmailTextModule.EmailTextBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EmailTextBaseFrontend FrontendItem
        {
            get { return (EmailTextBaseFrontend)base.FrontendItem; }

        }
        public new EmailTextBase DbItem
        {
            get
            {
                return (EmailTextBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Parent { get; protected set; }

        public CmsPropertyInfo Subject { get; protected set; }

        public CmsPropertyInfo Subject_Search { get; protected set; }

        public CmsPropertyInfo Body { get; protected set; }

        public CmsPropertyInfo Body_Search { get; protected set; }

        public CmsPropertyInfo Remarks { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo VisibleInCMS_AccessRequired { get; protected set; }

        public CmsPropertyInfo UsedInProject { get; protected set; }

        public CmsPropertyInfo CustomContentTags { get; protected set; }

        public CmsPropertyInfo WhenIsThisEmailSent { get; protected set; }

        public CmsPropertyInfo NotifyAdminAboutEmail { get; protected set; }

        public CmsPropertyInfo NotifyAdminCustomEmail { get; protected set; }

        public CmsPropertyInfo ProcessContentUsingNVelocity { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ChildEmails { get; protected set; }
        

        public CmsCollectionInfo EmailText_CultureInfos { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ChildEmails = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector(item => item.ChildEmails),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector(item => item.Parent)));
		
		//InitCollectionBaseOneToMany
		this.EmailText_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector(item => item.EmailText_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase>.GetPropertyBySelector(item => item.EmailText)));


			base.initBasicFields();
          
        }

    }
}
