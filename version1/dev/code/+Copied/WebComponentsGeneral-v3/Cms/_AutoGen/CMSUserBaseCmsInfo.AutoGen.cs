using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.WebComponentsGeneralV3.Cms.CmsUserModule;
using BusinessLogic_v3.Frontend.CmsUserModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class CmsUserBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase>
    {
		public CmsUserBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.CmsUserModule.CmsUserBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CmsUserBaseFrontend FrontendItem
        {
            get { return (CmsUserBaseFrontend)base.FrontendItem; }

        }
        public new CmsUserBase DbItem
        {
            get
            {
                return (CmsUserBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Name { get; protected set; }

        public CmsPropertyInfo Surname { get; protected set; }

        public CmsPropertyInfo Username { get; protected set; }

        public CmsPropertyInfo Password { get; protected set; }

        public CmsPropertyInfo LastLoggedIn { get; protected set; }

        public CmsPropertyInfo _LastLoggedIn_New { get; protected set; }

        public CmsPropertyInfo AccessType { get; protected set; }

        public CmsPropertyInfo SessionGUID { get; protected set; }

        public CmsPropertyInfo PasswordSalt { get; protected set; }

        public CmsPropertyInfo PasswordEncryptionType { get; protected set; }

        public CmsPropertyInfo PasswordIterations { get; protected set; }

        public CmsPropertyInfo HiddenFromNonCasaSoft { get; protected set; }

        public CmsPropertyInfo CmsCustomLogoFilename { get; protected set; }

        public CmsPropertyInfo CmsCustomLogoLinkUrl { get; protected set; }

        public CmsPropertyInfo Disabled { get; protected set; }

        public CmsPropertyInfo TicketingSystemSupportUser { get; protected set; }

        public CmsPropertyInfo Email { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo Affiliates { get; protected set; }
        

        public CmsCollectionInfo Classifieds { get; protected set; }
        

        public CmsCollectionInfo EventSubmissions { get; protected set; }
        

        public CmsCollectionInfo Members { get; protected set; }
        

        public CmsCollectionInfo AssignedTickets { get; protected set; }
        

        public CmsPropertyManyToManyCollection<CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsInfo> CmsUserRoles { get; protected set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.Affiliates = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase>.GetPropertyBySelector(item => item.Affiliates),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector(item => item.LinkedCmsUser)));
		
		//InitCollectionBaseOneToMany
		this.Classifieds = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase>.GetPropertyBySelector(item => item.Classifieds),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase>.GetPropertyBySelector(item => item.ApprovedBy)));
		
		//InitCollectionBaseOneToMany
		this.EventSubmissions = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase>.GetPropertyBySelector(item => item.EventSubmissions),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase>.GetPropertyBySelector(item => item.ApprovedBy)));
		
		//InitCollectionBaseOneToMany
		this.Members = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase>.GetPropertyBySelector(item => item.Members),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.AssignedToTicketingSystemSupportUser)));
		
		//InitCollectionBaseOneToMany
		this.AssignedTickets = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase>.GetPropertyBySelector(item => item.AssignedTickets),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TicketModule.TicketBase>.GetPropertyBySelector(item => item.AssignedTo)));
//InitFieldManyToManyBase_RightSide
		this.CmsUserRoles = this.AddManyToManyCollectionWithDirectProperty<CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsInfo>(
								item => item.CmsUserRoles, 
								CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
