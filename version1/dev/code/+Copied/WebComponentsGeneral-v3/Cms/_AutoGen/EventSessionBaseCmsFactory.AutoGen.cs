using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.EventSessionModule;
using CS.WebComponentsGeneralV3.Cms.EventSessionModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class EventSessionBaseCmsFactory_AutoGen : CmsFactoryBase<EventSessionBaseCmsInfo, EventSessionBase>
    {
       
       public new static EventSessionBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventSessionBaseCmsFactory)CmsFactoryBase<EventSessionBaseCmsInfo, EventSessionBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = EventSessionBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EventSession.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EventSession";

            this.QueryStringParamID = "EventSessionId";

            cmsInfo.TitlePlural = "Event Sessions";

            cmsInfo.TitleSingular =  "Event Session";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventSessionBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "EventSession/";
			UsedInProject = EventSessionBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
