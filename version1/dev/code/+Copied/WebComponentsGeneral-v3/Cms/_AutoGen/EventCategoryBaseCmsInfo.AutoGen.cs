using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.EventCategoryModule;
using CS.WebComponentsGeneralV3.Cms.EventCategoryModule;
using BusinessLogic_v3.Frontend.EventCategoryModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class EventCategoryBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase>
    {
		public EventCategoryBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.EventCategoryModule.EventCategoryBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventCategoryBaseFrontend FrontendItem
        {
            get { return (EventCategoryBaseFrontend)base.FrontendItem; }

        }
        public new EventCategoryBase DbItem
        {
            get
            {
                return (EventCategoryBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo Events { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.Events = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase>.GetPropertyBySelector(item => item.Events),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector(item => item.EventCategory)));


			base.initBasicFields();
          
        }

    }
}
