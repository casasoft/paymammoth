using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.EventBasicModule;
using CS.WebComponentsGeneralV3.Cms.EventBasicModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class EventBasicBaseCmsFactory_AutoGen : CmsFactoryBase<EventBasicBaseCmsInfo, EventBasicBase>
    {
       
       public new static EventBasicBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventBasicBaseCmsFactory)CmsFactoryBase<EventBasicBaseCmsInfo, EventBasicBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = EventBasicBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EventBasic.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EventBasic";

            this.QueryStringParamID = "EventBasicId";

            cmsInfo.TitlePlural = "Event Basics";

            cmsInfo.TitleSingular =  "Event Basic";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventBasicBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "EventBasic/";
			UsedInProject = EventBasicBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
