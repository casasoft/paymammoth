using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.BlogPostModule;
using BusinessLogic_v3.Cms.BlogPostModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class BlogPostBaseCmsFactory_AutoGen : CmsFactoryBase<BlogPostBaseCmsInfo, BlogPostBase>
    {
       
       public new static BlogPostBaseCmsFactory Instance
	    {
	         get
	         {
                 return (BlogPostBaseCmsFactory)CmsFactoryBase<BlogPostBaseCmsInfo, BlogPostBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = BlogPostBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "BlogPost.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "BlogPost";

            this.QueryStringParamID = "BlogPostId";

            cmsInfo.TitlePlural = "Blog Posts";

            cmsInfo.TitleSingular =  "Blog Post";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public BlogPostBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "BlogPost/";


        }
       
    }

}
