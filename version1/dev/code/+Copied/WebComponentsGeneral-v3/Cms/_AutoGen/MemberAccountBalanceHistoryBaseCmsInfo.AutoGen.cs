using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;
using CS.WebComponentsGeneralV3.Cms.MemberAccountBalanceHistoryModule;
using BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class MemberAccountBalanceHistoryBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase>
    {
		public MemberAccountBalanceHistoryBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberAccountBalanceHistoryBaseFrontend FrontendItem
        {
            get { return (MemberAccountBalanceHistoryBaseFrontend)base.FrontendItem; }

        }
        public new MemberAccountBalanceHistoryBase DbItem
        {
            get
            {
                return (MemberAccountBalanceHistoryBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Member { get; protected set; }

        public CmsPropertyInfo BalanceUpdate { get; protected set; }

        public CmsPropertyInfo Date { get; protected set; }

        public CmsPropertyInfo OrderLink { get; protected set; }

        public CmsPropertyInfo UpdateType { get; protected set; }

        public CmsPropertyInfo BalanceTotal { get; protected set; }

        public CmsPropertyInfo Comments { get; protected set; }

        public CmsPropertyInfo RenewableBalanceTotal { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
