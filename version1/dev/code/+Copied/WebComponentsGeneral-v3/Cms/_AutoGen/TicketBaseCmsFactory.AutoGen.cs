using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.TicketModule;
using CS.WebComponentsGeneralV3.Cms.TicketModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class TicketBaseCmsFactory_AutoGen : CmsFactoryBase<TicketBaseCmsInfo, TicketBase>
    {
       
       public new static TicketBaseCmsFactory Instance
	    {
	         get
	         {
                 return (TicketBaseCmsFactory)CmsFactoryBase<TicketBaseCmsInfo, TicketBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = TicketBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Ticket.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Ticket";

            this.QueryStringParamID = "TicketId";

            cmsInfo.TitlePlural = "Tickets";

            cmsInfo.TitleSingular =  "Ticket";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public TicketBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Ticket/";
			UsedInProject = TicketBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
