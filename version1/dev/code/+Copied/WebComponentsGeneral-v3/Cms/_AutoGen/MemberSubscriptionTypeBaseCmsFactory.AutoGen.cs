using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.MemberSubscriptionTypeModule;
using CS.WebComponentsGeneralV3.Cms.MemberSubscriptionTypeModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class MemberSubscriptionTypeBaseCmsFactory_AutoGen : CmsFactoryBase<MemberSubscriptionTypeBaseCmsInfo, MemberSubscriptionTypeBase>
    {
       
       public new static MemberSubscriptionTypeBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberSubscriptionTypeBaseCmsFactory)CmsFactoryBase<MemberSubscriptionTypeBaseCmsInfo, MemberSubscriptionTypeBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = MemberSubscriptionTypeBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberSubscriptionType.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberSubscriptionType";

            this.QueryStringParamID = "MemberSubscriptionTypeId";

            cmsInfo.TitlePlural = "Member Subscription Types";

            cmsInfo.TitleSingular =  "Member Subscription Type";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberSubscriptionTypeBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "MemberSubscriptionType/";
			UsedInProject = MemberSubscriptionTypeBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
