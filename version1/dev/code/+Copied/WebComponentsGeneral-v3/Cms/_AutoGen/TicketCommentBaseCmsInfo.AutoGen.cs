using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.TicketCommentModule;
using CS.WebComponentsGeneralV3.Cms.TicketCommentModule;
using BusinessLogic_v3.Frontend.TicketCommentModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class TicketCommentBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase>
    {
		public TicketCommentBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.TicketCommentModule.TicketCommentBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new TicketCommentBaseFrontend FrontendItem
        {
            get { return (TicketCommentBaseFrontend)base.FrontendItem; }

        }
        public new TicketCommentBase DbItem
        {
            get
            {
                return (TicketCommentBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Ticket { get; protected set; }

        public CmsPropertyInfo Comment { get; protected set; }

        public CmsPropertyInfo IPAddress { get; protected set; }

        public CmsPropertyInfo PostedOn { get; protected set; }

        public CmsPropertyInfo AssignedStaffUser { get; protected set; }

        public CmsPropertyInfo Member { get; protected set; }

        public CmsPropertyInfo CommentType { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo Attachments { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.Attachments = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase>.GetPropertyBySelector(item => item.Attachments),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase>.GetPropertyBySelector(item => item.TicketComment)));


			base.initBasicFields();
          
        }

    }
}
