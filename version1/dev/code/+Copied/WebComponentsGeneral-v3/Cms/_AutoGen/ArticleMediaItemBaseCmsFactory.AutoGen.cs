using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ArticleMediaItemModule;
using CS.WebComponentsGeneralV3.Cms.ArticleMediaItemModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ArticleMediaItemBaseCmsFactory_AutoGen : CmsFactoryBase<ArticleMediaItemBaseCmsInfo, ArticleMediaItemBase>
    {
       
       public new static ArticleMediaItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ArticleMediaItemBaseCmsFactory)CmsFactoryBase<ArticleMediaItemBaseCmsInfo, ArticleMediaItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ArticleMediaItemBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ArticleMediaItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ArticleMediaItem";

            this.QueryStringParamID = "ArticleMediaItemId";

            cmsInfo.TitlePlural = "Article Media Items";

            cmsInfo.TitleSingular =  "Article Media Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ArticleMediaItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ArticleMediaItem/";
			UsedInProject = ArticleMediaItemBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
