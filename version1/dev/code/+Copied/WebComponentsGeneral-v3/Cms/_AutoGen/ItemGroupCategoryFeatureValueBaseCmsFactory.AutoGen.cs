using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ItemGroupCategoryFeatureValueModule;
using BusinessLogic_v3.Cms.ItemGroupCategoryFeatureValueModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ItemGroupCategoryFeatureValueBaseCmsFactory_AutoGen : CmsFactoryBase<ItemGroupCategoryFeatureValueBaseCmsInfo, ItemGroupCategoryFeatureValueBase>
    {
       
       public new static ItemGroupCategoryFeatureValueBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ItemGroupCategoryFeatureValueBaseCmsFactory)CmsFactoryBase<ItemGroupCategoryFeatureValueBaseCmsInfo, ItemGroupCategoryFeatureValueBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ItemGroupCategoryFeatureValueBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ItemGroupCategoryFeatureValue.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ItemGroupCategoryFeatureValue";

            this.QueryStringParamID = "ItemGroupCategoryFeatureValueId";

            cmsInfo.TitlePlural = "Item Group Category Feature Values";

            cmsInfo.TitleSingular =  "Item Group Category Feature Value";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ItemGroupCategoryFeatureValueBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ItemGroupCategoryFeatureValue/";


        }
       
    }

}
