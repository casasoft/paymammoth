using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.Item_CultureInfoModule;
using BusinessLogic_v3.Cms.Item_CultureInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class Item_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<Item_CultureInfoBaseCmsInfo, Item_CultureInfoBase>
    {
       
       public new static Item_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Item_CultureInfoBaseCmsFactory)CmsFactoryBase<Item_CultureInfoBaseCmsInfo, Item_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = Item_CultureInfoBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Item_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Item_CultureInfo";

            this.QueryStringParamID = "Item_CultureInfoId";

            cmsInfo.TitlePlural = "Item _ Culture Infos";

            cmsInfo.TitleSingular =  "Item _ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Item_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Item_CultureInfo/";


        }
       
    }

}
