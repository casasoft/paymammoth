using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.CategorySpecificationModule;
using CS.WebComponentsGeneralV3.Cms.CategorySpecificationModule;
using BusinessLogic_v3.Frontend.CategorySpecificationModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class CategorySpecificationBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>
    {
		public CategorySpecificationBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.CategorySpecificationModule.CategorySpecificationBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CategorySpecificationBaseFrontend FrontendItem
        {
            get { return (CategorySpecificationBaseFrontend)base.FrontendItem; }

        }
        public new CategorySpecificationBase DbItem
        {
            get
            {
                return (CategorySpecificationBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo DataType { get; protected set; }

        public CmsPropertyInfo MeasurementUnit { get; protected set; }

        public CmsPropertyInfo Category { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
