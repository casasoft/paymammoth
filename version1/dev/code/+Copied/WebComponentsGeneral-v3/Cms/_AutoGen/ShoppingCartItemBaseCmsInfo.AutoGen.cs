using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ShoppingCartItemModule;
using CS.WebComponentsGeneralV3.Cms.ShoppingCartItemModule;
using BusinessLogic_v3.Frontend.ShoppingCartItemModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ShoppingCartItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>
    {
		public ShoppingCartItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ShoppingCartItemModule.ShoppingCartItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ShoppingCartItemBaseFrontend FrontendItem
        {
            get { return (ShoppingCartItemBaseFrontend)base.FrontendItem; }

        }
        public new ShoppingCartItemBase DbItem
        {
            get
            {
                return (ShoppingCartItemBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Quantity { get; protected set; }

        public CmsPropertyInfo ShoppingCart { get; protected set; }

        public CmsPropertyInfo LinkedProductVariation { get; protected set; }

        public CmsPropertyInfo LinkedSubscriptionTypePrice { get; protected set; }

        public CmsPropertyInfo AutoRenewSubscriptionTypePrice { get; protected set; }

        public CmsPropertyInfo OrderLinkWhenConverted { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
