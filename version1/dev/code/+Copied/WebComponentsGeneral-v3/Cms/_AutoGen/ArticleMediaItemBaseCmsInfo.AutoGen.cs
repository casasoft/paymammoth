using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ArticleMediaItemModule;
using CS.WebComponentsGeneralV3.Cms.ArticleMediaItemModule;
using BusinessLogic_v3.Frontend.ArticleMediaItemModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ArticleMediaItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase>
    {
		public ArticleMediaItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ArticleMediaItemModule.ArticleMediaItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ArticleMediaItemBaseFrontend FrontendItem
        {
            get { return (ArticleMediaItemBaseFrontend)base.FrontendItem; }

        }
        public new ArticleMediaItemBase DbItem
        {
            get
            {
                return (ArticleMediaItemBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Caption { get; protected set; }

        public CmsPropertyInfo ContentPage { get; protected set; }

        public CmsPropertyInfo VideoDuration { get; protected set; }

        public CmsPropertyInfo Filename { get; protected set; }

        public CmsPropertyInfo VideoLink { get; protected set; }

        public CmsPropertyInfo IsMainImage { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
