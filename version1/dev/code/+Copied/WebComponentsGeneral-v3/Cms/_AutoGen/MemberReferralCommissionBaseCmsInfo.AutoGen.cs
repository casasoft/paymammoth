using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.MemberReferralCommissionModule;
using CS.WebComponentsGeneralV3.Cms.MemberReferralCommissionModule;
using BusinessLogic_v3.Frontend.MemberReferralCommissionModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class MemberReferralCommissionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>
    {
		public MemberReferralCommissionBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.MemberReferralCommissionModule.MemberReferralCommissionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberReferralCommissionBaseFrontend FrontendItem
        {
            get { return (MemberReferralCommissionBaseFrontend)base.FrontendItem; }

        }
        public new MemberReferralCommissionBase DbItem
        {
            get
            {
                return (MemberReferralCommissionBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateIssued { get; protected set; }

        public CmsPropertyInfo Amount { get; protected set; }

        public CmsPropertyInfo Status { get; protected set; }

        public CmsPropertyInfo PaidOn { get; protected set; }

        public CmsPropertyInfo PaidBy { get; protected set; }

        public CmsPropertyInfo PaymentRemarks { get; protected set; }

        public CmsPropertyInfo MemberReferral { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
