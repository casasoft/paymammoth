using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentPageRatingModule;
using BusinessLogic_v3.Cms.ContentPageRatingModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentPageRatingBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentPageRatingModule.ContentPageRatingBase>
    {
		public ContentPageRatingBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentPageRatingModule.ContentPageRatingBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentPageRatingModule.ContentPageRatingBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentPageRatingBase DbItem
        {
            get
            {
                return (ContentPageRatingBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo IPAddress { get; private set; }

        public CmsPropertyInfo Date { get; private set; }

        public CmsPropertyInfo Rating { get; private set; }

        public CmsPropertyInfo ContentPage { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.IPAddress = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageRatingModule.ContentPageRatingBase>.GetPropertyBySelector( item => item.IPAddress),
        			isEditable: true,
        			isVisible: true
        			);

        this.Date = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageRatingModule.ContentPageRatingBase>.GetPropertyBySelector( item => item.Date),
        			isEditable: true,
        			isVisible: true
        			);

        this.Rating = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageRatingModule.ContentPageRatingBase>.GetPropertyBySelector( item => item.Rating),
        			isEditable: true,
        			isVisible: true
        			);

        this.ContentPage = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageRatingModule.ContentPageRatingBase>.GetPropertyBySelector( item => item.ContentPage),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
