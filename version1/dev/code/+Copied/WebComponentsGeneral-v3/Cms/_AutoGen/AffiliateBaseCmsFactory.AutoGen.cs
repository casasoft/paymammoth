using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.AffiliateModule;
using CS.WebComponentsGeneralV3.Cms.AffiliateModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class AffiliateBaseCmsFactory_AutoGen : CmsFactoryBase<AffiliateBaseCmsInfo, AffiliateBase>
    {
       
       public new static AffiliateBaseCmsFactory Instance
	    {
	         get
	         {
                 return (AffiliateBaseCmsFactory)CmsFactoryBase<AffiliateBaseCmsInfo, AffiliateBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = AffiliateBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Affiliate.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Affiliate";

            this.QueryStringParamID = "AffiliateId";

            cmsInfo.TitlePlural = "Affiliates";

            cmsInfo.TitleSingular =  "Affiliate";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public AffiliateBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Affiliate/";
			UsedInProject = AffiliateBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
