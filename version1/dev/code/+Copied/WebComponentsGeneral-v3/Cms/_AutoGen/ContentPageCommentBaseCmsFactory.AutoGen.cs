using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ContentPageCommentModule;
using BusinessLogic_v3.Cms.ContentPageCommentModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ContentPageCommentBaseCmsFactory_AutoGen : CmsFactoryBase<ContentPageCommentBaseCmsInfo, ContentPageCommentBase>
    {
       
       public new static ContentPageCommentBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContentPageCommentBaseCmsFactory)CmsFactoryBase<ContentPageCommentBaseCmsInfo, ContentPageCommentBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ContentPageCommentBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContentPageComment.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContentPageComment";

            this.QueryStringParamID = "ContentPageCommentId";

            cmsInfo.TitlePlural = "Content Page Comments";

            cmsInfo.TitleSingular =  "Content Page Comment";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContentPageCommentBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ContentPageComment/";


        }
       
    }

}
