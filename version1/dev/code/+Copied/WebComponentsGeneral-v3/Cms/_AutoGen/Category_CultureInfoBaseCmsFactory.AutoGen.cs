using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.Category_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Category_CultureInfoModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class Category_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<Category_CultureInfoBaseCmsInfo, Category_CultureInfoBase>
    {
       
       public new static Category_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Category_CultureInfoBaseCmsFactory)CmsFactoryBase<Category_CultureInfoBaseCmsInfo, Category_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = Category_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Category_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Category_CultureInfo";

            this.QueryStringParamID = "Category_CultureInfoId";

            cmsInfo.TitlePlural = "Category_ Culture Infos";

            cmsInfo.TitleSingular =  "Category_ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Category_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Category_CultureInfo/";
			UsedInProject = Category_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
