using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ProductVariationSizeModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationSizeModule;
using BusinessLogic_v3.Frontend.ProductVariationSizeModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ProductVariationSizeBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductVariationSizeModule.ProductVariationSizeBase>
    {
		public ProductVariationSizeBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariationSizeModule.ProductVariationSizeBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ProductVariationSizeModule.ProductVariationSizeBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductVariationSizeBaseFrontend FrontendItem
        {
            get { return (ProductVariationSizeBaseFrontend)base.FrontendItem; }

        }
        public new ProductVariationSizeBase DbItem
        {
            get
            {
                return (ProductVariationSizeBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
