using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.OrderModule;
using CS.WebComponentsGeneralV3.Cms.OrderModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class OrderBaseCmsFactory_AutoGen : CmsFactoryBase<OrderBaseCmsInfo, OrderBase>
    {
       
       public new static OrderBaseCmsFactory Instance
	    {
	         get
	         {
                 return (OrderBaseCmsFactory)CmsFactoryBase<OrderBaseCmsInfo, OrderBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = OrderBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Order.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Order";

            this.QueryStringParamID = "OrderId";

            cmsInfo.TitlePlural = "Orders";

            cmsInfo.TitleSingular =  "Order";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public OrderBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Order/";
			UsedInProject = OrderBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
