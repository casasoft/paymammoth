using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.KeywordModule;
using CS.WebComponentsGeneralV3.Cms.KeywordModule;
using BusinessLogic_v3.Frontend.KeywordModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class KeywordBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.KeywordModule.KeywordBase>
    {
		public KeywordBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.KeywordModule.KeywordBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.KeywordModule.KeywordBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new KeywordBaseFrontend FrontendItem
        {
            get { return (KeywordBaseFrontend)base.FrontendItem; }

        }
        public new KeywordBase DbItem
        {
            get
            {
                return (KeywordBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Keyword { get; protected set; }

        public CmsPropertyInfo FrequencyCount { get; protected set; }

        public CmsPropertyInfo CultureInfo { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
