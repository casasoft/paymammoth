using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ShippingRatesGroupModule;
using CS.WebComponentsGeneralV3.Cms.ShippingRatesGroupModule;
using BusinessLogic_v3.Frontend.ShippingRatesGroupModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ShippingRatesGroupBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase>
    {
		public ShippingRatesGroupBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ShippingRatesGroupModule.ShippingRatesGroupBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ShippingRatesGroupBaseFrontend FrontendItem
        {
            get { return (ShippingRatesGroupBaseFrontend)base.FrontendItem; }

        }
        public new ShippingRatesGroupBase DbItem
        {
            get
            {
                return (ShippingRatesGroupBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo CountriesApplicable { get; protected set; }

        public CmsPropertyInfo ShippingMethod { get; protected set; }

        public CmsPropertyInfo AppliesToRestOfTheWorld { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ShippingRatePrices { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ShippingRatePrices = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase>.GetPropertyBySelector(item => item.ShippingRatePrices),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase>.GetPropertyBySelector(item => item.ShippingRatesGroup)));


			base.initBasicFields();
          
        }

    }
}
