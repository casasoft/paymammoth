using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.OrderItemModule;
using CS.WebComponentsGeneralV3.Cms.OrderItemModule;
using BusinessLogic_v3.Frontend.OrderItemModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class OrderItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>
    {
		public OrderItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.OrderItemModule.OrderItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new OrderItemBaseFrontend FrontendItem
        {
            get { return (OrderItemBaseFrontend)base.FrontendItem; }

        }
        public new OrderItemBase DbItem
        {
            get
            {
                return (OrderItemBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Quantity { get; protected set; }

        public CmsPropertyInfo PriceIncTaxPerUnit { get; protected set; }

        public CmsPropertyInfo ItemReference { get; protected set; }

        public CmsPropertyInfo Remarks { get; protected set; }

        public CmsPropertyInfo DiscountPerUnit { get; protected set; }

        public CmsPropertyInfo ItemID { get; protected set; }

        public CmsPropertyInfo ItemType { get; protected set; }

        public CmsPropertyInfo ShipmentWeight { get; protected set; }

        public CmsPropertyInfo Order { get; protected set; }

        public CmsPropertyInfo LinkedProductVariation { get; protected set; }

        public CmsPropertyInfo SubscriptionTypePrice { get; protected set; }

        public CmsPropertyInfo AutoRenewSubscriptionTypePrice { get; protected set; }

        public CmsPropertyInfo LinkedProductVariationColour { get; protected set; }

        public CmsPropertyInfo LinkedProductVariationSize { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo TaxRate { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
