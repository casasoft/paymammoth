using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using CS.WebComponentsGeneralV3.Cms.SpecialOfferModule;
using BusinessLogic_v3.Frontend.SpecialOfferModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class SpecialOfferBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>
    {
		public SpecialOfferBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.SpecialOfferModule.SpecialOfferBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new SpecialOfferBaseFrontend FrontendItem
        {
            get { return (SpecialOfferBaseFrontend)base.FrontendItem; }

        }
        public new SpecialOfferBase DbItem
        {
            get
            {
                return (SpecialOfferBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Name { get; protected set; }

        public CmsPropertyInfo DateFrom { get; protected set; }

        public CmsPropertyInfo DateTo { get; protected set; }

        public CmsPropertyInfo RequiredTotalItemsToBuy { get; protected set; }

        public CmsPropertyInfo RequiredTotalToSpend { get; protected set; }

        public CmsPropertyInfo DiscountPercentage { get; protected set; }

        public CmsPropertyInfo RequiresPromoCode { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo QuantityLeft { get; protected set; }

        public CmsPropertyInfo Activated { get; protected set; }

        public CmsPropertyInfo RequirementsCanMatchAnyItem { get; protected set; }

        public CmsPropertyInfo IsExclusiveOffer { get; protected set; }

        public CmsPropertyInfo IsNotLimitedByQuantity { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo SpecialOffer_CultureInfos { get; protected set; }
        

        public CmsCollectionInfo SpecialOfferVoucherCodes { get; protected set; }
        

        public CmsPropertyManyToManyCollection<CS.WebComponentsGeneralV3.Cms.CategoryModule.CategoryBaseCmsInfo> ApplicableCategories { get; protected set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.SpecialOffer_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector(item => item.SpecialOffer_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase>.GetPropertyBySelector(item => item.SpecialOffer)));
		
		//InitCollectionBaseOneToMany
		this.SpecialOfferVoucherCodes = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector(item => item.SpecialOfferVoucherCodes),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase>.GetPropertyBySelector(item => item.SpecialOffer)));
//InitFieldManyToManyBase_RightSide
		this.ApplicableCategories = this.AddManyToManyCollectionWithDirectProperty<CS.WebComponentsGeneralV3.Cms.CategoryModule.CategoryBaseCmsInfo>(
								item => item.ApplicableCategories, 
								CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
