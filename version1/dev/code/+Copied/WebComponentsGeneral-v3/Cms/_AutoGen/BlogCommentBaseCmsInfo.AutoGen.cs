using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.BlogCommentModule;
using BusinessLogic_v3.Cms.BlogCommentModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class BlogCommentBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.BlogCommentModule.BlogCommentBase>
    {
		public BlogCommentBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.BlogCommentModule.BlogCommentBase dbItem)
            : base(BusinessLogic_v3.Cms.BlogCommentModule.BlogCommentBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new BlogCommentBase DbItem
        {
            get
            {
                return (BlogCommentBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo FullName { get; private set; }

        public CmsPropertyInfo Email { get; private set; }

        public CmsPropertyInfo Text { get; private set; }

        public CmsPropertyInfo IPAddress { get; private set; }

        public CmsPropertyInfo CommentPostedDate { get; private set; }

        public CmsPropertyInfo ParentBlogPost { get; private set; }

        public CmsPropertyInfo Accepted { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.FullName = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogCommentModule.BlogCommentBase>.GetPropertyBySelector( item => item.FullName),
        			isEditable: true,
        			isVisible: true
        			);

        this.Email = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogCommentModule.BlogCommentBase>.GetPropertyBySelector( item => item.Email),
        			isEditable: true,
        			isVisible: true
        			);

        this.Text = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogCommentModule.BlogCommentBase>.GetPropertyBySelector( item => item.Text),
        			isEditable: true,
        			isVisible: true
        			);

        this.IPAddress = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogCommentModule.BlogCommentBase>.GetPropertyBySelector( item => item.IPAddress),
        			isEditable: true,
        			isVisible: true
        			);

        this.CommentPostedDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogCommentModule.BlogCommentBase>.GetPropertyBySelector( item => item.CommentPostedDate),
        			isEditable: true,
        			isVisible: true
        			);

        this.ParentBlogPost = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogCommentModule.BlogCommentBase>.GetPropertyBySelector( item => item.ParentBlogPost),
        			isEditable: true,
        			isVisible: true
        			);

        this.Accepted = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogCommentModule.BlogCommentBase>.GetPropertyBySelector( item => item.Accepted),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
