using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.EventModule;
using CS.WebComponentsGeneralV3.Cms.EventModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class EventBaseCmsFactory_AutoGen : CmsFactoryBase<EventBaseCmsInfo, EventBase>
    {
       
       public new static EventBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventBaseCmsFactory)CmsFactoryBase<EventBaseCmsInfo, EventBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = EventBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Event.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Event";

            this.QueryStringParamID = "EventId";

            cmsInfo.TitlePlural = "Events";

            cmsInfo.TitleSingular =  "Event";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Event/";
			UsedInProject = EventBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
