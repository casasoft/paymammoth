using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.AdvertHitModule;
using CS.WebComponentsGeneralV3.Cms.AdvertHitModule;
using BusinessLogic_v3.Frontend.AdvertHitModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class AdvertHitBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase>
    {
		public AdvertHitBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.AdvertHitModule.AdvertHitBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AdvertHitBaseFrontend FrontendItem
        {
            get { return (AdvertHitBaseFrontend)base.FrontendItem; }

        }
        public new AdvertHitBase DbItem
        {
            get
            {
                return (AdvertHitBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo HitType { get; protected set; }

        public CmsPropertyInfo UserIP { get; protected set; }

        public CmsPropertyInfo DateTime { get; protected set; }

        public CmsPropertyInfo Advert { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
