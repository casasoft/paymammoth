using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.MemberReferralModule;
using CS.WebComponentsGeneralV3.Cms.MemberReferralModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class MemberReferralBaseCmsFactory_AutoGen : CmsFactoryBase<MemberReferralBaseCmsInfo, MemberReferralBase>
    {
       
       public new static MemberReferralBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberReferralBaseCmsFactory)CmsFactoryBase<MemberReferralBaseCmsInfo, MemberReferralBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = MemberReferralBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberReferral.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberReferral";

            this.QueryStringParamID = "MemberReferralId";

            cmsInfo.TitlePlural = "Member Referrals";

            cmsInfo.TitleSingular =  "Member Referral";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberReferralBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "MemberReferral/";
			UsedInProject = MemberReferralBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
