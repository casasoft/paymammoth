using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.MemberModule;
using CS.WebComponentsGeneralV3.Cms.MemberModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class MemberBaseCmsFactory_AutoGen : CmsFactoryBase<MemberBaseCmsInfo, MemberBase>
    {
       
       public new static MemberBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberBaseCmsFactory)CmsFactoryBase<MemberBaseCmsInfo, MemberBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = MemberBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Member.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Member";

            this.QueryStringParamID = "MemberId";

            cmsInfo.TitlePlural = "Members";

            cmsInfo.TitleSingular =  "Member";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Member/";
			UsedInProject = MemberBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
