using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ContactFormModule;
using CS.WebComponentsGeneralV3.Cms.ContactFormModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ContactFormBaseCmsFactory_AutoGen : CmsFactoryBase<ContactFormBaseCmsInfo, ContactFormBase>
    {
       
       public new static ContactFormBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContactFormBaseCmsFactory)CmsFactoryBase<ContactFormBaseCmsInfo, ContactFormBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ContactFormBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContactForm.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContactForm";

            this.QueryStringParamID = "ContactFormId";

            cmsInfo.TitlePlural = "Contact Forms";

            cmsInfo.TitleSingular =  "Contact Form";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContactFormBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ContactForm/";
			UsedInProject = ContactFormBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
