using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ArticleRatingModule;
using CS.WebComponentsGeneralV3.Cms.ArticleRatingModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ArticleRatingBaseCmsFactory_AutoGen : CmsFactoryBase<ArticleRatingBaseCmsInfo, ArticleRatingBase>
    {
       
       public new static ArticleRatingBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ArticleRatingBaseCmsFactory)CmsFactoryBase<ArticleRatingBaseCmsInfo, ArticleRatingBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ArticleRatingBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ArticleRating.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ArticleRating";

            this.QueryStringParamID = "ArticleRatingId";

            cmsInfo.TitlePlural = "Article Ratings";

            cmsInfo.TitleSingular =  "Article Rating";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ArticleRatingBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ArticleRating/";
			UsedInProject = ArticleRatingBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
