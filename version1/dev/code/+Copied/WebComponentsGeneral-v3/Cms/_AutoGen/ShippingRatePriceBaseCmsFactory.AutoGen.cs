using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ShippingRatePriceModule;
using CS.WebComponentsGeneralV3.Cms.ShippingRatePriceModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ShippingRatePriceBaseCmsFactory_AutoGen : CmsFactoryBase<ShippingRatePriceBaseCmsInfo, ShippingRatePriceBase>
    {
       
       public new static ShippingRatePriceBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ShippingRatePriceBaseCmsFactory)CmsFactoryBase<ShippingRatePriceBaseCmsInfo, ShippingRatePriceBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ShippingRatePriceBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ShippingRatePrice.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ShippingRatePrice";

            this.QueryStringParamID = "ShippingRatePriceId";

            cmsInfo.TitlePlural = "Shipping Rate Prices";

            cmsInfo.TitleSingular =  "Shipping Rate Price";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ShippingRatePriceBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ShippingRatePrice/";
			UsedInProject = ShippingRatePriceBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
