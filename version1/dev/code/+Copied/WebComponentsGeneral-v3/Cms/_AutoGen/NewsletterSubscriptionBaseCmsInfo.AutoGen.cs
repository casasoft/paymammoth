using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.NewsletterSubscriptionModule;
using CS.WebComponentsGeneralV3.Cms.NewsletterSubscriptionModule;
using BusinessLogic_v3.Frontend.NewsletterSubscriptionModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class NewsletterSubscriptionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase>
    {
		public NewsletterSubscriptionBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.NewsletterSubscriptionModule.NewsletterSubscriptionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new NewsletterSubscriptionBaseFrontend FrontendItem
        {
            get { return (NewsletterSubscriptionBaseFrontend)base.FrontendItem; }

        }
        public new NewsletterSubscriptionBase DbItem
        {
            get
            {
                return (NewsletterSubscriptionBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Email { get; protected set; }

        public CmsPropertyInfo Name { get; protected set; }

        public CmsPropertyInfo Telephone { get; protected set; }

        public CmsPropertyInfo DateAdded { get; protected set; }

        public CmsPropertyInfo Enabled { get; protected set; }

        public CmsPropertyInfo RegisteredIPAddress { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
