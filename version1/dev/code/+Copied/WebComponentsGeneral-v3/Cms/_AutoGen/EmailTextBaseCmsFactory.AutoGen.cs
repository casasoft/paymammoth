using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.EmailTextModule;
using CS.WebComponentsGeneralV3.Cms.EmailTextModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class EmailTextBaseCmsFactory_AutoGen : CmsFactoryBase<EmailTextBaseCmsInfo, EmailTextBase>
    {
       
       public new static EmailTextBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EmailTextBaseCmsFactory)CmsFactoryBase<EmailTextBaseCmsInfo, EmailTextBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = EmailTextBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EmailText.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EmailText";

            this.QueryStringParamID = "EmailTextId";

            cmsInfo.TitlePlural = "Email Texts";

            cmsInfo.TitleSingular =  "Email Text";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EmailTextBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "EmailText/";
			UsedInProject = EmailTextBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
