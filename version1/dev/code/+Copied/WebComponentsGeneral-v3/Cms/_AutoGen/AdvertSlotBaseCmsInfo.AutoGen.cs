using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.AdvertSlotModule;
using CS.WebComponentsGeneralV3.Cms.AdvertSlotModule;
using BusinessLogic_v3.Frontend.AdvertSlotModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class AdvertSlotBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>
    {
		public AdvertSlotBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.AdvertSlotModule.AdvertSlotBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AdvertSlotBaseFrontend FrontendItem
        {
            get { return (AdvertSlotBaseFrontend)base.FrontendItem; }

        }
        public new AdvertSlotBase DbItem
        {
            get
            {
                return (AdvertSlotBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo AdvertColumn { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo RoundNo { get; protected set; }

        public CmsPropertyInfo Width { get; protected set; }

        public CmsPropertyInfo Height { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo Adverts { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.Adverts = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>.GetPropertyBySelector(item => item.Adverts),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector(item => item.AdvertSlot)));


			base.initBasicFields();
          
        }

    }
}
