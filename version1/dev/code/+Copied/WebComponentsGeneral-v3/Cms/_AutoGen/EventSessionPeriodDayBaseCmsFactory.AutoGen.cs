using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.EventSessionPeriodDayModule;
using CS.WebComponentsGeneralV3.Cms.EventSessionPeriodDayModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class EventSessionPeriodDayBaseCmsFactory_AutoGen : CmsFactoryBase<EventSessionPeriodDayBaseCmsInfo, EventSessionPeriodDayBase>
    {
       
       public new static EventSessionPeriodDayBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventSessionPeriodDayBaseCmsFactory)CmsFactoryBase<EventSessionPeriodDayBaseCmsInfo, EventSessionPeriodDayBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = EventSessionPeriodDayBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EventSessionPeriodDay.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EventSessionPeriodDay";

            this.QueryStringParamID = "EventSessionPeriodDayId";

            cmsInfo.TitlePlural = "Event Session Period Daies";

            cmsInfo.TitleSingular =  "Event Session Period Day";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventSessionPeriodDayBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "EventSessionPeriodDay/";
			UsedInProject = EventSessionPeriodDayBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
