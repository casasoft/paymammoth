using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.MeasurementUnitModule;
using CS.WebComponentsGeneralV3.Cms.MeasurementUnitModule;
using BusinessLogic_v3.Frontend.MeasurementUnitModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class MeasurementUnitBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase>
    {
		public MeasurementUnitBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.MeasurementUnitModule.MeasurementUnitBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MeasurementUnitBaseFrontend FrontendItem
        {
            get { return (MeasurementUnitBaseFrontend)base.FrontendItem; }

        }
        public new MeasurementUnitBase DbItem
        {
            get
            {
                return (MeasurementUnitBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo CategorySpecifications { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.CategorySpecifications = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase>.GetPropertyBySelector(item => item.CategorySpecifications),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>.GetPropertyBySelector(item => item.MeasurementUnit)));


			base.initBasicFields();
          
        }

    }
}
