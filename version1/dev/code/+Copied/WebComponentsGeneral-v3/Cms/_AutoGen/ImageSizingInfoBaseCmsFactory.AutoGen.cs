using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
using CS.WebComponentsGeneralV3.Cms.ImageSizingInfoModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ImageSizingInfoBaseCmsFactory_AutoGen : CmsFactoryBase<ImageSizingInfoBaseCmsInfo, ImageSizingInfoBase>
    {
       
       public new static ImageSizingInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ImageSizingInfoBaseCmsFactory)CmsFactoryBase<ImageSizingInfoBaseCmsInfo, ImageSizingInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ImageSizingInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ImageSizingInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ImageSizingInfo";

            this.QueryStringParamID = "ImageSizingInfoId";

            cmsInfo.TitlePlural = "Image Sizing Infos";

            cmsInfo.TitleSingular =  "Image Sizing Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ImageSizingInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ImageSizingInfo/";
			UsedInProject = ImageSizingInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
