using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ContentTextModule;
using CS.WebComponentsGeneralV3.Cms.ContentTextModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ContentTextBaseCmsFactory_AutoGen : CmsFactoryBase<ContentTextBaseCmsInfo, ContentTextBase>
    {
       
       public new static ContentTextBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContentTextBaseCmsFactory)CmsFactoryBase<ContentTextBaseCmsInfo, ContentTextBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ContentTextBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContentText.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContentText";

            this.QueryStringParamID = "ContentTextId";

            cmsInfo.TitlePlural = "Content Texts";

            cmsInfo.TitleSingular =  "Content Text";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContentTextBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ContentText/";
			UsedInProject = ContentTextBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
