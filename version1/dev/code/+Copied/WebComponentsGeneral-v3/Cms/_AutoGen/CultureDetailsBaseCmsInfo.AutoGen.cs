using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using CS.WebComponentsGeneralV3.Cms.CultureDetailsModule;
using BusinessLogic_v3.Frontend.CultureDetailsModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class CultureDetailsBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>
    {
		public CultureDetailsBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.CultureDetailsModule.CultureDetailsBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CultureDetailsBaseFrontend FrontendItem
        {
            get { return (CultureDetailsBaseFrontend)base.FrontendItem; }

        }
        public new CultureDetailsBase DbItem
        {
            get
            {
                return (CultureDetailsBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo LanguageISOCode { get; protected set; }

        public CmsPropertyInfo DefaultCurrency { get; protected set; }

        public CmsPropertyInfo SpecificCountryCode { get; protected set; }

        public CmsPropertyInfo ScriptSuffix { get; protected set; }

        public CmsPropertyInfo IsDefaultCulture { get; protected set; }

        public CmsPropertyInfo BaseUrlRegex { get; protected set; }

        public CmsPropertyInfo BaseRedirectionUrl { get; protected set; }

        public CmsPropertyInfo GoogleAnalyticsRootDomain { get; protected set; }

        public CmsPropertyInfo GoogleAnalyticsID { get; protected set; }

        public CmsPropertyInfo BaseRedirectionUrlLocalhost { get; protected set; }

        public CmsPropertyInfo BaseUrlRegexLocalhost { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsPropertyManyToManyCollection<CS.WebComponentsGeneralV3.Cms.BannerModule.BannerBaseCmsInfo> Banners { get; protected set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

//InitFieldManyToManyBase_RightSide
		this.Banners = this.AddManyToManyCollectionWithDirectProperty<CS.WebComponentsGeneralV3.Cms.BannerModule.BannerBaseCmsInfo>(
								item => item.Banners, 
								CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
