using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.EventSubmissionModule;
using CS.WebComponentsGeneralV3.Cms.EventSubmissionModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class EventSubmissionBaseCmsFactory_AutoGen : CmsFactoryBase<EventSubmissionBaseCmsInfo, EventSubmissionBase>
    {
       
       public new static EventSubmissionBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventSubmissionBaseCmsFactory)CmsFactoryBase<EventSubmissionBaseCmsInfo, EventSubmissionBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = EventSubmissionBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EventSubmission.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EventSubmission";

            this.QueryStringParamID = "EventSubmissionId";

            cmsInfo.TitlePlural = "Event Submissions";

            cmsInfo.TitleSingular =  "Event Submission";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventSubmissionBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "EventSubmission/";
			UsedInProject = EventSubmissionBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
