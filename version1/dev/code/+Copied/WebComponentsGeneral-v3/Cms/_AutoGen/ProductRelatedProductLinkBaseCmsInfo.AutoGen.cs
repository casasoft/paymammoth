using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ProductRelatedProductLinkModule;
using CS.WebComponentsGeneralV3.Cms.ProductRelatedProductLinkModule;
using BusinessLogic_v3.Frontend.ProductRelatedProductLinkModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ProductRelatedProductLinkBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.ProductRelatedProductLinkBase>
    {
		public ProductRelatedProductLinkBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.ProductRelatedProductLinkBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ProductRelatedProductLinkModule.ProductRelatedProductLinkBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductRelatedProductLinkBaseFrontend FrontendItem
        {
            get { return (ProductRelatedProductLinkBaseFrontend)base.FrontendItem; }

        }
        public new ProductRelatedProductLinkBase DbItem
        {
            get
            {
                return (ProductRelatedProductLinkBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Product { get; protected set; }

        public CmsPropertyInfo RelatedProduct { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
