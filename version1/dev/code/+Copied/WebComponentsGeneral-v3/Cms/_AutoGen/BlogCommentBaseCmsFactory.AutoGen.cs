using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.BlogCommentModule;
using BusinessLogic_v3.Cms.BlogCommentModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class BlogCommentBaseCmsFactory_AutoGen : CmsFactoryBase<BlogCommentBaseCmsInfo, BlogCommentBase>
    {
       
       public new static BlogCommentBaseCmsFactory Instance
	    {
	         get
	         {
                 return (BlogCommentBaseCmsFactory)CmsFactoryBase<BlogCommentBaseCmsInfo, BlogCommentBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = BlogCommentBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "BlogComment.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "BlogComment";

            this.QueryStringParamID = "BlogCommentId";

            cmsInfo.TitlePlural = "Blog Comments";

            cmsInfo.TitleSingular =  "Blog Comment";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public BlogCommentBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "BlogComment/";


        }
       
    }

}
