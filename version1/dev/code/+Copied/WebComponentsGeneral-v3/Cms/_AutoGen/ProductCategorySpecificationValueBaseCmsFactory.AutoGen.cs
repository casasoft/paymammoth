using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;
using CS.WebComponentsGeneralV3.Cms.ProductCategorySpecificationValueModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductCategorySpecificationValueBaseCmsFactory_AutoGen : CmsFactoryBase<ProductCategorySpecificationValueBaseCmsInfo, ProductCategorySpecificationValueBase>
    {
       
       public new static ProductCategorySpecificationValueBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductCategorySpecificationValueBaseCmsFactory)CmsFactoryBase<ProductCategorySpecificationValueBaseCmsInfo, ProductCategorySpecificationValueBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ProductCategorySpecificationValueBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductCategorySpecificationValue.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductCategorySpecificationValue";

            this.QueryStringParamID = "ProductCategorySpecificationValueId";

            cmsInfo.TitlePlural = "Product Category Specification Values";

            cmsInfo.TitleSingular =  "Product Category Specification Value";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductCategorySpecificationValueBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductCategorySpecificationValue/";
			UsedInProject = ProductCategorySpecificationValueBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
