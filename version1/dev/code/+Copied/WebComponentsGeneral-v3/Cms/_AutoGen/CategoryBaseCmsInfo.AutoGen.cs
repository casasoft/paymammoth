using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.CategoryModule;
using CS.WebComponentsGeneralV3.Cms.CategoryModule;
using BusinessLogic_v3.Frontend.CategoryModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class CategoryBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>
    {
		public CategoryBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CategoryModule.CategoryBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.CategoryModule.CategoryBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CategoryBaseFrontend FrontendItem
        {
            get { return (CategoryBaseFrontend)base.FrontendItem; }

        }
        public new CategoryBase DbItem
        {
            get
            {
                return (CategoryBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo TitleSingular { get; protected set; }

        public CmsPropertyInfo TitleSingular_Search { get; protected set; }

        public CmsPropertyInfo TitlePlural { get; protected set; }

        public CmsPropertyInfo TitlePlural_Search { get; protected set; }

        public CmsPropertyInfo TitleSEO { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo MetaKeywords { get; protected set; }

        public CmsPropertyInfo MetaDescription { get; protected set; }

        public CmsPropertyInfo CanDelete { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo ComingSoon { get; protected set; }

        public CmsPropertyInfo PriorityAll { get; protected set; }

        public CmsPropertyInfo Brand { get; protected set; }

        public CmsPropertyInfo ImportReference { get; protected set; }

        public CmsPropertyInfo DontShowInNavigation { get; protected set; }

        public CmsPropertyInfo IconFilename { get; protected set; }

        public CmsPropertyInfo OmitChildrenInNavigation { get; protected set; }

        public CmsPropertyInfo MetaTitle { get; protected set; }

        public CmsPropertyInfo ConsiderAsRootNode { get; protected set; }

        public CmsPropertyInfo SubTitle { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo BackgroundImages { get; protected set; }
        

        public CmsCollectionInfo Category_CultureInfos { get; protected set; }
        

        public CmsCollectionInfo ChildCategoryLinks { get; protected set; }
        

        public CmsCollectionInfo ParentCategoryLinks { get; protected set; }
        

        public CmsCollectionInfo CategoryFeatures { get; protected set; }
        

        public CmsCollectionInfo CategoryImages { get; protected set; }
        

        public CmsCollectionInfo CategorySpecifications { get; protected set; }
        

        public CmsCollectionInfo Classifieds { get; protected set; }
        

        public CmsCollectionInfo Events { get; protected set; }
        

        public CmsCollectionInfo EventBasics { get; protected set; }
        

        public CmsCollectionInfo EventSubmissions { get; protected set; }
        

        public CmsCollectionInfo ProductLinks { get; protected set; }
        

        public CmsPropertyManyToManyCollection<CS.WebComponentsGeneralV3.Cms.MemberModule.MemberBaseCmsInfo> SubscribedMembers { get; protected set; }
        public CmsPropertyManyToManyCollection<CS.WebComponentsGeneralV3.Cms.SpecialOfferModule.SpecialOfferBaseCmsInfo> ApplicableSpecialOffers { get; protected set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.BackgroundImages = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.BackgroundImages),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BackgroundImageModule.BackgroundImageBase>.GetPropertyBySelector(item => item.CategoryLink)));
		
		//InitCollectionBaseOneToMany
		this.Category_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.Category_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.ChildCategoryLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.ChildCategoryLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>.GetPropertyBySelector(item => item.ParentCategory)));
		
		//InitCollectionBaseOneToMany
		this.ParentCategoryLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.ParentCategoryLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>.GetPropertyBySelector(item => item.ChildCategory)));
		
		//InitCollectionBaseOneToMany
		this.CategoryFeatures = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.CategoryFeatures),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.CategoryImages = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.CategoryImages),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.CategorySpecifications = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.CategorySpecifications),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.Classifieds = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.Classifieds),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.Events = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.Events),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.EventBasics = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.EventBasics),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.EventSubmissions = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.EventSubmissions),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.ProductLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.ProductLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase>.GetPropertyBySelector(item => item.Category)));
//InitFieldManyToManyBase_RightSide
		this.SubscribedMembers = this.AddManyToManyCollectionWithDirectProperty<CS.WebComponentsGeneralV3.Cms.MemberModule.MemberBaseCmsInfo>(
								item => item.SubscribedMembers, 
								CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);
//InitFieldManyToManyBase_RightSide
		this.ApplicableSpecialOffers = this.AddManyToManyCollectionWithDirectProperty<CS.WebComponentsGeneralV3.Cms.SpecialOfferModule.SpecialOfferBaseCmsInfo>(
								item => item.ApplicableSpecialOffers, 
								CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
