using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.BannerModule;
using CS.WebComponentsGeneralV3.Cms.BannerModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class BannerBaseCmsFactory_AutoGen : CmsFactoryBase<BannerBaseCmsInfo, BannerBase>
    {
       
       public new static BannerBaseCmsFactory Instance
	    {
	         get
	         {
                 return (BannerBaseCmsFactory)CmsFactoryBase<BannerBaseCmsInfo, BannerBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = BannerBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Banner.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Banner";

            this.QueryStringParamID = "BannerId";

            cmsInfo.TitlePlural = "Banners";

            cmsInfo.TitleSingular =  "Banner";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public BannerBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Banner/";
			UsedInProject = BannerBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
