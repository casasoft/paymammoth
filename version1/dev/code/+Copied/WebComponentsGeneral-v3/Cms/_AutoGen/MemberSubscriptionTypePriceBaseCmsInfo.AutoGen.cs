using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;
using CS.WebComponentsGeneralV3.Cms.MemberSubscriptionTypePriceModule;
using BusinessLogic_v3.Frontend.MemberSubscriptionTypePriceModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class MemberSubscriptionTypePriceBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>
    {
		public MemberSubscriptionTypePriceBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberSubscriptionTypePriceBaseFrontend FrontendItem
        {
            get { return (MemberSubscriptionTypePriceBaseFrontend)base.FrontendItem; }

        }
        public new MemberSubscriptionTypePriceBase DbItem
        {
            get
            {
                return (MemberSubscriptionTypePriceBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DurationInDays { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo MemberSubscriptionType { get; protected set; }

        public CmsPropertyInfo Notification1BeforeInDays { get; protected set; }

        public CmsPropertyInfo Notification2BeforeInDays { get; protected set; }

        public CmsPropertyInfo Price { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo MemberSubscriptionLinks { get; protected set; }
        

        public CmsCollectionInfo OrderItems { get; protected set; }
        

        public CmsCollectionInfo ShoppingCartItems { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.MemberSubscriptionLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.GetPropertyBySelector(item => item.MemberSubscriptionLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase>.GetPropertyBySelector(item => item.SubscriptionTypePrice)));
		
		//InitCollectionBaseOneToMany
		this.OrderItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.GetPropertyBySelector(item => item.OrderItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector(item => item.SubscriptionTypePrice)));
		
		//InitCollectionBaseOneToMany
		this.ShoppingCartItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.GetPropertyBySelector(item => item.ShoppingCartItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>.GetPropertyBySelector(item => item.LinkedSubscriptionTypePrice)));


			base.initBasicFields();
          
        }

    }
}
