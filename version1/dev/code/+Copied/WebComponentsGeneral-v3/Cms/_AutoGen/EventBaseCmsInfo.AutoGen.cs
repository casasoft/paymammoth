using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.EventModule;
using CS.WebComponentsGeneralV3.Cms.EventModule;
using BusinessLogic_v3.Frontend.EventModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class EventBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventModule.EventBase>
    {
		public EventBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventModule.EventBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.EventModule.EventBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventBaseFrontend FrontendItem
        {
            get { return (EventBaseFrontend)base.FrontendItem; }

        }
        public new EventBase DbItem
        {
            get
            {
                return (EventBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo StartDate { get; protected set; }

        public CmsPropertyInfo EndDate { get; protected set; }

        public CmsPropertyInfo ImageFilename { get; protected set; }

        public CmsPropertyInfo Category { get; protected set; }

        public CmsPropertyInfo SubTitle { get; protected set; }

        public CmsPropertyInfo Price { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo Sessions { get; protected set; }

        public CmsPropertyInfo ClassSize { get; protected set; }

        public CmsPropertyInfo Dates { get; protected set; }

        public CmsPropertyInfo Duration { get; protected set; }

        public CmsPropertyInfo ShortDescription { get; protected set; }

        public CmsPropertyInfo Location { get; protected set; }

        public CmsPropertyInfo Summary { get; protected set; }

        public CmsPropertyInfo Time { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ContactForms { get; protected set; }
        

        public CmsCollectionInfo EventMediaItems { get; protected set; }
        

        public CmsCollectionInfo EventSessions { get; protected set; }
        

        public CmsCollectionInfo FileItems { get; protected set; }
        

        public CmsCollectionInfo Testimonials { get; protected set; }
        

        public CmsPropertyManyToManyCollection<CS.WebComponentsGeneralV3.Cms.MemberModule.MemberBaseCmsInfo> Members { get; protected set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ContactForms = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector(item => item.ContactForms),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector(item => item.Event)));
		
		//InitCollectionBaseOneToMany
		this.EventMediaItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector(item => item.EventMediaItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>.GetPropertyBySelector(item => item.Event)));
		
		//InitCollectionBaseOneToMany
		this.EventSessions = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector(item => item.EventSessions),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector(item => item.Event)));
		
		//InitCollectionBaseOneToMany
		this.FileItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector(item => item.FileItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FileItemModule.FileItemBase>.GetPropertyBySelector(item => item.Event)));
		
		//InitCollectionBaseOneToMany
		this.Testimonials = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector(item => item.Testimonials),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TestimonialModule.TestimonialBase>.GetPropertyBySelector(item => item.Event)));
//InitFieldManyToManyBase_RightSide
		this.Members = this.AddManyToManyCollectionWithDirectProperty<CS.WebComponentsGeneralV3.Cms.MemberModule.MemberBaseCmsInfo>(
								item => item.Members, 
								CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
