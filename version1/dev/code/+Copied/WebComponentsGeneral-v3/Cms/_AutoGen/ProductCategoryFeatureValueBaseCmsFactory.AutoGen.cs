using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule;
using CS.WebComponentsGeneralV3.Cms.ProductCategoryFeatureValueModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductCategoryFeatureValueBaseCmsFactory_AutoGen : CmsFactoryBase<ProductCategoryFeatureValueBaseCmsInfo, ProductCategoryFeatureValueBase>
    {
       
       public new static ProductCategoryFeatureValueBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductCategoryFeatureValueBaseCmsFactory)CmsFactoryBase<ProductCategoryFeatureValueBaseCmsInfo, ProductCategoryFeatureValueBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ProductCategoryFeatureValueBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductCategoryFeatureValue.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductCategoryFeatureValue";

            this.QueryStringParamID = "ProductCategoryFeatureValueId";

            cmsInfo.TitlePlural = "Product Category Feature Values";

            cmsInfo.TitleSingular =  "Product Category Feature Value";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductCategoryFeatureValueBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductCategoryFeatureValue/";
			UsedInProject = ProductCategoryFeatureValueBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
