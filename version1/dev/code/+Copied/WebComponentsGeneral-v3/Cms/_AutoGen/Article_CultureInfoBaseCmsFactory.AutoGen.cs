using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.Article_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Article_CultureInfoModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class Article_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<Article_CultureInfoBaseCmsInfo, Article_CultureInfoBase>
    {
       
       public new static Article_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Article_CultureInfoBaseCmsFactory)CmsFactoryBase<Article_CultureInfoBaseCmsInfo, Article_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = Article_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Article_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Article_CultureInfo";

            this.QueryStringParamID = "Article_CultureInfoId";

            cmsInfo.TitlePlural = "Article_ Culture Infos";

            cmsInfo.TitleSingular =  "Article_ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Article_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Article_CultureInfo/";
			UsedInProject = Article_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
