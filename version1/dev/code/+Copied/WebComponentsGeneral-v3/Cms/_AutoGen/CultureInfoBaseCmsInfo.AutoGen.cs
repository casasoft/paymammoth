using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.CultureInfoModule;
using BusinessLogic_v3.Frontend.CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CultureInfoModule.CultureInfoBase>
    {
		public CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CultureInfoModule.CultureInfoBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.CultureInfoModule.CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CultureInfoBaseFrontend FrontendItem
        {
            get { return (CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new CultureInfoBase DbItem
        {
            get
            {
                return (CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
