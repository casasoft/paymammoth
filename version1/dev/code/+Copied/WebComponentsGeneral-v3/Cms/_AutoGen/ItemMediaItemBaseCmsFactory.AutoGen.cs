using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ItemMediaItemModule;
using BusinessLogic_v3.Cms.ItemMediaItemModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ItemMediaItemBaseCmsFactory_AutoGen : CmsFactoryBase<ItemMediaItemBaseCmsInfo, ItemMediaItemBase>
    {
       
       public new static ItemMediaItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ItemMediaItemBaseCmsFactory)CmsFactoryBase<ItemMediaItemBaseCmsInfo, ItemMediaItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ItemMediaItemBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ItemMediaItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ItemMediaItem";

            this.QueryStringParamID = "ItemMediaItemId";

            cmsInfo.TitlePlural = "Item Media Items";

            cmsInfo.TitleSingular =  "Item Media Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ItemMediaItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ItemMediaItem/";


        }
       
    }

}
