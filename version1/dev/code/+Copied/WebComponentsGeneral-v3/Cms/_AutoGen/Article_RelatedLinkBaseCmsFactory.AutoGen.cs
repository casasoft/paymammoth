using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.Article_RelatedLinkModule;
using CS.WebComponentsGeneralV3.Cms.Article_RelatedLinkModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class Article_RelatedLinkBaseCmsFactory_AutoGen : CmsFactoryBase<Article_RelatedLinkBaseCmsInfo, Article_RelatedLinkBase>
    {
       
       public new static Article_RelatedLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Article_RelatedLinkBaseCmsFactory)CmsFactoryBase<Article_RelatedLinkBaseCmsInfo, Article_RelatedLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = Article_RelatedLinkBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Article_RelatedLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Article_RelatedLink";

            this.QueryStringParamID = "Article_RelatedLinkId";

            cmsInfo.TitlePlural = "Article_ Related Links";

            cmsInfo.TitleSingular =  "Article_ Related Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Article_RelatedLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Article_RelatedLink/";
			UsedInProject = Article_RelatedLinkBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
