using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ItemGroupCategoryModule;
using BusinessLogic_v3.Cms.ItemGroupCategoryModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ItemGroupCategoryBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ItemGroupCategoryModule.ItemGroupCategoryBase>
    {
		public ItemGroupCategoryBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ItemGroupCategoryModule.ItemGroupCategoryBase dbItem)
            : base(BusinessLogic_v3.Cms.ItemGroupCategoryModule.ItemGroupCategoryBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ItemGroupCategoryBase DbItem
        {
            get
            {
                return (ItemGroupCategoryBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ItemGroup { get; private set; }

        public CmsPropertyInfo Category { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.ItemGroup = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupCategoryModule.ItemGroupCategoryBase>.GetPropertyBySelector( item => item.ItemGroup),
        			isEditable: true,
        			isVisible: true
        			);

        this.Category = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupCategoryModule.ItemGroupCategoryBase>.GetPropertyBySelector( item => item.Category),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
