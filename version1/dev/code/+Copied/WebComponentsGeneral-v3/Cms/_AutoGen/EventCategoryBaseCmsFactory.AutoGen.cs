using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.EventCategoryModule;
using CS.WebComponentsGeneralV3.Cms.EventCategoryModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class EventCategoryBaseCmsFactory_AutoGen : CmsFactoryBase<EventCategoryBaseCmsInfo, EventCategoryBase>
    {
       
       public new static EventCategoryBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventCategoryBaseCmsFactory)CmsFactoryBase<EventCategoryBaseCmsInfo, EventCategoryBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	qParams.FillDefaultsForCms();
        	
            var q = EventCategoryBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EventCategory.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EventCategory";

            this.QueryStringParamID = "EventCategoryId";

            cmsInfo.TitlePlural = "Event Categories";

            cmsInfo.TitleSingular =  "Event Category";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventCategoryBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "EventCategory/";
			UsedInProject = EventCategoryBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
