using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductRelatedProductsModule;
using CS.WebComponentsGeneralV3.Cms.ProductRelatedProductsModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductRelatedProductsBaseCmsFactory_AutoGen : CmsFactoryBase<ProductRelatedProductsBaseCmsInfo, ProductRelatedProductsBase>
    {
       
       public new static ProductRelatedProductsBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductRelatedProductsBaseCmsFactory)CmsFactoryBase<ProductRelatedProductsBaseCmsInfo, ProductRelatedProductsBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	qParams.FillDefaultsForCms();
        	
            var q = ProductRelatedProductsBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductRelatedProducts.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductRelatedProducts";

            this.QueryStringParamID = "ProductRelatedProductsId";

            cmsInfo.TitlePlural = "Product Related Products";

            cmsInfo.TitleSingular =  "Product Related Products";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductRelatedProductsBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductRelatedProducts/";
			UsedInProject = ProductRelatedProductsBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
