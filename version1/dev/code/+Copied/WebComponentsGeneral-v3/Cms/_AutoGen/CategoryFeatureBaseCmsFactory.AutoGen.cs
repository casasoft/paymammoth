using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.CategoryFeatureModule;
using CS.WebComponentsGeneralV3.Cms.CategoryFeatureModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class CategoryFeatureBaseCmsFactory_AutoGen : CmsFactoryBase<CategoryFeatureBaseCmsInfo, CategoryFeatureBase>
    {
       
       public new static CategoryFeatureBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CategoryFeatureBaseCmsFactory)CmsFactoryBase<CategoryFeatureBaseCmsInfo, CategoryFeatureBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = CategoryFeatureBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CategoryFeature.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CategoryFeature";

            this.QueryStringParamID = "CategoryFeatureId";

            cmsInfo.TitlePlural = "Category Features";

            cmsInfo.TitleSingular =  "Category Feature";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CategoryFeatureBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "CategoryFeature/";
			UsedInProject = CategoryFeatureBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
