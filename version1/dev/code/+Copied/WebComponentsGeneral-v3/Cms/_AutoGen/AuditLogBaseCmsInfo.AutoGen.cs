using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.AuditLogModule;
using CS.WebComponentsGeneralV3.Cms.AuditLogModule;
using BusinessLogic_v3.Frontend.AuditLogModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class AuditLogBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AuditLogModule.AuditLogBase>
    {
		public AuditLogBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AuditLogModule.AuditLogBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.AuditLogModule.AuditLogBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AuditLogBaseFrontend FrontendItem
        {
            get { return (AuditLogBaseFrontend)base.FrontendItem; }

        }
        public new AuditLogBase DbItem
        {
            get
            {
                return (AuditLogBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ItemType { get; protected set; }

        public CmsPropertyInfo ItemID { get; protected set; }

        public CmsPropertyInfo CmsUser { get; protected set; }

        public CmsPropertyInfo CmsUserIpAddress { get; protected set; }

        public CmsPropertyInfo DateTime { get; protected set; }

        public CmsPropertyInfo StackTrace { get; protected set; }

        public CmsPropertyInfo Method { get; protected set; }

        public CmsPropertyInfo Changes { get; protected set; }

        public CmsPropertyInfo Remarks { get; protected set; }

        public CmsPropertyInfo Message { get; protected set; }

        public CmsPropertyInfo ItemTypeFull { get; protected set; }

        public CmsPropertyInfo UpdateType { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
