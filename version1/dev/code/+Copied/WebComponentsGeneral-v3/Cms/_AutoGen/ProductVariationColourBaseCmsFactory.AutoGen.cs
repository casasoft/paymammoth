using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductVariationColourModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationColourModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductVariationColourBaseCmsFactory_AutoGen : CmsFactoryBase<ProductVariationColourBaseCmsInfo, ProductVariationColourBase>
    {
       
       public new static ProductVariationColourBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductVariationColourBaseCmsFactory)CmsFactoryBase<ProductVariationColourBaseCmsInfo, ProductVariationColourBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ProductVariationColourBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductVariationColour.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductVariationColour";

            this.QueryStringParamID = "ProductVariationColourId";

            cmsInfo.TitlePlural = "Product Variation Colours";

            cmsInfo.TitleSingular =  "Product Variation Colour";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductVariationColourBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductVariationColour/";
			UsedInProject = ProductVariationColourBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
