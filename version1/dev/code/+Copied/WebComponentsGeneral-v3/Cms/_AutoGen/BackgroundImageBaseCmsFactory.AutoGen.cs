using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.BackgroundImageModule;
using CS.WebComponentsGeneralV3.Cms.BackgroundImageModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class BackgroundImageBaseCmsFactory_AutoGen : CmsFactoryBase<BackgroundImageBaseCmsInfo, BackgroundImageBase>
    {
       
       public new static BackgroundImageBaseCmsFactory Instance
	    {
	         get
	         {
                 return (BackgroundImageBaseCmsFactory)CmsFactoryBase<BackgroundImageBaseCmsInfo, BackgroundImageBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = BackgroundImageBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "BackgroundImage.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "BackgroundImage";

            this.QueryStringParamID = "BackgroundImageId";

            cmsInfo.TitlePlural = "Background Images";

            cmsInfo.TitleSingular =  "Background Image";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public BackgroundImageBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "BackgroundImage/";
			UsedInProject = BackgroundImageBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
