using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.AdvertModule;
using CS.WebComponentsGeneralV3.Cms.AdvertModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class AdvertBaseCmsFactory_AutoGen : CmsFactoryBase<AdvertBaseCmsInfo, AdvertBase>
    {
       
       public new static AdvertBaseCmsFactory Instance
	    {
	         get
	         {
                 return (AdvertBaseCmsFactory)CmsFactoryBase<AdvertBaseCmsInfo, AdvertBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = AdvertBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Advert.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Advert";

            this.QueryStringParamID = "AdvertId";

            cmsInfo.TitlePlural = "Adverts";

            cmsInfo.TitleSingular =  "Advert";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public AdvertBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Advert/";
			UsedInProject = AdvertBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
