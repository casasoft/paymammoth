using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ProductVersionMediaItemModule;
using CS.WebComponentsGeneralV3.Cms.ProductVersionMediaItemModule;
using BusinessLogic_v3.Frontend.ProductVersionMediaItemModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ProductVersionMediaItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductVersionMediaItemModule.ProductVersionMediaItemBase>
    {
		public ProductVersionMediaItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVersionMediaItemModule.ProductVersionMediaItemBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ProductVersionMediaItemModule.ProductVersionMediaItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductVersionMediaItemBaseFrontend FrontendItem
        {
            get { return (ProductVersionMediaItemBaseFrontend)base.FrontendItem; }

        }
        public new ProductVersionMediaItemBase DbItem
        {
            get
            {
                return (ProductVersionMediaItemBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ImageFilename { get; protected set; }

        public CmsPropertyInfo ProductVersion { get; protected set; }

        public CmsPropertyInfo IsMainImage { get; protected set; }

        public CmsPropertyInfo Caption { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
