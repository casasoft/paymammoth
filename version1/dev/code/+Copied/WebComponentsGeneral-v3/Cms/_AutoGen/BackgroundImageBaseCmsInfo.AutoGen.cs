using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.BackgroundImageModule;
using CS.WebComponentsGeneralV3.Cms.BackgroundImageModule;
using BusinessLogic_v3.Frontend.BackgroundImageModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class BackgroundImageBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.BackgroundImageModule.BackgroundImageBase>
    {
		public BackgroundImageBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.BackgroundImageModule.BackgroundImageBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.BackgroundImageModule.BackgroundImageBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new BackgroundImageBaseFrontend FrontendItem
        {
            get { return (BackgroundImageBaseFrontend)base.FrontendItem; }

        }
        public new BackgroundImageBase DbItem
        {
            get
            {
                return (BackgroundImageBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ImageFilename { get; protected set; }

        public CmsPropertyInfo CategoryLink { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
