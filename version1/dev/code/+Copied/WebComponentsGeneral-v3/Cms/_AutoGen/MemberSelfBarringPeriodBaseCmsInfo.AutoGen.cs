using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule;
using CS.WebComponentsGeneralV3.Cms.MemberSelfBarringPeriodModule;
using BusinessLogic_v3.Frontend.MemberSelfBarringPeriodModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class MemberSelfBarringPeriodBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBase>
    {
		public MemberSelfBarringPeriodBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberSelfBarringPeriodBaseFrontend FrontendItem
        {
            get { return (MemberSelfBarringPeriodBaseFrontend)base.FrontendItem; }

        }
        public new MemberSelfBarringPeriodBase DbItem
        {
            get
            {
                return (MemberSelfBarringPeriodBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Member { get; protected set; }

        public CmsPropertyInfo StartDate { get; protected set; }

        public CmsPropertyInfo EndDate { get; protected set; }

        public CmsPropertyInfo Timestamp { get; protected set; }

        public CmsPropertyInfo NotificationExpiry1Sent { get; protected set; }

        public CmsPropertyInfo NotificationExpiry2Sent { get; protected set; }

        public CmsPropertyInfo NotificationExpiredSent { get; protected set; }

        public CmsPropertyInfo IP { get; protected set; }

        public CmsPropertyInfo NotificationExpiry1_DateToSend { get; protected set; }

        public CmsPropertyInfo NotificationExpiry2_DateToSend { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
