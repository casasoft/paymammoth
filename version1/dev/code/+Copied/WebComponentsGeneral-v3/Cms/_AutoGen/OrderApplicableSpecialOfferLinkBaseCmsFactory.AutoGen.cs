using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule;
using CS.WebComponentsGeneralV3.Cms.OrderApplicableSpecialOfferLinkModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class OrderApplicableSpecialOfferLinkBaseCmsFactory_AutoGen : CmsFactoryBase<OrderApplicableSpecialOfferLinkBaseCmsInfo, OrderApplicableSpecialOfferLinkBase>
    {
       
       public new static OrderApplicableSpecialOfferLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (OrderApplicableSpecialOfferLinkBaseCmsFactory)CmsFactoryBase<OrderApplicableSpecialOfferLinkBaseCmsInfo, OrderApplicableSpecialOfferLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = OrderApplicableSpecialOfferLinkBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "OrderApplicableSpecialOfferLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "OrderApplicableSpecialOfferLink";

            this.QueryStringParamID = "OrderApplicableSpecialOfferLinkId";

            cmsInfo.TitlePlural = "Order Applicable Special Offer Links";

            cmsInfo.TitleSingular =  "Order Applicable Special Offer Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public OrderApplicableSpecialOfferLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "OrderApplicableSpecialOfferLink/";
			UsedInProject = OrderApplicableSpecialOfferLinkBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
