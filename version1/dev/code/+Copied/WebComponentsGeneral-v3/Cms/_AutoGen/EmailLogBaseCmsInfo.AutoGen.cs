using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.EmailLogModule;
using CS.WebComponentsGeneralV3.Cms.EmailLogModule;
using BusinessLogic_v3.Frontend.EmailLogModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class EmailLogBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>
    {
		public EmailLogBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.EmailLogModule.EmailLogBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EmailLogBaseFrontend FrontendItem
        {
            get { return (EmailLogBaseFrontend)base.FrontendItem; }

        }
        public new EmailLogBase DbItem
        {
            get
            {
                return (EmailLogBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateTime { get; protected set; }

        public CmsPropertyInfo FromEmail { get; protected set; }

        public CmsPropertyInfo FromName { get; protected set; }

        public CmsPropertyInfo ToEmail { get; protected set; }

        public CmsPropertyInfo ToName { get; protected set; }

        public CmsPropertyInfo ReplyToEmail { get; protected set; }

        public CmsPropertyInfo ReplyToName { get; protected set; }

        public CmsPropertyInfo Success { get; protected set; }

        public CmsPropertyInfo Subject { get; protected set; }

        public CmsPropertyInfo PlainText { get; protected set; }

        public CmsPropertyInfo HtmlText { get; protected set; }

        public CmsPropertyInfo Attachements { get; protected set; }

        public CmsPropertyInfo Resources { get; protected set; }

        public CmsPropertyInfo Host { get; protected set; }

        public CmsPropertyInfo Port { get; protected set; }

        public CmsPropertyInfo User { get; protected set; }

        public CmsPropertyInfo Pass { get; protected set; }

        public CmsPropertyInfo UsesSecureConnection { get; protected set; }

        public CmsPropertyInfo ResultDetails { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
