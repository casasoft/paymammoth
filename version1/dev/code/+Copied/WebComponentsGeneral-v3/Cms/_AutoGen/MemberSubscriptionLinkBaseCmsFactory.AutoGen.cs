using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.MemberSubscriptionLinkModule;
using CS.WebComponentsGeneralV3.Cms.MemberSubscriptionLinkModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class MemberSubscriptionLinkBaseCmsFactory_AutoGen : CmsFactoryBase<MemberSubscriptionLinkBaseCmsInfo, MemberSubscriptionLinkBase>
    {
       
       public new static MemberSubscriptionLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberSubscriptionLinkBaseCmsFactory)CmsFactoryBase<MemberSubscriptionLinkBaseCmsInfo, MemberSubscriptionLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = MemberSubscriptionLinkBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberSubscriptionLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberSubscriptionLink";

            this.QueryStringParamID = "MemberSubscriptionLinkId";

            cmsInfo.TitlePlural = "Member Subscription Links";

            cmsInfo.TitleSingular =  "Member Subscription Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberSubscriptionLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "MemberSubscriptionLink/";
			UsedInProject = MemberSubscriptionLinkBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
