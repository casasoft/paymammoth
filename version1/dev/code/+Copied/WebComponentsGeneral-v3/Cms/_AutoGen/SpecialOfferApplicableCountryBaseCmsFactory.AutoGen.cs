using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule;
using CS.WebComponentsGeneralV3.Cms.SpecialOfferApplicableCountryModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class SpecialOfferApplicableCountryBaseCmsFactory_AutoGen : CmsFactoryBase<SpecialOfferApplicableCountryBaseCmsInfo, SpecialOfferApplicableCountryBase>
    {
       
       public new static SpecialOfferApplicableCountryBaseCmsFactory Instance
	    {
	         get
	         {
                 return (SpecialOfferApplicableCountryBaseCmsFactory)CmsFactoryBase<SpecialOfferApplicableCountryBaseCmsInfo, SpecialOfferApplicableCountryBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	qParams.FillDefaultsForCms();
        	
            var q = SpecialOfferApplicableCountryBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "SpecialOfferApplicableCountry.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "SpecialOfferApplicableCountry";

            this.QueryStringParamID = "SpecialOfferApplicableCountryId";

            cmsInfo.TitlePlural = "Special Offer Applicable Countries";

            cmsInfo.TitleSingular =  "Special Offer Applicable Country";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public SpecialOfferApplicableCountryBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "SpecialOfferApplicableCountry/";
			UsedInProject = SpecialOfferApplicableCountryBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
