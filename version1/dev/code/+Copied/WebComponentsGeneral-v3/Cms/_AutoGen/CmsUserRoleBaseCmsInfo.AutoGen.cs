using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.CmsUserRoleModule;
using CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule;
using BusinessLogic_v3.Frontend.CmsUserRoleModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class CmsUserRoleBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase>
    {
		public CmsUserRoleBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CmsUserRoleBaseFrontend FrontendItem
        {
            get { return (CmsUserRoleBaseFrontend)base.FrontendItem; }

        }
        public new CmsUserRoleBase DbItem
        {
            get
            {
                return (CmsUserRoleBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsPropertyManyToManyCollection<CS.WebComponentsGeneralV3.Cms.CmsUserModule.CmsUserBaseCmsInfo> CmsUsers { get; protected set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

//InitFieldManyToManyBase_RightSide
		this.CmsUsers = this.AddManyToManyCollectionWithDirectProperty<CS.WebComponentsGeneralV3.Cms.CmsUserModule.CmsUserBaseCmsInfo>(
								item => item.CmsUsers, 
								CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
