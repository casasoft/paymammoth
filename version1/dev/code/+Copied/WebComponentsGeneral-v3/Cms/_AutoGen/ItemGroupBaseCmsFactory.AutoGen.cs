using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ItemGroupModule;
using BusinessLogic_v3.Cms.ItemGroupModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ItemGroupBaseCmsFactory_AutoGen : CmsFactoryBase<ItemGroupBaseCmsInfo, ItemGroupBase>
    {
       
       public new static ItemGroupBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ItemGroupBaseCmsFactory)CmsFactoryBase<ItemGroupBaseCmsInfo, ItemGroupBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ItemGroupBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ItemGroup.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ItemGroup";

            this.QueryStringParamID = "ItemGroupId";

            cmsInfo.TitlePlural = "Item Groups";

            cmsInfo.TitleSingular =  "Item Group";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ItemGroupBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ItemGroup/";


        }
       
    }

}
