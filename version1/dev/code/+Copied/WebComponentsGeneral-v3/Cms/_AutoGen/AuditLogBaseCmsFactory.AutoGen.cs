using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.AuditLogModule;
using CS.WebComponentsGeneralV3.Cms.AuditLogModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class AuditLogBaseCmsFactory_AutoGen : CmsFactoryBase<AuditLogBaseCmsInfo, AuditLogBase>
    {
       
       public new static AuditLogBaseCmsFactory Instance
	    {
	         get
	         {
                 return (AuditLogBaseCmsFactory)CmsFactoryBase<AuditLogBaseCmsInfo, AuditLogBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = AuditLogBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "AuditLog.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "AuditLog";

            this.QueryStringParamID = "AuditLogId";

            cmsInfo.TitlePlural = "Audit Logs";

            cmsInfo.TitleSingular =  "Audit Log";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public AuditLogBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "AuditLog/";
			UsedInProject = AuditLogBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
