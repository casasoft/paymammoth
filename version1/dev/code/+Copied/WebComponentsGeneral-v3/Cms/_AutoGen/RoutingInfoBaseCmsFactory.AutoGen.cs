using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.RoutingInfoModule;
using CS.WebComponentsGeneralV3.Cms.RoutingInfoModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class RoutingInfoBaseCmsFactory_AutoGen : CmsFactoryBase<RoutingInfoBaseCmsInfo, RoutingInfoBase>
    {
       
       public new static RoutingInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (RoutingInfoBaseCmsFactory)CmsFactoryBase<RoutingInfoBaseCmsInfo, RoutingInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = RoutingInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "RoutingInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "RoutingInfo";

            this.QueryStringParamID = "RoutingInfoId";

            cmsInfo.TitlePlural = "Routing Infos";

            cmsInfo.TitleSingular =  "Routing Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public RoutingInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "RoutingInfo/";
			UsedInProject = RoutingInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
