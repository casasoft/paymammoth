using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentPageCommentModule;
using BusinessLogic_v3.Cms.ContentPageCommentModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentPageCommentBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentPageCommentModule.ContentPageCommentBase>
    {
		public ContentPageCommentBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentPageCommentModule.ContentPageCommentBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentPageCommentModule.ContentPageCommentBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentPageCommentBase DbItem
        {
            get
            {
                return (ContentPageCommentBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Email { get; private set; }

        public CmsPropertyInfo Comment { get; private set; }

        public CmsPropertyInfo IPAddress { get; private set; }

        public CmsPropertyInfo PostedOn { get; private set; }

        public CmsPropertyInfo ContentPage { get; private set; }

        public CmsPropertyInfo Author { get; private set; }

        public CmsPropertyInfo Date { get; private set; }

        public CmsPropertyInfo Company { get; private set; }

        public CmsPropertyInfo IsApproved { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Email = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageCommentModule.ContentPageCommentBase>.GetPropertyBySelector( item => item.Email),
        			isEditable: true,
        			isVisible: true
        			);

        this.Comment = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageCommentModule.ContentPageCommentBase>.GetPropertyBySelector( item => item.Comment),
        			isEditable: true,
        			isVisible: true
        			);

        this.IPAddress = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageCommentModule.ContentPageCommentBase>.GetPropertyBySelector( item => item.IPAddress),
        			isEditable: true,
        			isVisible: true
        			);

        this.PostedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageCommentModule.ContentPageCommentBase>.GetPropertyBySelector( item => item.PostedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.ContentPage = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageCommentModule.ContentPageCommentBase>.GetPropertyBySelector( item => item.ContentPage),
        			isEditable: true,
        			isVisible: true
        			);

        this.Author = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageCommentModule.ContentPageCommentBase>.GetPropertyBySelector( item => item.Author),
        			isEditable: true,
        			isVisible: true
        			);

        this.Date = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageCommentModule.ContentPageCommentBase>.GetPropertyBySelector( item => item.Date),
        			isEditable: true,
        			isVisible: true
        			);

        this.Company = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageCommentModule.ContentPageCommentBase>.GetPropertyBySelector( item => item.Company),
        			isEditable: true,
        			isVisible: true
        			);

        this.IsApproved = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageCommentModule.ContentPageCommentBase>.GetPropertyBySelector( item => item.IsApproved),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
