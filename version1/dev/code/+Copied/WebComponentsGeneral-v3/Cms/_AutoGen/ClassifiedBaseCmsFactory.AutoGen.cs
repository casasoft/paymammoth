using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ClassifiedModule;
using CS.WebComponentsGeneralV3.Cms.ClassifiedModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ClassifiedBaseCmsFactory_AutoGen : CmsFactoryBase<ClassifiedBaseCmsInfo, ClassifiedBase>
    {
       
       public new static ClassifiedBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ClassifiedBaseCmsFactory)CmsFactoryBase<ClassifiedBaseCmsInfo, ClassifiedBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ClassifiedBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Classified.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Classified";

            this.QueryStringParamID = "ClassifiedId";

            cmsInfo.TitlePlural = "Classifieds";

            cmsInfo.TitleSingular =  "Classified";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ClassifiedBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Classified/";
			UsedInProject = ClassifiedBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
