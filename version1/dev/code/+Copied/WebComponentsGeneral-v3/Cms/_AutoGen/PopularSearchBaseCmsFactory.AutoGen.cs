using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.PopularSearchModule;
using CS.WebComponentsGeneralV3.Cms.PopularSearchModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class PopularSearchBaseCmsFactory_AutoGen : CmsFactoryBase<PopularSearchBaseCmsInfo, PopularSearchBase>
    {
       
       public new static PopularSearchBaseCmsFactory Instance
	    {
	         get
	         {
                 return (PopularSearchBaseCmsFactory)CmsFactoryBase<PopularSearchBaseCmsInfo, PopularSearchBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = PopularSearchBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "PopularSearch.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "PopularSearch";

            this.QueryStringParamID = "PopularSearchId";

            cmsInfo.TitlePlural = "Popular Searchs";

            cmsInfo.TitleSingular =  "Popular Search";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public PopularSearchBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "PopularSearch/";
			UsedInProject = PopularSearchBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
