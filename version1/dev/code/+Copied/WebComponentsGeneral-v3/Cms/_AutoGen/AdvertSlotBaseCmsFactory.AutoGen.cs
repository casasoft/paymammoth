using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.AdvertSlotModule;
using CS.WebComponentsGeneralV3.Cms.AdvertSlotModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class AdvertSlotBaseCmsFactory_AutoGen : CmsFactoryBase<AdvertSlotBaseCmsInfo, AdvertSlotBase>
    {
       
       public new static AdvertSlotBaseCmsFactory Instance
	    {
	         get
	         {
                 return (AdvertSlotBaseCmsFactory)CmsFactoryBase<AdvertSlotBaseCmsInfo, AdvertSlotBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = AdvertSlotBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "AdvertSlot.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "AdvertSlot";

            this.QueryStringParamID = "AdvertSlotId";

            cmsInfo.TitlePlural = "Advert Slots";

            cmsInfo.TitleSingular =  "Advert Slot";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public AdvertSlotBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "AdvertSlot/";
			UsedInProject = AdvertSlotBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
