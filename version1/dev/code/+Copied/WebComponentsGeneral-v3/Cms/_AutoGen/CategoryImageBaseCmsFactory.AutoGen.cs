using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.CategoryImageModule;
using CS.WebComponentsGeneralV3.Cms.CategoryImageModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class CategoryImageBaseCmsFactory_AutoGen : CmsFactoryBase<CategoryImageBaseCmsInfo, CategoryImageBase>
    {
       
       public new static CategoryImageBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CategoryImageBaseCmsFactory)CmsFactoryBase<CategoryImageBaseCmsInfo, CategoryImageBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = CategoryImageBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CategoryImage.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CategoryImage";

            this.QueryStringParamID = "CategoryImageId";

            cmsInfo.TitlePlural = "Category Images";

            cmsInfo.TitleSingular =  "Category Image";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CategoryImageBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "CategoryImage/";
			UsedInProject = CategoryImageBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
