using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ProductRelatedProductsModule;
using CS.WebComponentsGeneralV3.Cms.ProductRelatedProductsModule;
using BusinessLogic_v3.Frontend.ProductRelatedProductsModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ProductRelatedProductsBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductRelatedProductsModule.ProductRelatedProductsBase>
    {
		public ProductRelatedProductsBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductRelatedProductsModule.ProductRelatedProductsBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ProductRelatedProductsModule.ProductRelatedProductsBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductRelatedProductsBaseFrontend FrontendItem
        {
            get { return (ProductRelatedProductsBaseFrontend)base.FrontendItem; }

        }
        public new ProductRelatedProductsBase DbItem
        {
            get
            {
                return (ProductRelatedProductsBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Product { get; protected set; }

        public CmsPropertyInfo RelatedProduct { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
