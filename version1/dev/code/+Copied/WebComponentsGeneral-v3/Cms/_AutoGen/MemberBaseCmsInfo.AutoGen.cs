using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.MemberModule;
using CS.WebComponentsGeneralV3.Cms.MemberModule;
using BusinessLogic_v3.Frontend.MemberModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class MemberBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberModule.MemberBase>
    {
		public MemberBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberModule.MemberBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.MemberModule.MemberBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberBaseFrontend FrontendItem
        {
            get { return (MemberBaseFrontend)base.FrontendItem; }

        }
        public new MemberBase DbItem
        {
            get
            {
                return (MemberBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateRegistered { get; protected set; }

        public CmsPropertyInfo Username { get; protected set; }

        public CmsPropertyInfo Password { get; protected set; }

        public CmsPropertyInfo FirstName { get; protected set; }

        public CmsPropertyInfo LastName { get; protected set; }

        public CmsPropertyInfo Gender { get; protected set; }

        public CmsPropertyInfo DateOfBirth { get; protected set; }

        public CmsPropertyInfo Address1 { get; protected set; }

        public CmsPropertyInfo Address2 { get; protected set; }

        public CmsPropertyInfo Address3 { get; protected set; }

        public CmsPropertyInfo Locality { get; protected set; }

        public CmsPropertyInfo State { get; protected set; }

        public CmsPropertyInfo PostCode { get; protected set; }

        public CmsPropertyInfo Country { get; protected set; }

        public CmsPropertyInfo IDCard { get; protected set; }

        public CmsPropertyInfo LastLoggedIn { get; protected set; }

        public CmsPropertyInfo _LastLoggedIn_New { get; protected set; }

        public CmsPropertyInfo Accepted { get; protected set; }

        public CmsPropertyInfo Email { get; protected set; }

        public CmsPropertyInfo Telephone { get; protected set; }

        public CmsPropertyInfo Mobile { get; protected set; }

        public CmsPropertyInfo Fax { get; protected set; }

        public CmsPropertyInfo Remarks { get; protected set; }

        public CmsPropertyInfo Website { get; protected set; }

        public CmsPropertyInfo ActivatedOn { get; protected set; }

        public CmsPropertyInfo ActivatedIP { get; protected set; }

        public CmsPropertyInfo ActivationCode { get; protected set; }

        public CmsPropertyInfo SessionGUID { get; protected set; }

        public CmsPropertyInfo PreferredCultureInfo { get; protected set; }

        public CmsPropertyInfo ReferredByMember { get; protected set; }

        public CmsPropertyInfo AccountTerminated { get; protected set; }

        public CmsPropertyInfo AccountTerminatedOn { get; protected set; }

        public CmsPropertyInfo AccountTerminatedIP { get; protected set; }

        public CmsPropertyInfo ForgottenPassCode { get; protected set; }

        public CmsPropertyInfo SubscribedToNewsletter { get; protected set; }

        public CmsPropertyInfo CurrentShoppingCart { get; protected set; }

        public CmsPropertyInfo HowDidYouFindUs { get; protected set; }

        public CmsPropertyInfo LinkedAffiliate { get; protected set; }

        public CmsPropertyInfo CompanyName { get; protected set; }

        public CmsPropertyInfo PasswordEncryptionType { get; protected set; }

        public CmsPropertyInfo PasswordSalt { get; protected set; }

        public CmsPropertyInfo PasswordIterations { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo SubscriptionNotification1Sent { get; protected set; }

        public CmsPropertyInfo SubscriptionNotification2Sent { get; protected set; }

        public CmsPropertyInfo Paid { get; protected set; }

        public CmsPropertyInfo Activated { get; protected set; }

        public CmsPropertyInfo MiddleName { get; protected set; }

        public CmsPropertyInfo SubscriptionExpiryNotificationSent { get; protected set; }

        public CmsPropertyInfo ReferralSuccessful { get; protected set; }

        public CmsPropertyInfo ReferredFromUrl { get; protected set; }

        public CmsPropertyInfo AccountBalance { get; protected set; }

        public CmsPropertyInfo Blocked { get; protected set; }

        public CmsPropertyInfo SelfExcludedUntil { get; protected set; }

        public CmsPropertyInfo SelfExclusionSetOn { get; protected set; }

        public CmsPropertyInfo SelfExclusionSetByIp { get; protected set; }

        public CmsPropertyInfo AssignedToTicketingSystemSupportUser { get; protected set; }

        public CmsPropertyInfo PreferredTimezoneID { get; protected set; }

        public CmsPropertyInfo UsernameEncryptionType { get; protected set; }

        public CmsPropertyInfo UsernameEncryptionSalt { get; protected set; }

        public CmsPropertyInfo UsernameEncryptionIterations { get; protected set; }

        public CmsPropertyInfo BlockedReason { get; protected set; }

        public CmsPropertyInfo BlockedOn { get; protected set; }

        public CmsPropertyInfo BlockedUntil { get; protected set; }

        public CmsPropertyInfo BlockedCustomMessage { get; protected set; }

        public CmsPropertyInfo CurrentTotalInvalidLoginAttempts { get; protected set; }

        public CmsPropertyInfo KYCVerified { get; protected set; }

        public CmsPropertyInfo KYCProofOfAddressFilename { get; protected set; }

        public CmsPropertyInfo KYCProofOfIdentityFilename { get; protected set; }

        public CmsPropertyInfo KYCProcessStarted { get; protected set; }

        public CmsPropertyInfo KYCProcessStartedOn { get; protected set; }

        public CmsPropertyInfo IsFeatured { get; protected set; }

        public CmsPropertyInfo ImageFilename { get; protected set; }

        public CmsPropertyInfo CompanyProfile { get; protected set; }

        public CmsPropertyInfo AutoRenewSubscriptionTypePrice { get; protected set; }

        public CmsPropertyInfo AutoRenewLastMessage { get; protected set; }

        public CmsPropertyInfo KYCProcessRemarks { get; protected set; }

        public CmsPropertyInfo KYCVerifiedOn { get; protected set; }

        public CmsPropertyInfo RenewableAccountBalance { get; protected set; }

        public CmsPropertyInfo NextRenewableAccountBalanceTopupOn { get; protected set; }

        public CmsPropertyInfo NextMonthlyTopupOn { get; protected set; }

        public CmsPropertyInfo CompanyEmail { get; protected set; }

        public CmsPropertyInfo CompanyTelephone { get; protected set; }

        public CmsPropertyInfo VAT { get; protected set; }

        public CmsPropertyInfo Nationality { get; protected set; }

        public CmsPropertyInfo PassportNo { get; protected set; }

        public CmsPropertyInfo PassportPlaceOfIssue { get; protected set; }

        public CmsPropertyInfo AccountRegistrationIP { get; protected set; }

        public CmsPropertyInfo IDCardPlaceOfIssue { get; protected set; }

        public CmsPropertyInfo DrivingLicense { get; protected set; }

        public CmsPropertyInfo DrivingLicensePlaceOfIssue { get; protected set; }

        public CmsPropertyInfo RegisteredThroughSocialMedium { get; protected set; }

        public CmsPropertyInfo ApplicableSpecialOfferVoucherCode { get; protected set; }

        public CmsPropertyInfo PersonalAccountType { get; protected set; }

        public CmsPropertyInfo TwitterUserID { get; protected set; }

        public CmsPropertyInfo TwitterScreenName { get; protected set; }

        public CmsPropertyInfo AllowPromotions { get; protected set; }

        public CmsPropertyInfo JobPosition { get; protected set; }

        public CmsPropertyInfo IsContactPerson { get; protected set; }

        public CmsPropertyInfo MemberType { get; protected set; }

        public CmsPropertyInfo IsTestMember { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo Classifieds { get; protected set; }
        

        public CmsCollectionInfo ContactForms { get; protected set; }
        

        public CmsCollectionInfo Members { get; protected set; }
        

        public CmsCollectionInfo AccountBalanceHistoryItems { get; protected set; }
        

        public CmsCollectionInfo LoginInfo { get; protected set; }
        

        public CmsCollectionInfo SentReferrals { get; protected set; }
        

        public CmsCollectionInfo RegisteredReferrals { get; protected set; }
        

        public CmsCollectionInfo ResponsibleLimits { get; protected set; }
        

        public CmsCollectionInfo MemberSelfBarringPeriods { get; protected set; }
        

        public CmsCollectionInfo MemberSubscriptionLinks { get; protected set; }
        

        public CmsCollectionInfo Orders { get; protected set; }
        

        public CmsCollectionInfo ShoppingCarts { get; protected set; }
        

        public CmsPropertyManyToManyCollection<CS.WebComponentsGeneralV3.Cms.CategoryModule.CategoryBaseCmsInfo> SubscribedCategories { get; protected set; }
        public CmsPropertyManyToManyCollection<CS.WebComponentsGeneralV3.Cms.EventModule.EventBaseCmsInfo> Events { get; protected set; }
        public CmsPropertyManyToManyCollection<CS.WebComponentsGeneralV3.Cms.ArticleModule.ArticleBaseCmsInfo> ArticlesRead { get; protected set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.Classifieds = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.Classifieds),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase>.GetPropertyBySelector(item => item.CreatedBy)));
		
		//InitCollectionBaseOneToMany
		this.ContactForms = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.ContactForms),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector(item => item.SentByMember)));
		
		//InitCollectionBaseOneToMany
		this.Members = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.Members),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.ReferredByMember)));
		
		//InitCollectionBaseOneToMany
		this.AccountBalanceHistoryItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.AccountBalanceHistoryItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase>.GetPropertyBySelector(item => item.Member)));
		
		//InitCollectionBaseOneToMany
		this.LoginInfo = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.LoginInfo),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase>.GetPropertyBySelector(item => item.Member)));
		
		//InitCollectionBaseOneToMany
		this.SentReferrals = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.SentReferrals),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector(item => item.SentByMember)));
		
		//InitCollectionBaseOneToMany
		this.RegisteredReferrals = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.RegisteredReferrals),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector(item => item.RegisteredMember)));
		
		//InitCollectionBaseOneToMany
		this.ResponsibleLimits = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.ResponsibleLimits),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase>.GetPropertyBySelector(item => item.Member)));
		
		//InitCollectionBaseOneToMany
		this.MemberSelfBarringPeriods = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.MemberSelfBarringPeriods),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBase>.GetPropertyBySelector(item => item.Member)));
		
		//InitCollectionBaseOneToMany
		this.MemberSubscriptionLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.MemberSubscriptionLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase>.GetPropertyBySelector(item => item.Member)));
		
		//InitCollectionBaseOneToMany
		this.Orders = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.Orders),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector(item => item.Member)));
		
		//InitCollectionBaseOneToMany
		this.ShoppingCarts = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.ShoppingCarts),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase>.GetPropertyBySelector(item => item.Member)));
//InitFieldManyToManyBase_RightSide
		this.SubscribedCategories = this.AddManyToManyCollectionWithDirectProperty<CS.WebComponentsGeneralV3.Cms.CategoryModule.CategoryBaseCmsInfo>(
								item => item.SubscribedCategories, 
								CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);
//InitFieldManyToManyBase_RightSide
		this.Events = this.AddManyToManyCollectionWithDirectProperty<CS.WebComponentsGeneralV3.Cms.EventModule.EventBaseCmsInfo>(
								item => item.Events, 
								CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);
//InitFieldManyToManyBase_RightSide
		this.ArticlesRead = this.AddManyToManyCollectionWithDirectProperty<CS.WebComponentsGeneralV3.Cms.ArticleModule.ArticleBaseCmsInfo>(
								item => item.ArticlesRead, 
								CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
