using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ContentPageRatingModule;
using BusinessLogic_v3.Cms.ContentPageRatingModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ContentPageRatingBaseCmsFactory_AutoGen : CmsFactoryBase<ContentPageRatingBaseCmsInfo, ContentPageRatingBase>
    {
       
       public new static ContentPageRatingBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContentPageRatingBaseCmsFactory)CmsFactoryBase<ContentPageRatingBaseCmsInfo, ContentPageRatingBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ContentPageRatingBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContentPageRating.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContentPageRating";

            this.QueryStringParamID = "ContentPageRatingId";

            cmsInfo.TitlePlural = "Content Page Ratings";

            cmsInfo.TitleSingular =  "Content Page Rating";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContentPageRatingBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ContentPageRating/";


        }
       
    }

}
