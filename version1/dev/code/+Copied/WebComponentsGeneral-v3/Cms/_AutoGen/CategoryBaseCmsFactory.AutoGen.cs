using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.CategoryModule;
using CS.WebComponentsGeneralV3.Cms.CategoryModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class CategoryBaseCmsFactory_AutoGen : CmsFactoryBase<CategoryBaseCmsInfo, CategoryBase>
    {
       
       public new static CategoryBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CategoryBaseCmsFactory)CmsFactoryBase<CategoryBaseCmsInfo, CategoryBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = CategoryBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Category.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Category";

            this.QueryStringParamID = "CategoryId";

            cmsInfo.TitlePlural = "Categories";

            cmsInfo.TitleSingular =  "Category";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CategoryBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Category/";
			UsedInProject = CategoryBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
