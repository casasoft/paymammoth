using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.AttachmentModule;
using CS.WebComponentsGeneralV3.Cms.AttachmentModule;
using BusinessLogic_v3.Frontend.AttachmentModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class AttachmentBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase>
    {
		public AttachmentBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.AttachmentModule.AttachmentBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AttachmentBaseFrontend FrontendItem
        {
            get { return (AttachmentBaseFrontend)base.FrontendItem; }

        }
        public new AttachmentBase DbItem
        {
            get
            {
                return (AttachmentBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo FileName { get; protected set; }

        public CmsPropertyInfo TicketComment { get; protected set; }

        public CmsPropertyInfo ContentType { get; protected set; }

        public CmsPropertyInfo DateSubmitted { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
