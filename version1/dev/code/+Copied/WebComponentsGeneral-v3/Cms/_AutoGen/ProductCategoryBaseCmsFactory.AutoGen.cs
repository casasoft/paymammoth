using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using CS.WebComponentsGeneralV3.Cms.ProductCategoryModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductCategoryBaseCmsFactory_AutoGen : CmsFactoryBase<ProductCategoryBaseCmsInfo, ProductCategoryBase>
    {
       
       public new static ProductCategoryBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductCategoryBaseCmsFactory)CmsFactoryBase<ProductCategoryBaseCmsInfo, ProductCategoryBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ProductCategoryBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductCategory.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductCategory";

            this.QueryStringParamID = "ProductCategoryId";

            cmsInfo.TitlePlural = "Product Categories";

            cmsInfo.TitleSingular =  "Product Category";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductCategoryBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductCategory/";
			UsedInProject = ProductCategoryBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
