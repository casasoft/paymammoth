using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using CS.WebComponentsGeneralV3.Cms.Article_ParentChildLinkModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class Article_ParentChildLinkBaseCmsFactory_AutoGen : CmsFactoryBase<Article_ParentChildLinkBaseCmsInfo, Article_ParentChildLinkBase>
    {
       
       public new static Article_ParentChildLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Article_ParentChildLinkBaseCmsFactory)CmsFactoryBase<Article_ParentChildLinkBaseCmsInfo, Article_ParentChildLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = Article_ParentChildLinkBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Article_ParentChildLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Article_ParentChildLink";

            this.QueryStringParamID = "Article_ParentChildLinkId";

            cmsInfo.TitlePlural = "Article_ Parent Child Links";

            cmsInfo.TitleSingular =  "Article_ Parent Child Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Article_ParentChildLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Article_ParentChildLink/";
			UsedInProject = Article_ParentChildLinkBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
