using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.TestimonialsModule;
using CS.WebComponentsGeneralV3.Cms.TestimonialsModule;
using BusinessLogic_v3.Frontend.TestimonialsModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class TestimonialsBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.TestimonialsModule.TestimonialsBase>
    {
		public TestimonialsBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.TestimonialsModule.TestimonialsBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.TestimonialsModule.TestimonialsBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new TestimonialsBaseFrontend FrontendItem
        {
            get { return (TestimonialsBaseFrontend)base.FrontendItem; }

        }
        public new TestimonialsBase DbItem
        {
            get
            {
                return (TestimonialsBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo FullName { get; protected set; }

        public CmsPropertyInfo Testimonial { get; protected set; }

        public CmsPropertyInfo Event { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
