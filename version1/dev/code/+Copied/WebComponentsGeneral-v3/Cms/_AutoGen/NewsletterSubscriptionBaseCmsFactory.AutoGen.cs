using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.NewsletterSubscriptionModule;
using CS.WebComponentsGeneralV3.Cms.NewsletterSubscriptionModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class NewsletterSubscriptionBaseCmsFactory_AutoGen : CmsFactoryBase<NewsletterSubscriptionBaseCmsInfo, NewsletterSubscriptionBase>
    {
       
       public new static NewsletterSubscriptionBaseCmsFactory Instance
	    {
	         get
	         {
                 return (NewsletterSubscriptionBaseCmsFactory)CmsFactoryBase<NewsletterSubscriptionBaseCmsInfo, NewsletterSubscriptionBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = NewsletterSubscriptionBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "NewsletterSubscription.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "NewsletterSubscription";

            this.QueryStringParamID = "NewsletterSubscriptionId";

            cmsInfo.TitlePlural = "Newsletter Subscriptions";

            cmsInfo.TitleSingular =  "Newsletter Subscription";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public NewsletterSubscriptionBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "NewsletterSubscription/";
			UsedInProject = NewsletterSubscriptionBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
