using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule;
using CS.WebComponentsGeneralV3.Cms.SpecialOfferApplicableCountryModule;
using BusinessLogic_v3.Frontend.SpecialOfferApplicableCountryModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class SpecialOfferApplicableCountryBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule.SpecialOfferApplicableCountryBase>
    {
		public SpecialOfferApplicableCountryBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule.SpecialOfferApplicableCountryBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.SpecialOfferApplicableCountryModule.SpecialOfferApplicableCountryBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new SpecialOfferApplicableCountryBaseFrontend FrontendItem
        {
            get { return (SpecialOfferApplicableCountryBaseFrontend)base.FrontendItem; }

        }
        public new SpecialOfferApplicableCountryBase DbItem
        {
            get
            {
                return (SpecialOfferApplicableCountryBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo SpecialOffer { get; protected set; }

        public CmsPropertyInfo Country { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
