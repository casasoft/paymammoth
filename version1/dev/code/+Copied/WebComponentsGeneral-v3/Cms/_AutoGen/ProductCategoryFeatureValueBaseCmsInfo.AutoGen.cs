using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule;
using CS.WebComponentsGeneralV3.Cms.ProductCategoryFeatureValueModule;
using BusinessLogic_v3.Frontend.ProductCategoryFeatureValueModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class ProductCategoryFeatureValueBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase>
    {
		public ProductCategoryFeatureValueBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductCategoryFeatureValueBaseFrontend FrontendItem
        {
            get { return (ProductCategoryFeatureValueBaseFrontend)base.FrontendItem; }

        }
        public new ProductCategoryFeatureValueBase DbItem
        {
            get
            {
                return (ProductCategoryFeatureValueBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Product { get; protected set; }

        public CmsPropertyInfo CategoryFeature { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
