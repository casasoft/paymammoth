using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationMediaItemModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ProductVariationMediaItemBaseCmsFactory_AutoGen : CmsFactoryBase<ProductVariationMediaItemBaseCmsInfo, ProductVariationMediaItemBase>
    {
       
       public new static ProductVariationMediaItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductVariationMediaItemBaseCmsFactory)CmsFactoryBase<ProductVariationMediaItemBaseCmsInfo, ProductVariationMediaItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ProductVariationMediaItemBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductVariationMediaItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductVariationMediaItem";

            this.QueryStringParamID = "ProductVariationMediaItemId";

            cmsInfo.TitlePlural = "Product Variation Media Items";

            cmsInfo.TitleSingular =  "Product Variation Media Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductVariationMediaItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ProductVariationMediaItem/";
			UsedInProject = ProductVariationMediaItemBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
