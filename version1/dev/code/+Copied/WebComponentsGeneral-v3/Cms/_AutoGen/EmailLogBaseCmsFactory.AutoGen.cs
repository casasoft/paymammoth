using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.EmailLogModule;
using CS.WebComponentsGeneralV3.Cms.EmailLogModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class EmailLogBaseCmsFactory_AutoGen : CmsFactoryBase<EmailLogBaseCmsInfo, EmailLogBase>
    {
       
       public new static EmailLogBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EmailLogBaseCmsFactory)CmsFactoryBase<EmailLogBaseCmsInfo, EmailLogBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = EmailLogBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EmailLog.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EmailLog";

            this.QueryStringParamID = "EmailLogId";

            cmsInfo.TitlePlural = "Email Logs";

            cmsInfo.TitleSingular =  "Email Log";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EmailLogBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "EmailLog/";
			UsedInProject = EmailLogBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
