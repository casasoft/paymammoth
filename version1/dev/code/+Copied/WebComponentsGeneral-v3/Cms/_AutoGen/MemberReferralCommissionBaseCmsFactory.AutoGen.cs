using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.MemberReferralCommissionModule;
using CS.WebComponentsGeneralV3.Cms.MemberReferralCommissionModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class MemberReferralCommissionBaseCmsFactory_AutoGen : CmsFactoryBase<MemberReferralCommissionBaseCmsInfo, MemberReferralCommissionBase>
    {
       
       public new static MemberReferralCommissionBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberReferralCommissionBaseCmsFactory)CmsFactoryBase<MemberReferralCommissionBaseCmsInfo, MemberReferralCommissionBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = MemberReferralCommissionBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberReferralCommission.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberReferralCommission";

            this.QueryStringParamID = "MemberReferralCommissionId";

            cmsInfo.TitlePlural = "Member Referral Commissions";

            cmsInfo.TitleSingular =  "Member Referral Commission";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberReferralCommissionBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "MemberReferralCommission/";
			UsedInProject = MemberReferralCommissionBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
