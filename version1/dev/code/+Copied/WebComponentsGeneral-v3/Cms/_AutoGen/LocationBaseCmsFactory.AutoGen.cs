using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.LocationModule;
using CS.WebComponentsGeneralV3.Cms.LocationModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class LocationBaseCmsFactory_AutoGen : CmsFactoryBase<LocationBaseCmsInfo, LocationBase>
    {
       
       public new static LocationBaseCmsFactory Instance
	    {
	         get
	         {
                 return (LocationBaseCmsFactory)CmsFactoryBase<LocationBaseCmsInfo, LocationBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = LocationBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Location.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Location";

            this.QueryStringParamID = "LocationId";

            cmsInfo.TitlePlural = "Locations";

            cmsInfo.TitleSingular =  "Location";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public LocationBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "Location/";
			UsedInProject = LocationBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
