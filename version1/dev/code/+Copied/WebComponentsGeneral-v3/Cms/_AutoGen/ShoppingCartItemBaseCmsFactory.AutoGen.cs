using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using BusinessLogic_v3.Modules.ShoppingCartItemModule;
using CS.WebComponentsGeneralV3.Cms.ShoppingCartItemModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
	public abstract class ShoppingCartItemBaseCmsFactory_AutoGen : CmsFactoryBase<ShoppingCartItemBaseCmsInfo, ShoppingCartItemBase>
    {
       
       public new static ShoppingCartItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ShoppingCartItemBaseCmsFactory)CmsFactoryBase<ShoppingCartItemBaseCmsInfo, ShoppingCartItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = ShoppingCartItemBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ShoppingCartItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ShoppingCartItem";

            this.QueryStringParamID = "ShoppingCartItemId";

            cmsInfo.TitlePlural = "Shopping Cart Items";

            cmsInfo.TitleSingular =  "Shopping Cart Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ShoppingCartItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "ShoppingCartItem/";
			UsedInProject = ShoppingCartItemBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
