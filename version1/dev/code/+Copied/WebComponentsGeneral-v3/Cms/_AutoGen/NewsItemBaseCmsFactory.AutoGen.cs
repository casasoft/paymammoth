using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.NewsItemModule;
using BusinessLogic_v3.Cms.NewsItemModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class NewsItemBaseCmsFactory_AutoGen : CmsFactoryBase<NewsItemBaseCmsInfo, NewsItemBase>
    {
       
       public new static NewsItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (NewsItemBaseCmsFactory)CmsFactoryBase<NewsItemBaseCmsInfo, NewsItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = NewsItemBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "NewsItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "NewsItem";

            this.QueryStringParamID = "NewsItemId";

            cmsInfo.TitlePlural = "News Items";

            cmsInfo.TitleSingular =  "News Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public NewsItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "NewsItem/";


        }
       
    }

}
