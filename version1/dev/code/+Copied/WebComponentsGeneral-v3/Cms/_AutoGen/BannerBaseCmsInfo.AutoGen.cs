using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Modules.BannerModule;
using CS.WebComponentsGeneralV3.Cms.BannerModule;
using BusinessLogic_v3.Frontend.BannerModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms._AutoGen
{
    public abstract class BannerBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.BannerModule.BannerBase>
    {
		public BannerBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.BannerModule.BannerBase dbItem)
            : base(CS.WebComponentsGeneralV3.Cms.BannerModule.BannerBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new BannerBaseFrontend FrontendItem
        {
            get { return (BannerBaseFrontend)base.FrontendItem; }

        }
        public new BannerBase DbItem
        {
            get
            {
                return (BannerBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo MediaItemFilename { get; protected set; }

        public CmsPropertyInfo Link { get; protected set; }

        public CmsPropertyInfo DurationMS { get; protected set; }

        public CmsPropertyInfo HrefTarget { get; protected set; }

        public CmsPropertyInfo SlideshowDelaySec { get; protected set; }

        public CmsPropertyInfo HTMLDescription { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsPropertyManyToManyCollection<CS.WebComponentsGeneralV3.Cms.CultureDetailsModule.CultureDetailsBaseCmsInfo> Cultures { get; protected set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

//InitFieldManyToManyBase_RightSide
		this.Cultures = this.AddManyToManyCollectionWithDirectProperty<CS.WebComponentsGeneralV3.Cms.CultureDetailsModule.CultureDetailsBaseCmsInfo>(
								item => item.Cultures, 
								CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
