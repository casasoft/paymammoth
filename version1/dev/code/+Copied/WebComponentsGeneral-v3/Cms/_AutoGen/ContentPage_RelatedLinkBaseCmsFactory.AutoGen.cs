using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ContentPage_RelatedLinkModule;
using BusinessLogic_v3.Cms.ContentPage_RelatedLinkModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ContentPage_RelatedLinkBaseCmsFactory_AutoGen : CmsFactoryBase<ContentPage_RelatedLinkBaseCmsInfo, ContentPage_RelatedLinkBase>
    {
       
       public new static ContentPage_RelatedLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContentPage_RelatedLinkBaseCmsFactory)CmsFactoryBase<ContentPage_RelatedLinkBaseCmsInfo, ContentPage_RelatedLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ContentPage_RelatedLinkBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContentPage_RelatedLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContentPage_RelatedLink";

            this.QueryStringParamID = "ContentPage_RelatedLinkId";

            cmsInfo.TitlePlural = "Content Page _ Related Links";

            cmsInfo.TitleSingular =  "Content Page _ Related Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContentPage_RelatedLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ContentPage_RelatedLink/";


        }
       
    }

}
