using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.CategoryFeatureModule;
using BusinessLogic_v3.Modules.CategoryFeatureModule;

namespace CS.WebComponentsGeneralV3.Cms.CategoryFeatureModule
{
    public abstract class CategoryFeatureBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.CategoryFeatureBaseCmsInfo_AutoGen
    {
    	public CategoryFeatureBaseCmsInfo(CategoryFeatureBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            initListingItems();
            initRequired();
            initPriority();
            initCMSLink();
		    initIcon();
        }

        private void initCMSLink()
        {
            
        }

        private void initPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(Title, this.Category, Description, Link);
            this.EditOrderPriorities.AddColumnsToStart(Title, Link, Description, Icon);
        }

        private void initRequired()
        {
            Title.IsRequired = true;
            this.Description.IsRequired = false;
            this.Description.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;
            this.Link.IsRequired = false;
        }

        private void initIcon()
        {
            this.Icon = this.AddPropertyForMediaItem<CategoryFeatureBase>(item => item.Icon, true, true);
        }
        public CmsPropertyMediaItem<CategoryFeatureBase> Icon { get; set; }

        private void initListingItems()
        {
            IconFilename.ShowInListing = false;
            IconFilename.ShowInEdit = false;
            this.Category.ShowInListing = true;
            this.Category.ShowInEdit = true;
            Title.ShowInListing = true;
            Link.ShowInListing = true;
        }


    }
}
