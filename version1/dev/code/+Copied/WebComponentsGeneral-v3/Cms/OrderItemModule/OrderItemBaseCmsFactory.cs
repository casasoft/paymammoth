using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.OrderItemModule;

namespace CS.WebComponentsGeneralV3.Cms.OrderItemModule
{
    public abstract class OrderItemBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.OrderItemBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToAdd.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToDelete.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            base.initCmsParameters();
        }
    }
}
