using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.OrderItemModule;

namespace CS.WebComponentsGeneralV3.Cms.OrderItemModule
{
    public abstract class OrderItemBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.OrderItemBaseCmsInfo_AutoGen
    {
        public OrderItemBaseCmsInfo(OrderItemBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            
            base.customInitFields();
            
            this.Title.EditableInListing = true;
            this.ItemReference.ShowInListing = true;
            this.Quantity.EditableInListing = true;
           // this.SpecialOffer.ShowInListing = true;
            this.PriceIncTaxPerUnit.EditableInListing = true;
          //  this.ShippingCost.EditableInListing = true;
           // this.HandlingCost.EditableInListing = true;
            this.ShipmentWeight.ShowInListing= true;
            this.DiscountPerUnit.EditableInListing = true;
            this.TaxRate.EditableInListing = true;
            this.Order.IsSearchable = false;
            //this.SpecialOfferVoucherCode.IsSearchable = false;
            this.ListingOrderPriorities.AddColumnsToStart(this.Title,this.ItemReference,this.Quantity, this.PriceIncTaxPerUnit,  this.ShipmentWeight, this.DiscountPerUnit, this.TaxRate);
            
            
        }

        

	}
}
