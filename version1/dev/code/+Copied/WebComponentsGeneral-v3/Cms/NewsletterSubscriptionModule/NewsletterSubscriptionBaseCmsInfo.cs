using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.NewsletterSubscriptionModule;

namespace CS.WebComponentsGeneralV3.Cms.NewsletterSubscriptionModule
{
    public abstract class NewsletterSubscriptionBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.NewsletterSubscriptionBaseCmsInfo_AutoGen
    {
    	public NewsletterSubscriptionBaseCmsInfo(NewsletterSubscriptionBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();

            this.Enabled.ShowInListing = true;
            this.Enabled.EditableInListing = true;
            this.Name.ShowInListing = true;
            this.DateAdded.ShowInListing = true;
            this.Email.ShowInListing = true;
            this.Telephone.ShowInListing = true;

            this.ListingOrderPriorities.AddColumnsToStart(this.Enabled, this.DateAdded, this.Name, this.Email, this.Telephone);

        }
	}
}
