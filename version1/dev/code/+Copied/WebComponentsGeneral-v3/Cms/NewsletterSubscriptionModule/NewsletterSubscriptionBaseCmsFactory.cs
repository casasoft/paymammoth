using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.NewsletterSubscriptionModule;

namespace CS.WebComponentsGeneralV3.Cms.NewsletterSubscriptionModule
{
    public abstract class NewsletterSubscriptionBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.NewsletterSubscriptionBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
    }
}
