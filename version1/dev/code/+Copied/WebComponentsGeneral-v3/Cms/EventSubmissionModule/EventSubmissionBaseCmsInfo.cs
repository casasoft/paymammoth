using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EventSubmissionModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3.Cms.EventSubmissionModule
{

//BaseCmsInfo-Class

    public abstract class EventSubmissionBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.EventSubmissionBaseCmsInfo_AutoGen
    {
    	public EventSubmissionBaseCmsInfo(EventSubmissionBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
		    initListingFields();
		    initEditingRights();
		    initEditFields();
		    initApproveAndConvertButton();

            base.customInitFields();
        }

        private void initApproveAndConvertButton()
        {
            if (DbItem != null)
            {
                if (!DbItem.Approved && !DbItem.IsConvertedToRealEvent)
                {
                    var customOp = new CmsItemSpecificOperation(this, "Approve and Convert to Real Event", Code.Cms.EnumsCms.IMAGE_ICON.Success,
                                                                approveAndConvert);
                    customOp.ConfirmMessage =
                        "Are you sure you want to approve this event and convert it into a real event?\n\nRemember that once converted, this event submission cannot be converted again even if the event has been deleted!";
                    this.CustomCmsOperations.Add(customOp);
                }
            }
        }

        private void approveAndConvert()
        {
            DbItem.ApproveAndConvertToRealEvent(getLoggedCmsUser());
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Event approved and converted successfully.", General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

        private void initEditFields()
        {
            this.ApprovedBy.ShowInEdit = true;
            this.EditOrderPriorities.AddColumnsToStart(EventName, Category, EventDescription, EventStartDate,
                                                       EventEndDate, UserFullName, UserEmail);
            this.EventDescription.StringDataType = General_v3.Enums.STRING_DATA_TYPE.Html;
        }

        private void initEditingRights()
        {
            this.UserIpAddress.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.Timestamp.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.Approved.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.ApprovedOn.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.ApprovedBy.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.IsConvertedToRealEvent.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
        }

        private void initListingFields()
        {

            this.Category.ShowInEdit = true;
            this.UserFullName.ShowInListing = true;
            this.Timestamp.ShowInListing = true;
            this.Approved.ShowInListing = true;
            this.IsConvertedToRealEvent.ShowInListing = true;
            this.EventName.ShowInListing = true;
            this.EventStartDate.ShowInListing = true;
            this.EventEndDate.ShowInListing = true;
            this.UserEmail.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(Timestamp, EventName, EventStartDate, EventEndDate, UserFullName,
                                                          UserEmail, Approved, IsConvertedToRealEvent);
        }
	}
}
