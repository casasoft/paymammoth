using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.EventSubmissionModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.EventSubmissionModule
{

//BaseCmsFactory-Class

    public abstract class EventSubmissionBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.EventSubmissionBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.
            cmsInfo.ShowAddNewButton = false;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

    }
}
