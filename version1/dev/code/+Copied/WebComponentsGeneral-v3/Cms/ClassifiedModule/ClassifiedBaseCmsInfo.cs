using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ClassifiedModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3.Cms.ClassifiedModule
{

//BaseCmsInfo-Class

    public abstract class ClassifiedBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ClassifiedBaseCmsInfo_AutoGen
    {
    	public ClassifiedBaseCmsInfo(ClassifiedBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
		    initImages();
		    initListingFields();
		    initEditFields();
		    initButtons();
            base.customInitFields();
        }

        private void initButtons()
        {
            initApproveButton();
        }

        private void initApproveButton()
        {
            if (DbItem != null)
            {
                if (DbItem.Approved)
                {
                    var customOp = new CmsItemSpecificOperation(this, "Disapprove", Code.Cms.EnumsCms.IMAGE_ICON.Error,
                                                                customOperation_Disapprove);
                    customOp.ConfirmMessage = "Are you sure you want to disapprove this item?";
                    this.CustomCmsOperations.Add(customOp);
                }
                else
                {
                    var customOp = new CmsItemSpecificOperation(this, "Approve", Code.Cms.EnumsCms.IMAGE_ICON.Success,
                                                                customOperation_Approve);
                    customOp.ConfirmMessage = "Are you sure you want to approve this item?";
                    this.CustomCmsOperations.Add(customOp);
                }
            }
        }

        private void customOperation_Approve()
        {
            this.DbItem.Approve(BusinessLogic_v3.Modules.CmsUserModule.CmsUserSessionLogin.Instance.GetCurrentLoggedUser());
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Item approved successfully.", General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

        private void customOperation_Disapprove()
        {
            this.DbItem.Disapprove();
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Item disapproved successfully.", General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

        private void initEditFields()
        {
            Approved.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            ApprovedOn.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            ApprovedBy.ShowInEdit = true;
            ApprovedBy.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            ApprovedBy.AccessTypeRequired_View.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.Owner);
            PostedOn.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.CreatedBy.ShowInEdit = true;
            this.CreatedBy.AccessTypeRequired_View.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.Normal);
            this.CreatedBy.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam);
            this.Category.ShowInEdit = true;
            this.CreatedBy.AccessTypeRequired_View.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.Normal);
            this.CreatedBy.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.Normal);
            Submitted.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam);
            EditOrderPriorities.AddColumnsToStart(ID, Title, Approved, ApprovedOn, ApprovedBy, Price, Description,
                                                  IsPriceNegotiable, Submitted);
        }

        private void initListingFields()
        {
            this.Title.ShowInListing = true;
            this.CreatedBy.ShowInListing = true;
            this.Category.ShowInListing = true;
            this.Approved.ShowInListing = true;
            this.Image.ShowInListing = true;
            ListingOrderPriorities.AddColumnsToStart(Image, Title, Category, CreatedBy, Approved);
        }

        private void initImages()
        {
            this.Image = this.AddPropertyForMediaItem<ClassifiedBase>(item => item.Image);
        }

        public CmsPropertyMediaItem<ClassifiedBase> Image { get; private set; }

	}
}
