using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.ClassifiedModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.ClassifiedModule
{

//BaseCmsFactory-Class

    public abstract class ClassifiedBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.ClassifiedBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            cmsInfo.ShowAddNewButton = false;
            initRefreshLucene();
        	//any user-specific information, add them to 'cmsInfo'.
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

        private void initRefreshLucene()
        {

            var loggedUser = getLoggedCmsUser();
            {
                if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator))
                {
                    var op = new CmsGeneralOperation("Refresh Lucene Index", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.ReOrder, refreshLucene);
                    op.ConfirmMessage = "Refreshing the Lucene Index might take some time! Do only if search results are not working correctly!\n\nAlso, index is refreshed for this sections only. Other sections must be refreshed separately.";
                    this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
                }
            }
        }
        private void refreshLucene()
        {
            ClassifiedBaseFactory.Instance.ReCreateLuceneIndex();
        }

    }
}
