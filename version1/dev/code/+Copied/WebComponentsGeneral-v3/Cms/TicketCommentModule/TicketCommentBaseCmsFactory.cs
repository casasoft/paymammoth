using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.TicketCommentModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.TicketCommentModule
{

//BaseCmsFactory-Class

    public abstract class TicketCommentBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.TicketCommentBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
        }

    }
}
