using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.ProductRelatedProductsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.ProductRelatedProductsModule
{

//BaseCmsFactory-Class

    public abstract class ProductRelatedProductsBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.ProductRelatedProductsBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
        }

    }
}
