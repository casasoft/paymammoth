using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberResponsibleLimitsModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.MemberResponsibleLimitsModule
{

//BaseCmsInfo-Class

    public abstract class MemberResponsibleLimitsBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.MemberResponsibleLimitsBaseCmsInfo_AutoGen
    {
    	public MemberResponsibleLimitsBaseCmsInfo(MemberResponsibleLimitsBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
