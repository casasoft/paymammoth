using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.BackgroundImageModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CategoryModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Cms.BackgroundImageModule
{

    //BaseCmsInfo-Class

    public abstract class BackgroundImageBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.BackgroundImageBaseCmsInfo_AutoGen
    {
        public BackgroundImageBaseCmsInfo(BackgroundImageBase item)
            : base(item)
        {

        }

        private void initImages()
        {
            this.Image = this.AddPropertyForMediaItem<BackgroundImageBase>(item => item.Image, isRequired: true);
        }

        public CmsPropertyMediaItem<BackgroundImageBase> Image { get; private set; }
        public CmsPropertyLiteral<BackgroundImageBase> CategoryName { get; private set; }

        protected override void customInitFields()
        {
            //custom init field logic here
            initImages();
            addCategoryName();
            Image.ShowInEdit = true;
            Image.ShowInListing = true;
            CategoryLink.ShowInEdit = true;
            CategoryName.ShowInEdit = true;
            CategoryName.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            CategoryName.ShowInListing = true;
            ListingOrderPriorities.AddColumnsToStart(ID, Image, CategoryName);
            base.customInitFields();
        }

        private void addCategoryName()
        {
            CmsPropertyLiteral<BackgroundImageBase> p = new CmsPropertyLiteral<BackgroundImageBase>(this,
                "Category",
            item =>
            {
                CategoryBase cat = this.DbItem.CategoryLink;
                MySpan span = new MySpan();
                if (cat != null)
                {
                    span.InnerText = !string.IsNullOrWhiteSpace(cat.TitlePlural) ? cat.TitlePlural : cat.TitleSingular;
                }
                else
                {
                    span.InnerText = "N/A";
                }
                return span;
            });
            this.CategoryName = this.AddProperty(p);
        }
    }
}
