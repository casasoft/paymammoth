using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.OrderModule;

namespace CS.WebComponentsGeneralV3.Cms.OrderModule
{
    public abstract class OrderBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.OrderBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToDelete.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToAdd.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Cart;
        }

        
    }
}
