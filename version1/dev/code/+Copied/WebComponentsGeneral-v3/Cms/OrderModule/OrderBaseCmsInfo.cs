using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes;

using NHibernate.Criterion;

using BusinessLogic_v3.Modules.OrderModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3.Cms.OrderModule
{
    public abstract class OrderBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.OrderBaseCmsInfo_AutoGen
    {
        public OrderBaseCmsInfo(OrderBase item)
            : base(item)
        {

        }

        public override IEnumerable<CmsItemSpecificOperation> GetCustomCmsOperations()
        {
            if (this.DbItem.IsConfirmed)
            {
                this.CustomCmsOperations.Add(
                    new CmsItemSpecificOperation(this, "View Invoice Details", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Listing,
                        this.DbItem.ToFrontendBase().GetURL()));
            }
            addCustomOperation_MarkAsPaid();
            addCustomOperation_CancelOrder();
            return base.GetCustomCmsOperations();
        }

        private void addCustomOperation_MarkAsPaid()
        {
            var loggedInUser = getLoggedCmsUser();
            if (loggedInUser != null && this.DbItem != null && !this.DbItem.IsTransient() &&
                loggedInUser.CheckAccessTypeAtLeast(General_v3.Enums.CMS_ACCESS_TYPE.Administrator) && !this.DbItem.IsPaid())
            {
                this.CustomCmsOperations.Add(
                   new CmsItemSpecificOperation(this, "Mark as paid", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.EuroSign,markAsPaid));
            }
        }
        private void markAsPaid()
        {
            this.DbItem.MarkAsPaid("");
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Order maked as paid successfully", General_v3.Enums.STATUS_MSG_TYPE.Success);
        }
        private void addCustomOperation_CancelOrder()
        {
            var loggedInUser = getLoggedCmsUser();
            if (loggedInUser != null && this.DbItem != null && !this.DbItem.IsTransient() &&
                loggedInUser.CheckAccessTypeAtLeast(General_v3.Enums.CMS_ACCESS_TYPE.Administrator) && !this.DbItem.IsPaid())
            {
                var op = new CmsItemSpecificOperation(this, "Cancel Order", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Delete, cancelOrder);
                op.ConfirmMessage = "Are you sure you want to cancel order?";
                this.CustomCmsOperations.Add(op);
            }
        }
        private void cancelOrder()
        {
            this.DbItem.MarkAsCancelled();
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Order cancelled successfully", General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

        private void addTotalCostField()
        {


            this.TotalCost = this.AddProperty(new CmsCustomProperty<OrderBaseCmsInfo, double>(this, "Total Cost",
                item=>item.DbItem.GetTotalPrice(),
                null,null,null));
                
                
                
        }
        private void addCustomerFullNameField()
        {
            
            
            this.CustomerFullName = this.AddProperty(new CmsCustomProperty<OrderBaseCmsInfo, string>(this, "Name",
                item=> item.DbItem.GetCustomerFullName(),
                null,

              //  searchVal => NHibernate.Criterion.Projections.Property<
                
                
                null, //search

                (crit, sortType) => 
                    {
                        crit.AddOrder(new NHibernate.Criterion.Order(Projections.Property<OrderBase>(order => order.CustomerFirstName), (sortType == CS.General_v3.Enums.SORT_TYPE.Ascending)))
                            .AddOrder(new NHibernate.Criterion.Order(Projections.Property<OrderBase>(order => order.CustomerLastName), (sortType == CS.General_v3.Enums.SORT_TYPE.Ascending)));
                    }));
        }
                
                
        
        protected override void customInitFields()
        {
            var user = getLoggedCmsUser();
            base.customInitFields();
            addTotalCostField();
            addCustomerFullNameField();
            this.OrderCurrency.ShowInEdit = true;
            //this.Cancelled.ShowInListing = true;
            //this.CustomerName.ShowInListing = true;
            //this.CustomerSurname.ShowInListing = true;
            //this.CustomerFullName.ShowInListing = true;
          //  this.TotalCost.ShowInListing = true;
            this.OrderItems.ShowLinkInCms = true;
            //this.PaymentTransactions.ShowLinkInCms = true;
            this.CustomerEmail.ShowInListing = true;
            this.DateCreated.ShowInListing = true;
            this.Member.ShowInListing = true;
            this.OrderStatus.SetShowInListing(true);
            this.Paid.ShowInListing = true;
            this.Paid.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.IsTestOrder.HelpMessage = "If this is marked as true, this means that this order is a test order.  It will not be used for reporting purposes";
            //this.CheckedByAdmin.EditableInListing = true;
            //this.PaidOn.ShowInListing = true;
            this.Remarks.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;
            this.Reference.ShowInListing = true;
            this.Member.IsSearchable = false;
            this.SetDefaultSortField(this.LastUpdatedOn, CS.General_v3.Enums.SORT_TYPE.Descending);
            this.CustomerFullName.ShowInEdit = false;
            this.LastUpdatedOn.ShowInListing = true;
            this.LinkedAffiliate.ShowInEdit = true;
            this.LinkedAffiliate.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.PaymentMethod.ShowInListing = true;
            this.TotalCost.ShowInListing = true;

            this.ListingOrderPriorities.AddColumnsToStart(this.LastUpdatedOn, this.DateCreated, this.Reference, this.CustomerFullName, this.Member, this.CustomerEmail, this.TotalCost, this.Paid, this.PaidOn, this.PaymentMethod, this.TotalCost, this.OrderStatus);
            this.EditOrderPriorities.AddColumnsToStart(this.ID, this.DateCreated, this.Reference, this.CustomerFirstName, this.CustomerLastName, this.CustomerEmail,
                this.OrderStatus,
                this.CustomerAddress1, this.CustomerAddress2, this.CustomerAddress3,
                this.CustomerLocality, this.CustomerState, this.CustomerPostCode,
                this.CustomerCountry, this.AuthCode, this.TotalCost, this.Cancelled, this.CancelledOn, this.Remarks);
            this.IsShoppingCart.SetDefaultSearchValue(false);
            this.Remarks.IsSearchable = false;
            this.CreatedByAdmin.IsSearchable = false;
            this.GUID.IsSearchable = false;
            this.ItemID.IsSearchable = false;
            this.ItemType.IsSearchable = false;
            this.IsShoppingCart.IsSearchable = false;
            this.FixedDiscount.IsSearchable = false;
            this.TotalShippingCost.IsSearchable = false;
            this.AuthCode.IsSearchable = false;

            if (user == null || (user.CheckAccessIsLessThan(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)))
            {
                this.ItemID.ShowInEdit = false;
                this.ItemType.ShowInEdit = false;
                this.AuthCode.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, null);
                this.IsShoppingCart.ShowInEdit = false;

            }


        }
        public CmsCustomProperty<OrderBaseCmsInfo, double> TotalCost { get; private set; }
        public CmsCustomProperty<OrderBaseCmsInfo, string> CustomerFullName { get; private set; }
	}
}
