using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ShippingRatePriceModule;

namespace CS.WebComponentsGeneralV3.Cms.ShippingRatePriceModule
{
    public abstract class ShippingRatePriceBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.ShippingRatePriceBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
    }
}
