using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EventBasicModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.EventBasicModule
{

//BaseCmsInfo-Class

    public abstract class EventBasicBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.EventBasicBaseCmsInfo_AutoGen
    {
    	public EventBasicBaseCmsInfo(EventBasicBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
		    initMediaItem();
            this.Title.IsRequired = true;
            this.DescriptionHtml.IsRequired = true;
            this.DescriptionHtml.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.Summary.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.ID.ShowInListing = true;
            this.Title.ShowInListing = true;
            this.StartDate.ShowInListing = true;
            this.EndDate.ShowInListing = true;
            this.Location.ShowInListing = true;
		    this.ListingOrderPriorities.AddColumnsToStart(ID, Image, Title, StartDate, EndDate, Location);
            this.EventMediaItems.ShowLinkInCms = true;

		    this.EditOrderPriorities.AddColumnsToStart(Title, StartDate, EndDate, Location, Summary, DescriptionHtml, Image);

            base.customInitFields();
        }

        private void initMediaItem()
        {
            Image = this.AddPropertyForMediaItem<EventBasicBase>(item => item.Image, true, true);
        }
        public CmsPropertyMediaItem<EventBasicBase> Image { get; set; }
	}
}
