using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.ImageSizingInfoModule
{

//BaseCmsFactory-Class

    public abstract class ImageSizingInfoBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.ImageSizingInfoBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            cmsInfo.ShowInCmsMainMenu = true;
            cmsInfo.SetAllMinimumAccessRequiredAs(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam);

            base.getUserSpecificGeneralInfo(cmsInfo);
        }

    }
}
