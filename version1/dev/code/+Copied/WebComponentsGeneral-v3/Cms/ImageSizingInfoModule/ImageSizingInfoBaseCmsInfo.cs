using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.ImageSizingInfoModule
{

//BaseCmsInfo-Class

    public abstract class ImageSizingInfoBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ImageSizingInfoBaseCmsInfo_AutoGen
    {
    	public ImageSizingInfoBaseCmsInfo(ImageSizingInfoBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            this.Title.ShowInListing = true;
            this.MaximumWidth.EditableInListing = true;
            this.MaximumHeight.EditableInListing = true;
		    this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.MaximumWidth, this.MaximumHeight);
            this.SizingInfos.ShowLinkInCms = true;


            base.customInitFields();
        }
	}
}
