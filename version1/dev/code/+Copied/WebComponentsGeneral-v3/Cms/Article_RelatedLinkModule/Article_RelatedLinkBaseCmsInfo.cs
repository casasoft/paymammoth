using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.Article_RelatedLinkModule;

namespace CS.WebComponentsGeneralV3.Cms.Article_RelatedLinkModule
{
    public abstract class Article_RelatedLinkBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.Article_RelatedLinkBaseCmsInfo_AutoGen
    {
    	public Article_RelatedLinkBaseCmsInfo(Article_RelatedLinkBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
