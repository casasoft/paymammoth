using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.EventCategoryModule;

namespace CS.WebComponentsGeneralV3.Cms.EventCategoryModule
{
    public abstract class EventCategoryBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.EventCategoryBaseCmsInfo_AutoGen
    {
    	public EventCategoryBaseCmsInfo(EventCategoryBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
