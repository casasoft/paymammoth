using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.EventSessionPeriodModule;

namespace CS.WebComponentsGeneralV3.Cms.EventSessionPeriodModule
{
    public abstract class EventSessionPeriodBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.EventSessionPeriodBaseCmsInfo_AutoGen
    {
    	public EventSessionPeriodBaseCmsInfo(EventSessionPeriodBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
		    this.ID.ShowInListing = true;
            this.StartDate.ShowInListing = true;
            this.EndDate.ShowInListing = true;
		    this.ListingOrderPriorities.AddColumnsToStart(ID, StartDate, EndDate);
            this.EventSessionPeriodDays.ShowLinkInCms = true;
            base.customInitFields();
            
        }
	}
}
