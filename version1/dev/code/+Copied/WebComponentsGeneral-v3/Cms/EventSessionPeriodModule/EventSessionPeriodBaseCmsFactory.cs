using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.EventSessionPeriodModule;

namespace CS.WebComponentsGeneralV3.Cms.EventSessionPeriodModule
{
    public abstract class EventSessionPeriodBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.EventSessionPeriodBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
        
        protected override void initCustomCmsOperations()
        {    //here you should initialise any custom cms operations.  The main reason being that if this is
        //called in the initCmsParameters, you may not yet know the base url
            
            base.initCustomCmsOperations();
        }
    }
}
