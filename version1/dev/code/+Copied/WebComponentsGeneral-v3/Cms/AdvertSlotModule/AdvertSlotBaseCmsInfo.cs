using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.AdvertSlotModule;

namespace CS.WebComponentsGeneralV3.Cms.AdvertSlotModule
{
    public abstract class AdvertSlotBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.AdvertSlotBaseCmsInfo_AutoGen
    {


        public AdvertSlotBaseCmsInfo(AdvertSlotBase item)
            : base(item)
        {
            
            this.AccessTypeRequired_ToDelete.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
        }

        protected override void customInitFields()
        {
            base.customInitFields();
            this.Height.ShowInListing = true;
            this.Title.ShowInListing = true;
            this.Width.ShowInListing = true;
            this.Adverts.ShowLinkInCms = true;
            this.Identifier.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.RoundNo.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);

            this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.Width, this.Height);

        }
	}
}
