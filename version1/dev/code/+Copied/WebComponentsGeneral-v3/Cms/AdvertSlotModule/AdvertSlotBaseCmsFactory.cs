using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.AdvertSlotModule;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

namespace CS.WebComponentsGeneralV3.Cms.AdvertSlotModule
{
    public abstract class AdvertSlotBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.AdvertSlotBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToAdd.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Advert;
        }
    }
}
