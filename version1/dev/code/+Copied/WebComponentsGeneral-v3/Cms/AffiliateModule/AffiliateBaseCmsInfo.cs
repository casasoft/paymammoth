using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.AffiliateModule;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Extensions;



using BusinessLogic_v3.Modules.AffiliateModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Cms.AffiliateModule
{
    public abstract class AffiliateBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.AffiliateBaseCmsInfo_AutoGen
    {
    	public AffiliateBaseCmsInfo(AffiliateBase item)
            : base(item)
        {
            this.AccessTypeRequired_ToEdit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser, CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal, BusinessLogic_v3.Enums.CmsUserRoleEnum.Affiliate);
            this.AccessTypeRequired_ToView.CustomAccess += new CmsAccessType.CustomAccessDelegate(AccessTypeRequired_ToView_CustomAccess);
            
        }

        bool AccessTypeRequired_ToView_CustomAccess(ICmsUserBase loggedUser)
        {
            bool ok = true;
            if (loggedUser != null && loggedUser.AccessType == CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser)
            {
                if (this.DbItem.LinkedCmsUser != loggedUser)
                {
                    ok = false;
                }
            }
            
            return ok;
        }
    
		protected override void customInitFields()
        {
            addAffiliateLink();
            this.AffiliatePaymentInfos.ShowLinkInCms = true;
            //custom init field logic here
            initImages();
            this.Title.IsRequired = true;
            this.AffiliateLink.ShowInEdit = true;
            this.Priority.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.AffiliateWhiteLabelBaseUrl.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.ReferralCode.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.AffiliateUrl.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
		    this.AffiliateLink.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.Title.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser);
            this.Password.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser);
            this.Username.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser;
            this.CssFilename.ShowInEdit = false;
            this.LogoFilename.ShowInEdit = false;
            this.Logo.ShowInEdit = true;
            this.CommissionRate.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.CommissionRate.ShowInListing = true;
            this.ReferralCode.HelpMessage = "This is the referral code which is added as part of the Url, which identifies a user to an affiliate";
            this.AffiliateCode.HelpMessage = "An internal code for your reference, to identify the affiliate with";
            this.AffiliateUrl.HelpMessage = "The url to the affiliate's website";
            this.AffiliateUrl.Label = "Affiliate Website";
            this.HasWhiteLabellingFunctionality.HelpMessage = @"If applicable, if this is ticked, this affiliate will have 'white-labelling' functionality.  White-labelling means where the site will be styled according to 
the uploaded Css file, and can match the look-and-feel of the affiliate";
            if (!BusinessLogic_v3.Modules.ModuleSettings.Affiliates.WhiteLabellingFunctionalityEnabled)
            {
                this.HasWhiteLabellingFunctionality.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
                this.Css.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
                this.AffiliateWhiteLabelBaseUrl.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);

            }
            else
            {
                this.LinkedContentPageNode.ShowInEdit = true;
            }
            this.LinkedContentPageNode.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.AffiliateWhiteLabelBaseUrl.HelpMessage = "The white-labelling url for the affiliate, e.g http://www.myaffiliate.com/";

            this.LinkedCmsUser.ShowInEdit = true;
            this.LinkedCmsUser.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);

		    this.CommissionRate.HelpMessage = "Commision Rate (Percentage).  For example, for 15%, enter 15.";
            this.Username.IsRequired = true;
            this.Password.IsRequired = true;
            this.Title.ShowInListing = true;
            this.Username.ShowInListing = true;
            this.AffiliateLink.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.Username, this.AffiliateLink, this.CommissionRate);
            this.EditOrderPriorities.AddColumnsToStart(this.Title, this.AffiliateCode, this.AffiliateLink, this.Username, this.Password, this.ReferralCode, this.Logo, this.Css);
            this.AffiliateCode.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.Username.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.Activated.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.ActiveFrom.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.AffiliateCode.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.Title.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.Css.AccessTypeRequired_View.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);

            if (this.DbItem == null || this.DbItem._Temporary_Flag)
            {
                this.LinkedCmsUser.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            }
            base.customInitFields();

            

        }

        private void addAffiliateLink()
        {
            CmsPropertyLiteral<AffiliateBase> p = new CmsPropertyLiteral<AffiliateBase>(this,
                "Affiliate Link",
            item =>
                {
                    AffiliateBase affiliateFrontend = item;

                    MyAnchor a = new MyAnchor();
                    a.CssManager.AddClass("affiliate-link");
                    a.Href = affiliateFrontend.GetAffiliateFrontpageUrl();

                    a.HrefTarget = CS.General_v3.Enums.HREF_TARGET.Blank;
                    a.InnerText = a.Href;
                    return a;
                });



            this.AffiliateLink = this.AddProperty(p);
            //this.AffiliateLink = t
        }


        private void initImages()
        {
            
            this.Logo = this.AddPropertyForMediaItem<AffiliateBase>(item => item.Logo, false);
            this.Css = this.AddPropertyForMediaItem<AffiliateBase>(item => item.Css, false);
        }

        public CmsPropertyLiteral<AffiliateBase> AffiliateLink { get; private set; }


        public CmsPropertyMediaItem<AffiliateBase> Logo { get; private set; }
        public CmsPropertyMediaItem<AffiliateBase> Css { get; private set; }
        
	}
}
