using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.AffiliateModule;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.AffiliateModule
{
    public abstract class AffiliateBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.AffiliateBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
           
            base.initCmsParameters();

            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToAdd.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.UserSpecificGeneralInfoInContext.SetAllMinimumAccessRequiredAs(CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser);
            this.UserSpecificGeneralInfoInContext.SetAllAccessLevelRequiredToOverrideRolesAs(CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal);
            this.UserSpecificGeneralInfoInContext.AddAccessRoleRequiredToAll(Enums.CmsUserRoleEnum.Affiliate);
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Link;
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }

        protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
            var q = AffiliateBase.Factory.GetQuery(qParams);
            var loggedInUser = (CmsUserBase)getCurrentLoggedUser();

            if (loggedInUser.GetUserAccessType() == CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser)
            {
                q.Where(item => item.LinkedCmsUser == loggedInUser);
            }
            return q;
        }
        protected override void initCustomCmsOperations()
        {    //here you should initialise any custom cms operations.  The main reason being that if this is
        //called in the initCmsParameters, you may not yet know the base url
            
            base.initCustomCmsOperations();
        }
    }
}
