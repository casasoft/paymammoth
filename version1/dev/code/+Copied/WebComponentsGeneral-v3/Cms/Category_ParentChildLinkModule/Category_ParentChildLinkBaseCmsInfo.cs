using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;

namespace CS.WebComponentsGeneralV3.Cms.Category_ParentChildLinkModule
{
    public abstract class Category_ParentChildLinkBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.Category_ParentChildLinkBaseCmsInfo_AutoGen
    {
    	public Category_ParentChildLinkBaseCmsInfo(Category_ParentChildLinkBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
