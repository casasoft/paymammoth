using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CultureDetailsModule;

namespace CS.WebComponentsGeneralV3.Cms.CultureDetailsModule
{
    public abstract class CultureDetailsBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.CultureDetailsBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToAdd.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Flags;
        }
    }
}
