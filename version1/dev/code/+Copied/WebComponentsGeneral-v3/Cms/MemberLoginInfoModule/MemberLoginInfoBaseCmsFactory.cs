using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.MemberLoginInfoModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.MemberLoginInfoModule
{

//BaseCmsFactory-Class

    public abstract class MemberLoginInfoBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.MemberLoginInfoBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
        }

    }
}
