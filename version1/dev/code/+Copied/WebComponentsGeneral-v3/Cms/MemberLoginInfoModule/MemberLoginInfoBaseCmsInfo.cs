using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberLoginInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.MemberLoginInfoModule
{

//BaseCmsInfo-Class

    public abstract class MemberLoginInfoBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.MemberLoginInfoBaseCmsInfo_AutoGen
    {
    	public MemberLoginInfoBaseCmsInfo(MemberLoginInfoBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            this.DateTime.ShowInListing = true;
            this.SetShowInListingAndOrder(this.DateTime, this.LoginInfoType, this.IP, this.AdditionalMsg);
            this.EditOrderPriorities.AddColumnsToStart(this.DateTime, this.LoginInfoType, this.IP, this.AdditionalMsg);
            this.AccessTypeRequired_ToEdit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            base.customInitFields();
        }
	}
}
