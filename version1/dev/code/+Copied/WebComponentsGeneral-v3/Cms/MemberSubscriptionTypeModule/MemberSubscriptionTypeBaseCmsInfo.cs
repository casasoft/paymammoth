using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.MemberSubscriptionTypeModule;

namespace CS.WebComponentsGeneralV3.Cms.MemberSubscriptionTypeModule
{
    public abstract class MemberSubscriptionTypeBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.MemberSubscriptionTypeBaseCmsInfo_AutoGen
    {
    	public MemberSubscriptionTypeBaseCmsInfo(MemberSubscriptionTypeBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            this.MemberSubscriptionTypePrices.ShowLinkInCms = true;
            this.Title.ShowInListing = true;
            base.customInitFields();
        }
	}
}
