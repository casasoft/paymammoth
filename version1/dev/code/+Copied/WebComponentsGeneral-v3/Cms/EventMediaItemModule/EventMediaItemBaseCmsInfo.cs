using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.EventMediaItemModule;
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.EventMediaItemModule;

namespace CS.WebComponentsGeneralV3.Cms.EventMediaItemModule
{
    public abstract class EventMediaItemBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.EventMediaItemBaseCmsInfo_AutoGen
    {
    	public EventMediaItemBaseCmsInfo(EventMediaItemBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            
            ImageFilename.ShowInEdit = false;
            ImageFilename.ShowInListing = false;
            initMediaItem();
        }

        private void initMediaItem()
        {
            Image = this.AddPropertyForMediaItem<EventMediaItemBase>(item => item.Image, true, true);
        }
        public CmsPropertyMediaItem<EventMediaItemBase> Image { get; set; }

	}
}
