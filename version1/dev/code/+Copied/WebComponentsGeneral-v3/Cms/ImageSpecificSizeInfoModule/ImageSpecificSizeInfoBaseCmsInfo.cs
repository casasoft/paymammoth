using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.ImageSpecificSizeInfoModule
{

//BaseCmsInfo-Class

    public abstract class ImageSpecificSizeInfoBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ImageSpecificSizeInfoBaseCmsInfo_AutoGen
    {
    	public ImageSpecificSizeInfoBaseCmsInfo(ImageSpecificSizeInfoBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            this.Height.EditableInListing = true;
		    this.ImageFormat.EditableInListing = true;
            this.Title.ShowInListing = true;
            this.Width.EditableInListing = true;
		    this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.Height, this.Width, this.CropIdentifier, this.ImageFormat);
            this.CropIdentifier.EditableInListing = true;

            base.customInitFields();
        }
	}
}
