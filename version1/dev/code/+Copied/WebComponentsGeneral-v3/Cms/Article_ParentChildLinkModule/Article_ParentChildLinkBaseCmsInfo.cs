using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;

namespace CS.WebComponentsGeneralV3.Cms.Article_ParentChildLinkModule
{
    public abstract class Article_ParentChildLinkBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.Article_ParentChildLinkBaseCmsInfo_AutoGen
    {
    	public Article_ParentChildLinkBaseCmsInfo(Article_ParentChildLinkBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            
            base.customInitFields();
        }
	}
}
