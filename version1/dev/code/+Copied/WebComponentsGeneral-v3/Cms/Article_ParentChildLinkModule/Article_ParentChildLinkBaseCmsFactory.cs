using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

namespace CS.WebComponentsGeneralV3.Cms.Article_ParentChildLinkModule
{
    public abstract class Article_ParentChildLinkBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.Article_ParentChildLinkBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
    }
}
