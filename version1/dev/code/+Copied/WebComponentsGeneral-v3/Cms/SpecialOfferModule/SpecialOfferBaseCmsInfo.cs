using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.SpecialOfferModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3.Cms.SpecialOfferModule
{
    public abstract class SpecialOfferBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.SpecialOfferBaseCmsInfo_AutoGen
    {
        public SpecialOfferBaseCmsInfo(SpecialOfferBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();
            this.SpecialOfferVoucherCodes.ShowLinkInCms = true;
            this.Name.ShowInListing = true;
            this.DateFrom.ShowInListing = true;
            this.DateTo.ShowInListing = true;
          //  this.DiscountFixed.ShowInListing = true;
            this.DiscountPercentage.ShowInListing = true;
            this.Activated.EditableInListing = true;
          //  this.Category.ShowInListing = true;
            this.QuantityLeft.ShowInListing = true;
          //  this.Category.ShowInEdit = true;
            this.Title.IsRequired = true;
            this.QuantityLeft.IsRequired = true;
            //this.DateFrom.IsRequired = true;
            //this.DateTo.IsRequired = true;
            this.IsExclusiveOffer.ShowInListing = true;
            this.DateFrom.HelpMessage = "The date from which this special offer is valid. If left empty, it means that it is available from any day";
            this.DateTo.HelpMessage = "The date up to which this special offer is valid.  If left empty, it means this offer is valid indefinitely";
            this.DiscountPercentage.HelpMessage = "The discount in % applied for items matching this special offer";
           // this.DiscountFixed.HelpMessage = "The fixed discount applies if any items match this offer. A fixed discount is applied only ONCE";
            this.QuantityLeft.HelpMessage = "The total times this special offer can be used.  If indefinite, put in a very large number like 999999";
            this.IsExclusiveOffer.HelpMessage = "Whether this offer is exclusive.  If a cart matches various exclusive offers, only the 'best' of the exclusive offers will apply";
            this.RequirementsCanMatchAnyItem.HelpMessage = @"Whether the requirements of this offer can match any item, not just those in the applicable categories.  
                    Basically, if ticked this allows you to do an offer of say 'Spend �50 and get 20% off any dresses bought'.  The �50 can be from any section, not just dresses";

            this.Name.HelpMessage = "This is your internal name to identify this offer";
            this.Title.HelpMessage = "This is the 'title' the user sees.  (Multilingual)";
            this.RequiresPromoCode.ShowInListing = true;
           // this.DiscountFixed.IsRequired = true;
            this.DiscountPercentage.IsRequired = true;
            //this.ApplicableOrders.ShowInEdit = false;
            
            this.RequiredTotalItemsToBuy.HelpMessage = "The amount of items to buy - This allows you to create offers of the type 'Buy 2 dresses and get 10% off'";
            this.RequiredTotalToSpend.HelpMessage = "The amount of total to spend - This allows you to create offers of the type 'Spend �50 and get �5 off'";

            this.ApplicableCategories.FormGroup = "Product Restrictions";

            this.IsExclusiveOffer.FormGroup= this.RequirementsCanMatchAnyItem.FormGroup = this.RequiresPromoCode.FormGroup = 
                this.RequiredTotalItemsToBuy.FormGroup = 
                this.RequiredTotalToSpend.FormGroup = "Requirements";

            this.RequiresPromoCode.HelpMessage = "Whether this offer requires a promotional code in order to be used";
            this.ApplicableCategories.HelpMessage = "Applicable categories. If specified, this offer will only be valid for the selected categories. E.g, only on 'Dresses'.  Can select multiple categories";
            
            this.DiscountPercentage.FormGroup = "Discount Information";
            this.Description.StringDataType = General_v3.Enums.STRING_DATA_TYPE.Html;
            this.ListingOrderPriorities.AddColumnsToStart(this.Name,  this.DateFrom, this.DateTo,  this.DiscountPercentage, this.QuantityLeft, this.IsExclusiveOffer, this.RequiresPromoCode, this.Activated);
            this.EditOrderPriorities.AddColumnsToStart(this.Name, this.Title, this.Description, this.DateFrom,this.DateTo, this.DiscountPercentage, this.DiscountPercentage, this.RequiredTotalItemsToBuy,
                this.RequiredTotalToSpend, this.RequiresPromoCode,
                this.QuantityLeft, this.Activated);

            addCustomButtons();
        }
        private void addCustomButtons()
        {
            addGenerateVoucherCodesButton();
        }

        private void addGenerateVoucherCodesButton()
        {
            var cmsUser = getLoggedCmsUser();
            if (cmsUser != null && cmsUser.CheckAccessTypeAtLeast(General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator))
            {
                this.CustomCmsOperations.Add(new CmsItemSpecificOperation(this,
                    "Generate 25 Voucher Codes (8-letters)", Code.Cms.EnumsCms.IMAGE_ICON.Generate, generateVoucherCodes));
            }




        }
        private void generateVoucherCodes()
        {
            BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeGenerator gen = new BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeGenerator(this.DbItem);
            var codes = gen.GenerateRandomCodes(totalToGenerate: 25, length: 8);
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Voucher codes generated successfully", General_v3.Enums.STATUS_MSG_TYPE.Success);
        }
    }
}
