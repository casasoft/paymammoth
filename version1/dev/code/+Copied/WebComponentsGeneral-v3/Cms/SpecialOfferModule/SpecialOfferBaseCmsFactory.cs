using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.SpecialOfferModule;

namespace CS.WebComponentsGeneralV3.Cms.SpecialOfferModule
{
    public abstract class SpecialOfferBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.SpecialOfferBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Present_Gift;
        }
    }
}
