using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Util;
using NHibernate.Criterion;

using BusinessLogic_v3.Modules.SettingModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using BusinessLogic_v3.Modules.AuditLogModule;
using log4net;
using BusinessLogic_v3.Classes.LuceneClasses;

namespace CS.WebComponentsGeneralV3.Cms.SettingModule
{
    public abstract class SettingBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.SettingBaseCmsFactory_AutoGen
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(SettingBaseCmsFactory));
        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
            this.UserSpecificGeneralInfoInContext.SetAllMinimumAccessRequiredAs(CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal);
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Tools;
            addCustomOperations();
        }

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        private void addCustomOperations()
        {
            var loggedUser = getLoggedCmsUser();
            this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(new CmsGeneralOperation("Clear Frontend Cache", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Delete, _clearAppCache));
            this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(new CmsGeneralOperation("Send Test Email", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Email, CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsRoutesMapper.Instance.GetSettings_SendTestEmailPage()));

            {
                if (loggedUser != null &&  loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator))
                {
                    var op = new CmsGeneralOperation("Restart Application", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.ReOrder, restartApplication);
                    op.ConfirmMessage = "Are you sure you want to restart the application?  This might log out all users!";
                    this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
                }

            }
            {
                if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam))
                {
                    var op = new CmsGeneralOperation("Mark All Settings As Not Used In Project", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Update, 
                        BusinessLogic_v3.Modules.Factories.SettingFactory.MarkAllSettingsAsNotUsedInProject);
                    op.ConfirmMessage = "Are you sure you want to mark all settings as not used in project?  Settings will be reactivated once accessed by anybody.";
                    this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
                }

            }
            {
                if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam))
                {
                    var op = new CmsGeneralOperation("Remove Duplicate Settings", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Update, deleteDuplicateSettings
                        );
                    op.ConfirmMessage = "Are you sure you want to delete all duplicate settings in project?";
                    this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
                }

            }
            {
                if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft))
                {
                    var op = new CmsGeneralOperation("Delete Extra Audit Log Entries", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Delete, deleteExtraAuditLogEntries
                        );
                    op.ConfirmMessage = "Are you sure you want to delete all extra audit log entries?";
                    this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
                }

            }
            {
                if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam))
                {
                    var op = new CmsGeneralOperation("Refresh ALL Lucene Indexes", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.ReOrder, refreshAllLuceneIndexes);
                    op.ConfirmMessage = "This will refresh ALL lucene indexes.  This might take quite some time, based on the amount of data.  Are you sure?";
                    this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
                }

            }
        }

        private void refreshAllLuceneIndexes()
        {
            LuceneUtil.RecreateAllLuceneIndexes();
            CmsUtil.ShowStatusMessageInCMS("Lucene indexes refreshed successfully", General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

        private void deleteExtraAuditLogEntries()
        {
            _log.Debug("deleteExtraAuditLogEntries() - cms [START]");
             int result = AuditLogBaseFactory.Instance.DeleteExtraLogEntries();
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Audit log extra entries removed successfully. Result: " + result, General_v3.Enums.STATUS_MSG_TYPE.Success);

        }

        private void deleteDuplicateSettings()
        {
            BusinessLogic_v3.Modules.Factories.SettingFactory.DeleteDuplicateSettings();
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Duplicate settings has been deleted successfully", General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

        private void restartApplication()
        {
            System.Web.HttpRuntime.UnloadAppDomain();
        }

        //
        private void _clearAppCache()
        {
            CS.General_v3.Classes.Caching.CustomCacheDependencyController.Instance.InvalidateAll();
            CS.General_v3.Util.PageUtil.ClearAllCacheInContext();
            CachingUtil.InvalidateAllPagesOuputCachingDependency();
            //HttpContext.Current.Cache.Remove("AllPages");
            //HttpContext.Current.Cache.Insert("AllPages", DateTime.Now, null, System.DateTime.MaxValue, System.TimeSpan.Zero,
              //  System.Web.Caching.CacheItemPriority.NotRemovable, null);
            CS.WebComponentsGeneralV3.Code.Cms.Util.CmsUtil.ShowStatusMessageInCMS("Frontend cache cleared successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
            
            
        }

        protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
            var q =base._getQueryForSearchResults(qParams);
            q.RootCriteria.Add(NHibernate.Criterion.Restrictions.IsEmpty(Projections.Property<SettingBase>(item => item.ChildSettings).PropertyName));
            return q;

        }

        

    }
}
