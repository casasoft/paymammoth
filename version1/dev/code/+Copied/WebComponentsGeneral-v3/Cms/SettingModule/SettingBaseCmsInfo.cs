using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.SettingModule;

namespace CS.WebComponentsGeneralV3.Cms.SettingModule
{
    public abstract class SettingBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.SettingBaseCmsInfo_AutoGen
    {
        public SettingBaseCmsInfo(SettingBase item)
            : base(item)
        {

        }

        

        protected override void customInitFields()
        {
            base.customInitFields();
           // this.ID.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            bool localMachine = CS.General_v3.Util.Other.IsLocalTestingMachine;
            //localMachine = t;
            //this.Name.ShowInListing = true;
            //this.Name.EditableInListing = false;
            this.Value.ShowInListing = true;
            this.Value.EditableInListing = true;
            //this.Value.WidthInListing = 500;

            if (localMachine)
            {
                this.LocalhostValue.EditableInListing = this.LocalhostValue.ShowInListing = true;
            }

            this.Identifier.ShowInListing = true;
            this.UsedInProject.SetDefaultSearchValue(true);
            this.ListingOrderPriorities.AddColumnsToStart(this.Identifier, this.Name, this.Value, this.LocalhostValue);
            this.SetDefaultSortField(this.Identifier, CS.General_v3.Enums.SORT_TYPE.Ascending);
            this.Value.WidthInListing = 500;
            this.LocalhostValue.WidthInListing = 300;
            if (localMachine)
            {
                this.Value.CssClassForListingEditField.AddClass(CS.WebComponentsGeneralV3.Code.Cms.CmsConstants.CSS_FIELD_NORMAL);
            }
            else
            {
                this.Value.CssClassForListingEditField.AddClass(CS.WebComponentsGeneralV3.Code.Cms.CmsConstants.CSS_FIELD_LARGER);
            }
            this.LocalhostValue.CssClassForListingEditField.AddClass(CS.WebComponentsGeneralV3.Code.Cms.CmsConstants.CSS_FIELD_NORMAL);

        }
	}
}
