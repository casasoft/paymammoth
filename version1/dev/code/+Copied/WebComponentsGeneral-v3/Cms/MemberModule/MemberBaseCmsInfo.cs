using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules.MemberModule.SessionManager;
using CS.General_v3.Classes.URL;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace CS.WebComponentsGeneralV3.Cms.MemberModule
{
    public abstract class MemberBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.MemberBaseCmsInfo_AutoGen
    {
        public MemberBaseCmsInfo(MemberBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();
            initMediaItems();
            this.DateRegistered.ShowInListing = true;
            this.FirstName.ShowInListing = true;
            this.LastName.ShowInListing = true;
            this.Username.ShowInListing = true;
            this.Email.ShowInListing = true;
            this.Telephone.ShowInListing = true;
            this.Mobile.ShowInListing = true;
            this.Orders.ShowLinkInCms = true;
            this._LastLoggedIn_New.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.SessionGUID.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.ForgottenPassCode.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.AccountBalance.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);//you cannot edit,as it must be done from a seperate page to enter a message
            this.Password.HelpMessage = "Please note that the password is 'hashed', so you cannot know what the password is.  However, if you enter a new password, it will be saved and the hashed value will be visible in this field";
            this.ListingOrderPriorities.AddColumnsToStart(this.DateRegistered, this.FirstName, this.LastName, this.Username, this.Email, this.Telephone, this.Mobile);
            this.PasswordSalt.IsSearchable = false;
            this.UsernameEncryptionSalt.IsSearchable = false;
            this.UsernameEncryptionIterations.IsSearchable = false;
            this.EditOrderPriorities.AddColumnsToStart(this.ID, this.DateRegistered, this.Username,
                this.Password, this.FirstName, this.MiddleName, this.LastName, this.IDCard, this.Gender, this.DateOfBirth, this.Email, this.Address1, this.Address2, this.Address3,
                this.Locality, this.State, this.PostCode, this.Country, this.Telephone, this.Mobile,this.Fax, this.LastLoggedIn,
                this.KYCProcessStarted, this.KYCProcessStartedOn, this.KYCProofOfAddress,this.KYCProofOfIdentity, this.KYCVerified, this.KYCVerifiedOn);
            this.IsTestMember.HelpMessage = "If this is marked as true, this means that this member is a test member.  It will not be used for reporting purposes";
            this.KYCProofOfAddress.HelpMessage = "Upload a document showing proof of address of user";
            this.KYCProofOfIdentity.HelpMessage = "Upload a document showing proof of identity of user";
            this.KYCVerified.HelpMessage = "This is a flag which should be amended manually, whenever a user has been verified successfully";
            this.KYCVerifiedOn.HelpMessage = "Enter the date when the member has been verified.  If this is left null, but the member is marked as verified, then the current date is taken automatically";
            this.KYCProcessRemarks.HelpMessage = "Any remarks regarding the KYC procedure should go here";
            this.KYCProcessRemarks.StringDataType = General_v3.Enums.STRING_DATA_TYPE.MultiLine;
            this.KYCProcessStartedOn.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);

            this.LoginInfo.ShowLinkInCms = true;

            this.AccountBalanceHistoryItems.ShowLinkInCms = true;
            this.PasswordSalt.ShowInEdit = false;
            this.PasswordEncryptionType.ShowInEdit = false;
            this.CompanyProfile.StringDataType = General_v3.Enums.STRING_DATA_TYPE.Html;
            this.SetDefaultSortField(this.DateRegistered, General_v3.Enums.SORT_TYPE.Descending);
            this.Image.ShowInEdit = true;
            this.ImageFilename.ShowInEdit = false;
            addCustomOperations();
        }

        private void loginAsMember()
        {
            if (MemberSessionLoginManager.Instance != null && MemberBaseFactory.Instance != null)
            {
                MemberSessionLoginManager.Instance.Login(this.DbItem, true);
            }
            CS.WebComponentsGeneralV3.Code.Cms.Util.CmsUtil.ShowStatusMessageInCMS("Logged in successfully in frontend. Kindly go to the frontend, and you will be automatically logged in", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

        private void addOperation_UpdateAccountBalance()
        {
            if (BusinessLogic_v3.Modules.ModuleSettings.Members.UsesAccountBalanceSystem)
            {
                string baseUrl = CmsRoutesMapper.Instance.GetMembers_UpdateAccountBalance();
                URLClass urlClass = new URLClass();
                urlClass.SetUrl(baseUrl);
                //urlClass[MemberBaseCmsFactory.Instance.QueryStringParamID] = this.DbItem.ID;



                this.CustomCmsOperations.Add(new CmsItemSpecificOperation(this, "Update account balance",
                                                                          CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.EuroSign, urlClass.GetURL()));
            }

        }
        private void addOperation_StartKYCProcedure()
        {
            if (this.DbItem !=null && !this.DbItem.KYCVerified && !this.DbItem.KYCProcessStarted &&
                CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_KnowYourCustomer_Enabled))
            {//show button if not already verified, and it is switched on from module settings

                this.CustomCmsOperations.Add(new CmsItemSpecificOperation(this, "Start KYC Procedure", Code.Cms.EnumsCms.IMAGE_ICON.Listing, customOperation_StartKYCProcedure));
                                                                          
            }

        }
        private void customOperation_StartKYCProcedure()
        {
            this.DbItem.StartKYCProcedure();
            CS.WebComponentsGeneralV3.Code.Cms.Util.CmsUtil.ShowStatusMessageInCMSAndRefresh("Know-Your-Customer (KYC) process initiated", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);

        }

        private void addCustomOperations()
        {
            
            this.CustomCmsOperations.Add(new CmsItemSpecificOperation(this, "Login as member",
                 CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Member, loginAsMember));
            addOperation_UpdateAccountBalance();
            addOperation_StartKYCProcedure();

        }

        private void initMediaItems()
        {
            this.Image = this.AddPropertyForMediaItem<MemberBase>(item => item.Image, true, true);
            this.KYCProofOfAddress = this.AddPropertyForMediaItem<MemberBase>(item => item.KYCProofOfAddress, true, true);
            this.KYCProofOfIdentity = this.AddPropertyForMediaItem<MemberBase>(item => item.KYCProofOfIdentity, true, true);
            this.KYCProofOfAddressFilename.SetAccessTypeRequiredForAll(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.KYCProofOfIdentityFilename.SetAccessTypeRequiredForAll(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.ImageFilename.SetAccessTypeRequiredForAll(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
        }
        public CmsPropertyMediaItem<MemberBase> Image { get; set; }
        public CmsPropertyMediaItem<MemberBase> KYCProofOfAddress { get; set; }
        public CmsPropertyMediaItem<MemberBase> KYCProofOfIdentity { get; set; }
    }
}
