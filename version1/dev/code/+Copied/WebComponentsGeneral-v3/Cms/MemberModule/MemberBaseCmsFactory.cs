using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.MemberModule;

namespace CS.WebComponentsGeneralV3.Cms.MemberModule
{
    public abstract class MemberBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.MemberBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Member2;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            initRefreshLucene();
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

        private void initRefreshLucene()
        {

            var loggedUser = getLoggedCmsUser();
            {
                if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator))
                {
                    var op = new CmsGeneralOperation("Refresh Lucene Index", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.ReOrder, refreshLucene);
                    op.ConfirmMessage = "Refreshing the Lucene Index might take some time! Do only if search results are not working correctly!\n\nAlso, index is refreshed for this sections only. Other sections must be refreshed separately.";
                    this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
                }
            }
        }
        private void refreshLucene()
        {
            MemberBaseFactory.Instance.ReCreateLuceneIndex();
        }
    }
}
