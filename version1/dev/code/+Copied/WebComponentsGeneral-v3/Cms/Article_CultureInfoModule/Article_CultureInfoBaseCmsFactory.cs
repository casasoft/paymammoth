using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.Article_CultureInfoModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

namespace CS.WebComponentsGeneralV3.Cms.Article_CultureInfoModule
{
    public abstract class Article_CultureInfoBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.Article_CultureInfoBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
        }

    }
}
