using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CategorySpecificationModule;

namespace CS.WebComponentsGeneralV3.Cms.CategorySpecificationModule
{
    public abstract class CategorySpecificationBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.CategorySpecificationBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Label2;
        }
    }
}
