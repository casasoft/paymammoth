using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.VacancyModule;

namespace CS.WebComponentsGeneralV3.Cms.VacancyModule
{
    public abstract class VacancyBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.VacancyBaseCmsInfo_AutoGen
    {
    	public VacancyBaseCmsInfo(VacancyBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            initEditPriorities();
            initListingItems();
            initRequired();
        }

        private void initEditPriorities()
        {
            this.EditOrderPriorities.AddColumnsToStart(Title, RefCode, Description);
            this.ListingOrderPriorities.AddColumnsToStart(Title, RefCode);
        }

        private void initListingItems()
        {
            Title.ShowInListing = true;
            RefCode.ShowInListing = true;
        }

        private void initRequired()
        {
            Title.IsRequired = true;
            Description.IsRequired = true;
            Description.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            RefCode.IsRequired = true;
        }
	}
}
