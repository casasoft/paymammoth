using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductVariationColourModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.ProductVariationColourModule
{

//BaseCmsInfo-Class

    public abstract class ProductVariationColourBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ProductVariationColourBaseCmsInfo_AutoGen
    {
    	public ProductVariationColourBaseCmsInfo(ProductVariationColourBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            this.Title.ShowInListing = true;
            this.HexColor.ShowInListing = true;
		    this.ListingOrderPriorities.AddColumnsToStart(Title, HexColor);
            base.customInitFields();
        }
	}
}
