using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.AdvertHitModule;

namespace CS.WebComponentsGeneralV3.Cms.AdvertHitModule
{
    public abstract class AdvertHitBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.AdvertHitBaseCmsInfo_AutoGen
    {
    	public AdvertHitBaseCmsInfo(AdvertHitBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
