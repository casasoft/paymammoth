using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;

namespace CS.WebComponentsGeneralV3.Cms.ProductCategorySpecificationValueModule
{
    public abstract class ProductCategorySpecificationValueBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ProductCategorySpecificationValueBaseCmsInfo_AutoGen
    {
    	public ProductCategorySpecificationValueBaseCmsInfo(ProductCategorySpecificationValueBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            initListingItems();
            initListingPriority();
        }

        private void initListingItems()
        {
            this.Title.ShowInListing = true;
            this.Title.EditableInListing = true;
            Value.ShowInListing = true;
            this.Value.EditableInListing = true;
            
        }

        private void initListingPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.Value);
            
            
        }

	}
}
