using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.ArticleCommentModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace CS.WebComponentsGeneralV3.Cms.ArticleCommentModule
{
    public abstract class ArticleCommentBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ArticleCommentBaseCmsInfo_AutoGen
    {
    	public ArticleCommentBaseCmsInfo(ArticleCommentBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            this.Author.ShowInListing = true;
            this.PostedOn.ShowInListing = true;
            this.Email.ShowInListing = true;
            this.Comment.ShowInListing = true;
            this.IsApproved.ShowInListing = true;
		    this.IsApproved.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.ListingOrderPriorities.AddColumnsToStart(PostedOn, Author, Email);

            this.IPAddress.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.Author.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.PostedOn.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.Email.AccessTypeRequired_Edit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
		    addApproveDisapproveButton();
            base.customInitFields();
            
        }

        private void addApproveDisapproveButton()
        {
            if (DbItem != null)
            {
                if (DbItem.IsApproved)
                {
                    var customOp = new CmsItemSpecificOperation(this, "Disapprove", Code.Cms.EnumsCms.IMAGE_ICON.Error,
                                                                disapprove);
                    customOp.ConfirmMessage = "Are you sure you want to disapprove this item?";
                    this.CustomCmsOperations.Add(customOp);
                }
                else
                {
                    var customOp = new CmsItemSpecificOperation(this, "Approve", Code.Cms.EnumsCms.IMAGE_ICON.Success,
                                                                approve);
                    customOp.ConfirmMessage = "Are you sure you want to approve this item?";
                    this.CustomCmsOperations.Add(customOp);
                }
            }
        }

        private void disapprove()
        {
            this.DbItem.Disapprove();
            CmsUtil.ShowStatusMessageInCMSAndRefresh("Item disapproved successfully.", General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

        private void approve()
        {
            bool result = this.DbItem.Approve();
            if (result)
            {
                CmsUtil.ShowStatusMessageInCMSAndRefresh("Item approved successfully.",
                                                         General_v3.Enums.STATUS_MSG_TYPE.Success);
            }
            else
            {
                CmsUtil.ShowStatusMessageInCMSAndRefresh("Item was not approved. Is the parent reply approved?",
                                                         General_v3.Enums.STATUS_MSG_TYPE.Error);
            }
        }
	}
}
