using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.EmailTextModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.EmailTextModule
{
    public abstract class EmailTextBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.EmailTextBaseCmsFactory_AutoGen
    {
        
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            cmsInfo.ShowInCmsMainMenu = true;
            cmsInfo.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.EmailEdit;
            cmsInfo.RenderType = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.SECTION_RENDER_TYPE.Tree;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        public override IEnumerable<ICmsHierarchyItem> GetRootItems(IEnumerable<CmsPropertySearchInfo> searchCriteria)
        {
            var q = EmailTextBase.Factory.GetQuery();
            q.Where( item => item.Parent == null);
            var list = EmailTextBase.Factory.FindAll(q);
            return list.ConvertAll(item => GetCmsItemFromDBObject(item));
            
        }


        protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
            var q = EmailTextBase.Factory.GetQuery(qParams);
            var tmp1 = q.Fetch(item => item.Parent).Eager;
            var tmp2 = q.Fetch(item => item.ChildEmails).Eager;
            return q;
        }


    }
}
