using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Controls.WebControls.Classes;
using BusinessLogic_v3.Modules._Common;
using CS.General_v3.Classes.HelperClasses;

using BusinessLogic_v3.Modules.EmailTextModule;

namespace CS.WebComponentsGeneralV3.Cms.EmailTextModule
{
    using Code.Cms.Classes;
    using Code.Cms.CmsObjects.Functionality;
    using CS.General_v3.Classes.Interfaces.Hierarchy;
    using CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructureBasic;
    using CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructure;

    public abstract class EmailTextBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.EmailTextBaseCmsInfo_AutoGen, ICmsHierarchyItem, ICmsHierarchyInfo
    {
        private CmsItemHierarchyCommonImpl _commonImpl_CmsItemHierarchy = null;



        public EmailTextBaseCmsInfo(EmailTextBase item)
            : base(item)
        {

            this._commonImpl_CmsItemHierarchy = new CmsItemHierarchyCommonImpl(this, typeof(EmailTextBaseCmsInfo));
        }



        protected override void initBasicFields()
        {
            base.initBasicFields();

            this.Remarks.AccessTypeRequired_Edit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.Subject.IsRequired = true;
            this.Body.IsRequired = true;
            this.Identifier.IsRequired = true;
            this.Identifier.AccessTypeRequired_Edit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.Parent.IsRequired = true;
            this.Title.IsRequired = true;
            this.Body.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.Remarks.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;
            this.Title.AccessTypeRequired_Edit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.Title.ShowInListing = true;
            this.Subject.ShowInListing = true;
            this.Parent.ShowInListing = true;
            this.ChildEmails.ShowLinkInCms = true;
            this.CustomContentTags.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.WhenIsThisEmailSent.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.Parent.ShowInEdit = true;
            this.EditOrderPriorities.AddColumnsToStart(this.ID, this.Title, this.Subject, this.Body, this.Identifier);
            this.UsedInProject.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.UsedInProject.SetDefaultSearchValue(true);
        }

        #region ICmsHierarchyInfo Members

        ICmsCollectionInfo ICmsHierarchyInfo.SubitemLinkedCollection
        {
            get { return this.ChildEmails; }
        }


        string ICmsHierarchyInfo.GetTitleInHierarchy()
        {
            return this.ToString();
        }

        #endregion

        #region IHierarchy Members





        IEnumerable<IHierarchy> IHierarchy.GetChildren()
        {
            return this.getChildrenWithoutTemporary();
        }




        bool IHierarchy.Visible
        {
            get { return true; }
        }

        #endregion


        #region ITreeItem Members

        string ITreeItem.EditURL
        {
            get { return _commonImpl_CmsItemHierarchy.EditURL; }
        }

        string ITreeItem.AddNewItemURL
        {
            get { return _commonImpl_CmsItemHierarchy.AddNewItemURL; }
        }

        bool ITreeItem.AllowUpdate
        {
            get { return CmsUserSessionLogin.Instance.GetLoggedInUser().CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator); }
        }

        bool ITreeItem.AllowDelete
        {
            get { return CmsUserSessionLogin.Instance.GetLoggedInUser().CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft); }
        }

        bool ITreeItem.AllowAddSubItems
        {
            get { return CmsUserSessionLogin.Instance.GetLoggedInUser().CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft); }
        }

        bool ITreeItem.CanRemove(out string errorMsg)
        {
            errorMsg = "";
            return ((ITreeItem)this).AllowDelete;

        }


        OperationResult ITreeItem.Remove()
        {
            return this.DeleteFromCms();

        }



        string ITreeItem.Message_DeleteOK
        {
            get { return _commonImpl_CmsItemHierarchy.Message_DeleteOK; }
        }

        string ITreeItem.Message_ConfirmDelete
        {
            get { return _commonImpl_CmsItemHierarchy.Message_ConfirmDelete; }
        }

        bool ITreeItem.AddedExtraButtons
        {
            get
            {
                return _commonImpl_CmsItemHierarchy.AddedExtraButtons;

            }
            set
            {
                _commonImpl_CmsItemHierarchy.AddedExtraButtons = value;

            }
        }

        #endregion

        #region ITreeItemBasic Members


        int ITreeItemBasic.Priority
        {
            get
            {
                return 0;

            }
            set
            {

            }
        }

        string ITreeItemBasic.ImageURL
        {
            get { return _commonImpl_CmsItemHierarchy.ImageUrl; }
        }

        string ITreeItemBasic.LinkURL
        {
            get { return _commonImpl_CmsItemHierarchy.LinkUrl; }
        }

        private List<EmailTextBaseCmsInfo> _children = null;
        protected List<EmailTextBaseCmsInfo> getChildrenWithoutTemporary()
        {
            if (_children == null)
            {
                var dbItems = this.DbItem.GetChildrenWithoutTemporary();
                List<EmailTextBaseCmsInfo> list = new List<EmailTextBaseCmsInfo>();
                foreach (var dbItem in dbItems)
                {
                    list.Add((EmailTextBaseCmsInfo)factory.GetCmsItemFromDBObject(dbItem));
                }
                _children = list;
                _children = list.Where<EmailTextBaseCmsInfo>(item => item.DbItem.UsedInProject).ToList();
            }
            return _children;
        }

        IEnumerable<CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> ITreeItemBasic.GetChildTreeItems()
        {
            return getChildrenWithoutTemporary();

        }

        List<ExtraButton> ITreeItemBasic.ExtraButtons
        {
            get { return _commonImpl_CmsItemHierarchy.ExtraButtons; }
        }

        void ITreeItemBasic.AddExtraButtons()
        {
            _commonImpl_CmsItemHierarchy.AddExtraButtons();
        }

        #endregion

        #region ICmsHierarchyItem Members

        ICmsHierarchyInfo ICmsHierarchyItem.GetCmsHierarchyInfo()
        {
            return this;

        }

        #endregion

        #region IHierarchy Members


        string IHierarchy.Title
        {
            get { return getTitleForHierarchy(); }
        }

        #endregion

        private string getTitleForHierarchy()
        {
            return this.DbItem.Title;
        }

        public override string TitleForCms
        {
            get
            {
                return base.TitleForCms;
            }
        }

        #region ITreeItem Members


        OperationResult ITreeItem.Save()
        {
            return this.SaveFromCms();

        }

        #endregion

        #region ITreeItemBasic Members


        string ITreeItemBasic.Title
        {
            get { return getTitleForHierarchy(); }
        }

        #endregion

        public class TREE_ITEM : CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructure.TreeItem
        {
            public TREE_ITEM()
            {
                //this.ExtraButtons = new List<CS.WebComponentsGeneralV3.Code.Cms.WebControls.ListingClasses.TreeStructure.TreeStructureBasic.ExtraButton>();
            }
            public override bool CanRemove(out string errorMessage)
            {
                errorMessage = "";
                return true;

            }

            private EmailTextBase _item = null;
            public void FillFromCategory(EmailTextBase cat)
            {
                _item = cat;
            }
            #region ITreeItem Members

            public override long ID
            {
                get
                {
                    return _item.ID;
                }

            }

            public override string Title
            {
                get
                {
                    return _item.Title;
                }
                set
                {
                    _item.Title = value;
                }
            }

            public override int Priority
            {
                get
                {
                    return 0;
                }
                set
                {

                }
            }

            public override string ImageURL
            {
                get
                {
                    return "/_common/static/images/components/v1/tree/icon_category.gif";
                }

            }

            public override string EditURL
            {
                get
                {
                    return "/cms/EmailText/EmailText.aspx?id=" + _item.ID;
                }

            }

            public override string AddNewItemURL
            {
                get
                {
                    return "/cms/EmailText/EmailText.aspx?parentID=" + _item.ID + "&id=0";
                }

            }

            public override bool AllowUpdate
            {
                get { return true; }
            }



            public override bool AllowDelete
            {
                get
                {
                    return CmsUserSessionLogin.Instance.CheckAccessTypeOfLoggedUser(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator);


                }
            }



            public override OperationResult Save()
            {
                return _item.Save();
            }
            //private bool _parsedChildItems = false;
            //public override List<CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> GetChildTreeItems()
            //{
            //    if (!_parsedChildItems)
            //    {
            //        _parsedChildItems = true;
            //        var list = new List<CS.WebComponentsGeneralV3.Code.Cms.WebControls.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic>();
            //        foreach (var c in _item.ChildEmails)
            //        {
            //            list.Add(c.GetAsITreeItem());
            //        }
            //        this.ChildItems = list;
            //    }
            //    return this.ChildItems;
            //}


            public override string Message_DeleteOK
            {
                get
                {

                    return "Email text deleted successfully";
                }

            }


            public override string Message_ConfirmDelete
            {
                get
                {

                    string msg = @"Are you sure you want to delete this email text, and all its sub-texts?\r\n\r\n";

                    return msg;
                }

            }


            #endregion

            #region ITreeItemBasic Members



            #endregion

            #region ITreeItem Members


            public override bool AllowAddSubItems
            {
                get { return CmsUserSessionLogin.Instance.CheckAccessTypeOfLoggedUser(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator); }
            }


            #endregion
        }
        private TREE_ITEM _item = null;
        public TREE_ITEM GetAsITreeItem()
        {
            if (_item == null)
            {
                _item = new TREE_ITEM();
                _item.FillFromCategory(this.DbItem);
            }
            return _item;
        }


        #region IHierarchy Members

        long IHierarchy.ID
        {
            get { return this.DbItem.ID; }
        }

        #endregion

        #region ITreeItemBasic Members

        long ITreeItemBasic.ID
        {
            get { return this.DbItem.ID; }
        }

        #endregion

        #region IHierarchy Members

        IEnumerable<IHierarchy> IHierarchy.GetParents()
        {

            return CS.General_v3.Util.ListUtil.GetListFromSingleItem((EmailTextBaseCmsInfo)factory.GetCmsItemFromDBObject(this.DbItem.Parent));

        }

        #endregion

        IHierarchy IHierarchy.GetMainParent()
        {
            return (EmailTextBaseCmsInfo)factory.GetCmsItemFromDBObject(this.DbItem.Parent);
        }

    }
}
