using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberModule;
using CS.WebComponentsGeneralV3.Code.Controls.WebControls.Common;

namespace CS.WebComponentsGeneralV3.Cms.MemberSelfBarringPeriodModule
{

    //BaseCmsInfo-Class

    public abstract class MemberSelfBarringPeriodBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.MemberSelfBarringPeriodBaseCmsInfo_AutoGen
    {
        public MemberSelfBarringPeriodBaseCmsInfo(MemberSelfBarringPeriodBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            //custom init field logic here
            addEmail();
            addStatus();
            Email.ShowInListing = true;
            Email.ShowInEdit = true;
            Email.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            Status.ShowInListing = true;
            Status.ShowInEdit = true;
            Status.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            StartDate.ShowInListing = true;
            EndDate.ShowInListing = true;
            NotificationExpiredSent.ShowInListing = true;
            NotificationExpiry1Sent.ShowInListing = true;
            NotificationExpiry2Sent.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(Email, StartDate, EndDate, NotificationExpiry1Sent, NotificationExpiry2Sent,
                                        NotificationExpiredSent, Status);
            base.customInitFields();
        }

        private void addStatus()
        {
            CmsPropertyLiteral<MemberSelfBarringPeriodBase> p = new CmsPropertyLiteral<MemberSelfBarringPeriodBase>(this,
                "Status",
            item =>
            {
                MemberBase m = this.DbItem.Member;
                MemberSelfBarringPeriodBase sbpCmsItem = this.DbItem;
                MySpan span = new MySpan();
                if (m != null)
                {
                    MemberSelfBarringPeriodBase sbp = (MemberSelfBarringPeriodBase)m.GetCurrentSelfBarringPeriod();
                    if (sbp != null)
                    {
                        span.InnerHtml = sbp.ID == sbpCmsItem.ID ? "Active" : "Inactive";
                        span.CssClass += sbp.ID == sbpCmsItem.ID ? " sbp-active" : " sbp-inactive";
                    }
                    else
                    {
                        span.InnerHtml = "Inactive";
                        span.CssClass += " sbp-inactive";
                    }
                }
                return span;
            });

            this.Status = this.AddProperty(p);
        }

        private void addEmail()
        {
            CmsPropertyLiteral<MemberSelfBarringPeriodBase> p = new CmsPropertyLiteral<MemberSelfBarringPeriodBase>(this,
                "Email",
            item =>
            {
                MemberBase m = this.DbItem.Member;
                MySpan span = new MySpan();
                if (m != null)
                {
                    span.InnerHtml = m.Email;
                }
                return span;
            });

            this.Email = this.AddProperty(p);
        }

        public CmsPropertyLiteral<MemberSelfBarringPeriodBase> Email { get; private set; }
        public CmsPropertyLiteral<MemberSelfBarringPeriodBase> Status { get; private set; }
    }
}
