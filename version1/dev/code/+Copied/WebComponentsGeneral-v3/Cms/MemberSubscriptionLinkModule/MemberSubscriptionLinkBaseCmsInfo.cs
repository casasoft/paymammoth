using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberSubscriptionLinkModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.MemberSubscriptionLinkModule
{

//BaseCmsInfo-Class

    public abstract class MemberSubscriptionLinkBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.MemberSubscriptionLinkBaseCmsInfo_AutoGen
    {
    	public MemberSubscriptionLinkBaseCmsInfo(MemberSubscriptionLinkBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
