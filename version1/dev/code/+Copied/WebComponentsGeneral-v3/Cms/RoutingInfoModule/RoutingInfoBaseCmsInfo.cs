using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.RoutingInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.RoutingInfoModule
{

//BaseCmsInfo-Class

    public abstract class RoutingInfoBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.RoutingInfoBaseCmsInfo_AutoGen
    {
    	public RoutingInfoBaseCmsInfo(RoutingInfoBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();

            this.PhysicalPath.EditableInListing = true;
            this.VirtualPath.EditableInListing = true;
            this.RouteName.ShowInListing = true;
            this.Identifier.ShowInListing = true;
            this.Published.ShowInListing = this.Published.EditableInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(this.Published, this.Identifier, this.RouteName, this.VirtualPath, this.PhysicalPath);
            this.VirtualPath.EditableInListing = true;

            this.EditOrderPriorities.AddColumnsToStart(this.RouteName, this.VirtualPath, this.PhysicalPath, this.Published, this.Identifier);

        }
	}
}
