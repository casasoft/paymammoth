using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductVariationSizeModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.ProductVariationSizeModule
{

//BaseCmsInfo-Class

    public abstract class ProductVariationSizeBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ProductVariationSizeBaseCmsInfo_AutoGen
    {
    	public ProductVariationSizeBaseCmsInfo(ProductVariationSizeBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
