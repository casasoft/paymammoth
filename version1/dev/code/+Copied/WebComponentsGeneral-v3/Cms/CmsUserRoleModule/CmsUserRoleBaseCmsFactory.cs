using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CmsUserRoleModule;

namespace CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule
{
    public abstract class CmsUserRoleBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.CmsUserRoleBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.SetAllMinimumAccessRequiredAs(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = false;
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Padlock;
        }
    }
}
