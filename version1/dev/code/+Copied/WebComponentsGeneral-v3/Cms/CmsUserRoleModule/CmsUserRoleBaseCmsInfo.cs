using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CmsUserRoleModule;

namespace CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule
{
    public abstract class CmsUserRoleBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.CmsUserRoleBaseCmsInfo_AutoGen
    {
        public CmsUserRoleBaseCmsInfo(CmsUserRoleBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();
            this.Title.ShowInListing = true;
            this.Title.IsRequired = true;
            this.Identifier.IsRequired = true;
        }
	}
}
