﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Cms.CmsObjects;

using System.Web.UI.WebControls;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes;

namespace CS.WebComponentsGeneralV3.Cms.ArticleModule
{
    public class ArticleParentLinksParams : CmsPropertyManyToManyWithCustomClassParamsBase<ArticleBase, Article_ParentChildLinkBase, ArticleBase>
    {
        

        protected override IEnumerable<System.Web.UI.WebControls.ListItem> getListItemsFromItem(ArticleBase item, bool sortByText = true)
        {
            List<ListItem> listItemColl = new List<ListItem>();
            var cpList = ArticleBase.Factory.FindAll();
            foreach (var cp in cpList)
            {
                
                if (cp != null &&
                    (item == null || (item != null && cp.ID != item.ID && !ArticleParentChildLinksCacher.Instance.CheckIfItemIsParentOf(item, cp))))
                {
                    ListItem li = new ListItem(cp.GetFullTitle(), cp.ID.ToString());
                    listItemColl.Add(li);
                }
            }
            if (sortByText)
                listItemColl.Sort((c1, c2) => (c1.Text.CompareTo(c2.Text)));
            return listItemColl;
        }

        //protected override string getUniqueIDFromItem(Article_ParentChildLinkBase item)
        //{
        //    return item.Parent.ID.ToString();
        //}

        //protected override void setValuesToItem(ArticleBase item, IEnumerable<string> values)
        //{
            
        ////    item.ParentArticleLinks

        //    var parents = values.ToList().ConvertAll<ArticleBase>(s => ArticleBase.Factory.GetByPrimaryKey(long.Parse(s)));

        //    var result = CS.General_v3.Util.ListUtil.CompareTwoListsForRequiredAndNonRequired(
        //        parents,
        //        item.ParentArticleLinks.Collection,
        //        (item1,item2)=>
        //            {
        //                return item1.ID == item2.Parent.ID;
        //            });


        //    item.SetParentsAs(parents);
            
        //}
        
    
        protected override System.Linq.Expressions.Expression<Func<ArticleBase,ICollectionManager<Article_ParentChildLinkBase>>>  getPropertySelector()
        {
            return (cp => cp.ParentArticleLinks);
        }

        protected override System.Linq.Expressions.Expression<Func<Article_ParentChildLinkBase, ArticleBase>> getChildPropertyFromLinkedItemSelector()
        {
            return link => link.Parent;
        }

        protected override System.Linq.Expressions.Expression<Func<Article_ParentChildLinkBase, ArticleBase>> getParentPropertyFromLinkedItemSelector()
        {
            return link => link.Child;
        }
    }
}
