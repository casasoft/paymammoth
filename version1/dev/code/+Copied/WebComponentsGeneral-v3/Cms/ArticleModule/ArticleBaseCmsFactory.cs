using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;


using BusinessLogic_v3.Modules.ArticleModule;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.Article_CultureInfoModule;

namespace CS.WebComponentsGeneralV3.Cms.ArticleModule
{
    public abstract class ArticleBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.ArticleBaseCmsFactory_AutoGen
    {
        private string _QUERYSTRING_PARENTID = "ParentID";
        public string QUERYSTRING_PARENTID { get { return _QUERYSTRING_PARENTID; } }
        public long? GetParentIDFromCurrentQueryString()
        {
            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long?>(QUERYSTRING_PARENTID);
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            cmsInfo.RenderType = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.SECTION_RENDER_TYPE.Tree;

           

            cmsInfo.ShowInCmsMainMenu = true;
            cmsInfo.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Book;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

      
       


        public override IEnumerable<ICmsHierarchyItem> GetRootItems(IEnumerable<CmsPropertySearchInfo> searchCriteria)
        {

            var loggedInUser = getCurrentLoggedUser();

            if (loggedInUser.AccessType == CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser)
            {
                { //affiliates - This should restrict the content pages section for affiliates to ONLY those allowed for the affiliate.
                    var q = ArticleBase.Factory.GetQuery();
                    bool ok = false;


                    if (loggedInUser.CheckUserHasRole(BusinessLogic_v3.Enums.CmsUserRoleEnum.Affiliate))
                    {
                        var affiliateForUser = loggedInUser.GetAffiliateForCmsUser();
                        if (affiliateForUser != null)
                        {

                            var affiliateNode = affiliateForUser.GetAffiliateContentPageNode(true);
                            q.Where(cp => cp.ID == affiliateNode.ID);
                            ok = true;
                        }
                    }
                    q = q.Where(x => x.UsedInProject);
                    if (!ok)
                        q.Take(0);
                
                    return ConvertDataItemsToCmsItems(ArticleBase.Factory.FindAll(q));
                }
            }
            else
            {
                List<ICmsHierarchyItem> list = new List<ICmsHierarchyItem>();
                var rootNodes = ArticleBaseFactory.Instance.GetRootNodes(getOnlyUsedInProject:true);
                list.AddRange(GetListOfCmsItemFromDBObjects(rootNodes));

                //.. list.Clear();
                //list.Add( GetCmsItemFromDBObject(ArticleBase.GetInstance(774344)));


                return list;

            }
        }
        public override ArticleBaseCmsInfo CreateNewItem(bool isTemporary)
        {
            var newItem = base.CreateNewItem(isTemporary);
           
            long? parentID = GetParentIDFromCurrentQueryString();
            if (parentID.GetValueOrDefault() > 0)
            {
                var parent = ArticleBase.Factory.GetByPrimaryKey(parentID.Value);
                newItem.DbItem.AddParentArticle(parent);


                //newItem.DbItem.AddArticleToParentArticles(parent);
                newItem.DbItem.AllowAddChildren_AccessRequired = parent.AllowAddSubChildren_AccessRequired;
                newItem.DbItem.AllowAddSubChildren_AccessRequired = parent.AllowAddSubChildren_AccessRequired;
                newItem.DbItem.AllowUpdate_AccessRequired = parent.AllowUpdate_AccessRequired;
                newItem.DbItem.AllowDelete_AccessRequired = parent.AllowDelete_AccessRequired;

            }
               
            
            return newItem; 

        }

        
    }
}
