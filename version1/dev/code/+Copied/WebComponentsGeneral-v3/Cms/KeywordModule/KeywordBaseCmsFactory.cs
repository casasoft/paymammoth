using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.KeywordsGenerator;


using BusinessLogic_v3.Modules.KeywordModule;

namespace CS.WebComponentsGeneralV3.Cms.KeywordModule
{
    public abstract class KeywordBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.KeywordBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToAdd.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            if (BusinessLogic_v3.Modules.ModuleSettings.Shop.EcommerceEnabled)
            {
                this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
            }
            base.initCmsParameters();
        }

        protected override void initCustomCmsOperations()
        {
            var user = getCurrentLoggedUser();
            if (user != null && user.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator))
            {
                var op = new CmsGeneralOperation("Generate Keywords",
                                                                     CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Generate,
                                                                     generateKeywords);
                op.ConfirmMessage = "Are you sure you want to generate keywords? This might take quite some time!";

                this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
            }
            base.initCustomCmsOperations();
        }
        private void generateKeywords()
        {
                KeywordGenerator keyGen = KeywordGenerator.CreateInstance();
                keyGen.GenerateKeywords();
                CS.WebComponentsGeneralV3.Code.Cms.Util.CmsUtil.ShowStatusMessageInCMS("Keywords generated successfully",
                                                                                 CS.General_v3.Enums.
                                                                                     STATUS_MSG_TYPE.Success);
        }

    }
}
