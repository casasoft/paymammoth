using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.KeywordModule;

namespace CS.WebComponentsGeneralV3.Cms.KeywordModule
{
    public abstract class KeywordBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.KeywordBaseCmsInfo_AutoGen
    {
        public KeywordBaseCmsInfo(KeywordBase item)
            : base(item)
        {
            this.AccessTypeRequired_ToDelete.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.AccessTypeRequired_ToEdit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.AccessTypeRequired_ToView.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;

        }
        protected override void initBasicFields()
        {
            base.initBasicFields();
            this.Keyword.ShowInListing = true;
            this.FrequencyCount.ShowInListing = true;
            this.CultureInfo.ShowInListing = true;
            this.SetDefaultSortField(this.Keyword, CS.General_v3.Enums.SORT_TYPE.Ascending);
            this.ListingOrderPriorities.AddColumnsToStart(this.Keyword, this.FrequencyCount, this.CultureInfo);
            
        }


	}
}
