using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.MemberAccountBalanceHistoryModule
{

//BaseCmsInfo-Class

    public abstract class MemberAccountBalanceHistoryBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.MemberAccountBalanceHistoryBaseCmsInfo_AutoGen
    {
    	public MemberAccountBalanceHistoryBaseCmsInfo(MemberAccountBalanceHistoryBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            this.Date.ShowInListing = true;
            this.BalanceTotal.ShowInListing = true;
            this.BalanceUpdate.ShowInListing = true;
            this.UpdateType.ShowInListing = true;
            this.Comments.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(this.Date, this.UpdateType, this.Comments, this.BalanceUpdate, this.BalanceTotal);
            this.SetDefaultSortField(this.Date, General_v3.Enums.SORT_TYPE.Descending);

            this.AccessTypeRequired_ToEdit.SetAccess(General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            

            base.customInitFields();
        }
	}
}
