using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.EventSessionModule;

namespace CS.WebComponentsGeneralV3.Cms.EventSessionModule
{
    public abstract class EventSessionBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.EventSessionBaseCmsInfo_AutoGen
    {
    	public EventSessionBaseCmsInfo(EventSessionBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            

            this.EventSessionPeriods.ShowLinkInCms = true;
            initListingItems();
            initRequired();
            initEditPriorities();
        }


        private void initEditPriorities()
        {
            this.EditOrderPriorities.AddColumnsToStart(Title, Location, Description, StartDate, EndDate, SlotsAvailable, SlotsTaken);
        }

        private void initListingItems()
        {
            this.Title.ShowInListing = true;
            this.Cost.ShowInListing = true;
            this.SlotsAvailable.ShowInListing = true;
            this.SlotsTaken.ShowInListing = true;
            this.StartDate.ShowInListing = true;
            this.EndDate.ShowInListing = true;
            this.Location.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(Title, Cost, SlotsAvailable, SlotsTaken, StartDate, EndDate, Location);
        }

        private void initRequired()
        {
            this.Title.IsRequired = true;
            this.Description.IsRequired = true;
            this.Description.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.EndDate.IsRequired = true;
            this.Location.IsRequired = true;
            this.StartDate.IsRequired = true;
        }
	}
}
