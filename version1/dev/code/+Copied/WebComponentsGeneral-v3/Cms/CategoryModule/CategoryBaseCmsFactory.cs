using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;


using BusinessLogic_v3.Modules.CategoryModule;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;

namespace CS.WebComponentsGeneralV3.Cms.CategoryModule
{
    public abstract class CategoryBaseCmsFactory : CS.WebComponentsGeneralV3.Cms._AutoGen.CategoryBaseCmsFactory_AutoGen
    {
        private string _QUERYSTRING_PARENTID = "ParentID";
        public string QUERYSTRING_PARENTID { get { return _QUERYSTRING_PARENTID; } }
        public long? GetParentIDFromCurrentQueryString()
        {
            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long?>(QUERYSTRING_PARENTID);
        }
        public override CategoryBaseCmsInfo CreateNewItem(bool isTemporary)
        {
            var newItem = base.CreateNewItem(isTemporary);

            long? parentID = GetParentIDFromCurrentQueryString();
            if (parentID.GetValueOrDefault() > 0)
            {

                newItem.DbItem.AddParentCategory(CategoryBase.Factory.GetByPrimaryKey(parentID.Value));
            }
            return newItem;
        }
        
        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Category;
            this.UserSpecificGeneralInfoInContext.RenderType = CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.SECTION_RENDER_TYPE.Tree;
            if (CategoryBaseFactory.UsedInProject)
                this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
            base.initCmsParameters();
        }
        public override IEnumerable<ICmsHierarchyItem> GetRootItems(IEnumerable<CmsPropertySearchInfo> searchCriteria)
        {
            var rootList = CategoryBase.Factory.GetRootCategories();
            
            List<ICmsHierarchyItem> list = new List<ICmsHierarchyItem>();
            foreach (var c in rootList)
            {
                list.Add(GetCmsItemFromDBObject(c));
            }
            return list;
        }
    }
}
