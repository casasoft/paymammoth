using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.ArticleRatingModule;

namespace CS.WebComponentsGeneralV3.Cms.ArticleRatingModule
{
    public abstract class ArticleRatingBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ArticleRatingBaseCmsInfo_AutoGen
    {
    	public ArticleRatingBaseCmsInfo(ArticleRatingBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
