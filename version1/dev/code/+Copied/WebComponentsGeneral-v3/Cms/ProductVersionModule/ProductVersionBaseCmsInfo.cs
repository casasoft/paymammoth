using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductVersionModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace CS.WebComponentsGeneralV3.Cms.ProductVersionModule
{

//BaseCmsInfo-Class

    public abstract class ProductVersionBaseCmsInfo : CS.WebComponentsGeneralV3.Cms._AutoGen.ProductVersionBaseCmsInfo_AutoGen
    {
    	public ProductVersionBaseCmsInfo(ProductVersionBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            this.Title.ShowInListing = true;
            this.ReleaseDate.ShowInListing = true;
		    this.ReleaseDetails.StringDataType = General_v3.Enums.STRING_DATA_TYPE.Html;
            this.ListingOrderPriorities.AddColumnsToStart(Title, ReleaseDate);
		    this.EditOrderPriorities.AddColumnsToStart(ID, Title, ReleaseDate, TrialFile, FullFile, ReleaseDetails);
            this.ProductVersionMediaItems.ShowLinkInCms = true;
		    initUpload();
            base.customInitFields();
        }

        private void initUpload()
        {
            this.FullFile = this.AddPropertyForMediaItem<ProductVersionBase>(item => item.DownloadLink, true, true);
            this.TrialFile = this.AddPropertyForMediaItem<ProductVersionBase>(item => item.TrialLink, true, true);
        }
        public CmsPropertyMediaItem<ProductVersionBase> FullFile { get; set; }
        public CmsPropertyMediaItem<ProductVersionBase> TrialFile { get; set; }
	}
}
