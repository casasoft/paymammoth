﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Constants
{
    public static class ParameterNames
    {
        public static string ID = "Id";
        public static string Code = "Code";
        public static string ReferralId = "rid";
        public static string DATE_FROM = "dateFrom";
        public static string DATE_TO = "dateTo";
        public static string ORDER = "orderID";
        public static string GUID = "guid";
        public static string ACCESS_TOKEN = "accessToken";
        public static string UID = "uID";
        public static string LANG_PARAM = "lang";
        public static string Cms_CheckForUntranslatedText = "cms_CheckUntranslatedText";
        
    }
}
