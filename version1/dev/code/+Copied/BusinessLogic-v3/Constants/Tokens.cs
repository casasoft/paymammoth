﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Constants
{
    public class Tokens
    {

        public const string TAG_AUTHOR = "[AUTHOR]";
        public const string TAG_CATEGORY = "[CATEGORY]";
        public const string TAG_AMOUNT = "[AMT]";
        public const string TAG_QUANTITY = "[QUANTITY]";
        public const string TAG_TERMS_LINK = "[TERMS_LINK]";
        public const string TAG_DATETIME = "[DATE_TIME]";
        public const string INVOICE_LINK = "[INVOICE_LINK]";
        public const string INVOICE_PAYMENT_URL = "[PAYMENT_URL]";
        public const string INVOICE_NUMBER = "[INVOICE_NUMBER]";
        public const string COUNTRY = "[COUNTRY]";
        public const string MINIMUM_MEMBER_AGE_ALLOWED = "[MINIMUM_AGE_ALLOWED]";
        public const string COMMENTS_COUNT = "[COMMENTS_COUNT]";
        public const string DATE = "[DATE]";
        public const string SITE_COPYRIGHT_YEAR = "[SITE_COPYRIGHT_YEAR]";
        public const string SITE_COPYRIGHT_SITE_NAME = "[SITE_COPYRIGHT_SITE_NAME]";
        public const string MAX_FILE_SIZE_KB = "[MAX_FILE_SIZE_KB]";
        public const string MANAGING_DIRECTOR = "[MANING_DIRECTOR]";
        public const string SEARCH_KEYWORDS = "[SEARCH_KEYWORDS]";
        public const string USER_FIRST_NAME = "[USER_FIRST_NAME]";
        public const string PRODUCT_ITEM_CODE = "[PRODUCT_ITEM_CODE]";
        public const string PRODUCT_VARIATION_REF_CODE = "[PRODUCT_VARIATION_REF_CODE]";
        public const string PRODUCT_REQUESTED_QUANTITY = "[PRODUCT_REQUESTED_QUANTITY]";
        public const string ITEM_TEXT = "[ITEM_TEXT]";
        public const string PRODUCT_AVAILABLE_QUANTITY = "[PRODUCT_AVAILABLE_QUANTITY]";
        public const string SUBSCRIPTION_NAME = "[SUBSCRIPTION_NAME]";
        public const string START_DATE = "[START_DATE]";
        public const string END_DATE = "[END_DATE]";
        public const string INVOICE_VAT_VALUE = "[INVOICE_VAT_VALUE]";
        public const string DISCOUNT_COUPON_CODE = "[DISCOUNT_COUPON_CODE]";
        public const string DISCOUNT_COUPON_EXPIRES_ON = "[DISCOUNT_COUPON_EXPIRES_ON]";
        public const string DISCOUNT_COUPON_HAS_EXPIRY = "[DISCOUNT_COUPON_HAS_EXPIRY]";
        public const string SHOW_AMOUNT_VALUE = "[SHOW_AMOUNT_VALUE]";
        public const string DISCOUNT_COUPON_TITLE = "[DISCOUNT_COUPON_TITLE]";
        public const string EVENT_TITLE = "[EVENT_TITLE]";
        public const string ROUTE_PARAM_TITLE = "ROUTE_PARAM_TITLE";
        public const string ROUTE_PARAM_ID = "ROUTE_PARAM_ID";
        public const string ROUTE_PARAM_CATEGORY_ID = "ROUTE_PARAM_CATEGORYID";
        public const string ROUTE_PARAM_KEYWORDS = "ROUTE_PARAM_KEYWORDS";
        public const string ROUTE_PARAM_CATEGORY = "ROUTE_PARAM_CATEGORY";
        public const string ROUTE_PARAM_DAY = "ROUTE_PARAM_DAY";
        public const string ROUTE_PARAM_MONTH = "ROUTE_PARAM_MONTH";
        public const string ROUTE_PARAM_YEAR = "ROUTE_PARAM_YEAR";
        public const string EVENT_MAXIMUM_SLOTS = "[EVENT_MAXIMUM_SLOTS]";
        public const string EVENT_MAXIMUM_SLOTS_GREATER_THAN_1 = "[EVENT_MAXIMUM_SLOTS_GREATER_THAN_1]";
        public const string REGISTER_URL = "[REGISTER_URL]";
        public const string FILE_TITLE = "[FILE_TITLE]";
        public const string FIRST_NAME = "[FIRST_NAME]";
        public const string KEYWORD_NAME = "[KEYWORD_NAME]";
        public const string TOTAL_NEW_ARTICLES = "[TOTAL_NEW_ARTICLES]";
        public const string LAST_VISIT_DATE = "[LAST_VISIT_DATE]";
        public const string TODAY_DATE = "[TODAY_DATE]";
        public const string LOCATION = "[LOCATION]";
    }
}
