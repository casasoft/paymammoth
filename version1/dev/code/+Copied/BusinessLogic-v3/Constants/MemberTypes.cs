﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Constants
{
    public class MemberTypes
    {
        public const string Teacher = "Teacher";
        public const string Student = "Student";
        public const string User = "User";
        public const string Merchant = "Merchant";
    }
}
