﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Constants
{
    public static class SessionIdentifiers
    {
        public const string PAY_MAMMOTH_SESSION_IDENTIFIER = "P_m_Id_Session";
        public const string CHECKOUT_DELIVERY_DETAILS = "checkoutDeliveryDetails";
        public const string CART_REGISTRATION = "cartRegistration";
        public const string FACEBOOK_USERID = "fb_uid";
        public const string FACEBOOK_ACCESS_TOKEN = "fb_accesstoken";
    }
}
