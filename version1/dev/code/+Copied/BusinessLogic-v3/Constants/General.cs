﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Constants
{
    public static class General
    {
        /// <summary>
        /// The total precision to store for numbers storing money related information
        /// </summary>
        public const int NUMBER_PRECISION_FINANCIAL = 2;
        public const string NODE_IDENTIFIER_ROOT = "root";

        public const string LUCENE_BASE_RELATIVE_INDEX_LOCATION = @"\App_data\LuceneIndex\";

    }
}
