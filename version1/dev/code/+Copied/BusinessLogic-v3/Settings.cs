﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3
{
    public static class Settings
    {
        public static TOutputDataType GetSettingFromDatabase<TOutputDataType>(Enums.BusinessLogicSettingsEnum normalIdentifier)
        {

            return CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<TOutputDataType>(normalIdentifier);
        }

        public static string GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum normalIdentifier)
        {
            return GetSettingFromDatabase<string>(normalIdentifier);

        }

        public static class Emails
        {
            public static string OrderPaid_NotificationEmail 
            {
                get 
                {
                    string s = GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum.ModuleSettings_Shop_OrderPaid_NotificationEmailSendTo);
                    if (string.IsNullOrWhiteSpace(s)) s = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_Notification_Email);
                    if (string.IsNullOrWhiteSpace(s)) s = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_AdminNotificationToEmail);
                    if (string.IsNullOrWhiteSpace(s)) s = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromEmail);
                    return s;
                }
            }
        }





    }
}
