﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using Quartz.Impl;
using log4net;
using BusinessLogic_v3.Classes.Scheduling;
using log4net;

namespace BusinessLogic_v3.Util
{
    public static class SchedulingUtil
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(SchedulingUtil));
        private const string GROUP_NAME = "SchedulingUtil-Jobs";
        
        /// <summary>
        /// schedules a method to be called based on a repeated schedule
        /// </summary>
        /// <param name="methodToCall">Method to call</param>
        /// <param name="nameOfJob">Name of job.  If left null (not recommeded) a GUID is generated</param>
        /// <param name="timeToStart">Time to start, in server local time, not universal time.</param>
        /// <param name="intervalToRepeatInSecs">Interval to repeat, every X seconds</param>
        /// <param name="totalTimesToRepeat">Total times to repeat.  If null, it will repeat forever</param>
        /// <param name="isUniversalTime">Whether the start time is in universal time or not</param>
        public static IJobDetail ScheduleRepeatMethodCall(Action methodToCall, string nameOfJob, DateTime? timeToStart, int intervalToRepeatInSecs, int? totalTimesToRepeat, bool isUniversalTime = false)
        {
            if (nameOfJob == null) nameOfJob = CS.General_v3.Util.Random.GetGUID();
            string groupKey = GROUP_NAME;
            


            IJobDetail job = new JobDetailImpl(nameOfJob, groupKey, typeof(MethodCallJob));
            job.JobDataMap[MethodCallJob.DATA_METHOD] = methodToCall;
            DateTime universalTime;
            if (timeToStart != null)
            {
                if (isUniversalTime)
                    universalTime = timeToStart.Value;
                else
                    universalTime = timeToStart.Value.ToUniversalTime();
            }
            else
            {
                universalTime= CS.General_v3.Util.Date.Now.ToUniversalTime();
            }

            ITrigger trigger = TriggerBuilder.Create().StartAt(universalTime)
                .WithSimpleSchedule(x=>

                                    {
                                        if (totalTimesToRepeat.HasValue)
                                            x.WithRepeatCount(totalTimesToRepeat.Value);
                                        else
                                            x.RepeatForever();
                                        x.WithIntervalInSeconds(intervalToRepeatInSecs);
                                    }
                ).Build();



            
            BusinessLogic_v3.Classes.Scheduling.QuartzNet.Scheduler.ScheduleJob(job, trigger);
            return job;
        }
        /// <summary>
        /// Schedules a method to be called on a one-off basis.
        /// </summary>
        /// <param name="methodToCall">Method to call</param>
        /// <param name="nameOfJob">name of job</param>
        /// <param name="timeToStart">time to start, in server time (not universal time)</param>
        public static IJobDetail ScheduleOneOffMethodCall(Action methodToCall, string nameOfJob, DateTime? timeToStart, bool isUniversalTime = false)
        {
            
            if (nameOfJob == null) nameOfJob = CS.General_v3.Util.Random.GetGUID();
            string groupKey = GROUP_NAME;


            IJobDetail job = new JobDetailImpl(nameOfJob, groupKey, typeof(MethodCallJob));
            job.JobDataMap[MethodCallJob.DATA_METHOD] = methodToCall;

            if (timeToStart == null)
                timeToStart = CS.General_v3.Util.Date.Now;

            ITrigger trigger = TriggerBuilder.Create().StartAt(timeToStart.Value.ToUniversalTime())
                .WithSimpleSchedule().Build();



            
            BusinessLogic_v3.Classes.Scheduling.QuartzNet.Scheduler.ScheduleJob(job, trigger);
            return job;
        }

        /// <summary>
        /// Stops a job from executing
        /// </summary>

        /// <param name="nameOfJob">name of job</param>
        /// <param name="group">name of group. Optional.</param>

        public static bool StopJob(JobKey key)
        {
            var scheduler = BusinessLogic_v3.Classes.Scheduling.QuartzNet.Scheduler;
            bool deleted = scheduler.DeleteJob(key);

            return deleted;
        }

        /// <summary>
        /// Stops a job from executing
        /// </summary>
        
        /// <param name="nameOfJob">name of job</param>
        /// <param name="group">name of group. Optional.</param>
        
        public static bool StopJob(string nameOfJob, string group = null)
        {
            string groupKey = group;
            if (groupKey == null)
                groupKey = GROUP_NAME;
            JobKey jobKey = new JobKey(nameOfJob,groupKey);
            return StopJob(jobKey);
            
            
        }


        public static bool StopJob(IJobDetail job)
        {
            return StopJob(job.Key);
            
        }
    }
}
