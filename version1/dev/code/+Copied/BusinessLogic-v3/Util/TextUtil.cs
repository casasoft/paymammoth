﻿namespace BusinessLogic_v3.Util
{
    using System.Collections.Generic;
    using BusinessLogic_v3.Classes.Text;
    using BusinessLogic_v3.Classes.Interfaces;
    using System;
    using CS.General_v3.Classes.Text;

    public static class TextUtil
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="contentTagsAndValues">A list of content tags and values, e.g. [QUANTITY], 10, [ITEM], Testing...</param>
        /// <returns></returns>
        public static string ReplaceTagsForContentTagItem(IItemWithContentTags item, ITokenReplacer contentTagsAndValues)
        {
            
                if (contentTagsAndValues != null)
                {
                    return contentTagsAndValues.ReplaceString(item.GetItemContent());
                }
                else
                {
                    return item.GetItemContent();
                }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="contentTagsAndValues">A list of content tags and values, e.g. [QUANTITY], 10, [ITEM], Testing...</param>
        /// <returns></returns>
        public static string ReplaceTagsForContentTagItem(IItemWithContentTags item, Dictionary<string, string> contentTagsAndValues)
        {
            TokenReplacerNew tokenReplacer = new TokenReplacerNew(item);
            //TokenReplacerWithContentTagItem tokenReplacer = new TokenReplacerWithContentTagItem(item);
            foreach (string contentTag in contentTagsAndValues.Keys)
            {
                string value = contentTagsAndValues[contentTag];
                tokenReplacer[contentTag] = value;
            }

            return tokenReplacer.ReplaceString(item.GetItemContent());
        }
        public static string ReplaceTagsForContentTagItem(IItemWithContentTags item, params string[] contentTagsAndValues)
        {
            Dictionary<string, string> dictContentTagsAndValues = new Dictionary<string, string>();
            if (contentTagsAndValues != null)
            {
                if (contentTagsAndValues.Length % 2 != 0)
                    throw new InvalidOperationException("contentTagsAndValues must be in pairs");
                for (int i = 0; i < contentTagsAndValues.Length; i += 2)
                {
                    dictContentTagsAndValues[contentTagsAndValues[i]] = contentTagsAndValues[i + 1];
                }
            }
            return ReplaceTagsForContentTagItem(item, dictContentTagsAndValues);
        }
    }
}