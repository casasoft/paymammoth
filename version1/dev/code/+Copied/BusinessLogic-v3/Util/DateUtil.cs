﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;

namespace BusinessLogic_v3.Util
{
    public static class DateUtil
    {
        
        public static DateTime ConvertServerDateToLoggedInUserPreferredTimezone(DateTime date)
        {
            var user = BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.GetLoggedInUserAlsoFromRememberMe();
            if (user != null)
            {
                return user.ConvertServerDateToUserPreferredTimezone(date);
            }
            else
            {
                return date;
            }
        }

        public static string[] GetMultilingualFullMonthNames()
        {
            string[] monthNames = new string[12];
            monthNames[0] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_January).GetContent(parseLinks: true);
            monthNames[1] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_February).GetContent(parseLinks: true);
            monthNames[2] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_March).GetContent(parseLinks: true);
            monthNames[3] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_April).GetContent(parseLinks: true);
            monthNames[4] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_May).GetContent(parseLinks: true);
            monthNames[5] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_June).GetContent(parseLinks: true);
            monthNames[6] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_July).GetContent(parseLinks: true);
            monthNames[7] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_August).GetContent(parseLinks: true);
            monthNames[8] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_September).GetContent(parseLinks: true);
            monthNames[9] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_October).GetContent(parseLinks: true);
            monthNames[10] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_November).GetContent(parseLinks: true);
            monthNames[11] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Month_Name_December).GetContent(parseLinks: true);

            return monthNames;
        }

        public static string[] GetMultilingualShort3LetterMonthNames()
        {
            string[] monthNames = new string[12];
            monthNames[0] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_January).GetContent(parseLinks: true);
            monthNames[1] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_February).GetContent(parseLinks: true);
            monthNames[2] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_March).GetContent(parseLinks: true);
            monthNames[3] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_April).GetContent(parseLinks: true);
            monthNames[4] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_May).GetContent(parseLinks: true);
            monthNames[5] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_June).GetContent(parseLinks: true);
            monthNames[6] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_July).GetContent(parseLinks: true);
            monthNames[7] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_August).GetContent(parseLinks: true);
            monthNames[8] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_September).GetContent(parseLinks: true);
            monthNames[9] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_October).GetContent(parseLinks: true);
            monthNames[10] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_November).GetContent(parseLinks: true);
            monthNames[11] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Month_Name_December).GetContent(parseLinks: true);

            return monthNames;
        }

        public static string[] GetMultilingualFullDayNames()
        {
            string[] dayNames = new string[7];
            dayNames[0] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Day_Name_Monday).GetContent(parseLinks: true);
            dayNames[1] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Day_Name_Tuesday).GetContent(parseLinks: true);
            dayNames[2] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Day_Name_Wednesday).GetContent(parseLinks: true);
            dayNames[3] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Day_Name_Thursday).GetContent(parseLinks: true);
            dayNames[4] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Day_Name_Friday).GetContent(parseLinks: true);
            dayNames[5] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Day_Name_Saturday).GetContent(parseLinks: true);
            dayNames[6] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Full_Day_Name_Sunday).GetContent(parseLinks: true);
            return dayNames;
        }

        public static string[] GetMultilingualShort3LetterDayNames()
        {
            string[] dayNames = new string[7];
            dayNames[0] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Day_Name_Monday).GetContent(parseLinks: true);
            dayNames[1] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Day_Name_Tuesday).GetContent(parseLinks: true);
            dayNames[2] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Day_Name_Wednesday).GetContent(parseLinks: true);
            dayNames[3] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Day_Name_Thursday).GetContent(parseLinks: true);
            dayNames[4] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Day_Name_Friday).GetContent(parseLinks: true);
            dayNames[5] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Day_Name_Saturday).GetContent(parseLinks: true);
            dayNames[6] = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.Calendar_Short_Day_Name_Sunday).GetContent(parseLinks: true);
            return dayNames;
        }
    }
}
