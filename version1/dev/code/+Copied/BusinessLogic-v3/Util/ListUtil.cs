﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Reflection;
using System.ComponentModel;
using CS.General_v3.Classes.Attributes;

namespace BusinessLogic_v3.Util
{
    public static class ListUtil
    {
        public static ListItemCollection GetListItemCollectionFromEnum(Type enumType, string identifierPrefix,
            string blankItemOnTopValue = null, Enums.ENUM_SORT_BY sortBy = Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false)
        {

            ListItemCollection lc = new ListItemCollection();
            IList<string> names = GetListOfEnumNames(enumType, identifierPrefix, sortBy);
            List<Enum> values = GetListOfEnumValues(enumType, identifierPrefix, sortBy);

            for (int i = 0; i < values.Count; i++)
            {

                lc.Add(new ListItem(names[i], (Convert.ToInt32(values[i]).ToString())));
            }
            if (blankItemOnTopValue != null)
            {
                lc.Insert(0, new ListItem("", blankItemOnTopValue));
            }

            return lc;
        }

        public static IList<string> GetListOfEnumNames(Type enumType, string identifierPrefix, Enums.ENUM_SORT_BY sortBy = Enums.ENUM_SORT_BY.NameAscending)
        {
            List<string> list = new List<string>();
            var values = GetListOfEnumValues(enumType, identifierPrefix, sortBy);
            foreach (Enum value in values)
            {
                string s = StringValueOf(value);
                list.Add(s);

            }
            return list;
        }

        public static List<Enum> GetListOfEnumValues(Type enumType, string identifierPrefix, Enums.ENUM_SORT_BY sortBy = Enums.ENUM_SORT_BY.NameAscending)
        {
            List<Enum> list = new List<Enum>();
            Array values = Enum.GetValues(enumType);
            for (int i = 0; i < values.Length; i++)
            {
                var val = values.GetValue(i);
                list.Add((Enum)val);
            }
            if (sortBy == Enums.ENUM_SORT_BY.NameAscending || sortBy == Enums.ENUM_SORT_BY.NameDescending)
            {
                list.Sort((itemA, itemB) => StringValueOf(itemA).CompareTo(StringValueOf(itemB)) * (sortBy == Enums.ENUM_SORT_BY.NameAscending ? 1 : -1));
            }
            else if (sortBy == Enums.ENUM_SORT_BY.PriorityAttributeValue)
            {
                list.Sort((itemA, itemB) => PriorityValueOf(itemA).CompareTo(PriorityValueOf(itemB)));
            }

            return list;
        }

        public static string StringValueOf(this Enum value, bool addSpacesToCamelCasedName = false)
        {
            return _StringValueOf(value, addSpacesToCamelCasedName);
        }

        private static string _StringValueOf(object value, bool addSpacesToCamelCasedName)
        {
            string s = null;
            if (value != null)
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                if (fi != null)
                {
                    DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                    if (attributes.Length > 0)
                    {
                        return attributes[0].Description;
                    }
                }
                s = _getEnumValueName(value, addSpacesToCamelCasedName);
            }
            return s;
        }

        private static string _getEnumValueName(object value, bool addSpacesToCamelCasedName)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            string s = value.ToString();
            if (addSpacesToCamelCasedName)
                s = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(s);
            return s;
        }

        public static int PriorityValueOf(this Enum value, int defaultValue = 0)
        {
            return PriorityValueOfNullable(value) ?? defaultValue;
        }

        public static int? PriorityValueOfNullable(this Enum value)
        {
            string s = null;
            if (value != null)
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                if (fi != null)
                {
                    PriorityAttribute[] attributes = (PriorityAttribute[])fi.GetCustomAttributes(typeof(PriorityAttribute), false);
                    if (attributes.Length > 0)
                    {
                        return attributes[0].Priority;
                    }
                }
            }
            return null;
        }
    }
}
