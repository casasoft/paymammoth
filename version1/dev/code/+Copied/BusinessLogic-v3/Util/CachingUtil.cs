﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Caching;
using BusinessLogic_v3.Classes.DbObjects;
using System.Reflection;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace BusinessLogic_v3.Util
{
    public static class CachingUtil
    {
        /// <summary>
        /// Retrieves the cache dependency for this control
        /// </summary>
        /// <param name="factories"></param>
        /// <returns></returns>
        public static CacheDependency GetOutputCacheDependencyForFactories(params IBaseDbFactory[] factories)
        {

            List<string> list = new List<string>();


            list.Add(CS.General_v3.Util.CachingUtil.ALL_PAGES_PARAM);
            foreach (var f in factories)
            {
                if (f != null)
                {
                    list.Add(f.GetOutputCacheDependencyKey());
                }
            }



            CacheDependency dep = CS.General_v3.Util.CachingUtil.GetCacheDependency(list.ToArray());

            return dep;
        }
        public static CacheDependency GetOutputCacheDependencyForDBObjects(params BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject[] items)
        {
            return GetOutputCacheDependencyForDBObjects(items.ToList());
           
        }
        public static CacheDependency GetOutputCacheDependencyForProperty(string uniqueKeyPrefix, PropertyInfo property, object obj)
        {
            string key = uniqueKeyPrefix + "_" + property.Name;
            CacheDependency dependency = CS.General_v3.Util.CachingUtil.GetCacheDependency(key);
            return dependency;
        }
        public static void UpdateCacheDependencyValueForProperty(string uniqueKeyPrefix, PropertyInfo property, object obj)
        {
            string key = uniqueKeyPrefix + "_" + property.Name;
            
        }
        public static CacheDependency GetOutputCacheDependencyForDBObjects(IEnumerable<BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject> items)
        {

            List<string> list = new List<string>();


            list.Add(CS.General_v3.Util.CachingUtil.ALL_PAGES_PARAM);
            foreach (IBaseDbObject f in items)
            {
                list.Add(f.GetOutputCacheDependencyKey());
            }

            CacheDependency dependency = CS.General_v3.Util.CachingUtil.GetCacheDependency(list.ToArray());
            return dependency;
        }
    }
}
