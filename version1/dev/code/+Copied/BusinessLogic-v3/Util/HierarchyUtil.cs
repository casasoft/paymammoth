﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.General;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules.SettingModule;
namespace BusinessLogic_v3.Util
{
    public static class HierarchyUtil
    {

        public static IEnumerable<IItemHierarchy> RemoveAllIHierarchyItemsWithoutValidParentFromDatabase(IEnumerable<IItemHierarchy> items)
        {


           // List<ISettingBase> list2 =  null;
            //var t = list2.ToFrontendList();
            

            List<IItemHierarchy> list = items.ToList();
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                if (item.ParentID > 0)
                {
                    bool found = false;
                    for (int j = 0; j < list.Count; j++)
                    {
                        var itemCmp = list[j];
                        if (item.ParentID == itemCmp.ID)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        item.Delete();
                        list.RemoveAt(i);
                        i--;
                    }
                }
            }
            return list;
        }


    }
}
