﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Util
{
    using CS.General_v3.Util;
    using Modules.ArticleModule;
    using Modules.ContentTextModule;

    public static class DataUtil
    {
        public static void UpdateDBItemFieldByReflection(long itemID, string fullItemTypeClassName, string propertyName, string newValue)
        {
            using (var transaction = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                var itemType = ReflectionUtil.GetTypeFromExecutingAssemblies(fullItemTypeClassName);
                var itemFactory = BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryForType(itemType);
                var item = itemFactory.GetByPrimaryKey(itemID);
                
                var propInfo = ReflectionUtil.GetPropertyForType(itemType, propertyName, true, false);
                var castedValue = CS.General_v3.Util.Other.ConvertStringToBasicDataType(newValue, propInfo.PropertyType);
                propInfo.SetValue(item, castedValue, null);
                item.Save();
                transaction.Commit();
            }
        }
    }
}
