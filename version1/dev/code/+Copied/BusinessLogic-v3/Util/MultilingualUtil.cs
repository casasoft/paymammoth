﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Attributes;
using BusinessLogic_v3.Classes.Multilingual;
using System.Reflection;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Text.LanguageClassification;

namespace BusinessLogic_v3.Util
{
    public static class MultilingualUtil
    {
        /// <summary>
        /// Returns the value for a multilingual property (on the main item, not the culture info), in a specific culture.  E.g, you can call this method on a 'Product.Title' (NOT Product_CultureInfo),
        /// to get the title value in French.
        /// </summary>
        /// <param name="multilingualItem"></param>
        /// <param name="mainProperty"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public static object GetValueForCultureForMultilingualProperty(this IMultilingualItem multilingualItem, PropertyInfo mainProperty, ICultureDetailsBase culture)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(culture, "Culture is required");
            object result = null;
            
            var cultureInfos = multilingualItem.GetMultilingualContentInfos();
            var defaultCulture = BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture();
            var cultureInfoForDefault = cultureInfos.FirstOrDefault(x => x.CultureInfo != null && x.CultureInfo.ID == culture.ID);
            if (cultureInfoForDefault != null)
            {
                string pName = mainProperty.Name;
                Type t = cultureInfoForDefault.GetType();
                var cultureInfoProperty = t.GetProperty(pName);
                result = cultureInfoProperty.GetValue(cultureInfoForDefault, null);
            }



            return result;

        }
        /// <summary>
        /// Returns the value for a multilingual property (on the main item, not the culture info), in a specific culture.  E.g, you can call this method on a 'Product.Title' (NOT Product_CultureInfo),
        /// to get the title value in French.
        /// </summary>
        /// <param name="multilingualItem"></param>
        /// <param name="mainProperty"></param>
        
        /// <returns></returns>
        public static object GetValueForDefaultCultureForMultilingualProperty(this IMultilingualItem multilingualItem, PropertyInfo mainProperty)
        {
            
            var culture = BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture();
            return GetValueForCultureForMultilingualProperty(multilingualItem, mainProperty, culture);
        }
        /// <summary>
        /// Checks whether a property is multilingual
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static bool CheckIfPropertyIsMultilingual(PropertyInfo p)
        {
            return CS.General_v3.Util.ReflectionUtil.CheckIfPropertyContainsAttribute<MultilingualAttribute>(p);
        }
        
        /// <summary>
        /// Returns the culture info for an item, for a specific culture.  E.g, Get Italian culture info of 'Product'
        /// </summary>
        /// <param name="multilingualItem"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public static IMultilingualContentInfo GetCultureInfoForMultilingualItemForCulture(this IMultilingualItem multilingualItem, ICultureDetailsBase culture)
        {
            var list = multilingualItem.GetMultilingualContentInfos();
            return list.FirstOrDefault(x => x.CultureInfo == culture);
            
        }

        /// <summary>
        /// Retreives all the multilingual properties of a type
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static List<PropertyInfo> GetAllMultilingualProperties(this IMultilingualContentInfo content)
        {
            Type t = content.GetType();
            return _getAllMultilingualPropertiesForType(t);

        }
        private static List<PropertyInfo> _getAllMultilingualPropertiesForType(Type t)
        {
            
            var propertyList = t.GetProperties();
            List<PropertyInfo> list = new List<PropertyInfo>();
            foreach (var p in propertyList)
            {
                if (CheckIfPropertyIsMultilingual(p))
                    list.Add(p);
            }
            return list;
        }

        /// <summary>
        /// Retrieves all the multilingual properties of a type
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static List<PropertyInfo> GetAllMultilingualProperties(this IMultilingualItem content)
        {
            Type t = content.GetType();
            return _getAllMultilingualPropertiesForType(t);

        }

        /// <summary>
        /// Returns whether the given object contains multilingual properties
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool CheckIfDbObjectContainsMultilingualProperties(this IBaseDbObject db)
        {
            return db is IMultilingualItem;
        }

        /// <summary>
        /// Returns the culture info for an item, for a specific culture.  E.g, Get Italian culture info of 'Product'
        /// </summary>
        /// <param name="multilingualItem"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public static bool CheckIfCultureInfoContainsUntranslatedText(this IMultilingualContentInfo content)
        {
            TextLanguageClassifier languageClassifier = new TextLanguageClassifier();
            var multilingualProperties = GetAllMultilingualProperties(content);
            var culture = content.CultureInfo;

            bool containsUntranslatedText = false;

            foreach (var p in multilingualProperties)
            {
                object val = p.GetValue(content, null);
                if (val is string)
                {
                    string sVal = (string)val;
                    var resultLanguage = languageClassifier.ClassifyLanguage(sVal);
                    if (resultLanguage.HasValue && resultLanguage.Value != culture.LanguageISOCode)
                    {
                        containsUntranslatedText = true;
                        break;
                    }

                }
            }
            return containsUntranslatedText;

        }
       
        /// <summary>
        /// Checks a piece of text, whether it matches the given language.  e.g 'Siamo Italiano' should match 'IT'
        /// </summary>
        /// <param name="txt"></param>
        /// <param name="lang"></param>
        /// <param name="languageClassifier"></param>
        /// <returns></returns>
        public static bool CheckTextWhetherMatchesLanguage(string txt, CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang, TextLanguageClassifier languageClassifier = null)
        {
            if (languageClassifier == null) languageClassifier = new TextLanguageClassifier();

            
            string sVal = (string)txt;
            var resultLanguage = languageClassifier.ClassifyLanguage(sVal);
            return (resultLanguage.HasValue && resultLanguage.Value == lang);
            
        }

        /// <summary>
        /// Checks whether a db object has untranslated text in the given culture. 
        /// </summary>
        /// <param name="dbObject"></param>
        /// <param name="currentCulture"></param>
        /// <returns></returns>
        public static bool CheckIfDbObjectContainsUntranslatedText(IMultilingualItem dbObject, ICultureDetailsBase currentCulture)
        {

            var cultureInfo = GetCultureInfoForMultilingualItemForCulture(dbObject, currentCulture);

            bool containsUntranslated = false;
            if (cultureInfo != null)
            {
                containsUntranslated = CheckIfCultureInfoContainsUntranslatedText(cultureInfo);
            }
            return containsUntranslated;

        }
    }
}
