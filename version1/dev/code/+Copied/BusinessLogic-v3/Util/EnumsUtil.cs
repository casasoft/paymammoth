﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ContentTextModule;

namespace BusinessLogic_v3.Util
{
    using System.Web.UI.WebControls;
    using BusinessLogic_v3.Modules;
    using BusinessLogic_v3.Modules.ContentTextModule;

    public static class EnumUtils
    {

        public static ContentTextBaseFrontend GetWeightUnitAsContentText(CS.General_v3.Enums.WEIGHT_UNITS weightUnit)
        {
            switch (weightUnit)
            {
                case CS.General_v3.Enums.WEIGHT_UNITS.Grams:
                    return Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_WeightUnits_Grams).ToFrontendBase();
                case CS.General_v3.Enums.WEIGHT_UNITS.Kilograms:
                    return Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_WeightUnits_KiloGrams).ToFrontendBase();
                case CS.General_v3.Enums.WEIGHT_UNITS.Ounce:
                    return Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_WeightUnits_Ounce).ToFrontendBase();
                case CS.General_v3.Enums.WEIGHT_UNITS.Pound:
                    return Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_WeightUnits_Pound).ToFrontendBase();
                case CS.General_v3.Enums.WEIGHT_UNITS.Tonne:
                    return Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_WeightUnits_Tonne).ToFrontendBase();
                default:
                    throw new InvalidOperationException("Value <" + weightUnit + "> not mapped");
            }
        }
        /*
        public static ContentTextBaseFrontend GetVolumeUnitAsContentText(CS.General_v3.Enums.VOLUME_UNITS volumeUnit)
        {
            switch (volumeUnit)
            {
                case CS.General_v3.Enums.VOLUME_UNITS.CubicCentimetres:
                    return Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_VolumeUnits_CubicCentimetre).ToFrontendBase();
                case CS.General_v3.Enums.VOLUME_UNITS.CubicMetres:
                    return Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_VolumeUnits_CubicMetre).ToFrontendBase();
                default:
                    throw new InvalidOperationException("Value <" + volumeUnit + "> not mapped");
            }
        }
        */
        /*
        public static ContentTextBaseFrontend GetTemperatureUnitAsContentText(CS.General_v3.Enums.TEMPERATURE_UNITS temperatureUnit)
        {
            switch (temperatureUnit)
            {
                case CS.General_v3.Enums.TEMPERATURE_UNITS.Celcius:
                    return Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_TemperatureUnits_Celcius).ToFrontendBase();
                case CS.General_v3.Enums.TEMPERATURE_UNITS.Fahrenheit:
                    return Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_TemperatureUnits_Fahrenheit).ToFrontendBase();
                default:
                    throw new InvalidOperationException("Value <" + temperatureUnit + "> not mapped");
            }
        }
        */
        /// <summary>
        


        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="blankItemOnTopValue"></param>
        /// <param name="sortItemsAlphabetically">If you have PriorityAttribute with each item, that will take precedence</param>
        /// <param name="addSpacesToCamelCasedName"></param>
        /// <returns></returns>
        public static ListItemCollection GetListItemCollectionFromEnumAndAddContentTexts(Type enumType, out List<ContentTextBaseFrontend> contentTexts, ListItem blankItemOnTop = null, 
            CS.General_v3.Enums.ENUM_SORT_BY sortBy = CS.General_v3.Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false, long? selectedValue = null)
        {
            contentTexts = new List<ContentTextBaseFrontend>();
            //return CS.General_v3.Util.EnumUtils.GetListItemCollectionFromEnum(enumType, blankItemOnTop, sortBy, addSpacesToCamelCasedName, selectedValue);
            ListItemCollection lc = new ListItemCollection();
            List<Enum> values = CS.General_v3.Util.EnumUtils.GetListOfEnumValues(enumType, sortBy);
            //IList<string> names = CS.General_v3.Util.EnumUtils.GetListOfEnumNames(enumType, sortBy);
            var contentTextsWithIdentifierPrefix = ContentTextBaseFactory.Instance.getContentTextsForEnumTypes(enumType);
            for (int i = 0; i < values.Count; i++)
            {
                long value = (Convert.ToInt64(values[i]));
                //var contentText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextForEnumValue(enumType, values[i]).ToFrontendBase();
                var contentText =
                    ContentTextBaseFactory.Instance.getContentTextFromListForEnumValue(
                        contentTextsWithIdentifierPrefix, enumType, values[i]).ToFrontendBase();
                string label = contentText.GetContent();
               // string label = names[i];
                var li = new ListItem(label, value.ToString());
                if (selectedValue.HasValue && value == selectedValue.Value)
                {
                    li.Selected = true;
                }
                lc.Add(li);
                contentTexts.Add(contentText);
            }
            if (blankItemOnTop != null)
            {
                lc.Insert(0, blankItemOnTop);
                contentTexts.Insert(0, null);
            }

            return lc;
        }
        public static ListItemCollection GetListItemCollectionFromEnumAndAddContentTexts(Type enumType, ListItem blankItemOnTop = null,
            CS.General_v3.Enums.ENUM_SORT_BY sortBy = CS.General_v3.Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false, long? selectedValue = null)
        {
            List<ContentTextBaseFrontend> contentTexts = null;
            return GetListItemCollectionFromEnumAndAddContentTexts(enumType, out contentTexts, blankItemOnTop, sortBy, addSpacesToCamelCasedName, selectedValue);
        }
        public static ListItemCollection GetListItemCollectionFromEnumAndAddContentTexts(Type enumType, string blankItemOnTopText = null, CS.General_v3.Enums.ENUM_SORT_BY sortBy = CS.General_v3.Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false)
        {
            ListItem blankItem = null;
            if (blankItemOnTopText != null)
            {
                blankItem = new ListItem(blankItemOnTopText);
            }
            return GetListItemCollectionFromEnumAndAddContentTexts(enumType, blankItem, sortBy, addSpacesToCamelCasedName);
        }

        public static ListItemCollection GetListItemCollectionFromEnumAndAddContentTexts<TEnumType>(ListItem blankItemOnTop = null, CS.General_v3.Enums.ENUM_SORT_BY sortBy = CS.General_v3.Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false, TEnumType? selectedValue = null) where TEnumType : struct, IConvertible
        {
            Type enumType = typeof(TEnumType);
            long? selValue = null;
            if (selectedValue.HasValue) selValue = (Convert.ToInt64(selectedValue.Value));
            return GetListItemCollectionFromEnumAndAddContentTexts(enumType, blankItemOnTop, sortBy, addSpacesToCamelCasedName, selValue);
        }
        public static ListItemCollection GetListItemCollectionFromEnumAndAddContentTexts<TEnumType>(out List<ContentTextBaseFrontend> contentTexts, ListItem blankItemOnTop = null, CS.General_v3.Enums.ENUM_SORT_BY sortBy = CS.General_v3.Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false, TEnumType? selectedValue = null) where TEnumType : struct, IConvertible
        {
            Type enumType = typeof(TEnumType);
            long? selValue = null;
            if (selectedValue.HasValue) selValue = (Convert.ToInt64(selectedValue.Value));
            return GetListItemCollectionFromEnumAndAddContentTexts(enumType, out contentTexts, blankItemOnTop, sortBy, addSpacesToCamelCasedName, selValue);
        }
        public static ListItemCollection GetListItemCollectionFromEnumAndAddContentTextsAndSelectedValues<TEnumType>(ListItem blankItemOnTop = null, 
            CS.General_v3.Enums.ENUM_SORT_BY sortBy = CS.General_v3.Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false, List<TEnumType>
            selectedValues = null) where TEnumType : struct, IConvertible
        {
            Type enumType = typeof (TEnumType);

            ListItemCollection lc = new ListItemCollection();
            List<Enum> values = CS.General_v3.Util.EnumUtils.GetListOfEnumValues(enumType, sortBy);
            IList<string> names = CS.General_v3.Util.EnumUtils.GetListOfEnumNames(enumType, sortBy);

            for (int i = 0; i < values.Count; i++)
            {
                long value = (Convert.ToInt64(values[i]));
                string label = names[i];
                var li = new ListItem(label, value.ToString());

                if(selectedValues != null)
                {
                    foreach (var selectedValue in selectedValues)
                    {
                        long? selValue = null;
                        selValue = (Convert.ToInt64(selectedValue));
                        if (value == selValue)
                        {
                            li.Selected = true;
                            break;
                        }
                    }
                }
                lc.Add(li);
            }
            if (blankItemOnTop != null)
            {
                lc.Insert(0, blankItemOnTop);
            }

            return lc;
        }
    }
}
