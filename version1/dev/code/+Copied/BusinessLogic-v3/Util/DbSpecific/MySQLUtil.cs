﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Processes;
using MySql.Data.MySqlClient;

namespace BusinessLogic_v3.Util.DbSpecific
{
    public static class MySQLUtil
    {
        /// <summary>
        /// Executes the 'SET FOREIGN_KEY_CHECK = X;" statement
        /// </summary>
        /// <param name="p"></param>
        /// <param name="session"></param>
        public static void SetForeignKeyCheck(bool checkForKeys, Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            if (session == null) session = nHibernateUtil.GetCurrentSessionFromContext();
            var sqlQuery = session.CreateSqlQuery("Set FOREIGN_KEY_CHECKS = " + (checkForKeys ? "1" : "0") + ";");
            int result = sqlQuery.ExecuteUpdate();

        }

        public static DbConnection CreateConnection(string host, string database, string user, string pass, int? port, string charset)
        {
            string connstr = GetConnectionString(host, database, user, pass, port, charset);

            return CreateConnection(connstr);
        }
        public static string GetConnectionString(string host, string database, string user, string pass, int? port, string characterSet)
        {
            string charSet = "";
            if (port <= 0) port = null;
            if (!string.IsNullOrEmpty(characterSet))
            {
                charSet = "charset=" + characterSet + ";";
            }

            string connstr = "Data Source=" + host + ";Database="
                        + database + ";User Id=" + user
                                       + ";Password=\"" + pass + "\";" + charSet + "Allow Zero Datetime=Yes;";
            if (port.HasValue)
            {
                connstr += "Port=" + port.Value + ";";
            }
            return connstr;

        }
        public static System.Data.Common.DbConnection CreateConnection(string connectionString)
        {
            string connstr = connectionString;
            MySqlConnection conn = null;
            bool retry = false;
            int times = 0;
            do
            {
                try
                {
                    conn = new MySqlConnection(connstr);
                    retry = false;
                }
                catch (MySqlException ex)
                {
                    if (ex.Message.Trim().ToLower() == "too many connections")
                    {
                        retry = (times < 10);
                        times++;
                        if (!retry)
                        {
                            throw ex;
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(500); //wait half a second
                        }

                    }
                    else
                    {
                        throw ex;
                    }
                }
            } while (retry);
            //conn.Open();
            return conn;
        }

        //public static string DumpDatabase(string mysqldumpPath, string databaseName, string user, string pass, string host = "localhost", int port = 3306, bool extendedInsert = true)
        //{
        //    string args = " --extended-insert="+(extendedInsert ? "TRUE" :"FALSE") +" -u " + user +
        //        " -p" + pass + " -h " + host + " " + databaseName;
        //    Process.MyProcess p = new Process.MyProcess(mysqldumpPath, args);

        //    p.Start();
        //    string db = "";
        //    try
        //    {
        //        db = p.GetOutputText();
        //    }
        //    catch (Exception ex)
        //    {
        //        db = "";
        //    }
        //    return db;
        //}
        public static bool DumpDatabase(string mysqldumpPath, string outputPath, string databaseName,
            string user, string pass, string host = "localhost", int port = 3306, bool extendedInsert = true,
            bool disableKeys = true, bool dumpOnlySchema = false)
        {
            string args = " --extended-insert=" + (extendedInsert ? "TRUE" : "FALSE") + " -u " + user +
                " -p" + pass + " -h " + host;

            if (dumpOnlySchema) args += " -d";
            if (disableKeys) args += " -K";

            args += " --databases " + databaseName;

            ProcessDumpOutputToFile p = new ProcessDumpOutputToFile();
            p.ExecutableFilePath = mysqldumpPath;
            p.Arguments = args;
            p.OutputPath = outputPath;
            bool ok = p.ExecuteProcess();

            p.Dispose();
            return ok;


        }
        /*
        public static string DumpDatabase(string mysqldumpPath)
        {
            return DumpDatabase(mysqldumpPath,CS.General_v3.Settings.Database.DatabaseName,CS.General_v3.Settings.Database.User,
                CS.General_v3.Settings.Database.Pass,CS.General_v3.Settings.Database.Host);
/*            string args = " --extended-insert=FALSE -u " + CS.General_v3.Settings.Database.User + 
                " -p" + CS.General_v3.Settings.Database.Pass + " -h " + CS.General_v3.Settings.Database.Host + " " + CS.General_v3.Settings.Database.Name;
            Process.MyProcess p = new Process.MyProcess(mysqldumpPath, args);
            p.Start();
            string db = p.GetOutputText();

            return db;*-/
        }*/

        public static void GetData(DataRow row, out int o, string name)
        {
            o = Convert.ToInt32(row[name]);
        }
        public static void GetData(DataRow row, out double o, string name)
        {
            o = Convert.ToDouble(row[name]);
        }
        public static void GetData(DataRow row, out DateTime o, string name)
        {
            o = Convert.ToDateTime(row[name]);
        }
        public static void GetData(DataRow row, out bool? o, string name)
        {
            bool b;
            GetData(row, out b, name);
            o = b;

        }
        public static void GetData(DataRow row, out bool o, string name)
        {
            o = Convert.ToBoolean(row[name]);
        }


        public enum CRITERIA_SQL_TYPE
        {
            NameValuePairs,
            LastObjectIsValue,
            LastObjectIsValueSplitToKeywords
        }

        public static string forRegExpMySQL(object txt)
        {
            string tmp = (string)txt;
            if (txt != null && tmp != "")
            {
                tmp = tmp.Replace(@"\", @"\\\\");//replace with four slashes
                tmp = tmp.Replace("’", @"\’");
                tmp = tmp.Replace("‘", @"\‘");
                tmp = tmp.Replace("'", @"\'"); //replace with one slashes before
                tmp = tmp.Replace(".", @"\\."); //replace with two slashes before (
                tmp = tmp.Replace("+", @"\\+");
                tmp = tmp.Replace("*", @"\\*");
                tmp = tmp.Replace("(", @"\\(");
                tmp = tmp.Replace(")", @"\\)");
                tmp = tmp.Replace("^", @"\\^");
                tmp = tmp.Replace("$", @"\\$");
                tmp = tmp.Replace("?", @"\\?");
                tmp = tmp.Replace("|", @"\\|");
                tmp = tmp.Replace("{", @"\\{");
                tmp = tmp.Replace("}", @"\\}");
                tmp = tmp.Replace("-", @"\\-");
                tmp = tmp.Replace("[", @"\\[");
                tmp = tmp.Replace("[", @"\\]");
            }
            else
            {
                tmp = "";
            }
            return tmp;

        }


        /// <summary>
        /// Creates an SQL condition from another related table.  The end result is something similar to
        /// ([NAME] in (SELECT [relatedfield] from [relatedtable] {where [relatedconditions]}))
        /// </summary>
        /// <param name="Name">The property name</param>
        /// <param name="RelatedTable">The table name</param>
        /// <param name="RelatedField">The field name</param>
        /// <param name="RelatedConditions">The conditions. Must include the WHERE!</param>
        /// <returns>The SQL statement</returns>
        public static string CriteriaRelatedForSQL(string Name, string RelatedTable, string RelatedField, string RelatedConditions)
        {

            string SQL = "(" + Name + " in (SELECT " + RelatedField + " from " + RelatedTable + RelatedConditions + "))";
            return SQL;
        }

        /// <summary>
        /// Converts a list into a multiple ORed condition.
        /// Example:  [Name = LIST[0]][NAME = LIST[1]]...
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string CriteriaForSQL(string Name, List<DateTime> list)
        {
            if (list == null) return "";
            object[] oList = new object[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                oList[i] = list[i];
            }
            return CriteriaForSQL(Name, oList);
        }
        /// <summary>
        /// Converts a list into a multiple ORed condition.
        /// Example:  [Name = LIST[0]][NAME = LIST[1]]...
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string CriteriaForSQL(string Name, List<string> list)
        {
            if (list == null) return "";
            object[] oList = new object[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                oList[i] = list[i];
            }
            return CriteriaForSQL(Name, oList);
        }
        /// <summary>
        /// Converts a list into a multiple ORed condition.
        /// Example:  [Name = LIST[0]][NAME = LIST[1]]...
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string CriteriaForSQL(string Name, List<Enum> list)
        {
            if (list == null) return "";

            object[] oList = new object[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                oList[i] = list[i];
            }
            return CriteriaForSQL(Name, oList);
        }

        /// <summary>
        /// Converts a list into a multiple ORed condition.
        /// Example:  [Name = LIST[0]][NAME = LIST[1]]...
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string CriteriaForSQL(string Name, List<double> list)
        {
            if (list == null) return "";
            object[] oList = new object[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                oList[i] = list[i];
            }
            return CriteriaForSQL(Name, oList);
        }

        /// <summary>
        /// Converts a list into a multiple ORed condition.
        /// Example:  [Name = LIST[0]][NAME = LIST[1]]...
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string CriteriaForSQL(string Name, List<int> list)
        {
            if (list == null) return "";
            object[] oList = new object[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                oList[i] = list[i];
            }
            return CriteriaForSQL(Name, oList);
        }
        /// <summary>
        /// Converts an object list into a multiple ORed condition.
        /// Example:  [Name = oList[0]][NAME = oList[1]]...
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string CriteriaForSQL(string Name, object[] oList)
        {
            if (oList == null) return "";

            string SQL = "";
            for (int i = 0; i < oList.Length; i++)
            {
                object o = CheckCriteriaOK(oList[i]);
                if (o != null)
                {
                    if (!(o is string))
                    {
                        SQL += "[" + Name + " = " + forMySql(o) + "]";
                    }
                    else
                    {
                        SQL += "[" + Name + " REGEXP '.*" + forRegExpMySQL(o) + ".*']";
                    }
                }
            }
            if (SQL != "")
                SQL = "(" + SQL + ")";
            return SQL;


        }

        /// <summary>
        /// Returns a string formatted for SQL, with () signifiying ANDed conditions and [] signifiying ORed conditions
        /// </summary>
        /// <param name="type">The type of paramters specified.  
        /// NameValuePairs means that the parameter list alternates 
        /// between NAME1,VALUE1,NAME2,VALUE2,NAME3,VALUE3,etc....
        /// 
        /// LastObjectIsValue means that the paramter list does not alternate but the value is the last one, for example
        /// NAME1,NAME2,NAME3,NAME4,VALUE_FOR_ALL.
        /// 
        /// LastObjectIsValueSplitIntoKeywords means that the value is split into keywords, and each keyword is ORed.  For example, if the value
        /// contains 'a great airport', 'a' must be in any of the columns, 'great' must be in any of the columns, etc
        /// 
        /// All the conditions are then ORed together
        /// </param>
        /// <param name="oList"></param>
        /// <returns></returns>
        public static string CriteriaForSQL(CRITERIA_SQL_TYPE type, params object[] oList)
        {
            string sql = "";
            string currSQL = "";
            object o = null;
            int interval = 0;
            int max = 0;
            if (type == CRITERIA_SQL_TYPE.LastObjectIsValue || type == CRITERIA_SQL_TYPE.LastObjectIsValueSplitToKeywords)
            {
                interval = 1;
                max = oList.Length - 1;
                o = oList[oList.Length - 1];
                currSQL = CriteriaForSQL("TEST", o);
            }

            if (type != CRITERIA_SQL_TYPE.LastObjectIsValueSplitToKeywords && (type == CRITERIA_SQL_TYPE.NameValuePairs || currSQL != ""))
            {
                for (int i = 0; i < max; i += interval)
                {
                    string name = (string)oList[i];
                    if (type == CRITERIA_SQL_TYPE.NameValuePairs)
                    {
                        o = oList[i + 1];
                    }
                    currSQL = CriteriaForSQL(name, o);
                    if (currSQL != "")
                    {
                        sql += "[" + currSQL + "]";
                    }

                }
            }
            else if (type == CRITERIA_SQL_TYPE.LastObjectIsValueSplitToKeywords)
            {
                string sValue = (string)o;
                if (sValue != null)
                {
                    string[] tokens = sValue.Split(' ');
                    for (int j = 0; j < tokens.Length; j++)
                    {
                        string currSQL2 = "";
                        o = tokens[j];

                        for (int i = 0; i < max; i += interval)
                        {
                            string name = (string)oList[i];
                            currSQL = CriteriaForSQL(name, o);
                            if (currSQL != "")
                            {
                                currSQL2 += "[" + currSQL + "]";
                            }

                        }
                        if (currSQL2 != "")
                        {
                            sql += "(" + currSQL2 + ")";
                        }
                    }
                }
            }
            if (sql != "")
                sql = "(" + sql + ")";
            return sql;
        }

        /// <summary>
        /// Returns a condition with a range
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="oFromItem"></param>
        /// <param name="oToItem"></param>
        /// <returns></returns>
        public static string CriteriaForSQL(string Name, object oFromItem, object oToItem)
        {

            object oFrom = CheckCriteriaOK(oFromItem);
            object oTo = CheckCriteriaOK(oToItem);
            if (oFrom is DateTime)
            {
                DateTime tmp = (DateTime)oFrom;
                oFrom = (object)new DateTime(tmp.Year, tmp.Month, tmp.Day, 0, 0, 0);


            }
            if (oTo is DateTime)
            {
                DateTime tmp = (DateTime)oTo;
                oTo = (object)new DateTime(tmp.Year, tmp.Month, tmp.Day, 23, 59, 59);
            }
            string sql = "";
            if ((oFrom != null) || (oTo != null))
            {
                if (oFrom != null)
                {
                    sql += "(" + Name + " >= " + forMySql(oFrom) + ")";
                }
                if (oTo != null)
                {
                    sql += "(" + Name + " <= " + forMySql(oTo) + ")";
                }
            }
            return sql;
        }
        /// <summary>
        /// Searches for a string in the name.  If use regex is set to true, it will search anywhere, and if false it must be an exact match
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="sValue"></param>
        /// <param name="useRegEx"></param>
        /// <returns></returns>
        public static string CriteriaForSQL(string Name, string sValue, bool useRegEx)
        {
            string s = "";
            if (sValue != null && sValue != "")
            {
                if (useRegEx)
                    s = "(" + Name + @" REGEXP '" + forRegExpMySQL(sValue) + @"')";
                else
                    s = "(" + Name + " = " + forMySql(sValue) + ")";
            }
            return s;

        }
        /// <summary>
        /// Does a search, but for strings and as whole words.  For example, if I search for fly, and the word is 'butterfly', it is not found
        /// but if it is "I fly so high", then it is returned
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="value"></param>
        /// <param name="WholeWords">Whether to use whole words or not when searching</param>
        /// <returns></returns>
        public static string CriteriaForSQLWholeWords(string Name, string value)
        {

            string s = "";
            if (value != null && value != "")
            {
                s = "(" + Name + @" REGEXP '(\\||^| |\r|\n|,|\\.|;|:|-|\\+|=)" + forRegExpMySQL(value) + @"(\\||$| |\r|\n|\\.|,|;|:|-|\\+|=)')";
            }
            return s;
        }
        public static string CriteriaForSQL(string Name, object oValue)
        {
            object o = CheckCriteriaOK(oValue);

            string sql = "";
            if (o != null)
            {
                if (!(o is string))
                {
                    sql = "(" + Name + " = " + forMySql(o) + ")";
                }
                else
                {
                    sql = CriteriaForSQL(Name, (string)oValue, true);

                    //                    sql += "(" + Name + " REGEXP '" + forRegExpMySQL(o) + "')";
                }

            }
            return sql;
        }

        /// <summary>
        /// Checks whether a criteria is ok (not set to an unspecified value)
        /// </summary>
        /// <param name="o">object to check</param>
        /// <returns>The new object. Null if criteria is NOT ok</returns>
        public static object CheckCriteriaOK(object o)
        {
            object ret = o;
            if (o != null)
            {
                if (o is DateTime || o is DateTime?)
                {
                    DateTime tmp = (DateTime)o;
                    if (tmp.CompareTo(DateTime.MinValue) == 0)
                    {
                        ret = null;
                    }
                    else
                    {
                        ret = tmp.Date;
                    }
                }

                else if (o is bool || o is bool?)
                {
                    if (o == null)
                    {
                        ret = null;
                    }
                    else
                    {
                        ret = (bool)o;
                    }
                }
                else if (o is string)
                {
                    string tmp = (string)o;
                    if (tmp == "")
                    {
                        ret = null;
                    }
                }
                else if (o is int || o is int?)
                {
                    int tmp = (int)o;
                    if (tmp == int.MinValue)
                    {
                        ret = null;
                    }
                }
                else if (o is double || o is double?)
                {
                    double tmp = (double)o;
                    if (tmp == double.MinValue || (Convert.ToInt32(tmp) == Int32.MinValue))
                    {
                        ret = null;
                    }
                }
                else // is enumeration
                {
                    int tmp = (int)o;
                    if (tmp == 0)
                    {
                        ret = null;
                    }
                }
            }
            else
                ret = null;
            return ret;
        }


        /// <summary>
        /// Returns a formatted string (COND1)(COND2)([COND3][COND4][COND5]...)(COND6)
        /// to the SQL equivalent.  conditions seperated with )( are ANDed together
        /// while those seperated with a ][ are ORed together.  
        /// Adds a WHERE before the final SQL
        /// </summary>
        /// <param name="conds">The string</param>
        /// <returns>The SQL string</returns>
        public static string condsToStr(string conds)
        {
            return condsToStr(conds, true);
        }
        /// <summary>
        /// Returns a formatted string (COND1)(COND2)([COND3][COND4][COND5]...)(COND6)
        /// to the SQL equivalent.  conditions seperated with )( are ANDed together
        /// while those seperated with a ][ are ORed together.
        /// </summary>
        /// <param name="conds">The string</param>
        /// <param name="addWhere">Whether to add a WHERE before</param>
        /// <returns>The SQL string</returns>
        public static string condsToStr(string conds, bool addWhere)
        {
            string txt = conds.Replace(")(", ") AND (");
            txt = txt.Replace("][", ") or (");
            txt = txt.Replace("]", ")");
            txt = txt.Replace("[", "(");
            if (addWhere && txt != "")
                txt = " WHERE " + txt;

            return txt;




        }

        public static string getMySQLNowTime()
        {
            string ret;
            if (CS.General_v3.Settings.Others.ServerGMT > 0)
                ret = "(DATE_ADD(NOW(),INTERVAL " + CS.General_v3.Settings.Others.ServerGMT.ToString() + " HOUR))";
            else if (CS.General_v3.Settings.Others.ServerGMT < 0)
                ret = "(DATE_ADD(NOW(),INTERVAL " + (CS.General_v3.Settings.Others.ServerGMT).ToString() + " HOUR))";
            else
                ret = "NOW()";
            return ret;
        }

        public static string forMySql(object data, bool placeQuotes, bool hasTimeIfDate)
        {
            string ret = "";
            if (data != null && !(data is System.DBNull))
            {
                if (data is string)
                {
                    string tmpData = (string)data;
                    if (tmpData == null)
                        tmpData = "";
                    tmpData = tmpData.Replace("\\", "\\\\");
                    tmpData = tmpData.Replace("'", "\\'");
                    tmpData = tmpData.Replace("\"", "\\\"");
                    tmpData = tmpData.Replace("’", "\\’");
                    tmpData = tmpData.Replace("‘", "\\‘");
                    ret = tmpData;
                    if (placeQuotes)
                        ret = "'" + ret + "'";
                }
                else if (data is int ||
                    data is Int32 || data is Int16 || data is Int64 || data is short
                    || data is byte || data is long || data is Byte ||
                    data is UInt32 || data is uint)
                {
                    ret = data.ToString();
                }
                else if (data is double || data is Double || data is float)
                {
                    if (((data is double || data is Double) && (double)data == double.MinValue) ||
                        ((data is float) && (float)data == float.MinValue))
                        ret = "-1.7976931348623157E+308";
                    else
                        ret = data.ToString();

                }
                else if (data is MySql.Data.Types.MySqlDateTime)
                {
                    MySql.Data.Types.MySqlDateTime mysqlDate = (MySql.Data.Types.MySqlDateTime)data;
                    if (hasTimeIfDate)
                        ret = mysqlDate.GetDateTime().ToString("yyyy-MM-dd HH:mm:ss");
                    else
                        ret = mysqlDate.GetDateTime().ToString("yyyy-MM-dd");
                    if (placeQuotes)
                        ret = "'" + ret + "'";
                }
                else if (data is DateTime)
                {
                    DateTime tmpData = (DateTime)data;
                    if (hasTimeIfDate)
                        ret = tmpData.ToString("yyyy-MM-dd HH:mm:ss");
                    else
                        ret = tmpData.ToString("yyyy-MM-dd");
                    if (placeQuotes)
                        ret = "'" + ret + "'";
                }
                else if (data is char || data is Char)
                {
                    ret = data.ToString();
                    if (placeQuotes)
                        ret = "'" + ret + "'";
                }
                else if (data is bool)
                {
                    if ((bool)data)
                        ret = "1";
                    else
                        ret = "0";

                }
                else // data is enumeration
                {
                    ret = ((int)data).ToString();
                }
            }
            else
            {
                ret = "NULL";
            }
            return ret;

        }

        public static string forMySql(object data)
        {
            return forMySql(data, true, true);
        }
        public static string forMySql(DateTime data, bool hasTime)
        {
            return forMySql(data, true, hasTime);
        }

        /// <summary>
        /// Takes a DateTime object, and converts it into
        /// MySQL format (yyyy-mm-dd]
        /// </summary>
        /// <param name="dt">The date to parse</param>
        /// <returns>The MySQL date string</returns>
        public static string dateForMySQL(DateTime dt)
        {
            return dateForMySQL(dt, false);
        }


        /// <summary>
        /// Takes a DateTime object, and converts it into
        /// MySQL format (yyyy-mm-dd hh:mm:ss] (if it has time)
        /// </summary>
        /// <param name="dt">The date to parse</param>
        /// <param name="hasTime">Whether the string includes time</param>
        /// <returns>The MySQL date string</returns>
        public static string dateForMySQL(DateTime dt, bool hasTime)
        {
            string ret = "";
            ret += dt.Year.ToString() + "-" + dt.Month.ToString("00") + "-" +
                dt.Day.ToString("00");
            if (hasTime)
                ret += " " + dt.Hour.ToString("00") + ":" + dt.Minute.ToString("00") +
                    ":" + dt.Second.ToString("00");
            return ret;
        }

        /// <summary>
        /// Takes a date, formatted in [dd/mm/yyyy] , and converts it into
        /// MySQL format (yyyy-mm-dd]
        /// </summary>
        /// <param name="sDt">The date to parse</param>
        /// <returns>The MySQL date string</returns>
        public static string dateForMySQL(string sDt)
        {
            return dateForMySQL(sDt, false);
        }


        /// <summary>
        /// Takes a date, formatted in [dd/mm/yyyy] and a time, and converts it into
        /// MySQL format (yyyy-mm-dd hh:mm:ss]
        /// </summary>
        /// <param name="sDt">The date & time to parse</param>
        /// <param name="hasTime">Whether the string includes time</param>
        /// <returns>The MySQL date string</returns>
        public static string dateForMySQL(string sDt, bool hasTime)
        {
            DateTime dt = DateTime.Parse(sDt);
            string ret = "";
            ret += dt.Year.ToString() + "-" + dt.Month.ToString("00") + "-" +
                dt.Day.ToString("00");
            if (hasTime)
                ret += " " + dt.Hour.ToString("00") + ":" + dt.Minute.ToString("00") +
                    ":" + dt.Second.ToString("00");
            return ret;
        }


      
    }
}
