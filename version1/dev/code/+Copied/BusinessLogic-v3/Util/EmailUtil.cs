﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Text;

namespace BusinessLogic_v3.Util
{
    using CS.General_v3.Classes.Email;
    using Modules.CultureDetailsModule;
using CS.General_v3.Classes.Text;
    using CS.General_v3.Classes.HelperClasses;

    public static class EmailUtil
    {
        public static EmailMessage CreateEmailMessage(
            string subject, 
            string htmlContent, 
            string fromEmail,
            string fromName,
            string toEmail,
            string toName,
            bool sendSync = false,
            string contentTag = "[CONTENT]",
            string subjectTag = "[SUBJECT]",
            bool loadWithDefaultTemplate = true, 
            bool convertImagesToLinkedResources = true, 
            bool convertURLsToFull = true, 
            string baseURL = null, 
            ITokenReplacer tokenReplacer = null,
            nVelocityContext nVelocityContext = null)
        {
            EmailMessage email = new EmailMessage();
            email.SetFromEmail(fromEmail, fromName);
            email.AddToEmails(toEmail, toName);
            
            email.TokenReplacer = tokenReplacer;






            htmlContent = CS.General_v3.Util.EmailUtil.ParseDefaultEmailTokens(htmlContent);
            string htmlText = "";
            htmlText = CS.General_v3.Util.EmailUtil.ParseDefaultEmailTokens(htmlText);

            if (loadWithDefaultTemplate)
            {
                string htmlTemplate = BusinessLogic_v3.Modules.Factories.EmailTextFactory.GetHtmlEmailTemplate();
                htmlText = htmlTemplate;

                string signature = BusinessLogic_v3.Modules.Factories.EmailTextFactory.GetEmailSignature();
                if (signature == null)
                {
                    signature = string.Empty;
                }

                string signatureTag = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Email_SignatureTag);
                if (string.IsNullOrWhiteSpace(signatureTag)) signatureTag = "[Signature]";

                htmlText = CS.General_v3.Util.Text.ReplaceTexts(htmlText, htmlContent, contentTag); //replace content tag with real content
                htmlText = CS.General_v3.Util.Text.ReplaceTexts(htmlText, signature, signatureTag);
            }
            else
            {
                htmlText = htmlContent;
            }
            //string content = CS.General_v3.Util.Text.GetTextBetween(contentTag, contentTag, ref htmlText, false, false);
            htmlText = CS.General_v3.Util.Text.ReplaceTexts(htmlText, subject, subjectTag);
            string plainText = htmlContent;
            plainText = CS.General_v3.Util.Text.ConvertHTMLToPlainText(plainText, true);
            if (string.IsNullOrEmpty(plainText))
                plainText = htmlText;

            var parser = new CS.General_v3.Classes.Email.EmailHtmlParser();
            htmlText = parser.ParseHTML(htmlText, convertImagesToLinkedResources, convertURLsToFull, baseURL);

            email.Resources = parser.LinkedResources;
            if (tokenReplacer != null)
            {
                subject = tokenReplacer.ReplaceString(subject);
                htmlText = tokenReplacer.ReplaceString(htmlText);
                plainText = tokenReplacer.ReplaceString(plainText);
                
            }
            if (nVelocityContext != null)
            {
                subject = nVelocityContext.ParseTemplate(subject);
                htmlText = nVelocityContext.ParseTemplate(htmlText);
                plainText = nVelocityContext.ParseTemplate(plainText);
            }
            email.BodyHtml = htmlText;
            email.BodyPlain = plainText;
            email.Subject = subject;
            
            return email;


        }
    }
}
