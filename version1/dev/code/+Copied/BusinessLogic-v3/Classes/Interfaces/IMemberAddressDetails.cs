﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Interfaces
{
    public interface IMemberAddressDetails
    {
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string AddressLine3 { get; set; }
        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? AddressCountry { get; set; }
        string AddressLocality { get; set; }
        string AddressState { get; set; }
        string AddressPostCode { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string MiddleName { get; set; }
         void CopyMemberAddressDetails(IMemberAddressDetails copyFrom);
         string GetAddressAsOneLine(string delimeter = ", ", bool includeCountry = false);
         string GetFullName();
        
    }
}
