﻿using System;
using System.Web.UI;

namespace BusinessLogic_v3.Classes.Interfaces
{
    public interface IListingItemData
    {
        BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject DbObject { get; }
        long ID { get; }
        string GetTitle();
        string GetShortDescription(bool convertFullDescriptionToPlainTextIfShortDescriptionIsEmpty, bool requiredAsHtml);

        string ImageUrl { get; }
        string Url { get; }
        bool HasDiscount { get; }
        double PriceActual { get; }
        double PriceBeforeDiscount { get; }
        string ReferenceCode { get; }
        DateTime CreatedOnDate { get; }
        Control GetFreeText();
    }
}
