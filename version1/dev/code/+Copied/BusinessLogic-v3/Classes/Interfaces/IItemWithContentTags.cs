﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DefaultValues;
using BusinessLogic_v3.Classes.DbObjects;

namespace BusinessLogic_v3.Classes.Interfaces
{
    public interface IItemWithContentTags : IBaseContentDefaultValueAttribute, IBaseDbObject
    {
      
        string[] CustomContentTags { get; set; }

        string GetItemContent();
    }
}
