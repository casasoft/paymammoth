﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using Quartz;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Modules.MemberModule.SubscriptionManager;

namespace BusinessLogic_v3.Classes.Background
{
    public class MemberExpiryChecker 
    {

        private MemberExpiryChecker()
        {

        }
        public static MemberExpiryChecker Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<MemberExpiryChecker>(); } }
        
        #region IJob Members

        private object _padlock = new object();

        public void CheckForExpiredMembers()
        {
            lock (_padlock)
            {
                NHClasses.NhManager.CreateNewSessionForContext();
                MemberSubscriptionManager.Instance.CheckSubscriptions();
                NHClasses.NhManager.DisposeCurrentSessionInContext();
            }
        }

        #endregion
    }
}
