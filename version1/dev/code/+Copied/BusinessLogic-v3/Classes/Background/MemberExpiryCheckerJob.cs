﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.SettingModule;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using Quartz;
using CS.General_v3.Classes.Attributes;
using Quartz.Impl;

namespace BusinessLogic_v3.Classes.Background
{
    public class MemberExpiryCheckerJob : IJob
    {
        public static void InitJob()
        {
            if (!CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
            {

                int repeatEveryMinutes =
                    SettingBaseFactory.Instance.GetSetting<int>(
                        Enums.BusinessLogicSettingsEnum.MemberExpiryCheckerJobFrequencyInMinutes, true);
                IJobDetail job = new JobDetailImpl("MemberExpiryCheckerJob", typeof(MemberExpiryCheckerJob));
                ITrigger t = TriggerBuilder.Create().WithIdentity("MemberExpiryCheckerTrigger").StartNow().WithSimpleSchedule(x => x.WithIntervalInMinutes(repeatEveryMinutes).RepeatForever())
                    .Build();
                
                
                BusinessLogic_v3.Classes.Scheduling.QuartzNet.Scheduler.ScheduleJob(job, t);
            }
            
        }


        #region IJob Members

        public void Execute(IJobExecutionContext context)
        {
            
        //    NhManagerIOC.NhManager.CreateNewSessionForContext();
            MemberExpiryChecker.Instance.CheckForExpiredMembers();
        //    NhManagerIOC.NhManager.DisposeCurrentSessionInContext();
        }

        #endregion
        


    }
}
