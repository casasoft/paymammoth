﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Modules.SettingModule;
using Quartz;
using log4net;

namespace BusinessLogic_v3.Classes.Background
{
    public abstract class BaseRecurringTask<T>
    {
        private ILog _log = null;
        public static T Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<T>(); } }
        public bool Started { get; set; }
        protected readonly object _lock = new object();
        private IJobDetail _currentJob = null;
        public Enum SettingForRecurringTaskInterval { get; set; }

        public BaseRecurringTask(Enum settingForRecurringTaskInterval)
        {
            _log = LogManager.GetLogger(this.GetType());
            this.SettingForRecurringTaskInterval = settingForRecurringTaskInterval;
            
        }

        void Instance_OnItemUpdate(IBaseDbObject item, Enums.UPDATE_TYPE updateType)
        {
            if (this.Started)
            {
                SettingBase setting = (SettingBase) item;
                if (setting.CheckIfIdentifierIsEnum(this.SettingForRecurringTaskInterval))
                {//if setting changed, restart
                    Restart();
                }
            }
        }



        protected abstract void recurringTask();
        private readonly object _taskLock = new object();
        private void _recurringTaskCall()
        {
            lock (_taskLock)
            { //only once at a time
                if (_log.IsDebugEnabled) _log.Debug("Recurring Task - Start");
                recurringTask();
                if (_log.IsDebugEnabled) _log.Debug("Recurring Task - Stop");
            }
        }


        private void startRecurringTask()
        {

            int repeatIntervalInSecs = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(this.SettingForRecurringTaskInterval);
            _currentJob = BusinessLogic_v3.Util.SchedulingUtil.ScheduleRepeatMethodCall(_recurringTaskCall, "RecurringTask-" + this.GetType().Name, null,
                intervalToRepeatInSecs: repeatIntervalInSecs,
                totalTimesToRepeat: null);

        }

        public void StartIfNotStarted()
        {
            lock (_lock)
            {
                if (!this.Started)
                    Start();
            }
        }
        public void StopIfStarted()
        {
            lock (_lock)
            {
                if (this.Started)
                    Stop();
            }
        }

        public void Start()
        {
            lock (_lock)
            {
                if (this.Started)
                    throw new InvalidOperationException("Cannot be started twice");
                BusinessLogic_v3.Modules.SettingModule.SettingBaseFactory.Instance.OnItemUpdate += new DbObjects.General.OnItemUpdateHandler(Instance_OnItemUpdate);

                startRecurringTask();
                this.Started = true;
            }
        }

        public void Stop()
        {
            lock (_lock)
            {
                if (!this.Started)
                    throw new InvalidOperationException("Cannot be stopped if not started");
                BusinessLogic_v3.Modules.SettingModule.SettingBaseFactory.Instance.OnItemUpdate -= new DbObjects.General.OnItemUpdateHandler(Instance_OnItemUpdate);
                BusinessLogic_v3.Util.SchedulingUtil.StopJob(_currentJob);
                this.Started = false;
            }
        }
        public void Restart()
        {
            Stop();
            Start();
        }



    }
}
