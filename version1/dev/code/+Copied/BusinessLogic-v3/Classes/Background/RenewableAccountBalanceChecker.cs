﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Modules.MemberModule.AccountBalanceManager;

namespace BusinessLogic_v3.Classes.Background
{
    public class RenewableAccountBalanceChecker
    {
        private RenewableAccountBalanceChecker()
        {

        }
        public static RenewableAccountBalanceChecker Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<RenewableAccountBalanceChecker>(); } }
        
        #region IJob Members

        private object _padlock = new object();

        public void CheckRenewableAccountBalances()
        {
            lock (_padlock)
            {
                NHClasses.NhManager.CreateNewSessionForContext();
                MemberAccountBalanceManager.Instance.CheckRenewableAccountBalances();
                NHClasses.NhManager.DisposeCurrentSessionInContext();
            }
        }

        #endregion
    }
}
