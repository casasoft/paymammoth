﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule;

namespace BusinessLogic_v3.Classes.Background
{
    public class SelfBarringNotificationChecker : BusinessLogic_v3.Classes.Notifications.NotificationSender.BaseNotificationSender<SelfBarringNotificationChecker, MemberSelfBarringPeriodBase>
    {

        public SelfBarringNotificationChecker()
            : base(
                BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Member_ResponsibleGaming_SelfBarring_ExpiryNotifier_CheckEveryXSeconds,
                BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Member_ResponsibleGaming_SelfBarring_ExpiryNotifier_PageSize)
        {
            addNotifications();

        }
        private void addNotifications()
        {
            base.AddNotificationToSend(
                x => x.NotificationExpiry1Sent,
                x => x.NotificationExpiry1_DateToSend,
                x => x.SendNotificationExpiry1Email());
            base.AddNotificationToSend(
                x => x.NotificationExpiry2Sent,
                x => x.NotificationExpiry2_DateToSend,
                x => x.SendNotificationExpiry2Email());
            base.AddNotificationToSend(
                x => x.NotificationExpiredSent,
                x => x.EndDate,
                x => x.SendExpiredEmail());
        }


    }
}
