﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using BusinessLogic_v3.Modules.SettingModule;
using Quartz.Impl;

namespace BusinessLogic_v3.Classes.Background
{
    public class RenewableAccountBalanceCheckerJob : IJob
    {
        public static void InitJob()
        {
            if (!CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
            {

                int repeatEveryMinutes =
                    SettingBaseFactory.Instance.GetSetting<int>(
                        Enums.BusinessLogicSettingsEnum.RenewableAccountBalanceCheckerJobFrequencyInMinutes, true);
                IJobDetail job = new JobDetailImpl("RenewableAccountBalanceCheckerJob", typeof(RenewableAccountBalanceCheckerJob));
                ITrigger t = TriggerBuilder.Create().WithIdentity("RenewableAccountBalanceCheckerTrigger").StartNow().WithSimpleSchedule(x => x.WithIntervalInMinutes(repeatEveryMinutes).RepeatForever())
                    .Build();


                BusinessLogic_v3.Classes.Scheduling.QuartzNet.Scheduler.ScheduleJob(job, t);
            }

        }


        #region IJob Members

        public void Execute(IJobExecutionContext context)
        {

            //    NhManagerIOC.NhManager.CreateNewSessionForContext();
            RenewableAccountBalanceChecker.Instance.CheckRenewableAccountBalances();
            //    NhManagerIOC.NhManager.DisposeCurrentSessionInContext();
        }

        #endregion
    }
}
