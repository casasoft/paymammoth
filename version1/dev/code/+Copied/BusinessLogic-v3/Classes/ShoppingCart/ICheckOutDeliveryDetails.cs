﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.ShoppingCart
{
    interface ICheckOutDeliveryDetails
    {
        string Address1 { get; set; }
        string Address2 { get; set; }
        string Locality { get; set; }
        string Postcode { get; set; }
        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 Country { get; set; }

    }
}
