﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.ShoppingCart
{
    public class CheckOutDeliveryDetails : ICheckOutDeliveryDetails
    {
        public string Address1 { get; set;}
        public string Address2 { get; set; }
        public string Locality { get; set; }
        public string Postcode { get; set; }
        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 Country { get; set; }
    }
}
