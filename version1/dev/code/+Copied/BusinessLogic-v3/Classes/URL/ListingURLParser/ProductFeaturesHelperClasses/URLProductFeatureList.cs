﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.URL.ListingURLParser.ProductFeaturesHelperClasses
{
    public class URLProductFeatureList : List<URLProductFeature>
    {
        public override string ToString()
        {
            string s = "";
            for (int i = 0; i < Count; i++)
            {
                if (i > 0)
                {
                    s += ";";
                }
                s += this[i].ToString();
            }
            return s;
        }

        public static URLProductFeatureList FromString(string s)
        {
            URLProductFeatureList featList = new URLProductFeatureList();
            if (!string.IsNullOrEmpty(s))
            {
                var data = s.Split(';');
                foreach (var item in data) { featList.Add(URLProductFeature.FromString(item)); }
            }
            return featList;
        }

    }
}
