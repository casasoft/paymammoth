﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryModule;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.URL.ListingURLParser.ProductFeaturesHelperClasses;

namespace BusinessLogic_v3.Classes.URL.ListingURLParser
{
    public class ListingParameters
    {
        public string Keywords { get; set; }
        public List<BrandBase> Brands { get; set; }
        public List<CategoryBase> Categories { get; set; }
        private URLProductFeatureList _productFeatures;
        public bool? ViewSpecial { get; set; }
        public URLProductFeatureList ProductFeatures
        {
            get {
                int z = 3;
                return _productFeatures; }
            set {
                int x = 2;
                _productFeatures = value; 
            }
        }

        public ListingParameters()
        {
            ProductFeatures = new URLProductFeatureList();
            this.Brands = new List<BrandBase>();
            this.Categories = new List<CategoryBase>();
        }

        public static List<BrandBase> GetBrandsFromBrandIDs(List<int> brandIDs)
        {
            var brandList = new List<BrandBase>();
            if (brandIDs.IsNotNullOrEmpty())
            {
                foreach (var id in brandIDs)
                {
                    var b = BrandBase.Factory.GetByPrimaryKey(id);
                    if (b != null)
                    {
                        brandList.Add(b);
                    }
                }
            }
            return brandList;
        }

        public static List<CategoryBase> GetCategoriesFromCategoryIDs(List<int> categoryIDs)
        {
            List<CategoryBase> categoryList = new List<CategoryBase>();
            if (categoryIDs.IsNotNullOrEmpty())
            {
                foreach (var id in categoryIDs)
                {
                    var c = CategoryBaseFactory.Instance.GetByPrimaryKey(id);
                    if (c != null)
                    {
                        categoryList.Add(c);
                    }
                }
            }
            return categoryList;
        }

        public static List<string> GetCategoryTitlesFromCategories(List<CategoryBase> list)
        {
            List<string> titleList = new List<string>();
            foreach (var item in list)
            {
                titleList.Add(item.TitlePlural);
            }
            return titleList;
        }

        public static List<string> GetBrandTitlesFromBrands(List<BrandBase> list)
        {
            List<string> titleList = new List<string>();
            foreach (var item in list)
            {
                titleList.Add(item.Title);
            }
            return titleList;
        }


        private static List<string> getTitlesFromList<T>(List<T> list, Func<T, string> titleDelegate)
        {
            List<string> titlesList = new List<string>();
            foreach (var item in list)
            {
                titlesList.Add(titleDelegate(item));
            }
            return titlesList;
        }

        public static List<long> GetIDsFromList<T>(List<T> list, Func<T, long> idDelegate) {
            List<long> idList = new List<long>();
            foreach (var item in list)
            {
                idList.Add(idDelegate(item));
            }
            return idList;
        }

    }
}
