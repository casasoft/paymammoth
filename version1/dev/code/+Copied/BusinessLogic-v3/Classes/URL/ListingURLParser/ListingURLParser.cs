﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.URL.URLParser.v1;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.Routing;
using CS.General_v3.Controls.WebControls.Specialized.Paging;

namespace BusinessLogic_v3.Classes.URL.ListingURLParser
{
    public abstract class ListingURLParser : URLParser_Base, IParamsInfo
    {
        public ListingParameters ListingParameters { get; set; }
        private string _customURL = null;

        public ListingURLParser(string id, string customURL = null) : base(id, urlToLoad: customURL)
        {
            _customURL = customURL; 
            this.PagingInfo = new URLParser_PagingInfo(this);
        }
      
        public List<string> GetKeywordsAsList()
        {
            List<string> list = new List<string>();
            string s = this.ListingParameters.Keywords ?? "";
            if (!string.IsNullOrEmpty(s))
            {
                list.AddRange(s.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries));
            }
            return list;
        }

        protected override string getPageURL()
        {
            if (_customURL.IsNotNullOrEmpty())
            {
                return _customURL;
            }
            else
            {
                throw new InvalidOperationException("Custom URL is null or empty. And no specific route data is specified.");
            }
        }

        public URLParser_PagingInfo PagingInfo { get; private set; }


        #region IParamsInfo Members

        IPagingInfo IParamsInfo.PagingInfo
        {
            get
            {
                return (IPagingInfo)PagingInfo;
            }
            set
            {
                PagingInfo = (URLParser_PagingInfo)value;
            }
        }

        #endregion
    }
}
