﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.Routing;
using BusinessLogic_v3.Classes.URL.ListingURLParser.ProductFeaturesHelperClasses;

namespace BusinessLogic_v3.Classes.URL.ListingURLParser
{
    public class ListingURLParserShop : ListingURLParser
    {
        private const string PARAM_FILTERS = "filters";

        public ListingURLParserShop(string id, string customURL, bool initValuesFromURL = true)
            : base(id, customURL)
        {
            ListingParameters = new ListingParameters();
            this.AddParameterRouteData(ShopRoute.ROUTE_PARAM_KEYWORDS, null, true);
            this.AddParameterRouteData(ShopRoute.ROUTE_NAME_CATEGORY, null, true);
            //this.AddParameterRouteData(ShopRoute.ROUTE_PARAM_BRANDS, null, true);
            this.AddParameterRouteData(ShopRoute.ROUTE_PARAM_CATEGORY_ID, null, true);
            //this.AddParameterRouteData(ShopRoute.ROUTE_PARAM_BRAND_IDS, null, true);
            //this.AddParameterRouteData(ShopRoute.ROUTE_PARAM_VIEWSPECIAL, null, true);
            this.AddParameterQueryString(PARAM_FILTERS, null, false, false);

            if (initValuesFromURL)
            {
                initValues();
            }
        }

        private void initValues()
        {
            this.ListingParameters.Keywords = this.GetParameter(ShopRoute.ROUTE_PARAM_KEYWORDS).GetValue();
            this.ListingParameters.Categories = ListingParameters.GetCategoriesFromCategoryIDs(this.GetParameter(ShopRoute.ROUTE_PARAM_CATEGORY_ID).GetValueAsListOfInteger());
            //this.ListingParameters.Brands = ListingParameters.GetBrandsFromBrandIDs(this.GetParameter(ShopRoute.ROUTE_PARAM_BRAND_IDS).GetValueAsListOfInteger());
            //this.ListingParameters.ProductFeatures = URLProductFeatureList.FromString(this.GetParameter(PARAM_FILTERS).GetValue());
            //this.ListingParameters.ViewSpecial = this.GetParameter(ShopRoute.ROUTE_PARAM_VIEWSPECIAL).GetValueAsBool();
        }

        protected override void updateURLParameterValues()
        {
            //if (ListingParameters.Keywords.IsNotNullOrEmpty())
            //{
            //    this.GetParameter(ShopRoute.ROUTE_PARAM_KEYWORDS).SetValue(ListingParameters.Keywords);
            //}

            //if (ListingParameters.ViewSpecial.HasValue)
            //{
            //    this.GetParameter(ShopRoute.ROUTE_PARAM_VIEWSPECIAL).SetValue(ListingParameters.ViewSpecial);
            //}

            //if (ListingParameters.Categories.IsNotNullOrEmpty())
            //{
            //    this.GetParameter(ShopRoute.ROUTE_PARAM_CATEGORIES).SetValue(ListingParameters.GetCategoryTitlesFromCategories(ListingParameters.Categories));
            //    this.GetParameter(ShopRoute.ROUTE_PARAM_CATEGORY_IDS).SetValue(ListingParameters.GetIDsFromList(ListingParameters.Categories, (item) => item.ID));           
            //}
            //if (ListingParameters.Brands.IsNotNullOrEmpty())
            //{
            //    this.GetParameter(ShopRoute.ROUTE_PARAM_BRANDS).SetValue(ListingParameters.GetBrandTitlesFromBrands(ListingParameters.Brands));
            //    this.GetParameter(ShopRoute.ROUTE_PARAM_BRAND_IDS).SetValue(ListingParameters.GetIDsFromList(ListingParameters.Brands, (item) => item.ID));           
            //}
            //if (ListingParameters.ProductFeatures.IsNotNullOrEmpty())
            //{
            //    this.GetParameter(PARAM_FILTERS).SetValue(ListingParameters.ProductFeatures.ToString());
            //}
        }

        protected override string getRouteName()
        {
            //if (this.ListingParameters.Brands.IsNotNullOrEmpty() && this.ListingParameters.Categories.IsNotNullOrEmpty())
            //{
            //    return ShopRoute.ROUTE_NAME_BRANDS_AND_CATEGORIES;
            //}
            //else if (this.ListingParameters.Brands.IsNotNullOrEmpty() && this.ListingParameters.Keywords.IsNotNullOrEmpty())
            //{
            //    return ShopRoute.ROUTE_NAME_KEYWORDS_AND_BRANDS;
            //}
            //else if (this.ListingParameters.Categories.IsNotNullOrEmpty() && this.ListingParameters.Keywords.IsNotNullOrEmpty())
            //{
            //    return ShopRoute.ROUTE_NAME_KEYWORDS_AND_CATEGORIES;
            //}
            //else if (this.ListingParameters.Keywords.IsNotNullOrEmpty())
            //{
            //    return ShopRoute.ROUTE_NAME_KEYWORDS;
            //}
            //else if (this.ListingParameters.Brands.IsNotNullOrEmpty())
            //{
            //    return ShopRoute.ROUTE_NAME_BRANDS;
            //}
            //else if (this.ListingParameters.Categories.IsNotNullOrEmpty())
            //{
            //    return ShopRoute.ROUTE_NAME_CATEGORIES;
            //}
            //else if (this.ListingParameters.ViewSpecial.HasValue)
            //{
            //    return ShopRoute.ROUTE_NAME_VIEW_SPECIAL;
            //}
            //else
            //{
            //    return ShopRoute.ROUTE_NAME_DEFAULT_SEARCH;
            //}
            return null;
        }
    }
}
