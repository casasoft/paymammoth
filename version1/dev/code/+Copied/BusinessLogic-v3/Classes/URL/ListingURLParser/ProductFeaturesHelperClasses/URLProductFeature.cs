﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.URL.ListingURLParser.ProductFeaturesHelperClasses
{
    public class URLProductFeature
    {
          public string ID { get; set; } 
        public List<string> Values { get; set; }

        public URLProductFeature()
        {
            Values = new List<string>();
        }
        
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append(ID + ",");
            foreach (var val in Values)
            {
                if (val.Equals(Values.LastOrDefault()))
                {
                    str.Append(val);
                }
                else
                {
                    str.Append(val + ",");
                }
            }
            return str.ToString();
        }

        public static URLProductFeature FromString(string s)
        {
            URLProductFeature pFeature = new URLProductFeature();
            var splittedString = s.Split(',');
            pFeature.ID = splittedString[0].ToString();
            for (int i = 1; i < splittedString.Length; i++)
            {
                pFeature.Values.Add(splittedString[i]);
            }
            return pFeature;
        }
    }
    
}
