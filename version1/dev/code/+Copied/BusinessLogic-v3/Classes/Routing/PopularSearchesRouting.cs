﻿using System.IO;
namespace BusinessLogic_v3.Classes.Routing
{
    public static class PopularSearchesRouting
    {
        public const string ROUTE_NAME_POPULAR_SEARCHES = "popularSearches";
        public const string ROUTE_PARAM_ID = "Id";
        public const string ROUTE_PARAM_POPULAR_SEARCH = "PopularSearchTitle";
        public static string PhysicalFileLocation = "/popular-search.aspx";
        public static void MapRoute()
        {
            //Properties
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME_POPULAR_SEARCHES, "/search/{" + ROUTE_PARAM_POPULAR_SEARCH + "}/{" + ROUTE_PARAM_ID + "}/", PhysicalFileLocation, null,
               new CS.General_v3.Classes.Routing.MyRouteValueDictionary(ROUTE_PARAM_ID, "[0-9]+"));

        }
        public static void MapRouteIfFileExists()
        {
            string localPath = CS.General_v3.Util.PageUtil.MapPath(PhysicalFileLocation);
            if (File.Exists(localPath))
            {
                MapRoute();
            }
        }

        public static string GetUrl(string title, long id)
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME_POPULAR_SEARCHES, new CS.General_v3.Classes.Routing.MyRouteValueDictionary(
                 ROUTE_PARAM_POPULAR_SEARCH, CS.General_v3.Util.RoutingUtil.ConvertStringForRouteUrl(title), ROUTE_PARAM_ID, id.ToString()));
        }
    }
}
