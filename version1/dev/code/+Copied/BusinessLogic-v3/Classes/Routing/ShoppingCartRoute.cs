﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Routing;

namespace BusinessLogic_v3.Classes.Routing
{
    public class ShoppingCartRoute
    {
        //public static string SHOPPING_CART_ROUTE_NAME = "shoppingCartRoute";
        public static string DELIVERY_DETAILS_ROUTE_NAME = "deliveryDetailsRoute";
        public static string CHECKOUT_ROUTE_NAME = "checkOutRoute";

        public static void MapRoute()
        {
            //initShoppingCartRoute();
            initDeliveryDetailsRoute();
            initCheckOutRoute();
        }

        private static void initCheckOutRoute()
        {
            string checkoutPhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_CheckOut);
            string checkoutVirtualFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RouteVirtualPath_CheckOut);
            CS.General_v3.Util.RoutingUtil.MapPageRoute(CHECKOUT_ROUTE_NAME, checkoutVirtualFilePath, checkoutPhysicalFilePath, null, null);
        }

        private static void initDeliveryDetailsRoute()
        {
            string deliveryDetailsPhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_DeliveryDetails);
            string deliveryDetailsVirtualFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RouteVirtualPath_DeliveryDetails);
            CS.General_v3.Util.RoutingUtil.MapPageRoute(DELIVERY_DETAILS_ROUTE_NAME, deliveryDetailsVirtualFilePath, deliveryDetailsPhysicalFilePath, null, null);
        }

        
        //public static string GetCartURL()
        //{
        //    return CS.General_v3.Util.RoutingUtil.GetRouteUrl(SHOPPING_CART_ROUTE_NAME, null);
        //}

        public static string GetCheckOutURL()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(CHECKOUT_ROUTE_NAME, null);
        }

        public static string GetDeliveryDetailsURL()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(DELIVERY_DETAILS_ROUTE_NAME, null);
        }
    }
}
