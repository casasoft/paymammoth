﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Routing.Member
{
    public class ResetPasswordRoute
    {
        public static string ROUTE_NAME = "resetPasswordRoute";

        public static void MapRoute()
        {
            string resetPasswordPhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_ResetPassword);
            string resetPasswordVirtualFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RouteVirtualPath_ResetPassword);
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME, resetPasswordVirtualFilePath, resetPasswordPhysicalFilePath, null, null);
        }

        public static string GetURL()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME, null);
        }
    }
}
