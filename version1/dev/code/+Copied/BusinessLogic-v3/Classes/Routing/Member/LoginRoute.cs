﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
namespace BusinessLogic_v3.Classes.Routing.Member
{
    public class LoginRoute
    {
        public static string ROUTE_NAME = "loginRoute";

        public static void MapRoute()
        {
            string loginPhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_Login);
            string loginVirtualFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RouteVirtualPath_Login);
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME, loginVirtualFilePath, loginPhysicalFilePath, null, null);
        }

        public static string GetURL()
        {
            var loginPage = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Members_LoginPage).ToFrontendBase();
            return loginPage.GetUrl();
        }
    }
}
