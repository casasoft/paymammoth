﻿namespace BusinessLogic_v3.Classes.Routing.Member
{
    public class ProfileRoute
    {
        public static string ROUTE_NAME = "profileRoute";

        public static void MapRoute()
        {
            string profilePhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_Profile);
            string profileVirtualFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RouteVirtualPath_Profile);
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME, profileVirtualFilePath, profilePhysicalFilePath, null, null);
        }

        public static string GetURL()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME, null);
        }
    }
}
