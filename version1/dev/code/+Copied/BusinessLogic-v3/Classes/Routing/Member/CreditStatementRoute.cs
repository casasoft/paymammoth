﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Routing.Member
{
    public class CreditStatementRoute
    {
        public static string ROUTE_NAME = "creditStatementRoute";

        public static void MapRoute()
        {
            string logoutPhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_CreditStatement);
            string logoutVirtualFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RouteVirtualPath_CreditStatement);
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME, logoutVirtualFilePath, logoutPhysicalFilePath, null, null);
        }

        public static string GetURL()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME, null);
        }
    }
}
