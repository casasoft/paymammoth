﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Routing.Member
{
    public class ForgotPasswordRoute
    {
        public static string ROUTE_NAME = "forgotPasswordRoute";

        public static void MapRoute()
        {
            string forgotPasswordVirtualFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RouteVirtualPath_ForgotPassword);
            string forgotPasswordPhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_ForgotPassword);
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME, forgotPasswordVirtualFilePath, forgotPasswordPhysicalFilePath, null, null);
        }

        public static string GetURL()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME, null);
        }
    }
}
