﻿namespace BusinessLogic_v3.Classes.Routing.Member
{
    public class RegisterRoute
    {
        public static string ROUTE_NAME = "registerRoute";

        public static void MapRoute()
        {
            string registerPhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_Register);
            string registerVirtualFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RouteVirtualPath_Register);
            
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME, registerVirtualFilePath, registerPhysicalFilePath, null, null);
        }

        public static string GetURL()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME, null);
        }
    }
}
