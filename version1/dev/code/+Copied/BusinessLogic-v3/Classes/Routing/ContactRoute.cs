﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Routing
{
    public class ContactRoute
    {
        public static string ROUTE_NAME = "contactRoute";
        public static void MapRoute()
        {
            string profilePhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_ContactUs);
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME, "/contact/", profilePhysicalFilePath, null, null);
        }

        public static string GetURL()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME, null);
        }
    }
}
