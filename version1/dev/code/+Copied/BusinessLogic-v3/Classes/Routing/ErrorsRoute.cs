﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Classes.Routing;
using BusinessLogic_v3.Frontend.RoutingInfoModule;

namespace BusinessLogic_v3.Classes.Routing
{
    using Extensions;
    using BusinessLogic_v3.Constants;
    using CS.General_v3.Util;

    public static class ErrorsRoute
    {
        /*
        public static string ROUTE_NAME = "routeErrors";
        public static string ROUTE_PARAM_IDENTIFIER = "Identifier";

        public static void MapRoute()
        {
            string errorsPhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_Error);

            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME, "/error/{" + ROUTE_PARAM_IDENTIFIER + "}", errorsPhysicalFilePath, null, null);

        }*/
        private static RoutingInfoBaseFrontend getRouteInfo()
        {
            var route = BusinessLogic_v3.Modules.Factories.RoutingInfoFactory.GetRouteByIdentifier(BusinessLogic_v3.Enums.RouteUrlsEnum.Error).ToFrontendBase();
            return route;

        }

        public static Enums.ERROR_PAGE_TYPE? GetErrorIdentifierFromRoute()
        {
            return RoutingUtil.GetRouteValue<Enums.ERROR_PAGE_TYPE?>(Tokens.ROUTE_PARAM_ID);
        }

        public static string GetPageNotFoundErrorURL()
        {
            var route = getRouteInfo();
            MyRouteValueDictionary rvd = new MyRouteValueDictionary();
            rvd[Tokens.ROUTE_PARAM_ID] = BusinessLogic_v3.Enums.ERROR_PAGE_TYPE.PageNotFound.ToString();
            return route.Data.GetMappedUrlForRoute(rvd);
        }
        public static string GetGenericErrorURL()
        {
            var route = getRouteInfo();
            MyRouteValueDictionary rvd = new MyRouteValueDictionary();
            rvd[Tokens.ROUTE_PARAM_ID] = BusinessLogic_v3.Enums.ERROR_PAGE_TYPE.Generic.ToString();
            return route.Data.GetMappedUrlForRoute(rvd);
        }
    }
}
