﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Routing
{
    public class CheckOutRoute
    {
        public static string ROUTE_NAME = "checkOutRoute";

        public static void MapRoute()
        {
            string logoutPhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_CheckOut);
            string logoutVirtualFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RouteVirtualPath_CheckOut);
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME, logoutVirtualFilePath, logoutPhysicalFilePath, null, null);
        }

        public static string GetURL()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME, null);
        }
    }
}
