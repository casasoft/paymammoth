﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Routing;

namespace BusinessLogic_v3.Classes.Routing
{
    public class OrderRoute
    {
        public static string ROUTE_NAME = "orderRoute";

        public static string ROUTE_PARAM_ID = "order_route_id";

        public static void MapRoute()
        {
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME, "/orders/invoices/{" + ROUTE_PARAM_ID + "}",
                "/_ComponentsGeneric/Frontend/Pages/Common/Orders/invoice.aspx",
                 null,
                 new MyRouteValueDictionary(ROUTE_PARAM_ID, "[0-9]+"));
        }

        public static long? GetIdFromCurrentRoute()
        {
            return CS.General_v3.Util.PageUtil.GetFromRouteData<long?>(ROUTE_PARAM_ID);
        }

        public static string GetURL(long id)
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME, new MyRouteValueDictionary(ROUTE_PARAM_ID, id.ToString()));
        }
    }
}
