﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Routing;

namespace BusinessLogic_v3.Classes.Routing
{
    public static class AdvertRoute
    {

        public static string ROUTE_NAME_ADVERT = "advertRoute";

        public static string ROUTE_PARAM_TITLE = "advert_route_title";
        public static string ROUTE_PARAM_ID = "advert_route_id";

        public static void MapRoute()
        {
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME_ADVERT, "/Redirect/{" + ROUTE_PARAM_TITLE + "}/{" + ROUTE_PARAM_ID + "}",
                "/_ComponentsGeneric/Frontend/Pages/Common/Advert/advertRedirect.aspx",
                 null,
                 new MyRouteValueDictionary(ROUTE_PARAM_ID, "[0-9]+"));
        }

        public static long? GetIdFromCurrentRoute()
        {
            return CS.General_v3.Util.PageUtil.GetFromRouteData<long?>(ROUTE_PARAM_ID);
        }

        public static string GetAdvertLinkURL(string title, long id)
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME_ADVERT, new MyRouteValueDictionary(
                    ROUTE_PARAM_TITLE, CS.General_v3.Util.RoutingUtil.ConvertStringForRouteUrl(title),
                    ROUTE_PARAM_ID, id.ToString()));
        }

    }
}
