﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Routing
{
    public static class VacancyRoute
    {
        public static string ROUTE_NAME = "vacancy_route";
        public static string ROUTE_PARAM_TITLE = "Title";
        public static string ROUTE_PARAM_ID = "Id";

        public static void MapRoute()
        {
            CS.General_v3.Util.RoutingUtil.MapPageRoute(
                routeName: ROUTE_NAME,
                routeUrl:
                    "/vacancy/{" + ROUTE_PARAM_TITLE + "}/{" + ROUTE_PARAM_ID + "}/",
                physicalFile: "/vacancy.aspx",
                defaultValues: null,
                constraints: new CS.General_v3.Classes.Routing.MyRouteValueDictionary(ROUTE_PARAM_ID, "[0-9]+"));
        }

        public static long? GetVacancyIDFromRouteData()
        {
            long? vacancyID = CS.General_v3.Util.RoutingUtil.GetRouteValueAsLongNullable(ROUTE_PARAM_ID);
            return vacancyID;
        }

        public static string GetURL(long id, string title)
        {
            string routeTitle = CS.General_v3.Util.Text.TxtForRewrite(title);
            string url = "";
            url = CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME,
                                                                          new CS.General_v3.Classes.Routing.
                                                                              MyRouteValueDictionary(
                                                                              ROUTE_PARAM_TITLE, routeTitle,
                                                                              ROUTE_PARAM_ID, id.ToString()));
            return url;
        }
    }
}
