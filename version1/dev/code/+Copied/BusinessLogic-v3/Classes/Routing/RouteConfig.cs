﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Routing.Member;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;

using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using BusinessLogic_v3.Modules.RoutingInfoModule;

namespace BusinessLogic_v3.Classes.Routing
{
    public class RouteConfig
    {
        public static RouteConfig Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<RouteConfig>(
                    CS.General_v3.Classes.Application.AppInstance.Instance.GetProjectAssemblies());
            }
        }

        protected RouteConfig()
        {

        }

        protected virtual void mapAdditionalRoutes()
        {
            
            //ArticlesRoute.MapRoute();
           // ErrorsRoute.MapRoute();

            if (ModuleSettings.Shop.IsCurrentWebsiteAShop)
            {
                ShopRoute.MapRoutes();
            }

            if(ModuleSettings.Generic.IsSiteWithProducts)
            {
                ProductRoute.MapRoute();
            }

            if (ModuleSettings.Generic.IsSiteWithRegistration)
            {
                RegisterRoute.MapRoute();
                LoginRoute.MapRoute();
                LogoutRoute.MapRoute();
                OrderHistoryRoute.MapRoute();
                CreditStatementRoute.MapRoute();
                ForgotPasswordRoute.MapRoute();
                ResetPasswordRoute.MapRoute();
            }

            if (ModuleSettings.Generic.IsSiteWithOrders)
            {
                OrderRoute.MapRoute();
                PurchaseSuccessRoute.MapRoute();
                ShoppingCartRoute.MapRoute();
            }

            if(ModuleSettings.Generic.IsSiteWithClassifieds)
            {
                ClassifiedRoute.MapRoute();
            }

            ContactRoute.MapRoute();
            PopularSearchesRouting.MapRouteIfFileExists();
            
        }

        private void mapRoutes()
        {
            NHClasses.NhManager.CreateNewSessionForContext();
            
            mapAdditionalRoutes();
            
            RoutingInfoBaseFactory.Instance.RegisterRoutes<Enums.RouteUrlsEnum>();

            NHClasses.NhManager.DisposeCurrentSessionInContext();
        }

        public void MapRoutes()
        {
            if (!CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
            {
                CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(mapRoutes, "RouteConfig-mapRoutes");
            }

        }
    }
}
