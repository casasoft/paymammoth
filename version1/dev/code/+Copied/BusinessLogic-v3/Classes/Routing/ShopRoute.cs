﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util;
using CS.General_v3.Classes.Routing;

namespace BusinessLogic_v3.Classes.Routing
{
    public class ShopRoute
    {
        private const string REGEX_IDS = "[0-9]+(,[0-9]+)*";

        public static string SEARCH_PAGE_URL = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_SearchPage);

        #region Route names
        //public static string ROUTE_NAME_KEYWORDS = "route_keywords";
        //public static string ROUTE_NAME_BRANDS = "route_brands";
        //public static string ROUTE_NAME_CATEGORIES = "route_categories";
        //public static string ROUTE_NAME_BRANDS_AND_CATEGORIES = "route_brands_and_categories";
        //public static string ROUTE_NAME_KEYWORDS_AND_BRANDS = "route_keywords_and_brands";
        //public static string ROUTE_NAME_KEYWORDS_AND_CATEGORIES = "route_keywords_and_categories";
        //public static string ROUTE_NAME_VIEW_SPECIAL = "route_view_special";
        //public static string ROUTE_NAME_DEFAULT_SEARCH = "route_search_shop_default";

        public static string ROUTE_NAME_CATEGORY = "route_category";
        public static string ROUTE_NAME_KEYWORDS = "route_keywords";
        public static string ROUTE_NAME_DEFAULT_SEARCH = "route_default";
        #endregion

        #region Route parameters
        public static string ROUTE_PARAM_KEYWORDS = "Keywords";
        public static string ROUTE_PARAM_CATEGORY_TITLE = "Category";
        public static string ROUTE_PARAM_CATEGORY_ID = "CategoryID";

        //public static string ROUTE_PARAM_BRAND_IDS = "BrandIDs";
        //public static string ROUTE_PARAM_VIEWSPECIAL = "ViewSpecial";
        //public static string ROUTE_PARAM_BRANDS = "Brands";

        #endregion

        public static void MapRoutes()
        {
            initKeywordsRoute();
            initCategoriesRoute();
            initDefaultSearchRoute();

            ProductRoute.MapRoute();

        }

        #region - ShopRoute version 2

        //private static void initViewSpecialRoute()
        //{
        //    RoutingUtil.MapPageRoute(
        //     routeName: ROUTE_NAME_VIEW_SPECIAL,
        //     routeUrl: "/search/view-special/{" + ROUTE_PARAM_VIEWSPECIAL + "}/",
        //     physicalFile: SEARCH_PAGE_URL,
        //     defaultValues: null,
        //     constraints: null);
        //}

        //private static void initKeywordsAndBrandsRoute()
        //{
        //    RoutingUtil.MapPageRoute(
        //      routeName: ROUTE_NAME_KEYWORDS_AND_BRANDS,
        //      routeUrl: "/search/keywords/{" + ROUTE_PARAM_KEYWORDS + "}/brands/{" + ROUTE_PARAM_BRANDS + "}/{" + ROUTE_PARAM_BRAND_IDS + "}/",
        //      physicalFile: SEARCH_PAGE_URL,
        //      defaultValues: null,
        //      constraints: new MyRouteValueDictionary(ROUTE_PARAM_BRAND_IDS, REGEX_IDS));
        //}


        //private static void initKeywordsAndCategoriesRoute()
        //{
        //    RoutingUtil.MapPageRoute(
        //      routeName: ROUTE_NAME_KEYWORDS_AND_CATEGORIES,
        //      routeUrl: "/search/keywords/{" + ROUTE_PARAM_KEYWORDS + "}/categories/{" + ROUTE_PARAM_CATEGORIES + "}/{" + ROUTE_PARAM_CATEGORY_IDS + "}/",
        //      physicalFile: SEARCH_PAGE_URL,
        //      defaultValues: null,
        //      constraints: new MyRouteValueDictionary(ROUTE_PARAM_CATEGORY_IDS, REGEX_IDS));
        //}


        //private static void initBrandsAndCategoriesRoute()
        //{
        //    RoutingUtil.MapPageRoute(
        //      routeName: ROUTE_NAME_BRANDS_AND_CATEGORIES,
        //      routeUrl: "/search/brands/{" + ROUTE_PARAM_BRANDS + "}/categories/{" + ROUTE_PARAM_CATEGORIES + "}/{" + ROUTE_PARAM_CATEGORY_IDS + "}-{" + ROUTE_PARAM_BRAND_IDS + "}/",
        //      physicalFile: SEARCH_PAGE_URL,
        //      defaultValues: null,
        //      constraints: new MyRouteValueDictionary(ROUTE_PARAM_BRAND_IDS, REGEX_IDS, ROUTE_PARAM_CATEGORY_IDS, REGEX_IDS));
        //}

        //private static void initBrandsRoute()
        //{
        //    RoutingUtil.MapPageRoute(
        //      routeName: ROUTE_NAME_BRANDS,
        //      routeUrl: "/search/brands/{" + ROUTE_PARAM_BRANDS + "}/{" + ROUTE_PARAM_BRAND_IDS + "}/",
        //      physicalFile: SEARCH_PAGE_URL,
        //      defaultValues: null,
        //      constraints: new MyRouteValueDictionary(ROUTE_PARAM_BRAND_IDS, REGEX_IDS));
        //}
        #endregion  


        private static void initDefaultSearchRoute()
        {
            RoutingUtil.MapPageRoute(
             routeName: ROUTE_NAME_DEFAULT_SEARCH,
             routeUrl: "/products/",
             physicalFile: SEARCH_PAGE_URL,
             defaultValues: null,
             constraints: null);
        }


        private static void initCategoriesRoute()
        {
            RoutingUtil.MapPageRoute(
            routeName: ROUTE_NAME_CATEGORY,
            routeUrl: "/products/{" + ROUTE_PARAM_CATEGORY_TITLE + "}/{" + ROUTE_PARAM_CATEGORY_ID + "}", 
            physicalFile: SEARCH_PAGE_URL,
            defaultValues: null,
            constraints: new MyRouteValueDictionary(ROUTE_PARAM_CATEGORY_ID, REGEX_IDS));
        }

        private static void initKeywordsRoute()
        {
            RoutingUtil.MapPageRoute(
                routeName: ROUTE_NAME_KEYWORDS,
                routeUrl: "/products/{" + ROUTE_PARAM_KEYWORDS + "}/",
                physicalFile: SEARCH_PAGE_URL,
                defaultValues: null,
                constraints: null);
        }

        public static long? GetCategoryIDFromRouteData()
        {
            long? eventID = CS.General_v3.Util.RoutingUtil.GetRouteValueAsLongNullable(ROUTE_PARAM_CATEGORY_ID);
            return eventID;
        }

    }
}
