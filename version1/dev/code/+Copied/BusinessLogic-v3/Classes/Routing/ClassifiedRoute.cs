﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Constants;
using CS.General_v3.Classes.Routing;

namespace BusinessLogic_v3.Classes.Routing
{
    public class ClassifiedRoute
    {
        public static string ROUTE_NAME_CLASSIFIED = "classifiedPageRoute";

        public static string ROUTE_PARAM_TITLE = "classified_route_title";
        public static string ROUTE_PARAM_ID = "classified_route_id";

        public static void MapRoute()
        {
            CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME_CLASSIFIED, "/classifieds/{" + ROUTE_PARAM_TITLE + "}/{" + ROUTE_PARAM_ID + "}",
                "/_ComponentsGeneric/Frontend/Pages/Common/Classified/classified.aspx",
                 null,
                 new MyRouteValueDictionary(ROUTE_PARAM_ID, "[0-9]+"));
        }

        public static long? GetIdFromCurrentRoute()
        {
            return CS.General_v3.Util.PageUtil.GetFromRouteData<long?>(ROUTE_PARAM_ID);
        }

        public static string GetURL(string title, long id)
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME_CLASSIFIED, new MyRouteValueDictionary(
                    ROUTE_PARAM_TITLE, CS.General_v3.Util.RoutingUtil.ConvertStringForRouteUrl(title),
                    ROUTE_PARAM_ID, id.ToString()));
        }
    }
}
