﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Classes.Routing
{
    public static class ProductRoute
    {
        public static string ROUTE_NAME_WITH_CATEGORY = "product_and_category_route";
        public static string ROUTE_NAME = "product_route";

        public static string ROUTE_PARAM_TITLE = "Title";
        public static string ROUTE_PARAM_ID = "Id";
        public static string ROUTE_PARAM_CATEGORY = "Category";

        public static void MapRoute()
        {
            CS.General_v3.Util.RoutingUtil.MapPageRoute(
            routeName: ROUTE_NAME_WITH_CATEGORY, 
            routeUrl: "/product/{" + ROUTE_PARAM_TITLE + "}/{"+ ROUTE_PARAM_CATEGORY + "}/{" + ROUTE_PARAM_ID + "}/", 
            physicalFile: "/product.aspx",
            defaultValues: null, 
            constraints: new CS.General_v3.Classes.Routing.MyRouteValueDictionary(ROUTE_PARAM_ID, "[0-9]+"));

            CS.General_v3.Util.RoutingUtil.MapPageRoute(
            routeName: ROUTE_NAME,
            routeUrl: "/product/{" + ROUTE_PARAM_TITLE + "}/{" + ROUTE_PARAM_ID + "}/",
            physicalFile: "/product.aspx",
            defaultValues: null,
            constraints: new CS.General_v3.Classes.Routing.MyRouteValueDictionary(ROUTE_PARAM_ID, "[0-9]+"));
        
        }

        public static long? GetProductIDFromRouteData()
        {
            long? itemGroupID = CS.General_v3.Util.RoutingUtil.GetRouteValueAsLongNullable(ROUTE_PARAM_ID);
            return itemGroupID;
        }

        public static string GetURL(long id, string title, string categoryTitle)
        {
            string routeTitle = CS.General_v3.Util.Text.TxtForRewrite(title);
            string routeCategoryTitle = CS.General_v3.Util.Text.TxtForRewrite(categoryTitle);

            string url = "";
            if (categoryTitle.IsNotNullOrEmpty())
            {
                url = CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME_WITH_CATEGORY,
                                                                              new CS.General_v3.Classes.Routing.
                                                                                  MyRouteValueDictionary(
                                                                                  ROUTE_PARAM_TITLE, routeTitle,
                                                                                  ROUTE_PARAM_ID, id.ToString(),
                                                                                  ROUTE_PARAM_CATEGORY,
                                                                                  routeCategoryTitle));
            }
            else
            {
                url = CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME,
                                                                          new CS.General_v3.Classes.Routing.
                                                                              MyRouteValueDictionary(
                                                                              ROUTE_PARAM_TITLE, routeTitle,
                                                                              ROUTE_PARAM_ID, id.ToString()));
            }
            return url;
        }
    }
}
