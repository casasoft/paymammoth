﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
namespace BusinessLogic_v3.Classes.Routing
{
    public class PurchaseSuccessRoute
    {
        public static string ROUTE_NAME = "purchaseRoute";
        public const string ROUTE_DEFAULT_VIRTUAL_PATH = "/members/purchase/success/";
        public static void MapRoute()
        {
            string physicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_PurchaseSuccess);
            if (!string.IsNullOrEmpty(physicalFilePath))
            {
                CS.General_v3.Util.RoutingUtil.MapPageRoute(ROUTE_NAME, ROUTE_DEFAULT_VIRTUAL_PATH, physicalFilePath, null, null);
            }
        }

        public static string GetURL()
        {
            var article = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.PayPippa_FinalSuccessPage).ToFrontendBase();
            return article.GetUrl();
        }
    }
}
