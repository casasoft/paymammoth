﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Classes.Routing;

namespace BusinessLogic_v3.Classes.Routing
{
    using Constants;
    using Extensions;

    public static class ArticlesRoute
    {
        /*ublic static bool ROUTE_INCLUDE_CULTURE = false;
        public static string ROUTE_PARAM_CULTURE = "Culture";
        public static string ROUTE_PARAM_CULTURE_CONSTRAINT = "[a-zA-Z]{2}";
        public static string ROUTE_PARAM_LANG = "Lang";

        public static string ROUTE_NAME = "routeArticle";
        public static string ROUTE_NAME_DIALOG = "routeDialogArticle"; 
        public static string ROUTE_PARAM_TITLE = "Title";
        public static string ROUTE_PARAM_ID = "Id";
            
        public static void MapRoute()
        {
            bool prependMultilingualPrefix = ModuleSettings.Generic.MultiLingualPrependRoutingURLsWithLanguageCode;
            string articlePhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_Article);
            string articleDialogPhysicalFilePath = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.RoutePhysicalPath_DialogArticle);

            mapArticleRoute(prependMultilingualPrefix, "content", articlePhysicalFilePath, ROUTE_NAME);
            mapArticleRoute(prependMultilingualPrefix, "content-dialog", articleDialogPhysicalFilePath, ROUTE_NAME_DIALOG);
           
        }
        private static void mapArticleRoute(bool prependMultilingualPrefix, string prefix, string physicalFilePath, string routeName)
        {
            if (prependMultilingualPrefix)
            {
                CS.General_v3.Util.RoutingUtil.MapPageRoute(routeName, "{" + ROUTE_PARAM_LANG + "}/" + prefix + "/{" + ROUTE_PARAM_TITLE + "}/{" + ROUTE_PARAM_ID + "}/", physicalFilePath, null,
                                                                 new CS.General_v3.Classes.Routing.MyRouteValueDictionary(ROUTE_PARAM_ID, "[0-9]+"));
            }
            else
            {
                CS.General_v3.Util.RoutingUtil.MapPageRoute(routeName, "/" + prefix + "/{" + ROUTE_PARAM_TITLE + "}/{" + ROUTE_PARAM_ID + "}/", physicalFilePath, null,
                                                                  new CS.General_v3.Classes.Routing.MyRouteValueDictionary(ROUTE_PARAM_ID, "[0-9]+"));
            }
        }

        //public static void MapRoute2()
        //{
        //    bool includeLanguage = ROUTE_INCLUDE_CULTURE;

        //    string routeURL = CS.General_v3.Util.Text.AppendStrings("/",
        //        "Article",
        //        (includeLanguage ? "{" + ROUTE_PARAM_CULTURE + "}" : null),
        //        "{" + ROUTE_PARAM_TITLE + "}",
        //        "{" + ROUTE_PARAM_ID + "}");

        //    var rvd = new MyRouteValueDictionary(ROUTE_PARAM_ID, "[0-9]+");
        //    if (includeLanguage)
        //    {
        //        rvd[ROUTE_PARAM_CULTURE] = ROUTE_PARAM_CULTURE_CONSTRAINT;
        //    }



        //    CS.General_v3.Util.RoutingUtil.MapPageRoute(
        //        ROUTE_NAME, routeURL,
        //        "/" + ROUTE_NAME, null,
        //        rvd);

        //   // CS.General_v3.Util.RoutingUtil.GetRouteValue
        //}
            */
        public static long? GetIDFromRouteData()
        {
            long? contentPageID = CS.General_v3.Util.RoutingUtil.GetRouteValueAsLongNullable(Tokens.ROUTE_PARAM_ID);
            return contentPageID;
        }

        //public static string GetUR2L(long id, string title, string culture = null)
        //{
        //    if (string.IsNullOrWhiteSpace(title)) title = "Article";
        //    string generatedRouteTitle = title;
        //    string routeTitle = CS.General_v3.Util.Text.TxtForRewrite(generatedRouteTitle);
        //    var rvd = new CS.General_v3.Classes.Routing.MyRouteValueDictionary(ROUTE_PARAM_TITLE, routeTitle,
        //            ROUTE_PARAM_ID, id.ToString());
        //    if (ROUTE_INCLUDE_CULTURE)
        //    {
        //        if (culture == null)
        //            culture = CS.General_v3.Enums.ISO_ENUMS.Language_ISO639_3letter_ToCode(BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture().GetLanguageISOEnumValue());
        //        rvd[ROUTE_PARAM_CULTURE] = culture;
        //    }

        //    string url = CS.General_v3.Util.RoutingUtil.GetRouteUrl(ROUTE_NAME,
        //        rvd);
        //    return url;
        //}
        public static string GetURL(long id, string title, bool dialogPage)
        {

            var routeInfo = BusinessLogic_v3.Modules.Factories.RoutingInfoFactory.GetRouteByIdentifier(dialogPage ? BusinessLogic_v3.Enums.RouteUrlsEnum.ArticleDialog : Enums.RouteUrlsEnum.Article).ToFrontendBase();
            //string routeName = dialogPage ? ROUTE_NAME_DIALOG : ROUTE_NAME;

            if (string.IsNullOrWhiteSpace(title)) title = "Content Page";
            string generatedRouteTitle = title;
            string routeTitle = CS.General_v3.Util.Text.TxtForRewrite(generatedRouteTitle);

            var rvd = new CS.General_v3.Classes.Routing.MyRouteValueDictionary(Tokens.ROUTE_PARAM_TITLE, routeTitle, Tokens.ROUTE_PARAM_ID, id.ToString());
           /* if (ModuleSettings.Generic.MultiLingualPrependRoutingURLsWithLanguageCode)
            {
                var lang = Modules.Factories.CultureDetailsFactory.GetCurrentCultureInSession();//.CultureDetailsBaseFactory.Instance.GetCurrentCultureInSession();
                rvd.Add(ROUTE_PARAM_LANG, lang.GetLanguage2LetterCode());
            }*/


            string url = routeInfo.Data.GetMappedUrlForRoute(rvd);
            return url;
        }
    }
}
