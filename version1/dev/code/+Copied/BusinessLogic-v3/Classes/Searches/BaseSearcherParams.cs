﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;

namespace BusinessLogic_v3.Classes.Searches
{
    public class BaseSearcherParams
    {
        

        public Enum SortBy { get; set; }
        public string TestSessionGuid { get; set; }
        public int PageNo { get; set; }
        public int ShowAmount { get { return _showAmt; } set { setShowAmount(value); } }
        private int _showAmt { get; set; }

        private void setShowAmount(int amount)
        {
            int maxShowAmount =
                Factories.SettingFactory.GetSettingValue<int>(
                    Enums.BusinessLogicSettingsEnum.Paging_MaximumShowAmountValueAllowedForSearches);
            _showAmt = amount < maxShowAmount ? amount : maxShowAmount;
        }
    }
}
