﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Culture;
using BusinessLogic_v3.Classes.LuceneClasses;
using BusinessLogic_v3.Classes.LuceneClasses.Analyzers;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.EventBasicModule;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Classes.Searches.EventBasicSearcher
{
    public class EventBasicSearcher : BaseSearcher<EventBasicBase, EventBasicSearcherParams>
    {
        protected override NHibernate.IQueryOver<EventBasicBase, EventBasicBase> createQuery()
        {
            return EventBasicBaseFactory.Instance.GetQuery();
        }

        protected override void addQueryConditions()
        {
            addKeywordFilterToQuery();
            addStartEndDatesToQuery();
            addSortByToQuery();
            base.addQueryConditions();
        }

        private void addSortByToQuery()
        {
            if (SearchParams.SortBy != Enums.EVENTS_SORT_BY.Relevance)
            {
                switch (SearchParams.SortBy)
                {
                    case Enums.EVENTS_SORT_BY.StartDateAsc:
                        _query = _query.OrderBy(item => item.StartDate).Asc;
                        break;
                    case Enums.EVENTS_SORT_BY.StartDateDesc:
                        _query = _query.OrderBy(item => item.EndDate).Desc;
                        break;
                    case Enums.EVENTS_SORT_BY.TitleAscending:
                        _query = _query.OrderBy(item => item.Title).Asc;
                        break;
                    case Enums.EVENTS_SORT_BY.TitleDescending:
                        _query = _query.OrderBy(item => item.Title).Desc;
                        break;
                    default:
                        throw new InvalidOperationException("Sort by <" + SearchParams.SortBy + "> not implemented!");
                }
            }
        }

        private void addStartEndDatesToQuery()
        {
            if (SearchParams.StartDateFrom.HasValue)
            {
                _query = _query.Where(item => item.StartDate <= SearchParams.StartDateFrom.Value && item.EndDate >= SearchParams.StartDateFrom.Value);
            }
            if (SearchParams.EndDateTo.HasValue)
            {
                _query = _query.Where(item => item.EndDate <= SearchParams.EndDateTo.Value);
            }
        }

        private void addKeywordFilterToQuery()
        {
            if (!string.IsNullOrWhiteSpace(SearchParams.Keywords))
            {
                LanguageAnalyzer analyzer = new LanguageAnalyzer();
                string strs = analyzer.AnalyseString(SearchParams.Keywords, (CultureDetailsBase)DefaultCultureManager.Instance.GetCulture());
                Type classType = EventBasicBaseFactory.Instance.GetMainTypeCreatedByFactory();
                IEnumerable<long> results = LuceneUtil.GetResults<EventBasicBase>(analyzer, classType, strs, null,
                                                                                  item => item.DescriptionHtml,
                                                                                  item => item.Title);
                var disjunction = Restrictions.Disjunction();
                foreach (long itemId in results)
                {
                    disjunction.Add(Restrictions.On<EventBasicBase>(item => item.ID).IsLike(itemId));
                }
                _query = _query.Where(disjunction);
            }
        }
    }
}
