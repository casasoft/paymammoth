﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Searches.EventBasicSearcher
{
    public class EventBasicSearcherParams : BaseSearcherParams
    {
        public string Keywords { get; set; }
        public new Enums.EVENTS_SORT_BY SortBy { get; set; }
        public DateTime? StartDateFrom { get; set; }
        public DateTime? EndDateTo { get; set; }
    }
}
