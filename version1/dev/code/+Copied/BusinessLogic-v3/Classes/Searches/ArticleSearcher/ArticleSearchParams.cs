﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Iesi.Collections.Generic;
using BusinessLogic_v3.Modules.ArticleModule;

namespace BusinessLogic_v3.Classes.Searches.ArticleSearcher
{
    public class ArticleSearchParams : BaseSearcherParams
    {
        public static ArticleSearchParams CreateLastTypeInstance()
        {
            return CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<ArticleSearchParams>();
        }
        public ArticleSearchParams()
        {
            this.Parents = new HashedSet<IArticleBase>();
            this.KeywordsSearchInHtmlText = true;
        }

        public string Keywords { get; set; }
        public bool KeywordsSearchInHtmlText { get; set; }
        
        public int TotalResults { get; set; }
        public new BusinessLogic_v3.Enums.CONTENT_PAGE_SORT_BY SortBy { get; set; }
        public HashedSet<IArticleBase> Parents { get; set; }
        private bool _showOnlyLeafNodes = true;
        public bool ShowOnlyLeafNodes
        {
            get { return _showOnlyLeafNodes; }
            set { _showOnlyLeafNodes = value; }
        }

        private bool _excludeItemsNotVisibleInFrontend = true;
        public bool ExcludeItemsNotVisibleInFrontend
        {
            get { return _excludeItemsNotVisibleInFrontend; }
            set { _excludeItemsNotVisibleInFrontend = value; }
        }

        public bool IncludeNotVisibleInFrontend { get; set; }

        public bool LoadAlsoNonSearchableItems { get; set; }

        public bool IsFeatured { get; set; }

        public DateTime? LastPublishedFrom { get; set; }
        public DateTime? LastPublishedTo { get; set; }

        public HashedSet<long> ArticleIdsToExclude { get; set; }
    }
}
