﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using BusinessLogic_v3.Modules.ArticleModule;
using CS.General_v3.Extensions;
using NHibernate;

using NHibernate.Criterion;
using nHibernateUtil = BusinessLogic_v3.Util.nHibernateUtil;
using BusinessLogic_v3.Classes.Exceptions;
using BusinessLogic_v3.Classes.LuceneClasses.Analyzers;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.Culture;
using BusinessLogic_v3.Classes.LuceneClasses;
using BusinessLogic_v3.Modules.Article_CultureInfoModule;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Util;
using BusinessLogic_v3.Modules;

namespace BusinessLogic_v3.Classes.Searches.ArticleSearcher
{
    public class ArticleSearcher
    {
        public ArticleSearchParams SearchParams { get; set; }

        public List<string> _keywordsList;

        public ArticleSearcher()
        {
            SearchParams = new ArticleSearchParams();
            _keywordsList = new List<string>();
        }

        private IQueryOver<ArticleBase, ArticleBase> _m_query = null;
        public IQueryOver<ArticleBase, ArticleBase> _query
        {
            get
            {
                if (_m_query == null)
                {
                    _m_query = createQuery();
                }
                return _m_query;

            }
            set { _m_query = value; }
        }

        private IEnumerable<long> luceneIds { get; set; }

        private void addExcludeItemsNotVisibleInFrontendToSearchCriteria()
        {
            if (SearchParams.ExcludeItemsNotVisibleInFrontend)
            {
                _query = _query.Where((item) => item.Published);
            }
        }

        public SearchPagedResults<IArticleBase> GetSearchResults()
        {
            addQueryConditions();
            if (SearchParams.SortBy != Enums.CONTENT_PAGE_SORT_BY.Relevance || string.IsNullOrWhiteSpace(SearchParams.Keywords))
            {
                int totalResults = 0;
                IEnumerable<ArticleBase> resultsList = nHibernateUtil.
                    LimitCriteriaByPrimaryKeysAndReturnResultAndTotalCount<ArticleBase>(
                        _query.RootCriteria,

                        SearchParams.PageNo,
                        SearchParams.ShowAmount,
                        out
                            totalResults);
                return new SearchPagedResults<IArticleBase>(resultsList, totalResults);
            }
            else
            {
                return getResultsCombinedWithLucene();
            }
        }

        private SearchPagedResults<IArticleBase> getResultsCombinedWithLucene()
        {
            int maxAmt =
                Factories.SettingFactory.GetSettingValue<int>(
                    Enums.BusinessLogicSettingsEnum.ArticleSearcher_MaxAmountOfIDsToLoadForLuceneSearch);
            _query.Take(maxAmt);
            _query = _query.Select(Projections.Property("ID"));
            List<long> limitedIds = _query.List<long>().ToList();
            SearchPagedResults<IArticleBase> results = new SearchPagedResults<IArticleBase>(new List<ArticleBase>(), 0);

            if (limitedIds.Count > 0)
            {
                LanguageAnalyzer analyzer = new LanguageAnalyzer();
                string strs = analyzer.AnalyseString(SearchParams.Keywords,
                                                     (CultureDetailsBase)
                                                     DefaultCultureManager.Instance.GetCulture());
                Type classType = Article_CultureInfoBaseFactory.Instance.GetMainTypeCreatedByFactory();
                int totalResults;
                luceneIds = LuceneUtil.GetResultsFromMultilingual<Article_CultureInfoBase, ArticleBase>(analyzer,
                                                                                                        classType,
                                                                                                        strs,
                                                                                                        out totalResults,
                                                                                                        null,
                                                                                                        SearchParams.
                                                                                                            PageNo,
                                                                                                        SearchParams.
                                                                                                            ShowAmount,
                                                                                                        limitedIds,
                                                                                                        item =>
                                                                                                        item.PageTitle,
                                                                                                        item =>
                                                                                                        item.HtmlText,
                                                                                                        item =>
                                                                                                        item.
                                                                                                            _Computed_ParentArticleNames);

                if (luceneIds.Count() > 0)
                {
                    var q = createQuery();
                    var disjunction = Restrictions.Disjunction();
                    foreach (long itemId in luceneIds)
                    {
                        disjunction.Add(Restrictions.On<ArticleBase>(item => item.ID).IsLike(itemId));
                    }
                    q.Where(disjunction);
                    List<ArticleBase> res = q.List().ToList();
                    List<ArticleBase> finalRes = new List<ArticleBase>();
                    if (res.Count > 0)
                    {
                        Dictionary<long, ArticleBase> d = res.ToDictionary(x => x.ID);
                        finalRes = luceneIds.Select(i => d[i]).ToList();
                    }
                    results = new SearchPagedResults<IArticleBase>(finalRes, totalResults);
                }
            }
            return results;
        }

        protected virtual void addQueryConditions()
        {
            luceneIds = null;
            addKeywordsToSearchCriteria();
            addContentPageListToSearchCriteria();
            addLeafNodesToSearchCriteria();
            addExcludeItemsNotVisibleInFrontendToSearchCriteria();
            addFeaturedToSearchCriteria();
            addVisibleToFrontendCriteria();
            addLoadOnlySearchableArticlesToCriteria();
            addLastPublishedDatesToCriteria();
            addExcludeArticles();
            addOrderByToSearchCriteria();
            addTestSessionGuid();
        }

        private void addExcludeArticles()
        {
            if (SearchParams.ArticleIdsToExclude != null && SearchParams.ArticleIdsToExclude.Count > 0)
            {
                foreach (long id in SearchParams.ArticleIdsToExclude)
                {
                    _query = _query.Where(item => item.ID != id);
                }
            }
        }

        private void addLastPublishedDatesToCriteria()
        {
            if (SearchParams.LastPublishedFrom.HasValue || SearchParams.LastPublishedTo.HasValue)
            {
                _query = _query.Where(item => item.PublishedOn != null);
            }
            if (SearchParams.LastPublishedFrom.HasValue)
            {
                _query = _query.Where(item => item.PublishedOn >= SearchParams.LastPublishedFrom.Value);
            }
            if (SearchParams.LastPublishedTo.HasValue)
            {
                _query = _query.Where(item => item.PublishedOn <= SearchParams.LastPublishedTo.Value);
            }
        }

        private void addTestSessionGuid()
        {
            if (!string.IsNullOrWhiteSpace(SearchParams.TestSessionGuid))
            {
                _query.Where(x => x._TestItemSessionGuid == SearchParams.TestSessionGuid);
            }
        }

        private void addLoadOnlySearchableArticlesToCriteria()
        {
            if (!SearchParams.LoadAlsoNonSearchableItems)
            {
                _query.Where(item => item.DoNotShowInSearchResults == false && item.DoNotShowInSearchResults_Computed == false);
            }
        }

        private void addVisibleToFrontendCriteria()
        {
            if (!SearchParams.IncludeNotVisibleInFrontend)
            {
                _query = _query.Where(item => item.NotVisibleInFrontend == false);
            }
        }

        private void addOrderByToSearchCriteria()
        {
            if (SearchParams.SortBy != Enums.CONTENT_PAGE_SORT_BY.Relevance)
            {
                switch (SearchParams.SortBy)
                {
                    case Enums.CONTENT_PAGE_SORT_BY.MostRated:
                        _query = _query.OrderBy((item) => item.AvgRating).Desc;
                        break;
                    case Enums.CONTENT_PAGE_SORT_BY.MostPopular:
                        _query = _query.OrderBy((item) => item.ViewCount).Desc;
                        break;
                    case Enums.CONTENT_PAGE_SORT_BY.DateAscending:
                        _query = _query.OrderBy(item => item.CreatedOnDate).Asc;
                        break;
                    case Enums.CONTENT_PAGE_SORT_BY.DateDescending:
                        _query = _query.OrderBy(item => item.CreatedOnDate).Desc;
                        break;
                    case Enums.CONTENT_PAGE_SORT_BY.TitleAscending:
                        _query = _query.OrderBy(item => item.PageTitle).Asc;
                        break;
                    case Enums.CONTENT_PAGE_SORT_BY.TitleDescending:
                        _query = _query.OrderBy(item => item.PageTitle).Desc;
                        break;
                    default:
                        throw new InvalidSortByValueException("ContentPage SortBy value <" + SearchParams.SortBy +
                                                              "> not implemented!");
                }
            }
        }

        private void addFeaturedToSearchCriteria()
        {
            if (SearchParams.IsFeatured)
            {
                _query = _query.Where((item) => item.IsFeatured);
            }

        }


        private void addLeafNodesToSearchCriteria()
        {
            if (SearchParams.ShowOnlyLeafNodes)
            {
                _query = _query.Where(item => item._Computed_IsLeafNode);
            }
        }

        private void addContentPageListToSearchCriteria()
        {
            if (SearchParams.Parents != null && SearchParams.Parents.Count > 0)
            {
                loadArticleParentAlias();
                var queryDisjunction = Restrictions.Disjunction();
                foreach (var cp in SearchParams.Parents)
                {
                    IEnumerable<ArticleBase> cpTree = CS.General_v3.Util.TreeUtil.GetChildrenAndSubChildrenFromItem<ArticleBase>(cp as ArticleBase, item => item.GetChildArticles());
                    queryDisjunction.Add(Restrictions.On(() => aliasArticleParent.Parent).IsInG(cpTree.ToList()));
                    //query.RootCriteria.Add(Restrictions.On(() => aliasArticleParent.Parent).IsInG(cpTree.ToList()));
                }
                _query.RootCriteria.Add(queryDisjunction);
            }
        }
        /* 2012-04-03 Removed to make loading parents with OR instead of AND
        private void addContentPageListToSearchCriteria(IQueryOver<ArticleBase, ArticleBase> query)
        {
            if (SearchParameters.Parents != null)
            {
                loadArticleParentAlias();
                var queryDisjunction = Restrictions.Disjunction();
                foreach (var cp in SearchParameters.Parents)
                {
                    IEnumerable<ArticleBase> cpTree = CS.General_v3.Util.TreeUtil.GetChildrenAndSubChildrenFromItem<ArticleBase>(cp as ArticleBase, item => item.GetChildArticles());
                    query.RootCriteria.Add(Restrictions.On(() => aliasArticleParent.Parent).IsInG(cpTree.ToList()));
                }
            }
        }*/

        private Article_ParentChildLinkBase aliasArticleParent = null;
        private bool _loadArticleParentAlias = false;
        private void loadArticleParentAlias()
        {
            if (!_loadArticleParentAlias)
            {
                _loadArticleParentAlias = true;
                _query.JoinQueryOver(item => item.ParentArticleLinks, () => aliasArticleParent);

            }
        }

        #region Keywords functions
        private void addKeywordsToSearchCriteria()
        {
            if (SearchParams.SortBy != Enums.CONTENT_PAGE_SORT_BY.Relevance)
            {
                if (!string.IsNullOrWhiteSpace(SearchParams.Keywords))
                {
                    LanguageAnalyzer analyzer = new LanguageAnalyzer();
                    string strs = analyzer.AnalyseString(SearchParams.Keywords,
                                                         (CultureDetailsBase)
                                                         DefaultCultureManager.Instance.GetCulture());
                    Type classType = Article_CultureInfoBaseFactory.Instance.GetMainTypeCreatedByFactory();
                    int totalResults;
                    luceneIds = LuceneUtil.GetResultsFromMultilingual<Article_CultureInfoBase, ArticleBase>(analyzer,
                                                                                                            classType,
                                                                                                            strs, out totalResults, null,
                                                                                                            null, null, null,
                                                                                                            item =>
                                                                                                            item.
                                                                                                                PageTitle,
                                                                                                            item =>
                                                                                                            item.
                                                                                                                HtmlText, item => item._Computed_ParentArticleNames);
                    var disjunction = Restrictions.Disjunction();
                    foreach (long itemId in luceneIds)
                    {
                        disjunction.Add(Restrictions.On<ArticleBase>(item => item.ID).IsLike(itemId));
                    }
                    _query = _query.Where(disjunction);
                }
            }
        }

        private List<ArticleBase> getArticleParentsByKeyword(string keyword)
        {

            var root = ArticleBase.Factory.GetRootNode();
            var matchingContentPages = root.GetChildArticlesMatchingKeyword(keyword);


            IEnumerable<ArticleBase> matchingCategoriesIncSubCategories = CS.General_v3.Util.TreeUtil.GetChildrenAndSubChildrenFromItem<ArticleBase>(matchingContentPages, item => item.GetChildArticles());
            return matchingCategoriesIncSubCategories.ToList();
            //return matchingCategoriesIncSubCategories.ToList().ConvertAll((categoryItem) => categoryItem.Data);
        }

        private void parseKeywordsString()
        {
            if (!string.IsNullOrWhiteSpace(SearchParams.Keywords))
            {
                _keywordsList = getListOfStringsFromString(SearchParams.Keywords);
            }
        }

        private List<string> getListOfStringsFromString(string keywords)
        {
            List<string> stringList = new List<string>(keywords.Split(' '));
            return stringList;
        }
        #endregion


        protected IQueryOver<ArticleBase, ArticleBase> createQuery()
        {
            return ArticleBaseFactory.Instance.GetQuery();
        }
    }
}
