﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ClassifiedModule;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.CategoryModule;
using NHibernate.Criterion;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.LuceneClasses.Analyzers;
using NHibernate.Search;
using BusinessLogic_v3.Util;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers;
using Lucene.Net.Store;
using Lucene.Net.Index;
using Lucene.Net.Documents;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.Culture;
using BusinessLogic_v3.Classes.LuceneClasses;

namespace BusinessLogic_v3.Classes.Searches.ClassifiedSearcher
{
    public class ClassifiedSearcher : BaseSearcher<ClassifiedBase, ClassifiedSearcherParams>
    {
        public ClassifiedSearcher()
        {


        }

        protected override NHibernate.IQueryOver<ClassifiedBase, ClassifiedBase> createQuery()
        {
            return ClassifiedBase.Factory.GetQuery();
        }

        protected override void addQueryConditions()
        {
            addDateConditions();
            addKeywordConditions();
            addCategories();
            addSortBy();
            base.addQueryConditions();
        }

        private void addSortBy()
        {
            if (SearchParams.SortBy != Enums.CLASSIFIEDS_SORT_BY.Relevance)
            {
                switch (SearchParams.SortBy)
                {
                    case Enums.CLASSIFIEDS_SORT_BY.DateAsc:
                        _query = _query.OrderBy(item => item.PostedOn).Asc;
                        break;
                    case Enums.CLASSIFIEDS_SORT_BY.DateDesc:
                        _query = _query.OrderBy(item => item.PostedOn).Desc;
                        break;
                    case Enums.CLASSIFIEDS_SORT_BY.PriceAsc:
                        _query = _query.OrderBy(item => item.Price).Asc;
                        break;
                    case Enums.CLASSIFIEDS_SORT_BY.PriceDesc:
                        _query = _query.OrderBy(item => item.Price).Desc;
                        break;
                    case Enums.CLASSIFIEDS_SORT_BY.TitleAsc:
                        _query = _query.OrderBy(item => item.Title).Asc;
                        break;
                    case Enums.CLASSIFIEDS_SORT_BY.TitleDesc:
                        _query = _query.OrderBy(item => item.Title).Desc;
                        break;
                    default:
                        throw new InvalidOperationException("Sort by <" + SearchParams.SortBy + "> not implemented!");
                }
            }
        }

        private void addCategories()
        {
            if (SearchParams.Categories.IsNotNullOrEmpty())
            {
                HashedSet<ICategoryBase> categoryTree = new HashedSet<ICategoryBase>();
                foreach (var category in SearchParams.Categories)
                {
                    var flattenedList = CS.General_v3.Util.TreeUtil.
                        GetChildrenAndSubChildrenFromItem<ICategoryBase>(category,
                                                                         item => item.GetChildCategories()).Cast
                        <CategoryBase>().Where((item) => item.Published);
                    categoryTree.AddAll(flattenedList);
                }
                _query.WhereRestrictionOn(item => item.Category).IsInG(categoryTree);
            }
        }

        private void addKeywordConditions()
        {
            if (!string.IsNullOrWhiteSpace(SearchParams.Keywords))
            {
                LanguageAnalyzer analyzer = new LanguageAnalyzer();
                string strs = analyzer.AnalyseString(SearchParams.Keywords, (CultureDetailsBase) DefaultCultureManager.Instance.GetCulture());
                Type classType = ClassifiedBaseFactory.Instance.GetMainTypeCreatedByFactory();
                IEnumerable<long> results = LuceneUtil.GetResults<ClassifiedBase>(analyzer, classType, strs, null, item => item.Description, item => item.Title);
                var disjunction = Restrictions.Disjunction();
                foreach (long itemId in results)
                {
                    disjunction.Add(Restrictions.On<ClassifiedBase>(item => item.ID).IsLike(itemId));
                }
                _query.Where(disjunction);
            }
        }

        private void addDateConditions()
        {
            if (SearchParams.PostedOnFrom.HasValue)
            {
                _query = _query.Where(item => item.PostedOn >= SearchParams.PostedOnFrom.Value);
            }
            if (SearchParams.PostedOnTo.HasValue)
            {
                _query = _query.Where(item => item.PostedOn <= SearchParams.PostedOnTo.Value);
            }
        }
    }
}
