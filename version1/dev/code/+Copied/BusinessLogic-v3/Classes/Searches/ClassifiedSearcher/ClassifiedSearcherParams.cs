﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.CategoryModule;

namespace BusinessLogic_v3.Classes.Searches.ClassifiedSearcher
{
    public class ClassifiedSearcherParams : BaseSearcherParams
    {
        public DateTime? PostedOnFrom { get; set; }
        public DateTime? PostedOnTo { get; set; }
        public string Keywords { get; set; }
        public IEnumerable<ICategoryBase> Categories { get; set; }
        public new Enums.CLASSIFIEDS_SORT_BY SortBy { get; set; }
    }
}
