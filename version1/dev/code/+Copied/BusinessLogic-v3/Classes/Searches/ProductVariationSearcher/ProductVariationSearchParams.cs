﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Searches.ProductVariationSearcher
{
    public class ProductVariationSearchParams
    {
        public string Colour { get; set; }
        public string Size { get; set; }
    }
}
