﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.FileItemModule;
using nHibernateUtil = BusinessLogic_v3.Util.nHibernateUtil;


namespace BusinessLogic_v3.Classes.Searches.FileItemSearcher
{
    public class FileItemSearcher
    {
        
        public static FileItemSearcher CreateLastTypeInstance()
        {
            return CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<FileItemSearcher>();
        }

        protected NHibernate.IQueryOver<FileItemBase, FileItemBase> query;
        
        public FileItemSearchParams FileItemSearchParameters { get; set; }
        public int TotalResults { get; set; }

        public IEnumerable<IFileItemBase> GetSearchResults()
        {
            query = FileItemBase.Factory.GetQuery();
            orderFileItems(query);
            try
            {
                int totResults = 0;
                var list = nHibernateUtil.LimitQueryByPrimaryKeysAndReturnResultAndTotalCount<FileItemBase>(query, FileItemSearchParameters.PageNo,
                                                FileItemSearchParameters.ShowAmt, out totResults);
                this.TotalResults = totResults;
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void orderFileItems(NHibernate.IQueryOver<FileItemBase, FileItemBase> query)
        {
            switch (FileItemSearchParameters.SortBy)
            {
                case Enums.FILE_ITEM_SORT_BY.DateDesc:
                    query = query.OrderBy((item) => item.UploadedOn).Desc;
                    break;
                case Enums.FILE_ITEM_SORT_BY.DateAsc:
                    query = query.OrderBy((item) => item.UploadedOn).Asc;
                    break;
                case Enums.FILE_ITEM_SORT_BY.TitleAsc:
                    query = query.OrderBy((item) => item.Title).Asc;
                    break;
                case Enums.FILE_ITEM_SORT_BY.TitleDesc:
                    query = query.OrderBy((item) => item.Title).Desc;
                    break;
                case Enums.FILE_ITEM_SORT_BY.UploadedByAsc:
                    query = query.OrderBy((item) => item.UploadedBy).Asc;
                    break;
                case Enums.FILE_ITEM_SORT_BY.UploadedByDesc:
                    query = query.OrderBy((item) => item.UploadedBy).Desc;
                    break;    
            }

        }


    }
}
