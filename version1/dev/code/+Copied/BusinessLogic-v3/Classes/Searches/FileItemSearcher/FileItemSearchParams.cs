﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Searches.FileItemSearcher
{
    public class FileItemSearchParams
    {

        public static FileItemSearchParams CreateLastTypeInstance()
        {
            return CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<FileItemSearchParams>();
        }

        public int TotalResults { get; set; }
        public int PageNo { get; set; }
        public int ShowAmt { get; set; }
        public Enums.FILE_ITEM_SORT_BY SortBy { get; set; }
    }
}
