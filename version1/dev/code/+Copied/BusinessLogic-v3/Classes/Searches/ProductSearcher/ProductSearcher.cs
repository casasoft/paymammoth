﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductVariationModule;
using BusinessLogic_v3.Util;
using NHibernate;
using NHibernate.Criterion;
using CS.General_v3.Extensions;
using Iesi.Collections.Generic;
using CS.General_v3.Util;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Modules.Category_CultureInfoModule;
using BusinessLogic_v3.Modules.Product_CultureInfoModule;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;


namespace BusinessLogic_v3.Classes.Searches.ProductSearcher
{
    public class ProductSearcher
    {

        public static ProductSearcher CreateLastTypeInstance()
        {
            return CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<ProductSearcher>();
        }

        public ProductSearchParams ProductSearchParameters { get; set; }
        public int TotalResults { get; set; }

        private List<string> _keywordsList;

        private ProductSearcher()
        {
            this.ProductSearchParameters = ProductSearchParams.CreateLastTypeInstance();

            _keywordsList = new List<string>();
        }

        protected IQueryOver<ProductBase, ProductBase> query = null;



        private void addActivatedToSearchCriteria(IQueryOver<ProductBase, ProductBase> query)
        {

            query = query.Where((itemGroup) => itemGroup.Published == true);
        }

        private void addBrandToSearchCriteria(IQueryOver<ProductBase, ProductBase> query)
        {
            if (ProductSearchParameters.Brands.IsNotNullOrEmpty())
            {
                query = query.WhereRestrictionOn((itemGroup) => itemGroup.Brand).IsInG(ProductSearchParameters.Brands);
            }
        }

        private void addSpecialOffersToSearchCriteria(IQueryOver<ProductBase, ProductBase> query)
        {
            if (ProductSearchParameters.IsSpecialOffer.HasValue)
            {
                query =
                    query.Where((itemGroup) => itemGroup.IsSpecialOffer == ProductSearchParameters.IsSpecialOffer.Value);
            }
        }

        private void orderProducts(IQueryOver<ProductBase, ProductBase> query)
        {
            if (!ProductSearchParameters.RandomOrder)
            {
                query.RootCriteria.ClearOrders();
                switch (ProductSearchParameters.SortBy)
                {
                    case Enums.ITEM_GROUP_SORT_BY.FeaturedAscending:
                        {
                            query = query.OrderBy((item) => item.IsFeatured).Asc;
                            break;
                        }
                    // case Enums.ITEM_GROUP_SORT_BY.FeaturedDescending:
                    //     query = query.OrderBy((item) => item.IsFeatured).Desc;
                    //     break;
                    case Enums.ITEM_GROUP_SORT_BY.PriceAscending:
                        {
                            query = query.OrderBy((item) => item.PriceRetailIncTaxPerUnit).Asc;
                            break;
                        }
                    case Enums.ITEM_GROUP_SORT_BY.PriceDescending:
                        {
                            query = query.OrderBy((item) => item.PriceRetailIncTaxPerUnit).Desc;
                            break;
                            //  case Enums.ITEM_GROUP_SORT_BY.Priority:
                            //      query = query.OrderBy((item) => item.Priority).Asc;
                            //     break;
                        }
                    case Enums.ITEM_GROUP_SORT_BY.TitleAscending:
                        {
                            createProductCultureInfoAlias();
                            //query = query.OrderBy((item) => item.Title).Asc;
                            query = query.OrderBy(Projections.Property(() => productCultureInfoAlias._ActualTitle)).Asc;
                            //var tmp = query.OrderByAlias(() => productCultureInfoAlias._ActualTitle).Asc;
                            //if (queryOverWithProductCulture == null)
                            //{
                            //    queryOverWithProductCulture =
                            //        query.JoinQueryOver<Product_CultureInfoBase>(item => item.Product_CultureInfos,
                            //                                                     () => aliasCultureInfoProduct);
                            //}
                            //queryOverWithProductCulture = queryOverWithProductCulture.OrderBy(item => item._ActualTitle).Asc;
                            break;
                        }
                    case Enums.ITEM_GROUP_SORT_BY.TitleDescending:
                        {
                            createProductCultureInfoAlias();
                            query = query.OrderBy(Projections.Property(() => productCultureInfoAlias._ActualTitle)).Desc;
                            //var tmp = query.OrderByAlias(() => productCultureInfoAlias._ActualTitle).Desc;
                            //if (queryOverWithProductCulture == null)
                            //{
                            //    queryOverWithProductCulture =
                            //        query.JoinQueryOver<Product_CultureInfoBase>(item => item.Product_CultureInfos,
                            //                                                     () => aliasCultureInfoProduct);
                            //}
                            //queryOverWithProductCulture =
                            //    queryOverWithProductCulture.OrderBy(item => item._ActualTitle).Desc;

                            //query = query.OrderBy((item) => item.Title).Desc;
                            break;
                        }
                    default:
                        throw new InvalidOperationException("Enumeration item is not used in switch statement");
                }

                //var tmp2 = query.ThenBy(x => x.Priority).Asc;

            }
        }

        private void addCategoryFeatureListToSearchCriteria(IQueryOver<ProductBase, ProductBase> query)
        {



            StringBuilder sb = new StringBuilder();
            if (this.ProductSearchParameters.CategoryFeatures != null)
            {
                foreach (var f in this.ProductSearchParameters.CategoryFeatures)
                {
                    if (f != null)
                    {
                        string featureKeyword = "[" + f.ID + "]";
                        query =
                            query.Where(
                                g => g.CategoryFeatureValues_ForSearch.IsLike(featureKeyword, MatchMode.Anywhere));
                    }
                }
            }

        }

        private void checkPriceCriteria()
        {
            var p = this.ProductSearchParameters;
            if (p.PriceIncTaxPerUnitFrom.HasValue || p.PriceIncTaxPerUnitTo.HasValue)
            {
                if (p.PriceIncTaxPerUnitFrom.HasValue)
                    query = query.Where(g => g._ActualPrice >= p.PriceIncTaxPerUnitFrom.Value);
                if (p.PriceIncTaxPerUnitTo.HasValue)
                    query = query.Where(g => g._ActualPrice <= p.PriceIncTaxPerUnitTo.Value);
            }
        }

        private void addColourListAndSizeListToSearchCriteria(IQueryOver<ProductBase, ProductBase> query)
        {


            if ((ProductSearchParameters.ColourList != null && ProductSearchParameters.ColourList.Count() > 0) ||
                (ProductSearchParameters.SizeList != null && ProductSearchParameters.SizeList.Count() > 0))
            {

                loadItemAlias();
                //IQueryOver<ProductBase, ProductVariationBase> joinQuery = query.Inner.JoinQueryOver<ProductVariationBase>((item) => item.Items);
                if (ProductSearchParameters.ColourList != null && ProductSearchParameters.ColourList.Count() > 0)
                {
                    query.RootCriteria.Add(
                        Restrictions.On(() => aliasProdVariation.Colour).IsInG<string>(
                            ProductSearchParameters.ColourList.ToList()));
                }
                if (ProductSearchParameters.SizeList != null && ProductSearchParameters.SizeList.Count() > 0)
                {
                    query.RootCriteria.Add(
                        Restrictions.On(() => aliasProdVariation.Size).IsInG<string>(
                            ProductSearchParameters.SizeList.ToList()));
                }
            }
        }

        private void addCategoryListToSearchCriteria(IQueryOver<ProductBase, ProductBase> query)
        {
            if (ProductSearchParameters.Categories.IsNotNullOrEmpty())
            {
                loadProductCategoryAlias();
                foreach (var category in ProductSearchParameters.Categories)
                {

                    IEnumerable<ICategoryBase> categoryTree = CS.General_v3.Util.TreeUtil.
                        GetChildrenAndSubChildrenFromItem<ICategoryBase>(category,
                                                                         item => item.GetChildCategories()).Cast
                        <CategoryBase>().Where((item) => item.Published);
                    query.RootCriteria.Add(
                        Restrictions.On(() => aliasProductCategory.Category).IsInG(categoryTree.ToList()));
                }
            }
        }

        private ProductCategoryBase aliasProductCategory = null;

        private bool _loadProductCategoryAlias = false;

        private void loadProductCategoryAlias()
        {
            if (!_loadProductCategoryAlias)
            {
                _loadProductCategoryAlias = true;
                query.Left.JoinQueryOver(item => item.CategoryLinks, () => aliasProductCategory) ;

            }
        }

        private ProductVariationBase aliasProdVariation = null;
        private bool _loadItemAlias = false;

        private void loadItemAlias()
        {
            if (!_loadItemAlias)
            {
                _loadItemAlias = true;

                query.JoinQueryOver(item => item.ProductVariations, () => aliasProdVariation);
                loadOnlyActivatedItems();


            }
        }

        private void loadOnlyActivatedItems()
        {
            query.Where(() => aliasProdVariation.Published == true);
        }

        //private IQueryOver<ProductBase, Product_CultureInfoBase> queryOverWithProductCulture;
        //private Product_CultureInfoBase aliasCultureInfoProduct = null;
        private Product_CultureInfoBase productCultureInfoAlias { get; set; }
        private bool productCultureInfoAlias_Created { get; set; }

        private void createProductCultureInfoAlias()
        {
            if (!productCultureInfoAlias_Created)
            {
                query.JoinQueryOver<Product_CultureInfoBase>(item => item.Product_CultureInfos, () => productCultureInfoAlias);
                CultureDetailsBase applicableCulture = null;
                if (ProductSearchParameters.ApplicableCulture != null)
                {
                    applicableCulture = (CultureDetailsBase)ProductSearchParameters.ApplicableCulture;
                }
                else
                {
                    applicableCulture = (CultureDetailsBase)BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture();
                }
                query.Where(() => productCultureInfoAlias.CultureInfo.ID == applicableCulture.ID);
                productCultureInfoAlias_Created = true;
            }
        }

        private void addKeywordsToSearchCriteria(IQueryOver<ProductBase, ProductBase> query)
        {
            parseKeywordsString();
            if (_keywordsList.Count > 0)
            {
                loadProductCategoryAlias();
                createProductCultureInfoAlias();

                foreach (var keyword in _keywordsList)
                {
                    var queryDisjunction = Restrictions.Disjunction();

                    
                    //queryDisjunction.Add(NHibernate.Criterion.Restrictions.On<Product_CultureInfoBase>(item => item._ActualTitle)
                    //    .IsLike(keyword, MatchMode.Anywhere));

                    //todo: To Test [2012/05/28]
                    queryDisjunction.Add(NHibernate.Criterion.Restrictions.On(() => productCultureInfoAlias._ActualTitle)
                        .IsLike(keyword, MatchMode.Anywhere));
                    queryDisjunction.Add(NHibernate.Criterion.Restrictions.On(() => productCultureInfoAlias.Material)
                        .IsLike(keyword, MatchMode.Anywhere));

                    queryDisjunction.Add(NHibernate.Criterion.Restrictions.On<ProductBase>(item => item.ReferenceCode)
                        .IsLike(keyword, MatchMode.Anywhere));

                    var brandCriteria = query.RootCriteria.GetAssociationLinkCriteriaByAlias<ProductBase>(group => group.Brand);

                    queryDisjunction.Add(Restrictions.Like(
                            nHibernateUtil.GetProjectionPathForAssociation<ProductBase, BrandBase>(item => item.Brand,
                                                                                                   brand => brand.Title),
                            keyword, MatchMode.Anywhere));

                    var matchingCategories = getCategoriesByKeyword(keyword);
                    if (matchingCategories.Count() > 0)
                    {
                        queryDisjunction.Add(Restrictions.On(() => aliasProductCategory.Category).IsInG(matchingCategories.ToList()));
                    }
                    query.Where(queryDisjunction);
                }
            }
        }

        private IEnumerable<ICategoryBase> getCategoriesByKeyword(string keyword)
        {
            var q = CategoryBase.Factory.GetQuery(new DbObjects.Parameters.GetQueryParams(orderByPriority: false));
            var qo = q.JoinQueryOver<Category_CultureInfoBase>(item => item.Category_CultureInfos);

            var disjunction = Restrictions.Disjunction();
            disjunction.Add(Restrictions.On<Category_CultureInfoBase>(c => c.TitlePlural).IsLike(keyword, MatchMode.Anywhere));
            disjunction.Add(Restrictions.On<Category_CultureInfoBase>(c => c.TitleSingular).IsLike(keyword, MatchMode.Anywhere));
            qo.Where(disjunction);
            qo.Where(item => item.Published);
            q.Where((item) => item.Published);
            var results = CategoryBase.Factory.FindAll(q);
            HashedSet<ICategoryBase> list = new HashedSet<ICategoryBase>();
            foreach (var c in results)
            {
                list.AddAll(c.GetAllCategoriesUnderThis(includeThis: true));
            }
            return list;
        }

        private void parseKeywordsString()
        {
            if (!string.IsNullOrWhiteSpace(ProductSearchParameters.Keywords))
            {
                _keywordsList = getListOfStringsFromString(ProductSearchParameters.Keywords);
            }
        }

        private List<string> getListOfStringsFromString(string keywords)
        {
            List<string> stringList = new List<string>(keywords.Split(' '));
            return stringList;
        }

        private void addQuantityCondition()
        {
            if (BusinessLogic_v3.Modules.ModuleSettings.Shop.UsesQuantities)
            {
                /* 2011/09/28 This was switched off, as we said that the system should still show items out of stock, to show to ppl that they once
                 * had this item, and they can contact the shop about it to re-stock */
                //loadItemAlias();
                //query = query.Where(() => aliasItem.Quantity > 0);
            }
        }

        private void addRestrictions()
        {
            addSpecialOffersToSearchCriteria(query);
            addBrandToSearchCriteria(query);
            addKeywordsToSearchCriteria(query);
            addCategoryListToSearchCriteria(query);
            addExclusions(query);

            addCategoryFeatureListToSearchCriteria(query);
            addColourListAndSizeListToSearchCriteria(query);
            addActivatedToSearchCriteria(query);
            addQuantityCondition();
            checkPriceCriteria();
            orderProducts(query);
            randomOrder(query);
            ProductBaseFactory.Instance.RestrictCriteriaForFrontend(query.RootCriteria);
        }

        public SearchPagedResults<IProductBase> GetSearchResults()
        {
            query = ProductBase.Factory.GetQuery(new DbObjects.Parameters.GetQueryParams(orderByPriority:false));

            addRestrictions();
            
            try
            {

                int totResults = query.RowCount();

                //query.Skip(((ProductSearchParameters.PageNo - 1) * ProductSearchParameters.ShowAmount));
                //query.Take(ProductSearchParameters.ShowAmount);
                //var results = ProductBaseFactory.Instance.FindAll(query);

                var list = nHibernateUtil.LimitQueryByPrimaryKeysAndReturnResultAndTotalCount<ProductBase>(query, ProductSearchParameters.PageNo,
                                                ProductSearchParameters.ShowAmount, out totResults);

                this.TotalResults = totResults;
                return new SearchPagedResults<IProductBase>(list, TotalResults);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void addExclusions(IQueryOver<ProductBase, ProductBase> query)
        {
            if (ProductSearchParameters.ExcludeProducts != null)
            {
                foreach (ProductBase product in ProductSearchParameters.ExcludeProducts)
                {
                    query = query.Where(item => item.ID != product.ID);
                }
            }
        }

        private void randomOrder(IQueryOver<ProductBase, ProductBase> query)
        {
            if (ProductSearchParameters.RandomOrder)
            {
                query.RootCriteria.AddOrder(BusinessLogic_v3.Util.nHibernateUtil.GetRandomOrder());
            }
        }
    }
}
