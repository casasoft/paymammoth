﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryFeatureModule;
using BusinessLogic_v3.Modules.CategoryModule;
using Iesi.Collections;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.CultureDetailsModule;


namespace BusinessLogic_v3.Classes.Searches.ProductSearcher
{
    public class ProductSearchParams : BaseSearcherParams
    {
        public static ProductSearchParams CreateLastTypeInstance()
        {
            return CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<ProductSearchParams>();
        }

        private bool? _isSpecialOffer = null;
        public bool? IsSpecialOffer {
            get { return _isSpecialOffer; }
            set {
                _isSpecialOffer = value; 
            }
        }
        public int TotalResults { get; set; }
        public HashedSet<ICategoryBase> Categories { get; set; }
        public HashedSet<ICategoryFeatureBase> CategoryFeatures { get; set; }
        public string Keywords { get; set; }
        public new BusinessLogic_v3.Enums.ITEM_GROUP_SORT_BY SortBy { get; set; }
        public HashedSet<string> ColourList { get; set; }
        public HashedSet<string> SizeList { get; set; }
        public HashedSet<IBrandBase> Brands { get; set; }
        public double? PriceIncTaxPerUnitFrom { get; set; }
        public double? PriceIncTaxPerUnitTo { get; set; }
        public bool RandomOrder { get; set; }
        public ICultureDetailsBase ApplicableCulture { get; set; }

        public HashedSet<IProductBase> ExcludeProducts { get; set; }

        private ProductSearchParams()
        {
            Categories = new HashedSet<ICategoryBase>();
            CategoryFeatures = new HashedSet<ICategoryFeatureBase>();
            ColourList = new HashedSet<string>();
            SizeList = new HashedSet<string>();
            this.Brands = new HashedSet<IBrandBase>();
        }
    }
}
