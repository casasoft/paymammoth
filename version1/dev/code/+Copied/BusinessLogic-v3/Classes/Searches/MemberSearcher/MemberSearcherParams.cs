﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Searches.MemberSearcher
{
    public class MemberSearcherParams : BaseSearcherParams
    {
        public string Keywords { get; set; }
        public string MemberType { get; set; }
        public new Enums.MEMBER_SORT_BY SortBy { get; set; }
    }
}
