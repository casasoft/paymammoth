﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Classes.LuceneClasses.Analyzers;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.Culture;
using BusinessLogic_v3.Classes.LuceneClasses;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Classes.Searches.MemberSearcher
{
    public class MemberSearcher : BaseSearcher<MemberBase, MemberSearcherParams>
    {
        protected override NHibernate.IQueryOver<MemberBase, MemberBase> createQuery()
        {
            return MemberBaseFactory.Instance.GetQuery();
        }

        protected override void addQueryConditions()
        {
            addKeywordsToQuery();
            addMemberTypeToQuery();
            addSortByToQuery();
            base.addQueryConditions();
        }

        private void addSortByToQuery()
        {
            if(SearchParams.SortBy != Enums.MEMBER_SORT_BY.Relevance)
            {
                throw new InvalidOperationException("Sort by <" + SearchParams.SortBy + "> not implemented!");
            }
        }

        private void addMemberTypeToQuery()
        {
            if(!string.IsNullOrWhiteSpace(SearchParams.MemberType))
            {
                _query = _query.Where(item => item.MemberType == SearchParams.MemberType);
            }
        }

        private void addKeywordsToQuery()
        {
            if(!string.IsNullOrWhiteSpace(SearchParams.Keywords))
            {
                LanguageAnalyzer analyzer = new LanguageAnalyzer();
                string strs = analyzer.AnalyseString(SearchParams.Keywords, (CultureDetailsBase)DefaultCultureManager.Instance.GetCulture());
                Type classType = MemberBaseFactory.Instance.GetMainTypeCreatedByFactory();
                IEnumerable<long> results = LuceneUtil.GetResults<MemberBase>(analyzer, classType, strs, null,
                                                                              item => item.FirstName,
                                                                              item => item.LastName,
                                                                              item => item.MiddleName,
                                                                              item => item.CompanyName,
                                                                              item => item.CompanyProfile);
                var disjunction = Restrictions.Disjunction();
                foreach (long itemId in results)
                {
                    disjunction.Add(Restrictions.On<MemberBase>(item => item.ID).IsLike(itemId));
                }
                _query = _query.Where(disjunction);
            }
        }
    }
}
