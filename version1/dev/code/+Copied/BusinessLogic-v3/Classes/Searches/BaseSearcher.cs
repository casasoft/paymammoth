﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Util;
using NHibernate;
using BusinessLogic_v3.Classes.DbObjects;
using CS.General_v3.Classes.HelperClasses;
using NHibernate.Criterion;
using CS.General_v3.Util;

namespace BusinessLogic_v3.Classes.Searches
{
    public abstract class BaseSearcher<TItemType, TParamsType> 
        where TParamsType : BaseSearcherParams, new()
        where TItemType : BaseDbObject
    {

        private IQueryOver<TItemType,TItemType> _m_query = null;
        public IQueryOver<TItemType,TItemType> _query
        {
            get
            {
                if (_m_query == null)
                {
                    _m_query = createQuery();
                }
                return _m_query;

            }
            set { _m_query = value; }

        }

        protected abstract IQueryOver<TItemType, TItemType> createQuery();

        public TParamsType SearchParams { get; set; }

        public BaseSearcher()
        {
            this.SearchParams = new TParamsType();
        }

        protected virtual void addQueryConditions()
        {
            addTestSessionGuid();
        }

        public SearchPagedResults<TItemType> GetSearchResults()
        {
            addQueryConditions();
            int totalResults = 0;
            IEnumerable<TItemType> resultsList = nHibernateUtil.LimitCriteriaByPrimaryKeysAndReturnResultAndTotalCount<TItemType>(
                _query.RootCriteria,

                                                                                                         SearchParams.PageNo,
                                                                                                         SearchParams.ShowAmount,
                                                                                                         out
                                                                                                             totalResults);
            return new SearchPagedResults<TItemType>(resultsList, totalResults);
        }

        private void addTestSessionGuid()
        {
            if (!string.IsNullOrWhiteSpace(SearchParams.TestSessionGuid))
            {
                _query.Where(x => x._TestItemSessionGuid == SearchParams.TestSessionGuid);
                //_query.RootCriteria.Add(Expression.Where<TItemType>(x => x._TestItemSessionGuid == SearchParams.TestSessionGuid));
            }
        }
    }
}
