﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Modules.MemberModule;

namespace BusinessLogic_v3.Classes.Searches.TicketSearcher
{
    public class TicketSearcherParams : BaseSearcherParams
    {
        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }
        public Enums.TICKET_SORTBY? SortBy
        {
            get { return (Enums.TICKET_SORTBY?)base.SortBy; }
            set { base.SortBy = value; }
        }

        public Enums.TICKET_STATE? State { get; set; }
        public ICmsUserBase TicketingSystemSupportUser { get; set; }
        public IMemberBase Member { get; set; }
    }
}
