﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.TicketModule;
using CS.General_v3.Util;

namespace BusinessLogic_v3.Classes.Searches.TicketSearcher
{
    public class TicketSearcher : BaseSearcher<TicketBase, TicketSearcherParams>
    {
        protected override void addQueryConditions()
        {
            addCreatedDateToFilter();
            addCreatedDateFromFilter();
            addTicketStateFilter();
            addTicketingSystemSupportUserFilter();
            addMemberFilter();
            addSortByFilter();
            base.addQueryConditions();
        }

        private void addSortByFilter()
        {
            if(SearchParams.SortBy.HasValue)
            {
                switch(SearchParams.SortBy.Value)
                {
                    case Enums.TICKET_SORTBY.CreatedDateAsc:
                        _query = _query.OrderBy(item => item.CreatedOn).Asc;
                        break;
                    case Enums.TICKET_SORTBY.CreatedDateDesc:
                        _query = _query.OrderBy(item => item.CreatedOn).Desc;
                        break;
                    case Enums.TICKET_SORTBY.PriorityAsc:
                        _query = _query.OrderBy(item => item.TicketPriority).Asc;
                        break;
                    case Enums.TICKET_SORTBY.PriorityDesc:
                        _query = _query.OrderBy(item => item.TicketPriority).Desc;
                        break;
                    case Enums.TICKET_SORTBY.StatusAsc:
                        _query = _query.OrderBy(item => item.TicketState).Asc;
                        break;
                    case Enums.TICKET_SORTBY.StatusDesc:
                        _query = _query.OrderBy(item => item.TicketState).Desc;
                        break;
                    case Enums.TICKET_SORTBY.TitleAsc:
                        _query = _query.OrderBy(item => item.Title).Asc;
                        break;
                    case Enums.TICKET_SORTBY.TitleDesc:
                        _query = _query.OrderBy(item => item.Title).Desc;
                        break;
                    default:
                        throw new InvalidOperationException("Sort by value <" + SearchParams.SortBy.Value +
                                                            "> not yet implemented!");
                }
            }
        }

        private void addMemberFilter()
        {
            if(SearchParams.Member!=null)
            {
                _query = _query.Where(item => item.Member.ID == SearchParams.Member.ID);
            }
        }

        private void addTicketingSystemSupportUserFilter()
        {
            if(SearchParams.TicketingSystemSupportUser!=null)
            {
                _query = _query.Where(item => item.AssignedTo.ID == SearchParams.TicketingSystemSupportUser.ID);
            }
        }

        private void addTicketStateFilter()
        {
            if(SearchParams.State.HasValue)
            {
                _query = _query.Where(item => item.TicketState == SearchParams.State.Value);
            }
        }

        private void addCreatedDateFromFilter()
        {
            if (SearchParams.CreatedDateFrom.HasValue)
            {
                _query = _query.Where(item => item.CreatedOn >= SearchParams.CreatedDateFrom.Value.GetStartOfDay());
            }
        }

        private void addCreatedDateToFilter()
        {
            if (SearchParams.CreatedDateTo.HasValue)
            {
                _query = _query.Where(item => item.CreatedOn <= SearchParams.CreatedDateTo.Value.GetEndOfDay());
            }
        }

        protected override NHibernate.IQueryOver<TicketBase, TicketBase> createQuery()
        {
            return TicketBase.Factory.GetQuery();
        }
    }
}
