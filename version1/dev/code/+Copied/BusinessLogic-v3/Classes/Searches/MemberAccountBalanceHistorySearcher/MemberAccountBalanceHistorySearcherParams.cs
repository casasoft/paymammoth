﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.MemberModule;

namespace BusinessLogic_v3.Classes.Searches.MemberAccountBalanceHistorySearcher
{
    public class MemberAccountBalanceHistorySearcherParams : BaseSearcherParams
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_SORT_BY? SortBy
        {
            get { return (Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_SORT_BY?)base.SortBy; }
            set { base.SortBy = value; }
        }
        public IMemberBase Member { get; set; }
    }
}
