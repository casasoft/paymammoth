﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;
using CS.General_v3.Util;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules.OrderItemModule;
using NHibernate;

namespace BusinessLogic_v3.Classes.Searches.MemberAccountBalanceHistorySearcher
{
    public class MemberAccountBalanceHistorySearcher : BaseSearcher<MemberAccountBalanceHistoryBase, MemberAccountBalanceHistorySearcherParams>
    {
        public MemberAccountBalanceHistorySearcher()
        {

            
        }
        
        protected override void addQueryConditions()
        {
            addMemberToQuery();
            addDateFromFilter();
            addDateToFilter();
            addSortByToSearchQuery();
            base.addQueryConditions();
        }
        private void addMemberToQuery()
        {
            if (SearchParams.Member != null)
            {
                _query = _query.Where(item => item.Member.ID == SearchParams.Member.ID);
            }
        }

        private void addDateFromFilter()
        {
            if (SearchParams.DateFrom.HasValue)
            {
                _query = _query.Where(item => item.Date >= SearchParams.DateFrom.Value.GetStartOfDay());
            }
        }

        private void addDateToFilter()
        {
            if (SearchParams.DateTo.HasValue)
            {
                _query = _query.Where(item => item.Date <= SearchParams.DateTo.Value.GetEndOfDay());
            }
        }

        private void addSortByToSearchQuery()
        {
            if (SearchParams.SortBy.HasValue)
            {

                switch (SearchParams.SortBy.Value)
                {
                    case Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_SORT_BY.DateAsc:
                        _query = _query.OrderBy(item => item.Date).Asc;
                        break;
                    case Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_SORT_BY.DateDesc:
                        _query = _query.OrderBy(item => item.Date).Desc;
                        break;
                    default:
                        SearchParams.SortBy = Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_SORT_BY.DateDesc;
                        addSortByToSearchQuery();
                        break;
                }
                _query = _query.OrderBy(item => item.ID).Desc;
            }
        }

        protected override NHibernate.IQueryOver<MemberAccountBalanceHistoryBase, MemberAccountBalanceHistoryBase> createQuery()
        {
            return MemberAccountBalanceHistoryBase.Factory.GetQuery();
            
        }
    }
}
