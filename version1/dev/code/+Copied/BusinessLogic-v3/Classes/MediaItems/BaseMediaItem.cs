﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.MediaItems;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;


namespace BusinessLogic_v3.Classes.MediaItems
{
    public abstract class BaseMediaItem : IMediaItemDB
    {
        public BusinessLogic_v3.Classes.DB.IBaseDbObject Item { get; private set; }

        public BaseMediaItem(BusinessLogic_v3.Classes.DB.IBaseDbObject item)
        {
            this.Item = item;
        }
        #region IMediaItemDB Members

        public abstract string Identifier {get;set;}

        public abstract string GenerateFilename(string uploadedFilenameOnly, string uploadedFilenameExtension);

        public virtual OperationResult SetAsMainItem()
        {
            // update this if this image can be set as main item
            return null;
        }

        public virtual void OnDeletedItem()
        {

        }

        #endregion

        #region IMediaItemInfo Members


        public virtual int Priority
        {
            get
            {
                return Item.Priority;
                // update this if this image can have priority
                    

            }
            set
            {
                Item.Priority = value;
            }
        }

        public virtual string Caption
        {
            get
            {
                // update this if image has captions
                return "";
            }
            set
            {

            }
        }


        public virtual OperationResult Delete()
        {
            return null;

        }

        public virtual OperationResult Save()
        {
            return this.Item.Save();

        }

        public string ExtraValueChoice
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        #endregion


    }
}
