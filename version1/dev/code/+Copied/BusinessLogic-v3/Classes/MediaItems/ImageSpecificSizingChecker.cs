﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.MediaItems
{
    public class ImageSpecificSizingChecker
    {
        public static ImageSpecificSizingChecker Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<ImageSpecificSizingChecker>(); }
        }
        private ImageSpecificSizingChecker()
        {

        }

        private int minutesBetweenChecks = 60;

        

        private bool checkIfImageSizeNotCheckedRecently<TEnum>(BaseMediaItemImage<TEnum> imageInfo)
            where TEnum: struct
        {
            bool result = false;
            string key = getLastCheckedCacheKey(imageInfo);
            DateTime? lastDate = (CS.General_v3.Util.PageUtil.GetCachedObject<DateTime?>(key));
            if (lastDate == null || (CS.General_v3.Util.Date.DateIsBeforeNowBy(lastDate.Value,0,minutesBetweenChecks,0)))
            {
                result = true;
               
                
            }
            return result;
        }
        private string getLastCheckedCacheKey(BaseMediaItemFile imageInfo)
        {
            string key = "BL.ImageSpecificSizingChecker_" + imageInfo.GetType().FullName + "." + imageInfo.MediaItemInfo.DbItem.ID;
            return key;
        }

        private void updateLastCheckedOnInCache(BaseMediaItemFile imageInfo)
        {
            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
            {
                string key = getLastCheckedCacheKey(imageInfo);
                CS.General_v3.Util.PageUtil.SetCachedObject(key, CS.General_v3.Util.Date.Now, minutesBetweenChecks, throwErrorIfNoContext: false);
            }
        }

        /// <summary>
        /// Checks the image sizes, and creates any ones that do not exist (or are to be overwritten)
        /// </summary>
        /// <param name="overwriteExisting">Overwrite existing files</param>
        public void CheckImageSizes<TEnum>(BaseMediaItemImage<TEnum> imageInfo, bool overwriteExisting)
            where TEnum: struct
        {
            

            if (overwriteExisting || checkIfImageSizeNotCheckedRecently(imageInfo))
            {
                string localPath = imageInfo.GetLocalPath();
                if (System.IO.File.Exists(localPath))
                {
                    foreach (var size in imageInfo.MediaItemInfo.SizesInfo)
                    {
                        checkSpecificImageFiles(imageInfo, size, localPath, overwriteExisting);

                    }
                }
                updateLastCheckedOnInCache(imageInfo);
            }
        }

        private void checkSpecificImageFiles<TEnum>(BaseMediaItemImage<TEnum> imageInfo, IMediaItemImageSpecificSizeInfo size, string originalImagelocalPath, bool overwriteExisting)
            where TEnum: struct
        {


            
            string newLocalPath = imageInfo.getLocalPathForSpecificSize(size.Title);

            

            if (overwriteExisting || !System.IO.File.Exists(newLocalPath))
            {
                var imageFormat = CS.General_v3.Enums.FilenameToImageFormat(originalImagelocalPath);

                if (imageInfo.checkIfExtensionCanBeResized(imageFormat))
                {
                    if (size.CropIdentifier != CS.General_v3.Enums.CROP_IDENTIFIER.None)
                    {
                        CS.General_v3.Util.Image.ResizeAndCropImage(originalImagelocalPath, size.Width, size.Height, newLocalPath, size.CropIdentifier);
                    }
                    else
                    {
                        CS.General_v3.Util.Image.ResizeImage(originalImagelocalPath, size.Width, size.Height, newLocalPath);
                    }
                }
                else
                {
                    CS.General_v3.Util.IO.CopyFile(originalImagelocalPath, newLocalPath);
                }
            }
            
        }


    }
}
