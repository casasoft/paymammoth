﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;

namespace BusinessLogic_v3.Classes.MediaItems
{
    public interface IMediaItemImageInfo :IMediaItemInfo
    {
        ICollection<IMediaItemImageSpecificSizeInfo> SizesInfo { get;  }
        int MaxWidth { get; }
        int MaxHeight { get; }
    }
}
