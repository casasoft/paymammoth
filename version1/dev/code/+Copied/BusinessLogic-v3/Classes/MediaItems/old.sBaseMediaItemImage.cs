﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.MediaItems;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;


namespace BusinessLogic_v3.Classes.MediaItems
{
    public abstract class BaseMediaItemImage : BaseMediaItem, IMediaItemImageDB
    {

        public BaseMediaItemImage(BusinessLogic_v3.Classes.DB.IBaseDbObject item)
            : base(item)
        {
            
        }

        #region IMediaItemImageDB Members

        public CS.General_v3.Classes.MediaItems.ImageSizes.IImageSizing ImageSizing
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
