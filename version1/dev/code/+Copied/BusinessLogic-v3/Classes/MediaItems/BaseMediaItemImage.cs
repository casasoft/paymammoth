﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BusinessLogic_v3.Classes.MediaItems
{
    public class BaseMediaItemImage<TEnum> : BaseMediaItemFile, IMediaItemImage<TEnum> where TEnum : struct
    {
        public new IMediaItemImageInfo MediaItemInfo
        {
            get { return (IMediaItemImageInfo) base.MediaItemInfo; }
            set { base.MediaItemInfo = value; }


        }

        public CS.General_v3.Enums.IMAGE_FORMAT GetOriginalImageFormat()
        {
            
            return CS.General_v3.Enums.FilenameToImageFormat(this.MediaItemInfo.Filename).GetValueOrDefault( CS.General_v3.Enums.IMAGE_FORMAT.JPEG);
        }


        internal bool checkIfExtensionCanBeResized(CS.General_v3.Enums.IMAGE_FORMAT? format)
        {
            if (format.HasValue)
            {
                return (format.Value != CS.General_v3.Enums.IMAGE_FORMAT.GIF);
            }
            else
            {
                //No format specified
                return false;
            }
        }

        private bool checkIfFileIsImageAndCanBeResized()
        {
            return CS.General_v3.Util.Image.CheckImageFileExtension(this.MediaItemInfo.Filename) && checkIfExtensionCanBeResized(this.GetOriginalImageFormat());
        }

        private void resizeOriginalImage()
        {

            string localPath = GetLocalPath();
            if (checkIfFileIsImageAndCanBeResized())
            {
                CS.General_v3.Util.Image.ResizeImage(localPath, this.MediaItemInfo.MaxWidth, this.MediaItemInfo.MaxHeight, localPath);
            }

        }

        protected IMediaItemImageSpecificSizeInfo getSpecificSizeInfoByEnum(Enum size)
        {

            string sSize = CS.General_v3.Util.EnumUtils.StringValueOf(size);
            return getSpecificSizeInfoByTitle(sSize);
            
        }
        protected IMediaItemImageSpecificSizeInfo getSpecificSizeInfoByTitle(string title)
        {
            
            return this.MediaItemInfo.SizesInfo.Where(s => string.Compare(s.Title, title, true) == 0).FirstOrDefault();
        }
        protected string getFilenameForSpecificSize(IMediaItemImageSpecificSizeInfo size)
        {
            string fileNameExtension = null;
            if (size.ImageFormat.GetValueOrDefault(  CS.General_v3.Enums.IMAGE_FORMAT.None) != CS.General_v3.Enums.IMAGE_FORMAT.None)
            {
                fileNameExtension = CS.General_v3.Enums.ImageFormatToExtension(size.ImageFormat.Value);
            }
            else
            {
                fileNameExtension = this.GetFilenameExtension();
            }

            //string s = this.GetFilenameOnlyWithoutExtension() + "-" + size.Title + "." + fileNameExtension;
            string s = this.GetFilenameOnlyWithoutExtension() + "." + fileNameExtension;

            s = CS.General_v3.Util.IO.ConvertStringToFilename(s);
            return s;
        }
        public string GetUrlForSpecificSize(TEnum size, bool includeBaseUrl = true)
        
        {
            return this.getUrlForSpecificSize(CS.General_v3.Util.EnumUtils.StringValueOf(size), includeBaseUrl);
        }
        protected string getUrlForSpecificSize(string size, bool includeBaseUrl = true, bool checkImageSizes = true)
        {
            IMediaItemImageSpecificSizeInfo sizeInfo = getSpecificSizeInfoByTitle(size);
            return getUrlForSpecificSize(sizeInfo, includeBaseUrl, checkImageSizes);
            

        }
        protected string getUrlForSpecificSize(IMediaItemImageSpecificSizeInfo sizeInfo, bool includeBaseUrl = true, bool checkImageSizes = true)
        {
            if (!IsEmpty())
            {
                
                if (checkImageSizes)
                {
                    checkImageSpecificSizes(false);
                }

                string localPath = getLocalPathForSpecificSize(sizeInfo);
                string webPath = CS.General_v3.Util.PageUtil.MapPathFromLocalToWeb(localPath, includeBaseUrl: includeBaseUrl);
                //string webPath = GetBaseFolderUrl(includeBaseUrl);
                //webPath += CS.General_v3.Util.IO.ConvertStringToFilename(sizeInfo.Title) + "/" + CS.General_v3.Util.IO.ConvertStringToFilename(getFilenameForSpecificSize(sizeInfo));
                return webPath;
               
            }
            else
                return null;

        }
        public string GetLocalPathForSpecificSize<TEnum>(TEnum size)
            where TEnum : struct
        {
            return this.getLocalPathForSpecificSize(CS.General_v3.Util.EnumUtils.StringValueOf(size));
        }
        
        internal string getLocalPathForSpecificSize(string size)
        {
            var sizeInfo = getSpecificSizeInfoByTitle(size);
            return getLocalPathForSpecificSize(sizeInfo);
        }

        internal string getLocalPathForSpecificSize(IMediaItemImageSpecificSizeInfo sizeInfo)
        {
            if (!IsEmpty())
            {
                if (checkIfFileIsImageAndCanBeResized())
                {
                    string webPath = GetBaseFolderUrl(includeBaseUrl: false);
                    webPath += CS.General_v3.Util.IO.ConvertStringToFilename(sizeInfo.Title) + "/" + CS.General_v3.Util.IO.ConvertStringToFilename(getFilenameForSpecificSize(sizeInfo));
                    string localPath = CS.General_v3.Util.PageUtil.MapPath(webPath);
                    return localPath;
                }
                else
                {
                    return GetLocalPath();
                }
            }
            else
                return null;

        }


        #region IMediaItemImage<TEnum> Members

        string IMediaItemImage<TEnum>.GetOriginalImageLocalPath()
        {
            return this.GetLocalPath();
            
        }

        string IMediaItemImage<TEnum>.GetOriginalImageUrl(bool includeBaseUrl = true)
        {
            return this.GetUrl(includeBaseUrl);
            
        }

        string IMediaItemImage<TEnum>.GetSpecificSizeUrl(TEnum size, bool includeBaseUrl = true)
        {
            return this.GetUrlForSpecificSize(size, includeBaseUrl);
            
        }
        string IMediaItemImage<TEnum>.GetSpecificSizeUrlForGenericEnum(Enum size, bool includeBaseUrl = true)
        {
            return this.getUrlForSpecificSize(CS.General_v3.Util.EnumUtils.StringValueOf(size), includeBaseUrl);
        }

        string IMediaItemImage<TEnum>.GetSpecificSizeLocalPath(TEnum size)
        {
            return this.GetLocalPathForSpecificSize<TEnum>(size);
            
        }

        

        #endregion

        protected void checkImageSpecificSizes(bool overwriteExisting)
        {
            if (checkIfFileIsImageAndCanBeResized())
            {
                ImageSpecificSizingChecker.Instance.CheckImageSizes<TEnum>(this, overwriteExisting);
            }

        }

        public override void DeleteFile()
        {
            base.DeleteFile();
            if (this.MediaItemInfo != null && this.MediaItemInfo.SizesInfo != null)
            {
                foreach (var size in this.MediaItemInfo.SizesInfo)
                {
                    string path = this.getLocalPathForSpecificSize(size);
                    if (File.Exists(path))
                    {
                        CS.General_v3.Util.IO.DeleteFile(path);
                    }
                }
            }
        }

        protected override void postUploadFile(System.IO.Stream fileContents, string fileName, bool autoSave, CS.General_v3.Classes.HelperClasses.OperationResult result)
        {
            if (result.IsSuccessful)
            {
                resizeOriginalImage();
                checkImageSpecificSizes(true);
            }
            base.postUploadFile(fileContents, fileName, autoSave, result);
        }
        

        public override string GetUrl(bool includeBaseUrl = true)
        {
            return GetImageUrlForLargestSize(includeBaseUrl);
            
        }
        public string GetOriginalImageUrl(bool includeBaseUrl)
        {
            return base.GetUrl(includeBaseUrl);
        }

        public string GetImageUrlForLargestSize(bool includeBaseUrl = true)
        {
            IMediaItemImageSpecificSizeInfo largestSize = null;
            int largestSizeTotal = 0;
            foreach (var size in this.MediaItemInfo.SizesInfo)
            {
                int sizeTotal = size.Width * size.Height;
                if (largestSize == null || sizeTotal > largestSizeTotal)
                {
                    largestSize = size;
                    largestSizeTotal = sizeTotal;
                }

            }
            return getUrlForSpecificSize(largestSize, includeBaseUrl);
            
        }

        public override string GetThumbnailImageUrl(bool includeBaseUrl = true)
        {
            IMediaItemImageSpecificSizeInfo leastSize = null;
            int leastSizeTotal = 0;
            foreach (var size in this.MediaItemInfo.SizesInfo)
            {
                int sizeTotal = size.Width * size.Height;
                if (leastSize == null || sizeTotal < leastSizeTotal)
                {
                    leastSize = size;
                    leastSizeTotal = sizeTotal;
                }
                
            }
            if (leastSize != null)
                return getUrlForSpecificSize(leastSize, includeBaseUrl);
            else
                return base.GetThumbnailImageUrl(includeBaseUrl);


        }
    }
}
