﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.MediaItems.Impl
{
    public class MediaItemImageSpecificSizeInfo : IMediaItemImageSpecificSizeInfo
    {
        #region IMediaItemImageSpecificSizeInfo Members

        public int Width { get; set; }

        public int Height { get; set; }

        public CS.General_v3.Enums.CROP_IDENTIFIER CropIdentifier { get; set; }

        public CS.General_v3.Enums.IMAGE_FORMAT? ImageFormat { get; set; }

        public string Title { get; set; }

        #endregion
    }
}
