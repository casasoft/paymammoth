﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Linq;
using BusinessLogic_v3.Classes.DbObjects;
using System.Reflection;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Frontend._Base;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Classes.MediaItems.Impl
{
    public class MediaItemImageInfo : MediaItemInfoImpl, IMediaItemImageInfo
    {
        public MediaItemImageInfo()
        {
            this.SizesInfo = new List<IMediaItemImageSpecificSizeInfo>();
            this.MaxWidth = 1600;
            this.MaxHeight = 1200;

        }



        

        public void InitByDb<TItem>(TItem item, 
            Type enumType,
            System.Linq.Expressions.Expression<Func<TItem, string>> filenamePropertySelector,
            Func<string> titleRetriever, 
            string imageIdentifier)
            where TItem: IBaseDbObject
        {
            base.InitByDb<TItem>(item, filenamePropertySelector, titleRetriever, imageIdentifier);

            
            this.EnumType = enumType;
            loadFromDb();

        }

        public Type EnumType { get; set; }

        protected ImageSizingInfoBase getImageSizingInfoFromDb()
        {
            string identifier = this.Identifier;
            ImageSizingInfoBase sizingInfo = ImageSizingInfoBaseFactory.Instance.GetByIdentifier(identifier);



            if (sizingInfo == null)
            {
                var session = nHibernateUtil.GetCurrentSessionFromContext();
                using (var t = session.BeginTransaction())
                {
                    sizingInfo = ImageSizingInfoBase.Factory.CreateNewItem();
                    sizingInfo.MaximumWidth = 1600;
                    sizingInfo.MaximumHeight = 1200;
                    sizingInfo.Identifier = identifier;
                    sizingInfo.Title = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(identifier);
                    sizingInfo.Create();
                    if (t.IsActive)
                        t.Commit();
                }
                sizingInfo.CreateDefaultSpecificSizingFromEnumType(this.EnumType);
                    


            }
            return sizingInfo;
        }

        private void loadFromDb()
        {

            ImageSizingInfoBase imageSizingInfo = getImageSizingInfoFromDb();
            this.MaxWidth = imageSizingInfo.MaximumWidth;
            this.MaxHeight = imageSizingInfo.MaximumHeight;
            if (imageSizingInfo.SizingInfos != null)
            {
                var enumValues = CS.General_v3.Util.EnumUtils.GetListOfEnumValues(this.EnumType);
                foreach (Enum enumValue in enumValues)
                {
                    var specificSizeInfo = imageSizingInfo.GetSpecificSizeInfoByIdentifier(enumValue);
                    if (specificSizeInfo != null)
                    {
                        MediaItemImageSpecificSizeInfo specificSizing = new MediaItemImageSpecificSizeInfo();
                        specificSizing.CropIdentifier = specificSizeInfo.CropIdentifier;
                        specificSizing.Height = specificSizeInfo.Height;
                        specificSizing.Width = specificSizeInfo.Width;
                        specificSizing.Title = specificSizeInfo.Identifier;
                        specificSizing.ImageFormat = specificSizeInfo.ImageFormat;
                        this.SizesInfo.Add(specificSizing);
                    }
                    else
                    {
                        throw new InvalidOperationException("Specific Size <" + enumValue + "> not found for '" + this.Identifier + "'");
                    }

                }


            }

        }

        #region IMediaItemImageInfo Members

        public List<IMediaItemImageSpecificSizeInfo> SizesInfo { get; private set; }
        public int MaxWidth { get; set; }

        public int MaxHeight { get; set; }

        #endregion


        #region IMediaItemImageInfo Members

        ICollection<IMediaItemImageSpecificSizeInfo> IMediaItemImageInfo.SizesInfo
        {
            get { return this.SizesInfo.Cast < IMediaItemImageSpecificSizeInfo>().ToList(); }
        }



        #endregion
    }
}
