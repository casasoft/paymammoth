﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
using System.Linq.Expressions;

namespace BusinessLogic_v3.Classes.MediaItems.Impl
{
    public class MediaItemInfoImpl : IMediaItemInfo
    {

        public MediaItemInfoImpl()
        {


            this.BaseUploadFolder = "/uploads/media/";
        }



        private IBaseDbObject _item = null;

        public void InitByDb<TItem>(TItem item, 
            System.Linq.Expressions.Expression<Func<TItem, string>> filenamePropertySelector,
            Func<string> titleRetriever, 
            string identifier)
            where TItem : IBaseDbObject
        {
            
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(filenamePropertySelector, "Filename property selector must be filled in");
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(identifier, "Identifier must be specified");
            this._item = item;
            propertyFilename = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(filenamePropertySelector);
            if (titleRetriever != null)
                this.titleRetriever = titleRetriever;
            this.Identifier = identifier;
          //  this.Title = title;
            

        }



        public string Identifier { get; private set; }
        private PropertyInfo propertyFilename;
        private Func<string> titleRetriever;

        //public string ImageTitle { get; set; }

      

        #region IMediaItemInfo Members

        public string BaseUploadFolder { get; set; }

        public string Filename
        {
            get
            {
                return (string)propertyFilename.GetValue(this.DbItem, null);
                
            }
            set { propertyFilename.SetValue(this.DbItem, value, null); }


        }

        public string Title
        {
            get
            {
                if (this.titleRetriever != null)
                    return titleRetriever();

                else
                    return null;

            }

        }

        public DbObjects.Objects.IBaseDbObject DbItem
        {

            get { return _item; }
            set { _item = value; }
        }

        public bool DeleteDbItemOnRemove { get; set; }
        #endregion





        #region IMediaItemInfo Members

        public string GetBaseUploadFolder()
        {
            string s = this.BaseUploadFolder;
            if (!string.IsNullOrWhiteSpace(this.Identifier))
                s += CS.General_v3.Util.IO.ConvertStringToFilename(this.Identifier) + "/";
            return s;
            
        }

        #endregion
    }
}
