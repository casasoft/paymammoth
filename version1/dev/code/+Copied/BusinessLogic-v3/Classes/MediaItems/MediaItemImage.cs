﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.MediaItems;
using BusinessLogic_v3.DB;
using System.Reflection;

namespace BusinessLogic_v3.Classes.MediaItems
{
    /// <summary>
    /// Creates an image class.  Image can be linked either to database settings, or custom-settings providers
    /// </summary>
    /// <typeparam name="TItem">Class of Item Image will be stored in, e.g Artist</typeparam>
    /// <typeparam name="TEnum">Enumeration of image sizes, e.g ALBUM_IMAGE_SIZE</typeparam>
    public class MediaItemImage<TEnum> : CS.General_v3.Classes.MediaItems.MediaItemImageWithDBObjectAndEnum<TEnum>
        where TEnum : struct
    {

        private PropertyInfo property = null;
        /// <summary>
        /// Creates a new image class
        /// </summary>
        /// <param name="dbItem">Instance of dbItem, of type TItem as passed in constructor, e.g. instance of 'Artist'</param>
        /// <param name="title">Title of image type to be stored in.  If left null, name of TItem Type will be taken, e.g. 'Artist'</param>
        /// <param name="customSizes">A list of static custom size infos</param>
        /// <param name="loadFromDelegate">A delegate which will return a size info, for dynamic size info generation</param>
        /// <param name="loadFromDelegates">A list of delegates which will each return a size info, for dynamic size info generation</param>
        public MediaItemImage(IMediaItemImageDB dbItem, PropertyInfo propertyInfo)
            : base(dbItem)
        {
            this.property = propertyInfo;
            
            

            initBusinessLogicImage(dbItem);
        }

        protected void initBusinessLogicImage(IMediaItemDB dbItem)
        {
            
            string baseUploadFolder = Util.GetUploadFolderFromRootByPropertyInfo(property);

            string settingsName = Util.GetSettingNameByPropertyInfo(property, null);
            init
            initImageBase(baseUploadFolder, customSizes, null, null, loadFromDelegates, null);
            initImageWithDBObjectAndEnum(settingsName);
        }



    }
}
