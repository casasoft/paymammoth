﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace BusinessLogic_v3.Classes.MediaItems
{
    public interface IMediaItemInfo
    {
        /// <summary>
        /// Web-based
        /// </summary>
        string GetBaseUploadFolder();
        string Filename { get; set; }
        string Title { get;  }
        IBaseDbObject DbItem { get; set; }
        bool DeleteDbItemOnRemove { get; set; }

    }
}
