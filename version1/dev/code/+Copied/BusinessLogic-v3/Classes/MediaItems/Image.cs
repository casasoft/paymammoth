﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.MediaItems;
using BusinessLogic_v3.DB;

namespace BusinessLogic_v3.Classes.MediaItems
{
    /// <summary>
    /// Creates an image class.  Image can be linked either to database settings, or custom-settings providers
    /// </summary>
    /// <typeparam name="TItem">Class of Item Image will be stored in, e.g Artist</typeparam>
    /// <typeparam name="TEnum">Enumeration of image sizes, e.g ALBUM_IMAGE_SIZE</typeparam>
    public class Image<TItem, TEnum> : CS.General_v3.Classes.MediaItems.ImageWithDBObjectAndEnum<TEnum>
        where TItem : CS.General_v3.Classes.nHibernateDB.BaseDbObject_Base, IMediaItemDB
        where TEnum : struct
    {
        
        public static string BASE_UPLOAD_FOLDER = "/uploads/media/";

        /// <summary>
        /// Creates a new image class
        /// </summary>
        /// <param name="dbItem">Instance of dbItem, of type TItem as passed in constructor, e.g. instance of 'Artist'</param>
        /// <param name="title">Title of image type to be stored in.  If left null, name of TItem Type will be taken, e.g. 'Artist'</param>
        /// <param name="customSizes">A list of static custom size infos</param>
        /// <param name="loadFromDelegate">A delegate which will return a size info, for dynamic size info generation</param>
        /// <param name="loadFromDelegates">A list of delegates which will each return a size info, for dynamic size info generation</param>
        public Image(TItem dbItem, string title = null, List<ImageSizeInfo> customSizes = null,
             ImageSizeInfo.ImageSizeInfoDelegate loadFromDelegate = null, List<ImageSizeInfo.ImageSizeInfoDelegate> loadFromDelegates = null)
            : base(dbItem)
        {
            List<ImageSizeInfo.ImageSizeInfoDelegate> loadFrom = new List<ImageSizeInfo.ImageSizeInfoDelegate>();
            if (loadFromDelegate != null) loadFrom.Add(loadFromDelegate);
            if (loadFromDelegates != null) loadFrom.AddRange(loadFromDelegates);

            initBusinessLogicImage(dbItem, title, customSizes, loadFrom);
        }

        protected void initBusinessLogicImage(TItem dbItem, string title = null, List<ImageSizeInfo> customSizes = null,
            List<ImageSizeInfo.ImageSizeInfoDelegate> loadFromDelegates = null)
        {
            string classTitle = title;

            if (string.IsNullOrEmpty(classTitle))
            {
                classTitle = CS.General_v3.Util.IO.ConvertStringToFilename(CS.General_v3.Util.ReflectionUtil<object>.GetNameFromType(typeof(TItem)));
            }

            string baseUploadFolder = BASE_UPLOAD_FOLDER + classTitle + "/";

            string settingsName = Util.GetBaseSettingName(typeof(TItem));

            initImageBase(baseUploadFolder, customSizes, null, null, loadFromDelegates, null);
            initImageWithDBObjectAndEnum(settingsName);
        }

        public void CreateImageSizesInDatabase()
        {
            for (int i = 0; i < this.imageSizes.Count; i++)
            {
                var imageSize = this.imageSizes[i];
                string settingName = base.baseEnumSetting + "/" + imageSize.Title;

                string s = SettingBase.GetSetting(settingName);
                if (string.IsNullOrWhiteSpace(s))
                {
                    SettingBase.SetSetting(settingName, "1600x1200-None", Enums.CMS_ACCESS_TYPE.CasaSoft);
                }



            }
        }


    }
}
