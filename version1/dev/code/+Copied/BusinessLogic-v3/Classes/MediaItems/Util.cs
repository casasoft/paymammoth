﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.MediaItems;
using log4net;
using System.Reflection;
using BusinessLogic_v3.Classes.MediaItems;

namespace BusinessLogic_v3.Classes.MediaItems
{
    public static class Util
    {
        public static ILog _log = log4net.LogManager.GetLogger(typeof(Util));
        public static string BASE_UPLOAD_FOLDER = "/uploads/media/";




        public static string GetUploadFolderFromRootByPropertyInfo(PropertyInfo pInfo)
        {
            return Util.BASE_UPLOAD_FOLDER + CS.General_v3.Util.IO.ConvertStringToFilename(pInfo.DeclaringType.Name) + "/" + CS.General_v3.Util.IO.ConvertStringToFilename(pInfo.Name) + "/";
        }

    }
}
