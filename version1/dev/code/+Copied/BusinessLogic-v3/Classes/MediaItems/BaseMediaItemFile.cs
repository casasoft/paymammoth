﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Classes.MediaItems
{
    public abstract class BaseMediaItemFile : IMediaItem
    {
        public IMediaItemInfo MediaItemInfo { get; set; }
        public virtual bool IsEmpty()
        {

            return (string.IsNullOrEmpty(this.MediaItemInfo.Filename));

        }
        public string GetFilenameExtension()
        {
            return CS.General_v3.Util.IO.GetExtension(this.MediaItemInfo.Filename);
        }
        public string GetFilenameOnlyWithoutExtension()
        {
            return CS.General_v3.Util.IO.GetFilenameOnly(this.MediaItemInfo.Filename);
        }
        public string GetLocalBaseFolder()
        {

            string localPath = null;
            if (!IsEmpty())
            {

                string webPath = GetBaseFolderUrl(includeBaseUrl: false);


                localPath = CS.General_v3.Util.IO.EnsurePathEndsWithSlash(CS.General_v3.Util.PageUtil.MapPath(webPath));
            }
            return localPath;
        }

        

        public string GetLocalPath()
        {
            string localPath = null;
            if (!IsEmpty())
            {
                string webPath = GetBaseFolderUrl(includeBaseUrl:false) + CS.General_v3.Util.IO.ConvertStringToFilename(this.MediaItemInfo.Filename);

                //string webPath = GetUrl(includeBaseUrl: false);
                localPath = CS.General_v3.Util.PageUtil.MapPath(webPath);
            }
            return localPath;
        }

        public bool CheckIfOriginalFileExists()
        {
            bool exists = false;
            string localPath = GetLocalPath();
            if (!string.IsNullOrWhiteSpace(localPath))
            {
                exists = (File.Exists(localPath));
            }
            return exists;
            
        }
        public string GetBaseFolderUrl(bool includeBaseUrl = true)
        {
            string s = null;
            if (!IsEmpty())
            {
                if (includeBaseUrl)
                    s = CS.General_v3.Util.IO.EnsurePathDoesNotEndWithSlash(CS.General_v3.Util.PageUtil.GetApplicationBaseUrl().GetURL(fullyQualified: true));
                string baseUploadFolder = this.MediaItemInfo.GetBaseUploadFolder();
                s += CS.General_v3.Util.IO.EnsurePathStartsAndEndsWithSlash(baseUploadFolder);
                s = s.Replace("\\", "/");
            }
            return s;
        }
        public virtual string GetUrl(bool includeBaseUrl = true)
        {
            string s =null;
            if (!IsEmpty())
            {
                string localPath = GetLocalPath();
                s = CS.General_v3.Util.PageUtil.MapPathFromLocalToWeb(localPath);
                //s = GetBaseFolderUrl(includeBaseUrl) + this.MediaItemInfo.Filename;
                s = s.Replace("\\", "/");
            }
            return s;
        }

        protected string getFilename(string originalFilename)
        {
            string ext = CS.General_v3.Util.IO.GetExtension(originalFilename);
            string filenameOnly = CS.General_v3.Util.IO.GetFilenameOnly(originalFilename);

            List<string> fileNameComponents = new List<string>();
            fileNameComponents.Add(this.MediaItemInfo.Title);
            fileNameComponents.Add(filenameOnly);

            int rndLen = 3;
            if (this.MediaItemInfo.DbItem == null || this.MediaItemInfo.DbItem.IsTransient())
            {
                rndLen = 9;
            }

            fileNameComponents.Add(CS.General_v3.Util.Random.GetAlpha(rndLen, rndLen));

            if (this.MediaItemInfo.DbItem != null && !this.MediaItemInfo.DbItem.IsTransient())
            {
                fileNameComponents.Add(this.MediaItemInfo.DbItem.ID.ToString());
            }
            string s = CS.General_v3.Util.Text.AppendStrings("-", fileNameComponents);
            s =CS.General_v3.Util.IO.ConvertStringToFilename(s);
            s += "." +  ext;
            

            return s;
        }

        public virtual void DeleteFile()
        {
            if (!IsEmpty())
            {
                string path = GetLocalPath();
                if (File.Exists(path))
                {
                    CS.General_v3.Util.IO.DeleteFile(path);
                }

            }
        }

        protected virtual void postUploadFile(Stream fileContents, string fileName, bool autoSave, OperationResult result)
        {

        }
        public OperationResult UploadFileFromUrl(string url)
        {
            string filename = CS.General_v3.Util.IO.GetFilenameAndExtension(url);
            string ext = CS.General_v3.Util.IO.GetExtension(filename);
            string tmpFile = CS.General_v3.Util.IO.GetTempFilename(ext);
            OperationResult result = new OperationResult();

            try
            {
                CS.General_v3.Util.Web.DownloadFile(url, tmpFile);
            }
            catch (Exception ex)
            {
                result.AddException(ex);
            }
            if (result.IsSuccessful)
            {
                return UploadFileFromLocalPath(tmpFile);
            }
            else
            {
                return result;
            }
        }

        public OperationResult UploadFileFromLocalPath(FileInfo file, bool autoSave = true)
        {
            return UploadFileFromLocalPath(file.FullName, autoSave);
        }

        public OperationResult UploadFileFromLocalPath(string localPath, bool autoSave = true)
        {
            OperationResult result = new OperationResult();
            if (File.Exists(localPath))
            {
                FileStream fs = new FileStream(localPath, FileMode.Open, FileAccess.Read);
                string filename = CS.General_v3.Util.IO.GetFilenameAndExtension(localPath);
                this.UploadFile(fs, filename, autoSave);
                postUploadFile(fs, filename, true, result);
                fs.Dispose();
                
            }
            else
            {
                result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "File does not exist");
            }
            
            return result;

        }

        public virtual OperationResult UploadFile(Stream fileContents, string fileName, bool autoSave = true)
        {
         //   continue here
                //continue implementing the new 'media uploading' to a very simple way that always expects to load settings from database, no need to complicate matters with generalisation

            OperationResult result = new OperationResult();
            DeleteFile();
            string saveFilename = getFilename(fileName);
            NHibernateClasses.Transactions.MyTransaction t = null;
            if (autoSave)
                t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext();

            this.MediaItemInfo.Filename = saveFilename;
            string pathToSave = GetLocalPath();

            CS.General_v3.Util.IO.SaveStreamToFile(fileContents, pathToSave);
            if (!this.MediaItemInfo.DbItem.IsTransient() && autoSave)
            {
                this.MediaItemInfo.DbItem.Update();
                t.Commit();
            }
            if (t != null) t.Dispose();

            postUploadFile(fileContents, fileName, autoSave, result);
            return result;

        }
        public virtual void Delete()
        {
            using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                DeleteFile();
                this.MediaItemInfo.Filename = null;
                if (this.MediaItemInfo.DeleteDbItemOnRemove)
                {
                    this.MediaItemInfo.DbItem.Delete();
                }
                else
                {
                    if (!this.MediaItemInfo.DbItem.IsTransient())
                        this.MediaItemInfo.DbItem.Update();
                }
                t.Commit();
            }
        }

        #region IMediaItem Members

        string IMediaItem.Filename
        {
            get { return this.MediaItemInfo.Filename; }
        }

        public virtual string GetThumbnailImageUrl(bool includeBaseUrl = true)
        {
            return this.GetUrl(includeBaseUrl);
            
        }

        #endregion
    }
}
