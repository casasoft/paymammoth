﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.MediaItems;
using BusinessLogic_v3.DB;
using CS.General_v3.Classes.HelperClasses;
using System.Reflection;

namespace BusinessLogic_v3.Classes.MediaItems
{
    /// <summary>
    /// Creates an image class.  Image can be linked either to database settings, or custom-settings providers
    /// </summary>
    /// <typeparam name="TItem">Class of Item File will be stored in, e.g Artist</typeparam>
    /// <typeparam name="TEnum">Enumeration of image sizes, e.g ALBUM_IMAGE_SIZE</typeparam>
    public class MediaItemFile : CS.General_v3.Classes.MediaItems.MediaItemBaseFile
     
        
    {

        protected IMediaItemDB dbItem;
        protected PropertyInfo property;

        /// <summary>
        /// Creates a new image class
        /// </summary>
        /// <param name="dbItem">Instance of dbItem, of type TItem as passed in constructor, e.g. instance of 'Artist'</param>
        /// <param name="title">Title of image type to be stored in.  If left null, name of TItem Type will be taken, e.g. 'Artist'</param>
        /// <param name="customSizes">A list of static custom size infos</param>
        /// <param name="loadFromDelegate">A delegate which will return a size info, for dynamic size info generation</param>
        /// <param name="loadFromDelegates">A list of delegates which will each return a size info, for dynamic size info generation</param>
        public MediaItemFile(IMediaItemDB dbItem, PropertyInfo propertyInfo)
            : base()
        {
            this.property = propertyInfo;
            initMediaItem(dbItem);
            this.property = propertyInfo;
        }

        private void initMediaItem(IMediaItemDB dbItem)
        {
            
            this.dbItem = dbItem;

            this.Folder = Util.GetUploadFolderFromRootByPropertyInfo(property);
        }

        public override string Identifier
        {
            get { return this.dbItem.Identifier; }
            set { this.dbItem.Identifier = value; }
        }

        protected override void generateIdentifier(string uploadedFilename)
        {
            string fileNameOnly = CS.General_v3.Util.IO.GetFilenameOnly(uploadedFilename);
            string ext = CS.General_v3.Util.IO.GetExtension(uploadedFilename);

            this.dbItem.GenerateFilename(fileNameOnly, ext);
            
        }


        public override int Priority
        {
            get
            {
                return this.dbItem.Priority;
                
            }
            set
            {
                this.dbItem.Priority = value;
                
            }
        }

        public override CS.General_v3.Enums.ITEM_TYPE ItemType
        {
            get { return CS.General_v3.Enums.ItemTypeFromFilename(this.Identifier);}
        }

        public override string GetThumbnailImageUrl()
        {
            string extension = CS.General_v3.Util.IO.GetExtension(this.Identifier);
            switch (extension)
            {

            }
            return "#";
        }

        public override string GetLargeImageUrl()
        {
            string extension = CS.General_v3.Util.IO.GetExtension(this.Identifier);
            switch (extension)
            {

            }
            return "#";
            
        }


        public override string GetMediaItemUrl()
        {
            return this.GetWebUrl();
        }

        public override string Caption
        {
            get
            {
                return this.dbItem.Caption;
                
            }
            set
            {
                this.dbItem.Caption = value;
                
            }
        }


        public override OperationResult Delete()
        {
            return this.dbItem.Delete();
            
        }

        public override OperationResult SetMediaItem(System.IO.Stream txt, string filename)
        {

            return this.UploadFile(txt, filename);

        }

        public override OperationResult Save()
        {
            return this.dbItem.Save();
            
        }

        public override OperationResult SetAsMainItem()
        {
            return this.dbItem.SetAsMainItem();
            
        }

        public override string ExtraValueChoice
        {
            get
            {
                return this.dbItem.ExtraValueChoice;
                
            }
            set
            {
                this.dbItem.ExtraValueChoice = value;
                
            }
        }

        public override OperationResult UploadFile(System.IO.Stream fileConents, string fileName, bool autoSave = true)
        {
            var result = base.uploadFile(this.Folder, null, null, null, fileConents, fileName);
            if (autoSave)
            {
                var saveResult = dbItem.Save();
                result.AddFromOperationResult(saveResult);
            }
            
            return result;
        }
        
    }
}
