using System.IO;
using CS.General_v3.Classes.HelperClasses;

namespace BusinessLogic_v3.Classes.MediaItems
{
    public interface IMediaItem
    {
        bool IsEmpty();
        string Filename { get; }
        string GetFilenameOnlyWithoutExtension();
        string GetLocalPath();
        string GetUrl(bool includeBaseUrl = true);
        string GetThumbnailImageUrl(bool includeBaseUrl = true);
        OperationResult UploadFile(Stream fileContents, string fileName, bool autoSave = true);
        void Delete();
    }
}