using System.IO;
using CS.General_v3.Classes.HelperClasses;
using System;

namespace BusinessLogic_v3.Classes.MediaItems
{
    public interface IMediaItemImage<TEnum>
        where TEnum: struct
    {
        IMediaItemImageInfo MediaItemInfo { get; set; }
        CS.General_v3.Enums.IMAGE_FORMAT GetOriginalImageFormat();
        bool IsEmpty();
        string GetFilenameOnlyWithoutExtension();
        string GetOriginalImageLocalPath();
        string GetOriginalImageUrl(bool includeBaseUrl = true);
        string GetSpecificSizeUrl(TEnum size, bool includeBaseUrl = true);
        string GetSpecificSizeUrlForGenericEnum(Enum size, bool includeBaseUrl = true);
        string GetSpecificSizeLocalPath(TEnum size);


        OperationResult UploadFile(Stream fileContents, string fileName, bool autoSave = true);
        void Delete();
    }
}