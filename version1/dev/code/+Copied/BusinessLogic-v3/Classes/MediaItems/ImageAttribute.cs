﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.MediaItems;
using BusinessLogic_v3.DB;
using CS.General_v3.Classes.MediaItems.ImageSizes;
using log4net;

using System.Reflection;

namespace BusinessLogic_v3.Classes.MediaItems
{
    public class ImageAttribute: Attribute
        
    {
        private ILog _log = log4net.LogManager.GetLogger(typeof(ImageAttribute));
         public Type EnumType { get; set; }

        public ImageAttribute(Type enumType)
        {
          
            
            this.EnumType = enumType;
        }


        public void CheckImageSizesInDatabase(PropertyInfo pInfo )
        {
            if (_log.IsDebugEnabled) _log.Debug("CheckImageSizesInDatabase() <" + pInfo.DeclaringType.FullName + "." + pInfo.Name + " - Start");

            Type containingType = pInfo.DeclaringType;

            var enumValues = CS.General_v3.Util.EnumUtils.GetListOfEnumValues(EnumType);
            //_log.Info("CheckImageSizesInDatabase() - Start");
            var settingFactory = BusinessLogic_v3.Modules.v2.Factories.SettingDataFactory;
            bool oneFound = false;
            foreach (var enumValue in enumValues) {
            
                string enumTitle = CS.General_v3.Util.EnumUtils.StringValueOf(enumValue);
                string settingName = Util.GetSettingNameByPropertyInfo(pInfo, enumTitle);

                CS.General_v3.Classes.Attributes.SettingsInfoAttribute defaultValues = new CS.General_v3.Classes.Attributes.SettingsInfoAttribute();

                string s = settingFactory.GetSettingValueByStringIdentifier<string>(settingName, defaultValues, false);
                if (_log.IsDebugEnabled) _log.Debug("Checking Enum <" + enumTitle + ">");

                if (string.IsNullOrWhiteSpace(s))
                {
                    ImageDefaultSizeInfoAttribute imageDefaultSizeAttrib = (ImageDefaultSizeInfoAttribute)CS.General_v3.Util.EnumUtils.GetAttributeFromEnumValue(enumValue, typeof(ImageDefaultSizeInfoAttribute));
                    string defValue = "1600x1200-None";
                    if (imageDefaultSizeAttrib != null)
                        defValue = imageDefaultSizeAttrib.GetAsStringValue();
                    if (_log.IsDebugEnabled) _log.Debug("Value for Enum <" + enumTitle + "> not found, default Value is <" + defValue + ">");
                    settingFactory.SetSettingByStringIdentifier(settingName, defValue, new CS.General_v3.Classes.Attributes.SettingsInfoAttribute(defaultValue: defValue));

                    
                }
                
            }

            if (_log.IsDebugEnabled)  _log.Debug("CheckImageSizesInDatabase() <" + pInfo.DeclaringType.FullName + "." + pInfo.Name + " - End");
           // _log.Info("CheckImageSizesInDatabase() - End");

        }


    }
}
