﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;

namespace BusinessLogic_v3.Classes.MediaItems
{
    public interface IMediaItemImageSpecificSizeInfo
    {
        int Width { get; }
        int Height { get; }
        CS.General_v3.Enums.CROP_IDENTIFIER CropIdentifier { get; }
        CS.General_v3.Enums.IMAGE_FORMAT? ImageFormat { get; }
        
        string Title { get; }
    }
}
