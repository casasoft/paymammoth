﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.MediaItems
{
    public class ImageSpecificSizeDefaultValuesAttribute : Attribute
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public CS.General_v3.Enums.CROP_IDENTIFIER CropType { get; set; }
        public CS.General_v3.Enums.IMAGE_FORMAT ImageFormat { get; set; }
        public ImageSpecificSizeDefaultValuesAttribute()
        {
            this.CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None;
            this.ImageFormat = CS.General_v3.Enums.IMAGE_FORMAT.None ;
            this.Width = 640;
            this.Height = 480;
        }
        
        public string GetAsStringValue()
        {
            return Width + "x" + Height + "-" + CS.General_v3.Util.EnumUtils.StringValueOf(CropType);
        }
        public override string ToString()
        {
            return GetAsStringValue();
        }
    }
}
