﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using MySql.Data.MySqlClient;
using NHibernate;
using NHibernate.Cfg;
using System.Reflection;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Exceptions;
using NHibernate.Impl;
using log4net;
using BusinessLogic_v3.Classes.NHibernateClasses.SessionContextManagers;
using BusinessLogic_v3.Classes.NHibernateClasses.NHConfiguration;

namespace BusinessLogic_v3.Classes.NHibernateClasses.NHManager
{
    public class StandardNhManager : BaseNhManager, INhManager
    {
        
        public static StandardNhManager Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<StandardNhManager>(); }
        }
        public override void CloseSession(MyNHSessionBase session, bool commitOrRollbackExistingTransactions = true)
        {

            if (session != null)
            {
                //if (commitOrRollbackExistingTransactions && session.Transaction != null)
                //{
                //    if (session.Transaction.IsActive)
                //    {
                //        //if there is an active session, commit it
                //        session.Transaction.Commit();
                //    }
                //    else
                //    {
                //        //
                //        session.Transaction.Rollback();
                //    }
                //}
                try
                {
                    if (session.IsOpen)
                    {
                        session.Flush();
                        session.Close();
                    }
                    session.Dispose();
                }
                catch (GenericADOException ex)
                {
                    int k = 5;
                    throw;
                }
            }
        }


    }
}
