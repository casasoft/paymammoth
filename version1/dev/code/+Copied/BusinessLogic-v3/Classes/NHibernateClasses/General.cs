﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using NHibernate.Cfg.MappingSchema;

namespace BusinessLogic_v3.Classes.NHibernateClasses
{
    public static class General
    {

        public static string SerializeHbmMappingToString(HbmMapping hbmElement)
        {
            string result;
            var setting = new XmlWriterSettings { Indent = true };
            var serializer = new XmlSerializer(typeof(HbmMapping));
            using (var memStream = new MemoryStream(2048))
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(memStream, setting))
                {
                    serializer.Serialize(xmlWriter, hbmElement);
                }
                memStream.Position = 0;
                using (var sr = new StreamReader(memStream))
                {
                    result = sr.ReadToEnd();
                }
            }
            return result;
        }
    }
}
