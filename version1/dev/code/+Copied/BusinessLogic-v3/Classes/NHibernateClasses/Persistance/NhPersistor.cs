﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Persistors;
using BusinessLogic_v3.Util;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses.Transactions;
using NHibernate;
using NHibernate.Exceptions;
using BusinessLogic_v3.Classes.DbObjects;
using log4net;
using System.Diagnostics;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Persistance
{
    public class NhPersistor : IPersistor
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(NhPersistor));
        public NhPersistor()
        {
            //CS.General_v3.Util.ContractsUtil.RequiresNotNullable(_dbItem, "db item required");
        }

        #region IPersistor Members

        /// <summary>
        /// checks if a transaction exists, else throws error if in local-mode
        /// </summary>
        private void checkSessionAndTransactionExists(MyNHSessionBase session)
        {
            
            if (session == null)
            {
                throw new InvalidOperationException("Tried to call Create/Save/Update/Delete when a session does not exist for the current context.");
            }
            if ((session.Transaction == null || !session.Transaction.IsActive))
            {
                StringBuilder sb = new StringBuilder("Tried to call Create/Save/Update/Delete when a transaction was not active for the current session.  " +
                                                        "This is bad practice, and must be fixed by wrapping this save and any property updates/changes in a BeginTransaction() and Commit().  " +
                                                        "[IMPORTANT: This error will only be thrown in LOCAL-mode (development)].");
                sb.AppendLine();
                sb.AppendLine("Stack Trace:");
                sb.AppendLine("---------------");
                StackTrace stackTrace = new StackTrace();
                sb.AppendLine(stackTrace.ToString());
                
                if (CS.General_v3.Util.Other.IsLocalTestingMachine)
                {
                    //throw error only in local machine
                    throw new InvalidOperationException(sb.ToString());
                }
                else
                { //else, log it
                    _log.Warn(sb.ToString());
                }
            }
        }

        public void Save(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {



            if (session == null) session = nHibernateUtil.GetCurrentSessionFromContext();
            checkSessionAndTransactionExists(session);

            
            try
            {
                if (session is MyNHSessionNormal)

                    session.SaveOrUpdate(itemToPersist);
                else if (session is MyNHSessionStateless)
                    session.Update(itemToPersist);
                else
                    throw new InvalidOperationException("Never should happen");

            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;

            }
            
            
        }

        public void SaveAfter(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties, bool isNew)
        {
            if (session == null) session = nHibernateUtil.GetCurrentSessionFromContext();
            //var f = itemToPersist.GetFactory();
            //f.OnItemUpdated((DbObjects.Objects.IBaseDbObject) itemToPersist, (isNew ? Enums.UPDATE_TYPE.CreateNew : Enums.UPDATE_TYPE.Update));
            
        }



        public void Update(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
            if (session == null) session = nHibernateUtil.GetCurrentSessionFromContext();
            checkSessionAndTransactionExists(session);

            try
            {
                session.Update(itemToPersist);
            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;
            }
        }



        public void Delete(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool deletePermanently, bool autoCommit)
        {
            if (session == null) session = nHibernateUtil.GetCurrentSessionFromContext();

            checkSessionAndTransactionExists(session);

            try
            {

                session.Delete(itemToPersist);
                
            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;
            }
            
        }

        public void DeleteAfter(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool deletePermanently, bool autoCommit)
        {
            if (session == null) session = nHibernateUtil.GetCurrentSessionFromContext();
          //  var f = itemToPersist.GetFactory();
          //  f.OnItemUpdated((DbObjects.Objects.IBaseDbObject)itemToPersist, Enums.UPDATE_TYPE.Delete);
        }


        public void SaveOrUpdateCopy(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
            if (session == null) session = nHibernateUtil.GetCurrentSessionFromContext();
            checkSessionAndTransactionExists(session);

            try
            {
                session.Merge(itemToPersist);
            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;

            }
        }

        #endregion

        #region IPersistor Members

        public void Create(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
            if (session == null) session = nHibernateUtil.GetCurrentSessionFromContext();
            checkSessionAndTransactionExists(session);


            try
            {
                if (session is MyNHSessionNormal)

                    session.SaveOrUpdate(itemToPersist);
                else if (session is MyNHSessionStateless)
                {
                    ((MyNHSessionStateless)session).Insert(itemToPersist);
                }
                else
                    throw new InvalidOperationException("Never should happen");
            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;

            }
            
           
        }

        #endregion
    }
}
