﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.ConnectionProviders;
using MySql.Data.MySqlClient;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;

namespace BusinessLogic_v3.Classes.NHibernateClasses.DBConfig
{
    public class MySqlDbConfig: INHibernateDbConfig
    {
        
        #region IFluentDbConfig Members


        public MySqlDbConfig()
        {
            this.BatchSize = NHClasses.NH_BATCH_SIZE;

        }
        
            public int BatchSize { get; set; }

        public void ConfigureDatabase(Configuration cfg)
        {
            
            MySql.Data.MySqlClient.MySqlConnectionStringBuilder connStrBuilder = new MySqlConnectionStringBuilder();
            connStrBuilder.Server = CS.General_v3.Settings.Database.Host;
            connStrBuilder.UserID = CS.General_v3.Settings.Database.User;
            connStrBuilder.Password = CS.General_v3.Settings.Database.Pass;
            connStrBuilder.Database = CS.General_v3.Settings.Database.DatabaseName;
            connStrBuilder.Port = (uint)CS.General_v3.Settings.Database.Port;
            string connStr = connStrBuilder.ToString();
            //if (!connStr.EndsWith(";")) connStr += ";";
            //connStr += "MultipleActiveResultSets=true;";

            cfg= cfg.DataBaseIntegration(x =>
            {
                x.Dialect<MySQLDialect>();
                x.Driver<MySqlDataDriver>();
                x.ConnectionProvider<StandardConnectionProvider>();
                //  x.SchemaAction = SchemaAutoAction.Create;
                x.ConnectionString = connStr;
                x.BatchSize = (short)this.BatchSize;
                
            });



            //var persistenceConfig =
            //        FluentNHibernate.Cfg.Db.MySQLConfiguration.Standard

            //        .ConnectionString(connectionString)
            //        .Provider<StandardConnectionProvider>()
            //    //.Cache(c => c.UseQueryCache()
            //    // .ProviderClass<NHibernate.Cache.HashtableCacheProvider>())
            //    //.MaxFetchDepth(3)

            //        .AdoNetBatchSize(35);

            ////  persistenceConfig.DoNot.UseReflectionOptimizer();
            ////persistenceConfig.UseReflectionOptimizer();
            //return persistenceConfig;
        }


        


        #endregion
    }
}
