﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using NHibernate;
using NHibernate.Engine;
using log4net;
using System.Diagnostics;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Transactions
{
    public class MyTransaction : ITransaction
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(MyTransaction));
        bool _transactionOwner = false;
        bool _transactionActive = false;
        public bool IsActive
        {
            get
            {
                return _transactionActive;
            }
        }
        private MyNHSessionBase _session = null;
        private ITransaction _t = null;
        public MyTransaction(MyNHSessionBase session)
        {
            this._session = session;
        }

        protected ITransaction getTransactionFromSession(System.Data.IsolationLevel? isolationLevel)
        {
            if (isolationLevel.HasValue)
                return _session._beginTransaction(isolationLevel.Value);
            else
                return _session._beginTransaction();
        }

        public void Begin(System.Data.IsolationLevel? isolationLevel = null)
        {
            if (_transactionActive) return; //return to bad programer's code

            try
            {


                _transactionOwner = _session.Transaction == null || !_session.Transaction.IsActive;
                if (_transactionOwner)
                    _t = getTransactionFromSession(isolationLevel);
                else
                    _t = _session.Transaction;

                if (!_transactionOwner)
                {
                    onNestedTransactionError();
                }

                _transactionActive = true;
            }
            catch
            {
                _transactionOwner = false;
                _transactionActive = false;
                throw;
            }
        }
        


        protected bool sessionHasOpenTransaction()
        {
            return _session.Transaction != null && _session.Transaction.IsActive;
        }

        private void onNestedTransactionError()
        {

            if (CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                //if local, throw error.
                throw new InvalidOperationException("Nested transactions are not supported by NHibernate/database, and when used mean that there is a fault in the system design.  Please correct");


            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Nested transactions are not supported, and when used mean that there is a fault in the system design.  Please correct");
                sb.AppendLine();
                sb.AppendLine("Stack Trace:");
                sb.AppendLine("------------------");
                StackTrace stackTrace = new StackTrace();
                sb.AppendLine(stackTrace.ToString());
                _log.Warn(sb.ToString());
            }
        }

        private bool _commited = false;

        public void Commit()
        {
            _commited = true;
            if (!_transactionActive || !sessionHasOpenTransaction())
            {
                _transactionActive = false;
                return;
            }

            try
            {
                if (_transactionOwner)
                { //if owner commit
                    _t.Commit();
                }
                

            }
            catch (Exception ex)//when you don't know what you are committing to.
            {
                //_t.Rollback();
                
                 throw;
            }
            finally
            {
                _transactionActive = false;
            }
        }
        private bool _rolledBack = false; 

        public void Rollback()
        {
            _rolledBack = true;
            _transactionActive = false;

            if (!sessionHasOpenTransaction())
                return;
            else
            {
                
                _t.Rollback();
            }
            

        }

        #region ITransaction Members


        public void Enlist(System.Data.IDbCommand command)
        {
            _t.Enlist(command);
            
        }

        public void RegisterSynchronization(NHibernate.Transaction.ISynchronization synchronization)
        {
            _t.RegisterSynchronization(synchronization);
            
        }

        public bool WasCommitted
        {
            get { return _t.WasCommitted;}
        }

        public bool WasRolledBack
        {
            get { return _t.WasRolledBack; }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (CS.General_v3.Util.Other.IsLocalTestingMachine && !_commited && !_rolledBack)
            {
                //throw new InvalidOperationException("A transaction should never be disposed without either Committing or RollBack. This error will ONLY happen on dev. machines");
            }

            if (_t !=null)
                _t.Dispose();
            
        }

        #endregion

        [Obsolete("Use Commit()")]
        public void CommitIfActiveElseFlush()
        {
            this.Commit();
            
        }

        #region ITransaction Members

        void ITransaction.Begin(System.Data.IsolationLevel isolationLevel)
        {
            this.Begin(isolationLevel);
            
        }

        void ITransaction.Begin()
        {
            this.Begin(null);
            
        }

        #endregion
    }
}
