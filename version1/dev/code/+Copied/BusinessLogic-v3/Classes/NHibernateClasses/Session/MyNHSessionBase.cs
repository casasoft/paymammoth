﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using System.Data;
using NHibernate.Impl;
using BusinessLogic_v3.Classes.NHibernateClasses.Transactions;
using NHibernate.Search;
using BusinessLogic_v3.Classes.DbObjects;
using Lucene.Net.Store;
using Lucene.Net.Index;
using Lucene.Net.Documents;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.LuceneClasses.Analyzers;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.Culture;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Session
{
    public abstract class MyNHSessionBase : IDisposable
    {

        public abstract ISQLQuery CreateSqlQuery(string sql);

        public static MyNHSessionBase LoadFromSession(NHibernate.Impl.SessionImpl session)
        {
            return new MyNHSessionNormal(session);
        }
        public static MyNHSessionBase LoadFromSession(NHibernate.Impl.StatelessSessionImpl session)
        {
            
            
            return new MyNHSessionStateless(session);
        }


        private IFullTextSession _fullTextSession = null;

        /// <summary>
        /// Returns a Full Text Session, for use with Lucene queries
        /// </summary>
        /// <returns></returns>
        public IFullTextSession GetFullTextSession()
        {
            if (_fullTextSession == null)
                _fullTextSession = Search.CreateFullTextSession(this.GetAsISession());
            return _fullTextSession;


        }

        /// <summary>
        /// Manually indexes an entity in lucene
        /// </summary>
        /// <param name="entity"></param>
        public void LuceneIndex(object entity)
        {
            if (entity != null)
            {
                //LanguageAnalyzer analyzer = new LanguageAnalyzer();

                //Directory directory = FSDirectory.Open(new System.IO.DirectoryInfo(CS.General_v3.Constants.LUCENE_BASE_INDEX_LOCATION + entity.GetType().Name));
                //IndexWriter iwriter = new IndexWriter(directory, analyzer, true,
                //                                      new IndexWriter.MaxFieldLength(25000));
                //Document doc = new Document();
                //var properties = ReflectionUtil.GetAllPropertiesForType(entity.GetType(), false, false, false);
                //CultureDetailsBase culture = (CultureDetailsBase)DefaultCultureManager.Instance.GetCulture();
                //foreach (var p in properties)
                //{
                //    string stemmedText = null;
                //    if (p.PropertyType.Name == "string" || p.PropertyType.Name == "String")
                //    {
                //        string text = (string)
                //                      ReflectionUtil.GetPropertyValueByName(entity, p.Name, typeof(string));
                //        stemmedText = analyzer.AnalyseString(text, culture);
                        
                //    }
                //    else if(p.PropertyType.Name == "Int64" || p.PropertyType.Name=="int64")
                //    {
                //        stemmedText =
                //            ((long) ReflectionUtil.GetPropertyValueByName(entity, p.Name, typeof (long))).ToString();
                //    }

                //    if (!string.IsNullOrWhiteSpace(stemmedText))
                //    {
                //        doc.Add(new Field(p.Name, stemmedText,
                //                          Field.Store.YES, Field.Index.ANALYZED));
                //    }
                //}
                //iwriter.AddDocument(doc);
                //iwriter.Close();

                var fullTextSession = GetFullTextSession();
                fullTextSession.Index(entity);
            }
        }
        /// <summary>
        /// Removes the entity from lucene
        /// </summary>
        /// <param name="entity"></param>
        public void LucenePurge(IBaseDbObject entity)
        {
            if (entity != null)
            {
                var fullTextSession = GetFullTextSession();
                fullTextSession.Purge(entity.GetType(), entity.ID);
            }

        }
        /// <summary>
        /// Removes all entity information of the specified type from the Lucene index
        /// </summary>
        /// <param name="t"></param>
        public void LucenePurgeAll(Type t)
        {
            if (t != null)
            {
                var fullTextSession = GetFullTextSession();
                fullTextSession.PurgeAll(t);
            }

        }





        //public bool AutoBeginTransaction { get; set; }
        public abstract IMultiCriteria CreateMultiCriteria();

        public abstract IMultiQuery CreateMultiQuery();
        public abstract IQuery CreateQuery(string queryString);
        public abstract IQuery CreateQuery(IQueryExpression queryExpression);
        protected AbstractSessionImpl _session { get; private set; }
        public MyNHSessionBase(AbstractSessionImpl session)
        {
            
            this._session = session;
            
            checkTransaction();


        }


        public abstract ITransaction Transaction { get; }

        protected void checkTransaction()
        {

            
        }

        public void RollbackCurrentTransaction()
        {
            if (Transaction != null && Transaction.IsActive)
            {
                Transaction.Rollback();

                checkTransaction();
                
            }

            
        }
        public void CommitCurrentTransaction(bool beginAnotherTransaction = true)
        {
            if (Transaction != null && Transaction.IsActive)
            {

                Transaction.Commit();
                if (beginAnotherTransaction)
                    checkTransaction();
            }


        }

        public abstract IDbConnection Connection { get; }

        public abstract IQueryOver<T, T> QueryOver<T>() where T : class;
        public abstract void Lock(object obj, LockMode lockMode);
        public abstract void Lock(string entityName, object obj, LockMode lockMode);
        public abstract bool IsOpen { get; }
        public abstract void Close();
        public abstract void Flush();
        public abstract void Clear();

        #region IDisposable Members

        protected abstract void _dispose();
        


        public void Dispose()
        {
            //if (this.AutoBeginTransaction && this.Transaction != null && this.Transaction.IsActive)
            //{
            //    CommitCurrentTransaction(beginAnotherTransaction: false);
            //}
            if (this.Transaction != null)
                this.Transaction.Dispose();
            
            _dispose();
        }


        public abstract ISession GetAsISession();
        public abstract IStatelessSession GetAsIStatelessSession();
        #endregion
        public abstract ICriteria CreateCriteria(string entityName, string alias);

        public abstract ICriteria CreateCriteria(string entityName);

        public abstract ICriteria CreateCriteria(Type persistentClass, string alias);
        public abstract ICriteria CreateCriteria(Type persistentClass);

        public abstract ICriteria CreateCriteria<T>(string alias)where T : class;

        

        public abstract ICriteria CreateCriteria<T>() where T : class;

        public abstract T Get<T>(object id);
        public abstract T Get<T>(object id, LockMode lockMode);
        public abstract object Get(string entityName, object id);
        public abstract object Get(Type clazz, object id, LockMode lockMode);

        public abstract object Get(Type clazz, object id);


        public abstract void Refresh(object o);

        public abstract void Delete(object o);

        public abstract object Save(object o);
        public abstract void Update(object o);
        public abstract void Evict(object o);

        [Obsolete("Use Merge(object) instead")]
        public abstract object SaveOrUpdateCopy(object o);
        public abstract object Merge(object o);


        public abstract void SaveOrUpdate(object o);


        public MyTransaction BeginTransaction()
        {
           
            MyTransaction result = new MyTransaction(this);
            result.Begin();
            return result;
        }
        public MyTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            //var nhTransaction = _beginTransaction(isolationLevel);
            MyTransaction result = new MyTransaction(this);
            result.Begin(isolationLevel);
            return result;
        }
        protected internal abstract ITransaction _beginTransaction();
        protected internal abstract ITransaction _beginTransaction(IsolationLevel isolationLevel);
    }
}
