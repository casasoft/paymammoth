﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Impl;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Session
{
    public class MyNHSessionNormal : MyNHSessionBase, ISession
    {
       
        private NHibernate.Impl.SessionImpl s
        {
            get
            {
                return _session;
            }
        }
        private NHibernate.Impl.SessionImpl _nhSession
        {
            get
            {
                return _session;
            }
        }
        protected new NHibernate.Impl.SessionImpl _session
        {
            get { return (SessionImpl) base._session; }
        }
        public MyNHSessionNormal(NHibernate.Impl.SessionImpl nhSession) : base(nhSession)
        {
            
            //this._nhSession = nhSession;
        }
        


        #region ISession Members

        public EntityMode ActiveEntityMode
        {
            get { return this._nhSession.ActiveEntityMode; }
        }

        protected internal override ITransaction _beginTransaction(System.Data.IsolationLevel isolationLevel)
        {
            return this._nhSession.BeginTransaction(isolationLevel);
            
        }

        protected internal override ITransaction _beginTransaction()
        {
            return this._nhSession.BeginTransaction();
            
        }

        public CacheMode CacheMode
        {
            get
            {
                return this._nhSession.CacheMode;
                
            }
            set
            {
                this._nhSession.CacheMode = value;
                
            }
        }

        public void CancelQuery()
        {
            _nhSession.CancelQuery();
            
        }

        public override void Clear()
        {
            _nhSession.Clear();
            
        }

        public override void Close()
        {
            
            _nhSession.Close();
            
        }

        public override System.Data.IDbConnection Connection
        {
            get { return _nhSession.Connection; }
        }

        public bool Contains(object obj)
        {
            return _nhSession.Contains(obj);
            
        }

        public override  ICriteria CreateCriteria(string entityName, string alias)
        {
            return _nhSession.CreateCriteria(entityName, alias);
            
        }

        public  override ICriteria CreateCriteria(string entityName)
        {
            return _nhSession.CreateCriteria(entityName);
        }

        public override  ICriteria CreateCriteria(Type persistentClass, string alias)
        {
            return _nhSession.CreateCriteria(persistentClass, alias);
        }

        public override  ICriteria CreateCriteria(Type persistentClass)
        {
            return _nhSession.CreateCriteria(persistentClass);
        }

        public  override ICriteria CreateCriteria<T>(string alias)
        {
            return _nhSession.CreateCriteria<T>(alias);
        }

        public override ICriteria CreateCriteria<T>()
        {
            return _nhSession.CreateCriteria<T>();
        }

        public IQuery CreateFilter(object collection, string queryString)
        {
            return _nhSession.CreateFilter(collection, queryString);
        }

        public override IMultiCriteria CreateMultiCriteria()
        {
            return _nhSession.CreateMultiCriteria();
        }

        public override IMultiQuery CreateMultiQuery()
        {
            return _nhSession.CreateMultiQuery();
        }

        public override IQuery CreateQuery(string queryString)
        {
            return _nhSession.CreateQuery(queryString);
        }

        public ISQLQuery CreateSQLQuery(string queryString)
        {
            return _nhSession.CreateSQLQuery(queryString);
        }

        public bool DefaultReadOnly
        {
            get
            {
                return _nhSession.DefaultReadOnly;
                
            }
            set
            {
                _nhSession.DefaultReadOnly = value;
                
            }
        }

        public int Delete(string query, object[] values, NHibernate.Type.IType[] types)
        {
            return _nhSession.Delete(query, values, types);
        }

        public int Delete(string query, object value, NHibernate.Type.IType type)
        {
            return _nhSession.Delete(query, value, type);
        }

        public int Delete(string query)
        {
            return _nhSession.Delete(query);
        }

        public void Delete(string entityName, object obj)
        {
            _nhSession.Delete(entityName ,obj );
        }

        public override void Delete(object obj)
        {
            _nhSession.Delete(obj );
        }

        public void DisableFilter(string filterName)
        {
             _nhSession.DisableFilter(filterName);
        }

        public System.Data.IDbConnection Disconnect()
        {
            return _nhSession.Disconnect();
        }

        public IFilter EnableFilter(string filterName)
        {
            return _nhSession.EnableFilter(filterName);
        }

        public override void Evict(object obj)
        {
            _nhSession.Evict(obj);
            
            
        }

        public override void Flush()
        {
            
            try
            {
                s.Flush();
            }
            catch (Exception ex)
            {
                throw;
            }
            
        }

        public FlushMode FlushMode
        {
            get
            {
                return s.FlushMode;
                
            }
            set
            {
                s.FlushMode = value;
                
            }
        }

        public override T Get<T>(object id, LockMode lockMode)
        {
            return s.Get<T>(id, lockMode);
            
        }

        public override T Get<T>(object id)
        {
            return s.Get<T>(id);
            
        }

        public override object Get(string entityName, object id)
        {
            
            return s.Get(entityName, id);
            
        }

        public override object Get(Type clazz, object id, LockMode lockMode)
        {
            return s.Get(clazz, id, lockMode);
            
        }

        public override object Get(Type clazz, object id)
        {
            return s.Get(clazz, id);
            
        }

        public LockMode GetCurrentLockMode(object obj)
        {
            return s.GetCurrentLockMode(obj);
            
        }

        public IFilter GetEnabledFilter(string filterName)
        {
            return s.GetEnabledFilter(filterName);
        }

        public string GetEntityName(object obj)
        {
            return s.GetEntityName(obj);
            
        }

        public object GetIdentifier(object obj)
        {
            return s.GetIdentifier(obj);
        }

        public IQuery GetNamedQuery(string queryName)
        {
            return s.GetNamedQuery(queryName);
        }

        public ISession GetSession(EntityMode entityMode)
        {
            return s.GetSession(entityMode);
        }

        public NHibernate.Engine.ISessionImplementor GetSessionImplementation()
        {
            return s.GetSessionImplementation();
        }

        public bool IsConnected
        {
            get { return s.IsConnected; }
        }

        public bool IsDirty()
        {
            return s.IsDirty();
        }

        public override bool IsOpen
        {
            get { return s.IsOpen; }
        }

        public bool IsReadOnly(object entityOrProxy)
        {
            return s.IsReadOnly(entityOrProxy);
        }

        public void Load(object obj, object id)
        {
            s.Load(obj, id);
        }

        public object Load(string entityName, object id)
        {
            return s.Load(entityName, id);
        }

        public T Load<T>(object id)
        {
            return s.Load<T>(id);
        }

        public T Load<T>(object id, LockMode lockMode)
        {
            return s.Load<T>(id, lockMode);
        }

        public object Load(Type theType, object id)
        {
            return s.Load(theType, id);
        }

        public object Load(string entityName, object id, LockMode lockMode)
        {
            return s.Load(entityName, id, lockMode);
        }

        public object Load(Type theType, object id, LockMode lockMode)
        {
            return s.Load(theType, id,lockMode);
        }

        public override void Lock(string entityName, object obj, LockMode lockMode)
        {
            s.Lock(entityName, obj, lockMode);
        }

        public override void Lock(object obj, LockMode lockMode)
        {
            s.Lock(obj, lockMode);
        }

        public object Merge(string entityName, object obj)
        {
            return s.Merge(entityName, obj);
        }

        public override object Merge(object obj)
        {
            return s.Merge(obj);
        }

        public void Persist(string entityName, object obj)
        {
            s.Persist(entityName, obj);
        }

        public void Persist(object obj)
        {
            s.Persist(obj);
        }
        

        public IQueryOver<T, T> QueryOver<T>(System.Linq.Expressions.Expression<Func<T>> alias) where T : class
        {
            return s.QueryOver<T>(alias);
        }

        public override IQueryOver<T, T> QueryOver<T>() 
        {
            return s.QueryOver<T>();
        }

        public void Reconnect(System.Data.IDbConnection connection)
        {
            s.Reconnect(connection);
        }

        public void Reconnect()
        {
            s.Reconnect();
        }

        public void Refresh(object obj, LockMode lockMode)
        {
            s.Refresh(obj, lockMode);
        }

        public override void Refresh(object obj)
        {
            try
            {
                s.Refresh(obj);
            }
            catch (HibernateException ex)
            {
                int k = 5;
                throw;
            }
        }

        public void Replicate(string entityName, object obj, ReplicationMode replicationMode)
        {
            s.Replicate(entityName, obj, replicationMode);
        }

        public void Replicate(object obj, ReplicationMode replicationMode)
        {
            s.Replicate(obj, replicationMode);
        }

        public object Save(string entityName, object obj)
        {
            return s.Save(entityName, obj);
        }

        public void Save(object obj, object id)
        {
            s.Save(obj, id);
        }

        public override object Save(object obj)
        {
            return s.Save(obj);
        }

        public void SaveOrUpdate(string entityName, object obj)
        {
            s.SaveOrUpdate(entityName, obj);
        }

        public override void SaveOrUpdate(object obj)
        {
            s.SaveOrUpdate(obj);
        }

        public object SaveOrUpdateCopy(object obj, object id)
        {
            return s.SaveOrUpdateCopy(obj, id);
        }

        public override object SaveOrUpdateCopy(object obj)
        {
            return s.SaveOrUpdateCopy(obj);
        }

        public ISessionFactory SessionFactory
        {
            get { return s.SessionFactory; }
        }

        public ISession SetBatchSize(int batchSize)
        {
            return s.SetBatchSize(batchSize);
        }

        public void SetReadOnly(object entityOrProxy, bool readOnly)
        {
            s.SetReadOnly(entityOrProxy, readOnly);
        }

        public NHibernate.Stat.ISessionStatistics Statistics
        {
            get { return s.Statistics; }
        }

        public override ITransaction Transaction
        {
            get { return s.Transaction;}
        }

        public void Update(string entityName, object obj)
        {
            s.Update(entityName, obj);
        }

        public void Update(object obj, object id)
        {
            s.Update(obj, id);
        }

        public override void Update(object obj)
        {
            s.Update(obj);

        }

        #endregion

        #region IDisposable Members

        protected override void _dispose()
        {
            s.Dispose();
            
            
        }

        #endregion

        #region ISession Members


        public override IQuery CreateQuery(IQueryExpression queryExpression)
        {
            return _session.CreateQuery(queryExpression);
            
        }

        #endregion

        public override ISession GetAsISession()
        {
            return _session;
            
        }

        public override IStatelessSession GetAsIStatelessSession()
        {
            throw new NotImplementedException("Cannot convert a ISession to an IStateless session");
        }

        #region ISession Members


        System.Data.IDbConnection ISession.Close()
        {
            return _nhSession.Close();
            
        }

        #endregion

        #region ISession Members


        public T Merge<T>(string entityName, T entity) where T : class
        {
            return _session.Merge<T>(entityName, entity);
            
        }

        public T Merge<T>(T entity) where T : class
        {
            return _session.Merge<T>(entity);
            
        }

        public IQueryOver<T, T> QueryOver<T>(string entityName, System.Linq.Expressions.Expression<Func<T>> alias) where T : class
        {
            return _session.QueryOver<T>(entityName, alias);
            
        }

        public IQueryOver<T, T> QueryOver<T>(string entityName) where T : class
        {
            return _session.QueryOver<T>(entityName);
            
        }

        #endregion

        #region ISession Members

        ITransaction ISession.BeginTransaction(System.Data.IsolationLevel isolationLevel)
        {
            return _beginTransaction(isolationLevel);
            
        }

        ITransaction ISession.BeginTransaction()
        {
            return _beginTransaction();
            
        }

        #endregion

        public override ISQLQuery CreateSqlQuery(string sql)
        {
            return this._nhSession.CreateSQLQuery(sql);
            
        }
    }
}
