﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Session
{
    public class LazyStatelessSession : BaseLazySession, IDisposable
    {
        protected override MyNHSessionBase createSession()
        {
            return NHClasses.NhManager.CreateNewStatelessSession();
        }
        public new MyNHSessionStateless GetSession()
        {
            return (MyNHSessionStateless)base.GetSession();
        }
    }
}
