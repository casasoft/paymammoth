﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Session
{
    public abstract class BaseLazySession : IDisposable
    {
        
       // public ITransaction Transaction { get; private set; }

        protected bool _createdSession = false;
        private MyNHSessionBase _session = null;
        protected abstract MyNHSessionBase createSession();
        private readonly object padlock = new object();
        public bool IsSessionCreated()
        {
            return _createdSession;
        }

        public MyNHSessionBase GetSession(bool createIfNotCreated = true)
        {



            if (!_createdSession && createIfNotCreated)
            {
                lock (padlock)
                {
                    if (!_createdSession)
                    {
                        _session = createSession();
                      

                        _createdSession = true;
                    }
                }
                
            }
            return _session;

        }
        //public bool AutoBeginTransaction { get; set; }
        public BaseLazySession()
        {
            //this.AutoBeginTransaction = autoBeginTransaction;
        }

        #region IDisposable Members
        


        public void Dispose(bool throwErrorIfSessionDoesNotExistOrDisposed = true)
        {
            lock (padlock)
            {
                //Commit();
                if (_createdSession)
                {
                    if (!_session.IsOpen && throwErrorIfSessionDoesNotExistOrDisposed)
                        throw new InvalidOperationException(this.GetType().FullName + " - Session already closed / disposed!");
                   

                    
                    _session.Dispose();
                    _session = null;
                }
            }

        }

        #endregion

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            this.Dispose();
            
        }

        #endregion
    }
}
