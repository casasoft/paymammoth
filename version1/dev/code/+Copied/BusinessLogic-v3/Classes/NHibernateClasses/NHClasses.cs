﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.NHConfiguration;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using BusinessLogic_v3.Classes.NHibernateClasses.SessionContextManagers;

namespace BusinessLogic_v3.Classes.NHibernateClasses
{
    public static class NHClasses
    {
        /// <summary>
        /// This enables NHibernate to load collections / entities in batch, rather than 1 by 1.
        /// </summary>
        public const int NH_BATCH_SIZE = 250;
        static NHClasses()
        {
            initialise();
        }
        public static INHConfig NHConfig { get; set; }
        public static ISessionContextManager GetSessionManager()
        {
            return SessionContextUtil.GetSessionContextManagerBasedOnContext();
            
        }

        public static INhManager NhManager { get; set; }
         
        private static void initialise()
        {
            
            NHConfig = StandardNHConfig.CreateLastInstance();
            switch (CS.General_v3.Settings.Database.DatabaseType)
            {

                case CS.General_v3.Settings.Database.DATABASE_TYPE.MySQL:
                    {
                        NhManager = StandardNhManager.Instance;
                        
                    }
                    break;
                case CS.General_v3.Settings.Database.DATABASE_TYPE.SQLite:
                    {
                        NhManager = StandardNhManager.Instance;
                        NHConfig = StandardNHConfig.CreateLastInstance();
                        
                    }
                    break;
                default:
                    throw new InvalidOperationException("Invalid data type");
            }


        }

    }
}
