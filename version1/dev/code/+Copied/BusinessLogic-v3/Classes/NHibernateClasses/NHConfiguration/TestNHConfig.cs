﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.NHConfiguration;
using CS.General_v3.Extensions;

using System.Reflection;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using log4net;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MySql.Data.MySqlClient;
using BusinessLogic_v3.Classes.NHibernateClasses.DBConfig;

namespace BusinessLogic_v3.Classes.NHibernateClasses
{
    public class TestNHConfig : BaseNHConfig, INHConfig
    {
        public static TestNHConfig CreateLastInstance()
        {
            return new TestNHConfig();
            
        }

        private const string SERIALIZED_CFG_PATH = "/App_Data/nhSavedMappings/nh_config.test.bin";


        
        private TestNHConfig()
        {
            
        }

        private TestNHConfig(Iesi.Collections.Generic.HashedSet<Type> mappedTypes, Iesi.Collections.Generic.HashedSet<Assembly> mappedAssemblies)
            : base(mappedTypes, mappedAssemblies)
        {
            
            
        }


        protected override string getSerializedConfigPath()
        {
            return SERIALIZED_CFG_PATH;
            
        }
    }
}
