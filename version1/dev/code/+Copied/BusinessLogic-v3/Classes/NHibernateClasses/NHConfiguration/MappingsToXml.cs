﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using NHibernate.Cfg.MappingSchema;
using System.Reflection;

namespace BusinessLogic_v3.Classes.NHibernateClasses.NHConfiguration
{
    public class MappingsToXml
    {
        private XmlDocument doc = null;
        private XmlNode rootNode = null;
        public MappingsToXml()
        {
            doc = new XmlDocument();
            rootNode = doc.CreateElement("Mappings");

        }

        public void SaveFile(string path)
        {
            doc.Save(path);
        }

        private XmlNode createElement(XmlNode parent, string name)
        {
            var node = doc.CreateElement(name);
            parent.AppendChild(node);
            return node;
        }
        private void AddString(XmlNode node, string label, string value)
        {
            var newNode = createElement(node, label);
            newNode.InnerText = value;

        }
        private void AddString<T>(XmlNode node, T item, System.Linq.Expressions.Expression<Func<T,object>> selector)
        {
            PropertyInfo pInfo = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(selector);
            object value = pInfo.GetValue(item, null);
            string s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value, ifEnumConvertToIntValue: false);
            AddString(node, pInfo.Name, s);

        }
        public void AddMappings(IEnumerable<HbmMapping> mappings)
        {
            foreach (var m in mappings)
            {
                AddMapping(m);
            }

        }

        public void AddMapping(HbmMapping item)
        {
            XmlNode root = createElement(rootNode, "Mapping");
            foreach (var obj in item.Items)
            {
                if (obj is HbmClass)
                {
                    AddClass((HbmClass)obj, root);
                }
                else
                {
                    
                    AddString(root, "UnknownItem", obj != null ? obj.ToString() : "NULL");

                }
            }


        }
        public void AddClass(HbmClass item, XmlNode parent)
        {
            XmlNode root = createElement(parent, "Class");
            AddString(root, item, x => x.@abstract);
            AddString(root, item, x => x.abstractSpecified);
            AddString(root, item, x => x.batchsize);
            AddString(root, item, x => x.BatchSize);
            AddString(root, item, x => x.batchsizeSpecified);
            AddString(root, item, x => x.cache);
            AddString(root, item, x => x.catalog);
            AddString(root, item, x => x.check);
            AddString(root, item, x => x.comment);
            AddString(root, item, x => x.CompositeId);
            AddString(root, item, x => x.discriminator);
            AddString(root, item, x => x.discriminatorvalue);
            AddString(root, item, x => x.DiscriminatorValue);
            AddString(root, item, x => x.dynamicinsert);
            AddString(root, item, x => x.DynamicInsert);
            AddString(root, item, x => x.dynamicupdate);
            AddString(root, item, x => x.DynamicUpdate);
            AddString(root, item, x => x.entityname);
            AddString(root, item, x => x.EntityName);
            AddString(root, item, x => x.filter);
            AddString(root, item, x => x.Id);
            AddString(root, item, x => x.InheritableMetaData);
            AddString(root, item, x => x.IsAbstract);
            AddString(root, item, x => x.Item);
            AddString(root, item, x => x.lazy);
            AddString(root, item, x => x.lazySpecified);
            AddString(root, item, x => x.loader);
            AddString(root, item, x => x.meta);
            AddString(root, item, x => x.mutable);
            AddString(root, item, x => x.name);
            AddString(root, item, x => x.Name);
            AddString(root, item, x => x.naturalid);
            AddString(root, item, x => x.node);
            AddString(root, item, x => x.Node);
            AddString(root, item, x => x.optimisticlock);
            AddString(root, item, x => x.persister);
            AddString(root, item, x => x.Persister);
            AddString(root, item, x => x.polymorphism);
            AddString(root, item, x => x.Properties);
            AddString(root, item, x => x.proxy);
            AddString(root, item, x => x.Proxy);
            AddString(root, item, x => x.resultset);
            AddString(root, item, x => x.rowid);
            AddString(root, item, x => x.schema);
            AddString(root, item, x => x.schemaaction);
            AddString(root, item, x => x.selectbeforeupdate);
            AddString(root, item, x => x.SelectBeforeUpdate);
            AddString(root, item, x => x.sqldelete);
            AddString(root, item, x => x.SqlDelete);
            AddString(root, item, x => x.SqlInsert);
            AddString(root, item, x => x.SqlLoader);
            AddString(root, item, x => x.SqlUpdate);
            AddString(root, item, x => x.Subselect);
            AddString(root, item, x => x.Synchronize);
            AddString(root, item, x => x.table);
            AddString(root, item, x => x.Timestamp);
            AddString(root, item, x => x.UseLazy);
            AddString(root, item, x => x.Version);
            AddString(root, item, x => x.where);
            

            foreach (var obj in item.Items)
            {
                if (obj is HbmProperty)
                {
                    addProperty((HbmProperty) obj, root);
                }
                else if (obj is HbmSet)
                {
                    addSet((HbmSet)obj, root);

                }
                else if (obj is HbmList)
                {

                }
                else if (obj is HbmBag)
                {

                }
                else
                {
                    AddString(parent, "UnknownItem", obj != null ? obj.ToString() : "NULL");

                }
            }
        }

        private void addSet(HbmSet item, XmlNode parent)
        {
            XmlNode root = createElement(parent, "Set");
            AddString(root, item, x => x.access);
            AddString(root, item, x => x.batchsize);
            AddString(root, item, x => x.Access);
            AddString(root, item, x => x.BatchSize);
            AddString(root, item, x => x.batchsizeSpecified);
            AddString(root, item, x => x.cache);
            AddString(root, item, x => x.Cache);
            AddString(root, item, x => x.cascade);
            AddString(root, item, x => x.Cascade);
            AddString(root, item, x => x.catalog);
            AddString(root, item, x => x.Catalog);
            AddString(root, item, x => x.check);
            AddString(root, item, x => x.Check);
            AddString(root, item, x => x.collectiontype);
            AddString(root, item, x => x.CollectionType);
            AddString(root, item, x => x.comment);
            AddString(root, item, x => x.ElementRelationship);
            AddString(root, item, x => x.embedxml);
            AddString(root, item, x => x.fetch);
            AddString(root, item, x => x.fetchSpecified);
            AddString(root, item, x => x.FetchMode);
            AddString(root, item, x => x.filter);
            AddString(root, item, x => x.Filters);
            AddString(root, item, x => x.generic);
            AddString(root, item, x => x.Generic);
            AddString(root, item, x => x.genericSpecified);
            AddString(root, item, x => x.InheritableMetaData);
            AddString(root, item, x => x.inverse);
            AddString(root, item, x => x.Inverse);
            AddString(root, item, x => x.IsLazyProperty);
            AddString(root, item, x => x.Item);
            AddString(root, item, x => x.key);
            AddString(root, item, x => x.Key);
            AddString(root, item, x => x.lazy);
            AddString(root, item, x => x.Lazy);
            AddString(root, item, x => x.lazySpecified);
            AddString(root, item, x => x.loader);
            AddString(root, item, x => x.MappedMetaData);
            AddString(root, item, x => x.meta);
            AddString(root, item, x => x.mutable);
            AddString(root, item, x => x.Mutable);
            AddString(root, item, x => x.name);
            AddString(root, item, x => x.Name);
            AddString(root, item, x => x.node);
            AddString(root, item, x => x.optimisticlock);
            AddString(root, item, x => x.OptimisticLock);
            AddString(root, item, x => x.orderby);
            AddString(root, item, x => x.OrderBy);
            AddString(root, item, x => x.OuterJoin);
            AddString(root, item, x => x.outerjoin);
            AddString(root, item, x => x.outerjoinSpecified);
            AddString(root, item, x => x.persister);
            AddString(root, item, x => x.PersisterQualifiedName);
            AddString(root, item, x => x.schema);
            AddString(root, item, x => x.Schema);
            AddString(root, item, x => x.sort);
            AddString(root, item, x => x.Sort);
            AddString(root, item, x => x.sqldelete);
            AddString(root, item, x => x.SqlDelete);
            AddString(root, item, x => x.sqldeleteall);
            AddString(root, item, x => x.SqlDeleteAll);
            AddString(root, item, x => x.sqlinsert);
            AddString(root, item, x => x.SqlInsert);
            AddString(root, item, x => x.SqlLoader);
            AddString(root, item, x => x.sqlupdate);
            AddString(root, item, x => x.SqlUpdate);
            AddString(root, item, x => x.subselect);
            AddString(root, item, x => x.Subselect);
            AddString(root, item, x => x.subselect1);
            AddString(root, item, x => x.synchronize);
            AddString(root, item, x => x.table);
            AddString(root, item, x => x.Table);
            AddString(root, item, x => x.where);
            AddString(root, item, x => x.Where);
            
        }
        private void addProperty(HbmProperty item, XmlNode parent)
        {
            XmlNode root = createElement(parent, "Property");
            AddString(root, item, x => x.Access);
            AddString(root, item, x => x.Columns);
            AddString(root, item, x => x.column);
            AddString(root, item, x => x.formula);
            AddString(root, item, x => x.generated);
            AddString(root, item, x => x.Formulas);
            AddString(root, item, x => x.index);
            AddString(root, item, x => x.InheritableMetaData);
            AddString(root, item, x => x.insert);
            AddString(root, item, x => x.insertSpecified);
            AddString(root, item, x => x.IsLazyProperty);
            AddString(root, item, x => x.lazy);
            AddString(root, item, x => x.length);
            AddString(root, item, x => x.MappedMetaData);
            AddString(root, item, x => x.meta);
            AddString(root, item, x => x.name);
            AddString(root, item, x => x.Name);
            AddString(root, item, x => x.node);
            AddString(root, item, x => x.notnull);
            AddString(root, item, x => x.notnullSpecified);
            AddString(root, item, x => x.optimisticlock);
            AddString(root, item, x => x.OptimisticLock);
            AddString(root, item, x => x.precision);
            AddString(root, item, x => x.scale);
            AddString(root, item, x => x.type);
            AddString(root, item, x => x.Type);
            AddString(root, item, x => x.unique);
            AddString(root, item, x => x.type1);
            AddString(root, item, x => x.uniquekey);
            AddString(root, item, x => x.update);
            AddString(root, item, x => x.updateSpecified);
            
            
        }

    }
}
