﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using BusinessLogic_v3.Classes.Application;
using BusinessLogic_v3.Classes.NHibernateClasses.DBConfig;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings.LuceneMapping;
using log4net;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;
using NHibernate.Search.Event;
using NHibernate.Tool.hbm2ddl;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Search.Store;
using BusinessLogic_v3.Classes.NHibernateClasses.Listeners;
using NHibernate.Event;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Mapping.ByCode;

namespace BusinessLogic_v3.Classes.NHibernateClasses.NHConfiguration
{
    public abstract class BaseNHConfig
    {
        protected ILog _log = null;
        public BaseNHConfig()
        {
           _mappedTypes = new Iesi.Collections.Generic.HashedSet<Type>();
            init();
        }
        private void init()
        {
            _log = log4net.LogManager.GetLogger(this.GetType());
            
        }

        public BaseNHConfig(Iesi.Collections.Generic.HashedSet<Type> mappedTypes, Iesi.Collections.Generic.HashedSet<Assembly> mappedAssemblies)
        {
            _mappedTypes = mappedTypes;
            init();
            
        }
        protected abstract string getSerializedConfigPath();
        
        private Iesi.Collections.Generic.HashedSet<Type> _mappedTypes = null;
        /// <summary>
        /// Returns whether configuration exists already and is valid, or needs to be rebuilt (non existant or 'CheckMappings' is true)
        /// </summary>
        /// <returns></returns>
        public  bool CheckIfConfigurationExistsAndValid()
        {
            bool valid = false;
            string localPath = CS.General_v3.Util.PageUtil.MapPath(getSerializedConfigPath());
            if (!CS.General_v3.Settings.Database.CheckMappingsAndOthers && File.Exists(localPath))
            {
                valid = true;
            }
            return valid;

        }

        public void SetMappings(IEnumerable<Type> mappedTypes)
        {
            if (mappedTypes != null)
            {
                _mappedTypes.AddAll(mappedTypes);
            }
           
        }


        private void initialiseListeners(Configuration cfg)
        {


            List<IPostUpdateEventListener> postUpdateListeners = new List<IPostUpdateEventListener>();
            List<IPreUpdateEventListener> preUpdateListeners = new List<IPreUpdateEventListener>();
            List<IPreDeleteEventListener> preDeleteListeners = new List<IPreDeleteEventListener>();
            List<IPostDeleteEventListener> postDeleteListeners = new List<IPostDeleteEventListener>();
            List<IPostInsertEventListener> postInsertListeners = new List<IPostInsertEventListener>();
            List<IPreInsertEventListener> preInsertListeners = new List<IPreInsertEventListener>();

            //if (false)
            {
                var listener = new FullTextIndexEventListener();
                postUpdateListeners.Add(listener);
                postDeleteListeners.Add(listener);
                postInsertListeners.Add(listener);
            }
            //if (false)
            {
                var listener = new FactoryEventCallerListener();
                postUpdateListeners.Add(listener);
                postDeleteListeners.Add(listener);
                postInsertListeners.Add(listener);
            }
            {
                var listener = new CrudEventListeners();
                preDeleteListeners.Add(listener);
            }

            //2012/feb/08 - this was swtiched off as it seems better using an interceptor
            //{
            //    var listener = new AuditLogListener();
            //    preUpdateListeners.Add(listener);
            //    postDeleteListeners.Add(listener);
            //    preInsertListeners.Add(listener);
            //}





            cfg.AppendListeners(NHibernate.Event.ListenerType.PostUpdate, postUpdateListeners.ToArray());
            cfg.AppendListeners(NHibernate.Event.ListenerType.PostInsert, postInsertListeners.ToArray());
            cfg.AppendListeners(NHibernate.Event.ListenerType.PostDelete, postDeleteListeners.ToArray());
            cfg.AppendListeners(NHibernate.Event.ListenerType.PreUpdate, preUpdateListeners.ToArray());
            cfg.AppendListeners(NHibernate.Event.ListenerType.PreInsert, preInsertListeners.ToArray());
            cfg.AppendListeners(NHibernate.Event.ListenerType.PreDelete, preDeleteListeners.ToArray());

            //}
            //{
            //    AuditLogListener auditLogListener = new AuditLogListener();
            //    cfg.SetListener(NHibernate.Event.ListenerType.PostUpdate, auditLogListener);
            //    cfg.SetListener(NHibernate.Event.ListenerType.PostInsert, auditLogListener);
            //    cfg.SetListener(NHibernate.Event.ListenerType.PostDelete, auditLogListener);
            //}
        }

        private void initialiseLuceneNet(Configuration cfg)
        {

            //this was removed as it is being done in initialiseListeners();

            //FullTextIndexEventListener fullTextListener = new FullTextIndexEventListener();

            //cfg.SetListener(NHibernate.Event.ListenerType.PostUpdate, fullTextListener);
            //cfg.SetListener(NHibernate.Event.ListenerType.PostInsert, fullTextListener);
            //cfg.SetListener(NHibernate.Event.ListenerType.PostDelete, fullTextListener);
            
            string nhIndexPath = CS.General_v3.Util.PageUtil.MapPath(Constants.General.LUCENE_BASE_RELATIVE_INDEX_LOCATION);

            cfg.SetProperty("hibernate.search.default.directory_provider", typeof(FSDirectoryProvider).AssemblyQualifiedName);
            cfg.SetProperty("hibernate.search.default.indexBase", nhIndexPath);
            cfg.SetProperty("hibernate.search.default.indexBase.create", "true");
            //cfg.SetProperty("hibernate.search.analyzer", "Lucene.Net.Analysis.SimpleAnalyzer");
            cfg.SetProperty("hibernate.search.mapping", typeof(LuceneMapper).AssemblyQualifiedName);//this sets the lucene mapper
            


        }

        public Configuration GetConfiguration()
        {
            Configuration cfg = loadConfigurationFromFile();
            
            if (cfg == null)
            {
                cfg = createNHibernateConfig();
                _cfgToBeSaved = cfg;
                
                NotLoadedFromFile = true;
            }
            return cfg;



        }

        public bool NotLoadedFromFile { get; set; }

        protected virtual void initConfigProperties(Configuration config)
        {
            //config.Properties["current_session_context_class"] = "web";

            {
                var interceptor = AppInstance.Instance.GetNhibernateInterceptor();
                if (interceptor != null)
                {
                    config.SetInterceptor(interceptor);
                }
            }


            config = config.SetProperty(NHibernate.Cfg.Environment.UseProxyValidator, "false"); //this removes the validation for proxies, such that they all require the virtual keyword

          


            config = config.SetProperty(NHibernate.Cfg.Environment.UseSecondLevelCache, "true");

            config = config.SetProperty(NHibernate.Cfg.Environment.UseQueryCache, "true");

            config = config.SetProperty(NHibernate.Cfg.Environment.FormatSql, "true");
            config = config.SetProperty(NHibernate.Cfg.Environment.ShowSql, "false");

            config = config.SetProperty(NHibernate.Cfg.Environment.CacheProvider, typeof(NHibernate.Caches.SysCache.SysCacheProvider).AssemblyQualifiedName);
            {
                bool useReflectionOptimizer = true;
                string sUseReflectionOptimizer = (useReflectionOptimizer ? "true" : "false");
                NHibernate.Cfg.Environment.UseReflectionOptimizer = useReflectionOptimizer;
                config = config.SetProperty(NHibernate.Cfg.Environment.PropertyUseReflectionOptimizer, sUseReflectionOptimizer);
                config = config.SetProperty("hibernate.use_reflection_optimizer", sUseReflectionOptimizer);
            }
            config.Properties["relativeExpiration"] = "300";

           // _fluentDbConfig.InitConfigProperties(config);

        }

        



        protected INHibernateDbConfig getDbConfigBasedOnSettingsDatabaseType()
        {
            switch (CS.General_v3.Settings.Database.DatabaseType)
            {
                case CS.General_v3.Settings.Database.DATABASE_TYPE.MySQL: return new DBConfig.MySqlDbConfig();
                case CS.General_v3.Settings.Database.DATABASE_TYPE.SQLite: return new DBConfig.SQLiteDbConfig();
            }
            throw new InvalidOperationException("Invalid Database Type");
        }

        private void initMappings(Configuration cfg)
        {
            bool skipNormalMaps = false;
            var mapper = new ModelMapper();
            
            var typesList = _mappedTypes.ToList();
            if (!skipNormalMaps)
            {
                for (int i = 0; i < typesList.Count; i++)
                {
                    var t = typesList[i];
                    mapper.AddMapping(t);
                }
            }
            var customMappings = AppInstance.Instance._getCustomNhibernateMappings();
            if (customMappings.IsNotNullOrEmpty())
            {
                foreach (var t in customMappings)
                {
                    mapper.AddMapping(t);
                    
                }
            }
            try
            {
                
                HbmMapping compiledMapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
                if (CS.General_v3.Settings.Database.GenerateNHibernateXmlMappingFiles)
                {
                    string xml = compiledMapping.AsString();
                    string localPath = CS.General_v3.Util.PageUtil.MapPath("/App_Data/nhSavedMappings/mappings.xml");
                    CS.General_v3.Util.IO.SaveToFile(localPath, xml);
                }


                //if (true && CS.General_v3.Settings.Database. CS.General_v3.Util.Other.IsLocalTestingMachine)  
                //{
                //    string xml= General.SerializeHbmMappingToString(compiledMapping);
                //    CS.General_v3.Util.IO.SaveToFile(@"d:\test.xml", xml);

                //    //MappingsToXml xmlMapper = new MappingsToXml();
                //    //xmlMapper.AddMappings(compiledMapping.Items.Cast<HbmMapping>());
                //    //xmlMapper.SaveFile(@"d:\test.xml");
                //}
                cfg.AddMapping(compiledMapping);
            }
            catch (Exception ex)
            {
                int k = 5;
                throw;
            }
            
            

        }


        
        private Configuration createNHibernateConfig()
        {
            
            //skipNormalMaps = true;


            var cfg = new Configuration();

        

            //IPersistenceConfigurer dbCfg = null;
            var dbConfig = getDbConfigBasedOnSettingsDatabaseType();
            dbConfig.ConfigureDatabase(cfg);
           // cfg.Properties["hibernate.adonet.batch_size"] = NHClasses.NH_BATCH_SIZE.ToString();
            //cfg.Properties["hibernate.batch_size"] = NHClasses.NH_BATCH_SIZE.ToString();
            //cfg.Properties["batch_size"] = NHClasses.NH_BATCH_SIZE.ToString();

            AppInstance.Instance.onPreNHibernateConfigMapping(cfg);

            //dbCfg = _fluentDbConfig.GetDbConfiguration();

           // cfg = cfg.Database(dbCfg);
            initConfigProperties(cfg);
            initMappings(cfg);
            initialiseLuceneNet(cfg);
            initialiseListeners(cfg);
            

            return cfg;
        }

        public virtual void CreateSchema(Configuration config)
        {

            SchemaExport schema = new SchemaExport(config);
            schema.Execute(true, true, false);


        }


        private Configuration loadConfigurationFromFile()
        {
            Configuration cfg = null;
            string localPath = CS.General_v3.Util.PageUtil.MapPath(getSerializedConfigPath());
            if (!CS.General_v3.Settings.Database.CheckMappingsAndOthers) //dont check mappings if possible
            {
                cfg = CS.General_v3.Util.ReflectionUtil.DeserializeObjectFromFile<Configuration>(localPath);
            }
            else
            {
                CS.General_v3.Util.IO.DeleteFile(localPath);
            }

            return cfg;

        }

        private Configuration _cfgToBeSaved = null;

        private void saveConfigurationToFile()
        {
            Configuration config = _cfgToBeSaved;
            string localPath = CS.General_v3.Util.PageUtil.MapPath(getSerializedConfigPath());
            if (!CS.General_v3.Settings.Database.CheckMappingsAndOthers)
            {
                CS.General_v3.Util.ReflectionUtil.SerializeObjectToFile(config, localPath);
            }
            _cfgToBeSaved = null;
        }

        public void SaveConfigurationToFile(Configuration config)
        {
            if (NotLoadedFromFile)
            {
                _cfgToBeSaved = config;
                saveConfigurationToFile();
            }
        }

    }
}
