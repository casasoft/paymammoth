﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using NHibernate.Cfg;

namespace BusinessLogic_v3.Classes.NHibernateClasses.NHConfiguration
{
    public interface INHConfig
    {
        
        void SetMappings(IEnumerable<Type> mappedTypes);
        Configuration GetConfiguration();
        void CreateSchema(Configuration config);
        void SaveConfigurationToFile(Configuration config);
        bool CheckIfConfigurationExistsAndValid();
    }
}
