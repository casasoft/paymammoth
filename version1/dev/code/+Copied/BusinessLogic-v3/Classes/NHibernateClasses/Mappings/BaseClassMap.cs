﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings.LuceneMapping;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings
{
    public abstract class BaseClassMap<TData> : ClassMapping<TData>
        where TData : BaseDbObject
    {
        public virtual Enums.NHIBERNATE_CACHE_TYPE CacheType { get; set; }

        public string TableName { get; set; }

        private bool _performDynamicUpdates = true;

        /// <summary>
        /// If true, updates are only done on changed properties.
        /// </summary>
        public bool PerformDynamicUpdates
        {
            get { return _performDynamicUpdates; }
            set { _performDynamicUpdates = value; }
        }


        protected virtual void initMappings()
        {

            this.Id(x => x.ID, IdMappingInfo);
            
            addProperty(x => x._Temporary_LastUpdOn, _Temporary_LastUpdOnMappingInfo);
            addProperty(x => x._Temporary_Flag, _Temporary_FlagMappingInfo);
            addProperty(x => x.Published, PublishedMappingInfo);
            addProperty(x => x.PublishedOn, PublishedOnMappingInfo);
            addProperty(x => x.Deleted, DeletedMappingInfo);
            addProperty(x => x._ComputedDeletedValue, _ComputedDeletedValueMappingInfo);
            addProperty(x => x.DeletedOn, DeletedOnMappingInfo);
            addProperty(x => x.Priority, PriorityMappingInfo);
            addProperty(x => x.LastEditedOn, LastEditedOnMappingInfo);
            
            addProperty(x => x._TestItemSessionGuid, _TestItemSessionGuidMappingInfo);
            //addCollection(item => item.LastEditedBy, LastEditedByMappingInfo);
            //addCollection(item => item.DeletedBy, DeletedByMappingInfo);
            addManyToOneCollection(x => x.LastEditedBy, LastEditedByMappingInfo);
            addManyToOneCollection(x => x.DeletedBy, DeletedByMappingInfo);

            
            
        }
        protected void defaultPropertyMappings(IPropertyMapInfo mapInfo)
        {
            
        }

        protected void defaultCollectionMappings(ICollectionMapInfo mapInfo)
        {
            
        }
        protected void addManyToOneCollection<TElement>(Expression<Func<TData, TElement>> linkedObj, Action<ICollectionMapInfo> mapping)
            where TElement: class
        {
            

            CollectionManyToOneMapInfo<TData, TElement> mapInfo = new CollectionManyToOneMapInfo<TData, TElement>(this, linkedObj);

            defaultCollectionMappings(mapInfo);
            mapping(mapInfo);
            mapInfo.MapCollection();
            LuceneMappingsManager.Instance.CheckMappingForCollection(typeof(TData), mapInfo);

        }

        protected void addCollection<TElement>(Expression<Func<TData, IEnumerable<TElement>>> collection, Action<ICollectionMapInfo> mapping,Enums.NHIBERNATE_COLLECTION_RELATIONSHIP relationshipType)
        {
            CollectionOneToManyMapInfo<TData, TElement> mapInfo = new CollectionOneToManyMapInfo<TData, TElement>(this, collection, relationshipType);
            defaultCollectionMappings(mapInfo);
            mapping(mapInfo);
            mapInfo.MapCollection();
            LuceneMappingsManager.Instance.CheckMappingForCollection(typeof(TData), mapInfo);

        }

        protected void addProperty<TProperty>(Expression<Func<TData, TProperty>> property, Action<IPropertyMapInfo> mapping)
        {
            
            PropertyMapInfo<TData,TProperty> mapInfo = new PropertyMapInfo<TData,TProperty>(this,property);
            defaultPropertyMappings(mapInfo);
            mapping(mapInfo);
            
            mapInfo.MapProperty<TProperty>();
            LuceneMappingsManager.Instance.CheckMappingForProperty(typeof(TData), mapInfo);

            
        }

        protected virtual void IdMappingInfo(IIdMapper mappingInfo)
        {
            mappingInfo.Generator(Generators.HighLow);
            mappingInfo.Access(Accessor.Field);
            


        }
        protected virtual void _Temporary_LastUpdOnMappingInfo(IPropertyMapInfo mappingInfo)
        {
            //setDefaultPropertyMappings(mappingInfo);
        }

        protected virtual void DeletedMappingInfo(IPropertyMapInfo mappingInfo)
         {
            // setDefaultPropertyMappings(mappingInfo);
             
         }
        protected virtual void PublishedMappingInfo(IPropertyMapInfo mappingInfo)
        {
            // setDefaultPropertyMappings(mappingInfo);

        }
        protected virtual void PublishedOnMappingInfo(IPropertyMapInfo mappingInfo)
        {
            // setDefaultPropertyMappings(mappingInfo);

        }
        protected virtual void _ComputedDeletedValueMappingInfo(IPropertyMapInfo mappingInfo)
        {
            // setDefaultPropertyMappings(mappingInfo);

        }



        protected virtual void DeletedByMappingInfo(ICollectionMapInfo mappingInfo)
        {
            mappingInfo.CustomType = Modules.CmsUserModule.CmsUserBaseFactory.Instance.GetMainTypeCreatedByFactory();
            
            //setDefaultPropertyMappings(mappingInfo);

        }

        protected virtual void LastEditedByMappingInfo(ICollectionMapInfo mappingInfo)
         {
             mappingInfo.CustomType = Modules.CmsUserModule.CmsUserBaseFactory.Instance.GetMainTypeCreatedByFactory();
            
             //setDefaultPropertyMappings(mappingInfo);

         }
        protected virtual void PriorityMappingInfo(IPropertyMapInfo mappingInfo)
        {
            
            // setDefaultPropertyMappings(mappingInfo);

        }
        protected virtual void _TestItemSessionGuidMappingInfo(IPropertyMapInfo mappingInfo)
         {
            // setDefaultPropertyMappings(mappingInfo);

         }
        protected virtual void DeletedOnMappingInfo(IPropertyMapInfo mappingInfo)
         {
            // setDefaultPropertyMappings(mappingInfo);

         }

        protected virtual void LastEditedOnMappingInfo(IPropertyMapInfo mappingInfo)
        {
            // setDefaultPropertyMappings(mappingInfo);

        }


        protected void _Temporary_FlagMappingInfo(IPropertyMapInfo mappingInfo)
         {
            // setDefaultPropertyMappings(mappingInfo);
         }



        public new virtual int? BatchSize
        {
            get { return NHClasses.NH_BATCH_SIZE; }
        }

       
        protected virtual bool IsLazyLoaded
        {
            get
            {
                return true;
            }
        }

        protected virtual void specifyCacheType(ICacheMapper mappingInfo)
        {
            mappingInfo.Usage(MappingsUtil.GetNhibernateCacheUsageFromCacheType(this.CacheType));
        }
        private void setTableName()
        {
            if (string.IsNullOrEmpty(this.TableName))
            {
                Type t = typeof (TData);
                string name = t.Name;
                if (name.EndsWith("Impl"))
                    name = CS.General_v3.Util.Text.RemoveLastNumberOfCharactersFromString(name, 4);
                this.TableName= "`" + name +"`";
            }
        }
        protected virtual void VersionMappingInfo(IVersionMapper mappingInfo)
        {
            mappingInfo.Access(Accessor.Field);
            
            //mappingInfo.UnsavedValue(default(it));

        }

        private void mapVersionField()
        {
            this.Version(x => x.Version, VersionMappingInfo);
        }

        public BaseClassMap()
        {
           // initName();
            
            this.CacheType = Enums.NHIBERNATE_CACHE_TYPE.Default;
            base.Lazy(IsLazyLoaded);
            if (this.CacheType != Enums.NHIBERNATE_CACHE_TYPE.Default)
            {
                base.Cache(specifyCacheType);
            }


            if (BatchSize.HasValue)
            {
                base.BatchSize(this.BatchSize.Value);
            }
            initMappings();
            setTableName();
            mapVersionField();

            this.DynamicUpdate(this.PerformDynamicUpdates);
            

            

            base.Table(this.TableName);
        }

    }
}
