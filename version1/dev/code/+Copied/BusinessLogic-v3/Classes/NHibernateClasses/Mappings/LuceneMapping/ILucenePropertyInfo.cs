using Lucene.Net.Analysis;
using NHibernate.Properties;
using NHibernate.Search.Bridge;

//BusinessLogic_v3.Classes.NHibernateClasses.Mappings.Lucene
namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings.LuceneMapping
{
    public interface ILucenePropertyInfo
    {
        bool MappedInLucene { get; set; }
        NHibernate.Search.Attributes.Index Index { get; set; }
        NHibernate.Search.Attributes.Store Store { get; set; }
        IFieldBridge CustomBridge { get; set; }
        bool UseStemming { get; set; }
        bool IsMultilingual { get; set; }
        Analyzer CustomAnalyzer { get; set; }
        float? Boost { get; set; }
        IGetter Getter { get; set; }

        NHibernate.Search.Mapping.FieldMapping CreateFieldMapping();
    }
}