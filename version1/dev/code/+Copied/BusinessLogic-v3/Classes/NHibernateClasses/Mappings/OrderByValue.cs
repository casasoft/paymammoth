﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings
{
    public class OrderByValue
    {
        public OrderByValue(PropertyInfo property, CS.General_v3.Enums.SORT_TYPE sortType = CS.General_v3.Enums.SORT_TYPE.Ascending)
        {
            this.ColumnName = property.Name;
            this.SortType = sortType;
        }
        public OrderByValue(string property, CS.General_v3.Enums.SORT_TYPE sortType = CS.General_v3.Enums.SORT_TYPE.Ascending)
        {
            this.ColumnName = property;
            this.SortType = sortType;
        }
        
        public string ColumnName { get; set; }
        public CS.General_v3.Enums.SORT_TYPE SortType { get; set; }
        public string ToSql()
        {
            return "`" + ColumnName + "` " + (SortType == CS.General_v3.Enums.SORT_TYPE.Ascending ? "Asc" : "Desc");
        }

    }
}
