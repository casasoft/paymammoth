﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using NHibernate.Engine;
using NHibernate.Properties;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings.LuceneMapping.Getters
{
    [Serializable]
    public sealed class MultilingualGetter : BaseGetter
    {
      
        /// <summary>
        /// Initializes a new instance of <see cref="MultilingualGetter"/>.
        /// </summary>
        /// <param name="clazz">The <see cref="System.Type"/> that contains the Property <c>get</c>.</param>
        /// <param name="property">The <see cref="PropertyInfo"/> for reflection.</param>
        /// <param name="propertyName">The name of the Property.</param>
        public MultilingualGetter(System.Type clazz, PropertyInfo property, string propertyName) : base(clazz,property, propertyName)
        {
            

        }


        /// <summary>
        /// Gets the value of the Property from the object.
        /// </summary>
        /// <param name="target">The object to get the Property value from.</param>
        /// <returns>
        /// The value of the Property for the target.
        /// </returns>
        public override object Get(object target)
        {
            
            if (target is IMultilingualContentInfo)
            {
                try
                {
                    IMultilingualContentInfo multiLingualTarget = (IMultilingualContentInfo)target;
                    string propertyValue = (string)property.GetValue(target, new object[0]);
                    string result = propertyValue;
                    if (!string.IsNullOrWhiteSpace(propertyValue))
                    {
                        MultilingualLuceneTextContent mlText = new MultilingualLuceneTextContent();
                        mlText.Culture = multiLingualTarget.CultureInfo.GetCultureCode();
                        mlText.Text = propertyValue;
                        result = mlText.GetTextIncCulture();
                        
                    }
                    return result;
                }
                catch (Exception e)
                {
                    throw new PropertyAccessException(e, "Exception occurred", false, clazz, propertyName);
                }
            }
            else
            {
                throw new InvalidOperationException("Multilingual Getter is only available on IMultilingualContentInfo objects");
            }
            
        }


    }
}
