﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings.LuceneMapping.Getters
{
    public class DbObjectIdGetter : BaseGetter
    {
        
        /// <summary>
        /// Initializes a new instance of <see cref="DbObjectIdGetter"/>.
        /// </summary>
        /// <param name="clazz">The <see cref="System.Type"/> that contains the Property <c>get</c>.</param>
        /// <param name="property">The <see cref="PropertyInfo"/> for reflection.</param>
        /// <param name="propertyName">The name of the Property.</param>
        public DbObjectIdGetter(System.Type clazz, PropertyInfo property, string propertyName)
            : base(clazz, property, propertyName)
        {
            

        }
        public override object Get(object target)
        {
            
                try
                {

                    object oPropertyValue = (IBaseDbObject)property.GetValue(target, new object[0]);
                    if (oPropertyValue is IBaseDbObject)
                    {
                        IBaseDbObject pValue = (IBaseDbObject) oPropertyValue;
                        if (pValue != null)
                            return pValue.ID;
                        else
                            return null;
                    }
                    else
                    {
                        throw new NotImplementedException("This can only be specified on an object that is an IBaseDbObject");
                    }
                    
                }
                catch (Exception e)
                {
                    throw new PropertyAccessException(e, "Exception occurred", false, clazz, propertyName);
                }
            
        }
    }
}
