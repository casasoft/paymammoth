using System;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings.LuceneMapping;
using NHibernate.Type;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings
{
    public interface IPropertyMapInfo
    {
        int? Length { get; set; }
        bool? IsNullable { get; set; }
        string CustomColumnName { get; set; }
        string DefaultValue { get; set; }
        bool IsIndexed { get; set; }
        bool MapEnumAsString { get; set; }
        Type CustomTypeMapping { get; set; }
        ILucenePropertyInfo LuceneInfo { get; }
    }
}