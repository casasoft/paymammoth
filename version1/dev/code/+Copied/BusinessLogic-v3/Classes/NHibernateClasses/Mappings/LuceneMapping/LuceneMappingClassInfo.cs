﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate.Search.Mapping;
using BusinessLogic_v3.Classes.DbObjects;
using NHibernate.Search.Bridge;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings.LuceneMapping
{
    [Serializable]
    public class LuceneMappingClassInfo
    {
        public Type ClassType { get; set; }
        public List<ILucenePropertyInfo> PropertyInfos { get; private set; }
        public LuceneMappingClassInfo(Type classType)
        {
            this.PropertyInfos = new List<ILucenePropertyInfo>();
            this.ClassType = classType;
        }


        public DocumentMapping CreateDocumentMapping()
        {
            DocumentMapping map = new DocumentMapping(this.ClassType);
            foreach (var p in this.PropertyInfos)
            {
                map.Fields.Add(p.CreateFieldMapping());
            }

            DocumentIdMapping idMap = new DocumentIdMapping(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(x => x.ID),
                BridgeFactory.LONG,
                null);
            map.DocumentId = idMap;

            return map;
        }

    }
}
