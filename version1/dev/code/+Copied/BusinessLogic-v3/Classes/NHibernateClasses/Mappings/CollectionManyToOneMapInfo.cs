﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate.Mapping.ByCode;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings
{
    /// <summary>
    /// This implements both a OneToMany collection from the 'many' side, e.g Store.Products, or the many-to-many
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    /// <typeparam name="TElement"></typeparam>
    public class CollectionManyToOneMapInfo<TData, TElement> : BaseCollectionMapInfo<TData>
        where TData : BaseDbObject
        where TElement : class
    {

        

        public CollectionManyToOneMapInfo(BaseClassMap<TData> classMap, System.Linq.Expressions.Expression<Func<TData, TElement>> selector,
           Enums.NHIBERNATE_COLLECTION_RELATIONSHIP relationshipType = Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany)
            : base(classMap,relationshipType)
        {
            this.Selector = selector;
            
            this.setProperty(CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(this.Selector));
            base.LuceneInfo = new LuceneMapping.LucenePropertyInfo(this.Property);
        }
        public System.Linq.Expressions.Expression<Func<TData, TElement>> Selector {get; private set;}


        private void mapLazyAttribute(IManyToOneMapper mappingInfo)
        {
            switch (this.LazyMode)
            {
                case CollectionLazy.Lazy:
                case CollectionLazy.Extra:
                    mappingInfo.Lazy(LazyRelation.Proxy);
                    break;
                case CollectionLazy.NoLazy:
                    mappingInfo.Lazy(LazyRelation.NoLazy);
                    break;

            }
        }
        private void mapFetchType(IManyToOneMapper mappingInfo)
        {
            switch (this.FetchType)
            {
                case Enums.NHIBERNATE_COLLECTION_FETCHMODE.Default:
                    break;
                case Enums.NHIBERNATE_COLLECTION_FETCHMODE.Join:

                    mappingInfo.Fetch(FetchKind.Join); break;
                case Enums.NHIBERNATE_COLLECTION_FETCHMODE.Select:
                    mappingInfo.Fetch(FetchKind.Select); break;
                case Enums.NHIBERNATE_COLLECTION_FETCHMODE.SubSelect:
                    throw new InvalidOperationException("SubSelect cannot be defined on CollectionManyToOneMappingInfo");
            }
        }

        private void checkCustomType()
        {
            if (this.CustomType == null)
            {
                this.CustomType = BusinessLogic_v3.Classes.DbObjects.DbFactoryController.Instance.GetFactoryForType(typeof(TElement)).GetMainTypeCreatedByFactory();
            }
        }

        protected void fillManyToOneMapping(IManyToOneMapper mappingInfo)
        {

            //mappingInfo.NotFound(NotFoundMode.Ignore);
            
            checkCustomType();
            if (this.CustomType != null)
                mappingInfo.Class(this.CustomType);
            if (this.Property.Name == "ParentArticleLinks")
            {
                int k = 5;
            }
            mappingInfo.Cascade(this.CascadeType.GetNhibernateCascade());
            
            
            mapLazyAttribute(mappingInfo);

           
            if (string.IsNullOrWhiteSpace(this.ManyToManyTableName) && this.RelationshipType == Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.ManyToMany)
            {
                throw new InvalidOperationException("Many-to-many relationships must specify the table name");
            }
            if (!string.IsNullOrWhiteSpace(this.ColumnName))
                mappingInfo.Column(this.ColumnName);
            
            mapFetchType(mappingInfo);
            mappingInfo.Access(Accessor.Field);
            if (this.IsIndexed)
            {
               mappingInfo.Index(this.Property.Name + "Index");
            }
            
        }

        protected override void _mapCollection()
        {
            this.ClassMap.ManyToOne<TElement>(this.Selector, fillManyToOneMapping);
            


        }



    }
}
