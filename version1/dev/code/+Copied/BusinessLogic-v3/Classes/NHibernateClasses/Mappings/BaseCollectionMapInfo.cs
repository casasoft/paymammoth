﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using NHibernate.Mapping.ByCode;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings.Cascades;
using IBaseDbObject = BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings
{


    /// <summary>
    /// Adds a collection mapping
    /// </summary>
    /// <typeparam name="TData">This is the class that contains the collection. E.g Store</typeparam>
    /// <typeparam name="TElement">This is the type of the element in the collection, e.g a Products collection would be Product</typeparam>
    public abstract class BaseCollectionMapInfo<TData> : ICollectionMapInfo
        where TData : BaseDbObject
    {

        public bool DontMapCollection { get; set; }
        public Enums.NHIBERNATE_COLLECTION_TYPE CollectionType { get; set; }
        public Enums.NHIBERNATE_COLLECTION_RELATIONSHIP RelationshipType { get; private set; }
        public string ManyToManyTableName { get; set; }
        public string ParentKey { get; set; }
        public string ChildKey { get; set; }

        public NHibernate.Mapping.ByCode.CollectionLazy? LazyMode { get; set; }
        public List<OrderByValue> OrderByValuesSql { get; set; }
        public bool IsIndexed { get; set; }
        public BaseClassMap<TData> ClassMap { get; private set; }
        public OrderByValue AddOrderBy(PropertyInfo pInfo, CS.General_v3.Enums.SORT_TYPE sortType = CS.General_v3.Enums.SORT_TYPE.Ascending)
        {
            var value = new OrderByValue(pInfo, sortType);
            this.OrderByValuesSql.Add(value);
            return value;
        }
        public CollectionCascadeInfo CascadeType { get; private set; }
        public Enums.NHIBERNATE_COLLECTION_FETCHMODE FetchType { get; set; }
        public Enums.NHIBERNATE_CACHE_TYPE CacheType { get; set; }
        public bool? Inverse { get; set; }
        public int? BatchSize { get; set; }


        /// <summary>
        /// This only applies to a ManyToOne relationship
        /// </summary>
        public string ColumnName { get; set; }
        public PropertyInfo Property { get; private set; }
       

        public BaseCollectionMapInfo(BaseClassMap<TData> classMap,
           Enums.NHIBERNATE_COLLECTION_RELATIONSHIP relationshipType)
        {
            this.CascadeType = new CollectionCascadeInfo();
            this.ClassMap = classMap;
            this.CollectionType = Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            this.CacheType = Enums.NHIBERNATE_CACHE_TYPE.Default;
            this.FetchType = Enums.NHIBERNATE_COLLECTION_FETCHMODE.Default;

            this.OrderByValuesSql = new List<OrderByValue>();
            this.RelationshipType = relationshipType;
            //this.Inverse = Inverse;
            //this.ParentKey = ParentKey;
           // this.ChildKey = ChildKey;
            this.BatchSize = null;
            if (this.RelationshipType == Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany)
            {
                this.OrderByValuesSql.Add(new OrderByValue(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(x => x.Priority)));
            }

            initDefaultValues();
            
        }
        protected void setProperty(PropertyInfo p)
        {
            this.Property = p;
            string name = this.Property.Name.ToLower();
            string className = this.Property.DeclaringType.Name;

            if (name.Contains("_CultureInfo".ToLower()))
            {
                if (!className.Contains("CultureDetails"))
                {
                    this.FetchType = Enums.NHIBERNATE_COLLECTION_FETCHMODE.Join;
                }
                else
                {
                    int k = 5;
                }

                //this.CacheType = FluentNHEnums.CACHE_TYPE.ReadWrite;
            }
        }

        private void initDefaultValues()
        {

            // by default, cascade persistance only.
            this.CascadeType.Persist = true;
            this.CascadeType.ReAttach = true;
            this.CascadeType.Refresh = true;
            this.CascadeType.Detach = true;
            
            //this.CascadeType = Cascade.DeleteOrphans;
                

        }


        //protected void mapCacheTypeProperty(CachePart cache)
        //{
        //    if (CacheType != FluentNHEnums.CACHE_TYPE.Default)
        //    {
        //        switch (CacheType)
        //        {
        //            case FluentNHEnums.CACHE_TYPE.NonStrictReadWrite: cache.NonStrictReadWrite(); break;
        //            case FluentNHEnums.CACHE_TYPE.ReadWrite: cache.ReadWrite(); break;
        //            case FluentNHEnums.CACHE_TYPE.ReadOnly: cache.ReadOnly(); break;
        //        }
        //    }
        //}
        protected void mapCacheType(ICacheMapper mappingInfo)
        {
            if (this.CacheType != Enums.NHIBERNATE_CACHE_TYPE.Default)
            {
                mappingInfo.Usage(MappingsUtil.GetNhibernateCacheUsageFromCacheType(this.CacheType));

            }
        }


        protected void mapKeyInfo(IKeyMapper<TData> mappingInfo)
        {
            
            if (this.RelationshipType == Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.ManyToMany)
            {
                mappingInfo.Column(this.ParentKey);
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(this.ColumnName))
                {
                    mappingInfo.Column(this.ColumnName);
                }

            }
            
            if (IsIndexed)
            {
                //TESTmappingInfo.ForeignKey(this.Property.Name + "_Index"); //todo: not sure if this is right
                
                
                
                
                //map.Index(this.Property.Name + "_Index");
            }
            //x.Key(key =>
            //    {
            //        key.Column("ProductId"); //this should be the local field, i.e if mapping a Store.Products collection, this should be 'StoreId'
            //        key.ForeignKey("FK_StoreProducts_Store");
            //    });
        }


        protected void checkForMultilingualClass()
        {
            string pName = this.Property.Name.ToLower();
            if (pName.Contains("_CultureInfos".ToLower())) //automatically set default values for the multilingual collection
            {
                this.CascadeType.SetAll();
                this.FetchType = Enums.NHIBERNATE_COLLECTION_FETCHMODE.Select;

            }

        }
        public Type CustomType { get; set; }

        
        protected abstract void _mapCollection();
        public void MapCollection()
        {
            
            checkForMultilingualClass();
            if (!this.DontMapCollection)
            {
              _mapCollection();
            }
        }











        public LuceneMapping.ILucenePropertyInfo LuceneInfo { get; protected set; }

    }
}
