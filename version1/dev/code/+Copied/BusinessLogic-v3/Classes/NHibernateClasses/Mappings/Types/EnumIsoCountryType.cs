﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings.Types
{
    [Serializable]
    public class EnumIsoCountryType : EnumNullableToStringType<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166>

    {
        protected override string convertEnumValueToString(object value)
        {
            string s = null;
            if (value is Enum)
            {
                Enum enumVal = (Enum)value;
                s = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode((CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166)enumVal);
            }
            return s;
        }
        protected override Enum convertStringToEnumValue(string xml)
        {
            Enum val = null;
            if (!string.IsNullOrWhiteSpace(xml))
            {
                val = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(xml);
                if (val == null) val = base.convertStringToEnumValue(xml);
            }

            return val;

        }
    }
}
