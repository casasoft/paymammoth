﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate.Search.Bridge;
using Lucene.Net.Analysis;
using NHibernate.Properties;
using NHibernate.Search.Mapping;
using System.Reflection;
using BusinessLogic_v3.Classes.LuceneClasses.Analyzers;
using BusinessLogic_v3.Classes.DbObjects;
using Lucene.Net.Analysis.Standard;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings.LuceneMapping
{
    [Serializable]
    public class LucenePropertyInfo : ILucenePropertyInfo
    {
        public static LucenePropertyInfo CreateLucenePropertyInfo<T>(System.Linq.Expressions.Expression<Func<T, object>> selector)
        {
            var pInfo = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(selector);
            return new LucenePropertyInfo(pInfo);
        }


        public LucenePropertyInfo(PropertyInfo propertyInfo)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(propertyInfo, "Property Info is required");
            this.Property = propertyInfo;
            this.Index = NHibernate.Search.Attributes.Index.Tokenized;
            this.Store = NHibernate.Search.Attributes.Store.No;
            
        }
        public PropertyInfo Property { get; private set; }
        public Type GetClassType()
        {
            return this.Property.DeclaringType;
        }

        public NHibernate.Search.Attributes.Index Index { get; set; }
        public NHibernate.Search.Attributes.Store Store { get; set; }

        /// <summary>
        /// If no custom bridge defined, default is taken
        /// </summary>
        public IFieldBridge CustomBridge { get; set; }

        public bool UseStemming { get; set; }
        public bool IsMultilingual { get; set; }

        public Analyzer CustomAnalyzer { get; set; }
        public float? Boost { get; set; }
        public IGetter Getter { get; set; }



        #region ILucenePropertyInfo Members

        public bool MappedInLucene{get;set;}

        #endregion

        #region ILucenePropertyInfo Members


        private IFieldBridge getBridgeBasedOnDataType()
        {
            if (this.CustomBridge != null)
                return this.CustomBridge;
            else
            {
                if (this.Property.PropertyType == typeof (long) || isPropertyAnIDbObject())
                    return BridgeFactory.LONG;
                else if (this.Property.PropertyType == typeof (bool))
                    return BridgeFactory.BOOLEAN;
                else if (this.Property.PropertyType == typeof (double))
                    return BridgeFactory.DOUBLE;
                else if (this.Property.PropertyType == typeof (DateTime))
                    return BridgeFactory.DATE_SECOND;
                else if (this.Property.PropertyType == typeof (float))
                    return BridgeFactory.FLOAT;
                else if (this.Property.PropertyType == typeof (int))
                    return BridgeFactory.INTEGER;
                else if (this.Property.PropertyType == typeof (short))
                    return BridgeFactory.SHORT;
                else if (this.Property.PropertyType == typeof (string))
                    return BridgeFactory.STRING;
                else
                {
                    throw new InvalidOperationException("Default bridge does not exist for type '" + this.Property.PropertyType.FullName + "', please define in 'CustomBridge'. Property: " + this.Property.Name);

                }
            }

        }

        protected bool isPropertyAnIDbObject()
        {
            return CS.General_v3.Util.ReflectionUtil.CheckIfTypeExtendsFrom(this.Property.PropertyType, typeof(IBaseDbObject));
        }

        private Analyzer getAnalyser()
        {
            if (this.CustomAnalyzer != null)
                return this.CustomAnalyzer;
            else
            {
                if (this.Property.PropertyType == typeof(string))
                {
                    LanguageAnalyzer analyzer = new LanguageAnalyzer();
                    analyzer.Test = DateTime.Now.Second + 5;
                    return analyzer;
                }
                else
                {
                    StandardAnalyzer analyzer = new StandardAnalyzer( Lucene.Net.Util.Version.LUCENE_29);
                    return analyzer;
                }
            }
        }

        private IGetter getFieldGetter()
        {
            if (IsMultilingual)
            {
                return new Getters.MultilingualGetter(this.GetClassType(), this.Property, this.Property.Name);
            }
            else if (CS.General_v3.Util.ReflectionUtil.CheckIfTypeExtendsFrom(this.Property.PropertyType, typeof(IBaseDbObject)))
            {
                return new Getters.DbObjectIdGetter(this.GetClassType(), this.Property, this.Property.Name);
            }
            else
                return new NHibernate.Properties.BasicPropertyAccessor.BasicGetter(this.GetClassType(), this.Property, this.Property.Name);
            
        }

        public NHibernate.Search.Mapping.FieldMapping CreateFieldMapping()
        {
            var bridge  = getBridgeBasedOnDataType();
            var getter = getFieldGetter();
            //getter = new BasicPropertyAccessor.BasicGetter(this.GetClassType(), this.Property, this.Property.Name);
            //bridge = null;

            FieldMapping map = new FieldMapping(this.Property.Name, bridge, getter);
            map.Index = this.Index;
            map.Store = this.Store;
            map.Store = NHibernate.Search.Attributes.Store.Yes;
            map.Analyzer = getAnalyser();
            //map.Analyzer = new StandardAnalyzer();
            map.Boost = this.Boost;
            return map;
            
        }

        #endregion
    }
}
