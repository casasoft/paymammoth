﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.Type;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings.Types
{
    /// <summary>
    /// Base class for enum types.
    /// </summary>
    [Serializable]
    public abstract class AbstractNullableEnumType : PrimitiveType, IDiscriminatorType
    {
        protected AbstractNullableEnumType(SqlType sqlType, System.Type enumType)
            : base(sqlType)
        {
            if (CS.General_v3.Util.ReflectionUtil.CheckIfTypeIsEnum(enumType))
            {
                this.type = enumType;
                if (!CS.General_v3.Util.ReflectionUtil.CheckIfTypeIsNullable(enumType))
                {
                    this.TypeEnum = enumType;
                    defaultValue = Enum.GetValues(enumType).GetValue(0);
                }
                else
                {
                    IsNullable = true;
                    this.TypeEnum = enumType.GetGenericArguments()[0];
                    defaultValue = null;
                }
            }
            else
            {
                throw new MappingException(enumType.Name + " did not inherit from System.Enum or Nullable<Enum>");
            }
            
        }

        private readonly object defaultValue;
        private readonly System.Type type;
        public System.Type TypeEnum {get; private set;}
        public bool IsNullable { get; private set; }
        public override System.Type ReturnedClass
        {
            get { return type; }
        }


        #region IIdentifierType Members

        /// <summary>
        /// Parses a given string to its corresponding enum value
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        protected virtual Enum convertStringToEnumValue(string xml)
        {
            var value = (Enum)Enum.Parse(this.TypeEnum, xml);
            return value;
        }

        public object StringToObject(string xml)

        {
            
            object value = null;
            if (IsNullable)
            {
                if (!string.IsNullOrWhiteSpace(xml))
                {
                    try
                    {
                        value = convertStringToEnumValue(xml);
                        
                    }
                    catch (Exception ex)
                    {
                        value = null;
                    }
                }
            }
            else
            {
                value = convertStringToEnumValue(xml);
                
            }
            return value;
        }

        #endregion


        public override object FromStringValue(string xml)
        {
            return StringToObject(xml);
        }

        public override System.Type PrimitiveClass
        {
            get { return this.type; }
        }

        public override object DefaultValue
        {
            get { return defaultValue; }
        }
    }
}
