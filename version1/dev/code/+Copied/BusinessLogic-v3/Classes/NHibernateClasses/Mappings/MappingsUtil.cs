﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Mapping.ByCode;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings
{
    public static class MappingsUtil
    {
        public static CacheUsage GetNhibernateCacheUsageFromCacheType(Enums.NHIBERNATE_CACHE_TYPE cacheType)
        {
            switch (cacheType)
            {
                case Enums.NHIBERNATE_CACHE_TYPE.NonstrictReadWrite:

                    return (CacheUsage.NonstrictReadWrite);
                    break;
                case Enums.NHIBERNATE_CACHE_TYPE.ReadOnly:
                    return (CacheUsage.ReadOnly);
                    break;
                case Enums.NHIBERNATE_CACHE_TYPE.ReadWrite:
                    return (CacheUsage.ReadWrite);
                    break;
                case Enums.NHIBERNATE_CACHE_TYPE.Transactional:
                    return (CacheUsage.Transactional);
                    break;
                default:
                    throw new InvalidOperationException("Invalid cache type");
            }
        }
    }
}
