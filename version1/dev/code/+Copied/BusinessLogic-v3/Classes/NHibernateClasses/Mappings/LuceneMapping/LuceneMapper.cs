﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Search.Mapping;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Mappings.LuceneMapping
{
    [Serializable]
    public class LuceneMapper :ISearchMapping
    {
        #region ISearchMapping Members

        public ICollection<DocumentMapping> Build(NHibernate.Cfg.Configuration cfg)
        {
            
            List<DocumentMapping> mapList = new List<DocumentMapping>();
            var mappedClasses = LuceneMappingsManager.Instance.GetAllMappedClasses();
            foreach (var c in mappedClasses)
            {
                mapList.Add(c.CreateDocumentMapping());
            }
            return mapList;
        }

        #endregion
    }
}
