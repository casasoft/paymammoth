﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Event;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Listeners
{
    [Serializable]
    public class FactoryEventCallerListener : IPostDeleteEventListener, IPostInsertEventListener, IPostUpdateEventListener
    {
        #region IPostDeleteEventListener Members

        public void OnPostDelete(PostDeleteEvent @event)
        {
            BusinessLogic_v3.Classes.DbObjects.ItemChanges.ItemChangeManager.Instance.DeletedItem(@event.Entity, (long)@event.Id);
            
        }

        #endregion

        #region IPostInsertEventListener Members

        public void OnPostInsert(PostInsertEvent @event)
        {
            BusinessLogic_v3.Classes.DbObjects.ItemChanges.ItemChangeManager.Instance.CreatedItem(@event.Entity, (long)@event.Id);
            
        }

        #endregion

        #region IPostUpdateEventListener Members

        public void OnPostUpdate(PostUpdateEvent @event)
        {
            BusinessLogic_v3.Classes.DbObjects.ItemChanges.ItemChangeManager.Instance.UpdatedItem(@event.Entity, (long)@event.Id);
            
            
        }

        #endregion


    }
}
