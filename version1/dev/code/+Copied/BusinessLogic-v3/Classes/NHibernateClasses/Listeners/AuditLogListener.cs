﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Event;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Listeners
{
    [Serializable]
    public class AuditLogListener : IPostDeleteEventListener, IPostInsertEventListener, IPostUpdateEventListener, IPreUpdateEventListener, IPreInsertEventListener
    {
        //currently not used, but using interceptor instead
        #region IPostDeleteEventListener Members

        public void OnPostDelete(PostDeleteEvent @event)
        {
            BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.LogDelete(@event.Entity, @event.Id, @event.DeletedState, @event.Persister.PropertyNames, @event.Persister.PropertyTypes);
        }

        #endregion

        #region IPostInsertEventListener Members

        public void OnPostInsert(PostInsertEvent @event)
        {
           // BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.LogAdd(@event.Entity, @event.Id, @event.State, @event.Persister.PropertyNames, @event.Persister.PropertyTypes);
            
        }

        #endregion

        #region IPostUpdateEventListener Members

        public void OnPostUpdate(PostUpdateEvent @event)
        {
           // BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.LogChange(@event.Entity, @event.Id, @event.State, @event.OldState, @event.Persister.PropertyNames, @event.Persister.PropertyTypes);
            
        }

        #endregion



        #region IPreInsertEventListener Members

        public bool OnPreInsert(PreInsertEvent @event)
        {
            BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.LogAdd(@event.Entity, @event.Id, @event.State, @event.Persister.PropertyNames, @event.Persister.PropertyTypes);
            return false;
        }

        #endregion

        #region IPreUpdateEventListener Members

        public bool OnPreUpdate(PreUpdateEvent @event)
        {
            BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.LogChange(@event.Entity, @event.Id, @event.State, @event.OldState, @event.Persister.PropertyNames, @event.Persister.PropertyTypes);
            return false;
        }

        #endregion
    }
}
