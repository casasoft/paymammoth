﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate.Event;
using IBaseDbObject = BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Listeners
{
    [Serializable]
    public class CrudEventListeners : IPreDeleteEventListener
    {
        #region IPreDeleteEventListener Members

        public bool OnPreDelete(PreDeleteEvent e)
        {
            bool ok = true;
            if (e.Entity is BaseDbObject)
            {
                BaseDbObject dbObject = (BaseDbObject) e.Entity;
                 ok =dbObject.nhibernate_onPreDelete(e);
            }
            
            return ok;

        }

        #endregion
    }
}
