﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;

namespace BusinessLogic_v3.Classes.NHibernateClasses.SessionContextManagers
{
    public abstract class BaseSessionContextManager
    {
        protected abstract Stack<BaseLazySession> getStack();
        


        public MyNHSessionBase GetCurrentSession(bool throwErrorIfNoSessionexists = false)
        {
            //  checkToCreateFirstSessionOnFirstAccess();
            
            var stack = getStack();
            if (stack.Count > 0)
                return stack.Peek().GetSession();
            else
            {
                if (throwErrorIfNoSessionexists)
                    throw new InvalidOperationException(this.GetType().FullName + " - Session does not exist at the current context");
            }
            return null;

        }

        private void attachSessionToCurrentContext(BaseLazySession session)
        {
            var stack = getStack();
            var newSession = session;

            stack.Push(newSession);

        }
        public MyNHSessionNormal CreateNewSessionForContextAndReturn()
        {
            CreateNewSessionForContext();
            return (MyNHSessionNormal)GetCurrentSession();
        }



        public void CreateNewSessionForContext()
        {
            LazySession sessionTransaction = new LazySession();
            attachSessionToCurrentContext(sessionTransaction);
           

        }
      


        public MyNHSessionStateless CreateNewStatelessSessionForContext()
        {

            LazyStatelessSession session = new LazyStatelessSession();
            attachSessionToCurrentContext(session);
            return session.GetSession();
        }

        /// <summary>
        /// This should return a lock, that locks only for this specific context, not for all like the '_padlock';
        /// </summary>
        /// <returns></returns>
        protected abstract object getContextLock();

        protected static readonly object _padlock = new object();
        public MyNHSessionBase DisposeCurrentSessionInContext(bool throwErrorIfSessionDoesNotExistOrDisposed = true)
        {


            var stack = getStack();

            

            if (stack.Count == 0 && throwErrorIfSessionDoesNotExistOrDisposed)
            {
              

                {
                    throw new InvalidOperationException(this.GetType().FullName + " - No Session exists to be disposed!");
                }
            }
            MyNHSessionBase session = null;
            if (stack.Count > 0)
            {

                var lazySession = stack.Pop();
          //      lazySession.Commit();
                session = lazySession.GetSession(createIfNotCreated: false);
                if (session != null)
                {
                    //[KARL] 25/may/2012 - this was disabled as now there is no need to call commit /flush, as all events
                    //are handed by transactions

                    //if (session.Transaction != null && session.Transaction.IsActive)
                    //{
                    //    session.Transaction.Commit();
                    //}
                    //session.Flush();
                }

                lazySession.Dispose(throwErrorIfSessionDoesNotExistOrDisposed);
                //sessionTransaction.
                //session = sessionTransaction.Session;
                //if (!session.IsOpen && throwErrorIfSessionDoesNotExistOrDisposed)
                //    throw new InvalidOperationException(this.GetType().FullName + " - Session already closed / disposed!");
                //sessionTransaction.Commit();
                //sessionTransaction.Dispose();
                //NhManagerIOC.NhManager.CloseSession(session);
            }
            /*session.Flush();
            session.Close();
            session.Dispose();*/
            return session;
        }
    }
}
