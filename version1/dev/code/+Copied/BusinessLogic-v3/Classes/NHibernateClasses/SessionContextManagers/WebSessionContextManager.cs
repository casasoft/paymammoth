﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;

namespace BusinessLogic_v3.Classes.NHibernateClasses.SessionContextManagers
{
    public class WebSessionContextManager : BaseSessionContextManager, ISessionContextManager
    {


        private static readonly WebSessionContextManager _instance = new WebSessionContextManager();
        public static WebSessionContextManager Instance
        {
            get
            {
                return _instance;
            }
        }
        private WebSessionContextManager()
        {

        }


        private object _contextLock
        {
            get
            {
                return (Stack<BaseLazySession>)Context.Items["WebSessionContextManager.Lock"];
            }
            set
            {
                Context.Items["WebSessionContextManager.Lock"] = value;
            }
        }
        protected object contextLock
        {
            get
            {
                if (_contextLock == null)
                {
                    lock (_padlock)
                    {
                        if (_contextLock == null)
                        {
                            _contextLock = new object();
                        }
                    }
                }
                return _contextLock;
            }
        }

        private Stack<BaseLazySession> ___httpContextSessionStack
        {
            get
            {
                return (Stack<BaseLazySession>)System.Web.HttpContext.Current.Items["___httpContextSessionStack_2"];
            }
            set
            {
                System.Web.HttpContext.Current.Items["___httpContextSessionStack_2"] = value;
            }
        }
        private Stack<BaseLazySession> httpContextSessionStack
        {
            get
            {
                if (___httpContextSessionStack == null)
                {
                    lock (_padlock)
                    {
                        if (___httpContextSessionStack == null)
                        {
                            ___httpContextSessionStack = new Stack<BaseLazySession>();
                        }
                    }
                }

                return ___httpContextSessionStack;
            }

        }

        //protected override bool createSessionInContextOnFirstAccess 
        //{
        //    get
        //    {
        //        return ((bool?)Context.Items["createSessionInContextOnFirstAccess"]).GetValueOrDefault();
        //    }
        //    set
        //    {
        //        Context.Items["createSessionInContextOnFirstAccess"] = value;
        //    }
        //}
        //protected override bool firstContextAccessCalled
        //{
        //    get
        //    {
        //        return ((bool?)Context.Items["firstContextAccessCalled"]).GetValueOrDefault();
        //    }
        //    set
        //    {
        //        Context.Items["firstContextAccessCalled"] = value;
        //    }
        //}


        protected HttpContext Context { get { return System.Web.HttpContext.Current; } }

        protected override Stack<BaseLazySession> getStack()
        {
            Stack<BaseLazySession> stack = null;

            stack = httpContextSessionStack;

            return stack;
        }





        protected override object getContextLock()
        {
            return this.contextLock;
        }

        //protected override void checkToCreateFirstSessionOnFirstAccess()
        //{
        //    if (createSessionInContextOnFirstAccess)
        //    {
        //        lock (System.Web.HttpContext.Current)
        //        {
        //            if (createSessionInContextOnFirstAccess)
        //            {
        //                _createNewSessionForContext();
        //                createSessionInContextOnFirstAccess = false;
        //            }
        //        }
        //    }
        //}

    }
}
