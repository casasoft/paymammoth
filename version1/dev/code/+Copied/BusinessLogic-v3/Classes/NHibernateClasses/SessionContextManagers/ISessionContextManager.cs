using System.Web;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;

namespace BusinessLogic_v3.Classes.NHibernateClasses.SessionContextManagers
{
    public interface ISessionContextManager
    {
        MyNHSessionBase GetCurrentSession(bool throwErrorIfNoSessionexists = false);
        //MyNHSessionBase AttachSessionToCurrentContext(MyNHSessionBase session);
        void CreateNewSessionForContext();
        MyNHSessionNormal CreateNewSessionForContextAndReturn();

        MyNHSessionStateless CreateNewStatelessSessionForContext();
        MyNHSessionBase DisposeCurrentSessionInContext(bool throwErrorIfSessionDoesNotExistOrDisposed = true);
        
    }
}