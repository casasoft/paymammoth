﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.NHibernateClasses.SessionContextManagers
{
    public static class SessionContextUtil
    {
        /// <summary>
        /// Returns the web-based, or thread, based on whether the HttpContext exists or not
        /// </summary>
        /// <returns></returns>
        public static ISessionContextManager GetSessionContextManagerBasedOnContext()
        {
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Items != null)
                return WebSessionContextManager.Instance;
            else
                return ThreadSessionContextManager.Instance;
        }
    }
}
