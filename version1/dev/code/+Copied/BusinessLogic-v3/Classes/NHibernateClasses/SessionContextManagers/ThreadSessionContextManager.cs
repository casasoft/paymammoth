﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;

namespace BusinessLogic_v3.Classes.NHibernateClasses.SessionContextManagers
{
    public class ThreadSessionContextManager : BaseSessionContextManager, ISessionContextManager
    {


        private static readonly ThreadSessionContextManager _instance = new ThreadSessionContextManager();
        public static ThreadSessionContextManager Instance
        {
            get
            {
                return _instance;
            }
        }
        private ThreadSessionContextManager()
        {

        }
        //private static readonly object _padlock = new object();
        [ThreadStatic]
        private static Stack<BaseLazySession> __threadSessionStack;
        private Stack<BaseLazySession> threadSessionStack
        {
            get
            {
                if (__threadSessionStack == null)
                {
                    lock (threadLock)
                    {
                        if (__threadSessionStack == null)
                        {
                            __threadSessionStack = new Stack<BaseLazySession>();
                        }
                    }
                }
                return __threadSessionStack;
            }
        }




        //[ThreadStatic]
        //private static bool _createSessionOnFirstAccess = false;
        /////// <summary>
        /////// This indicates that on the first session access, a new session should be created
        /////// </summary>
        ////protected override bool createSessionInContextOnFirstAccess { get { return _createSessionOnFirstAccess; } set { _createSessionOnFirstAccess = value; } }

        //[ThreadStatic]
        //private static bool _firstContextAccessCalled;
        //protected override bool firstContextAccessCalled
        //{
        //    get { return _firstContextAccessCalled; }
        //    set { _firstContextAccessCalled = value; }
        //}


        [ThreadStatic]
        private static object _threadLock = null;
        protected object threadLock
        {
            get
            {
                if (_threadLock == null)
                {
                    lock (_padlock)
                    {
                        if (_threadLock == null)
                        {
                            _threadLock = new object();
                        }
                    }
                }
                return _threadLock;
            }
        }


        protected override Stack<BaseLazySession> getStack()
        {
            return threadSessionStack;
        }

        protected override object getContextLock()
        {
            return threadLock;

        }

        //protected override void checkToCreateFirstSessionOnFirstAccess()
        //{
        //    if (createSessionInContextOnFirstAccess) //this means the session was not really created yet
        //    {
        //        lock (threadLock)
        //        {
        //            if (createSessionInContextOnFirstAccess)
        //            {
        //                _createNewSessionForContext();
        //                createSessionInContextOnFirstAccess = false;

        //            }
        //        }
        //    } 
        //}









    }
}
