﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Util;
using NHibernate;
using BusinessLogic_v3.Classes.DbObjects;
using log4net;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Interceptors
{
    [Serializable]
    public class BaseNhInterceptor : EmptyInterceptor
    {
        [NonSerialized]
        private ILog _m_log = null;

        
        protected ILog _log
        {
            get
            {
                if (_m_log == null) _m_log = LogManager.GetLogger(this.GetType());
                return _m_log;
            }
        }
        

        protected string getLogForObjectStatusChange(object entity, object id, object[] state, string[] propertyNames)
        {
            
            StringBuilder sb = new StringBuilder();
            if (entity != null)
            {
                sb.AppendLine("Object Status for <" + entity.ToString() + ">, ID: " + id + ", Type: " + entity.GetType().FullName);
                sb.AppendLine("==============================================");
                
                for (int i = 0; i < propertyNames.Length; i++)
                {
                    sb.AppendLine(propertyNames[i] + ": " + state[i]);
                    sb.AppendLine("----");
                }
            }
            else
            {
                sb.AppendLine("Entity is NULL - " + id);
            }
            sb.AppendLine();
            sb.AppendLine("============== STACK TRACE ==================");
            sb.AppendLine(System.Environment.StackTrace);
            return sb.ToString();
        }
        protected void logObjectStatusChange(CS.General_v3.Enums.LOG4NET_MSG_TYPE msgType, object entity, object id, object[] state, string[] propertyNames)
        {
            string s = getLogForObjectStatusChange(entity, id, state, propertyNames);
            
            CS.General_v3.Util.Log4NetUtil.LogMsg(_log, msgType, s, null);
            
        }

        public static BaseNhInterceptor CreateInterceptor()
        {
            return CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<BaseNhInterceptor>();
        }

        protected BaseNhInterceptor()
        {
            
        }

        public override bool OnLoad(object entity, object id, object[] state, string[] propertyNames, global::NHibernate.Type.IType[] types)
        {
            return base.OnLoad(entity, id, state, propertyNames, types);



        }

        public override void OnDelete(object entity, object id, object[] state, string[] propertyNames, NHibernate.Type.IType[] types)
        {
            base.OnDelete(entity, id, state, propertyNames, types);
            BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.LogDelete(entity, id, state, propertyNames, types);
            //BusinessLogic_v3.Classes.DB.ItemChanges.ItemChangeManager.Instance.DeletedItem(entity, (long)id);
            
        }
        

        
        
        public override bool OnSave(object entity, object id, object[] state, string[] propertyNames, NHibernate.Type.IType[] types)
        {//this is equivalent to 'Create' item
            bool result = base.OnSave(entity, id, state, propertyNames, types);
            BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.LogAdd(entity, id, state, propertyNames, types);
           // BusinessLogic_v3.Classes.DB.ItemChanges.ItemChangeManager.Instance.CreatedItem(entity, (long)id);
            
           // return result;
            if (false && entity is BaseDbObject)
            {
                bool? flagValue = (bool?) nHibernateUtil.GetPropertyValueFromListOfStatesAndNames("_Temporary_Flag", state, propertyNames);
                if (flagValue.GetValueOrDefault())
                {
                    result = nHibernateUtil.UpdatePropertyFromListOfStatesAndNames("_Temporary_LastUpdOn", state, propertyNames, DateTime.Now) || result;
                }
            }
            return result;
          
        }
        public override bool OnFlushDirty(object entity, object id, object[] currentState, object[] previousState, string[] propertyNames, NHibernate.Type.IType[] types)
        {
            //this is called when an object is updated

            bool result = base.OnFlushDirty(entity, id, currentState, previousState, propertyNames, types);
            BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.LogChange(entity, id, currentState, previousState, propertyNames, types);
//            BusinessLogic_v3.Classes.DB.ItemChanges.ItemChangeManager.Instance.UpdatedItem(entity, (long)id);
            if (false && entity is BaseDbObject)
            {
                bool? flagValue = (bool?) nHibernateUtil.GetPropertyValueFromListOfStatesAndNames("_Temporary_Flag", currentState, propertyNames);
                if (flagValue.GetValueOrDefault())
                {
                    result = nHibernateUtil.UpdatePropertyFromListOfStatesAndNames("_Temporary_LastUpdOn", currentState, propertyNames, DateTime.Now) || result;
                }
            }
            return result;

           
            
        }
    }
}
