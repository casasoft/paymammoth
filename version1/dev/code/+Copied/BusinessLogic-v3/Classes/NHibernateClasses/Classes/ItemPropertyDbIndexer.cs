﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using IBaseDbObject = BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject;

namespace BusinessLogic_v3.Classes.NHibernateClasses.Classes
{
    public class ItemPropertyDbIndexer<TItem> :IDisposable where TItem: BaseDbObject
    {
        private Dictionary<object, long> _dict = new Dictionary<object, long>();
        private Func<TItem, object> _keyRetriever = null;
        private IBaseDbFactory<TItem> factory = null;
        public ItemPropertyDbIndexer(IBaseDbFactory<TItem> factory, Func<TItem, object> keyRetriever)
        {
            this.factory = factory;
            _keyRetriever = keyRetriever;
            _dict = new Dictionary<object, long>();
        }

        public bool IsCaseSensitive { get; set; }

        private object getKey(TItem item)
        {
            var key = _keyRetriever(item);
            
            if (!IsCaseSensitive && key is string)
            {
                key = ((string)key).ToLower();
            }
            return key;
        }

        private bool _initCache = false;

        public void AddItems( IEnumerable<TItem> items)
        {
            foreach (var item in items)
            {
                AddItem(item);
            }
        }
        public void AddItem(TItem item)
        {
            var key = getKey(item);
            _dict[key] = item.ID;
        }



        private void fillCacheFromDb()
        {
            _dict.Clear();
            var items = factory.GetAllItemsInRepository().Cast<TItem>();
            AddItems(items);
            this.factory.OnItemUpdate += new DbObjects.General.OnItemUpdateHandler(factory_OnItemUpdate);
            _initCache = true;
                
        }

        void factory_OnItemUpdate(IBaseDbObject item, Enums.UPDATE_TYPE updateType)
        {
            if (updateType == Enums.UPDATE_TYPE.CreateNew || updateType == Enums.UPDATE_TYPE.Update)
            {
                AddItem((TItem)item);
            }
            else if (updateType == Enums.UPDATE_TYPE.Delete) 
            {
                var key = getKey((TItem)item);
                _dict.Remove(key);
            }
        }

        public TItem GetItemForKey(object key)
        {
            if (!_initCache)
            {
                fillCacheFromDb();
            }
            if (_dict.ContainsKey(key))
            {
                long id = _dict[key];
                return this.factory.GetByPrimaryKey(id);
            }
            else
                return null;
        }

        #region IDisposable Members

        public void Dispose()
        {
            this.factory.OnItemUpdate -= factory_OnItemUpdate;
            
        }

        #endregion
    }
}
