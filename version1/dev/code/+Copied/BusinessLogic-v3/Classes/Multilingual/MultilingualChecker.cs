﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Modules.CultureDetailsModule;

namespace BusinessLogic_v3.Classes.Multilingual
{
    public class MultilingualChecker
    {

        public delegate void ItemToAddHandler(ICultureDetailsBase cultureDetails);
        public delegate void ItemRemovedHandler(IMultilingualContentInfo item);
        public event ItemToAddHandler ItemToAdd;
        public event ItemRemovedHandler ItemRemoved;
        private ICultureDetailsBaseFactory getCultureFactory()
        {
            return CultureDetailsBaseFactory.Instance;
            
        }
        private IEnumerable<ICultureDetailsBase> _cultureInfos = null;
        private void loadCultureInfosFromDb()
        {
           // var session = CS.General_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            _cultureInfos = getCultureFactory().GetAvailableLanguages(true);
            //CS.General_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
        }

        public void CheckItemCultureInfos(IMultilingualItem item, IEnumerable<ICultureDetailsBase> CultureInfos = null)
        {
            _cultureInfos = CultureInfos;


            //item.BonusCategory_CultureInfos.Clear();
            if (_cultureInfos == null)
            {
                loadCultureInfosFromDb();
                
            }

            List<IMultilingualContentInfo> itemCultureInfos = new List<IMultilingualContentInfo>();

            itemCultureInfos.AddRange(item.GetMultilingualContentInfos());


            var cmpResults = CS.General_v3.Util.ListUtil.CompareTwoListsForRequiredAndNonRequired<ICultureDetailsBase, IMultilingualContentInfo>(
                _cultureInfos, itemCultureInfos, ((culture, contentInfo) => contentInfo.CultureInfoID == culture.ID));

            for (int i = 0; i < cmpResults.ItemsToBeAdded.Count; i++)
            {
                var itemToAdd = cmpResults.ItemsToBeAdded[i];
                ItemToAdd(itemToAdd);
            }
            for (int i=0; i < cmpResults.ItemsToBeRemoved.Count;i++)
            {
                var itemToRemove = cmpResults.ItemsToBeRemoved[i];
                itemToRemove.Delete();
                if (ItemRemoved != null)
                    ItemRemoved(itemToRemove);
            }

        }
    }
}
