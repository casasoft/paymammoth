﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Multilingual.DataCopier
{
    /// <summary>
    /// Copies all empty fields of culture infos not of default-language, to their default language attribute
    /// </summary>
    public class MultilingualDataCopier
    {
        private List<Type> getAllFinalTypesThatAreCultureInfos()
        {
            var typeList = CS.General_v3.Util.ReflectionUtil.GetAllTypesWhichExtendOrImplement(typeof(IMultilingualContentInfo));
            foreach (var t in typeList)
            {
                var f = BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryForType(t);
                var itemList = f.FindAll();
                foreach (var item in itemList)
                {
                    parseItem(item);
                }

            }
            return typeList;
        }

        private void parseItem(DbObjects.Objects.IBaseDbObject item)
        {
            throw new NotImplementedException();
        }
        
    }
}
