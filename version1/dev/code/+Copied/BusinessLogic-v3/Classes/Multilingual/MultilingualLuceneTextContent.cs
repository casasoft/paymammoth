﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BusinessLogic_v3.Modules.CultureDetailsModule.HelperClasses;

namespace BusinessLogic_v3.Classes.Multilingual
{
    /// <summary>
    /// Contains information about a multilingual piece of text, including the culture of the given text
    /// </summary>
    public class MultilingualLuceneTextContent
    {
        public string Text { get; set; }
        public CultureCode Culture { get; set; }
        public MultilingualLuceneTextContent()
        {
            this.Culture = new CultureCode();
        }
        public MultilingualLuceneTextContent(CultureCode culture, string text)
        {
            this.Text = text;
            this.Culture = culture;
            
        }
        public string GetTextIncCulture()
        {
            return "{[" + this.Culture.GetCultureCode() + "]} " + this.Text;
        }
        private static readonly Regex regex_Culture = new Regex(@"\{\[(.*?)\]\} ", RegexOptions.Compiled);
        public void ParseText(string s)
        {
            if (this.Culture == null) this.Culture = new CultureCode();
                    
            if (s.StartsWith("{["))
            {//this is the culture of the text
                int len = 20;
                if (s.Length < len) len = s.Length;
                var match = regex_Culture.Match(s.Substring(0, len));
                if (match.Success)
                {
                    string cultureCodeStr = match.Groups[1].Value;
                    this.Culture.ParseFromString(cultureCodeStr);
                    
                    s = s.Substring(match.Length);
                }

            }
            this.Text = s;
        }
    }
}
