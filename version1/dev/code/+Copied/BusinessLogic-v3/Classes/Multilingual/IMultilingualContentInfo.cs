﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace BusinessLogic_v3.Classes.Multilingual
{
    public interface IMultilingualContentInfo : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
        long CultureInfoID { get; }
        BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase CultureInfo { get; }
        IBaseDbObject GetLinkedItem();
    }
}
