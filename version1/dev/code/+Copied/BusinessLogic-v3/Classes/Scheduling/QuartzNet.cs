﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;


namespace BusinessLogic_v3.Classes.Scheduling
{
    public static class QuartzNet
    {
        private static ILog _log = LogManager.GetLogger(typeof(QuartzNet));
        public static Quartz.ISchedulerFactory SchedulerFactory { get; internal set; }
        public static Quartz.IScheduler Scheduler { get; internal set; }
        static QuartzNet()
        {
            _log.Debug("QuartzNet - Initialise - Start");
            
            SchedulerFactory = new Quartz.Impl.StdSchedulerFactory();
            Scheduler = SchedulerFactory.GetScheduler();
            
            Scheduler.Start();
            _log.Debug("QuartzNet - Initialise - Finish");
        }
        
    }
}
