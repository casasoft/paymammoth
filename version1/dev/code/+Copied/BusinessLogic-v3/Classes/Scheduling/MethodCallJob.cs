﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;

namespace BusinessLogic_v3.Classes.Scheduling
{
    public class MethodCallJob : IJob
    {
        #region IJob Members
        public const string DATA_METHOD = "method";
        public void Execute(IJobExecutionContext context)
        {
            Action methodToCall = (Action)context.JobDetail.JobDataMap[DATA_METHOD];
            methodToCall();
            
        }

        #endregion
    }
}
