﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Globalization;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.CurrencyModule;

namespace BusinessLogic_v3.Classes.Culture
{
    public class DefaultSessionCurrentCultureInfoManager : BaseSessionCurrentCultureInfoManager, ISessionCurrentCultureInfoManager
    {
        private static readonly DefaultSessionCurrentCultureInfoManager _instance = new DefaultSessionCurrentCultureInfoManager();
        public static DefaultSessionCurrentCultureInfoManager Instance
        {
            get
            {
                return _instance;
            }
        }
        protected DefaultSessionCurrentCultureInfoManager(string sessionAndCookieIdentifier = "_DefaultSessionCultureManager_")
        {
            this.SessionAndCookie_Identifier = sessionAndCookieIdentifier;
          //  loadDefaultCulture();
        }
        private void loadDefaultCulture()
        {
            //var c = GetCulture();
        }

        private ICultureDetailsBaseFactory getCultureFactory()
        {
            return CultureDetailsBaseFactory.Instance;
        }

        public virtual ICultureDetailsBase GetCulture()
        {

            ICultureDetailsBase culture = null;
            long? cultureId = null;
            if (System.Web.HttpContext.Current != null)
                cultureId = this.CustomCultureOverrideIDForCurrentContext;
            if (cultureId.GetValueOrDefault() <= 0)
            {
                if (CS.General_v3.Util.PageUtil.CheckSessionExistsAtContext())
                {
                    cultureId = cultureCodeInSession;
                }
            }
            if (cultureId.GetValueOrDefault() <= 0)
            {
                if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
                {
                    cultureId = CS.General_v3.Util.PageUtil.GetCookieLongNullable(SessionAndCookie_Identifier + "Culture");
                }
                
            }
            if (cultureId.GetValueOrDefault() <= 0)
            {
                cultureId = CultureDetailsBase.Factory.GetDefaultCulture().ID;
            }
            if (cultureId.GetValueOrDefault() > 0)
            {
                culture = getCultureFactory().GetByPrimaryKey(cultureId.Value);
            }
            if (culture == null)
            {
                string defaultCode = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Culture_DefaultCultureCode);

                culture = getCultureFactory().GetCultureByCode(defaultCode);
            }
            return culture;
        }




        public virtual void SetCultureByCode(string code)
        {
            CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? cmpCode = CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639FromCode(code);
            var currCode = this.GetCulture().GetLanguageISOEnumValue();
            if (cmpCode.HasValue && cmpCode != currCode)
            {
                var culture = getCultureFactory().GetCultureByCode(cmpCode.Value);
                SetCulture(culture);
            }
        }

        protected string SessionAndCookie_Identifier= "";
        
        
        private long? cultureCodeInSession
        {
            get
            {
                return (long?)CS.General_v3.Util.PageUtil.GetSessionObject(SessionAndCookie_Identifier + "Culture");
            }
            set
            {
                if (CS.General_v3.Util.PageUtil.CheckSessionExistsAtContext())
                {
                    CS.General_v3.Util.PageUtil.SetSessionObject(SessionAndCookie_Identifier + "Culture", value);
                }
                long? id = value;
                if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext() && CS.General_v3.Util.PageUtil.CheckResponseExistsAtContext())
                {
                    CS.General_v3.Util.PageUtil.SetCookie(SessionAndCookie_Identifier + "Culture", id.ToString());
                }
            }
        }

        public virtual void SetCulture(ICultureDetailsBase culture)
        {
            long? id = null;
            if (culture != null) id = culture.ID;
            cultureCodeInSession = id;
        }

        private long? currencyCodeInSession
        {
            get
            {
                return (long?)CS.General_v3.Util.PageUtil.GetSessionObject(SessionAndCookie_Identifier + "Currency");
            }
            set
            {
                if (CS.General_v3.Util.PageUtil.CheckSessionExistsAtContext())
                {
                    CS.General_v3.Util.PageUtil.SetSessionObject(SessionAndCookie_Identifier + "Currency", value);
                }
                long? id = value;
                if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext() && CS.General_v3.Util.PageUtil.CheckResponseExistsAtContext())
                {
                    CS.General_v3.Util.PageUtil.SetCookie(SessionAndCookie_Identifier + "Currency", id.ToString());
                }
            }
        }
        public virtual ICurrencyBase GetCurrency()
        {
            ICurrencyBase currency = null;
            long? currencyID = this.CustomCurrencyOverrideIDForCurrentContext;
            if (currencyID.GetValueOrDefault()<=0)
                currencyID = currencyCodeInSession;
            if (currencyID.GetValueOrDefault() <= 0)
            {
                currencyID = CS.General_v3.Util.PageUtil.GetCookieLongNullable(SessionAndCookie_Identifier + "Currency");
            }
            if (currencyID.GetValueOrDefault() > 0)
            {
                currency =CurrencyBaseFactory.Instance.GetByPrimaryKey(currencyID.Value);
            }
            if (currency == null)
            {
                var culture = GetCulture();
                currency = culture.DefaultCurrency;

                //currency = BusinessLogic_v3.Modules.Factories.CurrencyFactory.GetDefaultCurrency();
                SetCurrency(currency);
                
            }
            return currency;
        }

        public virtual void SetCurrency(ICurrencyBase currency)
        {
            long? currencyId = null;
            if (currency != null)
                currencyId = currency.ID;
           

            _currentGlobalizationCurrencyHTML = null;
            _currentGlobalizationCurrencyUnicode = null;

            currencyCodeInSession = currencyId;
        }


        public virtual void SetCurrencyByCode(string code)
        {
            var cmpCode = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217FromCode(code);
            var currCode = this.GetCurrency().GetCurrencyISOAsEnumValue();
            if (cmpCode != currCode)
            {
                var currency = CurrencyBase.Factory.GetByCurrencyCode(code);
                SetCurrency(currency);
            }
        }

        private CultureInfo _currentGlobalizationCultureInfo = null;
        public override CultureInfo GetCurrentCultureFormatProvider()
        {
            if (_currentGlobalizationCultureInfo == null)
            {
                _currentGlobalizationCultureInfo = GetCulture().GetCultureGlobalizationInfo();
            }
            return _currentGlobalizationCultureInfo;
        }
        private NumberFormatInfo _currentGlobalizationCurrencyHTML = null;
        public override NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForHTML()
        {
            if (_currentGlobalizationCurrencyHTML == null)
            {
                CultureInfo currentCulture = GetCurrentCultureFormatProvider();
                NumberFormatInfo numInfo = (NumberFormatInfo)currentCulture.NumberFormat.Clone();
                var curr = GetCurrency();
                string htmlSymbol = CS.General_v3.Util.Text.HtmlEncode(curr.Symbol);
                numInfo.CurrencySymbol = htmlSymbol;
                _currentGlobalizationCurrencyHTML = numInfo;
            }
            return _currentGlobalizationCurrencyHTML;
        }
        private NumberFormatInfo _currentGlobalizationCurrencyUnicode = null;
        public override NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForUnicode()
        {
            if (_currentGlobalizationCurrencyUnicode == null)
            {
                CultureInfo currentCulture = GetCurrentCultureFormatProvider();
                NumberFormatInfo numInfo = (NumberFormatInfo)currentCulture.NumberFormat.Clone();
                numInfo.CurrencySymbol = GetCurrency().Symbol;
                _currentGlobalizationCurrencyUnicode = numInfo;
            }
            return _currentGlobalizationCurrencyUnicode;
        }
        public override CS.General_v3.Classes.PorterStemmer.StemmerInterface GetStemmerForCurrentCulture()
        {
            return this.GetCulture().GetStemmerForCulture();
            
            
        }



        //public override string FormatPriceAsHTML(double d, string specificCurrencySymbol = null)
        //{
        //    return CS.General_v3.Util.Number.FormatNumber(d, CS.General_v3.Util.Number.NUMBER_FORMAT_TYPE.Currency, GetCurrentCurrencyNumberFormatProviderForHTML());
        //}


        void ISessionCurrentCultureInfoManager.SetCurrency(ICurrencyBase currency)
        {
            this.SetCurrency((CurrencyBase)currency);
            
        }

        public long? CustomCurrencyOverrideIDForCurrentContext
        {
            get
            {
                return (long?)CS.General_v3.Util.PageUtil.GetContextObject("CustomCurrencyOverrideIDForCurrentContext");
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetContextObject("CustomCurrencyOverrideIDForCurrentContext", value);
            }
        }
        public long? CustomCultureOverrideIDForCurrentContext
        {
            get
            {
                return (long?)CS.General_v3.Util.PageUtil.GetContextObject("CustomCultureOverrideIDForCurrentContext");
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetContextObject("CustomCultureOverrideIDForCurrentContext", value);
            }
        }
        

        public override CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 GetCurrentCurrencyCode()
        {
            
            return this.GetCurrency().GetCurrencyISOAsEnumValue();
            
            
        }
        public override CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 GetCurrentLanguageCode()
        {
            return this.GetCulture().GetLanguageISOEnumValue();
            

            
        }


    }
}
