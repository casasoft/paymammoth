﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.CurrencyModule;
using CS.General_v3.Classes.CultureManager;
using CS.General_v3.Classes.PorterStemmer;

using System.Globalization;


namespace BusinessLogic_v3.Classes.Culture
{
    public interface ISessionCurrentCultureInfoManager : IBaseSessionCultureManager
    {
        long? CustomCultureOverrideIDForCurrentContext { get; set; }
        ICultureDetailsBase GetCulture();
        void SetCulture(ICultureDetailsBase culture);
        void SetCultureByCode(string cultureCode);
        long? CustomCurrencyOverrideIDForCurrentContext { get; set; }
        ICurrencyBase GetCurrency();
        void SetCurrency(ICurrencyBase currency);
        void SetCurrencyByCode(string currencyCode);

        



    }
}
