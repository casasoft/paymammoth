﻿using System;
using System.Globalization;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.CurrencyModule;

namespace BusinessLogic_v3.Classes.Culture 
{
    public interface ICultureManager
    {

        string FormatPriceAsHTML(double d, string specificCurrencySymbol = null);
        System.Globalization.CultureInfo GetCurrentCultureFormatProvider();
        System.Globalization.CultureInfo GetCultureFormatProvider(ICultureDetailsBase culture);
        System.Globalization.NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForHTML();
        System.Globalization.NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForUnicode();
        NumberFormatInfo GetCurrencyNumberFormatProviderForUnicode(ICultureDetailsBase culture, ICurrencyBase currency = null);
        

        CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 GetCurrentLanguageCode();
        CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 GetCurrentCurrencyCode();


        CS.General_v3.Classes.PorterStemmer.StemmerInterface GetStemmerForCurrentCulture();

        string FormatNumberWithCurrentCulture(double d, CS.General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE formatType);
        string FormatNumberWithCurrentCulture(double d, string formatType);
        string FormatNumberWithCulture(double d, CS.General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE formatType, ICultureDetailsBase culture, ICurrencyBase currency = null);

        string FormatDateTimeWithCurrentCulture(DateTime date, CS.General_v3.Util.Date.DATETIME_FORMAT formatType, bool showGmt = false, double? localGmtOffset = null, double? resultGmtOffset = null);
        string FormatDateTimeWithCulture(DateTime date, CS.General_v3.Util.Date.DATETIME_FORMAT formatType, ICultureDetailsBase culture, bool showGmt = false, double? localGmtOffset = null, double? resultGmtOffset = null);


        ICultureDetailsBase CustomCultureOverrideForCurrentContext { get; set; }
        ICultureDetailsBase GetCulture();
        void SetCulture(ICultureDetailsBase culture);
        void SetCultureByCode(string code, bool throwErrorIfCultureDoesNotExist = true);
        long? CustomCurrencyOverrideIDForCurrentContext { get; set; }
        ICurrencyBase GetCurrency();
        void SetCurrency(ICurrencyBase currency);
        void SetCurrencyByCode(string currencyCode);
    }
}
