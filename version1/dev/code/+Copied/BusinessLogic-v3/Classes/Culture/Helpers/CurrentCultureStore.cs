﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.CultureDetailsModule;

namespace BusinessLogic_v3.Classes.Culture.Helpers
{
    public class CurrentCultureStore
    {
        private ICultureDetailsBase _cachedCurrentCultureInContext
        {
            get { return CS.General_v3.Util.PageUtil.GetContextObject<ICultureDetailsBase>("DefaultCultureManager.getCachedCurrentCultureForContext"); }
            set { CS.General_v3.Util.PageUtil.SetContextObject("DefaultCultureManager.getCachedCurrentCultureForContext", value); }
        }
       
        [ThreadStatic]
        private static ICultureDetailsBase _m_currentThread_currentCulture = null;


        public ICultureDetailsBase GetCachedCurrentCultureForContext()
        {
            
            
            ICultureDetailsBase culture = null;
            //2012/jul/05 [karl] this was updated to store the entire culture, not just the id in session
            //2012/jul/05 [karl] this check was removed as it is used very frequently, and I think a thread static variable should be enough

            //if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
            //{
            //    culture = _cachedCurrentCultureInContext;
            //}
            //else
            //{
            culture = _m_currentThread_currentCulture;
            //}

            return culture;
        }
        public void SetCachedCurrentCultureInContext(ICultureDetailsBase culture)
        {
            //long? id = (culture != null ? (long?)culture.ID : null);
            _m_currentThread_currentCulture = culture;
            if (System.Web.HttpContext.Current != null)
            {
                _cachedCurrentCultureInContext = culture;
            }
        }

        public CurrentCultureStore()
        {
        
        }
        


        public void ResetCache()
        {
            _m_currentThread_currentCulture = null;
            
        }
    }
}
