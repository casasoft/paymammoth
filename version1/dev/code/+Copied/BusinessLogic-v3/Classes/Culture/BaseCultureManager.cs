﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.CurrencyModule;
using CS.General_v3.Classes.PorterStemmer;


namespace BusinessLogic_v3.Classes.Culture
{
    public abstract class BaseCultureManager 
    {
        /// <summary>
        /// Returns the current culture format provider
        /// </summary>
        /// <returns></returns>
        public abstract CultureInfo GetCurrentCultureFormatProvider();
        /// <summary>
        /// Returns the culture format provider for the given culture
        /// </summary>
        /// <param name="culture"></param>
        /// <returns></returns>
        public abstract CultureInfo GetCultureFormatProvider(ICultureDetailsBase culture);
        /// <summary>
        /// Returns the current currency number format provider, utilising the Html symbol for the currency code
        /// </summary>
        /// <returns></returns>
        public abstract NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForHTML();
        /// <summary>
        /// Returns the current currency number format provider, utilising Unicode to show the currency code
        /// </summary>
        /// <returns></returns>
        public abstract NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForUnicode();
        /// <summary>
        /// Returns the currency number format provider using the given culture, and if specified the custom currency
        /// </summary>
        /// <param name="culture">Culture to use</param>
        /// <param name="currency">Custom currency.  If left null, current default is taken</param>
        /// <returns></returns>
        public abstract NumberFormatInfo GetCurrencyNumberFormatProviderForUnicode(ICultureDetailsBase culture, ICurrencyBase currency = null);
        

        public abstract StemmerInterface GetStemmerForCurrentCulture();



        public string FormatNumberWithCurrentCulture(double d, CS.General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE formatType)
        {
            var culture = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture();
            return BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCulture(d, formatType, culture, null);

            return CS.General_v3.Util.NumberUtil.FormatNumber(d, formatType, GetCurrentCurrencyNumberFormatProviderForUnicode());
        }

        public string FormatNumberWithCulture(double d, CS.General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE formatType, ICultureDetailsBase culture, ICurrencyBase currency = null)
        {
            
            return CS.General_v3.Util.NumberUtil.FormatNumber(d, formatType, GetCurrencyNumberFormatProviderForUnicode(culture, currency));
        }
        
        public string FormatNumberWithCurrentCulture(double d, string formatType)
        {
            return d.ToString(formatType, GetCurrentCultureFormatProvider());
        }

        public string FormatDateTimeWithCurrentCulture(DateTime date, CS.General_v3.Util.Date.DATETIME_FORMAT formatType, bool showGmt = false, double? localGmtOffset = null, double? resultGmtOffset = null)
        {
            return CS.General_v3.Util.Date.FormatDateTime(date, formatType, GetCurrentCultureFormatProvider(), showGmt, localGmtOffset, resultGmtOffset);

        }

        public string FormatDateTimeWithCulture(DateTime date, CS.General_v3.Util.Date.DATETIME_FORMAT formatType, ICultureDetailsBase culture,
            bool showGmt = false, double? localGmtOffset = null, double? resultGmtOffset = null)
        {


            return CS.General_v3.Util.Date.FormatDateTime(date, formatType, GetCultureFormatProvider(culture), showGmt, localGmtOffset, resultGmtOffset);
            

        }

        public virtual string FormatPriceAsHTML(double d, CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 currencySymbol)
        {
            string currSymbol = CS.General_v3.Enums.ISO_ENUMS.GetCurrencySymbol(currencySymbol);
            if (string.IsNullOrEmpty(currSymbol))
            {
                currSymbol = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode(currencySymbol);
            }
            return FormatPriceAsHTML(d, currSymbol);
        }
        public virtual string FormatPriceAsHTML(double d, string specificCurrencySymbol = null)
        {
            return CS.General_v3.Util.NumberUtil.FormatNumber(d, CS.General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency, GetCurrentCurrencyNumberFormatProviderForHTML(), specificCurrencySymbol);
        }

        #region IBaseSessionCultureManager Members


        public virtual CS.General_v3. Enums.ISO_ENUMS.LANGUAGE_ISO639 GetCurrentLanguageCode()
        {
            return CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.English;
        }

        public virtual CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 GetCurrentCurrencyCode()
        {
            return CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro;
        }

        #endregion
    }
}
