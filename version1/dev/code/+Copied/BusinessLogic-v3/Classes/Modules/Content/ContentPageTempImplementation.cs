﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;

namespace BusinessLogic_v3.Classes.Modules.Content
{
    public class ContentPageTempImplementation : IContentPage
    {
        public string ThumbnailURL { get; set; }
        private ContentPageCommonFunctionality _commonFunctionality;
        public ContentPageTempImplementation()
        {
            VisibleInFrontend = true;
            _commonFunctionality = new ContentPageCommonFunctionality(this);
            this.Children = new List<ContentPageTempImplementation>();
            this.LastEditedOn = CS.General_v3.Util.Date.Now;
        }




        #region IContentPage Members

        public long ID { get; set; }
        public string Identifier { get; set; }

        public string HtmlText { get; set; }

        public string CustomLink { get; set; }

        public string Title { get; set; }


        public string GetThumbnailURL()
        {
            return ThumbnailURL;
        }


        public string FriendlyName { get; set; }
        public string Color { get; set; }

        public bool VisibleInFrontend { get; set; }

        public IList<ContentPageTempImplementation> Children { get; private set; }


        public bool ExcludeFromRewriteUrl { get; set; }

        public IContentPage ParentContentPage { get; set; }

        public JSObject GetAsJsObject(bool forFlash = false, int maxDepth = 0, int currDepth = 0)
        {
            return _commonFunctionality.GetJsObject(forFlash, maxDepth, currDepth);
        }

      


        public string GetUrl(bool fullyQualifiedUrl = true, string langCode = null)
        {
            return _commonFunctionality.GetUrl(fullyQualifiedUrl, langCode);
        }

        #endregion

        
        public IEnumerable<IContentPage> GetChildPages()
        {
            return (IEnumerable<IContentPage>)this.Children;
            
        }

        public bool ContainsContent()
        {
            return this._commonFunctionality.ContainsContent();
            
        }

        public bool HasChildPages()
        {
            return this._commonFunctionality.HasChildPages();
            
        }

        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }




        public void AddChildPage(ContentPageTempImplementation cp)
        {
            this.Children.Add(cp);
        }
        public void AddChildPage(IContentPage cp)
        {
            AddChildPage((ContentPageTempImplementation)cp);
            
        }
        
        public DateTime LastEditedOn { get; set; }


        public bool DoNotShowLastEditedOn
        {
            get;
            set;
        }


        string IHierarchy.Href
        {
            get
            {
               return this.GetUrl();
            }
        }

        CS.General_v3.Enums.HREF_TARGET IHierarchy.HrefTarget
        {
            get
            {
                return CS.General_v3.Enums.HREF_TARGET.Self;
            }
        }

        CS.General_v3.Controls.WebControls.Specialized.Hierarchy.IHierarchy IHierarchy.ParentHierarchy
        {
            get
            {
                return this.ParentContentPage;
            }
        }

        bool IHierarchy.Selected
        {
            get
            {
                //TODO: Karl [19/1/2011] - Load this from route data
                return true;
            }

        }

        string IHierarchy.GetFullHierarchyName(string delimiter, bool reverseOrder, bool includeRoot)
        {
            throw new NotImplementedException();
        }

        bool IHierarchy.Visible
        {
            get
            {
                return this.VisibleInFrontend;
            }
        }


        IEnumerable<IHierarchy> IHierarchy.GetChildren()
        {
            return this.GetChildPages();
        }
    }
}
