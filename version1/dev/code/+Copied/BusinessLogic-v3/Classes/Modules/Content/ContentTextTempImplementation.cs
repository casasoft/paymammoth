﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Modules.Content
{
    public class ContentTextTempImplementation : IContentText
    {

        #region IContentText Members

        public string Identifier { get; set; }


        public string PlainText { get; set; }

        public string HtmlText { get; set; }

        public string Title
        {
            get;
            set;
        }

        #endregion

       
    }
}
