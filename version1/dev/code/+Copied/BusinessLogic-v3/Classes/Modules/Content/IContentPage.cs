﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;

namespace BusinessLogic_v3.Classes.Modules.Content
{
    public interface IContentPage : IHierarchy
    {
        long ID { get; }
        string Identifier { get; }
        string HtmlText { get; }
        string FriendlyName { get; }
        string Title { get; }
        string CustomLink { get; }
        string Color { get; }

        DateTime LastEditedOn { get; }
        bool DoNotShowLastEditedOn { get; }

        void AddChildPage(IContentPage cp);
        
        IEnumerable<IContentPage> GetChildPages();
        bool VisibleInFrontend { get; }
        /// <summary>
        /// Whether content page has got content i.e. clickable
        /// </summary>
        /// <returns></returns>
        bool ContainsContent();
        /// <summary>
        /// Whether includes children pages
        /// </summary>
        /// <returns></returns>
        bool HasChildPages();
        
        string GetUrl(bool fullyQualifiedUrl = true, string langCode = null);
        string GetThumbnailURL();
        string MetaKeywords { get; set; }
        string MetaDescription { get; set; }
        bool ExcludeFromRewriteUrl { get; }

        IContentPage ParentContentPage { get; }

        JSObject GetAsJsObject(bool forFlash = false, int maxDepth = 0, int currDepth = 0);
    }
}
