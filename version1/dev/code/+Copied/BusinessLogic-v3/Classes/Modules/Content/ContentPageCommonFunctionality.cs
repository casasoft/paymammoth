﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Classes.Routing;

namespace BusinessLogic_v3.Classes.Modules.Content
{
    public class ContentPageCommonFunctionality
    {
        public static bool ROUTING_INCLUDE_CULTURE = false;
        public static string ROUTING_ROUTE_URL_PREPEND = "Content";
        public static string ROUTING_PARAM_CULTURE = "Culture";
        public static string ROUTING_PARAM_CULTURE_CONSTRAINT = "[a-zA-Z]{2}";
        public static string ROUTING_PARAM_TITLE = "Title";
        public static string ROUTING_PARAM_ID = "ID";
        public static string ROUTING_PAGE_NAME = "/contentPage.aspx";
        public static string ROUTING_ROUTE_NAME = "ContentPages";

        private static bool _MAPPED_ROUTE = false;


        private IContentPage _contentPage;
        public ContentPageCommonFunctionality(IContentPage contentPage)
        {
            _contentPage = contentPage;
        }
        public bool ContainsContent()
        {
            return !string.IsNullOrEmpty(_contentPage.HtmlText);
        }

        public JSObject GetJsObject(bool forFlash = false, int maxDepth = 0, int currDepth = 0)
        {
            JSObject obj = forFlash ? new JSObjectFlash() : new JSObject();

            string pageTitle = CS.General_v3.Util.Text.HtmlEncode(_contentPage.Title);
            //pageTitle = this.PageTitle;
            obj.AddProperty("title", pageTitle);

            if (!string.IsNullOrEmpty(_contentPage.Color))
            {
                obj.AddProperty("color", CS.General_v3.Util.Colors.ColorUtil.ColorToInt(_contentPage.Color));
            }
            if (!string.IsNullOrEmpty(_contentPage.Identifier))
            {
                obj.AddProperty("identifier", _contentPage.Identifier);
            }

            obj.AddProperty("id", _contentPage.ID);

            obj.AddProperty("url", this.GetUrl());

            JSArray aChildren = forFlash ? new JSArrayFlash() : new JSArray();
            if (maxDepth == 0 || currDepth < maxDepth)
            {
                var childPages = _contentPage.GetChildPages();
                foreach (var childPg in childPages)
                {
                    if (childPg.VisibleInFrontend)
                    {
                        aChildren.AddItem(childPg.GetAsJsObject(forFlash, maxDepth, currDepth + 1));
                            
                            //.GetJsObjectFlash(maxDepth, currDepth + 1) : childPages[i].GetJsObject(maxDepth, currDepth + 1));
                    }
                }
                obj.AddProperty("childPages", aChildren);
            }

            return obj;
        }

        public string GetUrl(bool fullyQualifiedUrl = true, string cultureCode = null)
        {

            string url = "";
            if (!string.IsNullOrEmpty(_contentPage.CustomLink))
            {

                url = this._contentPage.CustomLink;
                if (fullyQualifiedUrl && (!url.ToLower().StartsWith("http://") && !url.ToLower().StartsWith("https://")))
                {
                    if (url.StartsWith("/")) url = url.Substring(1);
                    url = CS.General_v3.Util.PageUtil.GetBaseURL() + url;
                }
            }
            else
            {
                string s = "";
                IContentPage curr = _contentPage;
                if (curr.ID == 1004)
                {
                    int k = 5;
                }

                MapRoute();

                while (curr != null && curr.ParentContentPage != null)
                {
                    if (!curr.ExcludeFromRewriteUrl)
                    {
                        if (!string.IsNullOrEmpty(s))
                            s = "-" + s;
                        s = CS.General_v3.Util.Text.TxtForRewrite(curr.FriendlyName) + s;
                    }
                    curr = curr.ParentContentPage;
                }
                if (string.IsNullOrEmpty(s))
                    s = "ContentPage";
                var rvd = new CS.General_v3.Classes.Routing.MyRouteValueDictionary(ROUTING_PARAM_TITLE, s, ROUTING_PARAM_ID, _contentPage.ID.ToString());

                

                if (ROUTING_INCLUDE_CULTURE)
                {
                    if (cultureCode == null && BusinessLogic_v3.Settings.SettingsMain.CurrentCultureManager.GetCulture() != null)
                    {
                        cultureCode = BusinessLogic_v3.Settings.SettingsMain.CurrentCultureManager.GetCulture().LanguageISOCode;
                    }

                    rvd[ROUTING_PARAM_CULTURE] = cultureCode;
                }

                url = CS.General_v3.Util.RoutingUtil.GetRouteUrl(
                    ROUTING_ROUTE_NAME,
                       rvd, fullyQualifiedUrl);
            }
            return url;
        }
        public static void MapRoute(bool forceMap = false)
        {
            if (forceMap || !_MAPPED_ROUTE)
            {
                _MAPPED_ROUTE = true;
                bool includeLanguage = ROUTING_INCLUDE_CULTURE;

                string routeURL = CS.General_v3.Util.Text.AppendStrings("/",
                    ROUTING_ROUTE_URL_PREPEND,
                    (includeLanguage ? "{" + ROUTING_PARAM_CULTURE + "}" : null),
                    "{" + ROUTING_PARAM_TITLE + "}",
                    "{" + ROUTING_PARAM_ID + "}");

                var rvd = new MyRouteValueDictionary(ROUTING_PARAM_ID, "[0-9]+");
                if (includeLanguage)
                {
                    rvd[ROUTING_PARAM_CULTURE] = ROUTING_PARAM_CULTURE_CONSTRAINT;
                }



                CS.General_v3.Util.RoutingUtil.MapPageRoute(
                    ROUTING_ROUTE_NAME, routeURL,
                    ROUTING_PAGE_NAME, null,
                    rvd);
            }
        }


        public bool HasChildPages()
        {
            return this._contentPage.GetChildPages().Count() > 0;
            
        }
    }
}
