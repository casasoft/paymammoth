﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.Exceptions;

namespace BusinessLogic_v3.Classes.Exceptions
{
    public class NotEnoughFundsException : Exception
    {

        public NotEnoughFundsException(string msg = null) 
            : base(msg)
        {

        }

        public NotEnoughFundsException()
            : base()
        {
        
        }
    }
}
