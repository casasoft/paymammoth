﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Exceptions
{
    public class ContentPageNotSetException : Exception
    {
        public ContentPageNotSetException(string msg = null) 
            : base(msg)
        {

        }

        public ContentPageNotSetException()
            : base()
        {
        
        }
    }
}
