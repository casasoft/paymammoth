﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.Exceptions;

namespace BusinessLogic_v3.Classes.Exceptions
{
    public class ResponsibleLimitsReachedException : Exception
    {

        public ResponsibleLimitsReachedException(string msg = null) 
            : base(msg)
        {

        }

        public ResponsibleLimitsReachedException()
            : base()
        {
        
        }
    }
}
