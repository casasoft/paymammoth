﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Exceptions
{
    public class AutoRenewNotAllowedException : Exception
    {
        public AutoRenewNotAllowedException(string msg = null) 
            : base(msg)
        {

        }

        public AutoRenewNotAllowedException()
            : base()
        {
        
        }
    }
}
