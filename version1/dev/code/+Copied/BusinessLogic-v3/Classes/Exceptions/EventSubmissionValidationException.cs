﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Exceptions
{
    public class EventSubmissionValidationException : Exception
    {
        public EventSubmissionValidationException(string msg = null) 
            : base(msg)
        {

        }

        public EventSubmissionValidationException()
            : base()
        {
        
        }
    }
}
