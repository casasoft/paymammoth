﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.Exceptions;

namespace BusinessLogic_v3.Classes.Exceptions
{
    public class PropertyUpdateException : OperationException
    {

        public PropertyUpdateException(string msg = null, CS.General_v3.Enums.STATUS_MSG_TYPE status = CS.General_v3.Enums.STATUS_MSG_TYPE.Error) 
            : base(msg, status)
        {

        }

        public PropertyUpdateException(OperationResult result) : base(result)
        {
        
        }
    }
}
