﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.Exceptions;

namespace BusinessLogic_v3.Classes.Exceptions
{
    public class NoNhibernateSessionException : Exception
    {

        public NoNhibernateSessionException(string msg = null) 
            : base(msg)
        {

        }

        public NoNhibernateSessionException()
            : base("Nhibernate Session does not exist at context - make sure that you create one before you try to access NH objects")
        {
        
        }
    }
}
