﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Exceptions.DataSourcesExceptions
{
    public class MaltaAirportWeatherFailedConditionParsingException : Exception
    {
        public MaltaAirportWeatherFailedConditionParsingException(string msg = null) 
            : base(msg)
        {

        }

        public MaltaAirportWeatherFailedConditionParsingException(string msg, Exception innerException)
            : base(msg, innerException)
        {

        }

        public MaltaAirportWeatherFailedConditionParsingException()
            : base()
        {
        
        }
    }
}
