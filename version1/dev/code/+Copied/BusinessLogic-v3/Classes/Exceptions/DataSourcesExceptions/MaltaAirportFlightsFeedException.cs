﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Exceptions.DataSourcesExceptions
{
    public class MaltaAirportFlightsFeedException : Exception
    {
        public MaltaAirportFlightsFeedException(string msg = null) 
            : base(msg)
        {

        }

        public MaltaAirportFlightsFeedException(string msg, Exception innerException)
            : base(msg, innerException)
        {

        }

        public MaltaAirportFlightsFeedException()
            : base()
        {
        
        }
    }
}
