﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Exceptions.DataSourcesExceptions
{
    public class MaltaAirportFlightsInvalidLineException : Exception
    {
        public MaltaAirportFlightsInvalidLineException(string msg = null) 
            : base(msg)
        {

        }

        public MaltaAirportFlightsInvalidLineException(string msg, Exception innerException)
            : base(msg, innerException)
        {

        }

        public MaltaAirportFlightsInvalidLineException()
            : base()
        {
        
        }
    }
}
