﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Exceptions.DataSourcesExceptions
{
    public class MaltaAirportWeatherFeedException : Exception
    {
        public MaltaAirportWeatherFeedException(string msg = null) 
            : base(msg)
        {

        }

        public MaltaAirportWeatherFeedException(string msg, Exception innerException)
            : base(msg, innerException)
        {

        }

        public MaltaAirportWeatherFeedException()
            : base()
        {
        
        }
    }
}
