﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Exceptions
{
    public class InvalidSortByValueException : Exception
    {
        public InvalidSortByValueException(string msg = null) 
            : base(msg)
        {

        }

        public InvalidSortByValueException()
            : base()
        {
        
        }
    }
}
