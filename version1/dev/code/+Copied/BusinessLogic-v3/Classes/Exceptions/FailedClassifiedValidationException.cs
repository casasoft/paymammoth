﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Exceptions
{
    public class FailedClassifiedValidationException : Exception
    {
        public FailedClassifiedValidationException(string msg = null) 
            : base(msg)
        {

        }

        public FailedClassifiedValidationException()
            : base()
        {
        
        }
    }
}
