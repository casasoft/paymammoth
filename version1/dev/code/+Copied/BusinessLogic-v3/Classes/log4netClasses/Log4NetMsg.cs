﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace BusinessLogic_v3.Classes.log4netClasses
{
    public class Log4NetMsg
    {

        private int _Item = 0;
        public int Item
        {
            get { return _Item; }
            set { _Item = value; }
        }

        private DateTime _TimeStamp = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        public DateTime TimeStamp
        {
            get { return _TimeStamp; }
            set { _TimeStamp = value; }
        }

        public int Index { get; set; }

        
        private string _Level = string.Empty;
        public string Level
        {
            get { return _Level; }
            set { _Level = value; }
        }
        public BusinessLogic_v3.Classes.log4netClasses.GeneralLog4Net.LOG4NET_TYPE LevelType
        {
            get { return CS.General_v3.Util.EnumUtils.EnumValueNullableOf<BusinessLogic_v3.Classes.log4netClasses.GeneralLog4Net.LOG4NET_TYPE>(this.Level).GetValueOrDefault(BusinessLogic_v3.Classes.log4netClasses.GeneralLog4Net.LOG4NET_TYPE.INFO); }
        }

        private string _Thread = string.Empty;
        public string Thread
        {
            get { return _Thread; }
            set { _Thread = value; }
        }

        private string _Message = string.Empty;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        private string _MachineName = string.Empty;
        public string MachineName
        {
            get { return _MachineName; }
            set { _MachineName = value; }
        }

        private string _UserName = string.Empty;
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        private string _HostName = string.Empty;
        public string HostName
        {
            get { return _HostName; }
            set { _HostName = value; }
        }

        private string _App = string.Empty;
        public string App
        {
            get { return _App; }
            set { _App = value; }
        }

        private string _Throwable = string.Empty;
        public string Throwable
        {
            get { return _Throwable; }
            set { _Throwable = value; }
        }

        private string _Class = string.Empty;
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }

        private string _Method = string.Empty;
        public string Method
        {
            get { return _Method; }
            set { _Method = value; }
        }

        private string _File = string.Empty;
        public string File
        {
            get { return _File; }
            set { _File = value; }
        }

        private string _Line = string.Empty;
        public string Line
        {
            get { return _Line; }
            set { _Line = value; }
        }

        public override string ToString()
        {
            return this.LoggerName + " - " + this.Message;
        }



        public string LoggerName { get; set; }

        public string ClassAndMethod
        {
            get { return this.Class + "." + this.Method; }
        }
    }
}
