﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.log4netClasses
{
    public class Log4NetMsgSearcher
    {
        public Log4NetMsgSearchParams SearchParams { get; set; }
        private List<Log4NetMsg> _data = null;
        public Log4NetMsgSearcher(IEnumerable<Log4NetMsg> data)
        {
            _data = data.ToList();
            this.SearchParams = new Log4NetMsgSearchParams();
        }

        
        


            protected bool checkIfStringMatch(IEnumerable<string> criteria, string value)
        {
            value = value ?? "";
            value = value.ToLower();
            foreach (var crit in criteria)
            {
                string sCrit = crit.ToLower();
                if (value.Contains(sCrit))
                    return true;
            }
            return false;
        }

        public List<Log4NetMsg> Search(Log4NetMsgSearchParams searchParams = null)
        {
            if (searchParams != null)
                this.SearchParams = searchParams;

            for (int i = 0; i < _data.Count; i++)
            {
                var item = _data[i];
                bool ok = true;
                if (this.SearchParams.LevelTypes.Count > 0)
                {
                    if (item.LevelType == GeneralLog4Net.LOG4NET_TYPE.FATAL)
                    {
                        int k = 5;
                    }
                    if (!this.SearchParams.LevelTypes.Contains(item.LevelType))
                    {
                        ok = false;
                    }
                }
                if (this.SearchParams.LoggerName.Count > 0)
                {
                    if (!checkIfStringMatch(this.SearchParams.LoggerName, item.LoggerName))
                    {
                        ok = false;
                    }
                }
                if (this.SearchParams.Message.Count > 0)
                {
                    if (!checkIfStringMatch(this.SearchParams.Message, item.Message))
                    {
                        ok = false;
                    }
                }
                if (this.SearchParams.ThreadID.Count > 0)
                {
                    if (!checkIfStringMatch(this.SearchParams.ThreadID, item.Thread))
                    {
                        ok = false;
                    }
                }
                if (this.SearchParams.DateFrom.HasValue)
                {
                    if (item.TimeStamp < this.SearchParams.DateFrom)
                        ok = false;
                }
                if (this.SearchParams.DateTo.HasValue)
                {
                    if (item.TimeStamp > this.SearchParams.DateTo)
                        ok = false;
                }
                if (!ok)
                {
                    _data.RemoveAt(i);
                    i--;
                }
            }

            return CS.General_v3.Util.Paging.SplitList(_data, this.SearchParams.PagingInfo.PageNo, this.SearchParams.PagingInfo.ShowAmount).ToList();



        }
    }
}
