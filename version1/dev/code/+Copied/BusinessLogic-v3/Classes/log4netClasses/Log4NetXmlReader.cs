﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using System.Xml;
using CS.General_v3.Classes.HelperClasses;

namespace BusinessLogic_v3.Classes.log4netClasses
{
    [Serializable]
    public class Log4NetXmlReader
    {
        public static Log4NetXmlReader InstanceInSession
        {
            get 
            {
                var reader = CS.General_v3.Util.PageUtil.GetSessionObject<Log4NetXmlReader>("Log4NetXmlReader_Session");
                if (reader == null)
                {
                    reader = new Log4NetXmlReader();
                    InstanceInSession = reader;
                }
                return reader;
            }
            private set { CS.General_v3.Util.PageUtil.SetSessionObjectFromSerializableItem("Log4NetXmlReader_Session", value); }
        }



        private List<Log4NetMsg> result = null;
        public List<Log4NetMsg> GetLastResult()
        {
            return result.ToList();
        }

        private Log4NetXmlReader()
        {
            this.result = new List<Log4NetMsg>();
        }
        public void test()
        {
            string path = @"c:\log.txt";
            ReadXmlFile(path);


        }
        


        private OperationResult _operationResult = null;
        private int currIndex = 0;
        private void loadFile(string path)
        {
            
            
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            string sXml = string.Empty;
            string sBuffer = string.Empty;
            int iIndex = 1;

            
            try
            {
                string tmpFile = CS.General_v3.Util.IO.GetTempFilename(".xml");
                File.Copy(path, tmpFile,true);

                FileStream oFileStream = new FileStream(tmpFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite);
                StreamReader oStreamReader = new StreamReader(oFileStream);
                sBuffer = string.Format("<root>{0}</root>", oStreamReader.ReadToEnd());
                oStreamReader.Close();
                oFileStream.Close();

                #region Read File Buffer
                ////////////////////////////////////////////////////////////////////////////////
                StringReader oStringReader = new StringReader(sBuffer);
                XmlTextReader oXmlTextReader = new XmlTextReader(oStringReader);
                oXmlTextReader.Namespaces = false;
                while (oXmlTextReader.Read())
                {
                    if ((oXmlTextReader.NodeType == XmlNodeType.Element) && (oXmlTextReader.Name == "log4j:event"))
                    {
                        Log4NetMsg logentry = new Log4NetMsg();

                        logentry.Item = iIndex;

                        double dSeconds = Convert.ToDouble(oXmlTextReader.GetAttribute("timestamp"));
                        logentry.TimeStamp = dt.AddMilliseconds(dSeconds).ToLocalTime();
                        logentry.Thread = oXmlTextReader.GetAttribute("thread");
                        logentry.LoggerName = oXmlTextReader.GetAttribute("logger");

                        #region get level
                        ////////////////////////////////////////////////////////////////////////////////
                        logentry.Level = oXmlTextReader.GetAttribute("level");
                        
                        ////////////////////////////////////////////////////////////////////////////////
                        #endregion

                        #region read xml
                        ////////////////////////////////////////////////////////////////////////////////
                        while (oXmlTextReader.Read())
                        {
                            if (oXmlTextReader.Name == "log4j:event")   // end element
                                break;
                            else
                            {
                                switch (oXmlTextReader.Name)
                                {
                                    case ("log4j:message"):
                                        {
                                            logentry.Message = oXmlTextReader.ReadString();
                                            break;
                                        }
                                    case ("log4j:data"):
                                        {
                                            switch (oXmlTextReader.GetAttribute("name"))
                                            {
                                                case ("log4jmachinename"):
                                                    {
                                                        logentry.MachineName = oXmlTextReader.GetAttribute("value");
                                                        break;
                                                    }
                                                case ("log4net:HostName"):
                                                    {
                                                        logentry.HostName = oXmlTextReader.GetAttribute("value");
                                                        break;
                                                    }
                                                case ("log4net:UserName"):
                                                    {
                                                        logentry.UserName = oXmlTextReader.GetAttribute("value");
                                                        break;
                                                    }
                                                case ("log4japp"):
                                                    {
                                                        logentry.App = oXmlTextReader.GetAttribute("value");
                                                        break;
                                                    }
                                            }
                                            break;
                                        }
                                    case ("log4j:throwable"):
                                        {
                                            logentry.Throwable = oXmlTextReader.ReadString();
                                            break;
                                        }
                                    case ("log4j:locationInfo"):
                                        {
                                            logentry.Class = oXmlTextReader.GetAttribute("class");
                                            logentry.Method = oXmlTextReader.GetAttribute("method");
                                            logentry.File = oXmlTextReader.GetAttribute("file");
                                            logentry.Line = oXmlTextReader.GetAttribute("line");
                                            break;
                                        }
                                }
                            }
                        }
                        ////////////////////////////////////////////////////////////////////////////////
                        #endregion
                        this.result.Add(logentry);
                        
                        iIndex++;

                   
                    }
                }
                ////////////////////////////////////////////////////////////////////////////////
                #endregion

                File.Delete(tmpFile);
            }
            catch (Exception ex)
            {
                _operationResult.AddException(ex);
            }


           
        }

        private void init()
        {
            _operationResult = new OperationResult();
            this.result.Clear();
        }
        private void sortResults()
        {
            this.result = CS.General_v3.Util.ListUtil.SortByMultipleComparers(this.result,
                ((x1,x2)=>-(x1.TimeStamp.CompareTo(x2.TimeStamp))),
                ((x1,x2)=>-(x1.Item.CompareTo(x2.Item)))).ToList();

            //this.result.Sort((i1, i2) =>
            //                     {
            //                         int timestampCmp = -i1.TimeStamp.CompareTo(i2.TimeStamp);
            //                         if (timestampCmp == 0)
            //                             return -i1.Item.CompareTo(i2.Item); //compare by item # in file
            //                         else
            //                             return timestampCmp;
                                     
            //                     });
            for (int i = 0; i < this.result.Count; i++)
            {
                var item = this.result[i];
                item.Index = i;
            }
        }

        private List<string> lastFilePaths = new List<string>();

        public List<Log4NetMsg> ReadXmlFiles(params string[] filePaths)
        {
            lastFilePaths.Clear();
            init();
            foreach (var p in filePaths)
            {
                lastFilePaths.Add(p);
                loadFile(p);
            }
            onEnd();
            return result;
        }

        public void Refresh()
        {
            ReadXmlFiles(this.lastFilePaths.ToArray());
        }

        private void onEnd()
        {
            sortResults();
        }


        public List<Log4NetMsg> ReadXmlFile(string filePath)
        {
            return ReadXmlFiles(filePath);
            
            
        }


       

    }
}
