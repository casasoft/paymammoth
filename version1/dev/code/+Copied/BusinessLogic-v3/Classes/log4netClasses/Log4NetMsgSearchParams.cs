﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.log4netClasses
{
    public class Log4NetMsgSearchParams : BusinessLogic_v3.Classes.URL.ListingURLParser.ListingURLParser
    {
       // public int PageNo { get; set; }
      //  public int ShowAmt { get; set; }
        
        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _LevelTypes = null;
        public List<BusinessLogic_v3.Classes.log4netClasses.GeneralLog4Net.LOG4NET_TYPE> LevelTypes { get; set; }


        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _LoggerName = null;
        public List<string> LoggerName { get; set; }

        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _ThreadID = null;
        public List<string> ThreadID { get; set; }

        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _Message = null;
        public List<string> Message { get; set; }

        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _DateFrom = null;
        public DateTime? DateFrom { get; set; }
        private CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString _DateTo = null;
        public DateTime? DateTo { get; set; }

            public Log4NetMsgSearchParams(string customUrl = null) : base("", customUrl)
        {
            this._LevelTypes = new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "levelTypes");
            this._LoggerName = new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "loggerName");
            this._Message = new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "message");
            this._ThreadID= new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "threadID");
            _DateFrom = new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "dateFrom");
            _DateTo= new CS.General_v3.Classes.URL.URLParser.v1.URLParser_ParamInfoQueryString(this, "dateTo");
            this.PagingInfo.DefaultShowAmtValue = 250;
            loadInitialValues();
            
        }



        protected override string getRouteName()
        {
            return BusinessLogic_v3.Classes.Cms.CmsObjects.CmsRoutesMapper.Route_LogFiles_ViewLogEntries;
            
        }

        protected override string getPageURL()
        {
            return BusinessLogic_v3.Classes.Cms.CmsObjects.CmsRoutesMapper.Instance.GetLogFiles_ViewPage();
            
        }
        private void loadInitialValues()
        {
            this.LevelTypes = _LevelTypes.GetValueAsListOfEnums<BusinessLogic_v3.Classes.log4netClasses.GeneralLog4Net.LOG4NET_TYPE>();

            this.LoggerName = _LoggerName.GetValueAsListOfString();
            this.Message = _Message.GetValueAsListOfString();
            this.ThreadID = _ThreadID.GetValueAsListOfString();
            this.DateFrom = _DateFrom.GetValueAsDateTimeNullable();
            this.DateTo= _DateFrom.GetValueAsDateTimeNullable();
        }

        protected override void updateURLParameterValues()
        {
            _LevelTypes.SetValueFromListOfEnum < BusinessLogic_v3.Classes.log4netClasses.GeneralLog4Net.LOG4NET_TYPE>(this.LevelTypes);
            _LoggerName.SetValue(this.LoggerName);
            _Message.SetValue(this.Message);
            _ThreadID.SetValue(this.ThreadID);
            _DateFrom.SetValue(this.DateFrom);
            _DateTo.SetValue(this.DateTo);
           

        }
    }
}
