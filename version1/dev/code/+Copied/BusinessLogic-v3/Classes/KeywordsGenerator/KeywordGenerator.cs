﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.KeywordModule;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductVariationModule;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using BusinessLogic_v3.Util;
using Iesi.Collections.Generic;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;

using System.Threading;
using log4net;

namespace BusinessLogic_v3.Classes.KeywordsGenerator
{
    public class KeywordGenerator
    {
        ILog _log = log4net.LogManager.GetLogger(typeof(KeywordGenerator));
        public static KeywordGenerator CreateInstance()
        {
            Type lastType = CS.General_v3.Util.ReflectionUtil.GetLastTypeInInheritanceChain(typeof(KeywordGenerator), null);
            return (KeywordGenerator)CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructor(lastType);
        }
        private static readonly object _padlock = new object();
        private CultureDetailsBase defaultCulture = null;
        private Dictionary<string, KeywordItem> _values = new Dictionary<string, KeywordItem>();
        public KeywordItem GetKeywordItem(CultureDetailsBase culture, string keyword)
        {
            string key = KeywordItem.GetUniqueKey(culture, keyword);
            if (_values.ContainsKey(key))
            {
                ISession s;
                

                return _values[key];
            }
            else
            {
                KeywordItem k = new KeywordItem(culture, keyword);
                _values[key] = k;
                return k;
            }
            
        }
        protected void _addKeywords(IEnumerable<CultureDetailsBase> cultures, string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                string[] tokens = text.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var t in tokens)
                {
                    if (!string.IsNullOrWhiteSpace(t))
                    {
                        foreach (var c in cultures)
                        {
                            var k = GetKeywordItem(c, t);
                            k.Frequency++;
                        }
                    }
                }
            }

        }

        protected void addKeywords(CultureDetailsBase culture, string text)
        {
            List<CultureDetailsBase> list = new List<CultureDetailsBase>();
            if (culture != null)
                list.Add(culture);
            else
            {
                var allCultures = CultureDetailsBase.Factory.FindAll();
                list.AddRange(allCultures);
            }
            _addKeywords(list, text);
        }

        protected virtual void generateKeywordFromProduct(ProductBase item)
        {
            foreach (var details in item.Product_CultureInfos)
            {
                addKeywords(details.CultureInfo, details.Description);
                addKeywords(details.CultureInfo, details.Title);
            }
        }
        protected virtual void generateKeywordFromItem(ProductVariationBase item)
        {
            if (item.Colour != null)
                addKeywords(defaultCulture, item.Colour.Title);
            if (item.Size != null)
                addKeywords(defaultCulture, item.Size);
            //foreach (var details in item.Item_CultureInfos)
            //{
            //    addKeywords(details.CultureInfo, details..Colour);
            //    addKeywords(details.CultureInfo, details.Size);
            //}
        }
        protected virtual void generateKeywordFromBrand(BrandBase brand)
        {
            addKeywords(null, brand.Title);
            foreach (var details in brand.Brand_CultureInfos)
            {
                addKeywords(details.CultureInfo, details.Description);
                
            }
        }
        protected virtual void generateKeywordFromCategory(CategoryBase item)
        {
            foreach (var details in item.Category_CultureInfos)
            {
                addKeywords(details.CultureInfo, details.Description);
                addKeywords(details.CultureInfo, details.TitlePlural);
                addKeywords(details.CultureInfo, details.TitleSingular);
            }
        }


        private void generateKeywordsFromDbItems<T>(IQueryOver<T> query, System.Linq.Expressions.Expression<Func<T, long>> selector,
            Action<T> methodToCall) where T : class
        {
            int pageSize = 100;
            int pageNum = 1;

            
            IEnumerable<T> results = null;
            do
            {
                results = nHibernateUtil.LimitQueryByPrimaryKeysAndReturnResult<T>(query,
                                                                                               pageNum,
                                                                                              pageSize);
                foreach (var item in results)
                {
                    methodToCall(item);
                }
                pageNum++;

            } while (results.Count() > 0);
        }


        public virtual void GenerateKeywordsOnSeperateThread()
        {
            lock (_padlock)
            {
                CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(this.GenerateKeywords, "KeywordGenerator", ThreadPriority.Lowest);
                
            }
        }

        public virtual void GenerateKeywords()
        {
            lock (_padlock)
            {
                _log.Debug("Generate Keywords [Start]");
                NHClasses.NhManager.CreateNewSessionForContext();
                _log.Debug("Generating Keywords - Categories");
                generateKeywordsFromDbItems(CategoryBase.Factory.GetQuery(), item => item.ID, generateKeywordFromCategory);
                _log.Debug("Generating Keywords - Bransd");
                generateKeywordsFromDbItems(BrandBase.Factory.GetQuery(), item => item.ID, generateKeywordFromBrand);
                _log.Debug("Generating Keywords - Items");
                generateKeywordsFromDbItems(ProductVariationBase.Factory.GetQuery(), item => item.ID, generateKeywordFromItem);
                _log.Debug("Generating Keywords - Item Groups");
                generateKeywordsFromDbItems(ProductBase.Factory.GetQuery(), item => item.ID, generateKeywordFromProduct);
                _log.Debug("Generating Keywords - Disposing session");
                NHClasses.NhManager.DisposeCurrentSessionInContext();
                saveKeywords();
                _log.Debug("Generate Keywords [Finish]");
            }
        }
        private void deleteAll()
        {
            _log.Debug("Generate Keywords - deleteAll() [Start]");
            nHibernateUtil.DeleteAllItemsFromTableInBulk(typeof(KeywordBase));
             _log.Debug("Generate Keywords - deleteAll() [Finish]");
        }
        public MyNHSessionBase session = null;
        protected virtual void saveKeywords()
        {
            _log.Debug("Generating Keywords - saveKeywords() [Start]");
            session = NHClasses.NhManager.CreateNewStatelessSessionForContext();
            var transaction = session.BeginTransaction();

            deleteAll();
            
            var keywords = _values.Values.ToList();
            for (int i=0;i < keywords.Count;i++)
            {
                var k = keywords[i];
                
                k.SaveInDb();
                if (i > 0 && (i % 250 == 0))
                {
                    _log.Debug("Generating Keywords - saveKeywords() [Flusing, i = "+i+"]");
                    transaction.Commit();
                    transaction.Dispose();
                    transaction = session.BeginTransaction();
                }
            }
            transaction.Commit();
            transaction.Dispose();

            NHClasses.NhManager.DisposeCurrentSessionInContext();
            _log.Debug("Generating Keywords - saveKeywords() [Finish]");
        }

        protected KeywordGenerator()
        {
            this.defaultCulture = CultureDetailsBase.Factory.GetDefaultCulture();
        }

        

    }
}
