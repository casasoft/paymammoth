﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.KeywordModule;

namespace BusinessLogic_v3.Classes.KeywordsGenerator
{
    public class KeywordItem
    {
        public KeywordItem(CultureDetailsBase culture, string Keyword)
        {
            this.Keyword = Keyword;
            this.CultureDetails = culture;
            this.Frequency = 0;
        }
        public CultureDetailsBase CultureDetails { get; set; }
        public string Keyword { get; set; }
        public int Frequency { get; set; }
        public override bool Equals(object obj)
        {
            if (obj is KeywordItem)
            {
                KeywordItem cmp = (KeywordItem)obj;
                if (cmp.CultureDetails == this.CultureDetails &&
                    string.Compare(cmp.Keyword, this.Keyword, true) == 0)
                    return true;

            }
            return base.Equals(obj);
        }
        public void SaveInDb()
        {
            KeywordBase k = KeywordBase.Factory.CreateNewItem();
            k.CultureInfo = CultureDetailsBase.Factory.GetByPrimaryKey(this.CultureDetails.ID);
            k.Keyword = this.Keyword;
            k.FrequencyCount = this.Frequency;
            k.Create();
        }

        public static string GetUniqueKey(CultureDetailsBase culture, string keyword)
        {
            string s = culture.ID + "-" + keyword.ToLower();
            return s;
        }

        public override int GetHashCode()
        {
            string s = GetUniqueKey(this.CultureDetails,this.Keyword);
            return s.GetHashCode();
        }
    }
}
