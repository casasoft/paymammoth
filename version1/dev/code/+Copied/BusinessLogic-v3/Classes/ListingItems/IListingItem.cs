﻿



using System;





namespace BusinessLogic_v3.Classes.ListingItems
{
    public interface IListingItem
    {
        DateTime Date { get;  }
        string GetUrl();
        long ID { get;  }
        string ShortDescription { get;  }
        string Title { get;  }
    }
}
