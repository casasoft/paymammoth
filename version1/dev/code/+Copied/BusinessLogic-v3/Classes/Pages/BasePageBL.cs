﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Frontend.ArticleModule;
using log4net;

namespace BusinessLogic_v3.Classes.Pages
{
    public abstract class BasePageBL : Page
    {

        public abstract void UpdateTitle(string pageTitle, bool useOpenGraph = true);
        public abstract void UpdateTitleFromContentPage(ArticleBaseFrontend page, bool useOpenGraph = true);
        public abstract void UpdateTitleAndMetaTags(string title, string metaKeywords, string metaDesc, bool useOpenGraph = true);
        public abstract void UpdateOpenGraphPageType(CS.General_v3.Enums.OPEN_GRAPH_TYPE type, bool overwrite = true);
        public abstract void UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH tag, string value, bool overwrite = true);

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
    }
}
