﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ArticleModule;

using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;

namespace BusinessLogic_v3.Classes.Pages
{
    public abstract class BaseMasterPage : CS.General_v3.Classes.Pages.BaseMasterPage
    {
        public new class FUNCTIONALITY : CS.General_v3.Classes.Pages.BaseMasterPage.FUNCTIONALITY
        {
            protected new BaseMasterPage _masterPage
            {
                get
                {
                    return (BaseMasterPage)base._masterPage;
                }
            }
            public FUNCTIONALITY(BaseMasterPage masterPage)
                : base(masterPage)
            {

            }
        }
        protected override CS.General_v3.Classes.Pages.BaseMasterPage.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }


        public void GenerateMasterSEO(IArticleBase contentPage)
        {

            if (contentPage != null)
            {
                
                
                if ((contentPage.MetaKeywords != null) && (contentPage.MetaDescription != null) && (!string.IsNullOrEmpty(((IHierarchy)contentPage).Title)))
                {
                    BasePage current = (BasePage)CS.General_v3.Util.PageUtil.GetCurrentPage();
                    current.Functionality.UpdateTitleAndMetaFromContentPage(contentPage, true);
                }
            }
        }

        public new BasePage Page
        {
            get
            {
                return
                    (BasePage)base.Page;
            }
        }
    }
}
