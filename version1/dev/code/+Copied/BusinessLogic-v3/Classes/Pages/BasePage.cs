﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.IO;
using System.Text.RegularExpressions;
using BusinessLogic_v3.Modules.AffiliateModule;
using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Modules.MemberReferralModule;
using log4net;

using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;
using BusinessLogic_v3.Modules.CultureDetailsModule;

namespace BusinessLogic_v3.Classes.Pages
{
    

    public abstract class BasePage : CS.General_v3.Classes.Pages.BasePage
    {
        
        


        private ILog _log = log4net.LogManager.GetLogger(typeof(BasePage));
        //public delegate void MessageHandler(string msg);

        public new class FUNCTIONALITY : CS.General_v3.Classes.Pages.BasePage.FUNCTIONALITY
        {
            public bool OutputLanguageFromCurrentCulture { get; set; }
            
            public bool DisableSkype { get; set; }
            protected new BasePage _basePage
            {
                get
                {
                    return (BasePage)base._basePage;
                }
            }
            public FUNCTIONALITY(BasePage pg)
                : base(pg)
            {
                
                this.DisableSkype = false;
                this.OutputLanguageFromCurrentCulture = true;
            }


            public virtual void UpdateTitleAndMetaFromContentPage(ArticleBase page, bool includeWebsiteTitle = true, string websiteTitle = null, bool ifEmptyMetaDescUseHtmlText = true, bool useFacebookOpenGraph = false)
            {
                string metaDesc = page.MetaDescription;
                if (ifEmptyMetaDescUseHtmlText && string.IsNullOrWhiteSpace(metaDesc) && page.HtmlText != null)
                {
                    metaDesc = CS.General_v3.Util.Text.ConvertHTMLToPlainText(page.HtmlText, false);
                    int maxLength = 155;
                    if (metaDesc.Length > maxLength)
                    {
                        metaDesc = metaDesc.Substring(0, maxLength);
                    }
                }

                string title = page.MetaTitle;
                if (string.IsNullOrWhiteSpace(title))
                {
                    title = page.PageTitle;
                }
                UpdateTitleAndMetaTags(title, page.MetaKeywords, metaDesc, includeWebsiteTitle, websiteTitle, useFacebookOpenGraph);
            }
            public virtual void UpdateTitleAndMetaFromContentPage(IArticleBase page, bool includeWebsiteTitle = true, string websiteTitle = null, bool useOpenGraph = false)
            {
                string title = page.MetaTitle;
                if (string.IsNullOrWhiteSpace(title))
                {
                    title = page.PageTitle;
                }
                UpdateTitleAndMetaTags(title, page.MetaKeywords, page.MetaDescription, includeWebsiteTitle, websiteTitle, useOpenGraph);
            }
        }
        public new FUNCTIONALITY Functionality
        {
            get
            {
                return (FUNCTIONALITY)base.Functionality;
            }
            
        }
        protected override CS.General_v3.Classes.Pages.BasePage.FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        public BasePage()
        {
            

        }
        private void initDisableSkypeJS()
        {
            if (this.Functionality.DisableSkype)
            {
                string js = "new js.com.cs." + CS.General_v3.Settings.Others.JavaScript_Com_Version + ".UI.Skype.DisableSkypeController();";
                CS.General_v3.Util.JSUtil.AddJSScriptToPage(js);
                
            }
        }
        private void updatePageCulture()
        {
            CultureDetailsBase currLang = Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetCurrentCultureInSession();
            string cultureCode = currLang.GetCultureCode();
            this.Page.Culture = cultureCode;

        }
        protected override void OnInit(EventArgs e)
        {
            initDefaultOpenGraphImage();
            updatePageCulture();
            base.OnInit(e);
        }
        private void initDefaultOpenGraphImage()
        {
            string openGraphImage = CS.General_v3.Settings.GetSettingFromDatabase<string>(CS.General_v3.Enums.SETTINGS_ENUM.OpenGraph_DefaultImage);
            if (!String.IsNullOrEmpty(openGraphImage))
            {
                openGraphImage = CS.General_v3.Util.PageUtil.ConvertRelativeUrlToAbsoluteUrl(openGraphImage);
                this.Functionality.UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH.Image, openGraphImage);
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            initDisableSkypeJS();
            checkForCurrentAffiliate();
            checkForCurrentMemberReferral();
            initDefaultLanguageTag();
            base.OnLoad(e);
        }

        private void checkForCurrentMemberReferral()
        {
            MemberReferralsBaseManager.Instance.CheckForReferringMember();
        }
         private void initDefaultLanguageTag()
        {
            if (this.Functionality.OutputLanguageFromCurrentCulture)
            {
                var culture = CultureDetailsBase.Factory.GetCurrentCultureInSession();

                var langIsoCode = culture.GetLanguageISOEnumValue();
                if (langIsoCode.HasValue && string.IsNullOrWhiteSpace(this.Functionality.MetaLanguage.Content))
                {
                    this.Functionality.MetaLanguage.Content = CS.General_v3.Enums.ISO_ENUMS.Language_ISO639_2letter_ToCode(langIsoCode.Value);
                    this.Functionality.UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH.Language, this.Functionality.MetaLanguage.Content);
                }
            }
        }


         private const string QS_PARAM_COOKIE_ENABLED = "___c";
        
        private void checkForCurrentAffiliate()
        {
            AffiliateSessionManager.Instance.CheckForCurrentAffiliate();
            
        }

    }
}
