﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Pricing 
{
    public class PricingInfo : IPricingInfo
    {
        /// <summary>
        /// Price excluding tax, PER UNIT
        /// </summary>
        public virtual double RetailPriceBeforeDiscount { get; set; }
        /// <summary>
        /// The discount, PER UNIT
        /// </summary>
        public virtual double Discount { get; set; }
        /// <summary>
        /// Tax amount, PER UNIT
        /// </summary>
        public virtual double TaxRate { get; set; }

        /// <summary>
        /// This is used to be able to add any additional costs, like a 'case'
        /// </summary>
        public virtual double AdditionalCostPerUnit { get; set; }

        //public virtual double ShipmentWeightInKg { get; set; }
        /// <summary>
        /// ShippingCost, PER UNIT
        /// </summary>
        public virtual double ShippingCost { get; set; }

        public virtual int Quantity { get; set; }

        public double GetTaxRate()
        {
            return this.TaxRate;
             

        }

        public PricingInfo()
        {
            init();
            
        }
        private void init()
        {
            this.Quantity = 1;
        }




        //public virtual void SetPriceIncTax(double priceIncTax, double taxRate)
        //{
        //    this.PriceExcTax = CS.General_v3.Util.FinancialUtil.GetPriceExcTaxFromPriceIncTax(priceIncTax, taxRate);
        //    this.TaxAmount = priceIncTax - this.PriceExcTax;
        //}


        public virtual double GetTaxAmount(double price)
        {
            double d = price;
            double tax = CS.General_v3.Util.FinancialUtil.GetTaxOnlyAmountFromPriceIncTax(d, this.TaxRate);
            return tax;
        }


        public virtual double GetUnitPrice(bool includeTax = true, bool reduceDiscount = true)
        {
            double d = this.RetailPriceBeforeDiscount;
            if (reduceDiscount) d -= this.Discount;
            d += this.AdditionalCostPerUnit;
            if (!includeTax)
            {
                double tax = this.GetTaxAmount(d);
                d -= tax;
            }
            return d;
        }
        public virtual double GetTotalPrice(bool includeTax = true, bool reduceDiscount = true)
        {
            double unitPrice = GetUnitPrice(includeTax, reduceDiscount);
            double total = unitPrice * (double)this.Quantity;
            return total;
        }
        public virtual double GetTotalDiscount()
        {
            return this.Discount * (double)this.Quantity;
        }

        //public double GetTotalShipmentWeight()
        //{
        //    return this.ShipmentWeightInKg * this.ShippingCost;
        //}
        public virtual bool HasDiscount()
        {
            return this.Discount > 0;
            
        }




        #region IPricingInfo Members


        public double GetTaxAmount(bool includeDiscount = true)
        {
            double d = GetUnitPrice(reduceDiscount: true);

            return CS.General_v3.Util.FinancialUtil.GetTaxOnlyAmountFromPriceIncTax(d, this.TaxRate);

        }

        #endregion
    }
}
