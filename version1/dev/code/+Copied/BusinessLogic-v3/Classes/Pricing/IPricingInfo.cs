namespace BusinessLogic_v3.Classes.Pricing
{
    public interface IPricingInfo
    {
        double RetailPriceBeforeDiscount { get; }
        double Discount { get;  }
        double TaxRate { get;  }
        
        int Quantity { get;  }
        //double GetPriceExcTax();
        //double GetDiscount();
        //double GetTaxAmount();
        double GetUnitPrice(bool includeTax = true, bool reduceDiscount = true);
        double GetTotalPrice(bool includeTax = true, bool reduceDiscount = true);
        double GetTaxAmount(bool includeDiscount = true);
        
        bool HasDiscount();
    }
}