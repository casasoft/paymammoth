﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Hosting;
using BusinessLogic_v3.Classes.Background;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;
using CS.General_v3.Classes.Attributes;
using CS.General_v3.Classes.Email;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using log4net;
using NHibernate;
using System.Reflection;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;


using BusinessLogic_v3.Classes.Culture;

using BusinessLogic_v3.Classes.Hierarchies;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.SettingModule;
using BusinessLogic_v3.Classes.Emails;
using CS.General_v3.Extensions;
namespace BusinessLogic_v3.Classes.Application
{
    public abstract class AppInstance  : CS.General_v3.Classes.Application.AppInstance
    {
       





        protected virtual void initBusinessLogicModuleSettings()
        {
            
        }

        public IInterceptor GetNhibernateInterceptor()
        {
            return null;
            

        }





        private void initOtherItems()
        {
         

            
        }
        protected virtual void initRouting()
        {
            
            
            
            Classes.Routing.RouteConfig.Instance.MapRoutes();
        
        }
        

        protected virtual void onPostApplicationStart()
        {
            
            

           if (!CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
           {
                launchBackgroundTasks();
                initialiseAuditLogger();

           }
           
        }
        

        private void initialiseAuditLogger()
        {
            BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.StartLogging();
        }

        /// <summary>
        /// This method is used to launch background tasks.  If in unit testing environment, no background tasks are launched.
        /// </summary>
        protected virtual void launchBackgroundTasks()
        {
            CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(removeAnyRecursiveContent, "RemoveAnyRecursiveContent", System.Threading.ThreadPriority.Lowest);
            if (BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Subscriptions_Enabled))
            {
                MemberExpiryCheckerJob.InitJob(); //.Instance.InitTimer(); 
            }
            if (BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_RenewableAccountBalances_Enabled))
            {
                RenewableAccountBalanceCheckerJob.InitJob();
            }
            if (BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBaseFactory.UsedInProject)
            {
                SelfBarringNotificationChecker.Instance.Start();
            }
            BusinessLogic_v3.Classes.DbObjects.TempItems.DbTempItemChecker.Instance.Start();
            BusinessLogic_v3.Classes.Emails.EmailDbLogger.Instance.Init();
            ArticleCustomRouteMapper.UpdateRouteMappingsForAllArticles();
            if (BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Shop_Enabled) && 
                BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBaseFactory.UsedInProject)
            {
                BusinessLogic_v3.Modules.ProductModule.ProductSpecialOfferPricingManager.Instance.Start();
            }


        }

        /// <summary>
        /// Checks for any recursive data which was resulting in problems, in content pages & categories mainly
        /// </summary>
        
        private void removeAnyRecursiveContent()
        {
            //return;

            if (!CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
            {
                BusinessLogic_v3.Classes.NHibernateClasses.NHClasses.NhManager.CreateNewSessionForContext();
                {
                    var factory = ArticleBase.Factory;
                    if (ArticleBaseFactory.UsedInProject)
                    {
                        HierarchyRecursionRemover remover = new HierarchyRecursionRemover();
                        remover.Check(factory.GetRootNodes());
                    }
                }
                
                {
                    
                    if (CategoryBaseFactory.UsedInProject)
                    {
                        var factory = CategoryBase.Factory;


                        HierarchyRecursionRemover remover = new HierarchyRecursionRemover();
                        remover.Check(factory.GetRootCategories());
                    }
                }
                NHClasses.NhManager.DisposeCurrentSessionInContext();
            }
        }








        //public string SHOP_ORDER_REF_GENERATION_FORMAT_NUM_TOKEN = "{Num}";
        public string ShopOrderReferenceGenerationFormat
        {
            get
            {
                return Enums.BusinessLogicSettingsEnum.Ecommerce_ShopOrderReferenceGenerationFormat.GetSettingValueForBusinessLogicSetting<string>(); 
            }
        }
        public int GetNextOrderReference()
        {
            Enum identifier = BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Ecommerce_CurrentOrderReference;
            
          


            return SettingBase.Factory.GetSettingAsIntAndIncrement(identifier);
            
            
        }

        public void OnEmailSent(CS.General_v3.Classes.Email.EmailMessage email)
        {
            EmailDbLogger.Instance.LogEmailInDb(email);
            


            
        }


        private void mapNhibernateInterceptor(Configuration cfg)
        {
            if (_log.IsDebugEnabled) _log.Debug("Mapping Interceptor",null);
            var interceptor = Classes.NHibernateClasses.Interceptors.BaseNhInterceptor.CreateInterceptor();
            if (interceptor != null)
            {
                cfg.SetInterceptor(interceptor);
                if (_log.IsDebugEnabled) _log.Debug("Mapped Interceptor <" + interceptor.GetType().FullName);

            }
            else
            {
                if (_log.IsDebugEnabled) _log.Debug("No Interceptor mapped <" + interceptor.GetType().FullName);
            }
        }




        public static AppInstance Instance 
        { 
            get{return (AppInstance)CS.General_v3.Classes.Application.AppInstance.Instance;}
            set { CS.General_v3.Classes.Application.AppInstance.Instance = value; }

        }

        protected AppInstance()
        {
            int j = 5;
            this.ProjectAssemblies = new HashedSet<Assembly>();

            //this.UpdateSchemaOnTestingMachine = true;
            // this.CurrentCultureManager = new BaseSessionCultureManagerTemp();
            this.EnableLog4Net = true;
            //  this.CastleActiveRecordTypes = new List<Type> ();
            initSettingsClasses();
            this.AddProjectAssembly(typeof(AppInstance));
            this.AddProjectAssembly(this.GetType());

            CS.General_v3.Classes.Email.EmailMessage.OnEmailSent += new Action<EmailMessage>(EmailMessage_OnEmailSent);
        }

        void EmailMessage_OnEmailSent(EmailMessage obj)
        {
            OnEmailSent(obj);
        }

        public HashedSet<Assembly> ProjectAssemblies { get; private set; }
        public void AddProjectAssembly(Assembly a)
        {
            this.ProjectAssemblies.Add(a);
        }
        public void AddProjectAssembly(Type t)
        {
            var a = Assembly.GetAssembly(t);
            AddProjectAssembly(a);

        }

        protected internal HashedSet<Type> _getCustomNhibernateMappings()
        {

            HashedSet<Type> list = new HashedSet<Type>();
            getCustomNhibernateMappings(list);
            return list;
        }

        protected virtual void getCustomNhibernateMappings(HashedSet<Type> list)
        {


        }

        public override IEnumerable<Assembly> GetProjectAssemblies()
        {

            return this.ProjectAssemblies.ToList();
        }




        public bool IsWebApplication
        {
            get
            {
                return CS.General_v3.Util.Other.IsWebApplication;

            }
        }

        /*protected void callPostInitialisationForFactories()
        {
            //CS.General_v3.Classes.Factories.DbFactoryController.Instance.FinishedInitialisingFactories();
            var factories = CS.General_v3.Classes.Factories.DbFactoryController.Instance.FinishedInitialisingFactories();
            foreach (var f in factories)
            {
                f.onPostInitialisation();
            }
        }

        */
        protected static ILog _log = log4net.LogManager.GetLogger(typeof(AppInstance));

        public event EventHandler SessionStart;




        private bool _onApplicationStart_Called = false;
        protected readonly object _lock = new object();

        private void checkForNhProfiler()
        {
            

            if (CS.General_v3.Settings.Database.UseNHibernateProfiler)
            {
                HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
            }
        }

        protected virtual void onApplicationStart()
        {
            checkForNhProfiler();
            registerDataFactoriesToBeUsed();
            initFactories_InitTempRepositories();
            initFactories_After();
            initRouting();

            initOtherItems();


            initBusinessLogicModuleSettings();

            //this is meant to be overriden.  The other method provides uniqueness and locking
        }

        public bool EnableLog4Net { get; set; }
        protected virtual void registerDataFactoriesToBeUsed()
        {

        }
        protected virtual void initFactories()
        {

        }

        protected virtual void initFactories_InitTempRepositories()
        {


        }
        protected virtual void initFactories_After()
        {
            DbFactoryController.Instance.FinishedInitialisingFactories();
            BusinessLogic_v3.Classes.DbObjects.TempItems.DbTempController.Instance.FinishedInitialisingFactories();
        }

        protected virtual void initNHibernateMappings()
        {

        }

        private void initDb()
        {
            if (!string.IsNullOrWhiteSpace(CS.General_v3.Settings.Database.DatabaseName))
            {

                if (true || !NHClasses.NHConfig.CheckIfConfigurationExistsAndValid())
                {
                    initNHibernateMappings();
                }



                NHClasses.NhManager.Initialise();


            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectSpecific">This must be the project-specific class library assembly.  You can retrieve such an assembly by calling Assembly.GetFromType(typeof([any-class-in-project-library]));</param>
        public void OnApplicationStart()
        {

            lock (_lock)
            {
                if (!_onApplicationStart_Called)
                {
                    if (EnableLog4Net)
                    {

                        CS.General_v3.Util.Log4NetUtil.ConfigureForDefaultWebApp();
                    }
                    if (_log.IsInfoEnabled) _log.Info("OnApplicationStart - CalledFirstTime");

                    CS.General_v3.Util.ApplicationUtil.MonitorFatalExceptions();

                    initFactories();
                    initDb();



                    //  throw new InvalidOperationException("bla bla");

                    _log.Info("OnApplicationStart - CalledFirstTime");
                    NHClasses.NhManager.CreateNewSessionForContext();
                    this.onApplicationStart();
                    this.onPostApplicationStart();

                    NHClasses.NhManager.DisposeCurrentSessionInContext();

                    _onApplicationStart_Called = true;
                    if (!CS.General_v3.Settings.Database.CheckMappingsAndOthers)
                    {
                        CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(
                                                                                        CS.General_v3.Classes.Reflection.LastInheritanceChainTypeFinder.Instance.SaveToFile,
                                                                                        "NhConfig-SaveToFile"
                        );
                    }
                }
            }
        }
        private bool _onFirstSessionStart_Called = false;
        protected virtual void onFirstSessionStart()
        {
            CS.General_v3.Util.PageUtil.SetBaseUrlOfApplication();
            BusinessLogic_v3.Classes.WebApplications.KeepAlive.Instance.StartIfEnabled();

            CachingUtil.InvalidateAllPagesOuputCachingDependency();

        }

        public override bool IsFirstSessionStartCalled()
        {
            return _onFirstSessionStart_Called;
        }

        private void onFirstSessionStartCheck()
        {
            lock (_lock)
            {
                if (!_onFirstSessionStart_Called)
                {
                    _onFirstSessionStart_Called = true;
                    onFirstSessionStart();







                }
            }
        }

        protected virtual void onSessionStart()
        {

        }

        /// <summary>
        /// This method must be called from the global.asax to signify session start
        /// </summary>
        public void OnSessionStart()
        {

            NHClasses.NhManager.CreateNewSessionForContext();
            onFirstSessionStartCheck();
            onSessionStart();
            if (this.SessionStart != null)
                this.SessionStart(this, null);
            NHClasses.NhManager.DisposeCurrentSessionInContext();
            if (!CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
            {
                BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.SetCultureFromCurrentURL();
            }
        }


        protected virtual void initSettingsClasses()
        {


        }






        protected virtual void updateDBSchema(Configuration cfg)
        {
            try
            {
                SchemaExport schema = new SchemaExport(cfg);


                //  schema.Create(true, true);
                schema.Execute(true, true, false);
            }
            catch (Exception ex)
            {

                throw;
            }

        }




        public bool CheckDefaultsInRealDatabase
        {
            get
            {
                return CS.General_v3.Settings.Database.CheckDefaultsInRealDatabase;
            }
        }







        public override string GetSettingFromDatabase(string setting, SettingsInfoAttribute settingInfo)
        {
            return Modules.SettingModule.SettingBaseFactory.Instance.GetSetting(setting, settingInfo, createIfNotExists: true);
        }

        protected virtual internal void onPreNHibernateConfigMapping(Configuration cfg)
        {
            mapNhibernateInterceptor(cfg);
        }

        public override bool IsCurrentUserLoggedInCms()
        {

            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext() && BusinessLogic_v3.Modules.CmsUserModule.CmsUserSessionLogin.Instance.IsLoggedIn)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
    }
}
