﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.SettingModule;

namespace BusinessLogic_v3.Classes.Application
{
    public abstract class AppSettingsBase<TSettings, TEnumType>
        where TEnumType: struct
    {

        public static TSettings Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<TSettings>(); } }

        protected AppSettingsBase()
        {
            Initialise();
        }


        
        protected T getSetting<T>(TEnumType setting)
        {
            return CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<T>((Enum)(object)setting);
        }
        protected string getSetting(TEnumType setting)
        {
            return getSetting<string>(setting);
            
        }
        public void Initialise()
        {
            ReloadSettings();
            SettingBaseFactory.Instance.OnItemUpdate += new BusinessLogic_v3.Classes.DbObjects.General.OnItemUpdateHandler(Instance_OnItemUpdate);
        }

        public abstract void ReloadSettings();

        void Instance_OnItemUpdate(BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject item, BusinessLogic_v3.Enums.UPDATE_TYPE updateType)
        {
            ReloadSettings();
        }
    }
}
