﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DbObjects.Parameters
{
    public delegate void UpdateComputedDelValueHandler(UpdateComputedDelValueArgs args);
    public class UpdateComputedDelValueArgs
    {
        public bool? ComputedDeletedValue { get; set; }

    }
}
