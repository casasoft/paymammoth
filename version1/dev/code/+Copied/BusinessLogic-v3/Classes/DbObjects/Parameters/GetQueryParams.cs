﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using NHibernate;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Classes.DbObjects.Parameters
{
    public class GetQueryParams
    {
        public bool LoadTemporaryItems {get;set;}
        public bool LoadDeletedItems {get;set;}
        public bool OrderByPriority {get;set;}
        public bool LoadOnlyItemsForFrontend { get; set; }
        public bool LoadNonPublishedItems { get; set; }
        public MyNHSessionBase Session {get;set;}
        public void FillDefaultsForCms()
        {
            LoadTemporaryItems = false;
            //this.LoadDeletedItems = false;
            this.LoadNonPublishedItems = true;
            this.OrderByPriority = false;
            this.LoadOnlyItemsForFrontend = false;
        }

        public GetQueryParams(bool loadTempItems = false, bool loadDelItems = false, 
            bool orderByPriority = true, bool loadNonPublishedItems = false,

            MyNHSessionBase session = null)
        {
            this.LoadOnlyItemsForFrontend = true;
            this.LoadTemporaryItems = loadTempItems;
            this.LoadDeletedItems = loadDelItems;
            this.OrderByPriority = orderByPriority;
            this.LoadNonPublishedItems = loadNonPublishedItems;
            this.Session = session;

        }

        public MyNHSessionBase GetSessionOrDefault()
        {
            MyNHSessionBase session = null;
            if (this.Session != null)
                session = this.Session;
            else
                session = BusinessLogic_v3.Classes.NHibernateClasses.NHClasses.NhManager.GetCurrentSessionFromContext();
            return session;
        }

        public void FillCriteria(ICriteria crit, IBaseDbFactory factory)
        {
            foreach (var o in getOrderList())
            {
                crit.AddOrder(o);
            }
            foreach (var c in getCriterion(factory))
            {
                crit.Add(c);
            }
        }
        private List<Order> getOrderList()
        {
            List<Order> list = new List<Order>();
            if (OrderByPriority)
                list.Add(new Order(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(d => d.Priority), true));
            return list;
        }

        private List<ICriterion> getCriterion(IBaseDbFactory factory)
        {

            List<ICriterion> list = new List<ICriterion>();
            if (!LoadTemporaryItems)
            { //dont show temporary items
                NHibernate.Criterion.Disjunction disj = new Disjunction();
                disj.Add(NHibernate.Criterion.Expression.Eq(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(d => d._Temporary_Flag), false));
                disj.Add(NHibernate.Criterion.Expression.Eq(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(d => d._Temporary_Flag), null));
                list.Add(disj);
            }
            if (!LoadNonPublishedItems)
            {//load only published items

                list.Add(NHibernate.Criterion.Expression.Eq(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(d => d.Published), true));
                NHibernate.Criterion.Disjunction disj = new Disjunction();
                disj.Add(NHibernate.Criterion.Expression.IsNull(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(d => d.PublishedOn)));
                disj.Add(NHibernate.Criterion.Expression.Le(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(d => d.PublishedOn), CS.General_v3.Util.Date.Now));
                list.Add(disj);
            }
            if (!LoadDeletedItems)
            { //don't show deleted items
                
                { //where (deleted = false || deleted = null)
                    NHibernate.Criterion.Disjunction disjDeleted = new Disjunction();
                    disjDeleted.Add(NHibernate.Criterion.Expression.Eq(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(d => d.Deleted), false));

                    disjDeleted.Add(NHibernate.Criterion.Expression.Eq(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(d => d.Deleted), null));
                    list.Add(disjDeleted);
                }
                {//where (_ComputedDeletedValue = false || _ComputedDeletedValue = null)
                    NHibernate.Criterion.Disjunction disjComputedValue = new Disjunction();
                    disjComputedValue.Add(NHibernate.Criterion.Expression.Eq(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(d => d._ComputedDeletedValue), false));

                    disjComputedValue.Add(NHibernate.Criterion.Expression.Eq(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(d => d._ComputedDeletedValue), null));
                    list.Add(disjComputedValue);
                }
            }
            {
                NHibernate.Criterion.Disjunction disj = new Disjunction();
                if (!factory.LoadOnlyItemsCreatedInThisUnitTestSession) 
                {//if load all items, allow also those that are null / empty
                    disj.Add(NHibernate.Criterion.Expression.Where<BaseDbObject>(x => x._TestItemSessionGuid == null || x._TestItemSessionGuid == ""));
                }
                if (CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
                {
                    disj.Add(NHibernate.Criterion.Expression.Where<BaseDbObject>(x => x._TestItemSessionGuid == CS.General_v3.Util.Other.UnitTestingEnvironment_SessionGuid));
                }
                list.Add(disj);
            }

            return list;
        }

        public void FillDetachedCriteria(DetachedCriteria crit, IBaseDbFactory factory)
        {
            foreach (var o in getOrderList())
            {
                crit.AddOrder(o);
            }
            foreach (var c in getCriterion(factory))
            {
                crit.Add(c);
            }
            

        }
        public void FillQueryOver(IQueryOver query, IBaseDbFactory factory)
        {
            FillCriteria(query.RootCriteria, factory);
        }

    }
}
