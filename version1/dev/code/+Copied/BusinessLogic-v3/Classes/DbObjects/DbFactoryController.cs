﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using CS.General_v3.Classes.Factories;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.NHibernateClasses;

namespace BusinessLogic_v3.Classes.DbObjects
{
    public class DbFactoryController : FactoryController<IBaseDbFactory>
        
    {
        private static readonly DbFactoryController _Instance = new DbFactoryController();
        public static DbFactoryController Instance
        {
            get
            {
                return _Instance;
            }
        }
        
    }
}
