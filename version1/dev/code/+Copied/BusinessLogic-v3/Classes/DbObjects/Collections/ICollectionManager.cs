﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DbObjects.Collections
{
    public interface ICollectionManager<TCollectionItem> : IEnumerable<TCollectionItem>
    {
        TCollectionItem CreateNewItem();
        void AddRange(IEnumerable<TCollectionItem> list);
        void Remove(TCollectionItem item, bool removeToOtherSideAsWell = true);
        void Add(TCollectionItem item, bool addToOtherSideAsWell = true);
        
        void SetItems(IEnumerable<TCollectionItem> list);
        List<TCollectionItem> ToList();
        List<TCollectionItem> GetItemsOnlyAvailableToFrontend();
        
    }
    public interface ICollectionManager : IEnumerable
    {
        IList GetItemsOnlyAvailableToFrontend();
        void AddRange(IEnumerable<object> list);
        void Remove(object item, bool removeToOtherSideAsWell = true);
        void Add(object item, bool addToOtherSideAsWell = true);
        void Clear();
        void SetItems(IEnumerable<object> list);
        IList ToList();



        void AddByPrimaryKey(long id);
    }
}
