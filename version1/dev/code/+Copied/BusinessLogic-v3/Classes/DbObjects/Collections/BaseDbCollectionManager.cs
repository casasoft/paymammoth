﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using CS.General_v3.Util;
using System.Collections;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Classes.DbObjects.Collections
{
    public abstract class BaseDbCollectionManager<TItem, TCollectionItem> : ICollectionManager<TCollectionItem>, ICollectionManager, IEnumerable<TCollectionItem>
              where TItem : IBaseDbObject
        where TCollectionItem : IBaseDbObject
    {
        protected TItem dbItem = default(TItem);
        protected IBaseDbCollectionInfo<TItem, TCollectionItem> collectionInfo { get; private set; }
        protected void setCollectionInfo(IBaseDbCollectionInfo<TItem, TCollectionItem> collectionInfo)
        {
            
            this.collectionInfo = collectionInfo;
        }

        public BaseDbCollectionManager(TItem dbItem)
        {
            this.dbItem = dbItem;
            
            
        }
        public IEnumerable<TCollectionItem> Where(Func<TCollectionItem, bool> comparer)
        {
            return this.collectionInfo.Collection.Where(comparer);
        }
        public bool Contains(Func<TCollectionItem, bool> comparer)
        {
            return this.collectionInfo.Collection.Contains(comparer);
        }
        public TCollectionItem First()
        {
            return this.collectionInfo.Collection.First();
        }
        public TCollectionItem FirstOrDefault()
        {
            return this.collectionInfo.Collection.FirstOrDefault();
        }
        public TCollectionItem Last()
        {
            return this.collectionInfo.Collection.Last();
        }
        public TCollectionItem LastOrDefault()
        {
            return this.collectionInfo.Collection.LastOrDefault();
        }
        public int Count
        {
            get { return this.collectionInfo.Collection.Count(); }
        }
        

        /// <summary>
        /// Don't use this to iterate a collection, as it is immensely inefficient!
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public TCollectionItem GetElementAt(int index)
        {
            int curr = 0;
            var enumerator = this.GetEnumerator();
            while (curr <= index)
            {
                enumerator.MoveNext();
                curr++;
            }
            return enumerator.Current;
            
        }

        /// <summary>
        /// Please note that this DOES NOT save in database
        /// </summary>
        /// <returns></returns>
        public virtual TCollectionItem CreateNewItem()
        {
            this.dbItem.MarkAsDirty();
            TCollectionItem item = BusinessLogic_v3.Classes.DbObjects.DbFactoryController.Instance.CreateItemWhichImplementsType<TCollectionItem>();
            Add(item);
            return item;

        }
        protected virtual void onPreRemove(TCollectionItem item, bool removeToOtherSideAsWell)
        {

        }
        public void AddRange(IEnumerable<TCollectionItem> list)
        {
            foreach (var item in list)
            {
                Add(item);
            }
        }

        public void Remove(TCollectionItem item, bool removeFromOtherSideAsWell = true)
        {
            if (item != null)
            {
                onPreRemove(item, removeFromOtherSideAsWell);
                
                CS.General_v3.Util.ListUtil.RemoveFromEnumerable(this.collectionInfo.Collection, item);

                

                
            }
        }
        protected virtual void onPreAdd(TCollectionItem item, bool addToOtherSideAsWell)
        {

        }

        public void Add(TCollectionItem item, bool addToOtherSideAsWell = true)
        {
            if (item != null)
            {
                onPreAdd(item, addToOtherSideAsWell);
                CS.General_v3.Util.ListUtil.AddToEnumerable(this.collectionInfo.Collection, item);
                
                
            }
        }
        public void Clear()
        {
            IList<TCollectionItem> list = ((IEnumerable<TCollectionItem>)collectionInfo.Collection).ToList();
            foreach (var item in list)
            {
                Remove(item);
            }
        }
        
        public void SetItems(IEnumerable<TCollectionItem> list)
        {
            Clear();
            AddRange(list);
        }

        #region IEnumerable<TCollectionItem> Members

        protected IEnumerable<TCollectionItem> castedCollection
        {
            get
            {
                return (IEnumerable<TCollectionItem>)this.collectionInfo.Collection;
            }
        }

        public IEnumerator<TCollectionItem> GetEnumerator()
        {
            return castedCollection.GetEnumerator();
            
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.collectionInfo.Collection.GetEnumerator();
            
        }

        #endregion


        #region ICollectionManager Members

        void ICollectionManager.AddRange(IEnumerable<object> list)
        {
            this.AddRange(list.Cast<TCollectionItem>());
            
        }

        void ICollectionManager.Remove(object item, bool removeToOtherSideAsWell = true)
        {
            this.Remove((TCollectionItem)item, removeToOtherSideAsWell);
            
        }

        void ICollectionManager.Add(object item, bool addToOtherSideAsWell = true)
        {
            this.Add((TCollectionItem)item, addToOtherSideAsWell);
            
        }

        void ICollectionManager.SetItems(IEnumerable<object> list)
        {
            this.SetItems(list.Cast<TCollectionItem>());
            
        }

        #endregion

        #region ICollectionManager<TCollectionItem> Members


        public List<TCollectionItem> ToList()
        {
            return this.castedCollection.ToList();
            
        }

        #endregion

        #region ICollectionManager<TCollectionItem> Members


        public List<TCollectionItem> GetItemsOnlyAvailableToFrontend()
        {
            
            return this.collectionInfo.Collection.Where(item => item.IsAvailableToFrontend()).ToList();
            
        }

        #endregion

        #region ICollectionManager Members

        IList ICollectionManager.GetItemsOnlyAvailableToFrontend()
        {
            return this.GetItemsOnlyAvailableToFrontend();
            
        }

        #endregion

        #region ICollectionManager Members


        IList ICollectionManager.ToList()
        {
            return this.ToList();
            
        }

        #endregion

        #region ICollectionManager Members


        public void AddByPrimaryKey(long id)
        {
            var factory = BusinessLogic_v3.Classes.DbObjects.DbFactoryController.Instance.GetFactoryForType(typeof(TCollectionItem));
            TCollectionItem item = (TCollectionItem) factory.GetByPrimaryKey(id);
            if (item != null)
            {
                this.Add(item);
            }




        }

        #endregion
    }

}
