﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DbObjects.Collections
{
    public interface IDbCollectionOneToManyInfo<TItem, TCollectionItem> : IBaseDbCollectionInfo<TItem, TCollectionItem>
    {
        
        void SetLinkOnItem(TCollectionItem item, TItem value);
    }

}
