﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using CS.General_v3.Classes.Factories;

namespace BusinessLogic_v3.Classes.DbObjects
{
    public class DataFactoriesController : FactoryController<BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory>
    {
        public static DataFactoriesController Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<DataFactoriesController>(null);

            }
        }

        
    }
}
