﻿using System;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Classes.HelperClasses;

using NHibernate;
using CS.General_v3.Classes.Login;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
namespace BusinessLogic_v3.Classes.DbObjects.Objects
{
    public interface IBaseDbObject : CS.General_v3.Classes.Factories.IBaseObject
    {
        IBaseDbFactory GetFactory();
        bool Published { get; set; }
        DateTime? PublishedOn { get; set; }
        bool Deleted { get; set; }
        bool _ComputedDeletedValue { get; set; }
        DateTime _Temporary_LastUpdOn { get; set; }
        bool _Temporary_Flag { get; set; }
        DateTime? DeletedOn { get; set; }
        DateTime? LastEditedOn { get; set; }
        bool IsAvailableToFrontend();
        ICmsUserBase DeletedBy { get; set; }
        ICmsUserBase LastEditedBy { get; set; }
        int Priority { get; set; }
        string _TestItemSessionGuid { get; set; }
        bool IsSavedInDB();
        void UpdateComputedDeletedValue(bool autoSave = false);
        bool IsTransient();
        void MarkAsDirty();
        void MarkAsNotTemporary();
        void MarkAsTemporary();
        void InitPropertiesForNewItem();

       

        object ItemLock { get; }
        
      

        void resetDirtyFlag();
        void Evict(MyNHSessionBase session = null);
        void RefreshFromDb(MyNHSessionBase session = null);
        
        void ReloadFromDb(MyNHSessionBase  session = null);

        OperationResult Create(SaveParams saveParams = null);
        [Obsolete("Use Create()")]
        OperationResult CreateAndCommit(SaveParams saveParams = null);
        
        OperationResult Save(SaveParams saveParams = null);
        [Obsolete("Use Save()")]
        OperationResult SaveAndCommit(SaveParams saveParams = null);
        
        OperationResult Update(SaveParams saveParams = null);
        [Obsolete("Use Update()")]
        OperationResult UpdateAndCommit(SaveParams saveParams = null);
        
        OperationResult Delete(DeleteParams delParams = null);
        [Obsolete("Use Delete()")]
        OperationResult DeleteAndCommit(DeleteParams delParams = null);

        void Merge(MyNHSessionBase session = null);
        string GetOutputCacheDependencyKey();

        void DeleteTemporaryItemAsTimeElapsed();
        event UpdateComputedDelValueHandler OnUpdateComputedDeletedValue;
        event SaveBeforeAfterHandler OnSaveBefore;
        event SaveBeforeAfterHandler OnSaveAfter;
        event DeleteBeforeAfterHandler OnDeleteBefore;
        event DeleteBeforeAfterHandler OnDeleteAfter;

        int? Version { get; }

        
    }
}
