﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Factories;

namespace BusinessLogic_v3.Classes.DbObjects.TempItems
{
    public class DbTempController : CS.General_v3.Classes.Factories.FactoryController<IBaseDbFactory>
    {
        public static DbTempController Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstance<DbTempController>();
                
            }
        }


    }
}
