﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BusinessLogic_v3.Classes.DataImport.Generic.v2;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using BusinessLogic_v3.Modules.ProductVariationModule;
using BusinessLogic_v3.Classes.DataImport.Generic.v2;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.ProductVariationColourModule;
using BusinessLogic_v3.Modules.ProductVariationSizeModule;

namespace BusinessLogic_v3.Classes.DataImport
{
    public class ProductDataImporter : GenericDataImporter_v2
    {
        public static ProductDataImporter Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<ProductDataImporter>(null); }
        }
        public enum COLUMN_NAMES
        {
            [Description("Ref. Code")]
            RefCode,
            Barcode,
            Category,
            [Description("Sub Category")]
            SubCategory,
            [Description("Sub Sub Category")]
            SubSubCategory,
            Title,
            Description,
            [Description("Price Wholesale")]
            PriceWholesale,
            [Description("Price Before")]
            PriceBefore,
            [Description("Price (RRP)")]
            PriceRRP,
            [Description("VAT Rate")]
            VATRate,
            Colour,
            Size,
            Warranty,
            Brand,
            Quantity

        }
        protected override void onJobStarted()
        {
            BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.AddAuditLog(Enums.AUDITLOG_MSG_TYPE.Information,null, "Item Groups - Data import started");
            base.onJobStarted();
        }
     //   public bool RemoveAnyExistingImages { get; set; }

        private List<char> _categoryDelimeter = null;

        private ProductDataImporter()
        {
            _categoryDelimeter = new List<char>();
            _categoryDelimeter.Add('>');
            _categoryDelimeter.Add('\\');
            _categoryDelimeter.Add('=');
        }

        protected override List<ColumnInfo> getColumnInfos()
        {
            List<ColumnInfo> list = new List<ColumnInfo>();
            list.Add(new ColumnInfo("Ref. Code", warningOnEmpty: true));
            list.Add(new ColumnInfo("Barcode", required:false, warningOnEmpty: true));
            list.Add(new ColumnInfo("Category"));
            list.Add(new ColumnInfo("Sub Category", required: false));
            list.Add(new ColumnInfo("Sub Sub Category", required: false));
            list.Add(new ColumnInfo("Title", required: true, warningOnEmpty: true));
            list.Add(new ColumnInfo("Description", required: false));
            list.Add(new ColumnInfo("Price Wholesale", required: false));
            list.Add(new ColumnInfo("Price Before", required: false));
            list.Add(new ColumnInfo("Price (RRP)", required: false));
            list.Add(new ColumnInfo("VAT Rate", required: false));
            list.Add(new ColumnInfo("Colour", required: false));
            list.Add(new ColumnInfo("Size", required: false));
            list.Add(new ColumnInfo("Warranty", required: false));
            list.Add(new ColumnInfo("Brand", required: false));
            list.Add(new ColumnInfo("Quantity", required: false));
            return list;
        }


        
        protected override void processLine(CS.General_v3.Util.Encoding.CSV.CSVLine csvLine, int lineIndex, ImportResultLine resultsLine)
        {
            string refCode = csvLine.GetNextToken();
            string barCode = csvLine.GetNextToken();
            string category = csvLine.GetNextToken();
            string subCategory = csvLine.GetNextToken();
            string subSubCategory = csvLine.GetNextToken();
            string title = csvLine.GetNextToken();
            string description = csvLine.GetNextToken();
            string priceWholesale = csvLine.GetNextToken();
            string priceBefore = csvLine.GetNextToken();
            string priceRRP = csvLine.GetNextToken();
            string vatRate = csvLine.GetNextToken();
            string colour = csvLine.GetNextToken();
            string size = csvLine.GetNextToken();
            string warrantyText = csvLine.GetNextToken();
            string brand = csvLine.GetNextToken();
            string quantity = csvLine.GetNextToken();

            ProductBase itemGroup = null;
            ProductVariationBase item = null;

            if (resultsLine.Result.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Error)
            {
                bool isNew = false;
                bool isNewProduct = false;


                itemGroup = getProductByReferenceCode(refCode, out isNewProduct, autoSaveIfNew: true);
                item = getItemByBarCode(barCode, itemGroup, out isNew, autoSaveIfNew: true);
                if (item.Product.ID != itemGroup.ID)
                {
                    item.Product = itemGroup;
                }

                var itemData = item;
                var itemgroupData = itemGroup;
                itemData.Barcode = barCode.ToString();
                itemData.Colour = ProductVariationColourBaseFactory.Instance.GetByIdentifier(colour);
                itemData.Size = size; // ProductVariationSizeBaseFactory.Instance.GetByTitle(size);
                if (this.RemoveAnyExistingImages)
                {
                    item.RemoveAllImages(autoSave: false);
                }
                setItemQuantity(itemData, quantity);
                checkForImages(item, refCode);
                itemgroupData.ReferenceCode = refCode;
                itemgroupData.Title = title;
                //itemgroupData.PriceWholesale = getParsedPrice(priceWholesale);
                itemgroupData.PriceRetailIncTaxPerUnit = getParsedPrice(priceBefore);
                itemgroupData.PriceDiscounted = getParsedPrice(priceRRP);
                //itemgroupData.PriceRetailTaxPerUnit = getTaxAmountFromTaxField(itemgroupData.PriceRetailIncTaxPerUnit, vatRate);
                if (string.IsNullOrWhiteSpace(itemgroupData.Description))
                {
                    itemgroupData.Description = description;
                }

                string sFullCat = CS.General_v3.Util.Text.AppendStrings("=", category, subCategory, subSubCategory);

                itemgroupData.AddCategory(getCategoryFromExcelCategoryOrCreateNew(sFullCat));
                BrandBase itemGroupBrand = getBrandOrCreateNewBrand(brand);
                if (itemGroupBrand != null)
                {
                    itemgroupData.Brand = itemGroupBrand;
                }
                itemgroupData.Save();

                itemData.Save();
            }
        }
      
        

       

        private CategoryBase getCategoryFromExcelCategoryOrCreateNew(string category)
        {
            List<string> cats = new List<string>();
            {
                string[] tokens = category.Split(_categoryDelimeter.ToArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int j = 0; j < tokens.Length; j++)
                {
                    var t = tokens[j];
                    t = t.Trim();
                    cats.Add(t);
                }
            }

            CategoryBase currentCat = CategoryBaseFactory.Instance.GetRootCategory();
            for (int i = 0; i < cats.Count; i++)
            {
                string cat = cats[i];
                if (!string.IsNullOrWhiteSpace(cat))
                {
                    var m = Regex.Match(cat, "^([0-9]{1,6} ?- ?)");
                    if (m.Success)
                    {
                        cat = cat.Substring(m.Groups[1].Length);
                    }
                    var nextCat = currentCat.GetChildCategoryByTitleOrImportRef(cat);

                    currentCat = nextCat;
                }
            }
            return currentCat;
            
            

        }

        private BrandBase getBrandOrCreateNewBrand(string brand)
        {
            
            return (BrandBase)BrandBase.Factory.GetBrandByTitleOrImportRef(brand);
        }

        private double getTaxAmountFromTaxField(double retailPrice, string taxStr)
        {
            double d = 0;
            string s = Regex.Replace(taxStr, "[^0-9]", string.Empty);
            if (!double.TryParse(s, out d))
            {
                d = 0;
            }
            return d;
        }

        private double getParsedPrice(string priceValue)
        {
            if (priceValue.IsNotNullOrEmpty())
            {
                return double.Parse(Regex.Replace(priceValue, "[^0-9.]", ""));
            }
            return 0;
        }
        private IEnumerable<FileInfo> sortFilesByFileName(IEnumerable<FileInfo> files)
        {
            var fileList = files.ToList();
            fileList.Sort((f1, f2) => (f1.FullName.CompareTo(f2.FullName)));
            return fileList;

        }

        private void checkForImages(ProductVariationBase item, string referenceCode = null)
        {
            string filename = string.Empty;
            if (item.Barcode.IsNotNullOrEmpty())
            {
                filename = item.Barcode;
            }
            else
            {
                filename =  referenceCode;
            }

            bool oneFound = false;
            
            string regex = filename + @"\.*?";
            var imageFiles = getImagesFromFileSystem(regex);
            if (imageFiles.IsNotNullOrEmpty())
            {
                imageFiles = sortFilesByFileName(imageFiles);
                foreach (var imgFile in imageFiles)
                {
                    oneFound = true;
                    string name = imgFile.Name;


                    ProductVariationMediaItemBase mediaItem = item.GetMediaItemByReference(filename);
                    if (mediaItem == null)
                    {
                        mediaItem = item.MediaItems.CreateNewItem();
                        mediaItem.Reference = name;

                        // var fs = imageFile.OpenRead();
                        mediaItem.CreateAndCommit();
                        try
                        {
                            mediaItem.Image.UploadFileFromLocalPath(imgFile);
                            mediaItem.Update();
                        }
                        catch (Exception ex)
                        {
                            mediaItem.Delete();
                            this.currentResultLine.Result.AddException(ex);
                        }
                        //fs.Dispose();

                    }
                }
            }
            if (!oneFound)
            {
                this.currentResultLine.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Image not found for item with barcode '" + item.Barcode + "'");
            }
        }


        private void setItemQuantity(IProductVariationBase itemData, string quantity)
        {
            if (quantity.IsNotNullOrEmpty())
            {
                itemData.Quantity = int.Parse(quantity);
            }
            else
            {
                itemData.Quantity = 0;
            }
        }

        private ProductBase getProductByReferenceCode(string refCode, out bool isNewProduct, bool autoSaveIfNew)
        {
            isNewProduct = false;
            return null;
            //var itemGroupMatched = ProductBaseFactory.Instance.GetItemByReferenceCode(refCode);
            //if (itemGroupMatched == null)
            //{
            //    itemGroupMatched = ProductBase.Factory.CreateNewItem();
            //    isNewProduct = true;
            //    if (autoSaveIfNew)
            //        itemGroupMatched.Create();
            //    return itemGroupMatched;
            //}
            //else
            //{
            //    isNewProduct = false;
            //    return itemGroupMatched;
            //}
        }
        
        private ProductVariationBase getItemByBarCode(string barCode, ProductBase itemGroup, out bool isNew, bool autoSaveIfNew)
        {
            isNew = false;
            return null;
            //ProductVariationBase item = ProductVariationBaseFactory.Instance.GetItemByBarcode(barCode);
            //if (item == null)
            //{
            //    item = itemGroup.ProductVariations.CreateNewItem();
            //    isNew = true;
            //    if (autoSaveIfNew)
            //        item.Create();
            //    return item;
            //}
            //else
            //{
            //    isNew = false;
            //    return item;
            //}
        }
    }
}
