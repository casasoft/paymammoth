using BusinessLogic_v3.Classes.DataImport.Helpers;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Util.Encoding.CSV;
using System;

namespace BusinessLogic_v3.Classes.DataImport
{
    public interface IDataImporterBase
    {
        DataImportParams Parameters { get; set; }
        DataImportResults Results { get; set; }
        
        //string CurrentCsvFilename { get; set; }
        //string CurrentZipFilename { get; set; }
        DateTime? StartedOn { get;  }
        DateTime? FinishedOn { get;  }
        void ParseCsvFileForColumns();

        bool TerminateImportJob();
        int GetTotalLineCount();
        
        CSVLine GetHeadersFromCsvFile();
        ///// <summary>
        ///// Adds patsh to look for
        ///// </summary>
        ///// <param name="paths">Can be comma seperatred or pipe-line</param>
        //void AddPathsToLookForFiles(string paths);

        //void ExtractZIPFile(string zipPath);
        //void ExtractZIPFile(System.IO.Stream zipFile);
        bool IsFinished();
        bool IsRunning();

        bool LoadCsvFile();

        OperationResult StartProcessing();

        int GetCurrentLineIndex();

        bool IsCsvFileLoaded();
        ImportStatus CurrentStatus { get; }
    }
}