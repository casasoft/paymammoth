﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BusinessLogic_v3.Classes.DataImport.Helpers;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.ZIP;
using CS.General_v3.Util.Encoding.CSV;
using log4net;
using BusinessLogic_v3.Classes.DataImport.Generic;

namespace BusinessLogic_v3.Classes.DataImport
{//this is the one latest
    //2012-05-09
    public enum ImportStatus
    {
        Idle,
        LoadedCsv,
        Finished,
        Running

    }
    public abstract class DataImporterBase<TInstance> : IDataImporterBase
    {
        public static TInstance Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<TInstance>(null); }
        }

        private DataImportFilesToSearch _fileSearcher = null;
        private void loadFileSearcher()
        {
            _fileSearcher = new DataImportFilesToSearch();
            _fileSearcher.FilePathsToSearch.AddRange(this.Parameters.FilePathsToSearchForFiles);
            _fileSearcher.ParseFiles();
        }

        public DataImportParams Parameters { get; set; }

        public ImportStatus CurrentStatus { get; private set; }

        //public int FlushSessionEveryNumOfLines { get; set; }
        protected DataImporterBase()
        {
            _log = LogManager.GetLogger(this.GetType().Name);
            this.Parameters = new DataImportParams();

            this.CurrentStatus = ImportStatus.Idle;
            //this.TotalLinesToskipAfterHeader = 0;
            //FlushSessionEveryNumOfLines = 100;
            initialiseColumns();
            postInitialiseColumns();
        }


        protected abstract void initialiseColumns();
        protected virtual void postInitialiseColumns()
        {
        }

        public DataImportResults Results {get;set;}
        protected CSVFile csvFile = null;
        protected bool getBoolFromString(string s)
        {
            return getBoolNullableFromString(s).GetValueOrDefault();
        }
        
        protected bool _terminate = false;
        private readonly object _padlock = new object();

        protected virtual object getPadlock()
        {
            return _padlock;

        }

        public bool TerminateImportJob()
        {
            bool ok = false;
            csvFile = null;
            if (_started)
            {
                
                
                this.Results.GeneralImportResult.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Data import job terminated");
                _terminate = true;
                _fileSearcher = null;
                ok = true;
            }
            this.CurrentStatus = ImportStatus.Idle;
            return ok;
        }
        private int? __headerIndex = null;
        /// <summary>
        /// Index where header is located (starts at 0);
        /// </summary>
        protected int getHeaderIndex()
        {
            if (!__headerIndex.HasValue) __headerIndex = this.Parameters.HeaderLineIndex;
            if (!__headerIndex.HasValue)
            {//take first non-blank line
                for (int i = 0; i < csvFile.Count; i++)
                {
                    var csvLine = csvFile[i];
                    if (!csvLine.IsLineBlank())
                    {
                        __headerIndex = i;
                        break;
                    }
                }
            }
            return __headerIndex.GetValueOrDefault(-1);

        }

        protected int getStartLineIndex()
        {
            return getHeaderIndex() + 1;
        }




        protected bool? getBoolNullableFromString(string s)
        {
            s = s ?? "";
            s = s.ToLower();
            if (string.IsNullOrEmpty(s))
                return null;
            else
                return s == "1" || s == "y" || s == "yes" || s == "true";
        }
        private string processStringForNumbers(string s)
        {
            s = CS.General_v3.Util.Text.ReplaceTexts(s, "", ",", "€", " ", "$").Trim();
            return s;
        }

        protected int? getIntFromString(string s)
        {
            s = processStringForNumbers(s);
            double? d = getDoubleFromString(s);
            if (d.HasValue)
                return (int)Math.Round(d.Value);
            else
                return null;

        }
        
        protected double? getDoubleFromString(string s)
        {
            s = processStringForNumbers(s);
            if (string.IsNullOrEmpty(s))
                return null;
            else
                return Convert.ToDouble(s);
        }

        private bool validateColumnsForLine(CSVLine csvLine, int lineIndex, DataImportResultLine resultsLine)
        {
            bool ok = true;
            foreach (var c in this.Parameters.ColumnsCollection.Columns)
            {//this will go through each column, and verify that they are all filled in as per the column requirements

                var index = c.LinkedColumnIndexInCsv;
                if (index.HasValue)
                {
                    string s = csvLine[index.Value];
                    bool validationResult = c.ValidateString(s, currentResultLine);
                    ok = ok && validationResult;

                }
            }
            return ok;
        }

        /// <summary>
        /// Preprocess line.  If result is false, it wont be processed
        /// </summary>
        /// <param name="csvLine"></param>
        /// <returns></returns>
        protected virtual bool preProcessLine(CSVLine csvLine, int lineIndex, DataImportResultLine resultsLine)
        {
            return true;
        }
        protected abstract void processLine(CSVLine csvLine, int lineIndex, DataImportResultLine resultsLine);

        


        protected virtual bool preProcessFile(CSVFile csvFile)
        {
           


            return true;
            
        }
        protected virtual void postProcessFile()
        {

        }

        public CSVLine GetHeadersFromCsvFile()
        {
            int headerIndex = getHeaderIndex();
            if (headerIndex > -1)
                return this.csvFile[headerIndex];
            else
                return null;
        }

        protected DataImportResultLine currentResultLine { get; private set; }
        protected CSVLine currentCsvLine { get; private set; }
        protected ILog _log { get; private set; }
        
        protected bool _started { get; private set; }

        public int CurrentLineIndex { get; private set; }

        public int GetTotalLineCount()
        {
            int cnt = 0;
            if (csvFile != null)
                cnt = csvFile.Count;
            return cnt;
            
        }
        protected virtual void onPreStart()
        {

        }

        private void start()
        {
            this.AddPathsToLookForFiles(CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.ProductsBulkImportUploadPath));
            loadFileSearcher();

            this.FinishedOn = null;
            

            BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            int flushCount = 0;
            onPreStart();
            int start = getStartLineIndex();
            for (int i = start; i < csvFile.Count; i++)
            {
                this.CurrentLineIndex = i;
                var csvLine = csvFile[i];

                _log.Debug("Processing line #" + i);
                if (!csvLine.IsLineBlank())
                {
                    DataImportResultLine resultsLine = this.Results.AddNewResultLine(csvLine);
                    this.currentResultLine = resultsLine;
                    this.currentCsvLine = csvLine;
                    bool validationResult = validateColumnsForLine(csvLine, i, resultsLine);

                    if (validationResult && preProcessLine(csvLine, i, resultsLine))
                    {
                        processLine(csvLine, i, resultsLine);
                        flushCount++;
                        if (flushCount >= this.Parameters.FlushSessionEveryNumOfLines)
                        {
                            var session = BusinessLogic_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
                            session.Flush();
                            //session.Clear();

                            flushCount = 0;
                        }
                    }
                }
                if (_terminate)
                {
                    break;
                }
            }
            _log.Debug("Finished importing all lines, disposing session");
            BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
            
            _log.Debug("Disposed session successfully");
            onJobEnd();
            _log.Debug("Import done successfully");
            this.CurrentStatus = ImportStatus.Idle;
        }

        private void onJobEnd()
        {
            postProcessFile();
            removeExtractedFiles();
            _terminate = false;
            _started = false;
            this.FinishedOn = CS.General_v3.Util.Date.Now;
        }
        public DateTime? StartedOn { get; private set; }
        public DateTime? FinishedOn { get; private set; }

        protected virtual void onJobStarted()
        {

        }
        private void createNewResults()
        {
            this.Results = new DataImportResults(this.getStartLineIndex(), GetHeadersFromCsvFile().Tokens.ToArray());

        }
        private bool checkForHeaders()
        {
            bool ok = true;
            int headerIndex = getHeaderIndex();
            if (headerIndex == -1)
            {
                ok = false;
                this.Results.GeneralImportResult.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "No headers found in uploaded file");
            }
            return ok;

        }

        protected virtual void _startProcessingFile()
        {
            
            lock (getPadlock())
            {
                this.StartedOn = CS.General_v3.Util.Date.Now;
                _terminate = false;
                _started = true;
                

                
                


                if (preProcessFile(csvFile))
                {
                    this.CurrentStatus = ImportStatus.Running;
                    onJobStarted();
                    CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(start, "DataImporter-" + this.GetType().Name + "-Process", System.Threading.ThreadPriority.Lowest);
                }
                else
                {
                    this.Results.GeneralImportResult.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Error while pre-processing file");
                    onJobEnd();
                }
              


            }
        }

       
        private string tmpImagesDir = null;
        private IEnumerable<System.IO.FileInfo> _getFilesFromFileSystem(string filenameStartsWith, List<string> extensions = null)
        {
            return _fileSearcher.GetFilesStartingWith(filenameStartsWith, extensions);
        }

        ///// <summary>
        ///// Returns an image from the extracted files
        ///// </summary>
        ///// <param name="imageFilename"></param>
        ///// <param name="filenameIncludesRegExp"></param>
        ///// <returns></returns>
        //private IEnumerable< System.IO.FileInfo> _getFilesFromFileSystem(string filenameStartsWith)
        //{
        //    List<FileInfo> fileResults = new List<FileInfo>();
            
        //    List<string> pathsToSearch = new List<string>();
            
        //    if (!string.IsNullOrEmpty(tmpImagesDir))
        //    {
        //        pathsToSearch.Add(tmpImagesDir);
        //    }
        //    foreach (var path in this.Parameters.FilePathsToSearchForFiles)
        //    {
        //        string sPath = path;
        //        if (sPath.Contains("/"))
        //            sPath = CS.General_v3.Util.PageUtil.MapPath(sPath);
        //        var files = CS.General_v3.Util.IO.FindFilesInFolder(sPath, filename, filenameIncludesRegExp, true);
        //        if (files.Count > 0)
        //        {
        //            fileResults.AddRange(files);
        //        }
        //    }
        //    return fileResults;
        //}

        /// <summary>
        /// Adds patsh to look for
        /// </summary>
        /// <param name="paths">Can be comma seperatred or pipe-line</param>
        public void AddPathsToLookForFiles(string paths)
        {
            if (!string.IsNullOrWhiteSpace(paths))
            {
                string[] tokens = paths.Split(new string[] { ",", "|" }, StringSplitOptions.RemoveEmptyEntries); ;
                foreach (var t in tokens)
                {
                    string path = t.Trim();
                    string localPath = path;
                    if (localPath.Contains("/")) localPath = CS.General_v3.Util.PageUtil.MapPath(localPath);

                    if (Directory.Exists(localPath))
                    {
                        this.Parameters.FilePathsToSearchForFiles.Add(localPath.ToLower());
                    }
                }
            }
        }
        

        public List<FileInfo> GetImagesFromFileSystem(string startsWith)
        {

            //string s = CS.General_20101215.Util.IO.GetFilenameOnly(imgNameRegexp);
            //string regex = s + ".(" + CS.General_20101215.Util.Image.GetImageFilesExtensions().ConvertListToString("|") + ")";
            var files = _getFilesFromFileSystem(startsWith, CS.General_v3.Util.Image.GetImageFilesExtensions()).ToList();
            //var imgExtensions = CS.General_v3.Util.Image.GetImageFilesExtensions().ConvertAll<string>(item=>item.ToLower());
            //for (int i = 0; i < files.Count; i++)
            //{
            //    var f = files[i];
            //    string ext = CS.General_v3.Util.IO.GetExtension(f.Name);

            //    if (!imgExtensions.Contains(ext.ToLower()))
            //    {
            //        files.Remove(f);
            //        i--;
            //    }
            //}
            return files;

        }

        

        public void ExtractZIPFile(string zipPath)
        {
            if (!string.IsNullOrEmpty(zipPath))
            {
                string localPath = "";
                if (zipPath.Contains("/"))
                    localPath = CS.General_v3.Util.PageUtil.MapPath(zipPath);
                else
                    localPath = zipPath;
                if (System.IO.File.Exists(localPath))
                {
                    FileStream fs = new FileStream(localPath, FileMode.Open, FileAccess.Read);
                    ExtractZIPFile(fs);
                    fs.Dispose();
                }
            }
        }

        public void ExtractZIPFile(System.IO.Stream zipFile)
        {
            if (zipFile != null && zipFile.Length > 0)
            {
                string tmpZipPath = CS.General_v3.Util.IO.GetTempFilename("zip");
                CS.General_v3.Util.IO.SaveStreamToFile(zipFile, tmpZipPath);
                tmpImagesDir = CS.General_v3.Util.IO.CreateTempDir();
                ZipExtractor zipExtractor = new ZipExtractor(EnumsZip.ZIP_COMPONENT_TYPE.IonicZip);
                zipExtractor.ExtractZIP(tmpZipPath, tmpImagesDir);
                CS.General_v3.Util.IO.DeleteFile(tmpZipPath);

            }
        }
        /// <summary>
        /// Removes extract files. No need to call, as it is automatically called.
        /// </summary>
        protected void removeExtractedFiles()
        {
            if (!string.IsNullOrEmpty(tmpImagesDir))
            {
                CS.General_v3.Util.IO.DeleteDirectory(tmpImagesDir);
            }
        }

        #region IGenericDataImporter_v2 Members


        public string CurrentCsvFilename { get; set; }

        public string CurrentZipFilename { get; set; }

        public bool IsFinished()
        {
            return !_started;

        }


        #endregion

        #region IGenericDataImporter_v2 Members


        public bool IsRunning()
        {
            return _started;
            
        }

        #endregion

        

        public bool RemoveAnyExistingImages { get; set; }
        

        

         public bool AllowColumnsNotToBeExactlyInSameOrder { get; set; }



      




        #region IGenericDataImporter_v2 Members


       
        #endregion

         #region IDataImporterBase Members

         public bool LoadCsvFile()
         {
             
             CSVFile csv = new CSVFile();
             bool ok = csv.LoadFromFile(this.Parameters.CsvFilePath);
             if (ok)
             {
                 this.CurrentStatus = ImportStatus.LoadedCsv;
                 this.csvFile = csv;
                 this.Results = null;
                 
                 if (checkForHeaders())
                 {
                     ParseCsvFileForColumns();
                 }
             }
             return ok;

         }
         public bool IsCsvFileLoaded()
         {
             return csvFile != null;
         }

         public void ParseCsvFileForColumns()
         {
             
            var headerLine = GetHeadersFromCsvFile();
            foreach (var col in this.Parameters.ColumnsCollection.Columns)
            {
                 col.TryToMatchInCsvLine(headerLine);
            }

         }

         #endregion

         #region IDataImporterBase Members


         public OperationResult StartProcessing()
         {
             createNewResults();
             
             OperationResult result = new OperationResult();

             if (_started)
             {
                 result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Another import process is already running. Please wait for it to finish or terminate it in order to start a new one");

             }
             else
             {
                 _startProcessingFile();
             }
             return result;
         }

         #endregion

         #region IDataImporterBase Members



         public int GetCurrentLineIndex()
         {
             return this.CurrentLineIndex;
             
         }

         #endregion

        /// <summary>
        /// Retrieves column data information based on an enumeration name
        /// </summary>
        /// <param name="colName"></param>
        /// <param name="line"></param>
        /// <returns></returns>
         protected string getColumnData(Enum colName, CSVLine line)
         {
             return getColumnData<string>(colName, line);
         }
         /// <summary>
         /// Retrieves column data information based on an enumeration name
         /// </summary>
         /// <param name="colName"></param>
         /// <param name="line"></param>
         /// <returns></returns>
         protected TOutput getColumnData<TOutput>(Enum colName, CSVLine line)
         {
             string s = null;
             var col = this.Parameters.ColumnsCollection.GetColumn(colName);
             if (col != null && col.LinkedColumnIndexInCsv.HasValue)
             {
                 int index = col.LinkedColumnIndexInCsv.Value;
                 if (index < line.Count)
                 {
                     s = line[index];
                 }
             }
             if (s != null) s = s.Trim();
             TOutput output = CS.General_v3.Util.Other.ConvertStringToBasicDataType<TOutput>(s);
             
             return output;
             

         }
         /// <summary>
         /// Retrieves column data information based on an enumeration name
         /// </summary>
         /// <param name="colName"></param>
         /// <param name="line"></param>
         /// <returns></returns>
         protected TOutput getNumericColumnData<TOutput>(Enum colName, CSVLine line)
         {
             string s = getColumnData(colName, line);
             s = Regex.Replace(s, "[^0-9.]", "");

             return CS.General_v3.Util.Other.ConvertStringToBasicDataType<TOutput>(s);

         }
         /// <summary>
         /// Retrieves column data information based on an enumeration name
         /// </summary>
         /// <param name="colName"></param>
         /// <param name="line"></param>
         /// <returns></returns>
         protected double getNumericColumnData(Enum colName, CSVLine line)
         {
             return getNumericColumnData<double>(colName, line);

         }
    }
}
