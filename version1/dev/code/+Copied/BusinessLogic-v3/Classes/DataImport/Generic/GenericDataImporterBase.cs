﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using CS.General_v3.Util.Encoding.CSV;
using System.IO;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DataImport.Helpers;

namespace BusinessLogic_v3.Classes.DataImport.Generic
{
    public abstract class GenericDataImporterBase
    {
        public enum DataImportResult
        {
            AlreadyStarted,
            CouldNotFindHeaderLine,
            OK =0
        }

        public DataImportParams Parameters { get; set; }
        public ImportResults Results { get; set; }
        public ColumnPreParser ColumnPreParser { get; set; }

        protected bool _started { get; private set; }
       
        protected readonly object _lock = new object();



        public GenericDataImporterBase()
        {
            this.Parameters = new DataImportParams();

           
        }
        public bool TerminateImportJob()
        {
            bool ok = false;
            if (_started)
            {
                //this.Results.GeneralImportResult.AddStatusMsg(Enums.STATUS_MSG_TYPE.Warning, "Data import job terminated");
               // _terminate = true;
                ok = true;
            }
            return ok;
        }
        protected CSVFile csvFile = null;


        private int? headerIndex = null;

        /// <summary>
        /// Index where header is located (starts at 0);
        /// </summary>
        protected int getHeaderIndex()
        {
            if (!headerIndex.HasValue)
            {
                if (this.Parameters.HeaderLineIndex.HasValue)
                {
                    headerIndex = this.Parameters.HeaderLineIndex.Value;
                }
                else
                {
                    for (int i = 0; i < csvFile.Count; i++)
                    {

                        if (checkHeadings(i))
                        {
                            headerIndex = i;
                            break;
                        }
                    }
                }
            }
            return headerIndex.GetValueOrDefault(-1);
            
        }

        

        protected abstract IEnumerable<string> getCSVFileHeaders();

        private int getStartLineIndex()
        {
            return this.getHeaderIndex() + 1;
        }

        private bool checkForHeaderIndex()
        {
            headerIndex = null;
            int result = getHeaderIndex();
            if (result == -1)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Could not find line with correct headings. Format should be: ");
                sb.AppendLine("");
                var headings = getCSVFileHeaders().ToList();
                for (int i=0; i < headings.Count;i++)
                
                {
                    var h = headings[i];
                    if (i>0)
                        sb.Append (" | ");
                    sb.Append(h);
                }

                Results.ErrorMessages.Add(sb.ToString());
            }
            return headerIndex.HasValue;
        }


        private bool checkHeadings(int lineIndex)
        {
            var headings = getCSVFileHeaders();
            bool result = true;
            if (headings != null && headings.Count() > 0)
            {
                int i = 0;
                var line = csvFile[lineIndex];
                foreach (var h in headings)
                {
                    if (string.Compare(line[i], h, true) != 0)
                    {
                        result = false;
                       // Results.ErrorMessages.Add("First line does not contain correct headings");
                        break;
                    }
                    i++;
                }
            }
            return result;

        }


        
        private string processStringForNumbers(string s)
        {
            s = CS.General_v3.Util.Text.ReplaceTexts(s, "", ",", "€", " ", "$").Trim();
            return s;
        }

        protected int? getIntFromString(string s)
        {
            s = processStringForNumbers(s);
            double? d = getDoubleFromString(s);
            if (d.HasValue)
                return (int)Math.Round(d.Value);
            else
                return null;

        }
        
        protected double? getDoubleFromString(string s)
        {
            s = processStringForNumbers(s);
            if (string.IsNullOrEmpty(s))
                return null;
            else
                return Convert.ToDouble(s);
        }
        /// <summary>
        /// Preprocess line.  If result is false, it wont be processed
        /// </summary>
        /// <param name="csvLine"></param>
        /// <returns></returns>
        protected virtual bool preProcessLine(CSVLine csvLine, ImportResultLine resultsLine)
        {
            return true;
        }
        protected abstract void processLine(CSVLine csvLine, ImportResultLine resultsLine);
        protected virtual bool preProcessFile(CSVFile csvFile)
        {
            return true;
        }
        protected virtual void postProcessFile()
        {

        }



        public ImportResults ProcessFile(CSVFile csvFile)
        {
            this.csvFile = csvFile;
            this.Results = new ImportResults(this.getStartLineIndex(), getCSVFileHeaders());
            bool ok = checkForHeaderIndex();
            int flushCount = 0;
            if (ok && preProcessFile(csvFile))
            {
                int startLine = getStartLineIndex();
                for (int i = startLine; i < csvFile.Count; i++ )
                {
                    var csvLine = csvFile[i];

                    if (csvLine.CheckIfAtLeastOneFilled())
                    {
                        ImportResultLine resultsLine = this.Results.AddNewResultLine();
                        if (preProcessLine(csvLine, resultsLine))
                        {
                            processLine(csvLine, resultsLine);
                            flushCount++;
                            if (flushCount >= this.Parameters.FlushSessionEveryNumOfLines)
                            {
                                NHClasses.NhManager.FlushCurrentSessionInContext();
                                flushCount = 0;
                            }
                        }
                    }
                }
            }
            postProcessFile();
            removeExtractedFiles();
            
            return this.Results;
        }

        public ImportResults ProcessFile(string sCSVFile)
        {
            CSVFile csvFile = new CSVFile(sCSVFile);
            return this.ProcessFile(csvFile);
        }

        private string tmpImagesDir = null;
        
        /// <summary>
        /// Returns an image from the extracted files
        /// </summary>
        /// <param name="imageFilename"></param>
        /// <param name="filenameIncludesRegExp"></param>
        /// <returns></returns>
        private System.IO.FileInfo getFFromExtracted(string imageFilename, bool filenameIncludesRegExp=false)
        {
            FileInfo f = null;
            List<string> pathsToSearch = new List<string>();
            
            if (!string.IsNullOrEmpty(tmpImagesDir))
            {
                pathsToSearch.Add(tmpImagesDir);
            }
            foreach (var path in this.Parameters.FilePathsToSearchForFiles)
            {
                var files = CS.General_v3.Util.IO.FindFilesInFolder(path, imageFilename, filenameIncludesRegExp, true);
                if (files.Count > 0)
                {
                    f = files[0];
                    break;
                }
            }
            return f;
        }

        


        protected FileInfo GetFileFromExtracted(string img, bool filenameIncludesRegExp=false)
        {
            var fs = getFFromExtracted(img,filenameIncludesRegExp);

            if (fs != null)
            {
                return fs;
               

                
            }
            else
            {
                return null;
            }
        }

        public void ExtractZIPFile(string zipPath)
        {
            if (!string.IsNullOrEmpty(zipPath))
            {
                string localPath = "";
                if (zipPath.Contains("/"))
                    localPath = CS.General_v3.Util.PageUtil.MapPath(zipPath);
                else
                    localPath = zipPath;
                if (System.IO.File.Exists(localPath))
                {
                    FileStream fs = new FileStream(localPath, FileMode.Open, FileAccess.Read);
                    ExtractZIPFile(fs);
                    fs.Dispose();
                }
            }
        }

        public void ExtractZIPFile(System.IO.Stream zipFile)
        {
            if (zipFile != null && zipFile.Length > 0)
            {
                string tmpZipPath = CS.General_v3.Util.IO.GetTempFilename("zip");
                CS.General_v3.Util.IO.SaveStreamToFile(zipFile, tmpZipPath);
                tmpImagesDir = CS.General_v3.Util.IO.CreateTempDir();

                CS.General_v3.Classes.ZIP.ZipExtractor zipExtractor = new CS.General_v3.Classes.ZIP.ZipExtractor(CS.General_v3.Classes.ZIP.EnumsZip.ZIP_COMPONENT_TYPE.IonicZip);
                zipExtractor.ExtractZIP(tmpZipPath, tmpImagesDir);
                CS.General_v3.Util.IO.DeleteFile(tmpZipPath);

            }
        }
        /// <summary>
        /// Removes extract files. No need to call, as it is automatically called.
        /// </summary>
        protected void removeExtractedFiles()
        {
            if (!string.IsNullOrEmpty(tmpImagesDir))
            {
                CS.General_v3.Util.IO.DeleteDirectory(tmpImagesDir);
            }
        }
    }
}
