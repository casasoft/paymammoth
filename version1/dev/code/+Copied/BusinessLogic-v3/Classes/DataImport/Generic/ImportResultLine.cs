﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataImport.Generic
{
    public class ImportResultLine
    {

        public ImportResults.RESULT_TYPE ResultType { get; private set; }
        //p/ublic List<string> ErrorMessages { get; set; }
      //  public List<string> WarningMessages { get; set; }
        public string[] ColumnValues { get; private set; }
        public int LineIndex { get; set; }
        public void MarkAsWarning()
        {
            if (this.ResultType == ImportResults.RESULT_TYPE.OK)
                this.ResultType = ImportResults.RESULT_TYPE.Warning;
        }
        public void MarkAsError()
        {
            this.ResultType = ImportResults.RESULT_TYPE.Error;
        }
        
        public ImportResultLine(int lineIndex, int totalCols) 
        {
            this.ColumnValues = new string[totalCols];
            this.LineIndex = lineIndex;
            
          //  this.ErrorMessages = new List<string>();
         //   this.WarningMessages = new List<string>();

        }
        private void addValue(int colIndex, string value)
        {
            string s = ColumnValues[colIndex] ?? "";
            if (!string.IsNullOrEmpty(s))
                s += "\r\n";
            s += value;
            ColumnValues[colIndex] = s;
        }
        public void AddValueAsWarning(int colIndex, string value)
        {
            addValue(colIndex, value);
            MarkAsWarning();
        }
        public void AddValueAsError(int colIndex, string value)
        {
            addValue(colIndex, value);
            MarkAsError();
        }

     /*   public void AddErrorMessage(string msg)
        {
            this.ErrorMessages.Add(msg);
            this.ResultType = ImportResults.RESULT_TYPE.Error;
        }
        public void AddWarningMessage(string msg)
        {
            this.WarningMessages.Add(msg);
            if (this.ResultType == ImportResults.RESULT_TYPE.OK)
                this.ResultType = ImportResults.RESULT_TYPE.Warning;    
        }*/

    }
}
