﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace BusinessLogic_v3.Classes.DataImport.Generic
{
    public class DataImportFilesToSearch
    {
        public List<string> FilePathsToSearch { get; set; }

        private Dictionary<string, List<FileInfo>> files = null;

        public DataImportFilesToSearch()
        {
            this.FilePathsToSearch = new List<string>();
        }
        private void addFile(FileInfo file)
        {
            string name = file.Name.ToLower();
            List<FileInfo> list = null;
            if (!files.TryGetValue(name, out list))
            {
                list = new List<FileInfo>();
                files[name] = list;
            }
            list.Add(file);
        }

        private void loadPath(string path)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            var files = dir.GetFiles("*", SearchOption.AllDirectories);
            foreach (var f in files)
            {
                addFile(f);
            }

        }
        public void ParseFiles()
        {
            files = new Dictionary<string, List<FileInfo>>();
            foreach (var p in FilePathsToSearch)
            {
                loadPath(p);
            }
        }

        private bool checkIfFileMatchesExtensions(FileInfo f, List<string> extensions)
        {
            return CS.General_v3.Util.IO.CheckIfFileMatchesExtensions(f.Name, extensions);

            bool ok = true;
            if (extensions != null)
            {
                ok = false;
                foreach (var ext in extensions)
                {
                    string tmp = "." + ext;
                    if (string.Compare(tmp, f.Extension, true) == 0)
                    {
                        ok = true;
                        break;
                    }
                }
            }
            return ok;
        }

        public List<FileInfo> GetFilesStartingWith(string startsWith, List<string> extensions)
        {
            string regex = startsWith;
            if (!regex.StartsWith("^")) regex = "^" + regex;
            List<FileInfo> result = new List<FileInfo>();
            if (files != null)
            {
                string cmpName = startsWith.ToLower();
                //string cmpExt = extension.ToLower();
                var keyList = files.Keys.ToList();
                for (int i = 0; i < keyList.Count; i++)
                {
                    var key = keyList[i];
                    if (Regex.IsMatch(key,regex, RegexOptions.Singleline | RegexOptions.IgnoreCase))
                    {
                        var fileList = files[key];
                        foreach (var f in fileList)
                        {
                            if (checkIfFileMatchesExtensions(f, extensions))
                            {
                                result.Add(f);
                            }
                        }

                        
                    }
                }
            }
            return result;
        }
    }
}
