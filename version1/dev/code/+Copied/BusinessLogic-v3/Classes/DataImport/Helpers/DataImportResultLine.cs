﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Util.Encoding.CSV;

namespace BusinessLogic_v3.Classes.DataImport.Helpers
{
    public class DataImportResultLine
    {
        
        

        
        public OperationResult Result { get; private set; }
        public int LineIndex { get; set; }
        public string Link { get; set; }
        public CSVLine CsvLine { get; private set; }
        public DataImportResultLine(int lineIndex, CSVLine line) 
        {
            
        //    this.ColumnValues = new List<string>();
            this.LineIndex = lineIndex;
            this.Result = new OperationResult();
            this.CsvLine = line;

        }



    }
}
