﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;

namespace BusinessLogic_v3.Classes.DataImport.Helpers
{
    public class DataImportColumnCollection
    {
        public List<DataImportColumnInfo> Columns { get; set; }
        public DataImportColumnCollection()
        {
            this.Columns = new List<DataImportColumnInfo>();
        }

        public OperationResult CheckIfAllCsvColumnIndexAreDefined()
        {
            OperationResult result = new OperationResult();

            var verificationResult = VerifyNoDuplicateColumnIndexes();

            if (verificationResult.IsSuccessful)
            {
                foreach (var c in Columns)
                {
                    if (!c.LinkedColumnIndexInCsv.HasValue && c.Validation.Required && c.IsUsed())
                    {
                        result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Column <" + c.GetMainTitle() + "> not linked to a column in the CSV file");
                    }
                }
            }
            else
            {
                result.AddFromOperationResult(verificationResult);
            }
            return result;
            
            
        }

        public DataImportColumnInfo GetColumnByName(string s)
        {
            foreach (var c in this.Columns)
            {
                foreach (var name in c.ColumnNames)
                {
                    if (string.Compare(name, s, true) == 0)
                    {
                        return c;
                    }
                }
            }
            return null;
        }
        public DataImportColumnInfo GetColumn(Enum v)
        {
            string s = CS.General_v3.Util.EnumUtils.StringValueOf(v);
            return GetColumnByName(s);
        }

        public OperationResult VerifyNoDuplicateColumnIndexes()
        {
            OperationResult result = new OperationResult();
            for (int i = 0; i < this.Columns.Count; i++)
            {
                var colToCheck = this.Columns[i];
                if (colToCheck.IsUsed())
                {
                    for (int j = i + 1; j < this.Columns.Count; j++)
                    {
                        var colCmp = this.Columns[j];
                        if (colToCheck.LinkedColumnIndexInCsv == colCmp.LinkedColumnIndexInCsv)
                        {
                            result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Columns <" + colToCheck.GetMainTitle() + "> and <" + colToCheck.GetMainTitle() + "> are linked to the same column in the uploaded file");
                        }

                    }
                }
            }
            return result;
        }


    }
}
