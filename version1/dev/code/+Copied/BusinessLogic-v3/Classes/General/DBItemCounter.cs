﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace BusinessLogic_v3.Classes.General
{
    public class DBItemCounter<TItem> : ItemCounter<TItem>
        where TItem : IBaseDbObject
    {
        public DBItemCounter()
            : base(t => t.ID)
        {

        }
        
    }
}
