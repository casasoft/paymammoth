﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using log4net;
using NHibernate;

namespace BusinessLogic_v3.Classes.General
{
    public class ItemsByIdentifierCache<TItem> 
        where TItem: IItemWithIdentifier, BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject

    {

        private ILog _logger = LogManager.GetLogger(typeof(TItem));

        protected readonly object _lock = new object();

        public bool Enabled { get; set; }
        
        private Dictionary<string, long?> lookupByIdentifier = null;
        
        private IItemWithIdentifierFactory<TItem> factory = null;



        public ItemsByIdentifierCache(IItemWithIdentifierFactory<TItem> factory)
        {
            this.Enabled = true;

            //this.Enabled = false;
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(factory, null);
            this.lookupByIdentifier = new Dictionary<string, long?>();
            
            this.factory = factory;
            this.factory.OnItemUpdate +=new BusinessLogic_v3.Classes.DbObjects.General.OnItemUpdateHandler(factory_OnItemUpdate);
            
        }


        
        void factory_OnItemUpdate(BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject item, Enums.UPDATE_TYPE updateType)
        {
            if (updateType != Enums.UPDATE_TYPE.Delete)
            {
                ItemUpdated((TItem) (object) item);
            }



        }
        private TItem loadFromFactory(long pKey)
        {
            var item = factory.GetItemByPKeyFromDB(pKey);
            if (item != null)
            {
                UpdateCache(item);
            }
            return item;
        }

        private TItem loadFromFactory(string identifier)
        {
            var item = factory.GetItemByIdentifierFromDB(identifier);
            UpdateCache(identifier, item);


            return item;
        }
        public void UpdateCache(string identifier, TItem item)
        {
            if (Enabled)
            {
                checkCache();

                if (item != null)
                {

                    UpdateCache(item);
                }
                else
                {
                    identifier = identifier ?? "";
                    

                    lookupByIdentifier[identifier] = null;
                }
            }
        }
        public void UpdateCache(TItem item)
        {
            if (Enabled)
            {
                checkCache();

                if (item != null && !string.IsNullOrWhiteSpace(item.Identifier))
                {

                    //2011-10-14 Added by Mark
                    try
                    {
                        lookupByIdentifier[item.Identifier] = ((IBaseDbObject)item).ID;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("UpdateCache error in BusinessLogic_v3.Classes.General.ItemsByIdentifierCache<TItem> line 101: item.Identifier = " + item.Identifier);
                        CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Error, ex);
                    }
                    
                }
                
            }
        }

        public void ItemRemoved(string identifier)
        {
            string lowerIdentifier = identifier;
            if (lookupByIdentifier.ContainsKey(lowerIdentifier))
            {
                lookupByIdentifier.Remove(identifier);
            }
            
        }

        public void ItemUpdated(TItem item)
        {
            UpdateCache(item);
        }


        private void checkCache()
        {
            if (!_initCacheFirstTime)
            {
                lock (_lock)
                {
                    if (!_initCacheFirstTime)
                    {
                        _initCacheFirstTime = true;
                        FillCache();

                    }
                }
            }
        }

        public TItem GetItemByIdentifier(Enum enumValue)
        {
            string identifier = CS.General_v3.Util.EnumUtils.StringValueOf(enumValue);
            return GetItemByIdentifier(identifier);
        }
        public TItem GetItemByIdentifier(string identifier)
        {
            checkCache();

            string lowercaseIdentifier = (identifier ?? "");
            TItem item = default(TItem);
            if (Enabled && lookupByIdentifier.ContainsKey(lowercaseIdentifier))
            {
                long? pKey = getItemByIdentifier(identifier);
                if (pKey.GetValueOrDefault() > 0)
                {

                    item = factory.GetItemByPKeyFromDB(pKey.Value);
                    if (item != null && item.Deleted)
                    {
                        item = default(TItem);
                    }
                    if (item == null)
                    {
                        ItemRemoved(identifier);
                    }
                }
            }
            if (item == null)
            {
                item = loadFromFactory(identifier);
                if (item != null && item.Deleted )
                {
                    item = default(TItem);
                }
                UpdateCache(item);
                //lookupByIdentifier[lowercaseIdentifier] = item;
            }
        
            return item;

        }
        private long? getItemByIdentifier(string s)
        {
            s = s ?? "";
            
            if (Enabled &&  lookupByIdentifier.ContainsKey(s))
                return lookupByIdentifier[s];
            else
                return null;
            
        }

        private bool _initCacheFirstTime = false;
        public void FillCache()
        {
            lock (_lock)
            {
                _initCacheFirstTime = true;
                // lookupByPKey.Clear();
                lookupByIdentifier.Clear();
                if (Enabled)
                {
                    MyNHSessionNormal session = (MyNHSessionNormal) BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
                    //session.FlushMode = FlushMode.Never;
                    
                    //using (var t = session.BeginTransaction())
                    {
                        var fullList = this.factory.GetAllItemsFromDB();
                        foreach (var item in fullList)
                        {

                            UpdateCache(item);

                        }
                    }
                    BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
                }
             
            }

        }
    }
}
