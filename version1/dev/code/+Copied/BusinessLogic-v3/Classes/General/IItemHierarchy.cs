﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.General
{
    public interface IItemHierarchy
    {
        int ParentID { get; }
        int ID { get;  }
        IList<IItemHierarchy> Children { get; }
        void Delete();
    }
}
