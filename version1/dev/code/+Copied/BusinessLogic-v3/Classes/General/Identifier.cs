﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.General
{
    public class Identifier
    {
        public Identifier()
        {
            this.Identifiers = new List<string>();
        }
        public Identifier(IEnumerable<string> identifiers) : this()
        {
            
            this.Identifiers.AddRange(identifiers);
        }
        public Identifier(params string[] identifiers)
            : this ((IEnumerable<string>)identifiers)
        {
          
        }
        public List<string> Identifiers { get; private set; }

        public string GetIdentifier()
        {
            return CS.General_v3.Util.Text.AppendStrings(".", Identifiers);
        }
        public override string ToString()
        {
            return GetIdentifier();
        }
        

    }
}
