﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;


namespace BusinessLogic_v3.Classes.General
{
    public interface IItemWithIdentifier
    {
        long ID { get; }
        string Identifier { get; }
        void Evict(MyNHSessionBase session = null);
    }
}
