﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.General
{
    public class ItemCounter<TItem> 
        
    {
        public class ItemCount
        {
            public long UniqueID;
            public TItem Item;
            public int Count;
            public ItemCount(long uniqueID, TItem item, int count)
            {
                this.UniqueID = uniqueID;
                this.Item = item;
                this.Count = count;
            }
        }


        public delegate long GetUniqueIDHandler(TItem item);

        private GetUniqueIDHandler _getUniqueIDHandler = null;
        public ItemCounter(GetUniqueIDHandler getUniqueIDHandler)
        {
            _getUniqueIDHandler = getUniqueIDHandler;
        }

        private List<TItem> items = new List<TItem>();

        private Dictionary<long, ItemCount> counter = new Dictionary<long, ItemCount>();

        public void AddItems(IEnumerable<TItem> items)
        {
            foreach (var item in items)
            {
                AddItem(item);
            }
        }
        public void AddItem(TItem item)
        {
            items.Add(item);

            ItemCount itemCount = null;
            long uniqueID =_getUniqueIDHandler(item);
            if (counter.ContainsKey(uniqueID))
            {
                itemCount = counter[uniqueID];
            }
            else
            {
                itemCount = new ItemCount(uniqueID, item, 0);
                counter[uniqueID] = itemCount;
            }
            itemCount.Count++;
            

        }

        public IList<TItem> GetItemsSortedByMostPopular(int amt = -1)
        {
            var itemList = CS.General_v3.Util.ListUtil.GetListFromEnumerator(counter.Values);
            if (amt == -1) amt = itemList.Count;
            itemList.Sort((item1, item2) => (-item1.Count.CompareTo(item2.Count)));
            List<TItem> result = new List<TItem>();
            for (int i = 0; i < itemList.Count && i < amt; i++)
            {
                result.Add(itemList[i].Item);
            }
            return result;
        }
        public TItem GetMostPopularItem()
        {
            ItemCount maxItem = null;
            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                long uniqueID =_getUniqueIDHandler(item);
                if (maxItem == null ||
                    (counter[uniqueID].Count > maxItem.Count))
                {
                    maxItem = counter[uniqueID];
                }
            }
            
            if (maxItem != null)
                return maxItem.Item;
            else
                return default(TItem);

                
        }

    }
}
