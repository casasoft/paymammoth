﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.General
{
    public interface IItemWithIdentifierFactory<TItem> 
        where TItem : IItemWithIdentifier
    {
        
        TItem GetItemByIdentifierFromDB(string identifier);

        /// <summary>
        /// This is important that it is retrieved from database, and not from cache!
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TItem GetItemByPKeyFromDB(long id);
        
        IEnumerable<TItem> GetAllItemsFromDB();
        
        
        event BusinessLogic_v3.Classes.DbObjects.General.OnItemUpdateHandler OnItemUpdate;
        
        
        
    }
}
