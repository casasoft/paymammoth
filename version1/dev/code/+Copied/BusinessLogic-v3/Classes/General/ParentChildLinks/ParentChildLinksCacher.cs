﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using Iesi.Collections.Generic;
using IBaseDbObject = BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject;

namespace BusinessLogic_v3.Classes.General.ParentChildLinks
{
    /// <summary>
    /// Performs parent child link caching
    /// </summary>
    public abstract class ParentChildLinksCacher<TItem> where TItem: BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject
    {

        protected ParentChildLinksCacher()
        {

            init();
        }
        private void init()
        {
            var f = dbFactory;
            f.OnItemUpdate += new BusinessLogic_v3.Classes.DbObjects.General.OnItemUpdateHandler(f_OnItemUpdate);
        }

        void f_OnItemUpdate(IBaseDbObject item, Enums.UPDATE_TYPE updateType)
        {
            ResetCache();
        }

        protected object _padlock = new object();
        private Dictionary<long, HashedSet<long>> _childrenCache;
        private Dictionary<long, HashedSet<long>> _parentsCache;
        private bool loadedData = false;
        private void parseLink(IParentChildLink link)
        {
            if (link.ParentID.HasValue && link.ChildID.HasValue)
            {
                long parentID = link.ParentID.Value;
                long childID = link.ChildID.Value;
                { //add parent
                    HashedSet<long> children = null;
            
                    if (!_childrenCache.TryGetValue(parentID, out children))
                    {
                        children = new HashedSet<long>();
                        _childrenCache[parentID] = children;
                    }
                    children.Add(childID);
                }
                { //add parent
                    HashedSet<long> parents = null;

                    if (!_parentsCache.TryGetValue(childID, out parents))
                    {
                        parents = new HashedSet<long>();
                        _parentsCache[childID] = parents;
                    }
                    parents.Add(parentID);
                }
            }
        }

        protected abstract IEnumerable<IParentChildLink> getAllLinks();



        private void loadAll()
        {
            if (!loadedData)
            {
                lock (_padlock)
                {
                    if (!loadedData)
                    {
                        _childrenCache = new Dictionary<long, HashedSet<long>>();
                        _parentsCache = new Dictionary<long, HashedSet<long>>();
                        
                        var allLinks = getAllLinks();

                        foreach (var link in allLinks)
                        {
                            parseLink(link);
                        }
                        loadedData = true;
                    }
                }
            }

        }
        private void _getAllChildIdsFor(long parentID, HashedSet<long> result)
        {
            HashedSet<long> childIds = null;
            if (_childrenCache.TryGetValue(parentID, out childIds))
            {
                result.AddAll(childIds); 
                foreach (var id in childIds) //add also the children of its children
                {
                    _getAllChildIdsFor(id, result);
                }
            }
        }

        public HashedSet<long> GetAllChildIdsFor(long parentID)
        {
            loadAll();

            HashedSet<long> result = new HashedSet<long>();
            _getAllChildIdsFor(parentID, result);
            return result;
        }

        protected abstract IBaseDbFactory dbFactory { get; }

        public HashedSet<TItem> GetAllChildrenFor(TItem parent)
        {

            if (parent != null)
            {
                var childIds = GetAllChildIdsFor(parent.ID);
                var factory = dbFactory;
                HashedSet<TItem> set = new HashedSet<TItem>();
                set.AddAll(childIds.ToList().ConvertAll<TItem>(id => (TItem)factory.GetByPrimaryKey(id)));
                return set;
            }
            else
            {
                return null;
            }

        }
        private void _getAllParentIdsFor(long childID, HashedSet<long> result)
        {
            HashedSet<long> parents = null;
            if (_parentsCache.TryGetValue(childID, out parents))
            {
                result.AddAll(parents);
                foreach (var id in parents) //add also the parents of its parents
                {
                    _getAllParentIdsFor(id, result);
                }
            }
            
        }
        public HashedSet<long> GetAllParentIdsFor(long childID)
        {
            loadAll();

            HashedSet<long> result = new HashedSet<long>();
            _getAllParentIdsFor(childID, result);
            return result;

        }
        public HashedSet<TItem> GetAllParentsFor(TItem child)
        {

            if (child != null)
            {
                var parentIds = GetAllParentIdsFor(child.ID);
                HashedSet<TItem> set = new HashedSet<TItem>();
                set.AddAll(parentIds.ToList().ConvertAll<TItem>(id => (TItem)dbFactory.GetByPrimaryKey(id)));
                return set;
            }
            else
            {
                return null;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="category"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        public bool CheckIfItemIsParentOf(TItem parent, TItem childToCheck)
        {
            

            var allParentIds = GetAllParentIdsFor(childToCheck.ID);
            return (allParentIds.Contains(parent.ID));
        }
        
        
        public void ResetCache()
        {
            loadedData = false;
        }

    }
}
