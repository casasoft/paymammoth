﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.General.ParentChildLinks
{
    public interface IParentChildLink
    {
        long? ParentID { get;  }
        long? ChildID { get;  }
    }
}
