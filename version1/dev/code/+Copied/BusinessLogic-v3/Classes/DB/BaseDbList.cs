﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DB
{
    public abstract class BaseDbList<TItem> : CS.General_v3.Classes.NHibernateClasses.BaseDbList<TItem>, Iesi.Collections.Generic.ISet<IBaseDbObject>
        where TItem: BusinessLogic_v3.Classes.DB.IBaseDbObject
    {

        #region ISet<IBaseDbObject> Members

        bool Iesi.Collections.Generic.ISet<IBaseDbObject>.Add(BusinessLogic_v3.Classes.DB.IBaseDbObject o)
        {
            return this.Add((TItem)o);
            
        }

        bool Iesi.Collections.Generic.ISet<IBaseDbObject>.AddAll(ICollection<IBaseDbObject> c)
        {
            
            return this.AddAll(getListOfTItemFromIBaseDbObject(c));
            
        }

        private Iesi.Collections.Generic.HashedSet<TItem> getListOfTItemFromIBaseDbObject(IEnumerable<IBaseDbObject> baseList)
        {
            Iesi.Collections.Generic.HashedSet<TItem> list = new Iesi.Collections.Generic.HashedSet<TItem>();
            foreach (var item in baseList)
            {
                list.Add((TItem)item);

            }
            return list;
        }
        private Iesi.Collections.Generic.HashedSet<IBaseDbObject> getListOfIBaseDbObjectFromTItem(IEnumerable<TItem> baseList)
        {
            Iesi.Collections.Generic.HashedSet<IBaseDbObject> list = new Iesi.Collections.Generic.HashedSet<IBaseDbObject>();
            foreach (var item in baseList)
            {
                list.Add((BusinessLogic_v3.Classes.DB.IBaseDbObject)item);

            }
            return list;
        }
        bool Iesi.Collections.Generic.ISet<IBaseDbObject>.ContainsAll(ICollection<IBaseDbObject> c)
        {
            return this.ContainsAll(getListOfTItemFromIBaseDbObject(c));
            
        }

        Iesi.Collections.Generic.ISet<IBaseDbObject> Iesi.Collections.Generic.ISet<IBaseDbObject>.ExclusiveOr(Iesi.Collections.Generic.ISet<IBaseDbObject> a)
        {
            return getListOfIBaseDbObjectFromTItem( this.ExclusiveOr(getListOfTItemFromIBaseDbObject(a)));
            
        }

        Iesi.Collections.Generic.ISet<IBaseDbObject> Iesi.Collections.Generic.ISet<IBaseDbObject>.Intersect(Iesi.Collections.Generic.ISet<IBaseDbObject> a)
        {
            return getListOfIBaseDbObjectFromTItem(this.Intersect(getListOfTItemFromIBaseDbObject(a)));
            
        }

        Iesi.Collections.Generic.ISet<IBaseDbObject> Iesi.Collections.Generic.ISet<IBaseDbObject>.Minus(Iesi.Collections.Generic.ISet<IBaseDbObject> a)
        {
            return getListOfIBaseDbObjectFromTItem(this.Minus(getListOfTItemFromIBaseDbObject(a)));
            
        }

        bool Iesi.Collections.Generic.ISet<IBaseDbObject>.RemoveAll(ICollection<IBaseDbObject> c)
        {
            return this.RemoveAll(getListOfTItemFromIBaseDbObject(c));
            
        }

        bool Iesi.Collections.Generic.ISet<IBaseDbObject>.RetainAll(ICollection<IBaseDbObject> c)
        {
            return (this.RetainAll(getListOfTItemFromIBaseDbObject(c)));
        }

        Iesi.Collections.Generic.ISet<IBaseDbObject> Iesi.Collections.Generic.ISet<IBaseDbObject>.Union(Iesi.Collections.Generic.ISet<IBaseDbObject> a)
        {
            return getListOfIBaseDbObjectFromTItem(this.Union(getListOfTItemFromIBaseDbObject(a)));
        }

        #endregion

        #region ICollection<IBaseDbObject> Members

        void ICollection<IBaseDbObject>.Add(BusinessLogic_v3.Classes.DB.IBaseDbObject item)
        {
            this.Add((TItem)item);
            
        }

        bool ICollection<IBaseDbObject>.Contains(BusinessLogic_v3.Classes.DB.IBaseDbObject item)
        {
            return this.Contains((TItem)item);
            
        }

        void ICollection<IBaseDbObject>.CopyTo(BusinessLogic_v3.Classes.DB.IBaseDbObject[] array, int arrayIndex)
        {
            for (int i = 0; i < this.Count; i++)
            {
                var currItem = this[i];
                int currIndex = i + arrayIndex;
                array[currIndex] = currItem;
            }

            
        }

        bool ICollection<IBaseDbObject>.Remove(BusinessLogic_v3.Classes.DB.IBaseDbObject item)
        {
            return this.Remove((TItem)item);
            
        }

        #endregion

        #region IEnumerable<IBaseDbObject> Members

        IEnumerator<IBaseDbObject> IEnumerable<IBaseDbObject>.GetEnumerator()
        {
            return getListOfIBaseDbObjectFromTItem(this).GetEnumerator();
            
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion
    }
}
