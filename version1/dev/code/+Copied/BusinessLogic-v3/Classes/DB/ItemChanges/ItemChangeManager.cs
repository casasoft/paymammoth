﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace BusinessLogic_v3.Classes.DB.ItemChanges
{
    public class ItemChangeManager
    {
        private ILog _log = LogManager.GetLogger(typeof(ItemChangeManager));
        public static ItemChangeManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<ItemChangeManager>(); } }

        private ItemChangeManager()
        {

        }


        private IBaseDbFactory getFactoryThatGeneratesEntity(object entity)
        {
            return BusinessLogic_v3.Classes.DbObjects.DbFactoryController.Instance.GetFactoryForObject(entity);
        }

        public void UpdatedItem(object entity, long pKey)
        {
            _logUpdate(entity, pKey, Enums.UPDATE_TYPE.Update);
        }

        private void _logUpdate(object entity, long pKey, Enums.UPDATE_TYPE updateType)
        {
            IBaseDbObject dbObject = null;
            if (entity is IBaseDbObject)
            {
                dbObject = (IBaseDbObject)entity;
                var factory = getFactoryThatGeneratesEntity(entity);
                if (factory != null)
                {
                    

                    factory.OnItemUpdated(dbObject, updateType);
                }
                else
                {
                    _log.Warn("No factory found for entity <" + entity + ">, Pkey: " + pKey + ", Type: " + entity.GetType().AssemblyQualifiedName);
                }
            }
        }

        public void DeletedItem(object entity, long pKey)
        {
            _logUpdate(entity, pKey, Enums.UPDATE_TYPE.Delete);

        }
        public void CreatedItem(object entity, long pKey)
        {
            _logUpdate(entity, pKey, Enums.UPDATE_TYPE.CreateNew);

        }
        
        



    }
}
