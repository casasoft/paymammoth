﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects.Persistors;
using BusinessLogic_v3.Classes.Multilingual;
using BusinessLogic_v3.Classes.NHibernateClasses.Persistance;
using BusinessLogic_v3.Classes.NHibernateClasses.Transactions;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.Exceptions;


using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;
using CS.General_v3.Classes.Login;
using NHibernate;
using NHibernate.Criterion;
using log4net;
using BusinessLogic_v3.Frontend._Base;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Classes.DB
{
    public abstract class BaseDbObject : BusinessLogic_v3.Classes.DB.IBaseDbObject
      
    {

        private ILog _m_log = null;
        protected ILog _log
        {
            get 
            {
                if (_m_log == null)
                {
                    _m_log = LogManager.GetLogger(this.GetType());
                }
                return _m_log;
            }
        }

        private ICmsItemFactoryBL _cmsFactory;
        public ICmsItemFactoryBL CmsFactory
        {
            get 
            {
                 
                if (_cmsFactory == null)
                {
                    _cmsFactory = BusinessLogic_v3.Classes.Cms.Factories.CmsFactoriesController.Instance.GetFactoryForType(this.GetType());
                    if (_cmsFactory == null)
                    {
                        throw new InvalidOperationException("Cms factory for type <" + this.GetType() + "> does not exist. Make sure it is added to project, and registered to controller");
                    }
                }
                return _cmsFactory; 
            }
           
        }


        //protected virtual BusinessLogic_v3.Classes.NHibernateClasses.IBaseDbFactory getFactory()
        //{
        //    return CS.General_v3.Classes.Factories.DbFactoryController.Instance.GetFactoryForObject(this);
            
        //}
        public virtual void SetCurrentCultureInfoDBId(BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase culture)
        {
            long? id = null;
            if (culture != null)
                id = culture.ID;
            this.SetCurrentCultureInfoDBId(id);
        }

        private CmsUserBase _deletedby;
        public virtual CmsUserBase DeletedBy
        {
            get { return _deletedby; }
            set { _deletedby = value; }
        }





        public bool CheckIfStillTemporary()
        {
            bool isTemp = (this.ID == 0 || this._Temporary_Flag);
            return isTemp;
        }


        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members

        protected internal virtual bool hasLastEditedFields()
        {
            return true;
        }

        //protected virtual DateTime ___LastEditedOn_BaseDbObject { get; set; }
        //protected virtual CmsUserBase ___LastEditedBy_BaseDbObject { get; set; }


        #endregion




        
        public long GetCurrentCultureDBId()
        {

            long? id = _currentCultureInfoID;
            if (!id.HasValue)
            {
                id = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture().ID;
                //string defaultCode = CS.General_v3.Settings.GetSetting(CS.General_v3.Enums.SETTINGS_ENUM.Culture_DefaultCultureCode);
                //id = BusinessLogic_v3.DB.CultureDetailsBaseFactory.Instance.GetCultureByCode(defaultCode).ID;
                //id = CS.General_v3.Classes.Settings.SettingsMain.GetCurrentDefaultCultureDBID();
            }
            return id.Value;
        }






        
        #region IBaseDbObject Members

      

        #endregion

        #region IBaseDbObject Members

        

        #endregion
        

        private IPersistor _persistor;
        protected IPersistor persistor
        {
            get
            {
                if (_persistor == null)
                {
                    _persistor = new NhPersistor();
                }
                return _persistor;
            }
            set
            {
                _persistor = value;
                if (_persistor == null)
                {
                    int k = 5;
                }
            }
        }


        public BaseDbObject()
        {
            this.OnSaveBefore += this.Save_Before;
            this.OnSaveAfter += this.Save_After;
            this.OnDeleteBefore += this.Delete_Before;
            this.OnDeleteAfter += this.Delete_After;

        }


        protected object getItemLock()
        {
            return this.GetFactory().GetItemLock(this);
        }


        public bool ReferenceEquals(BaseDbObject obj)
        {
            return Object.ReferenceEquals(this, obj);
        }

        protected MyTransaction beginTransaction()
        {
            return nHibernateUtil.BeginTransactionFromSessionInCurrentContext();
        }
        protected MyTransaction beginTransactionBasedOnAutoSaveBool(bool autoSave)
        {
            return BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionBasedOnAutoSaveBool(autoSave);
            

        }
        protected MyTransaction beginTransactionIfNotOneAlreadyActive()
        {
            return nHibernateUtil.BeginTransactionIfNotOneAlreadyActive();

        }

        public static bool operator ==(BaseDbObject obj1, BaseDbObject obj2)
        {
            
            return Object.Equals(obj1, obj2);
        }
        public static bool operator !=(BaseDbObject obj1, BaseDbObject obj2)
        {
            return !Object.Equals(obj1, obj2);
        }



        public virtual bool IsAvailableToFrontend()
        {
            return this.Published && !this.Deleted && !this._Temporary_Flag;
        }

        protected MyNHSessionBase _currentSession = null;

        private bool isDirty { get; set; }
        protected void markAsDirty()
        {
            this.MarkAsDirty();
        }
        public virtual void MarkAsDirty()
        {
            isDirty = true;
        }

        protected virtual void checkPropertyUpdateAndUpdateDirtyFlag(object p1, object p2)
        {
            if (
                (p1 != null && p2 != null && p1.GetHashCode() != p2.GetHashCode()) ||
                (p1 == null && p2 != null) ||
                (p1 != null && p2 == null)
                )
            {
                if (!(p1 is DateTime) || !(p2 is DateTime))
                {
                    markAsDirty();
                }
                else
                {
                    DateTime d1 = (DateTime)p1;
                    DateTime d2 = (DateTime)p2;
                    if ((d1.Year != d2.Year) ||
                        (d1.Month != d2.Month) ||
                        (d1.Day != d2.Day) ||
                        (d1.Hour != d2.Hour) ||
                        (d1.Minute != d2.Minute))
                    {
                        markAsDirty();
                    }
                }

            }

        }

        protected virtual bool _alwaysDeletePermanently { get; set; }

        private bool checkIfItemIsNew()
        {
            bool isNew = IsTransient() || _Temporary_Flag;

            return isNew;
        }
        private void checkSavePermanentlyVariable(bool savePermanently)
        {
            if (savePermanently) MarkAsNotTemporary();

        }


        //++++++++++++++++++++++++











        private IBaseDbFactory _factory = null;
        public IBaseDbFactory GetFactory()
        {
            if (_factory == null)
                _factory = DbFactoryController.Instance.GetFactoryForObject(this);
            return _factory;
        }
        protected MyNHSessionBase getCurrentSession()
        {
            MyNHSessionBase s = null;
            if (_currentSession == null)
                s = NHClasses.NhManager.GetCurrentSessionFromContext();
            else
                s = _currentSession;
            return s;
        }

        public virtual bool HasTemporaryFields()
        {
            return true;
        }




        protected internal virtual void updateLastEdited()
        {
            this.LastEditedOn = CS.General_v3.Util.Date.Now;
            CmsUserBase loggedUser = null;
            var currentCmsInstance = CmsUserSessionLogin.Instance;
            if (currentCmsInstance != null)
            {
                loggedUser = currentCmsInstance.GetLoggedInUser(checkForAutoLogin: false);


            }
            LastEditedBy = loggedUser;
          
        }

        /// <summary>
        /// Reloads the item from the database
        /// </summary>
        /// <param name="session"></param>
        public void ReloadFromDb(MyNHSessionBase session = null)
        {
            if (session == null) session = getCurrentSession();
            session.Refresh(this);


        }



        public virtual bool ComparePKey(BaseDbObject itemToCompareWith)
        {
            if (itemToCompareWith != null && itemToCompareWith.ID == this.ID)
            {

                return true;
            }
            else
            {
                return false;
            }
        }


        public virtual void MarkAsNotTemporary()
        {
            _Temporary_Flag = false;


        }
        public virtual void MarkAsTemporary()
        {
            _Temporary_Flag = true;
            _Temporary_LastUpdOn = DateTime.Now;

        }



        protected long? _currentCultureInfoID = null;
        

        public virtual void SetCurrentCultureInfoDBId(long? cultureInfoID)
        {
            _currentCultureInfoID = cultureInfoID;

        }






        /// <summary>
        /// Returns whether this object is yet saved in DB or not
        /// </summary>
        public virtual bool IsSavedInDB()
        {
            return (this.ID > 0);
        }



        public override sealed bool Equals(object obj)
        {
            BaseDbObject compareTo = obj as BaseDbObject;




            return (compareTo != null) &&
                   (hasSameNonDefaultIdAs(compareTo) ||
                // Since the IDs aren't the same, either of them must be transient to 
                // compare business value signatures
                    (((IsTransient()) || compareTo.IsTransient()) &&
                     HasSameBusinessSignatureAs(compareTo)));
        }

        /// <summary>
        /// Transient objects are not associated with an item already in storage.  For instance,
        /// a domain object instance is transient if its ID is 0.
        /// </summary>
        public virtual bool IsTransient()
        {
            return Equals(ID, default(long));
        }
        public virtual bool IsTemporary()
        {
            return IsTransient() || _Temporary_Flag;
        }


        /// <summary>
        /// Must be provided to properly compare two objects.
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {

            return IsTransient() ? base.GetHashCode() : (base.GetHashCode() * 31) + ID.GetHashCode();

            //if (!IsTransient())
            //    return this.ID.GetHashCode();
            //else
            //    return base.GetHashCode();
        }

        private bool HasSameBusinessSignatureAs(BaseDbObject compareTo)
        {

            Contract.Requires(compareTo != null, "compareTo may not be null");

            return GetHashCode().Equals(compareTo.GetHashCode());
        }

        /// <summary>
        /// Returns true if self and the provided persistent object have the same ID values 
        /// and the IDs are not of the default ID value
        /// </summary>
        private bool hasSameNonDefaultIdAs(BaseDbObject compareTo)
        {
            Contract.Requires(compareTo != null, "compareTo may not be null");

            return (!ID.Equals(default(long))) &&
                   (!compareTo.ID.Equals(default(long))) && ID.Equals(compareTo.ID);
        }


        #region CRUD operations

        public event DeleteBeforeAfterHandler OnDeleteBefore;
        public event DeleteBeforeAfterHandler OnDeleteAfter;

        protected virtual void Delete_Before(OperationResult result, DeleteBeforeAfterParams delParams)
        {

        }

        protected virtual void Delete_After(OperationResult result, DeleteBeforeAfterParams delParams)
        {
            invalidateOutputCaching();
            if (!delParams.DeletePermanently)
            { //if not deleted permanently, update the computed deleted value
                UpdateComputedDeletedValue(autoSave: true);
            }
        }

        private void invalidateOutputCaching()
        {
            this.GetFactory().InvalidateOutputCacheDependency();
            this.InvalidateOutputCacheDependency();
        }

        protected virtual void updateFormulaPropertiesBeforeSaving()
        {

        }
        protected virtual void updateFormulaPropertiesAfterSaving()
        {

        }
        protected void _save_Before(OperationResult result, SaveBeforeAfterParams sParams)
        {
            if (sParams.UpdateFormulaProperties)
                updateFormulaPropertiesBeforeSaving();
            if (OnSaveBefore != null) OnSaveBefore(result, sParams);
        }

        public event SaveBeforeAfterHandler OnSaveBefore;
        public event SaveBeforeAfterHandler OnSaveAfter;

        protected virtual void Save_Before(OperationResult result, SaveBeforeAfterParams sParams)
        {
            if (!sParams.SavePermanently && _Temporary_Flag)
            {
                _Temporary_LastUpdOn = CS.General_v3.Util.Date.Now;
            }
            if (isDirty)
            {
                updateLastEdited();
            }



        }
        protected void _save_After(OperationResult result, SaveBeforeAfterParams sParams)
        {
            if (sParams.UpdateFormulaProperties)
                updateFormulaPropertiesAfterSaving();
            if (OnSaveAfter != null) OnSaveAfter(result, sParams);
        }
        protected virtual void Save_After(OperationResult result, SaveBeforeAfterParams sParams)
        {
            invalidateOutputCaching();
            isDirty = false;
        }
        public void InvalidateOutputCacheDependency()
        {
            string key = GetOutputCacheDependencyKey();
            CS.General_v3.Util.CachingUtil.InvalidateCacheDependency(key);

        }

        /// <summary>
        /// This can be used to re-attach detached objects for lazy loading
        /// </summary>
        /// <param name="session"></param>
        public void Lock(MyNHSessionBase session = null)
        {
            Lock(LockMode.None, session);

        }

        /// <summary>
        /// This can be used to re-attach detached objects for lazy loading
        /// </summary>
        /// <param name="lockMode"></param>
        /// <param name="session"></param>
        public void Lock(LockMode lockMode = null, MyNHSessionBase session = null)
        {

            if (session == null) session = getCurrentSession();
            session.Lock(this, lockMode);

        }


        public virtual void Merge(MyNHSessionBase session = null)
        {
            if (session == null) session = getCurrentSession();
            session.Merge(this);


        }

        public void Evict(MyNHSessionBase session = null)
        {
            if (session == null) session = getCurrentSession();
            try
            {
                session.Evict(this);
            }
            catch (Exception ex)
            {
                int k = 5;
                throw;
            }
        }


        

        public void RefreshFromDb(MyNHSessionBase session = null)
        {
            if (session == null) session = getCurrentSession();
            try
            {
                session.Refresh(this);
            }
            catch (Exception ex)
            {
                int k = 5;
                throw;
            }
        }
        public void RefreshFromDbAndWaitRandomInterval(MyNHSessionBase session = null, int minWait = 250, int maxWait = 500)
        {

            int randomInterval = CS.General_v3.Util.Random.GetInt(minWait, maxWait);
            System.Threading.Thread.Sleep(randomInterval);
            RefreshFromDb();
            
        }
        /// <summary>
        /// Repeats a method / series of instructions indefintely, until no 'StaleObjectExceptions' are thrown.  Refreshes this db item every time such an error occurs.
        /// </summary>
        /// <param name="method"></param>
        /// <param name="minWaitEachTime">Minimum wait time before retyring, when an error occurs.</param>
        /// <param name="maxWaitEachTime">Maximum wait time before retyring, when an error occurs.</param>
        public void RepeatUntilNoNhibernateStaleExceptionAndRefresh(Action method, int minWaitEachTime = 250, int maxWaitEachTime = 500)
        {

            bool ok = false;
            do
            {
                try
                {
                    method();
                    ok = true;
                }
                catch (NHibernate.StaleObjectStateException)
                {
                    ok = false;
                    int intervalToWait = CS.General_v3.Util.Random.GetInt(minWaitEachTime, maxWaitEachTime);
                    System.Threading.Thread.Sleep(intervalToWait);
                    this.RefreshFromDb();

                }
            } while (!ok); 

        }



        #endregion


        #region IBaseDbObject Members


        protected virtual void updateDeletedFields()
        {
            if (CmsUserSessionLogin.Instance != null)
            {
                DeletedBy = CmsUserSessionLogin.Instance.GetLoggedInUser();
            }
            else
            {
                DeletedBy = null;
            }
            Deleted = true;
            DeletedOn = CS.General_v3.Util.Date.Now;


        }

        private OperationResult _delete(MyNHSessionBase session, bool deletePermanently, bool autoCommit)
        {
            if (!deletePermanently && _alwaysDeletePermanently) deletePermanently = true;
            if (session == null) session = getCurrentSession();
            OperationResult result = new OperationResult();
            if (OnDeleteBefore != null) OnDeleteBefore(result, new DeleteBeforeAfterParams(deletePermanently, session));
            if (result.IsSuccessful)
            {
                if (!deletePermanently)
                {
                    updateDeletedFields();
                    persistor.Update(this, result, session, autoCommit, true, true);
                    //if (autoCommit)
                    //    UpdateAndCommit();
                    //else
                    //    Update();
                }
                else
                {
                    persistor.Delete(this, result, session, deletePermanently, autoCommit);

                }
                persistor.DeleteAfter(this, result, session, deletePermanently, autoCommit);


                if (OnDeleteAfter != null) OnDeleteAfter(result, new DeleteBeforeAfterParams(deletePermanently, session));
            }

            return result;
        }
        private OperationResult _update(MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
            if (IsTransient())
            {
                throw new InvalidOperationException("You cannot call UPDATE if the object is still transient (not yet saved in database)");
            }

            OperationResult result = new OperationResult();
            if (session == null) session = getCurrentSession();
            checkSavePermanentlyVariable(savePermanently);
            _save_Before(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
            if (result.IsSuccessful)
            {
                persistor.Update(this, result, session, autoCommit, savePermanently, updateFormulaProperties);


                _save_After(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
                persistor.SaveAfter(this, result, session, autoCommit, savePermanently, updateFormulaProperties, false);
            }

            return result;
        }
        private OperationResult _save(MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {

            OperationResult result = new OperationResult();
            if (session == null) session = getCurrentSession();

            checkSavePermanentlyVariable(savePermanently);
            bool isNew = checkIfItemIsNew();
            _save_Before(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
            if (result.IsSuccessful)
            {
                persistor.Save(this, result, session, autoCommit, savePermanently, updateFormulaProperties);

                _save_After(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
                persistor.SaveAfter(this, result, session, autoCommit, savePermanently, updateFormulaProperties, isNew);


            }

            return result;
        }

        private OperationResult _saveCopy(MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
            OperationResult result = new OperationResult();
            if (session == null) session = getCurrentSession();

            checkSavePermanentlyVariable(savePermanently);
            bool isNew = checkIfItemIsNew();
            _save_Before(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
            if (result.IsSuccessful)
            {
                persistor.SaveOrUpdateCopy(this, result, session, autoCommit, savePermanently, updateFormulaProperties);
                _save_After(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
                persistor.SaveAfter(this, result, session, autoCommit, savePermanently, updateFormulaProperties, isNew);


            }


            return result;
        }
        private OperationResult _create(MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {

            OperationResult result = new OperationResult();
            if (session == null) session = getCurrentSession();
            checkSavePermanentlyVariable(savePermanently);
            bool isNew = checkIfItemIsNew();
            _save_Before(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
            if (result.IsSuccessful)
            {
                persistor.Create(this, result, session, autoCommit, savePermanently, updateFormulaProperties);


                _save_After(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
                persistor.SaveAfter(this, result, session, autoCommit, savePermanently, updateFormulaProperties, isNew);
            }

            return result;

        }
        public OperationResult Create(SaveParams sParams = null)
        {
            if (sParams == null) sParams = new SaveParams();
            return _create(session: sParams.Session, autoCommit: false, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);
        }

        [Obsolete("This should not be used anymore.  Use transactions instead")]
        public OperationResult CreateAndCommit(SaveParams sParams = null)
        {
            if (sParams == null) sParams = new SaveParams();
            return _create(session: sParams.Session, autoCommit: true, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);
        }
        [Obsolete("This should not be used anymore.  Use transactions instead")]
        public OperationResult SaveAndCommit(SaveParams sParams = null)
        {
            if (sParams == null) sParams = new SaveParams();
            return _save(session: sParams.Session, autoCommit: true, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);
        }


        [Obsolete("This should not be used anymore.  Use transactions instead")]
        public OperationResult UpdateAndCommit(SaveParams sParams = null)
        {
            if (sParams == null) sParams = new SaveParams();
            return _update(session: sParams.Session, autoCommit: true, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);

        }

        public OperationResult Save(SaveParams sParams = null)
        {
            if (sParams == null) sParams = new SaveParams();
            return _save(session: sParams.Session, autoCommit: false, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);
        }

        public OperationResult Update(SaveParams sParams = null)
        {

            if (sParams == null) sParams = new SaveParams();
            return _update(session: sParams.Session, autoCommit: false, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);

        }
        public OperationResult Delete(DeleteParams delParams = null)
        {
            if (delParams == null) delParams = new DeleteParams();
            return _delete(session: delParams.Session, autoCommit: false, deletePermanently: delParams.DeletePermanently);

        }
        [Obsolete("This should not be used anymore.  Use transactions instead")]
        public OperationResult DeleteAndCommit(DeleteParams delParams = null)
        {
            if (delParams == null) delParams = new DeleteParams();
            return _delete(session: delParams.Session, autoCommit: true, deletePermanently: delParams.DeletePermanently);

        }






        #endregion








        public virtual void DeleteItemPermanentlyIncludingLinked()
        {
            this.Delete(new DeleteParams(delPermanently: true));
        }

        public virtual void DeleteTemporaryItemAsTimeElapsed()
        {
            this.DeleteItemPermanentlyIncludingLinked();

        }

        protected virtual void resetDirtyFlag()
        {
            this.isDirty = false;

        }




        protected virtual void initPropertiesForNewItem()
        {
            this.Published = true;
            this.LastEditedOn = CS.General_v3.Util.Date.Now;
            if (CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
            {
                this._TestItemSessionGuid = CS.General_v3.Util.Other.UnitTestingEnvironment_SessionGuid;
            }
        }


        

        private bool _deleted;
        public virtual bool Deleted
        {
            get { return _deleted; }
            set { _deleted = value; }
        }
        private bool __computeddeletedvalue;
        public virtual bool _ComputedDeletedValue
        {
            get { return __computeddeletedvalue; }
            set { __computeddeletedvalue = value; }
        }
        private bool _published;
        public virtual bool Published
        {
            get { return _published; }
            set { _published = value; }
        }

        public event UpdateComputedDelValueHandler OnUpdateComputedDeletedValue;

        public void UpdateComputedDeletedValue(bool autoSave = false)
        {
            UpdateComputedDelValueArgs args = new UpdateComputedDelValueArgs();

            if (OnUpdateComputedDeletedValue != null)
            {

                OnUpdateComputedDeletedValue(args);
            }

            bool lastValue = this._ComputedDeletedValue;
            if (args.ComputedDeletedValue.HasValue)
            {
                this._ComputedDeletedValue = args.ComputedDeletedValue.Value;
            }
            else
            {
                this._ComputedDeletedValue = this.Deleted;
            }
            if (autoSave && lastValue != this._ComputedDeletedValue)
                this.Save();





        }

        private DateTime __temporary_lastupdon;
        public virtual DateTime _Temporary_LastUpdOn
        {
            get { return __temporary_lastupdon; }
            set { __temporary_lastupdon = value; }
        }

        private bool __temporary_flag;
        public virtual bool _Temporary_Flag
        {
            get { return __temporary_flag; }
            set { __temporary_flag = value; }
        }

        private DateTime? _deletedon;
        public virtual DateTime? DeletedOn
        {
            get { return _deletedon; }
            set { _deletedon = value; }
        }

        private DateTime? _lastEditedOn;
        public virtual DateTime? LastEditedOn
        {
            get { return _lastEditedOn; }
            set { _lastEditedOn = value; }
        }

        private CmsUserBase _LastEditedBy;
        public virtual CmsUserBase LastEditedBy
        {
            get { return _LastEditedBy; }
            set { _LastEditedBy = value; }
        }



        


        private int _priority;
        public virtual int Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        private long _id;
        public virtual long ID
        {
            get { return _id; }
            // private set { _id = value; }

        }



        public string GetOutputCacheDependencyKey()
        {
            return this.GetType().FullName + "_ID_" + this.ID;
        }

        public override string ToString()
        {
            return this.GetType().Name + " [" + this.ID + "]";

        }





        #region IBaseDbObject Members



        #endregion

        #region IBaseDbObject Members

        private string _testitemsessionguid;
        public string _TestItemSessionGuid
        {
            get { return _testitemsessionguid; }
            set { _testitemsessionguid = value; }
        }

        #endregion

        /// <summary>
        /// Resets the test item session guid, and saves immediatey if needed.
        /// </summary>
        public void ResetTestItemSessionGuid(bool autosave = true)
        {
            if (!string.IsNullOrWhiteSpace(_TestItemSessionGuid))
            {
                using (var t = beginTransactionBasedOnAutoSaveBool(autosave))
                {
                    _TestItemSessionGuid = null;
                    if (!this.IsTransient())
                    {
                        this.Save();
                        if (t != null)  t.CommitIfActiveElseFlush();
                    }
                }
            }
        }

        #region IBaseDbObject Members





        #endregion

        #region IBaseDbObject Members


        ICmsUserBase DbObjects.IBaseDbObject.DeletedBy
        {
            get
            {
                return this.DeletedBy;
                
            }
            set
            {
                this.DeletedBy = (CmsUserBase) value;
                
            }
        }

        ICmsUserBase DbObjects.IBaseDbObject.LastEditedBy
        {
            get
            {
                return this.LastEditedBy;

            }
            set
            {
                this.LastEditedBy = (CmsUserBase)value;

            }
        }



        #endregion

        #region IBaseDbObject Members


        void DbObjects.IBaseDbObject.InitPropertiesForNewItem()
        {
            this.initPropertiesForNewItem();
        }

        void DbObjects.IBaseDbObject.resetDirtyFlag()
        {
            this.resetDirtyFlag();
            
        }

        #endregion

        #region Multilingual
        protected IMultilingualContentInfo _currentCultureInfo { get; private set; }

        protected virtual IEnumerable<IMultilingualContentInfo> __getMultilingualContentInfos()
        {
            throw new InvalidOperationException("If this is not overriden, this class is not multilingual!");
        }
        public virtual void __CheckItemCultureInfos(bool autoSaveInDB, IEnumerable<CultureDetailsBase> CultureInfos = null)
        {
            throw new InvalidOperationException("If this is not overriden, this class is not multilingual!");
        }
        protected IMultilingualContentInfo __getCultureByCultureDBId(long cultureDBId)
        {
            bool sessionExists = NHClasses.NhManager.CheckSessionExistsForContext();
            MyNHSessionBase session = null;
            if (!sessionExists)
            {

                CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Debug, "Session did not exist when retrieving the multilingual collection");

                session = NHClasses.NhManager.CreateNewSessionForContextAndReturn();

                this.Merge(session);
            }
            var cultureInfos = this.__getMultilingualContentInfos();
            foreach (var cultureInfo in cultureInfos)
            {
                if (cultureInfo.CultureInfo.ID == cultureDBId)
                {
                    _currentCultureInfo = cultureInfo;
                    break;
                }
            }
            if (!sessionExists)
            {
                NHClasses.NhManager.DisposeCurrentSessionInContext();
            }
            return _currentCultureInfo;
        }
      

        protected IMultilingualContentInfo __getCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 language )
        {
            var culture = BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetCultureByCode(language);
            return __getCultureByCultureDBId(culture.ID);
        }
        protected IMultilingualContentInfo __getCurrentCultureInfo()
        {

            var currentCultureID = GetCurrentCultureDBId();
            return __getCultureInfoByCultureId(currentCultureID);
        }
        protected IMultilingualContentInfo __getCultureInfoByCultureId(long cultureID)
        {

            if (_currentCultureInfo == null || _currentCultureInfo.CultureInfo.ID != cultureID)
            {   //requires re-loading
                _currentCultureInfo = null;
                _currentCultureInfo = __getCultureByCultureDBId(cultureID);
            }

            if (_currentCultureInfo == null)
            {
                this.__CheckItemCultureInfos(autoSaveInDB: false);

            }

            if (_currentCultureInfo == null)
            {
                _currentCultureInfo = __getCultureByCultureDBId(cultureID);
            }
            return _currentCultureInfo;
        }
        #endregion

        #region IBaseDbObject Members

        private int? _version;
        public int? Version
        {
            get { return _version; }
        }

        #endregion

        #region IBaseDbObject Members


        public object ItemLock
        {
            get { return GetFactory().GetItemLock(this); }
        }

        #endregion

        protected internal virtual bool nhibernate_onPreDelete(NHibernate.Event.PreDeleteEvent e)
        {
            return false;
        }

    }
}
