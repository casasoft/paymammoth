﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DB
{
    public interface IDbFactory<TItem> : IBaseDbFactory<TItem>
        where TItem: BusinessLogic_v3.Classes.DB.IBaseDbObject
    {
        
    }
}
