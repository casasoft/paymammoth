﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DB
{
    public class DbTempController : CS.General_v3.Classes.Factories.FactoryController<IBaseDbFactory>
    {
        public static DbTempController Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstance<DbTempController>();
                
            }
        }


    }
}
