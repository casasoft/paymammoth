﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Exceptions;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.NHibernateClasses.Transactions;
using BusinessLogic_v3.Util;
using CS.General_v3.Util;
using Iesi.Collections;
using Iesi.Collections.Generic;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using BusinessLogic_v3.Util.DbSpecific;
using NHibernate.Search;

namespace BusinessLogic_v3.Classes.DB
{

    
    public abstract class BaseDbFactoryBase<TItem, TInterface> : CS.General_v3.Classes.Factories.BaseFactory, IBaseDbFactory<TInterface>
       
        where TItem : class, BusinessLogic_v3.Classes.DB.IBaseDbObject, TInterface
        where TInterface: IBaseDbObject
            

    

    {
        /// <summary>
        /// If true, this will only load items created in this session
        /// </summary>
        public bool LoadOnlyItemsCreatedInThisUnitTestSession { get; set; }
        public void ReCreateLuceneIndex()
        {
            var session = (MyNHSessionNormal)BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            session.FlushMode = FlushMode.Never;
            session.CacheMode = CacheMode.Ignore;
           // var fullTextSession =  Search.CreateFullTextSession(session);
            
                int pageSize = 1000;
                int pageNo = 0;
                bool containsData = false;
                session.LucenePurgeAll(this.GetMainTypeCreatedByFactory());
                do
                {
                    using (var t = session.BeginTransaction())
                    {
                        containsData = false;
                        var q = GetCriteria();
                        int from = (pageNo*pageSize);
                        q.SetFirstResult(from);
                        q.SetMaxResults(pageSize);
                        var results = q.List();
                        if (results.Count > 0)
                        {
                            containsData = true;
                            foreach (var item in results)
                            {
                                session.LuceneIndex(item);
                                

                            }
                        }
                        t.Commit();
                    }
                    session.Clear();

                    pageNo++;
                } while (containsData);


            

        }

        public static void _initialiseStaticInstance()
        {
            var tmp = Instance;
        }

        private static BaseDbFactory<TItem, TInterface> _Instance = null;
        private static bool _Instance_LoadedFirstTime = false;
        public static BaseDbFactory<TItem, TInterface> Instance
        {
            get
            {
                if (_Instance_LoadedFirstTime == false)
                {
                    _Instance = CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<BaseDbFactory<TItem, TInterface>>(null);
                    _Instance_LoadedFirstTime = true;
                }
                return _Instance;
                
            }
        }
        private ILog _log = null;
        

        //public DetachedCriteria GetDetachedCriteria(bool loadTemporary = false, bool loadDeleted = false)
        //{

        //    DetachedCriteria crit = DetachedCriteria.For<TItem>();
            
        //    if (!loadTemporary)
        //    {
        //        var disjunction =  Restrictions.Disjunction();

        //        disjunction.Add(Restrictions.EqProperty("_Temporary_Flag", Projections.Constant(false)));

        //        disjunction.Add(Restrictions.EqProperty("_Temporary_Flag", Projections.Constant(null)));

        //        crit.Add(disjunction);
        //    }

        //    if (!loadDeleted)
        //    {
        //        var disjunction = Restrictions.Disjunction();

        //        disjunction.Add(Restrictions.EqProperty("_Deleted", Projections.Constant(false)));

        //        disjunction.Add(Restrictions.EqProperty("_Deleted", Projections.Constant(null)));

        //        crit.Add(disjunction);
        //    }
        //    return crit;
        //} 

        private void checkInitialItems()
        {
            if (CS.General_v3.Settings.Database.CheckDefaultsInRealDatabase && UsedInProject)
            {
                ICriteria crit = this.GetCriteria(null);
                crit.SetMaxResults(1);
                int count= nHibernateUtil.GetCountFromCriteria(crit);
                
                if (count  == 0)
                    fillInitialItemsInEmtpyDb();
            }

        }
        protected virtual void fillInitialItemsInEmtpyDb()
        {

        }

        public override void OnPostInitialisation()
        {
            base.OnPostInitialisation();
            
            checkInitialItems();
            
        }


      

        public void RegisterFactoryWithController()
        {

            Type baseType = typeof(IBaseDbFactory);
            
            //if (!this.GetType().IsAssignableFrom(baseType) )
            if (!baseType.IsAssignableFrom( this.GetType()))
            {
                throw new InvalidOperationException("In order to register this factory with the controller, make sure it implements it's project specific interface, e.g 'ISettingDataFactory'");
            }

            //Type tDataInterface = typeof(TDataInterface);
         //   BusinessLogic_v3.Classes.DB.DataFactoriesController.Instance.RemoveFactoryForType(this.TypesCreatedByFactory);
            //this.TypesCreatedByFactory.Add(tDataInterface);
            BusinessLogic_v3.Classes.DB.DataFactoriesController.Instance.AddFactory(this);
           
        }







        TInterface IBaseDbFactory<TInterface>.CreateNewItem()
        {
            return this.CreateNewItem();
        }

        TInterface IBaseDbFactory<TInterface>.GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return this.GetByPrimaryKey(id, session, lockMode);
        }

        IEnumerable<TInterface> IBaseDbFactory<TInterface>.FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null)
        {
            return this.FindAll(query, session);
        }

        IEnumerable<TInterface> IBaseDbFactory<TInterface>.FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null)
        {
            return this.FindAll(query, session);
        }

        IEnumerable<TInterface> IBaseDbFactory<TInterface>.FindAll(MyNHSessionBase session = null)
        {
            return this.FindAll(session);
        }

        TInterface IBaseDbFactory<TInterface>.FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null)
        {
            return this.FindItem(query, session);
        }

        TInterface IBaseDbFactory<TInterface>.FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null)
        {
            return this.FindItem(query, session);
        }

        TInterface IBaseDbFactory<TInterface>.FindItem(MyNHSessionBase session = null)
        {
            return this.FindItem(session);
        }

        IEnumerable<TInterface> IBaseDbFactory<TInterface>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null)
        {

            return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
        }

        //private static bool? _usedInProject = null;
        //public static bool UsedInProject
        //{
        //    get 
        //    {
        //        if (!_usedInProject.HasValue)
        //        {
        //            _usedInProject = (Instance != null);
        //        }
        //        return _usedInProject.GetValueOrDefault();
        //    }
        //}
        public static bool UsedInProject { get; protected set; }
        protected MyTransaction beginTransaction()
        {
            var session = getCurrentSessionInContext();
            if (session == null)
                throw new NoNhibernateSessionException();

            return session.BeginTransaction();
        }

        protected override IEnumerable<object> __getAllItemsInRepository()
        {

            return this.FindAll();

        }

        public string GetOutputCacheDependencyKey()
        {
            string key = "Factory-" + this.GetType().Name;
            return key;
        }

        public void InvalidateOutputCacheDependency()
        {
            string key = GetOutputCacheDependencyKey();
            CS.General_v3.Util.CachingUtil.InvalidateCacheDependency(key);


        }



        protected void flushCurrentSession()
        {
            NHClasses.NhManager.FlushCurrentSessionInContext();
        }

        public event DbObjects.General.OnItemUpdateHandler OnItemUpdate;



        //public readonly object factoryLock = new object();


        public BaseDbFactoryBase()
            : base()
        {
            

            this.FactoryPriority = Int32.MaxValue;
            addTypeCreatedByFactory(typeof(TItem));

            _log = log4net.LogManager.GetLogger("BusinessLogic_v3.Classes.DB.BaseDBFactory - " + this.GetType().Name);

            RegisterFactoryWithController();
            DbFactoryController.Instance.AddFactory(this);
            InvalidateOutputCacheDependency();
        }

        //public static int callCount = 0;










        #region IBaseDbFactory Members


        protected virtual void OnItemUpdated(IBaseDbObject obj, Enums.UPDATE_TYPE updateType)
        {
            if (OnItemUpdate != null)
            {
                //switch this off temporarily (2012/feb/28)
                OnItemUpdate(obj, updateType);
            }



        }



        /// <summary>
        /// Should return the total number of hours, before removing a temporary item from the database
        /// </summary>
        /// <returns></returns>
        protected virtual int getTotalHoursToRemoveTemporary()
        {
            return 7 * 24; // 7 days;
        }

        int IBaseDbFactory.GetTotalHoursToRemoveTemporary()
        {
            return getTotalHoursToRemoveTemporary();
        }







        #endregion

        
        private string getContextKey(string key)
        {
            return this.GetType().FullName + key;
        }

        protected T getFromContext<T>(string key)
        {
            return CS.General_v3.Util.PageUtil.GetContextObject<T>(getContextKey(key), throwErrorIfContextDoesNotExist: false);
        }
        protected void setInContext(string key, object value)
        {
            CS.General_v3.Util.PageUtil.SetContextObject(getContextKey(key), value, throwErrorIfContextDoesNotExist: false);
        }





        #region IBaseFactory<TItem,TCriteria,TSet,TDBInfo,TFactory> Members

        protected override object _createNewItem()
        {
            var item = (IBaseDbObject)base._createNewItem();
            item.InitPropertiesForNewItem();
            item.resetDirtyFlag();
            return item;
        }

        public new TItem CreateNewItem()
        {
            return (TItem)base.CreateNewItem();

        }






        #endregion


        private Type _lastType = null;

        ///// <summary>
        ///// Returns the 'final' type of TItem
        ///// </summary>
        ///// <returns></returns>
        //protected Type getLastType()
        //{
        //    if (_lastType == null)
        //    {
        //        _lastType = CS.General_v3.Util.ReflectionUtil.GetLastTypeInInheritanceChain(typeof(TItem),
        //                                                                                   CS.General_v3.Classes.Application.AppInstance.Instance.GetProjectAssemblies());

        //    }
        //    return _lastType;
        //}


        //private void addType()
        //{
        //    //Type t = CS.General_v3.Util.ReflectionUtil.GetTypeWhichExtendsFromAndIsNotAbstract(DatabaseConstants.ProjectSpecificAssembly, typeof(TItem));
        //    Type t = getLastType();

        //    //var item = CreateInstance();
        //    //Type t = item.GetType();
        //    addTypeCreatedByFactory(t);
        //    // item.Delete();

        //}


        public abstract void CheckCultureInfos();







        protected void postGetQueryOver(IQueryOver q, GetQueryParams qParams)
        {
            if (qParams.LoadOnlyItemsForFrontend)
                this.FillCriteriaWithConditionsToShowOnlyFrontendItems(q.RootCriteria);
        }

        public IEnumerable<TItem> FindAll(MyNHSessionBase session = null)
        {
            GetQueryParams qParams = new GetQueryParams(session: session);
            var c = GetCriteria(qParams);

            return FindAll(c);
        }


        public IEnumerable<TItem> FindAll(IQuery query)
        {

            IEnumerable<TItem> dbList = null;
            try
            {
                dbList = query.List<TItem>();
            }
            catch (GenericADOException ex)
            {
                throw;
            }
            return dbList;

        }

        public IEnumerable<TItem> FindAll(IQueryOver query, MyNHSessionBase session = null)
        {
            return FindAll(query.RootCriteria);

        }

        protected MyNHSessionBase getCurrentSessionInContext()
        {
            var session = NHClasses.NhManager.GetCurrentSessionFromContext();
            return session;
        }

        public IEnumerable<TItem> FindAll(IDetachedQuery query)
        {
            var session = NHClasses.NhManager.GetCurrentSessionFromContext();
            var q = query.GetExecutableQuery(session.GetAsISession());
            return FindAll(q);
        }




        public IEnumerable<TItem> FindAll(ICriteria crit, MyNHSessionBase session = null)
        {

            Iesi.Collections.ISet dbList = null;
            dbList = (ISet)CS.General_v3.Util.ReflectionUtil.CreateGenericType(typeof(OrderedSet<>), GetMainTypeCreatedByFactory());

            try
            {

                var results = crit.List<TItem>();
                
                if (_log.IsDebugEnabled)
                {
                    if (results.Count >= 500)
                    {
                        StringBuilder sb = new StringBuilder();
                        string msg = "FindAll<" + typeof(TItem).FullName +
                                        ">() returned a very large list, which might slow down greatly performance - Total count: " +
                                        results.Count;
                        sb.AppendLine(msg);
                        StringBuilder errMsg = new StringBuilder();
                        errMsg.AppendLine(msg);
                        errMsg.AppendLine("Criteria: " + crit.ToString());
                        errMsg.AppendLine("Stack Trace: " + System.Environment.StackTrace);

                        Exception ex = new Exception(errMsg.ToString());


                        _log.Debug(sb.ToString(), ex);
                    }
                }

                dbList.AddAll(results.ToList());

            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;
            }
            catch (HibernateException ex)
            {
                int k = 5;
                throw;
            }
            return (IEnumerable<TItem>)dbList;



        }
        public IEnumerable<TItem> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return FindAll(query.RootCriteria);

        }

        public IEnumerable<TItem> FindAll(DetachedCriteria crit)
        {
            return FindAll(crit.GetExecutableCriteria(getCurrentSessionInContext().GetAsISession()));

        }

        private TItem getItemFromList(IEnumerable<TItem> list)
        {
            return list.FirstOrDefault();

        }

        public TItem FindItem(QueryOver query)
        {
            var list = FindAll(query);
            return getItemFromList(list);
        }

        public TItem FindItem(IQueryOver query)
        {
            var list = FindAll(query);
            return getItemFromList(list);
        }



        private void checkSession(ref MyNHSessionBase session)
        {
            if (session == null)
                session = getCurrentSessionInContext();

        }






        #region IBaseDbFactory<TItem> Members

        public TItem ReloadItemFromDatabase(TItem item)
        {
            TItem newItem = null;
            if (item != null)
            {
                long id = item.ID;
                var session = BusinessLogic_v3.Classes.NHibernateClasses.NHClasses.NhManager.CreateNewSession();
                session.FlushMode = FlushMode.Never;

                newItem = this.GetByPrimaryKey(id, session);
                session.Dispose();
                var currentSession = BusinessLogic_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
                if (currentSession != null)
                    currentSession.Merge(item);

            }
            return newItem;
        }

        public TItem GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null)
        {
            checkSession(ref session);

            TItem item = default(TItem);
            if (id > 0)
            {
                try
                {
                    Type lastType = GetMainTypeCreatedByFactory();
                    
                    object itemBase = session.Get(lastType, id, lockMode);
                    item = (TItem)itemBase;
                    if (item != null)
                    {
                        item.resetDirtyFlag();
                    }
                }
                catch (GenericADOException ex)
                {
                    int k = 5;
                    throw;
                }
            }
            return item;
        }

        #endregion


        #region IBaseDbFactory Members


        IBaseDbObject IBaseDbFactory.GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return GetByPrimaryKey(id, session, lockMode);

        }

        IEnumerable<IBaseDbObject> IBaseDbFactory.FindAll(IQueryOver query, MyNHSessionBase session = null)
        {
            return FindAll(query).Cast<IBaseDbObject>();

        }

        IEnumerable<IBaseDbObject> IBaseDbFactory.FindAll(ICriteria query, MyNHSessionBase session = null)
        {
            return FindAll(query).Cast<IBaseDbObject>();
        }

        IEnumerable<IBaseDbObject> IBaseDbFactory.FindAll(MyNHSessionBase session = null)
        {

            return FindAll().Cast<IBaseDbObject>();
        }

        public DetachedCriteria GetDetachedCriteria(GetQueryParams qParams = null)
        {
            if (qParams == null) qParams = new GetQueryParams();

            var crit = DetachedCriteria.For(GetMainTypeCreatedByFactory());
            qParams.FillDetachedCriteria(crit, this);


            return crit;


        }

        public virtual void FillCriteriaWithConditionsToShowOnlyFrontendItems(ICriteria crit)
        {

        }

        public virtual ICriteria GetQueryToRemoveTemporaryItems(int pageSize, int pageNum, GetQueryParams qParams = null)
        {
            DateTime date = DateTime.Now.AddHours(-getTotalHoursToRemoveTemporary());
            var crit = GetCriteria(qParams);
            crit.Add(Restrictions.Eq(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(item => item._Temporary_Flag), true));
            crit.Add(Restrictions.Lt(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(item => item._Temporary_LastUpdOn), date));

            nHibernateUtil.LimitCriteriaByPrimaryKeys(crit,

                pageNum, pageSize);
            return crit;

        }

        #endregion

        #region IBaseDbFactory Members


        protected ICriteria getRawCriteria(MyNHSessionBase session)
        {
            if (session == null)
                session = BusinessLogic_v3.Classes.NHibernateClasses.NHClasses.NhManager.GetCurrentSessionFromContext();
            var crit = session.CreateCriteria(GetMainTypeCreatedByFactory());
            return crit;
        }


        public ICriteria GetCriteria(GetQueryParams qParams = null)
        {
            if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

            var crit = getRawCriteria(session);
            qParams.FillCriteria(crit, this);
            if (qParams.LoadOnlyItemsForFrontend)
                FillCriteriaWithConditionsToShowOnlyFrontendItems(crit);
            return crit;

        }

        #endregion

        #region IBaseDbFactory Members


        IBaseDbObject IBaseDbFactory.CreateNewItem()
        {
            return this.CreateNewItem();

        }

        #endregion

        #region IBaseDbFactory<TItem> Members


        public IEnumerable<TItem> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null)
        {
            List<TItem> list = new List<TItem>();
            if (loadAsSeperateQueries)
            {
                foreach (var id in ids)
                {
                    var item = GetByPrimaryKey(id, session);
                    if (item != null)
                        list.Add(item);
                }
                return list;
            }
            else
            {

                GetQueryParams qParams = new GetQueryParams(session: session);
                var crit = GetCriteria(qParams);
                crit.Add(Restrictions.InG<long>("ID", ids));
                return FindAll(crit);


            }

        }

        #endregion


        #region IBaseDbFactory<TItem> Members


        public TItem FindItem(IQueryOver query, MyNHSessionBase session = null)
        {
            return this.FindAll(query, session).FirstOrDefault();

        }

        public TItem FindItem(ICriteria query, MyNHSessionBase session = null)
        {
            return this.FindAll(query, session).FirstOrDefault();
        }

        public TItem FindItem(MyNHSessionBase session = null)
        {
            return this.FindAll(session).FirstOrDefault();
        }

        #endregion

        #region IBaseDbFactory Members


        IBaseDbObject IBaseDbFactory.FindItem(IQueryOver query, MyNHSessionBase session = null)
        {
            return this.FindAll(query, session).FirstOrDefault();

        }

        IBaseDbObject IBaseDbFactory.FindItem(ICriteria query, MyNHSessionBase session = null)
        {
            return this.FindAll(query, session).FirstOrDefault();

        }

        IBaseDbObject IBaseDbFactory.FindItem(MyNHSessionBase session = null)
        {
            return this.FindAll(session).FirstOrDefault();
        }

        #endregion



        #region IBaseDbFactory Members


        IEnumerable<IBaseDbObject> IBaseDbFactory.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null)
        {
            return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

        }

        #endregion

        #region IBaseDbFactory Members


        public void RemoveAnyUnitTestingItems()
        {
            List<Exception> exceptionList = new List<Exception>();
            var session = nHibernateUtil.GetCurrentSessionFromContext();
            MySQLUtil.SetForeignKeyCheck(false, session);


            var crit = getRawCriteria(null);
            crit.Add(NHibernate.Criterion.Expression.Where<IBaseDbObject>(x => x._TestItemSessionGuid != null && x._TestItemSessionGuid != ""));
            var result = FindAll(crit).ToList();
            
            for (int i = 0; i < result.Count; i++)
            {

                try
                {
                    using (var t = beginTransaction())
                    {
                        var item = result[i];
                        DeleteParams delParams = new DeleteParams(delPermanently: true);
                        item.Delete(delParams);
                        t.Commit();
                    }
                }
                catch (Exception ex)
                {
                    exceptionList.Add(ex);
                }
            }
                
            
            MySQLUtil.SetForeignKeyCheck(true, session);

        }

        #endregion



        #region IBaseDbFactory Members


        void IBaseDbFactory.OnItemUpdated(IBaseDbObject obj, Enums.UPDATE_TYPE updateType)
        {
            this.OnItemUpdated(obj, updateType);
            
        }

        #endregion

        

    }

    public abstract class BaseDbFactory<TItem, TInterface> : BaseDbFactoryBase<TItem,TInterface>, IBaseDbFactory<TItem>
        where TItem : class, BusinessLogic_v3.Classes.DB.IBaseDbObject, TInterface
        where TInterface : IBaseDbObject
    {



        protected void ifSessionIsNullThrowError(MyNHSessionBase session)
        {
            if (session == null)
            {
                throw new InvalidOperationException("Session is null! Make sure that if you are in a page or handler, they extend from 'WebComponents.Classes.Pages.BasePageBase' or 'WebComponents.Classes.HttpHandlers.BaseHandler', or if you are in a background thread you must manage session creation yourself");
            }
        }
        

        #region IBaseDbFactory Members

        IBaseDbObject IBaseDbFactory.CreateNewItem()
        {
            return base.CreateNewItem();
            
        }

        IBaseDbObject IBaseDbFactory.GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return base.GetByPrimaryKey(id, session, lockMode);
            
        }

        IEnumerable<IBaseDbObject> IBaseDbFactory.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null)
        {
            return base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
            
        }

        IEnumerable<IBaseDbObject> IBaseDbFactory.FindAll(IQueryOver query, MyNHSessionBase session = null)
        {
            return base.FindAll(query, session);
            
        }

        IEnumerable<IBaseDbObject> IBaseDbFactory.FindAll(ICriteria query, MyNHSessionBase session = null)
        {
            return base.FindAll(query, session);
            
        }

        IEnumerable<IBaseDbObject> IBaseDbFactory.FindAll(MyNHSessionBase session = null)
        {
            return base.FindAll(session);
            
        }

        IBaseDbObject IBaseDbFactory.FindItem(IQueryOver query, MyNHSessionBase session = null)
        {
            return base.FindItem(query, session);
            
        }

        IBaseDbObject IBaseDbFactory.FindItem(ICriteria query, MyNHSessionBase session = null)
        {
            return base.FindItem(query, session);
            
            
        }

        IBaseDbObject IBaseDbFactory.FindItem(MyNHSessionBase session = null)
        {
            return base.FindItem(session);
            
        }

        void IBaseDbFactory.OnItemUpdated(IBaseDbObject obj, Enums.UPDATE_TYPE updateType)
        {

            base.OnItemUpdated(obj, updateType);
        }

        int IBaseDbFactory.GetTotalHoursToRemoveTemporary()
        {
            return base.getTotalHoursToRemoveTemporary();
            
        }

        #endregion
    }
}
