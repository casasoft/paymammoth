﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;

using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using log4net;


namespace BusinessLogic_v3.Classes.DB
{
    public class DbTempItemChecker
    {
        private ILog _log = log4net.LogManager.GetLogger(typeof(DbTempItemChecker));
        public static DbTempItemChecker Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<DbTempItemChecker>(); } }

        private Timer _tmr;

        private void initTempTimer()
        {
           
            
           // _tmr.Interval = (double)interval;
          //  _tmr.Elapsed += new ElapsedEventHandler(_timer_Elapsed);
           
            //CS.General_v3.Util.TimerUtil.CallMethodAtSpecifiedDateTime(
            // _checkForTemporaryItems();

            //double interval = getTotalHoursToCheckForTemporaryAndRemove() * 60 * 60 * 1000;
            //_temporaryCheckerTimer.Elapsed += new ElapsedEventHandler(_temporaryChecker_timer_Elapsed);
            //checkForTemporaryItems();
        }

        private static readonly object _timerLock = new object();

        private void _checkForTemporaryItems()
        {
            CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(checkForTemporaryItems, "CheckForTemporaryItems", ThreadPriority.Lowest);
        }




        


        private void checkFactory(IBaseDbFactory dbFactory)
        {




            var session = NHClasses.NhManager.CreateNewSessionForContextAndReturn();
                
            bool retry = false;
            int pageSize = 50;
            int pageNum = 1;
            do
            {
                retry = false;
                var transaction = session.BeginTransaction();
                try
                {
                    var q = dbFactory.GetQueryToRemoveTemporaryItems(pageSize, pageNum);
                    if (q != null)
                    {

                        var items = dbFactory.FindAll(q).ToList();

                        if (items.Count > 0)
                            retry = true;
                        foreach (var item in items)
                        {
                            
                            try
                            {
                                item.DeleteTemporaryItemAsTimeElapsed();
                            }
                            catch (Exception ex)
                            {

                                int k = 5;
                            }
                           
                        }

                    }
                }
                catch (Exception ex)
                {
                    CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Warn,
                        "Error when trying to delete items (page " + pageNum + ", Factory: " + dbFactory.GetType().FullName + ")",ex);
                }
                finally
                {
                    bool disposed = false;
                    try
                    {
                        transaction.Commit();
                        session.Flush();
                        session.Clear();
                        //NhManagerIOC.NhManager.DisposeCurrentSessionInContext();
                        disposed = true;
                    }
                    catch (Exception ex)
                    {

                        List<string> msgs = new List<string>();
                        Exception curr = ex;
                        while (curr != null)
                        {
                            msgs.Add(curr.Message);
                            curr = curr.InnerException;
                        }
                        CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Warn, "Error while trying to flush/clear session in temporary (page " + pageNum + ", Factory: " + dbFactory.GetType().FullName + "): " + CS.General_v3.Util.Text.AppendStrings(", ", msgs), ex);
                        
                        int k = 5;
                    }
                    finally
                    {
                        
                       // if (!disposed)
                            //NhManagerIOC.NhManager.DisposeCurrentSessionInContext(throwErrorIfSessionDoesNotExistOrDisposed: false);
                    }
                }
                pageNum++;
               // session = null;
                if (retry)
                    System.Threading.Thread.Sleep(2000); //wait 5 seconds until removing next batch
            } while (retry);
            try
            {
                NHClasses.NhManager.DisposeCurrentSessionInContext();
            }
            catch (Exception ex)
            {
                CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Warn, 
                    "Error while trying to DISPOSE session in temporary (Factory: " + dbFactory.GetType().FullName + ")", ex);
            }

        }

        private void checkForTemporaryItems()
        {

            lock (_timerLock)
            {
                _log.Debug("Checking for temporary items to remove");
                try
                {
                    var allFactories = DbFactoryController.Instance.GetAllFactories().ToList();
                    for (int i=0; i < allFactories.Count;i++)
                    
                    {
                        var factory = allFactories[i];
                        try
                        {
                            
                            checkFactory(factory);
                        }
                        catch (Exception ex)
                        {
                            CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Warn, new InvalidOperationException("General error in checkForTemporaryItems() [Factory: " + factory.GetType().FullName + "]", ex));
                        }
                    }
                }
                catch (Exception ex)
                {

                    CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Warn, new InvalidOperationException("General error in checkForTemporaryItems() [General]", ex));
                    
                }
            }
        }

        void _timer_Elapsed(object state)
        {
            //2011-06-17 Commented by Mark
            //_tmr.Stop();
            lock (_timerLock)
            {
                _checkForTemporaryItems();
            }
            //_tmr.Start();
        }
        private DbTempItemChecker()
        {
            initTempTimer();
        }

        public void Start()
        {
            return; //uncomment this to stop this from starting

            if (CS.General_v3.Settings.Database.EnableDbTempItemChecker)
            {

                int interval = 15*1000;

                interval = 3*60*60*1000; // 3 hours

                _tmr = new Timer(_timer_Elapsed, null, 5000, interval);
            }

            //return;

            //_checkForTemporaryItems();
            //_tmr.Start();

        }

    }
}
