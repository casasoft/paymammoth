﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using CS.General_v3.Classes.HelperClasses;

using BusinessLogic_v3.Classes.DbObjects;
using CS.General_v3.Classes.Interfaces.Hierarchy;

namespace BusinessLogic_v3.Classes.Hierarchies
{
    public class HierarchyRecursionRemover
    {

        public HierarchyRecursionRemover()
        {

        }

        
        public OperationResult Result { get; private set; }


        private void checkItem(List<long> currentParents, IHierarchyEditable c)
        {
            IBaseDbObject dbObject = (IBaseDbObject) c;
           // dbObject.ReloadFromDb();
            var childLinks = c.GetChildren().ToList();
            for (int i=0; i < childLinks.Count;i++)
            
            {
                var child = childLinks[i];
                if (child != null)
                {
                    if (currentParents.Contains(((IHierarchy)child).ID))
                    {
                        using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                        {
                            Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning,
                                                "Cannot have <" + child.ToString() + "> as a child of <" + c.ToString() + " when it is already as one of its parents");

                            c.RemoveChild(child);
                            dbObject.Save();
                            t.Commit();
                        }
                        //this is invalid
                    }
                    else
                    {
                        var newList = currentParents.ToList();
                        newList.Add(((IHierarchy)c).ID);
                        checkItem(newList, child);
                    }
                }
            }
        }



        public void Check(IEnumerable<IHierarchyEditable> rootItems)
        {
            Result = new OperationResult();

            foreach (var c in rootItems)
            {
                List<long> ids = new List<long>();
                checkItem(ids, c);
            }
            
        }

    }
}
