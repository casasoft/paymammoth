﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace BusinessLogic_v3.Classes.Hierarchies
{
    public interface IHierarchyDbItem : IBaseDbObject
        
    {

        IEnumerable<IHierarchyDbItem> GetParents();
        IEnumerable<IHierarchyDbItem> GetChildren();

    }
}
