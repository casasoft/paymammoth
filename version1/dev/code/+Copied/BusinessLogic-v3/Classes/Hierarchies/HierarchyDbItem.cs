﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;

namespace BusinessLogic_v3.Classes.Hierarchies
{
    /// <summary>
    /// This class should contain any methods that are common to hierarchy-type database objects
    /// </summary>
    public class HierarchyDbItem 
        
    {
        private IHierarchyDbItem _item = null;
        public HierarchyDbItem(IHierarchyDbItem item)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item,null);
            this._item = item;
            this._item.OnUpdateComputedDeletedValue += new BusinessLogic_v3.Classes.DbObjects.Parameters.UpdateComputedDelValueHandler(_item_OnUpdateComputedDeletedValue);
            this._item.OnDeleteAfter += new BusinessLogic_v3.Classes.DbObjects.Parameters.DeleteBeforeAfterHandler(_item_OnDeleteAfter);
        }

        void _item_OnDeleteAfter(CS.General_v3.Classes.HelperClasses.OperationResult result, BusinessLogic_v3.Classes.DbObjects.Parameters.DeleteBeforeAfterParams delParams)
        {
            if (!delParams.DeletePermanently && _item._ComputedDeletedValue)
            {
                foreach (var c in _item.GetChildren())
                {
                    c.UpdateComputedDeletedValue(autoSave: true);
                }
            }
        }

        void _item_OnUpdateComputedDeletedValue(BusinessLogic_v3.Classes.DbObjects.Parameters.UpdateComputedDelValueArgs args)
        {

            args.ComputedDeletedValue = (_item.Deleted || CheckIfAllParentsAreDeleted()); //mark as computed deleted if this is deleted, or all its parents are deleted

            
            
        }



        public  bool CheckIfAllParentsAreDeleted()
        {
            bool b = true;
            foreach (var p in _item.GetParents())
            {
                if (!p._ComputedDeletedValue)
                {
                    b = false;
                    break;
                }
            }
            return b;

        }




    }
}
