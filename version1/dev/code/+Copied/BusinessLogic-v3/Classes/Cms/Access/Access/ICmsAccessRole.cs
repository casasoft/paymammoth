﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Access
{
    public interface ICmsAccessRole 
    {
        string Identifier { get; }
        bool CompareRole(ICmsAccessRole compareTo);

    }
}
