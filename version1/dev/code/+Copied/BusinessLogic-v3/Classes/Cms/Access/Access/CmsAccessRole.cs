﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Access
{
    public class CmsAccessRole  : ICmsAccessRole
    {
        public CmsAccessRole(string identifier)
        {
            this.Identifier = identifier;
        }
        public CmsAccessRole(Enum enumValue)
        {
            this.Identifier = CS.General_v3.Util.EnumUtils.StringValueOf(enumValue);
        }

        public string Identifier{get;set;}

        public bool CompareRole(ICmsAccessRole compareTo)
        {
            return string.Compare(this.Identifier, compareTo.Identifier, true) == 0;
            
        }
        public override int GetHashCode()
        {
            return this.Identifier.GetHashCode();


        }
        public override bool Equals(object obj)
        {
            bool ok = false;
            if (obj is ICmsAccessRole)
            {
                ICmsAccessRole cmpObj = (ICmsAccessRole)obj;
                ok = (string.Compare(cmpObj.Identifier, this.Identifier, true) == 0);
            }
            return ok;

        }
    }
}
