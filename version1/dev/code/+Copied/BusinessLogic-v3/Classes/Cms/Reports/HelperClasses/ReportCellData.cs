﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;

namespace BusinessLogic_v3.Classes.Cms.Reports.HelperClasses
{
    public class ReportCellData
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public CSSManager CssManager { get; private set; }
        public ReportCellData(string title = null)
        {
            this.Title = title;
            this.CssManager = new CSSManager();
        }
    }
}
