﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.Reports.HelperClasses
{
    public class ReportSectionData
    {
        public string SectionTitle { get; set; }
        public string SectionHeader { get; set; }
        public string SectionFooter { get; set; }
    }
}
