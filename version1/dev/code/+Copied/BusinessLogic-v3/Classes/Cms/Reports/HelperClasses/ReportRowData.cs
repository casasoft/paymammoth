﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;

namespace BusinessLogic_v3.Classes.Cms.Reports.HelperClasses
{
    public class ReportRowData
    {
        public List<ReportCellData> Cells { get; set; }
        public CSSManager CssManager { get; private set; }
        

        public ReportRowData()
        {
            this.Cells = new List<ReportCellData>();
            this.CssManager = new CSSManager();
        }

    }
}
