﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;

namespace BusinessLogic_v3.Classes.Cms.Factories
{
    public class FactoriesController : CS.General_v3.Classes.Factories.FactoryController<ICmsItemFactoryBL>
    {
        public static FactoriesController Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<FactoriesController>(); } }
        private FactoriesController()
        {

        }

    }
}
