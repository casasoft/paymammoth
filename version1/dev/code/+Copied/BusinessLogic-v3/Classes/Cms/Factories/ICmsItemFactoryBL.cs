﻿using System;
using System.Collections.Generic;
using BusinessLogic_v3.Classes.Cms.Factories;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;



namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Factories
{
    public interface ICmsItemFactoryBL : CS.General_v3.Classes.Factories.IBaseFactory
    {
        IBaseDbFactory GetDbFactory();
        IEnumerable<ICmsItemInstanceBL> GetAllItemsInRepository();
        
        /// <summary>
        /// Type of CmsItem, e.g Artist
        /// </summary>
        Type CmsItemType { get; }
        /// <summary>
        /// Checks whether this factory belongs to the given type, e.g Is factory of Artist?
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
     //   bool CheckIfFactoryIsOfType(Type t);

        //CmsAccessType AccessTypeRequired_ToAdd { get; }
       // CmsAccessType AccessTypeRequired_ToView { get;  }
       // bool CurrentUserCanView();
        CS.General_v3.Classes.URL.URLClass GetAddUrl();
        int CmsMainMenuPriority { get; }
        bool ShowInCmsMainMenu { get; }
        

        CS.General_v3.Classes.URL.URLClass GetEditUrlForItemWithID(long id);

        string Name { get;  }

       
        ICmsItemInstanceBL GetCmsItemFromDBObject(BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject dbObject);

      //  ICmsItemFactory LinkedParent { get; }
        //IEnumerable<ICmsItemFactory> GetLinkedChildren();
        //IEnumerable<CmsPropertyInfo> GetLinkedProperties();
        ICmsItemInstanceBL GetCmsItemWithId(long id);
        /// <summary>
        /// This must take care about the links, making sure that if the item has linked items, it is automatically linked e.g, if you create a new product and there is currently the store id in the querystring, it is updated
        /// </summary>
        /// <returns></returns>
        ICmsItemInstanceBL CreateNewItem(bool isTemporary);
        ICmsItemInstanceBL GetEmptyCmsItemInfo();
        /// <summary>
        /// Returns the current ID based on the querystring
        /// </summary>
        /// <returns></returns>
        long? GetIDFromCurrentQuerystring();
        
        
        string GetGoBackUrlForListing();
        string GetGoBackUrlForEdit();

        string QueryStringParamID { get; }
        string TitlePlural { get;  }
        string TitleSingular { get; }

        
        CS.General_v3.Classes.URL.URLClass GetListingUrl(bool removeAnyQuerystringParameters = false);


        ICmsItemGeneralInfoBL UserSpecificGeneralInfoInContext { get; }
        

       
    }
}
