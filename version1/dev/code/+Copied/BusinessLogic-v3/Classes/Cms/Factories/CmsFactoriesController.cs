﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;

namespace BusinessLogic_v3.Classes.Cms.Factories
{
    public class CmsFactoriesController : CS.General_v3.Classes.Factories.FactoryController<ICmsItemFactoryBL>
    {
        public static CmsFactoriesController Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<CmsFactoriesController>(); } }
        private CmsFactoriesController()
        {

        }

    }
}
