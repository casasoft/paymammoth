using System.Collections.Generic;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Modules.CmsUserModule;

namespace BusinessLogic_v3.Classes.Cms.Factories
{
    public interface ICmsItemGeneralInfoBL
    {
        string TitlePlural { get; set; }
        string TitleSingular { get; set; }
        int CmsMainMenuPriority { get; set; }
        bool GetWhetherToShowInCmsMainMenu();
        bool GetWhetherToShowAddNewButton();

        bool CheckIfUserCanView(ICmsUserBase user);
        bool CheckIfUserCanAdd(ICmsUserBase user);
        bool CheckIfUserCanDelete(ICmsUserBase user);
        bool CheckIfUserCanExport(ICmsUserBase user);
        
        CmsAccessType GetAccessRequiredToView();
        CmsAccessType GetAccessRequiredToDelete();
        CmsAccessType GetAccessRequiredToAdd();
        
        string Name { get; set; }


        bool ShowInCmsMainMenu { get; set; }
    }
}