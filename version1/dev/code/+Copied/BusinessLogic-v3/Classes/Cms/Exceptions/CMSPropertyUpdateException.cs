﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.Exceptions
{
    public class CMSPropertyUpdateException : Exception
    {
        public CS.General_v3.Enums.STATUS_MSG_TYPE MessageType { get; set; }
        //public CS.General_v3.Enums.STATUS_MSG_TYPE MessageType { get; set; }
        public CMSPropertyUpdateException(string msg)
            : this (msg, CS.General_v3.Enums.STATUS_MSG_TYPE.Warning)
        {

        }
        public CMSPropertyUpdateException(string msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType)
            : base(msg)
        {
            this.MessageType = msgType;

        }
    }
}
