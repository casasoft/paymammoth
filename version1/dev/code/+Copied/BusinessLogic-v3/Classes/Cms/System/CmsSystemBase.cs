﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.System
{
    public abstract class CmsSystemBase
    {
        public abstract bool CheckIfCurrentRequestInCms();

        public static CmsSystemBase Instance
        {

            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<CmsSystemBase>(CS.General_v3.Classes.Application.AppInstance.Instance.GetProjectAssemblies()); }
        }


    }
}
