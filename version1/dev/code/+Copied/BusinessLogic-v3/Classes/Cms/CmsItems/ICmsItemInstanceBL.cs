﻿using System;
using System.Collections.Generic;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;

using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Classes.HelperClasses;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public interface ICmsItemInstanceBL
    {
        //ICMSPropertySpecificInfo GetPropertySpecificInfoForProperty(CMSPropertyGeneralInfo propertyInfo);
        BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject DbItem { get; }
        ICmsItemFactoryBL GetCmsFactoryForItem();

        bool CheckIfStillTemporary();
        bool HasTemporaryFields();

        void ErrorOccurredWhileSavingInCms();
        bool CheckIfUserCanEdit(ICmsUserBase user);
        bool CheckIfUserCanDelete(ICmsUserBase user);
        bool CheckIfUserCanView(ICmsUserBase user);
        CmsAccessType GetAccessRequiredToDelete();
        CmsAccessType GetAccessRequiredToEdit();
        CmsAccessType GetAccessRequiredToView();
        void CheckCustomAccess(OperationResult result);
        
    }
}
