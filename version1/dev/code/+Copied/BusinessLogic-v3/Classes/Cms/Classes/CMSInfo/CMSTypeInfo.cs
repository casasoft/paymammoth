﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.Classes.CMSInfo
{
    public class CMSTypeInfo<TItem>
    {
        public CMSTypeInfo()
        {
          //  _searchValues = new Dictionary<string, object>();
        }
        public string TitleSingular { get; set; }
        public string TitlePlural { get; set; }
        public string QueryStringParamID { get; set; }

        public void FillFromType()
        {
            this.TitleSingular = CS.General_v3.Util.ReflectionUtil<TItem>.GetNameFromType(typeof(TItem));
            this.TitlePlural = CS.General_v3.Util.Text.ConvertEnglishWordToPlural(CS.General_v3.Util.ReflectionUtil<TItem>.GetNameFromType(typeof(TItem)));
            this.QueryStringParamID = this.TitleSingular.Replace(" ", "") + "Id";


        }


        

        public List<CMSPropertyInfoBase<TItem>> Properties { get; set; }



    }
}
