﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms;
using BusinessLogic_v3.Classes.Cms.Util;


using BusinessLogic_v3.Classes.Exceptions;
using BusinessLogic_v3.Classes.DB;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Controls.WebControls.Common;
using NHibernate;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsDbOperations;

namespace BusinessLogic_v3.Classes.Cms.Pages
{
    public abstract class CmsBaseEditPage : BasePage
    {
        protected abstract FormFields _formFields { get; }
        protected abstract PlaceHolder _phBeforeForm { get; }
        protected abstract PlaceHolder _phAfterForm { get; }
         public CmsBaseEditPage()
        {

        }

         public CmsBaseEditPage(ICmsItemFactory factory)
        {
            //the parameter is just to satisfy old requirements
        }

    }
}