﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Classes.Cms.Util;
using BusinessLogic_v3.Modules.CmsUserModule;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;


namespace BusinessLogic_v3.Classes.Cms.Pages
{
    public abstract class BasePage : BusinessLogic_v3.Classes.Pages.BasePage
    {
        private void checkIPAddressRestriction()
        {
            //CmsUtil.CheckIPWhiteListingOmitted();
            if (this.Functionality.CheckRestrictedIPAccess && CmsUserSessionLogin.Instance.IsLoggedIn)
            {
                bool ok = CmsUtil.IsUserIPWhitelistedToAccessCMS();
                if (!ok)
                {
                    CmsUserSessionLogin.Instance.Logout();

                    this.Master.Functionality.ShowIPRestricedError();
                }

            }
        }
        protected override void OnLoad(EventArgs e)
        {
            checkIPAddressRestriction();
            base.OnLoad(e);
        }

        protected string getFactoryTitleFromRouteData()
        {
            return CS.General_v3.Util.PageUtil.GetRouteDataVariable(CmsConstants.ROUTING_PARAM_TITLE);
        }

        private void setCustomCulture()
        {
            
            BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.CustomCultureOverrideIDForCurrentContext = this.Master.Functionality.CurrentCmsCultureID;
        }
        private void resetCustomCulture()
        {
            BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.CustomCultureOverrideIDForCurrentContext = null;
        }

        
        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
            //resetCustomCulture();
        }
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            base.Render(writer);
            resetCustomCulture();
        }

        public new BaseMasterPage Master
        {
            get
            {
                return (BaseMasterPage)base.Master;
            }
        }

        public CmsUserBase GetCurrentLoggedInUser()
        {
            return this.Functionality.GetCurrentLoggedInUser();
        }
        
        
        private void checkAccess()
        {
            bool ok = this.Functionality.VerifyAccess();
            if (!ok)
            //if (!Functionality.NoAuthenticationRequired && !Functionality.MemberLogin.CheckAuthentication(Page.Functionality.AccessTypeRequired))
            {
                //Authentication is required and no member is logged in
                this.Master._lastLoggedInURLWithoutAccessForUser = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                this.Master.Functionality.ShowStatusMessage("You do not have access to visit this section.  ", CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
                string loginUrl = CmsObjects.CmsRoutesMapper.Instance.GetLoginPage();
                //string root = CmsObjects.CmsSystemBase.Instance.GetCmsRoot();
                CS.General_v3.Util.PageUtil.RedirectPage(loginUrl);
                
            }
        }

        /// <summary>
        /// This method is used to check further access requirements.  If this returns false, user is not given access.
        /// </summary>
        /// <returns></returns>
        protected virtual bool checkFurtherAccessRequirements()
        {
            return true;
        }

        public new class FUNCTIONALITY : BusinessLogic_v3.Classes.Pages.BasePage.FUNCTIONALITY
        {
            public bool CheckRestrictedIPAccess { get; set; }
            public CmsUserBase GetCurrentLoggedInUser()
            {
                
                return CmsUserSessionLogin.Instance.GetLoggedInUser();
            }

            private BasePage _page = null;
            /// <summary>
            /// Minium access level type to access this page (also required the roles as well)
            /// </summary>
            public virtual CmsAccessType AccessRequired { get; private set; }
            public virtual void SetAccessRoleRequired(Enum enumValue)
            {
                this.AccessRequired.AccessRolesRequired.Clear();
                this.AccessRequired.AddAccessRole(enumValue);
            }
            


            public FUNCTIONALITY(BasePage page) : base(page)
            {
                _page = page;
                this.AccessRequired = new CmsAccessType(null);
                this.CheckRestrictedIPAccess = true; // by default always check for restricted IP access
 

 
            }

            public bool VerifyAccess()

            {
                var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
                bool ok = false;

                if (loggedUser != null)
                {
                    ok = this.AccessRequired.CheckIfUserHasEnoughAccess(loggedUser);
                }
                else
                {
                    if (this.AccessRequired.MinimumAccessTypeRequired == CS.General_v3.Enums.CMS_ACCESS_TYPE.Everyone &&
                        this.AccessRequired.AccessRolesRequired.Count == 0)
                    {
                        ok = true;//only allow if no access is required and 

                    }

                }
                if (ok)
                {
                    ok = _page.checkFurtherAccessRequirements();

                }
                return ok;


            }

        }
        public new FUNCTIONALITY Functionality
        {
            get
            {
                return (FUNCTIONALITY)base.Functionality;
            }
            
        }
        
        public BasePage()
        {

            this.Functionality.OutputLanguageFromCurrentCulture = false;
            
            
            
        }
        protected override CS.General_v3.Classes.Pages.BasePage.FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
            
        }
        
        protected override void OnPreInit(EventArgs e)
        {
            
            
            setCustomCulture();
            
            base.OnPreInit(e);
        }
        protected override void OnInit(EventArgs e)
        {
            checkAccess(); 
            base.OnInit(e);
        }

        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get { return this.Master.HtmlTag; }
        }
    }
}