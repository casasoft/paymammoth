﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

namespace BusinessLogic_v3.Classes.Cms.Pages
{
    public abstract class LoginPage : BasePage
    {
        public LoginPage()
        {
            this.Functionality.AccessRequired.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Everyone;
            this.Functionality.CheckRestrictedIPAccess = false;
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            Master.Functionality.SetPageTitle(new PageTitle("Login", "default.aspx"));
            
            base.OnLoad(e);
        }
    }
}