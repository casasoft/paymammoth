﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Classes.URL;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Specialized.ListingClasses;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
using CS.General_v3.Controls.WebControls.Common.General;

using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.Util;
using System.Web.UI.WebControls;

using BusinessLogic_v3.Classes.Cms.WebControls;
using CS.General_v3.Classes.CSS;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Extensions;
using BusinessLogic_v3.Classes.DB;
using CS.General_v3.Classes.HelperClasses;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsDbOperations;

namespace BusinessLogic_v3.Classes.Cms.Pages
{
    public abstract class CmsBaseListingPage : BasePage



    {
        [Obsolete]
        protected abstract PlaceHolder _phListingComponents {get;}
        [Obsolete]
        protected abstract CS.General_v3.Controls.WebControls.Common.MyButton _btnListingUpdate { get; }
        [Obsolete]
        protected abstract CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.FormFields _formSearch { get; }
        [Obsolete]
        protected abstract PlaceHolder _phAfterListing { get; }
        [Obsolete]
        protected abstract PlaceHolder _phBeforeFormSearch { get; }
        [Obsolete]
        protected abstract PlaceHolder _phBeforeListing { get; }
       
        public CmsBaseListingPage()
        {

        }

        public CmsBaseListingPage(ICmsItemFactory factory)
        {
            //the parameter is just to satisfy old requirements
        }


    }
}