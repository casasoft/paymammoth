﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

namespace BusinessLogic_v3.Classes.Cms.Pages
{
    public abstract class WelcomePage : BasePage
    {
        public WelcomePage()
        {
            this.Functionality.AccessRequired.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser;
        }
        protected override void OnLoad(EventArgs e)
        {
            Master.Functionality.SetPageTitle(new PageTitle("Welcome", "welcome.aspx"));
            
            base.OnLoad(e);
        }
    }
}