﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls;

using BusinessLogic_v3.Modules.CultureDetailsModule;


namespace BusinessLogic_v3.Classes.Cms.UserControls
{
    public abstract class LanguageSelect : BaseUserControl
    {
        static LanguageSelect()
        {
            { //load languages
                
            }
        }
       
        public abstract CS.General_v3.Controls.WebControls.Common.MyDropDownList cmbLanguage { get; }
        private void initHandlers()
        {
            cmbLanguage.OnChangeRedirectToValue = true;
            
            //cmbLanguage.SelectedIndexChanged += new EventHandler(cmbLanguage_SelectedIndexChanged);
        }

        void cmbLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            long langCode = (long)cmbLanguage.FormValueAsInt;
            var lang = CultureDetailsBaseFactory.Instance.GetByPrimaryKey(langCode);
            if (lang != null)
            {



                CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
                url[BusinessLogic_v3.Classes.Cms.Pages.BaseMasterPage.QUERYSTRING_PARAM_CULTUREID] = langCode;


                this.Page.Master.Functionality.ShowStatusMessage("Language changed successfully to '" + lang.Title + "'", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
                url.RedirectTo();


            }
        }

        protected virtual void buttonClick()
        {
            cmbLanguage_SelectedIndexChanged(null, null);
           
        }

        private int sortLangList(ICultureDetailsBase l1, ICultureDetailsBase l2)
        {
            return string.Compare(l1.Title, l2.Title);
        }

        private void initLanguages()
        {
            cmbLanguage.Items.Clear();
            List<ICultureDetailsBase> langList = new List<ICultureDetailsBase>();
            langList.AddRange(Util.CmsUtil.GetLanguages());
            //IList<Language_Base> langList = this.Languages;
            langList.Sort(sortLangList);
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
            string selectedValue = null;
            foreach (var lang in langList)
            {
                
                url[BusinessLogic_v3.Classes.Cms.Pages.BaseMasterPage.QUERYSTRING_PARAM_CULTUREID] = lang.ID;
                string langURL = url.ToString();
                if (lang.ID == this.Page.Master.Functionality.GetCurrentCultureID())
                {
                    selectedValue = langURL;
                }

                cmbLanguage.Items.Add(new ListItem(lang.Title, langURL));
            }
            cmbLanguage.SetValue(selectedValue);
        }
        
        protected override void OnLoad(EventArgs e)
        {
            
            initHandlers();
            initLanguages();
            base.OnLoad(e);
        }
    }
}
