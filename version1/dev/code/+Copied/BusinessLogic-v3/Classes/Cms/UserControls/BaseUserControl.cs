﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.Pages;

namespace BusinessLogic_v3.Classes.Cms.UserControls
{
    public class BaseUserControl : CS.General_v3.Controls.UserControls.BaseUserControl
    {
        public BaseMasterPage Master
        {
            get { return this.Page.Master; }
        }




        public new BasePage Page
        {
            get { return (BasePage)base.Page; }
        }
        protected virtual void initControls()
        {
            
        }
    }
}
