﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.TestCMSItems
{
    public class Store
    {
        public string Title { get; set; }
        public List<Product> Products { get; set; }

    }
}
