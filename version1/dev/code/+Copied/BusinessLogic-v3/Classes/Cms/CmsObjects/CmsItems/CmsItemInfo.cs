﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Linq.Expressions;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.DB;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.Exceptions;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;



using System.Web.UI.WebControls;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.NHManager;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Classes.DbObjects.Collections;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems
{
    public abstract class CmsItemInfo : ICmsItemInstance
    {


        public ColumnPrioritizer ListingOrderPriorities { get; private set; }
        public ColumnPrioritizer EditOrderPriorities { get; private set; }
        


            protected CmsUserBase getLoggedCmsUser()
        {
            
            return CmsUserSessionLogin.Instance.GetLoggedInUser(); 
        }

        protected ICmsItemFactory factory { get; private set; }
    
       
        private BusinessLogic_v3.Classes.DB.IBaseDbObject _dbItem = null;

        public override string ToString()
        {
            if (_dbItem!= null)
                return _dbItem.ToString();
            else
                return base.ToString();
        }
        protected abstract BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem { get; }
        public BusinessLogic_v3.Frontend._Base.IBaseFrontend FrontendItem
        {
            get { return _frontendItem; }
        }

        public IBaseDbObject DbItem
        {
            get
            {
                return _dbItem;
                
            }
        }
        //protected ICmsItemInstance item { get; private set; }

        protected List< CmsFieldBase> cmsProperties { get; private set; }
        protected virtual void initBasicFields()
        {


        }
        protected virtual void customInitFields()
        {

        }
        //private const string SESSION_STATUS_MESSAGE = "cs-cms-status-msg";
       // private const string SESSION_STATUS_MESSAGE_TYPE = "cs-cms-status-msg-type";
        public static void ShowErrorMessageInCMS(string msg, Exception ex)
        {
            OperationResult result = new OperationResult(CS.General_v3.Enums.STATUS_MSG_TYPE.Error,msg);
            result.AddException(ex);
            ShowStatusMessageInCMS(result);
        }
        public static void ShowStatusMessageInCMS(OperationResult result)
        {
            
            string msg = result.GetMessageAsHtml();
            if (string.IsNullOrEmpty(msg))
            {
                switch (result.Status)
                {
                    case CS.General_v3.Enums.STATUS_MSG_TYPE.Success: msg = "Operation completed successfully"; break;

                    case CS.General_v3.Enums.STATUS_MSG_TYPE.Warning: msg = "Operation completed with warnings"; break;
                    case CS.General_v3.Enums.STATUS_MSG_TYPE.Error: msg = "Operation could not be completed. Error occurred"; break;
                }
            }

            ShowStatusMessageInCMS(msg, result.Status);
        }

        public static void ShowStatusMessageInCMS(string message, CS.General_v3.Enums.STATUS_MSG_TYPE type)
        {
            //StringBuilder sb = new StringBuilder();
            //sb.Append(message);
            //if (ex != null)
            //{
            //    sb.Append("Exception:
            //}
            //if (ex

            CS.General_v3.Util.PageUtil.SetSessionObject(BusinessLogic_v3.Classes.Cms.Pages.BaseMasterPage.SESSION_STATUS_MESSAGE, message);
            CS.General_v3.Util.PageUtil.SetSessionObject(BusinessLogic_v3.Classes.Cms.Pages.BaseMasterPage.SESSION_STATUS_MESSAGE_TYPE, type);
        }
        public CmsItemInfo(ICmsItemFactory factory, IBaseDbObject dbItem)
        {
            this.AccessTypeRequired_ToDelete = new CmsAccessType(this);
            this.AccessTypeRequired_ToEdit = new CmsAccessType(this);
            this.AccessTypeRequired_ToView = new CmsAccessType(this);

            this.CustomCmsOperations = new List<CmsItemSpecificOperation>();
            this.ListingOrderPriorities = new ColumnPrioritizer();
            this.EditOrderPriorities = new ColumnPrioritizer();
            this.factory = factory;
            this._dbItem = dbItem;
            
            cmsProperties = new List<CmsFieldBase>();
            initBasicFields();
            customInitFields();

            {
                var factoryAccessTypeToDelete = factory.UserSpecificGeneralInfoInContext.GetAccessRequiredToDelete();
                this.AccessTypeRequired_ToDelete.CopyMostRestrictiveAccess(factoryAccessTypeToDelete);
            }
            {
                var factoryAccessTypeToView = factory.UserSpecificGeneralInfoInContext.GetAccessRequiredToView();
                this.AccessTypeRequired_ToView.CopyMostRestrictiveAccess(factoryAccessTypeToView);
            }

        }
        public void RemoveProperty(CmsFieldBase property)
        {
            cmsProperties.Remove(property);
        }
        /// <summary>
        /// Adds a custom property
        /// </summary>
        /// <typeparam name="T">Must be of type 'CmsPropertyBase'</typeparam>
        /// <param name="property"></param>
        /// <returns></returns>
        public CmsFieldBase AddProperty(CmsFieldBase property) 
        {
            return this.AddProperty<CmsFieldBase>(property);
            

        }
        /// <summary>
        /// Adds a custom property
        /// </summary>
        /// <typeparam name="T">Must be of type 'CmsPropertyBase'</typeparam>
        /// <param name="property"></param>
        /// <returns></returns>
        public T AddProperty<T>(T property) where T : CmsFieldBase
        {
            
            cmsProperties.Add(property);
            return property;
            
        }
       
        



        ///// <summary>
        /////Creates a link between a class and another, directly in the edit page.  This should be a many-collection which allows multiple values
        ///// </summary>
        ///// <typeparam name="TMainItem">The main item, e.g Category</typeparam>
        ///// <typeparam name="TLinkedItem">The linked item, e.g CategoryParentChildLink</typeparam>
        ///// <param name="selector">Selector to retrieve the property info for the collection, from TMainItem to a list of TLInkedItem</param>
        ///// <param name="listItemChoicesRetriever">A function that gets the selected item, and returns a list of ListItem</param>
        ///// <param name="itemToUniqueIDConverter">A function that converts the linked item, to the Unique ID. This must be the same ID, which would be returned as value of the 'ListItemChoicesRetriever' </param>
        ///// <param name="itemValueSetter">A function that sets the values, based from a list of strings</param>
        ///// <param name="displayType"></param>
        ///// <returns></returns>
        //public CmsPropertyMultiChoiceGeneric<TMainItem2, TLinkedItem2> AddManyToManyCollection<TMainItem2, TLinkedItem2>(System.Linq.Expressions.Expression<Func<TMainItem2, IEnumerable<TLinkedItem2>>> selector,
        //    Func<TMainItem2, IEnumerable<ListItem>> listItemChoicesRetriever, Func<TLinkedItem2, string> itemToUniqueIDConverter, Action<TMainItem2, IEnumerable<string>> itemValueSetter,
        //    EnumsCms.MULTICHOICE_DISPLAY_TYPE displayType
        //    )
        //     where TMainItem2 : BaseDbObject
        //     where TLinkedItem2: BaseDbObject
        //{

        //    var p = new CmsPropertyMultiChoiceGeneric<TMainItem2, TLinkedItem2>(this.factory, selector, listItemChoicesRetriever,
        //        itemToUniqueIDConverter, itemValueSetter, displayType);

        //    this.AddProperty(p);
        //    return p;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TItem">This should be the type of the DB class, e.g Advert or Advert Base</typeparam>
        /// <param name="property">The property that it is linked to.  Get this by Reflection</param>
        /// <param name="isRequired"></param>
        /// <param name="showInListing"></param>
        /// <returns></returns>
        
        public CmsPropertyMediaItem<TItem> AddPropertyForMediaItem<TItem>(Expression<Func<TItem, IMediaItem>> property, bool isRequired = false, bool showInListing = false)
        {
            

            CmsPropertyMediaItem<TItem> p = new CmsPropertyMediaItem<TItem>(this, property, isRequired);
            p.ShowInListing = showInListing;
            this.AddProperty(p);
            return p;
        }
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <typeparam name="TItem">This should be the type of the DB class, e.g Advert or Advert Base</typeparam>
        ///// <param name="property">The property that it is linked to.  Get this by Reflection</param>
        ///// <param name="isRequired"></param>
        ///// <param name="showInListing"></param>
        ///// <returns></returns>
        //public CmsPropertyMediaItem<TFrontend> AddPropertyForMediaItem<TFrontend>(Expression<Func<TFrontend, BaseMediaItemFile>> property, bool isRequired = false, bool showInListing = false)
        //    where TFrontend : BusinessLogic_v3.Modules.v2._Base_2.IBaseFrontendBase
        //{


        //    CmsPropertyMediaItem<TFrontend> p = new CmsPropertyMediaItem<TFrontend>(this.factory, property, isRequired);
        //    p.ShowInListing = showInListing;
        //    this.AddProperty(p);
        //    return p;
        //}
        ///// </summary>
        ///// <typeparam name="TData"></typeparam>
        ///// <param name="valueGetter">Required.  Should retrieve the value for this property</param>
        ///// <param name="valueSetter">Optional.  Sets the value to this property. If not set, it means property is readonly</param>
        ///// <param name="getSearchCriterion">Optional.  Retrieves </param>
        ///// <param name="orderBySetter">Optional. An action which sets the order by to the given criteria</param>
        ///// <returns></returns>
        //public CmsCustomProperty<TItem, TDataType> AddCustomProperty<TItem, TDataType>(Func<TItem, TDataType> valueGetter,
        //    Func<TItem, TDataType, OperationResult> valueSetter,
        //    Func<TDataType, NHibernate.Criterion.ICriterion> getSearchCriterion,
        //Action<NHibernate.ICriteria, CS.General_v3.Enums.SORT_TYPE> orderBySetter)
        //    where TItem : ICmsItemInstance
        //{
        //    var cmsProperty = new CmsCustomProperty<TItem, TDataType>(this.factory, valueGetter, valueSetter, 
        //        getSearchCriterion, orderBySetter);
        //    this.AddProperty(cmsProperty);
        //    return cmsProperty;
        //}
        public CmsPropertyInfo AddProperty(PropertyInfo pInfo, bool isRequired = false, bool isEditable = false, bool isVisible = true)
        {


            var cmsProperty = new CmsPropertyInfo(this, pInfo);
            this.AddProperty(cmsProperty);
            

            cmsProperty.IsRequired = isRequired;
            { //init default non searchable fields

                List<string> listNonSearchableFields = new List<string>();
                listNonSearchableFields.Add("_Deleted");
                listNonSearchableFields.Add("ID");
                listNonSearchableFields.Add("_DeletedBy");
                listNonSearchableFields.Add("_DeletedOn");
                listNonSearchableFields.Add("_Temporary_Flag");
                listNonSearchableFields.Add("_Temporary_LastUpdOn");
                if (listNonSearchableFields.Contains(cmsProperty.PropertyInfo.Name))
                {
                    cmsProperty.IsSearchable = false;
                    var user = getLoggedCmsUser();
                    if (user == null || user.CheckAccessIsLessThan(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft))
                    {
                        cmsProperty.ShowInEdit = false;

                    }
                }
                


            }

            if (cmsProperty.PropertyInfo.Name.ToLower() == "id")
            {
                cmsProperty.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne, CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne, null);
            }

            if (!isEditable)
            {
                cmsProperty.AccessTypeRequired_Edit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne;

            }
            if (!isVisible)
                cmsProperty.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne;
            return cmsProperty;
        }
        public List<CmsFieldBase> GetListOfCMSProperties()
        {
            List<CmsFieldBase> list = new List<CmsFieldBase>();
            list.AddRange(this.cmsProperties);
            return list;
            
        }


        public CmsPropertyInfo GetCmsPropertyByPropertyInfo(PropertyInfo pInfo)
        {
            foreach (var item in cmsProperties)
            {
                if (item is CmsPropertyInfo)
                {
                    CmsPropertyInfo p = (CmsPropertyInfo)item;
                    if (p.PropertyInfo == pInfo)
                        return p;
                }
            }
            return null;
            
        }

        public bool CurrentUserCanDelete()
        {

            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            bool ok = false;
            if (loggedUser != null)
            {
                ok = (loggedUser.CheckAccess(this.AccessTypeRequired_ToDelete));
            }
            return ok;
        }
        
        public void SetAccessRequiredToOverrideForAllProperties(CS.General_v3.Enums.CMS_ACCESS_TYPE accessType)
        {

            foreach (var p in this.GetListOfCMSProperties())
            {
                p.AccessTypeRequired_View.AccessLevelRequiredToOverrideRoles = accessType;
                p.AccessTypeRequired_Edit.AccessLevelRequiredToOverrideRoles = accessType;
                
            }
        }


        #region ICmsItemInstance Members

        public CS.General_v3.Classes.HelperClasses.OperationResult DeleteFromCms()
        {

            CS.General_v3.Classes.HelperClasses.OperationResult result = new OperationResult();

            if (this.AccessTypeRequired_ToDelete.CheckIfCurrentLoggedInCmsUserHasEnoughAccess())
            {

                //  long id = this.DbItem.ID; 
                //var currentSession = NHClasses.NhManager.GetCurrentSessionFromContext();
                //bool b =currentSession.Contains(this.DbItem);
                //currentSession.Evict(this.DbItem);
                //b =currentSession.Contains(this.DbItem);


                // // NHClasses.NhManager.DisposeCurrentSessionInContext(); //dispose the current session, so that you can start with a 'clean slate'
                //  var cmsSession = NHClasses.NhManager.CreateNewSessionForContext();//create a new one





                var currSession = NHClasses.NhManager.GetCurrentSessionFromContext();

                try
                {
                    //  cmsSession.Merge(this.DbItem);

                    var deleteResult = this.DbItem.Delete();
                    result.AddFromOperationResult(deleteResult);
                    //   transaction.Commit();
                    NHClasses.NhManager.FlushCurrentSessionInContext();
                    NHClasses.NhManager.DisposeCurrentSessionInContext();

                    // NHClasses.NhManager.FlushCurrentSessionInContext();
                }
                catch (OperationException ex)
                {
                    result = ex.ResultInfo;
                    // transaction.Rollback();
                }
                catch (Exception ex)
                {
                    string msg = @"Error occurred while trying to delete <" + this.TitleForCms + "> with database ID: " + this.ID + @". Possible reasons could be 
that there are still items linked with this specific item.  Try to delete any linked items, like children, and try again.  If problem persists contact CasaSoft";

                    result.SetStatusInfo(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, msg, null, false);
                    result.AddException(ex);

                    //transaction.Rollback();

                }
                finally
                {
                    NHClasses.NhManager.CreateNewSessionForContext();
                }

                // transaction.Dispose(); 
                //   NHClasses.NhManager.DisposeCurrentSessionInContext(); //dispose the temporary session created only to delete
                //    NHClasses.NhManager.CreateNewSessionForContext(); //recreate a new one
                // currentSession.Merge(this.DbItem);
            }
            else
            {


                result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, this.DbItem.ToString() + " could not be deleted");
            }
            return result;
        }

        public IEnumerable<CmsFieldBase> GetCmsProperties()
        {
            return this.cmsProperties;
            
        }

        public ICmsItemFactory GetCmsFactoryForItem()
        {
            
            return CmsSystemBase.Instance.GetFactoryForType(this.GetType());
        }

        public long ID
        {
            get { return DbItem.ID; }
        }


        public OperationResult SaveFromCms(SaveParams saveParams = null)
        {
            //if ( item.hasLastEditedFields())
              //  item.updateLastEdited();
            OperationResult result = new OperationResult();


           
            try
            {
                result = DbItem.SaveAndCommit(saveParams);
                NHClasses.NhManager.FlushCurrentSessionInContext();
                if (result.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
                {
                    result.SetStatusInfo(CS.General_v3.Enums.STATUS_MSG_TYPE.Success, ((ICmsItemInstance)this).TitleForCms + " saved successfully", null, true);
                }
                else if (result.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Warning)
                {
                   // result.SetStatusInfo(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, ((ICmsItemInstance)this).TitleForCms + " saved successfully, with warnings.", null, false);
                }
            }
            catch (OperationException ex)
            {
                result = ex.ResultInfo;

            }
            catch (Exception ex)
            {
                result.SetStatusInfo(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Error occurred while trying to save item <" + this.TitleForCms + "> with ID: " + this.ID, null, false);
                result.AddException(ex);


            }
            finally
            {
               // NHClasses.NhManager.CreateNewSessionForContext();
            }
            
            
          

            return result;
        }




        public virtual string TitleForCms
        {
            get { return DbItem.ToString(); }
        }

        public bool CheckIfStillTemporary()
        {
            return DbItem._Temporary_Flag;
            
        }

        public bool HasTemporaryFields()
        {
            return true;
            
        }


        public bool CheckIfUserCanEdit(ICmsUserBase user)
        {
            if (user == null)
                user = getLoggedCmsUser();
            bool ok = false;
            ok = CheckIfUserCanView(user);
            if (ok)
                ok = AccessTypeRequired_ToEdit.CheckIfUserHasEnoughAccess(user);

            return ok;
        }
        public bool CheckIfUserCanView(ICmsUserBase user)
        {
            if (user == null)
                user = getLoggedCmsUser();
            bool ok = false;
            ok = AccessTypeRequired_ToView.CheckIfUserHasEnoughAccess(user);
            return ok;
        }
        public bool CheckIfUserCanDelete(ICmsUserBase user)
        {
            if (user == null)
                user = getLoggedCmsUser();

            bool ok = false;
            ok = CheckIfUserCanEdit(user);
            if (ok)
                ok = AccessTypeRequired_ToDelete.CheckIfUserHasEnoughAccess(user);
            return ok;
        }

        public virtual CmsAccessType AccessTypeRequired_ToDelete { get; private set; }

        public virtual CmsAccessType AccessTypeRequired_ToView { get; private set; }

        public virtual CmsAccessType AccessTypeRequired_ToEdit { get; private set; }
        
        public List<CmsItemSpecificOperation> CustomCmsOperations { get; private set; }

        /// <summary>
        /// Returns a list of cms operations
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<CmsItemSpecificOperation> GetCustomCmsOperations()
        {
            return this.CustomCmsOperations;

        }
        
        #endregion
        public virtual void ErrorOccurredWhileSavingInCms()
        {

        }

        

        #region ICmsItemInstance Members


        IBaseDbObject ICmsItemInstance.DbItem
        {
            get { return this.DbItem; }
        }

        #endregion

      


        public void SetDefaultSortField(CmsFieldBase sortBy, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            foreach (var p in this.cmsProperties)
            {
                if (p == sortBy)
                {
                    p.IsDefaultSortField = sortType;
                }
                else
                {
                    p.IsDefaultSortField = CS.General_v3.Enums.SORT_TYPE.None;
                }
            }
        }

        public void SetAccessRequiredToEditForProperties(CS.General_v3.Enums.CMS_ACCESS_TYPE minimumAccessRequired, CS.General_v3.Enums.CMS_ACCESS_TYPE accessToOverrideRoles,
            Enum role1, Enum role2, Enum role3, params CmsFieldBase[] properties)
        {
            foreach (var p in properties)
            {
                p.AccessTypeRequired_Edit.SetAccess(minimumAccessRequired, accessToOverrideRoles, role1, role2, role3);
            }

        }
        public void SetAccessRequiredToViewForProperties(CS.General_v3.Enums.CMS_ACCESS_TYPE minimumAccessRequired, CS.General_v3.Enums.CMS_ACCESS_TYPE accessToOverrideRoles,
            Enum role1, Enum role2, Enum role3, params CmsFieldBase[] properties)
        {
            foreach (var p in properties)
            {
                p.AccessTypeRequired_View.SetAccess(minimumAccessRequired, accessToOverrideRoles, role1, role2, role3);
            }

        }
        public void SetNonEditableFields(params CmsFieldBase[] properties)
        {
            foreach (var p in properties)
            {
                p.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne, CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            }
        }

        #region ICmsItemInstance Members


        ColumnPrioritizer ICmsItemInstance.GetListingOrderPrioritizer()
        {
            return this.ListingOrderPriorities;
            
        }

        ColumnPrioritizer ICmsItemInstance.GetEditOrderPrioritizer()
        {
            return this.EditOrderPriorities;
            
        }

        #endregion

        #region ICmsItemInstance Members


        public CmsAccessType GetAccessRequiredToDelete()
        {
            return this.AccessTypeRequired_ToDelete;
            
        }

        public CmsAccessType GetAccessRequiredToEdit()
        {
            return this.AccessTypeRequired_ToEdit;
            
        }

        public CmsAccessType GetAccessRequiredToView()
        {
            return this.AccessTypeRequired_ToView;
            
        }

        #endregion

        #region ICmsItemInstance Members


        public virtual void CheckCustomAccess(OperationResult result)
        {
            
        }

        #endregion
    }
}