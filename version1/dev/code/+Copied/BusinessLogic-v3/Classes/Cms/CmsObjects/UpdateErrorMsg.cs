﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.Exceptions;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public class UpdateErrorMsg2
    {
        public CS.General_v3.Enums.STATUS_MSG_TYPE StatusType { get; private set; }
        private List<string> errorMsgs = new List<string>();
        public void AddErrorMsg(string errorMsg, CS.General_v3.Enums.STATUS_MSG_TYPE statusMsgType, bool isHtml = true)
        {
            if (((int)statusMsgType > ((int)this.StatusType)));
                this.StatusType = statusMsgType;
            if (!isHtml)
                errorMsg = CS.General_v3.Util.Text.HtmlEncode(errorMsg);

            this.errorMsgs.Add(errorMsg);
        }
        
        public void AddErrorFromException(Exception ex, CmsPropertyFormData field)
        {
            if (field != null && (ex.InnerException is PropertyUpdateException || ex is PropertyUpdateException))
            {
                PropertyUpdateException propertyUpdateEx = null;
                if (ex is PropertyUpdateException) propertyUpdateEx = (PropertyUpdateException)ex;
                if (ex.InnerException is PropertyUpdateException) propertyUpdateEx = (PropertyUpdateException)ex.InnerException;

                this.AddErrorMsg("Error in updating field &lt;" + field.PropertyInfo.Label + "&gt; : " + propertyUpdateEx.Message, propertyUpdateEx.ResultInfo.Status);
            }
            else if (ex.InnerException is OperationException || ex is OperationException)
            {
                OperationException propertyUpdateEx = null;
                if (ex is OperationException) propertyUpdateEx = (OperationException)ex;
                if (ex.InnerException is OperationException) propertyUpdateEx = (OperationException)ex.InnerException;

                


            }
            else
            {
                Exception curr = ex;
                if (ex.InnerException != null) curr = ex.InnerException;
                string msg = null;
                if (field != null)
                {
                    msg = CS.General_v3.Util.Text.HtmlEncode("Error in updating field <" + field.PropertyInfo.Label + ">: " + curr.Message);
                        if (curr.InnerException != null)
                        msg += "<hr />" + CS.General_v3.Util.Text.TxtForHTML(curr.StackTrace);
                }
                else
                {
                     msg = CS.General_v3.Util.Text.HtmlEncode("Error occured while saving/updating information: " + curr.Message);
                    if (curr.InnerException != null)
                        msg += CS.General_v3.Util.Text.HtmlEncode(" (" + curr.InnerException.Message + ")");
                }
                msg += CS.General_v3.Util.Text.HtmlEncode(" (" + curr.InnerException.Message + ")");
                this.AddErrorMsg(msg, CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
            }
        }
        public string GetErrorMessageAsHtml()
        {
            return Util.ErrorsUtil.ConvertListOfErrorsToHTML(null, this.errorMsgs);
        }
    }
}
