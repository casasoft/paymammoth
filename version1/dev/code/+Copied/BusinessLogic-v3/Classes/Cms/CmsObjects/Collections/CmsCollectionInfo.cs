﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using System.Reflection;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DB;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using System.Web.UI.WebControls;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Collections
{
    public class CmsCollectionInfo : CmsFieldBase, ICmsCollectionInfo 
        
    {
        public bool ShowLinkInCms { get; set; }
        public PropertyInfo PropertyCollection { get; private set; }
        public PropertyInfo PropertyOnOtherSide { get; private set; }
        public Type LinkedType { get; private set; }
        public CmsCollectionInfo(ICmsItemInstance cmsItem, 
            PropertyInfo collectionProperty, 
            PropertyInfo propertyOnOtherSide)
            : base( cmsItem)
        {
            this.PropertyCollection= collectionProperty;
            this.PropertyOnOtherSide= propertyOnOtherSide;
            this.ShowInEdit = false;
            this.ShowInListing = false;
            this.IsSearchable = false;
            this.Label = collectionProperty.Name;
            this.LinkedType = this.PropertyOnOtherSide.DeclaringType;
        }

        public string GetLinkedQuerystringValueForItem(ICmsItemInstance item)
        {
            return item.DbItem.ID.ToString();

            //return item.GetCmsFactoryForItem().Name + "-" + item.ID;
        }

        public ICollectionManager GetCollection(ICmsItemInstance o)
        {
            return GetCollection(o.DbItem);
        }
        public ICollectionManager GetCollection(IBaseDbObject o)
        {
            return (ICollectionManager)PropertyCollection.GetValue(o, null);
        }


        public override TDataType GetValueForObject<TDataType>(ICmsItemInstance o)
        {
            return (TDataType)GetCollection(o);
        }

        public override CS.General_v3.Classes.HelperClasses.OperationResult SetValueForObject(ICmsItemInstance o, object value)
        {
            var collectionManager = GetCollection(o);

            throw new NotImplementedException();
        }

        public override object GetFormValueConvertedToPropertyDataType(object formValue, EnumsCms.SECTION_TYPE sectionType)
        {
            throw new NotImplementedException();
        }

        public override FormFieldBaseData GetFormFieldBaseDataForProperty(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {
            return null;
            throw new NotImplementedException();
        }

        protected override System.Web.UI.Control getControlNonEditable(ICmsItemInstance item)
        {
            Literal lit = new Literal();
            StringBuilder sb = new StringBuilder();
            var coll = GetCollection(item).ToList();
            if (coll.Count >0)
            {
                var cmsFactory= CmsFactoryController.Instance.GetFactoryForObject(coll[0]);
                foreach (var collItem in coll)
                {
                    var cmsItem = cmsFactory.GetCmsItemFromDBObject((BaseDbObject) collItem);

                    if (sb.Length > 0) sb.Append(", ");
                    sb.Append(CS.General_v3.Util.Text.HtmlEncode(cmsItem.TitleForCms));
                }

            }
            lit.Text = sb.ToString();
            return lit;
        }

        public override void AddCriterionToSearchCriteria(NHibernate.ICriteria crit, string searchValue)
        {

            
        }



        /// <summary>
        /// This will only work if the type is registered as a factory
        /// </summary>
        /// <returns></returns>
        public ICmsItemFactory GetFactoryForLinkedObject()
        {
            return CmsSystemBase.Instance.GetFactoryForType(this.LinkedType);
        }
        public const string QUERYSTRING_LINKED = "Linked";
      //  private bool _linkedWithInCms;

       
        /// <summary>
        /// Returns the listing url for the linked items, such that if this property represents Product.Store, this will return the Product Listing, for the Store specified in 'itemLinkedWith'
        /// </summary>
        /// <param name="itemLinkedWith"></param>
        /// <returns></returns>
        public string GetListingUrlForLinkedType(ICmsItemInstance itemLinkedWith)
        {
            var linkedFactory = this.GetFactoryForLinkedObject();
            
            CS.General_v3.Classes.URL.URLClass linkedUrl = linkedFactory.GetListingUrl();
            linkedUrl[this.GetLinkedQuerystringParamName()] = this.GetLinkedQuerystringValueForItem(itemLinkedWith);// itemLinkedWith.ID;
            //linkedUrl[ParentItem.QueryStringParamID] = item.ID;
            return linkedUrl.ToString();
        }

        public long? GetLinkedIDFromQuerystring()
        {
           // CS.General_v3.Util.ContractsUtil.Requires(ShowLinkInCms);
            string s = GetLinkedQuerystringParamName();

            string qsValue = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(s);
            string itemName;
            long itemValue;
            long? result = null;
            result = Util.CmsUtil.GetLinkItemAndValueFromQsParam(qsValue);
            return result;

        }

        public string GetLinkedQuerystringParamName()
        {
           // CS.General_v3.Util.ContractsUtil.Requires(ShowLinkInCms, "Make sure that you mark the link with 'LinkedWithInCms' for any hierarchy in cms");
            return this.PropertyOnOtherSide.Name + "-" + this.PropertyCollection.Name + "Link";
            

        }
        public override void ApplyOrderByToCriteria(NHibernate.ICriteria crit, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            

        }

        #region ICmsCollectionInfo Members


        ICollectionManager ICmsCollectionInfo.GetCollection(ICmsItemInstance o)
        {
            return (ICollectionManager) this.GetCollection(o);
            
        }

        ICollectionManager ICmsCollectionInfo.GetCollection(IBaseDbObject o)
        {
            return (ICollectionManager)this.GetCollection(o);
        }

        #endregion

        public bool PreFetchInListing { get; set; }
    }
}
