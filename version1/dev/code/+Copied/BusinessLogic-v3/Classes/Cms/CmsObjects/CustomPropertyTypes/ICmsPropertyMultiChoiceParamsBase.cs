using System.Reflection;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public interface ICmsPropertyMultiChoiceParamsBase
    {
        PropertyInfo Property { get; }

        IEnumerable<ListItem> GetListItemsFromItem(BusinessLogic_v3.Classes.DB.IBaseDbObject item);
        string GetUniqueIDFromItem(BusinessLogic_v3.Classes.DB.IBaseDbObject linkedItem);
        void SetValuesToItem(BusinessLogic_v3.Classes.DB.IBaseDbObject item, IEnumerable<string> values);
    }
}