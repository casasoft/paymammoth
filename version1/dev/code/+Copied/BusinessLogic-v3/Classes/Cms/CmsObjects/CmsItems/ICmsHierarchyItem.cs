﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems
{
    public interface ICmsHierarchyItem 
    {
        ICmsHierarchyInfo GetCmsHierarchyInfo();

    }
}
