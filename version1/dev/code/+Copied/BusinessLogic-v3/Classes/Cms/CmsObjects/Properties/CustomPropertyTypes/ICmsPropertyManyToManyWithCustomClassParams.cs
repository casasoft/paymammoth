using System.Reflection;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes
{
    /// <summary>
    /// This interface contains information for implementing a 'manual' many-to-many link. This means a relationship like for example Product [parent] -> has many -> Category [ child], implemented via a 'middle class' called
    /// ProductCategory. 
    /// </summary>
    public interface ICmsPropertyManyToManyWithCustomClassParams
    {
        PropertyInfo Property { get; }
        /// <summary>
        /// Should retrieve the property info that retrieves the 'linked item' from the many-to-many link, e.g gets the Category [linked item] from the ProductCategory [many-to-many link]
        /// </summary>
        /// <returns></returns>
        PropertyInfo GetPropertyToRetrieveLinkedItemFromLink();

        /// <summary>
        /// Returns the list of children available to be chosen for this relationship. (e.g Product [parent] -> has many -> Category [ child]  )
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        IEnumerable<ListItem> GetListItemsFromItem(BusinessLogic_v3.Classes.DB.IBaseDbObject item);


        


        /// <summary>
        /// Should return a unique identifier for the child. (e.g Product [parent] -> has many -> Category [ child]  )
        /// </summary>
        /// <param name="linkedItem">The db object for the [child].  (e.g Product [parent] -> has many -> Category [ child]  )</param>
        /// <returns></returns>
        string GetUniqueIDFromItem(BusinessLogic_v3.Classes.DB.IBaseDbObject linkedItem);

        /// <summary>
        /// Sets the 'children' to the list of selections in values. (e.g Product [parent] -> has many -> Category [ child]  )
        /// </summary>
        /// <param name="item"></param>
        /// <param name="values"></param>
        void SetValuesToItem(BusinessLogic_v3.Classes.DB.IBaseDbObject item, IEnumerable<string> values);

        
        /// <summary>
        /// Returns a custom criterion for searching this many-to-many link. For example, a search for the link between Product -> Category, would return a custom search
        /// that loads subcategories as well.  If returned as null, default search is taken, i.e one-to-one mapping.
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        NHibernate.Criterion.ICriterion GetCustomCriterionForSearch(NHibernate.ICriteria criteria, List<long> ids);
    }
}