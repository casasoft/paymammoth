﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Classes.DB;

using NHibernate.Criterion;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes
{
    /// <summary>
    /// Creates a link between a class and another, directly in the edit page.  This should be a many-collection which allows multiple values
    /// </summary>
    /// <typeparam name="TMainItem">The main item, e.g Product</typeparam>
    /// <typeparam name="TLink">The item, e.g ProductCategoryFeatureValue</typeparam>
    /// /// <typeparam name="TLink">The linked item, e.g CategoryFeature</typeparam>
    public class CmsPropertyManyToManyWithCustomClass<TMainItem, TLink, TLinkedItem> : CmsPropertyMultiChoice
        where TMainItem : BaseDbObject
        where TLink: BaseDbObject
        where TLinkedItem : BaseDbObject
        
       
    {
        public ICmsPropertyManyToManyWithCustomClassParams Parameters {get;set;}
        
        /// <summary>
        ///Creates a link between a class and another, directly in the edit page.  This should be a many-collection which allows multiple values
        /// </summary>
        /// <param name="itemFactory">CMS factory</param>
        /// <param name="selector">Selector to retrieve the property info for the collection, from TMainItem to a list of TLInkedItem</param>
        /// <param name="listItemChoicesRetriever">A function that returns a list of Web.ListItems possible for the given item. This is shown to the user for selection</param>
        /// <param name="itemToUniqueIDConverter">A function that converts the linked item, to the Unique ID. This must be the same ID, which would be returned as value of the 'ListItemChoicesRetriever' </param>
        /// <param name="itemValueSetter">A function that sets the values, based from a list of strings</param>
        public CmsPropertyManyToManyWithCustomClass(ICmsItemInstance cmsItem, ICmsPropertyManyToManyWithCustomClassParams parameters,
            EnumsCms.MULTICHOICE_DISPLAY_TYPE displayType
            )
            : base(cmsItem, null, displayType)
        {
            this.Parameters = parameters;
            setProperty(parameters.Property);
        }

        protected virtual IEnumerable<TLink> getSelectedValuesAsLinkedItemsForItem(ICmsItemInstance item)
        {


            IEnumerable<TLink> values = (IEnumerable<TLink>)this.PropertyInfo.GetValue(item.DbItem, null);

            return values;
        }

        protected override IEnumerable<string> getSelectedValuesForItem(ICmsItemInstance item)
        {

            IEnumerable<TLink> values = getSelectedValuesAsLinkedItemsForItem(item);
            List<string> list = new List<string>();
            foreach (var value in values)
            {
                list.Add(Parameters.GetUniqueIDFromItem(value));
            }
            return list;
        }

        protected override void setValueForCmsItem(ICmsItemInstance item, IEnumerable<string> values)
        {
            TMainItem dbItem = (TMainItem)item.DbItem;
            this.Parameters.SetValuesToItem(dbItem, values);
            
            
        }

        protected string getFullPropertyName()
        {
            return this.PropertyInfo.Name + "." + this.Parameters.GetPropertyToRetrieveLinkedItemFromLink().Name;
        }

        public override void AddCriterionToSearchCriteria(NHibernate.ICriteria criteria, string searchValue)
        {
            

            List<long> ids = new List<long>();
            List<string> sIDs = CS.General_v3.Util.Text.Split(searchValue, ",", "|");
            foreach (var sID in sIDs)
            {
                long id;
                if (long.TryParse(sID, out id))
                {
                    ids.Add(id);
                }
            }
            if (ids.Count > 0)
            {
                NHibernate.Criterion.ICriterion crit = this.Parameters.GetCustomCriterionForSearch(criteria, ids);
               // criteria.
                if (crit == null)
                {
                    criteria.CreateAlias(this.PropertyInfo.Name, this.PropertyInfo.Name);


                    string propertyName = getFullPropertyName();
                    crit = NHibernate.Criterion.Restrictions.InG(propertyName, ids);
                }
                if (crit != null)
                    criteria.Add(crit);
            }

        }
  

        public override object GetFormValueConvertedToPropertyDataType(object formValue, EnumsCms.SECTION_TYPE sectionType)
        {
            object result = null;
            var linkedItemFactory = CmsSystemBase.Instance.GetFactoryForType(typeof(TLinkedItem));
            { //this means a linked object
                string s = (formValue != null ? formValue.ToString() : null);
                // result = null;
                int num = 0;
                if (!int.TryParse(s, out num))
                    num = 0;
                if (num > 0)
                {
                    var factory = linkedItemFactory;
                    var item = factory.GetCmsItemWithId(num).DbItem;
                    result = item;
                }
                
                // break;
            }
            return result;
            
        }

        protected override IEnumerable<ListItem> getListItemChoices(ICmsItemInstance item)
        {
            BusinessLogic_v3.Classes.DB.IBaseDbObject dbItem = null;
            if (item != null) dbItem = item.DbItem;


            return this.Parameters.GetListItemsFromItem(dbItem);
            
        }

        

        protected override Control getControlNonEditable(ICmsItemInstance item)
        {

            Literal lit = new Literal();
            var selectedValues = getSelectedValuesAsLinkedItemsForItem(item);
            List<string> list = new List<string>();
            foreach (var v in selectedValues)
            {
                if (v != null)
                {
                    list.Add(v.ToString());
                }
            }

            lit.Text = CS.General_v3.Util.Text.AppendStrings(", ", list);
            return lit;

        }
    }
}