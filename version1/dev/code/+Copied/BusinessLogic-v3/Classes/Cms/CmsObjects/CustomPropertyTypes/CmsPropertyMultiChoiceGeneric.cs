﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
using System.Web.UI;
using BusinessLogic_v3.DB;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
using CS.General_v3.Classes.HelperClasses;
using System.Text;
using BusinessLogic_v3.Classes.DB;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    /// <summary>
    /// Creates a link between a class and another, directly in the edit page.  This should be a many-collection which allows multiple values
    /// </summary>
    /// <typeparam name="TMainItem">The main item, e.g ItemGroup</typeparam>
    /// <typeparam name="TLinkedItem">The linked item, e.g ItemGroupCategoryFeatureValue</typeparam>
    public class CmsPropertyMultiChoiceGeneric<TMainItem, TLinkedItem> : CmsPropertyMultiChoice
        where TMainItem : BaseDbObject
        where TLinkedItem: BaseDbObject
        
       
    {
        public ICmsPropertyMultiChoiceParamsBase Parameters {get;set;}
        
        /// <summary>
        ///Creates a link between a class and another, directly in the edit page.  This should be a many-collection which allows multiple values
        /// </summary>
        /// <param name="itemFactory">CMS factory</param>
        /// <param name="selector">Selector to retrieve the property info for the collection, from TMainItem to a list of TLInkedItem</param>
        /// <param name="listItemChoicesRetriever">A function that returns a list of Web.ListItems possible for the given item. This is shown to the user for selection</param>
        /// <param name="itemToUniqueIDConverter">A function that converts the linked item, to the Unique ID. This must be the same ID, which would be returned as value of the 'ListItemChoicesRetriever' </param>
        /// <param name="itemValueSetter">A function that sets the values, based from a list of strings</param>
        public CmsPropertyMultiChoiceGeneric(ICmsItemFactory itemFactory, ICmsPropertyMultiChoiceParamsBase parameters,
            EnumsCms.MULTICHOICE_DISPLAY_TYPE displayType
            )
            : base(itemFactory, null, displayType)
        {
            this.Parameters = parameters;
            setProperty(parameters.Property);
        }

        protected override IEnumerable<string> getSelectedValuesForItem(ICmsItemInstance item)
        {

            IEnumerable<TLinkedItem> values = (IEnumerable<TLinkedItem>)this.PropertyInfo.GetValue(item.DbItem, null);
            List<string> list = new List<string>();
            foreach (var value in values)
            {
                list.Add(Parameters.GetUniqueIDFromItem(value));
            }
            return list;
        }

        protected override void setValueForCmsItem(ICmsItemInstance item, IEnumerable<string> values)
        {
            TMainItem dbItem = (TMainItem)item.DbItem;
            this.Parameters.SetValuesToItem(dbItem, values);
            
            
        }

        protected override IEnumerable<ListItem> getListItemChoices(ICmsItemInstance item)
        {
            return this.Parameters.GetListItemsFromItem(item.DbItem);
            
        }

        

        protected override Control getControlNonEditable(ICmsItemInstance item)
        {

            Literal lit = new Literal();
            lit.Text = CS.General_v3.Util.Text.AppendStrings(", ", GetValueForObject(item));
            return lit;

        }
    }
}