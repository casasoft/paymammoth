﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;

using CS.General_v3.Classes.NHibernateClasses.NHManager;
using NHibernate;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.CmsDbOperations
{
    public class ItemUpdater
    {
        public bool IsNewItem { get; set; }
        public ItemUpdater( ICmsItemInstance item, bool isNewItem, EnumsCms.SECTION_TYPE sectionType)
        {
            this.PropertiesFromForm = new List<CmsPropertyFormData>();
            this.Item = item;
            this.IsNewItem = isNewItem;
            this.SectionType = sectionType;
            
        }
        public EnumsCms.SECTION_TYPE SectionType { get; set; }
        public ICmsItemInstance Item { get; set; }

        public List<CmsPropertyFormData> PropertiesFromForm { get; private set; }

        private StringBuilder changesDone = null;
        private void addChange(CmsPropertyFormData propertyFromForm, string oldValue, string newValue, bool errorHappened)
        {
            if (string.Compare(oldValue, newValue, false) != 0)
            {
                oldValue = oldValue ?? "";
                newValue = newValue?? "";
                StringBuilder changes = new StringBuilder();
                bool containsLine = false;

                if (
                    (
                    oldValue.Contains("\r") ||
                    oldValue.Contains("\n") ||
                    newValue.Contains("\r") ||
                    newValue.Contains("\n")))
                {
                    containsLine = true;
                }

                changes.Append(propertyFromForm.PropertyInfo.Label);
                if (errorHappened)
                {
                    changes.Append("[WARNING] ");
                }
                changes.Append(": ");
                if (!containsLine)
                {
                    changes.AppendLine("<[" + oldValue + "]>   --->   <[" + newValue + "]>");
                }
                else
                {
                    changes.AppendLine();
                    changes.AppendLine("Was:");
                    changes.AppendLine("----------------------------");
                    changes.AppendLine(oldValue);
                    changes.AppendLine();
                    changes.AppendLine("Updated to:");
                    changes.AppendLine("----------------------------");
                    changes.AppendLine(newValue);
                    changes.AppendLine("=============================================");
                }

                changesDone.Append(changes.ToString());
            }
        }

        public bool ChangesWereDone
        {
            get 
            {

                return this.changesDone.Length > 0;
            }
        }

        private void updatePropertiesInItemFromFormData()
        {
            foreach (var field in this.PropertiesFromForm)
            {
                if (field.FormControl != null) //if it's null, it should be in listing, whereby it does not have a form control and is not editable
                {
                    bool ok = true;
                    string oldValue = "";
                    string newValue = "";
                    try
                    {

                        oldValue = field.PropertyInfo.GetValueForObjectAsString(this.Item);

                        //object updatedValue = field.GetFormValue(Enums.SECTION_TYPE.EditPage);
                        var result = field.PropertyInfo.SetValueForObjectFromFormControl(this.Item, field.FormControl);
                        newValue = field.PropertyInfo.GetValueForObjectAsString(this.Item);
                        _result.AddFromOperationResult(result);
                    }

                    catch (Exception ex)
                    {
                        ok = false;
                        _result.AddException(ex);


                    }
                    addChange(field, oldValue, newValue, (!ok));
                }

            }

        }

        private void writeToAuditLog()
        {
            if (this.SectionType == EnumsCms.SECTION_TYPE.EditPage || (this.SectionType == EnumsCms.SECTION_TYPE.ListingPage && changesDone.Length > 0))
            {

                string msg = "";


                msg = (IsNewItem ? "Added new" : "Updated") + " " + Item.DbItem.GetType().Name + " <" + Item.DbItem.ToString() + ">, ID: " + Item.DbItem.ID.ToString();

                StringBuilder furtherInfo = new StringBuilder();
                furtherInfo.AppendLine("Status: " + CS.General_v3.Util.EnumUtils.StringValueOf(_result.Status));
                if (_result.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
                {
                    furtherInfo.AppendLine();
                    furtherInfo.AppendLine(_result.GetMessageAsString());
                }
                if (changesDone.Length > 0)
                {
                    furtherInfo.AppendLine();
                    furtherInfo.AppendLine("Changes Done:");
                    furtherInfo.AppendLine("-----------------");
                    furtherInfo.AppendLine(changesDone.ToString());
                    furtherInfo.AppendLine("=============================");
                }

                Util.CmsUtil.AddAuditLog((IsNewItem ? Enums.CMS_AUDIT_MESSAGE_TYPE.Add : Enums.CMS_AUDIT_MESSAGE_TYPE.Update),
                                         Item.DbItem.GetType().Name, Item.DbItem.ID,
                                         msg, furtherInfo.ToString());
            }
            

        }

        private OperationResult _result = null;
        public OperationResult UpdateDbItem()
        {
            _result = new OperationResult();
            changesDone = new StringBuilder();

            var session = NHClasses.NhManager.GetCurrentSessionFromContext();
            updatePropertiesInItemFromFormData();
            using (var transaction = session.BeginTransaction())
            {
                //transaction.AutoBeginTransactionOnCommit = true;
                

                if (_result.IsSuccessful)
                {
                    try
                    {
                        _result = this.Item.SaveFromCms();
                    }
                    catch (Exception ex)
                    {
                        _result.AddException(ex);
                    }
                }
                if (_result.IsSuccessful)
                {
                    transaction.CommitIfActiveElseFlush();
                }
                else
                {
                    this.Item.ErrorOccurredWhileSavingInCms();
                    //transaction.Rollback();
                }
                try
                {
                    transaction.Dispose();
                }
                catch (Exception ex)
                {
                    int k = 5;
                    throw;
                }
            }
            writeToAuditLog();
            return _result;
        }

    }
}
