﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Reflection;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public abstract class CmsPropertyMultiChoiceParamsBase<TMainItem, TLinkedItem> : ICmsPropertyMultiChoiceParamsBase
        where TMainItem : BusinessLogic_v3.Classes.DB.IBaseDbObject
        where TLinkedItem : BusinessLogic_v3.Classes.DB.IBaseDbObject

    {
        /// <summary>
        /// This should be a method that returns the list of possible list items for selection, for the [item]
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected abstract IEnumerable<ListItem> getListItemsFromItem(TMainItem item);
        /// <summary>
        /// This should return the Unique ID for the linked item, for example for a 'ContentPageParentChildLink', for a many-to-many from a child to a parent, it would be link.ParentID (string)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected abstract string getUniqueIDFromItem(TLinkedItem item);
        /// <summary>
        /// Should set the values to the main item, from a list of strings
        /// </summary>
        /// <param name="item"></param>
        /// <param name="values"></param>
        protected abstract void setValuesToItem(TMainItem item, IEnumerable<string> values);

        /// <summary>
        /// Returns the property that matches the many-to-many collection link, e.g cp => cp.ParentChildLinks;
        /// </summary>
        /// <returns></returns>
        protected abstract System.Linq.Expressions.Expression<Func<TMainItem, IEnumerable<TLinkedItem>>> getPropertySelector();
        


        public PropertyInfo Property { get; private set; }


        
        public CmsPropertyMultiChoiceParamsBase()
        {
            this.Property = CS.General_v3.Util.ReflectionUtil<TMainItem>.GetPropertyBySelector<IEnumerable<TLinkedItem>>(getPropertySelector());
            

        }



        #region ICmsPropertyMultiChoiceParamsBase Members

        IEnumerable<ListItem> ICmsPropertyMultiChoiceParamsBase.GetListItemsFromItem(BusinessLogic_v3.Classes.DB.IBaseDbObject item)
        {
            return getListItemsFromItem((TMainItem)item);
            
        }

        string ICmsPropertyMultiChoiceParamsBase.GetUniqueIDFromItem(BusinessLogic_v3.Classes.DB.IBaseDbObject linkedItem)
        {
            return getUniqueIDFromItem((TLinkedItem)linkedItem);
            
        }

        void ICmsPropertyMultiChoiceParamsBase.SetValuesToItem(BusinessLogic_v3.Classes.DB.IBaseDbObject item, IEnumerable<string> values)
        {
            setValuesToItem((TMainItem)item, values);
            
        }

        #endregion
    }
}
