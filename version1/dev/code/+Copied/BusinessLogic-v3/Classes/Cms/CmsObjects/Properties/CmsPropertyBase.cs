﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using CS.General_v3.Classes.CSS;
using CS.General_v3.Controls.WebControls.Common.General;
using System.Web.UI;

using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Extensions;
using NHibernate;
using BusinessLogic_v3.Classes.Cms.Util;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties
{
    
    public abstract class CmsPropertyBase : ICmsPropertyBase
    {

        /*protected int contextFieldID = 0;
        protected int getNextContextFieldID()
        {
            int current = CS.General_v3.Util.PageUtil.GetContextObject<int?>("cms_contextFieldID").GetValueOrDefault();
            current++;
            CS.General_v3.Util.PageUtil.SetContextObject("cms_contextFieldID", current);
            return current;
        }
        */
        

        public CmsPropertyBase(ICmsItemFactory itemGeneralInfo)
        {
      //      this.contextFieldID = getNextContextFieldID();
            this.IsDefaultSortField = CS.General_v3.Enums.SORT_TYPE.None;
            this.AccessTypeRequired_Edit = new CmsAccessType( CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser);
            this.AccessTypeRequired_View = new CmsAccessType(CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser);
            this.ShowInEdit = true;
            this.IsSearchable = true;
            this.CssClassForEditRow = new CSSManager();
            this.CssClassForListingColumn = new CSSManager();
            this.CssClassForListingEditField= new CSSManager();
            this.CssClassForListingHeader= new CSSManager();
            this.CssClassForSearch= new CSSManager();
            this.DateShowTime = true;

            this.CmsFactory = itemGeneralInfo;
            //this.PropertyInfo = pInfo;
            this.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.SingleLine;
            
            this.CssClassForListingColumn.AddClass( EnumsCms.GetCssClass(EnumsCms.CMS_CSS_CLASSES.ListingColumnCell));
            this.CssClassForEditRow.AddClass(EnumsCms.GetCssClass(EnumsCms.CMS_CSS_CLASSES.FieldEditInPage));
            this.CssClassForSearch.AddClass(EnumsCms.GetCssClass(EnumsCms.CMS_CSS_CLASSES.FieldEditInSearch));
            this.CssClassForListingEditField.AddClass(EnumsCms.GetCssClass(EnumsCms.CMS_CSS_CLASSES.FieldEditInListing));
            this.CssClassForListingHeader.AddClass(EnumsCms.GetCssClass(EnumsCms.CMS_CSS_CLASSES.ListingColumnHeader));
        }
        public string GetPropertyIdentifier()
        {
            //return this.ItemGeneralInfo.Name + "." + this.PropertyInfo.Name;
            return this.CmsFactory.Name + "." + this.Label;
        }


         
        public ICmsItemFactory CmsFactory { get; private set; }
        public string Category {get;set;}

        
        public EnumsCms.CMS_DATA_TYPE DataType { get; protected set; }
        private bool _EditableInListing = false;
        /// <summary>
        /// Whether property is updateable in listing.  
        /// </summary>
        public virtual bool EditableInListing
        {
            get { return _EditableInListing; }
            set 
            { 
                _EditableInListing = value;
                
                if (_EditableInListing)
                    _ShowInListing = true;
            
            }
        }

        public void SetShowInListing(bool editable)
        {
            this.ShowInListing = true;
            if (editable)
                this.EditableInListing = true;
        }

        /// <summary>
        /// Only applicable if data type is 'String'
        /// </summary>
        public CS.General_v3.Enums.STRING_DATA_TYPE StringDataType { get; set; }
        //public int FieldWidth { get; set; }
        public bool DateShowTime { get; set; }
        public string GetFieldCssClassForSectionType(EnumsCms.SECTION_TYPE sectionType)
        {
            switch (sectionType)
            {
                case EnumsCms.SECTION_TYPE.EditPage: return this.CssClassForEditRow.ToString();
                case EnumsCms.SECTION_TYPE.ListingPage: return this.CssClassForListingEditField.ToString();
                case EnumsCms.SECTION_TYPE.SearchInListing: return this.CssClassForSearch.ToString();

            }
            throw new InvalidOperationException("Unrecognised section type");
        }

        public CS.General_v3.Controls.WebControls.Specialized.ListingClasses.ColumnData GetColumnData()
        {
            List<string> cssClasses = new List<string>();
            cssClasses.Add(this.CssClassForListingHeader.ToString());
            cssClasses.Add(this.GetCSSClassNameForListing());
            string cssClass = CS.General_v3.Util.Text.AppendStrings(" ", cssClasses.ToArray());
            var col = new CS.General_v3.Controls.WebControls.Specialized.ListingClasses.ColumnData(this.Label,
                this.GetWidthForListingAsStr(), this.IsDefaultSortField, this.SortableInListing, cssClass);
            return col;
        
            
        }

        public virtual CS.General_v3.Enums.SORT_TYPE IsDefaultSortField {get;set;}
        public string GetWidthForListingAsStr()
        {
            string s = null;
            if (WidthInListing.HasValue)
                s = WidthInListing.Value + "px";
            return s;
        }

        public string GetCSSClassNameForListing()
        {
            return "thListing_" + this.getLabelWithoutSpaces();
            
        }

        public abstract TDataType GetValueForObject<TDataType>(ICmsItemInstance o);

        public abstract OperationResult SetValueForObject(ICmsItemInstance o, object value);

        public virtual OperationResult SetValueForObjectFromFormControl(ICmsItemInstance o, CS.General_v3.Controls.WebControls.Common.General.IMyFormWebControl formControl)
        {
            return SetValueForObject(o, formControl.FormValueObject);
        }
        
        public virtual bool IsRequired { get; set; }
       

        //public bool IsEditable { get; set; }
        public string SubTitle { get; set; }
        
        
        public string HelpMessage { get; set; }

        protected string getLabelWithoutSpaces()
        {
            string s = this.Label;
            s = s.Replace(" ", "");
            return s;
        }

        private string _label;
        public string Label
        {
            get {
                if (_label == null) _label = "";
                return _label; 
            }
            set { _label = value; }
        }


        //public int ListingColumnPriority { get; set; }
        public void SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE accessType)
        {
            this.AccessTypeRequired_Edit.MinimumAccessTypeRequired = accessType;
            this.AccessTypeRequired_View.MinimumAccessTypeRequired = accessType;
            if (!this.AccessTypeRequired_Edit.AccessLevelRequiredToOverrideRoles.HasValue ||
                (int)accessType > (int)this.AccessTypeRequired_Edit.AccessLevelRequiredToOverrideRoles)
            {
                this.AccessTypeRequired_Edit.AccessLevelRequiredToOverrideRoles = accessType;
            }
            if (!this.AccessTypeRequired_View.AccessLevelRequiredToOverrideRoles.HasValue ||
                (int)accessType > (int)this.AccessTypeRequired_View.AccessLevelRequiredToOverrideRoles)
            {
                this.AccessTypeRequired_View.AccessLevelRequiredToOverrideRoles = accessType;
            }
        }

        public CmsAccessType AccessTypeRequired_View { get; private set; }
        public CmsAccessType AccessTypeRequired_Edit { get; private set; }

        public bool CheckIfUserCanView(ICmsUserBase user)
        {
            if (user == null) user = CmsUtil.GetLoggedInUser();
            bool ok = AccessTypeRequired_View.CheckIfUserHasEnoughAccess(user);
            return ok;
        }
        public bool CheckIfUserCanEdit(ICmsUserBase user)
        {
            if (user == null) user = CmsUtil.GetLoggedInUser();
            bool ok = CheckIfUserCanView(user);
            if (ok) ok = AccessTypeRequired_Edit.CheckIfUserHasEnoughAccess(user);
            return ok;
        }

        public virtual bool IsReadOnly { get; protected set; }
        private bool _ShowInListing = false;
        public virtual string DefaultSearchValueStr{get;set;}
        public void SetDefaultSearchValue(object o)
        {
            string s = null;
            if (o != null)
            {
                if (o is bool)
                {
                    s = CS.General_v3.Util.Other.BoolToStr((bool)o);

                }
                else
                {
                    s = o.ToString();
                }
            }
            this.DefaultSearchValueStr = s;
        }

        public virtual bool ShowInListing
        {
            get { 
                
                return _ShowInListing; 
            }
            set 
            {
                _ShowInListing = value;
                
            }
        }

        private bool showInEdit;
        public virtual bool ShowInEdit
        {
            get { return showInEdit; }
            set
            {
                showInEdit = value;

                if (value == false)
                {
                    int k = 5;
                }
            }
        }

        public virtual bool SortableInListing {get;set;}
        
        public CSSManager CssClassForEditRow { get; private set; }
        public CSSManager CssClassForSearch { get; private set; }
        public CSSManager CssClassForListingHeader { get; private set; }
        public CSSManager CssClassForListingColumn { get; private set; }
        public CSSManager CssClassForListingEditField { get; private set; }

        public int? WidthInListing { get; set; }
        public int? WidthInEdit { get; set; }
        public int? HeightInEdit { get; set; }


        public string GetValueForObjectAsString(ICmsItemInstance cmsItem)
        {
            object o = GetValueForObject(cmsItem);
            string s = "";
            if (o != null)
                s = o.ToString();
            return s;
        }

        public object GetValueForObject(ICmsItemInstance cmsItem)
        {
            return GetValueForObject<object>(cmsItem);
            
        }

        public FormFieldListSingleChoiceDropdownData GetFormFieldForBoolNullable()
        {
            FormFieldListSingleChoiceDropdownData field = new FormFieldListSingleChoiceDropdownData(); 
            var listItems = CS.General_v3.Util.Other.GetListItemCollectionForBoolNullable();
            field.ListItems = listItems.Cast<ListItem>().ToList();
            return field;
        }

        public override string ToString()
        {
            return this.Label;
        }
        

        public const string QUERYSTRING_LINKED = "Linked";
        private bool _linkedWithInCms;

        /// <summary>
        /// Signifies that this property is linked with the other object.  For example, if this property info represents Product.Store, and it is marked as TRUE, it means that in the store listing
        /// or edit page, there will be a link to view it's products
        /// </summary>
        public virtual bool LinkedWithInCms
        {
            get { return _linkedWithInCms; }
            set { _linkedWithInCms = value; }
        }

        public string GetLinkedQuerystringValueForItem(ICmsItemInstance item)
        {
            return item.ID.ToString();

            //return item.GetCmsFactoryForItem().Name + "-" + item.ID;
        }

        //public long? GetLinkedQuerystringValuePrimaryKey()
        //{
        //    CS.General_v3.Util.ContractsUtil.Requires(LinkedWithInCms);

        //    return CS.General_v3.Util.PageUtil.RQLongNullable(GetLinkedQuerystringParamName());
        //}

        public string GetLinkedQuerystringParamName()
        {
            CS.General_v3.Util.ContractsUtil.Requires(LinkedWithInCms, "Make sure that you mark the link with 'LinkedWithInCms' for any hierarchy in cms");
            return this.CmsFactory.Name + "-" + this.getLabelWithoutSpaces() + "Link";
            return this.getLabelWithoutSpaces() + "Link";

            return "Linked" + this.getLabelWithoutSpaces();
        }

        public long? GetLinkedIDFromQuerystring()
        {
            CS.General_v3.Util.ContractsUtil.Requires(LinkedWithInCms);
            string s = GetLinkedQuerystringParamName();

            string qsValue = CS.General_v3.Util.PageUtil.RQ(s);
            string itemName;
            long itemValue;
            long? result= null;
            result = Util.CmsUtil.GetLinkItemAndValueFromQsParam(qsValue);
            return result;
            
        }
        
        public abstract object GetFormValueConvertedToPropertyDataType(object formValue, EnumsCms.SECTION_TYPE sectionType);

        /// <summary>
        /// Returns the listing url for the linked items, such that if this property represents Product.Store, this will return the Product Listing, for the Store specified in 'itemLinkedWith'
        /// </summary>
        /// <param name="itemLinkedWith"></param>
        /// <returns></returns>
        public string GetListingUrlForLinkedType(ICmsItemInstance itemLinkedWith)
        {
            var linkedFactory = this.CmsFactory;
            CS.General_v3.Classes.URL.URLClass linkedUrl = linkedFactory.GetListingUrl();
            linkedUrl[this.GetLinkedQuerystringParamName()] = this.GetLinkedQuerystringValueForItem(itemLinkedWith);// itemLinkedWith.ID;
            //linkedUrl[ParentItem.QueryStringParamID] = item.ID;
            return linkedUrl.ToString();
        }

        public virtual bool GetRequiredValueBasedOnSectionType(EnumsCms.SECTION_TYPE sectionType)
        {
            if (sectionType == EnumsCms.SECTION_TYPE.SearchInListing)
                return false;

            else
                return this.IsRequired;
        }
        protected void fillFormFieldBaseDataFromProperty(FormFieldBaseData field,  EnumsCms.SECTION_TYPE sectionType)
        {
            var p = this;
            field.Required =  p.GetRequiredValueBasedOnSectionType(sectionType);
            field.Title = p.Label;
            field.SubTitle = p.SubTitle;
            field.HelpMessage = p.HelpMessage;

        }

        public abstract FormFieldBaseData GetFormFieldBaseDataForProperty(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item,
          bool loadInitialValueFromPostback = false, object value = null);

        public FormFieldsRowItemField AddPropertyToFormFieldForCurrentCmsUser(FormFields formFields, EnumsCms.SECTION_TYPE sectionType,
            ICmsItemInstance item,
            bool loadInitialValueFromPostback = false, object value = null)
        {
            if (item == null && sectionType != EnumsCms.SECTION_TYPE.SearchInListing)
            {
                throw new InvalidOperationException("Item can only be null for searches");
            }
            FormFieldBaseData fieldBase = GetFormFieldBaseDataForProperty(sectionType, item, loadInitialValueFromPostback, value);

            FormFieldsRowItemField row = formFields.Functionality.AddGenericFormFieldItem(fieldBase);
            return row;

        }
        protected string getControlID(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item)
        {
            string id = "cmsProperty_" + CS.General_v3.Util.EnumUtils.StringValueOf<EnumsCms.SECTION_TYPE>(sectionType) + "_" + this.getLabelWithoutSpaces();
            if (item != null)
                id += "_" + item.ID.ToString();
            return id;
        }

        public IMyFormWebControl GetFormControlForCmsProperty(ICmsItemInstance item, object value, EnumsCms.SECTION_TYPE sectionType)
        {
            FormFieldBaseData fieldBase = GetFormFieldBaseDataForProperty(sectionType, item, value: value);
            //fieldBase.ID = "cmsProperty_" + this.getLabelWithoutSpaces() + "_" + item.ID;
            fieldBase.ID = getControlID(sectionType, item);
            return fieldBase.GetField();


            //object valueToSet = value;
            //IMyFormWebControl formCtrl = null;
            //switch (p.DataType)
            //{
            //    case Enums.CMS_DATA_TYPE.Bool:
            //        {
            //            MyCheckBox chk = new MyCheckBox();

            //            formCtrl = chk;
            //            break;
            //        }
            //    case Enums.CMS_DATA_TYPE.BoolNullable:
            //        {
            //            MyDropDownList cmb = new MyDropDownList(); formCtrl = cmb;
            //            cmb.CssManager.AddClass(p.GetFieldCssClassForSectionType(sectionType));
            //            bool? b = (bool?)value;

            //            cmb.Items.AddRange(CS.General_v3.Util.Other.GetListItemCollectionForBoolNullableAsArray());

            //            valueToSet = (b.HasValue ? (b.Value ? "1" : "0") : "");

            //            break;
            //        }
            //    case Enums.CMS_DATA_TYPE.Enumeration:
            //    case Enums.CMS_DATA_TYPE.LinkedObject:
            //        {
            //            MyDropDownList cmb = new MyDropDownList(); formCtrl = cmb;
            //            cmb.CssManager.AddClass(p.GetFieldCssClassForSectionType(sectionType));
            //            if (p.DataType == Enums.CMS_DATA_TYPE.Enumeration)
            //            {
            //                int? i = null;
            //                if (value != null)
            //                    i = (int)value;
            //                valueToSet = i;
            //            }
            //            else
            //            {
            //                ICmsItemInstance cmsItem = (ICmsItemInstance)value;
            //                if (cmsItem != null)
            //                {
            //                    valueToSet = cmsItem.ID;
            //                }
            //            }
            //            ListItemCollection listItems = p.GetListItemCollectionForDataType();
            //            //ListItemCollection listItems = CS.General_v3.Util.EnumUtils.GetListItemCollectionFromEnum(p.PropertyInfo.PropertyType, (!p.IsRequired ? "0" : null));

            //            cmb.Items.AddRange(CS.General_v3.Util.ListUtil.ConvertListItemCollectionToArray(listItems));





            //            break;
            //        }
            //    case Enums.CMS_DATA_TYPE.DateTime:
            //    case Enums.CMS_DATA_TYPE.DateTimeNullable:
            //        {
            //            MyTxtBoxDate txt = new MyTxtBoxDate(); formCtrl = txt;
            //            txt.CssManager.AddClass(p.GetFieldCssClassForSectionType(sectionType));

            //            break;
            //        }
            //    case Enums.CMS_DATA_TYPE.Double:
            //    case Enums.CMS_DATA_TYPE.DoubleNullable:
            //    case Enums.CMS_DATA_TYPE.Integer:
            //    case Enums.CMS_DATA_TYPE.IntegerNullable:
            //    case Enums.CMS_DATA_TYPE.LongNullable:
            //    case Enums.CMS_DATA_TYPE.Long:
            //        {
            //            MyTxtBoxNumeric txt = null;
            //            if (p.DataType == Enums.CMS_DATA_TYPE.Double || p.DataType == Enums.CMS_DATA_TYPE.DoubleNullable)
            //                txt = new MyTxtBoxNumericDouble();
            //            else
            //                txt = new MyTxtBoxNumericInteger();
            //            txt.CssManager.AddClass(p.GetFieldCssClassForSectionType(sectionType));
            //            formCtrl = txt;

            //        }
            //        break;
            //    case Enums.CMS_DATA_TYPE.String:
            //        {

            //            MyTxtBoxText txt = MyTxtBoxText.CreateTxtBoxBasedOnStringDataType(p.StringDataType);
            //            txt.CssManager.AddClass(p.GetFieldCssClassForSectionType(sectionType));
            //            formCtrl = txt;

            //        }
            //        break;
            //        /*
            //    case Enums.CMS_DATA_TYPE.LinkedObject:
            //        {
            //            if ((typeof(ICmsItemInstance)).IsAssignableFrom(p.PropertyInfo.PropertyType))
            //            {
            //                //this is an item link
            //                var factory = CmsInfoBase.Instance.GetFactoryForType(p.PropertyInfo.PropertyType);
            //                if (factory == null)
            //                    throw new InvalidOperationException("Factory not registered for property with type " + p.PropertyInfo.PropertyType.ToString());
            //                else
            //                {
            //                    MyDropDownList cmb = new MyDropDownList(); formCtrl = cmb;
            //                    cmb.CssManager.AddClass(p.GetFieldCssClassForSectionType(sectionType));
            //                    cmb.Items.AddRange(factory.GetAllAsListItemsForCms(p, !p.IsRequired));


            //                }
            //            }
            //            else
            //            {
            //                throw new InvalidOperationException("Unsupported data type");
            //            }
            //            break;
            //        }*/
            //    default:
            //        throw new InvalidOperationException("Unsupported data type");



            //}
            //formCtrl.Required = p.IsRequired;
            //if (valueToSet != null)
            //    formCtrl.Value = valueToSet;
            //formCtrl.Title = p.Label;
            //return formCtrl;
        }

        protected virtual Control getControlEditableForListing(ICmsItemInstance item)
        {
            var ctrl = (Control)getFormControlEditableForListing(item);
            return ctrl;
        }
        protected abstract Control getControlNonEditable(ICmsItemInstance item);
        public virtual Control GetControlNonEditable(ICmsItemInstance item)
        {
            return this.getControlNonEditable(item);
        }
        protected virtual IMyFormWebControl getFormControlEditableForListing(ICmsItemInstance item)
        {
            object value = this.GetValueForObject(item);
            var formCtrl = GetFormControlForCmsProperty(item, value, EnumsCms.SECTION_TYPE.ListingPage);
            return formCtrl;
        }
        
        public Control GetControlForCmsPropertyForListingAndCurrentUser(ICmsItemInstance item)
        {
            var p = this;
            Control ctrl = null;
            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();

            if (p.EditableInListing && (loggedUser != null && (p.CheckIfUserCanEdit(loggedUser))))
            {
                ctrl = getControlEditableForListing(item);

            }
            else
            {
                ctrl = getControlNonEditable(item);

            }

            return ctrl;
        }

        public virtual bool CheckIfCanSearch()
        {
            bool searchable = this.IsSearchable;
            if (searchable)
            {
                if (this.StringDataType == CS.General_v3.Enums.STRING_DATA_TYPE.Html)
                    searchable = false;
            }
            return searchable;
        }

        public virtual bool IsSearchable {get;set;}

        public abstract void AddCriterionToSearchCriteria(ICriteria crit, string searchValue);
        


        public abstract void ApplyOrderByToCriteria(NHibernate.ICriteria crit, CS.General_v3.Enums.SORT_TYPE sortType);
    }
}