﻿using System;
using System.Reflection;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.v2._Base_2;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using System.Web.UI.WebControls;
using CS.General_v3.Classes.MediaItems;
using System.Linq.Expressions;
using System.IO;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Specialized.MediaItems;
using CS.General_v3.Classes.HelperClasses;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TItem">This must be the type of the database item, e.g Game</typeparam>
    public class CmsPropertyMediaItemFrontend<TFrontend>  : CmsPropertyBase
        where TFrontend: BusinessLogic_v3.Modules.v2._Base_2.IBaseFrontendBase
      
        
    {
        public override bool IsSearchable
        {
            get
            {
                return false;
            }
            set
            {
                
            }
        }

        public CmsPropertyMediaItemFrontend(ICmsItemFactory itemGeneralInfo, Expression<Func<TFrontend, MediaItemBaseFile>> selector,
            bool isRequired = false)
            : base(itemGeneralInfo)
        {


            this.IsRequired = isRequired;
            this.PropertyInfo = CS.General_v3.Util.ReflectionUtil<TFrontend>.GetPropertyBySelector(selector);

            initInfoFromProperty();
        }
        public PropertyInfo PropertyInfo { get; set; }
        public OperationResult SetValueForObject(ICmsItemInstance o, Stream fileContents, string filename)
        {
            MediaItemBaseFile mediaItemDB = getMediaItemForObject(o);

            var result = mediaItemDB.UploadFile(fileContents, filename, false);

            if (result.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Warning)
            {
                result.StatusMessages.Clear();
                result.StatusMessages.Add(new OperationResultMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, this.Label + " could not be uploaded as the file contents were unavailable.  Kindly verify that the file still exists, and re-upload"));

            }

            return result;

        }

        public OperationResult SetValueForObject(ICmsItemInstance o, MyFileUpload txtUpload)
        {
            
            return SetValueForObject(o, txtUpload.FileContent, txtUpload.FileName);
        }

        public override OperationResult SetValueForObject(ICmsItemInstance o, object value)
        {
            if (o is MyFileUpload)
            {
                return SetValueForObject(o, (MyFileUpload)value);
            }
            else
            {
                throw new InvalidOperationException("For Media Items,t his must only be called with file upload controls");
            }
            
        }

        private MediaItemBaseFile getMediaItemForObject(ICmsItemInstance o)
        {
            var frontendItem = getFrontendItem(o);

            return (MediaItemBaseFile)this.PropertyInfo.GetValue(frontendItem, null);
        }
        private IBaseFrontendBase _frontendItem = null;
        protected IBaseFrontendBase getFrontendItem(ICmsItemInstance o)
        {
            if (_frontendItem == null)
            {
                var frontendFactory = BusinessLogic_v3.Classes.DB.FrontendFactoriesController.Instance.GetFactoryForObject(o.DbItem);


                _frontendItem = frontendFactory.LoadFromDataItem((IBaseDataBase) o.DbItem);
            }

            return _frontendItem;
        }

        public override TDataType GetValueForObject<TDataType>(ICmsItemInstance o)
        {
            var frontendItem = getFrontendItem(o);
            return (TDataType)this.PropertyInfo.GetValue(frontendItem, null);
        }
        public ICmsItemFactory GetFactoryForPropertyType()
        {
            return CmsSystemBase.Instance.GetFactoryForType(this.PropertyInfo.PropertyType);
        }
        private void initInfoFromProperty()
        {
            this.Label = this.PropertyInfo.Name;
            this.DataType = EnumsCms.CMS_DATA_TYPE.MediaItem;
            
            // this.EditableInListing = true;
            
            


        }

        public delegate ListItemCollection CustomListItemCollectionDelegate(CmsPropertyInfo propertyInfo);
        public event CustomListItemCollectionDelegate CustomListItemCollectionRetriever;

        public override object GetFormValueConvertedToPropertyDataType(object formValue, EnumsCms.SECTION_TYPE sectionType)
        {
            return (Stream)formValue;

        }



        public override FormFieldBaseData GetFormFieldBaseDataForProperty(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {
            FormFieldBaseData field = null;
            if (sectionType != EnumsCms.SECTION_TYPE.SearchInListing)
            {
                //this has no search counterpart

                FormFieldMediaItem mediaItemField = new FormFieldMediaItem("txt" + this.getLabelWithoutSpaces(), (MediaItemBaseFile)value);
                fillFormFieldBaseDataFromProperty(mediaItemField, sectionType);
                field = mediaItemField;
                

            }
            return field;
        }

        protected override System.Web.UI.Control getControlEditableForListing(ICmsItemInstance item)
        {
            return base.getControlEditableForListing(item);
        }

        protected override System.Web.UI.Control getControlNonEditable(ICmsItemInstance item)
        {
            var mediaItem = getMediaItemForObject(item);
            if (mediaItem != null && !mediaItem.IsEmpty())
            {
                string url = mediaItem.GetThumbnailImageUrl();
                string localPath = CS.General_v3.Util.PageUtil.MapPath(url);
                if (File.Exists(localPath))
                {

                    MyImage img = new MyImage();
                    img.CssClass = "cms-media-item-listing";
                    img.ImageUrl = mediaItem.GetThumbnailImageUrl();


                    img.HRef = img.ImageUrl;
                    img.HRefTarget = CS.General_v3.Enums.HREF_TARGET.Blank;
                    img.AlternateText = mediaItem.Identifier;
                    return img;
                }
                else
                {
                    return new Literal();
                }
            }
            else
            {
                Literal lit = new Literal();
                lit.Text = "&nbsp;";
                return lit;
            }
        }


        public override void AddCriterionToSearchCriteria(NHibernate.ICriteria crit, string searchValue)
        {
            //this needs only be implemented for items with search
            throw new InvalidOperationException("Invalid operation");
        
        }

        public override OperationResult SetValueForObjectFromFormControl(ICmsItemInstance o, CS.General_v3.Controls.WebControls.Common.General.IMyFormWebControl formControl)
        {
            MyFileUpload fileUpload = null;
            if (formControl is MediaItemControl)
            {
                fileUpload = ((MediaItemControl)formControl).Functionality.GetFileUploadControl();
            }
            else if (formControl is MyFileUpload)
            {
                fileUpload = (MyFileUpload)formControl;
            }
            else
            {
                throw new InvalidOperationException("Media item form control must be either FileUpload or MediaItemControl");
            }

            OperationResult result = SetValueForObject(o, fileUpload);
            return result;
            
        }

        public override void ApplyOrderByToCriteria(NHibernate.ICriteria crit, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            throw new NotImplementedException("Should never be called");
        }
    }
}