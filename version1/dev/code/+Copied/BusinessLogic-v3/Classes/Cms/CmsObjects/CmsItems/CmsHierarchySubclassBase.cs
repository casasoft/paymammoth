﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules._Common;

using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems
{
    public abstract class CmsHierarchySubclassBase : ICmsHierarchyInfo
    {
        protected CmsItemHierarchyCommonImpl _cmsHierarchyCommonImpl = null;
        protected CmsItemInfo _cmsItem = null;
        public CmsHierarchySubclassBase(CmsItemInfo item)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, null);
            _cmsHierarchyCommonImpl = new CmsItemHierarchyCommonImpl(this, item.GetType());
            _cmsItem = item;
            this.CustomCmsOperations = new List<CmsItemSpecificOperation>();
        }

        #region ICmsHierarchyInfo Members

        public abstract ICmsCollectionInfo SubitemLinkedCollection { get; }

        #endregion

        #region IHierarchy Members

        public long ID
        {
            get { return _cmsItem.DbItem.ID; }
        }

        string IHierarchy.Title
        {
            get
            {
                return this.GetTitleInHierarchy();
            }
        }
        public virtual string Href
        {
            get { return "#"; }
        }

        public CS.General_v3.Enums.HREF_TARGET HrefTarget
        {
            get { return CS.General_v3.Enums.HREF_TARGET.Self; }
        }

        protected abstract IEnumerable<ICmsHierarchyItem> getChildren(bool loadNonVisible = false);
        private IEnumerable<ICmsHierarchyInfo> getChildrenAsInfoEnumerable()
        {
            var list = CS.General_v3.Util.ListUtil.GetListFromEnumerator(getChildren());
            return list.ConvertAll<ICmsHierarchyInfo>(item => item.GetCmsHierarchyInfo());
        }
        protected abstract ICmsHierarchyItem getParent();
        private IEnumerable<CS.General_v3.Controls.WebControls.Specialized.Hierarchy.IHierarchy> _children = null;
        public IEnumerable<CS.General_v3.Controls.WebControls.Specialized.Hierarchy.IHierarchy> GetChildren()
        {
            if (_children == null)
                _children = getChildrenAsInfoEnumerable();
            return _children;
            
        }

        public CS.General_v3.Controls.WebControls.Specialized.Hierarchy.IHierarchy ParentHierarchy
        {
            get { return getParent().GetCmsHierarchyInfo(); }
        }

        public bool Selected
        {
            get { return false; }
        }

        public string GetFullHierarchyName(string delimiter, bool reverseOrder, bool includeRoot)
        {
            return _cmsHierarchyCommonImpl.GetFullHierarchyName(delimiter, reverseOrder, includeRoot);
            
        }

        public bool Visible
        {
            get { return true; }
        }

        #endregion

        #region ITreeItem Members

        public string EditURL
        {
            get { return _cmsHierarchyCommonImpl.EditURL; }
        }

        public string AddNewItemURL
        {
            get { return _cmsHierarchyCommonImpl.AddNewItemURL; }
        }

        public bool AllowUpdate
        {
            get { return true; }
        }

        public bool AllowDelete
        {
            get { return true; }
        }

        public bool AllowAddSubItems
        {
            get { return true; }
        }

        public abstract bool CanRemove(out string errorMsg);

        public OperationResult Remove()
        {
            return this.DeleteFromCms();
        }

        public OperationResult Save()
        {
            return this.SaveFromCms();
        }

        public string Message_DeleteOK
        {
            get { return _cmsHierarchyCommonImpl.Message_DeleteOK; }
        }

        public string Message_ConfirmDelete
        {
            get { return _cmsHierarchyCommonImpl.Message_ConfirmDelete; }
        }

        public bool AddedExtraButtons
        {
            get
            {
                return _cmsHierarchyCommonImpl.AddedExtraButtons;
                
            }
            set
            {
                _cmsHierarchyCommonImpl.AddedExtraButtons = value;
                
            }
        }

        #endregion

        #region ITreeItemBasic Members


        public abstract int Priority {get;set;}

        public string ImageURL
        {
            get { return _cmsHierarchyCommonImpl.ImageUrl; }
        }

        public string LinkURL
        {
            get { return _cmsHierarchyCommonImpl.LinkUrl; }
        }

        IEnumerable<CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> ITreeItemBasic.GetChildTreeItems()
        {
            return (IEnumerable<CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic>)GetChildren();
            
            
        }

        public List<CS.General_v3.Controls.WebControls.Classes.ExtraButton> ExtraButtons
        {
            get { return _cmsHierarchyCommonImpl.ExtraButtons; }
        }

        public void AddExtraButtons()
        {
            _cmsHierarchyCommonImpl.AddExtraButtons();
            
        }

        #endregion

        #region ICmsItemInstance Members

        public virtual CS.General_v3.Classes.HelperClasses.OperationResult DeleteFromCms()
        {
            return _cmsItem.DeleteFromCms();
        }

        public IEnumerable<CmsFieldBase> GetCmsProperties()
        {
            return _cmsItem.GetCmsProperties();
            
        }

        public ICmsItemFactory GetCmsFactoryForItem()
        {
            return _cmsItem.GetCmsFactoryForItem();
        }


        public virtual CS.General_v3.Classes.HelperClasses.OperationResult SaveFromCms(SaveParams saveParams = null)
        {
            return _cmsItem.SaveFromCms(saveParams);
        }

        public bool CheckIfStillTemporary()
        {
            return _cmsItem.CheckIfStillTemporary();
            
        }

        public bool HasTemporaryFields()
        {
            return _cmsItem.HasTemporaryFields();
        }

        public void ErrorOccurredWhileSavingInCms()
        {
            _cmsItem.ErrorOccurredWhileSavingInCms();
            
        }

        public CmsAccessType AccessTypeRequired_ToDelete
        {
            get { return _cmsItem.AccessTypeRequired_ToDelete; }
        }

        public CmsAccessType AccessTypeRequired_ToView
        {
            get { return _cmsItem.AccessTypeRequired_ToView; }
        }

        public CmsAccessType AccessTypeRequired_ToEdit
        {
            get { return _cmsItem.AccessTypeRequired_ToEdit; }
        }

        #endregion

        #region ICmsItemInstance Members

        public List<CmsItemSpecificOperation> CustomCmsOperations { get; private set; }
        public IEnumerable<CmsItemSpecificOperation> GetCustomCmsOperations()
        {
            return this.CustomCmsOperations;
        }

        #endregion

        #region ICmsItemInstance Members


        string ICmsItemInstance.TitleForCms
        {
            get { return this.GetTitleInHierarchy(); }
        }

        #endregion

        #region ICmsHierarchyInfo Members


        public abstract string GetTitleInHierarchy();

        #endregion

        #region ITreeItemBasic Members


        string CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic.Title
        {
            get { return this.GetTitleInHierarchy(); }
        }

        #endregion

        #region ICmsItemInstance Members


        public BusinessLogic_v3.Classes.DB.IBaseDbObject DbItem
        {
            get { return _cmsItem.DbItem; }
        }

        #endregion

        #region ICmsItemInstance Members


        public ColumnPrioritizer GetListingOrderPrioritizer()
        {
            return _cmsItem.ListingOrderPriorities;
            
        }

        public ColumnPrioritizer GetEditOrderPrioritizer()
        {
            return _cmsItem.EditOrderPriorities;
            
        }

        #endregion

        #region IHierarchy Members


        public IEnumerable<IHierarchy> GetParents()
        {
            return CS.General_v3.Util.ListUtil.GetListFromSingleItem(this.ParentHierarchy);
            
        }

        #endregion

        #region ICmsItemInstance Members


        public bool CheckIfUserCanEdit(ICmsUserBase user)
        {
            return this._cmsItem.CheckIfUserCanEdit(user);
            
        }

        public bool CheckIfUserCanDelete(ICmsUserBase user)
        {
            return this._cmsItem.CheckIfUserCanDelete(user);
        }

        public bool CheckIfUserCanView(ICmsUserBase user)
        {
            return this._cmsItem.CheckIfUserCanView(user);
        }

        #endregion

        #region ICmsItemInstance Members


        public CmsAccessType GetAccessRequiredToDelete()
        {

            return this._cmsItem.GetAccessRequiredToDelete();
        }

        public CmsAccessType GetAccessRequiredToEdit()
        {

            return this._cmsItem.GetAccessRequiredToEdit();
        }

        public CmsAccessType GetAccessRequiredToView()
        {

            return this._cmsItem.GetAccessRequiredToView();
        }

        #endregion



        #region ICmsItemInstance Members


        public void CheckCustomAccess(OperationResult result)
        {
            this._cmsItem.CheckCustomAccess(result);
            
        }

        #endregion
    }
}
