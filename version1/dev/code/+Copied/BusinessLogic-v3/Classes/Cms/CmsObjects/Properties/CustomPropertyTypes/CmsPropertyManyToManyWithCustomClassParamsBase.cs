﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Reflection;
using BusinessLogic_v3.Modules.ArticleModule;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes
{
    public abstract class CmsPropertyManyToManyWithCustomClassParamsBase<TMainItem, TManyToManyLink, TLinkedItem> : ICmsPropertyManyToManyWithCustomClassParams
        where TMainItem : BusinessLogic_v3.Classes.DB.IBaseDbObject
        where TManyToManyLink : BusinessLogic_v3.Classes.DB.IBaseDbObject
        where TLinkedItem: BusinessLogic_v3.Classes.DB.IBaseDbObject

    {
        /// <summary>
        /// This should be a method that returns the list of possible list items for selection, for the [item]
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected abstract IEnumerable<ListItem> getListItemsFromItem(TMainItem item);
        /// <summary>
        /// This should return the Unique ID for the linked item, for example for a 'ContentPageParentChildLink', for a many-to-many from a child to a parent, it would be link.ParentID (string)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected string getUniqueIDFromItem(TManyToManyLink item)
        {
            return getLinkedItemFromLink(item).ID.ToString();
        }

        /// <summary>
        /// Should set the values to the main item, from a list of strings
        /// </summary>
        /// <param name="item"></param>
        /// <param name="values"></param>
        //protected abstract void setValuesToItem(TMainItem item, IEnumerable<string> values);

        protected void setValuesToItem(TMainItem item, IEnumerable<string> values)
        {

            //    item.ParentArticleLinks

            IBaseDbFactory linkedItemFactory = BusinessLogic_v3.Classes.DB.DataFactoriesController.Instance.GetFactoryForType(typeof(TLinkedItem));
            List<TLinkedItem> linkedItems = values.ToList().ConvertAll<TLinkedItem>(s => (TLinkedItem)linkedItemFactory.GetByPrimaryKey(long.Parse(s)));

            ICollectionManager<TManyToManyLink> manyToManyLinksCollection = getManyToManyCollectionForItem(item);

            var result = CS.General_v3.Util.ListUtil.CompareTwoListsForRequiredAndNonRequired(
                linkedItems,
                manyToManyLinksCollection,
                (item1, item2) =>
                {
                    var linkedItem = getLinkedItemFromLink(item2);
                    return linkedItem != null &&  item1.ID == linkedItem.ID;
                });

            if (result.ItemsToBeAdded.Count > 0)
            {
                var manyToManyLinkFactory = BusinessLogic_v3.Classes.DB.DataFactoriesController.Instance.GetFactoryForType(typeof (TManyToManyLink));
                var pInfoParent = GetPropertyToRetrieveMainItemFromLink();
                var pInfoChild = GetPropertyToRetrieveLinkedItemFromLink();
                foreach (var itemToAdd in result.ItemsToBeAdded)
                {
                    TManyToManyLink newLink = (TManyToManyLink) manyToManyLinkFactory.CreateNewItem();
                    pInfoParent.SetValue(newLink, item,null);
                    pInfoChild.SetValue(newLink, itemToAdd, null);
                    
                    manyToManyLinksCollection.Add(newLink);
                }
            }
            foreach (var itemToRemove in result.ItemsToBeRemoved)
            {
                var delParams = new CS.General_v3.Classes.DbObjects.Parameters.DeleteParams();
                delParams.DeletePermanently = true;
                itemToRemove.Delete(delParams);
                manyToManyLinksCollection.Remove(itemToRemove);
            }


        }

        protected TLinkedItem getLinkedItemFromLink(TManyToManyLink link)
        {
            var pInfo = this.GetPropertyToRetrieveLinkedItemFromLink();
            return (TLinkedItem) pInfo.GetValue(link, null);
        }



        protected ICollectionManager<TManyToManyLink> getManyToManyCollectionForItem(TMainItem item)
        {
            return (ICollectionManager<TManyToManyLink>) this.Property.GetValue(item,null);
        }

        /// <summary>
        /// Returns the property that matches the many-to-many collection link, e.g cp => cp.ParentChildLinks;
        /// </summary>
        /// <returns></returns>
        protected abstract System.Linq.Expressions.Expression<Func<TMainItem, CS.General_v3.Classes.DbObjects.Collections.ICollectionManager<TManyToManyLink>>> getPropertySelector();

        /// <summary>
        /// Returns the property that retrieves the linked item, from the many-to-many collection link
        /// </summary>
        /// <returns></returns>
        protected abstract System.Linq.Expressions.Expression<Func<TManyToManyLink, TLinkedItem>> getChildPropertyFromLinkedItemSelector();
        /// <summary>
        /// Returns the property that retrieves the parent item, from the many-to-many collection link, e.g Parent
        /// </summary>
        /// <returns></returns>
        protected abstract System.Linq.Expressions.Expression<Func<TManyToManyLink, TMainItem>> getParentPropertyFromLinkedItemSelector();




        private PropertyInfo _property;
        public PropertyInfo Property
        {
            get 
            {
                if (_property == null)
                    _property = CS.General_v3.Util.ReflectionUtil<TMainItem>.GetPropertyBySelector(getPropertySelector());
                return _property; 
            }
            private set 
            {
                _property = value; 
            }
        }
        private PropertyInfo _propertyLinkedItemFromLink = null;
        public PropertyInfo GetPropertyToRetrieveLinkedItemFromLink()
        {
            if (_propertyLinkedItemFromLink == null)
                _propertyLinkedItemFromLink = CS.General_v3.Util.ReflectionUtil<TManyToManyLink>.GetPropertyBySelector(getChildPropertyFromLinkedItemSelector());
            return _propertyLinkedItemFromLink;
            
        }
        private PropertyInfo _propertyMainItemFromLink = null;
        public PropertyInfo GetPropertyToRetrieveMainItemFromLink()
        {
            if (_propertyMainItemFromLink == null)
                _propertyMainItemFromLink = CS.General_v3.Util.ReflectionUtil<TManyToManyLink>.GetPropertyBySelector(getParentPropertyFromLinkedItemSelector());
            return _propertyMainItemFromLink;

        }

        public CmsPropertyManyToManyWithCustomClassParamsBase()
        {
            

        }



        #region ICmsPropertyMultiChoiceParamsBase Members

        IEnumerable<ListItem> ICmsPropertyManyToManyWithCustomClassParams.GetListItemsFromItem(BusinessLogic_v3.Classes.DB.IBaseDbObject item)
        {
            return getListItemsFromItem((TMainItem)item);
            
        }

        string ICmsPropertyManyToManyWithCustomClassParams.GetUniqueIDFromItem(BusinessLogic_v3.Classes.DB.IBaseDbObject linkedItem)
        {
            return getUniqueIDFromItem((TManyToManyLink)linkedItem);
            
        }

        void ICmsPropertyManyToManyWithCustomClassParams.SetValuesToItem(BusinessLogic_v3.Classes.DB.IBaseDbObject item, IEnumerable<string> values)
        {
            setValuesToItem((TMainItem)item, values);
            
        }

        #endregion

        #region ICmsPropertyMultiChoiceParamsBase Members



        /// <summary>
        /// Returns a custom criterion for searching this many-to-many link. For example, a search for the link between Product -> Category, would return a custom search
        /// that loads subcategories as well.  If returned as null, default search is taken, i.e one-to-one mapping.
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        public virtual NHibernate.Criterion.ICriterion GetCustomCriterionForSearch(NHibernate.ICriteria criteria, List<long> ids)
        {
            return null;
        }



        #endregion
    }
}
