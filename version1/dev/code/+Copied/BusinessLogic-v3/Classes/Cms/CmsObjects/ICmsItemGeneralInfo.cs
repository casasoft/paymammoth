using System.Collections.Generic;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Modules.CmsUserModule;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public interface ICmsItemGeneralInfo
    {
        string TitlePlural { get; set; }
        string TitleSingular { get; set; }
        List<CmsGeneralOperation> CustomCmsOperations { get; }
        int CmsMainMenuPriority { get; set; }
        EnumsCms.IMAGE_ICON CmsImageIcon { get; set; }
        bool GetWhetherToShowInCmsMainMenu();
        EnumsCms.SECTION_RENDER_TYPE RenderType { get; set; }
        bool GetWhetherToShowAddNewButton();

        bool CheckIfUserCanView(ICmsUserBase user);
        bool CheckIfUserCanAdd(ICmsUserBase user);
        bool CheckIfUserCanDelete(ICmsUserBase user);

        CmsAccessType GetAccessRequiredToView();
        CmsAccessType GetAccessRequiredToDelete();
        CmsAccessType GetAccessRequiredToAdd();


    }
}