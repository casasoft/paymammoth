﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;

using NHibernate;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.CmsDbOperations
{
    public class ItemDeleter
    {
       
        public ItemDeleter(ICmsItemInstance item, EnumsCms.SECTION_TYPE sectionType)
        {
            this.Item = item;
            this.SectionType = sectionType;
            
        }
        public EnumsCms.SECTION_TYPE SectionType { get; set; }
        public ICmsItemInstance Item { get; set; }


        private void writeToAuditLog()
        {

            string msg = "";


            msg = Item.DbItem.GetType().Name + " ";
            switch (_result.Status)
            {
                case CS.General_v3.Enums.STATUS_MSG_TYPE.Success: msg += " deleted successfully";break;
                case CS.General_v3.Enums.STATUS_MSG_TYPE.Error: msg += " could not be deleted";break;
                case CS.General_v3.Enums.STATUS_MSG_TYPE.Warning: msg += " deleted with warning";break;
            }
            StringBuilder furtherInfo = new StringBuilder();
            furtherInfo.AppendLine("Status: " + CS.General_v3.Util.EnumUtils.StringValueOf(_result.Status));
            if (_result.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
            {
                furtherInfo.AppendLine();
                furtherInfo.AppendLine(_result.GetMessageAsString());
            }
            Util.CmsUtil.AddAuditLog(Enums.CMS_AUDIT_MESSAGE_TYPE.Delete,
                                         Item.DbItem.GetType().Name, Item.DbItem.ID,
                                         msg, furtherInfo.ToString());
            


        }

        private OperationResult _result = null;
        public OperationResult DeleteDbItem()
        {
            _result = new OperationResult();

            ICmsItemInstance item = this.Item;
            var deleteResult = item.DeleteFromCms();
            _result.AddFromOperationResult(deleteResult);


            writeToAuditLog();
            return _result;
        }

    }
}
