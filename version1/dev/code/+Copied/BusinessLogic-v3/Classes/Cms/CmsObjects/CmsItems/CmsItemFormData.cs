﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Controls.WebControls.Common.General;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems
{
    public class CmsItemFormData
    {
        public ICmsItemInstance CMSItem { get; set; }
        public List<CmsPropertyFormData> Properties { get; private set; }
        public CmsItemFormData(ICmsItemInstance cmsItem)
        {
            this.CMSItem = cmsItem;
            this.Properties = new List<CmsPropertyFormData>();
        }
    }
}