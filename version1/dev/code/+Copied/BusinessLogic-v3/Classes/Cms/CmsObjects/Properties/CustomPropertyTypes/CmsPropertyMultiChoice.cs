﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Classes.HelperClasses;
using NHibernate.Criterion;
using CS.General_v3.Controls.WebControls.Common;
using BusinessLogic_v3.Classes.Cms.Util;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes
{
    public abstract class CmsPropertyMultiChoice : CmsFieldBase
        
       
    {



      //  public ICmsItemFactory LinkedWithFactory { get; private set; }
        public PropertyInfo PropertyInfo { get; set; }

        public EnumsCms.MULTICHOICE_DISPLAY_TYPE DisplayType { get; set; }

        public CmsPropertyMultiChoice(ICmsItemInstance cmsItem, PropertyInfo propertyInfo, EnumsCms.MULTICHOICE_DISPLAY_TYPE displayType)
            : base(cmsItem)

        {
            this.DisplayType = displayType;
            //CS.General_v3.Util.ContractsUtil.RequiresNotNullable(propertyInfo, "Property info must be filled in");
          //  this.LinkedWithFactory = factoryLinkedWith;

            this.IsSearchable = false;
            this.TotalColumns = 3;
            this.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this.ItemsToShowInList = 10;
            setProperty(propertyInfo);
            
        }
        protected void setProperty(PropertyInfo p)
        {
            this.PropertyInfo = p;
            if (this.PropertyInfo != null)
            {
                this.Label = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(PropertyInfo.Name);
            }
        }

        /// <summary>
        /// Only applies for checkboxes
        /// </summary>
        public int TotalColumns { get; set; }
        /// <summary>
        /// Only applies for checkboxes
        /// </summary>
        public RepeatDirection RepeatDirection { get; set; }

        public override bool EditableInListing
        {
            get
            {
                return false;
            }
            set
            {

            }
        }
        public override bool IsSearchable
        {
            get
            {
                return base.IsSearchable;
            }
            set
            {
                base.IsSearchable = value;
            }
        }
        /*
        public override bool ShowInListing
        {
            get
            {
                return false;
            }
            set
            {
                base.ShowInListing = false;
            }
        }*/

        protected abstract IEnumerable<string> getSelectedValuesForItem(ICmsItemInstance item);

        public new IEnumerable<string> GetValueForObject(ICmsItemInstance item)
        {
            return getSelectedValuesForItem(item);
            
        }

        public override TDataType GetValueForObject<TDataType>(ICmsItemInstance o)
        {
            return (TDataType)this.PropertyInfo.GetValue(o.DbItem, null);
        }


        protected abstract void setValueForCmsItem(ICmsItemInstance item, IEnumerable<string> values);
        

        private OperationResult setValueFromString(ICmsItemInstance item, string sValues)
        {
            OperationResult result = new OperationResult();

          //  var currentList = GetValueForObject(item);
          //  currentList.Clear();
            sValues = sValues?? "";
            string[] sTokens = sValues.Split(new string[] { ","},  StringSplitOptions.RemoveEmptyEntries);
            setValueForCmsItem(item, sTokens);
            return result;

        }
        public override OperationResult SetValueForObjectFromFormControl(ICmsItemInstance o, IMyFormWebControl formControl)
        {
            string s = formControl.GetFormValueAsStr();
            return setValueFromString(o, s);

            
        }
        public override OperationResult SetValueForObject(ICmsItemInstance o, object value)
        {
            if (value is string)
                return setValueFromString(o, (string)value);
            else
            {
                throw new NotImplementedException("This can only be called with string values");
            }
            
        }


        public int ItemsToShowInList { get; set; }

        public override object GetFormValueConvertedToPropertyDataType(object formValue, EnumsCms.SECTION_TYPE sectionType)
        {
            

            throw new NotImplementedException();
        }
        protected abstract IEnumerable<ListItem> getListItemChoices(ICmsItemInstance item);
       

        /*protected override Control getControlNonEditable(ICmsItemInstance item)
        {
            var selectedList = GetValueForObject(item);
            StringBuilder sb = new StringBuilder();
            foreach (var selItem in selectedList)
            {
                if (sb.Length > 0) sb.Append(", ");

                var cmsItem = LinkedWithFactory.GetCmsItemFromDBObject((BaseDbObject) selItem);
                sb.Append(cmsItem.TitleForCms);
            }
            LiteralControl lit = new LiteralControl();
            lit.Text = sb.ToString();
            return lit;
        }*/

        
        public override void AddCriterionToSearchCriteria(NHibernate.ICriteria criteria, string searchValue)
        {
            NHibernate.Criterion.ICriterion crit = null;

            List<long> ids = new List<long>();
            List<string> sIDs = CS.General_v3.Util.Text.Split(searchValue, ",", "|");
            foreach (var sID in sIDs)
            {
                long id;
                if (long.TryParse(sID, out id))
                {
                    ids.Add(id);
                }
            }
            if (ids.Count > 0)
            {
                crit = NHibernate.Criterion.Expression.Eq(this.PropertyInfo.Name, ids[0]);
                //crit = Restrictions.InG<long>(this.PropertyInfo.Name, ids);
            }
            if (crit != null)
                criteria.Add(crit);
            
            

        }
        public override void ApplyOrderByToCriteria(NHibernate.ICriteria crit, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            throw new NotImplementedException("Should never be called");
        }

        private FormFieldBaseData getFormFieldForSearch(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {
            FormFieldListSingleChoiceDropdownData field = new FormFieldListSingleChoiceDropdownData();
            var listItems = this.getListItemChoices(null).ToList();
            listItems.Insert(0, new ListItem("", "")); // add blank option on top

            field.ListItems.AddRange(listItems.ToArray());

            return field;
        }
        private FormFieldBaseData getFormFieldAsCheckboxList(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {


            FormFieldMulipleChoiceDataAsChkBoxes field = new FormFieldMulipleChoiceDataAsChkBoxes();
            field.RepeatDirection = this.RepeatDirection;
            field.TotalColumns = this.TotalColumns;
            //field.HelpMessage = "To select multiple items, hold CTRL and select items.";
            var chkBoxList = field.GetField();

            if (item != null)
            {
                
                var allListItems = getListItemChoicesAndMarkSelected(item);


                chkBoxList.Items.AddRange(allListItems.ToArray());
            }
            return field;
        }

        private IEnumerable<ListItem> getListItemChoicesAndMarkSelected(ICmsItemInstance item)
        {
            var currentSelectedList = GetValueForObject(item);

            var allListItems = getListItemChoices(item);
            //  listBox.SelectedItem = null;
            foreach (ListItem listItem in allListItems)
            {
                listItem.Attributes.Remove("selected");
                listItem.Selected = false;
                foreach (var selItem in currentSelectedList)
                {
                    if (string.Compare(listItem.Value, selItem, true) == 0)
                    {
                        listItem.Attributes["selected"] = "selected";
                        listItem.Selected = true;
                        break;
                    }

                }
            }
            return allListItems;
        }

        private FormFieldBaseData getFormFieldAsListbox(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {

            
            FormFieldMulipleChoiceListBox field = new FormFieldMulipleChoiceListBox();
            field.ID = "cmsProperty_" + this.PropertyInfo.Name;
            field.Size = this.ItemsToShowInList;
            field.Width = this.WidthInEdit.GetValueOrDefault(600);
            field.Height = this.HeightInEdit.GetValueOrDefault(400); ;
            field.JQueryMultiSelect = true;

            field.FormFieldCssClass.AddClass("cms-many-to-many");
            field.Title = this.PropertyInfo.Name;
            field.HelpMessage = "To select multiple items, hold CTRL and select items.";
            
           // var listBox = field.GetField();

            if (item != null)
            {
               // listBox.SelectedIndex = -1;

                
                var allListItems = getListItemChoicesAndMarkSelected(item);
                //field.ListItems = new ListItemCollection();
                field.ListItems.Clear();
                field.ListItems.AddRange(allListItems.ToArray());

                //listBox.Items.AddRange(allListItems.ToArray());
            }
            return field;


        }

       
        public override FormFieldBaseData GetFormFieldBaseDataForProperty(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item, bool loadInitialValueFromPostback = false, object value = null)
        {
            FormFieldBaseData field = null;
            if (sectionType == EnumsCms.SECTION_TYPE.EditPage)
            {
                switch (this.DisplayType)
                {
                    case EnumsCms.MULTICHOICE_DISPLAY_TYPE.CheckboxList:
                        {
                            field = getFormFieldAsCheckboxList(sectionType, item, loadInitialValueFromPostback, value);
                            break;
                        }
                    case EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox:
                        {
                            field = getFormFieldAsListbox(sectionType, item, loadInitialValueFromPostback, value);
                            break;
                        }
                    default:
                        throw new InvalidOperationException("Invalid display type");

                }
            }
            else if (sectionType == EnumsCms.SECTION_TYPE.SearchInListing)
            {
                field = getFormFieldForSearch(sectionType, item, loadInitialValueFromPostback, value);
            }
            field.ID = "cmsProperty_" + this.PropertyInfo.Name;
            field.FormFieldCssClass.AddClass("cms-many-to-many");
            field.Title = this.Label;
            
            return field;


        }

    }
}