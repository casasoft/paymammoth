﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Cms.CategoryModule;
using BusinessLogic_v3.Cms.ProductModule;
using CS.General_v3.Classes.Routing;
using CS.General_v3.Classes.URL;
using BusinessLogic_v3.Classes.Cms.Util;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public class CmsRoutesMapper
    {
        public static CmsRoutesMapper Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<CmsRoutesMapper>(null); }
        }
        protected CmsRoutesMapper()
        {

        }
        public const string QUERYSTRING_ID = "ID";
        public string GetLoginPage()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl("CmsLoginPage",null);
        }


        public const string Route_Settings_SendTestEmail = "Settings-SendTestEmail";
        public const string Route_Products_Import = "Products-Import";
        public const string Route_LogFiles_Listing = "LogFiles-Listing";
        public const string Route_LogFiles_ViewLogEntries = "LogFiles-ViewLogEntries";
        public const string Route_LogFiles_ViewLogEntryDetailed = "LogFiles-ViewLogEntryDetailed";
        public const string Route_Category_MoveProducts = "Category-MoveProducts";
        public const string Route_CmsGeneral_Listing = "CmsListingPage";
        public const string Route_CmsGeneral_Edit = "CmsEditPage";
        public const string Route_CmsGeneral_Login = "CmsLoginPage";
        public const string Route_CmsGeneral_Welcome = "CmsWelcomePage";

        protected virtual void mapRoutes()
        {

            string baseCommonFolder = CmsUrls.BASE_CMS_COMMON_FOLDER;

            CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_CmsGeneral_Login, CmsUrls.CMS_FOLDER + "login", baseCommonFolder + "login.aspx", null, null, throwErrorIfPhysicalFileDoesNotExist: false);
            CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_CmsGeneral_Welcome, CmsUrls.CMS_FOLDER + "welcome.aspx", baseCommonFolder + "welcome.aspx", null, null, throwErrorIfPhysicalFileDoesNotExist: false);

            {//settings


                CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_Settings_SendTestEmail, CategoryBaseCmsFactory.Instance.GetCmsFolderUrl(includeBaseUrl: false) + "SendTestEmail",
                    baseCommonFolder + "settings/sendTestEmail.aspx", null, null, throwErrorIfPhysicalFileDoesNotExist: true);
            }
            {//itemGroups

                if (ProductBaseCmsFactory.UsedInProject)
                {
                    CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_Products_Import, ProductBaseCmsFactory.Instance.GetCmsFolderUrl(includeBaseUrl: false) + "Import",
                                                                baseCommonFolder + "Products/import.aspx", null, null, throwErrorIfPhysicalFileDoesNotExist: true);
                }
            }
            {//logFiles

                string folder = CmsSystemBase.Instance.GetCmsRoot() + "LogFiles/";

                CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_LogFiles_Listing, folder + "",
                    baseCommonFolder + "LogFiles/logBrowser.aspx", null, null, throwErrorIfPhysicalFileDoesNotExist: true);
                CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_LogFiles_ViewLogEntries, folder + "ViewLogEntries",
                    baseCommonFolder + "LogFiles/viewLogEntries.aspx", null, null, throwErrorIfPhysicalFileDoesNotExist: true);
                CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_LogFiles_ViewLogEntryDetailed, folder + "ViewLog-Detailed",
                    baseCommonFolder + "LogFiles/viewLogItem.aspx", null, null, throwErrorIfPhysicalFileDoesNotExist: true);
            }
            {//categories

                CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_Category_MoveProducts, CategoryBaseCmsFactory.Instance.GetCmsFolderUrl(includeBaseUrl: false) + "MoveProducts",
                    baseCommonFolder + "category/moveProducts.aspx", null, null, throwErrorIfPhysicalFileDoesNotExist: true);
            }

            CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_CmsGeneral_Listing, CmsUrls.CMS_FOLDER + "{" + CmsConstants.ROUTING_PARAM_TITLE + "}/listing", baseCommonFolder + "listing.aspx", null, null, throwErrorIfPhysicalFileDoesNotExist: false);
            CS.General_v3.Util.RoutingUtil.MapPageRoute(Route_CmsGeneral_Edit, CmsUrls.CMS_FOLDER + "{" + CmsConstants.ROUTING_PARAM_TITLE + "}/edit",
                baseCommonFolder + "edit.aspx", null, new MyRouteValueDictionary(CmsConstants.ROUTING_PARAM_ID, "[0-9]+"), throwErrorIfPhysicalFileDoesNotExist: true);


        }
        private bool _mappedRoutes = false;
        private readonly object _padlock = new object();
        public void MapRoutes()
        {
            if (!_mappedRoutes)
            {
                lock (_padlock)
                {
                    
                    if (!_mappedRoutes)
                    {
                        _mappedRoutes = true;
                        mapRoutes();
                    }
                    
                }
            }
        }

        public string GetCategory_MoveProductsPage(long CategoryID)
        {
            string url = CS.General_v3.Util.RoutingUtil.GetRouteUrl("Category-MoveProducts", null);
            URLClass urlClass = new URLClass(url);
            urlClass[CategoryBaseCmsFactory.Instance.QueryStringParamID] = CategoryID;
            return urlClass.ToString();


        }
        public string GetLogFiles_ListingPage()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(Route_LogFiles_Listing, null);
        }
        public string GetLogFiles_ViewPage()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(Route_LogFiles_ViewLogEntries, null);
        }
        public string GetLogFiles_ViewLogItem_Detailed(int id)
        {
            CS.General_v3.Classes.URL.URLClass url = new URLClass(CS.General_v3.Util.RoutingUtil.GetRouteUrl(Route_LogFiles_ViewLogEntryDetailed, null));
            url[QUERYSTRING_ID] = id;
            return url.ToString();

        }
        public string GetSettings_SendTestEmailPage()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(Route_Settings_SendTestEmail, null);
        }

        public string GetProducts_Import()
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(Route_Products_Import, null);
        }
        public string GetListingPage(string title)
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(Route_CmsGeneral_Listing,
                new MyRouteValueDictionary(CmsConstants.ROUTING_PARAM_TITLE, title));
        }

        public string GetEditPage(string title)
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(Route_CmsGeneral_Edit,
                new MyRouteValueDictionary(CmsConstants.ROUTING_PARAM_TITLE, title));
        }

    }
}
