﻿using System.Reflection;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using CS.General_v3.Classes.HelperClasses;
using System;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties
{
    public class CmsLinkedPropertyInfo  : CmsPropertyInfo
    {


        public CmsLinkedPropertyInfo(ICmsItemInstance cmsItem, PropertyInfo pInfo, PropertyInfo collectionProperty)
            : base(cmsItem, pInfo)
        {
            System.Diagnostics.Contracts.Contract.Requires(collectionProperty != null);
            this.LinkedCollectionProperty = collectionProperty;

        }


        /// <summary>
        /// Returns the factory for the 'LinkedItem' in CMS.  In order for this to return a value, this property must be marked as 'LinkedWithInCms'.  For example, if this property represenets 'Product.Store',
        /// the returned factory will be the factory for type 'Store'.
        /// </summary>
        /// <returns></returns>
        public ICmsItemFactory GetLinkedCmsFactory()
        {
            // if (LinkedWithInCms)
            return CmsSystemBase.Instance.GetFactoryForType(this.PropertyInfo.PropertyType);
            // else
            //    return null;
        }
        public override void AddCriterionToSearchCriteria(NHibernate.ICriteria criteria, string searchValue)
        {
            
            object nhValue = null;
            var linkedFactory = this.GetLinkedCmsFactory();
            if (this.CustomSearchField == null)
            {

                long? linkedPKey = CS.General_v3.Util.Other.ConvertStringToBasicDataType<long?>(searchValue);
                if (linkedPKey.GetValueOrDefault() > 0)
                {
                    var linkedItem = linkedFactory.GetCmsItemWithId(linkedPKey.Value);
                    if (linkedItem != null)
                        nhValue = linkedItem.DbItem;
                }
                addCriteriaForSearch(criteria, this.PropertyInfo, nhValue);
            }
            else
            {
                string s = searchValue;
                string path = this.PropertyInfo.Name + "." + this.CustomSearchField.Name;
                var linkCrit = CS.General_v3.Util.nHibernateUtil.GetAssociationLinkCriteriaByPath(criteria, this.PropertyInfo);
                //linkCrit.Add(Restrictions.Eq("Title", s));
                addCriteriaForSearch(linkCrit, this.CustomSearchField, s);

            }
            
        }

        public PropertyInfo LinkedCollectionProperty { get; private set; }

        /// <summary>
        /// Retrieves the collection from the 'other side'
        /// </summary>
        /// <returns></returns>
        public CmsCollectionInfo GetLinkedObjectCollectionFromOtherSide()
        {
            CS.General_v3.Util.ContractsUtil.Requires(this.DataType == EnumsCms.CMS_DATA_TYPE.LinkedObject, "To call this method, property must be a linked object");
            if (this.LinkedCollectionProperty != null)
            {
                var otherSideFactory = GetFactoryForLinkedObject();
                if (otherSideFactory != null)
                {
                    var otherSideCmsItem = otherSideFactory.GetEmptyCmsItemInfo();
                    foreach (var otherSideP in otherSideCmsItem.GetCmsProperties())
                    {
                        if (otherSideP is CmsCollectionInfo)
                        {
                            CmsCollectionInfo collInfo = (CmsCollectionInfo) otherSideP;
                            if (collInfo.PropertyCollection.Name == this.LinkedCollectionProperty.Name)
                                //if (this.PropertyInfo.PropertyType.IsAssignableFrom(collInfo.PropertyOnOtherSide.PropertyType))
                            {
                                return collInfo;
                            }
                        }
                    }
                }
                else
                {
                    //This means that the factory is not 'used in project', e.g a Linked Affiliate for the Member, when affiliates are not used.
                }
            }
            return null;

        }
    }
}