using System.Reflection;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Classes.DB;
using CS.General_v3.Classes.DbObjects.Collections;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Collections
{
    public interface ICmsCollectionInfo
    {
        bool ShowLinkInCms { get; set; }
        PropertyInfo PropertyCollection { get;  }
        PropertyInfo PropertyOnOtherSide { get;  }
        ICollectionManager GetCollection(ICmsItemInstance o);
        
        ICollectionManager GetCollection(IBaseDbObject o);
        /// <summary>
        /// This will only work if the type is registered as a factory
        /// </summary>
        /// <returns></returns>
        ICmsItemFactory GetFactoryForLinkedObject();
        string GetLinkedQuerystringValueForItem(ICmsItemInstance item);
        /// <summary>
        /// Returns the listing url for the linked items, such that if this property represents Product.Store, this will return the Product Listing, for the Store specified in 'itemLinkedWith'
        /// </summary>
        /// <param name="itemLinkedWith"></param>
        /// <returns></returns>
        string GetListingUrlForLinkedType(ICmsItemInstance itemLinkedWith);
        bool PreFetchInListing { get; set; }
        long? GetLinkedIDFromQuerystring();
        string GetLinkedQuerystringParamName();
    }
}