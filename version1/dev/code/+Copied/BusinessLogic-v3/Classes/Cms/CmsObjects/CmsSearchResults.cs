﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public class CmsSearchResults
    {
        public IEnumerable<ICmsItemInstance> SearchItems { get; set; }
        public int TotalCount { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }


    }
}
