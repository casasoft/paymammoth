﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Classes.CSS;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using NHibernate;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties
{
    public interface ICmsFieldBase
    {
        string GetPropertyIdentifier();
        ICmsItemFactory CmsFactory { get; }
        string Category { get; set; }
        EnumsCms.CMS_DATA_TYPE DataType { get; }

        /// <summary>
        /// Whether property is updateable in listing.  
        /// </summary>
        bool EditableInListing { get; set; }

        /// <summary>
        /// Only applicable if data type is 'String'
        /// </summary>
        CS.General_v3.Enums.STRING_DATA_TYPE StringDataType { get; set; }

        bool DateShowTime { get; set; }
        CS.General_v3.Enums.SORT_TYPE IsDefaultSortField { get; set; }
        bool IsRequired { get; set; }
        string SubTitle { get; set; }
        string HelpMessage { get; set; }
        string Label { get; set; }
        bool IsReadOnly { get; }
        string DefaultSearchValueStr { get; set; }
        bool ShowInListing { get; set; }
        bool ShowInEdit { get; set; }
        bool SortableInListing { get; set; }
        CSSManager CssClassForEditRow { get; }
        CSSManager CssClassForSearch { get; }
        CSSManager CssClassForListingHeader { get; }
        CSSManager CssClassForListingColumn { get; }
        CSSManager CssClassForListingEditField { get; }
        int? WidthInListing { get; set; }
        int? WidthInEdit { get; set; }
        int? HeightInEdit { get; set; }

        
        bool IsSearchable { get; set; }

        void SetShowInListing(bool editable);
        string GetFieldCssClassForSectionType(EnumsCms.SECTION_TYPE sectionType);
        CS.General_v3.Controls.WebControls.Specialized.ListingClasses.ColumnData GetColumnData();
        string GetWidthForListingAsStr();
        string GetCSSClassNameForListing();
        TDataType GetValueForObject<TDataType>(ICmsItemInstance o);
        OperationResult SetValueForObject(ICmsItemInstance o, object value);
        OperationResult SetValueForObjectFromFormControl(ICmsItemInstance o, CS.General_v3.Controls.WebControls.Common.General.IMyFormWebControl formControl);
        void SetDefaultSearchValue(object o);
        string GetValueForObjectAsString(ICmsItemInstance cmsItem);
        object GetValueForObject(ICmsItemInstance cmsItem);
        FormFieldListSingleChoiceDropdownData GetFormFieldForBoolNullable();
        string ToString();
        //string GetLinkedQuerystringValueForItem(ICmsItemInstance item);
        //string GetLinkedQuerystringParamName();
        //long? GetLinkedIDFromQuerystring();
        object GetFormValueConvertedToPropertyDataType(object formValue, EnumsCms.SECTION_TYPE sectionType);

        /// <summary>
        /// Returns the listing url for the linked items, such that if this property represents Product.Store, this will return the Product Listing, for the Store specified in 'itemLinkedWith'
        /// </summary>
        /// <param name="itemLinkedWith"></param>
        /// <returns></returns>
        //string GetListingUrlForLinkedType(ICmsItemInstance itemLinkedWith);

        bool GetRequiredValueBasedOnSectionType(EnumsCms.SECTION_TYPE sectionType);

        FormFieldBaseData GetFormFieldBaseDataForProperty(EnumsCms.SECTION_TYPE sectionType, ICmsItemInstance item,
                                                                          bool loadInitialValueFromPostback = false, object value = null);

        FormFieldsRowItemField AddPropertyToFormFieldForCurrentCmsUser(FormFields formFields, EnumsCms.SECTION_TYPE sectionType,
                                                                                       ICmsItemInstance item,
                                                                                       bool loadInitialValueFromPostback = false, object value = null);

        IMyFormWebControl GetFormControlForCmsProperty(ICmsItemInstance item, object value, EnumsCms.SECTION_TYPE sectionType);
        Control GetControlNonEditable(ICmsItemInstance item);
        Control GetControlForCmsPropertyForListingAndCurrentUser(ICmsItemInstance item);
        bool CheckIfCanSearch();
        void AddCriterionToSearchCriteria(ICriteria crit, string searchValue);
        void ApplyOrderByToCriteria(NHibernate.ICriteria crit, CS.General_v3.Enums.SORT_TYPE sortType);
        bool CheckIfUserCanView(ICmsUserBase user);
        bool CheckIfUserCanEdit(ICmsUserBase user);
    }

}
