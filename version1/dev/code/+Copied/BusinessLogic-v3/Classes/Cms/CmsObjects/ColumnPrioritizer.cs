﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Util;
namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public class ColumnPrioritizer
    {
        private List<CmsFieldBase> _properties;
        public ColumnPrioritizer()
        {
            _properties = new List<CmsFieldBase>();
        }
        private void addColumns(IEnumerable<CmsFieldBase> cmsProperties, int index)
        {
            var listCmsProperties = cmsProperties.ToList();
            for (int i = listCmsProperties.Count - 1; i >= 0; i--)
            {
                var p = listCmsProperties[i];

                if (checkIfPropertyAlreadyExists(p))
                {//if existing property, and this property is before the index to add, reduce index by 1
                    int existingIndex = _properties.FindIndex(item => item == p);
                    if (existingIndex < index)
                    {
                        index--;
                    }
                    removePropertyIfExists(p);
                }
                _properties.Insert(index, p);
               // index++;
            }
        }

       
        public void AddColumnsToStart(params CmsFieldBase[] cmsProperties)
        {
            this.AddColumnsToStart((IEnumerable<CmsFieldBase>)cmsProperties);
        }
        public void AddColumnsToStart(IEnumerable<CmsFieldBase> cmsProperties)
        {
            addColumns(cmsProperties, 0);
        }

        public void AddColumnBefore(CmsFieldBase p, CmsFieldBase propertyToAddBefore)
        {
            int existingIndex = _properties.FindIndex(item => item == propertyToAddBefore);
            
            if (existingIndex > -1)
            {
                addColumns(CS.General_v3.Util.ListUtil.GetListFromSingleItem(p), existingIndex);
            }
        }
        public void AddColumnAfter(CmsFieldBase p, CmsFieldBase propertyToAddAfter)
        {
            int existingIndex = _properties.FindIndex(item => item == propertyToAddAfter);

            if (existingIndex > -1)
            {
                addColumns(CS.General_v3.Util.ListUtil.GetListFromSingleItem(p), existingIndex+1);
            }
        }

        public void AddColumnsToEnd(params CmsFieldBase[] cmsProperties)
        {
            this.AddColumnsToEnd((IEnumerable<CmsFieldBase>)cmsProperties);
        }
        public void AddColumnsToEnd(IEnumerable<CmsFieldBase> cmsProperties)
        {
            addColumns(cmsProperties, _properties.Count);
        }
        private bool checkIfPropertyAlreadyExists(CmsFieldBase p)
        {
            return _properties.FindElem(item => item == p) != null;
        }
        private void removePropertyIfExists(CmsFieldBase p)
        {
            _properties.Remove(p);
        }


        public int GetPriorityFor(CmsFieldBase property)
        {
            int priority = 100;
            bool matched = false;
            for (int i = 0; i < _properties.Count; i++)
            {
                if (_properties[i] == property)
                {
                    matched = true;
                    break;
                }

                priority += 100;
            }
            if (!matched)
                priority = Int32.MaxValue;
            return priority;

        }

    }
}
