﻿using System;
using BusinessLogic_v3.Controls.WebControls.Specialised.MediaItems;
using CS.General_v3.Controls.WebControls.Common.General;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties
{
    public class CmsPropertyFormData
    {
        public CmsFieldBase PropertyInfo { get; set; }
        public IMyFormWebControl FormControl { get; set; }
        private BusinessLogic_v3.Classes.Cms.Pages.BasePage pg = null;
        public CmsPropertyFormData(BusinessLogic_v3.Classes.Cms.Pages.BasePage pg, CmsFieldBase pInfo, IMyFormWebControl formControl)
        {
            this.pg = pg;
            this.PropertyInfo = pInfo;
            this.FormControl = formControl;
            attachForAnyStatusMessages();
        }
        public object GetFormValue(EnumsCms.SECTION_TYPE sectionType )
        {

            object formValue = this.FormControl.FormValueObject;
            return this.PropertyInfo.GetFormValueConvertedToPropertyDataType(formValue, sectionType);
        }

        public override string ToString()
        {
            return PropertyInfo.ToString() + ": " + this.FormControl.FormValueObject;
            
        }

        private void attachForAnyStatusMessages()
        {
            if (FormControl is MediaItemControl)
            {
                MediaItemControl ctrl = (MediaItemControl)FormControl;
                ctrl.OnDelete_After += new EventHandler(ctrl_OnDelete_After);
                
            }

        }

        void ctrl_OnDelete_After(object sender, EventArgs e)
        {
            pg.Master.Functionality.ShowStatusMessageAndRedirectToSamePage(PropertyInfo.Label + " deleted successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

    }
}