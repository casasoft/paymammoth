﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Classes.DbObjects;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using CS.General_v3.Util;
using NHibernate;
using CS.General_v3.Classes.Factories;
using System.Collections;
using CS.General_v3.Classes.URL;
using BaseDbObject = BusinessLogic_v3.Classes.DB.BaseDbObject;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Factories
{
    public abstract class CmsFactoryBase<TCMSItemInfo, TDBItem> : CS.General_v3.Classes.Factories.BaseFactory,  ICmsItemFactory
        where TCMSItemInfo : CmsItemInfo
        where TDBItem : BusinessLogic_v3.Classes.DB.BaseDbObject
       
        
    {
        public static void _initialiseStaticInstance()
        {
            var tmp = Instance;

        }
        public static CmsFactoryBase<TCMSItemInfo, TDBItem> Instance
        {
            get 
            {

                return (CmsFactoryBase<TCMSItemInfo, TDBItem>)CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<CmsFactoryBase<TCMSItemInfo, TDBItem>>(null);;
            }
        }
        public static bool UsedInProject { get; set; }
        

        private CS.General_v3.Classes.DbObjects.IBaseDbFactory<TDBItem> _dbFactory = null;
        protected CS.General_v3.Classes.DbObjects.IBaseDbFactory<TDBItem> dbFactory
        {
            get
            {
                if (_dbFactory == null)
                {
                    _dbFactory = (CS.General_v3.Classes.DbObjects.IBaseDbFactory<TDBItem>)CS.General_v3.Classes.DbObjects.DbFactoryController.Instance.GetFactoryForType(typeof(TDBItem));
                }
                return _dbFactory;
            }
        }
        [Obsolete("Use 'this.UserSpecificGeneralInfoInContext.Name' instead")]
        public string Name
        {
            get { return this.UserSpecificGeneralInfoInContext.Name; }
            set { this.UserSpecificGeneralInfoInContext.Name = value; }

        }

        public CmsFactoryBase( )
        {
           // CS.General_v3.Util.ContractsUtil.RequiresNotNullable(dbFactory);
            this.CmsItemType = typeof(TCMSItemInfo);
            //this.dbFactory = dbFactory;

           // this.LinkedChildren = new List<ICmsItemFactory>();
         
           

            CmsSystemBase.Instance.RegisterItemType(this);
            

        }

        public override void OnPostInitialisation()
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(dbFactory, "Database factory must be initialised for Cms factory to work");
            this.addTypeCreatedByFactory(this.CmsItemType);
            this.addTypeCreatedByFactory(dbFactory.GetTypesCreatedByFactory());
            //initCmsParameters();
            base.OnPostInitialisation();
        }

        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase getLoggedCmsUser()
        {
            return CmsUserSessionLogin.Instance.GetLoggedInUser(); 
        }
        protected abstract IQueryOver _getQueryForSearchResults(bool showDeleted);
        protected virtual void parseQueryForListing(IQueryOver query)
        {

        }

        [Obsolete("Use the 'getUserSpecificGeneralInfo'")]
        protected virtual void initCmsParameters()
        {

        }
        public abstract TCMSItemInfo CreateCmsItemInfo(TDBItem item);
        public virtual TCMSItemInfo CreateNewItem(bool isTemporary)
        {
            var item = _createNewItem(isTemporary);
            
           
            return item;
            
            

        }
        protected abstract TCMSItemInfo _createNewItem(bool isTemporary);
        protected string baseFolder { get; set; }
      //  protected string listingPageName { get; set; }
      //  protected string editPageName { get; set; }

        public CS.General_v3.Classes.URL.URLClass GetAddUrl()
        {
            return GetEditUrlForItemWithID(0);
        }

        //public ICmsItemFactory LinkedParent { get; set; }
        
        public long? GetIDFromCurrentQuerystring()
        {

            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long?>(QueryStringParamID);
        }

        private void addCurrentCultureToUrl(URLClass url)
        {
            url[BusinessLogic_v3.Classes.Cms.Pages.BaseMasterPage.QUERYSTRING_PARAM_CULTUREID] = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture().ID;
            
        }

        public CS.General_v3.Classes.URL.URLClass GetEditUrlForItemWithID(long id)
        {

            string s = CmsRoutesMapper.Instance.GetEditPage(this.GetTitleForRoutingFolder());
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(s);
            url.ClearQueryString();
            url.LoadFromCurrentQuerystring();
            url[QueryStringParamID] = null;
            if (id > 0)
                url[QueryStringParamID] = id;
            addCurrentCultureToUrl(url);
            resetLastAddedQueryStringParam(url);
            url.PageURL = s;
            return url;
            
        }
        public bool CheckIfFactoryIsOfType(Type t)
        {
            if (this.CmsItemType.IsAssignableFrom(t) ||
                t.IsAssignableFrom(this.CmsItemType))
                return true;
            else
                return false;
        }





        public IEnumerable<CmsFieldBase> GetPropertyInfos(TCMSItemInfo item)
        {
            TDBItem dbItem = null;
            if (item != null)
                dbItem =(TDBItem)item.DbItem;
            var cmsInfo = CreateCmsItemInfo(dbItem);
            return cmsInfo.GetListOfCMSProperties();

        }

        public virtual PageTitle GetListingPageTitle()
        {
            string title = null;
            long id = GetIDFromCurrentQuerystring().GetValueOrDefault();
            title = this.TitlePlural;
           return new PageTitle(title, GetListingUrl().ToString() );

        }
        public virtual PageTitle GetEditPageTitle(long id)
        {
            string title = null;
            ICmsItemInstance item = null;
            title = this.TitleSingular;
            if (id > 0)
            {
                item = GetCmsItemWithId(id);
            }
            if (item != null && !item.CheckIfStillTemporary())
            {
                title = "Edit " + this.TitleSingular + " [" + item.TitleForCms + "]";
            }
            else
            {
                title = "Add new " + this.TitleSingular;
            }
            return new PageTitle(title, GetEditUrlForItemWithID(id).ToString());

        }
        public virtual PageTitle GetEditPageTitle()
        {
            long id = GetIDFromCurrentQuerystring().GetValueOrDefault();
            return GetEditPageTitle(id);
        }

        protected ICmsCollectionInfo getFirstLinkedProperty()
        {
            foreach (var p in this.GetPropertyInfos(null))
            {
                if (p is ICmsCollectionInfo)
                {
                    ICmsCollectionInfo collInfo = (ICmsCollectionInfo)p;
                    if (collInfo.ShowLinkInCms && collInfo.GetLinkedIDFromQuerystring().HasValue)
                    {
                        return collInfo;
                        
                    }
                }
            }
            return null;
        }

        public virtual IEnumerable<PageTitle> GetListingPageTitlesForThisAndLinked()
        {
            List<PageTitle> list = new List<PageTitle>();

            var linkedProperty = getFirstLinkedProperty();
            if (linkedProperty != null && linkedProperty.GetLinkedIDFromQuerystring().GetValueOrDefault() > 0)
            {
                long linkedID = linkedProperty.GetLinkedIDFromQuerystring().Value;

                var linkedFactory = linkedProperty.GetFactoryForLinkedObject();
                if (linkedFactory != this)
                {
                    list.AddRange(linkedFactory.GetEditPageTitlesIncludingLinked(linkedID));
                }
            }

            list.Add(GetListingPageTitle());
            return list;
        }
        public virtual IEnumerable<PageTitle> GetEditPageTitlesIncludingLinked(long itemID)
        {
            List<PageTitle> list = new List<PageTitle>();
            list.AddRange(this.GetListingPageTitlesForThisAndLinked());
            list.Add(this.GetEditPageTitle(itemID));
            return list;
        }
        public virtual IEnumerable<PageTitle> GetEditPageTitlesForThisAndLinked()
        {
            long id = GetIDFromCurrentQuerystring().GetValueOrDefault();
            return GetEditPageTitlesIncludingLinked(id);
        }
        public virtual string GetGoBackUrlForListing()
        {
            string s = null;
            {
                var linkedP = getFirstLinkedProperty();
                if (linkedP != null)
                {
                    long id = linkedP.GetLinkedIDFromQuerystring().GetValueOrDefault();
                    if (id > 0)
                    {
                        var linkedFactory = linkedP.GetFactoryForLinkedObject();
                        s = linkedFactory.GetEditPageTitlesIncludingLinked(id).ToString();
                    }
                }
                
            }
            return s;


        }
        public virtual string GetGoBackUrlForEdit()
        {
            return this.GetListingUrl().ToString();
        }
        private string _QueryStringParamID = null;

        public string QueryStringParamID
        {
            get 
            {
                string s = _QueryStringParamID;
                if (string.IsNullOrEmpty(s)) s = this.Name + "Id";
                return s;
                
            }
            set { _QueryStringParamID = value; }
        }
        

        protected string getBaseFolder()
        {
            string s = this.baseFolder ?? "";
            if (!s.EndsWith("/")) s += "/";
            return s;
        }
        private void resetLastAddedQueryStringParam(CS.General_v3.Classes.URL.URLClass url)
        {
            url[CmsConstants.QUERYSTRING_LASTADDED] = null;
        }

        /// <summary>
        /// Returns the cms folder, e.g /cms/Category/
        /// </summary>
        /// <returns></returns>
        public string GetCmsFolderUrl(bool includeBaseUrl = true)
        {
            string baseFolder = getBaseFolder();
            if (baseFolder.StartsWith("/")) baseFolder = baseFolder.Substring(1);
            string s = "/";
            if (includeBaseUrl)
                s = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl().GetURL(fullyQualified: true, includeRelativeUrl: false, appendQueryString: false); ;
            s += baseFolder;
            //string s = CS.General_v3.Util.PageUtil.GetBaseURL() + baseFolder;
            return s;
        }

        public virtual string GetTitleForRoutingFolder()
        {
            return this.Name;
        }

        
        


        public virtual CS.General_v3.Classes.URL.URLClass GetListingUrl(bool removeAnyQuerystringParams = false)
        {
            string s = CmsRoutesMapper.Instance.GetListingPage(this.GetTitleForRoutingFolder());
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(s);

            url.ClearQueryString();
            url.LoadFromCurrentQuerystring();

            if (removeAnyQuerystringParams)
                url.ClearQueryString();

            resetLastAddedQueryStringParam(url);

            addCurrentCultureToUrl(url);
            
            /*if (removeAnyQuerystringParams)
            {
                CmsInfoBase.Instance.RemoveAnyQuerystringParametersRelatedToCms(url);
            }*/

            url[QueryStringParamID] = null;
            
            return url;

            

        }

        public ICmsItemInstance GetCmsItemWithId(long id)
        {
            var dbItem =dbFactory.GetByPrimaryKey(id);
            ICmsItemInstance cmsItem = null;
            if (dbItem != null)
                cmsItem = CreateCmsItemInfo(dbItem);
            return cmsItem;
            

        }


        public List<TCMSItemInfo> ConvertDataItemsToCmsItems(IEnumerable<TDBItem> dbItems)
        {
            List<TCMSItemInfo> list = new List<TCMSItemInfo>();
            foreach (var dbItem in dbItems)
            {
                var cmsItem = CreateCmsItemInfo(dbItem);
                list.Add(cmsItem);
            }
            return list;

        }
        private List<TCMSItemInfo> convertDataItemsToCmsItems(IEnumerable dbItems)
        {
            List<TDBItem> list = new List<TDBItem>();
            foreach (var dbItem in dbItems)
            {
                list.Add((TDBItem)dbItem);
            }
            return ConvertDataItemsToCmsItems(list);
            

        }


        protected IEnumerable<ICmsItemInstance> getSearchResultsFromCriteria(NHibernate.ICriteria crit, int pageNo, int pageSize, out int totalResults)
        {
            if (pageSize > 500)
            {
                //2011-10-28 Done by Mark to help not hog the server if there is too much items to load
                pageSize = 500;
            }
            IEnumerable<TDBItem> results = nHibernateUtil.LimitCriteriaByPrimaryKeysAndReturnResultAndTotalCount<TDBItem>(crit,  pageNo, pageSize, out totalResults, useFutures: false);

         //   return results.ConvertAll<object, ICmsItemInstance>(item=>(ICmsItemInstance)item);
            //var list = dbFactory.FindAll(crit);
            //CS.General_v3.Util.nHibernateUtil.LimitQueryByPrimaryKeysAndGetTotalCount(

            return convertDataItemsToCmsItems(results);
        }

        
        
        public virtual CmsSearchResults GetSearchResultsForListing(IEnumerable<CmsPropertySearchInfo> searchCriteria, bool showDeleted, bool filterByLinkedItems, int pageNo, int pageSize, 
            CmsFieldBase propertyToSortWith, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            var query = _getQueryForSearchResults(showDeleted);
            parseQueryForListing(query);
            var crit = query.RootCriteria;
            
            if (searchCriteria != null) // fill search criterias
            {
                foreach (var p in searchCriteria)
                {
                    
                    p.AddCriterionToSearchCriteria(crit);
                    

                }
            }
            if (filterByLinkedItems)
            {
                foreach (var property in this.GetPropertyInfos(null)) // fill links
                {
                    if (property is CmsLinkedPropertyInfo)
                    {
                        CmsLinkedPropertyInfo p = (CmsLinkedPropertyInfo)property;
                        
                        CmsCollectionInfo collInfo = p.GetLinkedObjectCollectionFromOtherSide();

                        if (collInfo != null)
                        {


                            if (collInfo.ShowLinkInCms)
                            {
                                var linkedFactory = collInfo.CmsFactory;
                                long? linkedID = collInfo.GetLinkedIDFromQuerystring();
                                if (linkedID.GetValueOrDefault() > 0)
                                {
                                    //var item = linkedFactory.GetCmsItemWithId(linkedID.Value);
                                    //if (item != null)
                                    {
                                        var expression = Util.CmsUtil.GetNHibernateCriterion(p.PropertyInfo, linkedID.Value);
                                        if (expression != null)
                                            crit.Add(expression);
                                    }
                                }
                            }
                            if (collInfo.PreFetchInListing)
                            {
                                //fetch any items which will be used in the listing
                                //                            if (p.PropertyInfo.PropertyType.IsAssignableFrom(typeof(IEnumerable)))
                                crit.SetFetchMode(p.PropertyInfo.Name, FetchMode.Join); //if it is a collection, use a subselect (this should never be the case, as collections cannot be show in listings)
                                //else
                                //    crit.SetFetchMode(p.PropertyInfo.Name, FetchMode.Join); //if it is a direct link, use a join
                            }
                        }
                        
                    }
                }
            }
            if (propertyToSortWith != null)
            {
                propertyToSortWith.ApplyOrderByToCriteria(crit, sortType);

                //crit.AddOrder(new Order(sortBy, sortType == CS.General_v3.Enums.SORT_TYPE.Ascending));
            }
            
            
            
            

            
            CmsSearchResults searchResults = new CmsSearchResults();
            int totalResults ;
            var results = getSearchResultsFromCriteria(crit, pageNo, pageSize, out totalResults);

            searchResults.TotalCount = totalResults;
            searchResults.SearchItems = results.ConvertAll(item => (ICmsItemInstance)item);


            return searchResults;

        }

        protected virtual string getPrimaryKeyName()
        {
            return "ID";
            
        }

      


        /*
        public IEnumerable<CmsPropertyBase> GetPropertyInfos(TDBItem item)
        {
            var cmsInfo = CreateCmsItemInfo(item);
            return cmsInfo.GetListOfCMSProperties();

        }*/


        ICmsItemInstance ICmsItemFactory.CreateNewItem(bool isTemporary)
        {
            return this.CreateNewItem(isTemporary);
            
        }

        private CmsItemGeneralInfo _userSpecificGeneralInfoInContext
        {
            get
            {
                return CS.General_v3.Util.PageUtil.GetContextObject<CmsItemGeneralInfo>("CMS_" + this.CmsItemType.Name + "_userSpecificGeneralInfo");
            }
            set { CS.General_v3.Util.PageUtil.SetContextObject("CMS_" + this.CmsItemType.Name + "_userSpecificGeneralInfo", value); }
        }
        public CmsItemGeneralInfo UserSpecificGeneralInfoInContext
        {
            get 
            {
                var info = _userSpecificGeneralInfoInContext;
                if (info == null)
                {
                    info = new CmsItemGeneralInfo(this);
                    _userSpecificGeneralInfoInContext = info;
                    getUserSpecificGeneralInfo(info);
                    
                }
                return info;
            }
        }

        
        protected virtual void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            initCmsParameters();
            initCustomCmsOperations();
        }

        //CmsAccessType ICmsItemFactory.AccessTypeRequired_ToAdd
        //{
        //    get 
        //    {
        //        var userInfo = UserSpecificGeneralInfoInContext;
        //        return CmsAccessType.GetMostRestrictiveAccessType(userInfo.AccessTypeRequired_ToAdd, userInfo.AccessTypeRequired_ToView); 
        //    }
        //}
        //CmsAccessType ICmsItemFactory.AccessTypeRequired_ToView
        //{
        //    get 
        //    {
        //        var userInfo = UserSpecificGeneralInfoInContext; 
        //        return CmsAccessType.GetMostRestrictiveAccessType(userInfo.AccessTypeRequired_ToView); 
        //    }
        //}
        
        
        protected virtual CmsUserBase getCurrentLoggedUser()
        {
            return CmsUserSessionLogin.Instance.GetLoggedInUser();
        }

        



        /// <summary>
        /// Returns a list of ListItems
        /// </summary>
        /// <param name="propertyInfo">Property Info that is retrieving this information. Optional</param>
        /// <param name="addNullValue">Whehter to add a null value on top</param>
        /// <param name="excludeID">Id to exclude</param>
        /// <param name="filterByLinkedItems">Whether to filter search results by linked items (based on querystring)</param>
        /// <returns></returns>
        public virtual System.Web.UI.WebControls.ListItem[] GetAllAsListItemsForCms(CmsFieldBase propertyInfo = null, bool addNullValue = false, long excludeID=0, bool filterByLinkedItems= false)
        {
            var searchResults = this.GetSearchResultsForListing(null, false, filterByLinkedItems, 1, 0, null, CS.General_v3.Enums.SORT_TYPE.Ascending);
            List<System.Web.UI.WebControls.ListItem> list = new List<System.Web.UI.WebControls.ListItem>();
            foreach (var item in searchResults.SearchItems)
            {
                if (!item.CheckIfStillTemporary())
                {
                    if (item.DbItem.ID != excludeID)
                    {
                        list.Add(new System.Web.UI.WebControls.ListItem(item.TitleForCms, item.DbItem.ID.ToString()));
                    }
                }
            }
            list.Sort((item1, item2) => (item1.Text.CompareTo(item2.Text)));
            if (addNullValue)
                list.Insert(0,new System.Web.UI.WebControls.ListItem("", ""));
            return list.ToArray();
            
        }

        public virtual Type CmsItemType { get; protected set; }



        public virtual IEnumerable<ICmsHierarchyItem> GetRootItems(IEnumerable<CmsPropertySearchInfo> searchCriteria)
        {
            throw new InvalidOperationException("CMSFactoryBase<" + this.CmsItemType.ToString() + "> : Please override 'GetRootItems' if you want to list this item as a tree view");
            
            
        }


        
       

        protected virtual void initCustomCmsOperations()
        {
            
        }

        /// <summary>
        /// Returns a list of cms operations
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CmsGeneralOperation> GetCustomCmsOperations()
        {
          

            var info = this.UserSpecificGeneralInfoInContext;
             return info.CustomCmsOperations;


        }


        #region ICmsItemFactory Members

        public ICmsItemInstance GetEmptyCmsItemInfo()
        {
            return CreateCmsItemInfo(null);
        }

        public IEnumerable<CmsFieldBase> GetPropertyInfos(ICmsItemInstance item)
        {
            if (item == null)
            {
                item = GetEmptyCmsItemInfo();
                //item = _createNewItem();
            }

            return item.GetCmsProperties();
            
        }
        public TCMSItemInfo GetCmsItemFromDBObject(TDBItem dbObject)
        {
            return CreateCmsItemInfo(dbObject);
        }
        public IEnumerable<TCMSItemInfo> GetListOfCmsItemFromDBObjects(IEnumerable<TDBItem> dbObjects)
        {
            List<TCMSItemInfo> list = new List<TCMSItemInfo>();
            foreach (var dbItem in dbObjects)
            
            {
                if (dbItem != null)
                {
                    list.Add(GetCmsItemFromDBObject(dbItem));
                }
            }
            return list;
            
        }
        ICmsItemInstance ICmsItemFactory.GetCmsItemFromDBObject(BaseDbObject dbObject)
        {

            return CreateCmsItemInfo((TDBItem)dbObject);
            
        }

      
        

        public virtual CmsFieldBase GetDefaultSortFieldForListing(out CS.General_v3.Enums.SORT_TYPE sortType)
        {
            sortType = CS.General_v3.Enums.SORT_TYPE.None;
            return null;
            
        }

        #endregion





        #region ICmsItemFactory Members

        EnumsCms.SECTION_RENDER_TYPE ICmsItemFactory.RenderType
        {
            get { return this.UserSpecificGeneralInfoInContext.RenderType; }
        }

        int ICmsItemFactory.CmsMainMenuPriority
        {
            get { return this.UserSpecificGeneralInfoInContext.CmsMainMenuPriority; }
        }

        bool ICmsItemFactory.ShowInCmsMainMenu
        {
            get { return this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu; }
        }

        string ICmsItemFactory.Name
        {
            get { return this.UserSpecificGeneralInfoInContext.Name; }
        }

        EnumsCms.IMAGE_ICON ICmsItemFactory.CmsImageIcon
        {
            get { return this.UserSpecificGeneralInfoInContext.CmsImageIcon; }
        }


        [Obsolete("User 'this.UserSpecificGeneralInfoInContext.TitlePlural' instead")]
        public string TitlePlural
        {
            get { return this.UserSpecificGeneralInfoInContext.TitlePlural; }
            set { this.UserSpecificGeneralInfoInContext.TitlePlural = value; }

        }
        [Obsolete("User 'this.UserSpecificGeneralInfoInContext.TitleSingular' instead")]
        public string TitleSingular
        {
            get { return this.UserSpecificGeneralInfoInContext.TitleSingular; }
            set { this.UserSpecificGeneralInfoInContext.TitleSingular = value; }
        }


        protected override IEnumerable<object> __getAllItemsInRepository()
        {
            var dbList = this.dbFactory.GetAllItemsInRepository();
            return convertDataItemsToCmsItems(dbList);
            
        }

        public IEnumerable<TCMSItemInfo> GetAllItemsInRepository()
        {
            var list = __getAllItemsInRepository();
            return convertDataItemsToCmsItems(list);
            
        }


        IEnumerable<ICmsItemInstance> ICmsItemFactory.GetAllItemsInRepository()
        {
            return GetAllItemsInRepository();
            
        }

        #endregion

        #region ICmsItemFactory Members

        IBaseDbFactory ICmsItemFactory.GetDbFactory()
        {
            return this.dbFactory;
            
        }

        #endregion

        #region ICmsItemFactory Members


        public void ThrowErrorIfNotUsed()
        {
            var db = dbFactory;
            if (!UsedInProject)
            {
                throw new InvalidOperationException("Cannot use this section of the CMS if it is not used in project!");

            }
        }

        #endregion



        #region ICmsItemFactory Members


        ICmsItemGeneralInfo ICmsItemFactory.UserSpecificGeneralInfoInContext
        {
            get { return this.UserSpecificGeneralInfoInContext; }
        }

        #endregion

        #region ICmsItemFactory Members


        public bool GetUsedInProject()
        {
            return UsedInProject;
            
        }

        #endregion
    }
}