﻿using System;
using System.Collections.Generic;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;



namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Factories
{
    public interface ICmsItemFactory : CS.General_v3.Classes.Factories.IBaseFactory
    {
        IBaseDbFactory GetDbFactory();
        IEnumerable<ICmsItemInstance> GetAllItemsInRepository();
        /// <summary>
        /// Type of Render for listing - E.g Tree or Listing.  Default is listing
        /// </summary>
        EnumsCms.SECTION_RENDER_TYPE RenderType { get; }
        /// <summary>
        /// Type of CmsItem, e.g Artist
        /// </summary>
        Type CmsItemType { get; }
        /// <summary>
        /// Checks whether this factory belongs to the given type, e.g Is factory of Artist?
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
     //   bool CheckIfFactoryIsOfType(Type t);

        //CmsAccessType AccessTypeRequired_ToAdd { get; }
       // CmsAccessType AccessTypeRequired_ToView { get;  }
       // bool CurrentUserCanView();
        CS.General_v3.Classes.URL.URLClass GetAddUrl();
        int CmsMainMenuPriority { get; }
        bool ShowInCmsMainMenu { get; }
        IEnumerable<CmsGeneralOperation> GetCustomCmsOperations();
       // void InitCmsParameters();

        CS.General_v3.Classes.URL.URLClass GetEditUrlForItemWithID(long id);

        string Name { get;  }

        /// <summary>
        /// Returns the list of properties for the item
        /// </summary>
        /// <returns></returns>
        System.Collections.Generic.IEnumerable<CmsFieldBase> GetPropertyInfos(ICmsItemInstance item);

        ICmsItemInstance GetCmsItemFromDBObject(BusinessLogic_v3.Classes.DB.BaseDbObject dbObject);

      //  ICmsItemFactory LinkedParent { get; }
        //IEnumerable<ICmsItemFactory> GetLinkedChildren();
        //IEnumerable<CmsPropertyInfo> GetLinkedProperties();
        ICmsItemInstance GetCmsItemWithId(long id);
        /// <summary>
        /// This must take care about the links, making sure that if the item has linked items, it is automatically linked e.g, if you create a new product and there is currently the store id in the querystring, it is updated
        /// </summary>
        /// <returns></returns>
        ICmsItemInstance CreateNewItem(bool isTemporary);
        ICmsItemInstance GetEmptyCmsItemInfo();
        /// <summary>
        /// Returns the current ID based on the querystring
        /// </summary>
        /// <returns></returns>
        long? GetIDFromCurrentQuerystring();
        //PageTitle GetListingPageTitle();
        /// <summary>
        /// Returns the page titles for listing, for this item and any 'linked' items
        /// </summary>
        /// <returns></returns>
        IEnumerable<PageTitle> GetListingPageTitlesForThisAndLinked();
        /// <summary>
        /// Returns the page titles for an edit page, for this item and any 'linked' items.
        /// </summary>
        /// <returns></returns>
        IEnumerable<PageTitle> GetEditPageTitlesForThisAndLinked();
        string GetGoBackUrlForListing();
        string GetGoBackUrlForEdit();

        string QueryStringParamID { get; }
        string TitlePlural { get;  }
        string TitleSingular { get; }
        /// <summary>
        /// Returns all items to be used in a drop-down list item
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="addNullValue"></param>
        /// <param name="excludeID"></param>
        /// <returns></returns>
        System.Web.UI.WebControls.ListItem[] GetAllAsListItemsForCms(CmsFieldBase propertyInfo, bool addNullValue, long excludeID, bool filterByLinkedItems);
        IEnumerable<PageTitle> GetEditPageTitlesIncludingLinked(long itemID);

        /// <summary>
        /// Returns general search results for a 'listing' type
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortBy"></param>
        /// <param name="sortType"></param>
        /// <returns></returns>
        CmsSearchResults GetSearchResultsForListing(IEnumerable<CmsPropertySearchInfo> searchCriteria, bool showDeletedItems, bool filterByLinkedItems, int pageNo, int pageSize, CmsFieldBase sortBy, CS.General_v3.Enums.SORT_TYPE sortType);
        /// <summary>
        /// Returns search results for a 'hierarchical' listing
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        IEnumerable<ICmsHierarchyItem> GetRootItems(IEnumerable<CmsPropertySearchInfo> searchCriteria);
       

        CS.General_v3.Classes.URL.URLClass GetListingUrl(bool removeAnyQuerystringParameters = false);

        EnumsCms.IMAGE_ICON CmsImageIcon { get; }
        ICmsItemGeneralInfo UserSpecificGeneralInfoInContext { get; }

        //CmsPropertyBase GetDefaultSortFieldForListing(out CS.General_v3.Enums.SORT_TYPE sortType);

        void ThrowErrorIfNotUsed();

       
    }
}
