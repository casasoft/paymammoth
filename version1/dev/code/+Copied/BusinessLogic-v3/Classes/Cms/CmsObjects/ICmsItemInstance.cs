﻿using System;
using System.Collections.Generic;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Classes.HelperClasses;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public interface ICmsItemInstance
    {
        OperationResult DeleteFromCms();
        ColumnPrioritizer GetListingOrderPrioritizer();
        ColumnPrioritizer GetEditOrderPrioritizer();
        //ICMSPropertySpecificInfo GetPropertySpecificInfoForProperty(CMSPropertyGeneralInfo propertyInfo);
        IEnumerable<CmsFieldBase> GetCmsProperties();
        BusinessLogic_v3.Classes.DB.IBaseDbObject DbItem { get; }
        ICmsItemFactory GetCmsFactoryForItem();
       // long ID { get;  }
        //int Priority { get; set; }
        OperationResult SaveFromCms(SaveParams saveParams = null);
        string TitleForCms { get;  }

        bool CheckIfStillTemporary();
        bool HasTemporaryFields();

        void ErrorOccurredWhileSavingInCms();
        bool CheckIfUserCanEdit(ICmsUserBase user);
        bool CheckIfUserCanDelete(ICmsUserBase user);
        bool CheckIfUserCanView(ICmsUserBase user);
        CmsAccessType GetAccessRequiredToDelete();
        CmsAccessType GetAccessRequiredToEdit();
        CmsAccessType GetAccessRequiredToView();
        //CmsAccessType AccessTypeRequired_ToDelete {get;}
        //CmsAccessType AccessTypeRequired_ToView { get; }
        //CmsAccessType AccessTypeRequired_ToEdit { get; }

        IEnumerable<CmsItemSpecificOperation> GetCustomCmsOperations();


        void CheckCustomAccess(OperationResult result);
    }
}
