﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using CS.General_v3.Classes.HelperClasses;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.CustomPropertyTypes
{
    public class CmsCustomProperty<TItem, TDataType> : CmsPropertyGeneral
        where TItem: ICmsItemInstance
    {


        private Func<TItem, TDataType> _valueGetter = null;
        private Func<TItem, TDataType, OperationResult> _valueSetter = null;
        private Func<TDataType, NHibernate.Criterion.ICriterion> _getSearchCriterion = null;
        private Action<NHibernate.ICriteria, CS.General_v3.Enums.SORT_TYPE> _orderBySetter = null;
       /// <summary>
       /// 
       /// </summary>
       /// <param name="itemGeneralInfo"></param>
       /// <param name="valueGetter">Required.  Should retrieve the value for this property</param>
       /// <param name="valueSetter">Optional.  Sets the value to this property. If not set, it means property is readonly</param>
       /// <param name="getSearchCriterion">Optional.  Retrieves </param>
       /// <param name="orderBySetter">Optional. An action which sets the order by to the given criteria</param>
        public CmsCustomProperty(ICmsItemFactory itemGeneralInfo, string label,
            Func<TItem, TDataType> valueGetter,
            Func<TItem, TDataType, OperationResult> valueSetter = null,
            Func<TDataType, NHibernate.Criterion.ICriterion> getSearchCriterion = null,
        Action<NHibernate.ICriteria, CS.General_v3.Enums.SORT_TYPE> orderBySetter = null)
            : base(itemGeneralInfo)
        {

            this.Label = label;
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(valueGetter, "Getter must never be null");
            this._valueGetter = valueGetter;
            this._valueSetter = valueSetter;
            this._getSearchCriterion = getSearchCriterion;
            this._orderBySetter = orderBySetter;
            if (valueSetter == null) IsReadOnly = true;
            this.IsSearchable = (getSearchCriterion != null);
            this.SortableInListing = (orderBySetter != null);
            this.DataType = EnumsCms.GetDataTypeFromType(typeof(TDataType));
        }



        public override ICmsItemFactory GetFactoryForLinkedObject()
        {
            throw new NotImplementedException("This should never be applicable for a custom-property");
        }

        public override TDataType GetValueForObject<TDataType>(ICmsItemInstance o)
        {
            return (TDataType)(object)_valueGetter((TItem)o);
            
        }

        public override OperationResult SetValueForObject(ICmsItemInstance o, object value)
        {
            if (!IsReadOnly)
                return _valueSetter((TItem)o, (TDataType)value);
            else
                throw new InvalidOperationException("This should never be called on a read-only property");
            
        }

        public override NHibernate.Criterion.ICriterion GetNHibernateCriterionForSearch(string s)
        {


            if (_getSearchCriterion != null)
            {
                TDataType dataTypeCasted = CS.General_v3.Util.Other.ConvertStringToBasicDataType<TDataType>(s);

                return _getSearchCriterion(dataTypeCasted);
            }
            else
                throw new InvalidOperationException("This should never be searchable");
            
        }

        public override void ApplyOrderByToCriteria(NHibernate.ICriteria crit, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            if (_orderBySetter != null)
                _orderBySetter(crit, sortType);
            else
                throw new InvalidOperationException("This should never be sortable");
        }
    }
}
