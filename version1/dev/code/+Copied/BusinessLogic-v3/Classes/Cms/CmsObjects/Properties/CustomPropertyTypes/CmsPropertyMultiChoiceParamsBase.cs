﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Reflection;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes
{
    public abstract class CmsPropertyMultiChoiceParamsBase<TMainItem, TManyToManyLink, TLinkedItem> : ICmsPropertyMultiChoiceParamsBase
        where TMainItem : BusinessLogic_v3.Classes.DB.IBaseDbObject
        where TManyToManyLink : BusinessLogic_v3.Classes.DB.IBaseDbObject
        where TLinkedItem: BusinessLogic_v3.Classes.DB.IBaseDbObject

    {
        /// <summary>
        /// This should be a method that returns the list of possible list items for selection, for the [item]
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected abstract IEnumerable<ListItem> getListItemsFromItem(TMainItem item);
        /// <summary>
        /// This should return the Unique ID for the linked item, for example for a 'ContentPageParentChildLink', for a many-to-many from a child to a parent, it would be link.ParentID (string)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected abstract string getUniqueIDFromItem(TManyToManyLink item);
        /// <summary>
        /// Should set the values to the main item, from a list of strings
        /// </summary>
        /// <param name="item"></param>
        /// <param name="values"></param>
        protected abstract void setValuesToItem(TMainItem item, IEnumerable<string> values);

        /// <summary>
        /// Returns the property that matches the many-to-many collection link, e.g cp => cp.ParentChildLinks;
        /// </summary>
        /// <returns></returns>
        protected abstract System.Linq.Expressions.Expression<Func<TMainItem, IEnumerable<TManyToManyLink>>> getPropertySelector();

        /// <summary>
        /// Returns the property that retrieves the linked item, from the many-to-many collection link
        /// </summary>
        /// <returns></returns>
        protected abstract System.Linq.Expressions.Expression<Func<TManyToManyLink, TLinkedItem>> getChildPropertyFromLinkedItemSelector();


        private PropertyInfo _property;
        public PropertyInfo Property
        {
            get 
            {
                if (_property == null)
                    _property = CS.General_v3.Util.ReflectionUtil<TMainItem>.GetPropertyBySelector<IEnumerable<TManyToManyLink>>(getPropertySelector());
                return _property; 
            }
            private set 
            {
                _property = value; 
            }
        }
        private PropertyInfo _propertyLinkedItemFromLink = null;
        public PropertyInfo GetPropertyToRetrieveLinkedItemFromLink()
        {
            if (_propertyLinkedItemFromLink == null)
                _propertyLinkedItemFromLink = CS.General_v3.Util.ReflectionUtil<TManyToManyLink>.GetPropertyBySelector(getChildPropertyFromLinkedItemSelector());
            return _propertyLinkedItemFromLink;
            
        }
        

        public CmsPropertyMultiChoiceParamsBase()
        {
            

        }



        #region ICmsPropertyMultiChoiceParamsBase Members

        IEnumerable<ListItem> ICmsPropertyMultiChoiceParamsBase.GetListItemsFromItem(BusinessLogic_v3.Classes.DB.IBaseDbObject item)
        {
            return getListItemsFromItem((TMainItem)item);
            
        }

        string ICmsPropertyMultiChoiceParamsBase.GetUniqueIDFromItem(BusinessLogic_v3.Classes.DB.IBaseDbObject linkedItem)
        {
            return getUniqueIDFromItem((TManyToManyLink)linkedItem);
            
        }

        void ICmsPropertyMultiChoiceParamsBase.SetValuesToItem(BusinessLogic_v3.Classes.DB.IBaseDbObject item, IEnumerable<string> values)
        {
            setValuesToItem((TMainItem)item, values);
            
        }

        #endregion

        #region ICmsPropertyMultiChoiceParamsBase Members



        /// <summary>
        /// Returns a custom criterion for searching this many-to-many link. For example, a search for the link between Product -> Category, would return a custom search
        /// that loads subcategories as well.  If returned as null, default search is taken, i.e one-to-one mapping.
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        public virtual NHibernate.Criterion.ICriterion GetCustomCriterionForSearch(NHibernate.ICriteria criteria, List<long> ids)
        {
            return null;
        }



        #endregion
    }
}
