﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.WebControls;
using CS.General_v3.Controls.WebControls.Common;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public class CmsGeneralOperation
    {
        public delegate void NullHandler();

        public CmsGeneralOperation(string title, EnumsCms.IMAGE_ICON icon, string href)
        {
            this.Title = title;
            this.Icon = icon;
            this.Href = href;
        }

        public CmsGeneralOperation(string title, EnumsCms.IMAGE_ICON icon, NullHandler delegateToCall)
        {
            this.Title = title;
            this.Icon = icon;
            this.OnClick += delegateToCall;


        }

        private void _eventHandler(object sender, EventArgs e)
        {
            this.OnClick();
        }

        public EventHandler GetButtonClickAsEventHandler()
        {
            return _eventHandler;
        }

        public NullHandler GetButtonClickAsNullHandler()
        {
            return OnClick;
        }


        public string Title { get; set; }
        public EnumsCms.IMAGE_ICON Icon { get; set; }
        public string ConfirmMessage { get; set; }
        public string Href { get; set; }
        public event NullHandler OnClick;

        
        public bool IsLink()
        {
            return !string.IsNullOrWhiteSpace(Href);
        }
        public bool IsButton()
        {
            return this.OnClick != null;
        }

        public MyButton AddToButtonsBarAsButton(ButtonsBar buttonsBar)
        {
            if (!IsButton())
                throw new InvalidOperationException("Cannot add to buttons bar if this is not a button");
            var btn =  buttonsBar.AddButton(this.Title, this.Icon, this.GetButtonClickAsEventHandler());
                        btn.ConfirmMessage = this.ConfirmMessage;
            return btn;
              
        }
        public MyAnchor AddToButtonsBarAsLink(ButtonsBar buttonsBar)
        {
            if (!IsLink())
                throw new InvalidOperationException("Cannot add to buttons bar if this is not a link");
            var btn = buttonsBar.AddLink(this.Title, this.Icon, this.Href);
            return btn;
        }
        public CS.General_v3.Controls.WebControls.BaseWebControl AddToButtonsBar(ButtonsBar buttonsBar)
        {
            if (IsButton())
                return (CS.General_v3.Controls.WebControls.BaseWebControl)AddToButtonsBarAsButton(buttonsBar);
            else
                return (CS.General_v3.Controls.WebControls.BaseWebControl)AddToButtonsBarAsLink(buttonsBar);
        }
    }
}
