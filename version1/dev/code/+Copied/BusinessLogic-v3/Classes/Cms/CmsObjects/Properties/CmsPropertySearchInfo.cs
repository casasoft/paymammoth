﻿using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
using NHibernate.Criterion;
using CS.General_v3.Classes.URL;
using NHibernate;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties
{
    public class CmsPropertySearchInfo
    {
        public CmsPropertySearchInfo(CmsFieldBase pInfo, FormFieldsRowItemField formField)
        {
            this.PropertyInfo = pInfo;
            this.FormField = formField;
          //  initValue();

            object o = GetSearchValue();
            if (o != null)
            {
                object value = o;
                if (o is ICmsItemInstance)
                {
                    ICmsItemInstance cmsItem = (ICmsItemInstance)o;
                    value = cmsItem.DbItem.ID;
                }
                else if (o is BusinessLogic_v3.Classes.DB.IBaseDbObject)
                {
                    BusinessLogic_v3.Classes.DB.IBaseDbObject dbObj = (BusinessLogic_v3.Classes.DB.IBaseDbObject)o;
                    value = dbObj.ID;
                }
                this.FormField.Functionality.Field.Value = value;
            }
        }

        private object getFormValueConvertedToPropertyDataType()
        {
            object result = null;
            object formValue = this.FormField.Functionality.FieldData.GetFormValueAsObject();
            
            result = this.PropertyInfo.GetFormValueConvertedToPropertyDataType(formValue, EnumsCms.SECTION_TYPE.SearchInListing);

            

            return result;
        }
      
        //private object getFormValueConvertedToPropertyDataType()
        //{
        //    object o = this.FormField.Functionality.FieldData.GetFormValueAsObject();
        //    object result = null;
        //    switch (this.PropertyInfo.DataType)
        //    {
        //        case Enums.CMS_DATA_TYPE.Bool:
        //        case Enums.CMS_DATA_TYPE.BoolNullable:
        //            {
        //                result = CS.General_v3.Util.Other.StrToBoolNullable((string)o);
        //                break;
        //            }
        //        case Enums.CMS_DATA_TYPE.Integer:
        //        case Enums.CMS_DATA_TYPE.IntegerNullable:
        //            {
        //                result = o;
        //                break;
        //            }
        //        case Enums.CMS_DATA_TYPE.Enumeration:
        //            {
        //                string s = (o != null ? o.ToString() : null);
        //                // result = null;
        //                int num = 0;
        //                if (int.TryParse(s, out num))
        //                    result = num;
        //                break;
        //            }
        //        case Enums.CMS_DATA_TYPE.LinkedObject:
        //            { //this means a linked object
        //                string s = (o != null ? o.ToString() : null);
        //               // result = null;
        //                int num = 0;
        //                if (!int.TryParse(s, out num))
        //                    num = 0;
        //                if (num > 0)
        //                {
        //                    var factory =  this.PropertyInfo.GetFactoryForPropertyType();
        //                    var item = factory.GetCmsItemWithId(num);
        //                    result = item;
        //                }
        //                break;
        //               // break;
        //            }

        //        case Enums.CMS_DATA_TYPE.Long:
        //        case Enums.CMS_DATA_TYPE.LongNullable:
        //            {
        //                result = o;
        //                break;
        //            }
        //        case Enums.CMS_DATA_TYPE.Double:
        //        case Enums.CMS_DATA_TYPE.DoubleNullable:
        //            {
        //                result = o;
        //                break;
        //            }
        //        case Enums.CMS_DATA_TYPE.String:
        //            {
        //                result = o;
        //                break;
        //            }
        //        case Enums.CMS_DATA_TYPE.DateTime:
        //        case Enums.CMS_DATA_TYPE.DateTimeNullable:
        //            {
        //                result = o;
        //                break;
        //            }
        //        default:
        //            throw new InvalidOperationException("Invalid data type");
        //    }
        //    return result;
        //}

        public void UpdateValueFromForm(URLClass url)
        {
            var o = getFormValueConvertedToPropertyDataType();
            string s = null;
            if (o is BusinessLogic_v3.Classes.DB.IBaseDbObject)
            {
                BusinessLogic_v3.Classes.DB.IBaseDbObject item = (BusinessLogic_v3.Classes.DB.IBaseDbObject)o;
                s = item.ID.ToString();
            }
            else
            {
                s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(o);
            }
            updateUrl(url, s);
            
        }


        public CmsFieldBase PropertyInfo { get; set; }
        public FormFieldsRowItemField FormField { get; set; }

        private string getQueryStringKey()
        {
            string key =  "Search_"+ this.PropertyInfo.GetPropertyIdentifier();
            return key;

        }

        private void updateUrl(URLClass url, string value)
        {
            string key = getQueryStringKey();
            if (value == null)
                value = "";
            url[key] = value;
        }

        //private void saveInSession(object value)
        //{
        //    string key = this.PropertyInfo.GetPropertyIdentifier();
        //    CS.General_v3.Util.PageUtil.SetSessionObject("cmsSearch_" + key, value);

        //}
       /* private object loadSearchValueFromSession()
        {
            string key = this.PropertyInfo.GetPropertyIdentifier();
            return CS.General_v3.Util.PageUtil.GetSessionObject("cmsSearch_" + key);
            
        }*/


        public bool HasSearchValue()
        {
            string s = GetSearchValue(returnDefaultIfEmpty:false);
            return !string.IsNullOrEmpty(s);

        }

        /// <summary>
        /// Returns the search value
        /// </summary>
        /// <param name="returnDefaultIfEmpty">Whether to return the default search value, if empty</param>
        /// <returns></returns>
        public string GetSearchValue(bool returnDefaultIfEmpty = true)
        {
            string key = getQueryStringKey();
            string s = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(key);

            if (returnDefaultIfEmpty && s== null && this.PropertyInfo.DefaultSearchValueStr != null)
                s = this.PropertyInfo.DefaultSearchValueStr;
            //object o =   loadSearchValueFromSession();
            return s;
        }


        public void ClearSearchValue(URLClass url)
        {
            string key = getQueryStringKey();
            url.Remove(key);
            // saveInSession(null);
        }

        public void AddCriterionToSearchCriteria(ICriteria crit)
        {
            string searchValue = this.GetSearchValue();
            if (!string.IsNullOrWhiteSpace(searchValue ))
            {
                this.PropertyInfo.AddCriterionToSearchCriteria(crit, searchValue);
            }


        }
        public override string ToString()
        {
            string s = "";
            if (this.PropertyInfo != null)
                s += this.PropertyInfo.ToString() + ": ";
            s += this.GetSearchValue();
            return s;
        }
    }
}