﻿using System;
using System.Collections.Generic;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CmsUserModule;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Extensions
{
    public static class ICmsItemFactoryExtensions
    {
        
        public static bool AllowCurrentUserToAdd(this ICmsItemFactory factory)
        {
            return _AllowCurrentUserBasedOnAccess(factory, factory.UserSpecificGeneralInfoInContext.GetAccessRequiredToAdd());
        }
        private static bool _AllowCurrentUserBasedOnAccess(this ICmsItemFactory factory, CmsAccessType accessType)
        {
            var currentUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            bool ok = false;
            if (currentUser != null)
            {
                ok = currentUser.CheckAccess(accessType);
            }
            return ok;
        }
    }
}
