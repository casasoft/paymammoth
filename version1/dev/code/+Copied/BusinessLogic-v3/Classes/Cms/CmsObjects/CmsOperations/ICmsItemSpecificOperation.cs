﻿using System;
namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public interface ICmsItemSpecificOperation
    {
        BusinessLogic_v3.Classes.Cms.CmsObjects.ICmsItemInstance Item { get; }
    }
}
