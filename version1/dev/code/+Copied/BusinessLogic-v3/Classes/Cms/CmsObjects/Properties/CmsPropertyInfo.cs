﻿using System.Reflection;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using CS.General_v3.Classes.HelperClasses;
using System;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Properties
{
    public class CmsPropertyInfo  : CmsPropertyGeneral
    {


        public CmsPropertyInfo(ICmsItemInstance cmsItem, PropertyInfo pInfo)
            : base(cmsItem)
        {
            System.Diagnostics.Contracts.Contract.Requires(pInfo != null);
            this.PropertyInfo = pInfo;
            this.SortableInListing = true;
            
            initDefaultInfoFromProperty();

        }




        
            
        public PropertyInfo PropertyInfo { get; set; }
        public override bool IsRequired
        {
            get
            {
                bool r = base.IsRequired;
                if (!r)
                {
                    if (this.PropertyInfo != null)
                    {
                        var pType = this.PropertyInfo.PropertyType;

                        if (pType.IsPrimitive &&
                            (pType != typeof(string) && pType != typeof(bool)))
                        {
                            r = true;
                        }
                    }

                }
                return r;
                
            }
            set
            {
                base.IsRequired = value;
            }
        }
       



        
        public override OperationResult SetValueForObject(ICmsItemInstance o, object value)
        {
            //if block added by Franco 20110920 - System checks (and converts) toInt32 before Setting the Value.
            //Mark knows better why this happened in the first place.
            if (value != null)
            {
                if (value.GetType().ToString() == "System.Int64")
                {
                    value = Convert.ToInt32(value);
                }
            }
            this.PropertyInfo.SetValue(o.DbItem, value, null);
            OperationResult result = new OperationResult();
            return result;
        }

        public override TDataType GetValueForObject<TDataType>(ICmsItemInstance o)
        {
            return (TDataType)this.PropertyInfo.GetValue(o.DbItem, null);
        }
        /// <summary>
        /// This will only work if the type is registered as a factory
        /// </summary>
        /// <returns></returns>
        public override ICmsItemFactory GetFactoryForLinkedObject()
        {
            return CmsSystemBase.Instance.GetFactoryForType(this.PropertyInfo.PropertyType);
        }


        private void initDefaultInfoFromProperty()
        {
            this.Label = CS.General_v3.Util.Text.AddSpacesToCamelCasedText( this.PropertyInfo.Name);
            this.DataType = EnumsCms.GetDataTypeFromType(this.PropertyInfo.PropertyType);

            this.propertyType = this.PropertyInfo.PropertyType;
            // this.EditableInListing = true;

            if (this.PropertyInfo.Name.StartsWith("__"))
            {
                this.ShowInEdit = false;
                this.ShowInListing = false;
            }
            
            this.IsRequired = (this.DataType == EnumsCms.CMS_DATA_TYPE.Integer || this.DataType == EnumsCms.CMS_DATA_TYPE.DateTime || this.DataType == EnumsCms.CMS_DATA_TYPE.Long);

            if (this.DataType == EnumsCms.CMS_DATA_TYPE.LinkedObject)
            {
                this.ShowInEdit = false;
                this.IsSearchable = false;
                if (!this.PropertyInfo.Name.ToLower().Contains("_CultureInfos".ToLower()))
                {
                   // this.LinkedWithInCms = true;
                }
            }
            
            if (this.PropertyInfo.Name.ToLower().Contains("html"))
                this.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
       
            
        }

        /// <summary>
        /// Delegate that returns the list items for the property in question.  Please note that item CAN BE NULL
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public delegate ListItemCollection CustomListItemCollectionDelegate(CmsPropertyInfo propertyInfo, ICmsItemInstance item, EnumsCms.SECTION_TYPE sectionType);
        public event CustomListItemCollectionDelegate CustomListItemCollectionRetriever;

    
      
      

        protected void addCriteriaForSearch(NHibernate.ICriteria criteria,PropertyInfo p, object value)
        {
            ICriterion crit = null;
            if (value != null)
                crit = Util.CmsUtil.GetNHibernateCriterion(p, value);
            if (crit != null)
                criteria.Add(crit);
        }

        public override void AddCriterionToSearchCriteria(NHibernate.ICriteria criteria, string searchValue)
        {
            NHibernate.Criterion.ICriterion crit = null;
            object nhValue = null;
            if (this.DataType == EnumsCms.CMS_DATA_TYPE.LinkedObject)
            {
                throw new InvalidOperationException("This should be handled by overriden method in 'CmsLinkedPropertyInfo'");

            }
            else
            {
                nhValue = CS.General_v3.Util.Other.ConvertStringToBasicDataType(searchValue, this.propertyType);
                addCriteriaForSearch(criteria, this.PropertyInfo, nhValue);
            }
            
            
        }
      




        public override void ApplyOrderByToCriteria(NHibernate.ICriteria crit, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            crit.AddOrder(new NHibernate.Criterion.Order(this.PropertyInfo.Name, (sortType == CS.General_v3.Enums.SORT_TYPE.Ascending)));

            
        }

       
    }
}