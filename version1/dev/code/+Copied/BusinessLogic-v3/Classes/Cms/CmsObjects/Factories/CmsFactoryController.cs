﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Factories
{
    public sealed class CmsFactoryController : CS.General_v3.Classes.Factories.FactoryController<ICmsItemFactory>
    {
        public static CmsFactoryController Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<CmsFactoryController>(); }
        }
        private CmsFactoryController()
        {

        }
    }
}
