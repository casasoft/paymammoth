﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Modules.CmsUserModule;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public class MainMenuItem
    {
        public MainMenuItem Parent { get; private set; }
        public int Priority { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
        public BusinessLogic_v3.Classes.Cms.EnumsCms.IMAGE_ICON ImageIcon { get; set; }
        public CmsAccessType AccessTypeRequired { get; set; }
        public List<MainMenuItem> Children { get; private set; }


        public void SortChildrenByPriorityAndTitle(bool sortChildrenAsWell)
        {
            var sortedList = Util.CmsUtil.SortMenuItemsByPriorityAndTitle(this.Children);
            this.Children = new List<MainMenuItem>();
            this.Children.AddRange(sortedList);
            if (sortChildrenAsWell)
            {
                foreach (var child in this.Children)
                {
                    child.SortChildrenByPriorityAndTitle(true);
                }
            }
        }

        



        public MainMenuItem(string Title, string URL, BusinessLogic_v3.Classes.Cms.EnumsCms.IMAGE_ICON ImageIcon, CmsAccessType accessTypeRequired, int priority =0)
        {
            if (accessTypeRequired == null) accessTypeRequired = new CmsAccessType(null);
            this.Title = Title;
            this.URL = URL;
            this.ImageIcon = ImageIcon;
            this.AccessTypeRequired = accessTypeRequired;
            this.Children = new List<MainMenuItem>();
            this.Priority = priority;
        }
        public MainMenuItem(string Title, string URL, BusinessLogic_v3.Classes.Cms.EnumsCms.IMAGE_ICON ImageIcon)
            :this(Title,URL,ImageIcon, null)
        {
            
        }

        public void AddChild(MainMenuItem item)
        {
            item.Parent = this;
            this.Children.Add(item);
        }


        public void CheckMenuItemsForAccess()
        {
            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();

            for (int i = 0; i < this.Children.Count; i++)
            {
                var child = this.Children[i];
                if (loggedUser == null || (loggedUser != null && !loggedUser.CheckAccess(child.AccessTypeRequired)))
                {
                    this.Children.RemoveAt(i);
                    i--;
                }
                else
                {
                    child.CheckMenuItemsForAccess();
                }
            }
        }
        public override string ToString()
        {
            if (this.Parent != null)
                return this.Parent.Title + " > " + this.Title;
            else
                return this.Title;
            
        }

    }
}
