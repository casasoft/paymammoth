﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.CmsObjects
{
    public class CmsItemSpecificOperation : CmsGeneralOperation
    {
        

        public CmsItemSpecificOperation(ICmsItemInstance item, string title, EnumsCms.IMAGE_ICON icon, string href) : base(title,icon,href)
        {
            this.Item = item;
        }

        public CmsItemSpecificOperation(ICmsItemInstance item, string title, EnumsCms.IMAGE_ICON icon, NullHandler delegateToCall)
            : base(title, icon, delegateToCall)
        {
            this.Item = item;

        }



        public ICmsItemInstance Item { get; private set; }


    }
}
