﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Modules.CmsUserModule;


namespace BusinessLogic_v3.Classes.Cms.CmsObjects.Access
{
    public class CmsAccessType
    {
        private CmsUserBase _m_loggedUser = null;
        private CmsUserBase getLoggedCmsUser()
        {
            if (_m_loggedUser == null)
                _m_loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            return _m_loggedUser;
        }
        /// <summary>
        /// Checks the current logged user whether he has access to this particular access type
        /// </summary>
        /// <returns></returns>
        public bool CheckIfCurrentLoggedInCmsUserHasEnoughAccess()
        {
            
            bool ok = false;
            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            return CheckIfUserHasEnoughAccess(loggedUser);
        }
        /// <summary>
        /// Checks whether the given user has access to this particular access type
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool CheckIfUserHasEnoughAccess(ICmsUserBase user)
        {
            bool ok = false;
            
            if (user!= null)
            {
                ok = user.CheckAccess(this);
            }
            return ok;
        }

        public delegate bool CustomAccessDelegate(ICmsUserBase loggedUser);
        /// <summary>
        /// This allows you to attach futher custom access functionality
        /// </summary>
        public event CustomAccessDelegate CustomAccess;
        //public bool CheckForCustomAccess(ICmsUserBase user)
        //{
        //    bool ok = true;
        //    if (CustomAccess != null)
        //        ok = CustomAccess(user);
        //    return ok;
        //}

        public static CmsAccessType GetMostRestrictiveAccessType(IEnumerable<CmsAccessType> cmsAccessTypes)
        {
            List<CmsAccessType> listAccessTypes = new List<CmsAccessType>(); ;
            if (cmsAccessTypes != null)
            {
                listAccessTypes = cmsAccessTypes.ToList();
            }
            CmsAccessType newAccessType = null;
            if (listAccessTypes.Count > 0)
            {
                newAccessType = new CmsAccessType(listAccessTypes[0].CmsItem); //take the first cms item in this case
                //newAccessType.AccessLevelRequiredToOverrideRoles = CS.General_20101215.Enums.CMS_ACCESS_TYPE.Everyone;
                //newAccessType.MinimumAccessTypeRequired = CS.General_20101215.Enums.CMS_ACCESS_TYPE.Everyone;
                newAccessType.AccessRolesRequired.Clear();
                foreach (var item in cmsAccessTypes)
                {
                    if (item.MinimumAccessTypeRequired.HasValue &&
                        (!newAccessType.MinimumAccessTypeRequired.HasValue || (int)item.MinimumAccessTypeRequired.Value > (int)newAccessType.MinimumAccessTypeRequired.Value))
                    {
                        newAccessType.MinimumAccessTypeRequired = item.MinimumAccessTypeRequired;
                    }
                    newAccessType.AccessRolesRequired.AddRange(item.AccessRolesRequired);
                    if (item.AccessLevelRequiredToOverrideRoles.HasValue &&
                        (!newAccessType.AccessLevelRequiredToOverrideRoles.HasValue || (int)item.AccessLevelRequiredToOverrideRoles.Value > (int)newAccessType.AccessLevelRequiredToOverrideRoles.Value))
                    {
                        newAccessType.AccessLevelRequiredToOverrideRoles = item.AccessLevelRequiredToOverrideRoles;
                    }
                    newAccessType.SpecificCmsUsersAllowedAccess.AddAll(item.SpecificCmsUsersAllowedAccess);
                }

            }
            else
            {
                newAccessType = null;

            }
            return newAccessType;

        }


        


        /// <summary>
        /// This should allow the currently logged in user only if:
        /// - He has override access to the most restrictive override access
        /// - he has access to the minimum available AND (if specific users are filled in, he must be one of them);
        ///     
        /// </summary>
        /// <param name="result"></param>
        private void checkSpecificUsersForAccess(OperationResult result)
        {

            bool ok = false;
            if (SpecificCmsUsersAllowedAccess.Count > 0 && this.CmsItem != null)
            {

                var accessToOverrideRoles = this.GetAccessLevelRequiredToOverrideRoles();
                var minimumAccessRequired = this.GetMinimumAccessTypeRequired();
                var loggedUser = getLoggedCmsUser();

                if (loggedUser != null)
                {

                    if ((loggedUser.CheckAccessTypeAtLeast(accessToOverrideRoles)))
                    {
                        ok = true;
                    }
                    else if (loggedUser.CheckAccessTypeAtLeast(minimumAccessRequired))
                    {
                        if (this.SpecificCmsUsersAllowedAccess.Contains(loggedUser))
                        {
                            ok = true;
                        }
                    }

                }
            }
            else
            { //if no specific users are required, always return true
                ok = true;
            }
            if (!ok)
                result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "User does not have access for this section of the back-office");

        }

        ///// <summary>
        ///// This checks for any custom access required for this item
        ///// </summary>
        ///// <returns></returns>
        //private OperationResult checkCustomAccess()
        //{
        //    OperationResult result = new OperationResult();

        //    checkSpecificUsersForAccess(result);

        //    if (result.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success && this.CmsItem != null)
        //    {
        //        this.CmsItem.CheckCustomAccess(result);
        //    }
        //    return result;
        //}
        /// <summary>
        /// This updates the access type to 'NoOne', if the user is not allowed based on custom access.  This is only called if his access type is less
        /// than the level required for override
        /// </summary>
        /// <param name="accessType"></param>
        /// <returns></returns>
        private CS.General_v3.Enums.CMS_ACCESS_TYPE updateAccessTypeBasedOnCustomAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE accessType)
        {
            CS.General_v3.Enums.CMS_ACCESS_TYPE result = accessType;
            var loggedUser = getLoggedCmsUser();
            if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(this.GetAccessLevelRequiredToOverrideRoles()))
            {
                //ok, this is allowed
            }
            else
            {
                var opResult = CheckCustomAccess();
                if (opResult.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
                {
                    result = CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne;
                }
            }
            return result;
        }

        public static CmsAccessType GetMostRestrictiveAccessType(params CmsAccessType[] cmsAccessTypes)
        {
            return GetMostRestrictiveAccessType((IEnumerable<CmsAccessType>)cmsAccessTypes);
        }
        public CS.General_v3.Enums.CMS_ACCESS_TYPE GetMinimumAccessTypeRequired()
        {
            CS.General_v3.Enums.CMS_ACCESS_TYPE accessType = CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal;
            if (MinimumAccessTypeRequired.HasValue)
                accessType = this.MinimumAccessTypeRequired.Value;
            else
                accessType = CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal;
            //accessType = updateAccessTypeBasedOnCustomAccess(accessType);
            return accessType;

        }
        /// <summary>
        /// The minimum access type required for this type of access.  User must also meet the 'access roles' and 'specific users' criteria, if he is to check if he meets the minimum access.
        /// </summary>
        public CS.General_v3.Enums.CMS_ACCESS_TYPE? MinimumAccessTypeRequired { get; set; }
        /// <summary>
        /// The access roles required in order to access this item, if the user only meets the minimum access type required
        /// </summary>
        public CmsAccessRoleList AccessRolesRequired { get; private set; }
        public CS.General_v3.Enums.CMS_ACCESS_TYPE GetAccessLevelRequiredToOverrideRoles()
        {
            if (AccessLevelRequiredToOverrideRoles.HasValue)
                return AccessLevelRequiredToOverrideRoles.Value;
            else
                return CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator;


        }
        /// <summary>
        /// If a user meets this access level, this means that he does not require roles, or be a specific user.
        /// </summary>
        public CS.General_v3.Enums.CMS_ACCESS_TYPE? AccessLevelRequiredToOverrideRoles { get; set; }

        public ICmsItemInstance CmsItem { get; private set; }

        /// <summary>
        /// Creates a custom access type
        /// </summary>
        /// <param name="linkedCmsItem">Linked cms item.  Can be left null, if not available</param>
        /// <param name="minimumAccessRequired"></param>
        public CmsAccessType(ICmsItemInstance linkedCmsItem, CS.General_v3.Enums.CMS_ACCESS_TYPE? minimumAccessRequired = null)
        {
            this.SpecificCmsUsersAllowedAccess = new HashedSet<CmsUserBase>();
            this.CmsItem = linkedCmsItem;
            this.AccessRolesRequired = new CmsAccessRoleList();
            //this.AccessLevelRequiredToOverrideRoles = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.MinimumAccessTypeRequired = minimumAccessRequired;
        }

        public void AddAccessRole(ICmsAccessRole accessRole)
        {
            this.AccessRolesRequired.Add(accessRole);
        }
        public void AddAccessRole(Enum enumValue)
        {
            if (enumValue != null)
            {
                string s = CS.General_v3.Util.EnumUtils.StringValueOf(enumValue);
                this.AddAccessRole(s);
            }
        }
        public void AddAccessRole(string identifier)
        {
            CmsAccessRole role = new CmsAccessRole(identifier);
            this.AddAccessRole(role);
        }
        public override string ToString()
        {
            string s = "Min: " + this.MinimumAccessTypeRequired + ", Roles: " + CS.General_v3.Util.ListUtil.ConvertListToString(this.AccessRolesRequired, ",") + ". Override: " + this.AccessLevelRequiredToOverrideRoles;
            return s;
            
        }

        public void SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE minimumAccessRequired)
        {
            SetAccess(minimumAccessRequired, minimumAccessRequired);
        }

        public void SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE minimumAccessRequired, CS.General_v3.Enums.CMS_ACCESS_TYPE accessToOverrideRoles,
            params Enum[] accessRoles)
        {
            this.AccessRolesRequired.Clear();
            this.MinimumAccessTypeRequired= minimumAccessRequired;
            this.AccessLevelRequiredToOverrideRoles = accessToOverrideRoles;
            if (accessRoles != null)
            {
                foreach (var role in accessRoles)
                {
                    if (role != null)
                    {
                        AddAccessRole(role);
                    }
                }
            }
        }

        public CmsAccessType Clone()
        {
            var accessType = CS.General_v3.Util.Other.CloneDeep(this);
            accessType.CmsItem = this.CmsItem;
            accessType.SpecificCmsUsersAllowedAccess.AddAll(this.SpecificCmsUsersAllowedAccess);
            if (this.CustomAccess != null)
            {
                accessType.CustomAccess += this.CustomAccess;
            }
            return accessType;

        }
        /// <summary>
        /// This checks for any custom access required for this item
        /// </summary>
        /// <returns></returns>
        public OperationResult CheckCustomAccess()
        {
            OperationResult result = new OperationResult();

            checkSpecificUsersForAccess(result);

            if (result.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success && this.CmsItem != null)
            {
                this.CmsItem.CheckCustomAccess(result);
            }
            if (result.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success && CustomAccess != null)
            {
                var loggedUser = getLoggedCmsUser();
                bool ok = CustomAccess(loggedUser);
                if (!ok)
                {
                    result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "User does not have access to this item");
                }
            }
            return result;
        }

        public HashedSet<CmsUserBase> SpecificCmsUsersAllowedAccess { get; private set; }

        public void CopyMostRestrictiveAccess(CmsAccessType access)
        {
            if (this.MinimumAccessTypeRequired.HasValue && access.MinimumAccessTypeRequired.HasValue && (int)access.MinimumAccessTypeRequired.Value > (int)this.MinimumAccessTypeRequired.Value)
            {
                this.MinimumAccessTypeRequired = access.MinimumAccessTypeRequired;
            }
            if (this.AccessLevelRequiredToOverrideRoles.HasValue && access.AccessLevelRequiredToOverrideRoles.HasValue &&
                (int)access.AccessLevelRequiredToOverrideRoles.Value > (int)this.AccessLevelRequiredToOverrideRoles.Value)
            {
                this.AccessLevelRequiredToOverrideRoles = access.AccessLevelRequiredToOverrideRoles;
            }
            this.AccessRolesRequired.AddRange(access.AccessRolesRequired);
            this.SpecificCmsUsersAllowedAccess.AddAll(access.SpecificCmsUsersAllowedAccess);
        }

    }
}
