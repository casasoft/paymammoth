﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessLogic_v3.Classes.Cms
{
    public static class CmsConstants
    {
        public const string QUERYSTRING_LASTADDED = "LastAddedItem";
        public const string ROUTING_PARAM_TITLE = "Title";
        public const string ROUTING_PARAM_ID = "ID";
        
        
    }
}