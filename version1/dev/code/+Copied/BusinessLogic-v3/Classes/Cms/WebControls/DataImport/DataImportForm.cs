﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Specialized;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Classes.DataImport;
using System.IO;
using CS.General_v3.Util.Encoding.CSV;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace BusinessLogic_v3.Classes.Cms.WebControls.DataImport
{
    public class DataImportForm : Panel
    {
        public delegate ImportResults FormSubmittedEvent(CSVFile csvFileContents, Stream ZIPFile);
        public DataImportForm()
        {
            this.CssClass = "dataimportform";
            this.Functionality = new FUNCTIONALITY(this);
        }
        public class FUNCTIONALITY
        {
            private DataImportForm _form = null;
            public FUNCTIONALITY(DataImportForm form)
            {
                this._form = form;
                this.ShowZIPFile = true;
            }
            internal ImportResults callFormSubmitted(CSVFile csvFileContents, Stream ZIPFile)
            {
                return FormSubmitted(csvFileContents, ZIPFile);
            }
            public event FormSubmittedEvent FormSubmitted;
            public bool ShowZIPFile { get; set; }

        }

        public FUNCTIONALITY Functionality { get; set; }

        public FormFields Form { get; set; }
        public MyHeading hResults { get; set; }
        public MyUnorderedList ulMsgs { get; set; }
        public ResultsTable ResultsTable { get; set; }

        private void initControls()
        {
            this.Form = new FormFields(this.ID + "_dataImport"); this.Controls.Add(Form);
            hResults = new MyHeading(2, "Results"); this.Controls.Add(hResults);
            ulMsgs = new MyUnorderedList(); this.Controls.Add(ulMsgs); ulMsgs.CssClass = "dataimportform-msgs";
            ResultsTable = new ResultsTable(); this.Controls.Add(ResultsTable); ResultsTable.CssManager.AddClass("dataimportform-results");
            ulMsgs.Visible = false;
            ResultsTable.Visible = false;
            hResults.Visible = false;

        }
        private void initForm()
        {
            this.Form.AddFileUpload("txtCSV", "Excel File", true, "Excel File to upload with information", 400);
            if (this.Functionality.ShowZIPFile)
            {
                this.Form.AddFileUpload("txtZIPFileUpload", "ZIP File (Upload)", false, "ZIP File to upload with images", 400);
                this.Form.AddString("txtZIPPath", "Zip File (Path)", false, "Path of ZIP file on server to upload", 400, null);
            }
            this.Form.ButtonSubmitText = "Import";
            this.Form.ClickSubmit += new EventHandler(Form_ClickSubmit);
        }


        private void addErrorMsgToMsgs(string msg)
        {
            hResults.Visible = true;
            ulMsgs.Visible = true;
            ListItem item = new ListItem(msg);
            item.Attributes["class"] = "error";
            MyListItem li = new MyListItem();
            li.Controls.Add(new Literal() { Text = msg });
            ulMsgs.Controls.Add(li);
        }
        private void addWarningMsgToMsgs(string msg)
        {
            hResults.Visible = true;
            ulMsgs.Visible = true;
            ListItem item = new ListItem(msg);
            item.Attributes["class"] = "warning";
            MyListItem li = new MyListItem();
            li.Controls.Add(new Literal() { Text = msg });
            ulMsgs.Controls.Add(li);
        }
        private void addSuccessMsgToMsgs(string msg)
        {
            hResults.Visible = true;
            ulMsgs.Visible = true;
            ListItem item = new ListItem(msg);
            item.Attributes["class"] = "success";
            MyListItem li = new MyListItem();
            li.Controls.Add(new Literal() { Text = msg });
            ulMsgs.Controls.Add(li);
        }
        void Form_ClickSubmit(object sender, EventArgs e)
        {
            bool ok = true;


            var txtCSV = this.Form.GetControlAsMyFileUpload("txtCSV");

            MyFileUpload txtZIPFileUpload = null;
            string zipPath = null;
            string zipFilename = "";
            if (this.Functionality.ShowZIPFile)
            {
                txtZIPFileUpload = this.Form.GetControlAsMyFileUpload("txtZIPFileUpload");
                zipPath = this.Form.GetFormValueStr("txtZIPPath");
            }
            string csvFilename = txtCSV.FileName;
            Stream zipFile = null;
          //  string csvFileContents = "";
            if (txtCSV.FileContent.Length > 0)
            {
              //  csvFileContents = CS.General_v3.Util.IO.GetStringFromStream(txtCSV.FileContent);

                if (txtZIPFileUpload != null && txtZIPFileUpload.FileContent.Length > 0)
                {
                    zipFile = txtZIPFileUpload.FileContent;
                    zipFilename = txtZIPFileUpload.FileName;
                }
                else if (!string.IsNullOrEmpty(zipPath))
                {
                    string localPath = CS.General_v3.Util.PageUtil.MapPath(zipPath);
                    if (File.Exists(localPath))
                    {
                        zipFile = new MemoryStream(CS.General_v3.Util.IO.LoadFileAsByteArray(localPath));
                        zipFilename = zipPath;
                    }
                    else
                    {
                        ok = false;
                        addErrorMsgToMsgs("ZIP File does not exist at path <" + zipPath + ">");
                    }

                }

            }
            else
            {
                ok = false;
                addErrorMsgToMsgs("Invalid CSV File");
            }
            hResults.InnerHtml = CS.General_v3.Util.Text.HtmlEncode("Results - Excel: " + csvFilename + " | ZIP: " + zipFilename);
            if (ok)
            {
                try
                {
                    CSVFile csvFile = CSVFile.LoadFromStream(txtCSV.FileContent, txtCSV.FileName);

                    ImportResults results = this.Functionality.callFormSubmitted(csvFile, zipFile);
                    showResults(results);
                }
                catch (Exception ex)
                {
                    Cms.Util.CmsUtil.ShowErrorMessageInCMS("Error occurred while trying to parse data file", ex);
                }
            }


        }
        private void showResults(ImportResults results)
        {
            ResultsTable.Visible = true;
            if (results.Success)
            {
                addSuccessMsgToMsgs("Data imported successfully");
                ResultsTable.FillFromImportResults(results);

            }
            else
            {
                if (results.ErrorMessages.Count > 0)
                {
                    foreach (var errMsg in results.ErrorMessages)
                    {
                        addErrorMsgToMsgs(errMsg);
                    }

                }
                else
                {
                    addErrorMsgToMsgs("Error importing data");
                }
            }
        }
        protected override void OnInit(EventArgs e)
        {
            initControls();
            base.OnInit(e);

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            initForm();
        }
    }
}
