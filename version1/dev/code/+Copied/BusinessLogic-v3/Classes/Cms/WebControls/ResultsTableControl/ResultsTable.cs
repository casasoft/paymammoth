﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Classes.DataImport;

namespace BusinessLogic_v3.Classes.Cms.WebControls.ResultsTableControl
{
    public class ResultsTable : MyTable
    {
        public ResultsTable()
        {
            this.CssClassHeadingRow = "results-heading";
            this.CssClassHeadingCell = "heading";
            this.CssClassResultsLine = "results-line";
            this.CssClassResultsCell = "results-cell";
            this.CssClass = "results";
        }
        public string CssClassHeadingRow { get; set; }
        public string CssClassResultsLine { get; set; }
        public string CssClassResultsCell { get; set; }
        public string CssClassHeadingCell { get; set; }

        public List<string> ColumnTitles { get; set; }

        private void addHeader()
        {
            var header = this.AddRow(this.CssClassHeadingRow);
            header.AddCell(this.CssClassHeadingCell, "Line #");
            for (int i = 0; i < ColumnTitles.Count; i++)
            {
                var col = ColumnTitles[i];
                header.AddCell(this.CssClassHeadingCell, CS.General_v3.Util.Text.HtmlEncode(col));

            }
        }
        private void addLine(ResultLine line, bool alternating)
        {
            var row = this.AddRow(CssClassResultsLine);
            if (alternating)
                row.CssManager.AddClass("alt");
            switch (line.ResultType)
            {
                case ResultLine.RESULT_TYPE.OK: row.CssManager.AddClass("result-ok"); break;
                case ResultLine.RESULT_TYPE.Warning: row.CssManager.AddClass("result-warning"); break;
                case ResultLine.RESULT_TYPE.Error: row.CssManager.AddClass("result-error"); break;
            }
            row.AddCell(CssClassResultsCell, CS.General_v3.Util.Text.HtmlEncode(line.LineIndex.ToString()));
            for (int i = 0; i < line.ColumnValues.Count; i++)
            {
                var val = line.ColumnValues[i] ?? "";

                row.AddCell(CssClassResultsCell, CS.General_v3.Util.Text.TxtForHTML(CS.General_v3.Util.Text.HtmlEncode(val)));
            }

        }
        public void FillFromImportResults(IEnumerable<ResultLine> results)
        {
            addHeader();
            bool alt = false;
            foreach (var line in results)
            {
                addLine(line, alt);
                alt = !alt;
            }


        }
    }
}
