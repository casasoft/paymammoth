﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util.Encoding.CSV;

namespace BusinessLogic_v3.Classes.Cms.WebControls.DataImport_v2
{
    public class ColumnInfo_v2
    {
        public int ColumnIndex { get; set; }
        public string Title { get; set; }
        public bool Required { get; set; }
        public bool WarningOnEmpty { get; set; }
        public bool WarningOnInvalidDataType { get; set; }
        public CS.General_v3.Enums.DATA_TYPE DataType { get; set; }

        public ColumnInfo_v2(string Title, bool required = true, bool warningOnEmpty = false, bool warningOnInvalidDataType = false, CS.General_v3.Enums.DATA_TYPE dataType = CS.General_v3.Enums.DATA_TYPE.String)
        {
           this.Title = Title;
            this.Required =required;
            this.WarningOnInvalidDataType = warningOnInvalidDataType;
            this.DataType = dataType;
            this.WarningOnEmpty = warningOnEmpty;

        }

        public bool ParseString(string s, ImportResultLine_v2 lineResult)
        {
            bool ok = true;
            if (ok && string.IsNullOrWhiteSpace(s))
            {
                CS.General_v3.Enums.STATUS_MSG_TYPE msgType = (!Required ? CS.General_v3.Enums.STATUS_MSG_TYPE.Warning : CS.General_v3.Enums.STATUS_MSG_TYPE.Error);

                if (Required)
                {
                    ok = false;
                    lineResult.StatusMsgs.Add(new StatusMsg( CS.General_v3.Enums.STATUS_MSG_TYPE.Error,  "Column <" + Title + "> is required"));
                }
                else if (WarningOnEmpty)
                {
                    lineResult.StatusMsgs.Add(new StatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Column <" + Title + "> is empty"));
                    
                }
                
            }
            {
                string dataTypeErrMsg = null;
                switch (DataType)
                {
                    case CS.General_v3.Enums.DATA_TYPE.Double:
                    case CS.General_v3.Enums.DATA_TYPE.DoubleNullable:
                    case CS.General_v3.Enums.DATA_TYPE.Integer:
                    case CS.General_v3.Enums.DATA_TYPE.IntegerMultipleChoice:
                    case CS.General_v3.Enums.DATA_TYPE.IntegerNullable:
                        {
                            double d = 0;
                            if (!double.TryParse(s, out d))
                            {
                                ok = false;
                                dataTypeErrMsg = "Column <" + Title + "> value of '" + s + "' could not be parsed into a number";
                            }
                        }
                        break;
                    case CS.General_v3.Enums.DATA_TYPE.Bool:
                    case CS.General_v3.Enums.DATA_TYPE.BoolNullable:
                        {
                            bool? b = CS.General_v3.Util.Other.TextToBoolNullable(s);
                            if (b == null)
                            {
                                ok = false;
                                dataTypeErrMsg = "Column <" + Title + "> value of '" + s + "' could not be parsed into Yes/No (boolean) value";
                            }
                        }
                        break;
                }
                if (!string.IsNullOrEmpty(dataTypeErrMsg))
                {
                    CS.General_v3.Enums.STATUS_MSG_TYPE msgType = (WarningOnInvalidDataType ? CS.General_v3.Enums.STATUS_MSG_TYPE.Warning : CS.General_v3.Enums.STATUS_MSG_TYPE.Error);

                    lineResult.StatusMsgs.Add(new StatusMsg(msgType,dataTypeErrMsg));
                    
                }
            }
            return ok;
        }


    }
}
