﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure;
using System.Web.UI;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.CSS;

namespace BusinessLogic_v3.Classes.Cms.WebControls
{
    [ToolboxData("<{0}:CmsTreeStructure runat=server></{0}:CmsTreeStructure>")]  
    public class CmsTreeStructure : TreeStructureClass
    {
        


        public class FunctionalityCmsTreeStructure : TreeStructureClass.FUNCTIONALITY
        {
            protected new CmsTreeStructure _tree
            {
                get
                {
                    return (CmsTreeStructure)base._tree;
                }
            }
            public new IEnumerable<ICmsHierarchyInfo> RootItems
            {
                get
                {
                    return (IEnumerable<ICmsHierarchyInfo>)base.RootItems;
                }
                set
                {
                    base.RootItems = value;
                }
            }
            public FunctionalityCmsTreeStructure(CmsTreeStructure tree) : base(tree)
            {
                var f = this;
                f.CSSFile = "/_common/css/v1/components/tree.css";
                string imageFolder = "/_common/images/components/v1/tree/";
                f.ImageURL_AddSubItems_Up = imageFolder + "add_item_up.gif";
                f.ImageURL_AddSubItems_Over = imageFolder + "add_item_over.gif";
                f.ImageURL_Delete_Over = imageFolder + "delete_over.gif";
                f.ImageURL_Delete_Up = imageFolder + "delete_up.gif";
                f.ImageURL_Edit_Over = imageFolder + "edit_over.gif";
                f.ImageURL_Edit_Up = imageFolder + "edit_up.gif";
                f.PaddingPerLevel = 30;
                f.ShowPriorityAfterName = true;
            }
            protected override bool checkWhetherToRenderItem(ITreeItemBasic parentItem, ITreeItemBasic item, int level, System.Web.UI.HtmlControls.HtmlTableRow row)
            {
                bool ok = base.checkWhetherToRenderItem(parentItem, item, level, row);
                ICmsHierarchyInfo cmsParentItem = (ICmsHierarchyInfo)parentItem;
                ICmsHierarchyInfo cmsItem = (ICmsHierarchyInfo)item;
                if (ok && !cmsItem.CheckIfStillTemporary())
                {
                    {
                        CSSManager css = new CSSManager(row);
                        if (cmsItem.Visible)
                            css.AddClass("visible");
                        else
                            css.AddClass("not-visible");
                    }
                }
                else
                {
                    ok = false;
                }
                return ok;
                
            }
        }
        public new FunctionalityCmsTreeStructure Functionality
        {
            get
            {
                return (FunctionalityCmsTreeStructure)base.Functionality;
            }
        }
        

        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.ShowMessage += new FUNCTIONALITY.ShowMessageEvent(Functionality_ShowMessage);
            this.Functionality.RenderTree();
            
            base.OnLoad(e);
        }

        void Functionality_ShowMessage(OperationResult msg)
        {
            this.Page.Master.Functionality.ShowStatusMessageAndRedirectToSamePage(msg);
        }

        public new BusinessLogic_v3.Classes.Cms.Pages.BasePage Page
        {
            get
            {
                return (BusinessLogic_v3.Classes.Cms.Pages.BasePage)base.Page;
            }
        }

        protected override CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.TreeStructureBasicClass.FUNCTIONALITY createFunctionality()
        {
            return new FunctionalityCmsTreeStructure(this);
        }
        
        
    }
}
