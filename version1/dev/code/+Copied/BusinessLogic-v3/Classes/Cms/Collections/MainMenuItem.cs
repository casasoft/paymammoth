﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.Collections
{
    public class MainMenuItem
    {
        public MainMenuItem Parent { get; private set; }

        public string Title { get; set; }
        public string URL { get; set; }
        public EnumsCms.IMAGE_ICON ImageIcon { get; set; }
        public CS.General_v3.Enums.CMS_ACCESS_TYPE AccessTypeRequired { get; set; }
        public List<MainMenuItem> Children { get; private set; }

        public MainMenuItem(string Title, string URL, EnumsCms.IMAGE_ICON ImageIcon, CS.General_v3.Enums.CMS_ACCESS_TYPE accessTypeRequired)
        {
            this.Title = Title;
            this.URL = URL;
            this.ImageIcon = ImageIcon;
            this.AccessTypeRequired = accessTypeRequired;
            this.Children = new List<MainMenuItem>();
        }
        public MainMenuItem(string Title, string URL, EnumsCms.IMAGE_ICON ImageIcon)
            : this(Title, URL, ImageIcon, CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal)
        {
            
        }

        public void AddChild(MainMenuItem item)
        {
            item.Parent = this;
            this.Children.Add(item);
        }


        public void CheckMenuItemsForAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE currAccessType)
        {
            for (int i =0; i < this.Children.Count;i++)
            {
                var child = this.Children[i];
                if (!CS.General_v3.Enums.CheckAdminUserAccess(currAccessType,child.AccessTypeRequired))
                {
                    this.Children.RemoveAt(i);
                    i--;
                }
                else
                {
                    child.CheckMenuItemsForAccess(currAccessType);
                }
            }
        }
        public override string ToString()
        {
            if (this.Parent != null)
                return this.Parent.Title + " > " + this.Title;
            else
                return this.Title;
            
        }

    }
}
