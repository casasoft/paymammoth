﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Cms.Util
{
    public static class QueryStringUtil
    {
        public const string ParamSpecialOfferID = "SpecialOfferID";
        public static long GetSpecialOfferID()
        {
            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long>(ParamSpecialOfferID);
        }
    }
}
