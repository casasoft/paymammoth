﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.EmailLogModule;
using BusinessLogic_v3.Classes.NHibernateClasses;
using log4net;

using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using CS.General_v3.Classes.Email;
using BusinessLogic_v3.Modules.SettingModule;

namespace BusinessLogic_v3.Classes.Emails
{
    public class EmailDbLogger
    {
        private static ILog _log = log4net.LogManager.GetLogger(typeof(EmailDbLogger));
        public static EmailDbLogger Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<EmailDbLogger>(); }
        }
        private EmailDbLogger()
        {
            
        }
        public void Init()
        {
            
        }



        public void LogEmailInDb(EmailMessage email)
        {
            if (!email.IsErrorNotification)
            {

                try
                {
                    bool enabled = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Email_DbLogger_Enabled);
                    if (enabled)
                    {

                        var session = NHClasses.NhManager.CreateNewSessionForContextAndReturn();
                        using (var transaction = session.BeginTransaction())
                        {


                            EmailLogBase emailLog = EmailLogBase.Factory.CreateNewItem();
                            emailLog.DateTime = CS.General_v3.Util.Date.Now;
                            if (email.From != null)
                            {
                                emailLog.FromEmail = email.From.Address;
                                emailLog.FromName = email.From.DisplayName;
                            }

                            emailLog.ToEmail = email.GetWholeRecipientEmailListCommaDelimeted();
                            emailLog.ToName = email.GetWholeRecipientNameListCommaDelimeted();
                            if (email.ReplyTo != null)
                            {
                                emailLog.ReplyToEmail = email.ReplyTo.Address;
                                emailLog.ReplyToName = email.ReplyTo.DisplayName;
                            }
                            emailLog.Subject = email.Subject;
                            emailLog.PlainText = email.BodyPlain;
                            emailLog.HtmlText = email.BodyHtml;
                            if (email.Attachments != null)
                            {
                                emailLog.Attachements = CS.General_v3.Util.Text.AppendStrings("\r\n", email.Attachments.ToList().ConvertAll(x => x.Name));
                            }
                            if (email.Resources != null)
                            {
                                emailLog.Resources = CS.General_v3.Util.Text.AppendStrings("\r\n", email.Resources.ToList().ConvertAll(x => x.ContentId));
                            }
                            emailLog.Host = email.SmtpHost;
                            emailLog.Port = email.SmtpPort.ToString();
                            emailLog.User = email.SmtpUsername;
                            string hiddenPass = "";
                            {
                                if (email.SmtpPass.Length >= 10)
                                    hiddenPass = CS.General_v3.Util.Text.HideStringData(email.SmtpPass, 2, 2);
                                else
                                    hiddenPass = CS.General_v3.Util.Text.HideStringData(email.SmtpPass, 1, 1);
                            }
                            emailLog.Pass = hiddenPass;
                            emailLog.UsesSecureConnection = email.SmtpUseSecureAuthentication.GetValueOrDefault();
                            emailLog.ResultDetails = email.LastOperationResult.GetMessageAsString();
                            emailLog.Success = (email.LastOperationResult.IsSuccessful);
                            emailLog.Create();
                            transaction.Commit();
                        }
                        NHClasses.NhManager.DisposeCurrentSessionInContext();

                    }
                }
                catch (Exception ex)
                {
                    _log.Error("Error during logging of email", ex);
                }
            }
        }

        


    }
}
