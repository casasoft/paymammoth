﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Util;
using BusinessLogic_v3.Classes.Application;

namespace BusinessLogic_v3.Classes.Emails
{
    public class EmailMsgSender : CS.General_v3.Classes.Email.EmailMsgSender
    {
        public static EmailMsgSender Instance { get { return (EmailMsgSender) CS.General_v3.Classes.Email.EmailMsgSender.Instance; } }

        
        public override CS.General_v3.Classes.HelperClasses.OperationResult SendSynchronously(CS.General_v3.Classes.Email.EmailMessage emailMsg, bool throwErrorOnException = false)
        {
            nHibernateUtil.CreateNewSessionForCurrentContext();
            var result = base.SendSynchronously(emailMsg, throwErrorOnException); 
            nHibernateUtil.DisposeCurrentSessionInContext();

            //2012-05-11: I commented this line as this event is already being created in BusinessLogic_v3.Classes.Application.AppInstance
            //inside its protected constructor. ( line 226). Franco.
            //
            //AppInstance.Instance.OnEmailSent(emailMsg);

            return result;
        }
    }
}
