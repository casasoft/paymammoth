﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using log4net;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;

using System.IO;

namespace BusinessLogic_v3.Classes.WebApplications
{
    public class KeepAlive
    {
        private readonly static ILog _log = LogManager.GetLogger(typeof(KeepAlive));

        public static KeepAlive Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<KeepAlive>(); }
        }

        public bool Enabled { get; set; }


        private Timer _tmr = null;
        private KeepAlive()
        {
            this.Enabled = true;
            _tmr = new Timer();
            _tmr.Elapsed += new ElapsedEventHandler(_tmr_Elapsed);
            this.KeepAlivePath = "/_ComponentsGeneric/Frontend/Pages/Common/Other/keepAlive.ashx";
            this.IntervalInSeconds = 8 * 60; // 8 minutes
            
        }

        public string KeepAlivePath { get; set; }
        private string getKeepAliveFullUrl()
        {
            string path = KeepAlivePath;
            
            if (path.StartsWith("/"))
            {
                path = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + path.Substring(1);
            }
            return path;
        }

        private void keepAlive()
        {
            try
            {
                var session = NHClasses.NhManager.CreateNewSessionForContextAndReturn();

                string url = getKeepAliveFullUrl();

                
                CS.General_v3.Util.Web.HTTPGet(url);
                
                NHClasses.NhManager.DisposeCurrentSessionInContext();

            }
            catch (Exception ex)
            {
                if (_log.IsWarnEnabled) _log.Warn("Error during [Keep Alive]", ex);
            }
        }

        private bool checkKeepAlivePathExists()
        {
            string url = getKeepAliveFullUrl();
            string localPath = CS.General_v3.Util.PageUtil.MapPath(this.KeepAlivePath);
            return File.Exists(localPath);
        }

        void _tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(keepAlive, "Application-KeepAlive");
        }
        public int IntervalInSeconds { get; set; }
        public void Start()
        {
            
            _tmr.Interval = IntervalInSeconds * 1000;
            _tmr.Start();
        }
        public void Stop()
        {
            _tmr.Stop();
            
        }


        public void StartIfEnabled()
        {
            if (this.Enabled)
            {
                if (checkKeepAlivePathExists())
                {
                    Start();
                }
            }

        }
    }
}
