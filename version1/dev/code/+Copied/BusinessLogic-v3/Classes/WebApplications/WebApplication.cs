﻿using System;
using BusinessLogic_v3.Classes.NHibernateClasses;

namespace BusinessLogic_v3.Classes.WebApplications
{
    public class WebApplication : CS.General_v3.Classes.WebApplication
    {
        public new static WebApplication Instance
        {
            get { return (WebApplication)CS.General_v3.Classes.WebApplication.Instance; }
        }

        public WebApplication()
        {
            this.BeginRequest += new EventHandler(WebApplication_BeginRequest);
            this.EndRequest += new EventHandler(WebApplication_EndRequest);
            
        }


        void WebApplication_BeginRequest(object sender, EventArgs e)
        {
            //SessionContextManager.Instance.CreateNewSessionForContext2();
            //  SessionContextManager3.Instance.CreateNewSessionForContext2();
            if (NHClasses.NhManager.GetSessionFactory() != null)
            {

                bool ok = (verifyExtensionOfCurrentUrlRequiresSession(System.Web.HttpContext.Current));
                //ok = true;
                if (ok)
                {

                    string url = System.Web.HttpContext.Current.Request.Url.ToString();
                    //session management has been moved to the page itself
                    //NHClasses.NhManager.CreateNewSessionForContext(); //This no longer really creates the first session, if you are in Asp.Net mode.  Instead, it will create it only on the first 'GetSession' access.
                }

            }
            //if (verifyExtensionOfCurrentUrlRequiresSession(System.Web.HttpContext.Current))
            //{
            //    if (NHClasses.NhManager.GetSessionFactory() != null)
            //    {

            //    }
            //}
            //else
            //{
            //    int k = 5;
            //}
        }

        void WebApplication_EndRequest(object sender, EventArgs e)
        {
            //SessionContextManager.Instance.DisposeCurrentSessionInContext2();
            //SessionContextManager3.Instance.DisposeCurrentSessionInContext2();
            string url = System.Web.HttpContext.Current.Request.Url.ToString();
            bool ok = (verifyExtensionOfCurrentUrlRequiresSession(System.Web.HttpContext.Current));
            //ok = true;
            if (ok)
            {

                if (NHClasses.NhManager.GetSessionFactory() != null)
                {
                    //session management has been moved to the page itself
                   // NHClasses.NhManager.DisposeCurrentSessionInContext();
                }
            }


        }
    }
}
