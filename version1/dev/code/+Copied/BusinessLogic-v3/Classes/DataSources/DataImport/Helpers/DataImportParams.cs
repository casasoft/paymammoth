﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BusinessLogic_v3.Classes.DataImport.Helpers
{
    public class DataImportParams
    {
        public int? HeaderLineIndex { get; set; }
        public Iesi.Collections.Generic.HashedSet<string> FilePathsToSearchForFiles { get; private set; }
        public int FlushSessionEveryNumOfLines { get; set; }
        public DataImportColumnCollection ColumnsCollection { get; set; }
        public int TotalLinesToSkipAfterHeader { get; set; }
        public bool RemoveAnyExistingImages { get; set; }
        public DataImportFormParams FormParams { get; set; }
        public BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase Culture { get; set; }
        public string CsvFilePath { get; set; }
        public string ZipFilePath { get; set; }
        public string UploadedCsvFilename { get; set; }
        
        public List<string> CustomFilePathsToCheckForUploads { get; set; }


        public DataImportParams()
        {
            this.FormParams = new DataImportFormParams();
            this.ColumnsCollection = new DataImportColumnCollection(); 
            FlushSessionEveryNumOfLines = 100;
            this.FilePathsToSearchForFiles = new Iesi.Collections.Generic.HashedSet<string>();
            this.CustomFilePathsToCheckForUploads = new List<string>();
            this.TotalLinesToSkipAfterHeader = 0;

        }

        

    }
}
