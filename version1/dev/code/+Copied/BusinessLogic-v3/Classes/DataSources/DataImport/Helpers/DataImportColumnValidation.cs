﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataImport.Helpers
{
    public class DataImportColumnValidation
    {
        public bool Required { get; set; }
        public CS.General_v3.Enums.DATA_TYPE DataType { get; set; }
        public bool WarningIfEmpty { get; set; }
        public bool WarningOnInvalidDataForDataType { get; set; }
        public DataImportColumnValidation()
        {
            this.DataType = CS.General_v3.Enums.DATA_TYPE.String;
        }
    }
}
