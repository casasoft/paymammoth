﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DataImport.Helpers;
using CS.General_v3.Util.Encoding.CSV;

namespace BusinessLogic_v3.Classes.DataImport
{
    public class DataImporterBase
    {
        public enum ProcessStatus
        {
            AlreadyStarted,
            OK
        }

        public List<DataImportColumnInfo> Columns { get; set; }
        private readonly object _lock = new object();
        private bool isStarted = false;
        private bool flagToTerminate = false;
        public DateTime CurrentJobStartedOn { get; private set; }
        private CSVFile csvFile { get; set; }

        public DataImporterBase()
        {
            this.Columns = new List<DataImportColumnInfo>();
        }

        protected object getPadlock()
        {
            return _lock;
        }
        
        public virtual ProcessStatus ProcessFile(CSVFile csvFile)
        {
            ProcessStatus result = ProcessStatus.OK;

            if (isStarted)
            {
                result = ProcessStatus.AlreadyStarted;
                

            }
            else
            {
                startProcessingFile(csvFile);
            }
            return result;


        }
        protected virtual void startProcessingFile(CSVFile csvFile)
        {

            lock (getPadlock())
            {
                this.CurrentJobStartedOn = CS.General_v3.Util.Date.Now;
                flagToTerminate = false;
                isStarted = true;
                this.csvFile = csvFile;
                this.Results = new ImportResults(this.getStartLineIndex(), getCSVFileHeaders());
                bool ok = checkForHeaderIndex();
                if (ok && preProcessFile(csvFile))
                {
                    onJobStarted();
                    CS.General_20101215.Util.ThreadUtil.CallMethodOnSeperateThread(start, System.Threading.ThreadPriority.Lowest);
                }
                else
                {
                    onJobEnd();
                }

            }
        }
    }
}
