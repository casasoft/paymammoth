﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataImport.Helpers
{
    public class DataImportFormParams
    {
        public bool ShowRemoveExistingImagesCheckbox { get; set; }
        public bool ShowZipFile { get; set; }
        public bool ShowCustomUploadPath { get; set; }
            

    }
}
