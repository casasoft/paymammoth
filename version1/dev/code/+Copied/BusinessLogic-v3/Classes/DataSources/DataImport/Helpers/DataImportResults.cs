﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Util.Encoding.CSV;

namespace BusinessLogic_v3.Classes.DataImport.Helpers
{
    public class DataImportResults
    {
        public enum RESULT_TYPE
        {
            OK,
            Error,
            Warning
        }
        public OperationResult GeneralImportResult { get; private set; }
        public List<DataImportResultLine> Lines { get; set; }
        public List<string> ColumnNames { get; set; }
        public string CSVFilename { get; set; }
        public string ZIPFilename { get; set; }
        public bool Success
        {
            get
            {
                return this.GeneralImportResult.IsSuccessful;
                
            }

        }

        public DataImportResults(int StartLineIndex)
        {
            //this.Success = true;
            this.GeneralImportResult = new OperationResult();
            this.Lines = new List<DataImportResultLine>();
            this.ColumnNames = new List<string>();
            this.currLineIndex = StartLineIndex;

        }
        public DataImportResults(int StartLineIndex, IEnumerable<string> resultTableColumnNames)
            : this(StartLineIndex)
        {
            this.ColumnNames.AddRange(resultTableColumnNames);
        }
        public DataImportResults(int StartLineIndex, params string[] resultTableColumnNames)
            : this(StartLineIndex)
        {
            this.ColumnNames.AddRange(resultTableColumnNames);
        }


       // public List<string> ErrorMessages { get; set; }
        private int currLineIndex = 0;
        public DataImportResultLine AddNewResultLine(CSVLine csvLine)
        {
            DataImportResultLine line = new DataImportResultLine(currLineIndex, csvLine);
            currLineIndex++;
            this.Lines.Add(line);
            return line;
        }
        /*public void AddLine(RESULT_TYPE resultType, params string[] columnValues)
        {
            ImportResultLine line = new ImportResultLine(resultType, columnValues);
            AddLine(line);
        }*/
      
      
        public CS.General_v3.Enums.STATUS_MSG_TYPE  GetResultType()
        {
            CS.General_v3.Enums.STATUS_MSG_TYPE msgType = this.GeneralImportResult.Status;
            
            foreach (var line in this.Lines)
            {

                int lineStatusValue = (int)line.Result.Status;
                if (lineStatusValue > (int)msgType)
                {
                    msgType = line.Result.Status;
                }

            }
            return msgType;
        }


    }
}
