﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataImport.Helpers
{
    public class DataImportColumnInfo
    {
        public List<string> ColumnNames { get; set; }


        public DataImportColumnValidation Validation { get; private set; }
        public int? LinkedColumnIndexInCsv { get; set; }
        public string Description { get; set; }
        public DataImportColumnInfo()
        {
            this.ColumnNames = new List<string>();
            this.Validation = new DataImportColumnValidation();
        }
        public string GetMainTitle()
        {
            return ColumnNames.FirstOrDefault();
        }

        public void AddColumnName(params string[] names)
        {
            this.ColumnNames.AddRange(names);
        }
        public void AddColumnName(Enum enumValue, params string[] names)
        {
            List<string> list = new List<string>();
            {
                string enumToString = CS.General_v3.Util.EnumUtils.StringValueOf(enumValue);
                
                string spacedString = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(enumToString);
                list.Add(spacedString);
                if (enumToString != spacedString)
                {
                    list.Add(enumToString);
                }
            }


            if (names != null)
                list.AddRange(names);
            AddColumnName(list.ToArray());
        }


        public bool ValidateString(string s, DataImportResultLine lineResult)
        {
            bool ok = true;
            if (ok && string.IsNullOrWhiteSpace(s))
            {

                if (this.Validation.Required)
                {
                    ok = false;
                    lineResult.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Column <" + GetMainTitle() + "> is required");
                }
                else if (this.Validation.WarningIfEmpty)
                {
                    lineResult.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Column <" + GetMainTitle() + "> is empty");
                }

            }
            {
                string dataTypeErrMsg = null;
                switch (this.Validation.DataType)
                {
                    case CS.General_v3.Enums.DATA_TYPE.Double:
                    case CS.General_v3.Enums.DATA_TYPE.DoubleNullable:
                    case CS.General_v3.Enums.DATA_TYPE.Integer:
                    case CS.General_v3.Enums.DATA_TYPE.IntegerMultipleChoice:
                    case CS.General_v3.Enums.DATA_TYPE.IntegerNullable:
                        {
                            double d = 0;
                            
                            if (!string.IsNullOrWhiteSpace(s) && !double.TryParse(s, out d))
                            {
                                
                                ok = false;
                                dataTypeErrMsg = "Column <" + GetMainTitle() + "> value of '" + s + "' could not be parsed into a number";
                            }
                        }
                        break;
                    case CS.General_v3.Enums.DATA_TYPE.Bool:
                    case CS.General_v3.Enums.DATA_TYPE.BoolNullable:
                        {
                            bool? b = CS.General_v3.Util.Other.TextToBoolNullable(s);
                            if (b == null)
                            {
                                ok = false;
                                dataTypeErrMsg = "Column <" + GetMainTitle() + "> value of '" + s + "' could not be parsed into Yes/No (boolean) value";
                            }
                        }
                        break;
                }
                if (!string.IsNullOrEmpty(dataTypeErrMsg))
                {
                    if (this.Validation.WarningOnInvalidDataForDataType)
                        lineResult.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, dataTypeErrMsg);
                    else
                        lineResult.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, dataTypeErrMsg);
                }
            }
            return ok;
        }



        public bool TryToMatchInCsvLine(CS.General_v3.Util.Encoding.CSV.CSVLine headerLine)
        {
            bool found = false;
            for (int i=0; i < headerLine.Count;i++)
            {
                var token = headerLine[i];
                string s = token ?? "";
                s = s.Trim();
                foreach (var name in this.ColumnNames)
                {
                    if (string.Compare(s, name, true) == 0)
                    {
                        this.LinkedColumnIndexInCsv = i;
                        found = true;
                        break;
                    }
                }
                if (found)
                    break;

            }
            return found;
            
        }



        public bool NotUsed { get; set; }

        public bool IsUsed()
        {
            return !NotUsed;
            
        }
    }
}
