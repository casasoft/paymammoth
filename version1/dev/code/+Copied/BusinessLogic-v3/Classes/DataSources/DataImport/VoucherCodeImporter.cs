﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;
using BusinessLogic_v3.Classes.DataImport.Generic;
using CS.General_v3.Util.Encoding.CSV;

using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;

using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Classes.DataImport
{
    public class VoucherCodeImporter : BusinessLogic_v3.Classes.DataImport.Generic.GenericDataImporter
    {
        public VoucherCodeImporter(ISpecialOfferBase specialOffer)
        {
           
            this.SpecialOfferToAddTo = specialOffer;
            
        }
        protected override List<ColumnInfo> getColumnInfos()
        {
            List<ColumnInfo> list = new List<ColumnInfo>();
            list.Add(new ColumnInfo("Voucher", false));
            return list;
        }

        public ISpecialOfferBase SpecialOfferToAddTo {get;set;}
        private IEnumerable<SpecialOfferVoucherCodeBase> allVoucherCodes = null;
        private MyNHSessionBase session = null;
        protected override bool preProcessFile(CSVFile csvFile)
        {
            //session = new MyStatelessSessionScope();
            allVoucherCodes = SpecialOfferVoucherCodeBase.Factory.FindAll();
            
            return base.preProcessFile(csvFile);
        }
        protected override void postProcessFile()
        {
            
            base.postProcessFile();
        }

        protected override void processLine(CSVLine csvLine, ImportResultLine resultsLine)
        {
            //CS.General_v3.Classes.nHibernateDB.CastleActiveRecord.MySessionScope session = new CS.General_v3.Classes.nHibernateDB.CastleActiveRecord.MySessionScope();
            ImportResultLine line = resultsLine;
            int colIndex = 0;
            string code = CS.General_v3.Util.Text.AppendStrings("", csvLine.Tokens);

            if (!string.IsNullOrWhiteSpace(code))
            {
                
                var existingCode = this.allVoucherCodes.Where(item=> item.VoucherCode.EqualsToCaseInsensitive(code)).FirstOrDefault();
                if (existingCode != null)
                {
                    string spTitle = "";
                    if (existingCode.SpecialOffer != null)
                        spTitle = existingCode.SpecialOffer.Title;
                    line.AddValueAsError(0, "Code <" + code + "> already exists under special offer '" + spTitle + "'");

                }
                else
                {
                    SpecialOfferToAddTo.CreateVoucherCode(code, verifyExistsFirst: false);
                }
            }


            
        }

        

    }
}
