﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BusinessLogic_v3.Classes.DataImport.Helpers;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using BusinessLogic_v3.Modules.ProductVariationModule;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Classes.DataImport
{
    public class ProductsImporter : DataImporterBase<ProductsImporter>
    {
        public enum ColumnsInfo
        {
            RefCode,
            BarCode,
            Category,
            SubCategory,
            SubSubCategory,
            Title,
            Description,
            PriceWholesale,
            PriceRetail,
            PriceSpecialOffer,
            VatRate,
            Colour,
            Material,
            Warranty,
            Brand,
            Quantity



        }
        public bool DontOverwriteDescriptionsIfAlreadyFilled { get; set; }
        private readonly string[] CATEGORY_DELIMETERS = { "|", ">", "=", @"\", "/" };

        protected ProductsImporter()
        {
            

        }
        protected override void initialiseColumns()
        {
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.RefCode, "Ref. Code");

                col.Validation.Required = true;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.BarCode);
                col.Validation.Required = true;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Category);
                //col.Validation.Required = true;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.SubCategory);

                col.Validation.Required = false;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.SubSubCategory);

                col.Validation.Required = false;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Title);

                col.Validation.Required = false;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Description);

                col.Validation.Required = false;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.PriceWholesale, "Wholesale");

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.DoubleNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.PriceRetail, "Price (RRP)", "RRP", "Retail");

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.DoubleNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.PriceSpecialOffer, "Special Offer","Special Offer Price");

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.DoubleNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.VatRate, "VAT");

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.DoubleNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Colour);

                col.Validation.Required = false;

                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Material);

                col.Validation.Required = false;

                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Warranty);

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.IntegerNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Brand);

                col.Validation.Required = false;

                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
            {
                DataImportColumnInfo col = new DataImportColumnInfo();
                col.AddColumnName(ColumnsInfo.Quantity, "Qty");

                col.Validation.Required = false;
                col.Validation.DataType = CS.General_v3.Enums.DATA_TYPE.IntegerNullable;
                this.Parameters.ColumnsCollection.Columns.Add(col);
            }
        }

        protected override bool preProcessFile(CS.General_v3.Util.Encoding.CSV.CSVFile csvFile)
        {
            return base.preProcessFile(csvFile);
        }

        private ProductBase getProductByReferenceCode(string refCode)
        {
           // isNewItemGroup = false;
            ProductBase item = ProductBaseFactory.Instance.GetProductByReferenceCode(refCode);
            //var itemGroupMatched = _itemGroupList.Where((itemGroup) => itemGroup.Data.ReferenceCode == articleRef).FirstOrDefault();
            if (item == null)
            {
                item = ProductBase.Factory.CreateNewItem();
                item.ReferenceCode = refCode;
               // isNewItemGroup = true;
                
            }
            
            return item;
        }

        private ProductVariationBase getVariationByBarCode(string barCode, ProductBase product)
        {
            ProductVariationBase item = ProductVariationBaseFactory.Instance.GetByBarcode(barCode);
          //  isNew = false;
            if (item == null)
            {
                item = product.ProductVariations.CreateNewItem();
                item.Barcode = barCode;
            //    isNew = true;
                return item;
            }
            item.Product = product;
            product.ProductVariations.Add(item);
            return item;

        }
        private BrandBase getBrandOrCreateNewBrand(string brand)
        {
            return (BrandBase)BrandBase.Factory.GetBrandByTitleOrImportRef(brand);
        }
        
        private IEnumerable<FileInfo> sortFilesByFileName(IEnumerable<FileInfo> files)
        {
            var fileList = files.ToList();
            fileList.Sort((f1, f2) => (f1.FullName.CompareTo(f2.FullName)));
            return fileList;

        }

        


        private void checkForImages(ProductVariationBase item, string referenceCode)
        {
            //string filename = string.Empty;
            //if (item.Barcode.IsNotNullOrEmpty())
            //{
            //    filename = item.Barcode;
            //}
            //else
            //{
            //    filename = referenceCode;
            //}

            bool oneFound = false;

            

            string regex = CS.General_v3.Util.Text.ForRegExp(item.Barcode) + @"\.*?";
            var imageFiles = getImagesFromFileSystem(regex);
            if (imageFiles.IsNotNullOrEmpty())
            {
                imageFiles = sortFilesByFileName(imageFiles);
                foreach (var imgFile in imageFiles)
                {
                    oneFound = true;
                    string name = imgFile.Name;

                    ProductVariationMediaItemBase mediaItem = item.Product.GetMediaItemByReference(name);
                    //ProductVariationMediaItemBase mediaItem = item.GetMediaItemByReference(name);
                    if (mediaItem == null)
                    {
                        mediaItem = item.MediaItems.CreateNewItem();
                    }
                    else
                    {
                        if (mediaItem.ProductVariation != item)
                        {
                            mediaItem.ProductVariation.MediaItems.Remove(mediaItem);
                            mediaItem.ProductVariation = item;

                        }
                    }
                    mediaItem.Reference = name;

                        // var fs = imageFile.OpenRead();
                       
                    try
                    {
                        mediaItem.Image.UploadFileFromLocalPath(imgFile, autoSave: false);
                        //contiunue test imageFiles insert
                        //cointiue here
                        mediaItem.Save();
                    }
                    catch (Exception ex)
                    {
                        mediaItem.Delete();
                        this.currentResultLine.Result.AddException(ex);
                    }
                   

                }
            }
            if (!oneFound)
            {
                this.currentResultLine.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Image not found for item with barcode '" + item.Barcode + "'");
            }
        }
        private double getParsedPrice(string priceValue)
        {
            if (priceValue.IsNotNullOrEmpty())
            {
                return double.Parse(Regex.Replace(priceValue, "[^0-9.]", ""));
            }
            return 0;
        }

        private CategoryBase getCategoryFromExcelCategoryOrCreateNew(string category)
        {
            
            List<string> cats = new List<string>();
            {
                var tokens = CS.General_v3.Util.Text.Split(category, CATEGORY_DELIMETERS.ToArray());
                for (int j = 0; j < tokens.Count; j++)
                {
                    var t = tokens[j];
                    t = t.Trim();
                    cats.Add(t);
                }
            }

            CategoryBase currentCat = CategoryBaseFactory.Instance.GetRootCategory();
            for (int i = 0; i < cats.Count; i++)
            {
                string cat = cats[i];
                if (!string.IsNullOrWhiteSpace(cat))
                {
                    

                    var nextCat = currentCat.GetChildCategoryByTitleOrImportRef(cat);

                    currentCat = nextCat;
                }
            }
            return currentCat;



        }

        private void verifyRequiredFields(ProductBase product, DataImportResultLine resultLine,
            string category, string title, string description, double? priceRetail)
            
            
        {
            if (string.IsNullOrWhiteSpace(title)) resultLine.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Title is required");
            if (string.IsNullOrWhiteSpace(category)) resultLine.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Category is required");
            if (string.IsNullOrWhiteSpace(description)) resultLine.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Description is not filled");
            if (!priceRetail.HasValue) resultLine.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Retail price is required");


        }

        /// <summary>
        /// This method will parse the string for a refernece code
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        protected virtual string parseRefCode(string s)
        {
            return s;
        }
        protected virtual void setCmsLink(ProductBase product, DataImportResultLine resultsLine)
        {

        }

        protected override void processLine(CS.General_v3.Util.Encoding.CSV.CSVLine csvLine, int lineIndex, Helpers.DataImportResultLine resultsLine)
        {

            string refCode =parseRefCode( getColumnData(ColumnsInfo.RefCode, csvLine));
            string barCode = getColumnData(ColumnsInfo.BarCode, csvLine);
            string category = getColumnData(ColumnsInfo.Category, csvLine);
            string subCategory = getColumnData(ColumnsInfo.SubCategory, csvLine);
            string subSubCategory = getColumnData(ColumnsInfo.SubSubCategory, csvLine);
            string title = getColumnData(ColumnsInfo.Title, csvLine);
            string description = getColumnData(ColumnsInfo.Description, csvLine);
            double? priceWholesale = getNumericColumnData<double?>(ColumnsInfo.PriceWholesale, csvLine);
            double? priceSpecialOffer = getNumericColumnData<double?>(ColumnsInfo.PriceSpecialOffer, csvLine);
            double? priceRetail = getNumericColumnData<double?>(ColumnsInfo.PriceRetail, csvLine);
            double? vatRate = getNumericColumnData<double?>(ColumnsInfo.VatRate, csvLine);
            string colour = getColumnData(ColumnsInfo.Colour, csvLine);
            string size = getColumnData(ColumnsInfo.Material, csvLine);
            string warrantyText = getColumnData(ColumnsInfo.Warranty, csvLine);
            string brand = getColumnData(ColumnsInfo.Brand, csvLine);
            int? quantity = getNumericColumnData<int?>(ColumnsInfo.Quantity, csvLine);

            ProductBase product = null;
            ProductVariationBase variation = null;

            if (resultsLine.Result.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Error)
            {
                
                using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                {

                    product = getProductByReferenceCode(refCode); //get product, and if it does not exist, create
                    if (product.IsTransient()) verifyRequiredFields(product, resultsLine, category,title,description,priceRetail); // if this is a new product, verify that all the required fields are filled in
                    if (resultsLine.Result.IsSuccessful)
                    {



                        variation = getVariationByBarCode(barCode, product); //get the variation, else create

                        
                        var itemData = variation;
                        var itemgroupData = product;

                        bool isVariationLine = string.IsNullOrWhiteSpace(title); //checks whether this is just an empty variation line.  This is based on if the title is filled in.  
                                                                                   //A variation is a line which contains online variation specific info like colour, size, quantity



                        itemgroupData.ReferenceCode = refCode;


                        if (!isVariationLine)
                        {
//product fields - Fill in these fields, if they are filled in
                            if (string.IsNullOrWhiteSpace(itemgroupData.Description) || //description is empty
                                (!string.IsNullOrWhiteSpace(itemgroupData.Description) && !DontOverwriteDescriptionsIfAlreadyFilled)) //description is not empty, and dont overwrite descriptions is not flagged as true
                            {
                                itemgroupData.Description = description;
                            }

                            if (!string.IsNullOrWhiteSpace(title)) itemgroupData.Title = title;
                            if (priceWholesale.HasValue) itemgroupData.PriceWholesale = priceWholesale.Value;
                            if (priceRetail.HasValue) itemgroupData.PriceRetailIncTaxPerUnit = priceRetail.Value;
                            if (priceRetail.HasValue && priceSpecialOffer.GetValueOrDefault() > 0)
                            {
                                itemgroupData.PriceRetailBefore = priceRetail.Value;
                                itemgroupData.PriceRetailIncTaxPerUnit = priceSpecialOffer.Value;
                            }
                            itemgroupData.PriceRetailTaxPerUnit = CS.General_v3.Util.FinancialUtil.GetTaxOnlyAmountFromPriceIncTax(itemgroupData.PriceRetailIncTaxPerUnit, vatRate.GetValueOrDefault());

                            if (vatRate.GetValueOrDefault() <= 0) vatRate = CS.General_v3.Settings.GetSettingFromDatabase<double>(CS.General_v3.Enums.SETTINGS_ENUM.Others_VatRate);


                            string sFullCat = CS.General_v3.Util.Text.AppendStrings("=", category, subCategory, subSubCategory);
                            itemgroupData.AddCategory(getCategoryFromExcelCategoryOrCreateNew(sFullCat));

                            if (!string.IsNullOrWhiteSpace(brand))
                            {
                                BrandBase itemGroupBrand = getBrandOrCreateNewBrand(brand);
                                if (itemGroupBrand != null)
                                {
                                    itemgroupData.Brand = itemGroupBrand;
                                }
                            }
                        }

                        //variation fields

                        itemData.Barcode = barCode;
                        itemData.Colour = colour; // ProductVariationColourBaseFactory.Instance.GetByTitle(colour);
                        itemData.Size = size; // ProductVariationSizeBaseFactory.Instance.GetByTitle(size);

                        if (quantity.HasValue)
                        {
                            itemData.Quantity = quantity.Value;
                        }

                        //end of variation fields

                        if (this.RemoveAnyExistingImages)
                        {
                            variation.RemoveAllImages(autoSave: false);
                        }
                        setCmsLink(itemgroupData, resultsLine);

                        checkForImages(variation, refCode);

                        itemgroupData.Save();

                        itemData.Save();

                        t.Commit();
                    }
                }
            }
        }
    }
}
