﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataSources.MaltaAirportFlights
{
    /// <summary>
    /// Contains flight details for a Malta Internation Airport Arrival/Departure flight.
    /// </summary>
    public class MaltaAirportFlightDetails
    {
        internal MaltaAirportFlightDetails(string airline, string origin, string destination, string flight, string craft,
            string scheduled, string estimated, string remarks, DateTime timestamp, Enums.FLIGHT_TYPE flightType)
        {
            this.Airline = airline;
            this.Origin = origin;
            this.Destination = destination;
            this.Flight = flight;
            this.Craft = craft;
            this.Scheduled = scheduled;
            this.Estimated = estimated;
            this.Remarks = remarks;
            this.TimeStamp = timestamp;
            this.FlightType = flightType;
        }

        /// <summary>
        /// Airline name - Ex: Thomas Cook Airlines
        /// </summary>
        public string Airline { get; private set; }
        /// <summary>
        /// Place of Departure for flight - Ex: East Midlands
        /// </summary>
        public string Origin { get; private set; }
        /// <summary>
        /// Place of Arrival for flight - Ex: Malta
        /// </summary>
        public string Destination { get; private set; }
        /// <summary>
        /// Flight Number - Ex: TCX 5126
        /// </summary>
        public string Flight { get; private set; }
        /// <summary>
        /// Craft Number - Ex: 321
        /// </summary>
        public string Craft { get; private set; }
        /// <summary>
        /// Scheduled time of Departure or Arrival (in short SCH) - Ex: 18:45
        /// </summary>
        public string Scheduled { get; private set; }
        /// <summary>
        /// Estimated time of Departure or Arrival (in short EST or ETD or ETA) - Ex: 18:20
        /// </summary>
        public string Estimated { get; private set; }
        /// <summary>
        /// Any additional remarks regarding this flight - Ex: Landed
        /// </summary>
        public string Remarks { get; private set; }
        /// <summary>
        /// The time this entry was submitted / updated last as a DateTime object - Ex: 11-Jul-2012 18:45
        /// </summary>
        public DateTime TimeStamp { get; private set; }
        /// <summary>
        /// The type of flight relative to Malta Internation Airport - Ex: Departure
        /// </summary>
        public Enums.FLIGHT_TYPE FlightType { get; private set; }
    }
}
