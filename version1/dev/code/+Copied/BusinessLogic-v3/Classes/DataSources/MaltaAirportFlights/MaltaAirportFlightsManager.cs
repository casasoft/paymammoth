﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using log4net;
using System.IO;
using System.Web;
using BusinessLogic_v3.Classes.Exceptions.DataSourcesExceptions;

namespace BusinessLogic_v3.Classes.DataSources.MaltaAirportFlights
{
    public class MaltaAirportFlightsManager
    {
        #region ClassDefinitions
        private MaltaAirportFlightsManager()
        {
            CS.General_v3.Util.IO.WatchFileForChanges(CS.General_v3.Util.PageUtil.MapPath(ArrivalsTxtFilePath), invalidateArrivalsCache);
            CS.General_v3.Util.IO.WatchFileForChanges(CS.General_v3.Util.PageUtil.MapPath(DeparturesTxtFilePath), invalidateDeparturesCache);
        }


        private static readonly MaltaAirportFlightsManager _instance = new MaltaAirportFlightsManager();

        public static MaltaAirportFlightsManager Instance
        {
            get { return _instance; }
        }

        private string getArrivalsTxtFilePath()
        {
            return Factories.SettingFactory.GetSettingValue<string>(
                        Enums.BusinessLogicSettingsEnum.MaltaAirportFlights_ArrivalsTextFilePath);
        }

        private string getDeparturesTxtFilePath()
        {
            return Factories.SettingFactory.GetSettingValue<string>(
                        Enums.BusinessLogicSettingsEnum.MaltaAirportFlights_DeparturesTextFilePath);
        }
        #endregion

        private const string FIELD_DELIMITER_IN_FLIGHT_LINE = ",";

        public const string ARRIVALS_CACHE_KEY = "mia_flights_arrivals_cache-key_mgr";
        public const string DEPARTURES_CACHE_KEY = "mia_flights_departures_cache-key_mgr";

        /// <summary>
        /// Returns the relative filepath where the Arrivals TXT file is stored on the server.
        /// </summary>
        public string ArrivalsTxtFilePath { get { return getArrivalsTxtFilePath(); } }
        /// <summary>
        /// Returns the relative filepath where the Departures TXT file is stored on the server.
        /// </summary>
        public string DeparturesTxtFilePath { get { return getDeparturesTxtFilePath(); } }

        /// <summary>
        /// Returns the next flights arriving at Malta International Airport.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MaltaAirportFlightDetails> GetArrivalFlights()
        {
            return getFlights(ArrivalsTxtFilePath, Enums.FLIGHT_TYPE.Arrival);
        }

        private void invalidateDeparturesCache(object o, FileSystemEventArgs e)
        {
            CS.General_v3.Util.CachingUtil.RemoveItemFromCache(DEPARTURES_CACHE_KEY);
        }

        private void invalidateArrivalsCache(object o, FileSystemEventArgs e)
        {
            CS.General_v3.Util.CachingUtil.RemoveItemFromCache(ARRIVALS_CACHE_KEY);
        }

        /// <summary>
        /// Returns the next flights departing from Malta International Airport.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MaltaAirportFlightDetails> GetDepartureFlights()
        {
            return getFlights(DeparturesTxtFilePath, Enums.FLIGHT_TYPE.Departure);
        }

        private IEnumerable<MaltaAirportFlightDetails> getFlights(string filepath, Enums.FLIGHT_TYPE flightType)
        {
            List<MaltaAirportFlightDetails> results = new List<MaltaAirportFlightDetails>();
            try
            {
                string absolutePath = HttpContext.Current.Server.MapPath(filepath);
                using (StreamReader sr = new StreamReader(absolutePath))
                {
                    String line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        MaltaAirportFlightDetails item = getFlightFromLine(line, flightType);
                        if (item != null)
                        {
                            results.Add(item);
                        }
                    }
                    sr.DiscardBufferedData();
                    sr.Dispose();
                }
            }
            catch (Exception e)
            {
                string errorMsg = "An error occured whilst processing the <" + flightType + "s> TXT file.";
                MaltaAirportFlightsFeedException ex = new MaltaAirportFlightsFeedException(errorMsg, e);
                CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Error, ex);
            }
            if(flightType == Enums.FLIGHT_TYPE.Arrival)
            {
                CS.General_v3.Util.PageUtil.SetCachedObject(ARRIVALS_CACHE_KEY, DateTime.Now, 300);
            }
            else if (flightType == Enums.FLIGHT_TYPE.Departure)
            {
                CS.General_v3.Util.PageUtil.SetCachedObject(DEPARTURES_CACHE_KEY, DateTime.Now, 300);
            }
            return results;
        }

        private MaltaAirportFlightDetails getFlightFromLine(string line, Enums.FLIGHT_TYPE flightType)
        {
            MaltaAirportFlightDetails result = null;
            try
            {
                string[] properties = line.Split(",".ToCharArray().First());
                string airline = CS.General_v3.Util.Text.CapitaliseFirstLetterOfEachWord(properties[0]);
                string from = flightType == Enums.FLIGHT_TYPE.Arrival
                                  ? CS.General_v3.Util.Text.CapitaliseFirstLetterOfEachWord(properties[1])
                                  : "Malta";
                string to = flightType == Enums.FLIGHT_TYPE.Arrival
                                ? "Malta"
                                : CS.General_v3.Util.Text.CapitaliseFirstLetterOfEachWord(properties[1]);
                string flight = properties[2];
                string craft = properties[3];
                string schedule = properties[4];
                string estimate = properties[5];
                string remarks = CS.General_v3.Util.Text.CapitaliseFirstLetterOfEachWord(properties[6]);
                string timestamp_str = properties[7];
                int year = int.Parse(timestamp_str.Substring(0, 4));
                int month = int.Parse(timestamp_str.Substring(4, 2));
                int day = int.Parse(timestamp_str.Substring(6, 2));
                int hours = int.Parse(timestamp_str.Substring(8, 2));
                int minutes = int.Parse(timestamp_str.Substring(10, 2));
                DateTime timestamp = new DateTime(year, month, day, hours, minutes, 00);
                result = new MaltaAirportFlightDetails(airline, from, to, flight, craft, schedule, estimate, remarks,
                                                     timestamp, flightType);
            }
            catch(Exception e)
            {
                string errorMsg = "An error occured whilst converting line <" + line + "> for flight type <" +
                                  flightType +
                                  "> - see exception for details.";
                MaltaAirportFlightsInvalidLineException ex = new MaltaAirportFlightsInvalidLineException(errorMsg, e);
                CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Error, ex);
            }
            return result;
        }
    }
}
