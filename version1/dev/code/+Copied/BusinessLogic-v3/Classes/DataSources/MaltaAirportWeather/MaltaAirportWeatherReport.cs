﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DataSources.MaltaAirportWeather.HelperClasses;

namespace BusinessLogic_v3.Classes.DataSources.MaltaAirportWeather
{
    public class MaltaAirportWeatherReport : IWeatherReport
    {
        public DateTime Date { get; set; }
        public MaltaAirportForecast Forecast { get; set; }
        public MaltaAirportObservations Observations { get; set; }
        public IEnumerable<MaltaAirportOutlookMariners> OutlookMariners { get; set; }
        public MaltaAirportOutlook Outlook { get; set; }
        public MaltaAirportRainfall Rainfalls { get; set; }
        public MaltaAirportPrecipitation Precipitations { get; set; }
        public MaltaAirportTemperature Temperatures { get; set; }
        public MaltaAirportWindSpeed WindSpeeds { get; set; }
        public MaltaAirportWindDirection WindDirections { get; set; }
        public MaltaAirportForeignCities ForeignCities { get; set; }

        double? IWeatherReport.MaximumTemperature
        {
            get { return Outlook.TodayMaxTemp; }
        }

        double? IWeatherReport.MinimumTemperature
        {
            get { return Outlook.TodayMinTemp; }
        }

        #region Calculations
        private bool currentTempCalculated { get; set; }
        private double? _currentTemp { get; set; }

        private double? getCurrentTemperature()
        {
            if (!currentTempCalculated)
            {
                var luqaCity = ForeignCities.Cities.Where(item => item.Name == "Luqa").FirstOrDefault();
                if (luqaCity != null)
                {
                    _currentTemp = luqaCity.Temperature;
                }
                currentTempCalculated = true;
            }
            return _currentTemp;
        }

        private bool currentConditionCalculated { get; set; }
        private Enums.WEATHER_CONDITION? _currentCondition { get; set; }

        private Enums.WEATHER_CONDITION? getCurrentCondition()
        {
            if (!currentConditionCalculated)
            {
                var luqaCity = ForeignCities.Cities.Where(item => item.Name == "Luqa").FirstOrDefault();
                if (luqaCity != null)
                {
                    _currentCondition = luqaCity.CityOutlook;
                }
                currentConditionCalculated = true;
            }
            return _currentCondition;
        }
        #endregion

        double? IWeatherReport.CurrentTemperature
        {
            get { return getCurrentTemperature(); }
        }

        DateTime? IWeatherReport.Date
        {
            get { return Date; }
        }

        Enums.WEATHER_CONDITION? IWeatherReport.Condition
        {
            get { return getCurrentCondition(); }
        }

        public Enums.WEATHER_SOURCE DataSource
        {
            get { return Enums.WEATHER_SOURCE.MaltaInternationalAirport; }
        }
    }
}
