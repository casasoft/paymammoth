﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataSources.MaltaAirportWeather.HelperClasses
{
    public class MaltaAirportWindSpeed
    {
        public IEnumerable<MaltaAirportLocality> Localities { get; set; }
    }
}
