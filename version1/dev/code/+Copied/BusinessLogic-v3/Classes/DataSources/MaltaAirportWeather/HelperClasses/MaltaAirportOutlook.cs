﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataSources.MaltaAirportWeather.HelperClasses
{
    public class MaltaAirportOutlook
    {
        public DateTime? IssueTime { get; set; }
        public string OutlookUntil { get; set; }
        public string Details { get; set; }
        public string OutlookInMaltese { get; set; }
        public string Warnings { get; set; }
        public string Interference { get; set; }
        public string Weather { get; set; }
        public string Visibility { get; set; }
        public string OutlookWind { get; set; }
        public string Sea { get; set; }
        public string Swell { get; set; }
        public double? SeaTemp { get; set; }
        public string WarningsInMaltese { get; set; }
        public string InterferenceInMaltese { get; set; }
        public string WeatherInMaltese { get; set; }
        public string VisibilityInMaltese { get; set; }
        public string OutlookWindInMaltese { get; set; }
        public string SeaInMaltese { get; set; }
        public string SwellInMaltese { get; set; }
        public double? TodayMaxTemp { get; set; }
        public double? TodayMinTemp { get; set; }
        public double? UVIndex { get; set; }
        public double? HeatStress { get; set; }
    }
}
