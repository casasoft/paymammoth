﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataSources.MaltaAirportWeather.HelperClasses
{
    public class MaltaAirportForecast
    {
        /// <summary>
        /// Last Update Time.
        /// </summary>
        public DateTime Time { get; set; }
        public string ForecastValid { get; set; }
        public Enums.WEATHER_CONDITION? ConditionToday { get; set; }
        public double? Temperature { get; set; }
        public double? Humidity { get; set; }
        public double? AtmosphericPressure { get; set; }
        public string Wind { get; set; }
        public DateTime? Sunrise { get; set; }
        public string ForecastImagePath { get; set; }
        public DateTime? Sunset { get; set; }
        public IEnumerable<MaltaAirportDayForecast> DayForecasts { get; set; }
    }
}
