﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Util;
using System.Xml;
using BusinessLogic_v3.Classes.DataSources.MaltaAirportWeather.HelperClasses;
using System.Web;
using BusinessLogic_v3.Classes.Exceptions.DataSourcesExceptions;

namespace BusinessLogic_v3.Classes.DataSources.MaltaAirportWeather
{
    public class MaltaAirportWeatherManager
    {
        #region ClassDefinitions
        private MaltaAirportWeatherManager()
        {
        }

        private static MaltaAirportWeatherManager instance;
        private static object _obj = new object();
        public static MaltaAirportWeatherManager Instance
        {
            get
            {
                lock (_obj)
                {
                    if (instance == null)
                    {
                        instance = new MaltaAirportWeatherManager();
                    }
                    return instance;
                }
            }
        }
        #endregion

        public const string MIA_WEATHER_XML_URLPREFIX =
            "http://www.maltairport.com/weather/xml/WeatherReport_";

        public const string XML_EXTENSTION = ".xml";

        public const string MIA_MANAGER_CACHEKEY = "mia_mgr_cachekey_report";

        private string getXMLPath(DateTime? xmlDate = null)
        {
            DateTime currentTime = xmlDate.HasValue ? xmlDate.Value : Date.Now;
            string date = currentTime.Day.ToString("00");
            date += currentTime.Month.ToString("00");
            date += currentTime.Year.ToString();
            string xmlPath = MIA_WEATHER_XML_URLPREFIX + date + XML_EXTENSTION;
            return xmlPath;
        }

        /// <summary>
        /// Gets the weather report for the specified date - if null the current datetime will be used.
        /// </summary>
        /// <param name="weatherDate"></param>
        /// <returns></returns>
        public MaltaAirportWeatherReport GetWeatherReport(DateTime? weatherDate = null)
        {
            MaltaAirportWeatherReport report = new MaltaAirportWeatherReport();
            DateTime date = weatherDate.HasValue ? weatherDate.Value : Date.Now;
            try
            {
                report = getFromCache();
                if (report == null)
                {
                    string xmlPath = getXMLPath(weatherDate);
                    XmlDocument doc = new XmlDocument();
                    XmlTextReader reader = new XmlTextReader(xmlPath);
                    reader.MoveToContent();
                    doc.Load(reader);
                    reader.Close();
                    reader = null;
                    report = getWeatherReportFromDoc(doc, date);
                    setInCache(report);
                }
            }
            catch (Exception e)
            {
                string errorMsg = "An error occured whilst processing the MIA Weather XML File for date <" +
                                  date.ToString() + "> - see inner exception for details.";
                MaltaAirportWeatherFeedException ex = new MaltaAirportWeatherFeedException(errorMsg, e);
                CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Error, ex);
            }
            return report;
        }

        private void setInCache(MaltaAirportWeatherReport report)
        {
            double cacheMinutesUntilExpiry =
                Factories.SettingFactory.GetSettingValue<double>(
                    Enums.BusinessLogicSettingsEnum.MaltaAirportWeather_CacheMinutesTillExpiry);
            CS.General_v3.Util.PageUtil.SetCachedObject(MIA_MANAGER_CACHEKEY, report, cacheMinutesUntilExpiry);
        }

        private MaltaAirportWeatherReport getFromCache()
        {
            return CS.General_v3.Util.PageUtil.GetCachedObject<MaltaAirportWeatherReport>(MIA_MANAGER_CACHEKEY);
        }

        private MaltaAirportWeatherReport getWeatherReportFromDoc(XmlDocument doc, DateTime weatherReportDate)
        {
            XmlNode dateNode = doc.SelectSingleNode("/WeatherForecast/date");
            string dateApplicable = dateNode.ChildNodes[0].Value;
            int year = int.Parse(dateApplicable.Substring(0, 4));
            int month = int.Parse(dateApplicable.Substring(5, 2));
            int day = int.Parse(dateApplicable.Substring(8, 2));
            DateTime applicableDate = new DateTime(year, month, day);

            XmlNode forecastNode = doc.SelectSingleNode("/WeatherForecast/forecast");
            MaltaAirportWeatherReport report = new MaltaAirportWeatherReport();
            report.Date = applicableDate;
            report.Forecast = getForecastFromForecastNode(forecastNode, weatherReportDate);
            XmlNode observationsNode = doc.SelectSingleNode("/WeatherForecast/observations");
            report.Observations = getObservationsFromObservationsNode(observationsNode, weatherReportDate);
            XmlNodeList outlookMarinersNodes = doc.SelectNodes("/WeatherForecast/outlookMariners");
            report.OutlookMariners = getOutlookMarinersFromOutlookMarinersNodeList(outlookMarinersNodes,
                                                                                   weatherReportDate);
            XmlNode outlookNode = doc.SelectSingleNode("/WeatherForecast/outlook");
            report.Outlook = getOutlookFromOutlookNode(outlookNode, weatherReportDate);
            XmlNode rainfallNode = doc.SelectSingleNode("/WeatherForecast/rainfall");
            report.Rainfalls = getRainfallsFromRainfallNode(rainfallNode, weatherReportDate);
            XmlNode precipitationNode = doc.SelectSingleNode("/WeatherForecast/precipitation");
            report.Precipitations = getPrecipitationsFromPrecipitationNode(precipitationNode, weatherReportDate);
            XmlNode temperatureNode = doc.SelectSingleNode("/WeatherForecast/temperature");
            report.Temperatures = getTemperaturesFromTemperatureNode(temperatureNode, weatherReportDate);
            XmlNode windSpeedNode = doc.SelectSingleNode("/WeatherForecast/windspeed");
            report.WindSpeeds = getWindSpeedsFromWindSpeedNode(windSpeedNode, weatherReportDate);
            XmlNode windDirectionNode = doc.SelectSingleNode("/WeatherForecast/winddirection");
            report.WindDirections = getWindDirectionsFromWindDirectionNode(windDirectionNode, weatherReportDate);
            XmlNode foreignCitiesNode = doc.SelectSingleNode("/WeatherForecast/foreignCities");
            report.ForeignCities = getForeignCitiesFromForeignCitiesNode(foreignCitiesNode, weatherReportDate);
            return report;
        }

        private Enums.WEATHER_CONDITION? parseWeatherCondition(XmlNode conditionNode)
        {
            Enums.WEATHER_CONDITION? result = null;
            if (conditionNode != null)
            {
                return parseWeatherCondition(conditionNode.Value);
            }
            return result;
        }

        private Enums.WEATHER_CONDITION? parseWeatherCondition(string condition)
        {
            Enums.WEATHER_CONDITION? result = null;
            if (!string.IsNullOrWhiteSpace(condition))
            {
                string conditionLower = condition.ToLower().Replace(" ", string.Empty);
                Enums.WEATHER_CONDITION cond;
                bool ok = Enum.TryParse(conditionLower, out cond);
                if (ok)
                {
                    result = cond;
                }
                else
                {
                    string errorMsg = "Could not parse weather condition <" + condition +
                                      "> into BusinessLogic_v3.Enums.WEATHER_CONDITION.";
                    MaltaAirportWeatherFeedException ex = new MaltaAirportWeatherFeedException(errorMsg);
                    CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Error, ex);
                }
            }
            return result;
        }

        private MaltaAirportForeignCities getForeignCitiesFromForeignCitiesNode(XmlNode foreignCitiesNode, DateTime weatherReportDate)
        {
            MaltaAirportForeignCities result = new MaltaAirportForeignCities();
            List<MaltaAirportCity> cities = new List<MaltaAirportCity>();
            XmlNodeList citiesNodes = foreignCitiesNode.SelectNodes("city");
            foreach (XmlNode cityNode in citiesNodes)
            {
                MaltaAirportCity city = new MaltaAirportCity();
                city.Name = cityNode.Attributes["name"].Value;
                double tempVal;
                XmlNode valueNode = cityNode.SelectSingleNode("temperature").ChildNodes[0];
                if (valueNode != null && double.TryParse(valueNode.Value, out tempVal))
                {
                    city.Temperature = tempVal;
                }
                city.CityOutlook = parseWeatherCondition(cityNode.SelectSingleNode("cityoutlook").ChildNodes[0]);

                cities.Add(city);
            }
            result.Cities = cities;
            return result;
        }

        private MaltaAirportWindDirection getWindDirectionsFromWindDirectionNode(XmlNode windDirectionNode, DateTime weatherReportDate)
        {
            MaltaAirportWindDirection result = new MaltaAirportWindDirection();
            result.Localities = getLocalities(windDirectionNode);
            return result;
        }

        private MaltaAirportWindSpeed getWindSpeedsFromWindSpeedNode(XmlNode windSpeedNode, DateTime weatherReportDate)
        {
            MaltaAirportWindSpeed result = new MaltaAirportWindSpeed();
            result.Localities = getLocalities(windSpeedNode);
            return result;
        }

        private MaltaAirportTemperature getTemperaturesFromTemperatureNode(XmlNode temperatureNode, DateTime weatherReportDate)
        {
            MaltaAirportTemperature result = new MaltaAirportTemperature();
            result.Localities = getLocalities(temperatureNode);
            return result;
        }

        private MaltaAirportPrecipitation getPrecipitationsFromPrecipitationNode(XmlNode precipitationNode, DateTime weatherReportDate)
        {
            MaltaAirportPrecipitation result = new MaltaAirportPrecipitation();
            result.Localities = getLocalities(precipitationNode);
            return result;
        }

        private List<MaltaAirportLocality> getLocalities(XmlNode node)
        {
            List<MaltaAirportLocality> localities = new List<MaltaAirportLocality>();
            XmlNodeList items = node.SelectNodes("locality");
            foreach (XmlNode item in items)
            {
                MaltaAirportLocality locality = new MaltaAirportLocality();
                locality.Name = item.Attributes["name"].Value;
                if (locality.Name == "averagemalta")
                {
                    locality.Name = "Average Malta";
                }
                else if (locality.Name == "averagegozo")
                {
                    locality.Name = "Average Gozo";
                }
                locality.Name = CS.General_v3.Util.Text.CapitaliseFirstLetterOfEachWord(locality.Name);
                double value;
                XmlNode valueNode = item.ChildNodes[0];
                if (valueNode != null && double.TryParse(valueNode.Value, out value))
                {
                    locality.Value = value;
                }
                if (!string.IsNullOrWhiteSpace(locality.Name) && valueNode != null)
                {
                    localities.Add(locality);
                }
            }
            return localities;
        }

        private MaltaAirportRainfall getRainfallsFromRainfallNode(XmlNode rainfallNode, DateTime weatherReportDate)
        {
            MaltaAirportRainfall result = new MaltaAirportRainfall();
            result.Localities = getLocalities(rainfallNode);
            return result;
        }

        private MaltaAirportOutlook getOutlookFromOutlookNode(XmlNode outlookNode, DateTime weatherReportDate)
        {
            MaltaAirportOutlook outlook = new MaltaAirportOutlook();
            string time = outlookNode.SelectSingleNode("too").ChildNodes[0].Value;
            int hours = int.Parse(time.Substring(0, 2));
            int minutes = int.Parse(time.Substring(3, 2));
            outlook.IssueTime = new DateTime(weatherReportDate.Year, weatherReportDate.Month, weatherReportDate.Day,
                                                 hours, minutes, 0);
            outlook.OutlookUntil = outlookNode.SelectSingleNode("outlookuntil").ChildNodes[0].Value;
            XmlNode detailsNode = outlookNode.SelectSingleNode("details").ChildNodes[0];
            if (detailsNode != null)
            {
                outlook.Details = detailsNode.Value;
            }
            XmlNode outlookInMalteseNode = outlookNode.SelectSingleNode("moutlook").ChildNodes[0];
            if (outlookInMalteseNode != null)
            {
                string outlookInMaltese = outlookInMalteseNode.Value;

                if (!string.IsNullOrWhiteSpace(outlookInMaltese))
                {
                    outlook.OutlookInMaltese = HttpUtility.HtmlDecode(outlookInMaltese);
                }
            }
            outlook.Warnings = outlookNode.SelectSingleNode("warnings").ChildNodes[0].Value;
            outlook.Interference = outlookNode.SelectSingleNode("interference").ChildNodes[0].Value;
            outlook.Weather = outlookNode.SelectSingleNode("weather").ChildNodes[0].Value;
            outlook.Visibility = outlookNode.SelectSingleNode("visibility").ChildNodes[0].Value;
            outlook.OutlookWind = outlookNode.SelectSingleNode("outlookwind").ChildNodes[0].Value;
            outlook.Sea = outlookNode.SelectSingleNode("sea").ChildNodes[0].Value;
            outlook.Swell = outlookNode.SelectSingleNode("swell").ChildNodes[0].Value;
            double seaTemp;
            if (double.TryParse(outlookNode.SelectSingleNode("seatemperature").ChildNodes[0].Value, out seaTemp))
            {
                outlook.SeaTemp = seaTemp;
            }
            string warningsInMaltese = outlookNode.SelectSingleNode("mwarnings").ChildNodes[0].Value;
            if (!string.IsNullOrWhiteSpace(warningsInMaltese))
            {
                outlook.WarningsInMaltese = HttpUtility.HtmlDecode(warningsInMaltese);
            }
            string interferenceInMaltese = outlookNode.SelectSingleNode("minterference").ChildNodes[0].Value;
            if (!string.IsNullOrWhiteSpace(interferenceInMaltese))
            {
                outlook.InterferenceInMaltese = HttpUtility.HtmlDecode(interferenceInMaltese);
            }
            string weatherInMaltese = outlookNode.SelectSingleNode("mweather").ChildNodes[0].Value;
            if (!string.IsNullOrWhiteSpace(weatherInMaltese))
            {
                outlook.WeatherInMaltese = HttpUtility.HtmlDecode(weatherInMaltese);
            }
            string visibilityInMaltese = outlookNode.SelectSingleNode("mvisibility").ChildNodes[0].Value;
            if (!string.IsNullOrWhiteSpace(visibilityInMaltese))
            {
                outlook.VisibilityInMaltese = HttpUtility.HtmlDecode(visibilityInMaltese);
            }
            string outlookWindInMaltese = outlookNode.SelectSingleNode("moutlookwind").ChildNodes[0].Value;
            if (!string.IsNullOrWhiteSpace(outlookWindInMaltese))
            {
                outlook.OutlookWindInMaltese = HttpUtility.HtmlDecode(outlookWindInMaltese);
            }
            string seaInMaltese = outlookNode.SelectSingleNode("msea").ChildNodes[0].Value;
            if (!string.IsNullOrWhiteSpace(seaInMaltese))
            {
                outlook.SeaInMaltese = HttpUtility.HtmlDecode(seaInMaltese);
            }
            string swellInMaltese = outlookNode.SelectSingleNode("mswell").ChildNodes[0].Value;
            if (!string.IsNullOrWhiteSpace(swellInMaltese))
            {
                outlook.SwellInMaltese = HttpUtility.HtmlDecode(swellInMaltese);
            }
            double todayHigh;
            if (double.TryParse(outlookNode.SelectSingleNode("todayhigh").ChildNodes[0].Value, out todayHigh))
            {
                outlook.TodayMaxTemp = todayHigh;
            }
            double todayLow;
            if (double.TryParse(outlookNode.SelectSingleNode("todaylow").ChildNodes[0].Value, out todayLow))
            {
                outlook.TodayMinTemp = todayLow;
            }
            double uvindex;
            if (double.TryParse(outlookNode.SelectSingleNode("uvindex").ChildNodes[0].Value, out uvindex))
            {
                outlook.UVIndex = uvindex;
            }
            double heatstress;
            if (double.TryParse(outlookNode.SelectSingleNode("heatstress").ChildNodes[0].Value, out heatstress))
            {
                outlook.HeatStress = heatstress;
            }
            return outlook;
        }

        private IEnumerable<MaltaAirportOutlookMariners> getOutlookMarinersFromOutlookMarinersNodeList(XmlNodeList outlookMarinersNodes, DateTime weatherReportDate)
        {
            List<MaltaAirportOutlookMariners> result = new List<MaltaAirportOutlookMariners>();
            foreach (XmlNode n in outlookMarinersNodes)
            {
                MaltaAirportOutlookMariners newobj = new MaltaAirportOutlookMariners();
                string dateApplicable = n.Attributes["date"].Value;
                int year = int.Parse(dateApplicable.Substring(0, 4));
                int month = int.Parse(dateApplicable.Substring(5, 2));
                int day = int.Parse(dateApplicable.Substring(8, 2));
                newobj.Date = new DateTime(year, month, day);
                XmlNode warningNode = n.SelectSingleNode("warning");
                newobj.Warning = warningNode != null ? warningNode.ChildNodes[0].Value : null;
                XmlNode situationNode = n.SelectSingleNode("situation");
                newobj.Situation = situationNode != null ? situationNode.ChildNodes[0].Value : null;
                newobj.Weather = n.SelectSingleNode("weather").ChildNodes[0].Value;
                newobj.Visibility = n.SelectSingleNode("visibility").ChildNodes[0].Value;
                newobj.OutlookWind = n.SelectSingleNode("outlookwind").ChildNodes[0].Value;
                newobj.Sea = n.SelectSingleNode("sea").ChildNodes[0].Value;
                double seatemp;
                XmlNode seaTempNode = n.SelectSingleNode("seatemperature");
                if (seaTempNode != null && double.TryParse(seaTempNode.ChildNodes[0].Value, out seatemp))
                {
                    newobj.SeaTemp = seatemp;
                }
                newobj.Swell = n.SelectSingleNode("swell").ChildNodes[0].Value;
                result.Add(newobj);
            }
            return result;
        }

        private MaltaAirportObservations getObservationsFromObservationsNode(XmlNode observationsNode, DateTime weatherReportDate)
        {
            MaltaAirportObservations observations = new MaltaAirportObservations();
            double seaPressure;
            if (double.TryParse(observationsNode.SelectSingleNode("seapressure").ChildNodes[0].Value, out seaPressure))
            {
                observations.SeaPressure = seaPressure;
            }
            observations.ObserWind = observationsNode.SelectSingleNode("obserwind").ChildNodes[0].Value;
            double visibility;
            if (double.TryParse(observationsNode.SelectSingleNode("visibility").ChildNodes[0].Value, out visibility))
            {
                observations.Visibility = visibility;
            }
            observations.Clouds = observationsNode.SelectSingleNode("clouds").ChildNodes[0].Value;
            double humidity;
            if (double.TryParse(observationsNode.SelectSingleNode("humidity").ChildNodes[0].Value, out humidity))
            {
                observations.Humidity = humidity;
            }
            double sunhours;
            if (double.TryParse(observationsNode.SelectSingleNode("sunhours").ChildNodes[0].Value, out sunhours))
            {
                observations.SunHours = sunhours;
            }
            double maxday;
            if (double.TryParse(observationsNode.SelectSingleNode("maxday").ChildNodes[0].Value, out maxday))
            {
                observations.MaxShadeTemp = maxday;
            }
            double minnight;
            if (double.TryParse(observationsNode.SelectSingleNode("minnight").ChildNodes[0].Value, out minnight))
            {
                observations.MinAirTempPrevNight = minnight;
            }
            double seatemp;
            if (double.TryParse(observationsNode.SelectSingleNode("seatemp").ChildNodes[0].Value, out seatemp))
            {
                observations.SeaTemp = seatemp;
            }
            observations.Moon = observationsNode.SelectSingleNode("moon").ChildNodes[0].Value;
            return observations;
        }

        private MaltaAirportForecast getForecastFromForecastNode(XmlNode forecastNode, DateTime weatherReportDate)
        {
            MaltaAirportForecast forecast = new MaltaAirportForecast();
            XmlNode timeNode = forecastNode.SelectSingleNode("time");
            string time = timeNode.ChildNodes[0].Value;
            int hours = int.Parse(time.Substring(0, 2));
            int minutes = int.Parse(time.Substring(3, 2));
            int seconds = int.Parse(time.Substring(6, 2));
            forecast.Time = new DateTime(weatherReportDate.Year, weatherReportDate.Month, weatherReportDate.Day,
                                                 hours, minutes, seconds);
            forecast.ForecastValid = forecastNode.SelectSingleNode("forecastvalid").ChildNodes[0].Value;
            forecast.ConditionToday =
                parseWeatherCondition(forecastNode.SelectSingleNode("conditiontoday").ChildNodes[0]);
            double temp;
            if (double.TryParse(forecastNode.SelectSingleNode("temperature").ChildNodes[0].Value, out temp))
            {
                forecast.Temperature = temp;
            }
            double humidity;
            if (double.TryParse(forecastNode.SelectSingleNode("humidity").ChildNodes[0].Value, out humidity))
            {
                forecast.Humidity = humidity;
            }
            double pressure;
            if (double.TryParse(forecastNode.SelectSingleNode("atmospressure").ChildNodes[0].Value, out pressure))
            {
                forecast.AtmosphericPressure = pressure;
            }
            forecast.Wind = forecastNode.SelectSingleNode("wind").ChildNodes[0].Value;
            XmlNode sunRise = forecastNode.SelectSingleNode("sunrise");
            string sunrisetime = sunRise.ChildNodes[0].Value;
            hours = int.Parse(sunrisetime.Substring(0, 2));
            minutes = int.Parse(sunrisetime.Substring(3, 2));
            seconds = int.Parse(sunrisetime.Substring(6, 2));
            forecast.Sunrise = new DateTime(weatherReportDate.Year, weatherReportDate.Month, weatherReportDate.Day,
                                                 hours, minutes, seconds);
            XmlNode sunset = forecastNode.SelectSingleNode("sunset");
            string sunsetime = sunRise.ChildNodes[0].Value;
            hours = int.Parse(sunsetime.Substring(0, 2));
            minutes = int.Parse(sunsetime.Substring(3, 2));
            seconds = int.Parse(sunsetime.Substring(6, 2));
            forecast.Sunset = new DateTime(weatherReportDate.Year, weatherReportDate.Month, weatherReportDate.Day,
                                                 hours, minutes, seconds);
            forecast.ForecastImagePath = forecastNode.SelectSingleNode("forecastimage").ChildNodes[0].Value;
            XmlNodeList dayForecastNodes = forecastNode.SelectNodes("dayforecast");
            List<MaltaAirportDayForecast> dayForecasts = new List<MaltaAirportDayForecast>();
            foreach (XmlNode day in dayForecastNodes)
            {
                MaltaAirportDayForecast dayForecast = new MaltaAirportDayForecast();
                XmlNode dateNode = day.SelectSingleNode("forecastdate");
                string dateApplicable = dateNode.ChildNodes[0].Value;
                int year = int.Parse(dateApplicable.Substring(0, 4));
                int month = int.Parse(dateApplicable.Substring(5, 2));
                int dayval = int.Parse(dateApplicable.Substring(8, 2));
                dayForecast.ForecastDate = new DateTime(year, month, dayval);

                dayForecast.Condition = parseWeatherCondition(day.SelectSingleNode("condition").ChildNodes[0]);
                double highestTemp;
                if (double.TryParse(day.SelectSingleNode("high").ChildNodes[0].Value, out highestTemp))
                {
                    dayForecast.HighestTemperatureInC = highestTemp;
                }
                double lowest;
                if (double.TryParse(day.SelectSingleNode("low").ChildNodes[0].Value, out lowest))
                {
                    dayForecast.LowestTemperatureInC = lowest;
                }
                double heatstress;
                if (double.TryParse(day.SelectSingleNode("heat_stress").ChildNodes[0].Value, out heatstress))
                {
                    dayForecast.HeatStressIndexInC = heatstress;
                }
                dayForecast.Wind = day.SelectSingleNode("wind_direction").ChildNodes[0].Value;
                double uvindex;
                if (double.TryParse(day.SelectSingleNode("uvindex").ChildNodes[0].Value, out uvindex))
                {
                    dayForecast.UVIndex = uvindex;
                }
                dayForecast.ForecastImagePath = day.SelectSingleNode("forecastimage").ChildNodes[0].Value;
                dayForecasts.Add(dayForecast);
            }
            forecast.DayForecasts = dayForecasts;
            return forecast;
        }
    }
}
