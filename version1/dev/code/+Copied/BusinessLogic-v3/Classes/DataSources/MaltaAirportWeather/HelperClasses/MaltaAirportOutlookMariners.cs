﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataSources.MaltaAirportWeather.HelperClasses
{
    public class MaltaAirportOutlookMariners
    {
        public DateTime? Date { get; set; }
        public string Warning { get; set; }
        public string Situation { get; set; }
        public string Weather { get; set; }
        public string Visibility { get; set; }
        public string OutlookWind { get; set; }
        public string Sea { get; set; }
        public double? SeaTemp { get; set; }
        public string Swell { get; set; }
    }
}
