﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataSources.MaltaAirportWeather.HelperClasses
{
    public class MaltaAirportObservations
    {
        public double? SeaPressure { get; set; }
        public string ObserWind { get; set; }
        public double? Visibility { get; set; }
        public string Clouds { get; set; }
        public double? Humidity { get; set; }
        public double? SunHours { get; set; }
        public double? MaxShadeTemp { get; set; }
        public double? MinAirTempPrevNight { get; set; }
        public double? SeaTemp { get; set; }
        public string Moon { get; set; }
    }
}
