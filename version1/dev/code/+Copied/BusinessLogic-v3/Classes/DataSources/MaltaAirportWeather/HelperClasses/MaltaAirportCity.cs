﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataSources.MaltaAirportWeather.HelperClasses
{
    public class MaltaAirportCity
    {
        public string Name { get; set; }
        public double? Temperature { get; set; }
        public Enums.WEATHER_CONDITION? CityOutlook { get; set; }
    }
}
