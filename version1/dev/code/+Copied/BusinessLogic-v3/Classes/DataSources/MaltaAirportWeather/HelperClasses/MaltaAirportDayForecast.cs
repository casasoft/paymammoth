﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataSources.MaltaAirportWeather.HelperClasses
{
    public class MaltaAirportDayForecast
    {
        public DateTime? ForecastDate { get; set; }
        public Enums.WEATHER_CONDITION? Condition { get; set; }
        public double? HighestTemperatureInC { get; set; }
        public double? LowestTemperatureInC { get; set; }
        public double? HeatStressIndexInC { get; set; }
        public string Wind { get; set; }
        public string ForecastImagePath { get; set; }
        public double? UVIndex { get; set; }
    }
}
