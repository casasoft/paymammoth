﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.DataSources
{
    public interface IWeatherReport
    {
        double? MaximumTemperature { get; }
        double? MinimumTemperature { get; }
        double? CurrentTemperature { get; }
        DateTime? Date { get; }
        Enums.WEATHER_CONDITION? Condition { get; }
        Enums.WEATHER_SOURCE DataSource { get; }
    }
}
