﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.CategoryModule;



namespace BusinessLogic_v3.Classes.Categories
{
    using CS.General_v3.Classes.Interfaces.Hierarchy;

    /// <summary>
    /// Generates a list of hierarchies based on a given list of items.  Each items will have as children the list of subcategories assigned by calling 'SetCategories', which are filtered based on which
    /// have items existing in database or not.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FilterCategoriesBasedOnObjects<TItem, TChildCategory> 
        where TItem : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
        where TChildCategory : CategoryWithFilteredChildren<TItem>, new()
    {
        public delegate bool CategoryContainsItemsDelegate(TItem item, CategoryBase category);

        



        public FilterCategoriesBasedOnObjects()
        {
            
            
        }
        /// <summary>
        /// Sets the list of categories to filter
        /// </summary>
        /// <param name="categories"></param>
        public void SetCategories(IEnumerable<CategoryBase> categories)
        {
            this.categories = categories;
        }

        private void checkCategories()
        {
            if (this.categories == null) //if categories are null, load the root categories
            {
                CategoryBase root = CategoryBaseFactory.Instance.GetRootCategory();
                this.categories = root.GetChildCategories();

            }
        }

        private IEnumerable<CategoryBase> categories = null;

        public event CategoryContainsItemsDelegate CategoryContainsItemCheck;

        private List<TItem> items = null;

        private List<TChildCategory> result = null;


        private void parseCategory(IHierarchyEditable parent, TItem item, CategoryBase category)
        {

            bool ok = this.CategoryContainsItemCheck(item, category);
            if (ok)
            {
                TChildCategory subCat = new TChildCategory();
                subCat.Init(category, parent, item);

                parent.AddChild(subCat);
                foreach (var child in category.GetChildCategories())
                {
                    parseCategory(subCat, item, child);
                }
            }
        }

        private TChildCategory filterItem(TItem item)
        {
            TChildCategory cat = new TChildCategory();
            cat.Init(null, null, item);
            //ItemWithFilteredChildCategories<TItem> cat = new ItemWithFilteredChildCategories<TItem>(item, item.ToString());
            var catList = this.categories.ToList();
            for (int i = 0; i < catList.Count; i++)
            {

                var c = catList[i];
                parseCategory(cat, item, c);
            }

            return cat;

        }



        public List<TChildCategory> FilterItems(IEnumerable<TItem> itemsToFilter)
        {
            checkCategories();
            result = new List<TChildCategory>();
            this.items = itemsToFilter.ToList();
            for (int i=0; i < items.Count;i++)
            
            {
                var item = items[i];
                if (item != null)
                {
                    var filteredItem = filterItem(item);
                    result.Add(filteredItem);
                }
            }

            return result;


        }

    }
}
