﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;

using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;

namespace BusinessLogic_v3.Classes.Categories
{
    /// <summary>
    /// Returns a list of brands as hierarchies, with sub categories filtered by which really have items in database. 
    /// </summary>
    public class FilterCategoriesByBrandCached 
    {
        private static readonly object _lock = new object();
        public static FilterCategoriesByBrandCached Instance
        {
            get 
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<FilterCategoriesByBrandCached>(null);
            }
        }
        private bool isRequestedFirstTime = false;
        public FilterCategoriesByBrandCached()
        {
            this.Categories = new List<CategoryBase>();
            this.Brands = new List<BrandBase>();
            CategoryBaseFactory.Instance.OnItemUpdate +=new BusinessLogic_v3.Classes.DbObjects.General.OnItemUpdateHandler(Instance_OnItemUpdate);
            BrandBaseFactory.Instance.OnItemUpdate += new BusinessLogic_v3.Classes.DbObjects.General.OnItemUpdateHandler(Instance_OnItemUpdate);
           // _itemGroupsByBrand = new Dictionary<long, List<ProductBase>>();
        }

        public List<CategoryBase> Categories { get; private set; }
        public List<BrandBase> Brands { get; private set; }
        public void InitCache()
        {
            resetCache();
        }
        private void resetCache()
        {
            lock (_lock)
            {
                NHClasses.NhManager.CreateNewSessionForContext();
                FilterCategoriesByBrand filter = new FilterCategoriesByBrand();
                var catList = this.Categories.ConvertAll(item => CategoryBase.Factory.GetByPrimaryKey(item.ID));
                filter.SetCategories(catList);
                
                cachedResult = filter.GetFilteredBrandsAsHierarchy(this.Brands);
                NHClasses.NhManager.DisposeCurrentSessionInContext();
            }

        }

        private List<BrandWithCategories> cachedResult = null;

        public List<BrandWithCategories> GetAllFilteredBrands()
        {
            lock (_lock)
            {
                
                if (cachedResult == null)
                {
                    resetCache();
                }

                List<BrandWithCategories> list = new List<BrandWithCategories>();
                foreach (var item in cachedResult)
                {
                    item.ReloadFromDb();
                    list.Add(item);
                }
                isRequestedFirstTime = true;
                return list;
            }


        }



        void Instance_OnItemUpdate(IBaseDbObject item, Enums.UPDATE_TYPE updateType)
        {
            cachedResult = null;
            if (isRequestedFirstTime)
            {
                CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(resetCache, "FilterCategoriesByBrandCached-OnItemUpdateResetCache", System.Threading.ThreadPriority.Lowest);
            }


        }






    }
}
