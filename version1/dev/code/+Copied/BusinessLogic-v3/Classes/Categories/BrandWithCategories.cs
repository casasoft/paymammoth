﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.URL.ListingURLParser;
using BusinessLogic_v3.Modules.BrandModule;

namespace BusinessLogic_v3.Classes.Categories
{
    public class BrandWithCategories : CategoryWithFilteredChildren<BrandBase>
    {
        public BrandWithCategories()
        {

        }

        protected override string getUrl()
        {
            ListingURLParserShop url = new ListingURLParserShop(null, null, false);
            if (this.Category != null)
            {
                url.ListingParameters.Categories.Add(this.Category);
            }
            url.ListingParameters.Brands.Add(this.Item);
            return url.GetUrl();
            
        }

        public override void ReloadFromDb()
        {
            if (this.Item != null)
                this.Item = BrandBase.Factory.GetByPrimaryKey(this.Item.ID);
            base.ReloadFromDb();
        }

        public override string Title
        {
            get
            {
                if (this.Category == null)
                    return this.Item.Title;
                else
                    return base.Title;
            }
            set
            {
                base.Title = value;
            }
        }

    }
}
