﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryModule;

using BusinessLogic_v3.Modules.ProductModule;

namespace BusinessLogic_v3.Classes.Categories
{
    /// <summary>
    /// Returns a list of brands as hierarchies, with sub categories filtered by which really have items in database.
    /// </summary>
    public class FilterCategoriesByBrand : FilterCategoriesBasedOnObjects<BrandBase, BrandWithCategories>
    {
      
        public FilterCategoriesByBrand()
           
        {
            this.CategoryContainsItemCheck += new CategoryContainsItemsDelegate(BrandsCategoryFilter_CategoryContainsItemCheck);
           

        }


        private Dictionary<long, List<ProductBase>> _itemGroupsByBrand = null;



        
        private void loadProducts()
        {
            _itemGroupsByBrand = new Dictionary<long, List<ProductBase>>();
            var items = ProductBaseFactory.Instance.FindAll();

            foreach (var item in items)
            {
                var brand = item.Brand;
                long brandID = 0;
                if (brand != null)
                    brandID = brand.ID;

                List<ProductBase> list = null;
                if (_itemGroupsByBrand.ContainsKey(brandID))
                {
                    list = _itemGroupsByBrand[brandID];
                }
                else
                {
                    list = new List<ProductBase>();
                    _itemGroupsByBrand[brandID] = list;
                }
                list.Add(item);

            }
            

        }
        private List<ProductBase> getProductsByBrand(BrandBase brand)
        {
            if (_itemGroupsByBrand == null)
                loadProducts();
            if (_itemGroupsByBrand.ContainsKey(brand.ID))

                return _itemGroupsByBrand[brand.ID];
            else
                return new List<ProductBase>();
        }




        protected bool checkIfCategoryContainsItemsByBrand(CategoryBase category, BrandBase brand)
        {
//            var subcategoriesList = category.GetAllSubcategoriesAsFlatList<CategoryBase>();

            bool ok = false;
            var itemsByBrand = getProductsByBrand(brand);
            for (int i = 0; i < itemsByBrand.Count; i++)
            {
                var item = itemsByBrand[i];

                if (item.HasCategoryAsItsParent(category))
                {
                    ok = true;
                    break;
                }
                if (ok)
                {
                    break;
                }
            }
            return ok;

        }

        bool BrandsCategoryFilter_CategoryContainsItemCheck(BrandBase brand, CategoryBase category)
        {
            return checkIfCategoryContainsItemsByBrand(category, brand);
            

        }



        public List<BrandWithCategories> GetFilteredBrandsAsHierarchy(IEnumerable<BrandBase> brands)
        {
            if (brands == null)
                brands = BrandBaseFactory.Instance.FindAll();


            var result = base.FilterItems(brands);
            _itemGroupsByBrand = null;
              
            return result;
           

        }











    }
}
