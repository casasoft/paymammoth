﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.Interfaces.Hierarchy;



namespace BusinessLogic_v3.Classes.Categories
{
    public class ItemWithFilteredChildCategories<T> : IHierarchyEditable
        where T : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
        public ItemWithFilteredChildCategories(T item, string title)
        {
            this.Item = item;
            this.Title = title;
            this.HrefTarget = CS.General_v3.Enums.HREF_TARGET.Self;
            this.Visible = true;
            this.children = new List<CategoryWithFilteredChildren<T>>();
        }
        
        public string Title {get;set;}

        private T _item;
        public T Item
        {
            get 
            {
                if (_item != null) _item.Merge();
                return _item; 
            }

            set { _item = value; }
        }

        #region IHierarchy Members

        public long ID
        {
            get {return this.Item.ID; }
        }

        public string Href { get; set; }

        public CS.General_v3.Enums.HREF_TARGET HrefTarget { get; set; }

        private List<CategoryWithFilteredChildren<T>> children = null;

        public IEnumerable<CategoryWithFilteredChildren<T>> GetChildren()
        {
            return children;
            
        }

        public IHierarchy ParentHierarchy
        {
            get { return null; }
        }

        public bool Selected
        {
            get { return false; }
        }

        public string GetFullHierarchyName(string delimiter, bool reverseOrder, bool includeRoot)
        {
            return Title;
            
        }
        public void ReloadFromDb()
        {
            long id = this.Item.ID;
            
                

            foreach (var child in this.GetChildren())
            {
                child.ReloadFromDb();
            }
        }
        public bool Visible { get; set; }

        #endregion

        #region IHierarchyEditable Members

        void IHierarchyEditable.AddChild(IHierarchy hierarchy)
        {
            this.children.Add((CategoryWithFilteredChildren<T>)hierarchy);
            
        }

        void IHierarchyEditable.AddParent(IHierarchy hierarchy)
        {
            throw new NotImplementedException("This should never be called");
        }

        #endregion

        public override string ToString()
        {
            return this.Title;
        }

        #region IHierarchy Members


        IEnumerable<IHierarchy> IHierarchy.GetChildren()
        {
            return this.GetChildren();
            
        }

        #endregion

        #region IHierarchyEditable Members


        public void RemoveChild(IHierarchy child)
        {
            throw new NotImplementedException();
            
        }

        public void RemoveParent(IHierarchy parent)
        {
            
            throw new NotImplementedException();
        }

        IEnumerable<IHierarchyEditable> IHierarchyEditable.GetChildren()
        {
            return this.GetChildren();
            
        }

        IEnumerable<IHierarchyEditable> IHierarchyEditable.GetParents()
        {
            return (IEnumerable<IHierarchyEditable>) CS.General_v3.Util.ListUtil.GetListFromSingleItem(this.ParentHierarchy);
        }

        #endregion

        #region IHierarchy Members


        IEnumerable<IHierarchy> IHierarchy.GetParents()
        {
            return CS.General_v3.Util.ListUtil.GetListFromSingleItem(this.ParentHierarchy);
           
        }

        #endregion

        #region BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject Members

        public bool Deleted
        {
            get
            {
                return this.Item.Deleted;
                
            }
            set
            {
                this.Item.Deleted = value;
            }
        }

        public DateTime _Temporary_LastUpdOn
        {
            get
            {
                return this.Item._Temporary_LastUpdOn;

            }
            set
            {
                this.Item._Temporary_LastUpdOn = value;
            }
        }

        public bool _Temporary_Flag
        {
            get
            {
                return this.Item._Temporary_Flag;

            }
            set
            {
                this.Item._Temporary_Flag = value;
            }
        }

        public DateTime? DeletedOn
        {
            get
            {
                return this.Item.DeletedOn;

            }
            set
            {
                this.Item.DeletedOn = value;
            }
        }


        public int Priority
        {
            get
            {
                return this.Item.Priority;

            }
            set
            {
                this.Item.Priority = value;
            }
        }

        public bool IsSavedInDB()
        {
            return this.Item.IsSavedInDB();
            
        }

        public bool IsTransient()
        {
            return this.Item.IsTransient();
           
            
        }

        public void MarkAsNotTemporary()
        {
            this.Item.MarkAsNotTemporary();
            
        }

        public void MarkAsTemporary()
        {
            this.Item.MarkAsTemporary();
            
        }

        public void InitPropertiesForNewItem()
        {
            this.Item.InitPropertiesForNewItem();
            
        }

        public void resetDirtyFlag()
        {
            this.Item.resetDirtyFlag();
            
        }

        public void Evict(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            this.Item.Evict(session);
            
        }




        public void ReloadFromDb(MyNHSessionBase session = null)
        {
            this.Item.ReloadFromDb(session);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult Create(SaveParams sParams = null)
        {
            return this.Item.Create(sParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult CreateAndCommit(SaveParams sParams = null)
        {
            return this.Item.CreateAndCommit(sParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult Save(SaveParams sParams = null)
        {
            return this.Item.Save(sParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult SaveAndCommit(SaveParams sParams = null)
        {
            return this.Item.SaveAndCommit(sParams);
        }

        //public CS.General_v3.Classes.HelperClasses.OperationResult SaveCopy(SaveParams sParams = null)
        //{
        //    return this.Item.SaveCopy(sParams);
        //}

        //public CS.General_v3.Classes.HelperClasses.OperationResult SaveCopyAndFlush(SaveParams sParams = null)
        //{
        //    return this.Item.SaveCopyAndFlush(sParams);
        //}

        public CS.General_v3.Classes.HelperClasses.OperationResult Update(SaveParams sParams = null)
        {
            return this.Item.Update(sParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult UpdateAndCommit(SaveParams sParams = null)
        {
            return this.Item.UpdateAndCommit(sParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult Delete(DeleteParams delParams = null)
        {
            return this.Item.Delete(delParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult DeleteAndCommit(DeleteParams delParams = null)
        {
            return this.Item.DeleteAndCommit(delParams);
        }

        public string GetOutputCacheDependencyKey()
        {
            return this.Item.GetOutputCacheDependencyKey();
        }

        #endregion

        #region IBaseDbObject Members


        public DateTime? LastEditedOn
        {
            get
            {
                return this.Item.LastEditedOn;
                
            }
            set
            {
                this.Item.LastEditedOn = value;
                
            }
        }

        public void MarkAsDirty()
        {
            this.Item.MarkAsDirty();
            
        }

        public void Merge(MyNHSessionBase session = null)
        {
            this.Item.Merge(session);
            
        }

        public void DeleteTemporaryItemAsTimeElapsed()
        {
            this.Item.DeleteTemporaryItemAsTimeElapsed();
            
        }

        #endregion

        #region IBaseDbObject Members

        public bool Published
        {
            get
            {
                return this.Item.Published;
                
            }
            set
            {
                this.Item.Published = value;
                
            }
        }

        #endregion

      

        #region IBaseDbObject Members


        public bool IsAvailableToFrontend()
        {
            return this.Item.IsAvailableToFrontend();
            
        }

        #endregion

        #region IBaseDbObject Members


        public string _TestItemSessionGuid { get { return this.Item._TestItemSessionGuid; } set { this.Item._TestItemSessionGuid = value; }}
        

            #endregion

        #region IBaseDbObject Members

        public bool _ComputedDeletedValue { get { return this.Item._ComputedDeletedValue; } set { this.Item._ComputedDeletedValue = value; } }


            #endregion

        #region IBaseDbObject Members


        public void UpdateComputedDeletedValue(bool autoSave = false)
        {
            
        }

        public event UpdateComputedDelValueHandler OnUpdateComputedDeletedValue;

        public event SaveBeforeAfterHandler OnSaveBefore;

        public event SaveBeforeAfterHandler OnSaveAfter;

        public event DeleteBeforeAfterHandler OnDeleteBefore;

        public event DeleteBeforeAfterHandler OnDeleteAfter;

        #endregion

        #region IBaseDbObject Members

        public IBaseDbFactory GetFactory()
        {
            return _item.GetFactory();
            
        }

        #endregion

        IHierarchy IHierarchy.GetMainParent()
        {
            return (IHierarchyEditable)this.ParentHierarchy;
        }
    }
}
