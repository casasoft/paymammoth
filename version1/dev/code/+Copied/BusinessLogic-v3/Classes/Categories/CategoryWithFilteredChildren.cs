﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.CmsUserModule;


using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Classes.Interfaces.Hierarchy;


namespace BusinessLogic_v3.Classes.Categories
{
    public class CategoryWithFilteredChildren<T> : IHierarchyEditable
        where T : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
        public CategoryWithFilteredChildren()
        {

          
            this._children = new List<CategoryWithFilteredChildren<T>>();
            this._parents = new List<IHierarchyEditable>();
            this.Visible = true;

            this.HrefTarget = CS.General_v3.Enums.HREF_TARGET.Self;


        }
        public void Init(CategoryBase category, IHierarchy parent, T item)
        {
            this.Item = item;
            this.AddParent(parent);
            this.SetCategory(category);
            
        }

        public virtual T Item { get; set; }

        public override string ToString()
        {
            return this.Title;
            
        }

        private CategoryBase _category;
        public CategoryBase Category
        {
            get 
            {
                if (_category != null) _category.Merge();
                return _category; 
            }
        }
        public void SetCategory(CategoryBase category)
        {
            _category = category;
            if (this.Category != null)
            {
                this.Title = this.Category.TitlePlural;
                this.Href = this.Category.ToFrontendBase().GetUrl();
                this.HrefTarget = this.Category.HrefTarget;
                this.Visible = this.Category.Published;
            }
        }

        #region IHierarchy Members

        public virtual long ID
        {
            get {
                if (this.Category == null)
                    return this.Item.ID;
                else
                    return this.Category.ID;
            }
        }

        public virtual string Title { get; set; }
        private string _Href = null;
        public virtual string Href
        {
            get 
            {
                if (_Href == null)
                {
                    _Href = getUrl();

                }
                return _Href;
            }
            set { _Href = value; }
        }

        protected virtual string getUrl()
        {
            return this.Category.ToFrontendBase().GetUrl();
            
        }

        
        public virtual CS.General_v3.Enums.HREF_TARGET HrefTarget { get; set; }

        private List<CategoryWithFilteredChildren<T>> _children = null;
        private List<IHierarchyEditable> _parents = null;
        public IEnumerable<CategoryWithFilteredChildren<T>> GetChildren()
        {
            return _children;

        }

        IEnumerable<IHierarchy> IHierarchy.GetChildren()
        {
            return this.GetChildren();
            
            
        }

        public virtual void ReloadFromDb()
        {
            if (this.Category != null)
            {
                long id = this.Category.ID;

                _category = CategoryBase.Factory.GetByPrimaryKey(id);

                foreach (var child in this.GetChildren())
                {
                    child.ReloadFromDb();
                }
            }
        }
        


        public IHierarchy ParentHierarchy
        {
            get { return this._parents.FirstOrDefault(); }
        }

        public virtual bool Selected { get; set; }

        public virtual string GetFullHierarchyName(string delimiter, bool reverseOrder, bool includeRoot)
        {
            //return this.Category.GetFullHierarchyName(delimiter, reverseOrder, includeRoot);
            return "#";
            
        }

        public virtual bool Visible { get; set; }
        #endregion

        #region IHierarchyEditable Members

        public void AddChild(IHierarchy hierarchy)
        {
            this._children.Add((CategoryWithFilteredChildren<T>)hierarchy);
            
        }

        public void AddParent(IHierarchy hierarchy)
        {
            this._parents.Add((IHierarchyEditable) hierarchy);
            
            
        }

        #endregion

        #region IHierarchyEditable Members


        public void RemoveChild(IHierarchy child)
        {
            this._children.Remove((CategoryWithFilteredChildren<T>) child);
            
        }

        public void RemoveParent(IHierarchy parent)
        {
            this._parents.Remove((IHierarchyEditable) parent);
            
        }

        IEnumerable<IHierarchyEditable> IHierarchyEditable.GetChildren()
        {
            return this._children;
            
        }

        public IEnumerable<IHierarchyEditable> GetParents()
        {
            return this._parents;
            
        }

        #endregion

        #region IHierarchy Members


        IEnumerable<IHierarchy> IHierarchy.GetParents()
        {
            return this._parents;
            
        }

        #endregion

        #region BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject Members

        public bool Deleted
        {
            get
            {
                return this._category.Deleted;
            }
            set
            {
                this._category.Deleted = value;
            }
        }

        public DateTime _Temporary_LastUpdOn
        {
            get
            {
                return this._category._Temporary_LastUpdOn;
            }
            set
            {
                this._category._Temporary_LastUpdOn = value;
            }

        }

        public bool _Temporary_Flag
        {
            get
            {
                return this._category._Temporary_Flag;
            }
            set
            {
                this._category._Temporary_Flag = value;
            }

        }

        public DateTime? DeletedOn
        {
            get
            {
                return this._category.DeletedOn;
            }
            set
            {
                this._category.DeletedOn = value;
            }
        }

        public CS.General_v3.Classes.Login.IUser DeletedBy
        {
            get
            {
                return this._category.DeletedBy;
            }
            set
            {
                this._category.DeletedBy = (CmsUserBase) value;
            }
        }

        public int Priority
        {
            get
            {
                return this._category.Priority;
            }
            set
            {
                this._category.Priority = value;
            }
        }

        public bool IsSavedInDB()
        {
            return this._category.IsSavedInDB();
        }

        public bool IsTransient()
        {
            return this._category.IsTransient();
        }

        public void MarkAsNotTemporary()
        {
            this._category.MarkAsNotTemporary();
        }

        public void MarkAsTemporary()
        {
            this._category.MarkAsTemporary();
        }




        public void Evict(MyNHSessionBase session = null)
        {
            this._category.Evict(session);
        }

        public void ReloadFromDb(MyNHSessionBase session = null)
        {
            this._category.ReloadFromDb(session);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult Create(SaveParams sParams = null)
        {
            return this._category.Create(sParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult CreateAndCommit(SaveParams sParams = null)
        {
            return this._category.CreateAndCommit(sParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult Save(SaveParams sParams = null)
        {
            return this._category.Save(sParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult SaveAndCommit(SaveParams sParams = null)
        {
            return this._category.SaveAndCommit(sParams);
        }

        //public CS.General_v3.Classes.HelperClasses.OperationResult SaveCopy(SaveParams sParams = null)
        //{
        //    return this._category.SaveCopy(sParams);
        //}

        //public CS.General_v3.Classes.HelperClasses.OperationResult SaveCopyAndFlush(SaveParams sParams = null)
        //{
        //    return this._category.SaveCopyAndFlush(sParams);
        //}

        public CS.General_v3.Classes.HelperClasses.OperationResult Update(SaveParams sParams = null)
        {
            return this._category.Update(sParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult UpdateAndCommit(SaveParams sParams = null)
        {
            return this._category.UpdateAndCommit(sParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult Delete(DeleteParams delParams = null)
        {
            return this._category.Delete(delParams);
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult DeleteAndCommit(DeleteParams delParams = null)
        {
            return this._category.DeleteAndCommit(delParams);
        }

        public void Merge(MyNHSessionBase session = null)
        {
            this._category.Merge(session);
        }

        public string GetOutputCacheDependencyKey()
        {
            return this._category.GetOutputCacheDependencyKey();
            
        }

        #endregion

        #region IBaseDbObject Members


        public DateTime? LastEditedOn
        {
            get
            {
                return this.Item.LastEditedOn;
                
            }
            set
            {
                this.Item.LastEditedOn = value;
                
            }
        }

        public void MarkAsDirty()
        {
            this.Item.MarkAsDirty();
            
        }

        public void InitPropertiesForNewItem()
        {
            this.Item.InitPropertiesForNewItem();
            
        }

        public void resetDirtyFlag()
        {
            this.Item.resetDirtyFlag();
            
        }

        public void DeleteTemporaryItemAsTimeElapsed()
        {
            this.Item.DeleteTemporaryItemAsTimeElapsed();
            
        }

        #endregion

        #region IBaseDbObject Members

        public bool Published
        {
            get
            {
                return this.Item.Published;
                
            }
            set
            {
                this.Item.Published = value;
                
            }
        }

        #endregion

        #region IBaseDbObject Members


        public CS.General_v3.Classes.Login.IUser LastEditedBy
        {
            get
            {
                return _category.LastEditedBy;
                
            }
            set
            {
                _category.LastEditedBy = (CmsUserBase) value;
                
            }
        }

        #endregion

        #region IBaseDbObject Members


        public bool IsAvailableToFrontend()
        {
            return _category.IsAvailableToFrontend();
            
        }

        #endregion

        #region IBaseDbObject Members


        public string _TestItemSessionGuid { get; set; }

        #endregion

        #region IBaseDbObject Members


        public bool _ComputedDeletedValue { get { return this.Item._ComputedDeletedValue; } set { this.Item._ComputedDeletedValue = value; } }

        #endregion

        #region IBaseDbObject Members


        public void UpdateComputedDeletedValue(bool autoSave = false)
        {
            
        }

        public event UpdateComputedDelValueHandler OnUpdateComputedDeletedValue;

        public event SaveBeforeAfterHandler OnSaveBefore;

        public event SaveBeforeAfterHandler OnSaveAfter;

        public event DeleteBeforeAfterHandler OnDeleteBefore;

        public event DeleteBeforeAfterHandler OnDeleteAfter;

        #endregion

        #region IBaseDbObject Members

        public IBaseDbFactory GetFactory()
        {
            return Item.GetFactory();
            
        }

        #endregion

        IHierarchy IHierarchy.GetMainParent()
        {
            return this._parents.FirstOrDefault();
        }
    }
}
