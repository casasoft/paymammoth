﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Lucene.Net.Analysis;
using CS.General_v3.Util;
using Lucene.Net.Store;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers;
using Lucene.Net.Documents;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.Culture;
using log4net;
using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace BusinessLogic_v3.Classes.LuceneClasses
{
    public static class LuceneUtil
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(LuceneUtil));

        private static int _pageNo = 1;
        private static int _pageSize = 1000;


        public static void RecreateAllLuceneIndexes()
        {
            var factories = BusinessLogic_v3.Classes.DbObjects.DbFactoryController.Instance.GetAllFactories().ToList();
            _log.Debug("RecreateAllLuceneIndexes() - Total Factories: " + factories.Count);
            for (int i = 0; i < factories.Count; i++)
            {

                var f = factories[i];
                f.ReCreateLuceneIndex();
            }
        }

        public static IEnumerable<long> GetResultsFromMultilingual<TCultureInfo, TMainTable>(Analyzer analyzer, Type applicableClass,
            string keywords, out int TotalResults, string indexLocation = null, int? pageNo = null, int? showAmount = null, List<long> itemIdsToANDwith = null,
             params System.Linq.Expressions.Expression<Func<TCultureInfo, string>>[] fieldPropertiesSelector)
        {
            int pgNo = pageNo.HasValue ? pageNo.Value : _pageNo;
            int pgSize = showAmount.HasValue ? showAmount.Value : _pageSize;

            HashSet<long> results = new HashSet<long>();
            TotalResults = 0;
            if (!string.IsNullOrWhiteSpace(keywords))
            {
                IndexSearcher isearcher;
                CultureDetailsBase culture = (CultureDetailsBase)DefaultCultureManager.Instance.GetCulture();

                Query querySearch = getQuery<TCultureInfo>(analyzer, applicableClass, keywords, indexLocation,
                                                             fieldPropertiesSelector, out isearcher);

                Query queryCulture = getCultureQuery(analyzer, applicableClass, indexLocation, culture.ID);

                BooleanQuery bquery = new BooleanQuery();
                bquery.Add(querySearch, BooleanClause.Occur.MUST);
                bquery.Add(queryCulture, BooleanClause.Occur.MUST);

                if (itemIdsToANDwith != null && itemIdsToANDwith.Count > 0)
                {
                    Query queryIds = getItemsIdsQuery(analyzer, applicableClass, indexLocation, itemIdsToANDwith,
                                                      getActualColumnName(typeof (TMainTable).Name));
                    bquery.Add(queryIds, BooleanClause.Occur.MUST);
                }

                ScoreDoc[] hits = isearcher.Search(bquery, null, 1000000).ScoreDocs;
                TotalResults = hits.Count();
                string columnName = getActualColumnName(typeof (TMainTable).Name);
                if (columnName.EndsWith("Base"))
                {
                    columnName = columnName.Substring(0, columnName.Length - 4);
                }
                int offset = (pgNo - 1) * pgSize;
                int count = Math.Min(pgSize, TotalResults - offset);
                for (int i = 0; i < count; i++)
                {
                    ScoreDoc hitDoc = hits[offset + i];
                    Document doc = isearcher.Doc(hitDoc.doc);
                    string values = null;
                    values = doc.Get(columnName);
                    long val = 0;
                    if (long.TryParse(values, out val))
                    {
                        results.Add(val);
                    }
                }
            }
            return results;
        }

        private static string getActualColumnName(string classNameOfTable)
        {
            if (classNameOfTable.EndsWith("Base"))
            {
                return classNameOfTable.Substring(0, classNameOfTable.Length - 4);
            }
            return classNameOfTable;
        }

        private static Query getItemsIdsQuery(Analyzer analyzer, Type applicableClass, string indexLocation, List<long> itemIdsToANDwith, string columnName)
        {
            string baseDirectory = "";
            if (!string.IsNullOrWhiteSpace(indexLocation))
            {
                baseDirectory = indexLocation;
            }
            else
            {
                baseDirectory = Constants.General.LUCENE_BASE_RELATIVE_INDEX_LOCATION;
            }
            string path = CS.General_v3.Util.PageUtil.MapPath(baseDirectory + applicableClass.Name);
            Directory directory =
                FSDirectory.Open(
                    new System.IO.DirectoryInfo(path));
            QueryParser parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, columnName, analyzer);
            string ids = CS.General_v3.Util.Text.AppendStrings(" OR ", itemIdsToANDwith.ConvertAll(item => item.ToString()));
            Query query = parser.Parse(ids);
            return query;
        }

        private static Query getCultureQuery(Analyzer analyzer, Type applicableClass, string indexLocation, long cultureId)
        {
            string baseDirectory = "";
            if (!string.IsNullOrWhiteSpace(indexLocation))
            {
                baseDirectory = indexLocation;
            }
            else
            {
                baseDirectory = Constants.General.LUCENE_BASE_RELATIVE_INDEX_LOCATION;
            }
            string path = CS.General_v3.Util.PageUtil.MapPath(baseDirectory + applicableClass.Name);
            Directory directory =
                FSDirectory.Open(
                    new System.IO.DirectoryInfo(path));
            QueryParser parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "CultureInfo", analyzer);
            Query query = parser.Parse(cultureId.ToString());
            return query;
        }

        private static Query getQuery<T>(Analyzer analyzer, Type applicableClass, string keywords, string indexLocation,
            IEnumerable<Expression<Func<T, string>>> fieldPropertiesSelector, out IndexSearcher isearcher)
        {
            HashSet<string> fields = new HashSet<string>();
            foreach (var fieldProp in fieldPropertiesSelector)
            {
                fields.Add(ReflectionUtil.GetPropertyBySelector(fieldProp).Name);
            }
            string baseDirectory = "";
            if (!string.IsNullOrWhiteSpace(indexLocation))
            {
                baseDirectory = indexLocation;
            }
            else
            {
                baseDirectory = Constants.General.LUCENE_BASE_RELATIVE_INDEX_LOCATION;
            }
            string path = CS.General_v3.Util.PageUtil.MapPath(baseDirectory + applicableClass.Name);
            Directory directory =
                FSDirectory.Open(
                    new System.IO.DirectoryInfo(path));

            isearcher = new IndexSearcher(directory, true);
            MultiFieldQueryParser parser = new MultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_29, fields.ToArray(), analyzer);
            Query query = parser.Parse(keywords);
            return query;
        }


        public static IEnumerable<long> GetResults<T>(Analyzer analyzer, Type applicableClass, string keywords, string indexLocation = null,
             params System.Linq.Expressions.Expression<Func<T, string>>[] fieldPropertiesSelector)
        {
            HashSet<long> results = new HashSet<long>();
            if (!string.IsNullOrWhiteSpace(keywords))
            {
                IndexSearcher luceneSearcher;
                Query query = getQuery<T>(analyzer, applicableClass, keywords, indexLocation, fieldPropertiesSelector,
                                            out luceneSearcher);
                ScoreDoc[] hits = luceneSearcher.Search(query, null, 1000000).ScoreDocs;
                foreach (ScoreDoc hitDoc in hits)
                {

                    Document doc = luceneSearcher.Doc(hitDoc.doc);
                    string values = null;
                    values = doc.Get(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(x => x.ID));
                    long val = 0;
                    if (long.TryParse(values, out val))
                    {
                        results.Add(val);
                    }
                }
            }
            return results;
        }
    }
}
