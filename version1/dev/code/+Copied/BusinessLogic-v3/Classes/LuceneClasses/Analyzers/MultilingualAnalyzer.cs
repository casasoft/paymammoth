﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using System.Text.RegularExpressions;
using BusinessLogic_v3.Modules.CultureDetailsModule.HelperClasses;
using System.IO;

namespace BusinessLogic_v3.Classes.LuceneClasses.Analyzers
{
    public class MultilingualAnalyzer : Lucene.Net.Analysis.Analyzer
    {
        public int Test { get; set; }
        private string getStemmerNameFromLanguage(CultureCode culture)
        {
            string name = null;
            switch (culture.Language)
            {
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.English: 
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.French: 
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Spanish: 
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Italian: 
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Portuguese:
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Romanian: 
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.German:
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Dutch: 
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Swedish: 
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Norwegian:
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Danish: 
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Russian: 
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Finnish:
                    name = CS.General_v3.Util.EnumUtils.StringValueOf(culture.Language.Value);
                    break;

                

            }
            if (name != null)
            {
               // name += "Stemmer";
            }
            return name;
        }

        private TokenStream applyStopWordsFilter(CultureCode culture, TokenStream stream)
        {
            TokenStream result = stream;
            if (culture.Language == CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.English)
            {
                result = new StopFilter(stream, Lucene.Net.Analysis.StopAnalyzer.ENGLISH_STOP_WORDS_SET);
            }
            return result;

        }
        private TokenStream applyLowercaseFilter(CultureCode culture, TokenStream stream)
        {
            TokenStream result = stream;

            result = new LowerCaseFilter(stream);
            
            return result;

        }
        private TokenStream removeAccentFilter(CultureCode culture, TokenStream stream)
        {
            TokenStream result = stream;

            result = new ISOLatin1AccentFilter(stream);
            
            return result;

        }
        private TokenStream applyStemmingFilter(CultureCode culture, TokenStream stream)
        {
            TokenStream result = stream;


            string stemmerName = getStemmerNameFromLanguage(culture);
            //Lucene.Net.Analysis.Tokenizer tokenizer = Tokenizer.
            if (!string.IsNullOrWhiteSpace(stemmerName))
            {
                
                Lucene.Net.Analysis.Snowball.SnowballFilter filter = new Lucene.Net.Analysis.Snowball.SnowballFilter( stream, stemmerName);
                result = filter;

            }
            return result;

        }

        private TextReader stripHtml(TextReader reader)
        {
            
            string s = reader.ReadToEnd();
            CS.General_v3.Util.Text.ConvertHTMLToPlainText(s, false);
            MemoryStream ms = new MemoryStream();
            StreamWriter sw = new StreamWriter(ms);
            sw.Write(s);
            sw.Flush();
            ms.Seek(0, SeekOrigin.Begin);
            StreamReader sr = new StreamReader(ms);
            return sr;


        }

        protected TextReader getCultureAndStripHtml(TextReader reader, out CultureCode code)
        {
            string s = reader.ReadToEnd();
            CultureCode culture = new CultureCode();
            code = culture;
            if (s.StartsWith("{["))
            {//this is the culture of the text
                var match = Regex.Match(s.Substring(0,10), @"\{\[(.*?)\]\}");
                if (match.Success)
                {
                    string cultureCodeStr = match.Groups[1].Value;
                    culture.ParseFromString(cultureCodeStr);
                    s = s.Substring(match.Length);


                }

            }



            CS.General_v3.Util.Text.ConvertHTMLToPlainText(s, false);
            MemoryStream ms = new MemoryStream();
            StreamWriter sw = new StreamWriter(ms);
            sw.Write(s);
            sw.Flush();
            ms.Seek(0, SeekOrigin.Begin);
            StreamReader sr = new StreamReader(ms);
            return sr;
        }

        public override Lucene.Net.Analysis.TokenStream TokenStream(string fieldName, System.IO.TextReader readerOriginal)
        {
            CultureCode culture = null;

            TextReader reader = readerOriginal;
            reader = getCultureAndStripHtml(reader, out culture);

            

            StandardTokenizer tokenizer = new StandardTokenizer(reader);
            
            TokenStream curr = tokenizer;
            curr = applyLowercaseFilter(culture, curr); // make them lowercase
          //  curr = removeAccentFilter(culture, curr); //remove accents like éç
            curr = applyStopWordsFilter(culture, curr); //remove stop words, like 'a', 'and','but'.
            curr = applyStemmingFilter(culture, curr);
            
            
            return curr;
            

            
            
            
        }
    }
}
