﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.DbObjects;


using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Classes.DataUpdates
{
    public class ProductUpdater
    {
        public static ProductUpdater Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<ProductUpdater>(null); }
        }

        private static readonly object _lock = new object();

        private bool updatePage(int pageNo)
        {
            

            var session = NHClasses.NhManager.GetCurrentSessionFromContext();
            var q = ProductBase.Factory.GetQuery();
            q.Fetch(g => g.ProductCategoryFeatureValues);
            int pageSize = 250;
            bool ok = false;
            using (var transaction = session.BeginTransaction())
            {
                
                var list = nHibernateUtil.LimitQueryByPrimaryKeysAndReturnResult(q,  pageNo, pageSize);

                foreach (var item in list)
                {
                    ok = true;
                    item.Save();
                }
                transaction.Commit();
            }

            return ok;
        }

        public void UpdateAll()
        {
            lock (_lock)
            {
                NHClasses.NhManager.CreateNewSessionForContext();
                int pageNum = 0;
                do
                {
                    pageNum++;
                } while (updatePage(pageNum));

                NHClasses.NhManager.DisposeCurrentSessionInContext();
                
            }
        }

        private ProductUpdater()
        {

        }

    }
}
