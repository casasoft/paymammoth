﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IvanAkcheurov.NTextCat.Lib.Legacy;
namespace BusinessLogic_v3.Classes.Text.LanguageClassification
{
    public class TextLanguageClassifier
    {

        private const string corpusPath = "/_common/static/data/LanguageClassification/";

        private LanguageIdentifier languageIdentifier = null;

        public TextLanguageClassifier()
        {
            string localCorpusPath = CS.General_v3.Util.PageUtil.MapPath(corpusPath);
            languageIdentifier = new LanguageIdentifier(localCorpusPath);
            
        }

        public CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? ClassifyLanguage(string txtToClassify)
        {
            CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? result = null;

            string txt = txtToClassify;

            

            List<Tuple<string, double>> languages = languageIdentifier.ClassifyText(txt, null).ToList();
            Tuple<string, double> mostCertainLanguage = languages.FirstOrDefault();

            if (mostCertainLanguage != null)
            {
                string lang = mostCertainLanguage.Item1;
                result = ConvertStringToLanguageEnum(lang);
            }
            return result;
        }

        public static CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? ConvertStringToLanguageEnum(string s)
        {
            return CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639FromCode(s, throwErrorIfCodeNotValid: false);
        }

    }
}
