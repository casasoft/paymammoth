﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.Interfaces;

using BusinessLogic_v3.Extensions;
using CS.General_v3;
using CS.General_v3.Classes.Attributes;
using System.Reflection;
using CS.General_v3.Classes.Text;
using System.Text.RegularExpressions;
namespace BusinessLogic_v3.Classes.Text
{
    public class TokenReplacerNew : ITokenReplacer, IDictionary<string,object>
    {
        private IList<IItemWithContentTags> _itemWithContentTagsList = null;
       


        private nVelocityContext nvContext = null;

        private IDictionary<string, object> _dataAsInterface
        {
            get { return nvContext; }
        }

        public TokenReplacerNew()
        {
            init();
        }
        
        public TokenReplacerNew(params IItemWithContentTags[] items)
        {
            init();
            foreach (var item in items)
            {
                AddItemWithContentTag(item);
            }
        }
        public TokenReplacerNew(params string[] tags)
        {
            init();
            for (int i = 0; i < tags.Length; i += 2)
            {
                this[tags[i]] = tags[i + 1];
            }
           
        }

        private void init()
        {
            nvContext = new nVelocityContext();
            _itemWithContentTagsList = new List<IItemWithContentTags>();
        }

        public object this[string key] 
        { 
            get { return nvContext[key]; } 
            set { SetToken(key, value); }
        }

        private string parseKey(string key)
        {
            if (key.StartsWith("[")) key = key.Substring(1);
            if (key.EndsWith("]")) key = CS.General_v3.Util.Text.RemoveLastNumberOfCharactersFromString(key, 1);
            key = key.Replace(":", "-");
            return key;

        }

        public void SetToken(string key, object value)
        {
            if (key == null) throw new InvalidOperationException("Key cannot be null");
            key = parseKey(key);
            
            nvContext[key] = value;
            checkKeyInTags(key);

        }
        private void checkKeyInTags(string key)
        {
            if (_itemWithContentTagsList != null)
            {
                foreach (var item in _itemWithContentTagsList)
                {
                    item.CheckIfItemHasTagElseCreate(key);
                }
            }
        }
        public void AddItemWithContentTag(IItemWithContentTags item)
        {
            if (item != null && !_itemWithContentTagsList.Contains(item))
            {
                _itemWithContentTagsList.Add(item);
            }
        }

        #region ITokenReplacer Members

        private string parseStringForVariablesInSquareBrackets(string s)
        {
            if (s != null)
            {
                string regexFind = @"\[([A-Za-z0-9-_]{1,25})\:([([A-Za-z0-9-_]{1,40})\]";
                string regexReplace = @"$$$1-$2";

                s = Regex.Replace(s, regexFind, regexReplace, RegexOptions.IgnoreCase | RegexOptions.Singleline);

            }
            if (s != null)
            {
                string regexFind = @"\[([A-Za-z0-9-_]{1,50})\]";
                string regexReplace = @"$$$1";

                s= Regex.Replace(s, regexFind, regexReplace, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                
            }
           
            return s;

        }

        public string ReplaceString(string s)
        {
            string result = null;
            try
            {
                s = parseStringForVariablesInSquareBrackets(s);
                result = nvContext.ParseTemplate(s);
            }
            catch (Exception parseEx)
            {
                result = s;
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("nVelocity error when parsing content for:");
                if (_itemWithContentTagsList != null)
                {
                    foreach (var item in _itemWithContentTagsList)
                    {
                        if (item != null)
                        {
                            sb.AppendLine ("  - Type: " + item.GetType().FullName + " | ID: " + item.ID + " | ToString(): " + item.ToString());
                        }
                    }
                }
                InvalidOperationException invalidEx = new InvalidOperationException(sb.ToString(), parseEx);
                CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Error,invalidEx);

            }
            return result;


        }
        protected void fillDefaultTags(string preTag = null)
        {
            this["[" + preTag + "WebsiteName]"] = CS.General_v3.Enums.SETTINGS_ENUM.Others_WebsiteName.GetSettingFromDatabase(); ;
            // this["[" + preTag + "Facebook]"] = Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook.GetSettingFromDatabase();
            this["[" + preTag + "BaseWebsiteUrl]"] = CS.General_v3.Enums.SETTINGS_ENUM.Others_WebsiteBaseUrl.GetSettingFromDatabase();

        }
        public void ReplaceInString(ref string str1)
        {
            fillDefaultTags();
            str1 = nvContext.ParseTemplate(str1);
            
        }

        public void ReplaceInString(ref string str1, ref string str2)
        {
            ReplaceInString(ref str1);
            str2 = nvContext.ParseTemplate(str2);
        }

        public void ReplaceInString(ref string str1, ref string str2, ref string str3)
        {
            ReplaceInString(ref str1);
            ReplaceInString(ref str2);
            str3 = nvContext.ParseTemplate(str3);
        }

        public void ReplaceInString(ref string str1, ref string str2, ref string str3, ref string str4)
        {
            ReplaceInString(ref str1);
            ReplaceInString(ref str2);
            ReplaceInString(ref str3);
            str4 = nvContext.ParseTemplate(str4);
        }


        #endregion


        #region IDictionary<string,object> Members

        public void Add(string key, object value)
        {
            SetToken(key, value);
            


        }

        public bool ContainsKey(string key)
        {
            return this._dataAsInterface.ContainsKey(key);

        }

        public ICollection<string> Keys
        {
            get { return this._dataAsInterface.Keys; }
        }

        public bool Remove(string key)
        {
            return _dataAsInterface.Remove(key);

        }

        public bool TryGetValue(string key, out object value)
        {
            return _dataAsInterface.TryGetValue(key, out value);

        }

        public ICollection<object> Values
        {
            get { return _dataAsInterface.Values; }
        }

        #endregion

        #region ICollection<KeyValuePair<string,object>> Members

        public void Add(KeyValuePair<string, object> item)
        {
            SetToken(item.Key, item.Value);
        }

        public void Clear()
        {
            _dataAsInterface.Clear();

        }

        public bool Contains(KeyValuePair<string, object> item)
        {
            return _dataAsInterface.Contains(item);

        }

        public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
        {
            _dataAsInterface.CopyTo(array, arrayIndex);

        }

        public int Count
        {
            get { return _dataAsInterface.Count; }
        }

        public bool IsReadOnly
        {
            get { return _dataAsInterface.IsReadOnly; }
        }

        public bool Remove(KeyValuePair<string, object> item)
        {
            return _dataAsInterface.Remove(item);
        }

        #endregion

        #region IEnumerable<KeyValuePair<string,object>> Members

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return _dataAsInterface.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_dataAsInterface).GetEnumerator();
        }

        #endregion
    }
}
