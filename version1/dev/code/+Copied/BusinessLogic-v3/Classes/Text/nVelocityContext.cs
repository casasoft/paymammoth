﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;
using System.Collections.Specialized;
using System.Collections;
using BusinessLogic_v3.Extensions;
using Iesi.Collections.Generic;


namespace BusinessLogic_v3.Classes.Text
{
    public class nVelocityContext : IDictionary<string,object>
    {
        private ConcurrentDictionary<string, object> _data = null;

        private IDictionary<string, object> _dataAsInterface
        {
            get { return _data; }
        }

        private HashedSet<IItemWithContentTags> itemWithContentTagsList = null;
        public nVelocityContext()
        {
            _data = new ConcurrentDictionary<string, object>();
            itemWithContentTagsList = new HashedSet<IItemWithContentTags>();
        }
        public void LinkToItemWithContentTags(IItemWithContentTags item)
        {
            itemWithContentTagsList.Add(item);
            
        }

        private string parseKey(string s)
        {
            string result = s ?? "";
            //result = result.ToLower();
            return result;
        }

        public object this[string key]
        {
            get { return _data[parseKey(key)]; }
            set { SetToken(key, value); }
        }

        public void SetToken(string key, object value)
        {
            _data[parseKey(key)] = value;
            checkKeyInTags(key);
        }

        private void checkKeyInTags(string key)
        {
            if (itemWithContentTagsList != null)
            {

                foreach (var item in itemWithContentTagsList)
                {
                    string tag = "$" + key;
                    item.CheckIfItemHasTagElseCreate(tag);
                }
            }
        }
        public void FillFromNameValueCollection(NameValueCollection nv)
        {
            foreach (var key in nv.AllKeys)
            {
                SetToken(key,nv[key]);
            }
        }
       

        public void FillFromDictionary(IDictionary dictionary)
        {
            foreach (var key in dictionary.Keys)
            {
                string s = null;
                if (key != null)
                    s = key.ToString();
                if (!string.IsNullOrWhiteSpace(s))
                {
                    SetToken(s, dictionary[key]);
                }
            }


        }

        public Hashtable GetAsHashTable()
        {
            Hashtable ht = new Hashtable();
            foreach (var key in _data.Keys)
            {
                ht[key] = _data[key];
            }
            return ht;
        }

        /// <summary>
        /// Parses a template using nVelocity
        /// </summary>
        /// <param name="templateData"></param>
        /// <returns></returns>
        public string ParseTemplate(string templateData)
        {
            if (false && CS.General_v3.Util.Other.IsLocalTestingMachine)
                return templateData;
            else
                return CS.General_v3.Util.TemplateUtil.ParseTemplateUsingNVelocity(templateData, null, this.GetAsHashTable());
        }


        #region IDictionary<string,object> Members

        void IDictionary<string, object>.Add(string key, object value)
        {
            _dataAsInterface.Add(key, value);
            
            
        }

        bool IDictionary<string, object>.ContainsKey(string key)
        {
            return this._data.ContainsKey(key);
            
        }

        ICollection<string> IDictionary<string, object>.Keys
        {
            get { return this._data.Keys; }
        }

        bool IDictionary<string, object>.Remove(string key)
        {
            return _dataAsInterface.Remove(key);
            
        }

        bool IDictionary<string, object>.TryGetValue(string key, out object value)
        {
            return _dataAsInterface.TryGetValue(key, out value);
            
        }

        ICollection<object> IDictionary<string, object>.Values
        {
            get { return _dataAsInterface.Values; }
        }

        #endregion

        #region ICollection<KeyValuePair<string,object>> Members

        void ICollection<KeyValuePair<string, object>>.Add(KeyValuePair<string, object> item)
        {
            _dataAsInterface.Add(item);
        }

        void ICollection<KeyValuePair<string, object>>.Clear()
        {
            _dataAsInterface.Clear();
            
        }

        bool ICollection<KeyValuePair<string, object>>.Contains(KeyValuePair<string, object> item)
        {
            return _dataAsInterface.Contains(item);
            
        }

        void ICollection<KeyValuePair<string, object>>.CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
        {
            _dataAsInterface.CopyTo(array, arrayIndex);
            
        }

        int ICollection<KeyValuePair<string, object>>.Count
        {
            get { return _dataAsInterface.Count; }
        }

        bool ICollection<KeyValuePair<string, object>>.IsReadOnly
        {
            get { return _dataAsInterface.IsReadOnly; }
        }

        bool ICollection<KeyValuePair<string, object>>.Remove(KeyValuePair<string, object> item)
        {
            return _dataAsInterface.Remove(item);
        }

        #endregion

        #region IEnumerable<KeyValuePair<string,object>> Members

        IEnumerator<KeyValuePair<string, object>> IEnumerable<KeyValuePair<string, object>>.GetEnumerator()
        {
            return _dataAsInterface.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_dataAsInterface).GetEnumerator();
        }

        #endregion
    }
}
