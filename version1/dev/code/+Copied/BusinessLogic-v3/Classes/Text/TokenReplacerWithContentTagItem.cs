﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.Interfaces;

using BusinessLogic_v3.Extensions;
using CS.General_v3.Classes.Attributes;
using System.Reflection;
namespace BusinessLogic_v3.Classes.Text
{
    public class TokenReplacerWithContentTagItem : CS.General_v3.Classes.Text.TokenReplacer
    {
        private IList<IItemWithContentTags> _itemWithContentTagsList = null;
        public void AddItemWithContentTag(IItemWithContentTags item)
        {
            _itemWithContentTagsList.Add(item);
        }
        public TokenReplacerWithContentTagItem(IItemWithContentTags item)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, null);
            initBefore();
            _itemWithContentTagsList.Add(item);
            initAfter();
        }

        public TokenReplacerWithContentTagItem(IEnumerable<IItemWithContentTags> itemList)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(itemList, null);
            initBefore();
            foreach (var item in itemList)
            {
                _itemWithContentTagsList.Add(item);
            }
            initAfter();
            
        }

        private void initBefore()
        {
            _itemWithContentTagsList = new List<IItemWithContentTags>();
            
            
        }
        private void initAfter()
        {
            this.fillDefaultTags();
        }



        public void SetTokens(params string[] nameValuePairs)
        {
            if (nameValuePairs.Length % 2 > 0)
                throw new InvalidOperationException("name value pairs must contain an even number of items");
            for (int i = 0; i < nameValuePairs.Length; i += 2)
            {
                string name = nameValuePairs[i];
                string value = nameValuePairs[i + 1];

                this[name] = value;
                
            }

        }
        public void SetToken(string key, string value, bool checkForCustomContentTag)
        {
            base.SetToken(key, value);
            if (checkForCustomContentTag)
            {
                checkKeyInTags(key);
            }
        }

        private void checkKeyInTags(string key)
        {
            if (_itemWithContentTagsList != null)
            {
                foreach (var item in _itemWithContentTagsList)
                {
                    item.CheckIfItemHasTagElseCreate(key);
                }
            }
        }

        protected override void preReplaceToken(string token)
        {
            checkKeyInTags(token);
            base.preReplaceToken(token);
        }

        public override void SetToken(string key, string value)
        {
            SetToken(key, value, true);
        }






        private string getBaseNameForContentTags(object o)
        {
            Type t = o.GetType();
            string name = t.Name;
            if (name.EndsWith("Base"))
                name = CS.General_v3.Util.Text.RemoveLastNumberOfCharactersFromString(name, 4);
            return name;

        }

        //private void fillContentTagTokensFromObject(object o)
        //{
        //    Type t = o.GetType();

        //    {
        //        string baseName = getBaseNameForContentTags(o);
        //        var memberList = CS.General_v3.Util.ReflectionUtil.GetAllPropertiesAndMethodsForType(t, true, true);
        //        foreach (var m in memberList)
        //        {
        //            var attrib = CS.General_v3.Util.ReflectionUtil.GetAttributeForMember<ContentTagAttribute>(m);
        //            if (attrib != null)
        //            {
        //                string title = attrib.GetTitle(m);
        //                object value = null;
        //                if (m is PropertyInfo)
        //                {
        //                    PropertyInfo pInfo = (PropertyInfo)m;
        //                    value = pInfo.GetValue(o, null);
        //                }
        //                else if (m is MethodInfo)
        //                {
        //                    MethodInfo mInfo =(MethodInfo)m;
        //                    value = mInfo.Invoke(o, null);
        //                }
        //                else
        //                {
        //                    throw new InvalidOperationException("Unsupported type");
        //                }
        //                string s = attrib.GetValueAsString(value);
        //                this["[" + baseName +":" + title + "]"] = s;
        //            }
        //        }
                
        //    }

        //}

        

        //public void FillTokensFromObjectsContentTags(params IBaseDbObject[] dbObjects)
        //{
            
        //    foreach (var dbItem in dbObjects)
        //    {
        //        if (dbItem != null)
        //        {



        //            fillContentTagTokensFromObject(dbItem);
        //            foreach (var item in _itemWithContentTagsList)
        //            {
        //                BusinessLogic_v3.Extensions.IItemWithContentTagsExtensions.CheckIfItemHasTypeElseCreate(item, dbItem.GetType());
        //            }
        //        }
        //    }
            

        //}

        public nVelocityContext CreateNVelocityContext()
        {
            nVelocityContext nv = new nVelocityContext();
            FillNVelocityContext(nv);
            return nv;

        }

        public void FillNVelocityContext(nVelocityContext nv)
        {
            foreach (var item in this._itemWithContentTagsList)
            {
                nv.LinkToItemWithContentTags(item);
            }
            foreach (var key in this.nvReplace.AllKeys)
            {
                string nvKey = key;
                if (nvKey != null)
                {
                    if (nvKey.StartsWith("[")) nvKey = nvKey.Substring(1);
                    if (nvKey.EndsWith("]")) nvKey = nvKey.Substring(0,nvKey.Length-1);
                    nvKey = nvKey.Replace(":", "-");
                    nv[nvKey] = this.nvReplace[key];
                }

            }
        }

    }
}
