﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Attributes
{
    public class DbObjectAttribute : Attribute
    {

        public string ClassTitle { get; set; }
        
    }
}
