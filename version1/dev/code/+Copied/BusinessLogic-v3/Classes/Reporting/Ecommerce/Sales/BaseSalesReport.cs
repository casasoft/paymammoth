﻿using System;
using System.Collections.Generic;
using System.Linq;
using CS.General_v3.Classes.NHibernateClasses.NHManager;
using CS.General_v3.Controls.WebControls.Specialized.Reporting.v1;
using BusinessLogic_v3.DB;
using CS.General_v3.Controls.WebControls.Common;
using BusinessLogic_v3.Modules.v2.Order;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;

namespace BusinessLogic_v3.Classes.Reporting.Ecommerce.Sales
{
    public abstract class BaseSalesReport
    {
        public SalesReportParams ReportParams { get; set; }
      //  public ICurrency Currency { get; set; }
        protected BaseReport report = null;
        public BaseSalesReport()
        {
            this.ReportParams = new SalesReportParams();
            this.ReportParams.SortBy = Enums.REPORTING_SALES_SORTBY.PopularityDesc;
        }
        private void createReport()
        {
            report = new BaseReport();
            report.Functionality.Title = "Sales Report";
        }

        protected IEnumerable<OrderBase> orders = null;
        private void loadOrders()
        {
            var q = OrderBase.GetQuery();
            q.Where(order => order.IsConfirmed && order.Paid &&
                order.PaidOn >= CS.General_v3.Util.Date.GetStartOfDay(this.ReportParams.DateFrom) && order.PaidOn <= CS.General_v3.Util.Date.GetEndOfDay(this.ReportParams.DateTo));

            if (this.ReportParams.AffiliateId.GetValueOrDefault() >0)
            {
                q.Where(order => order.LinkedAffiliate.ID == this.ReportParams.AffiliateId);
            }
            if (this.ReportParams.Currencies.IsNotNullOrEmpty())
            {
                q.WhereRestrictionOn(order => order.OrderCurrency).IsInG<long>(this.ReportParams.Currencies.ToList().ConvertAll<long>(c => c.ID));
            }
            if (this.ReportParams.Countries.IsNotNullOrEmpty())
            {
                List<string> list = new List<string>();
                foreach (var c in this.ReportParams.Countries)
                {
                    list.Add(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode(c));
                    list.Add(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To3LetterCode(c));
                }

                q.WhereRestrictionOn(order => order.CustomerCountryStr).IsInG<string>(list);
            }
            q = q.Fetch(item => item.OrderItems).Eager;
            //q.RootCriteria.SetFetchMode("OrderItems", NHibernate.FetchMode.Select);



            orders = OrderBase.FindAll(q);


        }
        private void fillReportInfo()
        {
            report.Functionality.Title = "Item Sales & Commission Report";
//            report.Functionality.ShowGeneratedOn = true;
        }

        protected abstract void processReport();

        protected MyDiv generateTotalsDiv()
        {
            var session = NhManagerIOC.NhManager.GetCurrentSessionFromContext();
            MyDiv div = new MyDiv(cssClass: "sales-report-totals");

            using (var transaction = session.BeginTransaction())
            {
               
                double totalCommission = 0, totalSales = 0;
                var orderList = this.orders.ToList();
                for (int i = 0; i < orderList.Count; i++)
                {
                    var order = orderList[i];
                    OrderBaseFrontend orderFrontend = OrderBaseFrontend.LoadFrontendFromDataItem(order);

                    totalCommission += orderFrontend.GetTotalAffiliateCommission();
                    totalSales += orderFrontend.GetTotalPrice();
                }


                {
                    MyHeading hTotal = new MyHeading(2, "Total Sales from " + this.ReportParams.DateFrom.ToString("dd/MMM/yy") + " to " + this.ReportParams.DateTo.ToString("dd/MMM/yy"), cssClass: "total-sales", parentControlToAddTo: div);
                    {
                        MyParagraph p = new MyParagraph();
                        p.CssManager.AddClass("note");
                        p.InnerHtml = "Kindly note that the totals below are not necessarily for the items generated in this report, but for the total date range";
                    }
                    {
                        //sales
                        MyDiv divTotalSales = new MyDiv(cssClass: "total-sales", parentToAddTo: div);

                        MySpan spanLabel = new MySpan(cssClass: "label", parentToAddTo: divTotalSales);

                        spanLabel.InnerHtml = "Total Sales:";

                        MySpan spanValue = new MySpan(cssClass: "value", parentToAddTo: divTotalSales);
                        spanValue.InnerHtml = BusinessLogic_v3.Settings.SettingsMain.CurrentCultureManager.FormatPriceAsHTML(totalSales);
                    }

                    {
//total commission
                        MyDiv divTotalCommission = new MyDiv(cssClass: "total-commission", parentToAddTo: div);

                        MySpan spanLabel = new MySpan(cssClass: "label", parentToAddTo: divTotalCommission);

                        spanLabel.InnerHtml = "Total Commission:";

                        MySpan spanValue = new MySpan(cssClass: "value", parentToAddTo: divTotalCommission);
                        spanValue.InnerHtml = BusinessLogic_v3.Settings.SettingsMain.CurrentCultureManager.FormatPriceAsHTML(totalCommission);
                    }

                }
                transaction.Commit();
            }
            return div;
        }

        public virtual MyDiv GenerateReport()
        {
            MyDiv div = new MyDiv();
            createReport();
           // createReport();
            fillReportInfo();
            loadOrders();
            processReport();
            div.Controls.Add(report);
            div.Controls.Add(generateTotalsDiv());

            return div;

        }
    }
}
