﻿using System;
using System.Collections.Generic;
using System.Linq;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.Currencies;

namespace BusinessLogic_v3.Classes.Reporting.Ecommerce.Sales
{
    public class ItemsTotalSalesCounter<T> where T : IItemSale
    {
        private List<ItemTotalSales<T>> _items = null;
        public ItemsTotalSalesCounter()
        {
            _items = new List<ItemTotalSales<T>>();
            _count = new Dictionary<long, int>();
        }
        public void AddItem(T item)
        {
            ItemTotalSales<T> sale = new ItemTotalSales<T>(item);
            sale.DateSold = item.DateSold;
            sale.Currency = item.Currency;
            _items.Add(sale);
        }
        public IEnumerable<ItemTotalSales<T>> GetAllDistinctItems()
        {

            return _items.FilterDistinctItems(((i1, i2) => (i1.Item.ItemUniqueID == i2.Item.ItemUniqueID)));
        }

        public IEnumerable<ItemTotalSales<T>> GetAllItems()
        {
            return _items.ToList() ;
        }

        private Dictionary<long, int> _count = null;

        public int GetCountForItem(T item)
        {
            if (!_count.ContainsKey(item.ItemUniqueID))
            {
                _count[item.ItemUniqueID] = _items.Count(t => t.Item.ItemUniqueID == item.ItemUniqueID);
            }
            return _count[item.ItemUniqueID];
        }


        public int GetTotalSaleCount()
        {
            int cnt = 0;
            foreach (var key in _count.Keys)
            {
                cnt += _count[key];
            }
            return cnt;
        }
        public double GetTotalSalePrice()
        {
            double d = 0;
            foreach (var sale in _items)
            {
                d += IItemSaleExtensions.GetTotalPrice(sale.Item);
            }
            return d;
        }
        public double GetTotalQuantities()
        {
            int qty = 0;
            foreach (var sale in _items)
            {
                qty += sale.Item.Qty;
            }
            return qty;
        }
        public double GetTotalSaleCommissions()
        {
            double d = 0;
            foreach (var sale in _items)
            {
                d += sale.Item.Commission;
            }
            return d;
        }
    }
}
