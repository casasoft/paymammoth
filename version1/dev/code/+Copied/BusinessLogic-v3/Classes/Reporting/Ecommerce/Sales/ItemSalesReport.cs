﻿using System;
using System.Collections.Generic;
using CS.General_v3.Controls.WebControls.Specialized.Reporting.v1;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.Currencies;
using BusinessLogic_v3;
using BusinessLogic_v3.Classes.Reporting.Ecommerce.Sales;

namespace BusinessLogic_v3.Classes.Reporting.Ecommerce.Sales
{
    
    public class ItemSalesReport
    {

        public virtual bool ShowDistinctItemsOnly { get; set; }
   //     public ICurrency Currency { get; set; }
        public Enums.REPORTING_SALES_SORTBY SortBy { get; set; }
        private BaseReportSectionDataImpl reportSection = null;

        private ItemsTotalSalesCounter<IItemSale> _itemSales = null;
   
        public void AddItem(IItemSale item)
        {
            _itemSales.AddItem(item);
        }

        public ItemSalesReport(string title)
        {
            _itemSales = new ItemsTotalSalesCounter<IItemSale>();
            reportSection = new BaseReportSectionDataImpl();
            reportSection.Title = title;
            
            initItemDescColumnNames();
        }
        private void initItemDescColumnNames()
        {

            itemDescriptionColumnNames = new List<string>();
            itemDescriptionColumnNames.Add("Title");
        }


        private IEnumerable<ItemTotalSales<IItemSale>> sortItems(IEnumerable<ItemTotalSales<IItemSale>> list)
        {
            switch (this.SortBy)
            {
                case Enums.REPORTING_SALES_SORTBY.PopularityAsc:
                case Enums.REPORTING_SALES_SORTBY.PopularityDesc:
                    {
                        int multiplier = 1;
                        if (this.SortBy == Enums.REPORTING_SALES_SORTBY.PopularityDesc) multiplier = -1;

                        return list.SortByMultipleComparers((i1, i2) => _itemSales.GetCountForItem(i1.Item).CompareTo(_itemSales.GetCountForItem(i2.Item)) * multiplier,
                            (i1, i2) => (i1.Item.GetTitle().CompareTo(i2.Item.GetTitle())));
                    }
                case Enums.REPORTING_SALES_SORTBY.TitleAsc:
                case Enums.REPORTING_SALES_SORTBY.TitleDesc:
                    {
                        int multiplier = 1;
                        if (this.SortBy == Enums.REPORTING_SALES_SORTBY.TitleDesc) multiplier = -1;

                        return list.SortByMultipleComparers((i1, i2) => (i1.Item.GetTitle().CompareTo(i2.Item.GetTitle())) * multiplier);
                    }
                case Enums.REPORTING_SALES_SORTBY.PriceAsc:
                case Enums.REPORTING_SALES_SORTBY.PriceDesc:
                    {
                        int multiplier = 1;
                        if (this.SortBy == Enums.REPORTING_SALES_SORTBY.PriceDesc) multiplier = -1;

                        return list.SortByMultipleComparers(
                            ((i1, i2) => (i1.Item.Price.CompareTo(i2.Item.Price)) * multiplier),
                            ((i1, i2) => (i1.Item.GetTitle().CompareTo(i2.Item.GetTitle())))
                            );

                    }
                case Enums.REPORTING_SALES_SORTBY.DateSoldAsc:
                case Enums.REPORTING_SALES_SORTBY.DateSoldDesc:
                    {
                        int multiplier = 1;
                        if (this.SortBy == Enums.REPORTING_SALES_SORTBY.DateSoldDesc) multiplier = -1;

                        return list.SortByMultipleComparers(
                            ((i1, i2) => (i1.DateSold.GetValueOrDefault().CompareTo(i2.DateSold.GetValueOrDefault()))),
                            ((i1, i2) => (i1.Item.GetTitle().CompareTo(i2.Item.GetTitle())))
                            );

                    }
                default:
                    throw new InvalidOperationException("invalid enum value for sorting");
                
            }
        }

        protected List<string> itemDescriptionColumnNames { get; private set; }

        private void addHeaders()
        {
            if (!ShowDistinctItemsOnly)
            {
                this.reportSection.TableData.AddHeaderTop("Date Sold");
            }
            this.reportSection.TableData.AddHeaderTop("Reference");
            foreach (var col in itemDescriptionColumnNames)
            {
                this.reportSection.TableData.AddHeaderTop(col);
            }
            this.reportSection.TableData.AddHeaderTop("Currency");
            this.reportSection.TableData.AddHeaderTop("Price");
            this.reportSection.TableData.AddHeaderTop("Qty");
            this.reportSection.TableData.AddHeaderTop("Total");
            this.reportSection.TableData.AddHeaderTop("Commission");

            if (ShowDistinctItemsOnly)
            {
                this.reportSection.TableData.AddHeaderTop("Sale Count");
                this.reportSection.TableData.AddHeaderTop("Line Total");
            }
            


        }

        private void addRow(ItemTotalSales<IItemSale> item)
        {
            BaseReportTableRowDataImpl row = new BaseReportTableRowDataImpl();
            string url = item.Item.GetItemLinkUrl();
            if (!ShowDistinctItemsOnly)
            {
                row.AddCell(item.DateSold != null ? item.DateSold.Value.ToString("dd/MM/yyyy hh:mmtt") : "-");
            }

            row.AddCell(item.Item.Reference, url);
            foreach (var desc in item.Item.GetDescriptionColumns())
            {
                row.AddCell(desc, url);
            }
            
            row.AddCell(item.Item.Currency.CurrencyISOCode, url);
            {
                //var/ currProvider = BusinessLogic_v3.Settings.SettingsMain.CurrentCultureManager.GetCurrentCultureFormatProvider();
                //CS.General_v3.Util.Number.FormatNumber(item.Item.Price, CS.General_v3.Util.Number.NUMBER_FORMAT_TYPE.Currency, currProvider, item.Currency.HtmlCode);
                string sPrice = item.Item.Price.ToString("0.00");
                row.AddCell(sPrice);
            }
            {
                //var/ currProvider = BusinessLogic_v3.Settings.SettingsMain.CurrentCultureManager.GetCurrentCultureFormatProvider();
                //CS.General_v3.Util.Number.FormatNumber(item.Item.Price, CS.General_v3.Util.Number.NUMBER_FORMAT_TYPE.Currency, currProvider, item.Currency.HtmlCode);
                string sQty = item.Item.Qty.ToString();
                row.AddCell(sQty);
            }
            {
                //var/ currProvider = BusinessLogic_v3.Settings.SettingsMain.CurrentCultureManager.GetCurrentCultureFormatProvider();
                //CS.General_v3.Util.Number.FormatNumber(item.Item.Price, CS.General_v3.Util.Number.NUMBER_FORMAT_TYPE.Currency, currProvider, item.Currency.HtmlCode);
                double total = item.Item.Price * item.Item.Qty;
                string sTotal= total.ToString("0.00");
                row.AddCell(sTotal);
            }
            {//commission
                string sComm = "";
                if (item.Item.Commission != 0)
                    sComm = item.Item.Commission.ToString("0.00");
                row.AddCell(sComm);
            }
            if (ShowDistinctItemsOnly)
            {
                row.AddCell(_itemSales.GetCountForItem(item.Item).ToString());

                double total = item.Item.Price * _itemSales.GetCountForItem(item.Item);
                string s = total.ToString("0.00");
                row.AddCell(s);
            }
            

            this.reportSection.TableData.DataRows.Add(row);
        }

        private void addFooter()
        {
            BaseReportTableRowDataImpl row = new BaseReportTableRowDataImpl();
            row.CustomCss.AddClass("footer");
            if (!ShowDistinctItemsOnly)
            {
                row.AddCell("");
            }
            
            row.AddCell("");
            foreach (var c in this.itemDescriptionColumnNames)
            {
                row.AddCell("");
            }
            row.AddCell(""); //currency
            row.AddCell("Totals:", cssClass:"footer-total");
            row.AddCell(_itemSales.GetTotalQuantities().ToString(), cssClass: "footer-total-qty");
            row.AddCell(_itemSales.GetTotalSalePrice().ToString(),cssClass:"footer-price");
            row.AddCell(_itemSales.GetTotalSaleCommissions().ToString(), cssClass: "footer-commission");
            if (ShowDistinctItemsOnly)
            {
                row.AddCell(_itemSales.GetTotalSaleCount().ToString(), cssClass: "footer-salecount");

                row.AddCell(_itemSales.GetTotalSalePrice().ToString(), cssClass: "footer-price distinct");
            }
            else
            {
                
            
            }


            this.reportSection.TableData.DataRows.Add(row);
        }

        public BaseReportSectionDataImpl GenerateReportSection()
        {
            IEnumerable<ItemTotalSales<IItemSale>> items = null;
            if (ShowDistinctItemsOnly)
                items = _itemSales.GetAllDistinctItems();
            else
                items = _itemSales.GetAllItems();
           // var tracks = _trackSales.GetAllItems();
            var sortedItems = sortItems(items);
            
            addHeaders();
            foreach (var item in sortedItems)
            {
                addRow(item);
            }
            addFooter();
            return this.reportSection;


        }

    }
}
