﻿using System;
using BusinessLogic_v3.Modules.Currencies;

namespace BusinessLogic_v3.Classes.Reporting.Ecommerce.Sales
{
    public class ItemTotalSales<T> 
    {
        public ItemTotalSales(T item)
        {
            this.Item = item;
        }
        
        public T Item { get; set; }
        public DateTime? DateSold { get; set; }
        public ICurrency Currency { get; set; }
    }
}
