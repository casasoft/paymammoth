﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Classes.Reporting.Ecommerce.Sales
{
    [Serializable]
    public class SalesReportParams
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public long? AffiliateId { get; set; }
        public bool ShowDistinctItemsOnly { get; set; }
        public Enums.REPORTING_SALES_SORTBY SortBy { get; set; }
        public IEnumerable<BusinessLogic_v3.Modules.v2.Currency.ICurrencyBaseData> Currencies { get; set; }
        public IEnumerable<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166> Countries { get; set; }
    }
}
