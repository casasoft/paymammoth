﻿using System;
using BusinessLogic_v3.Modules.Currencies;
using System.Collections.Generic;
namespace BusinessLogic_v3.Classes.Reporting.Ecommerce.Sales
{

    public static class IItemSaleExtensions
    {

        public static string GetTitle(this IItemSale sale)
        {
            return CS.General_v3.Util.ListUtil.JoinList(sale.GetDescriptionColumns(), " - ");
        }
        public static double GetTotalPrice(this IItemSale sale)
        {
            return sale.Qty * sale.Price;
            
        }
    }

    public interface IItemSale 
    {
        long ItemUniqueID { get; }
        double Price { get; }
        int Qty { get; }
        IEnumerable<string> GetDescriptionColumns();

        
        string Reference { get;  }
        DateTime? DateSold { get; }
        ICurrency Currency { get; }
        string GetItemLinkUrl();


        double Commission { get; }
    }
}
