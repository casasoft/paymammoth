﻿using System;
using System.Reflection;

namespace BusinessLogic_v3.Classes.Notifications.NotificationSender
{
    public class NotificationToCheck<TItem>
    {
        public PropertyInfo PropertyFlag { get; set; }
        public PropertyInfo PropertyDateToSendFrom { get; set; }
        
        public Action<TItem> NotificationAction { get; set; }



    }
}
