﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using Quartz;
using NHibernate.Criterion;
using BusinessLogic_v3.Modules.SettingModule;
using IBaseDbObject = BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject;

namespace BusinessLogic_v3.Classes.Notifications.NotificationSender
{
    public abstract class BaseNotificationSender<TSenderType, TItem>
        where TSenderType : BaseNotificationSender<TSenderType, TItem>
        where TItem: IBaseDbObject


    {
        public static TSenderType Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<TSenderType>(); } }


        public bool IsStarted { get; private set; }
        protected Enum settingEnumNotificationInterval {get;set;}
        protected Enum settingEnumPageSize { get; set; }

        protected List<NotificationToCheck<TItem>> notificationsToCheck = null;

        private IJobDetail recurringJob = null;

        private readonly object _lock = new object();

       
        private IBaseDbFactory getFactoryForItem()
        {
            IBaseDbFactory f = BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryForType(typeof(TItem));
            return f;
        }

        private IEnumerable<TItem> getListOfItemsHavingNotications(int pageNo, int pageSize)
        {
            
            var factory = getFactoryForItem();
            var crit = factory.GetCriteria();
            var disjunction = new NHibernate.Criterion.Disjunction();
            foreach (var n in this.notificationsToCheck)
            {
                NHibernate.Criterion.Conjunction conj = new Conjunction();

                conj.Add(NHibernate.Criterion.Restrictions.Eq(n.PropertyFlag.Name, false));
                conj.Add(NHibernate.Criterion.Restrictions.Le(n.PropertyDateToSendFrom.Name, CS.General_v3.Util.Date.Now));
                disjunction.Add(conj);
            }
            crit.Add(disjunction);
            
            BusinessLogic_v3.Util.nHibernateUtil.LimitCriteriaByPrimaryKeys(crit, pageNo, pageSize);
            
            var list = factory.FindAll(crit).Cast<TItem>();

            return list;

        }


        public BaseNotificationSender(Enum settingEnumNotificationInterval, Enum settingEnumPageSize)
        {
            
            
            this.settingEnumNotificationInterval = settingEnumNotificationInterval;
            this.settingEnumPageSize = settingEnumPageSize;
            this.notificationsToCheck = new List<NotificationToCheck<TItem>>();

            BusinessLogic_v3.Modules.SettingModule.SettingBaseFactory.Instance.OnItemUpdate += new BusinessLogic_v3.Classes.DbObjects.General.OnItemUpdateHandler(Instance_OnItemUpdate);

        }

        void Instance_OnItemUpdate(IBaseDbObject item, BusinessLogic_v3.Enums.UPDATE_TYPE updateType)
        {
            if (IsStarted)
            {
                SettingBase setting = (SettingBase) item;
                string sID = CS.General_v3.Util.EnumUtils.StringValueOf(this.settingEnumNotificationInterval);
                if (string.Compare(setting.Identifier, sID, true) == 0)
                {
                    Stop();
                    Start();
                }

            }
        }

        public void AddNotificationToSend(System.Linq.Expressions.Expression<Func<TItem, bool>> notificationPropertyFlag,
            System.Linq.Expressions.Expression<Func<TItem, DateTime?>> notificationPropertyDateToCheck, Action<TItem> actionToPerform)
        {
            
            NotificationToCheck<TItem> n = new NotificationToCheck<TItem>();
            n.NotificationAction = actionToPerform;
            n.PropertyDateToSendFrom = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(notificationPropertyDateToCheck);
            n.PropertyFlag = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(notificationPropertyFlag);
            
            this.notificationsToCheck.Add(n);

        }

        private void sendNotifications()
        {
            lock (_lock)
            {//only one at a time
                BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();

                int currPageNo = 1;
                int pageSize = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(settingEnumPageSize);


                bool hasData = true;
                while (hasData)
                
                {
                    var listToCheck = getListOfItemsHavingNotications(currPageNo, pageSize);

                    foreach (var itemToCheck in listToCheck)
                    {
                        sendNotificationsForItem(itemToCheck);
                    }

                    hasData = (listToCheck.Count() > 0);
                    currPageNo++;
                    
                }


                BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();


            }

        }

        

        

        private void sendNotificationsForItem(TItem itemToCheck)
        {
           
            foreach (var n in this.notificationsToCheck)
            {
                bool value = (bool) n.PropertyFlag.GetValue(itemToCheck, null);
                DateTime? dateToSendFrom = (DateTime?)n.PropertyDateToSendFrom.GetValue(itemToCheck, null);
                if (!value && dateToSendFrom <= CS.General_v3.Util.Date.Now)
                {
                    n.NotificationAction(itemToCheck);
                    bool updValue = (bool)n.PropertyFlag.GetValue(itemToCheck, null);
                    if (!updValue)
                    {
                        if (CS.General_v3.Util.Other.IsLocalTestingMachine)
                        {
                            throw new InvalidOperationException("Make sure you set the property '" + n.PropertyFlag + "' to true after sending the notification");
                        }

                    }


                }

            }
                
            
        }
        
        public void Start()
        {
            lock (_lock)
            {
                if (IsStarted)
                {
                    throw new InvalidOperationException("Cannot start a notification sender twice");
                }

                int secsToRepeat = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(settingEnumNotificationInterval);

                recurringJob = BusinessLogic_v3.Util.SchedulingUtil.ScheduleRepeatMethodCall(sendNotifications, "BaseNotificationSender-" + typeof(TItem).Name + "-" + CS.General_v3.Util.Date.Now.ToString("yyyyMMddhhmmss"),
                                                                                             timeToStart: null,
                                                                                             intervalToRepeatInSecs: secsToRepeat,
                                                                                             totalTimesToRepeat: null);

                
                
                IsStarted = true;
                //CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(sendNotifications, "BaseNotificationSender-" + typeof(TItem).Name + "-" + CS.General_v3.Util.Date.Now.ToString("yyyyMMddhhmmss"), System.Threading.ThreadPriority.BelowNormal);


            }
        }

        public void Stop()
        {
            if (!IsStarted)
            {
                throw new InvalidOperationException("Notification sender not started");
            }
            IsStarted = false;

        }


    }
}
