﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;

namespace BusinessLogic_v3.Classes.Errors
{
    public class ErrorReporter : CS.General_v3.Error.ErrorReporter
    {
        public static ErrorReporter CreateInstance()
        {
            return CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<ErrorReporter>();
        }
        protected override void sendErrorWithException()
        {
            bool writeToLogOK = false;
            MyNHSessionBase session = null;
            try
            {
                //StringBuilder message = new StringBuilder("\r\n\r\nUnhandledException logged by Reporter.dll:\r\n\r\nappId=");

                HttpContext ctx = getHttpContext();

                string appId = (string)AppDomain.CurrentDomain.GetData(".appId");
                if (appId != null)
                {
                    // message.Append(appId);
                }
                var e = this.ErrorException;
                if (checkIfExceptionShouldBeReported(e))
                {
                    if (e != null)
                    {
                        details.AddExceptionDetails(e);

                    }
                    addContextInfoIfAvailable(ctx);
                    writeToLog();
                    writeToLogOK = true;
                    lock (_lock) //only one thread can be sending an error at the same time via email
                    {



                        // Reporter.ErrorDetails details = new Reporter.ErrorDetails();
                        bool ok = true;
                        string url = "";
                        bool sendEmail = true;


                        if (!CS.General_v3.Settings.Emails.SendEmailsOnError)
                        {
                            sendEmail = false;
                        }
                        if (ok &&
                            (CS.General_v3.Util.Other.IsLocalTestingMachine))
                        {
                            sendEmail = false;

                        }
                        if (SendAlways)
                        {
                            sendEmail = true;
                        }


                        details.AddFooter();

                        //
                        //details.LogToFile(this.ErrorException);
                        if (ok)
                        {
                            if (CS.General_v3.Settings.Database.UseRealDatabase)
                            {
                                session = NHClasses.NhManager.CreateNewSessionForContextAndReturn();
                            }
                            try
                            {

                                if (sendEmail)
                                {
                                    if (!CS.General_v3.Util.Other.IsLocalTestingMachine)
                                    {
                                        details.SendEmail();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                details.AddLine("Exception when trying to send email': " + ex.GetType().FullName + " (" + ex.Message + ")\r\n\r\nStackTrace: " + ex.StackTrace);
                            }
                            if (CS.General_v3.Settings.Database.UseRealDatabase)
                            {
                                NHClasses.NhManager.DisposeCurrentSessionInContext();
                            }

                        }
                        //if (ex.GetBaseException() != null && ex.GetBaseException() != ex)
                        //details.ReportError(ex.GetBaseException(), sendAlways: sendAlways);
                    }
                }
            }
            catch (Exception ex)
            {
                details.AddLine("General error when trying to report an error: " + ex.GetType().FullName + " (" + ex.Message + ")\r\n\r\nStackTrace: " + ex.StackTrace);
            }
            finally
            {
                if (!writeToLogOK)
                {
                    CS.General_v3.Util.Log4NetUtil.LogMsg(_log, this.ErrorLog4NetType, details.GetAsString(), this.ErrorException);
                    writeToLogOK = true;
                }

            }
            if (session != null && session.IsOpen)
            {
                NHClasses.NhManager.DisposeCurrentSessionInContext(throwErrorIfSessionDoesNotExistOrDisposed: false);
            }
        }
    }
}
