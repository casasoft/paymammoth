﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using CS.General_v3.Classes.Text;

namespace BusinessLogic_v3.Classes.EnquiryForm
{
    public class EnquiryParams
    {
        public long RelatedObjectID { get; set; }
        public string SendToName { get; set; }
        public string SendToEmail { get; set; }
        public string SendFromName { get; set; }
        public string SendFromEmail { get; set; }
        public string ReplyToName { get; set; }
        public string ReplyToEmail { get; set; }
        public Enum EmailText { get; set; }
        public TokenReplacer TokenReplacer { get; set; }
        public string Enquiry { get; set; }
    }
}
