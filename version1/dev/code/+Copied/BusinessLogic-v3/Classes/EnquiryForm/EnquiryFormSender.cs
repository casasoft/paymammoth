﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Modules.EmailTextModule;
using CS.General_v3.Util;

namespace BusinessLogic_v3.Classes.EnquiryForm
{
    public class EnquiryFormSender<TRelatedObject> where TRelatedObject : IBaseDbObject
    {
        public EnquiryParams SenderParams { get; private set; }
        public EnquiryFormSender(EnquiryParams sparams)
        {
            if (sparams == null)
            {
                throw new InvalidOperationException("Paramters must be specified!");
            }
            this.SenderParams = sparams;
        }

        public void SendForm()
        {
            CS.General_v3.Classes.Email.EmailForm emailAdmin = null;
            var emailDb = EmailTextBaseFactory.Instance.GetByIdentifier(SenderParams.EmailText);
            var email = emailDb.GetEmailMessage(SenderParams.TokenReplacer);
            emailAdmin = new CS.General_v3.Classes.Email.EmailForm(email.Subject, email.BodyPlain, true, email.Resources);

            emailAdmin.AddSection("Contact Form Details");
            emailAdmin.AddField("Date Sent", Date.Now);

            emailAdmin.AddField("From", SenderParams.SendFromName);
            emailAdmin.AddField("Sender Email", SenderParams.SendFromEmail);

            emailAdmin.AddField("Enquiry Type", typeof (TRelatedObject).Name + " - Object ID: "+SenderParams.RelatedObjectID);

            addAdditionalInformationToAdminContactForm(emailAdmin);

            emailAdmin.AddField("Enquiry", SenderParams.Enquiry, true, 10);
            emailAdmin.SendEmailAsync(SenderParams.SendFromEmail, SenderParams.SendFromName, SenderParams.SendToEmail,
                                      SenderParams.SendToName, SenderParams.ReplyToEmail, SenderParams.ReplyToName);
        }

        protected virtual void addAdditionalInformationToAdminContactForm(CS.General_v3.Classes.Email.EmailForm emailAdmin)
        {
            //
        }
    }
}
