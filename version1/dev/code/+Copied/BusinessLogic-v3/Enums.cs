﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ContentTextModule.DefaultValues;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Classes.Attributes;
using System.ComponentModel;
//  BusinessLogic_v3.Enums
namespace BusinessLogic_v3
{
    using JavaScript.Classes.Internationalisation;
    using BusinessLogic_v3.Modules.ArticleModule.DefaultValues;
    using Modules;
    using Modules.ContentTextModule;
    using Modules.EmailTextModule.DefaultValues;
    using BusinessLogic_v3.Classes.Routing;
    using BusinessLogic_v3.Frontend.ContentTextModule;
    using CS.General_v3.Classes.Login;
    using Modules.MemberModule;
    using BusinessLogic_v3.Modules.RoutingInfoModule.Attributes;
    using BusinessLogic_v3.Classes.Text;
    using BusinessLogic_v3.Constants;

    public static class Enums
    {
        public const string TAG_ERROR_PAGE_CONTACT_US_LINK = "[ContactUsLink]";

        public enum CMS_INLINE_EDITING_TYPE
        {
            ContentText = 0,
            ArticleTitle = 10,
            ArticleHtml = 20
        }
        public enum NHIBERNATE_COLLECTION_TYPE
        {
            Bag,
            Set,
            List

        }

        public enum CATEGORY_ROOT_NODES
        {
            Events,
            Classifieds
        }

        public enum CONTENT_PAGE_TYPE
        {
            Article,
            Product,
            Greeting,
            Classified
        }

        public enum MEMBER_SORT_BY
        {
            [DescriptionAttribute("Relevance")]
            Relevance
        }

        public enum MEMBER_SUBSCRIPTION_PURCHASE_CONDITION
        {
            NewSubscription,
            Renewal,
            ChangeSubscriptionType
        }

        public enum USER_REGISTRATION_REQUIRED_KEY
        {
            Username,
            Email,
            UsernameAndEmail
        }
        public enum CONTACT_DETAILS_FIELDS
        {
            DirectorName,
            Telephone,
            Fax,
            Email,
            Website,
            Mobile,
            Map,
            Address,
            TwitterURL,
            FacebookURL,
            LinkedinURL,
            SkypeURL,
            SkypeName
        }
        public enum PROFILE_FIELDS /*IMPORTANT THESE VALUES MUST NOT CHANGE NAME AS THEY ARE USED IN SETTINGS*/
        {
            Name,
            Surname,
            IdCard,
            AccountType,
            VatNumber,
            Street,
            Street2,
            City,
            ZIP_PostCode,
            Country,
            BirthDate,
            Email,
            ConfirmEmail,
            Mobile,
            Telephone,
            Username,
            Language,
            TimeZone,
            Company,
            FullName,
            Password,
            ConfirmPassword,
            AllowPromotions,
            AcceptTerms,
            Gender
        }

        public enum TICKET_STATE
        {
            Open,
            Closed,
            ReOpened
        }

        public enum TICKET_COMMENT_TYPE
        {
            PostedByClient,
            PostedByStaff
        }

        public enum TICKET_USER_TYPE
        {
            NormalMember,
            TicketingSystemStaff
        }

        public enum TICKET_PRIORITY
        {
            Trivial = 0,
            Minor = 100,
            Major = 200,
            Critical = 300,
            Blocker = 400
        }

        public enum TICKET_SORTBY
        {
            CreatedDateAsc,
            CreatedDateDesc,
            PriorityAsc,
            PriorityDesc,
            StatusAsc,
            StatusDesc,
            TitleAsc,
            TitleDesc
        }

        public enum TICKET_UPDATING_FROM
        {
            Frontend,
            CMS
        }

        public enum UPDATE_TYPE
        {
            Update,
            CreateNew,
            Delete
        }
        public enum NHIBERNATE_CACHE_TYPE
        {
            ReadOnly,
            ReadWrite,
            NonstrictReadWrite,
            Transactional,
            Default
        }
        public enum NHIBERNATE_COLLECTION_RELATIONSHIP
        {
            OneToMany,
            ManyToMany

        }
        public enum NHIBERNATE_COLLECTION_FETCHMODE
        {
            Default,
            Join,
            Select,
            SubSelect
        }

        public enum ADVERT_HIT_TYPE
        {
            Impression = 0,
            Click = 100


        }

        public enum PRIZE_POSITION_PREFIX
        {
            PrizePosition
        }

        public enum MEMBER_REGISTRATION_RESULT
        {
            EmailAlreadyExists,
            UsernameAlreadyExists,
            Success
        }
        public enum MEMBER_LOGIN_INFO
        {
            InvalidLoginAttempt,
            Login,
            Logout

        }
        public enum SHIPPING_REQUEST_PROPERTIES_PREFIX
        {
            ShippingRequestProperties
        }

        public enum MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE_CONTENTTEXT_PREFIX
        {
            UpdateTypeContentTextPrefix
        }

        public enum MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE
        { // WHEN YOU UPDATE THIS UPDATE THE METHOD MemberAccountBalanceHistoryUpdateTypeToContentText

            Deposit,
            Withdraw,
            Purchase,
            Adjustment,
            Won,
            RenewalBasedOnSubscription
        }




        public enum MEMBER_ACCOUNT_BALANCE_UPDATE_TYPE_BALANCE_CHANGE_TYPE
        {
            RenewableAccountBalance,
            AccountBalance
        }



        public enum MEMBER_RESPONSIBLE_LIMITS_FREQUENCY
        {
            Day,
            Week,
            Month,
            Year
        }

        public static IContentTextBase GetMemberResponsibleLimitsFrequencyAsContentText(MEMBER_RESPONSIBLE_LIMITS_FREQUENCY frequency)
        {
            switch (frequency)
            {
                case MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Day: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ResponsibleGaming_YourLimits_Freq_Day); break;
                case MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Week: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ResponsibleGaming_YourLimits_Freq_Week); break;
                case MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Month: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ResponsibleGaming_YourLimits_Freq_Month); break;
                case MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Year: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ResponsibleGaming_YourLimits_Freq_Year); break;
                default:
                    throw new InvalidOperationException("Value <" + frequency + "> not mapped");
            }
        }
        public static int GetMemberResponsibleLimitsFrequencyAsDays(MEMBER_RESPONSIBLE_LIMITS_FREQUENCY frequency)
        {
            switch (frequency)
            {
                case MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Day:
                    return 1;
                case MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Week:
                    return 7;
                case MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Month:
                    return 30;
                case MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Year:
                    return 365;
                default:
                    throw new InvalidOperationException("Value <" + frequency + "> not mapped");
            }
        }

        public enum MEMBER_ACCOUNT_BALANCE_HISTORY_SORT_BY
        {
            [DescriptionAttribute("Date (Newest First)")]
            DateDesc,
            [DescriptionAttribute("Date (Oldest First)")]
            DateAsc
        }
        public static IContentTextBase MemberAccountBalanceHistorySortByToContentTexts(MEMBER_ACCOUNT_BALANCE_HISTORY_SORT_BY value)
        {
            switch (value)
            {
                case MEMBER_ACCOUNT_BALANCE_HISTORY_SORT_BY.DateDesc: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Sorting_DateDescending); break;
                case MEMBER_ACCOUNT_BALANCE_HISTORY_SORT_BY.DateAsc: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Sorting_DateAscending); break;
                default:
                    throw new InvalidOperationException("Value <" + value + "> not mapped");
            }
        }
        public enum MEMBER_SUBSCRIPTION_TYPE
        {
            Expired,
            Activated
        }
        public enum MEMBER_REFERRAL_COMMISSION_STATUS
        {
            Paid,
            Pending
        }

        public enum MEMBER_REFERRAL_COMMISSION_PAID_BY
        {
            Paypal,
            Cash,
            Cheque,
            BankTransfer,
            Other
        }

        public enum SHOW_AMOUNT
        {
            [DescriptionAttribute("10 per page")]
            _10PerPage = 10,
            [DescriptionAttribute("25 per page")]
            _25PerPage = 25,
            [DescriptionAttribute("50 per page")]
            _50PerPage = 50,
            [DescriptionAttribute("100 per page")]
            _100PerPage = 100,
            [DescriptionAttribute("250 per page")]
            _250PerPage = 250
        }
        public static IContentTextBase GetShowAmountAsContentText(SHOW_AMOUNT value)
        {
            switch (value)
            {
                case SHOW_AMOUNT._10PerPage: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Paging_ShowAmt_10PerPage); break;
                case SHOW_AMOUNT._25PerPage: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Paging_ShowAmt_25PerPage); break;
                case SHOW_AMOUNT._50PerPage: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Paging_ShowAmt_50PerPage); break;
                case SHOW_AMOUNT._100PerPage: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Paging_ShowAmt_100PerPage); break;
                case SHOW_AMOUNT._250PerPage: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Paging_ShowAmt_250PerPage); break;
                default:
                    throw new InvalidOperationException("Value <" + value + "> not mapped");
            }
        }
        public enum FILE_ITEM_SHOW_AMOUNT
        {
            [DescriptionAttribute("10 per page")]
            _10PerPage = 10,
            [DescriptionAttribute("25 per page")]
            _25PerPage = 25,
            [DescriptionAttribute("50 per page")]
            _50PerPage = 50,
            [DescriptionAttribute("100 per page")]
            _100PerPage = 100,
            [DescriptionAttribute("All")]
            All = 1000
        }

        public enum FILE_ITEM_SORT_BY
        {
            [DescriptionAttribute("Date Uploaded (Newest First)")]
            DateDesc,
            [DescriptionAttribute("Date Uploaded (Oldest First)")]
            DateAsc,
            [DescriptionAttribute("Uploaded By (A-Z)")]
            UploadedByAsc,
            [DescriptionAttribute("Uploaded By (Z-A)")]
            UploadedByDesc,
            [DescriptionAttribute("Title (A-Z)")]
            TitleAsc,
            [DescriptionAttribute("Title (Z-A)")]
            TitleDesc
        }


        public enum ORDER_HISTORY_SORT_BY_PREFIX_IDENTIFIER
        {
            OrderHistory_Combo_Prefix
        }

        public enum ORDER_HISTORY_SORT_BY
        {
            [DescriptionAttribute("Date (Oldest First)")]
            DateAsc,
            [DescriptionAttribute("Date (Newest First)")]
            DateDesc
        }

        public enum CLASSIFIEDS_SORT_BY
        {
            [DescriptionAttribute("Relevance"), Priority(0)]
            Relevance,
            [DescriptionAttribute("Date (Oldest First)"), Priority(10)]
            DateAsc,
            [DescriptionAttribute("Date (Newest First)"), Priority(10)]
            DateDesc,
            [DescriptionAttribute("Title (A..Z)"), Priority(20)]
            TitleAsc,
            [DescriptionAttribute("Title (Z..A)"), Priority(30)]
            TitleDesc,
            [DescriptionAttribute("Price (Cheapest First)"), Priority(40)]
            PriceAsc,
            [DescriptionAttribute("Price (Expensive First)"), Priority(50)]
            PriceDesc
        }

        public static IContentTextBase OrderHistorySortByToContentText(ORDER_HISTORY_SORT_BY value)
        {
            switch (value)
            {
                case ORDER_HISTORY_SORT_BY.DateAsc: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Sorting_DateAscending);
                case ORDER_HISTORY_SORT_BY.DateDesc: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.General_Sorting_DateDescending);
                default: throw new NotImplementedException("Value <" + value + "> not mapped");
            }
        }

        public enum ARTICLE_TYPE
        {
            None,
            ContentPages,
            Events,
            NewsItems,
            BlogPosts,

        }
        public enum INLINE_EDITING_TEXT_TYPE  //DO NOT CHANGE VALUES
        {
            PlainText = 0,
            Html = 1,
            HtmlWithAtLeastParagraphTag = 2 //Html content must include at least one paragraph tag
        }
        public enum CONTENT_TEXT_TYPE
        {
            PlainText = 0,
            Html = 1
        }

        public enum TESTIMONIAL_SORT_BY
        {
            [DescriptionAttribute("Name (A..Z)")]
            NameAsc,
            [DescriptionAttribute("Name (Z..A)")]
            NameDesc,
            [DescriptionAttribute("Date (Oldest First)")]
            DateAsc,
            [DescriptionAttribute("Date (Newest First")]
            DateDesc
        }
        
        public enum CONTENT_PAGE_SORT_BY
        {
            [DescriptionAttribute("Most Popular")]
            MostPopular,
            [DescriptionAttribute("Most Rated")]
            MostRated,
            [DescriptionAttribute("Title Ascending (A-Z)")]
            TitleAscending,
            [DescriptionAttribute("Title Descending (Z-A)")]
            TitleDescending,
            [DescriptionAttribute("Date Ascending  (Oldest First)")]
            DateAscending,
            [DescriptionAttribute("Date Descending (Newest First)")]
            DateDescending,
            [DescriptionAttribute("Relevance")]
            Relevance
        }
        public enum CONTENT_TEXT
        {
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " is required")]
            JSInternationalisation_FieldValidatorTexts_fieldRequired,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be a valid email address")]
            JSInternationalisation_FieldValidatorTexts_notValidEmail,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must contain only alpha numerical characters i.e. letters, digits and underscore (_)")]
            JSInternationalisation_FieldValidatorTexts_notValidAlphanumerical,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be a valid IP address, e.g. 192.168.210.123")]
            JSInternationalisation_FieldValidatorTexts_notValidIP,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be a valid credit card number")]
            JSInternationalisation_FieldValidatorTexts_notValidCreditCard,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be a valid website URL")]
            JSInternationalisation_FieldValidatorTexts_notValidWebsite,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " cannot contain less than " + JSInternationalisation.FieldValidatorTexts.TAG_LENGTH + " character(s)")]
            JSInternationalisation_FieldValidatorTexts_cannotContainLessThanCharRange,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " cannot contain more than " + JSInternationalisation.FieldValidatorTexts.TAG_LENGTH + " character(s)")]
            JSInternationalisation_FieldValidatorTexts_cannotContainMoreThanCharRange,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " cannot contain more than " + JSInternationalisation.FieldValidatorTexts.TAG_LENGTH + " word(s)")]
            JSInternationalisation_FieldValidatorTexts_cannotContainMoreThanWordsRange,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be a file with one of the following extension(s): " + JSInternationalisation.FieldValidatorTexts.TAG_VALUE)]
            JSInternationalisation_FieldValidatorTexts_notValidFileExtension,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be one of: " + JSInternationalisation.FieldValidatorTexts.TAG_VALUE)]
            JSInternationalisation_FieldValidatorTexts_notValidValueIn,
            [Description("(case-sensitive)")]
            JSInternationalisation_FieldValidatorTexts_caseSensitive,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " - invalid value")]
            JSInternationalisation_FieldValidatorTexts_invalidValue,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be smaller than or equal to " + JSInternationalisation.FieldValidatorTexts.TAG_VALUE)]
            JSInternationalisation_FieldValidatorTexts_numberMustBeSmallerOrEqualToRange,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be greater than or equal to " + JSInternationalisation.FieldValidatorTexts.TAG_VALUE)]
            JSInternationalisation_FieldValidatorTexts_numberMustBeGreaterOrEqualToRange,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be within the range " + JSInternationalisation.FieldValidatorTexts.TAG_VALUE)]
            JSInternationalisation_FieldValidatorTexts_numberMustBeWithinRange,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be positive")]
            JSInternationalisation_FieldValidatorTexts_numberMustBePositive,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be negative")]
            JSInternationalisation_FieldValidatorTexts_numberMustBeNegative,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be a whole number")]
            JSInternationalisation_FieldValidatorTexts_numberMustBeInteger,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " - Invalid number")]
            JSInternationalisation_FieldValidatorTexts_numberNotValid,

            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be between or equal to " + JSInternationalisation.FieldValidatorTexts.TAG_VALUE)]
            JSInternationalisation_FieldValidatorTexts_dateMustBeBetweenRange,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be before or equal to " + JSInternationalisation.FieldValidatorTexts.TAG_VALUE)]
            JSInternationalisation_FieldValidatorTexts_dateMustBeBeforeRange,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " must be after or equal to " + JSInternationalisation.FieldValidatorTexts.TAG_VALUE)]
            JSInternationalisation_FieldValidatorTexts_dateMustBeAfterRange,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " - Invalid date.  Date must be in the format: " + JSInternationalisation.FieldValidatorTexts.TAG_VALUE)]
            JSInternationalisation_FieldValidatorTexts_dateNotValid,

            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELDS + " - At least one of the fields must be filled")]
            JSInternationalisation_FieldValidatorTexts_atLeastOneIsRequired,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELDS + " - Values does not match")]
            JSInternationalisation_FieldValidatorTexts_sameValuesRequired,
            [Description(JSInternationalisation.FieldValidatorTexts.TAG_FIELDS + " - Values cannot be the same")]
            JSInternationalisation_FieldValidatorTexts_notSameValuesRequired,
            [Description("Please wait to finish validating " + JSInternationalisation.FieldValidatorTexts.TAG_FIELD + " with server")]
            JSInternationalisation_FieldValidatorTexts_waitingForAjaxToLoad,


            [Description("Password must include both lower and upper case letters")]
            JSInternationalisation_FieldValidatorTexts_passwordMustIncludeBothLowerAndUpperCase,
            [Description("Password must include at least one number")]
            JSInternationalisation_FieldValidatorTexts_passwordMustIncludeAtLeastOneNumber,
            [Description("Password must include at least one special characters (_ * ? ...)")]
            JSInternationalisation_FieldValidatorTexts_passwordMustIncludeASpecialCharacter,
            [Description("Password must include alphabetical characters")]
            JSInternationalisation_FieldValidatorTexts_passwordMustIncludeAlphabeticalCharacters,
            [ContentTextDefaultValues(Content = "You have successfully updated your profile!")]
            MembersArea_Profile_UpdatedSuccessful,
            [ContentTextDefaultValues(Content = "An error has been encountered, please try again. If problem persists, contact administration")]
            General_ErrorEncountered,
            [ContentTextDefaultValues(Content = "Name")]
            MembersArea_ProfileFields_Name,
            [ContentTextDefaultValues(Content = "ID Card")]
            MembersArea_ProfileFields_IDCard,
            [ContentTextDefaultValues(Content = "VAT Number")]
            MembersArea_ProfileFields_VatNumber,
            [ContentTextDefaultValues(Content = "Surname")]
            MembersArea_ProfileFields_Surname,
            [ContentTextDefaultValues(Content = "Street")]
            MembersArea_ProfileFields_Street,
            [ContentTextDefaultValues(Content = "City")]
            MembersArea_ProfileFields_City,
            [ContentTextDefaultValues(Content = "ZIP / Postal Code")]
            MembersArea_ProfileFields_PostalCode,
            [ContentTextDefaultValues(Content = "Birth Date.")]
            MembersArea_ProfileFields_DOB,
            [ContentTextDefaultValues(Content = "Email")]
            MembersArea_ProfileFields_Email,
            [ContentTextDefaultValues(Content = "Confirm Email")]
            MembersArea_ProfileFields_ConfirmEmail,
            [ContentTextDefaultValues(Content = "Mobile Phone")]
            MembersArea_ProfileFields_MobilePhone,
            [ContentTextDefaultValues(Content = "Username")]
            MembersArea_ProfileFields_Username,
            [ContentTextDefaultValues(Content = "Password")]
            MembersArea_ProfileFields_Password,
            [ContentTextDefaultValues(Content = "Old Password")]
            MembersArea_ProfileFields_OldPassword,
            [ContentTextDefaultValues(Content = "Services Offered")]
            MembersArea_ProfileFields_ServicesOffered,
            [ContentTextDefaultValues(Content = "Confirm Password")]
            MembersArea_ProfileFields_ConfirmPassword,
            [ContentTextDefaultValues(Content = "Country")]
            MembersArea_ProfileFields_Country,
            [ContentTextDefaultValues(Content = "Email already exists")]
            MembersArea_ProfileFields_EmailAlreadyExists,
            [ContentTextDefaultValues(Content = "No results found.")]
            General_Paging_NoResultsText,
            [ContentTextDefaultValues(Content = "results")]
            General_Paging_ResultsTextPlural,
            [ContentTextDefaultValues(Content = "result")]
            General_Paging_ResultsTextSingular,
            [ContentTextDefaultValues(Content = "Showing <span class='strong'>[FROM] - [TO]</span> of <span class='strong'>[TOTAL]</span> [ITEMS]", ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html)]
            General_Paging_ShowingResultsFullText,
            [ContentTextDefaultValues(Content = "items")]
            General_Paging_ItemsText,
            [ContentTextDefaultValues(Content = "«")]
            General_Paging_Navigation_ButtonFirstText,
            [ContentTextDefaultValues(Content = "»")]
            General_Paging_Navigation_ButtonLastText,
            [ContentTextDefaultValues(Content = "›")]
            General_Paging_Navigation_ButtonNextText,
            [ContentTextDefaultValues(Content = "‹")]
            General_Paging_Navigation_ButtonPrevText,
            [ContentTextDefaultValues(Content = "Sort By")]
            General_Paging_SortByText,
            [ContentTextDefaultValues(Content = "View")]
            General_Paging_ViewAmountText,
            [ContentTextDefaultValues(Content = "Invoice")]
            Orders_InvoiceSingular,
            [ContentTextDefaultValues(Content = "Invoices")]
            Orders_InvoicePlural,
            [ContentTextDefaultValues(Content = "Submit")]
            General_Button_Submit,
            [ContentTextDefaultValues(Content = "Remember Me")]
            MembersArea_ProfileFields_RememberMe,
            [ContentTextDefaultValues(Content = "Forgot Password?")]
            MembersArea_ProfileFields_ForgotPassword,
            [ContentTextDefaultValues(Content = "Or select one of these third party accounts")]
            MembersArea_SocialLogin_Message,
            [ContentTextDefaultValues(Content = "<a href='" + Tokens.REGISTER_URL + "' target='_top'>Register an account</a>", ContentType = Enums.CONTENT_TEXT_TYPE.Html)]
            MembersArea_ProfileFields_Register,
            [ContentTextDefaultValues(Content = "Age Confirmation & Terms Acceptance")]
            MembersArea_ProfileFields_AgeTermsConditions,
            [ContentTextDefaultValues(Content = "You cannot update your email address.  Please contact us if you need to update your email address")]
            MembersArea_ProfileFields_EmailInformationMessage,
            [ContentTextDefaultValues(Content = "You cannot update your username.  Please contact us if you need to update your email address")]
            MembersArea_ProfileFields_UsernameInformationMessage,
            [ContentTextDefaultValues(Content = "I confirm that I am over 18 years &amp; have read and accepted your <a target='_blank' href='" + Constants.Tokens.TAG_TERMS_LINK + "'>terms & conditions</a>")]
            MembersArea_ProfileFields_ConfirmationText,
            [ContentTextDefaultValues(Content = "Incorrect password, please try again")]
            MembersArea_ProfileFields_IncorrectPasswordMessage,
            [ContentTextDefaultValues(Content = "Enquiry")]
            MembersArea_ProfileFields_Enquiry,
            [ContentTextDefaultValues(Content = "Online Enquiry Form")]
            Contact_EnquiryForm_Title,
            [ContentTextDefaultValues(Content = "Submit Enquiry")]
            Contact_EnquiryForm_SubmitBtnText,
            [ContentTextDefaultValues(Content = "Address")]
            Contact_Details_Address,
            [ContentTextDefaultValues(Content = "Fax")]
            Contact_Details_Fax,
            [ContentTextDefaultValues(Content = "Map")]
            Contact_Details_Map,
            [ContentTextDefaultValues(Content = "Telephone")]
            Contact_Details_Telephone,
            [ContentTextDefaultValues(Content = "Website")]
            Contact_Details_Website,
            [ContentTextDefaultValues(Content = "enlarge map")]
            Contact_Details_MapText,
            [ContentTextDefaultValues(Content = "Contact Details")]
            Contact_Details_Title,
            [ContentTextDefaultValues(Content = "New Password")]
            MembersArea_ResetPassword_NewPassword,
            [ContentTextDefaultValues(Content = "Confirm Password")]
            MembersArea_ResetPassword_ConfirmPassword,
            [ContentTextDefaultValues(Content = "Invalid Login Details")]
            MembersArea_Login_InvalidLoginDetailsText,
            [ContentTextDefaultValues(Content = "Make sure you are visiting the correct link. If problem persists, please contact the administration")]
            MembersArea_ResetPassword_InvalidCodeText,
            [ContentTextDefaultValues(Content = "The email provided does not exist. Please try again")]
            MembersArea_ForgotPassword_InvalidEmail,
            [ContentTextDefaultValues(Content = "You must be logged in to access this section")]
            BasePage_MemberAuthentication_ErrorMessage,
            AnythingSlider_StartText,
            AnythingSlider_StopText,
            [ContentTextDefaultValues(Content = "You have chosen to self exclude your account. Please contact administration.")]
            MembersArea_Login_SelfExcludedErrorText,
            [ContentTextDefaultValues(Content = "Your account is blocked.")]
            MembersArea_Login_BlockedErrorText,
            [ContentTextDefaultValues(Content = "Your account is not yet activated. Please follow the instructions you were given on registration.")]
            MembersArea_Login_NotActivatedErrorText,

            [ContentTextDefaultValues(Content = "Your account is blocked.  Please contact us.")]
            MembersArea_Login_ResultMessage_Blocked,
            [ContentTextDefaultValues(Content = "Invalid Login Details.")]
            MembersArea_Login_ResultMessage_InvalidUser,
            [ContentTextDefaultValues(Content = "Invalid Login Details.")]
            MembersArea_Login_ResultMessage_InvalidPass,
            [ContentTextDefaultValues(Content = "Your account is not yet activated. Please follow the instructions you were given on registration.")]
            MembersArea_Login_ResultMessage_NotActivated,
            [ContentTextDefaultValues(Content = "Your account has been terminated.")]
            MembersArea_Login_ResultMessage_TerminatedAccount,
            [ContentTextDefaultValues(Content = "Your account is not accepted.")]
            MembersArea_Login_ResultMessage_NotAccepted,
            [ContentTextDefaultValues(Content = "You have been logged in successfully.")]
            MembersArea_Login_ResultMessage_Ok,
            [ContentTextDefaultValues(Content = "You have self excluded your account and will be reactivated on " + BusinessLogic_v3.Constants.Tokens.DATE + ".  Kindly contact us if you have any enquiries.")]
            MembersArea_Login_ResultMessage_SelfExcluded,

            [ContentTextDefaultValues(Content = Constants.Tokens.TAG_QUANTITY + " credit")]
            Purchase_CreditPurchaseDiffrenceTitleTextSingular,
            [ContentTextDefaultValues(Content = Constants.Tokens.TAG_QUANTITY + " credits")]
            Purchase_CreditPurchaseDiffrenceTitleTextPlural,

            [ContentTextDefaultValues(Content = "You must be logged in to access this page.  Please login.")]
            MembersArea_General_YouMustBeLoggedInToAccessThisPage,
            [ContentTextDefaultValues(Content = "You cannot be logged in to access this page.  Please logout.")]
            MembersArea_General_YouMustNotBeLoggedInToAccessThisPage,
            Members_Registration_Result,
            MembersArea_Login_ResultMessage,
            General_HideMessage,

            // INVOICE SECTION
            [ContentTextDefaultValues(Content = "Qty.")]
            MembersArea_Invoice_Quantity,
            [ContentTextDefaultValues(Content = "Description")]
            MembersArea_Invoice_DescriptionText,
            [ContentTextDefaultValues(Content = "Unit Price")]
            MembersArea_Invoice_UnitPriceText,
            [ContentTextDefaultValues(Content = "Total")]
            MembersArea_Invoice_TotalPriceText,
            [ContentTextDefaultValues(Content = "VAT %")]
            MembersArea_Invoice_TaxText,
            [ContentTextDefaultValues(Content = "Tel")]
            MembersArea_Invoice_TelephoneLabel,
            [ContentTextDefaultValues(Content = "Mob")]
            MembersArea_Invoice_MobileLabel,
            [ContentTextDefaultValues(Content = "Email")]
            MembersArea_Invoice_EmailLabel,
            [ContentTextDefaultValues(Content = "Invoice")]
            MembersArea_Invoice_HeadingTitle,
            [ContentTextDefaultValues(Content = "Client Details")]
            MembersArea_Invoice_ClientDetailsTitle,
            [ContentTextDefaultValues(Content = "Discount")]
            MembersArea_Invoice_DiscountText,
            [ContentTextDefaultValues(Content = "Shipping")]
            MembersArea_Invoice_ShippingText,
            [ContentTextDefaultValues(Content = "Invoice Details")]
            MembersArea_Invoice_DetailsTitle,
            [ContentTextDefaultValues(Content = "Payment Terms")]
            MembersArea_Invoice_PaymentTermsTitle,
            [ContentTextDefaultValues(Content = "Subtotal")]
            MembersArea_Invoice_SubTotal,
            [ContentTextDefaultValues(Content = "Username already exists")]
            MembersArea_ProfileFields_UsernameAlreadyExists,
            [ContentTextDefaultValues(Content = "You have entered an incorrect old password.  If you have problems changing your password, contact us.")]
            MembersArea_ProfileFields_IncorrectOldPassword,
            [ContentTextDefaultValues(Content = "Password and confirm password does not match.  Please make sure you confirm your password correctly.")]
            MembersArea_ProfileFields_IncorrectConfirmPassword,
            [ContentTextDefaultValues(Content = "No Expiry")]
            MembersArea_ResponsibleGaming_YourLimits_NoExpiry,
            [ContentTextDefaultValues(Content = "[VALID_UNTIL_DATE] (new limit of [NEW_LIMIT_VALUE] / [NEW_LIMIT_FREQUENCY] starts on [NEW_LIMIT_DATE])")]
            MembersArea_ResponsibleGaming_YourLimits_NewLimitSoon,
            CountdownTimer_Days,
            CountdownTimer_Hours,
            CountdownTimer_Min,
            CountdownTimer_Sec,
            [ContentTextDefaultValues(Content = "Submit")]
            Listing_DateFilter_Submit,
            [ContentTextDefaultValues(Content = "To")]
            Listing_DateFilter_ToText,
            [ContentTextDefaultValues(Content = "From")]
            Listing_DateFilter_FromText,
            [ContentTextDefaultValues(Content = "View your invoice: <a href=\"[INVOICE_URL]\" target=\"_blank\">#[INVOICE_NUMBER]</a>", ContentType = Enums.CONTENT_TEXT_TYPE.Html)]
            Orders_Invoice_Link,

            [ContentTextDefaultValues(Content = "No order exists for the given information.  If you have copied this link from an email, make sure that you copied the entire url.  If you still have any problems, do not hesitate to contact us.", ContentType = Enums.CONTENT_TEXT_TYPE.Html)]
            Orders_PayOnline_OrderDoesNotExist,
            [ContentTextDefaultValues(Content = "This order has already been paid successfully!", ContentType = Enums.CONTENT_TEXT_TYPE.Html)]
            Orders_PayOnline_OrderAlreadyPaid,
            [ContentTextDefaultValues(Content = "Your credit purchase was successful.")]
            Orders_Purchase_Successful_Text,
            [ContentTextDefaultValues(Content = "Order was completed with cheque. Please send the cheque to the following address:")]
            Orders_Purchase_With_Cheque_Text,
            [ContentTextDefaultValues(Content = "Order was completed but manual approval is still pending.")]
            Orders_Purchase_PendingOtherManualPayment_Text,
            [ContentTextDefaultValues(Content = "Credit Purchase Successful")]
            Orders_Purchase_Successful_Title,
            [ContentTextDefaultValues(Content = "You must be at least " + Constants.Tokens.MINIMUM_MEMBER_AGE_ALLOWED + " years old to register.")]
            Profile_Fields_MinimumAgeErrorMsg,
            [ContentTextDefaultValues(Content = "You must be at least " + Constants.Tokens.MINIMUM_MEMBER_AGE_ALLOWED + " years old to register if you are from " + Constants.Tokens.COUNTRY + ".")]
            Profile_Fields_MinimumAgeErrorMsgForCountry,
            [ContentTextDefaultValues(Content = "Full Name")]
            Article_Comment_FullName,
            [ContentTextDefaultValues(Content = "Email Address")]
            Article_Comment_EmailAddress,
            [ContentTextDefaultValues(Content = "Comment")]
            Article_Comment_Comment,
            [ContentTextDefaultValues(Content = "Post Comment")]
            Article_Comment_PostCommentTitle,
            [ContentTextDefaultValues(Content = "Post Reply")]
            Article_Comment_PostReplyTitle,
            [ContentTextDefaultValues(Content = "Post Comment")]
            Article_Comment_PostComment,
            [ContentTextDefaultValues(Content = "Contact No.")]
            Contact_EnquiryForm_ContactNo,
            [ContentTextDefaultValues(Content = "Company")]
            Contact_EnquiryForm_Company,
            [ContentTextDefaultValues(Content = "Last updated on " + Constants.Tokens.DATE)]
            Article_LastEdited,
            [ContentTextDefaultValues(Content = Constants.Tokens.DATE)]
            Article_CreatedOn,
            [ContentTextDefaultValues(Content = Constants.Tokens.COMMENTS_COUNT + " comments")]
            Article_ViewComments_PluralCount,
            [ContentTextDefaultValues(Content = Constants.Tokens.COMMENTS_COUNT + " comment")]
            Article_ViewComments_SingularCount,
            [ContentTextDefaultValues(Content = "Currently there are no comments for this blog")]
            Article_ViewComments_NoComments,
            Footer_FooterSitemap_Sitename,
            Footer_FooterProductCategories_Title,
            [ContentTextDefaultValues(Content = "Comment submitted successfully.Your comment will be added after being moderated by administrators.")]
            Article_Comment_SuccessMsg,
            [ContentTextDefaultValues(Content = "Comment has not been submitted.")]
            Article_Comment_ErrorMsg,
            [ContentTextDefaultValues(Content = "Get In Touch")]
            Footer_FooterContact_Title,
            [ContentTextDefaultValues(Content = "We would be glad to hear from you on any enquiry you might have.  Our team will get back to you shortly.")]
            Contact_EnquiryForm_Description,
            [ContentTextDefaultValues(Content = "website design & development by CasaSoft (Malta)")]
            Footer_CasaSoft,
            [ContentTextDefaultValues(Content = "&copy;  " + Constants.Tokens.SITE_COPYRIGHT_YEAR + ".  " + Constants.Tokens.SITE_COPYRIGHT_SITE_NAME + ". All Rights Reserved. ")]
            Footer_SiteCopyrightText,
            Footer_FooterContact_ContactUsNow,
            [ContentTextDefaultValues(Content = "Image Gallery")]
            ImageGallery_Title,
            [ContentTextDefaultValues(Content = "Click on images above to enlarge")]
            ImageGallery_Desc,
            [ContentTextDefaultValues(Content = "Specifications")]
            ProductPage_Specifications_Title,
            [ContentTextDefaultValues(Content = "File size is too big! Max. " + Constants.Tokens.MAX_FILE_SIZE_KB + "MBs.")]
            AttachmentFileSizeTooBig,
            [ContentTextDefaultValues(Content = "Twitter")]
            Contact_Details_Twitter,
            [ContentTextDefaultValues(Content = "Facebook")]
            Contact_Details_Facebook,
            [ContentTextDefaultValues(Content = "Follow us on twitter")]
            Contact_Details_TwitterText,
            [ContentTextDefaultValues(Content = "Follow us on faceboook")]
            Contact_Details_FacebookText,
            [ContentTextDefaultValues(Content = Constants.Tokens.MANAGING_DIRECTOR + " (Managing Director)")]
            Contact_Details_ManagingDirectorText,
            [ContentTextDefaultValues(Content = "&laquo; previous blog entry")]
            Article_PreviousArticle,
            [ContentTextDefaultValues(Content = "next blog entry &raquo;")]
            Article_NextArticle,
            [ContentTextDefaultValues(Content = "How did you find us?")]
            Contact_EnquiryForm_HowDidYouFindUs,
            [ContentTextDefaultValues(Content = "Subject")]
            Contact_EnquiryForm_Subject,
            [ContentTextDefaultValues(Content = "Company")]
            MembersArea_ProfileFields_Company,
            [ContentTextDefaultValues(Content = "Telephone")]
            MembersArea_ProfileFields_Telephone,
            [ContentTextDefaultValues(Content = "Linkedin")]
            Contact_Details_Linkedin,
            [ContentTextDefaultValues(Content = "Call us on skype")]
            Contact_Details_Skype,
            [ContentTextDefaultValues(Content = "Submit")]
            MembersArea_ProfileFields_RegistrationButtonText,
            [ContentTextDefaultValues(Content = "Submit")]
            MembersArea_ProfileFields_UpdateProfileButtonText,
            [ContentTextDefaultValues(Content = "I confirm that I am over 18 years &amp; have read and accepted your <a href='[TERMS_LINK]'>terms & conditions</a>", ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html)]
            MembersArea_ProfileFields_TermsAndConditionText,
            [ContentTextDefaultValues(Content = "Preferred Language")]
            MembersArea_ProfileFields_PreferredLanguage,
            [ContentTextDefaultValues(Content = "Your Timezone")]
            MembersArea_ProfileFields_Timezone,
            [ContentTextDefaultValues(Content = "Gender")]
            MembersArea_ProfileFields_Gender,
            [ContentTextDefaultValues(Content = "VAT Number")]
            MembersArea_ProfileFields_VAT,
            [ContentTextDefaultValues(Content = "Profile")]
            MembersArea_ProfileFields_Profile,
            [ContentTextDefaultValues(Content = "Logo")]
            MembersArea_ProfileFields_Logo,
            [ContentTextDefaultValues(Content = "Website")]
            MembersArea_ProfileFields_Website,
            [ContentTextDefaultValues(Content = "View Terms & Conditions")]
            ProductPage_ViewTermsAndConditions,
            Products_FeaturedProducts,
            Products_RelatedProducts,
            [ContentTextDefaultValues(Content = "Grams (g)")]
            General_WeightUnits_Grams,
            [ContentTextDefaultValues(Content = "Kilograms (kg)")]
            General_WeightUnits_KiloGrams,
            [ContentTextDefaultValues(Content = "Tonne")]
            General_WeightUnits_Tonne,
            [ContentTextDefaultValues(Content = "Pound (lb)")]
            General_WeightUnits_Pound,
            [ContentTextDefaultValues(Content = "Ounce (oz)")]
            General_WeightUnits_Ounce,
            [ContentTextDefaultValues(Content = "Cubmic Metres (m³)")]
            General_VolumeUnits_CubicMetre,
            [ContentTextDefaultValues(Content = "Cubic Centimetres (cm³)")]
            General_VolumeUnits_CubicCentimetre,
            [ContentTextDefaultValues(Content = "Fahrenheit (F)")]
            General_TemperatureUnits_Fahrenheit,
            [ContentTextDefaultValues(Content = "Celcius (°C)")]
            General_TemperatureUnits_Celcius,
            [ContentTextDefaultValues(Content = "Personal Information")]
            MembersArea_ProfileFields_TextPersonalInformation,
            [ContentTextDefaultValues(Content = "Account Information")]
            MembersArea_ProfileFields_TextAccountInformation,
            [ContentTextDefaultValues(Content = "Paid")]
            MembersArea_OrderHistory_Status_Paid,
            [ContentTextDefaultValues(Content = "Not Paid")]
            MembersArea_OrderHistory_Status_NotPaid,
            [ContentTextDefaultValues(Content = "Date")]
            MembersArea_OrderHistory_Heading_Date,
            [ContentTextDefaultValues(Content = "Description")]
            MembersArea_OrderHistory_Heading_Description,
            [ContentTextDefaultValues(Content = "Status")]
            MembersArea_OrderHistory_Heading_Status,
            [ContentTextDefaultValues(Content = "Invoice")]
            MembersArea_OrderHistory_Heading_Invoice,
            [ContentTextDefaultValues(Content = "Total")]
            MembersArea_OrderHistory_Heading_Total,
            [ContentTextDefaultValues(Content = "Date")]
            MembersArea_CreditStatement_Heading_Date,
            [ContentTextDefaultValues(Content = "Description")]
            MembersArea_CreditStatement_Heading_Description,
            [ContentTextDefaultValues(Content = "Monthly Credits")]
            MembersArea_CreditStatement_Heading_MonthlyCredits,
            [ContentTextDefaultValues(Content = "Additional Credits")]
            MembersArea_CreditStatement_Heading_AdditionalCredits,
            [ContentTextDefaultValues(Content = "Total Credits")]
            MembersArea_CreditStatement_Heading_TotalCredits,
            [ContentTextDefaultValues(Content = "You must be logged in to access this section")]
            General_YouMustBeLoggedInToAccessPage,
            MembersArea_ResponsibleGaming_YourLimits_Freq_Day,
            MembersArea_ResponsibleGaming_YourLimits_Freq_Month,
            MembersArea_ResponsibleGaming_YourLimits_Freq_Week,
            MembersArea_ResponsibleGaming_YourLimits_Freq_Year,
            [ContentTextDefaultValues(Content = "Email Already Exists")]
            Members_Registration_Result_EmailAlreadyExists,
            [ContentTextDefaultValues(Content = "Registration Successful")]
            Members_Registration_Result_Success,
            [ContentTextDefaultValues(Content = "Username Already Exists")]
            Members_Registration_Result_UsernameAlreadyExists,
            MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_Adjustment,
            MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_Won,
            MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_Withdraw,
            MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_RenewalBasedOnSubscription,
            MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_Purchase,
            MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_Deposit,
            [ContentTextDefaultValues(Content = "Date (Oldest First)")]
            General_Sorting_DateAscending,
            [ContentTextDefaultValues(Content = "Date (Newest First)")]
            General_Sorting_DateDescending,
            [ContentTextDefaultValues(Content = "10")]
            General_Paging_ShowAmt_10PerPage,
            [ContentTextDefaultValues(Content = "25")]
            General_Paging_ShowAmt_25PerPage,
            [ContentTextDefaultValues(Content = "50")]
            General_Paging_ShowAmt_50PerPage,
            [ContentTextDefaultValues(Content = "100")]
            General_Paging_ShowAmt_100PerPage,
            [ContentTextDefaultValues(Content = "250")]
            General_Paging_ShowAmt_250PerPage,
            [ContentTextDefaultValues(Content = "Title (A .. Z)")]
            Shop_ItemGroup_Sorting_TitleAscending,
            [ContentTextDefaultValues(Content = "Title (Z .. A)")]
            Shop_ItemGroup_Sorting_TitleDescending,
            [ContentTextDefaultValues(Content = "Price (Lowest First)")]
            Shop_ItemGroup_Sorting_PriceAscending,
            [ContentTextDefaultValues(Content = "Price (Highest First)")]
            Shop_ItemGroup_Sorting_PriceDescending,
            [ContentTextDefaultValues(Content = "Featured First")]
            Shop_ItemGroup_Sorting_FeaturedAscending,
            [ContentTextDefaultValues(Content = "Featured Last")]
            Shop_ItemGroup_Sorting_FeaturedDescending,
            [ContentTextDefaultValues(Content = "Priority")]
            Shop_ItemGroup_Sorting_Priority,
            General_Button_Clear,
            General_Password_TooWeak,
            General_Password_Perfect,
            General_Password_Strong,
            General_Password_Normal,
            General_Yes,
            General_No,
            [ContentTextDefaultValues(Content = "Register Account")]
            General_LoginFields_RegisterAccount,
            [ContentTextDefaultValues(Content = "Street 2")]
            MembersArea_ProfileFields_Street2,
            [ContentTextDefaultValues(Content = "Welcome " + Constants.Tokens.USER_FIRST_NAME + "!")]
            MasterPage_AuthenticationUser_WelcomeMessageLoggedIn,
            [ContentTextDefaultValues(Content = "Welcome, please login!")]
            MasterPage_AuthenticationUser_WelcomeMessageLoggedOut,
            [ContentTextDefaultValues(Content = Constants.Tokens.TAG_QUANTITY + " items")]
            Member_ShoppingCart_MasterPage_ItemsInCart,
            [ContentTextDefaultValues(Content = Constants.Tokens.TAG_QUANTITY + " item")]
            Member_ShoppingCart_MasterPage_ItemInCart,
            [ContentTextDefaultValues(Content = "Logout")]
            General_Logout,
            [ContentTextDefaultValues(Content = "You have been successfully logged out!")]
            General_Logout_Success,
            [ContentTextDefaultValues(Content = "Login")]
            General_LoginFields_LoginButtonText,
            [ContentTextDefaultValues(Content = "Searching for " + BusinessLogic_v3.Constants.Tokens.SEARCH_KEYWORDS + ":")]
            Search_KeywordsTitle,
            [ContentTextDefaultValues(Content = "<p>No results have been found</p>", ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html)]
            Search_NoResultsText,
            [ContentTextDefaultValues(Content = "Address 1")]
            ShoppingCart_DeliveryPage_Address1,
            [ContentTextDefaultValues(Content = "Address 2")]
            ShoppingCart_DeliveryPage_Address2,
            [ContentTextDefaultValues(Content = "Locality")]
            ShoppingCart_DeliveryPage_Locality,
            [ContentTextDefaultValues(Content = "Postcode")]
            ShoppingCart_DeliveryPage_PostCode,
            [ContentTextDefaultValues(Content = "Country")]
            ShoppingCart_DeliveryPage_Country,
            [ContentTextDefaultValues(Content = "Proceed")]
            ShoppingCart_DeliveryPage_ButtonProceed,
            [ContentTextDefaultValues(Content = "Item")]
            ShoppingCart_MyCart_Item,
            [ContentTextDefaultValues(Content = "Quantity")]
            ShoppingCart_MyCart_Quantity,
            [ContentTextDefaultValues(Content = "Unit Price")]
            ShoppingCart_MyCart_UnitPrice,
            [ContentTextDefaultValues(Content = "Home")]
            General_Home,
            [ContentTextDefaultValues(Content = "Search")]
            General_Search,
            [ContentTextDefaultValues(Content = "No items in cart")]
            Member_ShoppingCart_MasterPage_NoItemsInCart,
            [ContentTextDefaultValues(Content = "Item Code:" + BusinessLogic_v3.Constants.Tokens.PRODUCT_ITEM_CODE)]
            ProductPage_ItemCode,
            [ContentTextDefaultValues(Content = "Item Code")]
            ProductPage_ItemCodeLabel,
            [ContentTextDefaultValues(Content = "Quantity")]
            ProductPage_AddToCart_Quantity,
            [ContentTextDefaultValues(Content = "Material")]
            ProductPage_AddToCart_Material,
            [ContentTextDefaultValues(Content = "Add To Cart")]
            ProductPage_AddToCart_ButtonText,
            [ContentTextDefaultValues(Content = "Did you make any changes in the quantities?")]
            ShoppingCart_MyCart_DidYouMakeChangesToQuantities,
            [ContentTextDefaultValues(Content = "Update Quantities")]
            ShoppingCart_MyCart_UpdateQuantities,
            [ContentTextDefaultValues(Content = "Total:")]
            ShoppingCart_MyCart_Total,
            [ContentTextDefaultValues(Content = "Proceed To Checkout")]
            ShoppingCart_MyCart_ProceedToCheckout,
            [ContentTextDefaultValues(Content = "Quantities updated successfully!")]
            ShoppingCart_MyCart_QuantitiesUpdatedSuccessfully,
            [ContentTextDefaultValues(Content = "Add to cart")]
            ProductPage_AddToCart_Title,
            [ContentTextDefaultValues(Content = "Contact us from the form below should you have an enquiry on the above product.  We will get back to you shortly.")]
            ProductPage_ContactForm_EnquiryDescription,
            [ContentTextDefaultValues(Content = "Existing Customers")]
            ShoppingCart_ProceedToCheckOut_LoggedOut_Login_Title,
            [ContentTextDefaultValues(Content = "If you are an existing customer, please login to checkout:")]
            ShoppingCart_ProceedToCheckOut_LoggedOut_Login_Text,
            [ContentTextDefaultValues(Content = "New Customer")]
            ShoppingCart_ProceedToCheckOut_LoggedOut_Register_Title,
            [ContentTextDefaultValues(Content = "You will create an account during the checkout process.")]
            ShoppingCart_ProceedToCheckOut_LoggedOut_Register_Text,
            [ContentTextDefaultValues(Content = "Proceed To Payment")]
            ShoppingCart_CheckOut_ProceedToPayment,

            [ContentTextDefaultValues(Content = "Clicking on Proceed to Payment below will take you to a third party payment gateway which will handle payment securely.")]
            ShoppingCart_CheckOut_ProceedToPaymentText,
            [ContentTextDefaultValues(Content = "Order and pay later")]
            ShoppingCart_CheckOut_OrderAndPayLater,
            [ContentTextDefaultValues(Content =
                @"<p>By clicking on <strong>Order and pay later</strong> below, you place the order with the system.  A confirmation email will be sent.  Order payment can then be settlted either online or manually via cheque or cash.</p>")]
            ShoppingCart_CheckOut_OrderAndPayLater_Text,
            [ContentTextDefaultValues(Content = "You have successfully deleted an item from your cart!")]
            ShoppingCart_DeleteItem_Success,
            [ContentTextDefaultValues(Content = "An error occurred whilst processing your request. Please try again, if problem persists please contact administration")]
            ShoppingCart_DeleteItem_Error,
            [ContentTextDefaultValues(Content = "There are no items in your cart.", ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html)]
            ShoppingCart_NoItemsInCart,
            [ContentTextDefaultValues(Content = "If you have a discount / coupon code, enter it here:")]
            ShoppingCart_CheckOut_Discount_Text,
            [ContentTextDefaultValues(Content = "Apply")]
            ShoppingCart_CheckOut_Discount_Button,
            [ContentTextDefaultValues(Content = "Delivery:")]
            ShoppingCart_CheckOut_Details_Delivery,
            [ContentTextDefaultValues(Content = "Discount:")]
            ShoppingCart_CheckOut_Details_Discount,
            [ContentTextDefaultValues(Content = "<p>Clicking on Proceed to Payment below will take you to a third party payment gateway which will handle payment securely.</p>")]
            ShoppingCart_CheckOut_ProceedToPayment_Text,
            [ContentTextDefaultValues(Content = "The discount/coupon code you've just entered does not exist")]
            ShoppingCart_CheckOut_Discount_NotExists,
            [ContentTextDefaultValues(Content = "Your discount/coupon code has successfully been accepted")]
            ShoppingCart_CheckOut_Discount_Success,
            [ContentTextDefaultValues(Content = "The discount/couopon code you've just entered has expired")]
            ShoppingCart_CheckOut_Discount_ExpiredOrNotActiveYet,
            [ContentTextDefaultValues(Content = "Sub Total:")]
            ShoppingCart_MyCart_SubTotal,
            [ContentTextDefaultValues(Content = "items")]
            General_Items,
            [ContentTextDefaultValues(Content = "item")]
            General_Item,
            [ContentTextDefaultValues(Content = "You have successfully added " + BusinessLogic_v3.Constants.Tokens.PRODUCT_REQUESTED_QUANTITY + " " + BusinessLogic_v3.Constants.Tokens.ITEM_TEXT + " this item to your cart")]
            ProductPage_AddToCart_Success,
            [ContentTextDefaultValues(Content = "There are is not sufficient stock available for your rquest. The available ammount is " + BusinessLogic_v3.Constants.Tokens.PRODUCT_AVAILABLE_QUANTITY + " " + BusinessLogic_v3.Constants.Tokens.ITEM_TEXT)]
            ProductPage_AddToCart_NotEnoughQuantity_1,
            [ContentTextDefaultValues(Content = "No items in stock are available for this productes")]
            ProductPage_AddToCart_NotEnoughQuantity_2,
            [ContentTextDefaultValues(Content = "orders")]
            MembersArea_OrderHistory_ResultsTextPlural,
            [ContentTextDefaultValues(Content = "order")]
            MembersArea_OrderHistory_ResultsTextSingular,
            [ContentTextDefaultValues(Content = "Are you sure you want to delete this item from your cart?")]
            ShoppingCart_Delete_ConfirmMessage,
            [ContentTextDefaultValues(Content = "click on image to enlarge")]
            ProductPage_Slider_ClikImageToEnlarge,
            [ContentTextDefaultValues(Content = "Login with Facebook")]
            General_LoginFields_FacebookLoginButtonText,
            [ContentTextDefaultValues(Content = "You have successfully registered your merchant account!")]
            MembersArea_Profile_RegisteredSuccessful,
            [ContentTextDefaultValues(Content = "Invoice No:")]
            MembersArea_Invoice_InvoiceNoLabel,
            [ContentTextDefaultValues(Content = "Invoice Date:")]
            MembersArea_Invoice_InvoiceDateText,
            [ContentTextDefaultValues(Content = "statement")]
            MembersArea_CreditStatement_ResultsTextSingular,
            [ContentTextDefaultValues(Content = "statements")]
            MembersArea_CreditStatement_ResultsTextPlural,
            Products_NoDescriptionAvailable,
            SubscriptionPurchase_OrderItemDesription,
            SubscriptionPurchase_OrderItemTitle,
            [ContentTextDefaultValues(Content = "Credit Difference")]
            MembersArea_CreditStatement_Heading_CreditDifference,
            [ContentTextDefaultValues(Content = "VAT: " + BusinessLogic_v3.Constants.Tokens.INVOICE_VAT_VALUE + "%")]
            MembersArea_OrderHistory_Invoice_VAT,
            [ContentTextDefaultValues(Content = "Description:")]
            ProductPage_Description_DescriptionTitle,
            [ContentTextDefaultValues(Content = "Material:")]
            ProductPage_Description_MaterialTitle,
            [ContentTextDefaultValues(Content = "Available Colours:")]
            ProductPage_Description_AvailableColours,
            [ContentTextDefaultValues(Content = "There is no description available yet for this product.")]
            ProductPage_Description_NoDescription,
            [ContentTextDefaultValues(Content = "As Seen Above")]
            ProductPage_Description_NoAvailableColours,
            [ContentTextDefaultValues(Content = "Search")]
            General_Search_FieldParameter,
            [ContentTextDefaultValues(Content = "Price")]
            ProductPage_AddToCart_Price,
            [ContentTextDefaultValues(Content = "Got an Enquiry?")]
            ProductPage_ContactForm_ToggleButtonText,
            [ContentTextDefaultValues(Content = "Discount Code")]
            MembersArea_ProfileFields_TextDiscountInformation,
            [ContentTextDefaultValues(Content = "You have a discount code <strong>" + BusinessLogic_v3.Constants.Tokens.DISCOUNT_COUPON_CODE + " - " + BusinessLogic_v3.Constants.Tokens.DISCOUNT_COUPON_TITLE + "</strong> applied to your account. #if " + BusinessLogic_v3.Constants.Tokens.DISCOUNT_COUPON_HAS_EXPIRY + " This will expire on <strong> " + BusinessLogic_v3.Constants.Tokens.DISCOUNT_COUPON_EXPIRES_ON + ".#end</strong>&nbsp;If you would like to enter a new discount code, kindly enter it below</p>", ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html)]
            MembersArea_ProfileFields_Discounts_Available_DescriptionText,
            [ContentTextDefaultValues(Content = "You do not have any discount codes associated with your profile yet. If you would like to enter a new discount code, kindly enter it below.", ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html)]
            MembersArea_ProfileFields_Discounts_NotAvailable_DescriptionText,
            [ContentTextDefaultValues(Content = "Discount Code")]
            MembersArea_ProfileFields_DiscountCode,
            [ContentTextDefaultValues(Content = "Account Type")]
            MembersArea_ProfileFields_AccountType,
            [ContentTextDefaultValues(Content = "I would like to receive daily digest of news in my mailbox")]
            MembersArea_ProfileFields_Promotions,
            [ContentTextDefaultValues(Content = "Apply")]
            MembersArea_ProfileFields_ButtonApplyText,
            [ContentTextDefaultValues(Content = "Discount Code cannot be left empty!")]
            MembersArea_ProfileFields_Discounts_PleaseEnterADiscountCode,
            [ContentTextDefaultValues(Content = "You have successfully added a new discount code to your account")]
            MembersArea_ProfileFields_Discounts_Success,
            [ContentTextDefaultValues(Content = "Discount code seems to be invalid or incorrect, please try again!")]
            MembersArea_ProfileFields_Discounts_Error,
            [ContentTextDefaultValues(Content = BusinessLogic_v3.Constants.Tokens.SHOW_AMOUNT_VALUE + " per page")]
            General_Paging_ShowAmt_GeneralPerPage,
            [ContentTextDefaultValues(Content = "Applicable Discounts")]
            Member_ShoppingCart_Discounts_ApplicableDiscounts,
            [ContentTextDefaultValues(Content = "The following discounts are available for this check out")]
            Member_ShoppingCart_Discounts_ApplicableDiscountsText,
            [ContentTextDefaultValues(Content = "Please Login or Register in order to successfully add this discount code to your account")]
            MasterPage_AuthenticationUser_Discount_NotLoggedIn,
            [ContentTextDefaultValues(Content = "VIP Code")]
            MasterPage_AuthenticationUser_VIPCode_VIPCodeTitle,
            [ContentTextDefaultValues(Content = "Total Price")]
            ShoppingCart_MyCart_TotalPrice,
            General_Fields_Currency,
            General_Fields_Price,
            [ContentTextDefaultValues(Content = BusinessLogic_v3.Constants.Tokens.TAG_AMOUNT + " per page")]
            Listing_ShowAmountText,
            [ContentTextDefaultValues(Content = "Provide your details below and we’ll keep you updated with any courses and updates we have.")]
            NewsletterSubscription_DescriptionText,
            [ContentTextDefaultValues(Content = "* Rest assured your details will remain private")]
            NewsletterSubscription_AssuranceText,
            [ContentTextDefaultValues(Content = "Submit")]
            NewsletterSubscription_SubmitBtnTxt,
            [ContentTextDefaultValues(Content = "You have successfully subscribed to our newsletter")]
            NewsletterSubscription_Subscribe_Success,
            [ContentTextDefaultValues(Content = "Unfortunately the email provided already exists. Please try again!")]
            NewsletterSubscription_Subscribe_EmailAlreadyExists,
            [ContentTextDefaultValues(Content = "Email Address")]
            NewsletterSubscription_Email_PlaceHolderTxt,
            [ContentTextDefaultValues(Content = "Name")]
            NewsletterSubscription_Name_PlaceHolderTxt,
            [ContentTextDefaultValues(Content = "Your enquiry has been successfully sent. We will get back to you as soon as possible")]
            ProductPage_ContactForm_EnquirySentSuccessfully,
            [ContentTextDefaultValues(Content = "No search criteria has been found")]
            Search_NoSearchCriteriaFound,
            [ContentTextDefaultValues(Content = BusinessLogic_v3.Constants.Tokens.TAG_AMOUNT + " items")]
            Orders_MultipleItemDescription,
            [ContentTextDefaultValues(Content = "View More")]
            General_LatestNews_ViewMore,
            [ContentTextDefaultValues(Content = "read more&raquo;", ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html)]
            General_GenericListingItem_ReadMore,
            [ContentTextDefaultValues(Content = "Total:")]
            MembersArea_Invoice_TotalHeaderPriceText,
            [ContentTextDefaultValues(Content = "Exc. VAT")]
            MembersArea_Invoice_VATSummary_HeaderExcVAT,
            [ContentTextDefaultValues(Content = "VAT Rate")]
            MembersArea_Invoice_VATSummary_HeaderVATRate,
            [ContentTextDefaultValues(Content = "VAT Amt.")]
            MembersArea_Invoice_VATSummary_HeaderVATAmount,
            [ContentTextDefaultValues(Content = "Inc. VAT")]
            MembersArea_Invoice_VATSummary_HeaderIncVAT,
            [ContentTextDefaultValues(Content = "Totals:")]
            MembersArea_Invoice_VATSummary_TotalsLabel,
            [ContentTextDefaultValues(Content = "VAT Summary")]
            MembersArea_Invoice_VATSummary_Title,
            [ContentTextDefaultValues(Content = "Media Gallery")]
            Article_MediaItems_Title,
            [ContentTextDefaultValues(Content = "January")]
            Calendar_Full_Month_Name_January,
            [ContentTextDefaultValues(Content = "February")]
            Calendar_Full_Month_Name_February,
            [ContentTextDefaultValues(Content = "March")]
            Calendar_Full_Month_Name_March,
            [ContentTextDefaultValues(Content = "April")]
            Calendar_Full_Month_Name_April,
            [ContentTextDefaultValues(Content = "May")]
            Calendar_Full_Month_Name_May,
            [ContentTextDefaultValues(Content = "June")]
            Calendar_Full_Month_Name_June,
            [ContentTextDefaultValues(Content = "July")]
            Calendar_Full_Month_Name_July,
            [ContentTextDefaultValues(Content = "August")]
            Calendar_Full_Month_Name_August,
            [ContentTextDefaultValues(Content = "September")]
            Calendar_Full_Month_Name_September,
            [ContentTextDefaultValues(Content = "October")]
            Calendar_Full_Month_Name_October,
            [ContentTextDefaultValues(Content = "November")]
            Calendar_Full_Month_Name_November,
            [ContentTextDefaultValues(Content = "December")]
            Calendar_Full_Month_Name_December,
            [ContentTextDefaultValues(Content = "Today")]
            Calendar_Text_Today,
            [ContentTextDefaultValues(Content = "Month")]
            Calendar_Text_Month,
            [ContentTextDefaultValues(Content = "Week")]
            Calendar_Text_Week,
            [ContentTextDefaultValues(Content = "Day")]
            Calendar_Text_Day,
            [ContentTextDefaultValues(Content = "Jan")]
            Calendar_Short_Month_Name_January,
            [ContentTextDefaultValues(Content = "Feb")]
            Calendar_Short_Month_Name_February,
            [ContentTextDefaultValues(Content = "Mar")]
            Calendar_Short_Month_Name_March,
            [ContentTextDefaultValues(Content = "Apr")]
            Calendar_Short_Month_Name_April,
            [ContentTextDefaultValues(Content = "May")]
            Calendar_Short_Month_Name_May,
            [ContentTextDefaultValues(Content = "Jun")]
            Calendar_Short_Month_Name_June,
            [ContentTextDefaultValues(Content = "Jul")]
            Calendar_Short_Month_Name_July,
            [ContentTextDefaultValues(Content = "Aug")]
            Calendar_Short_Month_Name_August,
            [ContentTextDefaultValues(Content = "Sep")]
            Calendar_Short_Month_Name_September,
            [ContentTextDefaultValues(Content = "Oct")]
            Calendar_Short_Month_Name_October,
            [ContentTextDefaultValues(Content = "Nov")]
            Calendar_Short_Month_Name_November,
            [ContentTextDefaultValues(Content = "Dec")]
            Calendar_Short_Month_Name_December,
            [ContentTextDefaultValues(Content = "Sun")]
            Calendar_Short_Day_Name_Sunday,
            [ContentTextDefaultValues(Content = "Sat")]
            Calendar_Short_Day_Name_Saturday,
            [ContentTextDefaultValues(Content = "Fri")]
            Calendar_Short_Day_Name_Friday,
            [ContentTextDefaultValues(Content = "Thur")]
            Calendar_Short_Day_Name_Thursday,
            [ContentTextDefaultValues(Content = "Wed")]
            Calendar_Short_Day_Name_Wednesday,
            [ContentTextDefaultValues(Content = "Tue")]
            Calendar_Short_Day_Name_Tuesday,
            [ContentTextDefaultValues(Content = "Mon")]
            Calendar_Short_Day_Name_Monday,
            [ContentTextDefaultValues(Content = "Sunday")]
            Calendar_Full_Day_Name_Sunday,
            [ContentTextDefaultValues(Content = "Saturday")]
            Calendar_Full_Day_Name_Saturday,
            [ContentTextDefaultValues(Content = "Friday")]
            Calendar_Full_Day_Name_Friday,
            [ContentTextDefaultValues(Content = "Thursday")]
            Calendar_Full_Day_Name_Thursday,
            [ContentTextDefaultValues(Content = "Wednesday")]
            Calendar_Full_Day_Name_Wednesday,
            [ContentTextDefaultValues(Content = "Tuesday")]
            Calendar_Full_Day_Name_Tuesday,
            [ContentTextDefaultValues(Content = "Monday")]
            Calendar_Full_Day_Name_Monday,
            [ContentTextDefaultValues(Content = Constants.Tokens.EVENT_TITLE + " student")]
            Testimonials_User_Event,
            [ContentTextDefaultValues(Content = "Currently there are no testimonials")]
            Tesimonials_NoResultsText,
            [ContentTextDefaultValues(Content = "Login with Google")]
            General_LoginFields_GoogleLoginButtonText,
            [ContentTextDefaultValues(Content = "Tutors:")]
            EventPage_EventDetails_Label_Tutors,
            [ContentTextDefaultValues(Content = "Sessions:")]
            EventPage_EventDetails_Label_Sessions,
            [ContentTextDefaultValues(Content = "Duration:")]
            EventPage_EventDetails_Label_Duration,
            [ContentTextDefaultValues(Content = "Dates:")]
            EventPage_EventDetails_Label_Dates,
            [ContentTextDefaultValues(Content = "Class Size:")]
            EventPage_EventDetails_Label_ClassSize,
            [ContentTextDefaultValues(Content = "Price:")]
            EventPage_EventDetails_Label_Price,
            [ContentTextDefaultValues(Content = BusinessLogic_v3.Constants.Tokens.EVENT_MAXIMUM_SLOTS + " students")]
            EventPage_EventDetails_ClassSize,
            [ContentTextDefaultValues(Content = "Course Details")]
            EventPage_EventDetails_Title,
            [ContentTextDefaultValues(Content = "Anything you require regarding the coaurse can be found below:")]
            EventPage_EventDetails_Description,
            [ContentTextDefaultValues(Content = "Booking / Enquiry Form")]
            EventPage_Enquiry_Title,
            [ContentTextDefaultValues(Content = "If you’re interested in booking the above course or have any kind of enquiry, submit the form below or contact us through our live chat.")]
            EventPage_Enquiry_Description,
            [ContentTextDefaultValues(Content = "Login with Twitter")]
            General_LoginFields_TwitterLoginButtonText,
            [ContentTextDefaultValues(Content = "Session Calendar")]
            EventPage_Calendar_Title,
            [ContentTextDefaultValues(Content = "Below you can find the full list of sessions for this course.", ContentType = BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE.Html)]
            EventPage_Calendar_Text,
            [ContentTextDefaultValues(Content = "Currently, there are no restricted content uploaded by the administration.")]
            Members_FileListing_NoItems,
            [ContentTextDefaultValues(Content = "Access restricted content uploaded by the administration.")]
            Members_FileListing_Items,
            [ContentTextDefaultValues(Content = BusinessLogic_v3.Constants.Tokens.FILE_TITLE + " cannot be found. Please try again later. If problem persists, contact us.")]
            Members_FileListing_FileNotFound,
            [ContentTextDefaultValues(Content = "Date Uploaded")]
            Members_FileListing_Listing_DateUploaded,
            [ContentTextDefaultValues(Content = "Filename")]
            Members_FileListing_Listing_Filename,
            [ContentTextDefaultValues(Content = "Course")]
            Members_FileListing_Listing_Course,
            [ContentTextDefaultValues(Content = "Paid:")]
            MembersArea_Invoice_PaidTitle,
            [ContentTextDefaultValues(Content = "Payment Method:")]
            MembersArea_Invoice_PaymentMethodTitle,
            [ContentTextDefaultValues(Content = "Paid On:")]
            MembersArea_Invoice_PaidOnTitle,
            [ContentTextDefaultValues(Content = "Yes")]
            MembersArea_Invoice_PaidYes,
            [ContentTextDefaultValues(Content = "No")]
            MembersArea_Invoice_PaidNo,
            [ContentTextDefaultValues(Content = "Pay Online")]
            ShoppingCart_CheckOut_OnlinePaymentTitle,
            [ContentTextDefaultValues(Content = "Pay Later")]
            ShoppingCart_CheckOut_PayLaterTitle,
            [ContentTextDefaultValues(Content = "<p>You will verify your details within the next page after you click on Proceed To Checkout.</p>")]
            ShoppingCart_MyCart_ProceedToCheckoutText,
            [ContentTextDefaultValues(Content = "Please fill in your delivery details before proceeding to checkout")]
            ShoppingCart_CheckOut_DeliveryDetailsRequired,
            [ContentTextDefaultValues(Content = "Pay Online")]
            MembersArea_OrderHistory_PayOnlineButtonText,

            /**/
            [ContentTextDefaultValues(Content = @"<p>
            This invoice has not yet been paid. You can <a href='" + BusinessLogic_v3.Constants.Tokens.INVOICE_PAYMENT_URL + @"' target='_blank'>
                pay your invoice online</a>.</p>")]
            MembersArea_Invoice_OnlinePaymentFacility,
            [ContentTextDefaultValues(Content = "<strong>" + BusinessLogic_v3.Constants.Tokens.TAG_AUTHOR + "</strong> says:")]
            Article_Comment_Author,
            [ContentTextDefaultValues(Content = "Testimonials")]
            EventPage_Testimonials_Title,
            [ContentTextDefaultValues(Content = @"<p>
                You must be logged in to your account in order to submit a comment. Choose one of
                the below options:</p>")]
            Article_Comment_NeedLoginToComment,
            [ContentTextDefaultValues(Content = @"<p>
                You must be logged in to your account in order to leave an enquiry on ths classified. Choose one of
                the below options:</p>")]
            Classified_Enquiry_NeedLoginToEnquire,
            [ContentTextDefaultValues(Content = @"<p>
                You must be logged in to your account in order to submit a reply. Choose one of
                the below options:</p>")]
            Article_Comment_NeedLoginToReply,
            [ContentTextDefaultValues(Content = "<a href='{Article:Login}'>Login using your account</a>")]
            Article_Comment_LoginLink,
            [ContentTextDefaultValues(Content = "<a href='{Article:Members_Register}'>Register an account</a>")]
            Article_Comment_RegisterLink,
            [ContentTextDefaultValues(Content = "Yesterday, " + BusinessLogic_v3.Constants.Tokens.TAG_DATETIME)]
            DateTime_Yesterday,
            [ContentTextDefaultValues(Content = "Today, " + BusinessLogic_v3.Constants.Tokens.TAG_DATETIME)]
            DateTime_Today,
            Contact_Details_Mobile,
            [ContentTextDefaultValues(Content = "P.O.R")]
            Classifieds_MessageNoPrice,
            [ContentTextDefaultValues(Content = "&laquo;  go back")]
            General_Button_GoBack,
            [ContentTextDefaultValues(Content = "Email Address:")]
            Classifieds_Comment_EmailAddress,
            [ContentTextDefaultValues(Content = "Contact No.:")]
            Classifieds_Comment_ContactNo,
            [ContentTextDefaultValues(Content = "Contact Details")]
            Classifieds_ContactDetailsTitle,
            [ContentTextDefaultValues(Content = "Submit Enquiry")]
            Classifieds_Enquiry_Title,
            [ContentTextDefaultValues(Content = "Enquiry")]
            Classifieds_Enquiry_EnquiryFieldTitle,
            [ContentTextDefaultValues(Content = "You can also send a direct only enquiry online by filling in the form below and the email will be delivered directly to the owner.")]
            Classifieds_Enquiry_Description,
            [ContentTextDefaultValues(Content = "You enquiry has successfully been sent!")]
            Classified_Enquiry_SuccessMessage,
            [ContentTextDefaultValues(Content = "at the "+Tokens.LOCATION)]
            EventsListing_Location_Text
        }

        public enum NEWSLETTER_SUBSCRIBE_RESULT
        {
            Success,
            AlreadyExists
        }

        public enum SET_VOUCHERCODE_RESULT
        {
            ExpiredOrNotActiveYet,
            DoesNotExist,
            OK
        }

        public enum SHOPPING_CART_ITEM_UPDATE_QTY_RESULT
        {
            NotEnoughItemsAvailableInStock,
            OK,
            RemovedDueToSettingQuantityToZero,
            ErrorNoLinkedProductWithCartItem,
            ErrorProductVariationToUpdateNotSpecified,
            OutOfStock
        }

        public enum CONTENT_PAGE_IDENTIFIER
        {
            [ArticleDefaultValues(ArticleType = ARTICLE_TYPE.ContentPages, Identifier = "ContactUs", CustomLink = "/contact/", HtmlText = @"<p>If you want to contact us, fill in the form below!</p>")]
            ContactUs,

            [ArticleDefaultValues(ArticleType = ARTICLE_TYPE.ContentPages, LinkedRouteEnum = RouteUrlsEnum.Member_Register_Activate,
                HtmlText =
@"
<p>
			Your account could not be automatically verified. &nbsp;If you copied-and-pasted the link from an email, make sure you have copied the entire link. &nbsp;Or else, you can simply fill in the activation code below:</p>

"
)]
            Members_ActivateAccount,

            [ArticleDefaultValues(ArticleType = ARTICLE_TYPE.ContentPages,
                HtmlText =
@"

<p>
			Your account has been activated successfully, and you have been automatically logged in.</p>

"
)]
            Members_ActivateAccount_Success,
            [ArticleDefaultValues(DoNotRedirectOnLogin = true, HtmlText = @"<p>If you have forgot your password, simply provide us with your email address below.</p>
                                                <p>We will send you an email directly in your mailbox with instructions to reset your password.</p>", DoNotShowFooter = true, DoNotShowAddThis = true, DoNotShowLastEditedOn = true)]
            Members_ForgotPassword,
            Members_ReferralLink,
            [ArticleDefaultValues(PageTitle = "Login", CustomLink = "/members/login/", DoNotShowFooter = true, HtmlText = @"<p>Login to your account below by filling in your email address and password.  If you forgot your password, simply use our forgot password system.</p>")]
            Members_LoginPage,
            [ArticleDefaultValues(MetaTitle = "Invoice " + Constants.Tokens.INVOICE_NUMBER)]
            Members_Order_InvoicePage,
            [ArticleDefaultValues(NotVisibleInFrontend = true)]
            PayPippa_SuccessPage,
            [ArticleDefaultValues(NotVisibleInFrontend = true)]
            PayPippa_CancelPage,
            [ArticleDefaultValues(CustomLink = PurchaseSuccessRoute.ROUTE_DEFAULT_VIRTUAL_PATH, PageTitle = "Purchase Successful",
                                  HtmlText = @"<p>Your request has been processed successfully and payment has been effected.</p><p>You can view your invoice: <a href='" + Constants.Tokens.INVOICE_LINK + "' target='_blank'>#" + Constants.Tokens.INVOICE_NUMBER + "</a></p>",
                                  NotVisibleInFrontend = true)]
            PayPippa_FinalSuccessPage,
            UnlinkedArticles,
            AffiliateContentPages,
            [ArticleDefaultValues(Name = "Logout", PageTitle = "Logged Out Successfully", CustomLink = "/members/logout.aspx", HtmlText = "<p>You have been logged out successfully.</p>", DoNotShowFooter = true, DoNotShowNavigationBreadcrumbs = true, DoNotShowAddThis = true)]
            Members_Logout,
            [ArticleDefaultValues(Name = "Register", PageTitle = "Register", CustomLink = "/members/register/", HtmlText = "<p>Fill in the details below to register an account:</p>")]
            Members_Register,
            TermsConditions,

            /*
            
             **/
            [ArticleDefaultValues(Name = "Register Successful", PageTitle = "Register Successful", CustomLink = "/members/register/",
                HtmlText =
@"
   <p>
	You have successfully registered your account. You have been automatically logged in to your account.</p>
<p>
	An email with registration details has been submitted to your provided email address. Please make sure to check your <strong>spam / junk</strong> folder as well, if you do not receive the email.</p>
<p>
	Should you have any feedback or enquiries, feel free to <a href='{Article:ContactUs}'>contact us</a>.</p>          
            ")]

            Members_Register_RegistrationSuccessful,



            [ArticleDefaultValues(Name = "Register", PageTitle = "Register successful - Account requires activation",
                 HtmlText =
@"
  <p>
			You have successfully registered an account. &nbsp;Before you can use your account, you will need to <strong>activate it first</strong>. &nbsp;An email has been sent with details on how you can <strong>activate </strong>your account. &nbsp;If you cannot find the email, make sure that you check as well the <strong>spam </strong>or <strong>junk </strong>folder.</p>
		<p>
			If you have any problems in activating your account, do not hesitate to <a href='{Article:ContactUs}'>contact us</a>.</p>
   
            ")]

            Members_Register_RequiresActivation,
            [ArticleDefaultValues(Name = "Update Profile", PageTitle = "Update Profile", CustomLink = "/members/area/profile/", HtmlText = "<p>Update your profile details below:</p>")]
            MembersArea_Profile,

            [ArticleDefaultValues(Name = "Enquiry Form Submitted Successfully", PageTitle = "Enquiry Form Submitted Successfully",
                HtmlText = "<p>Your enquiry have been submitted successfully. We will get in touch with you as soon as possible!</p>")]
            ContactUs_EnquiryFormSentSuccessfully,


            [ArticleDefaultValues(DoNotRedirectOnLogin = true, PageTitle = "Sent email with instructions",
                HtmlText = @"<p>Your request has been submitted successfully.</p>
                                                <p>Follow the instructions within your email address in order to reset your password.  
If you have problems resetting your password, kindly contact us.</p>",
                                                                     DoNotShowFooter = true, DoNotShowAddThis = true, DoNotShowLastEditedOn = true)]


            Members_ForgotPassword_Success,
            [ArticleDefaultValues(DoNotRedirectOnLogin = true, PageTitle = "Error Resetting Password", HtmlText = @"<p>An error has been encountered while submitting your request.</p>
                                                <p>Please try again later.  If you have problems resetting your password, kindly contact us.</p>", DoNotShowFooter = true, DoNotShowAddThis = true, DoNotShowLastEditedOn = true)]

            Members_ForgotPassword_Error,

            [ArticleDefaultValues(DoNotRedirectOnLogin = true, PageTitle = "Password Reset Successfully", HtmlText = @"<p>Your password has been reset successfully.  </p>
                                                <p>You can now login using your new password.</p>", DoNotShowFooter = true, DoNotShowAddThis = true, DoNotShowLastEditedOn = true)]

            Members_ResetPassword_Success,
            Members_ResetPassword_Error,
            [ArticleDefaultValues(Name = "Reset Password", CustomLink = "/members/reset-password/", HtmlText = "<p>Please enter your new password below to be able to reset your password and access your account:</p>")]
            Members_ResetPassword,
            [ArticleDefaultValuesAttribute(
                PageTitle = "Page Not Found", DoNotRedirectOnLogin = true,
                HtmlText = @"<p>Sorry, the page you requested was not found or has been moved to another location.</p>

                            <p>Please make sure that you have written the correct URL and that there are no proper spelling and capitalisation mistakes. If you're having trouble locating a web page, <a href='/'>visit our homepage</a>.</p>

                            <p>If problem persists, <a href='" + TAG_ERROR_PAGE_CONTACT_US_LINK + "'>contact us</a>.</p>", CustomLink = "/error/PageNotFound/",
                                                                      DoNotShowAddThis = true, DoNotShowFooter = true, DoNotShowLastEditedOn = true, DoNotShowNavigationBreadcrumbs = true)]
            Error_PageNotFound,
            [ArticleDefaultValuesAttribute(
                PageTitle = "Error Encountered",
                HtmlText = @"<p>An error has been encountered while servicing your request.  Please try again later.</p>

                            <p>If problem persists, kindly <a href='" + TAG_ERROR_PAGE_CONTACT_US_LINK + "'>contact us</a>.</p>", CustomLink = "/error/Generic/",
                                                                      DoNotShowAddThis = true, DoNotShowFooter = true, DoNotShowLastEditedOn = true, DoNotShowNavigationBreadcrumbs = true)]
            Error_Generic,
            MainMenu,
            MainFooterMenu,
            Home,
            Blog,
            [ArticleDefaultValuesAttribute(Name = "News", CustomLink = "/news")]
            NewsAndUpdates,
            [ArticleDefaultValuesAttribute(Name = "Order History", HtmlText = "<p>Below you can find your list of orders placed on the site:</p>", CustomLink = "/members/area/order-history")]
            Members_OrderHistory,
            [ArticleDefaultValuesAttribute(Name = "Pay Online", HtmlText = "", LinkedRouteEnum = RouteUrlsEnum.Member_Order_PayOnline)]
            Members_Order_PayOnline,


            [ArticleDefaultValuesAttribute(Name = "Credit Statement", HtmlText = "<p>Below is an outline of how you have used your credits within our site:</p>", CustomLink = "/members/area/credit-statement")]
            Members_CreditStatement,
            [ArticleDefaultValuesAttribute(Name = "My Account", HtmlText = "<p>Welcome to your account!</p>")]
            Members_MyAccount,
            [ArticleDefaultValuesAttribute(
            PageTitle = "Delivery Details",
            HtmlText = @"<p>Please enter the delivery details you would like the items to be delivered to.  If you want to review your order, <a href='/cart'>go back to cart</a></p>", CustomLink = "/cart/delivery-details")]
            ShoppingCart_DeliveryDetails,
            [ArticleDefaultValuesAttribute(
            PageTitle = "My Cart", CustomLink = "/cart")]
            ShoppingCart,
            [ArticleDefaultValuesAttribute(
                PageTitle = "Checkout",
                HtmlText = @"<p>Please confirm your order below.  If you want to change some items, <a href='/cart'>go back to cart</a></p>", CustomLink = "/check-out")]
            CheckOut,
            Search,
            Product,
            [ArticleDefaultValuesAttribute(PageTitle = "Events", CustomLink = "/events")]
            Events,
            [ArticleDefaultValuesAttribute(PageTitle = "Testimonials", CustomLink = "/testimonials")]
            Testimonials,
            [ArticleDefaultValuesAttribute(PageTitle = "Event")]
            Event,
            [ArticleDefaultValuesAttribute(PageTitle = "Restricted Area", SubTitle = "Welcome " + BusinessLogic_v3.Constants.Tokens.USER_FIRST_NAME)]
            Members_FilesListing,
            [ArticleDefaultValuesAttribute(PageTitle = "Thanks for your order", CustomLink = BusinessLogic_v3.Constants.Routing.ORDER_AND_PAY_LATER,
                DoNotShowFooter = true, DoNotShowLastEditedOn = true, DoNotShowAddThis = true,
                HtmlText =
                        @"<p>
	Your order has been placed successfully and can be found within your order history. &nbsp;We will get back to you with payment details. &nbsp;You can also choose to <a href='" + BusinessLogic_v3.Constants.Tokens.INVOICE_PAYMENT_URL + @"' target='_blank'>pay your invoice online</a>.</p>
<p>
	You can view your invoice by clicking on the link below:</p>
<p style='text-align:center;'>
	<a class='order-and-pay-later-invoice-link' href='" + BusinessLogic_v3.Constants.Tokens.INVOICE_LINK + @"' target='_blank'>" + BusinessLogic_v3.Constants.Tokens.INVOICE_NUMBER + @"</a></p>
")]
            Members_Order_OrderAndPayLater,
            [ArticleDefaultValuesAttribute(PageTitle = "Classified", CustomLink = "/classified.aspx", DoNotShowNavigationBreadcrumbs=true)]
            Classified,
            [ArticleDefaultValuesAttribute(PageTitle = "Classifieds", CustomLink = "/classifieds.aspx")]
            Classifieds
        }

        public enum AUDITLOG_MSG_TYPE
        {
            Add = 100,
            Delete = 200,
            Login = 300,
            Update = 400,
            Information = 500

        }

        //public enum CMS_AUDIT_MESSAGE_TYPE
        //{
        //    Add = 100,
        //    Delete = 200,
        //    Login = 300,
        //    Update = 400,
        //    Information = 500

        //}
        public enum ITEM_FILTER_TYPE
        {
            FeatureList,
            Size,
            Colour
        }
        public enum ERROR_PAGE_TYPE  //IMPORTANT: If you add here, map sure to map it as well in error.aspx.cs in generic components
        {
            PageNotFound,
            Generic
        }
        public enum ENUM_SORT_BY
        {
            PriorityAttributeValue = 0,
            NameAscending = 100,
            NameDescending = 200,
            Value = 300
        }

        public enum REPORTING_SALES_SORTBY
        {
            [Description("Title (Asc)")]
            TitleAsc,
            [Description("Title (Desc)")]
            TitleDesc,
            [Description("Popularity Rating (Asc)")]
            PopularityAsc,
            [Description("Popularity Rating (Desc)")]
            PopularityDesc,
            [Description("Price (Asc)")]
            PriceAsc,
            [Description("Price (Desc)")]
            PriceDesc,
            [Description("Date Sold (Asc)")]
            DateSoldAsc,
            [Description("Date Sold (Desc)")]
            DateSoldDesc
        }
        public enum CATEGORY_SPECIFICATION_DATA_TYPE
        {
            Boolean = 10, String = 20, Integer = 30, DecimalNumber = 40
        }

        public enum ORDER_STATUS
        {
            [Description("New Order")]
            NewOrder = 100,
            [Description("Processing")]
            Processing = 200,
            [Description("Completed")]
            Completed = 300,
            [Description("Cancelled")]
            Cancelled = 400
        }

        public enum CmsUserRoleEnum
        {
            Affiliate = 100,

            Blogs = 200

        }

        public enum ContentTagType
        {
            ContentReplace = 100,
            ContentRemove = 200
        }

        public enum ShippingPricingType
        {
            FixedAmount = 0,
            PerKilogram = 100
        }



        public enum RouteUrlsEnum
        {
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/Article/article.aspx", VirtualPath = "/content/{" + Tokens.ROUTE_PARAM_TITLE + "}/{" + Tokens.ROUTE_PARAM_ID + "}/")]
            Article,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/Article/dialog-article.aspx", VirtualPath = "/dialog/{" + Tokens.ROUTE_PARAM_TITLE + "}/{" + Tokens.ROUTE_PARAM_ID + "}/")]
            ArticleDialog,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/Errors/error.aspx", VirtualPath = "/error/{" + Tokens.ROUTE_PARAM_ID + "}")]
            Error,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/Events/events.aspx", VirtualPath = "/events/")]
            Events,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/EventBasics/eventbasic.aspx", VirtualPath = "/event/{" + Tokens.ROUTE_PARAM_TITLE + "}/{" + Tokens.ROUTE_PARAM_ID + "}/")]
            EventBasic,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/ShoppingCart/cart.aspx", VirtualPath = "/cart")]
            ShoppingCart,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/Member/profile.aspx", VirtualPath = "/members/area/profile/")]
            Member_Profile,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/Member/activate.aspx", VirtualPath = "/members/register/activate/")]
            Member_Register_Activate,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/Member/pay-online.aspx", VirtualPath = "/members/order/payonline/")]
            Member_Order_PayOnline,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/Member/pay-later.aspx", VirtualPath = BusinessLogic_v3.Constants.Routing.ORDER_AND_PAY_LATER)]
            Member_Order_OrderAndPayLater,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/Events/event.aspx", VirtualPath = "/course/{" + Tokens.ROUTE_PARAM_TITLE + "}/{" + Tokens.ROUTE_PARAM_ID + "}/")]
            Event,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/Testimonials/testimonials.aspx", VirtualPath = "/testimonials/")]
            Testimonials,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/News/news.aspx", VirtualPath = "/news/")]
            News,
            [RouteDefaultInfo(PhysicalPath = "/_ComponentsGeneric/Frontend/Pages/Common/Member/files.aspx", VirtualPath = "/members/area/")]
            RestrictedFiles
        }

        public enum BusinessLogicSettingsEnum
        {
            [SettingsInfo(Priority = 001000, DefaultValue = "{Num}", Title = "Shop Order Reference - Generation Format", Description = "This is the format of which the order reference will be generated for an order")]
            Ecommerce_ShopOrderReferenceGenerationFormat,
            [SettingsInfo(DefaultValue = 1, Title = "Current Order Reference")]
            Ecommerce_CurrentOrderReference,

            [SettingsInfo(DefaultValue = "Y", Title = "Whether the email logger to database is enabled")]
            Email_DbLogger_Enabled,

            [SettingsInfo(DefaultValue = 5, Title = "Referral System Commission Value in Euro")]
            Member_ReferralSystemCommissionValueInEuro,

            [SettingsInfo(DefaultValue = 7, Description = "Total number of days to wait, before a larger limit will apply. If new limit is smaller than current, it applies immediately")]
            Member_ResponsibleGaming_DaysToWaitBeforeLargerLimitApplies,

            [SettingsInfo(DefaultValue = 1440, Title = "Member Expiry Checker Job Frequency In Minutes")]
            MemberExpiryCheckerJobFrequencyInMinutes,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_MultiLingual,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_IsSiteWithRegistration,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Article_CommentingEnabled,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Article_ShowImageGalleryForContentPages,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Member/register.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_Register,

            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_IsGenericMessageEnabled,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Contact_IsOnlineEnquiryFormEnabled,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Contact/contact.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_ContactUs,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Member/login.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_Login,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Member/logout.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_Logout,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Member/forgot-password.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_ForgotPassword,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Member/reset-password.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_ResetPassword,
            //[SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, Description = "This is used to prepend any routing URLs with the language code, e.g. /en/content/TITLE/123131/")]
            //ModuleSettings_Generic_MultiLingualPrependRoutingURLsWithLanguageCode,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_MultiLingualChangeDomain,
            [SettingsInfo(DefaultValue = 1)]
            CreditCostInclVat,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_IsSiteWithOrders,


            [SettingsInfo(DefaultValue = 20480, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Username_Encryption_TotalIterations,

            [SettingsInfo(DefaultValue = CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE.PlainText, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Username_Encryption_Type,
            [SettingsInfo(DefaultValue = "0B5D72C16D8C7F90", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Username_Encryption_Salt,


            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_UsesCreditSystem,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Subscriptions_Enabled,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Activation_RequiresActivation,
            [SettingsInfo(DefaultValue = "Id", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Activation_ParamterNames_ID,
            [SettingsInfo(DefaultValue = "Code", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Activation_ParamterNames_Code,

            [SettingsInfo(DefaultValue = 20480, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Password_TotalIterations,

            [SettingsInfo(DefaultValue = CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE.PBKDF2, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Password_EncryptionType,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]

            ModuleSettings_Member_Emails_RequiresUniqueEmail,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Emails_SendEmailToAdminOnRegistration,

            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_KnowYourCustomer_Enabled,


            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Login_ChangesSessionGuidWithEveryLogin,

            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, Description = "Whether to allow concurrent logins or not")]
            ModuleSettings_Member_Login_DoNotAllowConcurrentLogins,


            [SettingsInfo(DefaultValue = 5, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, Description = "Total number of invalid login attempts to block user from trying to login")]
            ModuleSettings_Member_Login_InvalidLoginAttemptsToBlock,

            [SettingsInfo(DefaultValue = 15, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, Description = "The total number of minutes to block user after too many invalid login attempts. 0 or -1 means indefinite")]
            ModuleSettings_Member_Login_TotalMinutesToBlockUserAfterTooManyInvalidLoginAttempt,


            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_MemberReferrals_Enabled,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_MemberReferrals_ReferralsLinkedToMembersCountForOrders,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_MemberReferrals_Cookies_UseCookies,
            [SettingsInfo(DefaultValue = 365, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_MemberReferrals_Cookies_DaysToStoreReferralCookie,
            [SettingsInfo(DefaultValue = "__mr_ck_n", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_MemberReferrals_Cookies_ParameterNames_Id,
            [SettingsInfo(DefaultValue = "__mr_ck_n_u", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_MemberReferrals_Cookies_ParameterNames_Url,
            [SettingsInfo(DefaultValue = "mr", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_MemberReferrals_Querystring_ParamName,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_MemberReferrals_General_ReferralSuccessfulOnRegister,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_MemberReferrals_General_ReferralSystemPaysCommission,

            [SettingsInfo(DefaultValue = "email.test@casasoft.com.mt", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Shop_OrderPaid_NotificationEmailSendTo,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner, Description = "This allows you to specify whether you want to allow orders to be paid later, rather than immediately online at checkout stage")]
            ModuleSettings_Shop_Orders_AllowPayLater,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Shop_Enabled,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Shop_Ecommerce_Enabled,
            [SettingsInfo(DefaultValue = true, Description = "Whether to send out emails to user on order confirmation (before actual payment)", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Shop_Ecommerce_OrderConfirmation_SendEmailToUser,
            [SettingsInfo(DefaultValue = false, Description = "This means whether the shop uses 'quantities'.  Not all shops have quantities, thus this would be ignored in search listings.", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Shop_Quantities_Enabled,
            [SettingsInfo(DefaultValue = true, Description = "", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Shop_BulkImport_Enabled,
            [SettingsInfo(DefaultValue = false, Description = "", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Shop_BulkImport_AssignRandomImageIfNoneFound,

            [SettingsInfo(DefaultValue = 0, Description = "",
               CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Taxes_RevenueTaxRate,




            [SettingsInfo(DefaultValue = "/members/login", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RouteVirtualPath_Login,
            [SettingsInfo(DefaultValue = "/members/logout", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RouteVirtualPath_Logout,
            [SettingsInfo(DefaultValue = "/members/forgot-password/", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RouteVirtualPath_ForgotPassword,

            [SettingsInfo(DefaultValue = "/members/register", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RouteVirtualPath_Register,
            [SettingsInfo(DefaultValue = "/members/reset-password/", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RouteVirtualPath_ResetPassword,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Article_DoNotShowFooter,
            [SettingsInfo(DefaultValue = 10000, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner, Description = "The amount of time before hiding the generic status message shown on top")]
            ModuleSettings_Generic_GenericMessageHideAfterMS,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner, Description = "Whether to show a hide message button when a generic message is shown")]
            ModuleSettings_Generic_GenericMessageShowHideButton,

            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Affiliates_Enabled,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Affiliates_AffiliatesLinkedToMembersCountForOrders,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Affiliates_Cookies_UseCookiesToStoreAffiliate,
            [SettingsInfo(DefaultValue = "__ar_ck_n", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Affiliates_Cookies_CookieName,
            [SettingsInfo(DefaultValue = 365, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Affiliates_Cookies_DaysToStoreAffiliateCookie,
            [SettingsInfo(DefaultValue = "ar", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Affiliates_Querystring_ParamName,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Affiliates_Whitelabelling_Enabled,
            [SettingsInfo(DefaultValue = 10, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            Member_ResponsibleGaming_AmountOfExpiredLimitsToShowInProfile,
            [SettingsInfo(DefaultValue = true, Description = "Enable / disable audit log", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            AuditLog_Enabled,
            [SettingsInfo(DefaultValue = true, Description = "Whether to log the stack trace as well with each entry", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            AuditLog_LogDetail_StackTrace,
            [SettingsInfo(DefaultValue = 50000, Description = "The amount of log entries to keep", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            AuditLog_TotalLogEntriesToKeep,

            [SettingsInfo(DefaultValue = 15, Description = "The interval in seconds, of the time between insertions", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            AuditLog_Insertor_InserterEveryXSeconds,

            [SettingsInfo(DefaultValue = 250, Description = "Batch size for each insertion", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            AuditLog_Insertor_BatchSize,

            [SettingsInfo(DefaultValue = false, Description = "Log only entries done from the Cms", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            AuditLog_LogOnlyCmsEntries,

            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_IsContactNoInEnquiryFormVisible,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_IsCompanyInEnquiryFormVisible,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_DoNotShowLabelWithContactDetails,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Generic_IsSiteWithCategories,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowMobileInFooter,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowTelephoneInFooter,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowFaxInFooter,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowEmailInFooter,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowWebsiteInFooter,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowAddressInFooter,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowMapInFooter,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_IsContactDetailsVisibleInFooter,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Categories/search.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_Search,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Categories/search.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_SearchPage,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowAddressInTable,
            [SettingsInfo(DefaultValue = 10240, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            AttachmentUploadMaxFileSizeInKB,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Article_DoNotShowNavigationBreadcrumbs,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowFacebookInFooter,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowTwitterInFooter,
            [SettingsInfo(DefaultValue = 150, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Article_DescriptionLimitValue,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_IsHowDidYouFindUsInEnquiryFormVisible,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Article_ShowRootNodeInNavigationBreadcrumbs,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowLinkedinInFooter,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Contact_Details_ShowSkypeInFooter,
            [SettingsInfo(DefaultValue = Enums.USER_REGISTRATION_REQUIRED_KEY.UsernameAndEmail, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            UserRegistrationRequiredKey,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Member_Activation_LoginWithUsername,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            AutoApproveArticleComments,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            Comments_SendNotificationEmailToCommentAuthor,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            Comments_SendNotificationEmailToAdministrator,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            Comments_SendNotificationEmailToArticleAuthor,


            [SettingsInfo(DefaultValue = "Name*,Surname*,Street*,City*,ZIP_PostCode,Country*,BirthDate*,Email*,ConfirmEmail*,Mobile,Telephone,Language*,Company", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            ModuleSettings_Member_ProfileFieldsPersonalInfoFields,
            [SettingsInfo(DefaultValue = "Username*,Password*,ConfirmPassword*,AcceptTerms*", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            ModuleSettings_Member_ProfileFieldsAccountInfoFields,
            [SettingsInfo(DefaultValue = "0", Description = "The minimum age (years) a user can be to register / update his profile.  You can set multiple age for different countries.  Age must be entered in 3 letter code country followed by a colon and the age.  Multiple age restrictions can be set by splitting them with a comma.  E.g. 18,21:MLT|EST|GER (which means, generally 18, but Malta, Estonia & Germany 21)", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Member_ProfileFieldsMinimumMemberAgeRequired,
            [SettingsInfo(DefaultValue = "", Description = "The list of excluded country codes, in 3 or 2 letter code format", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Member_ExcludedCountries,
            [SettingsInfo(DefaultValue = false, Description = "Whether or not to allow auto complete with login fields", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Member_LoginDoNotAllowAutoComplete,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Purchase/success.aspx", Description = "The physical path for the default success page", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            RoutePhysicalPath_PurchaseSuccess,
            [SettingsInfo(DefaultValue = true, Description = "Whether to show the top main image inside the full product page", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            ModuleSettings_Products_FullPageShowMainTopImage,
            [SettingsInfo(DefaultValue = 5, Description = "The amount of featured products to show", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            ModuleSettings_Products_ShowAmtFeaturedProducts,
            [SettingsInfo(DefaultValue = 5, Description = "The amount of related products to show", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            ModuleSettings_Products_ShowAmtRelatedProducts,
            [SettingsInfo(DefaultValue = true, Description = "Whether to show view terms & conditions with product full page", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            ModuleSettings_Products_ShowViewTerms,
            MetaTag_GoogleSiteVerification,
            [SettingsInfo(DefaultValue = 250, Description = "The number of records to be returned per chunk of expired subscription links queried.", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            SubscriptionCheckerQueryChunkSize,
            [SettingsInfo(DefaultValue = 250, Description = "The number of records to be returned per chunk of members with elapsed next topup date queried.", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            RenewableAccountBalanceCheckerQueryChunkSize,
            [SettingsInfo(DefaultValue = "/members/area/order-history", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RouteVirtualPath_OrderHistory,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Member/order-history.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_OrderHistory,
            [SettingsInfo(DefaultValue = "/members/area/credit-statement", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RouteVirtualPath_CreditStatement,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Member/credit-statement.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_CreditStatement,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Registration_AutoLoginAfterRegister,
            [SettingsInfo(DefaultValue = PASSWORD_STRENGTH_CHARACTERS.Alpha | PASSWORD_STRENGTH_CHARACTERS.Numerics, Description = "Bitwise operator of the password strength - None (1), Alpha (2), BothLetterCase = 4, Numerics = 8, SpecialCharacters = 16", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Password_StrengthLevel,
            [SettingsInfo(DefaultValue = false, Description = "Whether you would like to show a strength meter by default underneath the password", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Password_ShowStrengthMeter,
            [SettingsInfo(DefaultValue = 0, Description = "If you specify min length larger than 0, password must have minimum length", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_Password_MinLength,
            [SettingsInfo(DefaultValue = 360, Description = "The amount of minutes to wait between each check for members to renew their renewable credits.", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RenewableAccountBalanceCheckerJobFrequencyInMinutes,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_RenewableAccountBalances_Enabled,

            [SettingsInfo(DefaultValue = 180, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Member_ResponsibleGaming_SelfBarring_ExpiryNotifier_CheckEveryXSeconds,

            [SettingsInfo(DefaultValue = 250, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Member_ResponsibleGaming_SelfBarring_ExpiryNotifier_PageSize,

            [SettingsInfo(DefaultValue = 30, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            Member_ResponsibleGaming_SelfBarring_DaysBeforeToSendExpiryNotification1,
            [SettingsInfo(DefaultValue = 7, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            Member_ResponsibleGaming_SelfBarring_DaysBeforeToSendExpiryNotification2,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_ShowCombosInGeneralListing,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_ShowBreadcrumbsInGeneralListing,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Products/product.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_Product,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_IncludeHomeInBreadcrumbs,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_ShowRelatedProductsInFullpage,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_ShowTopAddThisInFullPage,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_IncludeMainImageInSlider,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_FullPageShowGallery,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_ShowDescriptionAddThis,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/ShoppingCart/checkout.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_CheckOut,
            [SettingsInfo(DefaultValue = "/check-out", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RouteVirtualPath_CheckOut,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Products_ShowProductImageSlider,
            [SettingsInfo(DefaultValue = false, DefaultLocalhostValue = true, Description = "If set to true, then forms are filled in with random item to facilitate testing", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_FillFormsWithRandomValues,
            RouteVirtualPath_DeliveryDetails,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/ShoppingCart/delivery-details.aspx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RoutePhysicalPath_DeliveryDetails,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_FullPageAddCategoryToMetaTitleForSEO,
            [SettingsInfo(DefaultValue = "/image/product-default-image.jpg", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_DefaultImageUrl,
            [SettingsInfo(DefaultValue = "/image/product-default-shopping-cart-image.jpg", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_DefaultShoppingCartImageUrl,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Payment_SucesPage_ShowPaymentDetails,
            [SettingsInfo(DefaultValue = 700, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Dialog_DefaultWidth,
            [SettingsInfo(DefaultValue = 500, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Dialog_DefaultHeight,
            ModuleSettings_Login_SocialLoginEnabled,
            ModuleSettings_Login_RedirectTopFrame,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/Pages/Common/Purchase/successHandler.ashx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Payment_Succes_HandlerUrl,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_FullPageAddParentCategoryToMetaTitleForSEO,
            [SettingsInfo(DefaultValue = "malta", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            SEO_KeywordsSuffix,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ThrowContentPageNotSetExceptionWhenLocalhost,
            [SettingsInfo(DefaultValue = 200, Description = "If no short descriptin is specified, this is the max. amount of characters to take from full description", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Products_MaxShortDescriptionCharacters,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Member_EmailTakesPrecedenceOverUsernameAndOverwrites,
            [SettingsInfo(DefaultValue = 155, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            SEO_MaxDescriptionLength,
            [SettingsInfo(Description = "Whether you would like to output a span before each category listing item with the identifier as CSS class", DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Categories_MenuListingOutputIconIdentifierAsSpan,
            [SettingsInfo(Description = "Whether you would like to output the image of the category before each listing item", DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Categories_MenuListingOutputIconImage,
            [SettingsInfo(Description = "Whether you would like to have a tooltip shown when a user rolls over the product category listing", DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Categories_ShowQTipWithProductCategoryListing,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            MembersArea_CreditStatement_ShowMonthlyCredits,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            MembersArea_CreditStatement_ShowCreditDifference,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            MembersArea_CreditStatement_ShowAdditionalCredits,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_LoadRandomIfNothingIsFeatured,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_RememberMeEnabled,
            [SettingsInfo(DefaultValue = +1, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Other_ServerGMTOffset,
            [SettingsInfo(DefaultValue = 500, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            General_Bannner_AnimationSpeedInMS,
            [SettingsInfo(DefaultValue = 3000, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            General_Bannner_PauseDurationBetweenSlidesInMS,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_FullPageShowContactForm,
            [SettingsInfo(DefaultValue = false, Description = "Show a toggle button to show/hide contact form", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Products_FullPageToggleContactForm,

            [SettingsInfo(DefaultValue = 60, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, Description = "This setting specifies the total time taken between checks for price updates, like for special offers")]
            Products_Pricing_PriceUpdateIntervalInSecs,

            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Products_RelatedProducts_GetRelatedBasedOnColour,

            [SettingsInfo(DefaultValue = 0, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Products_BrandsCarouselSlider_ItemWidth,
            [SettingsInfo(DefaultValue = 0, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Products_BrandsCarouselSlider_ItemHeight,
            [SettingsInfo(DefaultValue = 0, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Products_BrandsCarouselSlider_Items,
            [SettingsInfo(DefaultValue = 15000, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Products_BrandsCarouselSlider_Duration,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Member_ProfileFieldsShowDiscountCode,
            [SettingsInfo(DefaultValue = 75, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Products_BrandsCarouselSlider_Height,
            [SettingsInfo(DefaultValue = "10,25,50,100,250", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            General_ShowAmount_ShowAmountValues,

            [SettingsInfo(DefaultValue = true, DefaultLocalhostValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Other_CacheControls,
            [SettingsInfo(DefaultValue = false, DefaultLocalhostValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_IsSiteUsingMemberDiscounts,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Products_FullPage_ShowDescriptionTitle,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Products_FullPage_ShowAvailableColours,
            [SettingsInfo(DefaultValue = "10,25,50,100,250", Description = "The list of show amount values.", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Listing_ShowAmountComboValues,
            [SettingsInfo(DefaultValue = "25", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Listing_ShowAmountComboValuesDefaultValue,
            [SettingsInfo(DefaultValue = 60, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            RandomProductImagesFromCategoryCacheTimeoutValue,
            [SettingsInfo(Description = "Quantity must be greater than 0", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Products_AddToCart_QuantityMustBeGreaterThanZero,
            [SettingsInfo(DefaultValue = 250, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Paging_MaximumShowAmountValueAllowedForSearches,
            [SettingsInfo(DefaultValue = "/images/event-default-image.jpg", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Event_DefaultImageUrl,
            [SettingsInfo(Description = "Whether brands carousel is enabled", DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Products_BrandsCarouselSlider_Enabled,
            [SettingsInfo(Description = "Whether caption of banner is enabled", DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            General_Bannner_DoNotShowCaption,
            [SettingsInfo(Description = "Animation Effect of Banner Slider", DefaultValue = "random", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            General_Bannner_AnimationEffect,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_ShowDateCreatedInGenericListing,
            [SettingsInfo(DefaultValue = "dd MMMM yyy", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_DateFormatDateCreatedInGenericListing,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_ShowPagingBarInTopInGeneralListing,
            [SettingsInfo(DefaultValue = "EUR", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Ecommerce_DefaultCurrencyCode,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_IsSiteWithEvents,
            [SettingsInfo(DefaultValue = "/images/video-thumbnail-default.jpg", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_DefaultThumbnailVideoMediaItem,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Article_ShowMainImageInContentPage,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Calendar_EventsCalendar_ShowOnlyAvailableSessions,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_IsSiteUsingCufon,
            [SettingsInfo(DefaultValue = "10", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Testimonials_Scroller_ItemsToScroll,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Contact_Details_IsSubjectInEnquiryFormVisible,
            [SettingsInfo(DefaultValue = "Enquiry | General", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Contact_Subject,
            [SettingsInfo(DefaultValue = "Booking | Enquiry", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Event_Enquiry_Subject,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Contact_Details_ShowSocialNetworksAsIcons,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            Classifieds_ModerationEnabled,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            Classifieds_SendReceiptNotificationToOwnerUponSubmit,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            Classifieds_SendReceiptNotificationToAdminUponSubmit,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            Products_Checkout_ShowCheckbox,

            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/ShoppingCart.ascx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_ShoppingCart_CartUserControlPath,
            [SettingsInfo(DefaultValue = "/_ComponentsGeneric/Frontend/UserControls/ShoppingCart/ShoppingCartItem.ascx", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_ShoppingCart_ShoppingCartItemUserControlPath,
            [SettingsInfo(DefaultValue = 100, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Event_DescriptionLimitValue,
            [SettingsInfo(Title = "Listing Default Image", DefaultValue = "/images/default-listing-image.jpg", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            Listing_DefaultImageUrl,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Article_ShowPreviousNextArticle,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Article_ShowAddThisInHeading,
            [SettingsInfo(DefaultValue = false, Description = "Whether users must be logged in in order to place a comment", CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Article_CommentingRequireLogin,
            [SettingsInfo(DefaultValue = true, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Article_CommentingReplyEnabled,
            [SettingsInfo(DefaultValue = 200, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner)]
            ModuleSettings_Article_SummaryLimit,
            [SettingsInfo(DefaultValue = 900, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ArticleURLCacher_ItemExpiryInMinutes,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_IsSiteWithProducts,
            [SettingsInfo(DefaultValue = false, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft)]
            ModuleSettings_Generic_IsSiteWithClassifieds,
            [SettingsInfo(DefaultValue = 500000, CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoftDevTeam)]
            ArticleSearcher_MaxAmountOfIDsToLoadForLuceneSearch,
            [SettingsInfo(CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "/mia_flights/mia_web_arrivals.txt")]
            MaltaAirportFlights_ArrivalsTextFilePath,
            [SettingsInfo(CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "/mia_flights/mia_web_departures.txt")]
            MaltaAirportFlights_DeparturesTextFilePath,
            [SettingsInfo(CmsAccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, DefaultValue = 20)]
            MaltaAirportWeather_CacheMinutesTillExpiry
        }
        [Flags]
        public enum PASSWORD_STRENGTH_CHARACTERS
        {
            None = 1,
            Alpha = 2,
            BothLetterCase = 4,
            Numerics = 8,
            SpecialCharacters = 16
        }
        public static T GetSettingValueForBusinessLogicSetting<T>(this BusinessLogicSettingsEnum value)
        {
            return CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<T>(value);

        }

        public enum FLIGHT_TYPE
        {
            Arrival,
            Departure
        }

        public enum WEATHER_CONDITION
        {
            //DO NOT RENAME OR CAPITALISE THESE AS THEY ARE USED FOR PARSING!
            clear,
            partlycloudy,
            cloudy,
            windy,
            mist,
            rainy,
            thunderstorm,
            showers,
            sunny,
            fair
        }

        public enum WEATHER_SOURCE
        {
            MaltaInternationalAirport
        }

        public enum EmailsBusinessLogic
        {
            [EmailTextDefaultValues(Subject = "Documents required for proof of identity", Title = "Process Started",
                BodyHtml = "<p>KYC Process started</p>", WhenIsThisEmailSent = "When the website administrator initiates a KYC procedure on a member")]
            User_Members_KnowYourCustomer_ProcessStarted,


            User_Members_MemberActivatedSuccessfully,

            [EmailTextDefaultValues(Subject = "Forgotten Password", Title = "Forgotten Password", BodyHtml = "<p> Dear [Member:FirstName],</p> <p> &nbsp; &nbsp; &nbsp;You, or somebody on your behalf has requested your password to be resetted on [WebsiteName].</p> <p> &nbsp; &nbsp; &nbsp;In order to reset your password, kindly follow the below link:</p> <p> <a href=\"[Site:ForgottenPasswordLink]\">[Site:ForgottenPasswordLink]</a></p> <p> <em>(Note: If you cannot click on the above link, kindly copy-and-paste it into your favorite browser like Internet Explorer, Mozilla Firefox or Google Chrome)</em></p> <p> If you did not request to reset your password, kindly ignore this email.</p> <p> [Signature]</p> ", WhenIsThisEmailSent = "This is an email sent to the user requesting a reset password for his/her account.")]
            User_Members_ForgottenPassword,

            [EmailTextDefaultValues(Subject = "Activate Account", Title = "Registration Successful - Requires Activation",
                BodyHtml =
                @"
<p>
			Dear [Member:FirstName],</p>
		<p>
			Thank you for registering an account on <a href='\&quot;[Site:BaseWebsiteUrl]\&quot;'>[Site:WebsiteName]</a>. In order to use your account, you need to activate it first by verifying your email address. &nbsp;Simply click on the below link to activate your account.</p>
		<p>
			<a href='[ActivationLink]'>[ActivationLink]</a></p>
		<p>
			If you have any problems in activating your account, feel free to <a href='{Article:ContactUs}'>contact us</a>.</p>
		<p>
			[Signature]</p>

"
                , WhenIsThisEmailSent = "This is an email sent when a user account is registered and requires activation")]
            User_Members_OnRegistrationActivationRequired,



            [EmailTextDefaultValues(Subject = "Registration Successful", Title = "Registration Successful",
                BodyHtml = "<p> Dear [Member:FirstName],</p> <p> &nbsp;&nbsp;&nbsp;&nbsp; You have successfully registered an account on <a href=\"[Site:BaseWebsiteUrl]\">[Site:WebsiteName]</a>.</p> <p>[Signature]</p>",
                WhenIsThisEmailSent = "This is an email sent when a user account is registered and does not requires activation")]
            User_Members_OnRegistrationActivationNotRequired,

            User_Members_TerminatedAccount,


            [EmailTextDefaultValues(Subject = "Your account balance has been updated!", Title = "On Account Balance Update",
                BodyHtml =
                @"<p> Dear [Member:FullName],</p> 
<p> Your account balance has been updated!",
                WhenIsThisEmailSent = "This is an email sent when a website administrator manually updates the balance of a user")]
            User_Members_AccountBalance_OnUpdate,

            User_Members_MemberSendReferral,

            User_Members_MemberReferralWasSuccessful,

            User_Members_ExpiredNotification,

            User_Members_ExpiryNotification1,

            User_Members_ExpiryNotification2,

            [EmailTextDefaultValues(Subject = "Contact Form Submitted Successfully", Title = "Sent Contact Form",
                BodyHtml = @"<p> Dear [NAME],</p> <p> Your contact form has been successfully submitted. You shall be hearing from us shortly.</p> 
<p> [Signature]</p> ", WhenIsThisEmailSent = "This email is sent to user who just filled in a contact form, notifying him/her that his/her submission was successful.")]
            User_ContactForms_SentContactForm,

            [EmailTextDefaultValues(Subject = "Payment Successful", Title = "Payment Successful",
                BodyHtml = @"
<p>
	Dear [Member:FirstName],</p>
<p>
	Thank you for you payment.&nbsp; Below is a summary of the order details:</p>
<table border='0' cellpadding='2' cellspacing='2' style='width: 500px;'>
	<tbody>
		<tr>
			<td>
				Reference:</td>
			<td>
				<a href='[FrontendInvoiceLink]'>[Order:Reference]</a></td>
		</tr>
		<tr>
			<td>
				Total:</td>
			<td>
				[Order:TotalAmountIncVAT]</td>
		</tr>
	</tbody>
</table>
<p>
	You can also view the full order details by visiting the following link:</p>
<p>
	<a href='[FrontendInvoiceLink]'>[FrontendInvoiceLink]</a></p>
<p>
	A history of all your orders can also be viewed when you login on the website, from the <strong>Order History </strong>section.</p>
<p>
	[Signature]</p>

",
                WhenIsThisEmailSent = "Sent to users when a user pays.")]

            User_Invoices_PaymentSuccessful,

            [EmailTextDefaultValues(Subject = "Order has been created - kindly effect payment", Title = "Order Confirmation",
                            BodyHtml =
@"
<p>
	Dear [Order:CustomerFullName],</p>
<p>
	Thank you for placing an order (<strong>[Order:Reference]</strong>) with [WebsiteName]. &nbsp;You can easily effect payment online by clicking on the below link:</p>
<p>
	<a href='[PayOnlineLink]'>[PayOnlineLink]</a></p>
<p>
	If payment has already been effected, kindly ignore this email and you will receive another email about the payment status.</p>
<p>
	You can view the full details about the order from the following link:</p>
<p>
	<a href='[FrontendInvoiceLink]'>[FrontendInvoiceLink]</a></p>
<p>
	If you have any enquiries regarding this order, simply reply to this email.</p>
<p>
	[Signature]</p>

",

                            WhenIsThisEmailSent = "When an order is confirmed (before proceeding to actual payment)")]

            User_Invoices_OrderConfirmation,

            HtmlEmailTemplate,

            Signature,

            [EmailTextDefaultValues(Subject = "New Member Registered", Title = "New Member Registered",
                BodyHtml =
                @"
<p>
	A new member has registered on the website. Below is a short summary of his details.&nbsp;</p>
<table border='0' cellpadding='2' cellspacing='2' style='width: 500px;'>
	<tbody>
		<tr>
			<td>
				<strong>Name:</strong></td>
			<td>
				[Member:fullName]</td>
		</tr>
		<tr>
			<td>
				<strong>Email:</strong></td>
			<td>
				[Member:email]</td>
		</tr>
		<tr>
			<td>
				<strong>Address:</strong></td>
			<td>
				[Member:AddressOneLine]</td>
		</tr>
		<tr>
			<td>
				<strong>Telephone:</strong></td>
			<td>
				[Member:Telephone]</td>
		</tr>
		<tr>
			<td>
				<strong>Mobile:</strong></td>
			<td>
				[Member:Mobile]</td>
		</tr>
		<tr>
			<td>
				<strong>Username:</strong></td>
			<td>
				[Member:Username]</td>
		</tr>
	</tbody>
</table>
<p>
	For full details on the member, you can view the member directly in the CMS:</p>
<p>
	<a href='[CmsEditLink]'>[CmsEditLink]</a></p>

<p>
	[Signature]</p>
	

",
                WhenIsThisEmailSent = "Sent to administrators when a new user is registered.")]
            Administration_Members_OnMemberRegistration,

            [EmailTextDefaultValues(Subject = "New contact form", Title = "User Sent Contact Form", BodyHtml = "<p> A new contact form has been sent. Listed below you can find its details:</p> <p> [CONTENT]</p> <p> Auto-Mailer</p> ", WhenIsThisEmailSent = "This email is sent to the administration notifying them about a new contact form being filled in a site user.")]
            Administration_ContactForms_UserSentContactForm,

            [EmailTextDefaultValues(Subject = "User Payment Successful", Title = "User Payment Successful",
                BodyHtml = @"

<p>
	A payment has been done successfully.&nbsp; Below is a short summary of the order details:</p>
<table border='0' cellpadding='2' cellspacing='2' style='width: 500px;'>
	<tbody>
		<tr>
			<td>
				Reference:</td>
			<td>
				<a href='[FrontendInvoiceLink]'>[Order:Reference]</a></td>
		</tr>
		<tr>
			<td>
				Total:</td>
			<td>
				[Order:TotalAmountIncVAT]</td>
		</tr>
	</tbody>
</table>
<p>
	You can view the invoice from the following link:</p>

<p>
	<a href='[FrontendInvoiceLink]'>[FrontendInvoiceLink]</a></p>
<p>
	You can also view the invoice in the CMS/Back-office from the following link:</p>
<p>
	<a href='[CmsLink]'>[CmsLink]</a></p>
<p>
	[Signature]</p>
",
                WhenIsThisEmailSent = "Sent to administrators when a user pays.")]
            Administration_Invoices_UserPaymentSuccessful,

            [EmailTextDefaultValues(Subject = "A new order has been created - Payment pending", Title = "Order Confirmation",
               BodyHtml =
               @"
<p>
	A new order [Order:Reference] has been placed, and is now pending payment. &nbsp;</p>
<table border='0' cellpadding='3' cellspacing='1' style='width: 600px; '>
	<tbody>
		<tr>
			<td>
				<strong>Customer:</strong></td>
			<td>
				[Order:CustomerFullName]</td>
		</tr>
		<tr>
			<td>
				<strong>Email:</strong></td>
			<td>
				[Order:CustomerEmail]</td>
		</tr>
		<tr>
			<td>
				<strong>Total (inc. VAT):</strong></td>
			<td>
				[Order:TotalAmountIncVAT]</td>
		</tr>
	</tbody>
</table>
<p>
	You can view the order details from the following link:</p>
<p>
	<a href='[FrontendInvoiceLink]'>[FrontendInvoiceLink]</a></p>
<p>
	Below is also the link to view the order directly in CMS:</p>
<p>
	<a href='[CmsLink]'>[CmsLink]</a></p>
<p>
	___<br />
	AutoMailer</p>

",
               WhenIsThisEmailSent = "Sent to administrators when a user confirms an order (before proceeding to payment).")]
            Administration_Invoices_OrderConfirmation,

            User_CommentSubmitted,

            Administration_CommentSubmitted,

            SubscriptionPaidSuccessful,

            [EmailTextDefaultValues(Subject = "You have successfully changed your subscription type!", Title = "User: Successfully Changed your subscription type (incl. auto-renew)", BodyHtml = "<p>Dear [SubscriptionPurchase:ClientPersonalFullName],</p><p>you have successfully changed your subscription type. You are now on the [SubscriptionPurchase:SubscriptionTypeName] subscription valued at [SubscriptionPurchase:SubscriptionTypePriceValue].</p><p>Start Date: [SubscriptionPurchase:SubscriptionStartDate]</p><p>End Date: [SubscriptionPurchase:SubscriptionEndDate]</p><p>This subscription will be auto-renewed.</p><p>[Signature]</p>", WhenIsThisEmailSent = "This is an email sent to the user whenever s/he changes the subscription type (mid-way) s/he is subscribed to, and enables auto-renew.")]
            Subscriptions_ChangedSubscriptionType_InclAutoRenew,
            [EmailTextDefaultValues(Subject = "Thank you for purchasing a new subscription!", Title = "User: Successfully Purchased a new Subscription (incl. auto-renew)", BodyHtml = "<p> Dear [SubscriptionPurchase:ClientPersonalFullName],</p> <p style=\"line-height: 23px; \"> you have successfully purchased a new subscription.</p> <p style=\"line-height: 23px; \"> You are on [SubscriptionPurchase:SubscriptionTypeName] subscription valued at [SubscriptionPurchase:SubscriptionTypePriceValue].</p> <p style=\"line-height: 23px; \"> Start Date: [SubscriptionPurchase:SubscriptionStartDate]</p> <p style=\"line-height: 23px; \"> End Date: [SubscriptionPurchase:SubscriptionEndDate]</p> <p style=\"line-height: 23px; \"> This subscription will be auto-renewed.</p> <p style=\"line-height: 23px; \"> [Signature]</p> ", WhenIsThisEmailSent = "This is an email sent to the user when they purchase a new subscription, and enables auto-renew.")]
            Subscriptions_NewSubscription_InclAutoRenew,
            [EmailTextDefaultValues(Subject = "Thank you for renewing subscription!", Title = "User: Successfully Renewed a Subscription (incl. auto-renew)", BodyHtml = "<p> Dear [SubscriptionPurchase:ClientPersonalFullName],</p> <p style=\"line-height: 23px; \"> you have successfully renewed your subscription.</p> <p style=\"line-height: 23px; \"> You are on [SubscriptionPurchase:SubscriptionTypeName] subscription valued at [SubscriptionPurchase:SubscriptionTypePriceValue].</p> <p style=\"line-height: 23px; \"> Start Date: [SubscriptionPurchase:SubscriptionStartDate]</p> <p style=\"line-height: 23px; \"> End Date: [SubscriptionPurchase:SubscriptionEndDate]</p> <p style=\"line-height: 23px; \"> This subscription will be auto-renewed.</p> <p style=\"line-height: 23px; \"> [Signature]</p> ", WhenIsThisEmailSent = "This is an email sent to the user when they renew a subscription, and enables auto-renew.")]
            Subscriptions_Renewal_InclAutoRenew,
            [EmailTextDefaultValues(Subject = "You have successfully changed your subscription type!", Title = "User: Successfully Changed your subscription type", BodyHtml = "", WhenIsThisEmailSent = "This is an email sent to the user whenever s/he changes the subscription type (mid-way) s/he is subscribed to.")]
            Subscriptions_ChangedSubscriptionType,
            [EmailTextDefaultValues(Subject = "Thank you for purchasing a new subscription!", Title = "User: Successfully Purchased a new Subscription", BodyHtml = "", WhenIsThisEmailSent = "This is an email sent to the user when they purchase a new subscription.")]
            Subscriptions_NewSubscription,
            [EmailTextDefaultValues(Subject = "Thank you for renewing subscription!", Title = "User: Successfully Renewed a Subscription", BodyHtml = "", WhenIsThisEmailSent = "This is an email sent to the user when they renew a subscription.")]
            Subscriptions_Renewal,
            [EmailTextDefaultValues(Subject = "Your subscription has expired!", Title = "User: Subscription has expired!", BodyHtml = "<p> Dear [SubscriptionPurchase:ClientPersonalFullName],</p> <p style=\"line-height: 23px; \"> Your subscription has ended and was not renewed. To re-purchase a new subscription please login to your account.</p> <p style=\"line-height: 23px; \"> [Signature]</p> ", WhenIsThisEmailSent = "This is an email sent to the user when their subscription has expired and has not been renewed (since auto-renew is disabled for that user).")]
            Subscriptions_Expired,
            [EmailTextDefaultValues(Subject = "Your have successfully self barred your account!", Title = "User: Add a self-barring period!", BodyHtml = "", WhenIsThisEmailSent = "This email is sent to the affected user whenever a self barring period is added to the respective user.")]
            User_SelfBarring_SelfBarringSetSuccessfully,
            [EmailTextDefaultValues(Subject = "Your self barring period has expired.", Title = "User: Self-barring period expired!", BodyHtml = "", WhenIsThisEmailSent = "This email is sent to the affected user whenever a self barring period has expired.")]
            User_Members_SelfBarring_ExpiredNotification,
            [EmailTextDefaultValues(Subject = "Your self barring period will expire soon.", Title = "User: Self-barring period expiry notification 1!", BodyHtml = "", WhenIsThisEmailSent = "This email is sent to the affected user as a first notification about the expiry of a self barring period.")]
            User_Members_SelfBarring_ExpiryNotification1,
            [EmailTextDefaultValues(Subject = "Your self barring period will expire soon.", Title = "User: Self-barring period expiry notification 2!", BodyHtml = "", WhenIsThisEmailSent = "This email is sent to the affected user as a second notification about the expiry of a self barring period.")]
            User_Members_SelfBarring_ExpiryNotification2,
            Subscriptions_Downgraded,
            [EmailTextDefaultValues(Subject = "An enquiry was submitted on your classified post", Title = "User: Enquiry was submitted on your classified post", BodyHtml = "<p>Dear [Member:FirstName],</p> <p>An enquiry was sent on <a href=\"[Classified:URL]\">your classified listing</a>.</p> <p>[CONTENT]</p> <p>[Signature]</p>", WhenIsThisEmailSent = "This email is sent to the owner of a classified item notifying him/her that an enquiry was sent regarding that particular classified item. Enquiry will be included in the email message itself.")]
            Classifieds_Owner_EnquirySubmitted,
            [EmailTextDefaultValues(Subject = "Your classified item has been posted", Title = "User: Your classified item has been posted", BodyHtml = "<p>Dear [Member:FirstName],</p> <p>Your classified item has been submitted. It might be awaiting moderation before getting posted publicly.</p> <p>Once posted, you would be able to view your classified item here: <a href=\"[Classified:URL]\">[Classified:URL]</a>. <p>[Signature]</p>", WhenIsThisEmailSent = "This email is sent to the owner of the classified item upon getting submitted notifying him about his/her post.")]
            Classifieds_Owner_YourClassifiedItemHasBeenPosted,
            [EmailTextDefaultValues(Subject = "A classified item has been posted", Title = "Admin: A classified item has been posted", BodyHtml = "<p>Dear [Admin:Name],</p> <p>A classified item has been submitted. Depending on how the system is configured, you might be required to moderate and approve this item.</p> <p>The frontend link for this item: <a href=\"[Classified:URL]\">[Classified:URL]</a> <p>The CMS link for this item: <a href=\"[Classified:CMSLink]\">[Classified:CMSLink]</a> <p>[Signature]</p>", WhenIsThisEmailSent = "This email is sent to the website administration notifying them that a new Classified Item has been submitted.")]
            Classifieds_Admin_AClassifiedItemHasBeenPosted,
            [EmailTextDefaultValues(Subject = "You have subscribed to our newsletter", Title = "User: You have subscribed to our newsletter", BodyHtml = "<p>Dear [Newsletter:UserFullName],</p> <p>You have received this email to notify you that your email address has been subscribed to our mailing lists. This was done on [Newsletter:AppliedOn].</p> <p>Should you want to reverse this action, kindly visit our site for details on unsubscribing.</p> <p>[Signature]</p>", WhenIsThisEmailSent = "This email is sent to the user subscribing to the newsletters, notifying him / her so.")]
            NewsLetters_User_YouHaveSubscribed,
            [EmailTextDefaultValues(Subject = "You have unsubscribed from our newsletter", Title = "User: You have unsubscribed from our newsletter", BodyHtml = "<p>Dear [Newsletter:UserFullName],</p> <p>This is the last email shot you are receiving as you have successfully unsubscribed from the system.</p> <p>Nonetheless, we hope you still come visit us soon.</p> <p>[Signature]<p>", WhenIsThisEmailSent = "This email is sent to the user unsubscribing from the newsletters, notifying him / her so.")]
            NewsLetters_User_YouHaveUnSubscribed,
            [EmailTextDefaultValues(Subject = "A new event has been submitted", Title = "Admin: A new event has been submitted", BodyHtml = "<p>Dear [Admin:AdminToName],</p> <p>A new event has been submitted on your website by one of your users.</p> <p><b>Details for this event</b>:</p> <p> <ul> <li><u>User Full Name</u>: [User:FullName]</li> <li><u>User IP Address</u>: [User:IPAddress]</li> <li><u>Event Name</u>: [Event:Name]</li> <li><u>Event Start Date</u>: [Event:StartDate]</li> <li><u>Event End Date</u>: [Event:EndDate]</li> <li><u>Category</u>: [Event:Category]</li> <li><u>Submitted On</u>: [Event:Timestamp]</li> </ul> </p> <p>This event still needs moderation and approval before it gets shown live. You can go directly into the CMS to moderate this event submission by clicking <a href=\"[Admin:CmsLink]\">here</a> and logging with your CMS account.</p> <p>[Signature]<p>", WhenIsThisEmailSent = "This email is sent to the administrator email notifying him / her that a new event has been submitted.")]
            EventSubmission_Admin_NewSubmission,
            [EmailTextDefaultValues(Subject = "The event you submitted is now live!", Title = "User: The event you submitted is now live!", BodyHtml = "<p>Dear [User:FullName],</p> <p>The event titled '[Event:Name]' <i>([Event:StartDate] - [Event:EndDate])</i> you had submitted is now live!</p> <p>To view the event, follow <a href=\"[Event:FrontendURL]\">this</a> link.</p> <p>Thank you for contributing to a better and more vast collection of events taking place!</p> <p>[Signature]</p>", WhenIsThisEmailSent = "This email is sent to the user who created an event, notifying him that it is now live.")]
            EventSubmission_User_YourEventIsLive,
            [EmailTextDefaultValues(Subject = "You have successfully submitted an event!", Title = "User: You have successfully submitted an event!", BodyHtml = "<p>Dear [User:FullName],</p> <p>The event titled '[Event:Name]' <i>([Event:StartDate] - [Event:EndDate])</i> has been submitted and is awaiting moderation!</p> <p>Our staff has been notified about this and you shall receive an email upon your event going live.</p> <p>Thank you for contributing to a better and more vast collection of events taking place!</p> <p>[Signature]</p>", WhenIsThisEmailSent = "This email is sent to the user who created an event, notifying him that it has been submitted successfully but awaiting moderation.")]
            EventSubmission_User_YourEventHasBeenSubmitted
        }
        public enum REFERRAL_STATUS_TYPE
        {
            Invalid = 25,
            Pending = 50,
            Successful = 75
        }
        public enum BLOGS_SORT_BY
        {
            None,
            MostRecentBlogPost
        }
        public enum BLOG_POSTS_SORT_BY
        {
            TitleAscending,
            TitleDescending,
            DateAscending,
            DateDescending
        }
        public enum BLOG_COMMENTS_SORT_BY
        {
            DateAscending,
            DateDescending
        }
        public enum ITEM_GROUP_SORT_BY
        {
            [DescriptionAttribute("Title (A - Z)")]
            TitleAscending,
            [DescriptionAttribute("Title (Z - A)")]
            TitleDescending,
            [DescriptionAttribute("Price (Cheapest First)")]
            PriceAscending,
            [DescriptionAttribute("Price (Highest First)")]
            PriceDescending,
            [DescriptionAttribute("Featured")]
            FeaturedAscending
        }

        public enum EVENTS_SORT_BY
        {
            [DescriptionAttribute("Title (A..Z)")]
            TitleAscending,
            [DescriptionAttribute("Title (Z..A)")]
            TitleDescending,
            [DescriptionAttribute("Start Date (Closest First)")]
            StartDateAsc,
            [DescriptionAttribute("Start Date (Farthest First)")]
            StartDateDesc,
            [DescriptionAttribute("Relevance")]
            Relevance
        }

        public static IContentTextBase ItemGroupSortByToContentText(ITEM_GROUP_SORT_BY value)
        {
            switch (value)
            {
                case ITEM_GROUP_SORT_BY.TitleAscending: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Shop_ItemGroup_Sorting_TitleAscending);
                case ITEM_GROUP_SORT_BY.TitleDescending: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Shop_ItemGroup_Sorting_TitleDescending);
                case ITEM_GROUP_SORT_BY.PriceAscending: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Shop_ItemGroup_Sorting_PriceAscending);
                case ITEM_GROUP_SORT_BY.PriceDescending: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Shop_ItemGroup_Sorting_PriceDescending);
                //  case ITEM_GROUP_SORT_BY.Priority: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Shop_ItemGroup_Sorting_Priority);
                case ITEM_GROUP_SORT_BY.FeaturedAscending: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Shop_ItemGroup_Sorting_FeaturedAscending);
                //  case ITEM_GROUP_SORT_BY.FeaturedDescending: return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Shop_ItemGroup_Sorting_FeaturedDescending);
                default: throw new NotImplementedException("Value <" + value + "> not mapped");
            }
        }

        public enum LOCATION_SORT_BY
        {
            TitleAscending,
            TitleDescending
        }
        public enum BANNERS_SORT_BY
        {
            TitleAscending,
            TitleDescending,
            PriorityAscending,
            PriorityDescending
        }
        public enum LOCATION_TYPE
        {
            Country,
            Region,
            Town
        }

        public enum FACTORY_PRIORITIES
        {
            Settings = 10000,
            Currency = 20000,
            CultureDetails = 30000,
            Location = 40000


        }

        public enum SHIPPING_TYPE
        {
            PickUp = 1000
        }

        public static ContentTextBaseFrontend GetLoginStatusMessage(ILoginResult<IUser> result)
        {

            switch (result.LoginStatus)
            {
                case CS.General_v3.Enums.LOGIN_STATUS.InvalidPass:
                    return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Login_ResultMessage_InvalidPass).ToFrontendBase();
                case CS.General_v3.Enums.LOGIN_STATUS.InvalidUser:
                    return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Login_ResultMessage_InvalidUser).ToFrontendBase();
                case CS.General_v3.Enums.LOGIN_STATUS.NotAccepted:
                    return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Login_ResultMessage_NotAccepted).ToFrontendBase();
                case CS.General_v3.Enums.LOGIN_STATUS.NotActivated:
                    return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Login_ResultMessage_NotActivated).ToFrontendBase();
                case CS.General_v3.Enums.LOGIN_STATUS.Ok:
                    return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Login_ResultMessage_Ok).ToFrontendBase();
                case CS.General_v3.Enums.LOGIN_STATUS.SelfExcluded:
                    var ct = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Login_ResultMessage_SelfExcluded).ToFrontendBase();
                    string dateFormat = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.Website_ShortDateFormat);
                    ct.TokenReplacer[BusinessLogic_v3.Constants.Tokens.DATE] = result.BlockedUntil.GetValueOrDefault().ToString(dateFormat);
                    return ct;
                case CS.General_v3.Enums.LOGIN_STATUS.Blocked:

                    return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Login_ResultMessage_Blocked).ToFrontendBase();
                case CS.General_v3.Enums.LOGIN_STATUS.TerminatedAccount:
                    return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_Login_ResultMessage_TerminatedAccount).ToFrontendBase();
                default:
                    throw new InvalidOperationException("Value <" + result.LoginStatus + "> not mapped");

            }

        }

        public static IContentTextBase MemberAccountBalanceHistoryUpdateTypeToContentText(MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE value)
        {
            switch (value)
            {
                case MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE.Adjustment: return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_Adjustment);
                case MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE.Deposit: return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_Deposit);
                case MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE.Purchase: return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_Purchase);
                case MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE.RenewalBasedOnSubscription: return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_RenewalBasedOnSubscription);
                case MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE.Withdraw: return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_Withdraw);
                case MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE.Won: return Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_OrderHistory_AccountBalanceHistoryUpdateType_Won);
                default: throw new NotImplementedException("Value <" + value + "> not mapped");
            }
        }



    }
}
