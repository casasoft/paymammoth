using BusinessLogic_v3.Classes.General;
using CS.General_v3.Classes.Routing;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.RoutingInfoModule
{   

//BaseFactory-File

    public abstract class RoutingInfoBaseFactory : BusinessLogic_v3.Modules._AutoGen.RoutingInfoBaseFactoryAutoGen, IRoutingInfoBaseFactory, IItemWithIdentifierFactory<RoutingInfoBase>
    {

        protected RoutingInfoBase getRouteByStringIdentifier(string s)
        {
            return RoutingInfoCacheManager.Instance.GetItemByIdentifier(s);
            //var q = GetQuery();
            //q = q.Where(x => x.Identifier == s);
            //return FindItem(q);
        }

        private RoutingInfoBase createRouteWithDefaultValues(Enum e)
        {
            RoutingInfoBase route = null;
            using (var t= BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionIfNotOneAlreadyActive())
            {

                var attrib = ReflectionUtil.GetAttributeFromEnumValue<Attributes.RouteDefaultInfoAttribute>(e);
                {
                    StringBuilder sbError = new StringBuilder();
                    if (attrib == null)
                    {
                        sbError.AppendLine("Please make sure to specify a [RouteDefaultInfo] attribute for route with enum <" + e + ">");
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(attrib.PhysicalPath)) sbError.AppendLine("Please specify PhysicalPath in the [RouteDefaultInfo] attribute");
                        if (string.IsNullOrWhiteSpace(attrib.VirtualPath)) sbError.AppendLine("Please specify VirtualPath in the [RouteDefaultInfo] attribute");

                    }
                    if (sbError.Length > 0)
                    {
                        throw new InvalidOperationException("Invalid data / information for route <" + e + "> and it's [RouteDefaultInfo] attribute\r\n\r\n" + sbError.ToString());
                    }

                }

                route = CreateNewItem();
                route.Identifier = CS.General_v3.Util.EnumUtils.StringValueOf(e);
                route.RouteName = route.Identifier;
                route.CopyDefaultValues(attrib);
                route.Create();
                if (t != null) t.Commit();
            }
            return route;
        }

	    public RoutingInfoBase GetRouteByIdentifier(Enum e, bool createIfNotExists = true)
        {
            if (e == null) throw new InvalidOperationException("Identifier is required");
            string s = CS.General_v3.Util.EnumUtils.StringValueOf(e);
            var route = getRouteByStringIdentifier(s);
            if (route == null)
            {
                if (createIfNotExists)
                {
                    route  =createRouteWithDefaultValues(e);
                }
            }
            return route;


        }

        public void MapAllRoutes()
        {

            var list = FindAll();
            foreach (var r in list)
            {
                r.MapRoute();
            }
        }


        public string GetUrlForGeneralRoute(BusinessLogic_v3.Enums.RouteUrlsEnum route, MyRouteValueDictionary paramValues = null, bool fullyQualifiedUrl = true)
        {
            return GetUrlForRoute(route,paramValues, fullyQualifiedUrl);

        }


        public string GetUrlForRoute(Enum e, MyRouteValueDictionary paramValues, bool fullyQualifiedUrl = true)
        {
            string url = null;
            RoutingInfoBase r = GetRouteByIdentifier(e);
            if (r != null)
            {
                url = r.GetMappedUrlForRoute(paramValues, fullyQualifiedUrl);
            }
            else
            {
                throw new InvalidOperationException("Route <" + e + "> does not exist");
            }
            return url;

        }
        public string GetNameForRoute(Enum e)
        {
            string url = null;
            RoutingInfoBase r = GetRouteByIdentifier(e);
            return r.GetRouteNameForMapping();
        }
        

        public void RegisterRoutes<T>() where T: struct
        {
            var enumValues = CS.General_v3.Util.EnumUtils.GetListOfEnumValues(typeof(T));
            foreach (var e in enumValues)
            {
                var r = GetRouteByIdentifier(e);
                r.MapRoute();
            }


        }

        RoutingInfoBase IItemWithIdentifierFactory<RoutingInfoBase>.GetItemByIdentifierFromDB(string identifier)
        {
            var q = GetQuery();
            q = q.Where(item => item.Identifier == identifier);
            return FindItem(q);
        }

        RoutingInfoBase IItemWithIdentifierFactory<RoutingInfoBase>.GetItemByPKeyFromDB(long id)
        {
            return GetByPrimaryKey(id);
        }

        IEnumerable<RoutingInfoBase> IItemWithIdentifierFactory<RoutingInfoBase>.GetAllItemsFromDB()
        {
            return FindAll();
        }
    }
}
