using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.RoutingInfoModule
{

//BaseClassMap-File
    
    public abstract class RoutingInfoBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.RoutingInfoBaseMap_AutoGen<TData>
    	where TData : RoutingInfoBase
    {


        protected override void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
            base.IdentifierMappingInfo(mapInfo);
            mapInfo.IsIndexed = true;
            mapInfo.Length = 250;
        }

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
    }
   
}
