using CS.General_v3.Classes.Routing;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;


using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.RoutingInfoModule
{   

//IBaseFactory-File

	public interface IRoutingInfoBaseFactory : BusinessLogic_v3.Modules._AutoGen.IRoutingInfoBaseFactoryAutoGen
	{

	    string GetUrlForRoute(Enum e, MyRouteValueDictionary paramValues, bool fullyQualifiedUrl = true);
	    void RegisterRoutes<T>() where T : struct;
        string GetUrlForGeneralRoute(BusinessLogic_v3.Enums.RouteUrlsEnum route, MyRouteValueDictionary paramValues = null,  bool fullyQualifiedUrl = true);
	    void MapAllRoutes();
	    RoutingInfoBase GetRouteByIdentifier(Enum e, bool createIfNotExists = true);
        string GetNameForRoute(Enum e);
	}
}
