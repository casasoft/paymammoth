﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.General;

namespace BusinessLogic_v3.Modules.RoutingInfoModule
{
    public class RoutingInfoCacheManager : ItemsByIdentifierCache<RoutingInfoBase>
    {
        private RoutingInfoCacheManager() : base(RoutingInfoBase.Factory)
        {
            
        }
        public static RoutingInfoCacheManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<RoutingInfoCacheManager>(); } }
    }
}
