﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.RoutingInfoModule.Attributes
{
    public class RouteDefaultInfoAttribute :Attribute,IRouteDefaultInfo
    {
        #region IRouteDefaultInfo Members

        public string PhysicalPath { get; set; }

        public string VirtualPath { get; set; }

        #endregion
    }
}
