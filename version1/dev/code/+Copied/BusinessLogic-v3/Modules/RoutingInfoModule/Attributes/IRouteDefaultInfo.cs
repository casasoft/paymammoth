﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.RoutingInfoModule.Attributes
{
    public interface IRouteDefaultInfo
    {
        string PhysicalPath { get; set; }
        string VirtualPath { get; set; }

    }
}
