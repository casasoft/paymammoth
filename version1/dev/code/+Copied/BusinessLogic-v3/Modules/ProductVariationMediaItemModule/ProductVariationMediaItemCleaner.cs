﻿using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic_v3.Modules.ProductVariationMediaItemModule
{
    public class ProductVariationMediaItemCleaner
    {
        public ProductVariationMediaItemCleaner()
        {
            this.DeletedItems = new List<ProductVariationMediaItemBase>();
        }

        public List<ProductVariationMediaItemBase> DeletedItems { get; set; }

        private List<ProductVariationMediaItemBase> data = null;



        private void loadData()
        {

            var q = ProductVariationMediaItemBaseFactory.Instance.GetQuery();
            data = ProductVariationMediaItemBaseFactory.Instance.FindAll(q).ToList();
            this.DeletedItems = new List<ProductVariationMediaItemBase>();
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                foreach (var item in data)
                {
                    checkItem(item);
                }
                t.Commit();
            }
        }

        private void checkItem(ProductVariationMediaItemBase item)
        {
            if (!item.Image.CheckIfOriginalFileExists())
            {
                item.Delete();
                DeletedItems.Add(item);
            }
        }

        /// <summary>
        /// Cleans up any missing files, and returns the deleted items
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProductVariationMediaItemBase> CleanUpMissingFiles()
        {
            loadData();
            return this.DeletedItems;
        }
    }
}
