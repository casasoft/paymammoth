using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.ProductVariationMediaItemModule
{

//BaseClassMap-File
    
    public abstract class ProductVariationMediaItemBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.ProductVariationMediaItemBaseMap_AutoGen<TData>
    	where TData : ProductVariationMediaItemBase
    {

        //protected override void ProductVariationMappingInfo(ICollectionMapInfo mapInfo)
        //{
        //    base.ProductVariationMappingInfo(mapInfo);
        //    mapInfo.Inverse = true;
        //}
 
    }
   
}
