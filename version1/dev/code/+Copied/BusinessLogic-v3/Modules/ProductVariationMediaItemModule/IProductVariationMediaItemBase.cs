using BusinessLogic_v3.Classes.MediaItems;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using CS.General_v3.Classes.Interfaces.ImageItem;
using CS.General_v3.Classes.Interfaces.Gallery;

namespace BusinessLogic_v3.Modules.ProductVariationMediaItemModule
{

//IBaseClass-File
	
    public interface IProductVariationMediaItemBase : BusinessLogic_v3.Modules._AutoGen.IProductVariationMediaItemBaseAutoGen, IImageItem 
    {
        IMediaItemImage<ProductVariationMediaItemBase.ImageSizingEnum> Image { get; }
    }
}
