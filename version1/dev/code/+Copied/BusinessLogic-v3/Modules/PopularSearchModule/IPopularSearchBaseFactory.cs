using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.PopularSearchModule
{   

//IBaseFactory-File

	public interface IPopularSearchBaseFactory : BusinessLogic_v3.Modules._AutoGen.IPopularSearchBaseFactoryAutoGen
    {
        IEnumerable<IPopularSearchBase> GetPopularSearchesForFrontend(int count);
		

    }

}
