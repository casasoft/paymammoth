using BusinessLogic_v3.Util;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.PopularSearchModule
{   

//BaseFactory-File

	public abstract class PopularSearchBaseFactory : BusinessLogic_v3.Modules._AutoGen.PopularSearchBaseFactoryAutoGen, IPopularSearchBaseFactory
    {






        public void FillCriteriaConditionsWithItemsForFrontendOnly(ICriteria crit)
        {
            crit.Add(Restrictions.Where<PopularSearchBase>(item => item.Published));
            crit.Add(Restrictions.Where<PopularSearchBase>(item => !item.HideFromFrontend));

        }


        #region IPopularSearchBaseDataFactory Members

        public IEnumerable<IPopularSearchBase> GetPopularSearchesForFrontend(int count)
        {
            var q = GetQuery();
            FillCriteriaConditionsWithItemsForFrontendOnly(q.RootCriteria);

            return nHibernateUtil.LimitQueryByPrimaryKeysAndReturnResult(q, 1, count);

        }

        #endregion
    }

}
