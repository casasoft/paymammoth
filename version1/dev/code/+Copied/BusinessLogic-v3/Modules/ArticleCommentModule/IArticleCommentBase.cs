using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using CS.General_v3.Classes.Interfaces.BlogPosts;
using BusinessLogic_v3.Modules.ArticleModule;

namespace BusinessLogic_v3.Modules.ArticleCommentModule
{

//IBaseClass-File
	
    public interface IArticleCommentBase : BusinessLogic_v3.Modules._AutoGen.IArticleCommentBaseAutoGen, ICommentData
    {
        string Comment { get; set; }
        string IPAddress { get; set; }
        string Author { get; set; }
        void SendNotificationEmail();

        /// <summary>
        /// Get the article in which this comment is displayed.
        /// </summary>
        /// <returns></returns>
        IArticleBase GetArticle();

        void Disapprove(bool autoSave = true);
        bool Approve(bool autoSave = true);

        /// <summary>
        /// Returns true if Parent Reply is approved or there are no parents any more.
        /// </summary>
        /// <returns></returns>
        bool IsParentReplyApproved();

        /// <summary>
        /// Returns all the child replies by recursion as a flat list.
        /// </summary>
        /// <param name="getOnlyApproved">If true, only approved replies will be returned.</param>
        /// <returns></returns>
        IEnumerable<ArticleCommentBase> GetChildrenReplies(bool getOnlyApproved = true);

        /// <summary>
        /// Get the number of chilren replies (by recursion) which are approved.
        /// </summary>
        /// <returns></returns>
        int GetNumberOfApprovedChildrenReplies();
    }
}
