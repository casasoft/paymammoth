using BusinessLogic_v3.Classes.General;
using BusinessLogic_v3.Modules.AdvertModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.AdvertColumnModule
{

//BaseClass-File

    public abstract class AdvertColumnBase : BusinessLogic_v3.Modules._AutoGen.AdvertColumnBaseAutoGen, IAdvertColumnBase, IItemWithIdentifier
    {
    
		

    
        public AdvertColumnBase()
        {
            
        }    
    	protected override void initPropertiesForNewItem()
        {
        	//Fill any default values for NEW items.
        
            base.initPropertiesForNewItem();
        }
    	

        public static AdvertColumnBase GetColumnByIdentifier(string identifier)
        {
            return Factory.GetColumnByIdentifier(identifier);


        }
        public IEnumerable<AdvertBase> GetAdvertsToShow()
        {
            var list = new List<AdvertBase>();

            foreach (var slot in this.AdvertSlots)
            {
                AdvertBase ad = (AdvertBase)slot.GetNextAdvertToShow();
                if (ad != null)
                {
                    list.Add(ad);
                }
            }
            return list;
        }
        public override string ToString()
        {
            return this.Title;

        }


       

        internal void CopyFromInterface(IAdvertColumnBase columnTemp)
        {
            this.Identifier = this.Title = columnTemp.Identifier;
        }
    }
}
