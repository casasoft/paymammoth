using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;


using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using CS.General_v3.Classes.HelperClasses;

namespace BusinessLogic_v3.Modules.TestimonialModule
{   

//IBaseFactory-File

	public interface ITestimonialBaseFactory : BusinessLogic_v3.Modules._AutoGen.ITestimonialBaseFactoryAutoGen
    {

        /// <summary>
        /// Get Testimonials for paging
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="showAmt"></param>
        /// <param name="sortBy"></param>
        /// <returns></returns>
        SearchPagedResults<ITestimonialBase> GetTestimonials(int pageNo, int showAmt, BusinessLogic_v3.Enums.TESTIMONIAL_SORT_BY sortBy);

        /// <summary>
        /// Get random testimonials based on the amount needed
        /// </summary>
        /// <param name="totalAmounts"></param>
        /// <returns></returns>
        IEnumerable<ITestimonialBase> GetTestimonials(int totalAmounts);
    }

}
