using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Modules.TestimonialModule
{   

//BaseFactory-File

	public abstract class TestimonialBaseFactory : BusinessLogic_v3.Modules._AutoGen.TestimonialBaseFactoryAutoGen, ITestimonialBaseFactory
    {


        /// <summary>
        /// Get Testimonials for paging
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="showAmt"></param>
        /// <param name="sortBy"></param>
        /// <returns></returns>
        public SearchPagedResults<ITestimonialBase> GetTestimonials(int pageNo, int showAmt, Enums.TESTIMONIAL_SORT_BY sortBy)
        {
            var q = GetQuery();
            switch(sortBy)
            {
                case Enums.TESTIMONIAL_SORT_BY.DateAsc:
                    q = q.OrderBy(item => item.DateSubmitted).Asc;
                    break;
                case Enums.TESTIMONIAL_SORT_BY.DateDesc:
                    q = q.OrderBy(item => item.DateSubmitted).Desc;
                    break;
                case Enums.TESTIMONIAL_SORT_BY.NameAsc:
                    q = q.OrderBy(item => item.FullName).Asc;
                    break;
                case Enums.TESTIMONIAL_SORT_BY.NameDesc:
                    q = q.OrderBy(item => item.FullName).Desc;
                    break;
            }
            int totalResults = 0;
            IEnumerable<TestimonialBase> resultsList = nHibernateUtil.
                LimitCriteriaByPrimaryKeysAndReturnResultAndTotalCount<TestimonialBase>(
                    q.RootCriteria, pageNo, showAmt, out totalResults);
            return new SearchPagedResults<ITestimonialBase>(resultsList, totalResults);
        }

        /// <summary>
        /// Get random testimonials based on the amount needed
        /// </summary>
        /// <param name="totalAmounts"></param>
        /// <returns></returns>
        public IEnumerable<ITestimonialBase> GetTestimonials(int totalAmounts)
        {
            var q = GetQuery();
            q.RootCriteria.AddOrder(nHibernateUtil.GetRandomOrder());
            q.Take(totalAmounts);
            return FindAll(q);
        }
    }
}
