﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Util;

namespace BusinessLogic_v3.Modules.EventSubmissionModule
{
    public class EventSubmissionValidator
    {
        private EventSubmissionBase _eventSubmission { get; set; }

        public EventSubmissionValidator(EventSubmissionBase eventSubmissionInstance)
        {
            if(eventSubmissionInstance == null)
            {
                throw new InvalidOperationException(
                    "This class can only be instantiated with a non-null instance of EventSubmissionBase!");
            }
            this._eventSubmission = eventSubmissionInstance;
        }

        private StringBuilder sb;

        /// <summary>
        /// Validates all the required logic / conditions for this EventSubmission - failure will cause an exception to be thrown.
        /// </summary>
        public void Validate()
        {
            sb = new StringBuilder();
            checkRequiredFields();
            throwExceptionIfFailed();
        }

        private void throwExceptionIfFailed()
        {
            string exceptionMsg = sb.ToString();
            if(!string.IsNullOrWhiteSpace(exceptionMsg))
            {
                throw new EventSubmissionValidationException(exceptionMsg);
            }
        }

        private void checkRequiredFields()
        {
            if(string.IsNullOrWhiteSpace(_eventSubmission.EventName))
            {
                sb.AppendLine("'EventName' must be specified!");
            }
            if(string.IsNullOrWhiteSpace(_eventSubmission.UserFullName))
            {
                sb.AppendLine("'UserFullName' must be specified!");
            }
            if (string.IsNullOrWhiteSpace(_eventSubmission.UserEmail))
            {
                sb.AppendLine("'UserEmail' must be specified!");
            }
            if (_eventSubmission.EventStartDate == new DateTime())
            {
                sb.AppendLine("'EventStartDate' must be set!");
            }
            if (_eventSubmission.EventStartDate != new DateTime() && _eventSubmission.EventStartDate < Date.Now)
            {
                sb.AppendLine("'EventStartDate' must be now or the future!");
            }
            if (_eventSubmission.EventEndDate == new DateTime())
            {
                sb.AppendLine("'EventEndDate' must be set!");
            }
            if (_eventSubmission.EventEndDate != new DateTime() && (_eventSubmission.EventEndDate < Date.Now || _eventSubmission.EventEndDate < _eventSubmission.EventStartDate))
            {
                sb.AppendLine("'EventEndDate' must be now or the future and must be greater than the starting date!");
            }
            if (string.IsNullOrWhiteSpace(_eventSubmission.UserIpAddress))
            {
                sb.AppendLine("'UserIpAddress' must be specified!");
            }
            if (_eventSubmission.Timestamp == new DateTime())
            {
                sb.AppendLine(
                    "'Timestamp' must be set! If this happens, check on backend as this should be set by the backend on the creation of the EventSubmission object. Not a UI problem.");
            }
            if (_eventSubmission.Category == null)
            {
                sb.AppendLine("'Category' cannot be null!");
            }
        }
    }
}
