using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.CmsUserModule;

namespace BusinessLogic_v3.Modules.EventSubmissionModule
{

//IBaseClass-File
	
    public interface IEventSubmissionBase : BusinessLogic_v3.Modules._AutoGen.IEventSubmissionBaseAutoGen
    {
        /// <summary>
        /// Manages validation for this EventSubmission object.
        /// </summary>
        EventSubmissionValidator Validator { get; }

        /// <summary>
        /// Wraps the save process into a transaction (if enabled), and where applicable does extra logic (such as sending emails) before saving - only to be called when submitting not to save!
        /// </summary>
        /// <param name="autoSave"></param>
        void Submit(bool autoSave = true);

        /// <summary>
        /// Approves this event submission and converts it into a real event - if CMSUser is null or has already been approved an exception will be thrown!
        /// </summary>
        /// <param name="cmsUserApproving">The CMS User approving this submission.</param>
        /// <param name="autoSave">Whether or not to wrap the process in a transaction.</param>
        void ApproveAndConvertToRealEvent(ICmsUserBase cmsUserApproving, bool autoSave = true);

        /// <summary>
        /// Convert this submission to a real event - if it hasn't been approved yet, an exception will be thrown!
        /// </summary>
        /// <param name="autoSave">Whether or not to wrap the process in a transaction.</param>
        void ConvertToRealEvent(bool autoSave = true);

        /// <summary>
        /// Approves this event submission - if CMSUser is null or it has already been approved, an exception will be thrown!
        /// </summary>
        /// <param name="cmsUserApproving">The CMS User approving this submission.</param>
        /// <param name="autoSave">Whether or not to wrap the process in a transaction.</param>
        void ApproveSubmission(ICmsUserBase cmsUserApproving, bool autoSave = true);

        /// <summary>
        /// Manages email logic for this EventSubmission object.
        /// </summary>
        EventSubmissionMailer Mailer { get; }

        /// <summary>
        /// If event submission has been converted into an event, returns the URL of the actual real event, otherwise null!
        /// </summary>
        /// <returns></returns>
        string GetEventUrl();
    }
}
