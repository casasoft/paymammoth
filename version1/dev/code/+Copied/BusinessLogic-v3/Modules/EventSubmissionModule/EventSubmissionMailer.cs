﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Text;
using BusinessLogic_v3.Modules.EmailTextModule;
using BusinessLogic_v3.Classes.Culture;
using CS.General_v3.Classes.URL;

namespace BusinessLogic_v3.Modules.EventSubmissionModule
{
    public class EventSubmissionMailer
    {
        private EventSubmissionBase _eventSubmission { get; set; }

        public EventSubmissionMailer(EventSubmissionBase eventSubmissionInstance)
        {
            if(eventSubmissionInstance == null)
            {
                throw new InvalidOperationException(
                    "This class can only be instantiated with a non-null instance of EventSubmissionBase!");
            }
            this._eventSubmission = eventSubmissionInstance;
        }

        public void EmailUserOnSubmit()
        {
            mail(_eventSubmission.UserFullName, _eventSubmission.UserEmail, GeneralFromName, GeneralFromEmail, GeneralReplyToName,
                 GeneralReplyToEmail, Enums.EmailsBusinessLogic.EventSubmission_User_YourEventHasBeenSubmitted, getTokenReplacer());
        }

        public void EmailUserOnCopiedToRealEvent()
        {
            mail(_eventSubmission.UserFullName, _eventSubmission.UserEmail, GeneralFromName, GeneralFromEmail, GeneralReplyToName,
                 GeneralReplyToEmail, Enums.EmailsBusinessLogic.EventSubmission_User_YourEventIsLive, getTokenReplacer());
        }

        public void NotifyAdminOnSubmit()
        {
            mail(AdminEmailName, AdminEmailEmail, GeneralFromName, GeneralFromEmail, GeneralReplyToName,
                 GeneralReplyToEmail, Enums.EmailsBusinessLogic.EventSubmission_Admin_NewSubmission, getTokenReplacer());
        }

        #region General Emails
        string AdminEmailName
        {
            get
            {
                return
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_AdminNotificationToName);
            }
        }

        string AdminEmailEmail
        {
            get
            {
                return
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_AdminNotificationToEmail);
            }
        }
        
        string GeneralFromName
        {
            get
            {
                return
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromName);
            }
        }

        string GeneralFromEmail
        {
            get
            {
                return
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromEmail);
            }
        }

        string GeneralReplyToName
        {
            get
            {
                return
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_ReplyToName);
            }
        }

        string GeneralReplyToEmail
        {
            get
            {
                return
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_ReplyToEmail);
            }
        }
        #endregion

        protected TokenReplacer getTokenReplacer()
        {
            TokenReplacer tr = TokenReplacer.CreateBlank();
            tr["[User:FullName]"] = _eventSubmission.UserFullName;
            tr["[Event:Name]"] = _eventSubmission.EventName;
            tr["[Event:StartDate]"] =
                DefaultCultureManager.Instance.FormatDateTimeWithCurrentCulture(_eventSubmission.EventStartDate,
                                                                                CS.General_v3.Util.Date.DATETIME_FORMAT.
                                                                                    FullDateShortTime);
            tr["[Event:EndDate]"] =
                DefaultCultureManager.Instance.FormatDateTimeWithCurrentCulture(_eventSubmission.EventEndDate,
                                                                                CS.General_v3.Util.Date.DATETIME_FORMAT.
                                                                                    FullDateShortTime);
            tr["[User:IPAddress]"] = _eventSubmission.UserIpAddress;
            tr["[Event:Timestamp]"] =
                DefaultCultureManager.Instance.FormatDateTimeWithCurrentCulture(_eventSubmission.Timestamp,
                                                                                CS.General_v3.Util.Date.DATETIME_FORMAT.
                                                                                    FullDateShortTime);
            tr["[Event:Category]"] = _eventSubmission.Category.TitlePlural;
            URLClass eventSubmissionUrlClass = _eventSubmission.CmsFactory.GetEditUrlForItemWithID(_eventSubmission.ID);
            string cmsEventSubmissionUrl = eventSubmissionUrlClass != null ? eventSubmissionUrlClass.GetURL(true) : null;
            tr["[Admin:CmsLink]"] = cmsEventSubmissionUrl;
            tr["[Admin:AdminToName]"] = AdminEmailName;
            tr["[Event:FrontendURL]"] = _eventSubmission.GetEventUrl();
            return tr;
        }

        private void mail(string toName, string toEmail, string fromName, string fromEmail, string replyToName, string replyToEmail, Enum emailText, TokenReplacer tokenReplacer)
        {
            var emailDb = EmailTextBaseFactory.Instance.GetByIdentifier(emailText);
            var email = emailDb.GetEmailMessage(tokenReplacer);
            email.SetFromEmail(fromEmail, fromName);
            email.AddToEmails(toEmail, toName);
            email.SetReplyToEmail(replyToEmail, replyToName);

            email.SendAsynchronously();
        }
    }
}
