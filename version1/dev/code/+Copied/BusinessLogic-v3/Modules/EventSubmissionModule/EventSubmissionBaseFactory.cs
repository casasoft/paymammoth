using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CategoryModule;

namespace BusinessLogic_v3.Modules.EventSubmissionModule
{   

//BaseFactory-File

	public abstract class EventSubmissionBaseFactory : BusinessLogic_v3.Modules._AutoGen.EventSubmissionBaseFactoryAutoGen, IEventSubmissionBaseFactory
    {
		/// <summary>
		/// Returns all the categories applicable with events - IMPORTANT: A category node with identifier 'Events' must be set for this to work, otherwise an exception will be thrown!
		/// </summary>
		/// <returns></returns>
		public IEnumerable<ICategoryBase> GetEventCategories()
		{
		    CategoryBase rootNode = (CategoryBase) Factories.CategoryFactory.GetByIdentifier(Enums.CATEGORY_ROOT_NODES.Events,
		                                                                      throwErrorIfDoesNotExist: false);
            if(rootNode == null)
            {
                throw new InvalidOperationException("For this to work, a Category node with identifier <" +
                                                    Enums.CATEGORY_ROOT_NODES.Events +
                                                    "> must be specified! Its subchildren are the actual categories which will apply with the event.");
            }
		    return rootNode.GetCategoriesUnderThis();
		}

    }

}
