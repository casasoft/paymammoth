using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;


using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Modules.CategoryModule;

namespace BusinessLogic_v3.Modules.EventSubmissionModule
{

    //IBaseFactory-File

    public interface IEventSubmissionBaseFactory : BusinessLogic_v3.Modules._AutoGen.IEventSubmissionBaseFactoryAutoGen
    {
        /// <summary>
        /// Returns all the categories applicable with events - IMPORTANT: A category node with identifier 'Events' must be set for this to work, otherwise an exception will be thrown!
        /// </summary>
        /// <returns></returns>
        IEnumerable<ICategoryBase> GetEventCategories();
    }
}
