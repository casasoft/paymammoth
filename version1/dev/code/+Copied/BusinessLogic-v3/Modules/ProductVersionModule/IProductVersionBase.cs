using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductVersionMediaItemModule;

namespace BusinessLogic_v3.Modules.ProductVersionModule
{

//IBaseClass-File
	
    public interface IProductVersionBase : BusinessLogic_v3.Modules._AutoGen.IProductVersionBaseAutoGen
    {
        string GetTrialLocalPath();
        string GetDownloadLocalPath();
        IProductBase GetProduct();
        IEnumerable<IProductVersionMediaItemBase> GetAllImages();
        IProductVersionMediaItemBase GetMainImage();
    }
}
