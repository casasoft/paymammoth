using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.Article_CultureInfoModule
{

//BaseClassMap-File
    
    public abstract class Article_CultureInfoBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.Article_CultureInfoBaseMap_AutoGen<TData>
    	where TData : Article_CultureInfoBase
    {


        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }

        protected override void CultureInfoMappingInfo(ICollectionMapInfo mapInfo)
        {

            mapInfo.LuceneInfo.MappedInLucene = true;
            

            mapInfo.LuceneInfo.Index = NHibernate.Search.Attributes.Index.UnTokenized;
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
            base.CultureInfoMappingInfo(mapInfo);
        }

        protected override void PageTitleMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            mapInfo.LuceneInfo.IsMultilingual = true;
            
            base.PageTitleMappingInfo(mapInfo);
        }
        protected override void HtmlTextMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            mapInfo.LuceneInfo.IsMultilingual = true;
            base.HtmlTextMappingInfo(mapInfo);
        }

        protected override void _Computed_ParentArticleNamesMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            mapInfo.LuceneInfo.IsMultilingual = true;
            base._Computed_ParentArticleNamesMappingInfo(mapInfo);
        }

        protected override void ArticleMappingInfo(ICollectionMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            base.ArticleMappingInfo(mapInfo);
        }
    }
}
