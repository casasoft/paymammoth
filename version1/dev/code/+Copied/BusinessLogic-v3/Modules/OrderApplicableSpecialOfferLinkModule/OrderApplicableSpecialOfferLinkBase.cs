using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.SpecialOfferModule;

namespace BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule
{

//BaseClass-File
	
    public abstract class OrderApplicableSpecialOfferLinkBase : BusinessLogic_v3.Modules._AutoGen.OrderApplicableSpecialOfferLinkBaseAutoGen, IOrderApplicableSpecialOfferLinkBase
    {
    
		#region BaseClass-AutoGenerated
		#endregion
    
        public OrderApplicableSpecialOfferLinkBase()
        {
            
        }    
    	protected override void initPropertiesForNewItem()
        {
        	//Fill any default values for NEW items.
        
            base.initPropertiesForNewItem();
        }
    	public override string ToString()
        {
            return base.ToString();
        }

        public void SetSpecialOffer(ISpecialOfferBase sp)
        {
            if (sp is SpecialOfferVoucherCodeBase)
            {
                this.SpecialOfferVoucherCodeLink = (SpecialOfferVoucherCodeBase)sp;
            }
            else if (sp is SpecialOfferBase)
            {
                this.SpecialOfferLink = (SpecialOfferBase) sp;

            }
            else
            {
                throw new InvalidOperationException("sp must be either a special offer, or a voucher code");
            }
        }
        public ISpecialOfferBase GetSpecialOffer()
        {
            if (this.SpecialOfferVoucherCodeLink != null)
                return this.SpecialOfferVoucherCodeLink;
            else
                return this.SpecialOfferLink;

        }
    }
}
