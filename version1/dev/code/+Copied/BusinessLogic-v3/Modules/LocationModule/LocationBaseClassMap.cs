using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.LocationModule
{

//BaseClassMap-File
    
    public abstract class LocationBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.LocationBaseMap_AutoGen<TData>
    	where TData : LocationBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
        protected override void ChildLocationsMappingInfo(ICollectionMapInfo mapInfo)
        {
            mapInfo.CacheType = Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            base.ChildLocationsMappingInfo(mapInfo);
        }
 
    }
   
}
