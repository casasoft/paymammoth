using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.LocationModule
{   

//IBaseFactory-File

	public interface ILocationBaseFactory : BusinessLogic_v3.Modules._AutoGen.ILocationBaseFactoryAutoGen
    {

        IEnumerable<ILocationBase> GetAllLocationsByOrder(Enums.LOCATION_SORT_BY orderBy);
        IEnumerable<ILocationBase> GetAllLocationsByType(Enums.LOCATION_TYPE locationType, Enums.LOCATION_SORT_BY orderBy);


    }

}
