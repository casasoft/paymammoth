using System.Collections;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.LocationModule
{   

//BaseFactory-File

	public abstract class LocationBaseFactory : BusinessLogic_v3.Modules._AutoGen.LocationBaseFactoryAutoGen, ILocationBaseFactory
    {


        #region ILocationBaseDataFactory Members

        public IEnumerable<ILocationBase>  GetAllLocationsByOrder(Enums.LOCATION_SORT_BY orderBy) 
        {
            var query = GetQuery();
            query.Cacheable();
            query = orderItemsInRepository(orderBy, query);
            return FindAll(query);
        }

        public IEnumerable<ILocationBase> GetAllLocationsByType(Enums.LOCATION_TYPE locationType, Enums.LOCATION_SORT_BY orderBy) 
        {
            var query = GetQuery();
            query = orderItemsInRepository(orderBy, query);
            query.Cacheable();
            query = query.Where((locationItem) => locationItem.LocationType == locationType);
            return FindAll(query);
        }

        private NHibernate.IQueryOver<LocationBase, LocationBase>
            orderItemsInRepository(Enums.LOCATION_SORT_BY orderBy, NHibernate.IQueryOver<LocationBase, LocationBase> query)
        {
            switch (orderBy)
            {
                case Enums.LOCATION_SORT_BY.TitleAscending:
                    query = query.OrderBy((location) => location.Title).Asc;
                    break;
                case Enums.LOCATION_SORT_BY.TitleDescending:
                    query = query.OrderBy((location) => location.Title).Desc;
                    break;
                default:
                    throw new InvalidOperationException("Specified enumeration value is not in switch statement");
            }
            return query;
        }

        #endregion
        public LocationBase GetByReferenceID(string refID)
        {
            var q = GetQuery();
            q = q.Where(item => item.ReferenceID == refID);
            q.Cacheable();
            return FindItem(q);

        }


        
    }

}
