using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using log4net;

namespace BusinessLogic_v3.Modules.AuditLogModule
{   

//BaseFactory-File

	public abstract class AuditLogBaseFactory : BusinessLogic_v3.Modules._AutoGen.AuditLogBaseFactoryAutoGen, IAuditLogBaseFactory
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(AuditLogBaseFactory));
        public int DeleteExtraLogEntries()
        {
            int result = -1;
            _log.Debug("DeleteExtraLogEntries()");
            //using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
           // {
                result = _deleteExtraLogEntries();
            //    t.Commit();
            //}
            return result;

        }



        private int _deleteExtraLogEntries()
        {
            _log.Debug("_deleteExtraLogEntries() [START]");
            int result = -1;
            long itemIdToDeleteFrom = 0;
            try
            {

                int totalLogEntriesToKeep = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(Enums.BusinessLogicSettingsEnum.AuditLog_TotalLogEntriesToKeep);
                _log.Debug("Total log entries to keep: " + totalLogEntriesToKeep);
                var session = BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();

                BusinessLogic_v3.Util.nHibernateUtil.RepeatUntilNoStaleObjectExceptionIsRaised(null,
                    () =>
                    {
                        var q = AuditLogBase.Factory.GetQuery();
                        q.Skip(totalLogEntriesToKeep);
                        q.Take(1); //only need to take 1
                        q = q.OrderBy(x => x.ID).Desc;
                        var item = AuditLogBase.Factory.FindItem(q);
                        if (item != null)
                        {

                            itemIdToDeleteFrom = 0;
                            itemIdToDeleteFrom = item.ID;
                            string HQL = "Delete AuditLogImpl a where a.ID <= " + itemIdToDeleteFrom;
                            _log.Debug("Executing '" + HQL + "'");

                            var deleteQuery = session.CreateQuery(HQL);
                            result = deleteQuery.ExecuteUpdate();
                            _log.Debug("Finished executing delete statement - Result: " + result);
                               

                        }
                        else
                        {
                            _log.Debug("No item found, hence no audit log items to delete");
                        }
                        
                    }, maxRepeatTimes:15,
                        minWait: 750, maxWait: 3000);

                BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
            }
            catch (Exception ex)
            {
                _log.Error("Error while trying to delete extra log entries with ID <= " + itemIdToDeleteFrom, ex);
                CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Error, ex);
            }
            _log.Debug("_deleteExtraLogEntries() [FINISH]");
            return result;
        }

    }

}
