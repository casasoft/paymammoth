using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.AuditLogModule
{

//BaseClassMap-File
    
    public abstract class AuditLogBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.AuditLogBaseMap_AutoGen<TData>
    	where TData : AuditLogBase
    {


        protected override void PublishedOnMappingInfo(IPropertyMapInfo mappingInfo)
        {
            base.PublishedOnMappingInfo(mappingInfo);
        }
    }
   
}
