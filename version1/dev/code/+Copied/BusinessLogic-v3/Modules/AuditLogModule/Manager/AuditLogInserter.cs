﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Background;
using BusinessLogic_v3.Util;
using log4net;

namespace BusinessLogic_v3.Modules.AuditLogModule.Manager
{
    public class AuditLogInserter : BaseRecurringTask<AuditLogInserter>
    {
        //public static AuditLogInserter Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<AuditLogInserter>(); } }
        private static readonly ILog _log = LogManager.GetLogger(typeof(AuditLogManager));
        
        public AuditLogInserter() : base(Enums.BusinessLogicSettingsEnum.AuditLog_Insertor_InserterEveryXSeconds)
        {

            
        }

      

        private readonly object _recurringLock = new object();
        protected override void  recurringTask()
        {
            lock (_recurringLock)
            {
                int batchSize = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.AuditLog_Insertor_BatchSize);

                var list = AuditLogManager.Instance.GetAuditLogChangesToLog(batchSize).ToList();
                if (list.Count > 0)
                {
                    var session = BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();

                    using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            var logToAdd = list[i];
                            logToAdd.GenerateLogEntry();

                        }
                        t.Commit();
                    }
                    BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();

                    session = BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();

                    AuditLogBaseFactory.Instance.DeleteExtraLogEntries();
                    

                    BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();


                }
                else
                {
                    lock (_lock)
                    {
                        if (!AuditLogManager.Instance.HasChangesToLog())
                        {
                            StopIfStarted();
                            //Stop();
                        }
                    }
                }
            }
        }
    }
}
