﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.AuditLogModule.Manager
{
    public class RevertChangesResult
    {
        public enum CHANGE_STATUS
        {
            ItemNotFound,
            Success

        }
        public List<RevertChangeResult> ChangeResults { get; private set; }
        public RevertChangesResult()
        {
            this.Status = CHANGE_STATUS.Success;
            this.ChangeResults = new List<RevertChangeResult>();
        }
        public bool CheckIfAllWereSuccessful()
        {
            foreach (var r in this.ChangeResults)
            {
                if (!r.Success)
                    return false;
            }
            return true;
        }
        public CHANGE_STATUS Status { get; set; }

    }
}
