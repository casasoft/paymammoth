﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Modules.AuditLogModule.Manager
{
    public class AuditChangeToLog
    {
        public Enums.AUDITLOG_MSG_TYPE UpdateType { get; set; }
        public object Entity {get;set;}
        public object Id {get;set;}
        public object[] CurrentState {get;set;}
        public object[] PreviousState { get; set; }
        public string[] PropertyNames {get;set;}
        public DateTime DateTime { get; set; }
        public long CmsUserId { get; set; }
        public string CmsUserIpAddress { get; set; }
        public string Message { get; set; }

        public NHibernate.Type.IType[] NhTypes { get; set; }
        


        private AuditLogBase _auditLog = null;



        private string getPropertyValueFromObject(object o, NHibernate.Type.IType type)
        {
            string s = null;
            
            if ((type != null && type.IsCollectionType) || 
                (type == null && o is IEnumerable && !(o is string)))
            {

                { //dont log collections for now, it can be problematic due to session being closed (Karl - 08/feb/2012)
                    if (false)
                    {
                        StringBuilder sb = new StringBuilder();

                        IEnumerable enumerable = (IEnumerable) o;
                        foreach (object item in enumerable)
                        {
                            if (sb.Length > 0)
                                sb.Append("|");
                            string sItem = getPropertyValueFromObject(item, null);
                            sb.Append(sItem);
                        }
                        s = sb.ToString();
                    }
                }
            }
            else if ((type != null && type.IsEntityType) || (type == null && o is IBaseDbObject))
            {
                int k = 5;
                //dont log entities for now, it can be problematic due to session being closed, etc
               // IBaseDbObject dbObject = (IBaseDbObject)o;
                //s = dbObject.ID.ToString();
            }
            else
            {
                s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(o);
            }
            return s;
        }

        private void checkPropertyChanges()
        {
            bool atLeastOneChange = false;
            AuditLogChanges _changes = new AuditLogChanges();
            if (this.UpdateType == Enums.AUDITLOG_MSG_TYPE.Add || this.UpdateType == Enums.AUDITLOG_MSG_TYPE.Update)
            {

                for (int i = 0; i < this.PropertyNames.Length; i++)
                {
                    var type = this.NhTypes[i];
                    string pName = this.PropertyNames[i];
                    object oldValue = null;
                    if (this.PreviousState != null)
                        oldValue = this.PreviousState[i];
                    object newValue = this.CurrentState[i];

                    string sOldValue = getPropertyValueFromObject(oldValue, type);
                    string sNewValue = getPropertyValueFromObject(newValue, type);
                    _changes.AddChange(pName, sOldValue, sNewValue);
                }

                string xml = _changes.SerializeToXml();
                _auditLog.Changes = xml;
            }

        }


        private void updateItemInfo()
        {
            if (this.Entity != null)
            {
                _auditLog.ItemType = this.Entity.GetType().Name;
                _auditLog.ItemTypeFull = this.Entity.GetType().AssemblyQualifiedName;
            }
            if (this.Id is int || this.Id is long)
            {
                _auditLog.ItemID = Convert.ToInt64(this.Id);
            }
            _auditLog.Message = Message;
            _auditLog.DateTime = this.DateTime;
            if (this.CmsUserId >0)
            {
                _auditLog.CmsUser = BusinessLogic_v3.Modules.CmsUserModule.CmsUserBaseFactory.Instance.GetByPrimaryKey(this.CmsUserId);
                _auditLog.CmsUserIpAddress = this.CmsUserIpAddress;
            }
        }
        private void initAuditLog()
        {
            _auditLog = AuditLogBase.Factory.CreateNewItem();


            _auditLog.UpdateType = this.UpdateType;

        }

        private void save()
        {
            _auditLog.Create();
        }

        private void _generate()
        {
            
            initAuditLog();

            checkPropertyChanges();

            updateItemInfo();
            _auditLog.UpdateDetailsFromCurrentEnvironment();

            save();
               
           
        }

        public AuditLogBase GenerateLogEntry()
        {
            //CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(_generate, System.Threading.ThreadPriority.Lowest);
            _generate();
            return this._auditLog;


        }


        
    }
}
