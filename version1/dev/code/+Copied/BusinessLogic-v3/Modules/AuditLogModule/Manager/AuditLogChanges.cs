﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace BusinessLogic_v3.Modules.AuditLogModule.Manager
{
    public class AuditLogChanges
    {

        public List<AuditLogChange> Changes { get; set; }
        public AuditLogChanges()
        {
            this.Changes = new List<AuditLogChange>();
        }

        public void AddChange(string pName, string sOldValue, string sNewValue)
        {
            if (string.Compare(sOldValue, sNewValue, false) != 0)
            {
                AuditLogChange change = new AuditLogChange();
                change.PropertyName = pName;
                change.From = sOldValue;
                change.To = sNewValue;
                this.Changes.Add(change);
            }

        }

        public string SerializeToXml()
        {
            return CS.General_v3.Util.XML.SerializeObjectToXmlString(this);
            
        }
        public static AuditLogChanges DeSerializeFromXml(string xml)
        {
            return CS.General_v3.Util.XML.DeSerializeObjectFromXmlString<AuditLogChanges>(xml);
            
            
        }
        public override string ToString()
        {
            return "Total Changes: " + this.Changes.Count;
        }
    }
}
