﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Modules.SettingModule;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Util;
using NHibernate;
using log4net;
using System.Collections.Concurrent;
using BusinessLogic_v3.Classes.Cms;
using BusinessLogic_v3.Classes.Cms.System;

namespace BusinessLogic_v3.Modules.AuditLogModule.Manager
{
    public class AuditLogManager
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(AuditLogManager));
        public static AuditLogManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<AuditLogManager>(); } }


        public bool CustomEnabled { get; set; }

        //public int TotalLogEntriesToKeep { get; set; }
        public bool LogStackTrace { get; set; }
       

        public bool Enabled { get; set; }
        public bool LogOnlyCmsEntries { get; set; }
        private readonly object _settingLock = new object();
        public bool Started { get; private set; }


        private bool _updatingVariables = false;

        private ConcurrentQueue<AuditChangeToLog> _auditQueue = null;

        private AuditLogManager()
        {
            this.CustomEnabled = true;
            _auditQueue = new ConcurrentQueue<AuditChangeToLog>();
            SettingBaseFactory.Instance.OnItemUpdate += new Classes.DbObjects.General.OnItemUpdateHandler(Instance_OnItemUpdate);
        }

        void Instance_OnItemUpdate(Classes.DbObjects.Objects.IBaseDbObject item, Enums.UPDATE_TYPE updateType)
        {
            UpdateVariablesFromSettings();
        }

        public bool IsEnabled(object entity)
        {
           
            bool ok =this.Started && this.CustomEnabled && Enabled;
            if (ok && LogOnlyCmsEntries)
            {
                if (!BusinessLogic_v3.Classes.Cms.System.CmsSystemBase.Instance.CheckIfCurrentRequestInCms())
                {
                    ok = false;
                }
            }
            return ok;
        }

        public void StartLogging()
        {
            
            UpdateVariablesFromSettings();
            this.Started = true;
            //DeleteExtraLogEntries();
        }

       
        public void UpdateVariablesFromSettings()
        {
            lock (_settingLock)
            {
                if (!_updatingVariables)
                {
                    _updatingVariables = true;
                    this.Enabled = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<bool>(Enums.BusinessLogicSettingsEnum.AuditLog_Enabled);
                    this.LogOnlyCmsEntries = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<bool>(Enums.BusinessLogicSettingsEnum.AuditLog_LogOnlyCmsEntries);
                    this.LogStackTrace = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<bool>(Enums.BusinessLogicSettingsEnum.AuditLog_LogDetail_StackTrace);
                    _updatingVariables = false;
                }
            }
        }


        public void StopLogging()
        {
            this.Started = false;
        }
        public bool HasChangesToLog()
        {
            return this._auditQueue.Count>0;
        }

        public IEnumerable<AuditChangeToLog> GetAuditLogChangesToLog(int total)
        {
            List<AuditChangeToLog> list = new List<AuditChangeToLog>();
            for (int i=0; i< total;i++)
            {
                AuditChangeToLog log = null;
                bool ok = this._auditQueue.TryDequeue(out log);
                if (ok)
                {
                    list.Add(log);

                }
                else
                {
                    break;
                }
            }
            return list;
        }

        protected bool validateEntity(object entity)
        {
            bool ok = true;
            if (entity is AuditLogBase || entity is SettingBase)
            {
                ok = false;
            }
            return ok;
        }
        protected bool validateEntityForAdd(object entity)
        {
            bool ok = validateEntity(entity);
            if (ok && entity is SettingBase)
            {
                ok = false;
            }
            return ok;
        }
        protected bool validateEntityForUpdate(object entity)
        {
            bool ok = validateEntity(entity);
            return ok;
        }
        protected bool validateEntityForDelete(object entity)
        {
            bool ok = validateEntity(entity);
            return ok;
        }

        private void addEntry(Enums.AUDITLOG_MSG_TYPE msgType, object entity, object id, object[] currentState, object[] previousState,
            string[] propertyNames, NHibernate.Type.IType[] types, string Message, bool fromCms)
        {
            bool inCms = false;
            AuditChangeToLog log = new AuditChangeToLog();
            log.UpdateType = msgType;
            log.Entity = entity;
            log.Id = id;
            log.CurrentState = currentState;
            log.PreviousState= previousState;
            log.PropertyNames = propertyNames;
            log.Message = Message;
            log.NhTypes = types;
            log.DateTime = CS.General_v3.Util.Date.Now;
            
            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext()&& fromCms && CmsUserModule.CmsUserSessionLogin.Instance.IsLoggedIn && CmsSystemBase.Instance.CheckIfCurrentRequestInCms())
            {
                inCms = true;
                log.CmsUserId = CmsUserModule.CmsUserSessionLogin.Instance.CurrentSessionLoggedInUserID;
                log.CmsUserIpAddress = CS.General_v3.Util.PageUtil.GetUserIP();
            }
            if (inCms || !CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.AuditLog_LogOnlyCmsEntries))
            {//log if in cms, or not only log cms entries
                _auditQueue.Enqueue(log);
                AuditLogInserter.Instance.StartIfNotStarted();
            }

            


        }

        public void LogAdd(object entity, object id, object[] currentState, string[] propertyNames, NHibernate.Type.IType[] types)
        {
            if (IsEnabled(entity) && validateEntityForAdd(entity))
            {
                addEntry(Enums.AUDITLOG_MSG_TYPE.Add, entity, id, currentState, null, propertyNames, types, null, fromCms: true);

             
            }
            
        }

        public void LogChange(object entity, object id, object[] currentState, object[] previousState, string[] propertyNames, NHibernate.Type.IType[] types)
        {
            if (IsEnabled(entity) && validateEntityForUpdate(entity))
            {
                addEntry(Enums.AUDITLOG_MSG_TYPE.Update, entity, id, currentState, previousState, propertyNames, types, null, fromCms: true);
            }
            

        }
      

        public void LogDelete(object entity, object id, object[] state, string[] propertyNames, NHibernate.Type.IType[] types)
        {
            if (IsEnabled(entity) && validateEntityForDelete(entity))
            {
                addEntry(Enums.AUDITLOG_MSG_TYPE.Delete, entity, id, state, null, propertyNames, types, null, fromCms: true);
            }
        }
        

        public void AddAuditLog(Enums.AUDITLOG_MSG_TYPE msgType, IBaseDbObject relatedItem, string msg, bool fromCms = true)
        {
            long? id= null;
            if (relatedItem !=null)
                id = relatedItem.ID;

            addEntry(msgType, relatedItem, id, null, null, null, null, msg, fromCms);



        }


        

    }
}
