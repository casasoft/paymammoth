﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Modules.AuditLogModule.Manager
{
    public class AuditLogReverter
    {
        

        private Type itemType = null;
        private IBaseDbObject dbObject = null;
        private AuditLogChanges _changes = null;
        public AuditLogBase AuditLog { get; private set; }
        public AuditLogReverter(AuditLogBase auditLog)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(auditLog, "Audit Log is required");
            this.AuditLog = auditLog;
        }
        private void loadInfo()
        {
            itemType = Type.GetType(AuditLog.ItemTypeFull);
            _changes = AuditLogChanges.DeSerializeFromXml(this.AuditLog.Changes);

            var factory = BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryForType(itemType);
            this.dbObject = factory.GetByPrimaryKey(this.AuditLog.ItemID);

        }

        private void revertChanges()
        {
            using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                for (int i = 0; i < _changes.Changes.Count; i++)
                {
                    var change = _changes.Changes[i];
                    _results.ChangeResults.Add(change.RevertChange(dbObject));

                }
                t.Commit();
            }
        }

        private RevertChangesResult _results = null;

        public RevertChangesResult RevertChanges()
        {
            if (this.AuditLog.UpdateType != Enums.AUDITLOG_MSG_TYPE.Update)
            {
                throw new InvalidOperationException("Cannot revert an audit-log that is not an Update");
            }
            _results = new RevertChangesResult();
            
            loadInfo();
            if (this.dbObject != null)
            {
                revertChanges();
            }
            else
            {
                _results.Status = RevertChangesResult.CHANGE_STATUS.ItemNotFound;
                
            }
            return _results;
        }

    }
}
