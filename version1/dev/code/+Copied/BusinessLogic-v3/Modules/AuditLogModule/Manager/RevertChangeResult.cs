﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.AuditLogModule.Manager
{
    public class RevertChangeResult
    {
        public AuditLogChange Change { get; set; }
        public bool Success { get; set; }
        public Exception Exception { get; set; }
    }
}
