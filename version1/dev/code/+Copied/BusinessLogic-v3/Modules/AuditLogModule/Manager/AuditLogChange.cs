﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using BusinessLogic_v3.Classes.DbObjects.Collections;

namespace BusinessLogic_v3.Modules.AuditLogModule.Manager
{
    public class AuditLogChange
    {
        public string PropertyName { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public override string ToString()
        {
            return this.PropertyName + " - From: " + this.From + " To: " + this.To;
        }

        private void updateOldValueToItem(PropertyInfo pInfo,BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject dbObject)
        {
            Type pType = pInfo.PropertyType;
            if (CS.General_v3.Util.ReflectionUtil.CheckIfTypeExtendsFrom(pType, typeof(BusinessLogic_v3.Classes.DbObjects.Collections.ICollectionManager<>)))
            {//this means that pType is a collection
                ICollectionManager currentValue = (ICollectionManager) pInfo.GetValue(dbObject, null);
                currentValue.Clear();

                var fromIds = CS.General_v3.Util.Text.Split(this.From, "|", ",");
                foreach (var fromId in fromIds)
                {
                    long id = 0;
                    if (long.TryParse(fromId, out id))
                    {
                        currentValue.AddByPrimaryKey(id);
                    }
                }
            }
            else //it should be a basic data type
            {
                object value = CS.General_v3.Util.Other.ConvertStringToBasicDataType(this.From, pType);

                pInfo.SetValue(dbObject, value, null);
            }

        }


        public RevertChangeResult RevertChange(BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject dbObject)
        {
            RevertChangeResult result = new RevertChangeResult();
            result.Success = false;
            try
            {
                Type t = dbObject.GetType();
                var pInfo = t.GetProperty(this.PropertyName);

                if (pInfo != null)
                {
                    updateOldValueToItem(pInfo, dbObject);
                    result.Success = true;
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Exception = ex;
            }
            return result;
        }
    }
}
