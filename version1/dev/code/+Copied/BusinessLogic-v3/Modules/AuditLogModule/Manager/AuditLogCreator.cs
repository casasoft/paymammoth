﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Util;
using NHibernate.Type;

using System.Collections;
using System.Diagnostics;
using log4net;

namespace BusinessLogic_v3.Modules.AuditLogModule.Manager
{
    public class AuditLogCreator
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(AuditLogCreator));
        public object Item { get; private set; }
        public object Id { get; private set; }
        public object[] CurrentState { get; private set; }
        public object[] PrevState { get; private set; }
        public string[] PropertyNames { get; private set; }
        public IType[] Types { get; private set; }
        public Enums.AUDITLOG_MSG_TYPE UpdateType { get; private set; }

        public AuditLogCreator(Enums.AUDITLOG_MSG_TYPE updateType, object entity, object id, object[] currentState, object[] previousState, string[] propertyNames, NHibernate.Type.IType[] types)
        {
            this.UpdateType = updateType;
            this.Item = entity;
            this.Id = id;
            this.CurrentState = currentState;
            this.PrevState = previousState;
            this.PropertyNames = propertyNames;
            this.Types = types;
            

        }

        private AuditLogBase _auditLog = null;
        

       
        private string getPropertyValueFromObject(object o)
        {
            string s = null;
            if (o is IEnumerable && !(o is string))
            {
                
                { //dont log collections for now, it can be problematic due to session being closed (Karl - 08/feb/2012)
                    StringBuilder sb = new StringBuilder();

                    IEnumerable enumerable = (IEnumerable) o;
                    foreach (object item in enumerable)
                    {
                        if (sb.Length > 0)
                            sb.Append("|");
                        string sItem = getPropertyValueFromObject(item);
                        sb.Append(sItem);
                    }
                    s = sb.ToString();
                }
            }
            else if (o is IBaseDbObject)
            {
                IBaseDbObject dbObject = (IBaseDbObject)o;
                s = dbObject.ID.ToString();
            }
            else
            {
                s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(o);
            }
            return s;
        }

        private void checkPropertyChanges()
        {
            AuditLogChanges _changes = new AuditLogChanges();
            if (this.UpdateType == Enums.AUDITLOG_MSG_TYPE.Add || this.UpdateType == Enums.AUDITLOG_MSG_TYPE.Update)
            {

                for (int i = 0; i < this.PropertyNames.Length; i++)
                {
                    string pName = this.PropertyNames[i];
                    object oldValue = null;
                    if (this.PrevState != null)
                        oldValue = this.PrevState[i];
                    object newValue = this.CurrentState[i];

                    string sOldValue = getPropertyValueFromObject(oldValue);
                    string sNewValue = getPropertyValueFromObject(newValue);
                    _changes.AddChange(pName, sOldValue, sNewValue);
                }

                string xml = _changes.SerializeToXml();
                _auditLog.Changes = xml;
            }

        }

        
        private void updateItemInfo()
        {
            
            _auditLog.ItemType = this.Item.GetType().Name;
            _auditLog.ItemTypeFull = this.Item.GetType().AssemblyQualifiedName;
            if (this.Id is int || this.Id is long)
            {
                _auditLog.ItemID =Convert.ToInt64(this.Id);
            }
        }
        private void initAuditLog()
        {
            _auditLog = AuditLogBase.Factory.CreateNewItem();
            
            
            _auditLog.UpdateType = this.UpdateType;
            
        }

        private void save()
        {
            _auditLog.Create();
        }

        private void _generate()
        {
            MyNHSessionBase session = null;
            try
            {
                session = nHibernateUtil.CreateNewSessionForCurrentContext();
                using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                {
                    initAuditLog();
                    
                    checkPropertyChanges();
                    
                    updateItemInfo();
                    _auditLog.UpdateDetailsFromCurrentEnvironment();

                    save();
                    t.Commit();
                }

            }
            catch (Exception ex)
            {
                _log.Error("Error occured while trying to log audit delete", ex);

            }
            finally
            {
                if (session != null)
                {
                    nHibernateUtil.DisposeCurrentSessionInContext();
                }
            }
        }

        public void GenerateLogEntry()
        {
            //CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(_generate, System.Threading.ThreadPriority.Lowest);
            _generate();
                
         
        }


    }
}
