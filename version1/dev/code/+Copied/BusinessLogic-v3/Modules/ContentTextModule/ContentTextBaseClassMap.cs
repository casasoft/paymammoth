using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.ContentTextModule
{

//BaseClassMap-File
    
    public abstract class ContentTextBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.ContentTextBaseMap_AutoGen<TData>
    	where TData : ContentTextBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }

        protected override void ContentText_CultureInfosMappingInfo(ICollectionMapInfo mapInfo)
        {
            mapInfo.FetchType = Enums.NHIBERNATE_COLLECTION_FETCHMODE.Join;
            mapInfo.CacheType = Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            base.ContentText_CultureInfosMappingInfo(mapInfo);
        }
        protected override void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            mapInfo.Length = 250;
            base.IdentifierMappingInfo(mapInfo);
        }
        
 
    }
   
}
