using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Classes.Text;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.ContentTextModule
{

//IBaseClass-File

    public interface IContentTextBase : BusinessLogic_v3.Modules._AutoGen.IContentTextBaseAutoGen, IItemWithContentTags
    {
        
        TokenReplacerWithContentTagItem CreateTokenReplacer();
        TokenReplacerNew CreateTokenReplacerNew();
        string GetContent(Dictionary<string, string> replacementTags = null, bool parseLinks = true);
        string _Content_ForEditing { get; set; }
    }
}
