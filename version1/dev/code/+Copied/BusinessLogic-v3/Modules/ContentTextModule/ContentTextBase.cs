using System.Text.RegularExpressions;
using BusinessLogic_v3.Classes.General;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Modules.ArticleModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.ContentTextModule
{
    using CS.General_v3.JavaScript.Data;
    using DefaultValues;
using BusinessLogic_v3.Classes.Hierarchies;
    using BusinessLogic_v3.Modules.CultureDetailsModule;

//BaseClass-File

    public abstract class ContentTextBase : BusinessLogic_v3.Modules._AutoGen.ContentTextBaseAutoGen, IContentTextBase,
        IItemWithIdentifier, IContentTextDefaultValues, IHierarchyDbItem, IItemWithContentTags
    {
    
		#region BaseClass-AutoGenerated
		#endregion
    

    	protected override void initPropertiesForNewItem()
        {
            
        	//Fill any default values for NEW items.
            this.UsedInProject = true;
            base.initPropertiesForNewItem();
        }

        private HierarchyDbItem hierarchyDbItemFunctionality = null;


        protected override void Delete_After(OperationResult result, DeleteBeforeAfterParams delParams)
        {
            base.Delete_After(result, delParams);
        }

        #region ContentImage Media Item Snippet (v3)

        // ContentImage : This should be the name of the image, e.g MainPhoto, etc. 
        // ContentTextBase : This should be the name of the class that is implementing this snippet, e.g EventBase or Member

        public enum ContentImageSizingEnum
        {

            [ImageSpecificSizeDefaultValues(Width = 99999, Height = 99999, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None)]
            Normal
        }

        public class ContentImageMediaItem : BaseMediaItemImage<ContentImageSizingEnum>
        {
            /* If you need to update SetAsMainItem, Priority, etc, these are fields/methods that can be overriden in the base class */


            public ContentTextBase Item { get; private set; }
            public ContentImageMediaItem(ContentTextBase item)
            {

                this.Item = item;

                MediaItemImageInfo imageInfo = new MediaItemImageInfo();
                imageInfo.InitByDb(this.Item, typeof(ContentImageSizingEnum),
                                        p => p.Content,  //this should be the filename field
                                        () => item.Name,  //this should be the title field.  Note that this is optional.  This is only used to generate the filename from, to include the title for Google Images purposes 
                                        "ContentText - ContentImage"); //this should be the identifier for this image, and what will as the title in the Cms/Sizing Info, so make it nice and understandable :)

                imageInfo.DeleteDbItemOnRemove = false; //if you want that on delete of this image, the item is deleted, set this as true.

                this.MediaItemInfo = imageInfo;

            }



        }

        protected ContentImageMediaItem _m_ContentImage = null;
        public ContentImageMediaItem ContentImage { get { if (_m_ContentImage == null) _m_ContentImage = new ContentImageMediaItem(this); return _m_ContentImage; } }



        #endregion
			
        public void MarkAsUsedInProject()
        {
            if (!this.UsedInProject)
            {
                this.UsedInProject = true;
                this.Update();
                if (this.Parent != null)
                {
                    this.Parent.MarkAsUsedInProject();
                }

            }

        }




        public static void RemoveAllIHierarchyItemsWithoutValidParentFromDatabase()
        {

            var list = ContentTextBase.Factory.FindAll();

            Util.HierarchyUtil.RemoveAllIHierarchyItemsWithoutValidParentFromDatabase(list.Cast<IItemHierarchy>());

        }
        public class JAVASCRIPT_OBJECT_CONVERSIONS
        {
            private ContentTextBase _ct;
            public JAVASCRIPT_OBJECT_CONVERSIONS(ContentTextBase ct)
            {
                _ct = ct;
            }

            public JSObjectFlash GetJsObjectFlash()
            {
                return GetJsObjectFlash(0, 0);
            }
            public JSObjectFlash GetJsObjectFlash(int maxDepth)
            {
                return GetJsObjectFlash(maxDepth, 0);
            }
            public JSObjectFlash GetJsObjectFlash(int maxDepth, int currDepth)
            {
                return (JSObjectFlash)GetJsObject(true, maxDepth, currDepth);
            }
            public JSObject GetJsObject()
            {
                return GetJsObject(false, 0, 0);
            }
            public JSObject GetJsObject(int maxDepth)
            {
                return GetJsObject(false, 0, 0);
            }
            public JSObject GetJsObject(int maxDepth, int currDepth)
            {
                return GetJsObject(false, maxDepth, currDepth);
            }
            public virtual JSObject GetJsObject(bool forFlash, int maxDepth, int currDepth)
            {
                JSObject obj = forFlash ? new JSObjectFlash() : new JSObject();
                obj.AddProperty("title", CS.General_v3.Util.Text.UrlEncode(_ct.Name)); ;
                obj.AddProperty("plainText", CS.General_v3.Util.Text.UrlEncode(_ct.GetContent())); ;
                obj.AddProperty("id", _ct.ID);

                obj.AddProperty("url", "");

                JSArray aChildren = forFlash ? new JSArrayFlash() : new JSArray();


                return obj;
            }
            public JSObject GetJsObjectWithHtml(bool forFlash = false, int maxDepth = 0, int currDepth = 0)
            {
                JSObject obj = GetJsObject(forFlash, maxDepth, currDepth);
                obj.AddProperty("html", _ct.GetContent());
                return obj;
            }
            public JSObject GetJsObjectFlashWithHtml(int maxDepth = 0, int currDepth = 0)
            {
                var obj = GetJsObject(true, maxDepth, currDepth);
                obj.AddProperty("html", _ct.GetContent());
                return obj;
            }


        }
        public JAVASCRIPT_OBJECT_CONVERSIONS JavascriptObjectConversions { get; private set; }


        public ContentTextBase()
        {

            this.JavascriptObjectConversions = new JAVASCRIPT_OBJECT_CONVERSIONS(this);
            this.hierarchyDbItemFunctionality = new HierarchyDbItem(this);
        }



        public override string ToString()
        {
            string s = "";
            if (this.Parent != null)
                s = this.Parent.ToString() + " > ";
            s += this.Name;
            return s;

        }
        private bool checkIfSameParent()
        {

            if (this.ID > 0 && this.Parent != null && this.Parent.ID == this.ID)
            {
                return true;
            }
            return false;
        }
        protected override void Save_Before(OperationResult result, SaveBeforeAfterParams sParams)
        {
            if (checkIfSameParent())
            {
                result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Cannot set parent to the same as itself");
            }
            checkContentType();
            base.Save_Before(result, sParams);
        }

        private void checkContentType()
        {
            string value = this.GetContent();
            this._Computed_IsHtml = CS.General_v3.Util.HtmlUtil.IsHtml(value);
        }


        public void UpdateMultilingualIDForThisAndAllChildren(int? multiLingualID)
        {
            this.SetCurrentCultureInfoDBId(multiLingualID);

            foreach (var item in this.ChildContentTexts)
            {
                if (item != null)
                {
                    item.UpdateMultilingualIDForThisAndAllChildren(multiLingualID);
                }
            }
        }
        public void ResetMultilingualIDForThisAndAllChildren()
        {
            this.SetCurrentCultureInfoDBId((ICultureDetailsBase)null);
            foreach (var item in this.ChildContentTexts)
            {
                if (item != null)
                {
                    item.ResetMultilingualIDForThisAndAllChildren();
                }
            }

        }


        public bool CheckIfItemContentMatchesKeywords(string keywords)
        {
            //keywords = CS.General_v3.Util.Text.ConvertStringForSearch(keywords);
            string[] tokens = keywords.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            List<string> dataToSearchOn = new List<string>();
            dataToSearchOn.Add(this.Name_Search);
            dataToSearchOn.Add(this.Identifier);
            dataToSearchOn.Add(this.Name);
            foreach (var langInfo in this.ContentText_CultureInfos)
            {
                dataToSearchOn.Add(langInfo.Content);

            }
            bool ok = true;
            for (int i = 0; i < tokens.Length; i++)
            {
                string keyword = tokens[i];
                string keywordForSearch = CS.General_v3.Util.Text.ConvertStringForSearch(keyword);
                bool keywordOk = false;
                foreach (var searchField in dataToSearchOn)
                {
                    if (!string.IsNullOrEmpty(searchField))
                    {
                        if (Regex.IsMatch(searchField, keyword, RegexOptions.IgnoreCase) ||
                            Regex.IsMatch(searchField, keywordForSearch, RegexOptions.IgnoreCase))
                        {
                            keywordOk = true;
                            break;
                        }
                    }
                }
                if (!keywordOk)
                {
                    ok = false;
                    break;
                }
            }
            return ok;
        }

        /// <summary>
        /// Checks if this item, and its children match the keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public bool CheckIfItemAndChildrenMatchesKeywords(string keywords)
        {
            bool ok = true;
            if (!string.IsNullOrEmpty(keywords))
            {
                var item = this;

                foreach (var child in item.ChildContentTexts.ToList())
                {
                    if (!child.CheckIfItemAndChildrenMatchesKeywords(keywords))
                    {
                        item.ChildContentTexts.Remove(child);
                    }
                }
                ok = (item.ChildContentTexts.Count> 0);

                if (!ok && item.ChildContentTexts.Count == 0)
                {
                    if (item.CheckIfItemContentMatchesKeywords(keywords))
                    {
                        ok = true;
                    }

                }
            }
            return ok;
        }
        //public string GetHTMLOrPlainText()
        //{
        //    return Content;
        //    /*
        //    if (!string.IsNullOrWhiteSpace(this.ContentHTML))
        //        return this.ContentHTML;
        //    else
        //        return this.ContentPlain;*/
        //}
        //public string GetPlainOrHtmlText()
        //{
        //    return Content;
        //    /*
        //    if (!string.IsNullOrWhiteSpace(this.ContentPlain))
        //        return this.ContentPlain;
        //    else
        //        return this.ContentHTML;*/
        //}
        private string _parsedContent = null;
        public string GetContent(Dictionary<string, string> replacementTags = null, bool parseLinks = true)
        {
            string result = null;
            //if (_parent == null)
            {
                if (replacementTags != null)
                {
                    result = BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(this, replacementTags);
                }
                else
                {
                    result = this.Content;
                }

                if (parseLinks)
                {
                    if (_parsedContent == null)
                    {
                        ArticleLinkTextParser parser = new ArticleLinkTextParser();
                        result = parser.ParseText(result);
                        _parsedContent = result;
                    }
                    if (result == null)
                    {
                        result = _parsedContent;
                    }
                }
                

            }
            return result;
        }


        string IItemWithContentTags.GetItemContent()
        {

            return GetContent();
        }

     









        #region ICmsHierarchyItem Members


        #endregion





        #region IHierarchyDbItem Members

        IEnumerable<IHierarchyDbItem> IHierarchyDbItem.GetParents()
        {
            return CS.General_v3.Util.ListUtil.GetListFromSingleItem(this.Parent);
        }

        IEnumerable<IHierarchyDbItem> IHierarchyDbItem.GetChildren()
        {
            return this.ChildContentTexts;
            
        }

        #endregion

        #region IItemWithContentTags Members

        string[] IItemWithContentTags.CustomContentTags
        {
            get
            {
                return IItemWithContentTagsExtensions.GetCustomContentTagsFromString(this.CustomContentTags);
            }
            set
            {
                this.CustomContentTags = CS.General_v3.Util.Text.AppendStrings(", ", value);
            }
        }

        #endregion

        #region IContentTextBase Members

        public Classes.Text.TokenReplacerWithContentTagItem CreateTokenReplacer()
        {
            return new Classes.Text.TokenReplacerWithContentTagItem(this);
            
        }

        #endregion


        /*public string GetContent()
        {
            return this.Content;
        }*/

        #region IContentTextBase Members


        public Classes.Text.TokenReplacerNew CreateTokenReplacerNew()
        {
            return new Classes.Text.TokenReplacerNew(this);
            
        }

        #endregion

        #region IContentTextBase Members

       
        
        string IContentTextBase.GetContent(Dictionary<string, string> replacementTags = null,bool parseLinks = true)
        {
            return this.GetContent(replacementTags: replacementTags,parseLinks: parseLinks);
            
        }

        #endregion

        #region IContentTextBase Members
        [Obsolete("this is only used for inline-editing")]
        public string _Content_ForEditing
        {
            get
            {
                return this.Content;
                
            }
            set
            {
                this.Content = value;
                
            }
        }

        #endregion
    }
}
