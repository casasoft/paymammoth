﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.General;

namespace BusinessLogic_v3.Modules.ContentTextModule
{
    public class ContentTextBaseCacheManager : ItemsByIdentifierCache<ContentTextBase>
    {
        private ContentTextBaseCacheManager() : base(ContentTextBase.Factory)
        {
            
        }
        public static ContentTextBaseCacheManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<ContentTextBaseCacheManager>(); } }

    }
}
