﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Modules.ContentTextModule.DefaultValues
{
    public class DefaultContentTextCreator : IContentTextDefaultValues
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public Enums.CONTENT_TEXT_TYPE ContentType { get; set; }

        public string CustomContentTags {get;set;}
        public string FullIdentifier { get; set; }
        public DefaultContentTextCreator(string identifier)
        {
            this.ContentType = Enums.CONTENT_TEXT_TYPE.PlainText;
            this.FullIdentifier = identifier;

            int lastUnderscore = identifier.LastIndexOf('_');
            int lastDashPos = identifier.LastIndexOf('-');
            if (lastDashPos == -1) lastDashPos = identifier.Length;
            
            this.Name = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(identifier.Substring(lastUnderscore + 1, lastDashPos - lastUnderscore-1));
            

        }
        public void FillFromDefaultValues(IContentTextDefaultValues values)
        {
            if (values != null)
            {

                CS.General_v3.Util.ReflectionUtil.CopyProperties<IContentTextDefaultValues>(values, this);

            }

        }
        public ContentTextBase CreateDefaultContentText(ContentTextBase parentToAddTo)
        {
            

            ContentTextBase result = null;
            //if (ArticleType == Enums.ARTICLE_TYPE.None) throw new InvalidOperationException("Cannot create a default article without specifying the article type");
            var identifiers = CS.General_v3.Util.Text.Split(FullIdentifier, "_");
            if (string.IsNullOrWhiteSpace(this.Name))
            {
                this.Name = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(identifiers.LastOrDefault());
            }

            string currID = null;


            ContentTextBase parent = parentToAddTo;
            
            for (int i = 0; i < identifiers.Count; i++)
            {
                bool isLastIdentifier = i == identifiers.Count - 1;
                if (i > 0)
                    currID += "_";
                currID += identifiers[i];
                var newItem = ContentTextBaseFactory.Instance._getContentTextByIdentifierFromDb(currID);
                if (newItem == null)
                {
                    //if (i < identifiers.Count - 1) // not last item
                    //{
                    //    DefaultArticleCreator creator = new DefaultArticleCreator(currID, this.ArticleType);
                    //    creator.FillFromDefaultValues(this);
                    //    newItem = creator.CreateDefaultArticle(parent);
                    //}
                    //else
                    { //last item, thus create
                        newItem = null;
                            
                        var session = nHibernateUtil.GetCurrentSessionFromContext();
                        using (var t = nHibernateUtil.BeginTransactionIfNotOneAlreadyActive())
                        {
                            newItem = ContentTextBaseFactory.Instance.CreateNewItem();
                            if (parent != null)
                            {

                                //Article_ParentChildLinkBase link = Article_ParentChildLinkBase.Factory.CreateNewItem();
                                //link.Child = newItem;
                                //link.Parent = parent;
                                //newItem.ParentArticleLinks.Add(link);
                                //parent.ChildArticleLinks.Add(link);
                                newItem.Parent = parent;
                            }
                            


                            newItem.ContentType = this.ContentType;
                            newItem.Identifier = currID;
                            /* 2012-01-12 Removed by Mark as images will now be HTML
                             * if (this.ContentType == Enums.CONTENT_TEXT_TYPE.Image)
                            {
                                if (!string.IsNullOrEmpty(this.Content))
                                {
                                    newItem.ContentImage.UploadFileFromLocalPath(this.Content);
                                }
                            }
                            else
                            {*/
                                if (!string.IsNullOrEmpty(this.Content) && (i == identifiers.Count-1)) //if content filled in, and last one
                                {
                                    newItem.Content = this.Content;
                                }
                                else
                                {
                                    //Get from Identifier
                                    string content = identifiers[i];
                                    //if (currID.IndexOf("_") != -1) content = content.Substring(content.LastIndexOf("_") + 1);
                                    content = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(content);
                                    newItem.Content = content;
                                }
                            //}
                            newItem.ContentType = this.ContentType;
                            newItem.Name = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(identifiers[i]);
                            if (isLastIdentifier)
                            {
                                newItem.CustomContentTags = this.CustomContentTags;
                            }
                            if (i == identifiers.Count - 1)
                            { //copy all properties

                                //CS.General_v3.Util.ReflectionUtil.CopyProperties<IContentTextDefaultValues>(this, newItem);
                            }
                            //if (parent != null)
                            //{
                            //    parent.Update();
                            //}
                            //session.Save(newItem);
                            newItem.Create();


                          //  NHibernate.ITransaction t2 = session.BeginTransaction();
                            if (t != null)
                            {
                                t.Commit();
                            }
                            //  t2.Commit();
                        }
                        

                    }


                }
                parent = newItem;

            }
            result = parent;
            return result;


        }
       

        #region IArticleDefaultValues Members


        public CS.General_v3.Enums.CMS_ACCESS_TYPE AccessTypeRequiredToDelete { get; set; }

        #endregion



    }
}
