﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DefaultValues;

namespace BusinessLogic_v3.Modules.ContentTextModule.DefaultValues
{

   
    public class ContentTextDefaultValuesAttribute : BaseContentDefaultValueAttribute, IContentTextDefaultValues
    {

        public string Content
        {
            get;
            set;
        }
        private Enums.CONTENT_TEXT_TYPE? _contentType = null;
        public Enums.CONTENT_TEXT_TYPE ContentType
        {
         get
            {

                if (_contentType.HasValue)
                {
                    return _contentType.Value;
                }
                else
                {
                    if (!string.IsNullOrEmpty(this.Content) && CS.General_v3.Util.HtmlUtil.IsHtml(this.Content))
                    {
                        return Enums.CONTENT_TEXT_TYPE.Html;
                    }
                    else
                    {
                        return Enums.CONTENT_TEXT_TYPE.PlainText;
                    }
                }
            }
            set
            {
                _contentType = value;
            }
        }

        public string CustomContentTags { get; set; }

        


        public ContentTextDefaultValuesAttribute()
        {
           // this.ContentType = Enums.CONTENT_TEXT_TYPE.PlainText;
        }
    }
}
