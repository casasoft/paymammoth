﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.ContentTextModule.DefaultValues
{
    public interface IContentTextDefaultValues
    {
        string Content { get; set; }
        Enums.CONTENT_TEXT_TYPE ContentType { get; set; }
        string CustomContentTags { get; set; }
    }
}
