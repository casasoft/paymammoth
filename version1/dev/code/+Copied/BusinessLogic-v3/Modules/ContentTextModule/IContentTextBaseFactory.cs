using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Frontend.ContentTextModule;

namespace BusinessLogic_v3.Modules.ContentTextModule
{

    //IBaseFactory-File
    /// <summary>
    /// A handler which returns a content text enum, mapped from each enum
    /// </summary>
    /// <param name="enumValue"></param>
    /// <returns></returns>
    public delegate IContentTextBase EnumToContentTextHandler<TEnum>(TEnum enumValue);
    public interface IContentTextBaseFactory : BusinessLogic_v3.Modules._AutoGen.IContentTextBaseFactoryAutoGen
    {
        
        IContentTextBase GetContentTextByStringIdentifier(string identifier, bool createIfNotExists = true);
        IContentTextBase GetContentTextByGenericIdentifier(Enum identifier, bool createIfNotExists = true);
        IContentTextBase GetRootContentText();

        //IContentTextBase GetContentTextFromEnumValue<TEnum>(TEnum value, EnumToContentTextHandler<TEnum> enumToContentTextEnumHandler) where TEnum : struct;
        //IEnumerable<IContentTextBase> GetContentTextListFromEnumType<TEnum>(EnumToContentTextHandler<TEnum> enumToContentTextEnumHandler, CS.General_v3.Enums.ENUM_SORT_BY sortBy = CS.General_v3.Enums.ENUM_SORT_BY.NameAscending) where TEnum : struct;
        //ListItemCollection GetContentTextListItemCollectionByEnumType<TEnum>(EnumToContentTextHandler<TEnum> enumToContentTextEnumHandler, CS.General_v3.Enums.ENUM_SORT_BY sortBy = CS.General_v3.Enums.ENUM_SORT_BY.NameAscending) where TEnum : struct;
        IContentTextBase GetContentTextForEnumValue(Type enumType, Enum enumValue);
        IContentTextBase GetContentTextForEnumValue<TEnum>(TEnum enumValue) where TEnum : struct;
        ListItemCollection GetContentTextListItemCollectionByEnumType<TEnum>(CS.General_v3.Enums.ENUM_SORT_BY sortBy = CS.General_v3.Enums.ENUM_SORT_BY.NameAscending) where TEnum : struct;

        /*
        ListItemCollection GetContentTextListItemCollectionByIdentifierEnum(Enum identifierPrefix,
                                                                                         Type enumerationType,
                                                                                         CS.General_v3.Enums.ENUM_SORT_BY
                                                                                             sortBy =
                                                                                             CS.General_v3.Enums.
	                                                                                         ENUM_SORT_BY.NameAscending);
        ListItemCollection GetContentTextListItemCollectionByIdentifierEnum(string identifierPrefix,
                                                                                         Type enumerationType,
                                                                                         CS.General_v3.Enums.ENUM_SORT_BY
                                                                                             sortBy =
                                                                                             CS.General_v3.Enums.
	                                                                                         ENUM_SORT_BY.NameAscending);

        ContentTextBase GetContentTextOfEnum(Enum identifierPrefix, Enum enumValue);

        ContentTextBase GetContentTextOfEnum(string identifierPrefix, Enum enumValue);*/
    }

}
