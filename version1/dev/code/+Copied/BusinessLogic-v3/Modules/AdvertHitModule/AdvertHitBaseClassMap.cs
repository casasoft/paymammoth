using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.AdvertHitModule
{

//BaseClassMap-File
    
    public abstract class AdvertHitBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.AdvertHitBaseMap_AutoGen<TData>
    	where TData : AdvertHitBase
    {
        protected override void LastEditedOnMappingInfo(IPropertyMapInfo mappingInfo)
        {
            
            base.LastEditedOnMappingInfo(mappingInfo);
        }
 
    }
   
}
