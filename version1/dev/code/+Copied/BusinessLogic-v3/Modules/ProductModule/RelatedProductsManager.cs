﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Classes.Searches.ProductSearcher;
using Iesi.Collections.Generic;
using CS.General_v3.Util;
using BusinessLogic_v3.Modules.ProductVariationModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Util;
using BusinessLogic_v3.Modules.SettingModule;
using BusinessLogic_v3.Modules.ProductVariationColourModule;

namespace BusinessLogic_v3.Modules.ProductModule
{
    public class RelatedProductsManager
    {
        private ProductBase _product { get; set; }

        public RelatedProductsManager(ProductBase product)
        {
            if(product == null)
            {
                throw new InvalidOperationException(
                    "Related Products Manager must always be initialised with a non-null ProductBase Object!");
            }
            this._product = product;
        }

        private int totalToLoad = 0;
        private List<ProductBase> relatedProductsResult = null;
        /// <summary>
        /// Preference is given to specified related products, if none found, products with the same variation colour are retrieved, if none found, products from the same category, else a random product is retrieved.
        /// </summary>
        /// <param name="numberToLoad">The maximum amount of products to return.</param>
        /// <returns>Randomised list of related products.</returns>
        public IEnumerable<ProductBase> GetRelatedProducts(int numberToLoad)
        {
            this.relatedProductsResult = new List<ProductBase>();
            this.totalToLoad = numberToLoad;
            loadFromManuallySet();
            if(this.relatedProductsResult.Count < totalToLoad)
            {
                loadFromOtherProductsWithSameColours();
            }
            if (this.relatedProductsResult.Count < totalToLoad)
            {
                loadFromOtherProductsWithSameMaterial();
            }
            if (this.relatedProductsResult.Count < totalToLoad)
            {
                loadFromSameCategory();
            }
            if (this.relatedProductsResult.Count < totalToLoad)
            {
                loadRandomProducts();
            }
            return relatedProductsResult;
        }

        /// <summary>
        /// Retrieve a random set of related products which have been manually set from the CMS.
        /// </summary>
        /// <param name="numberToLoad">The maximum amount of products to return.</param>
        /// <returns>List of related products.</returns>
        internal void loadFromManuallySet()
        {
            int remainingToLoad = (this.totalToLoad - relatedProductsResult.Count);
            List<ProductBase> result = _product.getRelatedProductsFromRelatedProductsLink();
            result.RandomizeEnumerable();
            relatedProductsResult.AddRange(result.Take(remainingToLoad));
            
        }

        /// <summary>
        /// Retrieve a random set of related products based on the criteria of having the same colour.
        /// </summary>
        /// <param name="numberToLoad">The maximum amount of products to return.</param>
        /// <returns>List of related products.</returns>
        internal void loadFromOtherProductsWithSameColours()
        {
            
            if (Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.Products_RelatedProducts_GetRelatedBasedOnColour))
            {
                int remainingToLoad = (this.totalToLoad - relatedProductsResult.Count);
            
                var colours = _product.GetAllColours().ToList().ConvertAll(x=>x.ID);
                var query = ProductBaseFactory.Instance.GetQuery();
                query = query.Where(x => x.ID != this._product.ID);
                
                query.RootCriteria.AddOrder(BusinessLogic_v3.Util.nHibernateUtil.GetRandomOrder());
                BusinessLogic_v3.Util.nHibernateUtil.LimitQueryByPrimaryKeys(query, 1, remainingToLoad);
                
                var qo = query.JoinQueryOver<ProductVariationBase>(item => item.ProductVariations);
                qo.WhereRestrictionOn(x => x.Colour).IsInG(colours);

                //var qo_col = qo.JoinQueryOver<ProductVariationColourBase>(item => item.Colour);
                
                //var queryDisjunction = Restrictions.Disjunction();

                //foreach (string colour in colours)
                //{
                //    queryDisjunction.Add(NHibernate.Criterion.Restrictions.On<ProductVariationColourBase>(item => item.Title)
                //                             .IsLike(colour, MatchMode.Anywhere));
                //}
                //qo_col.Where(queryDisjunction);
                IEnumerable<ProductBase> result = ProductBaseFactory.Instance.FindAll(query);

                relatedProductsResult.AddRange(result);
                //return result.ToList();
            }
            //return null;
        }
        /// <summary>
        /// Retrieve a random set of related products based on the criteria of having the same colour.
        /// </summary>
        /// <param name="numberToLoad">The maximum amount of products to return.</param>
        /// <returns>List of related products.</returns>
        internal void loadFromOtherProductsWithSameMaterial()
        {

            if (Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.Products_RelatedProducts_GetRelatedBasedOnColour))
            {
                int remainingToLoad = (this.totalToLoad - relatedProductsResult.Count);
                var query = ProductBaseFactory.Instance.GetQuery();
                var query_Culture = query.Left.JoinQueryOver<Product_CultureInfoModule.Product_CultureInfoBase>(x => x.Product_CultureInfos).Where(x=>x.Material == _product.Material);
                
                //query = query.Where(x => x.Material == _product.Material);
                query.RootCriteria.AddOrder(BusinessLogic_v3.Util.nHibernateUtil.GetRandomOrder());

                BusinessLogic_v3.Util.nHibernateUtil.LimitQueryByPrimaryKeys(query, 1, remainingToLoad);
                
                
                IEnumerable<ProductBase> result = ProductBaseFactory.Instance.FindAll(query);

                relatedProductsResult.AddRange(result);
                //return result.ToList();
            }
            //return null;
        }
        /// <summary>
        /// Retrieve a random set of related products based on the criteria of being under the same category.
        /// </summary>
        /// <param name="numberToLoad">The maximum amount of products to return.</param>
        /// <returns>List of related products.</returns>
        internal void loadFromSameCategory()
        {
            int remainingToLoad = (this.totalToLoad - relatedProductsResult.Count);
            
            IEnumerable<ICategoryBase> cats = _product.GetCategories();
            ProductSearchParams sparams = ProductSearchParams.CreateLastTypeInstance();
            HashedSet<ICategoryBase> categories = new HashedSet<ICategoryBase>();
            HashedSet<IProductBase> productsToExclude = new HashedSet<IProductBase>();
            productsToExclude.Add(_product);

            categories.AddAll(cats.ToList());
            sparams.Categories = categories;
            sparams.RandomOrder = true;
            sparams.PageNo = 1;
            sparams.ShowAmount = remainingToLoad;
            sparams.ExcludeProducts = productsToExclude;

            ProductSearcher searcher = ProductSearcher.CreateLastTypeInstance();
            searcher.ProductSearchParameters = sparams;
            relatedProductsResult.AddRange(searcher.GetSearchResults().GetResults().ToList().ConvertAll(item=> (ProductBase) item));
        }

        /// <summary>
        /// Get a random set of products from the Database without any specific link whatsoever.
        /// </summary>
        /// <param name="numberToLoad">The maximum amount of products to return.</param>
        /// <returns>List of random products.</returns>
        internal void loadRandomProducts()
        {
            int remainingToLoad = (this.totalToLoad - relatedProductsResult.Count);
            
            var q = ProductBaseFactory.Instance.GetQuery();
            q.Where(item => item.ID != _product.ID);
            q.RootCriteria.AddOrder(nHibernateUtil.GetRandomOrder());
            q.Take(remainingToLoad);

            
            //nHibernateUtil.LimitCriteriaByPrimaryKeys(q.RootCriteria, 1, numberToLoad);
            var list = ProductBaseFactory.Instance.FindAll(q).ToList();
            relatedProductsResult.AddRange(list);
        }
    }
}
