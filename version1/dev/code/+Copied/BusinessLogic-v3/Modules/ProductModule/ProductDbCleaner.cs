﻿using System.Linq;
using BusinessLogic_v3.Modules.AuditLogModule.Manager;

namespace BusinessLogic_v3.Modules.ProductModule
{
    public class ProductDbCleaner
    {

        public bool DeletePermanently { get; set; }

        private void removeProductPermanently(ProductBase p)
        {
            {
                var q = ShoppingCartItemModule.ShoppingCartItemBaseFactory.Instance.GetQuery(new Classes.DbObjects.Parameters.GetQueryParams(loadTempItems:true, loadDelItems:true, loadNonPublishedItems: true));
                q = q.WhereRestrictionOn(x => x.LinkedProductVariation).IsInG<long>(p.ProductVariations.ToList().ConvertAll(x=>x.ID));
                var list = ShoppingCartItemModule.ShoppingCartItemBaseFactory.Instance.FindAll(q);
                foreach (var item in list)
                {
                    item.Delete(new Classes.DbObjects.Parameters.DeleteParams(delPermanently: DeletePermanently));
                }

            }

            p.Delete(new Classes.DbObjects.Parameters.DeleteParams(delPermanently: DeletePermanently));
        }

        private bool removeProductPage()
        {
            bool removedProducts = false;
            var q = ProductBase.Factory.GetQuery(new Classes.DbObjects.Parameters.GetQueryParams(loadDelItems: DeletePermanently, loadTempItems: true, loadNonPublishedItems: true));
            q.Take(100);
            var list = ProductBase.Factory.FindAll(q).ToList();
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                    {
                        var p = list[i];
                        removeProductPermanently(p);
                        t.Commit();    
                    }
                        
                }
                removedProducts = true;
            }

            return removedProducts;

        }
        private void removeProducts()
        {
            bool continueResult = false;
            do
            {
                continueResult = removeProductPage();
            } while (continueResult);
            
        }

        public void CleanUpDb()
        {
            AuditLogManager.Instance.AddAuditLog(Enums.AUDITLOG_MSG_TYPE.Information, null, "Products cleaned from database", true);
            removeProducts();
        }

    }
}
