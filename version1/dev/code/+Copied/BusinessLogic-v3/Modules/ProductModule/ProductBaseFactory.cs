using BusinessLogic_v3.Classes.URL.ListingURLParser;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Util;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using BusinessLogic_v3.Classes.Searches.ProductSearcher;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.HelperClasses;

namespace BusinessLogic_v3.Modules.ProductModule
{

    //BaseFactory-File

    public abstract class ProductBaseFactory : BusinessLogic_v3.Modules._AutoGen.ProductBaseFactoryAutoGen, IProductBaseFactory
    {

        public ProductBaseFactory()
        {

        }

        public void RestrictCriteriaForFrontend(ICriteria crit)
        {
            crit.Add(Restrictions.Where<ProductBase>(item => item.Published));

        }


        public IEnumerable<IProductBase> GetCurrentSpecialOffers(int? Limit = null, bool Randomise = true)
        {
            var query = GetQuery();
            query = query.Where((itemGroup) => itemGroup.IsSpecialOffer == true);
            if (Randomise)
            {
                query.RootCriteria.AddOrder(nHibernateUtil.GetRandomOrder());
            }
            int limit = 999999;
            if (Limit.HasValue)
            {
                limit = Limit.Value;

            }
            RestrictCriteriaForFrontend(query.RootCriteria);
            return nHibernateUtil.LimitQueryByPrimaryKeysAndReturnResult(query, 1, limit);

        }

        #region IProductBaseFactory Members


        public IEnumerable<IProductBase> LoadProductsByIDs(List<long> itemGroupIDList)
        {
            var query = GetQuery();
            query = query.WhereRestrictionOn((item) => item.ID).IsInG<long>(itemGroupIDList);
            return FindAll(query);
        }

        #endregion

        #region IProductBaseFactory Members


        public IEnumerable<IProductBase> LoadItemsGroups(ProductSearchParams itemGroupSearchParams, out int totalResults)
        {
            ProductSearcher itemGroupSearcher = ProductSearcher.CreateLastTypeInstance();
            itemGroupSearcher.ProductSearchParameters = itemGroupSearchParams;
            var itemGroups = itemGroupSearcher.GetSearchResults();
            totalResults = itemGroupSearcher.TotalResults;
            return itemGroups;
        }

        #endregion

        #region IProductBaseFactory Members

        IProductBase IProductBaseFactory.GetItemByReferenceCode(string refCode)
        {
            var query = GetQuery();
            query = query.Where((itemGroup) => itemGroup.ReferenceCode == refCode);
            return this.FindItem(query);
        }

        #endregion

        #region IProductBaseFactory Members


        public IEnumerable<ProductBase> GetItemsByBrandAndCategory(BrandBase brand, CategoryBase category)
        {
            var q = GetQuery();
            q.Where(s => s.Brand.ID == brand.ID);
            var catList = category.GetAllCategoryIDsUnderThis();
            q.JoinQueryOver<ProductCategoryBase>(s => s.CategoryLinks).WhereRestrictionOn(s => s.Category).IsInG<long>(catList.ToList());


            return FindAll(q);
        }

        #endregion

        #region IProductBaseFactory Members

        IEnumerable<IProductBase> IProductBaseFactory.GetItemsByBrandAndCategory(IBrandBase brand, ICategoryBase category)
        {
            return this.GetItemsByBrandAndCategory((BrandBase)brand, (CategoryBase)category);

        }

        /// <summary>
        /// This method shall return all products associated with this category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public SearchPagedResults<IProductBase> GetItemsByCategory(ICategoryBase category, int pageNo, int pageSize, Enums.ITEM_GROUP_SORT_BY? sortBy)
        {
            ProductSearchParams sparams = ProductSearchParams.CreateLastTypeInstance();
            HashedSet<ICategoryBase> categories = new HashedSet<ICategoryBase>();
            categories.Add(category);
            sparams.Categories = categories;

            sparams.PageNo = pageNo;
            sparams.ShowAmount = pageSize;
            if (sortBy.HasValue)
            {
                sparams.SortBy = sortBy.Value;
            }

            ProductSearcher searcher = ProductSearcher.CreateLastTypeInstance();
            searcher.ProductSearchParameters = sparams;
            return searcher.GetSearchResults();
        }

        /// <summary>
        /// This method shall return all products associated with this keyword
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public SearchPagedResults<IProductBase> GetItemsByKeywords(string keywords, int pageNo, int pageSize, Enums.ITEM_GROUP_SORT_BY? sortBy)
        {
            ProductSearchParams sparams = ProductSearchParams.CreateLastTypeInstance();

            sparams.Keywords = keywords;

            sparams.PageNo = pageNo;
            sparams.ShowAmount = pageSize;
            if (sortBy.HasValue)
            {
                sparams.SortBy = sortBy.Value;
            }

            ProductSearcher searcher = ProductSearcher.CreateLastTypeInstance();
            searcher.ProductSearchParameters = sparams;
            return searcher.GetSearchResults();
        }
        #endregion

        #region IProductBaseFactory Members

        public IEnumerable<IProductBase> GetRandomFeaturedItems(int itemsToShow, bool loadRandomIfNothingIsFeatured = false)
        {
            var q = GetQuery();
            q = q.Where(item => item.IsFeatured);
            nHibernateUtil.LimitCriteriaByPrimaryKeys(q.RootCriteria, 1, itemsToShow);
            q.RootCriteria.AddOrder(nHibernateUtil.GetRandomOrder());
            IEnumerable<IProductBase> results = FindAll(q);
            if(results.Count() == 0 && loadRandomIfNothingIsFeatured)
            {
                q = GetQuery();
                nHibernateUtil.LimitCriteriaByPrimaryKeys(q.RootCriteria, 1, itemsToShow);
                q.RootCriteria.AddOrder(nHibernateUtil.GetRandomOrder());
                results = FindAll(q);
            }
            return results;
        }

        #endregion



        public ProductBase GetProductByReferenceCode(string refCode)
        {
            var q = GetQuery();
            q = q.Where(x => x.ReferenceCode == refCode);
            return FindItem(q);
            
        }
    }
}
