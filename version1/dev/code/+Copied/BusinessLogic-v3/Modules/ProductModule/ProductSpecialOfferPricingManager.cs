﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Background;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using log4net;

namespace BusinessLogic_v3.Modules.ProductModule
{
    public class ProductSpecialOfferPricingManager : BaseRecurringTask<ProductSpecialOfferPricingManager>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ProductSpecialOfferPricingManager));
        protected ProductSpecialOfferPricingManager() : base(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Products_Pricing_PriceUpdateIntervalInSecs)
        {

        }

        public static ProductSpecialOfferPricingManager Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<ProductSpecialOfferPricingManager>(); }
        }

        
        public void UpdatePricing()
        {
            
            SpecialOfferProductPricingUpdater updater = new SpecialOfferProductPricingUpdater();
            updater.UpdatePricing();
        }

        protected override void recurringTask()
        {

            UpdatePricing();
        }
    }
}
