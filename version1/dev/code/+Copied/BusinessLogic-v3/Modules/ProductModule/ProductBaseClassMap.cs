using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings.Cascades;

namespace BusinessLogic_v3.Modules.ProductModule
{

//BaseClassMap-File
    
    public abstract class ProductBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.ProductBaseMap_AutoGen<TData>
    	where TData : ProductBase
    {

        protected override void IsSpecialOfferMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            base.IsSpecialOfferMappingInfo(mapInfo);
        }

        protected override void BrandMappingInfo(ICollectionMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            base.BrandMappingInfo(mapInfo);
        }

        //protected override void ProductVariationsMappingInfo(ICollectionMapInfo mapInfo)
        //{
            

        //    base.ProductVariationsMappingInfo(mapInfo);
        //    mapInfo.CascadeType.SetAll();

        //    mapInfo.Inverse = false;

        //}
        //protected override void ProductCategoryFeatureValuesMappingInfo(ICollectionMapInfo mapInfo)
        //{
        //    base.ProductCategoryFeatureValuesMappingInfo(mapInfo);
        //    mapInfo.CascadeType.SetAll(); mapInfo.Inverse = false;
        //}
        //protected override void ProductCategorySpecificationValuesMappingInfo(ICollectionMapInfo mapInfo)
        //{
        //    base.ProductCategorySpecificationValuesMappingInfo(mapInfo);
        //    mapInfo.CascadeType.SetAll();
        //    mapInfo.Inverse = false;
        //}
        protected override void Product_CultureInfosMappingInfo(ICollectionMapInfo mapInfo)
        {
            base.Product_CultureInfosMappingInfo(mapInfo);
            mapInfo.CascadeType.SetAll();
            mapInfo.Inverse = false;
        }

        

    }
   
}
