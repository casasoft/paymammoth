using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Searches.ProductSearcher;
using BusinessLogic_v3.Classes.URL.ListingURLParser;
using CS.General_v3.Classes.HelperClasses;

namespace BusinessLogic_v3.Modules.ProductModule
{   

//IBaseFactory-File

	public interface IProductBaseFactory : BusinessLogic_v3.Modules._AutoGen.IProductBaseFactoryAutoGen
    {

        IEnumerable<IProductBase> GetCurrentSpecialOffers(int? Limit = null, bool Randomise = true);
        IEnumerable<IProductBase> LoadItemsGroups(ProductSearchParams itemGroupSearchParams, out int totalResults);
        IEnumerable<IProductBase> LoadProductsByIDs(List<long> itemGroupIDList);

        IProductBase GetItemByReferenceCode(string refCode);

        IEnumerable<IProductBase> GetItemsByBrandAndCategory(IBrandBase brand, ICategoryBase category);

        SearchPagedResults<IProductBase> GetItemsByCategory(ICategoryBase category, int pageNo, int pageSize, Enums.ITEM_GROUP_SORT_BY? sortBy);
        SearchPagedResults<IProductBase> GetItemsByKeywords(string keywords, int pageNo, int pageSize, Enums.ITEM_GROUP_SORT_BY? sortBy);

        IEnumerable<IProductBase> GetRandomFeaturedItems(int itemsToShow, bool loadRandomIfNothingIsFeatured = false);
     }

}
