using BusinessLogic_v3.Modules.ProductVariationColourModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Classes.Pricing;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Modules.ProductVersionModule;

namespace BusinessLogic_v3.Modules.ProductModule
{

//IBaseClass-File
	
    public interface IProductBase : BusinessLogic_v3.Modules._AutoGen.IProductBaseAutoGen, BusinessLogic_v3.Modules.SpecialOfferModule.Helpers.ISpecialOfferCalculationItem
    {
        string GetFrontendUrl();

        IProductVariationMediaItemBase GetMainImage();
        IEnumerable<ICategoryBase> GetCategories();
        void RemoveCategoryWhoseParentIs(CategoryModule.ICategoryBase fromCategory);
        IEnumerable<IProductVariationMediaItemBase> GetAllImages(bool includeMainImage = true);
        void AddCategory(CategoryModule.ICategoryBase toCategory);
        //double GetPriceExclTaxPerUnit();
        //double GetTaxAmountPerUnit();
        bool HasDiscountPrice();
        //double GetPrice();
        //double GetPriceBeforeDiscountIncTax();
        IPricingInfo PriceInfo { get; }
        RelatedProductsManager RelatedProductsManager { get; }
        string GetTitle();
        IEnumerable<IProductVariationColourBase> GetAllColours();
        /// <summary>
        /// Returns the total discount, from the 'manual' inputted discount, i.e Price Discoutned
        /// </summary>
        /// <returns></returns>
        double GetDiscountManual();
        IProductVersionBase GetProductLatestVersion();
        IEnumerable<IProductVersionBase> GetProductLatestVersions(int numberOfProductsToGet, int pageNo, out int TotalResults);
        string GetDescription(bool requiredAsHtml);
        string GetShortDescription(bool convertFullDescriptionToPlainTextIfShortDescriptionIsEmpty, bool requiredAsHtml);
    }
}
