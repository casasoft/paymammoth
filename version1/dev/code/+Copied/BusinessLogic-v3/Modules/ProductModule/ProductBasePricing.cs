﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Pricing;
using CS.General_v3.Util;
using BusinessLogic_v3.Modules.SpecialOfferModule;

namespace BusinessLogic_v3.Modules.ProductModule
{
    public class ProductBasePricing : PricingInfo
    {
        protected ProductBasePricing() : base()
        {

        }
        public static ProductBasePricing CreateInstance(ProductBase p)
        {
            ProductBasePricing priceInfo = CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<ProductBasePricing>();
            priceInfo._product = p;
            return priceInfo;

        }
        protected ProductBase _product = null;

        public override double RetailPriceBeforeDiscount
        {
            get
            {
                return Math.Round(_product.PriceRetailIncTaxPerUnit, Constants.General.NUMBER_PRECISION_FINANCIAL);
            }
            set
            {
                base.RetailPriceBeforeDiscount = value;
            }
        }


        //public override double PriceExcTax
        //{
        //    get
        //    {
        //        return Math.Round(FinancialUtil.GetPriceExcTaxFromPriceIncTax(_product.PriceRetailIncTaxPerUnit, _product.GetTaxRateForProduct()), Constants.General.NUMBER_PRECISION_FINANCIAL) ;
        //    }
        //    set
        //    {
                
        //    }
        //}
        protected virtual void addAnyAdditionalDiscounts(SpecialOfferCalculator calc)
        {

        }

        private double getProductDiscount()
        {
            var spCalculator = SpecialOfferCalculator.GetSpecialOfferCalculatorFromContext();
            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext() && BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.IsLoggedIn)
            {
                var loggedInUser = BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.GetLoggedInUser();
                if (loggedInUser != null && loggedInUser.ApplicableSpecialOfferVoucherCode != null && loggedInUser.ApplicableSpecialOfferVoucherCode.IsCurrentlyAvailable())
                {
                    spCalculator.SpecialOffers.Add(loggedInUser.ApplicableSpecialOfferVoucherCode.SpecialOffer);
                }
            }
            addAnyAdditionalDiscounts(spCalculator);

            var specialOfferPrice = spCalculator.CalculatePercentageDiscountForProduct(this._product);

            double totalDiscount = specialOfferPrice;
            double discountManual = _product.GetDiscountManual();
            if (discountManual > totalDiscount)
            {// exclusive, if the manual discount is better, that is taken
                totalDiscount = discountManual;
            }
            //totalDiscount += _product.GetDiscountManual();
            
            return totalDiscount;
            


            //double discount = Math.Round(_product.PriceRetailIncTaxPerUnit - _product._ActualPrice, Constants.General.NUMBER_PRECISION_FINANCIAL);
            //return discount;
        }

        public override double Discount
        {
            get
            {
                return getProductDiscount();
               
            }
            set
            {
               
            }
        }
        public override double TaxRate
        {
            get
            {
                return _product.GetTaxRateForProduct();
                
            }
            set
            {
                base.TaxRate = value;
            }
        }

        //public override double TaxAmount
        //{
        //    get
        //    {
        //        return Math.Round(FinancialUtil.GetTaxOnlyAmountFromPriceIncTax(this.GetUnitPrice( _product.PriceDiscounted .PriceRetailIncTaxPerUnit, _product.GetTaxRateForProduct()), Constants.General.NUMBER_PRECISION_FINANCIAL);
                
        //    }
        //    set
        //    {
                
        //    }
        //}

    }


}
