using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.EventModule
{

    //IBaseFactory-File

    public interface IEventBaseFactory : BusinessLogic_v3.Modules._AutoGen.IEventBaseFactoryAutoGen
    {
        /// <summary>
        /// Returns all the events which would be active on the date supplied.
        /// </summary>
        /// <param name="date">Date on which active events should be loaded.</param>
        /// <returns></returns>
        IEnumerable<IEventBase> GetEventsActiveOnDate(DateTime date);

        /// <summary>
        /// Returns all the events which are active within a datetime range.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        IEnumerable<IEventBase> GetEventsActiveInTimePeriod(DateTime startDate, DateTime endDate);
    }
}
