using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules.TestimonialModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.EventMediaItemModule;
using BusinessLogic_v3.Modules.EventSessionPeriodDayModule;
using BusinessLogic_v3.Modules.EventSubmissionModule;

namespace BusinessLogic_v3.Modules.EventModule
{

//IBaseClass-File

    public interface IEventBase : BusinessLogic_v3.Modules._AutoGen.IEventBaseAutoGen
    {
        IMediaItemImage<EventBase.ImageSizingEnum> Image { get; }
        IEnumerable<IEventMediaItemBase> GetMediaItems();

        IEnumerable<ITestimonialBase> GetTestimonials(int totalAmount);

        /// <summary>
        /// This method shall return tutors related to this event
        /// </summary>
        /// <returns></returns>
        IEnumerable<IMemberBase> GetEventTutors();

        /// <summary>
        /// This method shall return the next upcoming date for the event (event session upcoming date)
        /// </summary>
        /// <returns></returns>
        DateTime? GetNextEventSessionDate();

        string GetShortDescription(bool convertFullDescriptionToPlainTextIfShortDescriptionIsEmpty);

        IEnumerable<IEventSessionPeriodDayBase> GetEventSessionDays();
    }
}
