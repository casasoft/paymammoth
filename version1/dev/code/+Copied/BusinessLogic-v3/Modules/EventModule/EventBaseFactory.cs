using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.EventModule
{   

//BaseFactory-File

	public abstract class EventBaseFactory : BusinessLogic_v3.Modules._AutoGen.EventBaseFactoryAutoGen, IEventBaseFactory
    {
        /// <summary>
        /// Returns all the events which would be active on the datetime supplied.
        /// </summary>
        /// <param name="date">Date on which active events should be loaded.</param>
        /// <returns></returns>
		public IEnumerable<IEventBase> GetEventsActiveOnDate(DateTime date)
		{
		    var q = GetQuery();
		    q.Where(item => item.EndDate >= date.GetStartOfDay() && item.StartDate <= date.GetEndOfDay());
            q = q.OrderBy(item => item.StartDate).Asc;
            return FindAll(q);
		}

        /// <summary>
        /// Returns all the events which are active within a datetime range.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public IEnumerable<IEventBase> GetEventsActiveInTimePeriod(DateTime startDate, DateTime endDate)
        {
            var q = GetQuery();
            q.Where(item => item.StartDate <= endDate && item.EndDate >= startDate);
            q = q.OrderBy(item => item.StartDate).Asc;
            return FindAll(q);
        }
    }
}
