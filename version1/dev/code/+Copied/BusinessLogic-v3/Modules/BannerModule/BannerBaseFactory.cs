using BusinessLogic_v3.Util;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using NHibernate.Criterion.Lambda;
using BusinessLogic_v3.Modules.CultureDetailsModule;

namespace BusinessLogic_v3.Modules.BannerModule
{   

//BaseFactory-File

	public abstract class BannerBaseFactory : BusinessLogic_v3.Modules._AutoGen.BannerBaseFactoryAutoGen, IBannerBaseFactory
    {

        #region IBannerBaseDataFactory Members

        public IEnumerable<IBannerBase> GetBannersByIdentifier(string identifier, Enums.BANNERS_SORT_BY sortBy)
        {
            var query = this.GetQuery();
            query = query.Where((bannerItem) => bannerItem.Identifier == identifier);
            IQueryOverOrderBuilder<BannerBase, BannerBase> orderBuilder = null;

            switch (sortBy)
            {
                case Enums.BANNERS_SORT_BY.PriorityAscending:
                case Enums.BANNERS_SORT_BY.PriorityDescending:
                    //var test = query.OrderBy(bannerItem => bannerItem.Priority);
                    orderBuilder = query.OrderBy(bannerItem => bannerItem.Priority);
                    break;
                case Enums.BANNERS_SORT_BY.TitleAscending:
                case Enums.BANNERS_SORT_BY.TitleDescending:
                    orderBuilder = query.OrderBy((bannerItem) => bannerItem.Title);
                    break;
            }

            switch (sortBy)
            {
                case Enums.BANNERS_SORT_BY.PriorityDescending:
                case Enums.BANNERS_SORT_BY.TitleDescending:
                    query = orderBuilder.Desc;
                    break;
            }

            return FindAll(query);
        }

        public IEnumerable<IBannerBase> GetBannersByCulture(Enums.BANNERS_SORT_BY sortBy, ICultureDetailsBase culture = null)
        {
            CultureDetailsBase cul = (CultureDetailsBase) culture;
            if(cul == null)
            {
                cul = (CultureDetailsBase) BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture();
            }
            var query = this.GetQuery();

            IQueryOverOrderBuilder<BannerBase, BannerBase> orderBuilder = null;
            CultureDetailsBase _cultureDetailsQueryAlias = null;
            query.JoinAlias(x => x.Cultures, () => _cultureDetailsQueryAlias, NHibernate.SqlCommand.JoinType.LeftOuterJoin);
            var disjunction = Restrictions.Disjunction();
            disjunction.Add(Restrictions.On<BannerBase>(c => c.Cultures).IsEmpty);
            disjunction.Add(Restrictions.Where(() => _cultureDetailsQueryAlias.ID == cul.ID));
            query.Where(disjunction);
            
            switch (sortBy)
            {
                case Enums.BANNERS_SORT_BY.PriorityAscending:
                case Enums.BANNERS_SORT_BY.PriorityDescending:
                    //var test = query.OrderBy(bannerItem => bannerItem.Priority);
                    orderBuilder = query.OrderBy(bannerItem => bannerItem.Priority);
                    break;
                case Enums.BANNERS_SORT_BY.TitleAscending:
                case Enums.BANNERS_SORT_BY.TitleDescending:
                    orderBuilder = query.OrderBy((bannerItem) => bannerItem.Title);
                    break;
            }

            switch (sortBy)
            {
                case Enums.BANNERS_SORT_BY.PriorityDescending:
                case Enums.BANNERS_SORT_BY.TitleDescending:
                    query = orderBuilder.Desc;
                    break;
            }

            return FindAll(query);
        }

        #endregion






        #region IBannerBaseDataFactory Members
        public IBannerBase GetRandomBanner()
        {
            var q = GetQuery();
            q.RootCriteria.AddOrder(nHibernateUtil.GetRandomOrder());
            return nHibernateUtil.LimitQueryByPrimaryKeysAndReturnResult(q, 1, 1).FirstOrDefault();
            
        }

        public IEnumerable<IBannerBase> GetAllBannersSortedBy(Enums.BANNERS_SORT_BY sortBy) 
        {
            var q = GetQuery();
            switch (sortBy)
            {
                case Enums.BANNERS_SORT_BY.PriorityAscending:
                    q = q.OrderBy(item => item.Priority).Asc;
                    break;
                case Enums.BANNERS_SORT_BY.PriorityDescending:
                    q = q.OrderBy(item => item.Priority).Desc;
                    break;
                case Enums.BANNERS_SORT_BY.TitleAscending:
                    q = q.OrderBy(item => item.Title).Asc;
                    break;
                case Enums.BANNERS_SORT_BY.TitleDescending:
                    q = q.OrderBy(item => item.Title).Desc;
                    break;
                default:
                    throw new Exception("Sort by value not implemented");
            }
            return FindAll(q);
        }

        #endregion
		

    }

}
