using BusinessLogic_v3.Classes.MediaItems;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.BannerModule
{

//IBaseClass-File
	
    public interface IBannerBase : BusinessLogic_v3.Modules._AutoGen.IBannerBaseAutoGen
    {
        string GetBannerURL();
    }
}
