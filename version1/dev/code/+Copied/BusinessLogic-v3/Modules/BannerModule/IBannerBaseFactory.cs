using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Modules.CultureDetailsModule;

namespace BusinessLogic_v3.Modules.BannerModule
{   

//IBaseFactory-File

	public interface IBannerBaseFactory : BusinessLogic_v3.Modules._AutoGen.IBannerBaseFactoryAutoGen
    {
        IEnumerable<IBannerBase> GetBannersByIdentifier(string identifier, Enums.BANNERS_SORT_BY sortBy);

        IEnumerable<IBannerBase> GetAllBannersSortedBy(Enums.BANNERS_SORT_BY sortBy);
	    IEnumerable<IBannerBase> GetBannersByCulture(Enums.BANNERS_SORT_BY sortBy, ICultureDetailsBase culture = null);
    }

}
