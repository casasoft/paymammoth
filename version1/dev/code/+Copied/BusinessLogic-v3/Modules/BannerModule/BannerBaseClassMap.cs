using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.BannerModule
{

//BaseClassMap-File
    
    public abstract class BannerBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.BannerBaseMap_AutoGen<TData>
    	where TData : BannerBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
        protected override void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {

            mapInfo.IsIndexed = true; 
            base.IdentifierMappingInfo(mapInfo);
        }
        

        
 
    }
   
}
