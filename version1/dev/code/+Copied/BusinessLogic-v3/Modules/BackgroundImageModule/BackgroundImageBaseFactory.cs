using BusinessLogic_v3.Util;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.BackgroundImageModule.Cached;

namespace BusinessLogic_v3.Modules.BackgroundImageModule
{

    //BaseFactory-File

    public abstract class BackgroundImageBaseFactory : BusinessLogic_v3.Modules._AutoGen.BackgroundImageBaseFactoryAutoGen, IBackgroundImageBaseFactory
    {
        /// <summary>
        /// Returns a random background image stored in the database.
        /// </summary>
        /// <returns>Background image class (IBackgroundImageBase).</returns>
        public IBackgroundImageBase GetRandomBackground()
        {
            var q = GetQuery();
            q.RootCriteria.AddOrder(nHibernateUtil.GetRandomOrder());
            nHibernateUtil.LimitCriteriaByPrimaryKeys(q.RootCriteria, 1, 1);
            IEnumerable<IBackgroundImageBase> results = FindAll(q);
            return results.FirstOrDefault();
        }

        /// <summary>
        /// Returns a random background image linked with a category, if none found, it checks its children, if none found it checks its parents.
        /// </summary>
        /// <param name="categoryLink">The category the background image is linked with.</param>
        /// <param name="getRandomOtherIfNoneFound">If no background image is linked with the specified category, children or parents, then a random image from the other background images is returned instead (true/false = enable/disable).</param>
        /// <returns>Background image class (IBackgroundImageBase).</returns>
        public IBackgroundImageBase GetRandomBackgroundLinkedWithCategory(ICategoryBase categoryLink, bool getRandomOtherIfNoneFound = true)
        {
            IBackgroundImageBase result =
                BackgroundImagesCategoryLinksCacheManager.Instance.GetRandomBackgroundLinkedWithCategory(categoryLink);
            if (result == null && getRandomOtherIfNoneFound)
            {
                return GetRandomBackground();
            }
            return result;
        }
    }

}
