using BusinessLogic_v3.Modules.CategoryModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;


using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.BackgroundImageModule
{   

//IBaseFactory-File

	public interface IBackgroundImageBaseFactory : BusinessLogic_v3.Modules._AutoGen.IBackgroundImageBaseFactoryAutoGen
	{
	    /// <summary>
	    /// Returns a random background image stored in the database.
	    /// </summary>
	    /// <returns>Background image class (IBackgroundImageBase).</returns>
	    IBackgroundImageBase GetRandomBackground();

        /// <summary>
        /// Returns a random background image linked with a category, if none found, it checks its children, if none found it checks its parents.
        /// </summary>
        /// <param name="categoryLink">The category the background image is linked with.</param>
        /// <param name="getRandomOtherIfNoneFound">If no background image is linked with the specified category, children or parents, then a random image from the other background images is returned instead (true/false = enable/disable).</param>
        /// <returns>Background image class (IBackgroundImageBase).</returns>
	    IBackgroundImageBase GetRandomBackgroundLinkedWithCategory(ICategoryBase categoryLink,
	                                                               bool getRandomOtherIfNoneFound = true);
	}

}
