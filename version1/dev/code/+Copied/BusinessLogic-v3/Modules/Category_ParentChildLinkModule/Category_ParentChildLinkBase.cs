using BusinessLogic_v3.Classes.General.ParentChildLinks;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.Category_ParentChildLinkModule
{

//BaseClass-File
	
    public abstract class Category_ParentChildLinkBase : BusinessLogic_v3.Modules._AutoGen.Category_ParentChildLinkBaseAutoGen, ICategory_ParentChildLinkBase
        , IParentChildLink
    {
    
		#region BaseClass-AutoGenerated
		#endregion
    
        public Category_ParentChildLinkBase()
        {
            
        }    
    	protected override void initPropertiesForNewItem()
        {
        	//Fill any default values for NEW items.
        
            base.initPropertiesForNewItem();
        }
    	public override string ToString()
        {
            return base.ToString();
        }
        #region IParentChildLink Members

        long? IParentChildLink.ParentID
        {

            get { return (this.ParentCategory != null ? (long?)this.ParentCategory.ID : null); }
        }

        long? IParentChildLink.ChildID
        {
            get { return (this.ChildCategory != null ? (long?)this.ChildCategory.ID : null); }
        }

        #endregion
        protected override void Save_Before(OperationResult result, SaveBeforeAfterParams sParams)
        {
            base.Save_Before(result, sParams);
        }
    }
}
