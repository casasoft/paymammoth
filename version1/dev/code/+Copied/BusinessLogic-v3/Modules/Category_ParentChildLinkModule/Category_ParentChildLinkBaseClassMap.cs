using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.Category_ParentChildLinkModule
{

//BaseClassMap-File
    
    public abstract class Category_ParentChildLinkBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.Category_ParentChildLinkBaseMap_AutoGen<TData>
    	where TData : Category_ParentChildLinkBase
    {


        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
    }
   
}
