﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules
{
    public static class ModuleSettings
    {
        
        public static class Affiliates
        {
            public static bool Enabled { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Enabled); } }
            /// <summary>
            /// This is whether members with linked affiliates count as affiliate-commissioned-orde
            /// </summary>
            public static bool AffiliatesLinkedToMembersCountForOrders { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_AffiliatesLinkedToMembersCountForOrders); } }
            /// <summary>
            /// This means whether to use cookies to store affialiate referral code, for any subsequent access to the system
            /// </summary>
            public static bool UseCookiesToStoreAffiliate { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Cookies_UseCookiesToStoreAffiliate); } }
            public static int DaysToStoreAffiliateCookie { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<int>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Cookies_DaysToStoreAffiliateCookie); } }
            public static string AffiliateQueryStringParamName { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Querystring_ParamName); } }
            public static string AffiliateCookieName { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Cookies_CookieName); } }
            public static bool WhiteLabellingFunctionalityEnabled { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Whitelabelling_Enabled); } }
        }
        public static class Contact
        {
            public static bool IsOnlineEnquiryFormEnabled
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_IsOnlineEnquiryFormEnabled);
                }
            }

            public static bool IsContactNoInEnquiryFormVisible
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_IsContactNoInEnquiryFormVisible);
                }
            }

            public static bool IsCompanyInEnquiryFormVisible
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_IsCompanyInEnquiryFormVisible);
                }
            }

            public static bool DoNotShowLabelWithContactDetails
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_DoNotShowLabelWithContactDetails);
                }
            }

            public static bool IsContactDetailsVisibleInFooter
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_IsContactDetailsVisibleInFooter);
                }
            }

            public static bool IsHowDidYouFindUsInEnquiryFormVisible
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_IsHowDidYouFindUsInEnquiryFormVisible);
                }
            }

            public static bool IsSubjectInEnquiryFormVisible
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Contact_Details_IsSubjectInEnquiryFormVisible);
                }
            }
        }

        public static class Members
        {

            public static string QS_PARAM_ACTIVATION_ID { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Activation_ParamterNames_ID); } }
            public static string QS_PARAM_ACTIVATION_CODE { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Activation_ParamterNames_Code); } }

            public static CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE PasswordEncryptionType 
            {
                get
                { 
                    return BusinessLogic_v3.Settings.GetSettingFromDatabase<CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Password_EncryptionType);
                } 
            }
            public static int Password_PBKDF2_TotalIterations { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<int>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Password_TotalIterations); } }
            public static CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE UsernameEncryptionType
            {
                get
                {
                    return BusinessLogic_v3.Settings.GetSettingFromDatabase<CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Username_Encryption_Type);
                }
            }
            public static string UsernameEncryptionSalt
            {
                get
                {
                    return BusinessLogic_v3.Settings.GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Username_Encryption_Salt);
                }
            }
            public static int Username_PBKDF2_TotalIterations { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<int>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Username_Encryption_TotalIterations); } }
            public static int Login_InvalidLoginAttemptsToBlock { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<int>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Login_InvalidLoginAttemptsToBlock); } }
            public static int Login_TotalMinutesToBlockUserAfterTooManyInvalidLoginAttempt { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<int>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Login_TotalMinutesToBlockUserAfterTooManyInvalidLoginAttempt); } }

            public static bool RequireUniqueEmail { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Emails_RequiresUniqueEmail); } }
            public static bool ChangeSessionGUIDEveryLogin { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Login_ChangesSessionGuidWithEveryLogin); } }
            public static bool RequiresActivation { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Activation_RequiresActivation); } }
            public static bool SendEmailToAdminOnRegistration { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Emails_SendEmailToAdminOnRegistration); } }
            public static bool UsesSubscriptionSystem { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Subscriptions_Enabled); } }
            public static bool UsesAccountBalanceSystem { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_UsesCreditSystem); } }
            public static bool LoginWithUsername { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Activation_LoginWithUsername); } }
                
        }
        public static class MemberReferrals
        {
            public static bool Enabled { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_MemberReferrals_Enabled); } }
            /// <summary>
            /// This is whether orders of a member with a referring member will count towards the referring member's commission.
            /// </summary>
            public static bool ReferralsLinkedToMembersCountForOrders { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_MemberReferrals_ReferralsLinkedToMembersCountForOrders); } }
            /// <summary>
            /// This means whether to use cookies to store member referral code, for any subsequent access to the system
            /// </summary>
            public static bool UseCookiesToStoreReferral { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_MemberReferrals_Cookies_UseCookies); } }
            public static int DaysToStoreReferralCookie { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<int>(Enums.BusinessLogicSettingsEnum.ModuleSettings_MemberReferrals_Cookies_DaysToStoreReferralCookie); } }
            public static string ReferralQueryStringParamName { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum.ModuleSettings_MemberReferrals_Querystring_ParamName); } }
            public static string ReferralCookieName_Id { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum.ModuleSettings_MemberReferrals_Cookies_ParameterNames_Id); } }
            public static string ReferralCookieName_Url  { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum.ModuleSettings_MemberReferrals_Cookies_ParameterNames_Url); } }

            public static bool ReferralSuccessfulOnRegister { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_MemberReferrals_General_ReferralSuccessfulOnRegister); } }
            public static bool ReferralSystemPaysCommission { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_MemberReferrals_General_ReferralSystemPaysCommission); } }
           


        }
        public static class Articles
        {
            /*
            public static bool EnableContentPagesCommentingSystem
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_EnableContentPagesCommentingSystem);
                }
            }*/

            public static bool ShowAddThisInHeading
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_ShowAddThisInHeading);
                }
            }
            /*
            public static bool EnableBlogCommentingSystem
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_EnableBlogCommentingSystem);
                }
            }
            public static bool EnableNewsCommentingSystem
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_EnableNewsCommentingSystem);
                }
            }*/
            public static bool DoNotShowFooter
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_DoNotShowFooter);
                }
            }
            public static bool ShowImageGalleryForContentPages
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_ShowImageGalleryForContentPages);
                }
            }

            public static bool DoNotShowNavigationBreadcrumbs
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_DoNotShowNavigationBreadcrumbs);
                }
            }

            public static bool ShowRootNodeInNavigationBreadcrumbs
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_ShowRootNodeInNavigationBreadcrumbs);
                }
            }
        }

        public static class Generic
        {
            public static bool ShowBreadcrumbsInGeneralListing
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_ShowBreadcrumbsInGeneralListing);
                }
            }

            public static bool ShowCombosInGeneralListing
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_ShowCombosInGeneralListing);
                }
            }
            public static bool MultiLingual
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_MultiLingual);
                }
            }

            //todo: 03/jan/2012 - karl removed this as it makes sense to be based on a property in the culture, and not as a setting.
            //public static bool MultiLingualChangeDomain
            //{
            //    get
            //    {
            //        return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_MultiLingualChangeDomain);
            //    }
            //}

            /*public static bool MultiLingualPrependRoutingURLsWithLanguageCode
            {
                get
                {
                    return MultiLingual && BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_MultiLingualPrependRoutingURLsWithLanguageCode);
                }
            }*/
            public static bool IsSiteWithRegistration
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_IsSiteWithRegistration);
                }
            }

            public static bool IsSiteWithProducts
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_IsSiteWithProducts);
                }
            }

            public static bool IsSiteWithOrders
            {
                get
                {
                    return
                        BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(
                            BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_IsSiteWithOrders);
                }
            }

            public static bool IsGenericMessageEnabled
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_IsGenericMessageEnabled);
                }
            }

            public static bool IsSiteWithCategories
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_IsSiteWithCategories);
                }
            }


            public static bool IsSiteUsingMemberDiscounts
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_IsSiteUsingMemberDiscounts);
                }
            }

            public static bool IsSiteUsingCufon
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_IsSiteUsingCufon);
                }
            }

            public static bool IsSiteWithClassifieds
            {
                get
                {
                    return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Generic_IsSiteWithClassifieds);
                }
            }
        }

        public static class Shop
        {

            public static bool EcommerceEnabled { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Shop_Ecommerce_Enabled); } }
            
            /// <summary>
            /// This means whether the shop uses 'quantities'.  Not all shops have quantities, thus this would be ignored in search listings.
            /// </summary>
            public static bool UsesQuantities { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Shop_Quantities_Enabled); } }

            public static bool CanBulkImportProducts { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Shop_BulkImport_Enabled); } }

            
            public static bool IsCurrentWebsiteAShop { get { return BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Shop_Enabled); } }
            public static class DataImport
            {
                static DataImport()
                {
                    ShowUploadPath = false;
                    ShowZIPFile =false;
                    ShowRemoveAnyExistingImages = true;
                }

                public static bool ShowUploadPath { get; set; }
                public static bool ShowZIPFile { get; set; }
                public static bool ShowRemoveAnyExistingImages { get; set; }
            }
        }
    }
}
