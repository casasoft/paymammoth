using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.CultureInfoModule
{

//BaseClass-File
	
    public abstract class CultureInfoBase : BusinessLogic_v3.Modules._AutoGen.CultureInfoBaseAutoGen, ICultureInfoBase
    {
    
		#region BaseClass-AutoGenerated
		#endregion
    
        public CultureInfoBase()
        {
            
        }    
    	protected override void initPropertiesForNewItem()
        {
        	//Fill any default values for NEW items.
        
            base.initPropertiesForNewItem();
        }
    	public override string ToString()
        {
            return base.ToString();
        }
    
    }
}
