using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.NewsletterSubscriptionModule
{   

//IBaseFactory-File

	public interface INewsletterSubscriptionBaseFactory : BusinessLogic_v3.Modules._AutoGen.INewsletterSubscriptionBaseFactoryAutoGen
    {

        INewsletterSubscriptionBase LoadNewsletterSubscriptionFromEmail(string email);
        INewsletterSubscriptionBase UnsubscribeNewsletterSubscriptionByEmail(string email);
	    bool AddSubscription(string email, string name = null, string tel = null);
	    INewsletterSubscriptionBase GetSubscriptionByEmail(string email);
	    Enums.NEWSLETTER_SUBSCRIBE_RESULT SubscribeNewsletterSubscription(string name, string email, string mob,
	                                                                      bool autoSave = true);
    }

}
