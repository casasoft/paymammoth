using BusinessLogic_v3.Extensions;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Modules.NewsletterSubscriptionModule
{   

//BaseFactory-File

	public abstract class NewsletterSubscriptionBaseFactory : BusinessLogic_v3.Modules._AutoGen.NewsletterSubscriptionBaseFactoryAutoGen, INewsletterSubscriptionBaseFactory
    {


        public NewsletterSubscriptionBaseFactory()
        {

        }

        #region BaseFactory-AutoGenerated

        #endregion

        public INewsletterSubscriptionBase GetSubscriptionByEmail(string email)
        {
            var q = GetQuery();
            q = q.Where(item => item.Email == email);
            return FindItem(q);
        }
        public bool AddSubscription(string email, string name = null, string tel = null)
        {
            NewsletterSubscriptionBase item = (NewsletterSubscriptionBase) GetSubscriptionByEmail(email);
            if (item == null || (item != null && item.IsDisabled()))
            {
                item = CreateNewItem();
                item.Email = email;
                item.Name = name;
                item.Telephone = tel;
                item.ActivateSubscription();
                
                return true;
            }
            else
            {
                return false;
                
            }

        }
        public bool RemoveSubscription(string email)
        {
            var item = GetSubscriptionByEmail(email);
            bool ok = false;
            if (item != null && item.Enabled)
            {
                ok = true;
                item.UnSubscribe();
            }
            return ok;


        }
        public Enums.NEWSLETTER_SUBSCRIBE_RESULT SubscribeNewsletterSubscription(string name, string email, string mob, bool autoSave = true)
        {
            Enums.NEWSLETTER_SUBSCRIBE_RESULT result;
            using (var t = nHibernateUtil.BeginTransactionBasedOnAutoSaveBool(autoSave))
            {
                var item = LoadNewsletterSubscriptionFromEmail(email);
                if (item != null && item.Enabled)
                {
                    //Already exists
                    result = Enums.NEWSLETTER_SUBSCRIBE_RESULT.AlreadyExists;
                }
                else if (item != null && !item.Enabled)
                {
                    //Re-enable
                    item.Enabled = true;
                    item.Save();
                    result = Enums.NEWSLETTER_SUBSCRIBE_RESULT.Success;
                }
                else
                {
                    //Not exist
                    item = CreateNewItem();
                    item.Name = name;
                    item.Email = email;
                    item.Telephone = mob;
                    item.ActivateSubscription(autoSave:false);
                    item.Save();
                    result = Enums.NEWSLETTER_SUBSCRIBE_RESULT.Success;
                }
                t.CommitIfNotNull();
            }
            return result;
        }

        public INewsletterSubscriptionBase LoadNewsletterSubscriptionFromEmail(string email)
        {
            return this.GetSubscriptionByEmail(email);
        }

        public INewsletterSubscriptionBase UnsubscribeNewsletterSubscriptionByEmail(string email)
        {
            var item = this.LoadNewsletterSubscriptionFromEmail(email);

            if (item != null)
            {
                item.UnSubscribe();
                
            }
            return item;

        }




    }

}
