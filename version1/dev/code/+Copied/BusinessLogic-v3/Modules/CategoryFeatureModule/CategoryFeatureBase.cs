using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.CategoryFeatureModule
{

//BaseClass-File
	
    public abstract class CategoryFeatureBase : BusinessLogic_v3.Modules._AutoGen.CategoryFeatureBaseAutoGen, ICategoryFeatureBase
    {
    
		#region BaseClass-AutoGenerated
		#endregion
    
        public CategoryFeatureBase()
        {
            
        }    
    	protected override void initPropertiesForNewItem()
        {
        	//Fill any default values for NEW items.
        
            base.initPropertiesForNewItem();
        }
    	public override string ToString()
        {
            return base.ToString();
        }

        #region BaseClass-AutoGenerated
        #endregion



        #region Icon Media Item Snippet (v3)

        // Icon : This should be the name of the image, e.g MainPhoto, etc. 
        // CategoryFeatureBase : This should be the name of the class that is implementing this snippet, e.g EventBase or Member

        public enum IconSizingEnum
        {
            [ImageSpecificSizeDefaultValues(Width = 120, Height = 90, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None)]
            Thumbnail,

            [ImageSpecificSizeDefaultValues(Width = 640, Height = 480, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None)]
            Normal
        }

        public class IconMediaItem : BaseMediaItemImage<IconSizingEnum>
        {
            /* If you need to update SetAsMainItem, Priority, etc, these are fields/methods that can be overriden in the base class */


            public CategoryFeatureBase Item { get; private set; }
            public IconMediaItem(CategoryFeatureBase item)
            {

                this.Item = item;

                MediaItemImageInfo imageInfo = new MediaItemImageInfo();
                imageInfo.InitByDb(this.Item, typeof(IconSizingEnum),
                                        p => p.IconFilename,  //this should be the filename field
                                        () => item.Title,  //this should be the title field.  Note that this is optional.  This is only used to generate the filename from, to include the title for Google Images purposes 
                                        "CategoryFeature - Icon"); //this should be the identifier for this image, and what will as the title in the Cms/Sizing Info, so make it nice and understandable :)

                imageInfo.DeleteDbItemOnRemove = false; //if you want that on delete of this image, the item is deleted, set this as true.

                this.MediaItemInfo = imageInfo;

            }



        }

        protected IconMediaItem _m_Icon = null;
        public IconMediaItem Icon { get { if (_m_Icon == null) _m_Icon = new IconMediaItem(this); return _m_Icon; } }



        #endregion




        


        protected override void Delete_After(OperationResult result, DeleteBeforeAfterParams delParams)
        {
            base.Delete_After(result, delParams);
            foreach (var f in this.ProductCategoryFeatureValues)
            {

                f.Delete();
            }
            this.ProductCategoryFeatureValues.Clear();

        }
        

    
    }
}
