using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.ArticleMediaItemModule
{

    //BaseClass-File

    public abstract class ArticleMediaItemBase : BusinessLogic_v3.Modules._AutoGen.ArticleMediaItemBaseAutoGen, IArticleMediaItemBase
    {

        #region BaseClass-AutoGenerated
        #endregion

        public ArticleMediaItemBase()
        {

        }
        protected override void initPropertiesForNewItem()
        {
            //Fill any default values for NEW items.

            base.initPropertiesForNewItem();
        }
        public override string ToString()
        {
            return base.ToString();
        }

        protected override void Save_Before(OperationResult result, SaveBeforeAfterParams sParams)
        {
            checkMainImage();
            base.Save_Before(result, sParams);
        }

        private void checkMainImage()
        {
            if(this.IsMainImage)
            {
                var otherImages = this.ContentPage.ArticleMediaItems.Where(item => item.ID != this.ID);
                foreach(var img in otherImages)
                {
                    img.IsMainImage = false;
                    img.Save();
                }
            }
        }

        #region Image Media Item Snippet (v3)

        // Image : This should be the name of the image, e.g MainPhoto, etc. 
        // ArticleMediaItemBase : This should be the name of the class that is implementing this snippet, e.g EventBase or Member

        public enum ImageSizingEnum
        {
            [ImageSpecificSizeDefaultValues(Width = 120, Height = 90, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None)]
            Thumbnail,
            [ImageSpecificSizeDefaultValues(Width = 640, Height = 480, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None)]
            Normal,
            [ImageSpecificSizeDefaultValues(Width = 1600, Height = 1600, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None)]
            Large
        }

        public class ImageMediaItem : BaseMediaItemImage<ImageSizingEnum>, IMediaItemImage<ImageSizingEnum>
        {
            /* If you need to update SetAsMainItem, Priority, etc, these are fields/methods that can be overriden in the base class */


            public ArticleMediaItemBase Item { get; private set; }
            public ImageMediaItem(ArticleMediaItemBase item)
            {

                this.Item = item;

                MediaItemImageInfo imageInfo = new MediaItemImageInfo();
                imageInfo.InitByDb(this.Item, typeof(ImageSizingEnum),
                                        p => p.Filename,  //this should be the filename field
                                        () => (Item.ContentPage != null ? Item.ContentPage.PageTitle : Item.ID.ToString()),  //this should be the title field.  Note that this is optional.  This is only used to generate the filename from, to include the title for Google Images purposes 
                                        "ArticleMediaItemBase - Image"); //this should be the identifier for this image, and what will as the title in the Cms/Sizing Info, so make it nice and understandable :)

                imageInfo.DeleteDbItemOnRemove = false; //if you want that on delete of this image, the item is deleted, set this as true.

                this.MediaItemInfo = imageInfo;

            }



        }

        protected ImageMediaItem _m_Image = null;
        public ImageMediaItem Image { get { if (_m_Image == null) _m_Image = new ImageMediaItem(this); return _m_Image; } }

        /// <summary>
        /// Add this signature to the interface as well.
        /// </summary>
        IMediaItemImage<ArticleMediaItemBase.ImageSizingEnum> IArticleMediaItemBase.Image
        {
            get { return this.Image; }
        }

        #endregion


    }
}
