﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Login;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules.ShoppingCartModule;


namespace BusinessLogic_v3.Modules
{
    public static class ModulesUtil
    {
        public static MemberBase GetCurrentLoggedUser()
        {

            return BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.GetCurrentLoggedUser();
        }
        //public static MemberBase GetCurrentLoggedUserOrTemp()
        //{

        //    return BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.GetCurrentLoggedUserOrTemp();
        //}
        //public static IUser GetTemporaryUser()
        //{

        //    return BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.GetTemporaryUser();
        //}

        public static IWebUserSessionLoginManager<MemberBase> GetMemberSessionLoginManager()
        {
            return BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance;
        }

        public static IShoppingCartBase GetCurrentShoppingCart(bool createIfNotExists)
        {
            return ShoppingCartManager.Instance.GetCurrentCartForContext(createIfNotExists);
            //var loginManager = BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance;
            //if (loginManager.IsLoggedIn)
            //{
            //    var user = loginManager.GetLoggedInUser();
            //    return user.GetShoppingCart(createIfNotExists: false);
            //}
            //else
            //{

            //}
        }

    }
}
