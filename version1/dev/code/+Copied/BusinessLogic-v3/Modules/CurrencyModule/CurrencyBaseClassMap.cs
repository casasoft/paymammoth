using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.CurrencyModule
{

//BaseClassMap-File
    
    public abstract class CurrencyBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.CurrencyBaseMap_AutoGen<TData>
    	where TData : CurrencyBase
    {


        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
    }
   
}
