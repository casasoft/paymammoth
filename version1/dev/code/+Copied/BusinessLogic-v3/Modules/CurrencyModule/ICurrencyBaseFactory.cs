using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.CurrencyModule
{   

//IBaseFactory-File

	public interface ICurrencyBaseFactory : BusinessLogic_v3.Modules._AutoGen.ICurrencyBaseFactoryAutoGen
    {

        ICurrencyBase GetByCurrencyCode(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 code);
        ICurrencyBase GetByCurrencyCode(string code) ;
        ICurrencyBase GetDefaultCurrency() ;

    }

}
