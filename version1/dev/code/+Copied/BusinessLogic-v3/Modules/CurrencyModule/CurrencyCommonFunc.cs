﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.CurrencyModule
{
    public class CurrencyCommonFunc
    {
        public ICurrencyBase Currency { get; private set; }
        public CurrencyCommonFunc(ICurrencyBase currency)
        {
            this.Currency = currency;
        }
        public CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 GetCurrencyISOAsEnumValue()
        {
            return this.Currency.GetCurrencyISOAsEnumValue();
            
        }


        /// <summary>
        /// Converts the item with the exchange rate.  The paramter price is MULTIPLIED with the exchange rate
        /// </summary>
        /// <param name="dPrice"></param>
        /// <returns></returns>
        public double ConvertPriceBasedOnExchangeRate(double dPrice)
        {
            return dPrice * this.Currency.ExchangeRateMultiplier;

        }

        /// <summary>
        /// Converts the item back to the base price, with the exchange rate.  The paramter price is DIVIDED with the exchange rate
        /// </summary>
        /// <param name="dPrice"></param>
        /// <returns></returns>
        public double GetBasePriceFromConvertedPrice(double dPrice)
        {
            return dPrice / this.Currency.ExchangeRateMultiplier;

        }

        


        public string FormatPriceAsHTML(double d, IFormatProvider formatProvider = null)
        {

            if (formatProvider == null)
                formatProvider = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCurrentCultureFormatProvider();

            string htmlSymbol = CS.General_v3.Util.Text.HtmlEncode(this.Currency.Symbol);
            string s = CS.General_v3.Util.NumberUtil.FormatNumber(d, CS.General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency, formatProvider, htmlSymbol);
            return s;
        }
        public string FormatPriceAsUnicodeText(double d, IFormatProvider formatProvider = null)
        {
            if (formatProvider == null)
                formatProvider = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCurrentCultureFormatProvider();


            string s = CS.General_v3.Util.NumberUtil.FormatNumber(d, CS.General_v3.Util.NumberUtil.NUMBER_FORMAT_TYPE.Currency, formatProvider, this.Currency.Symbol);
            return s;

        }

    }
}
