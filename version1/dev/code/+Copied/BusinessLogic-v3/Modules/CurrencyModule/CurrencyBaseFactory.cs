using BusinessLogic_v3.Classes.General;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.CurrencyModule
{   

//BaseFactory-File

	public abstract class CurrencyBaseFactory : BusinessLogic_v3.Modules._AutoGen.CurrencyBaseFactoryAutoGen, ICurrencyBaseFactory,
        IItemWithIdentifierFactory<CurrencyBase>
    {

        public CurrencyBaseFactory()
        {
            this.identifierCache = new ItemsByIdentifierCache<CurrencyBase>(this);

        }

        #region BaseFactory-AutoGenerated

        #endregion
        private ItemsByIdentifierCache<CurrencyBase> identifierCache = null;


//        protected override void fillInitialItemsInEmtpyDb()
        //        {Karl 17/may/2012 - this was removed.  We will now be creating standard items from the template DB, and not here.
//            using (var t = beginTransaction())
//            {
//                //Initialize repository
//                {
//                    {
////Euro
//                        var c = CreateNewItem();
//                        // c.Activated = true;
//                        c.SetCurrencyCodeFromISOEnum( CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro);
//                        c.ExchangeRateMultiplier = 1;
//                        c.Symbol = "�";
//                        c.Title = "Euro";
//                        c.Create();
//                    }
//                    {
////Dollar
//                        var c = CreateNewItem();
//                        // c.Activated = true;
//                        c.SetCurrencyCodeFromISOEnum(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedStatesOfAmericaDollars);
//                        c.ExchangeRateMultiplier = 1.3136;
                        
//                        c.Symbol = "$";
//                        c.Title = "Dollar";
//                        c.Create();
//                    }
//                    {
//                        var c = CreateNewItem();
//                        // c.Activated = true;
//                        c.SetCurrencyCodeFromISOEnum(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedKingdomPounds);
//                        c.ExchangeRateMultiplier = 0.854651919d;
                        
//                        c.Symbol = "�";
//                        c.Title = "Sterling";
//                        c.Create();
//                    }
//                }
//                t.Commit();
//            }
//            base.fillInitialItemsInEmtpyDb();
//        }
        public override void OnPostInitialisation()
        {
            base.OnPostInitialisation();
        }




        public CurrencyBase GetByCurrencyCode(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 code, bool loadFromCache = true)
        {
            string sCode = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode(code);
            return GetByCurrencyCode(sCode);


        }
        public CurrencyBase GetByCurrencyCode(string code, bool loadFromCache = true)
        {
            if (loadFromCache)
            {
                return this.identifierCache.GetItemByIdentifier(code);
            }
            else
            {
                var q = CurrencyBase.Factory.GetQuery().Where(c => c.CurrencyISOCode == code);
                return CurrencyBase.Factory.FindItem(q);
            }
            //return null;

        }






        #region IItemWithIdentifierFactory<CurrencyBase> Members

        CurrencyBase IItemWithIdentifierFactory<CurrencyBase>.GetItemByIdentifierFromDB(string identifier)
        {
            return GetByCurrencyCode(identifier, false);

        }

        CurrencyBase IItemWithIdentifierFactory<CurrencyBase>.GetItemByPKeyFromDB(long id)
        {
            return GetByPrimaryKey(id);

        }

        IEnumerable<CurrencyBase> IItemWithIdentifierFactory<CurrencyBase>.GetAllItemsFromDB()
        {
            return FindAll();

        }

        #endregion




        #region ICurrencyBaseDataFactory Members


        public CurrencyBase GetDefaultCurrency()
        {

            return CultureDetailsBaseFactory.Instance.GetDefaultCulture().DefaultCurrency;

        }

        #endregion


        #region ICurrencyBaseFactory Members

	    ICurrencyBase ICurrencyBaseFactory.GetByCurrencyCode(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 code)
        {
            return this.GetByCurrencyCode(code);
            
        }

	    ICurrencyBase ICurrencyBaseFactory.GetByCurrencyCode(string code)
        {
            return this.GetByCurrencyCode(code);
            
        }

        ICurrencyBase ICurrencyBaseFactory.GetDefaultCurrency()
        {
            return this.GetDefaultCurrency();
            
        }

        #endregion
    }

}
