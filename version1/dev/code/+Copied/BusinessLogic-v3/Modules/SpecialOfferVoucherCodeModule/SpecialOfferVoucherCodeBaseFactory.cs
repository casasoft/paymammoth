using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule
{   

//BaseFactory-File

	public abstract class SpecialOfferVoucherCodeBaseFactory : BusinessLogic_v3.Modules._AutoGen.SpecialOfferVoucherCodeBaseFactoryAutoGen, ISpecialOfferVoucherCodeBaseFactory
    {

        public ISpecialOfferVoucherCodeBase GetByVoucherCode(string code)
        {
            var q = GetQuery();
            q = q.Where(item => item.VoucherCode == code);
            return FindItem(q);
        }

    }

}
