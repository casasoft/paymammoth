﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.Helpers
{
    public class VoucherCodeGenerationResult
    {
        public enum VOUCHER_CODE_GEN_RESULT_TYPE
        {
            OK,
            AlreadyExists,
            NoValidCodeToGenerate
        }

        public SpecialOfferVoucherCodeBase VoucherCode { get; set; }
        public VOUCHER_CODE_GEN_RESULT_TYPE ResultType { get; set; }
    }
}
