﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.Helpers;

namespace BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule
{
    public class SpecialOfferVoucherCodeGenerator
    {
        public SpecialOfferBase SpecialOffer { get; private set; }

        public SpecialOfferVoucherCodeGenerator(SpecialOfferBase sp)
        {
            this.SpecialOffer = sp;
        }
        public List<Helpers.VoucherCodeGenerationResult> GenerateRandomCodes(int totalToGenerate, int length, int quantityToUseForEachCode = 1)
        {
            List<Helpers.VoucherCodeGenerationResult> result = new List<Helpers.VoucherCodeGenerationResult>();
            int generated = 0;
            int invalidCount = 0;
            int maxInvalidInARowToStop = 100;
            while (generated < totalToGenerate && invalidCount < maxInvalidInARowToStop)
            {
                string s = CS.General_v3.Util.Random.GetString(length, length).ToUpper();
                var code = this.SpecialOffer.CreateVoucherCode(s, quantity:quantityToUseForEachCode);
                if (code != null)
                {
                    invalidCount = 0; //reset the invalid count
                    VoucherCodeGenerationResult resultItem = new VoucherCodeGenerationResult();
                    resultItem.VoucherCode = code;
                    resultItem.ResultType = VoucherCodeGenerationResult.VOUCHER_CODE_GEN_RESULT_TYPE.OK;
                    result.Add(resultItem);
                    generated++;
                }
                else
                {
                    invalidCount++;
                }
            }
            for (int i = generated; i < totalToGenerate; i++)
            {
                VoucherCodeGenerationResult resultItem = new VoucherCodeGenerationResult();
                resultItem.ResultType = VoucherCodeGenerationResult.VOUCHER_CODE_GEN_RESULT_TYPE.NoValidCodeToGenerate;
                result.Add(resultItem);
                    
            }
            return result;

        }



    }
}
