using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules.OrderItemModule;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;

namespace BusinessLogic_v3.Modules.OrderModule
{

//IBaseClass-File
	
    public interface IOrderBase : BusinessLogic_v3.Modules._AutoGen.IOrderBaseAutoGen
    {

        double? GetTotalShippingCost();
        string GetAddressAsOneLine(string delimeter = ", ", bool includeCountry = false);
        double GetTotalTaxAmount();
        void CopyDetailsFromMember(MemberBase member = null, bool copyIfAlreadyFilled = false);

        IOrderItemBase AddOrderItem(string title, double unitTotalAmount, double VATPercentage, int quantity = 1,
                                   string reference = null, string itemType = null, int itemID = 0,
                                   string remarks = null, bool saveInDB = true);

        void PaymentProcessCompleted(string authCode, bool requiresManualPayment, PayMammoth.Connector.Enums.PaymentMethodSpecific paymentMethod,
                                     bool sendEmailsAboutSuccessfulPayment = true, bool saveInDB = true,
            bool throwErrorIfPaymentProcessAlreadyCompleted = false);

        void ConfirmOrder(bool autoSave = true);
        double GetTotalPrice(bool includeTax = true, bool includeShipping = true, bool reduceDiscount = true, bool autoSave = true);
        double GetTotalDiscount();
    }
}
