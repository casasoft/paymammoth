using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.OrderModule
{

//BaseClassMap-File
    
    public abstract class OrderBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.OrderBaseMap_AutoGen<TData>
    	where TData : OrderBase
    {


        protected override void IsShoppingCartMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            base.IsShoppingCartMappingInfo(mapInfo);
        }
        protected override void LastUpdatedOnMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            base.LastUpdatedOnMappingInfo(mapInfo);
        }

        protected override void CustomerEmailMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
           base.CustomerEmailMappingInfo(mapInfo);
        }
        protected override void OrderItemsMappingInfo(ICollectionMapInfo mapInfo)
        {
            base.OrderItemsMappingInfo(mapInfo);
            mapInfo.CascadeType.All = true;
        }
        protected override void DateCreatedMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            base.DateCreatedMappingInfo(mapInfo);
        }

        protected override void ReferenceMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            base.ReferenceMappingInfo(mapInfo);
        }


        protected override void ItemIDMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            base.ItemIDMappingInfo(mapInfo);
        }
        protected override void ItemTypeMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            base.ItemTypeMappingInfo(mapInfo);
        }
 
    }
   
}
