using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.OrderModule
{   

//IBaseFactory-File

	public interface IOrderBaseFactory : BusinessLogic_v3.Modules._AutoGen.IOrderBaseFactoryAutoGen
	{
	    IOrderBase GetOrderByReferenceCode(string refCode);


	}

}
