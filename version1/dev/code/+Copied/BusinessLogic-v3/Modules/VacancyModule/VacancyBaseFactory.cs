using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.VacancyModule
{   

//BaseFactory-File

	public abstract class VacancyBaseFactory : BusinessLogic_v3.Modules._AutoGen.VacancyBaseFactoryAutoGen, IVacancyBaseFactory
    {



        public IVacancyBase LoadVacancyFromRouteData()
        {
            var vacancyID = BusinessLogic_v3.Classes.Routing.VacancyRoute.GetVacancyIDFromRouteData();
            if (vacancyID.HasValue)
            {
                return GetByPrimaryKey(vacancyID.Value);
            }
            return null;
        }
    }

}
