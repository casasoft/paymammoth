﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.MemberModule;

namespace BusinessLogic_v3.Modules.MemberResponsibleLimitsModule
{
    /// <summary>
    /// This checks whether a member is still within his deposit limits or not
    /// </summary>
    public class MemberResponsibileLimitsTotalChecker
    {
        public MemberResponsibileLimitsTotalChecker(MemberBase member)
        {
            this._member = member;
        }

        private MemberBase _member = null;
        private MemberResponsibleLimitsBase limitsApplicable = null;
        public DateTime DateToCheck {get; private set;}

        public double? TotalDepositedInPeriod {get; private set;}
        public double? RemainingDepositLimit { get; private set; }
        /// <summary>
        /// Whether certain limits apply, or it is still unlimited
        /// </summary>
        public bool LimitsApply {get { return limitsApplicable != null; }}

        private void loadApplicableLimits()
        {
            limitsApplicable = _member.GetResponsibleLimitsApplicableOnDate(DateToCheck);
        }

        private void calculateTotalDepositedInPeriod()
        {
            DateTime startDate =this.limitsApplicable.CalculateStartDateForIntervalOn(this.DateToCheck);
            DateTime endDate = this.limitsApplicable.CalculateEndDateForIntervalOn(this.DateToCheck);

            var q = MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseFactory.Instance.GetQuery();
            q.Where(x => x.Member == this._member);
            q.Where(x => x.Date >= startDate && x.Date <= endDate);
            q.Where(x => x.UpdateType == Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE.Deposit);

            var historyList = MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseFactory.Instance.FindAll(q);
            this.TotalDepositedInPeriod = 0;
            foreach (var item in historyList)
            {
                this.TotalDepositedInPeriod += item.BalanceUpdate;
            }

            this.RemainingDepositLimit = limitsApplicable.Value - this.TotalDepositedInPeriod;
        }



        /// <summary>
        /// Calculates the limits available, based on today
        /// </summary>
        /// <returns></returns>
        public bool CalculateLimitsBasedOnToday()
        {
            return CalculateLimitsBasedOnDay(CS.General_v3.Util.Date.Now);
        }
        /// <summary>
        /// Calculates the limits available, based on a date.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public bool CalculateLimitsBasedOnDay(DateTime d)
        {
            this.DateToCheck = d;
            loadApplicableLimits();
            this.TotalDepositedInPeriod = null;
            this.RemainingDepositLimit = null;
            if (LimitsApply)
            {
                
                calculateTotalDepositedInPeriod();


            }
            else
            {
                
            }
            return this.LimitsApply;
        }




    }
}
