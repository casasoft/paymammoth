using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.ProductVariationSizeModule
{   

//BaseFactory-File

	public abstract class ProductVariationSizeBaseFactory : BusinessLogic_v3.Modules._AutoGen.ProductVariationSizeBaseFactoryAutoGen, IProductVariationSizeBaseFactory
    {



        public ProductVariationSizeBase GetByTitle(string size, bool createIfNotExists = true)
        {
            var q = GetQuery();
            q = q.Where(x => x.Title == size);
            var item = FindItem(q);
            if (item == null && createIfNotExists)
            {
                item = CreateNewItem();
                item.Title = size;
                item.CreateAndCommit();
            }
            return item;

        }
    }

}
