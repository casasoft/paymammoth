using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.ProductVariationSizeModule
{

//IBaseClass-File
	
    public interface IProductVariationSizeBase : BusinessLogic_v3.Modules._AutoGen.IProductVariationSizeBaseAutoGen
    {
    
    }
}
