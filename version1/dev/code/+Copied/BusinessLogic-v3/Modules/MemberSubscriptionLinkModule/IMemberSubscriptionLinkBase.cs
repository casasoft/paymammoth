using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;
using BusinessLogic_v3.Modules.MemberModule;

namespace BusinessLogic_v3.Modules.MemberSubscriptionLinkModule
{

//IBaseClass-File
	
    public interface IMemberSubscriptionLinkBase : BusinessLogic_v3.Modules._AutoGen.IMemberSubscriptionLinkBaseAutoGen
    {
        IMemberSubscriptionTypePriceBase GetSubscriptionTypePrice();
        IMemberBase GetMember();
        int? DaysRemainingInSubscription();
        bool AreThereMoreLinksAfterThis();
        double DaysRemainingToExpire(bool inclOtherLinks);
    }
}
