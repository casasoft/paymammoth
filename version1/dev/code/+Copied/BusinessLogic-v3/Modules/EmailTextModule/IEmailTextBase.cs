using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using CS.General_v3.Classes.Text;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.Text;

namespace BusinessLogic_v3.Modules.EmailTextModule
{

//IBaseClass-File

    public interface IEmailTextBase : BusinessLogic_v3.Modules._AutoGen.IEmailTextBaseAutoGen, IItemWithContentTags
    {
        ITokenReplacer CreateTokenReplacer();
        CS.General_v3.Classes.Email.EmailMessage GetEmailMessage(ITokenReplacer tokenReplacerToReplaceTags = null,
            nVelocityContext nvContext = null,
            ICultureDetailsBase culture = null, string contentTag = null, string subjectTag = null,
                 bool loadWithDefaultTemplate = true, bool convertImagesToLinkedResources = true, bool convertURLsToFull = true, string baseURL = null);
    }
}
