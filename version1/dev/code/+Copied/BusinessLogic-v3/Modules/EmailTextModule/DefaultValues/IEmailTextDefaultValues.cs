﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DefaultValues;

namespace BusinessLogic_v3.Modules.EmailTextModule.DefaultValues
{
    public interface IEmailTextDefaultValues : IBaseContentDefaultValueAttribute
    {
        string Title { get; set; }
        string BodyHtml { get; set; }
        string Subject { get; set; }
        string WhenIsThisEmailSent { get; set; }
        bool NotifyAdminAboutEmail { get; set; }



    }
}
