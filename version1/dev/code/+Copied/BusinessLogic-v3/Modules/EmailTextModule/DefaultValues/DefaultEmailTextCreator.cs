﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using BusinessLogic_v3.Classes.NHibernateClasses;
using System.Reflection;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Modules.EmailTextModule.DefaultValues
{
    public class DefaultEmailTextCreator : IEmailTextDefaultValues
    {
        public string FullIdentifier { get; private set; }
        public DefaultEmailTextCreator(string identifier)
        {
            this.FullIdentifier = identifier;
            
            int lastUnderscore = identifier.LastIndexOf('_');
            int lastDashPos = identifier.LastIndexOf('-');
            if (lastDashPos == -1) lastDashPos = identifier.Length;
            
            this.Title = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(identifier.Substring(lastUnderscore + 1, lastDashPos - lastUnderscore-1));
            

        }
        public void FillFromDefaultValues(IEmailTextDefaultValues values, string identifier)
        {
            if (values != null)
            {
                CS.General_v3.Util.ReflectionUtil.CopyProperties(values, this);
                //Updated by Mark 2011-11-17
                if (string.IsNullOrEmpty(this.Title) && !string.IsNullOrEmpty(this.Subject))
                {
                    this.Title = this.Subject;
                }
                if (string.IsNullOrEmpty(this.Subject) && !string.IsNullOrEmpty(this.Title))
                {
                    this.Subject = this.Title;
                }
                if (string.IsNullOrEmpty(this.Title)) this.Title = identifier;
                if (string.IsNullOrEmpty(this.Subject)) this.Subject = identifier;
            }

        }
        public EmailTextBase CreateEmail(bool autoSave = true)
        {
           
            EmailTextBase result = null;
            //if (ArticleType == Enums.ARTICLE_TYPE.None) throw new InvalidOperationException("Cannot create a default article without specifying the article type");
            var identifiers = CS.General_v3.Util.Text.Split(FullIdentifier, "_");

            if (string.IsNullOrWhiteSpace(this.BodyHtml)) this.BodyHtml = FullIdentifier;

            string currID = null;


            EmailTextBase parent = EmailTextBase.Factory.GetRoot();
            
            for (int i = 0; i < identifiers.Count; i++)
            {
                if (i > 0)
                    currID += "_";
                currID += identifiers[i];
                var newItem = EmailTextBase.Factory._getByIdentifier(currID);
                if (newItem == null)
                {
                    
                    { 
                        newItem = null;
                            
                        
                        using (var t = nHibernateUtil.BeginTransactionIfNotOneAlreadyActive())
                        {
                            newItem = EmailTextBase.Factory.CreateNewItem();
                            if (parent != null)
                            {

                                //Article_ParentChildLinkBase link = Article_ParentChildLinkBase.Factory.CreateNewItem();
                                //link.Child = newItem;
                                //link.Parent = parent;
                                //newItem.ParentArticleLinks.Add(link);
                                //parent.ChildArticleLinks.Add(link);
                                newItem.Parent = parent;

                                
                            }
                            newItem.Identifier = currID;
                            newItem.UsedInProject = true;

                            
                            
                            
                            if (i == identifiers.Count - 1)
                            { //copy all properties

                                CS.General_v3.Util.ReflectionUtil.CopyProperties<IEmailTextDefaultValues>(this, newItem);
                            }
                            if (string.IsNullOrWhiteSpace(newItem.Title)) newItem.Title = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(identifiers[i]);
                            if (string.IsNullOrWhiteSpace(newItem.Body)) newItem.Body = this.WhenIsThisEmailSent;

                            newItem.ResetTestItemSessionGuid(autosave:false);
                            newItem.Create();

                            if(t !=null)
                            {
                                t.Commit();
                            }
                        }
                        

                    }


                }
                parent = newItem;

            }
            result = parent;
            return result;


        }


        #region IArticleDefaultValues Members


        public CS.General_v3.Enums.CMS_ACCESS_TYPE AccessTypeRequiredToDelete { get; set; }

        #endregion



        #region IEmailTextDefaultValues Members

        public string Title { get; set; }

        public string BodyHtml { get; set; }

        public string Subject { get; set; }


        public bool NotifyAdminAboutEmail { get; set; }

        public string WhenIsThisEmailSent { get; set; }

        #endregion

       
    }
}
