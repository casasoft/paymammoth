﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DefaultValues;

namespace BusinessLogic_v3.Modules.EmailTextModule.DefaultValues
{
    public class EmailTextDefaultValuesAttribute : BaseContentDefaultValueAttribute, IEmailTextDefaultValues
    {
       

      

        #region IEmailTextDefaultValues Members

        public string Title { get; set; }
        public string BodyHtml { get; set; }
        public string WhenIsThisEmailSent { get; set; }
        public string Subject { get; set; }





        public bool NotifyAdminAboutEmail { get; set; }

        #endregion
    }
}
