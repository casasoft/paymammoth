using System.Net.Mail;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using CS.General_v3.Classes.Text;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.EmailTextModule
{   

//IBaseFactory-File

	public interface IEmailTextBaseFactory : BusinessLogic_v3.Modules._AutoGen.IEmailTextBaseFactoryAutoGen
    {


        IEmailTextBase GetEmailByIdentifier(Enums.EmailsBusinessLogic identifier) ;
        IEmailTextBase GetEmailByIdentifierAsString(Enum identifier);

        string GetHtmlEmailTemplate();
        string GetEmailSignature();

    }

}
