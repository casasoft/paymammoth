using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.EmailTextModule
{

//BaseClassMap-File
    
    public abstract class EmailTextBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.EmailTextBaseMap_AutoGen<TData>
    	where TData : EmailTextBase
    {


        protected override void ParentMappingInfo(ICollectionMapInfo mapInfo)
        {
            mapInfo.CascadeType.Clear();
            base.ParentMappingInfo(mapInfo);
        }
        protected override void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            mapInfo.Length = 250;
            base.IdentifierMappingInfo(mapInfo);
        }

    }
   
}
