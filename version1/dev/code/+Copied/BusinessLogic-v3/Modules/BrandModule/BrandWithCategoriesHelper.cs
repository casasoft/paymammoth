﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.ProductModule;

using CS.General_v3.Extensions;
using CS.General_v3.Util;

namespace BusinessLogic_v3.Modules.BrandModule
{
    using CS.General_v3.Classes.Interfaces.Hierarchy;

    public class BrandWithCategoriesHelper
    {
        private IEnumerable<ProductBase> _itemGroups;
        private IEnumerable<BrandBase> _brands;
        private CategoryBase _rootCategory;

        private List<HierarchyNavigationImpl> _categoryHierarchy;
        private List<CategoryBase> _categoriesTemp;

        private List<HierarchyNavigationImpl> _brandWithCategoryHierarchy { get; set; }

        public BrandWithCategoriesHelper()
        {
            _rootCategory = CategoryBaseFactory.Instance.GetRootCategory();
            _itemGroups = ProductBaseFactory.Instance.FindAll();
            _brands = BrandBaseFactory.Instance.FindAll();

            _categoriesTemp = new List<CategoryBase>();
            _categoryHierarchy = new List<HierarchyNavigationImpl>();
            _brandWithCategoryHierarchy = new List<HierarchyNavigationImpl>();
            initBrandWithCategoryHierarchy();
        }

        private void initBrandWithCategoryHierarchy()
        {

            foreach (var brand in _brands)
            {
                _categoryHierarchy.Clear();
                _categoriesTemp.Clear();
                var itemGroups = brand.Products.ToList();
                populateCategoryHierarchyFromBrandAndProducts(brand, itemGroups);

            }
        }

        private void populateCategoryHierarchyFromBrandAndProducts(BrandBase brand, IEnumerable<ProductBase> itemGroups)
        {
            HierarchyNavigationImpl hierarchy = new HierarchyNavigationImpl();
            populateHierarchyWithBrandDetails(hierarchy, brand);
            populateHierarchyWithProductCategories(hierarchy, itemGroups, brand);
            foreach (var item in _categoryHierarchy)
            {
                hierarchy.Children.Add(item);
            }
            _brandWithCategoryHierarchy.Add(hierarchy);
        }

        private void populateHierarchyWithProductCategories(HierarchyNavigationImpl hierarchy, IEnumerable<ProductBase> itemGroups, BrandBase brand)
        {

            foreach (var itemGroup in itemGroups)
            {
                addProductCategoryToCategoryList(itemGroup.GetMainCategory(), itemGroup, brand);
            }
        }

        private void addProductCategoryToCategoryList(CategoryBase categoryFrontend, ProductBase itemGroup, BrandBase brand)
        {

            CategoryBase rootCategory = CategoryBaseFactory.Instance.GetRootCategory();
            CategoryBase parentCategory = categoryFrontend.GetParent();

            if (parentCategory.ID != rootCategory.ID)
            {
                parentCategory = getTopMostParentCategory(parentCategory, rootCategory);
                _categoriesTemp.Add(parentCategory);
                populateHierarchyDataFromProductAndCategory(parentCategory, itemGroup, brand);
            }
            else
            {
                _categoriesTemp.Add(categoryFrontend);
                populateHierarchyDataFromProductAndCategory(categoryFrontend, itemGroup, brand);
            }
        }

        private CategoryBase getTopMostParentCategory(CategoryBase parentCategory, CategoryBase rootCategory)
        {


            while (parentCategory != null)
            {
                var pCat = parentCategory.GetParent();

                if (pCat.ID != rootCategory.ID)
                {
                    if (pCat.ID != rootCategory.ID)
                    {
                        parentCategory = parentCategory.GetParent();
                    }
                }
                else
                {
                    return parentCategory;
                }
            }
            return null;
        }


        private void populateHierarchyDataFromProductAndCategory(CategoryBase parentCategory, ProductBase itemGroup, BrandBase brand)
        {

            var categoryChildren = TreeUtil.GetChildrenAndSubChildrenFromItem<CategoryBase>(parentCategory, item => item.GetChildCategories());
            foreach (var cat in categoryChildren)
            {
                //var itemGroupFromCategory = _itemGroups.Where((itemGroupData) => itemGroupData.ID == itemGroup.ID).FirstOrDefault();
                if (itemGroup != null)
                {
                    HierarchyNavigationImpl categoryItemData = getHierarchyDataFromCategory(cat, brand);
                    var topMostCat = getTopMostParentCategory(cat, _rootCategory);
                    HierarchyNavigationImpl topMostCategory = getHierarchyDataFromCategory(topMostCat, brand);

                    if (topMostCat.ID != categoryItemData.ID)
                    {
                        var topParent = createHierarchyDataFromChildren(topMostCategory, topMostCat, brand);
                        _categoryHierarchy.Add(topParent);
                    }
                    else
                    {
                        _categoryHierarchy.Add(categoryItemData);
                    }
                }
            }
        }


        public HierarchyNavigationImpl createHierarchyDataFromChildren(HierarchyNavigationImpl parentHierarchy, CategoryBase parentCategory, BrandBase brand)
        {

            var parentCategorySubCategories = parentCategory.GetChildCategories();
            if (parentCategorySubCategories.IsNotNullOrEmpty())
            {
                foreach (var item in parentCategorySubCategories)
                {
                    var hierarchyData = getHierarchyDataFromCategory(item, brand);
                    parentHierarchy.Children.Add(hierarchyData);
                    createHierarchyDataFromChildren(hierarchyData, item, brand);
                }
            }
            else
            {
                return parentHierarchy;
            }
            return parentHierarchy;
        }

        private HierarchyNavigationImpl getHierarchyDataFromCategory(CategoryBase category, BrandBase brand)
        {
            HierarchyNavigationImpl hData = new HierarchyNavigationImpl();
            hData.Title = category.TitlePlural;
            hData.ID = category.ID;
            //ListingURLParserShop shopURLParser = new ListingURLParserShop(null, null, false);
            //shopURLParser.ListingParameters.Categories.Add(category);
            //shopURLParser.ListingParameters.Brands.Add(brand);
            //hData.Href = shopURLParser.GetUrl();
            //hData.Visible = true;
            return hData;
        }

        private void populateHierarchyWithBrandDetails(HierarchyNavigationImpl hierarchyData, BrandBase brand)
        {
            hierarchyData.Title = brand.Title;
            hierarchyData.Href = brand.GetBrandURLForSearch();
            hierarchyData.Visible = true;
        }

        public IEnumerable<IHierarchyNavigation> GetBrandHierarchyWithCategories()
        {
            return _brandWithCategoryHierarchy;
        }
    }
}