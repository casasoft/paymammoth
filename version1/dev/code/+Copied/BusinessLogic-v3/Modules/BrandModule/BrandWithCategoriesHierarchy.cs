﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BusinessLogic_v3.Modules.BrandModule
{
    using CS.General_v3.Classes.Interfaces.Hierarchy;

    public class BrandWithCategoriesHierarchy : IHierarchy
    {
        public BrandBase _brandBaseFrontend;
        public BrandWithCategoriesHierarchy(BrandBase brandFrontend)
        {
            _brandBaseFrontend = brandFrontend;
        }

        #region IHierarchy Members

        public long ID
        {
            get { return _brandBaseFrontend.ID; }
        }

        public string Title
        {
            get { return _brandBaseFrontend.Title; }
        }

        public string Href
        {
            get { return _brandBaseFrontend.GetBrandURLForSearch(); }
        }

        public CS.General_v3.Enums.HREF_TARGET HrefTarget
        {
            get { return CS.General_v3.Enums.HREF_TARGET.Self; }
        }

        public IEnumerable<IHierarchy> GetChildren()
        {
            return new List<IHierarchy>();
            //var hierarchy = BrandWithCategoriesManager.Instance.GetBrandHierarchyByBrandID(this.ID);
            //return hierarchy.GetChildren();
        }

        public IHierarchy ParentHierarchy
        {
            get { return null; }
        }

        //TODO: MARK - attach to routing when routing is done
        public bool Selected
        {
            get { return true; }
        }

        public string GetFullHierarchyName(string delimiter, bool reverseOrder, bool includeRoot)
        {
            return null;
        }

        public bool Visible
        {
            get { return true; }
        }

        #endregion

        #region IHierarchy Members



        public IEnumerable<IHierarchy> GetParents()
        {
            return new List<IHierarchy>();
            
        }

        #endregion

        #region IHierarchy Members



        #endregion

        IHierarchy IHierarchy.GetMainParent()
        {
            return GetParents().FirstOrDefault();
        }
    }
}
