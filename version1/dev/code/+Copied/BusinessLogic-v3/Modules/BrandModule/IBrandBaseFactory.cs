using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.BrandModule
{   

//IBaseFactory-File

	public interface IBrandBaseFactory : BusinessLogic_v3.Modules._AutoGen.IBrandBaseFactoryAutoGen
    {

        IEnumerable<IBrandBase> GetFeaturedBrands();
        IBrandBase GetBrandByTitleOrImportRef(string brandOrImportRef, bool createIfNotExists = true);

        IEnumerable<IBrandBase> GetAllBrandsForListing(bool loadNonVisibleInFrontendBrands = false);

    }

}
