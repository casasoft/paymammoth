using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.BrandModule
{

//BaseClassMap-File
    
    public abstract class BrandBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.BrandBaseMap_AutoGen<TData>
    	where TData : BrandBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                //return base.CacheType;
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
        
 
    }
   
}
