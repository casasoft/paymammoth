﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.URL.ListingURLParser;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.ProductModule;

using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.Interfaces.Hierarchy;

namespace BusinessLogic_v3.Modules.BrandModule
{

    public class BrandCategoriesHelper
    {
        //private BrandBase _brand;
        private List<BrandBase> _brands;
        private IEnumerable<ProductBase> _itemGroups;
        private List<CategoryBase> _categories;
        private List<HierarchyNavigationImpl> _categoryHierarchy { get; set; }

        public BrandCategoriesHelper() //Brand in constructor
        {
            _itemGroups = new List<ProductBase>();
            _categories = new List<CategoryBase>();
            _categoryHierarchy = new List<HierarchyNavigationImpl>();
            //_brand = brand;
            _brands = new List<BrandBase>();
            populateCategoriesFromBrand();
        }

        public HierarchyNavigationImpl GetBrandHierarchyWithCategoriesDistinct(BrandBase brand)
        {
            _categoryHierarchy.FilterDistinctItemsInList<HierarchyNavigationImpl>((item1, item2) => item1.ID.Equals(item2.ID));
            HierarchyNavigationImpl brandHierarchy = new HierarchyNavigationImpl();
            brandHierarchy.Title = brand.Title;
            brandHierarchy.Href = brand.GetBrandURLForSearch();
            brandHierarchy.Visible = true;
            foreach (var item in _categoryHierarchy)
            {
                brandHierarchy.Children.Add(item);
            }
            return brandHierarchy;
        }

        private void populateCategoriesFromBrand()
        {
            populateProducts();
            if (_itemGroups.IsNotNullOrEmpty())
            {
                populateCategories();
            }
        }

        private void populateCategories()
        {

            foreach (var itemgroup in _itemGroups)
            {
                addProductCategoryToCategoryList(itemgroup.GetMainCategory(), itemgroup);
                
            }
        }

        private void addProductCategoryToCategoryList(CategoryBase categoryFrontend, ProductBase itemGroup)
        {

            CategoryBase rootCategory = CategoryBaseFactory.Instance.GetRootCategory();
            CategoryBase parentCategory = categoryFrontend.GetParent();

            if (parentCategory.ID != rootCategory.ID)
            {
                parentCategory = getTopMostParentCategory(parentCategory, rootCategory);
                _categories.Add(parentCategory);
                populateHierarchyDataFromProductAndCategory(parentCategory, itemGroup);
            }
            else
            {
                _categories.Add(categoryFrontend);
                populateHierarchyDataFromProductAndCategory(categoryFrontend, itemGroup);
            }
        }

        private CategoryBase getTopMostParentCategory(CategoryBase parentCategory, CategoryBase rootCategory)
        {
            while (parentCategory != null)
            {
                var pCat = parentCategory.GetParent();

                if (pCat.ID != rootCategory.ID)
                {
                    if (pCat.ID != rootCategory.ID)
                    {
                        parentCategory = parentCategory.GetParent();
                    }
                }
                else
                {
                    return parentCategory;
                }
            }
            return null;
        }

        private void populateHierarchyDataFromProductAndCategory(CategoryBase parentCategory, ProductBase itemGroup)
        {

            var categoryChildren = TreeUtil.GetChildrenAndSubChildrenFromItem<CategoryBase>(parentCategory, item => item.GetChildCategories());
            foreach (var cat in categoryChildren)
            {
                var itemGroupFromCategory = cat.GetProducts().Where((itemGroupData) => itemGroupData.ID == itemGroup.ID).FirstOrDefault();
                var categoryRoot = CategoryBaseFactory.Instance.GetRootCategory();
                if (itemGroupFromCategory != null)
                {
                    HierarchyNavigationImpl categoryItemData = getHierarchyDataFromCategory(cat);
                    var topMostCat = getTopMostParentCategory(cat, categoryRoot);
                    HierarchyNavigationImpl topMostCategory = getHierarchyDataFromCategory(topMostCat);

                    if (topMostCat.ID != categoryItemData.ID)
                    {
                        var topParent = createHierarchyDataFromChildren(topMostCategory, topMostCat);
                        _categoryHierarchy.Add(topParent);
                    }
                    else
                    {
                        _categoryHierarchy.Add(categoryItemData);
                    }
                }
            }
        }

        public HierarchyNavigationImpl createHierarchyDataFromChildren(HierarchyNavigationImpl parentHierarchy, CategoryBase parentCategory)
        {
            var parentCategorySubCategories = parentCategory.GetChildCategories();
            if (parentCategorySubCategories.IsNotNullOrEmpty())
            {
                foreach (var item in parentCategorySubCategories)
                {
                    var hierarchyData = getHierarchyDataFromCategory(item);
                    parentHierarchy.Children.Add(hierarchyData);
                    createHierarchyDataFromChildren(hierarchyData, item);
                }
            }
            else
            {
                return parentHierarchy;
            }
            return parentHierarchy;
        }

        private HierarchyNavigationImpl getHierarchyDataFromCategory(CategoryBase category)
        {
            HierarchyNavigationImpl hData = new HierarchyNavigationImpl();
            hData.Title = category.TitlePlural;
            hData.ID = category.ID;
            ListingURLParserShop shopURLParser = new ListingURLParserShop(null, null, false);
            shopURLParser.ListingParameters.Categories.Add(category);
            //shopURLParser.ListingParameters.Brands.Add(_brand);
            hData.Href = shopURLParser.GetUrl();
            hData.Visible = true;
            return hData;
        }

        private void populateProducts()
        {
            //if (_brand != null)
            //{
            //   _itemGroups = _brand.GetProducts<ProductBase>();
            //}
            _itemGroups = ProductBaseFactory.Instance.FindAll();
        }
    }

}
