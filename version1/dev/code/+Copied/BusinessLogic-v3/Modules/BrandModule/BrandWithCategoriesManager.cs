﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;


namespace BusinessLogic_v3.Modules.BrandModule
{
    using CS.General_v3.Classes.Interfaces.Hierarchy;

    public class BrandWithCategoriesManager
    {
        private static BrandWithCategoriesManager _instance;
        private static IEnumerable<IHierarchy> _brandWithCategoriesHierarchy;

        private BrandWithCategoriesManager()
        {
        }

        public static BrandWithCategoriesManager Instance
        {
            get {
                checkBrandWithCategoriesHierarchy();
                return CS.General_v3.Classes.Singletons.Singleton.GetInstance<BrandWithCategoriesManager>();
            }
        }

        private static void checkBrandWithCategoriesHierarchy()
        {
            if (_brandWithCategoriesHierarchy == null)
            {
                initBrandWithCategoriesHierarchy();
            }
        }

        private static void initBrandWithCategoriesHierarchy()
        {
            Stopwatch s = new Stopwatch();
            s.Start();
            BrandWithCategoriesHelper brandWithCategoriesHelper = new BrandWithCategoriesHelper();
            _brandWithCategoriesHierarchy = brandWithCategoriesHelper.GetBrandHierarchyWithCategories();
            s.Stop();
            var x = s.ElapsedMilliseconds / 1000;
        }

        public IHierarchy GetBrandHierarchyByBrandID(long brandID)
        {
            return null;
        }

    }
}
