using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;
using BusinessLogic_v3.Classes.URL.ListingURLParser;
using BusinessLogic_v3.Modules.ProductModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.BrandModule
{

//BaseClass-File
	
    public abstract class BrandBase : BusinessLogic_v3.Modules._AutoGen.BrandBaseAutoGen, IBrandBase
    {

        public override string ToString()
        {
            return this.Title;
        }
        protected override void initPropertiesForNewItem()
        {
            this.Published = true;
            base.initPropertiesForNewItem();

        }





        

        #region BrandLogo Media Item Snippet (v3)

        // BrandLogo : This should be the name of the image, e.g MainPhoto, etc. 
        // BrandBase : This should be the name of the class that is implementing this snippet, e.g EventBase or Member

        public enum BrandLogoSizingEnum
        {
            [ImageSpecificSizeDefaultValues(Width = 120, Height = 90, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None)]
            Thumbnail,

            [ImageSpecificSizeDefaultValues(Width = 640, Height = 480, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None)]
            Normal
        }

        public class BrandLogoMediaItem : BaseMediaItemImage<BrandLogoSizingEnum>, IMediaItemImage<BrandLogoSizingEnum>
        {
            /* If you need to update SetAsMainItem, Priority, etc, these are fields/methods that can be overriden in the base class */


            public BrandBase Item { get; private set; }
            public BrandLogoMediaItem(BrandBase item)
            {

                this.Item = item;

                MediaItemImageInfo imageInfo = new MediaItemImageInfo();
                imageInfo.InitByDb(this.Item, typeof(BrandLogoSizingEnum),
                                        p => p.BrandLogoFilename,  //this should be the filename field
                                        null, //() => item.[ENTER TITLE PROPERTY HERE],  //this should be the title field.  Note that this is optional.  This is only used to generate the filename from, to include the title for Google Images purposes 
                                        "BrandBase - BrandLogo"); //this should be the identifier for this image, and what will as the title in the Cms/Sizing Info, so make it nice and understandable :)

                imageInfo.DeleteDbItemOnRemove = false; //if you want that on delete of this image, the item is deleted, set this as true.

                this.MediaItemInfo = imageInfo;

            }



        }

        protected BrandLogoMediaItem _m_BrandLogo = null;
        public BrandLogoMediaItem BrandLogo { get { if (_m_BrandLogo == null) _m_BrandLogo = new BrandLogoMediaItem(this); return _m_BrandLogo; } }

        /// <summary>
        /// Add this signature to the interface as well.
        /// </summary>
        IMediaItemImage<BrandBase.BrandLogoSizingEnum> IBrandBase.BrandLogo
        {
            get { return this.BrandLogo; }
        }

        #endregion
			


        

        public string GetBrandURLForSearch()
        {
            
            ListingURLParserShop searchParams = new ListingURLParserShop(null, null, false);
            searchParams.ListingParameters.Brands.Add(this);
            return searchParams.GetUrl();
        }

			
     
    
    }
}
