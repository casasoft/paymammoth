using BusinessLogic_v3.Classes.Searches.ClassifiedSearcher;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;


using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Modules.CategoryModule;

namespace BusinessLogic_v3.Modules.ClassifiedModule
{   

//IBaseFactory-File

	public interface IClassifiedBaseFactory : BusinessLogic_v3.Modules._AutoGen.IClassifiedBaseFactoryAutoGen
	{
	    /// <summary>
	    /// Returns all the categories applicable with classifieds - IMPORTANT: A category node with identifier 'Classifieds' must be set for this to work, otherwise an exception will be thrown!
	    /// </summary>
	    /// <returns></returns>
	    IEnumerable<ICategoryBase> GetClassifiedCategories();

	    SearchPagedResults<IClassifiedBase> GetClassifiedAds(ClassifiedSearcherParams searchParams);

	    IClassifiedBase GetClassifiedFromRoute();
	}

}
