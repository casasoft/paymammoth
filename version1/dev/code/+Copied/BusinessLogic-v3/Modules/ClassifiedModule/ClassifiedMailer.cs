﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Classes.Text;
using BusinessLogic_v3.Modules.EmailTextModule;
using CS.General_v3.Classes.URL;

namespace BusinessLogic_v3.Modules.ClassifiedModule
{
    public class ClassifiedMailer
    {
        protected ClassifiedBase _classified { get; private set; }

        public ClassifiedMailer(ClassifiedBase classifiedObject)
        {
            if (classifiedObject == null)
            {
                throw new InvalidOperationException(
                    "Mailer can only be initialised on a non-null instance of Classified!");
            }
            this._classified = classifiedObject;
        }

        public void SendNotificationToAdmin()
        {
            if (Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.Classifieds_SendReceiptNotificationToAdminUponSubmit))
            {
                string toName, toEmail, fromName, fromEmail, replyToName, replyToEmail;
                toName =
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_AdminNotificationToName);
                toEmail = _classified.CreatedBy.Email;
                fromName =
                    replyToName =
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_ReplyToName);
                fromEmail =
                    replyToEmail =
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_ReplyToEmail);

                TokenReplacer tr = GetTokenReplacer();

                Mail(toName, toEmail, fromName, fromEmail, replyToName, replyToEmail,
                     Enums.EmailsBusinessLogic.Classifieds_Admin_AClassifiedItemHasBeenPosted, tr);
            }
        }

        public TokenReplacer GetTokenReplacer()
        {
            TokenReplacer tr = TokenReplacer.CreateBlank();
            tr["[Member:FirstName]"] = _classified.CreatedBy.FirstName;
            tr["[Member:FullName]"] = _classified.CreatedBy.GetFullName();
            tr["[Classified:URL]"] = _classified.ToFrontendBase().GetURL();
            tr["[Classified:Title]"] = _classified.Title;
            tr["[Admin:Name]"] = Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_AdminNotificationToName);
            URLClass classifiedUrlClass = _classified.CmsFactory.GetEditUrlForItemWithID(_classified.ID);
            string cmsProductUrl = classifiedUrlClass != null ? classifiedUrlClass.GetURL(true) : null;
            tr["[Classified:CMSLink]"] = cmsProductUrl;
            return tr;
        }

        public void SendReceiptToOwner()
        {
            if (Factories.SettingFactory.GetSettingValue<bool>(Enums.BusinessLogicSettingsEnum.Classifieds_SendReceiptNotificationToOwnerUponSubmit))
            {
                string toName, toEmail, fromName, fromEmail, replyToName, replyToEmail;
                toName = _classified.CreatedBy.GetFullName();
                toEmail = _classified.CreatedBy.Email;
                fromName =
                    replyToName =
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_ReplyToName);
                fromEmail =
                    replyToEmail =
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_ReplyToEmail);

                TokenReplacer tr = GetTokenReplacer();

                Mail(toName, toEmail, fromName, fromEmail, replyToName, replyToEmail,
                     Enums.EmailsBusinessLogic.Classifieds_Owner_YourClassifiedItemHasBeenPosted, tr);
            }
        }

        public void Mail(string toName, string toEmail, string fromName, string fromEmail, string replyToName, string replyToEmail, Enum emailText, TokenReplacer tokenReplacer)
        {
            var emailDb = EmailTextBaseFactory.Instance.GetByIdentifier(emailText);
            var email = emailDb.GetEmailMessage(tokenReplacer);
            email.SetFromEmail(fromEmail, fromName);
            email.AddToEmails(toEmail, toName);
            email.SetReplyToEmail(replyToEmail, replyToName);

            email.SendAsynchronously();
        }
    }
}
