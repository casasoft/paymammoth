using BusinessLogic_v3.Classes.MediaItems;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.CmsUserModule;

namespace BusinessLogic_v3.Modules.ClassifiedModule
{

    //IBaseClass-File

    public interface IClassifiedBase : BusinessLogic_v3.Modules._AutoGen.IClassifiedBaseAutoGen
    {
        /// <summary>
        /// Submit an enquiry form to the owner of this classified item.
        /// </summary>
        /// <param name="message">The actual enquiry content.</param>
        /// <param name="fromName">Name of person sending the enquiry (Reply To Name).</param>
        /// <param name="fromEmail">Email of person sending the enquiry (Reply To Email).</param>
        void SubmitEnquiryForm(string message, string fromName, string fromEmail);

        /// <summary>
        /// Approve this classified item if (probably due to moderation) it has not been approved yet.
        /// </summary>
        /// <param name="cmsUser">The CMS User approving this item.</param>
        /// <param name="autoSave">Enables use of transactions for this change in DB Data.</param>
        void Approve(ICmsUserBase cmsUser, bool autoSave = true);

        /// <summary>
        /// Disapprove this classified item if it is currently approved.
        /// </summary>
        /// <param name="autoSave">Enables use of transactions for this change in DB Data.</param>
        void Disapprove(bool autoSave = true);

        ClassifiedMailer Mailer { get; }

        IMediaItemImage<ClassifiedBase.ImageSizingEnum> Image { get; }

        /// <summary>
        /// Validates the object then sends (if enabled from settings) emails to admin and owner.
        /// </summary>
        void Submit(bool autoSave = true);
    }
}
