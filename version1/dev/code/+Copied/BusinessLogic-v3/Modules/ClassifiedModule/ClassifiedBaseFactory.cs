using BusinessLogic_v3.Classes.Searches.ClassifiedSearcher;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Classes.Routing;

namespace BusinessLogic_v3.Modules.ClassifiedModule
{   

//BaseFactory-File

	public abstract class ClassifiedBaseFactory : BusinessLogic_v3.Modules._AutoGen.ClassifiedBaseFactoryAutoGen, IClassifiedBaseFactory
    {
        /// <summary>
        /// Returns all the categories applicable with classifieds - IMPORTANT: A category node with identifier 'Classifieds' must be set for this to work, otherwise an exception will be thrown!
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ICategoryBase> GetClassifiedCategories()
        {
            CategoryBase rootNode = (CategoryBase)Factories.CategoryFactory.GetByIdentifier(Enums.CATEGORY_ROOT_NODES.Classifieds,
                                                                              throwErrorIfDoesNotExist: false);
            if (rootNode == null)
            {
                throw new InvalidOperationException("For this to work, a Category node with identifier <" +
                                                    Enums.CATEGORY_ROOT_NODES.Classifieds +
                                                    "> must be specified! Its subchildren are the actual categories which will apply for classifieds.");
            }
            return rootNode.GetCategoriesUnderThis();
        }
        
        public SearchPagedResults<IClassifiedBase> GetClassifiedAds(ClassifiedSearcherParams searchParams)
        {
            ClassifiedSearcherParams sparams = searchParams;
            ClassifiedSearcher searcher = new ClassifiedSearcher();
            searcher.SearchParams = sparams;
            var results = searcher.GetSearchResults();
            return new SearchPagedResults<IClassifiedBase>(results.GetResults().Cast<IClassifiedBase>(), results.TotalResults);
        }

        public IClassifiedBase GetClassifiedFromRoute()
        {
            IClassifiedBase result = null;
            long? id = ClassifiedRoute.GetIdFromCurrentRoute();
            if(id.HasValue)
            {
                result = GetByPrimaryKey(id.Value);
            }
            return result;
        }
    }

}
