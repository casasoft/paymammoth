﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Exceptions;

namespace BusinessLogic_v3.Modules.ClassifiedModule
{
    public class ClassifiedValidator
    {
        protected ClassifiedBase _classified { get; private set; }

        public ClassifiedValidator(ClassifiedBase classifiedObject)
        {
            if(classifiedObject == null)
            {
                throw new InvalidOperationException(
                    "The validator can only be initialised with a non-null classified object!");
            }
            this._classified = classifiedObject;
        }

        protected virtual StringBuilder additionalValidation(StringBuilder sbInstanceOfAlreadyFoundErrors)
        {
            return sbInstanceOfAlreadyFoundErrors;
        }

        public void Validate()
        {
            StringBuilder sb = new StringBuilder();
            if (_classified.CreatedBy == null)
            {
                sb.AppendLine("Owner of this object must be set before saving!");
            }
            if (string.IsNullOrWhiteSpace(_classified.Title))
            {
                sb.AppendLine("Title must be set before saving!");
            }
            if (_classified.Category == null)
            {
                sb.AppendLine("Category must be set before saving!");
            }
            sb = additionalValidation(sb);
            string errorMsg = sb.ToString();
            if (!string.IsNullOrWhiteSpace(errorMsg))
            {
                throw new FailedClassifiedValidationException(errorMsg);
            }
        }
    }
}
