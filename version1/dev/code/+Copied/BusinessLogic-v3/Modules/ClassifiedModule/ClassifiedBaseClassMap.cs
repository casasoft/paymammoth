using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.ClassifiedModule
{

//BaseClassMap-File
    
    public abstract class ClassifiedBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.ClassifiedBaseMap_AutoGen<TData>
    	where TData : ClassifiedBase
    {

        protected override void TitleMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            base.TitleMappingInfo(mapInfo);
        }

        protected override void DescriptionMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            
            base.DescriptionMappingInfo(mapInfo);
        }

 
    }
   
}
