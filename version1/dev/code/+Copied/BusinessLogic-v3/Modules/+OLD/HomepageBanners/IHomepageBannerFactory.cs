﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BusinessLogic_v3.Modules.HomepageBanners
{
    public interface IHomepageBannerFactory : IBaseItemDataFactory<IHomepageBanner> 
    {
        
    }
}
