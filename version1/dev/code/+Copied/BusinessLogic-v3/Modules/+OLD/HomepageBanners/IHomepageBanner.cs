﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.HomepageBanners
{
    public interface IHomepageBanner : IBaseItemData
    {
    //    int Priority { get; }
        string Title { get; }
        bool Activated { get; }
        string GetWebPath();
        string Description { get; }
        string Name { get; }
        string Href { get; }
        CS.General_v3.Enums.HREF_TARGET HrefTarget { get; }
        double GetPlaybackLengthInSeconds();
        double SlideshowDelaySec { get; }

    }
}
