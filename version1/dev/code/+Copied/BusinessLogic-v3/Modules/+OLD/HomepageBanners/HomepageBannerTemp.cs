﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;

namespace BusinessLogic_v3.Modules.HomepageBanners
{
    public class HomepageBannerTemp  :BaseItemDataTemp, IHomepageBanner
    {
        public HomepageBannerTemp()
        {
            
        }


    //    public int Priority {get;set;}
       

        public string Title { get; set; }

        public bool Activated { get; set; }

        public string MediaItemURL { get; set; }

        public string GetWebPath()
        {
            return this.MediaItemURL;
        }

        public string Description { get; set; }

        public string Name { get; set; }

        #region IHomepageBanner Members


        public double PlaybackLengthSeconds { get; set; }

        public double GetPlaybackLengthInSeconds()
        {
            return PlaybackLengthSeconds;
        }

        public double SlideshowDelaySec { get; set; }

        #endregion

        #region IHomepageBanner Members


        public string Href { get; set; }

        public CS.General_v3.Enums.HREF_TARGET HrefTarget { get; set; }

        #endregion
    }
}
