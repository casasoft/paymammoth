﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;

namespace BusinessLogic_v3.Modules
{
    public interface IBaseItemDataFactory<TItem> 
        where TItem: IBaseItemData
        
    {
        TItem GetByPrimaryKey(long id);
    //    IEnumerable<TItem> GetByPrimaryKeys(IEnumerable<long> ids);
        TItem CreateItemInstance();
        IEnumerable<TItem> GetAllItemsInRepository();
        
    }
}
