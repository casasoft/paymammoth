﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Login;
using BusinessLogic_v3.Modules.Ordering;

namespace BusinessLogic_v3.Modules.Members
{
    public class MemberCommonFunc : UserCommonFunc
    {
        public new IMember User
        {
            get
            {
                return (IMember)base.User;
            }
            
        }
        public MemberCommonFunc(IMember member) : base(member)
        {
            System.Diagnostics.Contracts.Contract.Requires(member != null);
            member.SessionGUID = CS.General_v3.Util.Random.GetGUID();
            
        }

        public IOrder GetLastSuccessfulOrder()
        {
            IOrder lastOrder = null;
            DateTime lastDate = CS.General_v3.Util.Date.Now;
            var orders = User.GetOrderHistory();
            foreach (var o in orders)
            {
                if (o.Paid)
                {
                    if (lastOrder == null ||
                        (lastOrder != null && o.DateCreated > lastOrder.DateCreated))
                    {
                        lastOrder = o;
                        lastDate = o.DateCreated;
                    }
                }
            }
            return lastOrder;
            
        }

    }
}
