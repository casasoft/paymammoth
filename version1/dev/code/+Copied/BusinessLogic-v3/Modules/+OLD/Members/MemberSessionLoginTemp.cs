﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Ordering;
using CS.General_v3.Classes.Login;

namespace BusinessLogic_v3.Modules.Members
{
    public class MemberSessionLoginTemp :  MemberSessionLoginBase<MemberTemp>
    {




        public MemberSessionLoginTemp() : base(null)
        {

        }

        public new MemberTemp GetLoggedInUser()
        {
            return (MemberTemp)base.GetLoggedInUser();
        }
        public override IEnumerable<MemberTemp> GetUsersList(string username)
        {
            var members = Modules.Factories.MemberFactory.GetAllItemsInRepository();
            List<MemberTemp> result = new List<MemberTemp>();
            foreach (var m in members)
            {
                if (string.Compare(m.Username, username, true) == 0)
                {
                    result.Add((MemberTemp)m);
                }
            }
            return result;
        }

       
    }
}
