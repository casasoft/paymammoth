﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Ordering;
using CS.General_v3.Classes.Login;
using BusinessLogic_v3.Classes.Interfaces;

namespace BusinessLogic_v3.Modules.Members
{
    public interface IMember : IBaseItemData, IUser, IMemberAddressDetails
    {
        
        string ContactNumber { get; set; }
        
        string Email { get; set; }
        
        string GetAddressAsString();
        
        //long ID { get;  }
        
        new string Name { get; set; }
        new string Surname { get; set; }
        
        IEnumerable<IOrder> GetOrderHistory();
        IOrder GetShoppingCart();
        IOrder GetLastSuccessfulOrder();

        new string Password { get; }
        
        
        bool ReceivesNewsletter { get; set; }

        //new void Save(bool savePermanently = true);
        bool VerifyForgottenPasswordCodeAndChange(string resetCode, string newPass);

    }
}
