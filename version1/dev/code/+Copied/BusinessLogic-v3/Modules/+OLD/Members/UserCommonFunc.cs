﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Login;

namespace BusinessLogic_v3.Modules.Members
{
    public class UserCommonFunc
    {
        public IUser User { get; private set; }
        public UserCommonFunc(IUser user)
        {
            System.Diagnostics.Contracts.Contract.Requires(user != null);

            this.User = user;
        }
        public bool CheckSessionGUID(string guid)
        {

            return string.Compare(this.User.SessionGUID, guid, true) == 0;
        }
    }
}
