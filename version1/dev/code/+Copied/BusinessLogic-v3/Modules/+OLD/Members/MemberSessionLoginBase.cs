﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Login;

namespace BusinessLogic_v3.Modules.Members
{
    public abstract class MemberSessionLoginBase<TUser>:CS.General_v3.Classes.Login.WebUserSessionLoginManager<TUser>, IMemberSessionLoginManager
        where TUser : IMember
    {
        public MemberSessionLoginBase(string welcomeUrl, string loginID = "MemberSessionLogin") : base(welcomeUrl, loginID)
        {
            
        }

        /*
        public override IEnumerable<CS.General_v3.Classes.Login.IUser> GetUsersList(string username)
        {
            var members = Modules.Factories.MemberFactory.GetAllItemsInRepository();
            List<IUser> result = new List<IUser>();
            foreach (var m in members)
            {
                if (string.Compare(m.Username, username, true) == 0)
                {
                    result.Add(m);
                }
            }
            return result;

        }*/

        public override TUser GetUserByID(long id)
        {
            return (TUser)Modules.Factories.MemberFactory.GetByPrimaryKey(id);

        }

        public new TUser GetLoggedInUser()
        {
            return (TUser)base.GetLoggedInUser();
        }

        protected override TUser createNewTemporaryUser()
        {
            TUser user = (TUser)Modules.Factories.MemberFactory.CreateItemInstance();
            user.MarkAsTemporary();
            user.CreateAndFlush(false);
            return user;
        }

        public new TUser GetTemporaryUser()
        {
            return (TUser)base.GetTemporaryUser();
            
        }


        public new TUser GetCurrentLoggedUserOrTemp()
        {
            return (TUser)base.GetCurrentLoggedUserOrTemp();
            
        }



        #region IWebUserSessionLoginManager<IMember> Members

        IMember IWebUserSessionLoginManager<IMember>.GetTemporaryUser()
        {
            return this.GetTemporaryUser();
        }

        IMember IWebUserSessionLoginManager<IMember>.GetCurrentLoggedUserOrTemp()
        {
            return this.GetCurrentLoggedUserOrTemp();
        }

        #endregion

        #region IUserSessionLoginManager<IMember> Members


        void IUserSessionLoginManager<IMember>.Logout()
        {
            this.Logout();
        }

        long IUserSessionLoginManager<IMember>.LoggedInUserID
        {
            get { return this.LoggedInUserID; }
        }

        IMember IUserSessionLoginManager<IMember>.GetLoggedInUser()
        {
            return this.GetLoggedInUser();
        }

        bool IUserSessionLoginManager<IMember>.IsLoggedIn
        {
            get { return this.IsLoggedIn; }
        }

        #endregion

        #region IUserSessionLoginManager<IMember> Members

        ILoginResult<IUser> IUserSessionLoginManager<IMember>.Login(string username, string password, bool fromAdmin)
        {
            return (ILoginResult<IUser>)this.Login(username, password, fromAdmin);
            
        }

        #endregion

        #region IWebUserSessionLoginManager<IMember> Members


        public new ILoginResult<IMember> Login(IUser user, bool fromAdmin, bool autoRedirectToLastURL = false, bool rememberMe = false)
        {
            return (ILoginResult<IMember>) base.Login(user, fromAdmin, autoRedirectToLastURL, rememberMe);
            
        }

        #endregion
    }
}
