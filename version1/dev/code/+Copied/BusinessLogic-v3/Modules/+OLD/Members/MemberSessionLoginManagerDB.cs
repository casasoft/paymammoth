﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Login;
using BusinessLogic_v3.Modules.Members;
using BusinessLogic_v3.DB;


namespace BusinessLogic_v3.Modules.Members
{
    public partial class MemberSessionLoginManagerDB : MemberSessionLoginBase<BusinessLogic_v3.DB.MemberBase>
    {
        private static readonly MemberSessionLoginManagerDB _Instance = new MemberSessionLoginManagerDB(null);
        //public static MemberSessionLoginManager _Instance = null;
        public static MemberSessionLoginManagerDB Instance
        {
            get
            {
                return _Instance;
            }
        }
        public MemberSessionLoginManagerDB(string welcomeURL)
            : base(welcomeURL, loginID: "MemberUserLogin")
        {
        }

        protected override MemberBase _getTemporaryUser()
        {
            MemberBase m = (MemberBase)base._getTemporaryUser();
            if (m != null && !m._Temporary_Flag) //if not temporary member, then clear logged in user
            {
                clearTemporaryUser();
                m = (MemberBase)base._getTemporaryUser();
            }
            if (m != null)
            {
                if (m._Temporary_LastUpdOn < CS.General_v3.Util.Date.Now.AddHours(-1)) //only save once per hour
                {
                    
                    m.Update(savePermanently: false); //this is done so as to update the temporary last updated time
                }
            }
            return m;
        }

        


        public new MemberBase GetLoggedInUser()
        {
            return (MemberBase)base.GetLoggedInUser();
        }
        protected override bool checkPassword(MemberBase user, string password)
        {
            MemberBase member = (MemberBase)user;
            return member.CheckPassword(password);
            
        }

        public override IEnumerable<MemberBase> GetUsersList(string username)
        {
            var members = MemberBase.Factory.GetMembersByUsernameOrEmail(username);
            return members;
        }

        

        /*if (BusinessLogic_v3.Modules.ModuleSettings.Members.ChangeSessionGUIDEveryLogin || string.IsNullOrEmpty(this.SessionGUID))
            {
                GenerateSessionGUIDAndSave();
            }*/
        

        
    }
}
