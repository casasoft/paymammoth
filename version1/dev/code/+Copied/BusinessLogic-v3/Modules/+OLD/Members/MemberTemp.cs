﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Ordering;
using CS.General_v3.Classes.Login;

namespace BusinessLogic_v3.Modules.Members
{
    public class MemberTemp : BaseItemDataTemp, IMember
    {
        public MemberTemp()
        {
            this.Accepted = true;
            this.Activated = true;
            _memberFunctionality = new MemberCommonFunc(this);
        }
        private MemberCommonFunc _memberFunctionality = null;
       

        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string Password { get; set; }
        public List<OrderTemp> OrderHistoryList { get; set; }
        public OrderTemp CurrentShoppingCart { get; set; }
        public override string ToString()
        {
            return Name + " " + Surname;
        }

        public bool ReceivesNewsletter { get; set; }

        public string GetAddressAsString()
        {
            return "Joe Smith,<br/> Sun Street,<br/> London,<br/> LN1 AAB,<br/> United Kingdom";
        }




        public IEnumerable<IOrder> GetOrderHistory()
        {
            return this.OrderHistoryList;
            
        }


        public string Username{get;set;}

        public string SessionGUID{get;set;}

        public bool Activated{get;set;}

        public bool Accepted{get;set;}

        public bool AccountTerminated{get;set;}
        
        public DateTime? LastLoggedIn{get;set;}

        public DateTime? _LastLoggedIn_New { get; set; }

        public string FullName
        {
            get { return CS.General_v3.Util.Text.AppendStrings(" ", this.Name, this.Surname); }
        }

        public void LoggedIn()
        {

        }

        public void LoggedInFromAdmin()
        {

        }

        public void LoggedOut()
        {
        }



        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? AddressCountry { get; set; }
        public string AddressCity { get; set; }

        public string AddressState { get; set; }

        public string AddressZIPCode { get; set; }
        public void CopyMemberAddressDetails(Classes.Interfaces.IMemberAddressDetails copyFrom)
        {
            Classes.Interfaces.Extensions.IMemberAddressDetailsExtensions.CopyMemberAddressDetailsExtension(this, copyFrom);
            
        }

        public string GetAddressAsOneLine(string delimeter = ", ", bool includeCountry = false)
        {

            return Classes.Interfaces.Extensions.IMemberAddressDetailsExtensions.GetAddressAsOneLineExtension(this, delimeter, includeCountry);
            
        }

        public string GetFullName()
        {
            return Classes.Interfaces.Extensions.IMemberAddressDetailsExtensions.GetFullNameExtension(this);
            
        }


        IOrder IMember.GetShoppingCart()
        {
            return this.CurrentShoppingCart;
        }


        public bool VerifyForgottenPasswordCodeAndChange(string resetCode, string newPass)
        {
            
            return true;
        }


        public bool CheckSessionGUID(string guid)
        {
            return _memberFunctionality.CheckSessionGUID(guid);
            
        }

        #region IMember Members


        public IOrder GetLastSuccessfulOrder()
        {


            return this._memberFunctionality.GetLastSuccessfulOrder();
            
        }

        #endregion

        #region IUser Members


        public void GenerateSessionGUIDAndSave()
        {
            this.SessionGUID = CS.General_v3.Util.Random.GetGUID();
            this.Update();
        }

        #endregion

        #region IUser Members


        public bool CheckPassword(string password)
        {
            return this.Password == password;
            
        }

        #endregion

        #region IUser Members


        public void UpdatePasswordWithHashedValue(string hashedPassword, string salt, int totalIterations, CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE encryptionType)
        {
            this.Password = hashedPassword;
            this.PasswordSalt = salt;
            this.PasswordIterations = totalIterations;
            this.PasswordEncryptionType = encryptionType;
        }

        public string PasswordSalt { get; set; }

        public CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE PasswordEncryptionType { get; set; }
        void IUser.Save()
        {
        }

        #endregion

        #region IUser Members


        

        public int PasswordIterations {get;set;}

        #endregion

        #region IUser Members


        public void SetPassword(string password)
        {
            this.Password = password;
            
        }

        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members


        public void InitPropertiesForNewItem()
        {
            throw new NotImplementedException();
        }

        public void resetDirtyFlag()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IUser Members


        public CS.General_v3.Enums.LOGIN_STATUS OnPreLogin()
        {
            return CS.General_v3.Enums.LOGIN_STATUS.Ok;
        }

        #endregion
    }
}
