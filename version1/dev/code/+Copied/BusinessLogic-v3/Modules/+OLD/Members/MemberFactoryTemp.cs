﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Ordering;

namespace BusinessLogic_v3.Modules.Members
{
    public class MemberFactoryTemp :BaseItemDataFactoryTemp<MemberTemp, IMember>,  IMemberFactory
    {
        protected override void initTestRepository()
        {
            this.AddItemToTestRepository(new MemberTemp() { Username= "test", Password="test", Name = "Joe (Test)", Surname="Borg"});
            this.AddItemToTestRepository(new MemberTemp() { Username= "test1", Password="test", Name = "Joe (Test 1)", Surname="Borg"});
            this.AddItemToTestRepository(new MemberTemp() { Username= "test2", Password="test", Name = "Joe (Test 2)", Surname="Borg"});
            base.initTestRepository();
        }


    }
}
