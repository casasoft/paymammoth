﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Categories;
using BusinessLogic_v3.Modules.Ordering;

namespace BusinessLogic_v3.Modules.SpecialOffers
{
    public interface ISpecialOffer : IBaseItemData, ISpecialOfferItem
    {
        ICategory Category { get; }
        DateTime? DateFrom { get; }
        DateTime? DateTo { get; }
        string Title { get; }
        string Name { get; }
        double DiscountPercentage { get; }
        double DiscountFixed { get; }
        int RequiredTotalItemsToBuy { get; }
        double RequiredTotalToSpend { get; }
        int QuantityLeft { get; }
     
        string Description { get; }
        bool RequiresPromoCode { get; }
        //void MarkUsed(IOrder order);
   //     bool IsCurrentlyValid();
       // double MinimumFinalPrice { get; }


        
    }
}
