﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Ordering;

namespace BusinessLogic_v3.Modules.SpecialOffers
{
    public interface ISpecialOfferItem 
    {
        ISpecialOffer GetSpecialOffer();

        void MarkUsed(IOrder orderBase);
        bool IsCurrentlyValid();

    }
}
