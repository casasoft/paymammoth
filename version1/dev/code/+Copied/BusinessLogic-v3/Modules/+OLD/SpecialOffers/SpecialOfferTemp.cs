﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;

namespace BusinessLogic_v3.Modules.SpecialOffers
{
    public class SpecialOfferTemp  :BaseItemDataTemp, ISpecialOffer
    {

        public Categories.ICategory Category {get;set;}

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public string Title { get; set; }

        public string Name { get; set; }

        public double DiscountPercentage { get; set; }

        public double DiscountFixed { get; set; }

        public int RequiredTotalItemsToBuy { get; set; }

        public double RequiredTotalToSpend { get; set; }

        public int QuantityLeft { get; set; }

      //  public int Priority { get; set; }

        public string Description { get; set; }

        public string PromoCode { get; set; }

        public double MinimumFinalPrice { get; set; }

       

        #region ISpecialOffer Members


        public bool RequiresPromoCode {get;set;}

        #endregion

        #region ISpecialOffer Members


        public void MarkUsed(Ordering.IOrder order)
        {
            throw new NotImplementedException();
        }

        public bool IsCurrentlyValid()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ISpecialOfferItem Members

        public ISpecialOffer GetSpecialOffer()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ISpecialOfferItem Members


        public void MarkUsed(DB.OrderBase orderBase)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
