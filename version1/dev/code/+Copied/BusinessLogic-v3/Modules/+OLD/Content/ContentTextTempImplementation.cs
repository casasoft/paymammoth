﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Content
{
    public class ContentTextTempImplementation : BaseItemDataTemp, IContentText
    {
        public ContentTextTempImplementation()
            : this(null)
        {

        }
        public ContentTextTempImplementation(Enum identifierEnumValue = null)
        {
            if (identifierEnumValue != null) this.Identifier = CS.General_v3.Util.EnumUtils.StringValueOf(identifierEnumValue);
            
        }
        #region IContentText Members

        public string Identifier { get; set; }


        public string PlainText { get; set; }

        public string HtmlText { get; set; }

        public string Title
        {
            get;
            set;
        }

        #endregion



        
    }
}
