﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util;
using BusinessLogic_v3.DB;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;
namespace BusinessLogic_v3.Modules.Content
{
    public class ContentPageFactoryTemp : BaseItemDataFactoryTemp<ContentPageTempImplementation, IContentPage>, IContentPageFactory
    {
        public void CreateTestRepositoryInRealDatabase()
        {

            var list = GetRootNodes();
            var root = ContentPageBase.GetRootNode(false);
            foreach (var rootNode in list)
            {
                checkContentPage(root, rootNode);
                
            }
        }
        private ContentPageBase checkContentPage(ContentPageBase parent, IContentPage copyFrom)
        {
            ContentPageBase cp = ContentPageBase.Factory.GetContentPageByIdentifier(copyFrom.Identifier, throwErrorIfNull: false);
            if (cp == null)
            {
                cp = ContentPageBase.CreateNewInstance();

                cp.SetAllAccessRequiredTo(CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal);
                cp.Color = copyFrom.Color;
                cp.CustomLink = copyFrom.CustomLink;
                cp.DoNotShowLastEditedOn = copyFrom.DoNotShowLastEditedOn;
                cp.ExcludeFromRewriteUrl = copyFrom.ExcludeFromRewriteUrl;
                cp.Name = copyFrom.FriendlyName;
                if (string.IsNullOrEmpty(cp.Name ))
                    cp.Name = copyFrom.Identifier;


                cp.HtmlText = copyFrom.HtmlText;
                cp.Identifier = copyFrom.Identifier;
                cp.MetaDescription = copyFrom.MetaDescription;
                cp.MetaKeywords = copyFrom.MetaKeywords;



                cp.PageTitle = ((IHierarchy)copyFrom.GetAsHierarchy()).Title;
                if (string.IsNullOrEmpty(cp.PageTitle))
                    cp.PageTitle = copyFrom.Identifier;
                cp.VisibleInFrontend = copyFrom.VisibleInFrontend;
                cp.CreateAndFlush();
                parent.AddChildContentPage(cp);
                

                
            }
            foreach (var child in copyFrom.GetChildPages())
            {


                checkContentPage(cp, child);
            }
            return cp;
        }

        private ContentPageFactoryCommonFunctionality factoryCommonFunc = null;
        public ContentPageFactoryTemp()
        {
            this.factoryCommonFunc = new ContentPageFactoryCommonFunctionality(this);
        }
        public IEnumerable<IContentPage> GetRootNodes()
        {
            var items = this.GetItemsInRepository();
            return items.FindAll(item => item.ParentContentPage == null);
            
        }
        public IContentPage GetRoot()
        {
            var items = this.GetItemsInRepository();
            return items.FirstOrDefault();
            

        }
        private void addContentPageChildrenToTestRepository(IContentPage cp)
        {
            var children = cp.GetChildPages();
            foreach (var x in children)
            {
                ContentPageTempImplementation xCP = (ContentPageTempImplementation)x;
                xCP.ParentContentPage = cp;
                var cpRepository = this.GetByPrimaryKey(((IBaseItemData)x).ID);
                if (cpRepository == null)
                {
                    this.AddItemToTestRepository((ContentPageTempImplementation)x);
                    //addContentPageChildrenToTestRepository(x);
                }
            }
        }

        public override void AddItemToTestRepository(ContentPageTempImplementation item)
        {
            base.AddItemToTestRepository(item);
            addContentPageChildrenToTestRepository(item);
        }
        protected override void initTestRepository()
        {
            base.initTestRepository();
        }

       
        public IContentPage GetContentPageByIdentifier(Enum enumValue, bool loadFromCache = true)
        {
            string sIdentifier = enumValue.StringValueOf();

            var items = this.GetItemsInRepository();

            var cp = items.Find(item => string.Compare(item.Identifier, sIdentifier, true) == 0);
            if (cp == null)
            {
                throw new InvalidOperationException("Invalid content page with identifier <" + sIdentifier + ">");
            }
            return cp;
            
        }

        #region IContentPageFactory Members


        public IContentPage GetContentPageByRouteParameter()
        {
            return this.factoryCommonFunc.GetContentPageByRouteParameter();
        }

        #endregion
    }
}
