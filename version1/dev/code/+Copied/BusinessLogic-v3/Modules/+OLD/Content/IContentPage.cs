﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;

namespace BusinessLogic_v3.Modules.Content
{
    public interface IContentPage : IBaseItemData
    {
        bool DoNotShowLastEditedOn { get; }
        string Identifier { get; }
        string CustomRewriteURL { get; }
        string HtmlText { get; }
        string FriendlyName { get; }
        //string Title { get; }
        [Obsolete("Use 'PageTitle' instead")]
        string Title { get; }
        string PageTitle { get; }
        string CustomLink { get; }
        string Color { get; }

        DateTime LastEditedOn { get; }

        void AddChildPage(IContentPage cp);
        
        IEnumerable<IContentPage> GetChildPages();
        bool VisibleInFrontend { get; }
        /// <summary>
        /// Whether content page has got content i.e. clickable
        /// </summary>
        /// <returns></returns>
        bool ContainsContent();
        /// <summary>
        /// Whether includes children pages
        /// </summary>
        /// <returns></returns>
        bool HasChildPages();
        
        string GetUrl(bool fullyQualifiedUrl = true, string langCode = null);
        string GetThumbnailURL();
        string MetaTitle { get;  }
        string MetaKeywords { get; set; }
        string MetaDescription { get; set; }
        bool ExcludeFromRewriteUrl { get; }

        IContentPage ParentContentPage { get; }

        JSObject GetAsJsObject(bool forFlash = false, int maxDepth = 0, int currDepth = 0);

        IHierarchy GetAsHierarchy();
        void RedirectToPage();
        
    }
}
