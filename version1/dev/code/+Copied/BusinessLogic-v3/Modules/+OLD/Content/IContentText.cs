﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Content
{
    public interface IContentText : IBaseItemData
    {
        string Title { get; }
        string Identifier { get; }
        string PlainText { get; }
        string HtmlText { get; }
    }
}
