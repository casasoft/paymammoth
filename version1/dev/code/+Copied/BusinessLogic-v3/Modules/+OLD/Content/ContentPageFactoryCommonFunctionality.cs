﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Content
{
    public class ContentPageFactoryCommonFunctionality
    {
        public IContentPageFactory Factory { get; private set; }
        public ContentPageFactoryCommonFunctionality(IContentPageFactory factory)
        {
            this.Factory = factory;
        }

        public long? GetContentPageIDFromRouteParam()
        {
            object o = CS.General_v3.Util.PageUtil.GetRouteDataVariable(ContentPageCommonFunctionality.ROUTING_PARAM_ID);
            long? idResult = null;
            if (o != null)
            {
                string s = o.ToString();
                long id = 0;
                if (long.TryParse(s, out id)) idResult = id;


            }
            return idResult;
        }
        /// <summary>
        /// Retrieves the content page based on the route parameter
        /// </summary>
        /// <param name="pg"></param>
        /// <returns></returns>
        public IContentPage GetContentPageByRouteParameter()
        {
            if (ContentPageCommonFunctionality.ROUTING_INCLUDE_CULTURE)
            {
                //Update language code
                object oLang = CS.General_v3.Util.PageUtil.GetRouteDataVariable(ContentPageCommonFunctionality.ROUTING_PARAM_CULTURE);
                if (oLang != null)
                {
                    string sLangCode = oLang.ToString();
                    BusinessLogic_v3.Settings.SettingsMain.CurrentCultureManager.SetCultureByCode(sLangCode);
                }

            }
            var id = GetContentPageIDFromRouteParam();
            IContentPage cp = null;
            if (id != null)
            {
                cp = Factory.GetByPrimaryKey(id.Value);

            }
            return cp;

        }
    }
}
