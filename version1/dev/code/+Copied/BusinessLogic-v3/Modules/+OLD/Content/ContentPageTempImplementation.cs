﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;

namespace BusinessLogic_v3.Modules.Content
{
    public class ContentPageTempImplementation :BaseItemDataTemp, IContentPage, IHierarchy
    {
        public string ThumbnailURL { get; set; }
        private ContentPageCommonFunctionality _commonFunctionality;
        public ContentPageTempImplementation()
            : this(null)
        {
            this.VisibleInFrontend = true;
            this.Identifier = "TempIdentifier_" + this.ID;

        }
        public void SetIdentifier(Enum identifier)
        {
            this.Identifier = CS.General_v3.Util.EnumUtils.StringValueOf(identifier);
        }
        public ContentPageTempImplementation(Enum identifierEnumValue = null)
        {
            if (identifierEnumValue != null) this.Identifier = CS.General_v3.Util.EnumUtils.StringValueOf(identifierEnumValue);
            

            _commonFunctionality = new ContentPageCommonFunctionality(this);
            this.Children = new List<ContentPageTempImplementation>();
            this.LastEditedOn = CS.General_v3.Util.Date.Now;
        }




        #region IContentPage Members

        
        public string Identifier { get; set; }

        public string HtmlText { get; set; }

        public string CustomLink { get; set; }

        public string Title { get; set; }


        public string GetThumbnailURL()
        {
            return ThumbnailURL;
        }


        string IContentPage.FriendlyName
        {
            get
            {
                return this.Title;
            }
        }
        public string Color { get; set; }

        public bool VisibleInFrontend { get; set; }

        public IList<ContentPageTempImplementation> Children { get; private set; }


        public bool ExcludeFromRewriteUrl { get; set; }

        public IContentPage ParentContentPage { get; set; }

        public JSObject GetAsJsObject(bool forFlash = false, int maxDepth = 0, int currDepth = 0)
        {
            return _commonFunctionality.GetJsObject(forFlash, maxDepth, currDepth);
        }

      


        public string GetUrl(bool fullyQualifiedUrl = true, string langCode = null)
        {
            return _commonFunctionality.GetUrl(fullyQualifiedUrl, langCode);
        }

        #endregion

        
        public IEnumerable<IContentPage> GetChildPages()
        {
            return (IEnumerable<IContentPage>)this.Children;
            
        }

        public bool ContainsContent()
        {
            return this._commonFunctionality.ContainsContent();
            
        }

        public bool HasChildPages()
        {
            return this._commonFunctionality.HasChildPages();
            
        }

        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }




        public void AddChildPage(ContentPageTempImplementation cp)
        {
            cp.ParentContentPage = this;
            this.Children.Add(cp);
        }
        public void AddChildPage(IContentPage cp)
        {
            AddChildPage((ContentPageTempImplementation)cp);
            
        }




        public DateTime LastEditedOn { get; set; }



      

        public bool Selected {get;set;}

        public bool DoNotShowLastEditedOn {get;set;}



        #region IContentPage Members



        public void RedirectToPage()
        {
            _commonFunctionality.RedirectToPage();


        }


        #endregion

        #region IContentPage Members


        public string PageTitle {get;set;}

        #endregion

        #region IContentPage Members


        public IHierarchy GetAsHierarchy()
        {
            return this;
            
            
        }

        #endregion

        #region IHierarchy Members


        public string Href
        {
            get { return this.GetUrl(); }
        }

        public CS.General_v3.Enums.HREF_TARGET HrefTarget
        {
            get { return CS.General_v3.Enums.HREF_TARGET.Self; }
        }

        public IEnumerable<IHierarchy> GetChildren()
        {
            return this.Children;
            
        }

        public IHierarchy ParentHierarchy
        {
            get { return this.ParentContentPage.GetAsHierarchy(); }
        }

        public string GetFullHierarchyName(string delimiter, bool reverseOrder, bool includeRoot)
        {
            return null;
        }

        public bool Visible
        {
            get { return this.VisibleInFrontend; }
        }

        #endregion

        #region IContentPage Members


        public string CustomRewriteURL
        {
            get;
            set;
        }

        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members


        public void InitPropertiesForNewItem()
        {
            throw new NotImplementedException();
        }

        public void resetDirtyFlag()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IContentPage Members

        public string MetaTitle
        {
            get;
            set;
        }

        #endregion

        #region IHierarchy Members


        public IEnumerable<IHierarchy> GetChildren(bool loadNonVisible = false)
        {
            return this.Children;
        }

        public IEnumerable<IHierarchy> GetParents(bool loadNonVisible = false)
        {
            return CS.General_v3.Util.ListUtil.GetListFromSingleItem( this.ParentContentPage).Cast<IHierarchy>();
            
        }

        #endregion
    }
}
