﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util;
namespace BusinessLogic_v3.Modules.Content
{
    public class ContentTextFactoryTemp :BaseItemDataFactoryTemp<ContentTextTempImplementation, IContentText>, IContentTextFactory
    {



        public IContentText GetContentTextByIdentifier(Enum enumValue)
        {
            string sIdentifier = enumValue.StringValueOf();

            var items = this.GetItemsInRepository();

            return items.Find(item => string.Compare(item.Identifier, sIdentifier, true) == 0);
        }
    }
}
