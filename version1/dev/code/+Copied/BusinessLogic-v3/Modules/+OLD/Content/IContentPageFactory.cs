﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BusinessLogic_v3.Modules.Content
{
    public interface IContentPageFactory : IBaseItemDataFactory<IContentPage> 
    {
        IContentPage GetRoot();
        IContentPage GetContentPageByIdentifier(Enum enumValue, bool loadFromCache);
        IContentPage GetContentPageByRouteParameter();
    }
}
