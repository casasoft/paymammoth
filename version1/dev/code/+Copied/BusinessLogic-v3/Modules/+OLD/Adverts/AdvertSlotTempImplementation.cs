﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Adverts
{
    public class AdvertSlotTempImplementation : BaseItemDataTemp, IAdvertSlot
    {

        private List<AdvertTempImplementation> _adverts;

        public AdvertSlotColumnTempImplementation ParentAdvertSlotColumn { get; set; }
        public AdvertSlotTempImplementation()
        {
            this._adverts = new List<AdvertTempImplementation>();
        }



        #region IAdvertSlot Members

        public string Identifier { get; set; }

        public IAdvert GetAdvert()
        {
            if (_adverts.Count > 0)
            {
                return _adverts[CS.General_v3.Util.Random.GetInt(0, _adverts.Count - 1)];
            }
            else
            {
                return null;
            }
        }
        public void AddAdvert(AdvertTempImplementation advert)
        {
            this._adverts.Add(advert);
            advert.ParentAdvertSlot = this;
        }
        public IEnumerable<IAdvert> GetAdvertsInSlot()
        {
            return _adverts;
        }

        #endregion



        #region IAdvertSlot Members


        public IAdvertSlotColumn GetParentAdvertSlotColumn()
        {
            return ParentAdvertSlotColumn;
        }

        #endregion

        #region IAdvertSlot Members


        public int Width {get;set;}

        public int Height {get;set;}

        #endregion
    }
}
