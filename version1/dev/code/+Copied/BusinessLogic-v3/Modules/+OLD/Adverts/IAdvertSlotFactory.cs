﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BusinessLogic_v3.Modules.Adverts
{
    public interface IAdvertSlotFactory : IBaseItemDataFactory<IAdvertSlot>
    {
        IAdvertSlot GetAdvertSlotByIdentifier(string identifier);
    }
}
