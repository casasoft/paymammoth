﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DB=BusinessLogic_v3.DB;

namespace BusinessLogic_v3.Modules.Adverts
{
    public class AdvertSlotColumnFactoryTempImplementation : BaseItemDataFactoryTemp<AdvertSlotColumnTempImplementation, IAdvertSlotColumn>, IAdvertSlotColumnFactory
    {

        public AdvertSlotColumnFactoryTempImplementation()
        {
        }

        public IAdvertSlotColumn GetAdvertSlotColumnByIdentifier(string identifier)
        {
            return this.GetItemsInRepository().Find(item => item.Identifier == identifier);
        }

        public void CreateTestRepositoryInRealDatabase(AdvertSlotFactoryTempImplementation slotFactoryTemp)
        {
            var list = GetItemsInRepository();
            foreach (var item in list)
            {
                checkAdvertColumn(item);
                slotFactoryTemp.CreateTestRepositoryInRealDatabase(item);
                

            }
        }
        private DB.AdvertColumnBase checkAdvertColumn(IAdvertSlotColumn copyFrom)
        {
            DB.AdvertColumnBase item = DB.AdvertColumnBase.Factory.GetColumnByIdentifier(copyFrom.Identifier);
            if (item == null)
            {
                item = DB.AdvertColumnBase.CreateNewInstance();
                item.Identifier = copyFrom.Identifier;
                item.Title = copyFrom.Identifier;
                item.CreateAndFlush();
            }
            return item;
        }


    }
}
