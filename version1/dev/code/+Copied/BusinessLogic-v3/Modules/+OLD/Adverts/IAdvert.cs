﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Adverts
{
    public interface IAdvert : IBaseItemData
    {
        string Href { get; }
        CS.General_v3.Enums.HREF_TARGET HrefTarget { get; }
        string MediaItemURL { get; }
        string Title { get; }
        string Description { get; }

        IAdvertSlot GetParentAdvertSlot();
    }
}
