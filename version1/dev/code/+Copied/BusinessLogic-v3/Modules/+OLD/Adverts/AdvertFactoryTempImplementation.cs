﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Adverts
{
    public class AdvertFactoryTempImplementation : BaseItemDataFactoryTemp<AdvertTempImplementation, IAdvert>, IAdvertFactory
    {
        public override void AddItemToTestRepository(AdvertTempImplementation item)
        {
            throw new NotSupportedException("Add adverts to AdvertSlotColumnFactory not here");
        }
        public override AdvertTempImplementation GetByPrimaryKey(long id)
        {
            return GetItemsInRepository().Find(item => item.ID == id);
        }
        public override List<AdvertTempImplementation> GetItemsInRepository()
        {
            var advertSlots = Factories.AdvertSlotFactory.GetAllItemsInRepository();
            List<AdvertTempImplementation> ads = new List<AdvertTempImplementation>();
            foreach (var advertSlot in advertSlots)
            {
                var adSlotAds = advertSlot.GetAdvertsInSlot();
                foreach (var advert in adSlotAds)
                {
                    ads.Add((AdvertTempImplementation)advert);
                }
            }
            return ads;
        }

        //public void CreateTestRepositoryInRealDatabase(IAdvertSlot parent)
        //{


        //    var list = GetItemsInRepository();
        //    var dbList = DB.AdvertBase.FindAll();
        //    foreach (var item in list)
        //    {
        //        if (parent == null || (string.Compare(item.ParentAdvertSlot.Identifier, parent.Identifier, true) == 0))
        //        {
        //            checkAdvert(parent, item);
        //        }


        //    }
        //}
        //private DB.AdvertBase checkAdvert(IAdvertSlot parent, DB.AdvertBaseList dbList, IAdvert copyFrom)
        //{


        //    DB.AdvertBase item = DB.AdvertBase.Factory.GetAdvertSlotByIdentifier(copyFrom.Identifier);
        //    if (item == null)
        //    {
        //        if (parent != null)
        //        {
        //            item.AdvertColumn = DB.AdvertColumnBase.Factory.GetColumnByIdentifier(parent.Identifier);
        //        }
        //        item.Identifier = copyFrom.Identifier;
        //        item.Height = copyFrom.Height;
        //        item.Title = copyFrom.Identifier;
        //        item.Width = copyFrom.Width;
        //        item.CreateAndFlush();
        //    }
        //    return item;
        //}


    }
}
