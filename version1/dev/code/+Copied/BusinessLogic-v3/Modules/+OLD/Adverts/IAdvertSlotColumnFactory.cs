﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BusinessLogic_v3.Modules.Adverts
{
    public interface IAdvertSlotColumnFactory : IBaseItemDataFactory<IAdvertSlotColumn>
    {
        IAdvertSlotColumn GetAdvertSlotColumnByIdentifier(string identifier);
    }
}
