﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Adverts
{
    public class AdvertTempImplementation : BaseItemDataTemp, IAdvert
    {

        #region IAdvert Members

        public string Href { get; set; }

        public string MediaItemURL { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        #endregion

        #region IAdvert Members

        public AdvertSlotTempImplementation ParentAdvertSlot { get; set; }
        public IAdvertSlot GetParentAdvertSlot()
        {
            return ParentAdvertSlot;
        }






        #endregion

        #region IAdvert Members


        public CS.General_v3.Enums.HREF_TARGET HrefTarget { get; set; }

        #endregion
    }
}
