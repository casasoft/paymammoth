﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Adverts
{
    public class AdvertSlotColumnTempImplementation : BaseItemDataTemp, IAdvertSlotColumn
    {

        private List<AdvertSlotTempImplementation> _advertSlots { get; set; }


        public string Identifier { get; set; }

        public AdvertSlotColumnTempImplementation()
        {
            this._advertSlots = new List<AdvertSlotTempImplementation>();
            this.Identifier = "TempIdentifier_" + this.ID;
        }

        public void AddAdvertSlot(AdvertSlotTempImplementation advertSlot)
        {
            this._advertSlots.Add(advertSlot);
            advertSlot.ParentAdvertSlotColumn = this;
        }
        public IEnumerable<IAdvertSlot> GetAdvertSlots()
        {
            return _advertSlots;
        }


       
    }
}
