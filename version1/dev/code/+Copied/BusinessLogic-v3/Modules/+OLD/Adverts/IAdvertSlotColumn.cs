﻿using System;
using System.Collections.Generic;
namespace BusinessLogic_v3.Modules.Adverts
{
    public interface IAdvertSlotColumn : IBaseItemData
    {
        string Identifier { get; }

        IEnumerable<IAdvertSlot> GetAdvertSlots();
    }
}
