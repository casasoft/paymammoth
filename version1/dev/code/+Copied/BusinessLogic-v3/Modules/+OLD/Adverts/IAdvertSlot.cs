﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace BusinessLogic_v3.Modules.Adverts
{
    public interface IAdvertSlot :IBaseItemData
    {
        string Identifier { get; }

        IAdvert GetAdvert();
        int Width { get; }
        int Height { get; }
        IEnumerable<IAdvert> GetAdvertsInSlot();

        IAdvertSlotColumn GetParentAdvertSlotColumn();

    }
}
