﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BusinessLogic_v3.Modules.Adverts
{
    public class AdvertSlotFactoryTempImplementation : BaseItemDataFactoryTemp<AdvertSlotTempImplementation, IAdvertSlot>, IAdvertSlotFactory
    {

        public void CreateTestRepositoryInRealDatabase(IAdvertSlotColumn parent = null)
        {


            var list = GetItemsInRepository();
            foreach (var item in list)
            {
                if (parent == null || (string.Compare(item.ParentAdvertSlotColumn.Identifier, parent.Identifier, true) == 0))
                {
                    checkAdvertSlot(parent, item);
                }


            }
        }
        private DB.AdvertSlotBase checkAdvertSlot(IAdvertSlotColumn parent, IAdvertSlot copyFrom)
        {
            DB.AdvertSlotBase item = DB.AdvertSlotBase.Factory.GetAdvertSlotByIdentifier(copyFrom.Identifier);
            if (item == null)
            {
                item = DB.AdvertSlotBase.CreateNewInstance();
                if (parent != null)
                {
                    item.AdvertColumn = DB.AdvertColumnBase.Factory.GetColumnByIdentifier(parent.Identifier);
                }
                item.Identifier = copyFrom.Identifier;
                item.Height = copyFrom.Height;
                item.Title = copyFrom.Identifier;
                item.Width = copyFrom.Width;
                item.CreateAndFlush();
                createAdvertsForSlotFromTestInfo(copyFrom, item);
            }
            return item;
        }
        private void createAdvertsForSlotFromTestInfo(IAdvertSlot advertSlot, DB.AdvertSlotBase dbSlot)
        {
            foreach (var ad in advertSlot.GetAdvertsInSlot())
            {
                var dbAd = dbSlot.AddNewAdvert();
                dbAd.Activated = true;
                dbAd.AdvertShownPerRound = 1;
                dbAd.Description = ad.Description;


                dbAd.HrefTarget = ad.HrefTarget;
                dbAd.Link = ad.Href;
                dbAd.Title = ad.Title;
                dbAd.ShowDateFrom = new DateTime(2000, 1, 1);
                dbAd.ShowDateTo = new DateTime(2099, 1, 1);

                dbAd.CreateAndFlush();

                {
                    string localPath = CS.General_v3.Util.PageUtil.MapPath(ad.MediaItemURL);
                    string filename = CS.General_v3.Util.IO.GetFilenameAndExtension(localPath);
                    if (File.Exists(localPath))
                    {
                        FileStream fs = new FileStream(localPath, FileMode.Open, FileAccess.Read);
                        dbAd.Image.UploadFile(fs, filename);
                        fs.Dispose();
                    }
                }
            }

        }


        #region IAdvertSlotFactory Members

        public IAdvertSlot GetAdvertSlotByIdentifier(string identifier)
        {
            return this.GetItemsInRepository().Find(item => item.Identifier == identifier);
        }


        public override void AddItemToTestRepository(AdvertSlotTempImplementation item)
        {
            //throw new NotSupportedException("Add advert slots to AdvertSlotColumnFactory not here");
            base.AddItemToTestRepository(item);
        }
        public override AdvertSlotTempImplementation GetByPrimaryKey(long id)
        {
            return this.GetItemsInRepository().Find(item => item.ID == id);
        }

        //public override List<AdvertSlotTempImplementation> GetItemsInRepository()
        //{
        //    var columns = Factories.AdvertSlotColumnFactory.GetAllItemsInRepository();
        //    List<AdvertSlotTempImplementation> advertSlots = new List<AdvertSlotTempImplementation>();
        //    foreach (var column in columns)
        //    {
        //        var colAdvertSlots = column.GetAdvertSlots();
        //        foreach (var ad in colAdvertSlots)
        //        {
        //            advertSlots.Add((AdvertSlotTempImplementation)ad);
        //        }
        //    }
        //    return advertSlots;
        //}

        


        #endregion
    }
}
