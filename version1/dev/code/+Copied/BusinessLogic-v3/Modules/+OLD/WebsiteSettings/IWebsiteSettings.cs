﻿using System;
namespace BusinessLogic_v3.Modules.WebsiteSettings
{
    public interface IWebsiteSettings
    {

        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 CompanyCallingCodePrefix { get;  }
        CS.General_v3.Classes.Address.IAddress CompanyAddress { get;  }
        string CompanyEmail { get; }
        string CompanyFacebook { get; }
        string CompanyFaxWithoutPrefix { get; }
        double CompanyLatitude { get; }
        double CompanyLongitude { get; }
        string CompanyMobileWithoutPrefix { get; }
        string CompanySkype { get; }
        string CompanyTelephoneWithoutPrefix { get; }
        string CompanyTwitter { get; }
        string CompanyWebsite { get; }
        string GetCompanyContactWithPrefix(string companyContactWithoutPrefix, string addStringPrefix, string addStringSuffix); 
    }
}
