﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Address;

namespace BusinessLogic_v3.Modules.WebsiteSettings
{
    public class WebsiteSettingsTempImplementation : BusinessLogic_v3.Modules.WebsiteSettings.IWebsiteSettings
    {
        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 CompanyCallingCodePrefix { get; set; }
        public string CompanyTelephoneWithoutPrefix { get; set; }
        public string CompanyFaxWithoutPrefix { get; set; }
        public string CompanyMobileWithoutPrefix { get; set; }
        public IAddress CompanyAddress { get; set; }
        public string CompanyWebsite { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanySkype { get; set; }
        public string CompanyFacebook { get; set; }
        public string CompanyTwitter { get; set; }
        public double CompanyLatitude { get; set; }
        public double CompanyLongitude { get; set; }

        public string GetAddressAsOneLine(string delimeter = ", ")
        {
            string address = CS.General_v3.Util.Text.AppendStrings(delimeter, CompanyAddress.CompanyName, 
                CompanyAddress.Address1, CompanyAddress.Address2,
                CompanyAddress.City);
            if (!string.IsNullOrWhiteSpace(CompanyAddress.ZIPPostCode))
                address += ". " + CompanyAddress.ZIPPostCode;
            return address;
        }

        public string GetAddressAsHTML()
        {
           return GetAddressAsOneLine("<br/>");
        }

        public string GetCompanyContactWithPrefix(string companyContactWithoutPrefix, string addStringPrefix, string addStringSuffix)
        {
            string finalString;
            if(!String.IsNullOrWhiteSpace(addStringPrefix) || !(String.IsNullOrWhiteSpace(addStringSuffix)))
            {
                var callingPrefix = CS.General_v3.Util.Data.GetCountryCallingCode(CompanyCallingCodePrefix);
                finalString = addStringPrefix + callingPrefix + addStringSuffix + companyContactWithoutPrefix;
            }
            else
            {
                var callingPrefix = CS.General_v3.Util.Data.GetCountryCallingCode(CompanyCallingCodePrefix);
                finalString = callingPrefix + companyContactWithoutPrefix;
            }
            return finalString;
        }
    }
}
