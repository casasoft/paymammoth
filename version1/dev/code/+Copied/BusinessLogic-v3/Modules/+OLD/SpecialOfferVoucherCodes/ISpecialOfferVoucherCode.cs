﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Categories;
using BusinessLogic_v3.Modules.SpecialOffers;

namespace BusinessLogic_v3.Modules.SpecialOfferVoucherCodes
{
    public interface ISpecialOfferVoucherCode : IBaseItemData
    {
        string VoucherCode { get; }
        ISpecialOffer SpecialOffer { get; }

        
    }
}
