﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;

namespace BusinessLogic_v3.Modules.SpecialOfferVoucherCodes
{
    public class SpecialOfferVoucherCodeTemp : BaseItemDataTemp, ISpecialOfferVoucherCode
    {

        #region ISpecialOfferVoucherCode Members

        public string VoucherCode {get;set;}

        public SpecialOffers.ISpecialOffer SpecialOffer { get; set; }

        #endregion
    }
}
