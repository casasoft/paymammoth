﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Currencies
{
    public class CurrencyCommonFunc
    {
        public ICurrency Currency { get; private set; }
        public CurrencyCommonFunc(ICurrency currency)
        {
            this.Currency = currency;
        }
        public CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 GetCurrencyISOAsEnumValue()
        {
            return CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217FromCode(this.Currency.CurrencyISOCode);
            
        }


        /// <summary>
        /// Converts the item with the exchange rate.  The paramter price is MULTIPLIED with the exchange rate
        /// </summary>
        /// <param name="dPrice"></param>
        /// <returns></returns>
        public double ConvertPriceBasedOnExchangeRate(double dPrice)
        {
            return dPrice * this.Currency.ExchangeRateMultiplier;

        }

        /// <summary>
        /// Converts the item back to the base price, with the exchange rate.  The paramter price is DIVIDED with the exchange rate
        /// </summary>
        /// <param name="dPrice"></param>
        /// <returns></returns>
        public double GetBasePriceFromConvertedPrice(double dPrice)
        {
            return dPrice / this.Currency.ExchangeRateMultiplier;

        }

        public void SetCurrencyCodeFromISOEnum(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 currency)
        {
            this.Currency.CurrencyISOCode = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode(currency);
        }


        public string FormatPriceAsHTML(double d, IFormatProvider formatProvider = null)
        {

            if (formatProvider == null)
                formatProvider = BusinessLogic_v3.Settings.SettingsMain.CurrentCultureManager.GetCurrentCultureFormatProvider();


            string s = CS.General_v3.Util.Number.FormatNumber(d, CS.General_v3.Util.Number.NUMBER_FORMAT_TYPE.Currency, formatProvider, this.Currency.HtmlCode);
            return s;
        }
        public string FormatPriceAsUnicodeText(double d, IFormatProvider formatProvider = null)
        {
            if (formatProvider == null)
                formatProvider = BusinessLogic_v3.Settings.SettingsMain.CurrentCultureManager.GetCurrentCultureFormatProvider();


            string s = CS.General_v3.Util.Number.FormatNumber(d, CS.General_v3.Util.Number.NUMBER_FORMAT_TYPE.Currency, formatProvider, this.Currency.UnicodeText);
            return s;

        }

    }
}
