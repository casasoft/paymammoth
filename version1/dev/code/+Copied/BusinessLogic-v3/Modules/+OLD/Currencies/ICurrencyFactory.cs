﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BusinessLogic_v3.Modules.Currencies
{
    public interface ICurrencyFactory : IBaseItemDataFactory<ICurrency> 
    {
        ICurrency GetByCurrencyCode(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 code);
        ICurrency GetByCurrencyCode(string code);
        //ICurrency GetDefaultCurrency();

    }
}
