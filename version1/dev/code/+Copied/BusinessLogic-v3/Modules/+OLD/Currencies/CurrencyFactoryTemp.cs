﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Ordering;

namespace BusinessLogic_v3.Modules.Currencies
{
    public class CurrencyFactoryTemp : BaseItemDataFactoryTemp<CurrencyTemp, ICurrency>, ICurrencyFactory
    {

        protected override void initTestRepository()
        {
            {
                var curr = new CurrencyTemp();
                curr.CurrencyISOCode = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode( CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro);
                curr.ExchangeRateMultiplier = 1;
                curr.HtmlCode = "&euro;";
                curr.Title = "Euro";
                curr.UnicodeText = "€";
                this.AddItemToTestRepository(curr);
            }
            {
                var curr = new CurrencyTemp();
                curr.CurrencyISOCode = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedKingdomPounds);
                curr.ExchangeRateMultiplier = 0.851477693;
                curr.HtmlCode = "&pound;";
                curr.Title = "Pounds";
                curr.UnicodeText = "£";
                this.AddItemToTestRepository(curr);
            }
            {
                var curr = new CurrencyTemp();
                curr.CurrencyISOCode = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedStatesOfAmericaDollars);
                curr.ExchangeRateMultiplier = 1.35700;
                curr.HtmlCode = "$";
                curr.Title = "Dollar";
                curr.UnicodeText = "$";
                this.AddItemToTestRepository(curr);
            }

            base.initTestRepository();
        }

        public ICurrency GetByCurrencyCode(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 code)
        {
            string sCode = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode(code);

            return GetByCurrencyCode(sCode);
        }

        public ICurrency GetByCurrencyCode(string code)
        {
            return this.GetItemsInRepository().Find(item => string.Compare(item.CurrencyISOCode, code, true) == 0);
            
        }

        public ICurrency GetDefaultCurrency()
        {
            var currencieis = GetItemsInRepository();
            foreach (var item in currencieis)
            {
                if (item.ExchangeRateMultiplier == 1)
                    return item;
            }
            return currencieis[0];
        }
    }
}
