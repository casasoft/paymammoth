﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Categories;

namespace BusinessLogic_v3.Modules.Currencies
{
    public interface ICurrency : IBaseItemData
    {
        string Title { get; }
        string CurrencyISOCode { get; set; }
        CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 GetCurrencyISOAsEnumValue();
        string HtmlCode { get; }
        double ExchangeRateMultiplier { get; }
        string UnicodeText { get; }
        


    }
}
