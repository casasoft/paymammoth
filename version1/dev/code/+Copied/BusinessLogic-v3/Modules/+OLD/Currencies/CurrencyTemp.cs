﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;

namespace BusinessLogic_v3.Modules.Currencies
{
    public class CurrencyTemp  :BaseItemDataTemp, ICurrency
    {
        public CurrencyTemp()
        {
            _commonFunc = new CurrencyCommonFunc(this);
        }
        private CurrencyCommonFunc _commonFunc = null;

        public string Title {get;set;}

        public string CurrencyISOCode { get; set; }
       

        public CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 GetCurrencyISOAsEnumValue()
        {
            return _commonFunc.GetCurrencyISOAsEnumValue();
            
        }

        public string HtmlCode {get;set;}

        public double ExchangeRateMultiplier { get; set; }

        public string UnicodeText { get; set; }
    }
}
