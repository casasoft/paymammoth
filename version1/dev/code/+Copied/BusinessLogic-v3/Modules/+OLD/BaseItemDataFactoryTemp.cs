﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules
{
    public abstract class BaseItemDataFactoryTemp<TItemTemp, TInterface> : IBaseItemDataFactory<TInterface>
        where TItemTemp: BaseItemDataTemp, IBaseItemData, new()
        where TInterface: IBaseItemData
        
        
    {



        public BaseItemDataFactoryTemp()
        {
            initTestRepository();
        }
        protected virtual void initTestRepository()
        {

        }

        protected Dictionary<long, TItemTemp> _dict = new Dictionary<long, TItemTemp>();

        public virtual List<TItemTemp> GetItemsInRepository()
        {
            List<TItemTemp> list = new List<TItemTemp>();
            foreach (var key in _dict)
            {
                var item = _dict[key.Key];
                if (!item.IsDeleted)
                {
                    list.Add(item);
                }
            }
            return list;
            
        }

       // private long _lastPKey = 0;

        public virtual void AddItemToTestRepository(TItemTemp item)
        {
           // _lastPKey++;
            //item.ID = _lastPKey;
            _dict[item.ID] = item;
        }

        public virtual TItemTemp GetByPrimaryKey(long id)
        {
            if (_dict.ContainsKey(id))
            {
                return _dict[id];
            }
            else
            {
                return null;
            }
        }

        public TItemTemp CreateItemInstance()
        {
            return new TItemTemp();
        }

        TInterface IBaseItemDataFactory<TInterface>.GetByPrimaryKey(long id)
        {
            return (TInterface)(IBaseItemData)GetByPrimaryKey(id);
        }

        TInterface IBaseItemDataFactory<TInterface>.CreateItemInstance()
        {
            return (TInterface)(IBaseItemData)CreateItemInstance();
            
        }


        IEnumerable<TInterface> IBaseItemDataFactory<TInterface>.GetAllItemsInRepository()
        {
            return (IEnumerable<TInterface>)(IEnumerable<IBaseItemData>) this.GetItemsInRepository();
            
        }


        public virtual List<TInterface> CreateList()
        {
            return new List<TInterface>();
            
        }


        public CS.General_v3.Classes.HelperClasses.OperationResult SaveItemInstanceFromFrontend(TInterface item)
        {
            return new CS.General_v3.Classes.HelperClasses.OperationResult();
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult DeleteItemInstanceFromFrontend(TInterface item)
        {
            _dict.Remove(item.ID);
            return new CS.General_v3.Classes.HelperClasses.OperationResult();
        }

        #region IBaseDataFactory<TInterface> Members


        public IEnumerable<TInterface> GetByPrimaryKeys(IEnumerable<long> ids)
        {
            List<TInterface> items = new List<TInterface>();
            foreach(var id in ids)
            {
                var item = this.GetByPrimaryKey(id);
                items.Add((TInterface) (IBaseItemData) item);
            }
            return items;
        }

        #endregion
    }
}
