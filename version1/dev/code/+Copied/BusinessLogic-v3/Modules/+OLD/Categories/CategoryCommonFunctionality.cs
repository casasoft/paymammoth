﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Categories
{
    public class CategoryCommonFunctionality
    {
        public ICategory Category { get; private set; }
        public CategoryCommonFunctionality(ICategory cat)
        {
            this.Category = cat;
        }

        public List<long> GetIdsForThisAndAllSubCategories()
        {
            List<long> list = new List<long>();
            list.Add(Category.ID);
            var subCategories = Category.GetSubCategories();
            if (subCategories != null)
            {
                foreach (var child in subCategories)
                {
                    list.AddRange(child.GetIdsForThisAndAllSubCategories());

                }
            }
            return list;
                
            
        }

    }
}
