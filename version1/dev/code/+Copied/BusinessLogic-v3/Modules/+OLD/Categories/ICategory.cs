﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Categories
{
    public interface ICategory : IBaseItemData
    {

        


			

        string TitleSingular { get; }
        string TitlePlural { get; }
        string Description { get; }
        string MetaKeywords { get; }
        string MetaDescription { get; }
     //   int Priority { get; }
        string Identifier { get; }
        bool Visible { get; }
        IEnumerable<ICategory> GetSubCategories();
        ICollection<long> GetIdsForThisAndAllSubCategories();
    }
}
