﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;

namespace BusinessLogic_v3.Modules.Categories
{
    public class CategoryTemp  :BaseItemDataTemp, ICategory
    {
        private CategoryCommonFunctionality _categoryCommonFunctionality = null;
        public CategoryTemp()
        {
            this.SubCategories = new List<CategoryTemp>();
            _categoryCommonFunctionality = new CategoryCommonFunctionality(this);
        }

        public string TitleSingular { get; set; }

        public string TitlePlural { get; set; }

        public string Description { get; set; }

        public string MetaKeywords { get; set; }

        public string MetaDescription { get; set; }

      //  public int Priority { get; set; }

        public string Identifier { get; set; }

        public bool Visible { get; set; }

        public List<CategoryTemp> SubCategories { get; set; }

        IEnumerable<ICategory> ICategory.GetSubCategories()
        {
            return SubCategories;
            
        }

        ICollection<long> ICategory.GetIdsForThisAndAllSubCategories()
        {
            return this._categoryCommonFunctionality.GetIdsForThisAndAllSubCategories();

        }
    }
}
