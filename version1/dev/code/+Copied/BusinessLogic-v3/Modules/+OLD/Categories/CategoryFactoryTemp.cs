﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Ordering;

namespace BusinessLogic_v3.Modules.Categories
{
    public class CategoryFactoryTemp : BaseItemDataFactoryTemp<CategoryTemp, ICategory>, ICategoryFactory
    {

        #region ICategoryFactory Members

        public ICategory GetByIdentifier(Enum enumValue, bool loadFromCache)
        {
            string s = CS.General_v3.Util.EnumUtils.StringValueOf(enumValue);
            var list = GetItemsInRepository();
            return list.Find(item => string.Compare(s, item.Identifier, true) == 0);

        }

        #endregion
    }
}
