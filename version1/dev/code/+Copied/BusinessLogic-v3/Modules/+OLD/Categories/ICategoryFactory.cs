﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BusinessLogic_v3.Modules.Categories
{
    public interface ICategoryFactory : IBaseItemDataFactory<ICategory> 
    {
        ICategory GetByIdentifier(Enum enumValue, bool loadFromCache = true);
    }
}
