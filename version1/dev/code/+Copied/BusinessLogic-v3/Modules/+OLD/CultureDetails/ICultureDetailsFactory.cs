﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BusinessLogic_v3.Modules.CultureDetails
{
    public interface ICultureDetailsFactory : IBaseItemDataFactory<ICultureDetails> 
    {
        ICultureDetails GetCultureByCode(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 languageCode);
        ICultureDetails GetCultureByCode(string code1, string code2 = null);
        //ICultureDetails GetDefaultCulture();
        IEnumerable<ICultureDetails> GetAllCultureDetails();
    }
}
