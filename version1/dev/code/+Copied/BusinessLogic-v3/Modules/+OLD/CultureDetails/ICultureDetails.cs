﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Categories;
using System.Globalization;
using CS.General_v3.Classes.PorterStemmer;
using BusinessLogic_v3.Modules.Currencies;

namespace BusinessLogic_v3.Modules.CultureDetails
{
    public interface ICultureDetails : IBaseItemData
    {
        string Title { get; }
        string LanguageISOCode { get;  }
        string SpecificCountryCode { get; }
        string ScriptSuffix { get; }
        ICurrency DefaultCurrency { get; }
        CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 GetLanguageISOEnumValue();
        CultureInfo GetCultureGlobalizationInfo();
        string FormatNumber(double d, string format, string specificCurrencySymbol = null);
        string FormatNumber(double d, CS.General_v3.Util.Number.NUMBER_FORMAT_TYPE formatType, string specificCurrencySymbol = null);
        string FormatDate(DateTime d, string format);
        string FormatDate(DateTime d, CS.General_v3.Util.Date.DATETIME_FORMAT formatType);
        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? GetCountryISOEnumValue();
        StemmerInterface GetStemmerForCulture();



        
    }
}
