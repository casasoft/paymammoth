﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Ordering;
using BusinessLogic_v3.Modules.Currencies;

namespace BusinessLogic_v3.Modules.CultureDetails
{
    public class CultureDetailsFactoryTemp : BaseItemDataFactoryTemp<CultureDetailsTemp, ICultureDetails>, ICultureDetailsFactory
    {

        protected override void initTestRepository()
        {
            {
                var item= new CultureDetailsTemp();
                item.LanguageISOCode = "en";
                item.Title = "English";
                item.SpecificCountryCode = "MT";
                item.DefaultCurrency = (CurrencyTemp)Factories.CurrencyFactory.GetByCurrencyCode( CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro);
                this.AddItemToTestRepository(item);
            }

            base.initTestRepository();
        }


        public ICultureDetails GetCultureByCode(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 languageCode)
        {
            string code1 = CS.General_v3.Enums.ISO_ENUMS.Language_ISO639_2letter_ToCode(languageCode);
            string code2 = CS.General_v3.Enums.ISO_ENUMS.Language_ISO639_3letter_ToCode(languageCode);
            return GetCultureByCode(code1, code2);
        }

        public ICultureDetails GetCultureByCode(string code1, string code2 = null)
        {
            var list = this.GetItemsInRepository();
            foreach (var item in list)
            {
                if (string.Compare(item.LanguageISOCode,code1,true) == 0 || string.Compare(item.LanguageISOCode,code2,true) == 0)
                    return item;
            }
            return null;
        }

        public ICultureDetails GetDefaultCulture()
        {
            ICultureDetails culture = GetCultureByCode(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.English);
            if (culture == null)
            {
                var list = this.GetItemsInRepository();
                culture = list[0];
            }
            return culture;
        }


        public IEnumerable<ICultureDetails> GetAllCultureDetails()
        {
            return this.GetItemsInRepository();
            
        }
    }
}
