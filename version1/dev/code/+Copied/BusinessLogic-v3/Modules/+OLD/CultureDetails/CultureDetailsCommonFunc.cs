﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using CS.General_v3.Classes.PorterStemmer;

namespace BusinessLogic_v3.Modules.CultureDetails
{
    public class CultureDetailsCommonFunc
    {
        public ICultureDetails CultureDetails { get; private set; }
        public CultureDetailsCommonFunc(ICultureDetails CultureDetails)
        {
            this.CultureDetails = CultureDetails;
        }

        public CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 GetLanguageISOEnumValue()
        {
            return CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639FromCode(CultureDetails.LanguageISOCode);
        }

        public CultureInfo GetCultureGlobalizationInfo()
        {

            string cultureCode = null;

            string langCode = CS.General_v3.Enums.ISO_ENUMS.Language_ISO639_2letter_ToCode(GetLanguageISOEnumValue());
            string countryCode = null;

            {
                var enumCode = GetCountryISOEnumValue();
                if (enumCode.HasValue)
                {
                    countryCode = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode(enumCode.Value);
                }
            }

            cultureCode = langCode;
            if (!string.IsNullOrWhiteSpace(countryCode))
            {
                cultureCode += "-" + countryCode.ToUpper();
            }
            if (!string.IsNullOrWhiteSpace(CultureDetails.ScriptSuffix))
                cultureCode += "-" + CultureDetails.ScriptSuffix;


            return new CultureInfo(cultureCode, false);
        }


        public string FormatNumber(double d, string format, string specificCurrencySymbol = null)
        {
            return d.ToString(format, GetCultureGlobalizationInfo());
        }
        public string FormatNumber(double d, CS.General_v3.Util.Number.NUMBER_FORMAT_TYPE formatType, string specificCurrencySymbol = null)
        {


            return CS.General_v3.Util.Number.FormatNumber(d, formatType, this.GetCultureGlobalizationInfo(), specificCurrencySymbol);
        }
        public string FormatDate(DateTime d, string format)
        {
            return d.ToString(format, GetCultureGlobalizationInfo());
        }
        public string FormatDate(DateTime d, CS.General_v3.Util.Date.DATETIME_FORMAT formatType)
        {
            return CS.General_v3.Util.Date.FormatDateTime(d, formatType, this.GetCultureGlobalizationInfo());
        }

        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? GetCountryISOEnumValue()
        {
            CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? countryCode = null;
            if (!string.IsNullOrWhiteSpace(CultureDetails.SpecificCountryCode))
                countryCode = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(CultureDetails.SpecificCountryCode);
            return countryCode;
        }


        public StemmerInterface GetStemmerForCulture()
        {
            StemmerInterface stemmer = null;
            switch (GetLanguageISOEnumValue())
            {
                case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.English:
                    stemmer = new PorterStemmerEnglish();
                    break;


            }
            return stemmer;
        }

    }
}
