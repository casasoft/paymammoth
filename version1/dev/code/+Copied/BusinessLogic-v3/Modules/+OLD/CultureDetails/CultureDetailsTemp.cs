﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;
using System.Globalization;
using BusinessLogic_v3.Modules.Currencies;

namespace BusinessLogic_v3.Modules.CultureDetails
{
    public class CultureDetailsTemp  :BaseItemDataTemp, ICultureDetails
    {
        public CultureDetailsTemp()
        {
            _commonFunc = new CultureDetailsCommonFunc(this);
        }
        private CultureDetailsCommonFunc _commonFunc = null;

        public string LanguageISOCode { get; set; }

        public string SpecificCountryCode { get; set; }

        public string ScriptSuffix { get; set; }

        public CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 GetLanguageISOEnumValue()
        {
            return _commonFunc.GetLanguageISOEnumValue();
        }

        public string FormatDate(DateTime d, string format)
        {
            return _commonFunc.FormatDate(d, format);
        }

        public CultureInfo GetCultureGlobalizationInfo()
        {
            return _commonFunc.GetCultureGlobalizationInfo();
        }

        public string FormatNumber(double d, string format, string specificCurrencySymbol = null)
        {
            return _commonFunc.FormatNumber(d, format, specificCurrencySymbol);

        }

        public string FormatNumber(double d, CS.General_v3.Util.Number.NUMBER_FORMAT_TYPE formatType, string specificCurrencySymbol = null)
        {
            return _commonFunc.FormatNumber(d, formatType, specificCurrencySymbol);

        }

        public string FormatDate(DateTime d, CS.General_v3.Util.Date.DATETIME_FORMAT formatType)
        {
            return _commonFunc.FormatDate(d, formatType);

        }

        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? GetCountryISOEnumValue()
        {
            return _commonFunc.GetCountryISOEnumValue();

        }

        public CurrencyTemp DefaultCurrency { get; set; }



        ICurrency ICultureDetails.DefaultCurrency
        {
            get { return this.DefaultCurrency; }
        }


        public CS.General_v3.Classes.PorterStemmer.StemmerInterface GetStemmerForCulture()
        {
            return _commonFunc.GetStemmerForCulture();
            
        }

        #region ICultureDetails Members

        public string Title {get;set;}

        #endregion
    }
}
