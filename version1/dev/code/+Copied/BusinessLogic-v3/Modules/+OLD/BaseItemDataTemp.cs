﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.DbObjects;


namespace BusinessLogic_v3.Modules
{
    public abstract class BaseItemDataTemp :  IBaseItemData
        
    {

        
        private static Dictionary<Type, long> uniqueIDs = new Dictionary<Type, long>();

        private long getNextID()
        {

            long num = 0;
            Type t = this.GetType();
            if (uniqueIDs.ContainsKey(t))
            {
                num = uniqueIDs[t];
            }
            num++;
            uniqueIDs[t] = num;
            return num;
        }

        public BaseItemDataTemp()
        {
            this.ID = getNextID();
        }

        #region ICmsItemInstance Members


        public int Priority
        {
            get;
            set;
        }

        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members

        public bool CheckIfStillTemporary()
        {
            return this.ID == 0;
            
        }

        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members

        public bool HasTemporaryFields()
        {
            return false;
            
        }

        public bool IsSavedInDB()
        {
            return this.ID > 0;
            
        }

        public bool IsTemporary()
        {
            return CheckIfStillTemporary();
            
        }

        public bool IsTransient()
        {
            return CheckIfStillTemporary();
            
        }

        public void MarkAsNotTemporary()
        {
            
        }

        public long PrimaryKey
        {
            get { return this.ID; }
        }

        public long ID {get; protected set;}

        public void Evict(MyNHSessionBase session = null)
        {
            
        }

        public void Refresh(MyNHSessionBase session = null)
        {
             
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult Create(bool savePermanently = true, bool updateFormulaProperties = true, MyNHSessionBase session = null)
        {
            return new CS.General_v3.Classes.HelperClasses.OperationResult();
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult CreateAndFlush(bool savePermanently = true, bool updateFormulaProperties = true, MyNHSessionBase session = null)
        {
            return new CS.General_v3.Classes.HelperClasses.OperationResult();
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult Save(bool savePermanently = true, bool updateFormulaProperties = true, MyNHSessionBase session = null)
        {
            return new CS.General_v3.Classes.HelperClasses.OperationResult();

        }

        public CS.General_v3.Classes.HelperClasses.OperationResult SaveAndFlush(bool savePermanently = true, bool updateFormulaProperties = true, MyNHSessionBase session = null)
        {
            return new CS.General_v3.Classes.HelperClasses.OperationResult();

        }

        public CS.General_v3.Classes.HelperClasses.OperationResult SaveCopy(bool savePermanently = true, bool updateFormulaProperties = true, MyNHSessionBase session = null)
        {
            return new CS.General_v3.Classes.HelperClasses.OperationResult();

        }

        public CS.General_v3.Classes.HelperClasses.OperationResult SaveCopyAndFlush(bool savePermanently = true, bool updateFormulaProperties = true, MyNHSessionBase session = null)
        {
            return new CS.General_v3.Classes.HelperClasses.OperationResult();

        }

        public CS.General_v3.Classes.HelperClasses.OperationResult Update(bool savePermanently = true, bool updateFormulaProperties = true, MyNHSessionBase session = null)
        {
            return new CS.General_v3.Classes.HelperClasses.OperationResult();

        }

        public CS.General_v3.Classes.HelperClasses.OperationResult UpdateAndFlush(bool savePermanently = true, bool updateFormulaProperties = true, MyNHSessionBase session = null)
        {

            return new CS.General_v3.Classes.HelperClasses.OperationResult();
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult Delete(bool deletePermanently = false, MyNHSessionBase session = null)
        {
            this.IsDeleted = true;
            this.DeletedOn = CS.General_v3.Util.Date.Now;

            return new CS.General_v3.Classes.HelperClasses.OperationResult();

        }

        public CS.General_v3.Classes.HelperClasses.OperationResult DeleteAndFlush(bool deletePermanently = false, MyNHSessionBase session = null)
        {
            this.IsDeleted = true;
            this.DeletedOn = CS.General_v3.Util.Date.Now;
            return new CS.General_v3.Classes.HelperClasses.OperationResult();
        }

        public CS.General_v3.Classes.HelperClasses.OperationResult DeleteIncludingLinkedItems(MyNHSessionBase session = null)
        {
            return new CS.General_v3.Classes.HelperClasses.OperationResult();
        }

        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members

        public bool IsDeleted {get;set;}

        public DateTime? DeletedOn {get;set;}

        public CS.General_v3.Classes.Login.IUser DeletedBy {get;set;}

        
        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members


        public void MarkAsTemporary()
        {
            
        }

        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members


        public void Merge(MyNHSessionBase session = null)
        {
          
        }

        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members

        void BusinessLogic_v3.Classes.DB.IBaseDbObject.resetDirtyFlag()
        {
            
        }

        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members

        void BusinessLogic_v3.Classes.DB.IBaseDbObject.InitPropertiesForNewItem()
        {
            
            
        }

        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members


        public void OnLoad()
        {
            
        }

        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members

        public bool _Deleted { get; set; }

        public DateTime _Temporary_LastUpdOn { get; set; }

        public bool _Temporary_Flag { get; set; }

        public DateTime? _DeletedOn { get; set; }

        public CS.General_v3.Classes.Login.IUser _DeletedBy { get; set; }

        #endregion

        #region BusinessLogic_v3.Classes.DB.IBaseDbObject Members


        public string GetOutputCacheDependencyKey()
        {
            return this.GetType().FullName + "_ID_" + ID;
            
        }

        #endregion
    }
}
