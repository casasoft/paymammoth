﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Members;
using BusinessLogic_v3.Modules.Ordering;
using BusinessLogic_v3.Modules.Content;
using BusinessLogic_v3.Modules.Categories;
using BusinessLogic_v3.Modules.SpecialOffers;
using BusinessLogic_v3.Modules.Currencies;
using BusinessLogic_v3.Modules.HomepageBanners;
using BusinessLogic_v3.Modules.Adverts;
using BusinessLogic_v3.Modules.CultureDetails;

namespace BusinessLogic_v3.Modules
{
    public static class Factories
    {
        public static void InitDBFactories()
        {

            MemberFactory = BusinessLogic_v3.DB.MemberBase.Factory;
            //BusinessLogic_v3.DB.MemberSessionLoginManager._Instance = new BusinessLogic_v3.DB.MemberSessionLoginManager(null);

            MemberSessionLoginManager = BusinessLogic_v3.Modules.Members.MemberSessionLoginManagerDB.Instance;
            InvoiceFactory = BusinessLogic_v3.DB.OrderBase.Factory;
            InvoiceItemFactory = BusinessLogic_v3.DB.OrderItemBase.Factory;
            CurrencyFactory = BusinessLogic_v3.DB.CurrencyBase.Factory;
            CultureDetailsFactory = BusinessLogic_v3.DB.CultureDetailsBase.Factory;
            ContentPageFactory = BusinessLogic_v3.DB.ContentPageBase.Factory;
            ContentTextFactory = BusinessLogic_v3.DB.ContentTextBase.Factory;
            CategoryFactory = BusinessLogic_v3.DB.CategoryBase.Factory;
            SpecialOfferFactory = BusinessLogic_v3.DB.SpecialOfferBase.Factory;

            
            
        }
        public static void InitTempFactories()
        {
            MemberFactory = new MemberFactoryTemp();
            MemberSessionLoginManager = new MemberSessionLoginTemp();
            HomepageBannerFactory = new HomepageBannerFactoryTemp();
            InvoiceFactory = new OrderFactoryTemp();
            InvoiceItemFactory = new OrderItemFactoryTemp();
            CurrencyFactory = new CurrencyFactoryTemp();
            CultureDetailsFactory = new CultureDetailsFactoryTemp();
            ContentPageFactory = new ContentPageFactoryTemp();
            ContentTextFactory = new ContentTextFactoryTemp();
            CategoryFactory = new CategoryFactoryTemp();
            SpecialOfferFactory = new SpecialOfferFactoryTemp();
            AdvertSlotColumnFactory = new AdvertSlotColumnFactoryTempImplementation();
            AdvertSlotFactory = new AdvertSlotFactoryTempImplementation();
            AdvertFactory = new AdvertFactoryTempImplementation();
        }

        public static IMemberFactory MemberFactory { get; set; }
        public static IMemberSessionLoginManager MemberSessionLoginManager { get; set; }
        public static IOrderFactory InvoiceFactory { get; set; }
        public static IOrderItemFactory InvoiceItemFactory { get; set; }
        public static ICurrencyFactory CurrencyFactory { get; set; }
        public static ICultureDetailsFactory CultureDetailsFactory { get; set; }
        public static IContentPageFactory ContentPageFactory { get; set; }
        public static IContentTextFactory ContentTextFactory { get; set; }
        public static ICategoryFactory CategoryFactory { get; set; }
        public static ISpecialOfferFactory SpecialOfferFactory { get; set; }
        public static IHomepageBannerFactory HomepageBannerFactory { get; set; }
        public static IAdvertSlotColumnFactory AdvertSlotColumnFactory { get; set; }
        public static IAdvertSlotFactory AdvertSlotFactory { get; set; }
        public static IAdvertFactory AdvertFactory { get; set; }


    }
}
