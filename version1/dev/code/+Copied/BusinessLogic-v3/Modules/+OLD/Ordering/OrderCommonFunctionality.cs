﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.SpecialOffers;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodes;

namespace BusinessLogic_v3.Modules.Ordering
{
    public class OrderCommonFunctionality
    {
        private IOrder _order = null;
        public OrderCommonFunctionality(IOrder order)
        {

            this._order = order;
        }
        public double GetFixedDiscountForOrder()
        {
            double d = 0;
            
            var appliedSpecialOffers = _order.GetAppliedSpecialOffers();
            foreach (var sp in appliedSpecialOffers)
            {
                d += sp.DiscountFixed;
            }
            
            return d;

        }

        public double GetTotalPrice(bool includeTax, bool includeShipping, bool includeDiscount)
        {
            double dTotal = 0;
            if (_order != null)
            {
                var items = _order.GetOrderItems();
                if (items != null)
                {

                    foreach (var item in items)
                    {
                        if (item != null)
                        {
                            dTotal += item.GetTotalPrice(includeTax, includeDiscount) * item.Quantity;
                        }

                    }
                }
                if (includeDiscount)
                    dTotal -= GetFixedDiscountForOrder();
                if (includeShipping)
                {
                    dTotal += _order.GetTotalShippingCost().GetValueOrDefault();
                }
            }
            return dTotal;
        }

        public IEnumerable<ISpecialOffer> GetAppliedSpecialOffers()
        {
            List<ISpecialOffer> list = new List<ISpecialOffer>();
            foreach (var item in _order.GetOrderItems())
            {
                if (item.ApplicableSpecialOffer != null)
                    list.Add(item.ApplicableSpecialOffer);
            }
            CS.General_v3.Util.ListUtil.RemoveDuplicates(list, ((item1, item2) => (item1.ID.CompareTo(item2.ID))));
            return list;
        }

        public bool HasItemsToBeShipped()
        {
            foreach (var item in _order.GetOrderItems())
            {
                if (item.GetShipmentWeight() > 0)
                    return true;
            }
            return false;
        }

        public double GetTotalDiscount()
        {
            double d = 0;
            foreach (var item in _order.GetOrderItems())
            {
                d += item.GetTotalDiscount();
            }
            d += GetFixedDiscountForOrder();
            return d;
        }

      
    }
}
