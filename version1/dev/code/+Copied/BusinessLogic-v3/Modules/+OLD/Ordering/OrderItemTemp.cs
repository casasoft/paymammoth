﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Modules.SpecialOffers;

namespace BusinessLogic_v3.Modules.Ordering
{
    public class OrderItemTemp :BaseItemDataTemp, IOrderItem
    {
        public OrderItemTemp()
        {
            _commonFunctionality = new OrderItemCommonFunctionality(this);
        }
        private OrderItemCommonFunctionality _commonFunctionality = null;
        public OrderTemp Invoice { get; set; }
        IOrder IOrderItem.Invoice 
        {
            get { return this.Invoice; }
        }
       

        public string Title {get;set;}

        public long ItemID
        {
            get { return this.ID; }
        }
     

        public string ItemType {get;set;}

        public string ItemReference { get; set; }

        public int Quantity { get; set; }
        public double PriceExcTaxPerUnit { get; set; }

        public double TaxAmountPerUnit { get; set; }

        public double ShippingCost { get; set; }

        public double HandlingCost { get; set; }

        public double ShipmentWeight { get; set; }

        public double GetTotalPrice(bool includeTax = true,  bool reduceDiscount = true)
        {
            return _commonFunctionality.GetTotalPrice(includeTax, reduceDiscount);
            
            
        }


        public double DiscountPerUnit { get; set; }
        public SpecialOfferTemp ApplicableSpecialOffer { get; set; }
        SpecialOffers.ISpecialOffer IOrderItem.ApplicableSpecialOffer { get { return this.ApplicableSpecialOffer; } }

        #region IOrderItem Members


        public double GetTotalDiscount()
        {
            return _commonFunctionality.GetTotalDiscount();
            
        }

        #endregion

        #region IOrderItem Members


        public string GetTitle()
        {
            return this.Title;
            
        }

        public string GetItemReference()
        {
            return this.ItemReference;
            
        }

        public double GetPriceExcTaxPerUnit()
        {
            return this.PriceExcTaxPerUnit;
            
        }

        public double GetTaxAmountPerUnit()
        {
            return TaxAmountPerUnit;
            
        }

        public double GetDiscountPerUnit()
        {
            return this.DiscountPerUnit;
            
        }

        #endregion

        #region IOrderItem Members


        public double? GetShippingCost()
        {
            return 0;
            
        }

        public double? GetHandlingCost()
        {
            return 0;
            
        }

        public double GetShipmentWeight()
        {
            return 0;
            
        }

        #endregion
    }
}
