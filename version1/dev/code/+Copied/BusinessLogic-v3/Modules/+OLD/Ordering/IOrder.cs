﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Modules.Members;
using BusinessLogic_v3.Modules.SpecialOffers;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodes;

namespace BusinessLogic_v3.Modules.Ordering
{
    public interface IOrder: IBaseItemData, IMemberAddressDetails
    {
        
        string Reference { get; }
        DateTime DateCreated { get; }
        DateTime DateLastUpdated { get; }
        IMember Member { get; }
        string AuthCode { get; }
        bool Paid { get; }
        DateTime? PaidOn { get; }
        bool Cancelled { get; }
        DateTime? CancelledOn { get; }
        string ShipmentTrackingCode { get; }
        DateTime? ShippedOn { get; }
        bool IsOrderEmpty();
        double GetFixedDiscountForOrder();
        double GetTotalDiscount();
        double? GetTotalShippingCost();
        CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 OrderCurrency { get; }
        IEnumerable<Modules.SpecialOffers.ISpecialOffer> GetAppliedSpecialOffers();
        IEnumerable<IOrderItem> GetOrderItems();
        string GetTotalCostAsHtml(bool includeCurrencySign, bool includeShipping, bool htmlEncode = true);
        double GetTotalPrice(bool includeTax = true, bool includeShipping = true, bool includeDiscount = true);
        
        IEnumerable<ISpecialOfferVoucherCode> GetAppliedVoucherCodes();
        bool HasItemsRequiringShipping();
        void UpdateCurrencyToMatchCurrentSession();
    }
}
