﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.Ordering
{
    public class OrderItemCommonFunctionality
    {
        private IOrderItem _orderItem = null;
        public OrderItemCommonFunctionality(IOrderItem orderItem)
        {

            this._orderItem = orderItem;
        }
        public double GetTotalDiscount()
        {
            return this._orderItem.GetDiscountPerUnit() * this._orderItem.Quantity;
        }
        public double GetTotalPrice(bool includeTax, bool reduceDiscount)
        {
            double d = 0;

            int qty = _orderItem.Quantity;

            d += _orderItem.GetPriceExcTaxPerUnit() * qty;
            if (includeTax) d += _orderItem.GetTaxAmountPerUnit() * qty;
           // if (includeShipping) d += _orderItem.GetShippingCost().GetValueOrDefault();
          //  if (includeHandling) d += _orderItem.GetHandlingCost().GetValueOrDefault();
            if (reduceDiscount) d -= _orderItem.GetTotalDiscount();
            return d;
        }

    }
}
