﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;

namespace BusinessLogic_v3.Modules.Ordering
{
    public class OrderTemp :BaseItemDataTemp, IOrder
    {
        private OrderCommonFunctionality _commonFunctionality = null;
        public OrderTemp()
        {
            _commonFunctionality = new OrderCommonFunctionality(this);
            this.InvoiceItems = new List<OrderItemTemp>();
        }
        public List<OrderItemTemp> InvoiceItems { get; set; }

        public string Reference {get;set;}

        public DateTime DateCreated { get; set; }

        public Members.IMember Member { get; set; }

        public string AuthCode { get; set; }

        public bool Paid { get; set; }

        public DateTime? PaidOn { get; set; }

        public bool Cancelled { get; set; }

        public DateTime? CancelledOn { get; set; }

        public string ShipmentTrackingCode { get; set; }

        public DateTime? ShippedOn { get; set; }
        
        
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? AddressCountry { get; set; }

        public string AddressCity { get; set; }

        public string AddressState { get; set; }

        public string AddressZIPCode { get; set; }

        public string CustomerName { get; set; }
        public string CustomerSurname { get; set; }

        string IMemberAddressDetails.Name { get { return this.CustomerName; } set { this.CustomerName = value; } }
        string IMemberAddressDetails.Surname { get { return this.CustomerSurname; } set { this.CustomerSurname = value; } }
        

        public void CopyMemberAddressDetails(Classes.Interfaces.IMemberAddressDetails copyFrom)
        {
            BusinessLogic_v3.Classes.Interfaces.Extensions.IMemberAddressDetailsExtensions.CopyMemberAddressDetailsExtension(this, copyFrom);
            
        }

        public string GetAddressAsOneLine(string delimeter = ", ", bool includeCountry = false)
        {
            return BusinessLogic_v3.Classes.Interfaces.Extensions.IMemberAddressDetailsExtensions.GetAddressAsOneLineExtension(this, delimeter, includeCountry);
            
        }

        public string GetFullName()
        {
            return BusinessLogic_v3.Classes.Interfaces.Extensions.IMemberAddressDetailsExtensions.GetFullNameExtension(this); 
        }


        public IEnumerable<IOrderItem> GetOrderItems()
        {
            return this.InvoiceItems;
            
        }

        public double GetTotalPrice(bool includeTax = true, bool includeShipping = true,  bool reduceDiscount = true)
        {
            return _commonFunctionality.GetTotalPrice(includeTax, includeShipping, reduceDiscount);
            
            
        }


        public string GetTotalCostAsHtml(bool includeCurrencySign, bool includeShipping, bool htmlEncode = true)
        {
            double p = this.GetTotalPrice(includeShipping: includeShipping );
            string currencySign = null;

            if (includeCurrencySign)
            {
                currencySign = CS.General_v3.Enums.ISO_ENUMS.GetCurrencySymbol(this.OrderCurrency);
            }
            return CS.General_v3.Util.Number.FormatCurrency(p, null, null, currencySign, htmlEncode);
        }


        public double GetTotalDiscount()
        {
            return 0;
            
        }

        public IEnumerable<SpecialOffers.ISpecialOffer> GetAppliedSpecialOffers()
        {
            return _commonFunctionality.GetAppliedSpecialOffers();
        }


        public bool HasItemsRequiringShipping()
        {
            return _commonFunctionality.HasItemsToBeShipped();
            
        }

        #region IOrder Members


        public double FixedDiscount { get; set; }
        #endregion

        #region IOrder Members


        public SpecialOfferVoucherCodes.ISpecialOfferVoucherCode AppliedVoucherCode { get; set; }

        #endregion

        #region IOrder Members


        public double GetFixedDiscountForOrder()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SpecialOfferVoucherCodes.ISpecialOfferVoucherCode> GetAppliedVoucherCodes()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IOrder Members


        public bool IsOrderEmpty()
        {
            return this.GetOrderItems().Count() == 0;
            
        }

        #endregion

        #region IOrder Members


        public double? GetTotalShippingCost()
        {
            return 0;
            
        }

        #endregion

        #region IOrder Members


        public DateTime DateLastUpdated { get; set; }
        #endregion

        #region IOrder Members


        public void UpdateCurrencyToMatchCurrentSession()
        {
            
        }

        #endregion

        #region IOrder Members


        public CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 OrderCurrency { get; set; }
        #endregion
    }
}
