﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Modules.Members;
using BusinessLogic_v3.Modules.SpecialOffers;

namespace BusinessLogic_v3.Modules.Ordering
{
    public interface IOrderItem: IBaseItemData
    {
        IOrder Invoice { get; }
        string GetTitle();
        long ItemID { get; }
        string ItemType { get; }
        string GetItemReference();
        int Quantity { get; }
        double GetPriceExcTaxPerUnit();
        double GetTaxAmountPerUnit();

        // double? GetShippingCost();
       // double? GetHandlingCost();
        double GetShipmentWeight();
        double GetDiscountPerUnit();
        ISpecialOffer ApplicableSpecialOffer { get; }
        double GetTotalPrice(bool includeTax = true,  bool reduceDiscount = true);

        double GetTotalDiscount();
    }
}
