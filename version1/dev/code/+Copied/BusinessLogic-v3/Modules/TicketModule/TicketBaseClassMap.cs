using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.TicketModule
{

//BaseClassMap-File
    
    public abstract class TicketBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.TicketBaseMap_AutoGen<TData>
    	where TData : TicketBase
    {
        protected override void TicketPriorityMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.MapEnumAsString = false;
            base.TicketPriorityMappingInfo(mapInfo);
        }
 
    }
   
}
