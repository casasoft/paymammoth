﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using CS.General_v3.Classes.Login;

namespace BusinessLogic_v3.Modules.TicketModule
{
    public interface ITicketingSystemUser : IBaseDbObject, IUser
    {
        BusinessLogic_v3.Enums.TICKET_USER_TYPE UserType { get; }
    }
}
