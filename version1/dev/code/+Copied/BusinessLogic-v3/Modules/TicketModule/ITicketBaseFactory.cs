using BusinessLogic_v3.Classes.Searches.TicketSearcher;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.TicketModule
{   

//IBaseFactory-File

	public interface ITicketBaseFactory : BusinessLogic_v3.Modules._AutoGen.ITicketBaseFactoryAutoGen
	{

	    IEnumerable<ITicketBase> GetTickets(TicketSearcherParams searchParams);

	}

}
