using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Searches.TicketSearcher;

namespace BusinessLogic_v3.Modules.TicketModule
{   

//BaseFactory-File

	public abstract class TicketBaseFactory : BusinessLogic_v3.Modules._AutoGen.TicketBaseFactoryAutoGen, ITicketBaseFactory
    {
		
		public IEnumerable<ITicketBase> GetTickets(TicketSearcherParams searchParams)
		{
            if (searchParams != null)
            {
                TicketSearcher searcher = new TicketSearcher();
                searcher.SearchParams = searchParams;
                return searcher.GetSearchResults();
            }
            else
            {
                throw new InvalidOperationException("searchParams can never be null when calling this method!");
            }
		}
    }

}
