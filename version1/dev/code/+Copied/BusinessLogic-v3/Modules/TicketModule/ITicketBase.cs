using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules.TicketCommentModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.TicketModule
{

//IBaseClass-File
	
    public interface ITicketBase : BusinessLogic_v3.Modules._AutoGen.ITicketBaseAutoGen
    {
        Enums.TICKET_STATE GetCurrentState();
        void UpdateState(BusinessLogic_v3.Enums.TICKET_STATE state, Enums.TICKET_UPDATING_FROM updateFrom);
        void UpdateState(Enums.TICKET_STATE state, ITicketingSystemUser user);
        ITicketingSystemUser GetReopenedByUser();
        void ReOpenIssue(Enums.TICKET_UPDATING_FROM commandSource);
        void CloseIssue(Enums.TICKET_UPDATING_FROM commandSource);
        void AssignTicketingSystemSupportUser(ICmsUserBase user);
        Enums.TICKET_PRIORITY GetPriority();
        void SetPriority(Enums.TICKET_PRIORITY priority);
        void AssignMember(IMemberBase member);
        IEnumerable<ITicketCommentBase> GetTicketComments();
        ITicketCommentBase CreateTicketComment(Enums.TICKET_UPDATING_FROM source);
    }
}
