using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.Article_ParentChildLinkModule
{

//BaseClassMap-File
    
    public abstract class Article_ParentChildLinkBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.Article_ParentChildLinkBaseMap_AutoGen<TData>
    	where TData : Article_ParentChildLinkBase
    {
        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }

        protected override void ParentMappingInfo(ICollectionMapInfo mapInfo)
        {
          //  mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.All;
            
            base.ParentMappingInfo(mapInfo);
            mapInfo.Inverse = true;
        }
        protected override void ChildMappingInfo(ICollectionMapInfo mapInfo)
        {
           // mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.All;
            base.ChildMappingInfo(mapInfo);
            mapInfo.Inverse = true;
        }
    }
   
}
