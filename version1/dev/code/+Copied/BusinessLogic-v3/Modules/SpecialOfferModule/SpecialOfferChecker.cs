﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules.OrderItemModule;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;

namespace BusinessLogic_v3.Modules.SpecialOfferModule
{
    public class SpecialOfferChecker
    {
        private ConcurrentDictionary<long, ISpecialOfferBase> specialOffers = null;
        private IEnumerable<string> promoCodes = null;



        /// <summary>
        /// Loads applicable special offers, and also checks promo codes
        /// </summary>
        private void loadSpecialOffersMatchingPromoCodes()
        {
            specialOffers = new ConcurrentDictionary<long, ISpecialOfferBase>();

            var list = SpecialOfferBase.Factory.GetCurrentSpecialOffers();
            //remove any special offers that dont match the promo codes, and leave only those that do or dont require promo codes
            foreach (var offer in list)
            {
                if (!offer.RequiresPromoCode && offer.IsCurrentlyAvailable()) //checks if it matches, or doesnt require code
                {
                    specialOffers[offer.ID] = offer;

                }
            }

            foreach (var code in promoCodes)
            {
                var voucher = (SpecialOfferVoucherCodeBase) SpecialOfferVoucherCodeBase.Factory.GetByVoucherCode(code);
                if (voucher != null && voucher.IsCurrentlyValid())
                {
                    specialOffers[voucher.SpecialOffer.ID] = voucher;
                }
            }

        }

        private IEnumerable<OrderItemBase> getItemsApplicableToOffer(ISpecialOfferBase offer)
        {
            List<OrderItemBase> list = new List<OrderItemBase>();
            foreach (var item in order.OrderItems)
            {

                if (item.CheckIfItemAppliesToOffer(offer))
                {
                    list.Add(item);
                }
            }
            return list;
        }

        /// <summary>
        /// Checks whether the shopping cart exceeds requirements for this special offer
        /// </summary>
        /// <returns></returns>
        private bool checkIfOrderExceedsRequirements(ISpecialOfferBase offer)
        {
            var list = getItemsApplicableToOffer(offer);
            double totalSpent = list.GetTotalPriceForAllIncVAT(true,false);
            int totalQty = list.GetTotalQuantityForAll();

            if (totalSpent >= offer.RequiredTotalToSpend && totalQty >= offer.RequiredTotalItemsToBuy)
            {
                return true;
            }
            else
            {
                return false;
            }

        }



        private OrderBase order = null;
        private bool checkSpecialOffer(ISpecialOfferBase offer)
        {
            bool ok = false;
            //If shopping cart meets the demand for the offer
            if (checkIfOrderExceedsRequirements(offer))
            {
                bool result = false;
                foreach (var item in order.OrderItems)
                {
                    result = item.ApplySpecialOfferIfMoreAdvantageous(offer);
                    if (result) ok = true;
                }


            }
            return ok;
        }
       

        private void saveAllItems()
        {
            foreach (var item in this.order.OrderItems)
            {
                item.Save();
            }
        }
        public bool ApplySpecialOffers(string promoCode, OrderBase order)
        {
            promoCode = promoCode ?? "";
            promoCode = promoCode.Trim();
            this.order = order;
            this.promoCodes = CS.General_v3.Util.ListUtil.GetListFromSingleItem(promoCode);
            loadSpecialOffersMatchingPromoCodes();
            // removeSpecialOffers();


            bool ok = false;
            foreach (var offer in this.specialOffers.Values)
            {
                bool result = checkSpecialOffer(offer);
                if (result) ok = true;
            }
            saveAllItems();
            return ok;

        }
    }
}
