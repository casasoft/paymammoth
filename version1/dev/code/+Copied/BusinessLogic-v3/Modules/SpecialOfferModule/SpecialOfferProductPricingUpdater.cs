﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ProductModule;
using log4net;

namespace BusinessLogic_v3.Modules.SpecialOfferModule
{
    public class SpecialOfferProductPricingUpdater
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(SpecialOfferProductPricingUpdater));


        public SpecialOfferProductPricingUpdater()
        {

        }

        private List<ISpecialOfferBase> _specialOffers { get; set; }
        private SpecialOfferCalculator _specialOfferCalc = null;
        private void loadSpecialOffers()
        {
            this._specialOffers = SpecialOfferBaseFactory.Instance.GetCurrentSpecialOffers(loadOnlyWithoutRestrictions: true, preloadData: true).Cast<ISpecialOfferBase>().ToList();
            _specialOfferCalc = new SpecialOfferCalculator();
            _specialOfferCalc.SpecialOffers.AddAll(this._specialOffers);
            
        }

        public void UpdateProductPrice(ProductBase p)
        {
            if (p.ID == 51478529)
            {
                int k = 5;
            }
            double totalDiscount = 0;
            totalDiscount =  _specialOfferCalc.CalculatePercentageDiscountForProduct(p);
            //foreach (var sp in specialOffers)
            //{
            //    if (sp.CheckIfProductAppliesToSpecialOffer(p) && p.PriceDiscounted <= 0)
            //    {
            //        double productPrice = p.PriceInfo.GetTotalPrice(reduceDiscount: false);
            //        double offerDisc = sp.GetTotalDiscountByPercentageForPrice(productPrice);
            //        totalDiscount += offerDisc;
            //    }
            //}
            if (Math.Round(totalDiscount, Constants.General.NUMBER_PRECISION_FINANCIAL) != Math.Round(p._SpecialOfferDiscount, Constants.General.NUMBER_PRECISION_FINANCIAL))
            {
                p._SpecialOfferDiscount = Math.Round(totalDiscount, Constants.General.NUMBER_PRECISION_FINANCIAL);
                p.Save();
            }
        }

        private void updateAllProducts()
        {
            const int pageSize = 100;

            int pageNo = 1;
            bool toContinue = true;
            while (toContinue)
            {
                bool pageOK = false;
                try
                {
                    var q = ProductBaseFactory.Instance.GetQuery();
                    BusinessLogic_v3.Util.nHibernateUtil.FetchCollections<ProductBase, ProductCategoryModule.ProductCategoryBase>(q,
                                                                                                                                  NHibernate.FetchMode.Select, x => x.CategoryLinks,
                                                                                                                                  x => x.Category);
                    BusinessLogic_v3.Util.nHibernateUtil.LimitQueryByPrimaryKeys(q, pageNo, pageSize);
                    var list = ProductBaseFactory.Instance.FindAll(q).ToList();
                    toContinue = (list.Count > 0);
                   

                    using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                    {
                        foreach (var p in list)
                        {
                            UpdateProductPrice(p);
                        }
                        t.Commit();
                    }
                    System.Threading.Thread.Sleep(250); //pause a bit
                    pageOK = true;
                }
                catch (NHibernate.StaleStateException ex)
                {
                    BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
                    System.Threading.Thread.Sleep(CS.General_v3.Util.Random.GetInt(250,1000)); //sleep a bit
                    initSession();
                }
                if (pageOK)
                {
                    pageNo++;
                }

            }
        }

        private void initSession()
        {
            BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            loadSpecialOffers();
        }

        public void UpdatePricing()
        {


            initSession();
            
            if (BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Shop_Enabled))
            {
                updateAllProducts();
                
            }



            BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();



        }

    }
}
