﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Iesi.Collections;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ShoppingCartModule;

namespace BusinessLogic_v3.Modules.SpecialOfferModule
{
    public class SpecialOfferCalculator
    {



        public HashedSet<ISpecialOfferBase> SpecialOffers { get; set; }
        public SpecialOfferCalculator()
        {
            this.SpecialOffers = new HashedSet<ISpecialOfferBase>();
        }

        public static SpecialOfferCalculator GetSpecialOfferCalculatorFromContext()
        {
            const string key = "_SpecialOfferCalculator_contextInstance";
            SpecialOfferCalculator calc = null;
            
            calc =  CS.General_v3.Util.PageUtil.GetContextObject<SpecialOfferCalculator>(key, throwErrorIfContextDoesNotExist: false);
            if (true || calc == null)
            {
                calc = new SpecialOfferCalculator();
                calc.LoadAllOffersFromDb();
                //CS.General_v3.Util.PageUtil.SetContextObject(key, calc, throwErrorIfContextDoesNotExist: false);
            }
            return calc;
        }


        public void LoadAllOffersFromDb()
        {
            var list = SpecialOfferBaseFactory.Instance.GetCurrentSpecialOffers(loadOnlyWithoutRestrictions: true).Cast<ISpecialOfferBase>().ToList();
            this.SpecialOffers.AddAll(list);
        }
        public void LoadOffersAvailableOnlyToCart(ShoppingCartBase cart)
        {
            var list = SpecialOfferBaseFactory.Instance.GetSpecialOffersWithCartRequirements(preloadData: true);

            //var list = SpecialOfferBaseFactory.Instance.FindAll(q);
            List<ISpecialOfferBase> applicableOffers = new List<ISpecialOfferBase>();
            foreach (var sp in list)
            {
                if (cart.SpecialOfferCalculator.CheckIfCartMeetsOfferRequirements(sp))
                {
                    applicableOffers.Add(sp);
                }
            }

            //add the voucher code, if specified
            if (cart.SpecialOfferVoucherCode != null && cart.SpecialOfferVoucherCode.SpecialOffer != null)
            {
                applicableOffers.Add(cart.SpecialOfferVoucherCode);
            }
            this.SpecialOffers.AddAll(applicableOffers);

        }

        public HashedSet<ISpecialOfferBase> GetOffersUsedByCart(ShoppingCartBase cart)
        {


            HashedSet<ISpecialOfferBase> list = new HashedSet<ISpecialOfferBase>();

            foreach (var cartItem in cart.ShoppingCartItems)
            {//add all those actually used by cart
                var p = cartItem.GetLinkedProduct();
                if (p != null)
                {
                    list.AddAll(GetOffersUsedForProduct((ProductBase)p));
                }
            }

           
            return list;

        }


        public HashedSet<ISpecialOfferBase> GetNonExclusiveOffers()
        {
            HashedSet<ISpecialOfferBase> list = new HashedSet<ISpecialOfferBase>();
            foreach (var sp in this.SpecialOffers)
            {
                if (!sp.IsExclusiveOffer)
                {
                    list.Add(sp);
                }
            }
            return list;
        }
        public HashedSet<ISpecialOfferBase> GetExclusiveOffers()
        {
            HashedSet<ISpecialOfferBase> list = new HashedSet<ISpecialOfferBase>();
            foreach (var sp in this.SpecialOffers)
            {
                if (sp.IsExclusiveOffer)
                {
                    list.Add(sp);
                }
            }
            return list;
        }
        public ISpecialOfferBase GetBestExclusiveOfferForProduct(IProductBase p)
        {
            var exclusiveOffers = GetExclusiveOffers();
            ISpecialOfferBase bestOffer = null;
            double bestOfferDiscount = 0;
            foreach (var sp in exclusiveOffers)
            {
                if (sp.CheckIfProductAppliesToSpecialOffer(p))
                {
                    var discPrice = sp.GetTotalDiscountByPercentageForPrice(p.PriceInfo.GetTotalPrice(includeTax:true, reduceDiscount:false));
                    if (discPrice > bestOfferDiscount)
                    {
                        bestOffer = sp;
                        bestOfferDiscount = discPrice;
                    }
                }
            }
            return bestOffer;

        }

        public HashedSet<ISpecialOfferBase> GetOffersUsedForProduct(ProductBase product)
        {
            HashedSet<ISpecialOfferBase> list = new HashedSet<ISpecialOfferBase>();
            double totalDiscount = 0;
            var nonExclusiveOffers = GetNonExclusiveOffers();
            foreach (var sp in nonExclusiveOffers)
            {
                if (sp.CheckIfProductAppliesToSpecialOffer(product))
                {

                    var disc = sp.GetTotalDiscountByPercentageForPrice(product.PriceInfo.GetTotalPrice(includeTax: true, reduceDiscount: false));
                    if (disc > 0)
                    {
                        list.Add(sp);
                    }
                }
            }
            var exclusiveOffer = GetBestExclusiveOfferForProduct(product);
            if (exclusiveOffer != null)
            {

                var discPrice = exclusiveOffer.GetTotalDiscountByPercentageForPrice(product.PriceInfo.GetTotalPrice(includeTax: true, reduceDiscount: false));
                if (discPrice > 0) list.Add(exclusiveOffer);
                

            }
            return list;
            

        }

        public double CalculatePercentageDiscountForProduct(IProductBase product)
        {
            double totalDiscount = 0;
            var nonExclusiveOffers = GetNonExclusiveOffers();
            foreach (var sp in nonExclusiveOffers)
            {
                if (sp.CheckIfProductAppliesToSpecialOffer(product))
                {
                    var disc = sp.GetTotalDiscountByPercentageForPrice(product.PriceInfo.GetTotalPrice(includeTax: true, reduceDiscount: false));
                    totalDiscount += disc;
                }
            }
            var exclusiveOffer = GetBestExclusiveOfferForProduct(product);
            if (exclusiveOffer != null)
            {
                var discPrice = exclusiveOffer.GetTotalDiscountByPercentageForPrice(product.PriceInfo.GetTotalPrice(includeTax: true, reduceDiscount: false));
                totalDiscount += discPrice;

            }
            return totalDiscount;
        }





        //public double GetTotalFixedDiscount()
        //{
        //    double d = 0;
        //    foreach (var sp in this.SpecialOffers)
        //    {
        //        d += sp.DiscountFixed;
        //    }
        //    return d;
        //}
    }
}
