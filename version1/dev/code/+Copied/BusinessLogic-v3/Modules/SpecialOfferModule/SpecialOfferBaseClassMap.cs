using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.SpecialOfferModule
{

//BaseClassMap-File
    
    public abstract class SpecialOfferBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.SpecialOfferBaseMap_AutoGen<TData>
    	where TData : SpecialOfferBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
 
    }
   
}
