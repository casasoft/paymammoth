﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.SpecialOfferModule
{
    public class SpecialOfferManager
    {
        public SpecialOfferManager Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<SpecialOfferManager>(); }
        }

        private SpecialOfferManager()
        {

        }

        private List<SpecialOfferBase> specialOffersCached
        {
            get { return CS.General_v3.Util.PageUtil.GetCachedObject<List<SpecialOfferBase>>("SpecialOfferManager.specialOffersCached"); }
            set { CS.General_v3.Util.PageUtil.SetCachedObject("SpecialOfferManager.specialOffersCached", value, 60); }
        }

        private void loadFromDb()
        {
            var list = SpecialOfferBaseFactory.Instance.GetCurrentSpecialOffers(preloadData: true);
            specialOffersCached = list.ToList();
        }

        public IEnumerable<SpecialOfferBase> GetCurrentAvailableOffers()
        {
            if (specialOffersCached == null)
            {
                loadFromDb();
            }
            return specialOffersCached;
        }

    }
}
