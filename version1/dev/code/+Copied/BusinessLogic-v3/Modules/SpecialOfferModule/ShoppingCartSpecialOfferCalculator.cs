﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ShoppingCartModule;
using BusinessLogic_v3.Modules.ShoppingCartItemModule;
using BusinessLogic_v3.Modules.CategoryModule;
using Iesi.Collections.Generic;

namespace BusinessLogic_v3.Modules.SpecialOfferModule
{
    public class ShoppingCartSpecialOfferCalculator
    {
        private ShoppingCartBase _cart = null;
        public ShoppingCartSpecialOfferCalculator(ShoppingCartBase cart)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(cart, "Cart is required");
            this._cart = cart;
        }



        public double CalculateTotalDiscountForCart()
        {
            ReloadSpecialOffersApplicableOnlyToCart();
            double discount = 0;
            //discount += CalculateFixedDiscountForCart();
            foreach (var item in _cart.ShoppingCartItems)
            {
                var itemDiscount = CalculateTotalDiscountForCartItem(item);
                discount += itemDiscount;
            }
            
            return discount;
        }
        //public double CalculateFixedDiscountForCart()
        //{
        //    SpecialOfferCalculator calc = SpecialOfferCalculator.GetSpecialOfferCalculatorFromContext();
        //    calc.LoadOffersAvailableOnlyToCart(this._cart);
        //    return calc.GetTotalFixedDiscount();
        //    //var list = GetSpecialOffersApplicableOnlyToCart();
        //    //double totalDiscount = 0;
        //    //foreach (var sp in list)
        //    //{
        //    //    totalDiscount += sp.DiscountFixed;
        //    //}
        //    //return totalDiscount;
        //}
        public double CalculateTotalDiscountForCartItem(ShoppingCartItemBase item)
        {
            int qty = item.Quantity;
            double unitDiscount = CalculateDiscountForCartItemForOneUnit(item);
            return unitDiscount * qty;

            //double totalDiscount = 0;
            //var list = GetSpecialOffersApplicableOnlyToCart();
            //foreach (var sp in list)
            //{
            //    double itemDiscount = GetTotalDiscountForItemForSpecialOffer(item, sp);
            //    totalDiscount += itemDiscount;
            //}
            //return totalDiscount;
        }
        public double CalculateDiscountForCartItemForOneUnit(ShoppingCartItemBase item)
        {
            double totalDiscount = 0;
            SpecialOfferCalculator calc = SpecialOfferCalculator.GetSpecialOfferCalculatorFromContext();
            calc.LoadOffersAvailableOnlyToCart(item.ShoppingCart);
            double discountPrice = calc.CalculatePercentageDiscountForProduct(item.GetLinkedProduct());
            return discountPrice;

            //var list = GetSpecialOffersApplicableOnlyToCart();
            //foreach (var sp in list)
            //{
            //    double itemDiscount = GetDiscountForItemPerUnitForSpecialOffer(item, sp);
            //    totalDiscount += itemDiscount;
            //}
            //return totalDiscount;
        }
        //private HashedSet<ISpecialOfferBase> applicableSpecialOffers = null;
        /// <summary>
        /// Returns the applicable special offers.  This is cached, so subsequent calls to this will not automatically re-check.  This requires
        /// an explicit call to 'ReloadSpecialOffersApplicableToCart';
        /// </summary>
        /// <returns></returns>
        public HashedSet<ISpecialOfferBase> GetSpecialOffersApplicableOnlyToCart(bool forceReload = false)
        {
            SpecialOfferCalculator calc = SpecialOfferCalculator.GetSpecialOfferCalculatorFromContext();
            calc.LoadOffersAvailableOnlyToCart(this._cart);
            return calc.GetOffersUsedByCart(_cart);
            //if (applicableSpecialOffers == null || forceReload)
            //{
            //    ReloadSpecialOffersApplicableOnlyToCart();
            //}
            //HashedSet<ISpecialOfferBase> specialOffers = new HashedSet<ISpecialOfferBase>();
            //specialOffers.AddAll(this.applicableSpecialOffers);
            //return specialOffers;
            
        }

        public void ReloadSpecialOffersApplicableOnlyToCart()
        {
            return;
            var list = SpecialOfferBaseFactory.Instance.GetSpecialOffersWithCartRequirements(preloadData:true);
            
            //var list = SpecialOfferBaseFactory.Instance.FindAll(q);
            //List<ISpecialOfferBase> applicableOffers = new List<ISpecialOfferBase>();
            //foreach (var sp in list)
            //{
            //    if (CheckIfCartMeetsOfferRequirements(sp) && CheckIfOfferIsBeingUsed(sp))
            //    {
            //        applicableOffers.Add(sp);
            //    }
            //}
            ////add the voucher code, if specified
            //if (_cart.SpecialOfferVoucherCode != null && _cart.SpecialOfferVoucherCode.SpecialOffer != null)
            //{
            //    applicableOffers.Add(_cart.SpecialOfferVoucherCode);
            //}

            ////if (_cart.Member != null && _cart.Member.HasDiscountCode())
            ////{
            ////    applicableOffers.Add((ISpecialOfferBase) _cart.Member.GetCurrentDiscountCode());
            ////}

            ////split between exclusive and non exclusive

            //var nonExclusiveOffers = applicableOffers.Where(x => !x.IsExclusiveOffer).ToList();
            //var exclusiveOffers = applicableOffers.Where(x => x.IsExclusiveOffer).ToList();

            //HashedSet<ISpecialOfferBase> result = new HashedSet<ISpecialOfferBase>();
            //result.AddAll(nonExclusiveOffers);

            //if (exclusiveOffers.Count > 0)
            //{ //at least one exclusive offer
            //    result.Add(getBestSpecialOfferForCart(exclusiveOffers));
            //}

            
            //this.applicableSpecialOffers = result;
            //return result;
        }

        protected internal ISpecialOfferBase getBestSpecialOfferForCart(IEnumerable<ISpecialOfferBase> offers)
        {
            double bestDiscount = 0;
            ISpecialOfferBase bestOffer = null;
            if (offers != null)
            {
                foreach (var offer in offers)
                {
                    double offerDiscount = GetTotalDiscountForAllCartForSpecialOffer(offer);
                    if (offerDiscount > bestDiscount)
                    {
                        bestDiscount = offerDiscount;
                        bestOffer = offer;
                    }
                }
            }
            return bestOffer;
        }



        public List<ShoppingCartItemBase> GetItemsMatchingOffer(ISpecialOfferBase offer)
        {
            List<ShoppingCartItemBase> applicableItems = new List<ShoppingCartItemBase>();
            if (!offer.RequirementsCanMatchAnyItem && offer.HasCategoryRestrictions())
            {
                //offer requires only items in categories
                applicableItems.AddRange(_cart.GetShoppingCartItemsByProductCategory(offer.ApplicableCategories.Cast<CategoryBase>()));
            }
            else
            {
                applicableItems.AddRange(_cart.ShoppingCartItems);
            }
            return applicableItems;
        }

        public bool CheckIfCartMeetsOfferRequirements(ISpecialOfferBase offer)
        {
            bool ok = true;
            ok = ok && CheckIfCartMeetsItemTotalPriceRequirement(offer);
            ok = ok && CheckIfCartMeetsItemTotalQuantityRequirement(offer);
            
            return ok;

        }

        public bool CheckIfCartMeetsItemTotalQuantityRequirement(ISpecialOfferBase offer)
        {
            bool ok = false;
            if (offer.RequiredTotalItemsToBuy> 0)
            {
                List<ShoppingCartItemBase> applicableItems = GetItemsMatchingOffer(offer);

                int totalQty = 0;
                foreach (var item in applicableItems)
                {
                    int itemQty = item.GetQuantity();
                    totalQty += itemQty;
                    
                }
                ok = (totalQty>= offer.RequiredTotalItemsToBuy);
            }
            else
            {
                ok = true; //no restriction applies!
            }
            return ok;


        }
        public bool CheckIfCartMeetsItemTotalPriceRequirement(ISpecialOfferBase offer)
        {
            bool ok = false;
            if (offer.RequiredTotalToSpend > 0)
            {
                List<ShoppingCartItemBase> applicableItems = GetItemsMatchingOffer(offer);

                double totalPrice = 0;
                foreach (var item in applicableItems)
                {
                    double itemTotal = item.PriceInfo.GetTotalPrice(reduceDiscount: false);
                    totalPrice += itemTotal;
                }
                ok = (totalPrice >= offer.RequiredTotalToSpend);
            }
            else
            {
                ok = true; //no restriction applies!
            }
            return ok;


        }


        public double GetTotalDiscountForItemForSpecialOffer(ShoppingCartItemBase item, ISpecialOfferBase offer)
        {
            double discount = GetDiscountForItemPerUnitForSpecialOffer(item, offer);
            double total = discount * item.Quantity;
            return total;
        }
        public double GetDiscountForItemPerUnitForSpecialOffer(ShoppingCartItemBase item, ISpecialOfferBase offer)
        {
            double discount = 0;
            if (offer.CheckIfProductAppliesToSpecialOffer(item.GetLinkedProduct()))
            {
                double itemPrice = item.PriceInfo.GetUnitPrice(includeTax:true, reduceDiscount: false);
                discount = offer.GetTotalDiscountByPercentageForPrice(itemPrice);
            }
            return discount;
        }

        /// <summary>
        /// Returns the total discount.  Note that this does not take into consideration offer restriction, but just returns the discount.
        /// </summary>
        /// <param name="offer"></param>
        /// <returns></returns>
        public double GetTotalDiscountForAllCartForSpecialOffer(ISpecialOfferBase offer)
        {
            double totalDiscount = 0;
            //totalDiscount += offer.DiscountFixed;
            foreach (var item in _cart.ShoppingCartItems)
            {
                double itemDiscount = GetTotalDiscountForItemForSpecialOffer(item, offer);
                totalDiscount += itemDiscount;

            }
            return totalDiscount;
        }

        public bool CheckIfOfferIsBeingUsed(ISpecialOfferBase offer)
        {
            var discount = GetTotalDiscountForAllCartForSpecialOffer(offer);
            return (discount > 0);
        }


    }
}
