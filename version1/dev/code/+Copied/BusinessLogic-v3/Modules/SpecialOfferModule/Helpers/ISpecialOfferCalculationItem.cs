﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Classes.Pricing;

namespace BusinessLogic_v3.Modules.SpecialOfferModule.Helpers
{
    public interface ISpecialOfferCalculationItem
    {
        IProductBase Product { get; }
        int Quantity { get; }
        IPricingInfo PriceInfo { get; }


    }
}
