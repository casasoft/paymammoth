using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.ProductModule;

namespace BusinessLogic_v3.Modules.SpecialOfferModule
{

//IBaseClass-File
	
    public interface ISpecialOfferBase : BusinessLogic_v3.Modules._AutoGen.ISpecialOfferBaseAutoGen
    {

        void MarkUsed(OrderModule.OrderBase orderBase);

        bool IsCurrentlyAvailable();

        ISpecialOfferVoucherCodeBase CreateVoucherCode(string code, int quantity = 1, bool verifyExistsFirst = true);

        IEnumerable<ISpecialOfferVoucherCodeBase> GenerateVoucherCodes(int lengthOfCode, int amtToGenerate);

        bool HasCategoryRestrictions();

        

        
        bool CheckIfProductAppliesToSpecialOffer(IProductBase item);

        double GetTotalDiscountByPercentageForPrice(double itemPrice);

        
    }
}
