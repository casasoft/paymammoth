﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ProductModule;

namespace BusinessLogic_v3.Modules.CategoryModule
{
    public class CategoryProductsMover
    {

        private ICategoryBase fromCategory;
        private ICategoryBase toCategory;
        public void Move(ICategoryBase fromCategory, ICategoryBase toCategory)
        {
            this.fromCategory = fromCategory;
            this.toCategory = toCategory;
            this.MovedItems = new List<IProductBase>();
            IEnumerable<IProductBase> itemGroups = fromCategory.GetAllProductsUnderThis();
            foreach (var g in itemGroups)
            {
                g.RemoveCategoryWhoseParentIs(fromCategory);
                g.AddCategory(toCategory);
                g.Update();
            }
            this.MovedItems.AddRange(itemGroups);
        }
        public List<IProductBase> MovedItems { get; private set; }


    }
}
