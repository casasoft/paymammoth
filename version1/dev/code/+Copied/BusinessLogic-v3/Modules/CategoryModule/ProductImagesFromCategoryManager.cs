﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Extensions;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using BusinessLogic_v3.Modules.ProductVariationModule;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using NHibernate.Criterion;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Modules.CategoryModule
{
    public class ProductImagesFromCategoryManager
    {
        private CategoryBase _category { get; set; }

        private const string CACHE_KEY_PREFIX = "Cat_ProdImages_ForCatId_";

        public ProductImagesFromCategoryManager(CategoryBase ApplicableForCategory)
        {
            if(ApplicableForCategory == null)
            {
                throw new InvalidOperationException(
                    "You cannot create an instance of ProductImagesFromCategoryManager with a null ApplicableForCategory!!!");
            }
            this._category = ApplicableForCategory;
        }

        public IProductVariationMediaItemBase GetRandomProductImage()
        {
            IProductVariationMediaItemBase result = getRandomItemFromCache();
            if(result == null)
            {
                result = getFromDbAndCreateCache();
            }
            return result;
        }

        private IProductVariationMediaItemBase getRandomItemFromCache()
        {
            List<long> imagesIds =
                CS.General_v3.Util.PageUtil.GetCachedObject<List<long>>(CACHE_KEY_PREFIX + _category.ID);
            if(imagesIds!=null && imagesIds.Count > 0)
            {
                return getRandomFromListOfLong(imagesIds);
            }
            return null;
        }

        private IProductVariationMediaItemBase getRandomFromListOfLong(List<long> mediaItemIds)
        {
            IProductVariationMediaItemBase item = null;
            if (mediaItemIds != null && mediaItemIds.Count > 0)
            {
                int rndIndex = CS.General_v3.Util.Random.GetInt(0, mediaItemIds.Count - 1);
                long id = mediaItemIds[rndIndex];
                item = Factories.ProductVariationMediaItemFactory.GetByPrimaryKey(id);;
            }
            return item;
        }

        private IProductVariationMediaItemBase getFromDbAndCreateCache()
        {
            List<CategoryBase> allCatsAsList = (List<CategoryBase>)_category.GetChildCategories().ToList();
            allCatsAsList.Add(_category);
            HashedSet<CategoryBase> cats = new HashedSet<CategoryBase>();
            cats.AddAll(allCatsAsList);

            var query = ProductVariationMediaItemBase.Factory.GetQuery();
            var qo_prodVariation = query.JoinQueryOver<ProductVariationBase>(item => item.ProductVariation);
            var qo_prod = qo_prodVariation.JoinQueryOver<ProductBase>(item => item.Product);
            var qo_catLink = qo_prod.JoinQueryOver<ProductCategoryBase>(item => item.CategoryLinks);
            var qo_cat = qo_catLink.JoinQueryOver<CategoryBase>(item => item.Category);
            qo_cat.Where(Restrictions.On<CategoryBase>(item => item.ID).IsInG(cats.ConvertAll(item => item.ID).ToList()));
            query.RootCriteria.SetProjection(Projections.ProjectionList().Add(Projections.Property("ID")));
            IList<long> result = query.RootCriteria.List<long>();
            List<long> res = result.ToList();
            int timeout =
                Factories.SettingFactory.GetSettingValue<int>(
                    Enums.BusinessLogicSettingsEnum.RandomProductImagesFromCategoryCacheTimeoutValue);
            CS.General_v3.Util.PageUtil.SetCachedObject(CACHE_KEY_PREFIX + _category.ID, res, timeout);
            if (res != null && res.Count > 0)
            {
                return getRandomFromListOfLong(res);
            }
            return null;
        }
    }
}
