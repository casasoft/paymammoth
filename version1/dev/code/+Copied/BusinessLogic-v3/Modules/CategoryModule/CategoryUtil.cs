﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Iesi.Collections.Generic;

namespace BusinessLogic_v3.Modules.CategoryModule
{
    public static class CategoryUtil
    {
        /// <summary>
        /// Returns a flat list of all the subcategories, 
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        public static HashedSet<CategoryBase> GetAllSubCategoriesAsFlatList(IEnumerable<CategoryBase> categories)
        {
            HashedSet<CategoryBase> result = new HashedSet<CategoryBase>();
            if (categories != null)
            {
                foreach (var c in categories)
                {
                    result.AddAll(c.GetAllCategoriesUnderThis(includeThis: true));

                }
            }
            return result;

        }
    }
}
