﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.General.ParentChildLinks;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;
using BusinessLogic_v3.Classes.DbObjects;
using Iesi.Collections.Generic;

namespace BusinessLogic_v3.Modules.CategoryModule
{
    public class CategoryParentChildLinksCacher : ParentChildLinksCacher<CategoryBase>
    {
        public static CategoryParentChildLinksCacher Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<CategoryParentChildLinksCacher>(); }
        }
        private CategoryParentChildLinksCacher()
        {

            
        }


        protected override IEnumerable<IParentChildLink> getAllLinks()
        {
            var q = Category_ParentChildLinkBase.Factory.GetQuery();
            q = q.Fetch(item => item.ChildCategory).Eager;
            q = q.Fetch(item => item.ParentCategory).Eager;
            return Category_ParentChildLinkBase.Factory.FindAll(q);
            
        }

        protected override IBaseDbFactory dbFactory
        {
            get { return CategoryBase.Factory; }
        }
    }
}
