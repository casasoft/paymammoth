using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.CategoryModule
{   

//IBaseFactory-File

	public interface ICategoryBaseFactory : BusinessLogic_v3.Modules._AutoGen.ICategoryBaseFactoryAutoGen
    {

        ICategoryBase GetRootCategory(bool createIfDoesNotExist = true);
        ICategoryBase GetByIdentifier(Enum identifier, bool throwErrorIfDoesNotExist = true);


        IEnumerable<ICategoryBase> GetRootCategories();

    }

}
