using BusinessLogic_v3.Modules.CategoryImageModule;
using BusinessLogic_v3.Modules.EventModule;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;


namespace BusinessLogic_v3.Modules.CategoryModule
{
    using CS.General_v3.Classes.Interfaces.Hierarchy;
    using BusinessLogic_v3.Classes.MediaItems;

//IBaseClass-File
	
    public interface ICategoryBase : BusinessLogic_v3.Modules._AutoGen.ICategoryBaseAutoGen, IHierarchy
    {
        bool AddParentCategory(ICategoryBase category);

        void SetIdentifier(Enum identifier);
        /// <summary>
        /// Gets the first level child categories.
        /// </summary>
        /// <returns></returns>
        IEnumerable<ICategoryBase> GetChildCategories();
        /// <summary>
        /// Gets all the child categories recursively as a flat list.
        /// </summary>
        /// <returns></returns>
        IEnumerable<ICategoryBase> GetAllCategoriesUnderThis();
        IEnumerable<IEventBase> GetEvents();
        IEnumerable<ICategoryImageBase> GetAllCategoryImagesForThisAndChildren();
        bool AddChildCategory(ICategoryBase iCategoryBaseData);
        ICategoryImageBase GetRandomCategoryImage(bool getFromDatabase);
        IEnumerable<IProductBase> GetAllProductsUnderThis();
        IEnumerable<ICategoryBase> GetParentCategories(bool loadNonVisible);
        void RemoveParent(ICategoryBase iCategoryBaseData);

        void RemoveChild(ICategoryBase iCategoryBaseData);
        string GetFullCategoryTitleFromRoot(string delimiter, bool skipRoot);
        string GetTitleForSEO();
        new long ID { get; }
        IMediaItemImage<CategoryBase.IconSizingEnum> Icon { get; }
        ProductImagesFromCategoryManager ProductImagesManager { get; }
    }
}
