﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.CategoryModule
{
    public static class CategoryExtensions
    {
        public static CategoryBase GetCategoryByName(this IEnumerable<CategoryBase> list, string name)
        {
            CategoryBase cat = null;
            foreach (var c in list)
            {
                if (string.Compare(c.TitlePlural, name, true) == 0 || string.Compare(c.TitleSingular, name, true) == 0)
                {
                    cat = c;
                    break;
                }

            }
            return cat;
        }
        public static CategoryBase GetCategoryByName(this IEnumerable<CategoryBase> list, string name, CategoryBase parentCategory, bool createIfNotExists = true)
        {

            CategoryBase cat = GetCategoryByName(list, name);
            if (createIfNotExists && cat == null)
            {
                cat = CategoryBase.Factory.CreateNewItem();
                cat.TitleSingular = name;
                cat.TitlePlural = name;
                if (parentCategory != null)
                {
                    parentCategory.AddChildCategory(cat);

                }

                cat.CreateAndCommit();
                if (parentCategory != null)
                {
                    parentCategory.UpdateAndCommit();
                }
                // this.Add(cat);
            }
            return cat;
        }
    }
}
