using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.CategoryModule
{

//BaseClassMap-File
    
    public abstract class CategoryBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.CategoryBaseMap_AutoGen<TData>
    	where TData : CategoryBase
    {


        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
        protected override void ParentCategoryLinksMappingInfo(ICollectionMapInfo mapInfo)
        {
            mapInfo.CacheType = Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            mapInfo.CascadeType.SetAll();
                
            mapInfo.Inverse = false;
            base.ParentCategoryLinksMappingInfo(mapInfo);
        }

        protected override void ChildCategoryLinksMappingInfo(ICollectionMapInfo mapInfo)
        {
            mapInfo.FetchType = Enums.NHIBERNATE_COLLECTION_FETCHMODE.Select;
            mapInfo.CascadeType.SetAll();
            
            
            
            //// mapInfo.AddOrderBy(CS.General_v3.Util.ReflectionUtil<CategoryBase>.GetPropertyBySelector(item => item.Priority), CS.General_v3.Enums.SORT_TYPE.Ascending);
            //// mapInfo.AddOrderBy(CS.General_v3.Util.ReflectionUtil<CategoryBase>.GetPropertyBySelector(item => item.TitlePlural), CS.General_v3.Enums.SORT_TYPE.Ascending);
            mapInfo.CacheType = Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            base.ChildCategoryLinksMappingInfo(mapInfo);
        }

        protected override void Category_CultureInfosMappingInfo(ICollectionMapInfo mapInfo)
        {
            mapInfo.FetchType = Enums.NHIBERNATE_COLLECTION_FETCHMODE.Join;
            mapInfo.CacheType = Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            base.Category_CultureInfosMappingInfo(mapInfo);
        }

        

    }
   
}
