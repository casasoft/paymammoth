using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.AdvertModule
{

//BaseClassMap-File
    
    public abstract class AdvertBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.AdvertBaseMap_AutoGen<TData>
    	where TData : AdvertBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            
        }
        

    }
   
}
