using System.IO;
using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.AdvertModule
{
    using CS.General_v3.JavaScript.Data;

//BaseClass-File

    public abstract class AdvertBase : BusinessLogic_v3.Modules._AutoGen.AdvertBaseAutoGen, IAdvertBase, IMediaItemImageSpecificSizeInfo, IMediaItem
    {
    
		#region BaseClass-AutoGenerated
		#endregion


        /// <summary>
        /// When advert is clicked
        /// </summary>
        public void IncrementClickCounter()
        {
            
            this.AddClick();
            
        }
  
    	protected override void initPropertiesForNewItem()
        {
        	//Fill any default values for NEW items.
            
            base.initPropertiesForNewItem();
            this.Published = true;
            this.AdvertShownPerRound = 1;
            //this.AdvertRatio = 1;
            this.HrefTarget = CS.General_v3.Enums.HREF_TARGET.Blank;

        }


        public AdvertBase()
        {
            
        }

        
        public bool IsCurrentlyAvailable()
        {
            bool ok = false;
            var ad = this;
            if (!ad.Deleted && !ad._Temporary_Flag && (ad.Published) &&
                    (ad.ShowDateFrom == null || (ad.ShowDateFrom.HasValue && ad.ShowDateFrom.Value.Date <= CS.General_v3.Util.Date.Now.Date)) &&
                    (ad.ShowDateTo == null || (ad.ShowDateTo.HasValue && ad.ShowDateTo.Value.Date >= CS.General_v3.Util.Date.Now.Date)))
            {
                ok = true;
            }
            return ok;

        }
        private int? __lastAdvertShownPerRound = null;
        private int lastAdvertShownPerRound
        {
            get
            {
                if (__lastAdvertShownPerRound.HasValue)
                    return __lastAdvertShownPerRound.Value;
                else
                    return AdvertShownPerRound;
            }
        }
        public override int AdvertShownPerRound
        {
            get
            {
                int value = base.AdvertShownPerRound;
                if (value < 1)
                    value = 1;
                return value;
            }
            set
            {

                __lastAdvertShownPerRound = base.AdvertShownPerRound;

                int val = value;
                if (val < 1)
                    val = 1;

                base.AdvertShownPerRound = val;
            }
        }


        //public override double AdvertRatio
        //{
        //    get
        //    {
        //        return base.AdvertRatio;
        //    }
        //    set
        //    {
        //        __lastAdvertRatio = value;
        //        base.AdvertRatio = value;
        //    }
        //}

        

        #region Image Media Item Snippet (v3)

        // Image : This should be the name of the image, e.g MainPhoto, etc. 
        // AdvertBase : This should be the name of the class that is implementing this snippet, e.g EventBase or Member

        public enum ImageSizingEnum
        {
           

            [ImageSpecificSizeDefaultValues(Width = 200, Height = 200, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.MiddleMiddle)]
            Normal
        }

        public class ImageMediaItem : BaseMediaItemImage<ImageSizingEnum>
        {
            /* If you need to update SetAsMainItem, Priority, etc, these are fields/methods that can be overriden in the base class */


            public AdvertBase Item { get; private set; }
            public ImageMediaItem(AdvertBase item)
            {

                this.Item = item;

                MediaItemImageInfo imageInfo = new MediaItemImageInfo();
                imageInfo.SizesInfo.Add(item);
              
                imageInfo.DeleteDbItemOnRemove = false; //if you want that on delete of this image, the item is deleted, set this as true.

                this.MediaItemInfo = imageInfo;

            }



        }

        protected ImageMediaItem _m_Image = null;
        public ImageMediaItem MediaItem { get { if (_m_Image == null) _m_Image = new ImageMediaItem(this); return _m_Image; } }



        #endregion


        

        protected override void Save_After(OperationResult result, SaveBeforeAfterParams sParams)
        {
            bool isNotSavedInDB = !this.IsSavedInDB();
            bool adRatioChanged = false;


            if (isNotSavedInDB)
            {
                adRatioChanged = true;
            }
            else
            {

                if (this.AdvertShownPerRound != lastAdvertShownPerRound)
                    adRatioChanged = true;

            }

            //base.saveInDB(savePermanently, saveAsynchronously);
            if (adRatioChanged)
            {

                //this.AdvertSlot.UpdateAdvertRatios();

            }
             base.Save_After(result, sParams);
        }


        public override string Link
        {
            get
            {
                return base.Link;
            }
            set
            {
                //string s = CS.General_v3.Util.Text.EnsureTextIsWebLink(value);
                base.Link = value;

            }
        }
        public override string ToString()
        {
            return this.Title;
        }


        public virtual bool IsRoundReady()
        {
            return (this.RoundCounter >= this.AdvertShownPerRound);
        }
        public virtual void ResetRoundCounter()
        {
            this.RoundCounter = 0;
            this.Update();
        }
        /*public virtual void AddHit(bool autoSave)
        {
            this.RoundCounter++;
            if (autoSave)
                this.Save(true);
        }*/

        #region IAdvert Members

        #endregion

        protected string getGoogleAnalyticsTitle()
        {
            return this.Title + " [" + this.ID + "]";
        }

        public virtual string GetGoogleAnalyticsCodeForImpression(bool synchronousCode, string trackerVarName, string Category)
        {

            if (synchronousCode)
                return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_Sync(Category, this.getGoogleAnalyticsTitle(), "Impression", null, trackerVarName);
            else
                return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_ASync(Category, this.getGoogleAnalyticsTitle(), "Impression", null, trackerVarName);

        }
        public virtual string GetGoogleAnalyticsCodeForHit(bool synchronousCode, string trackerVarName, string Category)
        {

            if (synchronousCode)
                return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_Sync(Category, this.getGoogleAnalyticsTitle(), "Hit", null, trackerVarName);
            else
                return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_ASync(Category, this.getGoogleAnalyticsTitle(), "Hit", null, trackerVarName);

        }


        public virtual void AttachGoogleAnalyticsCodeToControl(bool synchronousCode, string trackerVarName, string Category, System.Web.UI.WebControls.WebControl ctrl)
        {
            ctrl.Attributes["onclick"] += GetGoogleAnalyticsCodeForHit(synchronousCode, trackerVarName, Category);

        }

       
        protected virtual string getAdvertUrlQueryString()
        {
            return string.Format("advertID={0}&r={1}", this.ID.ToString(), CS.General_v3.Util.Random.GetInt(0, Int32.MaxValue).ToString());
        }
        public virtual string GetAdvertUrl()
        {
            return "/advert_log.aspx?" + getAdvertUrlQueryString();
        }

        public JSObject GetJsObject(bool forFlash = false)
        {
            JSObject obj = forFlash ? new JSObjectFlash() : new JSObject();

            obj.AddProperty("id", ID);
            obj.AddProperty("title", Title);
            obj.AddProperty("link", this.Link);
            obj.AddProperty("advertLink", GetAdvertUrl());
            obj.AddProperty("itemURL", this.MediaItem.GetUrlForSpecificSize(ImageSizingEnum.Normal));
            obj.AddProperty("width", this.AdvertSlot.Width);
            obj.AddProperty("height", this.AdvertSlot.Height);
            return obj;
        }



        /*string CS.General_v3.Classes.Modules.Adverts.IAdvert.HRef
        {
            get
            {
                return this.Link;
            }
        }*/

        /*string CS.General_v3.Classes.Modules.Adverts.IAdvert.GetImageURL()
        {
            return this.Image.GetWebPathFromRoot(AdvertImageSize_Enum.Normal);
            
        }*/


     



        internal void CopyFromInterface(IAdvertBase advertTemp)
        {
            this.BackgroundColor = advertTemp.BackgroundColor;
            this.Description = advertTemp.Description;
            this.Link = advertTemp.Link;
            this.HrefTarget = advertTemp.HrefTarget;
            //this.BackgroundColor = advertTemp.MediaItemURL;
            //this.priori = advertTemp.Priority;
            this.Title = advertTemp.Title;


            {
                try
                {
                    string localPath = CS.General_v3.Util.PageUtil.MapPath(advertTemp.GetMediaItemUrl());
                    FileStream fs = new FileStream(localPath, FileMode.Open, FileAccess.Read);
                    this.MediaItem.UploadFile(fs, CS.General_v3.Util.IO.GetFilenameAndExtension(localPath));
                    fs.Dispose();
                }
                catch (FileNotFoundException ex)
                {

                }
                catch (FileLoadException ex)
                {

                }
            }
        }
        /*
        #region IAdvertBaseData Members


        public void IncrementImpressionCounter()
        {
            this.AddHit(true);
        }

        public void IncrementClickCounter()
        {
            
        }

        #endregion*/

        public virtual void AddImpression()
        {

            this.RoundCounter++;
            this.Update();
            addHit(Enums.ADVERT_HIT_TYPE.Impression);
        }
        private void addHit(Enums.ADVERT_HIT_TYPE hitType)
        {
            return;
            //string userIP = CS.General_v3.Util.PageUtil.GetUserIP();
            //AdvertHitCreator hitCreator = new AdvertHitCreator(this.ID, hitType, userIP);
            //hitCreator.AddHitOnSeperateThread();
        }

        public virtual void AddClick()
        {
            addHit(Enums.ADVERT_HIT_TYPE.Click);

        }


        



        #region IMediaItemImageSpecificSizeInfo Members

        int IMediaItemImageSpecificSizeInfo.Width
        {
            get { return this.AdvertSlot.Width; }
        }

        int IMediaItemImageSpecificSizeInfo.Height
        {
            get { return this.AdvertSlot.Height; }
        }

        CS.General_v3.Enums.CROP_IDENTIFIER IMediaItemImageSpecificSizeInfo.CropIdentifier
        {
            get { return CS.General_v3.Enums.CROP_IDENTIFIER.MiddleMiddle; }
        }

        CS.General_v3.Enums.IMAGE_FORMAT? IMediaItemImageSpecificSizeInfo.ImageFormat
        {
            get { return null; }
        }

        #endregion

        #region IMediaItem Members

        bool IMediaItem.IsEmpty()
        {
            return this.MediaItem.IsEmpty();
            
        }

        string IMediaItem.GetFilenameOnlyWithoutExtension()
        {
            return this.MediaItem.GetFilenameOnlyWithoutExtension();
            
        }

        string IMediaItem.GetLocalPath()
        {
            return this.MediaItem.GetLocalPath();
            
        }

        string IMediaItem.GetUrl(bool includeBaseUrl = true)
        {
            return this.MediaItem.GetUrl(includeBaseUrl);
            
        }

        string IMediaItem.GetThumbnailImageUrl(bool includeBaseUrl = true)
        {
            return this.MediaItem.GetThumbnailImageUrl(includeBaseUrl);
            
        }

        OperationResult IMediaItem.UploadFile(Stream fileContents, string fileName, bool autoSave = true)
        {
            return this.MediaItem.UploadFile(fileContents, fileName, autoSave);
            
        }

        void IMediaItem.Delete()
        {
            this.MediaItem.Delete();
            
        }

        #endregion

        #region IAdvertBase Members

        public string GetMediaItemUrl()
        {
            
            return this.MediaItem.GetUrl();
            
        }

        #endregion

        #region IAdvertBase Members


        IMediaItemImage<AdvertBase.ImageSizingEnum> IAdvertBase.MediaItem
        {
            get { return this.MediaItem; }
        }

        #endregion
    }
}
