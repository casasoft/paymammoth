﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ShoppingCartItemModule;

namespace BusinessLogic_v3.Modules.ShoppingCartModule
{
    public class ShoppingCartResultHelperBase
    {
        /// <summary>
        /// The Shopping Cart being affected.
        /// </summary>
        public ShoppingCartBase ShoppingCart { get; set; }

        /// <summary>
        /// The Shopping Cart Item which has just been affected.
        /// </summary>
        public ShoppingCartItemBase ShoppingCartItem { get; set; }

        /// <summary>
        /// If a cart item which has been added, has instead invoked an update of quantity since it was already included, the update quantity result is stored here.
        /// </summary>
        public Enums.SHOPPING_CART_ITEM_UPDATE_QTY_RESULT UpdateCartItemResult { get; set; }

        /// <summary>
        /// The amount of stock available in the system, which have just been assigned to this shopping cart item.
        /// </summary>
        public int AvailableQuantity { get; set; }

        /// <summary>
        /// The quantity requested by the user.
        /// </summary>
        public int RequestedQuantity { get; set; }
    }
}
