﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.ShoppingCartModule
{
    public class ShoppingCartManager
    {
        public const string CartCookie = "_cart";
        public const string CartCookieGuid = "_cartGuid";
        public const string CartCookieId = "_cartId";

        public static ShoppingCartManager Instance
        { 
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<ShoppingCartManager>(null); }
        }

        protected ShoppingCartManager()
        {

        }

        private IShoppingCartBase getCartForLoggedInMember(bool createIfNotExists)
        {
            IShoppingCartBase cart = null;
            var loginManager = BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance;
            if (loginManager.IsLoggedIn)
            {
                var user = loginManager.GetLoggedInUser();
                cart = user.GetShoppingCart(createIfNotExists: createIfNotExists);
            }
                
            return cart;
        }
        public void ClearTemporaryCartCookies()
        {
            CS.General_v3.Util.PageUtil.SetCookie(CartCookie, CartCookieId, null);
            CS.General_v3.Util.PageUtil.SetCookie(CartCookie, CartCookieGuid, null);
        }

        private string contextCookie_Guid
        {
            get { return CS.General_v3.Util.PageUtil.GetCookie(CartCookie, CartCookieGuid); }
            set { CS.General_v3.Util.PageUtil.SetCookie(CartCookie, CartCookieGuid, CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value)); } 
        }
        private long contextCookie_Id
        {
            get { return CS.General_v3.Util.PageUtil.GetCookie<long>(CartCookie, CartCookieId); }
            set { CS.General_v3.Util.PageUtil.SetCookie(CartCookie, CartCookieId, CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value)); } 
        }

        public IShoppingCartBase GetTemporaryCartForContext(bool createIfNotExists)
        {
            IShoppingCartBase cart = null;
            long cartId = contextCookie_Id;
            string cartGuid = contextCookie_Guid;
            if (cartId > 0 && !string.IsNullOrWhiteSpace(cartGuid))
            {
                cart = ShoppingCartBaseFactory.Instance.GetCartByGuidAndId(cartId, cartGuid);
            }
            if (cart == null && createIfNotExists)
            {
                using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                {
                    cart = ShoppingCartBaseFactory.Instance.CreateNewItem();
                    cart.IsTemporaryCart = true;
                    cart.Save();
                    t.Commit();

                }
                contextCookie_Id = cart.ID;
                contextCookie_Guid = cart.GUID;
            }

            return cart;
        }

        public IShoppingCartBase GetCurrentCartForContext(bool createIfNotExists)
        {
            IShoppingCartBase cart = null;
            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
            {

                if (BusinessLogic_v3.Modules.MemberModule.SessionManager.MemberSessionLoginManager.Instance.IsLoggedIn)
                {
                    cart = getCartForLoggedInMember(createIfNotExists);
                }
                else
                {
                    cart = GetTemporaryCartForContext(createIfNotExists);
                }

            }

            return cart;

        }

    }
}
