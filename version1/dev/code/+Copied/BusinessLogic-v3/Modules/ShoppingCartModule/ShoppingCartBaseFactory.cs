using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.ShoppingCartModule
{   

//BaseFactory-File

	public abstract class ShoppingCartBaseFactory : BusinessLogic_v3.Modules._AutoGen.ShoppingCartBaseFactoryAutoGen, IShoppingCartBaseFactory
    {

        public ShoppingCartBase GetCartByGuidAndId(long cartId, string guid)
        {
            ShoppingCartBase cart = GetByPrimaryKey(cartId);
            if (cart.VerifyGuid(guid))
            {
                return cart;
            }
            else
            {
                return null;
            }
        }

    }

}
