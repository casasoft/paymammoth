﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ShippingMethodModule;

namespace BusinessLogic_v3.Modules.ShoppingCartModule
{
    public class ShoppingCartDeliveryDetailsHelper
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Locality { get; set; }
        public string Postcode { get; set; }
        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 Country { get; set; }
        public IShippingMethodBase ShippingMethod { get; set; }
    }
}
