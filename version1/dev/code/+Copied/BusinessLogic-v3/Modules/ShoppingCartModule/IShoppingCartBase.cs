using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.ProductVariationModule;
using BusinessLogic_v3.Modules.ShoppingCartItemModule;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;
using BusinessLogic_v3.Modules.ShippingMethodModule;

namespace BusinessLogic_v3.Modules.ShoppingCartModule
{

//IBaseClass-File
	
    public interface IShoppingCartBase : BusinessLogic_v3.Modules._AutoGen.IShoppingCartBaseAutoGen
    {
        double GetTotalTaxAmount();
        IShoppingCartItemBase FindShoppingCartItemByProductVariation(IProductVariationBase productVariation);
        int GetTotalItemsInCart();
        ShoppingCartItemResultHelperBase UpdateShoppingCartItemQuantity(IProductVariationBase productVariation, int newQuantity, bool autoSave = true);
        IEnumerable<ISpecialOfferBase> GetAppliedSpecialOffers();

        /// <summary>
        /// Get the total price for all the items in this order.
        /// </summary>
        /// <param name="includeTax">True if tax is to be added with the total.</param>
        /// <param name="reduceDiscount">True if discount has to be deducted from the total.</param>
        /// <param name="deliveryInformation">If not null, the values of this class will be used to calculate the shipping information and included in the total.</param>
        /// <returns></returns>
        double GetTotalPrice(bool includeTax = true, bool reduceDiscount = true, bool includeShipping = false);
        ICultureDetailsBase GetCultureForShoppingCart();
        void UpdateCurrencyToMatchCurrentSession(bool autoSave = true);
        IMemberSubscriptionTypePriceBase GetLinkedSubscriptionTypePrice(out bool autoRenewEnabled);
        void ResetShoppingCart(bool autoSave = true);
        bool IsCartEmpty();
        string GetTotalCostAsHtml(bool includeCurrencySign, bool htmlEncode = true);
        bool HasItemsRequiringShipping();
        ShoppingCartResultHelperBase AddItemToShoppingCart(IProductVariationBase item, int productQuantity, bool autoSave = true);
        IOrderBase ConvertToOrder(ShoppingCartDeliveryDetailsHelper deliveryDetails = null, bool autoSave = true);
        IEnumerable<IShoppingCartItemBase> GetShoppingCartItemsWhichAreStillWithoutOrder();
        IEnumerable<IShoppingCartItemBase> GetAllShoppingCartItems();
        Enums.SET_VOUCHERCODE_RESULT SetSpecialOfferVoucherCode(ISpecialOfferVoucherCodeBase voucherCode, bool autoSave = true);
        Enums.SET_VOUCHERCODE_RESULT SetSpecialOfferVoucherCode(string voucherCode, bool autoSave = true);
        ISpecialOfferVoucherCodeBase GetSpecialOfferVoucherCode();
        double GetTotalDiscount();
        void RemoveSpecialOfferVoucherCode(bool autoSave = true);
        double GetTotalWeight();
        double GetDeliveryCostsForUserSelection();
        double GetDeliveryCostsForShippingMethod(IShippingMethodBase method,
                                                 CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 country);

        bool ContainsAtLeastOneItem();
        ShoppingCartSpecialOfferCalculator SpecialOfferCalculator { get; }
    }
}
