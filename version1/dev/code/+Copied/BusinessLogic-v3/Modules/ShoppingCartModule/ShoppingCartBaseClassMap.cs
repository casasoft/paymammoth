using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.ShoppingCartModule
{

//BaseClassMap-File
    
    public abstract class ShoppingCartBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.ShoppingCartBaseMap_AutoGen<TData>
    	where TData : ShoppingCartBase
    {

        protected override void GUIDMappingInfo(IPropertyMapInfo mapInfo)
        {
            base.GUIDMappingInfo(mapInfo);
            mapInfo.IsIndexed = true;
            mapInfo.Length = 100;
        }
        
        
 
    }
   
}
