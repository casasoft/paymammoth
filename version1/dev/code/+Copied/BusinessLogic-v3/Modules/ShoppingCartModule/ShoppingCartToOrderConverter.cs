﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules.ShippingMethodModule;
using BusinessLogic_v3.Modules.ShoppingCartItemModule;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;
using BusinessLogic_v3.Modules.OrderItemModule;

namespace BusinessLogic_v3.Modules.ShoppingCartModule
{
    public class ShoppingCartToOrderConverter
    {
        public ShoppingCartBase Cart { get; private set; }

        public ShoppingCartToOrderConverter(ShoppingCartBase cart)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(cart, "cart is required");
            this.Cart = cart;
        }

        private OrderBase _order = null;
        //private void _copySpecialOfferVoucherCode()
        //{
        //    var voucherCode = Cart.GetSpecialOfferVoucherCode();
        //    if (voucherCode != null)
        //    {

        //        _order.SpecialOfferVoucherCode = (SpecialOfferVoucherCodeBase)voucherCode;
        //        _order.SpecialOfferVoucherCodeOfferTitle = voucherCode.GetSpecialOfferTitle();
        //        _order.SpecialOfferVoucherCodeString = voucherCode.VoucherCode;


        //    }
        //}
        private void _copyApplicableSpecialOffers()
        {

            var offerList = Cart.GetAppliedSpecialOffers();
            foreach (var offer in offerList)
            {
                _order.AddApplicableSpecialOffer(offer);


            }
            {
                var code = (SpecialOfferVoucherCodeBase)Cart.GetSpecialOfferVoucherCode();
                if (code != null)
                {
                    _order.AddApplicableSpecialOffer(code);
                }
            }


        }
        private void copySpecialOffersInformation()
        {

            _copyApplicableSpecialOffers();
        }

        private void addCartItemToOrder(ShoppingCartItemBase cartItem)
        {
            OrderItemBase orderItem = null;



            orderItem = (OrderItemBase)Factories.OrderItemFactory.CreateNewItem();
            orderItem.AutoRenewSubscriptionTypePrice = cartItem.AutoRenewSubscriptionTypePrice;
            orderItem.DiscountPerUnit = cartItem.PriceInfo.Discount;
            orderItem.ItemReference = cartItem.GetItemReference();
            orderItem.ItemType = cartItem.GetItemType();
            orderItem.LinkedProductVariation = cartItem.LinkedProductVariation;
            orderItem.LinkedProductVariationColour = cartItem.LinkedProductVariation != null && cartItem.LinkedProductVariation.Colour != null
                                                            ? cartItem.LinkedProductVariation.Colour.Title
                                                            : null;
            orderItem.LinkedProductVariationSize = cartItem.LinkedProductVariation != null
                                                        ? cartItem.LinkedProductVariation.Size
                                                        : null;
            orderItem.Order = (OrderBase)_order;
            orderItem.TaxRate = cartItem.PriceInfo.TaxRate;

            orderItem.PriceIncTaxPerUnit = cartItem.PriceInfo.GetUnitPrice(includeTax:true, reduceDiscount:false);
            orderItem.Quantity = cartItem.Quantity;
            orderItem.ShipmentWeight = cartItem.GetShipmentWeight();
            //orderItem.SpecialOffer = SpecialOffer;
            //orderItem.SpecialOfferTitle = SpecialOffer != null ? SpecialOffer.Title : null;
            //orderItem.SpecialOfferVoucherCode = SpecialOfferVoucherCode;
            orderItem.SubscriptionTypePrice = cartItem.LinkedSubscriptionTypePrice;
            //orderItem.TaxAmountPerUnit = cartItem.PriceInfo.GetTaxAmount();
            orderItem.Title = cartItem.GetItemTitle();




            //orderItem.copyDetailsFromLinkedItemsToOrder();
            cartItem.additionalFieldsToParseWhenConvertingToOrderItem(orderItem);

            orderItem.Save();
            cartItem.OrderLinkWhenConverted = _order;
            _order.LinkedShoppingCartItems.Add(cartItem);
            _order.OrderItems.Add(orderItem);

        }
        private void convertCartItems()
        {
            IEnumerable<IShoppingCartItemBase> cartItems = Cart.ShoppingCartItems; // GetShoppingCartItemsWhichAreStillWithoutOrder();
            foreach (ShoppingCartItemBase shoppingCartItem in cartItems)
            {
                addCartItemToOrder(shoppingCartItem);
                removeShoppingCartItem(shoppingCartItem);

            }
        }
        private void removeShoppingCartItem(IShoppingCartItemBase item )
        {
            item.Delete();
        }



        /// <summary>
        /// Converts the shopping cart to an order, and confirms the order.
        /// </summary>
        /// <param name="deliveryDetails"></param>
        /// <param name="autoSave"></param>
        /// <returns></returns>
        public OrderBase ConvertToOrderAndConfirm(ShoppingCartDeliveryDetailsHelper deliveryDetails = null, bool autoSave = true)
        {



            OrderBase order = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionBasedOnAutoSaveBool(autoSave))
            {
                //create a new order
                order = (OrderBase)Factories.OrderFactory.CreateNewItem();
                this._order = order;
                order.Member = this.Cart.Member;
                order.CopyDetailsFromMember(this.Cart.Member, true);
                order.OrderCurrency = this.Cart.Currency;
                order.Save();

                copySpecialOffersInformation();

                //order.ShippingMethod = this.ShippingMethod;

                if (deliveryDetails != null)
                {
                    order.SetShippingDetails(deliveryDetails.Country, deliveryDetails.Address1, deliveryDetails.Address2, deliveryDetails.Address3, deliveryDetails.Postcode, deliveryDetails.Locality);
                    order.ShippingMethod = (ShippingMethodBase)deliveryDetails.ShippingMethod;
                    order.TotalShippingCost = Cart.GetDeliveryCostsForShippingMethod(order.ShippingMethod, deliveryDetails.Country);
                    order.ShipmentType = order.ShippingMethod != null ? order.ShippingMethod.Title : null;
                }
                order.Save();
                convertCartItems();
                //order.FixedDiscount = this.Cart.GetFixedDiscountForOrder();
                
                order.Save();


                Cart.Save();
                t.CommitIfNotNull();
            }
            order.ConfirmOrder();
            return order;
        }

    }
}
