using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.ProductRelatedProductLinkModule
{

//BaseClassMap-File
    
    public abstract class ProductRelatedProductLinkBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.ProductRelatedProductLinkBaseMap_AutoGen<TData>
    	where TData : ProductRelatedProductLinkBase
    {
        
        
 
    }
   
}
