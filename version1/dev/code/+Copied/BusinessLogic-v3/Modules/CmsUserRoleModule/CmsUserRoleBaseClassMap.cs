using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.CmsUserRoleModule
{

//BaseClassMap-File
    
    public abstract class CmsUserRoleBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.CmsUserRoleBaseMap_AutoGen<TData>
    	where TData : CmsUserRoleBase
    {


        protected override void CmsUsersMappingInfo(ICollectionMapInfo mapInfo)
        {
            mapInfo.Inverse = true;
            base.CmsUsersMappingInfo(mapInfo);
        }
    }
   
}
