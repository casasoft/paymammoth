using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.EmailLogModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.EmailLogModule.EmailLogBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.EmailLogModule.IEmailLogBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.EmailLogModule.EmailLogBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.EmailLogModule.IEmailLogBase item)
        {
        	return BusinessLogic_v3.Frontend.EmailLogModule.EmailLogBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EmailLogBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<EmailLogBaseFrontend, BusinessLogic_v3.Modules.EmailLogModule.IEmailLogBase>

    {
		
        
        protected EmailLogBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
