using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Modules.CmsUserRoleModule;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;

namespace BusinessLogic_v3.Modules._AutoGen
{
    
    public abstract class CmsUserRoleBaseMap_AutoGen<TData> : BusinessLogic_v3.Classes.NHibernateClasses.Mappings.BaseClassMap<TData>
    	where TData : CmsUserRoleBase
    {
        public CmsUserRoleBaseMap_AutoGen()
        {
            
        }
        
        


// [baseclassmap_properties]

//BaseClassMap_Property
             
        protected virtual void TitleMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        



// [baseclassmap_collections]

//BaseClassMap_Collection_ManyToManyRight

        protected virtual void CmsUsersMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
           // mapInfo.RelationshipType = Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.ManyToMany;               
           if (string.IsNullOrEmpty(mapInfo.ManyToManyTableName )) mapInfo.ManyToManyTableName = "CmsUserRoles";
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }  
           
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ParentKey)) mapInfo.ParentKey = "RoleID";
           if (string.IsNullOrEmpty(mapInfo.ChildKey )) mapInfo.ChildKey = "CmsUserID";
            
        }
     
     
            
        
        protected override void initMappings()
        {
           
            
//[BaseClassMap_InitProperties]

//[BaseClassMap_InitCollections]                
            
            base.initMappings();
        }
        
        
        
        
        
    }
   
}
