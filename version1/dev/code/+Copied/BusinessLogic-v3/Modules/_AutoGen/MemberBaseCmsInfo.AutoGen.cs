using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Cms.MemberModule;
using BusinessLogic_v3.Frontend.MemberModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberModule.MemberBase>
    {
		public MemberBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberModule.MemberBase dbItem)
            : base(BusinessLogic_v3.Cms.MemberModule.MemberBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberBaseFrontend FrontendItem
        {
            get { return (MemberBaseFrontend)base.FrontendItem; }

        }
        public new MemberBase DbItem
        {
            get
            {
                return (MemberBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateRegistered { get; protected set; }

        public CmsPropertyInfo Username { get; protected set; }

        public CmsPropertyInfo Password { get; protected set; }

        public CmsPropertyInfo FirstName { get; protected set; }

        public CmsPropertyInfo LastName { get; protected set; }

        public CmsPropertyInfo Gender { get; protected set; }

        public CmsPropertyInfo DateOfBirth { get; protected set; }

        public CmsPropertyInfo Address1 { get; protected set; }

        public CmsPropertyInfo Address2 { get; protected set; }

        public CmsPropertyInfo Address3 { get; protected set; }

        public CmsPropertyInfo Locality { get; protected set; }

        public CmsPropertyInfo State { get; protected set; }

        public CmsPropertyInfo PostCode { get; protected set; }

        public CmsPropertyInfo Country { get; protected set; }

        public CmsPropertyInfo IDCard { get; protected set; }

        public CmsPropertyInfo LastLoggedIn { get; protected set; }

        public CmsPropertyInfo _LastLoggedIn_New { get; protected set; }

        public CmsPropertyInfo Accepted { get; protected set; }

        public CmsPropertyInfo Email { get; protected set; }

        public CmsPropertyInfo Telephone { get; protected set; }

        public CmsPropertyInfo Mobile { get; protected set; }

        public CmsPropertyInfo Fax { get; protected set; }

        public CmsPropertyInfo Remarks { get; protected set; }

        public CmsPropertyInfo Website { get; protected set; }

        public CmsPropertyInfo ActivatedOn { get; protected set; }

        public CmsPropertyInfo ActivatedIP { get; protected set; }

        public CmsPropertyInfo ActivationCode { get; protected set; }

        public CmsPropertyInfo SessionGUID { get; protected set; }

        public CmsPropertyInfo PreferredCultureInfo { get; protected set; }

        public CmsPropertyInfo ReferredByMember { get; protected set; }

        public CmsPropertyInfo AccountTerminated { get; protected set; }

        public CmsPropertyInfo AccountTerminatedOn { get; protected set; }

        public CmsPropertyInfo AccountTerminatedIP { get; protected set; }

        public CmsPropertyInfo ForgottenPassCode { get; protected set; }

        public CmsPropertyInfo SubscribedToNewsletter { get; protected set; }

        public CmsPropertyInfo CurrentShoppingCart { get; protected set; }

        public CmsPropertyInfo HowDidYouFindUs { get; protected set; }

        public CmsPropertyInfo LinkedAffiliate { get; protected set; }

        public CmsPropertyInfo CompanyName { get; protected set; }

        public CmsPropertyInfo PasswordEncryptionType { get; protected set; }

        public CmsPropertyInfo PasswordSalt { get; protected set; }

        public CmsPropertyInfo PasswordIterations { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo CurrentSubscription { get; protected set; }

        public CmsPropertyInfo CurrentSubscriptionStartDate { get; protected set; }

        public CmsPropertyInfo CurrentSubscriptionEndDate { get; protected set; }

        public CmsPropertyInfo SubscriptionNotification1Sent { get; protected set; }

        public CmsPropertyInfo SubscriptionNotification2Sent { get; protected set; }

        public CmsPropertyInfo Paid { get; protected set; }

        public CmsPropertyInfo Activated { get; protected set; }

        public CmsPropertyInfo MiddleName { get; protected set; }

        public CmsPropertyInfo SubscriptionExpiryNotificationSent { get; protected set; }

        public CmsPropertyInfo IsSubscriptionExpired { get; protected set; }

        public CmsPropertyInfo ReferralSuccessful { get; protected set; }

        public CmsPropertyInfo ReferredFromUrl { get; protected set; }

        public CmsPropertyInfo AccountBalance { get; protected set; }

        public CmsPropertyInfo Blocked { get; protected set; }

        public CmsPropertyInfo SelfExcludedUntil { get; protected set; }

        public CmsPropertyInfo SelfExclusionSetOn { get; protected set; }

        public CmsPropertyInfo SelfExclusionSetByIp { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ContactForms { get; protected set; }
        

        public CmsCollectionInfo Members { get; protected set; }
        

        public CmsCollectionInfo AccountBalanceHistoryItems { get; protected set; }
        

        public CmsCollectionInfo SentReferrals { get; protected set; }
        

        public CmsCollectionInfo RegisteredReferrals { get; protected set; }
        

        public CmsCollectionInfo ResponsibleLimits { get; protected set; }
        

        public CmsCollectionInfo MemberSubscriptionLinks { get; protected set; }
        

        public CmsCollectionInfo Orders { get; protected set; }
        

        public CmsCollectionInfo PaymentTransactions { get; protected set; }
        

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.CategoryModule.CategoryBaseCmsInfo> SubscribedCategories { get; protected set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ContactForms = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.ContactForms),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector(item => item.SentByMember)));
		
		//InitCollectionBaseOneToMany
		this.Members = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.Members),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.ReferredByMember)));
		
		//InitCollectionBaseOneToMany
		this.AccountBalanceHistoryItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.AccountBalanceHistoryItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase>.GetPropertyBySelector(item => item.Member)));
		
		//InitCollectionBaseOneToMany
		this.SentReferrals = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.SentReferrals),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector(item => item.SentByMember)));
		
		//InitCollectionBaseOneToMany
		this.RegisteredReferrals = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.RegisteredReferrals),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector(item => item.RegisteredMember)));
		
		//InitCollectionBaseOneToMany
		this.ResponsibleLimits = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.ResponsibleLimits),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase>.GetPropertyBySelector(item => item.Member)));
		
		//InitCollectionBaseOneToMany
		this.MemberSubscriptionLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.MemberSubscriptionLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase>.GetPropertyBySelector(item => item.Member)));
		
		//InitCollectionBaseOneToMany
		this.Orders = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.Orders),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector(item => item.Member)));
		
		//InitCollectionBaseOneToMany
		this.PaymentTransactions = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.PaymentTransactions),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.PaymentTransactionModule.PaymentTransactionBase>.GetPropertyBySelector(item => item.Member)));
//InitFieldManyToManyBase_RightSide
		this.SubscribedCategories = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.CategoryModule.CategoryBaseCmsInfo>(
								item => item.SubscribedCategories, 
								Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
