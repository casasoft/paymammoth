using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.CategorySpecificationModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "CategorySpecification")]
    public abstract class CategorySpecificationBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ICategorySpecificationBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static CategorySpecificationBaseFactory Factory
        {
            get
            {
                return CategorySpecificationBaseFactory.Instance; 
            }
        }    
        /*
        public static CategorySpecificationBase CreateNewItem()
        {
            return (CategorySpecificationBase)Factory.CreateNewItem();
        }

		public static CategorySpecificationBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<CategorySpecificationBase, CategorySpecificationBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static CategorySpecificationBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static CategorySpecificationBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<CategorySpecificationBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<CategorySpecificationBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<CategorySpecificationBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.CATEGORY_SPECIFICATION_DATA_TYPE _datatype;
        
        public virtual BusinessLogic_v3.Enums.CATEGORY_SPECIFICATION_DATA_TYPE DataType 
        {
        	get
        	{
        		return _datatype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datatype,value);
        		_datatype = value;
        		
        	}
        }
        
/*
		public virtual long? MeasurementUnitID
		{
		 	get 
		 	{
		 		return (this.MeasurementUnit != null ? (long?)this.MeasurementUnit.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase _measurementunit;
        public virtual BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase MeasurementUnit 
        {
        	get
        	{
        		return _measurementunit;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_measurementunit,value);
        		_measurementunit = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MeasurementUnitModule.IMeasurementUnitBase BusinessLogic_v3.Modules._AutoGen.ICategorySpecificationBaseAutoGen.MeasurementUnit 
        {
            get
            {
            	return this.MeasurementUnit;
            }
            set
            {
            	this.MeasurementUnit = (BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase) value;
            }
        }
            
/*
		public virtual long? CategoryID
		{
		 	get 
		 	{
		 		return (this.Category != null ? (long?)this.Category.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _category;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase Category 
        {
        	get
        	{
        		return _category;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_category,value);
        		_category = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.ICategorySpecificationBaseAutoGen.Category 
        {
            get
            {
            	return this.Category;
            }
            set
            {
            	this.Category = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
