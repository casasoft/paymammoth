using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.AttachmentModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Attachment")]
    public abstract class AttachmentBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IAttachmentBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static AttachmentBaseFactory Factory
        {
            get
            {
                return AttachmentBaseFactory.Instance; 
            }
        }    
        /*
        public static AttachmentBase CreateNewItem()
        {
            return (AttachmentBase)Factory.CreateNewItem();
        }

		public static AttachmentBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<AttachmentBase, AttachmentBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static AttachmentBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static AttachmentBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<AttachmentBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<AttachmentBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<AttachmentBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _filename;
        
        public virtual string FileName 
        {
        	get
        	{
        		return _filename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_filename,value);
        		_filename = value;
        		
        	}
        }
        
/*
		public virtual long? TicketCommentID
		{
		 	get 
		 	{
		 		return (this.TicketComment != null ? (long?)this.TicketComment.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase _ticketcomment;
        public virtual BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase TicketComment 
        {
        	get
        	{
        		return _ticketcomment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ticketcomment,value);
        		_ticketcomment = value;
        	}
        }
            
        BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBase BusinessLogic_v3.Modules._AutoGen.IAttachmentBaseAutoGen.TicketComment 
        {
            get
            {
            	return this.TicketComment;
            }
            set
            {
            	this.TicketComment = (BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _contenttype;
        
        public virtual string ContentType 
        {
        	get
        	{
        		return _contenttype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_contenttype,value);
        		_contenttype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _datesubmitted;
        
        public virtual DateTime DateSubmitted 
        {
        	get
        	{
        		return _datesubmitted;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datesubmitted,value);
        		_datesubmitted = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
