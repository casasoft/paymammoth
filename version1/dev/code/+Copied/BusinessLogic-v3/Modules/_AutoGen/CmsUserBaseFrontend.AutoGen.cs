using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.CmsUserModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.CmsUserModule.CmsUserBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.CmsUserModule.CmsUserBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase item)
        {
        	return BusinessLogic_v3.Frontend.CmsUserModule.CmsUserBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CmsUserBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<CmsUserBaseFrontend, BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase>

    {
		
        
        protected CmsUserBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
