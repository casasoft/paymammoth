using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.PaymentTransactionModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.PaymentTransactionModule.PaymentTransactionBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.PaymentTransactionModule.IPaymentTransactionBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.PaymentTransactionModule.PaymentTransactionBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.PaymentTransactionModule.IPaymentTransactionBase item)
        {
        	return BusinessLogic_v3.Frontend.PaymentTransactionModule.PaymentTransactionBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class PaymentTransactionBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<PaymentTransactionBaseFrontend, BusinessLogic_v3.Modules.PaymentTransactionModule.IPaymentTransactionBase>

    {
		
        
        protected PaymentTransactionBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
