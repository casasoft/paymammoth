using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;
using BusinessLogic_v3.Modules.TicketModule;


namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public interface ITicketBaseFactoryAutoGen : IBaseDbFactory<BusinessLogic_v3.Modules.TicketModule.ITicketBase>
    {
    
    }
}
