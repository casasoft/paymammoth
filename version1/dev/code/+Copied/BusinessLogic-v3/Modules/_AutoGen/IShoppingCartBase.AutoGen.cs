using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ShoppingCartModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IShoppingCartBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        DateTime DateCreated { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase Member {get; set; }

		//Iproperty_normal
        bool Cancelled { get; set; }
		//Iproperty_normal
        DateTime? CancelledOn { get; set; }
		//Iproperty_normal
        DateTime LastUpdatedOn { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase Currency {get; set; }

		//Iproperty_normal
        string GUID { get; set; }
		//Iproperty_normal
        bool IsTemporaryCart { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase> ShoppingCartItems { get; }

 
		

    	
    	
      
      

    }
}
