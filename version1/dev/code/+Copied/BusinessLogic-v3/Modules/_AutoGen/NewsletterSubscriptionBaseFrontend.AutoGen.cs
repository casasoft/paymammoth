using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.NewsletterSubscriptionModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.NewsletterSubscriptionModule.NewsletterSubscriptionBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.NewsletterSubscriptionModule.INewsletterSubscriptionBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.NewsletterSubscriptionModule.NewsletterSubscriptionBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.NewsletterSubscriptionModule.INewsletterSubscriptionBase item)
        {
        	return BusinessLogic_v3.Frontend.NewsletterSubscriptionModule.NewsletterSubscriptionBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class NewsletterSubscriptionBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<NewsletterSubscriptionBaseFrontend, BusinessLogic_v3.Modules.NewsletterSubscriptionModule.INewsletterSubscriptionBase>

    {
		
        
        protected NewsletterSubscriptionBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
