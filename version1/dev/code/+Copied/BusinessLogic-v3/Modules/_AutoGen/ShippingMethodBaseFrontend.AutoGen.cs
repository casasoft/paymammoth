using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ShippingMethodModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ShippingMethodModule.ShippingMethodBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ShippingMethodModule.IShippingMethodBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ShippingMethodModule.ShippingMethodBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ShippingMethodModule.IShippingMethodBase item)
        {
        	return BusinessLogic_v3.Frontend.ShippingMethodModule.ShippingMethodBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ShippingMethodBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ShippingMethodBaseFrontend, BusinessLogic_v3.Modules.ShippingMethodModule.IShippingMethodBase>

    {
		
        
        protected ShippingMethodBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
