using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContactFormModule;
using BusinessLogic_v3.Cms.ContactFormModule;
using BusinessLogic_v3.Frontend.ContactFormModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContactFormBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>
    {
		public ContactFormBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase dbItem)
            : base(BusinessLogic_v3.Cms.ContactFormModule.ContactFormBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContactFormBaseFrontend FrontendItem
        {
            get { return (ContactFormBaseFrontend)base.FrontendItem; }

        }
        public new ContactFormBase DbItem
        {
            get
            {
                return (ContactFormBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateSent { get; protected set; }

        public CmsPropertyInfo Name { get; protected set; }

        public CmsPropertyInfo Company { get; protected set; }

        public CmsPropertyInfo Subject { get; protected set; }

        public CmsPropertyInfo Email { get; protected set; }

        public CmsPropertyInfo Telephone { get; protected set; }

        public CmsPropertyInfo Mobile { get; protected set; }

        public CmsPropertyInfo Address1 { get; protected set; }

        public CmsPropertyInfo Address2 { get; protected set; }

        public CmsPropertyInfo Address3 { get; protected set; }

        public CmsPropertyInfo ZIPCode { get; protected set; }

        public CmsPropertyInfo State { get; protected set; }

        public CmsPropertyInfo CountryStr { get; protected set; }

        public CmsPropertyInfo City { get; protected set; }

        public CmsPropertyInfo Fax { get; protected set; }

        public CmsPropertyInfo Website { get; protected set; }

        public CmsPropertyInfo IP { get; protected set; }

        public CmsPropertyInfo Enquiry { get; protected set; }

        public CmsPropertyInfo Remark { get; protected set; }

        public CmsPropertyInfo SentByMember { get; protected set; }

        public CmsPropertyInfo Surname { get; protected set; }

        public CmsPropertyInfo Vacancy { get; protected set; }

        public CmsPropertyInfo Username { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
