using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ShoppingCartItemModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ShoppingCartItemModule.ShoppingCartItemBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ShoppingCartItemModule.ShoppingCartItemBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase item)
        {
        	return BusinessLogic_v3.Frontend.ShoppingCartItemModule.ShoppingCartItemBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ShoppingCartItemBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ShoppingCartItemBaseFrontend, BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase>

    {
		
        
        protected ShoppingCartItemBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
