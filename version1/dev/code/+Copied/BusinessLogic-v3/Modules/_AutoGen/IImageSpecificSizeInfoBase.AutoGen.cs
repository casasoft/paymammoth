using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IImageSpecificSizeInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBase ImageSizingInfo {get; set; }

		//Iproperty_normal
        int Width { get; set; }
		//Iproperty_normal
        int Height { get; set; }
		//Iproperty_normal
        string Title { get; set; }
		//Iproperty_normal
        string Identifier { get; set; }
		//Iproperty_normal
        string Description { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.CROP_IDENTIFIER CropIdentifier { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.IMAGE_FORMAT? ImageFormat { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
