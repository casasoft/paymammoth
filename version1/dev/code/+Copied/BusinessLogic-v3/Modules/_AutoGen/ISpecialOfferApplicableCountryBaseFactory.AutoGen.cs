using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;
using BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule;


namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public interface ISpecialOfferApplicableCountryBaseFactoryAutoGen : IBaseDbFactory<BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule.ISpecialOfferApplicableCountryBase>
    {
    
    }
}
