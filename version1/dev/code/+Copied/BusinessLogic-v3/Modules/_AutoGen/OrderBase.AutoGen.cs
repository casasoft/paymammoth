using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.OrderModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Order")]
    public abstract class OrderBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IOrderBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static OrderBaseFactory Factory
        {
            get
            {
                return OrderBaseFactory.Instance; 
            }
        }    
        /*
        public static OrderBase CreateNewItem()
        {
            return (OrderBase)Factory.CreateNewItem();
        }

		public static OrderBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<OrderBase, OrderBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static OrderBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static OrderBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<OrderBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<OrderBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<OrderBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private DateTime _datecreated;
        
        public virtual DateTime DateCreated 
        {
        	get
        	{
        		return _datecreated;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datecreated,value);
        		_datecreated = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _reference;
        
        public virtual string Reference 
        {
        	get
        	{
        		return _reference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_reference,value);
        		_reference = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customerfirstname;
        
        public virtual string CustomerFirstName 
        {
        	get
        	{
        		return _customerfirstname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customerfirstname,value);
        		_customerfirstname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customeremail;
        
        public virtual string CustomerEmail 
        {
        	get
        	{
        		return _customeremail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customeremail,value);
        		_customeremail = value;
        		
        	}
        }
        
/*
		public virtual long? MemberID
		{
		 	get 
		 	{
		 		return (this.Member != null ? (long?)this.Member.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _member;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase Member 
        {
        	get
        	{
        		return _member;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_member,value);
        		_member = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IOrderBaseAutoGen.Member 
        {
            get
            {
            	return this.Member;
            }
            set
            {
            	this.Member = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _customeraddress1;
        
        public virtual string CustomerAddress1 
        {
        	get
        	{
        		return _customeraddress1;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customeraddress1,value);
        		_customeraddress1 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customeraddress2;
        
        public virtual string CustomerAddress2 
        {
        	get
        	{
        		return _customeraddress2;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customeraddress2,value);
        		_customeraddress2 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customeraddress3;
        
        public virtual string CustomerAddress3 
        {
        	get
        	{
        		return _customeraddress3;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customeraddress3,value);
        		_customeraddress3 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _authcode;
        
        public virtual string AuthCode 
        {
        	get
        	{
        		return _authcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_authcode,value);
        		_authcode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? _customercountry;
        
        public virtual CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? CustomerCountry 
        {
        	get
        	{
        		return _customercountry;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customercountry,value);
        		_customercountry = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customerpostcode;
        
        public virtual string CustomerPostCode 
        {
        	get
        	{
        		return _customerpostcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customerpostcode,value);
        		_customerpostcode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customerlocality;
        
        public virtual string CustomerLocality 
        {
        	get
        	{
        		return _customerlocality;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customerlocality,value);
        		_customerlocality = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customerstate;
        
        public virtual string CustomerState 
        {
        	get
        	{
        		return _customerstate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customerstate,value);
        		_customerstate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paid;
        
        public virtual bool Paid 
        {
        	get
        	{
        		return _paid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paid,value);
        		_paid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _paidon;
        
        public virtual DateTime? PaidOn 
        {
        	get
        	{
        		return _paidon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paidon,value);
        		_paidon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _cancelled;
        
        public virtual bool Cancelled 
        {
        	get
        	{
        		return _cancelled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cancelled,value);
        		_cancelled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _cancelledon;
        
        public virtual DateTime? CancelledOn 
        {
        	get
        	{
        		return _cancelledon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cancelledon,value);
        		_cancelledon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _remarks;
        
        public virtual string Remarks 
        {
        	get
        	{
        		return _remarks;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_remarks,value);
        		_remarks = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _createdbyadmin;
        
        public virtual bool CreatedByAdmin 
        {
        	get
        	{
        		return _createdbyadmin;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_createdbyadmin,value);
        		_createdbyadmin = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _guid;
        
        public virtual string GUID 
        {
        	get
        	{
        		return _guid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_guid,value);
        		_guid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _itemid;
        
        public virtual int ItemID 
        {
        	get
        	{
        		return _itemid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemid,value);
        		_itemid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _itemtype;
        
        public virtual string ItemType 
        {
        	get
        	{
        		return _itemtype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemtype,value);
        		_itemtype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isshoppingcart;
        
        public virtual bool IsShoppingCart 
        {
        	get
        	{
        		return _isshoppingcart;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isshoppingcart,value);
        		_isshoppingcart = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customerlastname;
        
        public virtual string CustomerLastName 
        {
        	get
        	{
        		return _customerlastname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customerlastname,value);
        		_customerlastname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _shippedon;
        
        public virtual DateTime? ShippedOn 
        {
        	get
        	{
        		return _shippedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shippedon,value);
        		_shippedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _shipmenttrackingcode;
        
        public virtual string ShipmentTrackingCode 
        {
        	get
        	{
        		return _shipmenttrackingcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shipmenttrackingcode,value);
        		_shipmenttrackingcode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _shipmenttype;
        
        public virtual string ShipmentType 
        {
        	get
        	{
        		return _shipmenttype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shipmenttype,value);
        		_shipmenttype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _shipmentremarks;
        
        public virtual string ShipmentRemarks 
        {
        	get
        	{
        		return _shipmentremarks;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shipmentremarks,value);
        		_shipmentremarks = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isconfirmed;
        
        public virtual bool IsConfirmed 
        {
        	get
        	{
        		return _isconfirmed;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isconfirmed,value);
        		_isconfirmed = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _shipmenttrackingurl;
        
        public virtual string ShipmentTrackingUrl 
        {
        	get
        	{
        		return _shipmenttrackingurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shipmenttrackingurl,value);
        		_shipmenttrackingurl = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _fixeddiscount;
        
        public virtual double FixedDiscount 
        {
        	get
        	{
        		return _fixeddiscount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_fixeddiscount,value);
        		_fixeddiscount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.ORDER_STATUS _orderstatus;
        
        public virtual BusinessLogic_v3.Enums.ORDER_STATUS OrderStatus 
        {
        	get
        	{
        		return _orderstatus;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_orderstatus,value);
        		_orderstatus = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double? _totalshippingcost;
        
        public virtual double? TotalShippingCost 
        {
        	get
        	{
        		return _totalshippingcost;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_totalshippingcost,value);
        		_totalshippingcost = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _lastupdatedon;
        
        public virtual DateTime LastUpdatedOn 
        {
        	get
        	{
        		return _lastupdatedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_lastupdatedon,value);
        		_lastupdatedon = value;
        		
        	}
        }
        
/*
		public virtual long? OrderCurrencyID
		{
		 	get 
		 	{
		 		return (this.OrderCurrency != null ? (long?)this.OrderCurrency.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase _ordercurrency;
        public virtual BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase OrderCurrency 
        {
        	get
        	{
        		return _ordercurrency;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ordercurrency,value);
        		_ordercurrency = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase BusinessLogic_v3.Modules._AutoGen.IOrderBaseAutoGen.OrderCurrency 
        {
            get
            {
            	return this.OrderCurrency;
            }
            set
            {
            	this.OrderCurrency = (BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase) value;
            }
        }
            
/*
		public virtual long? ShippingMethodID
		{
		 	get 
		 	{
		 		return (this.ShippingMethod != null ? (long?)this.ShippingMethod.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase _shippingmethod;
        public virtual BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase ShippingMethod 
        {
        	get
        	{
        		return _shippingmethod;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shippingmethod,value);
        		_shippingmethod = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ShippingMethodModule.IShippingMethodBase BusinessLogic_v3.Modules._AutoGen.IOrderBaseAutoGen.ShippingMethod 
        {
            get
            {
            	return this.ShippingMethod;
            }
            set
            {
            	this.ShippingMethod = (BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase) value;
            }
        }
            
/*
		public virtual long? LinkedAffiliateID
		{
		 	get 
		 	{
		 		return (this.LinkedAffiliate != null ? (long?)this.LinkedAffiliate.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase _linkedaffiliate;
        public virtual BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase LinkedAffiliate 
        {
        	get
        	{
        		return _linkedaffiliate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkedaffiliate,value);
        		_linkedaffiliate = value;
        	}
        }
            
        BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase BusinessLogic_v3.Modules._AutoGen.IOrderBaseAutoGen.LinkedAffiliate 
        {
            get
            {
            	return this.LinkedAffiliate;
            }
            set
            {
            	this.LinkedAffiliate = (BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private double _affiliatecommissionrate;
        
        public virtual double AffiliateCommissionRate 
        {
        	get
        	{
        		return _affiliatecommissionrate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_affiliatecommissionrate,value);
        		_affiliatecommissionrate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customermiddlename;
        
        public virtual string CustomerMiddleName 
        {
        	get
        	{
        		return _customermiddlename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customermiddlename,value);
        		_customermiddlename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymammothidentifier;
        
        public virtual string PayMammothIdentifier 
        {
        	get
        	{
        		return _paymammothidentifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymammothidentifier,value);
        		_paymammothidentifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _pendingmanualpayment;
        
        public virtual bool PendingManualPayment 
        {
        	get
        	{
        		return _pendingmanualpayment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_pendingmanualpayment,value);
        		_pendingmanualpayment = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private PayMammoth.Connector.Enums.PaymentMethodSpecific? _paymentmethod;
        
        public virtual PayMammoth.Connector.Enums.PaymentMethodSpecific? PaymentMethod 
        {
        	get
        	{
        		return _paymentmethod;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentmethod,value);
        		_paymentmethod = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _paypippaprocesscompletedon;
        
        public virtual DateTime? PayPippaProcessCompletedOn 
        {
        	get
        	{
        		return _paypippaprocesscompletedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypippaprocesscompletedon,value);
        		_paypippaprocesscompletedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _accountbalancedifference;
        
        public virtual double AccountBalanceDifference 
        {
        	get
        	{
        		return _accountbalancedifference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_accountbalancedifference,value);
        		_accountbalancedifference = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _istestorder;
        
        public virtual bool IsTestOrder 
        {
        	get
        	{
        		return _istestorder;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_istestorder,value);
        		_istestorder = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __MemberSubscriptionLinksCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.OrderModule.OrderBase, 
        	BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase,
        	BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.OrderModule.OrderBase, BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase>
        {
            private BusinessLogic_v3.Modules.OrderModule.OrderBase _item = null;
            public __MemberSubscriptionLinksCollectionInfo(BusinessLogic_v3.Modules.OrderModule.OrderBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase> Collection
            {
                get { return _item.__collection__MemberSubscriptionLinks; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.OrderModule.OrderBase, BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase item, BusinessLogic_v3.Modules.OrderModule.OrderBase value)
            {
                item.Order = value;
            }
        }
        
        private __MemberSubscriptionLinksCollectionInfo _MemberSubscriptionLinks = null;
        public __MemberSubscriptionLinksCollectionInfo MemberSubscriptionLinks
        {
            get
            {
                if (_MemberSubscriptionLinks == null)
                    _MemberSubscriptionLinks = new __MemberSubscriptionLinksCollectionInfo((BusinessLogic_v3.Modules.OrderModule.OrderBase)this);
                return _MemberSubscriptionLinks;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase> IOrderBaseAutoGen.MemberSubscriptionLinks
        {
            get {  return this.MemberSubscriptionLinks; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase> __collection__MemberSubscriptionLinks
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberSubscriptionLinkBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ApplicableSpecialOfferLinksCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.OrderModule.OrderBase, 
        	BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBase,
        	BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.IOrderApplicableSpecialOfferLinkBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.OrderModule.OrderBase, BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBase>
        {
            private BusinessLogic_v3.Modules.OrderModule.OrderBase _item = null;
            public __ApplicableSpecialOfferLinksCollectionInfo(BusinessLogic_v3.Modules.OrderModule.OrderBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBase> Collection
            {
                get { return _item.__collection__ApplicableSpecialOfferLinks; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.OrderModule.OrderBase, BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBase>.SetLinkOnItem(BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBase item, BusinessLogic_v3.Modules.OrderModule.OrderBase value)
            {
                item.Order = value;
            }
        }
        
        private __ApplicableSpecialOfferLinksCollectionInfo _ApplicableSpecialOfferLinks = null;
        public __ApplicableSpecialOfferLinksCollectionInfo ApplicableSpecialOfferLinks
        {
            get
            {
                if (_ApplicableSpecialOfferLinks == null)
                    _ApplicableSpecialOfferLinks = new __ApplicableSpecialOfferLinksCollectionInfo((BusinessLogic_v3.Modules.OrderModule.OrderBase)this);
                return _ApplicableSpecialOfferLinks;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.IOrderApplicableSpecialOfferLinkBase> IOrderBaseAutoGen.ApplicableSpecialOfferLinks
        {
            get {  return this.ApplicableSpecialOfferLinks; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBase> __collection__ApplicableSpecialOfferLinks
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'OrderApplicableSpecialOfferLinkBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __OrderItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.OrderModule.OrderBase, 
        	BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase,
        	BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.OrderModule.OrderBase, BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>
        {
            private BusinessLogic_v3.Modules.OrderModule.OrderBase _item = null;
            public __OrderItemsCollectionInfo(BusinessLogic_v3.Modules.OrderModule.OrderBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase> Collection
            {
                get { return _item.__collection__OrderItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.OrderModule.OrderBase, BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase item, BusinessLogic_v3.Modules.OrderModule.OrderBase value)
            {
                item.Order = value;
            }
        }
        
        private __OrderItemsCollectionInfo _OrderItems = null;
        public __OrderItemsCollectionInfo OrderItems
        {
            get
            {
                if (_OrderItems == null)
                    _OrderItems = new __OrderItemsCollectionInfo((BusinessLogic_v3.Modules.OrderModule.OrderBase)this);
                return _OrderItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase> IOrderBaseAutoGen.OrderItems
        {
            get {  return this.OrderItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase> __collection__OrderItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'OrderItemBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __LinkedShoppingCartItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.OrderModule.OrderBase, 
        	BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase,
        	BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.OrderModule.OrderBase, BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>
        {
            private BusinessLogic_v3.Modules.OrderModule.OrderBase _item = null;
            public __LinkedShoppingCartItemsCollectionInfo(BusinessLogic_v3.Modules.OrderModule.OrderBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase> Collection
            {
                get { return _item.__collection__LinkedShoppingCartItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.OrderModule.OrderBase, BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase item, BusinessLogic_v3.Modules.OrderModule.OrderBase value)
            {
                item.OrderLinkWhenConverted = value;
            }
        }
        
        private __LinkedShoppingCartItemsCollectionInfo _LinkedShoppingCartItems = null;
        public __LinkedShoppingCartItemsCollectionInfo LinkedShoppingCartItems
        {
            get
            {
                if (_LinkedShoppingCartItems == null)
                    _LinkedShoppingCartItems = new __LinkedShoppingCartItemsCollectionInfo((BusinessLogic_v3.Modules.OrderModule.OrderBase)this);
                return _LinkedShoppingCartItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase> IOrderBaseAutoGen.LinkedShoppingCartItems
        {
            get {  return this.LinkedShoppingCartItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase> __collection__LinkedShoppingCartItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ShoppingCartItemBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
