using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CmsUserRoleModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface ICmsUserRoleBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Title { get; set; }
		//Iproperty_normal
        string Identifier { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionManyToManyRightSide  
        
        ICollectionManager<BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase> CmsUsers { get; }
        
        
 
		

    	
    	
      
      

    }
}
