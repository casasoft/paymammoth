using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberReferralModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IMemberReferralBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        DateTime DateTime { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase SentByMember {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase RegisteredMember {get; set; }

		//Iproperty_normal
        string Email { get; set; }
		//Iproperty_normal
        string Name { get; set; }
		//Iproperty_normal
        string Surname { get; set; }
		//Iproperty_normal
        string Message { get; set; }
		//Iproperty_normal
        string Telephone { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.REFERRAL_STATUS_TYPE Status { get; set; }
		//Iproperty_normal
        string ReferredByIP { get; set; }
		//Iproperty_normal
        string ReferralCode { get; set; }
		//Iproperty_normal
        string Remarks { get; set; }
		//Iproperty_normal
        string SentByName { get; set; }
		//Iproperty_normal
        string SentByEmail { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberReferralCommissionModule.IMemberReferralCommissionBase> MemberReferralCommissions { get; }

 
		

    	
    	
      
      

    }
}
