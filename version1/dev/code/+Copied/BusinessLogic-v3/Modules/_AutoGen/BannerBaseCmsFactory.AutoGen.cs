using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.BannerModule;
using BusinessLogic_v3.Cms.BannerModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class BannerBaseCmsFactory_AutoGen : CmsFactoryBase<BannerBaseCmsInfo, BannerBase>
    {
       
       public new static BannerBaseCmsFactory Instance
	    {
	         get
	         {
                 return (BannerBaseCmsFactory)CmsFactoryBase<BannerBaseCmsInfo, BannerBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = BannerBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Banner.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Banner";

            this.QueryStringParamID = "BannerId";

            cmsInfo.TitlePlural = "Banners";

            cmsInfo.TitleSingular =  "Banner";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public BannerBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Banner/";
			UsedInProject = BannerBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
