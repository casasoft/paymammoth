using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ArticleModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IArticleBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Identifier { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.CMS_ACCESS_TYPE AllowUpdate_AccessRequired { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.CMS_ACCESS_TYPE AllowDelete_AccessRequired { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.CMS_ACCESS_TYPE AllowAddChildren_AccessRequired { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.CMS_ACCESS_TYPE AllowAddSubChildren_AccessRequired { get; set; }
		//Iproperty_normal
        string CustomLink { get; set; }
		//IProperty_Multilingual
	   string MetaKeywords { get; set ;}
		//IProperty_Multilingual
	   string MetaKeywords_Search { get; set ;}
		//IProperty_Multilingual
	   string MetaDescription { get; set ;}
		//IProperty_Multilingual
	   string MetaDescription_Search { get; set ;}
		//Iproperty_normal
        string Color { get; set; }
		//Iproperty_normal
        string Name { get; set; }
		//IProperty_Multilingual
	   string PageTitle { get; set ;}
		//IProperty_Multilingual
	   string PageTitle_Search { get; set ;}
		//Iproperty_normal
        bool ExcludeFromRewriteUrl { get; set; }
		//Iproperty_normal
        string ImageFilename { get; set; }
		//Iproperty_normal
        bool ShowAdverts { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.CMS_ACCESS_TYPE VisibleInCMS_AccessRequired { get; set; }
		//Iproperty_normal
        bool? DoNotShowLastEditedOn { get; set; }
		//Iproperty_normal
        bool IsFeatured { get; set; }
		//Iproperty_normal
        string MetaTitle { get; set; }
		//Iproperty_normal
        bool NoFollow { get; set; }
		//Iproperty_normal
        bool NoIndex { get; set; }
		//Iproperty_normal
        bool UsedInProject { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.ARTICLE_TYPE ArticleType { get; set; }
		//Iproperty_normal
        bool NotVisibleInFrontend { get; set; }
		//Iproperty_normal
        bool VisibleInFrontendOnlyWhenLoggedIn { get; set; }
		//Iproperty_normal
        bool NotVisibleInFrontendWhenLoggedIn { get; set; }
		//Iproperty_normal
        string CustomContentTags { get; set; }
		//Iproperty_normal
        bool? DoNotRedirectOnLogin { get; set; }
		//Iproperty_normal
        bool? DoNotShowAddThis { get; set; }
		//Iproperty_normal
        bool? DoNotShowFooter { get; set; }
		//IProperty_Multilingual
	   int? DialogWidth { get; set ;}
		//IProperty_Multilingual
	   int? DialogHeight { get; set ;}
		//Iproperty_normal
        bool DialogPage { get; set; }
		//Iproperty_normal
        double RatingCount { get; set; }
		//Iproperty_normal
        double AvgRating { get; set; }
		//Iproperty_normal
        bool? IsCommentable { get; set; }
		//Iproperty_normal
        string Summary { get; set; }
		//Iproperty_normal
        bool OmitChildrenInNavigation { get; set; }
		//Iproperty_normal
        bool ConsiderAsRootNode { get; set; }
		//Iproperty_normal
        bool? DoNotShowNavigationBreadcrumbs { get; set; }
		//Iproperty_normal
        DateTime CreatedOnDate { get; set; }
		//Iproperty_normal
        string PhysicalFilePath { get; set; }
		//Iproperty_normal
        bool ProcessContentUsingNVelocity { get; set; }
		//Iproperty_normal
        string DialogCssClass { get; set; }
		//Iproperty_normal
        bool? DialogModal { get; set; }
		//Iproperty_normal
        bool? DialogCloseable { get; set; }
		//Iproperty_normal
        bool? DoNotRedirectOnLogout { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.RoutingInfoModule.IRoutingInfoBase LinkedRoute {get; set; }

		//IProperty_Multilingual
	   string SubTitle { get; set ;}
		//Iproperty_normal
        CS.General_v3.Enums.HREF_TARGET? HrefTarget { get; set; }
		//Iproperty_normal
        bool? IsInheritedCommentable_Computed { get; set; }
		//Iproperty_normal
        string AuthorName { get; set; }
		//Iproperty_normal
        bool DoNotShowInSearchResults { get; set; }
		//Iproperty_normal
        bool DoNotShowInSearchResults_Computed { get; set; }
		//IProperty_Multilingual
	   string _ComputedURL { get; set ;}
		//IProperty_Multilingual
	   string _Computed_ParentArticleNames { get; set ;}
		//Iproperty_normal
        bool _Computed_IsLeafNode { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase> Affiliates { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBase> Article_CultureInfos { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBase> ChildArticleLinks { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBase> ParentArticleLinks { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBase> RelatedPages { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBase> ParentsOfWhichThisIsRelated { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase> ArticleComments { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBase> ArticleMediaItems { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBase> ArticleRatings { get; }

        //IBaseClass_CollectionManyToManyRightSide  
        
        ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> ReadBy { get; }
        
        
 
		

    	
    	
      
      

    }
}
