using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.MemberReferralModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "MemberReferral")]
    public abstract class MemberReferralBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IMemberReferralBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static MemberReferralBaseFactory Factory
        {
            get
            {
                return MemberReferralBaseFactory.Instance; 
            }
        }    
        /*
        public static MemberReferralBase CreateNewItem()
        {
            return (MemberReferralBase)Factory.CreateNewItem();
        }

		public static MemberReferralBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MemberReferralBase, MemberReferralBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MemberReferralBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MemberReferralBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MemberReferralBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MemberReferralBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MemberReferralBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private DateTime _datetime;
        
        public virtual DateTime DateTime 
        {
        	get
        	{
        		return _datetime;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datetime,value);
        		_datetime = value;
        		
        	}
        }
        
/*
		public virtual long? SentByMemberID
		{
		 	get 
		 	{
		 		return (this.SentByMember != null ? (long?)this.SentByMember.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _sentbymember;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase SentByMember 
        {
        	get
        	{
        		return _sentbymember;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_sentbymember,value);
        		_sentbymember = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IMemberReferralBaseAutoGen.SentByMember 
        {
            get
            {
            	return this.SentByMember;
            }
            set
            {
            	this.SentByMember = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
/*
		public virtual long? RegisteredMemberID
		{
		 	get 
		 	{
		 		return (this.RegisteredMember != null ? (long?)this.RegisteredMember.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _registeredmember;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase RegisteredMember 
        {
        	get
        	{
        		return _registeredmember;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_registeredmember,value);
        		_registeredmember = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IMemberReferralBaseAutoGen.RegisteredMember 
        {
            get
            {
            	return this.RegisteredMember;
            }
            set
            {
            	this.RegisteredMember = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _email;
        
        public virtual string Email 
        {
        	get
        	{
        		return _email;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_email,value);
        		_email = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _name;
        
        public virtual string Name 
        {
        	get
        	{
        		return _name;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_name,value);
        		_name = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _surname;
        
        public virtual string Surname 
        {
        	get
        	{
        		return _surname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_surname,value);
        		_surname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _message;
        
        public virtual string Message 
        {
        	get
        	{
        		return _message;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_message,value);
        		_message = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _telephone;
        
        public virtual string Telephone 
        {
        	get
        	{
        		return _telephone;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_telephone,value);
        		_telephone = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.REFERRAL_STATUS_TYPE _status;
        
        public virtual BusinessLogic_v3.Enums.REFERRAL_STATUS_TYPE Status 
        {
        	get
        	{
        		return _status;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_status,value);
        		_status = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _referredbyip;
        
        public virtual string ReferredByIP 
        {
        	get
        	{
        		return _referredbyip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_referredbyip,value);
        		_referredbyip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _referralcode;
        
        public virtual string ReferralCode 
        {
        	get
        	{
        		return _referralcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_referralcode,value);
        		_referralcode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _remarks;
        
        public virtual string Remarks 
        {
        	get
        	{
        		return _remarks;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_remarks,value);
        		_remarks = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _sentbyname;
        
        public virtual string SentByName 
        {
        	get
        	{
        		return _sentbyname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_sentbyname,value);
        		_sentbyname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _sentbyemail;
        
        public virtual string SentByEmail 
        {
        	get
        	{
        		return _sentbyemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_sentbyemail,value);
        		_sentbyemail = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __MemberReferralCommissionsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase, 
        	BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase,
        	BusinessLogic_v3.Modules.MemberReferralCommissionModule.IMemberReferralCommissionBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase, BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>
        {
            private BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase _item = null;
            public __MemberReferralCommissionsCollectionInfo(BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase> Collection
            {
                get { return _item.__collection__MemberReferralCommissions; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase, BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase item, BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase value)
            {
                item.MemberReferral = value;
            }
        }
        
        private __MemberReferralCommissionsCollectionInfo _MemberReferralCommissions = null;
        public __MemberReferralCommissionsCollectionInfo MemberReferralCommissions
        {
            get
            {
                if (_MemberReferralCommissions == null)
                    _MemberReferralCommissions = new __MemberReferralCommissionsCollectionInfo((BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase)this);
                return _MemberReferralCommissions;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberReferralCommissionModule.IMemberReferralCommissionBase> IMemberReferralBaseAutoGen.MemberReferralCommissions
        {
            get {  return this.MemberReferralCommissions; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase> __collection__MemberReferralCommissions
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberReferralCommissionBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
