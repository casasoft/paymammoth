using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.BannerModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IBannerBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Title { get; set; }
		//Iproperty_normal
        string MediaItemFilename { get; set; }
		//Iproperty_normal
        string Link { get; set; }
		//Iproperty_normal
        int DurationMS { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.HREF_TARGET HrefTarget { get; set; }
		//Iproperty_normal
        double SlideshowDelaySec { get; set; }
		//Iproperty_normal
        string HTMLDescription { get; set; }
		//Iproperty_normal
        string Identifier { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionManyToManyLeftSide  
        ICollectionManager<BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase> Cultures { get; }

 
		

    	
    	
      
      

    }
}
