using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using BusinessLogic_v3.Cms.Article_ParentChildLinkModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class Article_ParentChildLinkBaseCmsFactory_AutoGen : CmsFactoryBase<Article_ParentChildLinkBaseCmsInfo, Article_ParentChildLinkBase>
    {
       
       public new static Article_ParentChildLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Article_ParentChildLinkBaseCmsFactory)CmsFactoryBase<Article_ParentChildLinkBaseCmsInfo, Article_ParentChildLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = Article_ParentChildLinkBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Article_ParentChildLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Article_ParentChildLink";

            this.QueryStringParamID = "Article_ParentChildLinkId";

            cmsInfo.TitlePlural = "Article _ Parent Child Links";

            cmsInfo.TitleSingular =  "Article _ Parent Child Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Article_ParentChildLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Article_ParentChildLink/";
			UsedInProject = Article_ParentChildLinkBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
