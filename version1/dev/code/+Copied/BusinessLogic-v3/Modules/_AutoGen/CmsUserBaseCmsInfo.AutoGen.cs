using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Cms.CmsUserModule;
using BusinessLogic_v3.Frontend.CmsUserModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CmsUserBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase>
    {
		public CmsUserBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase dbItem)
            : base(BusinessLogic_v3.Cms.CmsUserModule.CmsUserBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CmsUserBaseFrontend FrontendItem
        {
            get { return (CmsUserBaseFrontend)base.FrontendItem; }

        }
        public new CmsUserBase DbItem
        {
            get
            {
                return (CmsUserBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Name { get; protected set; }

        public CmsPropertyInfo Surname { get; protected set; }

        public CmsPropertyInfo Username { get; protected set; }

        public CmsPropertyInfo Password { get; protected set; }

        public CmsPropertyInfo LastLoggedIn { get; protected set; }

        public CmsPropertyInfo _LastLoggedIn_New { get; protected set; }

        public CmsPropertyInfo AccessType { get; protected set; }

        public CmsPropertyInfo SessionGUID { get; protected set; }

        public CmsPropertyInfo PasswordSalt { get; protected set; }

        public CmsPropertyInfo PasswordEncryptionType { get; protected set; }

        public CmsPropertyInfo PasswordIterations { get; protected set; }

        public CmsPropertyInfo HiddenFromNonCasaSoft { get; protected set; }

        public CmsPropertyInfo CmsCustomLogoFilename { get; protected set; }

        public CmsPropertyInfo CmsCustomLogoLinkUrl { get; protected set; }

        public CmsPropertyInfo Disabled { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo Affiliates { get; protected set; }
        

        public CmsCollectionInfo CmsAuditLogs { get; protected set; }
        

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsInfo> CmsUserRoles { get; protected set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.Affiliates = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase>.GetPropertyBySelector(item => item.Affiliates),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector(item => item.LinkedCmsUser)));
		
		//InitCollectionBaseOneToMany
		this.CmsAuditLogs = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase>.GetPropertyBySelector(item => item.CmsAuditLogs),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>.GetPropertyBySelector(item => item.CmsUser)));
//InitFieldManyToManyBase_RightSide
		this.CmsUserRoles = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsInfo>(
								item => item.CmsUserRoles, 
								Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
