using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ImageSpecificSizeInfo")]
    public abstract class ImageSpecificSizeInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IImageSpecificSizeInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ImageSpecificSizeInfoBaseFactory Factory
        {
            get
            {
                return ImageSpecificSizeInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static ImageSpecificSizeInfoBase CreateNewItem()
        {
            return (ImageSpecificSizeInfoBase)Factory.CreateNewItem();
        }

		public static ImageSpecificSizeInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ImageSpecificSizeInfoBase, ImageSpecificSizeInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ImageSpecificSizeInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ImageSpecificSizeInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ImageSpecificSizeInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ImageSpecificSizeInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ImageSpecificSizeInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? ImageSizingInfoID
		{
		 	get 
		 	{
		 		return (this.ImageSizingInfo != null ? (long?)this.ImageSizingInfo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase _imagesizinginfo;
        public virtual BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase ImageSizingInfo 
        {
        	get
        	{
        		return _imagesizinginfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagesizinginfo,value);
        		_imagesizinginfo = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBase BusinessLogic_v3.Modules._AutoGen.IImageSpecificSizeInfoBaseAutoGen.ImageSizingInfo 
        {
            get
            {
            	return this.ImageSizingInfo;
            }
            set
            {
            	this.ImageSizingInfo = (BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private int _width;
        
        public virtual int Width 
        {
        	get
        	{
        		return _width;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_width,value);
        		_width = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _height;
        
        public virtual int Height 
        {
        	get
        	{
        		return _height;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_height,value);
        		_height = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.CROP_IDENTIFIER _cropidentifier;
        
        public virtual CS.General_v3.Enums.CROP_IDENTIFIER CropIdentifier 
        {
        	get
        	{
        		return _cropidentifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cropidentifier,value);
        		_cropidentifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.IMAGE_FORMAT? _imageformat;
        
        public virtual CS.General_v3.Enums.IMAGE_FORMAT? ImageFormat 
        {
        	get
        	{
        		return _imageformat;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imageformat,value);
        		_imageformat = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
