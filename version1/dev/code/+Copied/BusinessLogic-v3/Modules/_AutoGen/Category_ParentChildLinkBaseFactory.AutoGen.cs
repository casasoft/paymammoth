using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;

//DbObjectBuilder v8

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class Category_ParentChildLinkBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<Category_ParentChildLinkBase, ICategory_ParentChildLinkBase>, ICategory_ParentChildLinkBaseFactoryAutoGen
    
    {
    
        public Category_ParentChildLinkBaseFactoryAutoGen()
        {
        	
            
        }
        
		static Category_ParentChildLinkBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static Category_ParentChildLinkBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<Category_ParentChildLinkBaseFactory>(null);
            }
        }
        
		public IQueryOver<Category_ParentChildLinkBase, Category_ParentChildLinkBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Category_ParentChildLinkBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
