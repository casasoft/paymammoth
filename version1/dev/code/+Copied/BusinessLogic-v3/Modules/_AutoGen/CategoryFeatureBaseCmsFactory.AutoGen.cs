using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CategoryFeatureModule;
using BusinessLogic_v3.Cms.CategoryFeatureModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class CategoryFeatureBaseCmsFactory_AutoGen : CmsFactoryBase<CategoryFeatureBaseCmsInfo, CategoryFeatureBase>
    {
       
       public new static CategoryFeatureBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CategoryFeatureBaseCmsFactory)CmsFactoryBase<CategoryFeatureBaseCmsInfo, CategoryFeatureBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = CategoryFeatureBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CategoryFeature.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CategoryFeature";

            this.QueryStringParamID = "CategoryFeatureId";

            cmsInfo.TitlePlural = "Category Features";

            cmsInfo.TitleSingular =  "Category Feature";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CategoryFeatureBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "CategoryFeature/";
			UsedInProject = CategoryFeatureBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
