using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.AdvertModule;
using BusinessLogic_v3.Cms.AdvertModule;
using BusinessLogic_v3.Frontend.AdvertModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AdvertBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>
    {
		public AdvertBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AdvertModule.AdvertBase dbItem)
            : base(BusinessLogic_v3.Cms.AdvertModule.AdvertBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AdvertBaseFrontend FrontendItem
        {
            get { return (AdvertBaseFrontend)base.FrontendItem; }

        }
        public new AdvertBase DbItem
        {
            get
            {
                return (AdvertBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo AdvertSlot { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo Filename { get; protected set; }

        public CmsPropertyInfo AdvertShownPerRound { get; protected set; }

        public CmsPropertyInfo RoundCounter { get; protected set; }

        public CmsPropertyInfo VideoDuration { get; protected set; }

        public CmsPropertyInfo Link { get; protected set; }

        public CmsPropertyInfo ShowDateFrom { get; protected set; }

        public CmsPropertyInfo ShowDateTo { get; protected set; }

        public CmsPropertyInfo HrefTarget { get; protected set; }

        public CmsPropertyInfo BackgroundColor { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo AdvertHits { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.AdvertHits = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector(item => item.AdvertHits),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase>.GetPropertyBySelector(item => item.Advert)));


			base.initBasicFields();
          
        }

    }
}
