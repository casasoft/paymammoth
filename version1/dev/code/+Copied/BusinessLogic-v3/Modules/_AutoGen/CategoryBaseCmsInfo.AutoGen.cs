using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Cms.CategoryModule;
using BusinessLogic_v3.Frontend.CategoryModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CategoryBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>
    {
		public CategoryBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CategoryModule.CategoryBase dbItem)
            : base(BusinessLogic_v3.Cms.CategoryModule.CategoryBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CategoryBaseFrontend FrontendItem
        {
            get { return (CategoryBaseFrontend)base.FrontendItem; }

        }
        public new CategoryBase DbItem
        {
            get
            {
                return (CategoryBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo TitleSingular { get; protected set; }

        public CmsPropertyInfo TitleSingular_Search { get; protected set; }

        public CmsPropertyInfo TitlePlural { get; protected set; }

        public CmsPropertyInfo TitlePlural_Search { get; protected set; }

        public CmsPropertyInfo TitleSEO { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo MetaKeywords { get; protected set; }

        public CmsPropertyInfo MetaDescription { get; protected set; }

        public CmsPropertyInfo CanDelete { get; protected set; }

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo ComingSoon { get; protected set; }

        public CmsPropertyInfo PriorityAll { get; protected set; }

        public CmsPropertyInfo Brand { get; protected set; }

        public CmsPropertyInfo ImportReference { get; protected set; }

        public CmsPropertyInfo DontShowInNavigation { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo Category_CultureInfos { get; protected set; }
        

        public CmsCollectionInfo ChildCategoryLinks { get; protected set; }
        

        public CmsCollectionInfo ParentCategoryLinks { get; protected set; }
        

        public CmsCollectionInfo CategoryFeatures { get; protected set; }
        

        public CmsCollectionInfo CategoryImages { get; protected set; }
        

        public CmsCollectionInfo CategorySpecifications { get; protected set; }
        

        public CmsCollectionInfo Events { get; protected set; }
        

        public CmsCollectionInfo ProductLinks { get; protected set; }
        

        public CmsCollectionInfo SpecialOffers { get; protected set; }
        

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.MemberModule.MemberBaseCmsInfo> SubscribedMembers { get; protected set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.Category_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.Category_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.ChildCategoryLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.ChildCategoryLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>.GetPropertyBySelector(item => item.ParentCategory)));
		
		//InitCollectionBaseOneToMany
		this.ParentCategoryLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.ParentCategoryLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>.GetPropertyBySelector(item => item.ChildCategory)));
		
		//InitCollectionBaseOneToMany
		this.CategoryFeatures = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.CategoryFeatures),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.CategoryImages = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.CategoryImages),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.CategorySpecifications = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.CategorySpecifications),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.Events = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.Events),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.ProductLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.ProductLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase>.GetPropertyBySelector(item => item.Category)));
		
		//InitCollectionBaseOneToMany
		this.SpecialOffers = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.SpecialOffers),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector(item => item.Category)));
//InitFieldManyToManyBase_RightSide
		this.SubscribedMembers = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.MemberModule.MemberBaseCmsInfo>(
								item => item.SubscribedMembers, 
								Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
