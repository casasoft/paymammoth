using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ArticleMediaItemModule;

//DbObjectBuilder v8

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class ArticleMediaItemBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<ArticleMediaItemBase, IArticleMediaItemBase>, IArticleMediaItemBaseFactoryAutoGen
    
    {
    
        public ArticleMediaItemBaseFactoryAutoGen()
        {
        	
            
        }
        
		static ArticleMediaItemBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static ArticleMediaItemBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<ArticleMediaItemBaseFactory>(null);
            }
        }
        
		public IQueryOver<ArticleMediaItemBase, ArticleMediaItemBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ArticleMediaItemBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
