using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductRelatedProductLinkModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ProductRelatedProductLink")]
    public abstract class ProductRelatedProductLinkBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductRelatedProductLinkBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ProductRelatedProductLinkBaseFactory Factory
        {
            get
            {
                return ProductRelatedProductLinkBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductRelatedProductLinkBase CreateNewItem()
        {
            return (ProductRelatedProductLinkBase)Factory.CreateNewItem();
        }

		public static ProductRelatedProductLinkBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductRelatedProductLinkBase, ProductRelatedProductLinkBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductRelatedProductLinkBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductRelatedProductLinkBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductRelatedProductLinkBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductRelatedProductLinkBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductRelatedProductLinkBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? ProductID
		{
		 	get 
		 	{
		 		return (this.Product != null ? (long?)this.Product.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _product;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase Product 
        {
        	get
        	{
        		return _product;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_product,value);
        		_product = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.IProductRelatedProductLinkBaseAutoGen.Product 
        {
            get
            {
            	return this.Product;
            }
            set
            {
            	this.Product = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
/*
		public virtual long? RelatedProductID
		{
		 	get 
		 	{
		 		return (this.RelatedProduct != null ? (long?)this.RelatedProduct.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _relatedproduct;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase RelatedProduct 
        {
        	get
        	{
        		return _relatedproduct;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_relatedproduct,value);
        		_relatedproduct = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.IProductRelatedProductLinkBaseAutoGen.RelatedProduct 
        {
            get
            {
            	return this.RelatedProduct;
            }
            set
            {
            	this.RelatedProduct = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
