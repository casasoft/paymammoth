using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductCategoryModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductCategoryModule.ProductCategoryBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductCategoryModule.ProductCategoryBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductCategoryModule.ProductCategoryBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductCategoryBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductCategoryBaseFrontend, BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBase>

    {
		
        
        protected ProductCategoryBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
