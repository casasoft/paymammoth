using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Modules.SettingModule;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;

namespace BusinessLogic_v3.Modules._AutoGen
{
    
    public abstract class SettingBaseMap_AutoGen<TData> : BusinessLogic_v3.Classes.NHibernateClasses.Mappings.BaseClassMap<TData>
    	where TData : SettingBase
    {
        public SettingBaseMap_AutoGen()
        {
            
        }
        
        


// [baseclassmap_properties]

//BaseClassMap_Property
             
        protected virtual void NameMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void ValueMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void VisibleInCMS_AccessRequiredMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void DataTypeMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_PropertyLinkedObject

        protected virtual void ParentMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            mapInfo.CollectionType = BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "ParentId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        
//BaseClassMap_Property
             
        protected virtual void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void DescriptionMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void UsedInProjectMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void LocalhostValueMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void EnumTypeMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        



// [baseclassmap_collections]

//BaseClassMap_Collection_OneToMany

        protected virtual void ChildSettingsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "ParentID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
            
        
        protected override void initMappings()
        {
           
            
//[BaseClassMap_InitProperties]

//[BaseClassMap_InitCollections]                
            
            base.initMappings();
        }
        
        
        
        
        
    }
   
}
