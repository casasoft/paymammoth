using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CmsAuditLogModule;
using BusinessLogic_v3.Cms.CmsAuditLogModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class CmsAuditLogBaseCmsFactory_AutoGen : CmsFactoryBase<CmsAuditLogBaseCmsInfo, CmsAuditLogBase>
    {
       
       public new static CmsAuditLogBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CmsAuditLogBaseCmsFactory)CmsFactoryBase<CmsAuditLogBaseCmsInfo, CmsAuditLogBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = CmsAuditLogBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CmsAuditLog.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CmsAuditLog";

            this.QueryStringParamID = "CmsAuditLogId";

            cmsInfo.TitlePlural = "Cms Audit Logs";

            cmsInfo.TitleSingular =  "Cms Audit Log";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CmsAuditLogBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "CmsAuditLog/";
			UsedInProject = CmsAuditLogBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
