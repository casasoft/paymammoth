using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.AdvertModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IAdvertBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.AdvertSlotModule.IAdvertSlotBase AdvertSlot {get; set; }

		//Iproperty_normal
        string Title { get; set; }
		//Iproperty_normal
        string Description { get; set; }
		//Iproperty_normal
        string Filename { get; set; }
		//Iproperty_normal
        int AdvertShownPerRound { get; set; }
		//Iproperty_normal
        int RoundCounter { get; set; }
		//Iproperty_normal
        int VideoDuration { get; set; }
		//Iproperty_normal
        string Link { get; set; }
		//Iproperty_normal
        DateTime? ShowDateFrom { get; set; }
		//Iproperty_normal
        DateTime? ShowDateTo { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.HREF_TARGET HrefTarget { get; set; }
		//Iproperty_normal
        string BackgroundColor { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.AdvertHitModule.IAdvertHitBase> AdvertHits { get; }

 
		

    	
    	
      
      

    }
}
