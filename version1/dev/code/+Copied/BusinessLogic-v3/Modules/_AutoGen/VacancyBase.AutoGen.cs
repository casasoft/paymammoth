using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.VacancyModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Vacancy")]
    public abstract class VacancyBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IVacancyBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static VacancyBaseFactory Factory
        {
            get
            {
                return VacancyBaseFactory.Instance; 
            }
        }    
        /*
        public static VacancyBase CreateNewItem()
        {
            return (VacancyBase)Factory.CreateNewItem();
        }

		public static VacancyBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<VacancyBase, VacancyBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static VacancyBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static VacancyBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<VacancyBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<VacancyBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<VacancyBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _refcode;
        
        public virtual string RefCode 
        {
        	get
        	{
        		return _refcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_refcode,value);
        		_refcode = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ContactFormsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.VacancyModule.VacancyBase, 
        	BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase,
        	BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.VacancyModule.VacancyBase, BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>
        {
            private BusinessLogic_v3.Modules.VacancyModule.VacancyBase _item = null;
            public __ContactFormsCollectionInfo(BusinessLogic_v3.Modules.VacancyModule.VacancyBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase> Collection
            {
                get { return _item.__collection__ContactForms; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.VacancyModule.VacancyBase, BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase item, BusinessLogic_v3.Modules.VacancyModule.VacancyBase value)
            {
                item.Vacancy = value;
            }
        }
        
        private __ContactFormsCollectionInfo _ContactForms = null;
        public __ContactFormsCollectionInfo ContactForms
        {
            get
            {
                if (_ContactForms == null)
                    _ContactForms = new __ContactFormsCollectionInfo((BusinessLogic_v3.Modules.VacancyModule.VacancyBase)this);
                return _ContactForms;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase> IVacancyBaseAutoGen.ContactForms
        {
            get {  return this.ContactForms; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase> __collection__ContactForms
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ContactFormBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
