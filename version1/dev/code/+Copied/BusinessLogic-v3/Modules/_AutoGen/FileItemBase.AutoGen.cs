using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.FileItemModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "FileItem")]
    public abstract class FileItemBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IFileItemBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static FileItemBaseFactory Factory
        {
            get
            {
                return FileItemBaseFactory.Instance; 
            }
        }    
        /*
        public static FileItemBase CreateNewItem()
        {
            return (FileItemBase)Factory.CreateNewItem();
        }

		public static FileItemBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<FileItemBase, FileItemBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static FileItemBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static FileItemBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<FileItemBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<FileItemBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<FileItemBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _uploadedon;
        
        public virtual DateTime UploadedOn 
        {
        	get
        	{
        		return _uploadedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_uploadedon,value);
        		_uploadedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _uploadedby;
        
        public virtual string UploadedBy 
        {
        	get
        	{
        		return _uploadedby;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_uploadedby,value);
        		_uploadedby = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _uploadfilename;
        
        public virtual string UploadFilename 
        {
        	get
        	{
        		return _uploadfilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_uploadfilename,value);
        		_uploadfilename = value;
        		
        	}
        }
        
/*
		public virtual long? EventID
		{
		 	get 
		 	{
		 		return (this.Event != null ? (long?)this.Event.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.EventModule.EventBase _event;
        public virtual BusinessLogic_v3.Modules.EventModule.EventBase Event 
        {
        	get
        	{
        		return _event;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_event,value);
        		_event = value;
        	}
        }
            
        BusinessLogic_v3.Modules.EventModule.IEventBase BusinessLogic_v3.Modules._AutoGen.IFileItemBaseAutoGen.Event 
        {
            get
            {
            	return this.Event;
            }
            set
            {
            	this.Event = (BusinessLogic_v3.Modules.EventModule.EventBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
