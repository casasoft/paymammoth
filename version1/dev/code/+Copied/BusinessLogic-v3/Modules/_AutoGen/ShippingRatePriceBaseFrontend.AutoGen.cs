using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ShippingRatePriceModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ShippingRatePriceModule.ShippingRatePriceBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ShippingRatePriceModule.IShippingRatePriceBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ShippingRatePriceModule.ShippingRatePriceBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ShippingRatePriceModule.IShippingRatePriceBase item)
        {
        	return BusinessLogic_v3.Frontend.ShippingRatePriceModule.ShippingRatePriceBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ShippingRatePriceBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ShippingRatePriceBaseFrontend, BusinessLogic_v3.Modules.ShippingRatePriceModule.IShippingRatePriceBase>

    {
		
        
        protected ShippingRatePriceBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
