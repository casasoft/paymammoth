using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule;

//DbObjectBuilder v8

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class SpecialOffer_CultureInfoBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<SpecialOffer_CultureInfoBase, ISpecialOffer_CultureInfoBase>, ISpecialOffer_CultureInfoBaseFactoryAutoGen
    
    {
    
        public SpecialOffer_CultureInfoBaseFactoryAutoGen()
        {
        	
            
        }
        
		static SpecialOffer_CultureInfoBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static SpecialOffer_CultureInfoBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<SpecialOffer_CultureInfoBaseFactory>(null);
            }
        }
        
		public IQueryOver<SpecialOffer_CultureInfoBase, SpecialOffer_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<SpecialOffer_CultureInfoBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
