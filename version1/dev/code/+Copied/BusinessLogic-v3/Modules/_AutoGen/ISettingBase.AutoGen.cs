using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.SettingModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface ISettingBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Name { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.CMS_ACCESS_TYPE VisibleInCMS_AccessRequired { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.DATA_TYPE DataType { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.SettingModule.ISettingBase Parent {get; set; }

		//Iproperty_normal
        string Identifier { get; set; }
		//Iproperty_normal
        string Description { get; set; }
		//Iproperty_normal
        bool UsedInProject { get; set; }
		//Iproperty_normal
        string EnumType { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.SettingModule.ISettingBase> ChildSettings { get; }

 
		

    	
    	
      
      

    }
}
