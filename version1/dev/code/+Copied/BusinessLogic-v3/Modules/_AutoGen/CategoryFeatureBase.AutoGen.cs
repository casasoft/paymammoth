using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.CategoryFeatureModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "CategoryFeature")]
    public abstract class CategoryFeatureBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ICategoryFeatureBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static CategoryFeatureBaseFactory Factory
        {
            get
            {
                return CategoryFeatureBaseFactory.Instance; 
            }
        }    
        /*
        public static CategoryFeatureBase CreateNewItem()
        {
            return (CategoryFeatureBase)Factory.CreateNewItem();
        }

		public static CategoryFeatureBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<CategoryFeatureBase, CategoryFeatureBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static CategoryFeatureBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static CategoryFeatureBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<CategoryFeatureBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<CategoryFeatureBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<CategoryFeatureBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _iconfilename;
        
        public virtual string IconFilename 
        {
        	get
        	{
        		return _iconfilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_iconfilename,value);
        		_iconfilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _link;
        
        public virtual string Link 
        {
        	get
        	{
        		return _link;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_link,value);
        		_link = value;
        		
        	}
        }
        
/*
		public virtual long? CategoryID
		{
		 	get 
		 	{
		 		return (this.Category != null ? (long?)this.Category.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _category;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase Category 
        {
        	get
        	{
        		return _category;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_category,value);
        		_category = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.ICategoryFeatureBaseAutoGen.Category 
        {
            get
            {
            	return this.Category;
            }
            set
            {
            	this.Category = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
/*
		public virtual long? ItemGroupID
		{
		 	get 
		 	{
		 		return (this.ItemGroup != null ? (long?)this.ItemGroup.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _itemgroup;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase ItemGroup 
        {
        	get
        	{
        		return _itemgroup;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemgroup,value);
        		_itemgroup = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.ICategoryFeatureBaseAutoGen.ItemGroup 
        {
            get
            {
            	return this.ItemGroup;
            }
            set
            {
            	this.ItemGroup = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ProductCategoryFeatureValuesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase, 
        	BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase,
        	BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase, BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase>
        {
            private BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase _item = null;
            public __ProductCategoryFeatureValuesCollectionInfo(BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase> Collection
            {
                get { return _item.__collection__ProductCategoryFeatureValues; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase, BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase item, BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase value)
            {
                item.CategoryFeature = value;
            }
        }
        
        private __ProductCategoryFeatureValuesCollectionInfo _ProductCategoryFeatureValues = null;
        public __ProductCategoryFeatureValuesCollectionInfo ProductCategoryFeatureValues
        {
            get
            {
                if (_ProductCategoryFeatureValues == null)
                    _ProductCategoryFeatureValues = new __ProductCategoryFeatureValuesCollectionInfo((BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase)this);
                return _ProductCategoryFeatureValues;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBase> ICategoryFeatureBaseAutoGen.ProductCategoryFeatureValues
        {
            get {  return this.ProductCategoryFeatureValues; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase> __collection__ProductCategoryFeatureValues
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductCategoryFeatureValueBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
