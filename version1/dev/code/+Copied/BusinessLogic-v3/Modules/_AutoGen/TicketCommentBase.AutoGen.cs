using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.TicketCommentModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "TicketComment")]
    public abstract class TicketCommentBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ITicketCommentBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static TicketCommentBaseFactory Factory
        {
            get
            {
                return TicketCommentBaseFactory.Instance; 
            }
        }    
        /*
        public static TicketCommentBase CreateNewItem()
        {
            return (TicketCommentBase)Factory.CreateNewItem();
        }

		public static TicketCommentBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<TicketCommentBase, TicketCommentBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static TicketCommentBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static TicketCommentBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<TicketCommentBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<TicketCommentBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<TicketCommentBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? TicketID
		{
		 	get 
		 	{
		 		return (this.Ticket != null ? (long?)this.Ticket.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.TicketModule.TicketBase _ticket;
        public virtual BusinessLogic_v3.Modules.TicketModule.TicketBase Ticket 
        {
        	get
        	{
        		return _ticket;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ticket,value);
        		_ticket = value;
        	}
        }
            
        BusinessLogic_v3.Modules.TicketModule.ITicketBase BusinessLogic_v3.Modules._AutoGen.ITicketCommentBaseAutoGen.Ticket 
        {
            get
            {
            	return this.Ticket;
            }
            set
            {
            	this.Ticket = (BusinessLogic_v3.Modules.TicketModule.TicketBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _comment;
        
        public virtual string Comment 
        {
        	get
        	{
        		return _comment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_comment,value);
        		_comment = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _ipaddress;
        
        public virtual string IPAddress 
        {
        	get
        	{
        		return _ipaddress;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ipaddress,value);
        		_ipaddress = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _postedon;
        
        public virtual DateTime PostedOn 
        {
        	get
        	{
        		return _postedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_postedon,value);
        		_postedon = value;
        		
        	}
        }
        
/*
		public virtual long? AssignedStaffUserID
		{
		 	get 
		 	{
		 		return (this.AssignedStaffUser != null ? (long?)this.AssignedStaffUser.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _assignedstaffuser;
        public virtual BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase AssignedStaffUser 
        {
        	get
        	{
        		return _assignedstaffuser;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_assignedstaffuser,value);
        		_assignedstaffuser = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase BusinessLogic_v3.Modules._AutoGen.ITicketCommentBaseAutoGen.AssignedStaffUser 
        {
            get
            {
            	return this.AssignedStaffUser;
            }
            set
            {
            	this.AssignedStaffUser = (BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase) value;
            }
        }
            
/*
		public virtual long? MemberID
		{
		 	get 
		 	{
		 		return (this.Member != null ? (long?)this.Member.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _member;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase Member 
        {
        	get
        	{
        		return _member;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_member,value);
        		_member = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.ITicketCommentBaseAutoGen.Member 
        {
            get
            {
            	return this.Member;
            }
            set
            {
            	this.Member = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.TICKET_COMMENT_TYPE _commenttype;
        
        public virtual BusinessLogic_v3.Enums.TICKET_COMMENT_TYPE CommentType 
        {
        	get
        	{
        		return _commenttype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_commenttype,value);
        		_commenttype = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __AttachmentsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase, 
        	BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase,
        	BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase, BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase>
        {
            private BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase _item = null;
            public __AttachmentsCollectionInfo(BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase> Collection
            {
                get { return _item.__collection__Attachments; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase, BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase>.SetLinkOnItem(BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase item, BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase value)
            {
                item.TicketComment = value;
            }
        }
        
        private __AttachmentsCollectionInfo _Attachments = null;
        public __AttachmentsCollectionInfo Attachments
        {
            get
            {
                if (_Attachments == null)
                    _Attachments = new __AttachmentsCollectionInfo((BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase)this);
                return _Attachments;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBase> ITicketCommentBaseAutoGen.Attachments
        {
            get {  return this.Attachments; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase> __collection__Attachments
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'AttachmentBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
