using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.OrderApplicableSpecialOfferLinkModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.IOrderApplicableSpecialOfferLinkBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.IOrderApplicableSpecialOfferLinkBase item)
        {
        	return BusinessLogic_v3.Frontend.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class OrderApplicableSpecialOfferLinkBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<OrderApplicableSpecialOfferLinkBaseFrontend, BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.IOrderApplicableSpecialOfferLinkBase>

    {
		
        
        protected OrderApplicableSpecialOfferLinkBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
