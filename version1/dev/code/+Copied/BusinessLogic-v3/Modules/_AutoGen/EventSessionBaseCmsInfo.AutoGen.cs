using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EventSessionModule;
using BusinessLogic_v3.Cms.EventSessionModule;
using BusinessLogic_v3.Frontend.EventSessionModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventSessionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>
    {
		public EventSessionBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase dbItem)
            : base(BusinessLogic_v3.Cms.EventSessionModule.EventSessionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventSessionBaseFrontend FrontendItem
        {
            get { return (EventSessionBaseFrontend)base.FrontendItem; }

        }
        public new EventSessionBase DbItem
        {
            get
            {
                return (EventSessionBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Event { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo Location { get; protected set; }

        public CmsPropertyInfo StartDate { get; protected set; }

        public CmsPropertyInfo EndDate { get; protected set; }

        public CmsPropertyInfo Cost { get; protected set; }

        public CmsPropertyInfo SlotsAvailable { get; protected set; }

        public CmsPropertyInfo SlotsTaken { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo EventSessionPeriods { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.EventSessionPeriods = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector(item => item.EventSessionPeriods),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase>.GetPropertyBySelector(item => item.EventSession)));


			base.initBasicFields();
          
        }

    }
}
