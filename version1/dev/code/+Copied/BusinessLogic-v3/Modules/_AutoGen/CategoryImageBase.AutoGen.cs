using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.CategoryImageModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "CategoryImage")]
    public abstract class CategoryImageBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ICategoryImageBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static CategoryImageBaseFactory Factory
        {
            get
            {
                return CategoryImageBaseFactory.Instance; 
            }
        }    
        /*
        public static CategoryImageBase CreateNewItem()
        {
            return (CategoryImageBase)Factory.CreateNewItem();
        }

		public static CategoryImageBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<CategoryImageBase, CategoryImageBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static CategoryImageBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static CategoryImageBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<CategoryImageBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<CategoryImageBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<CategoryImageBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? CategoryID
		{
		 	get 
		 	{
		 		return (this.Category != null ? (long?)this.Category.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _category;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase Category 
        {
        	get
        	{
        		return _category;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_category,value);
        		_category = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.ICategoryImageBaseAutoGen.Category 
        {
            get
            {
            	return this.Category;
            }
            set
            {
            	this.Category = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _imagefilename;
        
        public virtual string ImageFilename 
        {
        	get
        	{
        		return _imagefilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagefilename,value);
        		_imagefilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
