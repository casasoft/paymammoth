using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;
using BusinessLogic_v3.Cms.Category_ParentChildLinkModule;
using BusinessLogic_v3.Frontend.Category_ParentChildLinkModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Category_ParentChildLinkBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>
    {
		public Category_ParentChildLinkBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase dbItem)
            : base(BusinessLogic_v3.Cms.Category_ParentChildLinkModule.Category_ParentChildLinkBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new Category_ParentChildLinkBaseFrontend FrontendItem
        {
            get { return (Category_ParentChildLinkBaseFrontend)base.FrontendItem; }

        }
        public new Category_ParentChildLinkBase DbItem
        {
            get
            {
                return (Category_ParentChildLinkBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ParentCategory { get; protected set; }

        public CmsPropertyInfo ChildCategory { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
