using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.RoutingInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "RoutingInfo")]
    public abstract class RoutingInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IRoutingInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static RoutingInfoBaseFactory Factory
        {
            get
            {
                return RoutingInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static RoutingInfoBase CreateNewItem()
        {
            return (RoutingInfoBase)Factory.CreateNewItem();
        }

		public static RoutingInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<RoutingInfoBase, RoutingInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static RoutingInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static RoutingInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<RoutingInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<RoutingInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<RoutingInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _routename;
        
        public virtual string RouteName 
        {
        	get
        	{
        		return _routename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_routename,value);
        		_routename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _physicalpath;
        
        public virtual string PhysicalPath 
        {
        	get
        	{
        		return _physicalpath;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_physicalpath,value);
        		_physicalpath = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _virtualpath;
        
        public virtual string VirtualPath 
        {
        	get
        	{
        		return _virtualpath;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_virtualpath,value);
        		_virtualpath = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
