using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.EventCategoryModule;
using BusinessLogic_v3.Cms.EventCategoryModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class EventCategoryBaseCmsFactory_AutoGen : CmsFactoryBase<EventCategoryBaseCmsInfo, EventCategoryBase>
    {
       
       public new static EventCategoryBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventCategoryBaseCmsFactory)CmsFactoryBase<EventCategoryBaseCmsInfo, EventCategoryBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = EventCategoryBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EventCategory.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EventCategory";

            this.QueryStringParamID = "EventCategoryId";

            cmsInfo.TitlePlural = "Event Categories";

            cmsInfo.TitleSingular =  "Event Category";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventCategoryBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "EventCategory/";
			UsedInProject = EventCategoryBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
