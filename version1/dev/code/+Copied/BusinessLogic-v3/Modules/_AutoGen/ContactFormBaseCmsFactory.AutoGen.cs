using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ContactFormModule;
using BusinessLogic_v3.Cms.ContactFormModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ContactFormBaseCmsFactory_AutoGen : CmsFactoryBase<ContactFormBaseCmsInfo, ContactFormBase>
    {
       
       public new static ContactFormBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContactFormBaseCmsFactory)CmsFactoryBase<ContactFormBaseCmsInfo, ContactFormBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ContactFormBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContactForm.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContactForm";

            this.QueryStringParamID = "ContactFormId";

            cmsInfo.TitlePlural = "Contact Forms";

            cmsInfo.TitleSingular =  "Contact Form";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContactFormBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ContactForm/";
			UsedInProject = ContactFormBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
