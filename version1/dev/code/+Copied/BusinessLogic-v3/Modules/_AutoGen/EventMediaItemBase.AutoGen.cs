using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.EventMediaItemModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "EventMediaItem")]
    public abstract class EventMediaItemBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IEventMediaItemBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static EventMediaItemBaseFactory Factory
        {
            get
            {
                return EventMediaItemBaseFactory.Instance; 
            }
        }    
        /*
        public static EventMediaItemBase CreateNewItem()
        {
            return (EventMediaItemBase)Factory.CreateNewItem();
        }

		public static EventMediaItemBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<EventMediaItemBase, EventMediaItemBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static EventMediaItemBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static EventMediaItemBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<EventMediaItemBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<EventMediaItemBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<EventMediaItemBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _imagefilename;
        
        public virtual string ImageFilename 
        {
        	get
        	{
        		return _imagefilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagefilename,value);
        		_imagefilename = value;
        		
        	}
        }
        
/*
		public virtual long? EventID
		{
		 	get 
		 	{
		 		return (this.Event != null ? (long?)this.Event.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.EventModule.EventBase _event;
        public virtual BusinessLogic_v3.Modules.EventModule.EventBase Event 
        {
        	get
        	{
        		return _event;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_event,value);
        		_event = value;
        	}
        }
            
        BusinessLogic_v3.Modules.EventModule.IEventBase BusinessLogic_v3.Modules._AutoGen.IEventMediaItemBaseAutoGen.Event 
        {
            get
            {
            	return this.Event;
            }
            set
            {
            	this.Event = (BusinessLogic_v3.Modules.EventModule.EventBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _videolink;
        
        public virtual string VideoLink 
        {
        	get
        	{
        		return _videolink;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_videolink,value);
        		_videolink = value;
        		
        	}
        }
        
/*
		public virtual long? EventBasicID
		{
		 	get 
		 	{
		 		return (this.EventBasic != null ? (long?)this.EventBasic.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase _eventbasic;
        public virtual BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase EventBasic 
        {
        	get
        	{
        		return _eventbasic;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_eventbasic,value);
        		_eventbasic = value;
        	}
        }
            
        BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBase BusinessLogic_v3.Modules._AutoGen.IEventMediaItemBaseAutoGen.EventBasic 
        {
            get
            {
            	return this.EventBasic;
            }
            set
            {
            	this.EventBasic = (BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
