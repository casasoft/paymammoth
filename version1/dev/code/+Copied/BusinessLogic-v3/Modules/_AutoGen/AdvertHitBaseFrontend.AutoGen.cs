using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.AdvertHitModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.AdvertHitModule.AdvertHitBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.AdvertHitModule.IAdvertHitBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.AdvertHitModule.AdvertHitBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.AdvertHitModule.IAdvertHitBase item)
        {
        	return BusinessLogic_v3.Frontend.AdvertHitModule.AdvertHitBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AdvertHitBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<AdvertHitBaseFrontend, BusinessLogic_v3.Modules.AdvertHitModule.IAdvertHitBase>

    {
		
        
        protected AdvertHitBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
