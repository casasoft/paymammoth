using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CultureInfoModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class CultureInfoBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DB.BaseDbFactory<CultureInfoBase, ICultureInfoBase>, ICultureInfoBaseFactoryAutoGen
    
    {
    
        public CultureInfoBaseFactoryAutoGen()
        {
        	
            
        }
        
		static CultureInfoBaseFactoryAutoGen()
        {
            UsedInProject = true;
        }
        
        
        public new static CultureInfoBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<CultureInfoBaseFactory>(null);
            }
        }
        
		public IQueryOver<CultureInfoBase, CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<CultureInfoBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
