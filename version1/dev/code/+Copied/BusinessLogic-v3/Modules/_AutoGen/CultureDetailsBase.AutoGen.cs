using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.CultureDetailsModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "CultureDetails")]
    public abstract class CultureDetailsBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ICultureDetailsBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static CultureDetailsBaseFactory Factory
        {
            get
            {
                return CultureDetailsBaseFactory.Instance; 
            }
        }    
        /*
        public static CultureDetailsBase CreateNewItem()
        {
            return (CultureDetailsBase)Factory.CreateNewItem();
        }

		public static CultureDetailsBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<CultureDetailsBase, CultureDetailsBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static CultureDetailsBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static CultureDetailsBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<CultureDetailsBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<CultureDetailsBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<CultureDetailsBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 _languageisocode;
        
        public virtual CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 LanguageISOCode 
        {
        	get
        	{
        		return _languageisocode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_languageisocode,value);
        		_languageisocode = value;
        		
        	}
        }
        
/*
		public virtual long? DefaultCurrencyID
		{
		 	get 
		 	{
		 		return (this.DefaultCurrency != null ? (long?)this.DefaultCurrency.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase _defaultcurrency;
        public virtual BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase DefaultCurrency 
        {
        	get
        	{
        		return _defaultcurrency;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_defaultcurrency,value);
        		_defaultcurrency = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase BusinessLogic_v3.Modules._AutoGen.ICultureDetailsBaseAutoGen.DefaultCurrency 
        {
            get
            {
            	return this.DefaultCurrency;
            }
            set
            {
            	this.DefaultCurrency = (BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? _specificcountrycode;
        
        public virtual CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? SpecificCountryCode 
        {
        	get
        	{
        		return _specificcountrycode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_specificcountrycode,value);
        		_specificcountrycode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _scriptsuffix;
        
        public virtual string ScriptSuffix 
        {
        	get
        	{
        		return _scriptsuffix;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_scriptsuffix,value);
        		_scriptsuffix = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isdefaultculture;
        
        public virtual bool IsDefaultCulture 
        {
        	get
        	{
        		return _isdefaultculture;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isdefaultculture,value);
        		_isdefaultculture = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _baseurlregex;
        
        public virtual string BaseUrlRegex 
        {
        	get
        	{
        		return _baseurlregex;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_baseurlregex,value);
        		_baseurlregex = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _baseredirectionurl;
        
        public virtual string BaseRedirectionUrl 
        {
        	get
        	{
        		return _baseredirectionurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_baseredirectionurl,value);
        		_baseredirectionurl = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _googleanalyticsrootdomain;
        
        public virtual string GoogleAnalyticsRootDomain 
        {
        	get
        	{
        		return _googleanalyticsrootdomain;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_googleanalyticsrootdomain,value);
        		_googleanalyticsrootdomain = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _googleanalyticsid;
        
        public virtual string GoogleAnalyticsID 
        {
        	get
        	{
        		return _googleanalyticsid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_googleanalyticsid,value);
        		_googleanalyticsid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _baseredirectionurllocalhost;
        
        public virtual string BaseRedirectionUrlLocalhost 
        {
        	get
        	{
        		return _baseredirectionurllocalhost;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_baseredirectionurllocalhost,value);
        		_baseredirectionurllocalhost = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _baseurlregexlocalhost;
        
        public virtual string BaseUrlRegexLocalhost 
        {
        	get
        	{
        		return _baseurlregexlocalhost;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_baseurlregexlocalhost,value);
        		_baseurlregexlocalhost = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionManyToManyRightSide  
        
		public class __BannersCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase,
			BusinessLogic_v3.Modules.BannerModule.BannerBase,
			BusinessLogic_v3.Modules.BannerModule.IBannerBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase, BusinessLogic_v3.Modules.BannerModule.BannerBase>
        {
            private BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase _item = null;
            public __BannersCollectionInfo(BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.BannerModule.BannerBase> Collection
            {
                get { return _item.__collection__Banners; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase,BusinessLogic_v3.Modules.BannerModule.BannerBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.BannerModule.BannerBase item)
            {
                return item.Cultures;
                
            }


            #endregion
        }

        private __BannersCollectionInfo _Banners = null;
        public __BannersCollectionInfo Banners
        {
            get
            {
                if (_Banners == null)
                    _Banners = new __BannersCollectionInfo((BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase)this);
                return _Banners;
            }
        }
        
           
        ICollectionManager<BusinessLogic_v3.Modules.BannerModule.IBannerBase> ICultureDetailsBaseAutoGen.Banners
        {
            get {  return this.Banners; }
        }
           

		protected virtual IEnumerable<BusinessLogic_v3.Modules.BannerModule.BannerBase> __collection__Banners
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'BannerBase' is not marked as UsedInProject"); }
        }





        

    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
