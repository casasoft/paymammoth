using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ArticleCommentModule;
using BusinessLogic_v3.Cms.ArticleCommentModule;
using BusinessLogic_v3.Frontend.ArticleCommentModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ArticleCommentBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase>
    {
		public ArticleCommentBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase dbItem)
            : base(BusinessLogic_v3.Cms.ArticleCommentModule.ArticleCommentBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ArticleCommentBaseFrontend FrontendItem
        {
            get { return (ArticleCommentBaseFrontend)base.FrontendItem; }

        }
        public new ArticleCommentBase DbItem
        {
            get
            {
                return (ArticleCommentBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Email { get; protected set; }

        public CmsPropertyInfo Comment { get; protected set; }

        public CmsPropertyInfo IPAddress { get; protected set; }

        public CmsPropertyInfo PostedOn { get; protected set; }

        public CmsPropertyInfo Article { get; protected set; }

        public CmsPropertyInfo Author { get; protected set; }

        public CmsPropertyInfo Date { get; protected set; }

        public CmsPropertyInfo Company { get; protected set; }

        public CmsPropertyInfo IsApproved { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
