using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using BusinessLogic_v3.Cms.ProductVariationMediaItemModule;
using BusinessLogic_v3.Frontend.ProductVariationMediaItemModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductVariationMediaItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase>
    {
		public ProductVariationMediaItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase dbItem)
            : base(BusinessLogic_v3.Cms.ProductVariationMediaItemModule.ProductVariationMediaItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductVariationMediaItemBaseFrontend FrontendItem
        {
            get { return (ProductVariationMediaItemBaseFrontend)base.FrontendItem; }

        }
        public new ProductVariationMediaItemBase DbItem
        {
            get
            {
                return (ProductVariationMediaItemBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ProductVariation { get; protected set; }

        public CmsPropertyInfo ImageFilename { get; protected set; }

        public CmsPropertyInfo Caption { get; protected set; }

        public CmsPropertyInfo ExtraValueChoice { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Reference { get; protected set; }

        public CmsPropertyInfo ImportReference { get; protected set; }

        public CmsPropertyInfo Size { get; protected set; }

        public CmsPropertyInfo Colour { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
