using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.AdvertSlotModule;
using BusinessLogic_v3.Cms.AdvertSlotModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class AdvertSlotBaseCmsFactory_AutoGen : CmsFactoryBase<AdvertSlotBaseCmsInfo, AdvertSlotBase>
    {
       
       public new static AdvertSlotBaseCmsFactory Instance
	    {
	         get
	         {
                 return (AdvertSlotBaseCmsFactory)CmsFactoryBase<AdvertSlotBaseCmsInfo, AdvertSlotBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = AdvertSlotBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "AdvertSlot.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "AdvertSlot";

            this.QueryStringParamID = "AdvertSlotId";

            cmsInfo.TitlePlural = "Advert Slots";

            cmsInfo.TitleSingular =  "Advert Slot";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public AdvertSlotBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "AdvertSlot/";
			UsedInProject = AdvertSlotBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
