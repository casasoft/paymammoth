using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.EventSubmissionModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.EventSubmissionModule.EventSubmissionBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.EventSubmissionModule.EventSubmissionBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBase item)
        {
        	return BusinessLogic_v3.Frontend.EventSubmissionModule.EventSubmissionBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventSubmissionBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<EventSubmissionBaseFrontend, BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBase>

    {
		
        
        protected EventSubmissionBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
