using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ArticleModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ArticleModule.ArticleBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ArticleModule.IArticleBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ArticleModule.ArticleBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ArticleModule.IArticleBase item)
        {
        	return BusinessLogic_v3.Frontend.ArticleModule.ArticleBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ArticleBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ArticleBaseFrontend, BusinessLogic_v3.Modules.ArticleModule.IArticleBase>

    {
		
        
        protected ArticleBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
