using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ContentText_CultureInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ContentText_CultureInfo")]
    public abstract class ContentText_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IContentText_CultureInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
      , IMultilingualContentInfo
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ContentText_CultureInfoBaseFactory Factory
        {
            get
            {
                return ContentText_CultureInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static ContentText_CultureInfoBase CreateNewItem()
        {
            return (ContentText_CultureInfoBase)Factory.CreateNewItem();
        }

		public static ContentText_CultureInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ContentText_CultureInfoBase, ContentText_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ContentText_CultureInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ContentText_CultureInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ContentText_CultureInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ContentText_CultureInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ContentText_CultureInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? CultureInfoID
		{
		 	get 
		 	{
		 		return (this.CultureInfo != null ? (long?)this.CultureInfo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase _cultureinfo;
        public virtual BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase CultureInfo 
        {
        	get
        	{
        		return _cultureinfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cultureinfo,value);
        		_cultureinfo = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase BusinessLogic_v3.Modules._AutoGen.IContentText_CultureInfoBaseAutoGen.CultureInfo 
        {
            get
            {
            	return this.CultureInfo;
            }
            set
            {
            	this.CultureInfo = (BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase) value;
            }
        }
            
/*
		public virtual long? ContentTextID
		{
		 	get 
		 	{
		 		return (this.ContentText != null ? (long?)this.ContentText.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase _contenttext;
        public virtual BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase ContentText 
        {
        	get
        	{
        		return _contenttext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_contenttext,value);
        		_contenttext = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase BusinessLogic_v3.Modules._AutoGen.IContentText_CultureInfoBaseAutoGen.ContentText 
        {
            get
            {
            	return this.ContentText;
            }
            set
            {
            	this.ContentText = (BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _content;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Content 
        {
        	get
        	{
        		return _content;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_content,value);
        		_content = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _imagealternatetext;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string ImageAlternateText 
        {
        	get
        	{
        		return _imagealternatetext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagealternatetext,value);
        		_imagealternatetext = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)




#region IMultilingualContentInfo Members

        long IMultilingualContentInfo.CultureInfoID
        {
            get
            {                          
            	if (this.CultureInfo != null)
                	return this.CultureInfo.ID;
               	else
               		return 0;
            }
        }
  		BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject IMultilingualContentInfo.GetLinkedItem()
        {
            return this.ContentText;
        }
#endregion



#endregion       

		

    }
}
