using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ShippingRatesGroupModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ShippingRatesGroupModule.ShippingRatesGroupBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ShippingRatesGroupModule.IShippingRatesGroupBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ShippingRatesGroupModule.ShippingRatesGroupBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ShippingRatesGroupModule.IShippingRatesGroupBase item)
        {
        	return BusinessLogic_v3.Frontend.ShippingRatesGroupModule.ShippingRatesGroupBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ShippingRatesGroupBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ShippingRatesGroupBaseFrontend, BusinessLogic_v3.Modules.ShippingRatesGroupModule.IShippingRatesGroupBase>

    {
		
        
        protected ShippingRatesGroupBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
