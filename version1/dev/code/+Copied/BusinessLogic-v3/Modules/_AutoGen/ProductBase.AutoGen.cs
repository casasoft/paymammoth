using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Product")]
    public abstract class ProductBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
        

			var cultureInfo = __getCurrentCultureInfo(); //since this is a new item, it is imperative that the culture is preloaded
            
            
            base.initPropertiesForNewItem();
        }
    	
  
    	public BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase) __getCultureInfoByLanguage(lang);
        }

     	public static ProductBaseFactory Factory
        {
            get
            {
                return ProductBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductBase CreateNewItem()
        {
            return (ProductBase)Factory.CreateNewItem();
        }

		public static ProductBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductBase, ProductBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Title
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Title;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Title_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Title,value);
                cultureInfo.Title = value;
            }
        }
        
        private string _title;
        public virtual string __Title_cultureBase
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		
        		_title = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private string _referencecode;
        
        public virtual string ReferenceCode 
        {
        	get
        	{
        		return _referencecode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_referencecode,value);
        		_referencecode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _supplierreferencecode;
        
        public virtual string SupplierReferenceCode 
        {
        	get
        	{
        		return _supplierreferencecode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_supplierreferencecode,value);
        		_supplierreferencecode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Description
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Description;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Description_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Description,value);
                cultureInfo.Description = value;
            }
        }
        
        private string _description;
        public virtual string __Description_cultureBase
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		
        		_description = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private double _priceretailinctaxperunit;
        
        public virtual double PriceRetailIncTaxPerUnit 
        {
        	get
        	{
        		return _priceretailinctaxperunit;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_priceretailinctaxperunit,value);
        		_priceretailinctaxperunit = value;
        		
        	}
        }
        
/*
		public virtual long? BrandID
		{
		 	get 
		 	{
		 		return (this.Brand != null ? (long?)this.Brand.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.BrandModule.BrandBase _brand;
        public virtual BusinessLogic_v3.Modules.BrandModule.BrandBase Brand 
        {
        	get
        	{
        		return _brand;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_brand,value);
        		_brand = value;
        	}
        }
            
        BusinessLogic_v3.Modules.BrandModule.IBrandBase BusinessLogic_v3.Modules._AutoGen.IProductBaseAutoGen.Brand 
        {
            get
            {
            	return this.Brand;
            }
            set
            {
            	this.Brand = (BusinessLogic_v3.Modules.BrandModule.BrandBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _extrainfo;
        
        public virtual string ExtraInfo 
        {
        	get
        	{
        		return _extrainfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_extrainfo,value);
        		_extrainfo = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _pricediscounted;
        
        public virtual double PriceDiscounted 
        {
        	get
        	{
        		return _pricediscounted;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_pricediscounted,value);
        		_pricediscounted = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _pdffilename;
        
        public virtual string PDFFilename 
        {
        	get
        	{
        		return _pdffilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_pdffilename,value);
        		_pdffilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isspecialoffer;
        
        public virtual bool IsSpecialOffer 
        {
        	get
        	{
        		return _isspecialoffer;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isspecialoffer,value);
        		_isspecialoffer = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _showprice;
        
        public virtual bool ShowPrice 
        {
        	get
        	{
        		return _showprice;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_showprice,value);
        		_showprice = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _weightinkg;
        
        public virtual double WeightInKg 
        {
        	get
        	{
        		return _weightinkg;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_weightinkg,value);
        		_weightinkg = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _warrantyinmonths;
        
        public virtual int WarrantyInMonths 
        {
        	get
        	{
        		return _warrantyinmonths;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_warrantyinmonths,value);
        		_warrantyinmonths = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _dateadded;
        
        public virtual DateTime DateAdded 
        {
        	get
        	{
        		return _dateadded;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dateadded,value);
        		_dateadded = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isfeatured;
        
        public virtual bool IsFeatured 
        {
        	get
        	{
        		return _isfeatured;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isfeatured,value);
        		_isfeatured = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _importreference;
        
        public virtual string ImportReference 
        {
        	get
        	{
        		return _importreference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_importreference,value);
        		_importreference = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _warrantytext;
        
        public virtual string WarrantyText 
        {
        	get
        	{
        		return _warrantytext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_warrantytext,value);
        		_warrantytext = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _metakeywords;
        
        public virtual string MetaKeywords 
        {
        	get
        	{
        		return _metakeywords;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metakeywords,value);
        		_metakeywords = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _categoryfeaturevalues_forsearch;
        
        public virtual string CategoryFeatureValues_ForSearch 
        {
        	get
        	{
        		return _categoryfeaturevalues_forsearch;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_categoryfeaturevalues_forsearch,value);
        		_categoryfeaturevalues_forsearch = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double? _taxratepercentage;
        
        public virtual double? TaxRatePercentage 
        {
        	get
        	{
        		return _taxratepercentage;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_taxratepercentage,value);
        		_taxratepercentage = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _productspecificationshtml;
        
        public virtual string ProductSpecificationsHtml 
        {
        	get
        	{
        		return _productspecificationshtml;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_productspecificationshtml,value);
        		_productspecificationshtml = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _dimensionslengthincm;
        
        public virtual double DimensionsLengthInCm 
        {
        	get
        	{
        		return _dimensionslengthincm;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dimensionslengthincm,value);
        		_dimensionslengthincm = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _dimensionswidthincm;
        
        public virtual double DimensionsWidthInCm 
        {
        	get
        	{
        		return _dimensionswidthincm;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dimensionswidthincm,value);
        		_dimensionswidthincm = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _dimensionsheightincm;
        
        public virtual double DimensionsHeightInCm 
        {
        	get
        	{
        		return _dimensionsheightincm;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dimensionsheightincm,value);
        		_dimensionsheightincm = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _dimensionsvolumeincc;
        
        public virtual double DimensionsVolumeInCc 
        {
        	get
        	{
        		return _dimensionsvolumeincc;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dimensionsvolumeincc,value);
        		_dimensionsvolumeincc = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _shortdescription;
        
        public virtual string ShortDescription 
        {
        	get
        	{
        		return _shortdescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shortdescription,value);
        		_shortdescription = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string MetaTitle
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.MetaTitle;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__MetaTitle_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.MetaTitle,value);
                cultureInfo.MetaTitle = value;
            }
        }
        
        private string _metatitle;
        public virtual string __MetaTitle_cultureBase
        {
        	get
        	{
        		return _metatitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metatitle,value);
        		
        		_metatitle = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string MetaDescription
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.MetaDescription;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__MetaDescription_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.MetaDescription,value);
                cultureInfo.MetaDescription = value;
            }
        }
        
        private string _metadescription;
        public virtual string __MetaDescription_cultureBase
        {
        	get
        	{
        		return _metadescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metadescription,value);
        		
        		_metadescription = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Material
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Material;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Material_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Material,value);
                cultureInfo.Material = value;
            }
        }
        
        private string _material;
        public virtual string __Material_cultureBase
        {
        	get
        	{
        		return _material;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_material,value);
        		
        		_material = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private double __actualprice;
        
        public virtual double _ActualPrice 
        {
        	get
        	{
        		return __actualprice;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__actualprice,value);
        		__actualprice = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string _ActualTitle
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo._ActualTitle;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	___ActualTitle_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo._ActualTitle,value);
                cultureInfo._ActualTitle = value;
            }
        }
        
        private string __actualtitle;
        public virtual string ___ActualTitle_cultureBase
        {
        	get
        	{
        		return __actualtitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__actualtitle,value);
        		
        		__actualtitle = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private double __specialofferdiscount;
        
        public virtual double _SpecialOfferDiscount 
        {
        	get
        	{
        		return __specialofferdiscount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__specialofferdiscount,value);
        		__specialofferdiscount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customcontenttags;
        
        public virtual string CustomContentTags 
        {
        	get
        	{
        		return _customcontenttags;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customcontenttags,value);
        		_customcontenttags = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __CategoryFeaturesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductModule.ProductBase, 
        	BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase,
        	BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>
        {
            private BusinessLogic_v3.Modules.ProductModule.ProductBase _item = null;
            public __CategoryFeaturesCollectionInfo(BusinessLogic_v3.Modules.ProductModule.ProductBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase> Collection
            {
                get { return _item.__collection__CategoryFeatures; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.SetLinkOnItem(BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase item, BusinessLogic_v3.Modules.ProductModule.ProductBase value)
            {
                item.ItemGroup = value;
            }
        }
        
        private __CategoryFeaturesCollectionInfo _CategoryFeatures = null;
        public __CategoryFeaturesCollectionInfo CategoryFeatures
        {
            get
            {
                if (_CategoryFeatures == null)
                    _CategoryFeatures = new __CategoryFeaturesCollectionInfo((BusinessLogic_v3.Modules.ProductModule.ProductBase)this);
                return _CategoryFeatures;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBase> IProductBaseAutoGen.CategoryFeatures
        {
            get {  return this.CategoryFeatures; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase> __collection__CategoryFeatures
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'CategoryFeatureBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ContactFormsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductModule.ProductBase, 
        	BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase,
        	BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>
        {
            private BusinessLogic_v3.Modules.ProductModule.ProductBase _item = null;
            public __ContactFormsCollectionInfo(BusinessLogic_v3.Modules.ProductModule.ProductBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase> Collection
            {
                get { return _item.__collection__ContactForms; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase item, BusinessLogic_v3.Modules.ProductModule.ProductBase value)
            {
                item.Product = value;
            }
        }
        
        private __ContactFormsCollectionInfo _ContactForms = null;
        public __ContactFormsCollectionInfo ContactForms
        {
            get
            {
                if (_ContactForms == null)
                    _ContactForms = new __ContactFormsCollectionInfo((BusinessLogic_v3.Modules.ProductModule.ProductBase)this);
                return _ContactForms;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase> IProductBaseAutoGen.ContactForms
        {
            get {  return this.ContactForms; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase> __collection__ContactForms
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ContactFormBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __Product_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductModule.ProductBase, 
        	BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase,
        	BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase>
        {
            private BusinessLogic_v3.Modules.ProductModule.ProductBase _item = null;
            public __Product_CultureInfosCollectionInfo(BusinessLogic_v3.Modules.ProductModule.ProductBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase> Collection
            {
                get { return _item.__collection__Product_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase item, BusinessLogic_v3.Modules.ProductModule.ProductBase value)
            {
                item.Product = value;
            }
        }
        
        private __Product_CultureInfosCollectionInfo _Product_CultureInfos = null;
        public __Product_CultureInfosCollectionInfo Product_CultureInfos
        {
            get
            {
                if (_Product_CultureInfos == null)
                    _Product_CultureInfos = new __Product_CultureInfosCollectionInfo((BusinessLogic_v3.Modules.ProductModule.ProductBase)this);
                return _Product_CultureInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBase> IProductBaseAutoGen.Product_CultureInfos
        {
            get {  return this.Product_CultureInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase> __collection__Product_CultureInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'Product_CultureInfoBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __CategoryLinksCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductModule.ProductBase, 
        	BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase,
        	BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase>
        {
            private BusinessLogic_v3.Modules.ProductModule.ProductBase _item = null;
            public __CategoryLinksCollectionInfo(BusinessLogic_v3.Modules.ProductModule.ProductBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase> Collection
            {
                get { return _item.__collection__CategoryLinks; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase item, BusinessLogic_v3.Modules.ProductModule.ProductBase value)
            {
                item.Product = value;
            }
        }
        
        private __CategoryLinksCollectionInfo _CategoryLinks = null;
        public __CategoryLinksCollectionInfo CategoryLinks
        {
            get
            {
                if (_CategoryLinks == null)
                    _CategoryLinks = new __CategoryLinksCollectionInfo((BusinessLogic_v3.Modules.ProductModule.ProductBase)this);
                return _CategoryLinks;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBase> IProductBaseAutoGen.CategoryLinks
        {
            get {  return this.CategoryLinks; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase> __collection__CategoryLinks
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductCategoryBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ProductCategoryFeatureValuesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductModule.ProductBase, 
        	BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase,
        	BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase>
        {
            private BusinessLogic_v3.Modules.ProductModule.ProductBase _item = null;
            public __ProductCategoryFeatureValuesCollectionInfo(BusinessLogic_v3.Modules.ProductModule.ProductBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase> Collection
            {
                get { return _item.__collection__ProductCategoryFeatureValues; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase item, BusinessLogic_v3.Modules.ProductModule.ProductBase value)
            {
                item.Product = value;
            }
        }
        
        private __ProductCategoryFeatureValuesCollectionInfo _ProductCategoryFeatureValues = null;
        public __ProductCategoryFeatureValuesCollectionInfo ProductCategoryFeatureValues
        {
            get
            {
                if (_ProductCategoryFeatureValues == null)
                    _ProductCategoryFeatureValues = new __ProductCategoryFeatureValuesCollectionInfo((BusinessLogic_v3.Modules.ProductModule.ProductBase)this);
                return _ProductCategoryFeatureValues;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBase> IProductBaseAutoGen.ProductCategoryFeatureValues
        {
            get {  return this.ProductCategoryFeatureValues; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase> __collection__ProductCategoryFeatureValues
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductCategoryFeatureValueBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ProductCategorySpecificationValuesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductModule.ProductBase, 
        	BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase,
        	BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase>
        {
            private BusinessLogic_v3.Modules.ProductModule.ProductBase _item = null;
            public __ProductCategorySpecificationValuesCollectionInfo(BusinessLogic_v3.Modules.ProductModule.ProductBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase> Collection
            {
                get { return _item.__collection__ProductCategorySpecificationValues; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase item, BusinessLogic_v3.Modules.ProductModule.ProductBase value)
            {
                item.Product = value;
            }
        }
        
        private __ProductCategorySpecificationValuesCollectionInfo _ProductCategorySpecificationValues = null;
        public __ProductCategorySpecificationValuesCollectionInfo ProductCategorySpecificationValues
        {
            get
            {
                if (_ProductCategorySpecificationValues == null)
                    _ProductCategorySpecificationValues = new __ProductCategorySpecificationValuesCollectionInfo((BusinessLogic_v3.Modules.ProductModule.ProductBase)this);
                return _ProductCategorySpecificationValues;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBase> IProductBaseAutoGen.ProductCategorySpecificationValues
        {
            get {  return this.ProductCategorySpecificationValues; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase> __collection__ProductCategorySpecificationValues
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductCategorySpecificationValueBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __RelatedProductLinksCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductModule.ProductBase, 
        	BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.ProductRelatedProductLinkBase,
        	BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.IProductRelatedProductLinkBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.ProductRelatedProductLinkBase>
        {
            private BusinessLogic_v3.Modules.ProductModule.ProductBase _item = null;
            public __RelatedProductLinksCollectionInfo(BusinessLogic_v3.Modules.ProductModule.ProductBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.ProductRelatedProductLinkBase> Collection
            {
                get { return _item.__collection__RelatedProductLinks; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.ProductRelatedProductLinkBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.ProductRelatedProductLinkBase item, BusinessLogic_v3.Modules.ProductModule.ProductBase value)
            {
                item.Product = value;
            }
        }
        
        private __RelatedProductLinksCollectionInfo _RelatedProductLinks = null;
        public __RelatedProductLinksCollectionInfo RelatedProductLinks
        {
            get
            {
                if (_RelatedProductLinks == null)
                    _RelatedProductLinks = new __RelatedProductLinksCollectionInfo((BusinessLogic_v3.Modules.ProductModule.ProductBase)this);
                return _RelatedProductLinks;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.IProductRelatedProductLinkBase> IProductBaseAutoGen.RelatedProductLinks
        {
            get {  return this.RelatedProductLinks; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.ProductRelatedProductLinkBase> __collection__RelatedProductLinks
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductRelatedProductLinkBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ProductVariationsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductModule.ProductBase, 
        	BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase,
        	BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>
        {
            private BusinessLogic_v3.Modules.ProductModule.ProductBase _item = null;
            public __ProductVariationsCollectionInfo(BusinessLogic_v3.Modules.ProductModule.ProductBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase> Collection
            {
                get { return _item.__collection__ProductVariations; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase item, BusinessLogic_v3.Modules.ProductModule.ProductBase value)
            {
                item.Product = value;
            }
        }
        
        private __ProductVariationsCollectionInfo _ProductVariations = null;
        public __ProductVariationsCollectionInfo ProductVariations
        {
            get
            {
                if (_ProductVariations == null)
                    _ProductVariations = new __ProductVariationsCollectionInfo((BusinessLogic_v3.Modules.ProductModule.ProductBase)this);
                return _ProductVariations;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase> IProductBaseAutoGen.ProductVariations
        {
            get {  return this.ProductVariations; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase> __collection__ProductVariations
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductVariationBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ProductVersionsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductModule.ProductBase, 
        	BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase,
        	BusinessLogic_v3.Modules.ProductVersionModule.IProductVersionBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase>
        {
            private BusinessLogic_v3.Modules.ProductModule.ProductBase _item = null;
            public __ProductVersionsCollectionInfo(BusinessLogic_v3.Modules.ProductModule.ProductBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase> Collection
            {
                get { return _item.__collection__ProductVersions; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductModule.ProductBase, BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase item, BusinessLogic_v3.Modules.ProductModule.ProductBase value)
            {
                item.Product = value;
            }
        }
        
        private __ProductVersionsCollectionInfo _ProductVersions = null;
        public __ProductVersionsCollectionInfo ProductVersions
        {
            get
            {
                if (_ProductVersions == null)
                    _ProductVersions = new __ProductVersionsCollectionInfo((BusinessLogic_v3.Modules.ProductModule.ProductBase)this);
                return _ProductVersions;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductVersionModule.IProductVersionBase> IProductBaseAutoGen.ProductVersions
        {
            get {  return this.ProductVersions; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase> __collection__ProductVersions
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductVersionBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
