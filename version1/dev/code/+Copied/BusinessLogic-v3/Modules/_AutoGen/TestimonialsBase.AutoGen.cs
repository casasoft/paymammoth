using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.TestimonialsModule;                

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public abstract class TestimonialsBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ITestimonialsBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static TestimonialsBaseFactory Factory
        {
            get
            {
                return TestimonialsBaseFactory.Instance; 
            }
        }    
        /*
        public static TestimonialsBase CreateNewItem()
        {
            return (TestimonialsBase)Factory.CreateNewItem();
        }

		public static TestimonialsBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<TestimonialsBase, TestimonialsBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static TestimonialsBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static TestimonialsBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<TestimonialsBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<TestimonialsBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<TestimonialsBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _fullname;
        public virtual string FullName 
        {
        	get
        	{
        		return _fullname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_fullname,value);
        		_fullname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _testimonial;
        public virtual string Testimonial 
        {
        	get
        	{
        		return _testimonial;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_testimonial,value);
        		_testimonial = value;
        		
        	}
        }
        
/*
		public virtual long? EventID
		{
		 	get 
		 	{
		 		return (this.Event != null ? (long?)this.Event.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.EventModule.EventBase _event;
        public virtual BusinessLogic_v3.Modules.EventModule.EventBase Event 
        {
        	get
        	{
        		return _event;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_event,value);
        		_event = value;
        	}
        }
            
        BusinessLogic_v3.Modules.EventModule.IEventBase BusinessLogic_v3.Modules._AutoGen.ITestimonialsBaseAutoGen.Event 
        {
            get
            {
            	return this.Event;
            }
            set
            {
            	this.Event = (BusinessLogic_v3.Modules.EventModule.EventBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
