using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ArticleCommentModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ArticleCommentModule.ArticleCommentBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ArticleCommentModule.ArticleCommentBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase item)
        {
        	return BusinessLogic_v3.Frontend.ArticleCommentModule.ArticleCommentBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ArticleCommentBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ArticleCommentBaseFrontend, BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase>

    {
		
        
        protected ArticleCommentBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
