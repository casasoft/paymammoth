using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.AdvertSlotModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.AdvertSlotModule.AdvertSlotBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.AdvertSlotModule.IAdvertSlotBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.AdvertSlotModule.AdvertSlotBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.AdvertSlotModule.IAdvertSlotBase item)
        {
        	return BusinessLogic_v3.Frontend.AdvertSlotModule.AdvertSlotBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AdvertSlotBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<AdvertSlotBaseFrontend, BusinessLogic_v3.Modules.AdvertSlotModule.IAdvertSlotBase>

    {
		
        
        protected AdvertSlotBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
