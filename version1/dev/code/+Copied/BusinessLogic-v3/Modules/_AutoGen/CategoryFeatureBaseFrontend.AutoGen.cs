using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.CategoryFeatureModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.CategoryFeatureModule.CategoryFeatureBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.CategoryFeatureModule.CategoryFeatureBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBase item)
        {
        	return BusinessLogic_v3.Frontend.CategoryFeatureModule.CategoryFeatureBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CategoryFeatureBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<CategoryFeatureBaseFrontend, BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBase>

    {
		
        
        protected CategoryFeatureBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
