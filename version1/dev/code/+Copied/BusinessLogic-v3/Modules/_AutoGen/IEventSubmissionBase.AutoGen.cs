using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.EventSubmissionModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IEventSubmissionBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string EventName { get; set; }
		//Iproperty_normal
        string UserFullName { get; set; }
		//Iproperty_normal
        string UserEmail { get; set; }
		//Iproperty_normal
        DateTime EventStartDate { get; set; }
		//Iproperty_normal
        DateTime EventEndDate { get; set; }
		//Iproperty_normal
        string UserIpAddress { get; set; }
		//Iproperty_normal
        DateTime Timestamp { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase Category {get; set; }

		//Iproperty_normal
        bool IsConvertedToRealEvent { get; set; }
		//Iproperty_normal
        bool Approved { get; set; }
		//Iproperty_normal
        DateTime? ApprovedOn { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase ApprovedBy {get; set; }

		//Iproperty_normal
        string EventDescription { get; set; }
		//Iproperty_normal
        string EventSummary { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBase> EventBasics { get; }

 
		

    	
    	
      
      

    }
}
