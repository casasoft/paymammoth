using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CmsUserModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface ICmsUserBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Name { get; set; }
		//Iproperty_normal
        string Surname { get; set; }
		//Iproperty_normal
        string Username { get; set; }
		//Iproperty_normal
        string Password { get; set; }
		//Iproperty_normal
        DateTime? LastLoggedIn { get; set; }
		//Iproperty_normal
        DateTime? _LastLoggedIn_New { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.CMS_ACCESS_TYPE AccessType { get; set; }
		//Iproperty_normal
        string SessionGUID { get; set; }
		//Iproperty_normal
        string PasswordSalt { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE PasswordEncryptionType { get; set; }
		//Iproperty_normal
        int PasswordIterations { get; set; }
		//Iproperty_normal
        bool HiddenFromNonCasaSoft { get; set; }
		//Iproperty_normal
        string CmsCustomLogoLinkUrl { get; set; }
		//Iproperty_normal
        bool Disabled { get; set; }
		//Iproperty_normal
        bool TicketingSystemSupportUser { get; set; }
		//Iproperty_normal
        string Email { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase> Affiliates { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase> Classifieds { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBase> EventSubmissions { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> Members { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.TicketModule.ITicketBase> AssignedTickets { get; }

        //IBaseClass_CollectionManyToManyLeftSide  
        ICollectionManager<BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBase> CmsUserRoles { get; }

 
		

    	
    	
      
      

    }
}
