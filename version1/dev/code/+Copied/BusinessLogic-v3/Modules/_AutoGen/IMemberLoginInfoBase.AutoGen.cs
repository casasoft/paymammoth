using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberLoginInfoModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IMemberLoginInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase Member {get; set; }

		//Iproperty_normal
        DateTime DateTime { get; set; }
		//Iproperty_normal
        string IP { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.MEMBER_LOGIN_INFO LoginInfoType { get; set; }
		//Iproperty_normal
        string AdditionalMsg { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
