using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.MeasurementUnitModule;
using BusinessLogic_v3.Cms.MeasurementUnitModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class MeasurementUnitBaseCmsFactory_AutoGen : CmsFactoryBase<MeasurementUnitBaseCmsInfo, MeasurementUnitBase>
    {
       
       public new static MeasurementUnitBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MeasurementUnitBaseCmsFactory)CmsFactoryBase<MeasurementUnitBaseCmsInfo, MeasurementUnitBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = MeasurementUnitBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MeasurementUnit.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MeasurementUnit";

            this.QueryStringParamID = "MeasurementUnitId";

            cmsInfo.TitlePlural = "Measurement Units";

            cmsInfo.TitleSingular =  "Measurement Unit";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MeasurementUnitBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "MeasurementUnit/";
			UsedInProject = MeasurementUnitBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
