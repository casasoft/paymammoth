using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Cms.ProductModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ProductBaseCmsFactory_AutoGen : CmsFactoryBase<ProductBaseCmsInfo, ProductBase>
    {
       
       public new static ProductBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductBaseCmsFactory)CmsFactoryBase<ProductBaseCmsInfo, ProductBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ProductBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Product.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Product";

            this.QueryStringParamID = "ProductId";

            cmsInfo.TitlePlural = "Products";

            cmsInfo.TitleSingular =  "Product";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Product/";
			UsedInProject = ProductBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
