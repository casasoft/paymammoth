using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ImageSizingInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ImageSizingInfo")]
    public abstract class ImageSizingInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IImageSizingInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ImageSizingInfoBaseFactory Factory
        {
            get
            {
                return ImageSizingInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static ImageSizingInfoBase CreateNewItem()
        {
            return (ImageSizingInfoBase)Factory.CreateNewItem();
        }

		public static ImageSizingInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ImageSizingInfoBase, ImageSizingInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ImageSizingInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ImageSizingInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ImageSizingInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ImageSizingInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ImageSizingInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _maximumwidth;
        
        public virtual int MaximumWidth 
        {
        	get
        	{
        		return _maximumwidth;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_maximumwidth,value);
        		_maximumwidth = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _maximumheight;
        
        public virtual int MaximumHeight 
        {
        	get
        	{
        		return _maximumheight;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_maximumheight,value);
        		_maximumheight = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __SizingInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase, 
        	BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase,
        	BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase, BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase>
        {
            private BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase _item = null;
            public __SizingInfosCollectionInfo(BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase> Collection
            {
                get { return _item.__collection__SizingInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase, BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase item, BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase value)
            {
                item.ImageSizingInfo = value;
            }
        }
        
        private __SizingInfosCollectionInfo _SizingInfos = null;
        public __SizingInfosCollectionInfo SizingInfos
        {
            get
            {
                if (_SizingInfos == null)
                    _SizingInfos = new __SizingInfosCollectionInfo((BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase)this);
                return _SizingInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBase> IImageSizingInfoBaseAutoGen.SizingInfos
        {
            get {  return this.SizingInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase> __collection__SizingInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ImageSpecificSizeInfoBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
