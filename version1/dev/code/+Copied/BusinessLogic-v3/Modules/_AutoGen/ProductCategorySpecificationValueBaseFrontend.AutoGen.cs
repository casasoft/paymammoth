using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductCategorySpecificationValueModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductCategorySpecificationValueBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductCategorySpecificationValueBaseFrontend, BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBase>

    {
		
        
        protected ProductCategorySpecificationValueBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
