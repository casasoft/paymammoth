using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.Article_RelatedLinkModule;

//DbObjectBuilder v8

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class Article_RelatedLinkBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<Article_RelatedLinkBase, IArticle_RelatedLinkBase>, IArticle_RelatedLinkBaseFactoryAutoGen
    
    {
    
        public Article_RelatedLinkBaseFactoryAutoGen()
        {
        	
            
        }
        
		static Article_RelatedLinkBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static Article_RelatedLinkBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<Article_RelatedLinkBaseFactory>(null);
            }
        }
        
		public IQueryOver<Article_RelatedLinkBase, Article_RelatedLinkBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Article_RelatedLinkBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
