using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.PopularSearchModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "PopularSearch")]
    public abstract class PopularSearchBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IPopularSearchBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static PopularSearchBaseFactory Factory
        {
            get
            {
                return PopularSearchBaseFactory.Instance; 
            }
        }    
        /*
        public static PopularSearchBase CreateNewItem()
        {
            return (PopularSearchBase)Factory.CreateNewItem();
        }

		public static PopularSearchBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<PopularSearchBase, PopularSearchBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static PopularSearchBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static PopularSearchBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<PopularSearchBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<PopularSearchBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<PopularSearchBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _searchurl;
        
        public virtual string SearchURL 
        {
        	get
        	{
        		return _searchurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_searchurl,value);
        		_searchurl = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _htmltext;
        
        public virtual string HtmlText 
        {
        	get
        	{
        		return _htmltext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_htmltext,value);
        		_htmltext = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _hidefromfrontend;
        
        public virtual bool HideFromFrontend 
        {
        	get
        	{
        		return _hidefromfrontend;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_hidefromfrontend,value);
        		_hidefromfrontend = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
