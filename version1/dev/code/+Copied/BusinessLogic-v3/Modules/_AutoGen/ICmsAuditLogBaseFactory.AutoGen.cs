using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CmsAuditLogModule;


namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public interface ICmsAuditLogBaseFactoryAutoGen : CS.General_v3.Classes.DbObjects.IBaseDbFactory<BusinessLogic_v3.Modules.CmsAuditLogModule.ICmsAuditLogBase>
    {
    
    }
}
