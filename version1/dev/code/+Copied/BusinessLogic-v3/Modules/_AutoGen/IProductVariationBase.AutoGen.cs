using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductVariationModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IProductVariationBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string SupplierRefCode { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ProductModule.IProductBase Product {get; set; }

		//Iproperty_normal
        string Barcode { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ProductVariationColourModule.IProductVariationColourBase Colour {get; set; }

		//IProperty_Multilingual
	   string Size { get; set ;}
		//Iproperty_normal
        int Quantity { get; set; }
		//Iproperty_normal
        int ReorderAmount { get; set; }
		//Iproperty_normal
        string ImportReference { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase> OrderItems { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfoBase> ProductVariation_CultureInfos { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBase> MediaItems { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase> ShoppingCartItems { get; }

 
		

    	
    	
      
      

    }
}
