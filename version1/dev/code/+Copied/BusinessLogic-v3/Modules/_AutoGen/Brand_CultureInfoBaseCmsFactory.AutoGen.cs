using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.Brand_CultureInfoModule;
using BusinessLogic_v3.Cms.Brand_CultureInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class Brand_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<Brand_CultureInfoBaseCmsInfo, Brand_CultureInfoBase>
    {
       
       public new static Brand_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Brand_CultureInfoBaseCmsFactory)CmsFactoryBase<Brand_CultureInfoBaseCmsInfo, Brand_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = Brand_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Brand_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Brand_CultureInfo";

            this.QueryStringParamID = "Brand_CultureInfoId";

            cmsInfo.TitlePlural = "Brand _ Culture Infos";

            cmsInfo.TitleSingular =  "Brand _ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Brand_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Brand_CultureInfo/";
			UsedInProject = Brand_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
