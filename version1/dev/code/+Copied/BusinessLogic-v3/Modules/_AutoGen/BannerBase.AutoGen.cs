using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.BannerModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Banner")]
    public abstract class BannerBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IBannerBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static BannerBaseFactory Factory
        {
            get
            {
                return BannerBaseFactory.Instance; 
            }
        }    
        /*
        public static BannerBase CreateNewItem()
        {
            return (BannerBase)Factory.CreateNewItem();
        }

		public static BannerBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<BannerBase, BannerBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static BannerBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static BannerBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<BannerBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<BannerBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<BannerBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _mediaitemfilename;
        
        public virtual string MediaItemFilename 
        {
        	get
        	{
        		return _mediaitemfilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_mediaitemfilename,value);
        		_mediaitemfilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _link;
        
        public virtual string Link 
        {
        	get
        	{
        		return _link;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_link,value);
        		_link = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _durationms;
        
        public virtual int DurationMS 
        {
        	get
        	{
        		return _durationms;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_durationms,value);
        		_durationms = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.HREF_TARGET _hreftarget;
        
        public virtual CS.General_v3.Enums.HREF_TARGET HrefTarget 
        {
        	get
        	{
        		return _hreftarget;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_hreftarget,value);
        		_hreftarget = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _slideshowdelaysec;
        
        public virtual double SlideshowDelaySec 
        {
        	get
        	{
        		return _slideshowdelaysec;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_slideshowdelaysec,value);
        		_slideshowdelaysec = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _htmldescription;
        
        public virtual string HTMLDescription 
        {
        	get
        	{
        		return _htmldescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_htmldescription,value);
        		_htmldescription = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionManyToManyLeftSide  
        
		public class __CulturesCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.BannerModule.BannerBase, 
			BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase,
			BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.BannerModule.BannerBase, BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>
        {
            private BusinessLogic_v3.Modules.BannerModule.BannerBase _item = null;
            public __CulturesCollectionInfo(BusinessLogic_v3.Modules.BannerModule.BannerBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> Collection
            {
                get { return _item.__collection__Cultures; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.BannerModule.BannerBase,BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.BannerModule.BannerBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase item)
            {
                return item.Banners;
                
            }


            #endregion
        }

        private __CulturesCollectionInfo _Cultures = null;
        public __CulturesCollectionInfo Cultures
        {
            get
            {
                if (_Cultures == null)
                    _Cultures = new __CulturesCollectionInfo((BusinessLogic_v3.Modules.BannerModule.BannerBase)this);
                return _Cultures;
            }
        }
            
        ICollectionManager<BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase> IBannerBaseAutoGen.Cultures
        {
            get {  return this.Cultures; }
        }
            
        
		protected virtual IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> __collection__Cultures
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'CultureDetailsBase' is not marked as UsedInProject"); }
        }





        

    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
