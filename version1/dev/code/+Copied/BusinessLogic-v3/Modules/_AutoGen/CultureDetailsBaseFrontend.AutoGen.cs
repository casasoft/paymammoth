using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.CultureDetailsModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.CultureDetailsModule.CultureDetailsBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.CultureDetailsModule.CultureDetailsBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase item)
        {
        	return BusinessLogic_v3.Frontend.CultureDetailsModule.CultureDetailsBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CultureDetailsBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<CultureDetailsBaseFrontend, BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase>

    {
		
        
        protected CultureDetailsBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
