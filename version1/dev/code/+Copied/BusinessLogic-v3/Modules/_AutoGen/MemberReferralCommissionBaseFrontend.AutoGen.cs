using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.MemberReferralCommissionModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.MemberReferralCommissionModule.MemberReferralCommissionBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.MemberReferralCommissionModule.IMemberReferralCommissionBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.MemberReferralCommissionModule.MemberReferralCommissionBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.MemberReferralCommissionModule.IMemberReferralCommissionBase item)
        {
        	return BusinessLogic_v3.Frontend.MemberReferralCommissionModule.MemberReferralCommissionBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberReferralCommissionBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<MemberReferralCommissionBaseFrontend, BusinessLogic_v3.Modules.MemberReferralCommissionModule.IMemberReferralCommissionBase>

    {
		
        
        protected MemberReferralCommissionBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
