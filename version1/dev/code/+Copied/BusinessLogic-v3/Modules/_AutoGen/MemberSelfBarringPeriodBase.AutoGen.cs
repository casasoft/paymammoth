using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "MemberSelfBarringPeriod")]
    public abstract class MemberSelfBarringPeriodBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IMemberSelfBarringPeriodBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static MemberSelfBarringPeriodBaseFactory Factory
        {
            get
            {
                return MemberSelfBarringPeriodBaseFactory.Instance; 
            }
        }    
        /*
        public static MemberSelfBarringPeriodBase CreateNewItem()
        {
            return (MemberSelfBarringPeriodBase)Factory.CreateNewItem();
        }

		public static MemberSelfBarringPeriodBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MemberSelfBarringPeriodBase, MemberSelfBarringPeriodBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MemberSelfBarringPeriodBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MemberSelfBarringPeriodBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MemberSelfBarringPeriodBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MemberSelfBarringPeriodBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MemberSelfBarringPeriodBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? MemberID
		{
		 	get 
		 	{
		 		return (this.Member != null ? (long?)this.Member.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _member;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase Member 
        {
        	get
        	{
        		return _member;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_member,value);
        		_member = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IMemberSelfBarringPeriodBaseAutoGen.Member 
        {
            get
            {
            	return this.Member;
            }
            set
            {
            	this.Member = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private DateTime _startdate;
        
        public virtual DateTime StartDate 
        {
        	get
        	{
        		return _startdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_startdate,value);
        		_startdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _enddate;
        
        public virtual DateTime EndDate 
        {
        	get
        	{
        		return _enddate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_enddate,value);
        		_enddate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _timestamp;
        
        public virtual DateTime Timestamp 
        {
        	get
        	{
        		return _timestamp;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_timestamp,value);
        		_timestamp = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _notificationexpiry1sent;
        
        public virtual bool NotificationExpiry1Sent 
        {
        	get
        	{
        		return _notificationexpiry1sent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notificationexpiry1sent,value);
        		_notificationexpiry1sent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _notificationexpiry2sent;
        
        public virtual bool NotificationExpiry2Sent 
        {
        	get
        	{
        		return _notificationexpiry2sent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notificationexpiry2sent,value);
        		_notificationexpiry2sent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _notificationexpiredsent;
        
        public virtual bool NotificationExpiredSent 
        {
        	get
        	{
        		return _notificationexpiredsent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notificationexpiredsent,value);
        		_notificationexpiredsent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _ip;
        
        public virtual string IP 
        {
        	get
        	{
        		return _ip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ip,value);
        		_ip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _notificationexpiry1_datetosend;
        
        public virtual DateTime? NotificationExpiry1_DateToSend 
        {
        	get
        	{
        		return _notificationexpiry1_datetosend;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notificationexpiry1_datetosend,value);
        		_notificationexpiry1_datetosend = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _notificationexpiry2_datetosend;
        
        public virtual DateTime? NotificationExpiry2_DateToSend 
        {
        	get
        	{
        		return _notificationexpiry2_datetosend;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notificationexpiry2_datetosend,value);
        		_notificationexpiry2_datetosend = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
