using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductVariationColourModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductVariationColourModule.ProductVariationColourBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductVariationColourModule.IProductVariationColourBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductVariationColourModule.ProductVariationColourBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductVariationColourModule.IProductVariationColourBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductVariationColourModule.ProductVariationColourBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductVariationColourBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductVariationColourBaseFrontend, BusinessLogic_v3.Modules.ProductVariationColourModule.IProductVariationColourBase>

    {
		
        
        protected ProductVariationColourBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
