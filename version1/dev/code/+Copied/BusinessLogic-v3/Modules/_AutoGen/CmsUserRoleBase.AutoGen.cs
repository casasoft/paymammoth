using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.CmsUserRoleModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "CmsUserRole")]
    public abstract class CmsUserRoleBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ICmsUserRoleBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static CmsUserRoleBaseFactory Factory
        {
            get
            {
                return CmsUserRoleBaseFactory.Instance; 
            }
        }    
        /*
        public static CmsUserRoleBase CreateNewItem()
        {
            return (CmsUserRoleBase)Factory.CreateNewItem();
        }

		public static CmsUserRoleBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<CmsUserRoleBase, CmsUserRoleBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static CmsUserRoleBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static CmsUserRoleBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<CmsUserRoleBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<CmsUserRoleBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<CmsUserRoleBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionManyToManyRightSide  
        
		public class __CmsUsersCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase,
			BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase,
			BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase, BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase>
        {
            private BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase _item = null;
            public __CmsUsersCollectionInfo(BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase> Collection
            {
                get { return _item.__collection__CmsUsers; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase,BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase item)
            {
                return item.CmsUserRoles;
                
            }


            #endregion
        }

        private __CmsUsersCollectionInfo _CmsUsers = null;
        public __CmsUsersCollectionInfo CmsUsers
        {
            get
            {
                if (_CmsUsers == null)
                    _CmsUsers = new __CmsUsersCollectionInfo((BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase)this);
                return _CmsUsers;
            }
        }
        
           
        ICollectionManager<BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase> ICmsUserRoleBaseAutoGen.CmsUsers
        {
            get {  return this.CmsUsers; }
        }
           

		protected virtual IEnumerable<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase> __collection__CmsUsers
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'CmsUserBase' is not marked as UsedInProject"); }
        }





        

    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
