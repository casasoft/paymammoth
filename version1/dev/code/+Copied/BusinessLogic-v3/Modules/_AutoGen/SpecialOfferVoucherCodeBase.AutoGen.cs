using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "SpecialOfferVoucherCode")]
    public abstract class SpecialOfferVoucherCodeBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ISpecialOfferVoucherCodeBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static SpecialOfferVoucherCodeBaseFactory Factory
        {
            get
            {
                return SpecialOfferVoucherCodeBaseFactory.Instance; 
            }
        }    
        /*
        public static SpecialOfferVoucherCodeBase CreateNewItem()
        {
            return (SpecialOfferVoucherCodeBase)Factory.CreateNewItem();
        }

		public static SpecialOfferVoucherCodeBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<SpecialOfferVoucherCodeBase, SpecialOfferVoucherCodeBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static SpecialOfferVoucherCodeBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static SpecialOfferVoucherCodeBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<SpecialOfferVoucherCodeBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<SpecialOfferVoucherCodeBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<SpecialOfferVoucherCodeBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? SpecialOfferID
		{
		 	get 
		 	{
		 		return (this.SpecialOffer != null ? (long?)this.SpecialOffer.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase _specialoffer;
        public virtual BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase SpecialOffer 
        {
        	get
        	{
        		return _specialoffer;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_specialoffer,value);
        		_specialoffer = value;
        	}
        }
            
        BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBase BusinessLogic_v3.Modules._AutoGen.ISpecialOfferVoucherCodeBaseAutoGen.SpecialOffer 
        {
            get
            {
            	return this.SpecialOffer;
            }
            set
            {
            	this.SpecialOffer = (BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private int _quantityleft;
        
        public virtual int QuantityLeft 
        {
        	get
        	{
        		return _quantityleft;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_quantityleft,value);
        		_quantityleft = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _vouchercode;
        
        public virtual string VoucherCode 
        {
        	get
        	{
        		return _vouchercode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_vouchercode,value);
        		_vouchercode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _createdon;
        
        public virtual DateTime CreatedOn 
        {
        	get
        	{
        		return _createdon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_createdon,value);
        		_createdon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _datefrom;
        
        public virtual DateTime? DateFrom 
        {
        	get
        	{
        		return _datefrom;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datefrom,value);
        		_datefrom = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _dateto;
        
        public virtual DateTime? DateTo 
        {
        	get
        	{
        		return _dateto;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dateto,value);
        		_dateto = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isnotlimitedbyquantity;
        
        public virtual bool IsNotLimitedByQuantity 
        {
        	get
        	{
        		return _isnotlimitedbyquantity;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isnotlimitedbyquantity,value);
        		_isnotlimitedbyquantity = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ShoppingCartsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase, 
        	BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase,
        	BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase, BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase>
        {
            private BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase _item = null;
            public __ShoppingCartsCollectionInfo(BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase> Collection
            {
                get { return _item.__collection__ShoppingCarts; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase, BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase item, BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase value)
            {
                item.SpecialOfferVoucherCode = value;
            }
        }
        
        private __ShoppingCartsCollectionInfo _ShoppingCarts = null;
        public __ShoppingCartsCollectionInfo ShoppingCarts
        {
            get
            {
                if (_ShoppingCarts == null)
                    _ShoppingCarts = new __ShoppingCartsCollectionInfo((BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase)this);
                return _ShoppingCarts;
            }
        }
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase> __collection__ShoppingCarts
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ShoppingCartBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
