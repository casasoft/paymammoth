using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;

//DbObjectBuilder v8

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class Article_ParentChildLinkBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<Article_ParentChildLinkBase, IArticle_ParentChildLinkBase>, IArticle_ParentChildLinkBaseFactoryAutoGen
    
    {
    
        public Article_ParentChildLinkBaseFactoryAutoGen()
        {
        	
            
        }
        
		static Article_ParentChildLinkBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static Article_ParentChildLinkBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<Article_ParentChildLinkBaseFactory>(null);
            }
        }
        
		public IQueryOver<Article_ParentChildLinkBase, Article_ParentChildLinkBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Article_ParentChildLinkBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
