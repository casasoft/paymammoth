using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.OrderModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.OrderModule.OrderBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.OrderModule.IOrderBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.OrderModule.OrderBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.OrderModule.IOrderBase item)
        {
        	return BusinessLogic_v3.Frontend.OrderModule.OrderBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class OrderBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<OrderBaseFrontend, BusinessLogic_v3.Modules.OrderModule.IOrderBase>

    {
		
        
        protected OrderBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
