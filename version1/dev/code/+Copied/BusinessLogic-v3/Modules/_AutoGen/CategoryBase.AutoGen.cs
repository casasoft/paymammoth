using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.CategoryModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Category")]
    public abstract class CategoryBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ICategoryBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
        

			var cultureInfo = __getCurrentCultureInfo(); //since this is a new item, it is imperative that the culture is preloaded
            
            
            base.initPropertiesForNewItem();
        }
    	
  
    	public BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase) __getCultureInfoByLanguage(lang);
        }

     	public static CategoryBaseFactory Factory
        {
            get
            {
                return CategoryBaseFactory.Instance; 
            }
        }    
        /*
        public static CategoryBase CreateNewItem()
        {
            return (CategoryBase)Factory.CreateNewItem();
        }

		public static CategoryBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<CategoryBase, CategoryBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static CategoryBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static CategoryBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<CategoryBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<CategoryBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<CategoryBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string TitleSingular
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.TitleSingular;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__TitleSingular_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.TitleSingular,value);
                cultureInfo.TitleSingular = value;
            }
        }
        
        private string _titlesingular;
        public virtual string __TitleSingular_cultureBase
        {
        	get
        	{
        		return _titlesingular;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_titlesingular,value);
        		
        		_titlesingular = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string TitleSingular_Search
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.TitleSingular_Search;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__TitleSingular_Search_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.TitleSingular_Search,value);
                cultureInfo.TitleSingular_Search = value;
            }
        }
        
        private string _titlesingular_search;
        public virtual string __TitleSingular_Search_cultureBase
        {
        	get
        	{
        		return _titlesingular_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_titlesingular_search,value);
        		
        		_titlesingular_search = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string TitlePlural
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.TitlePlural;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__TitlePlural_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.TitlePlural,value);
                cultureInfo.TitlePlural = value;
            }
        }
        
        private string _titleplural;
        public virtual string __TitlePlural_cultureBase
        {
        	get
        	{
        		return _titleplural;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_titleplural,value);
        		
        		_titleplural = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string TitlePlural_Search
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.TitlePlural_Search;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__TitlePlural_Search_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.TitlePlural_Search,value);
                cultureInfo.TitlePlural_Search = value;
            }
        }
        
        private string _titleplural_search;
        public virtual string __TitlePlural_Search_cultureBase
        {
        	get
        	{
        		return _titleplural_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_titleplural_search,value);
        		
        		_titleplural_search = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string TitleSEO
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.TitleSEO;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__TitleSEO_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.TitleSEO,value);
                cultureInfo.TitleSEO = value;
            }
        }
        
        private string _titleseo;
        public virtual string __TitleSEO_cultureBase
        {
        	get
        	{
        		return _titleseo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_titleseo,value);
        		
        		_titleseo = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Description
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Description;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Description_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Description,value);
                cultureInfo.Description = value;
            }
        }
        
        private string _description;
        public virtual string __Description_cultureBase
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		
        		_description = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string MetaKeywords
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.MetaKeywords;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__MetaKeywords_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.MetaKeywords,value);
                cultureInfo.MetaKeywords = value;
            }
        }
        
        private string _metakeywords;
        public virtual string __MetaKeywords_cultureBase
        {
        	get
        	{
        		return _metakeywords;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metakeywords,value);
        		
        		_metakeywords = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string MetaDescription
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.MetaDescription;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__MetaDescription_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.MetaDescription,value);
                cultureInfo.MetaDescription = value;
            }
        }
        
        private string _metadescription;
        public virtual string __MetaDescription_cultureBase
        {
        	get
        	{
        		return _metadescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metadescription,value);
        		
        		_metadescription = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private bool _candelete;
        
        public virtual bool CanDelete 
        {
        	get
        	{
        		return _candelete;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_candelete,value);
        		_candelete = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _comingsoon;
        
        public virtual bool ComingSoon 
        {
        	get
        	{
        		return _comingsoon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_comingsoon,value);
        		_comingsoon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _priorityall;
        
        public virtual int PriorityAll 
        {
        	get
        	{
        		return _priorityall;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_priorityall,value);
        		_priorityall = value;
        		
        	}
        }
        
/*
		public virtual long? BrandID
		{
		 	get 
		 	{
		 		return (this.Brand != null ? (long?)this.Brand.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.BrandModule.BrandBase _brand;
        public virtual BusinessLogic_v3.Modules.BrandModule.BrandBase Brand 
        {
        	get
        	{
        		return _brand;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_brand,value);
        		_brand = value;
        	}
        }
            
        BusinessLogic_v3.Modules.BrandModule.IBrandBase BusinessLogic_v3.Modules._AutoGen.ICategoryBaseAutoGen.Brand 
        {
            get
            {
            	return this.Brand;
            }
            set
            {
            	this.Brand = (BusinessLogic_v3.Modules.BrandModule.BrandBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _importreference;
        
        public virtual string ImportReference 
        {
        	get
        	{
        		return _importreference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_importreference,value);
        		_importreference = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _dontshowinnavigation;
        
        public virtual bool DontShowInNavigation 
        {
        	get
        	{
        		return _dontshowinnavigation;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dontshowinnavigation,value);
        		_dontshowinnavigation = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _iconfilename;
        
        public virtual string IconFilename 
        {
        	get
        	{
        		return _iconfilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_iconfilename,value);
        		_iconfilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _omitchildreninnavigation;
        
        public virtual bool OmitChildrenInNavigation 
        {
        	get
        	{
        		return _omitchildreninnavigation;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_omitchildreninnavigation,value);
        		_omitchildreninnavigation = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _metatitle;
        
        public virtual string MetaTitle 
        {
        	get
        	{
        		return _metatitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metatitle,value);
        		_metatitle = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _considerasrootnode;
        
        public virtual bool ConsiderAsRootNode 
        {
        	get
        	{
        		return _considerasrootnode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_considerasrootnode,value);
        		_considerasrootnode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _subtitle;
        
        public virtual string SubTitle 
        {
        	get
        	{
        		return _subtitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subtitle,value);
        		_subtitle = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __BackgroundImagesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.BackgroundImageModule.BackgroundImageBase,
        	BusinessLogic_v3.Modules.BackgroundImageModule.IBackgroundImageBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.BackgroundImageModule.BackgroundImageBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __BackgroundImagesCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.BackgroundImageModule.BackgroundImageBase> Collection
            {
                get { return _item.__collection__BackgroundImages; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.BackgroundImageModule.BackgroundImageBase>.SetLinkOnItem(BusinessLogic_v3.Modules.BackgroundImageModule.BackgroundImageBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.CategoryLink = value;
            }
        }
        
        private __BackgroundImagesCollectionInfo _BackgroundImages = null;
        public __BackgroundImagesCollectionInfo BackgroundImages
        {
            get
            {
                if (_BackgroundImages == null)
                    _BackgroundImages = new __BackgroundImagesCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _BackgroundImages;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.BackgroundImageModule.IBackgroundImageBase> ICategoryBaseAutoGen.BackgroundImages
        {
            get {  return this.BackgroundImages; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.BackgroundImageModule.BackgroundImageBase> __collection__BackgroundImages
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'BackgroundImageBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __Category_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase,
        	BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __Category_CultureInfosCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase> Collection
            {
                get { return _item.__collection__Category_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.Category = value;
            }
        }
        
        private __Category_CultureInfosCollectionInfo _Category_CultureInfos = null;
        public __Category_CultureInfosCollectionInfo Category_CultureInfos
        {
            get
            {
                if (_Category_CultureInfos == null)
                    _Category_CultureInfos = new __Category_CultureInfosCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _Category_CultureInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBase> ICategoryBaseAutoGen.Category_CultureInfos
        {
            get {  return this.Category_CultureInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase> __collection__Category_CultureInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'Category_CultureInfoBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ChildCategoryLinksCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase,
        	BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __ChildCategoryLinksCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase> Collection
            {
                get { return _item.__collection__ChildCategoryLinks; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>.SetLinkOnItem(BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.ParentCategory = value;
            }
        }
        
        private __ChildCategoryLinksCollectionInfo _ChildCategoryLinks = null;
        public __ChildCategoryLinksCollectionInfo ChildCategoryLinks
        {
            get
            {
                if (_ChildCategoryLinks == null)
                    _ChildCategoryLinks = new __ChildCategoryLinksCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _ChildCategoryLinks;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBase> ICategoryBaseAutoGen.ChildCategoryLinks
        {
            get {  return this.ChildCategoryLinks; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase> __collection__ChildCategoryLinks
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'Category_ParentChildLinkBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ParentCategoryLinksCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase,
        	BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __ParentCategoryLinksCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase> Collection
            {
                get { return _item.__collection__ParentCategoryLinks; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>.SetLinkOnItem(BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.ChildCategory = value;
            }
        }
        
        private __ParentCategoryLinksCollectionInfo _ParentCategoryLinks = null;
        public __ParentCategoryLinksCollectionInfo ParentCategoryLinks
        {
            get
            {
                if (_ParentCategoryLinks == null)
                    _ParentCategoryLinks = new __ParentCategoryLinksCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _ParentCategoryLinks;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBase> ICategoryBaseAutoGen.ParentCategoryLinks
        {
            get {  return this.ParentCategoryLinks; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase> __collection__ParentCategoryLinks
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'Category_ParentChildLinkBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __CategoryFeaturesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase,
        	BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __CategoryFeaturesCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase> Collection
            {
                get { return _item.__collection__CategoryFeatures; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.SetLinkOnItem(BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.Category = value;
            }
        }
        
        private __CategoryFeaturesCollectionInfo _CategoryFeatures = null;
        public __CategoryFeaturesCollectionInfo CategoryFeatures
        {
            get
            {
                if (_CategoryFeatures == null)
                    _CategoryFeatures = new __CategoryFeaturesCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _CategoryFeatures;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBase> ICategoryBaseAutoGen.CategoryFeatures
        {
            get {  return this.CategoryFeatures; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase> __collection__CategoryFeatures
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'CategoryFeatureBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __CategoryImagesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase,
        	BusinessLogic_v3.Modules.CategoryImageModule.ICategoryImageBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __CategoryImagesCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase> Collection
            {
                get { return _item.__collection__CategoryImages; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase>.SetLinkOnItem(BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.Category = value;
            }
        }
        
        private __CategoryImagesCollectionInfo _CategoryImages = null;
        public __CategoryImagesCollectionInfo CategoryImages
        {
            get
            {
                if (_CategoryImages == null)
                    _CategoryImages = new __CategoryImagesCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _CategoryImages;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.CategoryImageModule.ICategoryImageBase> ICategoryBaseAutoGen.CategoryImages
        {
            get {  return this.CategoryImages; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase> __collection__CategoryImages
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'CategoryImageBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __CategorySpecificationsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase,
        	BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __CategorySpecificationsCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase> Collection
            {
                get { return _item.__collection__CategorySpecifications; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>.SetLinkOnItem(BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.Category = value;
            }
        }
        
        private __CategorySpecificationsCollectionInfo _CategorySpecifications = null;
        public __CategorySpecificationsCollectionInfo CategorySpecifications
        {
            get
            {
                if (_CategorySpecifications == null)
                    _CategorySpecifications = new __CategorySpecificationsCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _CategorySpecifications;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBase> ICategoryBaseAutoGen.CategorySpecifications
        {
            get {  return this.CategorySpecifications; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase> __collection__CategorySpecifications
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'CategorySpecificationBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ClassifiedsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase,
        	BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __ClassifiedsCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase> Collection
            {
                get { return _item.__collection__Classifieds; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.Category = value;
            }
        }
        
        private __ClassifiedsCollectionInfo _Classifieds = null;
        public __ClassifiedsCollectionInfo Classifieds
        {
            get
            {
                if (_Classifieds == null)
                    _Classifieds = new __ClassifiedsCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _Classifieds;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase> ICategoryBaseAutoGen.Classifieds
        {
            get {  return this.Classifieds; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase> __collection__Classifieds
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ClassifiedBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EventsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.EventModule.EventBase,
        	BusinessLogic_v3.Modules.EventModule.IEventBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.EventModule.EventBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __EventsCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EventModule.EventBase> Collection
            {
                get { return _item.__collection__Events; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.EventModule.EventBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EventModule.EventBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.Category = value;
            }
        }
        
        private __EventsCollectionInfo _Events = null;
        public __EventsCollectionInfo Events
        {
            get
            {
                if (_Events == null)
                    _Events = new __EventsCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _Events;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EventModule.IEventBase> ICategoryBaseAutoGen.Events
        {
            get {  return this.Events; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EventModule.EventBase> __collection__Events
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EventBasicsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase,
        	BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __EventBasicsCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase> Collection
            {
                get { return _item.__collection__EventBasics; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.Category = value;
            }
        }
        
        private __EventBasicsCollectionInfo _EventBasics = null;
        public __EventBasicsCollectionInfo EventBasics
        {
            get
            {
                if (_EventBasics == null)
                    _EventBasics = new __EventBasicsCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _EventBasics;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBase> ICategoryBaseAutoGen.EventBasics
        {
            get {  return this.EventBasics; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase> __collection__EventBasics
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventBasicBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EventSubmissionsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase,
        	BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __EventSubmissionsCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase> Collection
            {
                get { return _item.__collection__EventSubmissions; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.Category = value;
            }
        }
        
        private __EventSubmissionsCollectionInfo _EventSubmissions = null;
        public __EventSubmissionsCollectionInfo EventSubmissions
        {
            get
            {
                if (_EventSubmissions == null)
                    _EventSubmissions = new __EventSubmissionsCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _EventSubmissions;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBase> ICategoryBaseAutoGen.EventSubmissions
        {
            get {  return this.EventSubmissions; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase> __collection__EventSubmissions
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventSubmissionBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ProductLinksCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CategoryModule.CategoryBase, 
        	BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase,
        	BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __ProductLinksCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase> Collection
            {
                get { return _item.__collection__ProductLinks; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase item, BusinessLogic_v3.Modules.CategoryModule.CategoryBase value)
            {
                item.Category = value;
            }
        }
        
        private __ProductLinksCollectionInfo _ProductLinks = null;
        public __ProductLinksCollectionInfo ProductLinks
        {
            get
            {
                if (_ProductLinks == null)
                    _ProductLinks = new __ProductLinksCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _ProductLinks;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBase> ICategoryBaseAutoGen.ProductLinks
        {
            get {  return this.ProductLinks; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase> __collection__ProductLinks
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductCategoryBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionManyToManyRightSide  
        
		public class __SubscribedMembersCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.CategoryModule.CategoryBase,
			BusinessLogic_v3.Modules.MemberModule.MemberBase,
			BusinessLogic_v3.Modules.MemberModule.IMemberBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.MemberModule.MemberBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __SubscribedMembersCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> Collection
            {
                get { return _item.__collection__SubscribedMembers; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase,BusinessLogic_v3.Modules.MemberModule.MemberBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.CategoryModule.CategoryBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.MemberModule.MemberBase item)
            {
                return item.SubscribedCategories;
                
            }


            #endregion
        }

        private __SubscribedMembersCollectionInfo _SubscribedMembers = null;
        public __SubscribedMembersCollectionInfo SubscribedMembers
        {
            get
            {
                if (_SubscribedMembers == null)
                    _SubscribedMembers = new __SubscribedMembersCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _SubscribedMembers;
            }
        }
        
           
        ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> ICategoryBaseAutoGen.SubscribedMembers
        {
            get {  return this.SubscribedMembers; }
        }
           

		protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> __collection__SubscribedMembers
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberBase' is not marked as UsedInProject"); }
        }





        
        //BaseClass_CollectionManyToManyRightSide  
        
		public class __ApplicableSpecialOffersCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.CategoryModule.CategoryBase,
			BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase,
			BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase, BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>
        {
            private BusinessLogic_v3.Modules.CategoryModule.CategoryBase _item = null;
            public __ApplicableSpecialOffersCollectionInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase> Collection
            {
                get { return _item.__collection__ApplicableSpecialOffers; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.CategoryModule.CategoryBase,BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.CategoryModule.CategoryBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase item)
            {
                return item.ApplicableCategories;
                
            }


            #endregion
        }

        private __ApplicableSpecialOffersCollectionInfo _ApplicableSpecialOffers = null;
        public __ApplicableSpecialOffersCollectionInfo ApplicableSpecialOffers
        {
            get
            {
                if (_ApplicableSpecialOffers == null)
                    _ApplicableSpecialOffers = new __ApplicableSpecialOffersCollectionInfo((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)this);
                return _ApplicableSpecialOffers;
            }
        }
        
           
        ICollectionManager<BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBase> ICategoryBaseAutoGen.ApplicableSpecialOffers
        {
            get {  return this.ApplicableSpecialOffers; }
        }
           

		protected virtual IEnumerable<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase> __collection__ApplicableSpecialOffers
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'SpecialOfferBase' is not marked as UsedInProject"); }
        }





        

    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
