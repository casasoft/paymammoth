using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.EventBasicModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.EventBasicModule.EventBasicBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.EventBasicModule.EventBasicBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBase item)
        {
        	return BusinessLogic_v3.Frontend.EventBasicModule.EventBasicBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventBasicBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<EventBasicBaseFrontend, BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBase>

    {
		
        
        protected EventBasicBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
