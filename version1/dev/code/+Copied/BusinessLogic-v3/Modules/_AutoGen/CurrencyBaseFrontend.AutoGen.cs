using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.CurrencyModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.CurrencyModule.CurrencyBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.CurrencyModule.CurrencyBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase item)
        {
        	return BusinessLogic_v3.Frontend.CurrencyModule.CurrencyBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CurrencyBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<CurrencyBaseFrontend, BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase>

    {
		
        
        protected CurrencyBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
