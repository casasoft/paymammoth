using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.AdvertHitModule;
using BusinessLogic_v3.Cms.AdvertHitModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class AdvertHitBaseCmsFactory_AutoGen : CmsFactoryBase<AdvertHitBaseCmsInfo, AdvertHitBase>
    {
       
       public new static AdvertHitBaseCmsFactory Instance
	    {
	         get
	         {
                 return (AdvertHitBaseCmsFactory)CmsFactoryBase<AdvertHitBaseCmsInfo, AdvertHitBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = AdvertHitBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "AdvertHit.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "AdvertHit";

            this.QueryStringParamID = "AdvertHitId";

            cmsInfo.TitlePlural = "Advert Hits";

            cmsInfo.TitleSingular =  "Advert Hit";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public AdvertHitBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "AdvertHit/";
			UsedInProject = AdvertHitBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
