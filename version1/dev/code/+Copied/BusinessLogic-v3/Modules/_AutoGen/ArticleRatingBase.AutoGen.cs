using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ArticleRatingModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ArticleRating")]
    public abstract class ArticleRatingBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IArticleRatingBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ArticleRatingBaseFactory Factory
        {
            get
            {
                return ArticleRatingBaseFactory.Instance; 
            }
        }    
        /*
        public static ArticleRatingBase CreateNewItem()
        {
            return (ArticleRatingBase)Factory.CreateNewItem();
        }

		public static ArticleRatingBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ArticleRatingBase, ArticleRatingBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ArticleRatingBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ArticleRatingBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ArticleRatingBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ArticleRatingBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ArticleRatingBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _ipaddress;
        
        public virtual string IPAddress 
        {
        	get
        	{
        		return _ipaddress;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ipaddress,value);
        		_ipaddress = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _date;
        
        public virtual DateTime Date 
        {
        	get
        	{
        		return _date;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_date,value);
        		_date = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _rating;
        
        public virtual double Rating 
        {
        	get
        	{
        		return _rating;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_rating,value);
        		_rating = value;
        		
        	}
        }
        
/*
		public virtual long? ArticleID
		{
		 	get 
		 	{
		 		return (this.Article != null ? (long?)this.Article.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ArticleModule.ArticleBase _article;
        public virtual BusinessLogic_v3.Modules.ArticleModule.ArticleBase Article 
        {
        	get
        	{
        		return _article;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_article,value);
        		_article = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase BusinessLogic_v3.Modules._AutoGen.IArticleRatingBaseAutoGen.Article 
        {
            get
            {
            	return this.Article;
            }
            set
            {
            	this.Article = (BusinessLogic_v3.Modules.ArticleModule.ArticleBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
