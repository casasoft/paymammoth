using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductVariationModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductVariationModule.ProductVariationBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductVariationModule.ProductVariationBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductVariationModule.ProductVariationBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductVariationBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductVariationBaseFrontend, BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase>

    {
		
        
        protected ProductVariationBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
