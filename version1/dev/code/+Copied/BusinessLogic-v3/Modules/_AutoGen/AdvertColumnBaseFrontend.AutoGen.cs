using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.AdvertColumnModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.AdvertColumnModule.AdvertColumnBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.AdvertColumnModule.IAdvertColumnBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.AdvertColumnModule.AdvertColumnBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.AdvertColumnModule.IAdvertColumnBase item)
        {
        	return BusinessLogic_v3.Frontend.AdvertColumnModule.AdvertColumnBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AdvertColumnBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<AdvertColumnBaseFrontend, BusinessLogic_v3.Modules.AdvertColumnModule.IAdvertColumnBase>

    {
		
        
        protected AdvertColumnBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
