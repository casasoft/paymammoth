using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.EventCategoryModule;                

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public abstract class EventCategoryBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IEventCategoryBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static EventCategoryBaseFactory Factory
        {
            get
            {
                return EventCategoryBaseFactory.Instance; 
            }
        }    
        /*
        public static EventCategoryBase CreateNewItem()
        {
            return (EventCategoryBase)Factory.CreateNewItem();
        }

		public static EventCategoryBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<EventCategoryBase, EventCategoryBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static EventCategoryBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static EventCategoryBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<EventCategoryBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<EventCategoryBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<EventCategoryBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EventsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase, 
        	BusinessLogic_v3.Modules.EventModule.EventBase,
        	BusinessLogic_v3.Modules.EventModule.IEventBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase, BusinessLogic_v3.Modules.EventModule.EventBase>
        {
            private BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase _item = null;
            public __EventsCollectionInfo(BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EventModule.EventBase> Collection
            {
                get { return _item.__collection__Events; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase, BusinessLogic_v3.Modules.EventModule.EventBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EventModule.EventBase item, BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase value)
            {
                item.EventCategory = value;
            }
        }
        
        private __EventsCollectionInfo _Events = null;
        public __EventsCollectionInfo Events
        {
            get
            {
                if (_Events == null)
                    _Events = new __EventsCollectionInfo((BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase)this);
                return _Events;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EventModule.IEventBase> IEventCategoryBaseAutoGen.Events
        {
            get {  return this.Events; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EventModule.EventBase> __collection__Events
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
