using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CmsUserModule;
using BusinessLogic_v3.Cms.CmsUserModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class CmsUserBaseCmsFactory_AutoGen : CmsFactoryBase<CmsUserBaseCmsInfo, CmsUserBase>
    {
       
       public new static CmsUserBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CmsUserBaseCmsFactory)CmsFactoryBase<CmsUserBaseCmsInfo, CmsUserBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = CmsUserBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CmsUser.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CmsUser";

            this.QueryStringParamID = "CmsUserId";

            cmsInfo.TitlePlural = "Cms Users";

            cmsInfo.TitleSingular =  "Cms User";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CmsUserBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "CmsUser/";
			UsedInProject = CmsUserBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
