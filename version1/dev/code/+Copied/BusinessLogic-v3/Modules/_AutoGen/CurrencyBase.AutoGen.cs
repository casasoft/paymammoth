using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.CurrencyModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Currency")]
    public abstract class CurrencyBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ICurrencyBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static CurrencyBaseFactory Factory
        {
            get
            {
                return CurrencyBaseFactory.Instance; 
            }
        }    
        /*
        public static CurrencyBase CreateNewItem()
        {
            return (CurrencyBase)Factory.CreateNewItem();
        }

		public static CurrencyBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<CurrencyBase, CurrencyBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static CurrencyBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static CurrencyBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<CurrencyBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<CurrencyBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<CurrencyBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _currencyisocode;
        
        public virtual string CurrencyISOCode 
        {
        	get
        	{
        		return _currencyisocode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_currencyisocode,value);
        		_currencyisocode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _symbol;
        
        public virtual string Symbol 
        {
        	get
        	{
        		return _symbol;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_symbol,value);
        		_symbol = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _exchangeratemultiplier;
        
        public virtual double ExchangeRateMultiplier 
        {
        	get
        	{
        		return _exchangeratemultiplier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_exchangeratemultiplier,value);
        		_exchangeratemultiplier = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __AffiliatePaymentInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, 
        	BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase,
        	BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>
        {
            private BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase _item = null;
            public __AffiliatePaymentInfosCollectionInfo(BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase> Collection
            {
                get { return _item.__collection__AffiliatePaymentInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase item, BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase value)
            {
                item.Currency = value;
            }
        }
        
        private __AffiliatePaymentInfosCollectionInfo _AffiliatePaymentInfos = null;
        public __AffiliatePaymentInfosCollectionInfo AffiliatePaymentInfos
        {
            get
            {
                if (_AffiliatePaymentInfos == null)
                    _AffiliatePaymentInfos = new __AffiliatePaymentInfosCollectionInfo((BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase)this);
                return _AffiliatePaymentInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBase> ICurrencyBaseAutoGen.AffiliatePaymentInfos
        {
            get {  return this.AffiliatePaymentInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase> __collection__AffiliatePaymentInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'AffiliatePaymentInfoBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __CultureDetailsesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, 
        	BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase,
        	BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>
        {
            private BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase _item = null;
            public __CultureDetailsesCollectionInfo(BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> Collection
            {
                get { return _item.__collection__CultureDetailses; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>.SetLinkOnItem(BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase item, BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase value)
            {
                item.DefaultCurrency = value;
            }
        }
        
        private __CultureDetailsesCollectionInfo _CultureDetailses = null;
        public __CultureDetailsesCollectionInfo CultureDetailses
        {
            get
            {
                if (_CultureDetailses == null)
                    _CultureDetailses = new __CultureDetailsesCollectionInfo((BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase)this);
                return _CultureDetailses;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase> ICurrencyBaseAutoGen.CultureDetailses
        {
            get {  return this.CultureDetailses; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> __collection__CultureDetailses
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'CultureDetailsBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __OrdersCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, 
        	BusinessLogic_v3.Modules.OrderModule.OrderBase,
        	BusinessLogic_v3.Modules.OrderModule.IOrderBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, BusinessLogic_v3.Modules.OrderModule.OrderBase>
        {
            private BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase _item = null;
            public __OrdersCollectionInfo(BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.OrderModule.OrderBase> Collection
            {
                get { return _item.__collection__Orders; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, BusinessLogic_v3.Modules.OrderModule.OrderBase>.SetLinkOnItem(BusinessLogic_v3.Modules.OrderModule.OrderBase item, BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase value)
            {
                item.OrderCurrency = value;
            }
        }
        
        private __OrdersCollectionInfo _Orders = null;
        public __OrdersCollectionInfo Orders
        {
            get
            {
                if (_Orders == null)
                    _Orders = new __OrdersCollectionInfo((BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase)this);
                return _Orders;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.OrderModule.IOrderBase> ICurrencyBaseAutoGen.Orders
        {
            get {  return this.Orders; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.OrderModule.OrderBase> __collection__Orders
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'OrderBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ShoppingCartsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, 
        	BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase,
        	BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase>
        {
            private BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase _item = null;
            public __ShoppingCartsCollectionInfo(BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase> Collection
            {
                get { return _item.__collection__ShoppingCarts; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase, BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase item, BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase value)
            {
                item.Currency = value;
            }
        }
        
        private __ShoppingCartsCollectionInfo _ShoppingCarts = null;
        public __ShoppingCartsCollectionInfo ShoppingCarts
        {
            get
            {
                if (_ShoppingCarts == null)
                    _ShoppingCarts = new __ShoppingCartsCollectionInfo((BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase)this);
                return _ShoppingCarts;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBase> ICurrencyBaseAutoGen.ShoppingCarts
        {
            get {  return this.ShoppingCarts; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase> __collection__ShoppingCarts
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ShoppingCartBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
