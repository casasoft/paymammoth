using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductVariationModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ProductVariation")]
    public abstract class ProductVariationBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductVariationBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
        

			var cultureInfo = __getCurrentCultureInfo(); //since this is a new item, it is imperative that the culture is preloaded
            
            
            base.initPropertiesForNewItem();
        }
    	
  
    	public BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase) __getCultureInfoByLanguage(lang);
        }

     	public static ProductVariationBaseFactory Factory
        {
            get
            {
                return ProductVariationBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductVariationBase CreateNewItem()
        {
            return (ProductVariationBase)Factory.CreateNewItem();
        }

		public static ProductVariationBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductVariationBase, ProductVariationBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductVariationBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductVariationBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductVariationBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductVariationBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductVariationBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _supplierrefcode;
        
        public virtual string SupplierRefCode 
        {
        	get
        	{
        		return _supplierrefcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_supplierrefcode,value);
        		_supplierrefcode = value;
        		
        	}
        }
        
/*
		public virtual long? ProductID
		{
		 	get 
		 	{
		 		return (this.Product != null ? (long?)this.Product.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _product;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase Product 
        {
        	get
        	{
        		return _product;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_product,value);
        		_product = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.IProductVariationBaseAutoGen.Product 
        {
            get
            {
            	return this.Product;
            }
            set
            {
            	this.Product = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _barcode;
        
        public virtual string Barcode 
        {
        	get
        	{
        		return _barcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_barcode,value);
        		_barcode = value;
        		
        	}
        }
        
/*
		public virtual long? ColourID
		{
		 	get 
		 	{
		 		return (this.Colour != null ? (long?)this.Colour.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase _colour;
        public virtual BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase Colour 
        {
        	get
        	{
        		return _colour;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_colour,value);
        		_colour = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductVariationColourModule.IProductVariationColourBase BusinessLogic_v3.Modules._AutoGen.IProductVariationBaseAutoGen.Colour 
        {
            get
            {
            	return this.Colour;
            }
            set
            {
            	this.Colour = (BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Size
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Size;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Size_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Size,value);
                cultureInfo.Size = value;
            }
        }
        
        private string _size;
        public virtual string __Size_cultureBase
        {
        	get
        	{
        		return _size;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_size,value);
        		
        		_size = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private int _quantity;
        
        public virtual int Quantity 
        {
        	get
        	{
        		return _quantity;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_quantity,value);
        		_quantity = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _reorderamount;
        
        public virtual int ReorderAmount 
        {
        	get
        	{
        		return _reorderamount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_reorderamount,value);
        		_reorderamount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _importreference;
        
        public virtual string ImportReference 
        {
        	get
        	{
        		return _importreference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_importreference,value);
        		_importreference = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __OrderItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, 
        	BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase,
        	BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>
        {
            private BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase _item = null;
            public __OrderItemsCollectionInfo(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase> Collection
            {
                get { return _item.__collection__OrderItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase item, BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase value)
            {
                item.LinkedProductVariation = value;
            }
        }
        
        private __OrderItemsCollectionInfo _OrderItems = null;
        public __OrderItemsCollectionInfo OrderItems
        {
            get
            {
                if (_OrderItems == null)
                    _OrderItems = new __OrderItemsCollectionInfo((BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase)this);
                return _OrderItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase> IProductVariationBaseAutoGen.OrderItems
        {
            get {  return this.OrderItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase> __collection__OrderItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'OrderItemBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ProductVariation_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, 
        	BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase,
        	BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase>
        {
            private BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase _item = null;
            public __ProductVariation_CultureInfosCollectionInfo(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase> Collection
            {
                get { return _item.__collection__ProductVariation_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase item, BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase value)
            {
                item.ProductVariation = value;
            }
        }
        
        private __ProductVariation_CultureInfosCollectionInfo _ProductVariation_CultureInfos = null;
        public __ProductVariation_CultureInfosCollectionInfo ProductVariation_CultureInfos
        {
            get
            {
                if (_ProductVariation_CultureInfos == null)
                    _ProductVariation_CultureInfos = new __ProductVariation_CultureInfosCollectionInfo((BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase)this);
                return _ProductVariation_CultureInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfoBase> IProductVariationBaseAutoGen.ProductVariation_CultureInfos
        {
            get {  return this.ProductVariation_CultureInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase> __collection__ProductVariation_CultureInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductVariation_CultureInfoBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __MediaItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, 
        	BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase,
        	BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase>
        {
            private BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase _item = null;
            public __MediaItemsCollectionInfo(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase> Collection
            {
                get { return _item.__collection__MediaItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase item, BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase value)
            {
                item.ProductVariation = value;
            }
        }
        
        private __MediaItemsCollectionInfo _MediaItems = null;
        public __MediaItemsCollectionInfo MediaItems
        {
            get
            {
                if (_MediaItems == null)
                    _MediaItems = new __MediaItemsCollectionInfo((BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase)this);
                return _MediaItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBase> IProductVariationBaseAutoGen.MediaItems
        {
            get {  return this.MediaItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase> __collection__MediaItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductVariationMediaItemBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ShoppingCartItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, 
        	BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase,
        	BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>
        {
            private BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase _item = null;
            public __ShoppingCartItemsCollectionInfo(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase> Collection
            {
                get { return _item.__collection__ShoppingCartItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase, BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase item, BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase value)
            {
                item.LinkedProductVariation = value;
            }
        }
        
        private __ShoppingCartItemsCollectionInfo _ShoppingCartItems = null;
        public __ShoppingCartItemsCollectionInfo ShoppingCartItems
        {
            get
            {
                if (_ShoppingCartItems == null)
                    _ShoppingCartItems = new __ShoppingCartItemsCollectionInfo((BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase)this);
                return _ShoppingCartItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase> IProductVariationBaseAutoGen.ShoppingCartItems
        {
            get {  return this.ShoppingCartItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase> __collection__ShoppingCartItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ShoppingCartItemBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
