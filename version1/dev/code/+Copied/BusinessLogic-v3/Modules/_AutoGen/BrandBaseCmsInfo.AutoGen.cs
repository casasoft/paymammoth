using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Cms.BrandModule;
using BusinessLogic_v3.Frontend.BrandModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class BrandBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.BrandModule.BrandBase>
    {
		public BrandBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.BrandModule.BrandBase dbItem)
            : base(BusinessLogic_v3.Cms.BrandModule.BrandBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new BrandBaseFrontend FrontendItem
        {
            get { return (BrandBaseFrontend)base.FrontendItem; }

        }
        public new BrandBase DbItem
        {
            get
            {
                return (BrandBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo BrandLogoFilename { get; protected set; }

        public CmsPropertyInfo IsFeatured { get; protected set; }

        public CmsPropertyInfo Url { get; protected set; }

        public CmsPropertyInfo ImportReference { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo Brand_CultureInfos { get; protected set; }
        

        public CmsCollectionInfo Products { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.Brand_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BrandModule.BrandBase>.GetPropertyBySelector(item => item.Brand_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase>.GetPropertyBySelector(item => item.Brand)));
		
		//InitCollectionBaseOneToMany
		this.Products = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BrandModule.BrandBase>.GetPropertyBySelector(item => item.Products),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductModule.ProductBase>.GetPropertyBySelector(item => item.Brand)));


			base.initBasicFields();
          
        }

    }
}
