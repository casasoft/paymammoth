using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.MemberLoginInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.MemberLoginInfoModule.MemberLoginInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.MemberLoginInfoModule.MemberLoginInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.MemberLoginInfoModule.MemberLoginInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberLoginInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<MemberLoginInfoBaseFrontend, BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBase>

    {
		
        
        protected MemberLoginInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
