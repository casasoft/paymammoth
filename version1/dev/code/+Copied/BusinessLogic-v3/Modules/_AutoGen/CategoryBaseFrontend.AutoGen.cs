using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.CategoryModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.CategoryModule.CategoryBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.CategoryModule.ICategoryBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.CategoryModule.CategoryBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.CategoryModule.ICategoryBase item)
        {
        	return BusinessLogic_v3.Frontend.CategoryModule.CategoryBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CategoryBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<CategoryBaseFrontend, BusinessLogic_v3.Modules.CategoryModule.ICategoryBase>

    {
		
        
        protected CategoryBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
