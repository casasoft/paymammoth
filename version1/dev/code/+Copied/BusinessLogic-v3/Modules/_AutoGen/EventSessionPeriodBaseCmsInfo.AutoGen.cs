using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EventSessionPeriodModule;
using BusinessLogic_v3.Cms.EventSessionPeriodModule;
using BusinessLogic_v3.Frontend.EventSessionPeriodModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventSessionPeriodBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase>
    {
		public EventSessionPeriodBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase dbItem)
            : base(BusinessLogic_v3.Cms.EventSessionPeriodModule.EventSessionPeriodBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventSessionPeriodBaseFrontend FrontendItem
        {
            get { return (EventSessionPeriodBaseFrontend)base.FrontendItem; }

        }
        public new EventSessionPeriodBase DbItem
        {
            get
            {
                return (EventSessionPeriodBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo EventSession { get; protected set; }

        public CmsPropertyInfo StartDate { get; protected set; }

        public CmsPropertyInfo EndDate { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
