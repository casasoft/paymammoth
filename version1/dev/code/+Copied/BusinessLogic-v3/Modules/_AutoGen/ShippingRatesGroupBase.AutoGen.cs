using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ShippingRatesGroupModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ShippingRatesGroup")]
    public abstract class ShippingRatesGroupBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IShippingRatesGroupBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ShippingRatesGroupBaseFactory Factory
        {
            get
            {
                return ShippingRatesGroupBaseFactory.Instance; 
            }
        }    
        /*
        public static ShippingRatesGroupBase CreateNewItem()
        {
            return (ShippingRatesGroupBase)Factory.CreateNewItem();
        }

		public static ShippingRatesGroupBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ShippingRatesGroupBase, ShippingRatesGroupBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ShippingRatesGroupBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ShippingRatesGroupBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ShippingRatesGroupBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ShippingRatesGroupBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ShippingRatesGroupBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _countriesapplicable;
        
        public virtual string CountriesApplicable 
        {
        	get
        	{
        		return _countriesapplicable;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_countriesapplicable,value);
        		_countriesapplicable = value;
        		
        	}
        }
        
/*
		public virtual long? ShippingMethodID
		{
		 	get 
		 	{
		 		return (this.ShippingMethod != null ? (long?)this.ShippingMethod.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase _shippingmethod;
        public virtual BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase ShippingMethod 
        {
        	get
        	{
        		return _shippingmethod;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shippingmethod,value);
        		_shippingmethod = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ShippingMethodModule.IShippingMethodBase BusinessLogic_v3.Modules._AutoGen.IShippingRatesGroupBaseAutoGen.ShippingMethod 
        {
            get
            {
            	return this.ShippingMethod;
            }
            set
            {
            	this.ShippingMethod = (BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _appliestorestoftheworld;
        
        public virtual bool AppliesToRestOfTheWorld 
        {
        	get
        	{
        		return _appliestorestoftheworld;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_appliestorestoftheworld,value);
        		_appliestorestoftheworld = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ShippingRatePricesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase, 
        	BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase,
        	BusinessLogic_v3.Modules.ShippingRatePriceModule.IShippingRatePriceBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase, BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase>
        {
            private BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase _item = null;
            public __ShippingRatePricesCollectionInfo(BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase> Collection
            {
                get { return _item.__collection__ShippingRatePrices; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase, BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase item, BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase value)
            {
                item.ShippingRatesGroup = value;
            }
        }
        
        private __ShippingRatePricesCollectionInfo _ShippingRatePrices = null;
        public __ShippingRatePricesCollectionInfo ShippingRatePrices
        {
            get
            {
                if (_ShippingRatePrices == null)
                    _ShippingRatePrices = new __ShippingRatePricesCollectionInfo((BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase)this);
                return _ShippingRatePrices;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ShippingRatePriceModule.IShippingRatePriceBase> IShippingRatesGroupBaseAutoGen.ShippingRatePrices
        {
            get {  return this.ShippingRatePrices; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase> __collection__ShippingRatePrices
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ShippingRatePriceBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
