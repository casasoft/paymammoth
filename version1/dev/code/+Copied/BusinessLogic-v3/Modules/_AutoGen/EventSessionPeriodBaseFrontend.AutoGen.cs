using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.EventSessionPeriodModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.EventSessionPeriodModule.EventSessionPeriodBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.EventSessionPeriodModule.IEventSessionPeriodBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.EventSessionPeriodModule.EventSessionPeriodBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.EventSessionPeriodModule.IEventSessionPeriodBase item)
        {
        	return BusinessLogic_v3.Frontend.EventSessionPeriodModule.EventSessionPeriodBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventSessionPeriodBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<EventSessionPeriodBaseFrontend, BusinessLogic_v3.Modules.EventSessionPeriodModule.IEventSessionPeriodBase>

    {
		
        
        protected EventSessionPeriodBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
