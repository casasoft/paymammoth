using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.BannerModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.BannerModule.BannerBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.BannerModule.IBannerBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.BannerModule.BannerBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.BannerModule.IBannerBase item)
        {
        	return BusinessLogic_v3.Frontend.BannerModule.BannerBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class BannerBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<BannerBaseFrontend, BusinessLogic_v3.Modules.BannerModule.IBannerBase>

    {
		
        
        protected BannerBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
