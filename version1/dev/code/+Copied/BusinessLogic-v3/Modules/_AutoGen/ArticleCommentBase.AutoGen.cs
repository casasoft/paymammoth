using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ArticleCommentModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ArticleComment")]
    public abstract class ArticleCommentBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IArticleCommentBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ArticleCommentBaseFactory Factory
        {
            get
            {
                return ArticleCommentBaseFactory.Instance; 
            }
        }    
        /*
        public static ArticleCommentBase CreateNewItem()
        {
            return (ArticleCommentBase)Factory.CreateNewItem();
        }

		public static ArticleCommentBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ArticleCommentBase, ArticleCommentBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ArticleCommentBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ArticleCommentBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ArticleCommentBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ArticleCommentBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ArticleCommentBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _email;
        
        public virtual string Email 
        {
        	get
        	{
        		return _email;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_email,value);
        		_email = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _comment;
        
        public virtual string Comment 
        {
        	get
        	{
        		return _comment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_comment,value);
        		_comment = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _ipaddress;
        
        public virtual string IPAddress 
        {
        	get
        	{
        		return _ipaddress;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ipaddress,value);
        		_ipaddress = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _postedon;
        
        public virtual DateTime PostedOn 
        {
        	get
        	{
        		return _postedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_postedon,value);
        		_postedon = value;
        		
        	}
        }
        
/*
		public virtual long? ArticleID
		{
		 	get 
		 	{
		 		return (this.Article != null ? (long?)this.Article.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ArticleModule.ArticleBase _article;
        public virtual BusinessLogic_v3.Modules.ArticleModule.ArticleBase Article 
        {
        	get
        	{
        		return _article;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_article,value);
        		_article = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase BusinessLogic_v3.Modules._AutoGen.IArticleCommentBaseAutoGen.Article 
        {
            get
            {
            	return this.Article;
            }
            set
            {
            	this.Article = (BusinessLogic_v3.Modules.ArticleModule.ArticleBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _author;
        
        public virtual string Author 
        {
        	get
        	{
        		return _author;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_author,value);
        		_author = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _company;
        
        public virtual string Company 
        {
        	get
        	{
        		return _company;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_company,value);
        		_company = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isapproved;
        
        public virtual bool IsApproved 
        {
        	get
        	{
        		return _isapproved;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isapproved,value);
        		_isapproved = value;
        		
        	}
        }
        
/*
		public virtual long? ReplyToID
		{
		 	get 
		 	{
		 		return (this.ReplyTo != null ? (long?)this.ReplyTo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase _replyto;
        public virtual BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase ReplyTo 
        {
        	get
        	{
        		return _replyto;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_replyto,value);
        		_replyto = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase BusinessLogic_v3.Modules._AutoGen.IArticleCommentBaseAutoGen.ReplyTo 
        {
            get
            {
            	return this.ReplyTo;
            }
            set
            {
            	this.ReplyTo = (BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __RepliesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase, 
        	BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase,
        	BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase, BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase>
        {
            private BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase _item = null;
            public __RepliesCollectionInfo(BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase> Collection
            {
                get { return _item.__collection__Replies; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase, BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase item, BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase value)
            {
                item.ReplyTo = value;
            }
        }
        
        private __RepliesCollectionInfo _Replies = null;
        public __RepliesCollectionInfo Replies
        {
            get
            {
                if (_Replies == null)
                    _Replies = new __RepliesCollectionInfo((BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase)this);
                return _Replies;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase> IArticleCommentBaseAutoGen.Replies
        {
            get {  return this.Replies; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase> __collection__Replies
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ArticleCommentBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
