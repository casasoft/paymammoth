using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ContentTagModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class ContentTagBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DB.BaseDbFactory<ContentTagBase, IContentTagBase>, IContentTagBaseFactoryAutoGen
    
    {
    
        public ContentTagBaseFactoryAutoGen()
        {
        	
            
        }
        

        
        
        public new static ContentTagBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<ContentTagBaseFactory>(null);
            }
        }
        
		public IQueryOver<ContentTagBase, ContentTagBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	
        	var q = session.QueryOver<ContentTagBase>();
            qParams.FillQueryOver(q);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
