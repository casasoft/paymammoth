using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductVariationModule;
using BusinessLogic_v3.Cms.ProductVariationModule;
using BusinessLogic_v3.Frontend.ProductVariationModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductVariationBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>
    {
		public ProductVariationBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase dbItem)
            : base(BusinessLogic_v3.Cms.ProductVariationModule.ProductVariationBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductVariationBaseFrontend FrontendItem
        {
            get { return (ProductVariationBaseFrontend)base.FrontendItem; }

        }
        public new ProductVariationBase DbItem
        {
            get
            {
                return (ProductVariationBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ReferenceCode { get; protected set; }

        public CmsPropertyInfo SupplierRefCode { get; protected set; }

        public CmsPropertyInfo Product { get; protected set; }

        public CmsPropertyInfo Barcode { get; protected set; }

        public CmsPropertyInfo Colour { get; protected set; }

        public CmsPropertyInfo Size { get; protected set; }

        public CmsPropertyInfo Quantity { get; protected set; }

        public CmsPropertyInfo ReorderAmount { get; protected set; }

        public CmsPropertyInfo ImportReference { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo OrderItems { get; protected set; }
        

        public CmsCollectionInfo PaymentTransactions { get; protected set; }
        

        public CmsCollectionInfo ProductVariation_CultureInfos { get; protected set; }
        

        public CmsCollectionInfo MediaItems { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.OrderItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.OrderItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector(item => item.LinkedProductVariation)));
		
		//InitCollectionBaseOneToMany
		this.PaymentTransactions = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.PaymentTransactions),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.PaymentTransactionModule.PaymentTransactionBase>.GetPropertyBySelector(item => item.LinkedItem)));
		
		//InitCollectionBaseOneToMany
		this.ProductVariation_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.ProductVariation_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase>.GetPropertyBySelector(item => item.ProductVariation)));
		
		//InitCollectionBaseOneToMany
		this.MediaItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.MediaItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase>.GetPropertyBySelector(item => item.ProductVariation)));


			base.initBasicFields();
          
        }

    }
}
