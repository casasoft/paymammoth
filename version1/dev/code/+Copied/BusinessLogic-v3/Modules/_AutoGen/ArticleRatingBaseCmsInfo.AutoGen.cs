using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ArticleRatingModule;
using BusinessLogic_v3.Cms.ArticleRatingModule;
using BusinessLogic_v3.Frontend.ArticleRatingModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ArticleRatingBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase>
    {
		public ArticleRatingBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase dbItem)
            : base(BusinessLogic_v3.Cms.ArticleRatingModule.ArticleRatingBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ArticleRatingBaseFrontend FrontendItem
        {
            get { return (ArticleRatingBaseFrontend)base.FrontendItem; }

        }
        public new ArticleRatingBase DbItem
        {
            get
            {
                return (ArticleRatingBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo IPAddress { get; protected set; }

        public CmsPropertyInfo Date { get; protected set; }

        public CmsPropertyInfo Rating { get; protected set; }

        public CmsPropertyInfo Article { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
