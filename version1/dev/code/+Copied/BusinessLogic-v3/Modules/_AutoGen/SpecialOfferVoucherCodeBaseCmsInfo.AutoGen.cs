using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;
using BusinessLogic_v3.Cms.SpecialOfferVoucherCodeModule;
using BusinessLogic_v3.Frontend.SpecialOfferVoucherCodeModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class SpecialOfferVoucherCodeBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase>
    {
		public SpecialOfferVoucherCodeBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase dbItem)
            : base(BusinessLogic_v3.Cms.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new SpecialOfferVoucherCodeBaseFrontend FrontendItem
        {
            get { return (SpecialOfferVoucherCodeBaseFrontend)base.FrontendItem; }

        }
        public new SpecialOfferVoucherCodeBase DbItem
        {
            get
            {
                return (SpecialOfferVoucherCodeBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo SpecialOffer { get; protected set; }

        public CmsPropertyInfo QuantityLeft { get; protected set; }

        public CmsPropertyInfo VoucherCode { get; protected set; }

        public CmsPropertyInfo CreatedOn { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo OrderItems { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.OrderItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase>.GetPropertyBySelector(item => item.OrderItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector(item => item.SpecialOfferVoucherCode)));


			base.initBasicFields();
          
        }

    }
}
