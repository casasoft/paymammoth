using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.EventCategoryModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class EventCategoryBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<EventCategoryBase, IEventCategoryBase>, IEventCategoryBaseFactoryAutoGen
    
    {
    
        public EventCategoryBaseFactoryAutoGen()
        {
        	
            
        }
        
		static EventCategoryBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static EventCategoryBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<EventCategoryBaseFactory>(null);
            }
        }
        
		public IQueryOver<EventCategoryBase, EventCategoryBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<EventCategoryBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
