using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;
using BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule;


namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public interface ISpecialOffer_CultureInfoBaseFactoryAutoGen : IBaseDbFactory<BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.ISpecialOffer_CultureInfoBase>
    {
    
    }
}
