using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.AffiliatePaymentInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.AffiliatePaymentInfoModule.AffiliatePaymentInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.AffiliatePaymentInfoModule.AffiliatePaymentInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.AffiliatePaymentInfoModule.AffiliatePaymentInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AffiliatePaymentInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<AffiliatePaymentInfoBaseFrontend, BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBase>

    {
		
        
        protected AffiliatePaymentInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
