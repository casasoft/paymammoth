using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Cms.BrandModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class BrandBaseCmsFactory_AutoGen : CmsFactoryBase<BrandBaseCmsInfo, BrandBase>
    {
       
       public new static BrandBaseCmsFactory Instance
	    {
	         get
	         {
                 return (BrandBaseCmsFactory)CmsFactoryBase<BrandBaseCmsInfo, BrandBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = BrandBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Brand.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Brand";

            this.QueryStringParamID = "BrandId";

            cmsInfo.TitlePlural = "Brands";

            cmsInfo.TitleSingular =  "Brand";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public BrandBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Brand/";
			UsedInProject = BrandBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
