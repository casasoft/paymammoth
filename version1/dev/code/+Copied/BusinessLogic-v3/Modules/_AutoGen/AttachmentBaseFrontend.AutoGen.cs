using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.AttachmentModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.AttachmentModule.AttachmentBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.AttachmentModule.AttachmentBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBase item)
        {
        	return BusinessLogic_v3.Frontend.AttachmentModule.AttachmentBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AttachmentBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<AttachmentBaseFrontend, BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBase>

    {
		
        
        protected AttachmentBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
