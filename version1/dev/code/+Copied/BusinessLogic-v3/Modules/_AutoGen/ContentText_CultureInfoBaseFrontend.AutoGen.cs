using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ContentText_CultureInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ContentText_CultureInfoModule.ContentText_CultureInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ContentText_CultureInfoModule.ContentText_CultureInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.ContentText_CultureInfoModule.ContentText_CultureInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentText_CultureInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ContentText_CultureInfoBaseFrontend, BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBase>

    {
		
        
        protected ContentText_CultureInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
