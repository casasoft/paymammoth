using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.PopularSearchModule;
using BusinessLogic_v3.Cms.PopularSearchModule;
using BusinessLogic_v3.Frontend.PopularSearchModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class PopularSearchBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.PopularSearchModule.PopularSearchBase>
    {
		public PopularSearchBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.PopularSearchModule.PopularSearchBase dbItem)
            : base(BusinessLogic_v3.Cms.PopularSearchModule.PopularSearchBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new PopularSearchBaseFrontend FrontendItem
        {
            get { return (PopularSearchBaseFrontend)base.FrontendItem; }

        }
        public new PopularSearchBase DbItem
        {
            get
            {
                return (PopularSearchBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo SearchURL { get; protected set; }

        public CmsPropertyInfo HtmlText { get; protected set; }

        public CmsPropertyInfo HideFromFrontend { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
