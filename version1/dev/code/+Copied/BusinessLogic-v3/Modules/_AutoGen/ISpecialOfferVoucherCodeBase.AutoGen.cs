using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface ISpecialOfferVoucherCodeBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBase SpecialOffer {get; set; }

		//Iproperty_normal
        int QuantityLeft { get; set; }
		//Iproperty_normal
        string VoucherCode { get; set; }
		//Iproperty_normal
        DateTime CreatedOn { get; set; }
		//Iproperty_normal
        DateTime? DateFrom { get; set; }
		//Iproperty_normal
        DateTime? DateTo { get; set; }
		//Iproperty_normal
        bool IsNotLimitedByQuantity { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
