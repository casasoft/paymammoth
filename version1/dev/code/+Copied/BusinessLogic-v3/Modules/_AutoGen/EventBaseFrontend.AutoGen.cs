using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.EventModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.EventModule.EventBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.EventModule.IEventBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.EventModule.EventBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.EventModule.IEventBase item)
        {
        	return BusinessLogic_v3.Frontend.EventModule.EventBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<EventBaseFrontend, BusinessLogic_v3.Modules.EventModule.IEventBase>

    {
		
        
        protected EventBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
