using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ArticleRatingModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ArticleRatingModule.ArticleRatingBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ArticleRatingModule.ArticleRatingBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBase item)
        {
        	return BusinessLogic_v3.Frontend.ArticleRatingModule.ArticleRatingBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ArticleRatingBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ArticleRatingBaseFrontend, BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBase>

    {
		
        
        protected ArticleRatingBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
