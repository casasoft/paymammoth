using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.MemberModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.MemberModule.MemberBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.MemberModule.IMemberBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.MemberModule.MemberBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.MemberModule.IMemberBase item)
        {
        	return BusinessLogic_v3.Frontend.MemberModule.MemberBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<MemberBaseFrontend, BusinessLogic_v3.Modules.MemberModule.IMemberBase>

    {
		
        
        protected MemberBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
