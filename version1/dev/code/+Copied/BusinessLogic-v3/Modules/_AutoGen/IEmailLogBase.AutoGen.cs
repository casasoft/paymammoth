using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.EmailLogModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IEmailLogBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        DateTime DateTime { get; set; }
		//Iproperty_normal
        string FromEmail { get; set; }
		//Iproperty_normal
        string FromName { get; set; }
		//Iproperty_normal
        string ToEmail { get; set; }
		//Iproperty_normal
        string ToName { get; set; }
		//Iproperty_normal
        string ReplyToEmail { get; set; }
		//Iproperty_normal
        string ReplyToName { get; set; }
		//Iproperty_normal
        bool Success { get; set; }
		//Iproperty_normal
        string Subject { get; set; }
		//Iproperty_normal
        string PlainText { get; set; }
		//Iproperty_normal
        string HtmlText { get; set; }
		//Iproperty_normal
        string Attachements { get; set; }
		//Iproperty_normal
        string Resources { get; set; }
		//Iproperty_normal
        string Host { get; set; }
		//Iproperty_normal
        string Port { get; set; }
		//Iproperty_normal
        string User { get; set; }
		//Iproperty_normal
        string Pass { get; set; }
		//Iproperty_normal
        bool UsesSecureConnection { get; set; }
		//Iproperty_normal
        string ResultDetails { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
