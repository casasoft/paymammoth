using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductVariationColourModule;
using BusinessLogic_v3.Cms.ProductVariationColourModule;
using BusinessLogic_v3.Frontend.ProductVariationColourModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductVariationColourBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase>
    {
		public ProductVariationColourBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase dbItem)
            : base(BusinessLogic_v3.Cms.ProductVariationColourModule.ProductVariationColourBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductVariationColourBaseFrontend FrontendItem
        {
            get { return (ProductVariationColourBaseFrontend)base.FrontendItem; }

        }
        public new ProductVariationColourBase DbItem
        {
            get
            {
                return (ProductVariationColourBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo HexColor { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ProductVariations { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ProductVariations = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase>.GetPropertyBySelector(item => item.ProductVariations),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.Colour)));


			base.initBasicFields();
          
        }

    }
}
