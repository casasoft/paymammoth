using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.Article_CultureInfoModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IArticle_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase CultureInfo {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase Article {get; set; }

		//Iproperty_normal
        string HtmlText { get; set; }
		//Iproperty_normal
        string HtmlText_Search { get; set; }
		//Iproperty_normal
        string MetaKeywords { get; set; }
		//Iproperty_normal
        string MetaKeywords_Search { get; set; }
		//Iproperty_normal
        string MetaDescription { get; set; }
		//Iproperty_normal
        string MetaDescription_Search { get; set; }
		//Iproperty_normal
        string PageTitle { get; set; }
		//Iproperty_normal
        string PageTitle_Search { get; set; }
		//Iproperty_normal
        int? DialogWidth { get; set; }
		//Iproperty_normal
        int? DialogHeight { get; set; }
		//Iproperty_normal
        string SubTitle { get; set; }
		//Iproperty_normal
        string _ComputedURL { get; set; }
		//Iproperty_normal
        string _Computed_ParentArticleNames { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
