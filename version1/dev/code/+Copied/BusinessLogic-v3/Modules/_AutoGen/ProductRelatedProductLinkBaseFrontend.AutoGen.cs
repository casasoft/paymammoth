using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductRelatedProductLinkModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductRelatedProductLinkModule.ProductRelatedProductLinkBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.IProductRelatedProductLinkBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductRelatedProductLinkModule.ProductRelatedProductLinkBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.IProductRelatedProductLinkBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductRelatedProductLinkModule.ProductRelatedProductLinkBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductRelatedProductLinkBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductRelatedProductLinkBaseFrontend, BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.IProductRelatedProductLinkBase>

    {
		
        
        protected ProductRelatedProductLinkBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
