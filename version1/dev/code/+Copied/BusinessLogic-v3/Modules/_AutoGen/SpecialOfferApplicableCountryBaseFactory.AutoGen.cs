using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class SpecialOfferApplicableCountryBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DB.BaseDbFactory<SpecialOfferApplicableCountryBase, ISpecialOfferApplicableCountryBase>, ISpecialOfferApplicableCountryBaseFactoryAutoGen
    
    {
    
        public SpecialOfferApplicableCountryBaseFactoryAutoGen()
        {
        	
            
        }
        
		static SpecialOfferApplicableCountryBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static SpecialOfferApplicableCountryBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<SpecialOfferApplicableCountryBaseFactory>(null);
            }
        }
        
		public IQueryOver<SpecialOfferApplicableCountryBase, SpecialOfferApplicableCountryBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<SpecialOfferApplicableCountryBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
