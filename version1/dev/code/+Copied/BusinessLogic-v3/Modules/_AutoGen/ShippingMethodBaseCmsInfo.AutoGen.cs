using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ShippingMethodModule;
using BusinessLogic_v3.Cms.ShippingMethodModule;
using BusinessLogic_v3.Frontend.ShippingMethodModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ShippingMethodBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase>
    {
		public ShippingMethodBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase dbItem)
            : base(BusinessLogic_v3.Cms.ShippingMethodModule.ShippingMethodBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ShippingMethodBaseFrontend FrontendItem
        {
            get { return (ShippingMethodBaseFrontend)base.FrontendItem; }

        }
        public new ShippingMethodBase DbItem
        {
            get
            {
                return (ShippingMethodBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo MinimumOrderAmount { get; protected set; }

        public CmsPropertyInfo ExtraChargeAmountIfMinimumNotReached { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo Orders { get; protected set; }
        

        public CmsCollectionInfo ShippingRatesGroups { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.Orders = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase>.GetPropertyBySelector(item => item.Orders),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector(item => item.ShippingMethod)));
		
		//InitCollectionBaseOneToMany
		this.ShippingRatesGroups = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase>.GetPropertyBySelector(item => item.ShippingRatesGroups),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase>.GetPropertyBySelector(item => item.ShippingMethod)));


			base.initBasicFields();
          
        }

    }
}
