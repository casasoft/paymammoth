using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.MemberReferralModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.MemberReferralModule.MemberReferralBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.MemberReferralModule.MemberReferralBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBase item)
        {
        	return BusinessLogic_v3.Frontend.MemberReferralModule.MemberReferralBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberReferralBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<MemberReferralBaseFrontend, BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBase>

    {
		
        
        protected MemberReferralBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
