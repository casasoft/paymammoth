using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.MemberSubscriptionTypeModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.MemberSubscriptionTypeModule.MemberSubscriptionTypeBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.IMemberSubscriptionTypeBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.MemberSubscriptionTypeModule.MemberSubscriptionTypeBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.IMemberSubscriptionTypeBase item)
        {
        	return BusinessLogic_v3.Frontend.MemberSubscriptionTypeModule.MemberSubscriptionTypeBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberSubscriptionTypeBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<MemberSubscriptionTypeBaseFrontend, BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.IMemberSubscriptionTypeBase>

    {
		
        
        protected MemberSubscriptionTypeBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
