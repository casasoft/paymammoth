using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.TicketModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Ticket")]
    public abstract class TicketBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ITicketBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static TicketBaseFactory Factory
        {
            get
            {
                return TicketBaseFactory.Instance; 
            }
        }    
        /*
        public static TicketBase CreateNewItem()
        {
            return (TicketBase)Factory.CreateNewItem();
        }

		public static TicketBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<TicketBase, TicketBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static TicketBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static TicketBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<TicketBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<TicketBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<TicketBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _ipaddress;
        
        public virtual string IPAddress 
        {
        	get
        	{
        		return _ipaddress;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ipaddress,value);
        		_ipaddress = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _createdon;
        
        public virtual DateTime CreatedOn 
        {
        	get
        	{
        		return _createdon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_createdon,value);
        		_createdon = value;
        		
        	}
        }
        
/*
		public virtual long? AssignedToID
		{
		 	get 
		 	{
		 		return (this.AssignedTo != null ? (long?)this.AssignedTo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _assignedto;
        public virtual BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase AssignedTo 
        {
        	get
        	{
        		return _assignedto;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_assignedto,value);
        		_assignedto = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase BusinessLogic_v3.Modules._AutoGen.ITicketBaseAutoGen.AssignedTo 
        {
            get
            {
            	return this.AssignedTo;
            }
            set
            {
            	this.AssignedTo = (BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase) value;
            }
        }
            
/*
		public virtual long? MemberID
		{
		 	get 
		 	{
		 		return (this.Member != null ? (long?)this.Member.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _member;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase Member 
        {
        	get
        	{
        		return _member;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_member,value);
        		_member = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.ITicketBaseAutoGen.Member 
        {
            get
            {
            	return this.Member;
            }
            set
            {
            	this.Member = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private DateTime? _closedon;
        
        public virtual DateTime? ClosedOn 
        {
        	get
        	{
        		return _closedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_closedon,value);
        		_closedon = value;
        		
        	}
        }
        
/*
		public virtual long? ClosedByID
		{
		 	get 
		 	{
		 		return (this.ClosedBy != null ? (long?)this.ClosedBy.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _closedby;
        public virtual BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase ClosedBy 
        {
        	get
        	{
        		return _closedby;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_closedby,value);
        		_closedby = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase BusinessLogic_v3.Modules._AutoGen.ITicketBaseAutoGen.ClosedBy 
        {
            get
            {
            	return this.ClosedBy;
            }
            set
            {
            	this.ClosedBy = (BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _closedbyip;
        
        public virtual string ClosedByIP 
        {
        	get
        	{
        		return _closedbyip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_closedbyip,value);
        		_closedbyip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _reopenedon;
        
        public virtual DateTime? ReOpenedOn 
        {
        	get
        	{
        		return _reopenedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_reopenedon,value);
        		_reopenedon = value;
        		
        	}
        }
        
/*
		public virtual long? ReOpenedByCmsUserID
		{
		 	get 
		 	{
		 		return (this.ReOpenedByCmsUser != null ? (long?)this.ReOpenedByCmsUser.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _reopenedbycmsuser;
        public virtual BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase ReOpenedByCmsUser 
        {
        	get
        	{
        		return _reopenedbycmsuser;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_reopenedbycmsuser,value);
        		_reopenedbycmsuser = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase BusinessLogic_v3.Modules._AutoGen.ITicketBaseAutoGen.ReOpenedByCmsUser 
        {
            get
            {
            	return this.ReOpenedByCmsUser;
            }
            set
            {
            	this.ReOpenedByCmsUser = (BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _reopenedbyip;
        
        public virtual string ReOpenedByIP 
        {
        	get
        	{
        		return _reopenedbyip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_reopenedbyip,value);
        		_reopenedbyip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.TICKET_PRIORITY _ticketpriority;
        
        public virtual BusinessLogic_v3.Enums.TICKET_PRIORITY TicketPriority 
        {
        	get
        	{
        		return _ticketpriority;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ticketpriority,value);
        		_ticketpriority = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.TICKET_STATE _ticketstate;
        
        public virtual BusinessLogic_v3.Enums.TICKET_STATE TicketState 
        {
        	get
        	{
        		return _ticketstate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ticketstate,value);
        		_ticketstate = value;
        		
        	}
        }
        
/*
		public virtual long? ReOpenedByMemberID
		{
		 	get 
		 	{
		 		return (this.ReOpenedByMember != null ? (long?)this.ReOpenedByMember.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _reopenedbymember;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase ReOpenedByMember 
        {
        	get
        	{
        		return _reopenedbymember;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_reopenedbymember,value);
        		_reopenedbymember = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.ITicketBaseAutoGen.ReOpenedByMember 
        {
            get
            {
            	return this.ReOpenedByMember;
            }
            set
            {
            	this.ReOpenedByMember = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __TicketCommentsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.TicketModule.TicketBase, 
        	BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase,
        	BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.TicketModule.TicketBase, BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase>
        {
            private BusinessLogic_v3.Modules.TicketModule.TicketBase _item = null;
            public __TicketCommentsCollectionInfo(BusinessLogic_v3.Modules.TicketModule.TicketBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase> Collection
            {
                get { return _item.__collection__TicketComments; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.TicketModule.TicketBase, BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase>.SetLinkOnItem(BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase item, BusinessLogic_v3.Modules.TicketModule.TicketBase value)
            {
                item.Ticket = value;
            }
        }
        
        private __TicketCommentsCollectionInfo _TicketComments = null;
        public __TicketCommentsCollectionInfo TicketComments
        {
            get
            {
                if (_TicketComments == null)
                    _TicketComments = new __TicketCommentsCollectionInfo((BusinessLogic_v3.Modules.TicketModule.TicketBase)this);
                return _TicketComments;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBase> ITicketBaseAutoGen.TicketComments
        {
            get {  return this.TicketComments; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase> __collection__TicketComments
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'TicketCommentBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
