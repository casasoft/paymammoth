using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentTagModule;
using BusinessLogic_v3.Cms.ContentTagModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentTagBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>
    {
		public ContentTagBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentTagBase DbItem
        {
            get
            {
                return (ContentTagBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo TagName { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo TagType { get; private set; }

        public CmsPropertyInfo UsedInProject { get; private set; }



// [basecmsinfo_collectiondeclarations]

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.ArticleModule.ArticleBaseCmsInfo> Articles { get; private set; }
        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.ContentTextModule.ContentTextBaseCmsInfo> ContentTexts { get; private set; }
        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.EmailTextModule.EmailTextBaseCmsInfo> EmailTexts { get; private set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.TagName = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>.GetPropertyBySelector( item => item.TagName),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.TagType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>.GetPropertyBySelector( item => item.TagType),
        			isEditable: true,
        			isVisible: true
        			);

        this.UsedInProject = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>.GetPropertyBySelector( item => item.UsedInProject),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initcollections]

//InitFieldManyToManyBase_RightSide
		this.Articles = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.ArticleModule.ArticleBaseCmsInfo>(
								item => item.Articles, 
								Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);
//InitFieldManyToManyBase_RightSide
		this.ContentTexts = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.ContentTextModule.ContentTextBaseCmsInfo>(
								item => item.ContentTexts, 
								Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);
//InitFieldManyToManyBase_RightSide
		this.EmailTexts = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.EmailTextModule.EmailTextBaseCmsInfo>(
								item => item.EmailTexts, 
								Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
