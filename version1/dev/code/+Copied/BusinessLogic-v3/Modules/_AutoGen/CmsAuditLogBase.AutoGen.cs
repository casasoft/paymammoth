using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using CS.General_v3.Classes.DbObjects.Collections;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.CmsAuditLogModule;                

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public abstract class CmsAuditLogBaseAutoGen 
    : BusinessLogic_v3.Classes.DB.BaseDbObject, ICmsAuditLogBaseAutoGen
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	
    	
    	
        
     	public static CmsAuditLogBaseFactory Factory
        {
            get
            {
                return CmsAuditLogBaseFactory.Instance; 
            }
        }    
        /*
        public static CmsAuditLogBase CreateNewItem()
        {
            return (CmsAuditLogBase)Factory.CreateNewItem();
        }

		public static CmsAuditLogBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<CmsAuditLogBase, CmsAuditLogBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static CmsAuditLogBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static CmsAuditLogBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<CmsAuditLogBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<CmsAuditLogBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<CmsAuditLogBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? CmsUserID
		{
		 	get 
		 	{
		 		return (this.CmsUser != null ? (long?)this.CmsUser.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _cmsuser;
        public virtual BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase CmsUser 
        {
        	get
        	{
        		return _cmsuser;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cmsuser,value);
        		_cmsuser = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase BusinessLogic_v3.Modules._AutoGen.ICmsAuditLogBaseAutoGen.CmsUser 
        {
            get
            {
            	return this.CmsUser;
            }
            set
            {
            	this.CmsUser = (BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private DateTime _datetime;
        public virtual DateTime DateTime 
        {
        	get
        	{
        		return _datetime;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datetime,value);
        		_datetime = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _message;
        public virtual string Message 
        {
        	get
        	{
        		return _message;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_message,value);
        		_message = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _furtherinformation;
        public virtual string FurtherInformation 
        {
        	get
        	{
        		return _furtherinformation;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_furtherinformation,value);
        		_furtherinformation = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.CMS_AUDIT_MESSAGE_TYPE _msgtype;
        public virtual BusinessLogic_v3.Enums.CMS_AUDIT_MESSAGE_TYPE MsgType 
        {
        	get
        	{
        		return _msgtype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_msgtype,value);
        		_msgtype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _ipaddress;
        public virtual string IPAddress 
        {
        	get
        	{
        		return _ipaddress;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ipaddress,value);
        		_ipaddress = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _objectname;
        public virtual string ObjectName 
        {
        	get
        	{
        		return _objectname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_objectname,value);
        		_objectname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _remarks;
        public virtual string Remarks 
        {
        	get
        	{
        		return _remarks;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_remarks,value);
        		_remarks = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private long _itemid;
        public virtual long ItemID 
        {
        	get
        	{
        		return _itemid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemid,value);
        		_itemid = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    
#region Multilingual
          


#endregion    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
