using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ContactFormModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ContactForm")]
    public abstract class ContactFormBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IContactFormBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ContactFormBaseFactory Factory
        {
            get
            {
                return ContactFormBaseFactory.Instance; 
            }
        }    
        /*
        public static ContactFormBase CreateNewItem()
        {
            return (ContactFormBase)Factory.CreateNewItem();
        }

		public static ContactFormBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ContactFormBase, ContactFormBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ContactFormBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ContactFormBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ContactFormBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ContactFormBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ContactFormBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private DateTime _datesent;
        
        public virtual DateTime DateSent 
        {
        	get
        	{
        		return _datesent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datesent,value);
        		_datesent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _name;
        
        public virtual string Name 
        {
        	get
        	{
        		return _name;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_name,value);
        		_name = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _company;
        
        public virtual string Company 
        {
        	get
        	{
        		return _company;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_company,value);
        		_company = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _subject;
        
        public virtual string Subject 
        {
        	get
        	{
        		return _subject;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subject,value);
        		_subject = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _email;
        
        public virtual string Email 
        {
        	get
        	{
        		return _email;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_email,value);
        		_email = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _telephone;
        
        public virtual string Telephone 
        {
        	get
        	{
        		return _telephone;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_telephone,value);
        		_telephone = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _mobile;
        
        public virtual string Mobile 
        {
        	get
        	{
        		return _mobile;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_mobile,value);
        		_mobile = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _address1;
        
        public virtual string Address1 
        {
        	get
        	{
        		return _address1;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_address1,value);
        		_address1 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _address2;
        
        public virtual string Address2 
        {
        	get
        	{
        		return _address2;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_address2,value);
        		_address2 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _address3;
        
        public virtual string Address3 
        {
        	get
        	{
        		return _address3;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_address3,value);
        		_address3 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _zipcode;
        
        public virtual string ZIPCode 
        {
        	get
        	{
        		return _zipcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_zipcode,value);
        		_zipcode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _state;
        
        public virtual string State 
        {
        	get
        	{
        		return _state;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_state,value);
        		_state = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _countrystr;
        
        public virtual string CountryStr 
        {
        	get
        	{
        		return _countrystr;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_countrystr,value);
        		_countrystr = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _city;
        
        public virtual string City 
        {
        	get
        	{
        		return _city;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_city,value);
        		_city = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _fax;
        
        public virtual string Fax 
        {
        	get
        	{
        		return _fax;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_fax,value);
        		_fax = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _website;
        
        public virtual string Website 
        {
        	get
        	{
        		return _website;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_website,value);
        		_website = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _ip;
        
        public virtual string IP 
        {
        	get
        	{
        		return _ip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ip,value);
        		_ip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _enquiry;
        
        public virtual string Enquiry 
        {
        	get
        	{
        		return _enquiry;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_enquiry,value);
        		_enquiry = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _remark;
        
        public virtual string Remark 
        {
        	get
        	{
        		return _remark;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_remark,value);
        		_remark = value;
        		
        	}
        }
        
/*
		public virtual long? SentByMemberID
		{
		 	get 
		 	{
		 		return (this.SentByMember != null ? (long?)this.SentByMember.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _sentbymember;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase SentByMember 
        {
        	get
        	{
        		return _sentbymember;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_sentbymember,value);
        		_sentbymember = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IContactFormBaseAutoGen.SentByMember 
        {
            get
            {
            	return this.SentByMember;
            }
            set
            {
            	this.SentByMember = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _surname;
        
        public virtual string Surname 
        {
        	get
        	{
        		return _surname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_surname,value);
        		_surname = value;
        		
        	}
        }
        
/*
		public virtual long? VacancyID
		{
		 	get 
		 	{
		 		return (this.Vacancy != null ? (long?)this.Vacancy.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.VacancyModule.VacancyBase _vacancy;
        public virtual BusinessLogic_v3.Modules.VacancyModule.VacancyBase Vacancy 
        {
        	get
        	{
        		return _vacancy;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_vacancy,value);
        		_vacancy = value;
        	}
        }
            
        BusinessLogic_v3.Modules.VacancyModule.IVacancyBase BusinessLogic_v3.Modules._AutoGen.IContactFormBaseAutoGen.Vacancy 
        {
            get
            {
            	return this.Vacancy;
            }
            set
            {
            	this.Vacancy = (BusinessLogic_v3.Modules.VacancyModule.VacancyBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _username;
        
        public virtual string Username 
        {
        	get
        	{
        		return _username;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_username,value);
        		_username = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _filefilename;
        
        public virtual string FileFilename 
        {
        	get
        	{
        		return _filefilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_filefilename,value);
        		_filefilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _howdidyoufindus;
        
        public virtual string HowDidYouFindUs 
        {
        	get
        	{
        		return _howdidyoufindus;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_howdidyoufindus,value);
        		_howdidyoufindus = value;
        		
        	}
        }
        
/*
		public virtual long? ProductID
		{
		 	get 
		 	{
		 		return (this.Product != null ? (long?)this.Product.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _product;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase Product 
        {
        	get
        	{
        		return _product;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_product,value);
        		_product = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.IContactFormBaseAutoGen.Product 
        {
            get
            {
            	return this.Product;
            }
            set
            {
            	this.Product = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
/*
		public virtual long? EventID
		{
		 	get 
		 	{
		 		return (this.Event != null ? (long?)this.Event.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.EventModule.EventBase _event;
        public virtual BusinessLogic_v3.Modules.EventModule.EventBase Event 
        {
        	get
        	{
        		return _event;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_event,value);
        		_event = value;
        	}
        }
            
        BusinessLogic_v3.Modules.EventModule.IEventBase BusinessLogic_v3.Modules._AutoGen.IContactFormBaseAutoGen.Event 
        {
            get
            {
            	return this.Event;
            }
            set
            {
            	this.Event = (BusinessLogic_v3.Modules.EventModule.EventBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
