using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Cms.OrderModule;
using BusinessLogic_v3.Frontend.OrderModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class OrderBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.OrderModule.OrderBase>
    {
		public OrderBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.OrderModule.OrderBase dbItem)
            : base(BusinessLogic_v3.Cms.OrderModule.OrderBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new OrderBaseFrontend FrontendItem
        {
            get { return (OrderBaseFrontend)base.FrontendItem; }

        }
        public new OrderBase DbItem
        {
            get
            {
                return (OrderBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateCreated { get; protected set; }

        public CmsPropertyInfo Reference { get; protected set; }

        public CmsPropertyInfo CustomerFirstName { get; protected set; }

        public CmsPropertyInfo CustomerEmail { get; protected set; }

        public CmsPropertyInfo Member { get; protected set; }

        public CmsPropertyInfo CustomerAddress1 { get; protected set; }

        public CmsPropertyInfo CustomerAddress2 { get; protected set; }

        public CmsPropertyInfo CustomerAddress3 { get; protected set; }

        public CmsPropertyInfo AuthCode { get; protected set; }

        public CmsPropertyInfo CustomerCountry { get; protected set; }

        public CmsPropertyInfo CustomerPostCode { get; protected set; }

        public CmsPropertyInfo CustomerLocality { get; protected set; }

        public CmsPropertyInfo CustomerState { get; protected set; }

        public CmsPropertyInfo Paid { get; protected set; }

        public CmsPropertyInfo PaidOn { get; protected set; }

        public CmsPropertyInfo Cancelled { get; protected set; }

        public CmsPropertyInfo CancelledOn { get; protected set; }

        public CmsPropertyInfo Remarks { get; protected set; }

        public CmsPropertyInfo CreatedByAdmin { get; protected set; }

        public CmsPropertyInfo GUID { get; protected set; }

        public CmsPropertyInfo ItemID { get; protected set; }

        public CmsPropertyInfo ItemType { get; protected set; }

        public CmsPropertyInfo IsShoppingCart { get; protected set; }

        public CmsPropertyInfo CustomerLastName { get; protected set; }

        public CmsPropertyInfo ShippedOn { get; protected set; }

        public CmsPropertyInfo ShipmentTrackingCode { get; protected set; }

        public CmsPropertyInfo ShipmentType { get; protected set; }

        public CmsPropertyInfo ShipmentRemarks { get; protected set; }

        public CmsPropertyInfo IsConfirmed { get; protected set; }

        public CmsPropertyInfo ShipmentTrackingUrl { get; protected set; }

        public CmsPropertyInfo FixedDiscount { get; protected set; }

        public CmsPropertyInfo OrderStatus { get; protected set; }

        public CmsPropertyInfo TotalShippingCost { get; protected set; }

        public CmsPropertyInfo LastUpdatedOn { get; protected set; }

        public CmsPropertyInfo OrderCurrency { get; protected set; }

        public CmsPropertyInfo ShippingMethod { get; protected set; }

        public CmsPropertyInfo LinkedAffiliate { get; protected set; }

        public CmsPropertyInfo AffiliateCommissionRate { get; protected set; }

        public CmsPropertyInfo CustomerMiddleName { get; protected set; }

        public CmsPropertyInfo PayMammothIdentifier { get; protected set; }

        public CmsPropertyInfo PendingManualPayment { get; protected set; }

        public CmsPropertyInfo PaymentMethod { get; protected set; }

        public CmsPropertyInfo PayPippaProcessCompletedOn { get; protected set; }

        public CmsPropertyInfo AccountBalanceDifference { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo MemberSubscriptionLinks { get; protected set; }
        

        public CmsCollectionInfo OrderItems { get; protected set; }
        

        public CmsCollectionInfo PaymentTransactions { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.MemberSubscriptionLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector(item => item.MemberSubscriptionLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase>.GetPropertyBySelector(item => item.Order)));
		
		//InitCollectionBaseOneToMany
		this.OrderItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector(item => item.OrderItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector(item => item.Order)));
		
		//InitCollectionBaseOneToMany
		this.PaymentTransactions = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector(item => item.PaymentTransactions),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.PaymentTransactionModule.PaymentTransactionBase>.GetPropertyBySelector(item => item.Order)));


			base.initBasicFields();
          
        }

    }
}
