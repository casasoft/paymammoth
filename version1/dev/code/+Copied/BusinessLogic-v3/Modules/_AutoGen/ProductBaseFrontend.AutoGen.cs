using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductModule.ProductBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductModule.IProductBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductModule.ProductBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductModule.IProductBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductModule.ProductBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductBaseFrontend, BusinessLogic_v3.Modules.ProductModule.IProductBase>

    {
		
        
        protected ProductBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
