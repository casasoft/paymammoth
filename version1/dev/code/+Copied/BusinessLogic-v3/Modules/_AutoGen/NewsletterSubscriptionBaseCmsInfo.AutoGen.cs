using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.NewsletterSubscriptionModule;
using BusinessLogic_v3.Cms.NewsletterSubscriptionModule;
using BusinessLogic_v3.Frontend.NewsletterSubscriptionModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class NewsletterSubscriptionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase>
    {
		public NewsletterSubscriptionBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase dbItem)
            : base(BusinessLogic_v3.Cms.NewsletterSubscriptionModule.NewsletterSubscriptionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new NewsletterSubscriptionBaseFrontend FrontendItem
        {
            get { return (NewsletterSubscriptionBaseFrontend)base.FrontendItem; }

        }
        public new NewsletterSubscriptionBase DbItem
        {
            get
            {
                return (NewsletterSubscriptionBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Email { get; protected set; }

        public CmsPropertyInfo Name { get; protected set; }

        public CmsPropertyInfo Telephone { get; protected set; }

        public CmsPropertyInfo DateAdded { get; protected set; }

        public CmsPropertyInfo Enabled { get; protected set; }

        public CmsPropertyInfo RegisteredIPAddress { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
