using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.TicketCommentModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.TicketCommentModule.TicketCommentBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.TicketCommentModule.TicketCommentBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBase item)
        {
        	return BusinessLogic_v3.Frontend.TicketCommentModule.TicketCommentBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class TicketCommentBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<TicketCommentBaseFrontend, BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBase>

    {
		
        
        protected TicketCommentBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
