using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CmsAuditLogModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class CmsAuditLogBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DB.BaseDbFactory<CmsAuditLogBase, ICmsAuditLogBase>, ICmsAuditLogBaseFactoryAutoGen
    
    {
    
        public CmsAuditLogBaseFactoryAutoGen()
        {
        	
            
        }
        
		static CmsAuditLogBaseFactoryAutoGen()
        {
            UsedInProject = true;
        }
        
        
        public new static CmsAuditLogBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<CmsAuditLogBaseFactory>(null);
            }
        }
        
		public IQueryOver<CmsAuditLogBase, CmsAuditLogBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	
        	var q = session.QueryOver<CmsAuditLogBase>();
            qParams.FillQueryOver(q);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
