using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ArticleCommentModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IArticleCommentBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Email { get; set; }
		//Iproperty_normal
        string Comment { get; set; }
		//Iproperty_normal
        string IPAddress { get; set; }
		//Iproperty_normal
        DateTime PostedOn { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase Article {get; set; }

		//Iproperty_normal
        string Author { get; set; }
		//Iproperty_normal
        string Company { get; set; }
		//Iproperty_normal
        bool IsApproved { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase ReplyTo {get; set; }

   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase> Replies { get; }

 
		

    	
    	
      
      

    }
}
