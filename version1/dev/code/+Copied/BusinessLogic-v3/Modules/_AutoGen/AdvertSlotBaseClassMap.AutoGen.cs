using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Modules.AdvertSlotModule;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;

namespace BusinessLogic_v3.Modules._AutoGen
{
    
    public abstract class AdvertSlotBaseMap_AutoGen<TData> : BusinessLogic_v3.Classes.NHibernateClasses.Mappings.BaseClassMap<TData>
    	where TData : AdvertSlotBase
    {
        public AdvertSlotBaseMap_AutoGen()
        {
            
        }
        
        


// [baseclassmap_properties]

//BaseClassMap_PropertyLinkedObject

        protected virtual void AdvertColumnMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            mapInfo.CollectionType = BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "AdvertColumnId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        
//BaseClassMap_Property
             
        protected virtual void TitleMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void RoundNoMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void WidthMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void HeightMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        



// [baseclassmap_collections]

//BaseClassMap_Collection_OneToMany

        protected virtual void AdvertsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "AdvertSlotID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
            
        
        protected override void initMappings()
        {
           
            
//[BaseClassMap_InitProperties]

//[BaseClassMap_InitCollections]                
            
            base.initMappings();
        }
        
        
        
        
        
    }
   
}
