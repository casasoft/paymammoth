using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.Article_CultureInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Article_CultureInfo")]
    public abstract class Article_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IArticle_CultureInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
      , IMultilingualContentInfo
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static Article_CultureInfoBaseFactory Factory
        {
            get
            {
                return Article_CultureInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static Article_CultureInfoBase CreateNewItem()
        {
            return (Article_CultureInfoBase)Factory.CreateNewItem();
        }

		public static Article_CultureInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<Article_CultureInfoBase, Article_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static Article_CultureInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static Article_CultureInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<Article_CultureInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<Article_CultureInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<Article_CultureInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? CultureInfoID
		{
		 	get 
		 	{
		 		return (this.CultureInfo != null ? (long?)this.CultureInfo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase _cultureinfo;
        public virtual BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase CultureInfo 
        {
        	get
        	{
        		return _cultureinfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cultureinfo,value);
        		_cultureinfo = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase BusinessLogic_v3.Modules._AutoGen.IArticle_CultureInfoBaseAutoGen.CultureInfo 
        {
            get
            {
            	return this.CultureInfo;
            }
            set
            {
            	this.CultureInfo = (BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase) value;
            }
        }
            
/*
		public virtual long? ArticleID
		{
		 	get 
		 	{
		 		return (this.Article != null ? (long?)this.Article.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ArticleModule.ArticleBase _article;
        public virtual BusinessLogic_v3.Modules.ArticleModule.ArticleBase Article 
        {
        	get
        	{
        		return _article;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_article,value);
        		_article = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase BusinessLogic_v3.Modules._AutoGen.IArticle_CultureInfoBaseAutoGen.Article 
        {
            get
            {
            	return this.Article;
            }
            set
            {
            	this.Article = (BusinessLogic_v3.Modules.ArticleModule.ArticleBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _htmltext;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string HtmlText 
        {
        	get
        	{
        		return _htmltext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_htmltext,value);
        		_htmltext = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _htmltext_search;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string HtmlText_Search 
        {
        	get
        	{
        		return _htmltext_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_htmltext_search,value);
        		_htmltext_search = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _metakeywords;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string MetaKeywords 
        {
        	get
        	{
        		return _metakeywords;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metakeywords,value);
        		_metakeywords = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _metakeywords_search;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string MetaKeywords_Search 
        {
        	get
        	{
        		return _metakeywords_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metakeywords_search,value);
        		_metakeywords_search = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _metadescription;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string MetaDescription 
        {
        	get
        	{
        		return _metadescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metadescription,value);
        		_metadescription = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _metadescription_search;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string MetaDescription_Search 
        {
        	get
        	{
        		return _metadescription_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metadescription_search,value);
        		_metadescription_search = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _pagetitle;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string PageTitle 
        {
        	get
        	{
        		return _pagetitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_pagetitle,value);
        		_pagetitle = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _pagetitle_search;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string PageTitle_Search 
        {
        	get
        	{
        		return _pagetitle_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_pagetitle_search,value);
        		_pagetitle_search = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int? _dialogwidth;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual int? DialogWidth 
        {
        	get
        	{
        		return _dialogwidth;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dialogwidth,value);
        		_dialogwidth = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int? _dialogheight;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual int? DialogHeight 
        {
        	get
        	{
        		return _dialogheight;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dialogheight,value);
        		_dialogheight = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _subtitle;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string SubTitle 
        {
        	get
        	{
        		return _subtitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subtitle,value);
        		_subtitle = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string __computedurl;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string _ComputedURL 
        {
        	get
        	{
        		return __computedurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__computedurl,value);
        		__computedurl = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string __computed_parentarticlenames;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string _Computed_ParentArticleNames 
        {
        	get
        	{
        		return __computed_parentarticlenames;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__computed_parentarticlenames,value);
        		__computed_parentarticlenames = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)




#region IMultilingualContentInfo Members

        long IMultilingualContentInfo.CultureInfoID
        {
            get
            {                          
            	if (this.CultureInfo != null)
                	return this.CultureInfo.ID;
               	else
               		return 0;
            }
        }
  		BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject IMultilingualContentInfo.GetLinkedItem()
        {
            return this.Article;
        }
#endregion



#endregion       

		

    }
}
