using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ContactFormModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IContactFormBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        DateTime DateSent { get; set; }
		//Iproperty_normal
        string Name { get; set; }
		//Iproperty_normal
        string Company { get; set; }
		//Iproperty_normal
        string Subject { get; set; }
		//Iproperty_normal
        string Email { get; set; }
		//Iproperty_normal
        string Telephone { get; set; }
		//Iproperty_normal
        string Mobile { get; set; }
		//Iproperty_normal
        string Address1 { get; set; }
		//Iproperty_normal
        string Address2 { get; set; }
		//Iproperty_normal
        string Address3 { get; set; }
		//Iproperty_normal
        string ZIPCode { get; set; }
		//Iproperty_normal
        string State { get; set; }
		//Iproperty_normal
        string CountryStr { get; set; }
		//Iproperty_normal
        string City { get; set; }
		//Iproperty_normal
        string Fax { get; set; }
		//Iproperty_normal
        string Website { get; set; }
		//Iproperty_normal
        string IP { get; set; }
		//Iproperty_normal
        string Enquiry { get; set; }
		//Iproperty_normal
        string Remark { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase SentByMember {get; set; }

		//Iproperty_normal
        string Surname { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.VacancyModule.IVacancyBase Vacancy {get; set; }

		//Iproperty_normal
        string Username { get; set; }
		//Iproperty_normal
        string FileFilename { get; set; }
		//Iproperty_normal
        string HowDidYouFindUs { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ProductModule.IProductBase Product {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.EventModule.IEventBase Event {get; set; }

   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
