using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductVersionModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductVersionModule.ProductVersionBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductVersionModule.IProductVersionBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductVersionModule.ProductVersionBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductVersionModule.IProductVersionBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductVersionModule.ProductVersionBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductVersionBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductVersionBaseFrontend, BusinessLogic_v3.Modules.ProductVersionModule.IProductVersionBase>

    {
		
        
        protected ProductVersionBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
