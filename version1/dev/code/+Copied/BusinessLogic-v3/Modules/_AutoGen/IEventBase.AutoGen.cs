using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.EventModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IEventBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Title { get; set; }
		//Iproperty_normal
        string Description { get; set; }
		//Iproperty_normal
        DateTime StartDate { get; set; }
		//Iproperty_normal
        DateTime EndDate { get; set; }
		//Iproperty_normal
        string ImageFilename { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase Category {get; set; }

		//Iproperty_normal
        string SubTitle { get; set; }
		//Iproperty_normal
        string Price { get; set; }
		//Iproperty_normal
        string Identifier { get; set; }
		//Iproperty_normal
        string Sessions { get; set; }
		//Iproperty_normal
        int ClassSize { get; set; }
		//Iproperty_normal
        string Dates { get; set; }
		//Iproperty_normal
        string Duration { get; set; }
		//Iproperty_normal
        string ShortDescription { get; set; }
		//Iproperty_normal
        string Location { get; set; }
		//Iproperty_normal
        string Summary { get; set; }
		//Iproperty_normal
        string Time { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase> ContactForms { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBase> EventMediaItems { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.EventSessionModule.IEventSessionBase> EventSessions { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.FileItemModule.IFileItemBase> FileItems { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.TestimonialModule.ITestimonialBase> Testimonials { get; }

        //IBaseClass_CollectionManyToManyRightSide  
        
        ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> Members { get; }
        
        
 
		

    	
    	
      
      

    }
}
