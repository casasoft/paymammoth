using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.AuditLogModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.AuditLogModule.AuditLogBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.AuditLogModule.IAuditLogBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.AuditLogModule.AuditLogBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.AuditLogModule.IAuditLogBase item)
        {
        	return BusinessLogic_v3.Frontend.AuditLogModule.AuditLogBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AuditLogBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<AuditLogBaseFrontend, BusinessLogic_v3.Modules.AuditLogModule.IAuditLogBase>

    {
		
        
        protected AuditLogBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
