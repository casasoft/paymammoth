using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.AuditLogModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "AuditLog")]
    public abstract class AuditLogBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IAuditLogBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static AuditLogBaseFactory Factory
        {
            get
            {
                return AuditLogBaseFactory.Instance; 
            }
        }    
        /*
        public static AuditLogBase CreateNewItem()
        {
            return (AuditLogBase)Factory.CreateNewItem();
        }

		public static AuditLogBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<AuditLogBase, AuditLogBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static AuditLogBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static AuditLogBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<AuditLogBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<AuditLogBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<AuditLogBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _itemtype;
        
        public virtual string ItemType 
        {
        	get
        	{
        		return _itemtype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemtype,value);
        		_itemtype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private long _itemid;
        
        public virtual long ItemID 
        {
        	get
        	{
        		return _itemid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemid,value);
        		_itemid = value;
        		
        	}
        }
        
/*
		public virtual long? CmsUserID
		{
		 	get 
		 	{
		 		return (this.CmsUser != null ? (long?)this.CmsUser.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _cmsuser;
        public virtual BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase CmsUser 
        {
        	get
        	{
        		return _cmsuser;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cmsuser,value);
        		_cmsuser = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase BusinessLogic_v3.Modules._AutoGen.IAuditLogBaseAutoGen.CmsUser 
        {
            get
            {
            	return this.CmsUser;
            }
            set
            {
            	this.CmsUser = (BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _cmsuseripaddress;
        
        public virtual string CmsUserIpAddress 
        {
        	get
        	{
        		return _cmsuseripaddress;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cmsuseripaddress,value);
        		_cmsuseripaddress = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _datetime;
        
        public virtual DateTime DateTime 
        {
        	get
        	{
        		return _datetime;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datetime,value);
        		_datetime = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _stacktrace;
        
        public virtual string StackTrace 
        {
        	get
        	{
        		return _stacktrace;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_stacktrace,value);
        		_stacktrace = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _method;
        
        public virtual string Method 
        {
        	get
        	{
        		return _method;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_method,value);
        		_method = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _changes;
        
        public virtual string Changes 
        {
        	get
        	{
        		return _changes;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_changes,value);
        		_changes = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _remarks;
        
        public virtual string Remarks 
        {
        	get
        	{
        		return _remarks;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_remarks,value);
        		_remarks = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _message;
        
        public virtual string Message 
        {
        	get
        	{
        		return _message;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_message,value);
        		_message = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _itemtypefull;
        
        public virtual string ItemTypeFull 
        {
        	get
        	{
        		return _itemtypefull;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemtypefull,value);
        		_itemtypefull = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.AUDITLOG_MSG_TYPE _updatetype;
        
        public virtual BusinessLogic_v3.Enums.AUDITLOG_MSG_TYPE UpdateType 
        {
        	get
        	{
        		return _updatetype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_updatetype,value);
        		_updatetype = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
