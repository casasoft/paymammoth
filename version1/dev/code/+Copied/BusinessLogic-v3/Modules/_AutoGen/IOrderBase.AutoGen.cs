using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.OrderModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IOrderBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        DateTime DateCreated { get; set; }
		//Iproperty_normal
        string Reference { get; set; }
		//Iproperty_normal
        string CustomerFirstName { get; set; }
		//Iproperty_normal
        string CustomerEmail { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase Member {get; set; }

		//Iproperty_normal
        string CustomerAddress1 { get; set; }
		//Iproperty_normal
        string CustomerAddress2 { get; set; }
		//Iproperty_normal
        string CustomerAddress3 { get; set; }
		//Iproperty_normal
        string AuthCode { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? CustomerCountry { get; set; }
		//Iproperty_normal
        string CustomerPostCode { get; set; }
		//Iproperty_normal
        string CustomerLocality { get; set; }
		//Iproperty_normal
        string CustomerState { get; set; }
		//Iproperty_normal
        bool Paid { get; set; }
		//Iproperty_normal
        DateTime? PaidOn { get; set; }
		//Iproperty_normal
        bool Cancelled { get; set; }
		//Iproperty_normal
        DateTime? CancelledOn { get; set; }
		//Iproperty_normal
        string Remarks { get; set; }
		//Iproperty_normal
        bool CreatedByAdmin { get; set; }
		//Iproperty_normal
        string GUID { get; set; }
		//Iproperty_normal
        int ItemID { get; set; }
		//Iproperty_normal
        string ItemType { get; set; }
		//Iproperty_normal
        bool IsShoppingCart { get; set; }
		//Iproperty_normal
        string CustomerLastName { get; set; }
		//Iproperty_normal
        DateTime? ShippedOn { get; set; }
		//Iproperty_normal
        string ShipmentTrackingCode { get; set; }
		//Iproperty_normal
        string ShipmentType { get; set; }
		//Iproperty_normal
        string ShipmentRemarks { get; set; }
		//Iproperty_normal
        bool IsConfirmed { get; set; }
		//Iproperty_normal
        string ShipmentTrackingUrl { get; set; }
		//Iproperty_normal
        double FixedDiscount { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.ORDER_STATUS OrderStatus { get; set; }
		//Iproperty_normal
        double? TotalShippingCost { get; set; }
		//Iproperty_normal
        DateTime LastUpdatedOn { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase OrderCurrency {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ShippingMethodModule.IShippingMethodBase ShippingMethod {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase LinkedAffiliate {get; set; }

		//Iproperty_normal
        double AffiliateCommissionRate { get; set; }
		//Iproperty_normal
        string CustomerMiddleName { get; set; }
		//Iproperty_normal
        string PayMammothIdentifier { get; set; }
		//Iproperty_normal
        bool PendingManualPayment { get; set; }
		//Iproperty_normal
        PayMammoth.Connector.Enums.PaymentMethodSpecific? PaymentMethod { get; set; }
		//Iproperty_normal
        DateTime? PayPippaProcessCompletedOn { get; set; }
		//Iproperty_normal
        double AccountBalanceDifference { get; set; }
		//Iproperty_normal
        bool IsTestOrder { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase> MemberSubscriptionLinks { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.IOrderApplicableSpecialOfferLinkBase> ApplicableSpecialOfferLinks { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase> OrderItems { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase> LinkedShoppingCartItems { get; }

 
		

    	
    	
      
      

    }
}
