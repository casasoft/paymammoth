using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule;                

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public abstract class SpecialOfferApplicableCountryBaseAutoGen : BusinessLogic_v3.Classes.DB.BaseDbObject, 
       											  ISpecialOfferApplicableCountryBaseAutoGen,
											      BusinessLogic_v3.Classes.DB.IBaseDbObject, 
											      BusinessLogic_v3.Classes.DbObjects.IBaseDbObject
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static SpecialOfferApplicableCountryBaseFactory Factory
        {
            get
            {
                return SpecialOfferApplicableCountryBaseFactory.Instance; 
            }
        }    
        /*
        public static SpecialOfferApplicableCountryBase CreateNewItem()
        {
            return (SpecialOfferApplicableCountryBase)Factory.CreateNewItem();
        }

		public static SpecialOfferApplicableCountryBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<SpecialOfferApplicableCountryBase, SpecialOfferApplicableCountryBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static SpecialOfferApplicableCountryBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static SpecialOfferApplicableCountryBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<SpecialOfferApplicableCountryBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<SpecialOfferApplicableCountryBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<SpecialOfferApplicableCountryBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? SpecialOfferID
		{
		 	get 
		 	{
		 		return (this.SpecialOffer != null ? (long?)this.SpecialOffer.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase _specialoffer;
        public virtual BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase SpecialOffer 
        {
        	get
        	{
        		return _specialoffer;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_specialoffer,value);
        		_specialoffer = value;
        	}
        }
            
        BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBase BusinessLogic_v3.Modules._AutoGen.ISpecialOfferApplicableCountryBaseAutoGen.SpecialOffer 
        {
            get
            {
            	return this.SpecialOffer;
            }
            set
            {
            	this.SpecialOffer = (BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 _country;
        public virtual CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 Country 
        {
        	get
        	{
        		return _country;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_country,value);
        		_country = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
