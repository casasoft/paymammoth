using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductRelatedProductsModule;                

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public abstract class ProductRelatedProductsBaseAutoGen : BusinessLogic_v3.Classes.DB.BaseDbObject, 
       											  IProductRelatedProductsBaseAutoGen,
											      BusinessLogic_v3.Classes.DB.IBaseDbObject, 
											      BusinessLogic_v3.Classes.DbObjects.IBaseDbObject
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ProductRelatedProductsBaseFactory Factory
        {
            get
            {
                return ProductRelatedProductsBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductRelatedProductsBase CreateNewItem()
        {
            return (ProductRelatedProductsBase)Factory.CreateNewItem();
        }

		public static ProductRelatedProductsBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductRelatedProductsBase, ProductRelatedProductsBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductRelatedProductsBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductRelatedProductsBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductRelatedProductsBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductRelatedProductsBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductRelatedProductsBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? ProductID
		{
		 	get 
		 	{
		 		return (this.Product != null ? (long?)this.Product.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _product;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase Product 
        {
        	get
        	{
        		return _product;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_product,value);
        		_product = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.IProductRelatedProductsBaseAutoGen.Product 
        {
            get
            {
            	return this.Product;
            }
            set
            {
            	this.Product = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
/*
		public virtual long? RelatedProductID
		{
		 	get 
		 	{
		 		return (this.RelatedProduct != null ? (long?)this.RelatedProduct.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _relatedproduct;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase RelatedProduct 
        {
        	get
        	{
        		return _relatedproduct;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_relatedproduct,value);
        		_relatedproduct = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.IProductRelatedProductsBaseAutoGen.RelatedProduct 
        {
            get
            {
            	return this.RelatedProduct;
            }
            set
            {
            	this.RelatedProduct = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
