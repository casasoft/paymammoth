using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.MemberSubscriptionTypePriceModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBase item)
        {
        	return BusinessLogic_v3.Frontend.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberSubscriptionTypePriceBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<MemberSubscriptionTypePriceBaseFrontend, BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBase>

    {
		
        
        protected MemberSubscriptionTypePriceBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
