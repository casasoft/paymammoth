using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberReferralModule;
using BusinessLogic_v3.Cms.MemberReferralModule;
using BusinessLogic_v3.Frontend.MemberReferralModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberReferralBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>
    {
		public MemberReferralBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase dbItem)
            : base(BusinessLogic_v3.Cms.MemberReferralModule.MemberReferralBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberReferralBaseFrontend FrontendItem
        {
            get { return (MemberReferralBaseFrontend)base.FrontendItem; }

        }
        public new MemberReferralBase DbItem
        {
            get
            {
                return (MemberReferralBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateTime { get; protected set; }

        public CmsPropertyInfo SentByMember { get; protected set; }

        public CmsPropertyInfo RegisteredMember { get; protected set; }

        public CmsPropertyInfo Email { get; protected set; }

        public CmsPropertyInfo Name { get; protected set; }

        public CmsPropertyInfo Surname { get; protected set; }

        public CmsPropertyInfo Message { get; protected set; }

        public CmsPropertyInfo Telephone { get; protected set; }

        public CmsPropertyInfo Status { get; protected set; }

        public CmsPropertyInfo ReferredByIP { get; protected set; }

        public CmsPropertyInfo ReferralCode { get; protected set; }

        public CmsPropertyInfo Remarks { get; protected set; }

        public CmsPropertyInfo SentByName { get; protected set; }

        public CmsPropertyInfo SentByEmail { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo MemberReferralCommissions { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.MemberReferralCommissions = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector(item => item.MemberReferralCommissions),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>.GetPropertyBySelector(item => item.MemberReferral)));


			base.initBasicFields();
          
        }

    }
}
