using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.FileItemModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.FileItemModule.FileItemBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.FileItemModule.IFileItemBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.FileItemModule.FileItemBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.FileItemModule.IFileItemBase item)
        {
        	return BusinessLogic_v3.Frontend.FileItemModule.FileItemBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class FileItemBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<FileItemBaseFrontend, BusinessLogic_v3.Modules.FileItemModule.IFileItemBase>

    {
		
        
        protected FileItemBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
