using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.EventSessionPeriodModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "EventSessionPeriod")]
    public abstract class EventSessionPeriodBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IEventSessionPeriodBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static EventSessionPeriodBaseFactory Factory
        {
            get
            {
                return EventSessionPeriodBaseFactory.Instance; 
            }
        }    
        /*
        public static EventSessionPeriodBase CreateNewItem()
        {
            return (EventSessionPeriodBase)Factory.CreateNewItem();
        }

		public static EventSessionPeriodBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<EventSessionPeriodBase, EventSessionPeriodBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static EventSessionPeriodBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static EventSessionPeriodBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<EventSessionPeriodBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<EventSessionPeriodBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<EventSessionPeriodBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? EventSessionID
		{
		 	get 
		 	{
		 		return (this.EventSession != null ? (long?)this.EventSession.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase _eventsession;
        public virtual BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase EventSession 
        {
        	get
        	{
        		return _eventsession;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_eventsession,value);
        		_eventsession = value;
        	}
        }
            
        BusinessLogic_v3.Modules.EventSessionModule.IEventSessionBase BusinessLogic_v3.Modules._AutoGen.IEventSessionPeriodBaseAutoGen.EventSession 
        {
            get
            {
            	return this.EventSession;
            }
            set
            {
            	this.EventSession = (BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private DateTime _startdate;
        
        public virtual DateTime StartDate 
        {
        	get
        	{
        		return _startdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_startdate,value);
        		_startdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _enddate;
        
        public virtual DateTime EndDate 
        {
        	get
        	{
        		return _enddate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_enddate,value);
        		_enddate = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EventSessionPeriodDaysCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase, 
        	BusinessLogic_v3.Modules.EventSessionPeriodDayModule.EventSessionPeriodDayBase,
        	BusinessLogic_v3.Modules.EventSessionPeriodDayModule.IEventSessionPeriodDayBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase, BusinessLogic_v3.Modules.EventSessionPeriodDayModule.EventSessionPeriodDayBase>
        {
            private BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase _item = null;
            public __EventSessionPeriodDaysCollectionInfo(BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EventSessionPeriodDayModule.EventSessionPeriodDayBase> Collection
            {
                get { return _item.__collection__EventSessionPeriodDays; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase, BusinessLogic_v3.Modules.EventSessionPeriodDayModule.EventSessionPeriodDayBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EventSessionPeriodDayModule.EventSessionPeriodDayBase item, BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase value)
            {
                item.EventSessionPeriod = value;
            }
        }
        
        private __EventSessionPeriodDaysCollectionInfo _EventSessionPeriodDays = null;
        public __EventSessionPeriodDaysCollectionInfo EventSessionPeriodDays
        {
            get
            {
                if (_EventSessionPeriodDays == null)
                    _EventSessionPeriodDays = new __EventSessionPeriodDaysCollectionInfo((BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase)this);
                return _EventSessionPeriodDays;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EventSessionPeriodDayModule.IEventSessionPeriodDayBase> IEventSessionPeriodBaseAutoGen.EventSessionPeriodDays
        {
            get {  return this.EventSessionPeriodDays; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EventSessionPeriodDayModule.EventSessionPeriodDayBase> __collection__EventSessionPeriodDays
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventSessionPeriodDayBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
