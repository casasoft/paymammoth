using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ShoppingCartModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ShoppingCart")]
    public abstract class ShoppingCartBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IShoppingCartBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ShoppingCartBaseFactory Factory
        {
            get
            {
                return ShoppingCartBaseFactory.Instance; 
            }
        }    
        /*
        public static ShoppingCartBase CreateNewItem()
        {
            return (ShoppingCartBase)Factory.CreateNewItem();
        }

		public static ShoppingCartBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ShoppingCartBase, ShoppingCartBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ShoppingCartBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ShoppingCartBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ShoppingCartBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ShoppingCartBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ShoppingCartBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private DateTime _datecreated;
        
        public virtual DateTime DateCreated 
        {
        	get
        	{
        		return _datecreated;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datecreated,value);
        		_datecreated = value;
        		
        	}
        }
        
/*
		public virtual long? MemberID
		{
		 	get 
		 	{
		 		return (this.Member != null ? (long?)this.Member.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _member;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase Member 
        {
        	get
        	{
        		return _member;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_member,value);
        		_member = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IShoppingCartBaseAutoGen.Member 
        {
            get
            {
            	return this.Member;
            }
            set
            {
            	this.Member = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _cancelled;
        
        public virtual bool Cancelled 
        {
        	get
        	{
        		return _cancelled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cancelled,value);
        		_cancelled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _cancelledon;
        
        public virtual DateTime? CancelledOn 
        {
        	get
        	{
        		return _cancelledon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cancelledon,value);
        		_cancelledon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _lastupdatedon;
        
        public virtual DateTime LastUpdatedOn 
        {
        	get
        	{
        		return _lastupdatedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_lastupdatedon,value);
        		_lastupdatedon = value;
        		
        	}
        }
        
/*
		public virtual long? CurrencyID
		{
		 	get 
		 	{
		 		return (this.Currency != null ? (long?)this.Currency.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase _currency;
        public virtual BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase Currency 
        {
        	get
        	{
        		return _currency;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_currency,value);
        		_currency = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase BusinessLogic_v3.Modules._AutoGen.IShoppingCartBaseAutoGen.Currency 
        {
            get
            {
            	return this.Currency;
            }
            set
            {
            	this.Currency = (BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase) value;
            }
        }
            
/*
		public virtual long? SpecialOfferVoucherCodeID
		{
		 	get 
		 	{
		 		return (this.SpecialOfferVoucherCode != null ? (long?)this.SpecialOfferVoucherCode.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase _specialoffervouchercode;
        public virtual BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase SpecialOfferVoucherCode 
        {
        	get
        	{
        		return _specialoffervouchercode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_specialoffervouchercode,value);
        		_specialoffervouchercode = value;
        	}
        }
            
		//BaseClass/Fields/Property_Normal
        private string _guid;
        
        public virtual string GUID 
        {
        	get
        	{
        		return _guid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_guid,value);
        		_guid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _istemporarycart;
        
        public virtual bool IsTemporaryCart 
        {
        	get
        	{
        		return _istemporarycart;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_istemporarycart,value);
        		_istemporarycart = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ShoppingCartItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase, 
        	BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase,
        	BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase, BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>
        {
            private BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase _item = null;
            public __ShoppingCartItemsCollectionInfo(BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase> Collection
            {
                get { return _item.__collection__ShoppingCartItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase, BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase item, BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase value)
            {
                item.ShoppingCart = value;
            }
        }
        
        private __ShoppingCartItemsCollectionInfo _ShoppingCartItems = null;
        public __ShoppingCartItemsCollectionInfo ShoppingCartItems
        {
            get
            {
                if (_ShoppingCartItems == null)
                    _ShoppingCartItems = new __ShoppingCartItemsCollectionInfo((BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase)this);
                return _ShoppingCartItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase> IShoppingCartBaseAutoGen.ShoppingCartItems
        {
            get {  return this.ShoppingCartItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase> __collection__ShoppingCartItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ShoppingCartItemBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
