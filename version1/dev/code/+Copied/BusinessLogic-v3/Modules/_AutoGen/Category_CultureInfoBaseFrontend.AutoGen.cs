using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.Category_CultureInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.Category_CultureInfoModule.Category_CultureInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.Category_CultureInfoModule.Category_CultureInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.Category_CultureInfoModule.Category_CultureInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Category_CultureInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<Category_CultureInfoBaseFrontend, BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBase>

    {
		
        
        protected Category_CultureInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
