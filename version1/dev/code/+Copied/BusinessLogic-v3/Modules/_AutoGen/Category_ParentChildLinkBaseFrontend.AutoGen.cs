using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.Category_ParentChildLinkModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.Category_ParentChildLinkModule.Category_ParentChildLinkBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.Category_ParentChildLinkModule.Category_ParentChildLinkBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBase item)
        {
        	return BusinessLogic_v3.Frontend.Category_ParentChildLinkModule.Category_ParentChildLinkBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Category_ParentChildLinkBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<Category_ParentChildLinkBaseFrontend, BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBase>

    {
		
        
        protected Category_ParentChildLinkBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
