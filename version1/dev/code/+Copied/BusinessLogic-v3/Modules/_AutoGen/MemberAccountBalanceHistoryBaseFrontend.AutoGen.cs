using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBase item)
        {
        	return BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberAccountBalanceHistoryBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<MemberAccountBalanceHistoryBaseFrontend, BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBase>

    {
		
        
        protected MemberAccountBalanceHistoryBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
