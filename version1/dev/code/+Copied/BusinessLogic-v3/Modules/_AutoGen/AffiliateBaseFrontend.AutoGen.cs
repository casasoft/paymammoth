using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.AffiliateModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.AffiliateModule.AffiliateBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.AffiliateModule.AffiliateBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase item)
        {
        	return BusinessLogic_v3.Frontend.AffiliateModule.AffiliateBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AffiliateBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<AffiliateBaseFrontend, BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase>

    {
		
        
        protected AffiliateBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
