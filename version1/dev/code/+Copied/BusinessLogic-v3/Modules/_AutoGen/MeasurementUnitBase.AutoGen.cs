using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.MeasurementUnitModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "MeasurementUnit")]
    public abstract class MeasurementUnitBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IMeasurementUnitBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static MeasurementUnitBaseFactory Factory
        {
            get
            {
                return MeasurementUnitBaseFactory.Instance; 
            }
        }    
        /*
        public static MeasurementUnitBase CreateNewItem()
        {
            return (MeasurementUnitBase)Factory.CreateNewItem();
        }

		public static MeasurementUnitBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MeasurementUnitBase, MeasurementUnitBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MeasurementUnitBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MeasurementUnitBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MeasurementUnitBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MeasurementUnitBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MeasurementUnitBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __CategorySpecificationsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase, 
        	BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase,
        	BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase, BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>
        {
            private BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase _item = null;
            public __CategorySpecificationsCollectionInfo(BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase> Collection
            {
                get { return _item.__collection__CategorySpecifications; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase, BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>.SetLinkOnItem(BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase item, BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase value)
            {
                item.MeasurementUnit = value;
            }
        }
        
        private __CategorySpecificationsCollectionInfo _CategorySpecifications = null;
        public __CategorySpecificationsCollectionInfo CategorySpecifications
        {
            get
            {
                if (_CategorySpecifications == null)
                    _CategorySpecifications = new __CategorySpecificationsCollectionInfo((BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase)this);
                return _CategorySpecifications;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBase> IMeasurementUnitBaseAutoGen.CategorySpecifications
        {
            get {  return this.CategorySpecifications; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase> __collection__CategorySpecifications
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'CategorySpecificationBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
