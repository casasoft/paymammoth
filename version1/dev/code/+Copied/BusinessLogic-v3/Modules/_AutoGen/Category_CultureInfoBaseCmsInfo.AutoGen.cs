using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.Category_CultureInfoModule;
using BusinessLogic_v3.Cms.Category_CultureInfoModule;
using BusinessLogic_v3.Frontend.Category_CultureInfoModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Category_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>
    {
		public Category_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.Category_CultureInfoModule.Category_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new Category_CultureInfoBaseFrontend FrontendItem
        {
            get { return (Category_CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new Category_CultureInfoBase DbItem
        {
            get
            {
                return (Category_CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; protected set; }

        public CmsPropertyInfo Category { get; protected set; }

        public CmsPropertyInfo TitleSingular { get; protected set; }

        public CmsPropertyInfo TitleSingular_Search { get; protected set; }

        public CmsPropertyInfo TitlePlural { get; protected set; }

        public CmsPropertyInfo TitlePlural_Search { get; protected set; }

        public CmsPropertyInfo TitleSEO { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo MetaKeywords { get; protected set; }

        public CmsPropertyInfo MetaDescription { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
