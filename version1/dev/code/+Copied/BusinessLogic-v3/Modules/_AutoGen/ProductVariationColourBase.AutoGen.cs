using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductVariationColourModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ProductVariationColour")]
    public abstract class ProductVariationColourBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductVariationColourBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
        

			var cultureInfo = __getCurrentCultureInfo(); //since this is a new item, it is imperative that the culture is preloaded
            
            
            base.initPropertiesForNewItem();
        }
    	
  
    	public BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase) __getCultureInfoByLanguage(lang);
        }

     	public static ProductVariationColourBaseFactory Factory
        {
            get
            {
                return ProductVariationColourBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductVariationColourBase CreateNewItem()
        {
            return (ProductVariationColourBase)Factory.CreateNewItem();
        }

		public static ProductVariationColourBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductVariationColourBase, ProductVariationColourBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductVariationColourBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductVariationColourBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductVariationColourBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductVariationColourBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductVariationColourBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Title
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Title;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Title_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Title,value);
                cultureInfo.Title = value;
            }
        }
        
        private string _title;
        public virtual string __Title_cultureBase
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		
        		_title = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private string _hexcolor;
        
        public virtual string HexColor 
        {
        	get
        	{
        		return _hexcolor;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_hexcolor,value);
        		_hexcolor = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ProductVariationsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase, 
        	BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase,
        	BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase, BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>
        {
            private BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase _item = null;
            public __ProductVariationsCollectionInfo(BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase> Collection
            {
                get { return _item.__collection__ProductVariations; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase, BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase item, BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase value)
            {
                item.Colour = value;
            }
        }
        
        private __ProductVariationsCollectionInfo _ProductVariations = null;
        public __ProductVariationsCollectionInfo ProductVariations
        {
            get
            {
                if (_ProductVariations == null)
                    _ProductVariations = new __ProductVariationsCollectionInfo((BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase)this);
                return _ProductVariations;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase> IProductVariationColourBaseAutoGen.ProductVariations
        {
            get {  return this.ProductVariations; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase> __collection__ProductVariations
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductVariationBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ProductVariationColour_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase, 
        	BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase,
        	BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.IProductVariationColour_CultureInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase, BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase>
        {
            private BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase _item = null;
            public __ProductVariationColour_CultureInfosCollectionInfo(BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase> Collection
            {
                get { return _item.__collection__ProductVariationColour_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase, BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase item, BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase value)
            {
                item.ProductVariationColour = value;
            }
        }
        
        private __ProductVariationColour_CultureInfosCollectionInfo _ProductVariationColour_CultureInfos = null;
        public __ProductVariationColour_CultureInfosCollectionInfo ProductVariationColour_CultureInfos
        {
            get
            {
                if (_ProductVariationColour_CultureInfos == null)
                    _ProductVariationColour_CultureInfos = new __ProductVariationColour_CultureInfosCollectionInfo((BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase)this);
                return _ProductVariationColour_CultureInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.IProductVariationColour_CultureInfoBase> IProductVariationColourBaseAutoGen.ProductVariationColour_CultureInfos
        {
            get {  return this.ProductVariationColour_CultureInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBase> __collection__ProductVariationColour_CultureInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductVariationColour_CultureInfoBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
