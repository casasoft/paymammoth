using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.OrderItemModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "OrderItem")]
    public abstract class OrderItemBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IOrderItemBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static OrderItemBaseFactory Factory
        {
            get
            {
                return OrderItemBaseFactory.Instance; 
            }
        }    
        /*
        public static OrderItemBase CreateNewItem()
        {
            return (OrderItemBase)Factory.CreateNewItem();
        }

		public static OrderItemBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<OrderItemBase, OrderItemBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static OrderItemBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static OrderItemBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<OrderItemBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<OrderItemBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<OrderItemBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _quantity;
        
        public virtual int Quantity 
        {
        	get
        	{
        		return _quantity;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_quantity,value);
        		_quantity = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _priceinctaxperunit;
        
        public virtual double PriceIncTaxPerUnit 
        {
        	get
        	{
        		return _priceinctaxperunit;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_priceinctaxperunit,value);
        		_priceinctaxperunit = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _itemreference;
        
        public virtual string ItemReference 
        {
        	get
        	{
        		return _itemreference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemreference,value);
        		_itemreference = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _remarks;
        
        public virtual string Remarks 
        {
        	get
        	{
        		return _remarks;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_remarks,value);
        		_remarks = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _discountperunit;
        
        public virtual double DiscountPerUnit 
        {
        	get
        	{
        		return _discountperunit;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_discountperunit,value);
        		_discountperunit = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private long _itemid;
        
        public virtual long ItemID 
        {
        	get
        	{
        		return _itemid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemid,value);
        		_itemid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _itemtype;
        
        public virtual string ItemType 
        {
        	get
        	{
        		return _itemtype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemtype,value);
        		_itemtype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _shipmentweight;
        
        public virtual double ShipmentWeight 
        {
        	get
        	{
        		return _shipmentweight;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shipmentweight,value);
        		_shipmentweight = value;
        		
        	}
        }
        
/*
		public virtual long? OrderID
		{
		 	get 
		 	{
		 		return (this.Order != null ? (long?)this.Order.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.OrderModule.OrderBase _order;
        public virtual BusinessLogic_v3.Modules.OrderModule.OrderBase Order 
        {
        	get
        	{
        		return _order;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_order,value);
        		_order = value;
        	}
        }
            
        BusinessLogic_v3.Modules.OrderModule.IOrderBase BusinessLogic_v3.Modules._AutoGen.IOrderItemBaseAutoGen.Order 
        {
            get
            {
            	return this.Order;
            }
            set
            {
            	this.Order = (BusinessLogic_v3.Modules.OrderModule.OrderBase) value;
            }
        }
            
/*
		public virtual long? LinkedProductVariationID
		{
		 	get 
		 	{
		 		return (this.LinkedProductVariation != null ? (long?)this.LinkedProductVariation.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase _linkedproductvariation;
        public virtual BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase LinkedProductVariation 
        {
        	get
        	{
        		return _linkedproductvariation;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkedproductvariation,value);
        		_linkedproductvariation = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase BusinessLogic_v3.Modules._AutoGen.IOrderItemBaseAutoGen.LinkedProductVariation 
        {
            get
            {
            	return this.LinkedProductVariation;
            }
            set
            {
            	this.LinkedProductVariation = (BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase) value;
            }
        }
            
/*
		public virtual long? SubscriptionTypePriceID
		{
		 	get 
		 	{
		 		return (this.SubscriptionTypePrice != null ? (long?)this.SubscriptionTypePrice.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase _subscriptiontypeprice;
        public virtual BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase SubscriptionTypePrice 
        {
        	get
        	{
        		return _subscriptiontypeprice;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subscriptiontypeprice,value);
        		_subscriptiontypeprice = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBase BusinessLogic_v3.Modules._AutoGen.IOrderItemBaseAutoGen.SubscriptionTypePrice 
        {
            get
            {
            	return this.SubscriptionTypePrice;
            }
            set
            {
            	this.SubscriptionTypePrice = (BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _autorenewsubscriptiontypeprice;
        
        public virtual bool AutoRenewSubscriptionTypePrice 
        {
        	get
        	{
        		return _autorenewsubscriptiontypeprice;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_autorenewsubscriptiontypeprice,value);
        		_autorenewsubscriptiontypeprice = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _linkedproductvariationcolour;
        
        public virtual string LinkedProductVariationColour 
        {
        	get
        	{
        		return _linkedproductvariationcolour;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkedproductvariationcolour,value);
        		_linkedproductvariationcolour = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _linkedproductvariationsize;
        
        public virtual string LinkedProductVariationSize 
        {
        	get
        	{
        		return _linkedproductvariationsize;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkedproductvariationsize,value);
        		_linkedproductvariationsize = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _taxrate;
        
        public virtual double TaxRate 
        {
        	get
        	{
        		return _taxrate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_taxrate,value);
        		_taxrate = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
