using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ContentTagModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IContentTagBaseAutoGen : BusinessLogic_v3.Classes.DB.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string TagName { get; set; }
		//Iproperty_normal
        string Description { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.ContentTagType TagType { get; set; }
		//Iproperty_normal
        bool UsedInProject { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionManyToManyRightSide  
        
        ICollectionManager<BusinessLogic_v3.Modules.ArticleModule.IArticleBase> Articles { get; }
        
        
        //IBaseClass_CollectionManyToManyRightSide  
        
        ICollectionManager<BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase> ContentTexts { get; }
        
        
        //IBaseClass_CollectionManyToManyRightSide  
        
        ICollectionManager<BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase> EmailTexts { get; }
        
        
 


    	
    	
      
      

    }
}
