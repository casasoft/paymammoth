using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule;
using BusinessLogic_v3.Cms.ImageSpecificSizeInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ImageSpecificSizeInfoBaseCmsFactory_AutoGen : CmsFactoryBase<ImageSpecificSizeInfoBaseCmsInfo, ImageSpecificSizeInfoBase>
    {
       
       public new static ImageSpecificSizeInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ImageSpecificSizeInfoBaseCmsFactory)CmsFactoryBase<ImageSpecificSizeInfoBaseCmsInfo, ImageSpecificSizeInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ImageSpecificSizeInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ImageSpecificSizeInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ImageSpecificSizeInfo";

            this.QueryStringParamID = "ImageSpecificSizeInfoId";

            cmsInfo.TitlePlural = "Image Specific Size Infos";

            cmsInfo.TitleSingular =  "Image Specific Size Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ImageSpecificSizeInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ImageSpecificSizeInfo/";
			UsedInProject = ImageSpecificSizeInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
