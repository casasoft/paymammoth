using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IProductVariationMediaItemBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase ProductVariation {get; set; }

		//Iproperty_normal
        string ImageFilename { get; set; }
		//Iproperty_normal
        string Caption { get; set; }
		//Iproperty_normal
        string ExtraValueChoice { get; set; }
		//Iproperty_normal
        string Title { get; set; }
		//Iproperty_normal
        string Reference { get; set; }
		//Iproperty_normal
        string ImportReference { get; set; }
		//Iproperty_normal
        string Size { get; set; }
		//Iproperty_normal
        string Colour { get; set; }
		//Iproperty_normal
        string VideoLink { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
