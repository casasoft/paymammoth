using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Cms.ProductModule;
using BusinessLogic_v3.Frontend.ProductModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductModule.ProductBase>
    {
		public ProductBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductModule.ProductBase dbItem)
            : base(BusinessLogic_v3.Cms.ProductModule.ProductBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductBaseFrontend FrontendItem
        {
            get { return (ProductBaseFrontend)base.FrontendItem; }

        }
        public new ProductBase DbItem
        {
            get
            {
                return (ProductBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo ReferenceCode { get; protected set; }

        public CmsPropertyInfo SupplierReferenceCode { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo PriceRetailIncTaxPerUnit { get; protected set; }

        public CmsPropertyInfo PriceWholesale { get; protected set; }

        public CmsPropertyInfo Brand { get; protected set; }

        public CmsPropertyInfo ExtraInfo { get; protected set; }

        public CmsPropertyInfo PriceRetailBefore { get; protected set; }

        public CmsPropertyInfo PDFFilename { get; protected set; }

        public CmsPropertyInfo IsSpecialOffer { get; protected set; }

        public CmsPropertyInfo ShowPrice { get; protected set; }

        public CmsPropertyInfo WeightInKg { get; protected set; }

        public CmsPropertyInfo WarrantyInMonths { get; protected set; }

        public CmsPropertyInfo DateAdded { get; protected set; }

        public CmsPropertyInfo IsFeatured { get; protected set; }

        public CmsPropertyInfo ImportReference { get; protected set; }

        public CmsPropertyInfo WarrantyText { get; protected set; }

        public CmsPropertyInfo MetaKeywords { get; protected set; }

        public CmsPropertyInfo CategoryFeatureValues_ForSearch { get; protected set; }

        public CmsPropertyInfo PriceRetailTaxPerUnit { get; protected set; }

        public CmsPropertyInfo TaxRatePercentage { get; protected set; }

        public CmsPropertyInfo ProductSpecificationsHtml { get; protected set; }

        public CmsPropertyInfo DimensionsLengthInCm { get; protected set; }

        public CmsPropertyInfo DimensionsWidthInCm { get; protected set; }

        public CmsPropertyInfo DimensionsHeightInCm { get; protected set; }

        public CmsPropertyInfo DimensionsVolumeInCc { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo CategoryFeatures { get; protected set; }
        

        public CmsCollectionInfo Product_CultureInfos { get; protected set; }
        

        public CmsCollectionInfo CategoryLinks { get; protected set; }
        

        public CmsCollectionInfo ProductCategoryFeatureValues { get; protected set; }
        

        public CmsCollectionInfo ProductCategorySpecificationValues { get; protected set; }
        

        public CmsCollectionInfo ProductVariations { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.CategoryFeatures = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductModule.ProductBase>.GetPropertyBySelector(item => item.CategoryFeatures),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.GetPropertyBySelector(item => item.ItemGroup)));
		
		//InitCollectionBaseOneToMany
		this.Product_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductModule.ProductBase>.GetPropertyBySelector(item => item.Product_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase>.GetPropertyBySelector(item => item.Product)));
		
		//InitCollectionBaseOneToMany
		this.CategoryLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductModule.ProductBase>.GetPropertyBySelector(item => item.CategoryLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase>.GetPropertyBySelector(item => item.Product)));
		
		//InitCollectionBaseOneToMany
		this.ProductCategoryFeatureValues = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductModule.ProductBase>.GetPropertyBySelector(item => item.ProductCategoryFeatureValues),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase>.GetPropertyBySelector(item => item.Product)));
		
		//InitCollectionBaseOneToMany
		this.ProductCategorySpecificationValues = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductModule.ProductBase>.GetPropertyBySelector(item => item.ProductCategorySpecificationValues),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase>.GetPropertyBySelector(item => item.Product)));
		
		//InitCollectionBaseOneToMany
		this.ProductVariations = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductModule.ProductBase>.GetPropertyBySelector(item => item.ProductVariations),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.Product)));


			base.initBasicFields();
          
        }

    }
}
