using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public class FactoriesAutoGen 
    {


// [baseproject_factories_interfaces]

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.AdvertModule.IAdvertBaseFactory _AdvertBaseFactory = BusinessLogic_v3.Modules.AdvertModule.AdvertBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.AdvertModule.IAdvertBaseFactory AdvertFactory
        {
            get { return _AdvertBaseFactory; }
            set { _AdvertBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.AdvertModule.IAdvertBaseFactory AdvertFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.AdvertModule.IAdvertBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.AdvertColumnModule.IAdvertColumnBaseFactory _AdvertColumnBaseFactory = BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.AdvertColumnModule.IAdvertColumnBaseFactory AdvertColumnFactory
        {
            get { return _AdvertColumnBaseFactory; }
            set { _AdvertColumnBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.AdvertColumnModule.IAdvertColumnBaseFactory AdvertColumnFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.AdvertColumnModule.IAdvertColumnBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.AdvertHitModule.IAdvertHitBaseFactory _AdvertHitBaseFactory = BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.AdvertHitModule.IAdvertHitBaseFactory AdvertHitFactory
        {
            get { return _AdvertHitBaseFactory; }
            set { _AdvertHitBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.AdvertHitModule.IAdvertHitBaseFactory AdvertHitFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.AdvertHitModule.IAdvertHitBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.AdvertSlotModule.IAdvertSlotBaseFactory _AdvertSlotBaseFactory = BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.AdvertSlotModule.IAdvertSlotBaseFactory AdvertSlotFactory
        {
            get { return _AdvertSlotBaseFactory; }
            set { _AdvertSlotBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.AdvertSlotModule.IAdvertSlotBaseFactory AdvertSlotFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.AdvertSlotModule.IAdvertSlotBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBaseFactory _AffiliateBaseFactory = BusinessLogic_v3.Modules.AffiliateModule.AffiliateBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBaseFactory AffiliateFactory
        {
            get { return _AffiliateBaseFactory; }
            set { _AffiliateBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBaseFactory AffiliateFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBaseFactory _AffiliatePaymentInfoBaseFactory = BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBaseFactory AffiliatePaymentInfoFactory
        {
            get { return _AffiliatePaymentInfoBaseFactory; }
            set { _AffiliatePaymentInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBaseFactory AffiliatePaymentInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ArticleModule.IArticleBaseFactory _ArticleBaseFactory = BusinessLogic_v3.Modules.ArticleModule.ArticleBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ArticleModule.IArticleBaseFactory ArticleFactory
        {
            get { return _ArticleBaseFactory; }
            set { _ArticleBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ArticleModule.IArticleBaseFactory ArticleFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ArticleModule.IArticleBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBaseFactory _Article_CultureInfoBaseFactory = BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBaseFactory Article_CultureInfoFactory
        {
            get { return _Article_CultureInfoBaseFactory; }
            set { _Article_CultureInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBaseFactory Article_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBaseFactory _Article_ParentChildLinkBaseFactory = BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBaseFactory Article_ParentChildLinkFactory
        {
            get { return _Article_ParentChildLinkBaseFactory; }
            set { _Article_ParentChildLinkBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBaseFactory Article_ParentChildLinkFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBaseFactory _Article_RelatedLinkBaseFactory = BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBaseFactory Article_RelatedLinkFactory
        {
            get { return _Article_RelatedLinkBaseFactory; }
            set { _Article_RelatedLinkBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBaseFactory Article_RelatedLinkFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBaseFactory _ArticleCommentBaseFactory = BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBaseFactory ArticleCommentFactory
        {
            get { return _ArticleCommentBaseFactory; }
            set { _ArticleCommentBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBaseFactory ArticleCommentFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBaseFactory _ArticleMediaItemBaseFactory = BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBaseFactory ArticleMediaItemFactory
        {
            get { return _ArticleMediaItemBaseFactory; }
            set { _ArticleMediaItemBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBaseFactory ArticleMediaItemFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBaseFactory _ArticleRatingBaseFactory = BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBaseFactory ArticleRatingFactory
        {
            get { return _ArticleRatingBaseFactory; }
            set { _ArticleRatingBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBaseFactory ArticleRatingFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBaseFactory _AttachmentBaseFactory = BusinessLogic_v3.Modules.AttachmentModule.AttachmentBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBaseFactory AttachmentFactory
        {
            get { return _AttachmentBaseFactory; }
            set { _AttachmentBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBaseFactory AttachmentFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.AuditLogModule.IAuditLogBaseFactory _AuditLogBaseFactory = BusinessLogic_v3.Modules.AuditLogModule.AuditLogBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.AuditLogModule.IAuditLogBaseFactory AuditLogFactory
        {
            get { return _AuditLogBaseFactory; }
            set { _AuditLogBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.AuditLogModule.IAuditLogBaseFactory AuditLogFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.AuditLogModule.IAuditLogBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.BackgroundImageModule.IBackgroundImageBaseFactory _BackgroundImageBaseFactory = BusinessLogic_v3.Modules.BackgroundImageModule.BackgroundImageBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.BackgroundImageModule.IBackgroundImageBaseFactory BackgroundImageFactory
        {
            get { return _BackgroundImageBaseFactory; }
            set { _BackgroundImageBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.BackgroundImageModule.IBackgroundImageBaseFactory BackgroundImageFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.BackgroundImageModule.IBackgroundImageBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.BannerModule.IBannerBaseFactory _BannerBaseFactory = BusinessLogic_v3.Modules.BannerModule.BannerBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.BannerModule.IBannerBaseFactory BannerFactory
        {
            get { return _BannerBaseFactory; }
            set { _BannerBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.BannerModule.IBannerBaseFactory BannerFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.BannerModule.IBannerBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.BrandModule.IBrandBaseFactory _BrandBaseFactory = BusinessLogic_v3.Modules.BrandModule.BrandBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.BrandModule.IBrandBaseFactory BrandFactory
        {
            get { return _BrandBaseFactory; }
            set { _BrandBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.BrandModule.IBrandBaseFactory BrandFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.BrandModule.IBrandBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBaseFactory _Brand_CultureInfoBaseFactory = BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBaseFactory Brand_CultureInfoFactory
        {
            get { return _Brand_CultureInfoBaseFactory; }
            set { _Brand_CultureInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBaseFactory Brand_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.CategoryModule.ICategoryBaseFactory _CategoryBaseFactory = BusinessLogic_v3.Modules.CategoryModule.CategoryBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.CategoryModule.ICategoryBaseFactory CategoryFactory
        {
            get { return _CategoryBaseFactory; }
            set { _CategoryBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.CategoryModule.ICategoryBaseFactory CategoryFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.CategoryModule.ICategoryBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBaseFactory _Category_CultureInfoBaseFactory = BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBaseFactory Category_CultureInfoFactory
        {
            get { return _Category_CultureInfoBaseFactory; }
            set { _Category_CultureInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBaseFactory Category_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBaseFactory _Category_ParentChildLinkBaseFactory = BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBaseFactory Category_ParentChildLinkFactory
        {
            get { return _Category_ParentChildLinkBaseFactory; }
            set { _Category_ParentChildLinkBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBaseFactory Category_ParentChildLinkFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBaseFactory _CategoryFeatureBaseFactory = BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBaseFactory CategoryFeatureFactory
        {
            get { return _CategoryFeatureBaseFactory; }
            set { _CategoryFeatureBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBaseFactory CategoryFeatureFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.CategoryImageModule.ICategoryImageBaseFactory _CategoryImageBaseFactory = BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.CategoryImageModule.ICategoryImageBaseFactory CategoryImageFactory
        {
            get { return _CategoryImageBaseFactory; }
            set { _CategoryImageBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.CategoryImageModule.ICategoryImageBaseFactory CategoryImageFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.CategoryImageModule.ICategoryImageBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBaseFactory _CategorySpecificationBaseFactory = BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBaseFactory CategorySpecificationFactory
        {
            get { return _CategorySpecificationBaseFactory; }
            set { _CategorySpecificationBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBaseFactory CategorySpecificationFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBaseFactory _ClassifiedBaseFactory = BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBaseFactory ClassifiedFactory
        {
            get { return _ClassifiedBaseFactory; }
            set { _ClassifiedBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBaseFactory ClassifiedFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBaseFactory _CmsUserBaseFactory = BusinessLogic_v3.Modules.CmsUserModule.CmsUserBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBaseFactory CmsUserFactory
        {
            get { return _CmsUserBaseFactory; }
            set { _CmsUserBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBaseFactory CmsUserFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBaseFactory _CmsUserRoleBaseFactory = BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBaseFactory CmsUserRoleFactory
        {
            get { return _CmsUserRoleBaseFactory; }
            set { _CmsUserRoleBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBaseFactory CmsUserRoleFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ContactFormModule.IContactFormBaseFactory _ContactFormBaseFactory = BusinessLogic_v3.Modules.ContactFormModule.ContactFormBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ContactFormModule.IContactFormBaseFactory ContactFormFactory
        {
            get { return _ContactFormBaseFactory; }
            set { _ContactFormBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ContactFormModule.IContactFormBaseFactory ContactFormFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ContactFormModule.IContactFormBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ContentTextModule.IContentTextBaseFactory _ContentTextBaseFactory = BusinessLogic_v3.Modules.ContentTextModule.ContentTextBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ContentTextModule.IContentTextBaseFactory ContentTextFactory
        {
            get { return _ContentTextBaseFactory; }
            set { _ContentTextBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ContentTextModule.IContentTextBaseFactory ContentTextFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ContentTextModule.IContentTextBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBaseFactory _ContentText_CultureInfoBaseFactory = BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBaseFactory ContentText_CultureInfoFactory
        {
            get { return _ContentText_CultureInfoBaseFactory; }
            set { _ContentText_CultureInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBaseFactory ContentText_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBaseFactory _CultureDetailsBaseFactory = BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBaseFactory CultureDetailsFactory
        {
            get { return _CultureDetailsBaseFactory; }
            set { _CultureDetailsBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBaseFactory CultureDetailsFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBaseFactory _CurrencyBaseFactory = BusinessLogic_v3.Modules.CurrencyModule.CurrencyBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBaseFactory CurrencyFactory
        {
            get { return _CurrencyBaseFactory; }
            set { _CurrencyBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBaseFactory CurrencyFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.EmailLogModule.IEmailLogBaseFactory _EmailLogBaseFactory = BusinessLogic_v3.Modules.EmailLogModule.EmailLogBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.EmailLogModule.IEmailLogBaseFactory EmailLogFactory
        {
            get { return _EmailLogBaseFactory; }
            set { _EmailLogBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.EmailLogModule.IEmailLogBaseFactory EmailLogFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.EmailLogModule.IEmailLogBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBaseFactory _EmailTextBaseFactory = BusinessLogic_v3.Modules.EmailTextModule.EmailTextBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBaseFactory EmailTextFactory
        {
            get { return _EmailTextBaseFactory; }
            set { _EmailTextBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBaseFactory EmailTextFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBaseFactory _EmailText_CultureInfoBaseFactory = BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBaseFactory EmailText_CultureInfoFactory
        {
            get { return _EmailText_CultureInfoBaseFactory; }
            set { _EmailText_CultureInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBaseFactory EmailText_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.EventModule.IEventBaseFactory _EventBaseFactory = BusinessLogic_v3.Modules.EventModule.EventBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.EventModule.IEventBaseFactory EventFactory
        {
            get { return _EventBaseFactory; }
            set { _EventBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.EventModule.IEventBaseFactory EventFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.EventModule.IEventBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBaseFactory _EventBasicBaseFactory = BusinessLogic_v3.Modules.EventBasicModule.EventBasicBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBaseFactory EventBasicFactory
        {
            get { return _EventBasicBaseFactory; }
            set { _EventBasicBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBaseFactory EventBasicFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBaseFactory _EventMediaItemBaseFactory = BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBaseFactory EventMediaItemFactory
        {
            get { return _EventMediaItemBaseFactory; }
            set { _EventMediaItemBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBaseFactory EventMediaItemFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.EventSessionModule.IEventSessionBaseFactory _EventSessionBaseFactory = BusinessLogic_v3.Modules.EventSessionModule.EventSessionBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.EventSessionModule.IEventSessionBaseFactory EventSessionFactory
        {
            get { return _EventSessionBaseFactory; }
            set { _EventSessionBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.EventSessionModule.IEventSessionBaseFactory EventSessionFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.EventSessionModule.IEventSessionBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.EventSessionPeriodModule.IEventSessionPeriodBaseFactory _EventSessionPeriodBaseFactory = BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.EventSessionPeriodModule.IEventSessionPeriodBaseFactory EventSessionPeriodFactory
        {
            get { return _EventSessionPeriodBaseFactory; }
            set { _EventSessionPeriodBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.EventSessionPeriodModule.IEventSessionPeriodBaseFactory EventSessionPeriodFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.EventSessionPeriodModule.IEventSessionPeriodBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.EventSessionPeriodDayModule.IEventSessionPeriodDayBaseFactory _EventSessionPeriodDayBaseFactory = BusinessLogic_v3.Modules.EventSessionPeriodDayModule.EventSessionPeriodDayBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.EventSessionPeriodDayModule.IEventSessionPeriodDayBaseFactory EventSessionPeriodDayFactory
        {
            get { return _EventSessionPeriodDayBaseFactory; }
            set { _EventSessionPeriodDayBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.EventSessionPeriodDayModule.IEventSessionPeriodDayBaseFactory EventSessionPeriodDayFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.EventSessionPeriodDayModule.IEventSessionPeriodDayBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBaseFactory _EventSubmissionBaseFactory = BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBaseFactory EventSubmissionFactory
        {
            get { return _EventSubmissionBaseFactory; }
            set { _EventSubmissionBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBaseFactory EventSubmissionFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.FileItemModule.IFileItemBaseFactory _FileItemBaseFactory = BusinessLogic_v3.Modules.FileItemModule.FileItemBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.FileItemModule.IFileItemBaseFactory FileItemFactory
        {
            get { return _FileItemBaseFactory; }
            set { _FileItemBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.FileItemModule.IFileItemBaseFactory FileItemFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.FileItemModule.IFileItemBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBaseFactory _ImageSizingInfoBaseFactory = BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBaseFactory ImageSizingInfoFactory
        {
            get { return _ImageSizingInfoBaseFactory; }
            set { _ImageSizingInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBaseFactory ImageSizingInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBaseFactory _ImageSpecificSizeInfoBaseFactory = BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBaseFactory ImageSpecificSizeInfoFactory
        {
            get { return _ImageSpecificSizeInfoBaseFactory; }
            set { _ImageSpecificSizeInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBaseFactory ImageSpecificSizeInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.KeywordModule.IKeywordBaseFactory _KeywordBaseFactory = BusinessLogic_v3.Modules.KeywordModule.KeywordBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.KeywordModule.IKeywordBaseFactory KeywordFactory
        {
            get { return _KeywordBaseFactory; }
            set { _KeywordBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.KeywordModule.IKeywordBaseFactory KeywordFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.KeywordModule.IKeywordBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.LocationModule.ILocationBaseFactory _LocationBaseFactory = BusinessLogic_v3.Modules.LocationModule.LocationBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.LocationModule.ILocationBaseFactory LocationFactory
        {
            get { return _LocationBaseFactory; }
            set { _LocationBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.LocationModule.ILocationBaseFactory LocationFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.LocationModule.ILocationBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.MeasurementUnitModule.IMeasurementUnitBaseFactory _MeasurementUnitBaseFactory = BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.MeasurementUnitModule.IMeasurementUnitBaseFactory MeasurementUnitFactory
        {
            get { return _MeasurementUnitBaseFactory; }
            set { _MeasurementUnitBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.MeasurementUnitModule.IMeasurementUnitBaseFactory MeasurementUnitFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.MeasurementUnitModule.IMeasurementUnitBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.MemberModule.IMemberBaseFactory _MemberBaseFactory = BusinessLogic_v3.Modules.MemberModule.MemberBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.MemberModule.IMemberBaseFactory MemberFactory
        {
            get { return _MemberBaseFactory; }
            set { _MemberBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.MemberModule.IMemberBaseFactory MemberFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.MemberModule.IMemberBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBaseFactory _MemberAccountBalanceHistoryBaseFactory = BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBaseFactory MemberAccountBalanceHistoryFactory
        {
            get { return _MemberAccountBalanceHistoryBaseFactory; }
            set { _MemberAccountBalanceHistoryBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBaseFactory MemberAccountBalanceHistoryFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBaseFactory _MemberLoginInfoBaseFactory = BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBaseFactory MemberLoginInfoFactory
        {
            get { return _MemberLoginInfoBaseFactory; }
            set { _MemberLoginInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBaseFactory MemberLoginInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBaseFactory _MemberReferralBaseFactory = BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBaseFactory MemberReferralFactory
        {
            get { return _MemberReferralBaseFactory; }
            set { _MemberReferralBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBaseFactory MemberReferralFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.MemberReferralCommissionModule.IMemberReferralCommissionBaseFactory _MemberReferralCommissionBaseFactory = BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.MemberReferralCommissionModule.IMemberReferralCommissionBaseFactory MemberReferralCommissionFactory
        {
            get { return _MemberReferralCommissionBaseFactory; }
            set { _MemberReferralCommissionBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.MemberReferralCommissionModule.IMemberReferralCommissionBaseFactory MemberReferralCommissionFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.MemberReferralCommissionModule.IMemberReferralCommissionBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBaseFactory _MemberResponsibleLimitsBaseFactory = BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBaseFactory MemberResponsibleLimitsFactory
        {
            get { return _MemberResponsibleLimitsBaseFactory; }
            set { _MemberResponsibleLimitsBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBaseFactory MemberResponsibleLimitsFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.IMemberSelfBarringPeriodBaseFactory _MemberSelfBarringPeriodBaseFactory = BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.IMemberSelfBarringPeriodBaseFactory MemberSelfBarringPeriodFactory
        {
            get { return _MemberSelfBarringPeriodBaseFactory; }
            set { _MemberSelfBarringPeriodBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.IMemberSelfBarringPeriodBaseFactory MemberSelfBarringPeriodFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.IMemberSelfBarringPeriodBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBaseFactory _MemberSubscriptionLinkBaseFactory = BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBaseFactory MemberSubscriptionLinkFactory
        {
            get { return _MemberSubscriptionLinkBaseFactory; }
            set { _MemberSubscriptionLinkBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBaseFactory MemberSubscriptionLinkFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.IMemberSubscriptionTypeBaseFactory _MemberSubscriptionTypeBaseFactory = BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.IMemberSubscriptionTypeBaseFactory MemberSubscriptionTypeFactory
        {
            get { return _MemberSubscriptionTypeBaseFactory; }
            set { _MemberSubscriptionTypeBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.IMemberSubscriptionTypeBaseFactory MemberSubscriptionTypeFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.IMemberSubscriptionTypeBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBaseFactory _MemberSubscriptionTypePriceBaseFactory = BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBaseFactory MemberSubscriptionTypePriceFactory
        {
            get { return _MemberSubscriptionTypePriceBaseFactory; }
            set { _MemberSubscriptionTypePriceBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBaseFactory MemberSubscriptionTypePriceFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.NewsletterSubscriptionModule.INewsletterSubscriptionBaseFactory _NewsletterSubscriptionBaseFactory = BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.NewsletterSubscriptionModule.INewsletterSubscriptionBaseFactory NewsletterSubscriptionFactory
        {
            get { return _NewsletterSubscriptionBaseFactory; }
            set { _NewsletterSubscriptionBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.NewsletterSubscriptionModule.INewsletterSubscriptionBaseFactory NewsletterSubscriptionFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.NewsletterSubscriptionModule.INewsletterSubscriptionBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.OrderModule.IOrderBaseFactory _OrderBaseFactory = BusinessLogic_v3.Modules.OrderModule.OrderBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.OrderModule.IOrderBaseFactory OrderFactory
        {
            get { return _OrderBaseFactory; }
            set { _OrderBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.OrderModule.IOrderBaseFactory OrderFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.OrderModule.IOrderBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.IOrderApplicableSpecialOfferLinkBaseFactory _OrderApplicableSpecialOfferLinkBaseFactory = BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.OrderApplicableSpecialOfferLinkBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.IOrderApplicableSpecialOfferLinkBaseFactory OrderApplicableSpecialOfferLinkFactory
        {
            get { return _OrderApplicableSpecialOfferLinkBaseFactory; }
            set { _OrderApplicableSpecialOfferLinkBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.IOrderApplicableSpecialOfferLinkBaseFactory OrderApplicableSpecialOfferLinkFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule.IOrderApplicableSpecialOfferLinkBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBaseFactory _OrderItemBaseFactory = BusinessLogic_v3.Modules.OrderItemModule.OrderItemBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBaseFactory OrderItemFactory
        {
            get { return _OrderItemBaseFactory; }
            set { _OrderItemBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBaseFactory OrderItemFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.PopularSearchModule.IPopularSearchBaseFactory _PopularSearchBaseFactory = BusinessLogic_v3.Modules.PopularSearchModule.PopularSearchBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.PopularSearchModule.IPopularSearchBaseFactory PopularSearchFactory
        {
            get { return _PopularSearchBaseFactory; }
            set { _PopularSearchBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.PopularSearchModule.IPopularSearchBaseFactory PopularSearchFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.PopularSearchModule.IPopularSearchBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductModule.IProductBaseFactory _ProductBaseFactory = BusinessLogic_v3.Modules.ProductModule.ProductBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductModule.IProductBaseFactory ProductFactory
        {
            get { return _ProductBaseFactory; }
            set { _ProductBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductModule.IProductBaseFactory ProductFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductModule.IProductBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBaseFactory _Product_CultureInfoBaseFactory = BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBaseFactory Product_CultureInfoFactory
        {
            get { return _Product_CultureInfoBaseFactory; }
            set { _Product_CultureInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBaseFactory Product_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBaseFactory _ProductCategoryBaseFactory = BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBaseFactory ProductCategoryFactory
        {
            get { return _ProductCategoryBaseFactory; }
            set { _ProductCategoryBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBaseFactory ProductCategoryFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBaseFactory _ProductCategoryFeatureValueBaseFactory = BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBaseFactory ProductCategoryFeatureValueFactory
        {
            get { return _ProductCategoryFeatureValueBaseFactory; }
            set { _ProductCategoryFeatureValueBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBaseFactory ProductCategoryFeatureValueFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBaseFactory _ProductCategorySpecificationValueBaseFactory = BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBaseFactory ProductCategorySpecificationValueFactory
        {
            get { return _ProductCategorySpecificationValueBaseFactory; }
            set { _ProductCategorySpecificationValueBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBaseFactory ProductCategorySpecificationValueFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.IProductRelatedProductLinkBaseFactory _ProductRelatedProductLinkBaseFactory = BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.ProductRelatedProductLinkBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.IProductRelatedProductLinkBaseFactory ProductRelatedProductLinkFactory
        {
            get { return _ProductRelatedProductLinkBaseFactory; }
            set { _ProductRelatedProductLinkBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.IProductRelatedProductLinkBaseFactory ProductRelatedProductLinkFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.IProductRelatedProductLinkBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBaseFactory _ProductVariationBaseFactory = BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBaseFactory ProductVariationFactory
        {
            get { return _ProductVariationBaseFactory; }
            set { _ProductVariationBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBaseFactory ProductVariationFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfoBaseFactory _ProductVariation_CultureInfoBaseFactory = BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfoBaseFactory ProductVariation_CultureInfoFactory
        {
            get { return _ProductVariation_CultureInfoBaseFactory; }
            set { _ProductVariation_CultureInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfoBaseFactory ProductVariation_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductVariationColourModule.IProductVariationColourBaseFactory _ProductVariationColourBaseFactory = BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductVariationColourModule.IProductVariationColourBaseFactory ProductVariationColourFactory
        {
            get { return _ProductVariationColourBaseFactory; }
            set { _ProductVariationColourBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductVariationColourModule.IProductVariationColourBaseFactory ProductVariationColourFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductVariationColourModule.IProductVariationColourBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.IProductVariationColour_CultureInfoBaseFactory _ProductVariationColour_CultureInfoBaseFactory = BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.ProductVariationColour_CultureInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.IProductVariationColour_CultureInfoBaseFactory ProductVariationColour_CultureInfoFactory
        {
            get { return _ProductVariationColour_CultureInfoBaseFactory; }
            set { _ProductVariationColour_CultureInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.IProductVariationColour_CultureInfoBaseFactory ProductVariationColour_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule.IProductVariationColour_CultureInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBaseFactory _ProductVariationMediaItemBaseFactory = BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBaseFactory ProductVariationMediaItemFactory
        {
            get { return _ProductVariationMediaItemBaseFactory; }
            set { _ProductVariationMediaItemBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBaseFactory ProductVariationMediaItemFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductVariationSizeModule.IProductVariationSizeBaseFactory _ProductVariationSizeBaseFactory = BusinessLogic_v3.Modules.ProductVariationSizeModule.ProductVariationSizeBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductVariationSizeModule.IProductVariationSizeBaseFactory ProductVariationSizeFactory
        {
            get { return _ProductVariationSizeBaseFactory; }
            set { _ProductVariationSizeBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductVariationSizeModule.IProductVariationSizeBaseFactory ProductVariationSizeFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductVariationSizeModule.IProductVariationSizeBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductVersionModule.IProductVersionBaseFactory _ProductVersionBaseFactory = BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductVersionModule.IProductVersionBaseFactory ProductVersionFactory
        {
            get { return _ProductVersionBaseFactory; }
            set { _ProductVersionBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductVersionModule.IProductVersionBaseFactory ProductVersionFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductVersionModule.IProductVersionBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ProductVersionMediaItemModule.IProductVersionMediaItemBaseFactory _ProductVersionMediaItemBaseFactory = BusinessLogic_v3.Modules.ProductVersionMediaItemModule.ProductVersionMediaItemBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ProductVersionMediaItemModule.IProductVersionMediaItemBaseFactory ProductVersionMediaItemFactory
        {
            get { return _ProductVersionMediaItemBaseFactory; }
            set { _ProductVersionMediaItemBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ProductVersionMediaItemModule.IProductVersionMediaItemBaseFactory ProductVersionMediaItemFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ProductVersionMediaItemModule.IProductVersionMediaItemBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.RoutingInfoModule.IRoutingInfoBaseFactory _RoutingInfoBaseFactory = BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.RoutingInfoModule.IRoutingInfoBaseFactory RoutingInfoFactory
        {
            get { return _RoutingInfoBaseFactory; }
            set { _RoutingInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.RoutingInfoModule.IRoutingInfoBaseFactory RoutingInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.RoutingInfoModule.IRoutingInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.SettingModule.ISettingBaseFactory _SettingBaseFactory = BusinessLogic_v3.Modules.SettingModule.SettingBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.SettingModule.ISettingBaseFactory SettingFactory
        {
            get { return _SettingBaseFactory; }
            set { _SettingBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.SettingModule.ISettingBaseFactory SettingFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.SettingModule.ISettingBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ShippingMethodModule.IShippingMethodBaseFactory _ShippingMethodBaseFactory = BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ShippingMethodModule.IShippingMethodBaseFactory ShippingMethodFactory
        {
            get { return _ShippingMethodBaseFactory; }
            set { _ShippingMethodBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ShippingMethodModule.IShippingMethodBaseFactory ShippingMethodFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ShippingMethodModule.IShippingMethodBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ShippingRatePriceModule.IShippingRatePriceBaseFactory _ShippingRatePriceBaseFactory = BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ShippingRatePriceModule.IShippingRatePriceBaseFactory ShippingRatePriceFactory
        {
            get { return _ShippingRatePriceBaseFactory; }
            set { _ShippingRatePriceBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ShippingRatePriceModule.IShippingRatePriceBaseFactory ShippingRatePriceFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ShippingRatePriceModule.IShippingRatePriceBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ShippingRatesGroupModule.IShippingRatesGroupBaseFactory _ShippingRatesGroupBaseFactory = BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ShippingRatesGroupModule.IShippingRatesGroupBaseFactory ShippingRatesGroupFactory
        {
            get { return _ShippingRatesGroupBaseFactory; }
            set { _ShippingRatesGroupBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ShippingRatesGroupModule.IShippingRatesGroupBaseFactory ShippingRatesGroupFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ShippingRatesGroupModule.IShippingRatesGroupBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBaseFactory _ShoppingCartBaseFactory = BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBaseFactory ShoppingCartFactory
        {
            get { return _ShoppingCartBaseFactory; }
            set { _ShoppingCartBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBaseFactory ShoppingCartFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBaseFactory _ShoppingCartItemBaseFactory = BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBaseFactory ShoppingCartItemFactory
        {
            get { return _ShoppingCartItemBaseFactory; }
            set { _ShoppingCartItemBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBaseFactory ShoppingCartItemFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBaseFactory _SpecialOfferBaseFactory = BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBaseFactory SpecialOfferFactory
        {
            get { return _SpecialOfferBaseFactory; }
            set { _SpecialOfferBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBaseFactory SpecialOfferFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.ISpecialOffer_CultureInfoBaseFactory _SpecialOffer_CultureInfoBaseFactory = BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.ISpecialOffer_CultureInfoBaseFactory SpecialOffer_CultureInfoFactory
        {
            get { return _SpecialOffer_CultureInfoBaseFactory; }
            set { _SpecialOffer_CultureInfoBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.ISpecialOffer_CultureInfoBaseFactory SpecialOffer_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.ISpecialOffer_CultureInfoBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.ISpecialOfferVoucherCodeBaseFactory _SpecialOfferVoucherCodeBaseFactory = BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.ISpecialOfferVoucherCodeBaseFactory SpecialOfferVoucherCodeFactory
        {
            get { return _SpecialOfferVoucherCodeBaseFactory; }
            set { _SpecialOfferVoucherCodeBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.ISpecialOfferVoucherCodeBaseFactory SpecialOfferVoucherCodeFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.ISpecialOfferVoucherCodeBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.TestimonialModule.ITestimonialBaseFactory _TestimonialBaseFactory = BusinessLogic_v3.Modules.TestimonialModule.TestimonialBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.TestimonialModule.ITestimonialBaseFactory TestimonialFactory
        {
            get { return _TestimonialBaseFactory; }
            set { _TestimonialBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.TestimonialModule.ITestimonialBaseFactory TestimonialFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.TestimonialModule.ITestimonialBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.TicketModule.ITicketBaseFactory _TicketBaseFactory = BusinessLogic_v3.Modules.TicketModule.TicketBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.TicketModule.ITicketBaseFactory TicketFactory
        {
            get { return _TicketBaseFactory; }
            set { _TicketBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.TicketModule.ITicketBaseFactory TicketFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.TicketModule.ITicketBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBaseFactory _TicketCommentBaseFactory = BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBaseFactory TicketCommentFactory
        {
            get { return _TicketCommentBaseFactory; }
            set { _TicketCommentBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBaseFactory TicketCommentFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBaseFactory>(); }
        } 

		//BaseProject_Factory_InterfaceDeclaration
		/*
        private static BusinessLogic_v3.Modules.VacancyModule.IVacancyBaseFactory _VacancyBaseFactory = BusinessLogic_v3.Modules.VacancyModule.VacancyBaseFactory.Instance;
        public static BusinessLogic_v3.Modules.VacancyModule.IVacancyBaseFactory VacancyFactory
        {
            get { return _VacancyBaseFactory; }
            set { _VacancyBaseFactory = value; }
        } 
        */
        public static BusinessLogic_v3.Modules.VacancyModule.IVacancyBaseFactory VacancyFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<BusinessLogic_v3.Modules.VacancyModule.IVacancyBaseFactory>(); }
        } 



    }
}
