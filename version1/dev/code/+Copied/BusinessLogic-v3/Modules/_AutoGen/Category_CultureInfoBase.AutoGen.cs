using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.Category_CultureInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Category_CultureInfo")]
    public abstract class Category_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ICategory_CultureInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
      , IMultilingualContentInfo
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static Category_CultureInfoBaseFactory Factory
        {
            get
            {
                return Category_CultureInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static Category_CultureInfoBase CreateNewItem()
        {
            return (Category_CultureInfoBase)Factory.CreateNewItem();
        }

		public static Category_CultureInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<Category_CultureInfoBase, Category_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static Category_CultureInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static Category_CultureInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<Category_CultureInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<Category_CultureInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<Category_CultureInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? CultureInfoID
		{
		 	get 
		 	{
		 		return (this.CultureInfo != null ? (long?)this.CultureInfo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase _cultureinfo;
        public virtual BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase CultureInfo 
        {
        	get
        	{
        		return _cultureinfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cultureinfo,value);
        		_cultureinfo = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase BusinessLogic_v3.Modules._AutoGen.ICategory_CultureInfoBaseAutoGen.CultureInfo 
        {
            get
            {
            	return this.CultureInfo;
            }
            set
            {
            	this.CultureInfo = (BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase) value;
            }
        }
            
/*
		public virtual long? CategoryID
		{
		 	get 
		 	{
		 		return (this.Category != null ? (long?)this.Category.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _category;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase Category 
        {
        	get
        	{
        		return _category;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_category,value);
        		_category = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.ICategory_CultureInfoBaseAutoGen.Category 
        {
            get
            {
            	return this.Category;
            }
            set
            {
            	this.Category = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _titlesingular;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string TitleSingular 
        {
        	get
        	{
        		return _titlesingular;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_titlesingular,value);
        		_titlesingular = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _titlesingular_search;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string TitleSingular_Search 
        {
        	get
        	{
        		return _titlesingular_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_titlesingular_search,value);
        		_titlesingular_search = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _titleplural;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string TitlePlural 
        {
        	get
        	{
        		return _titleplural;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_titleplural,value);
        		_titleplural = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _titleplural_search;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string TitlePlural_Search 
        {
        	get
        	{
        		return _titleplural_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_titleplural_search,value);
        		_titleplural_search = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _titleseo;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string TitleSEO 
        {
        	get
        	{
        		return _titleseo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_titleseo,value);
        		_titleseo = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _metakeywords;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string MetaKeywords 
        {
        	get
        	{
        		return _metakeywords;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metakeywords,value);
        		_metakeywords = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _metadescription;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string MetaDescription 
        {
        	get
        	{
        		return _metadescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metadescription,value);
        		_metadescription = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)




#region IMultilingualContentInfo Members

        long IMultilingualContentInfo.CultureInfoID
        {
            get
            {                          
            	if (this.CultureInfo != null)
                	return this.CultureInfo.ID;
               	else
               		return 0;
            }
        }
  		BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject IMultilingualContentInfo.GetLinkedItem()
        {
            return this.Category;
        }
#endregion



#endregion       

		

    }
}
