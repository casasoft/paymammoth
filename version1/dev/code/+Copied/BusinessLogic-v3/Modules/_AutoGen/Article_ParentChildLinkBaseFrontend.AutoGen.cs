using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.Article_ParentChildLinkModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.Article_ParentChildLinkModule.Article_ParentChildLinkBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.Article_ParentChildLinkModule.Article_ParentChildLinkBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBase item)
        {
        	return BusinessLogic_v3.Frontend.Article_ParentChildLinkModule.Article_ParentChildLinkBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Article_ParentChildLinkBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<Article_ParentChildLinkBaseFrontend, BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBase>

    {
		
        
        protected Article_ParentChildLinkBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
