using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
using BusinessLogic_v3.Cms.ImageSizingInfoModule;
using BusinessLogic_v3.Frontend.ImageSizingInfoModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ImageSizingInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase>
    {
		public ImageSizingInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.ImageSizingInfoModule.ImageSizingInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ImageSizingInfoBaseFrontend FrontendItem
        {
            get { return (ImageSizingInfoBaseFrontend)base.FrontendItem; }

        }
        public new ImageSizingInfoBase DbItem
        {
            get
            {
                return (ImageSizingInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo MaximumWidth { get; protected set; }

        public CmsPropertyInfo MaximumHeight { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo SizingInfos { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.SizingInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase>.GetPropertyBySelector(item => item.SizingInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase>.GetPropertyBySelector(item => item.ImageSizingInfo)));


			base.initBasicFields();
          
        }

    }
}
