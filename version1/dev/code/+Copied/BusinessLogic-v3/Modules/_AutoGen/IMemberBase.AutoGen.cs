using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IMemberBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        DateTime DateRegistered { get; set; }
		//Iproperty_normal
        string FirstName { get; set; }
		//Iproperty_normal
        string LastName { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.GENDER Gender { get; set; }
		//Iproperty_normal
        DateTime? DateOfBirth { get; set; }
		//Iproperty_normal
        string Address1 { get; set; }
		//Iproperty_normal
        string Address2 { get; set; }
		//Iproperty_normal
        string Address3 { get; set; }
		//Iproperty_normal
        string Locality { get; set; }
		//Iproperty_normal
        string State { get; set; }
		//Iproperty_normal
        string PostCode { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? Country { get; set; }
		//Iproperty_normal
        string IDCard { get; set; }
		//Iproperty_normal
        DateTime? LastLoggedIn { get; set; }
		//Iproperty_normal
        DateTime? _LastLoggedIn_New { get; set; }
		//Iproperty_normal
        bool Accepted { get; set; }
		//Iproperty_normal
        string Email { get; set; }
		//Iproperty_normal
        string Telephone { get; set; }
		//Iproperty_normal
        string Mobile { get; set; }
		//Iproperty_normal
        string Fax { get; set; }
		//Iproperty_normal
        string Remarks { get; set; }
		//Iproperty_normal
        string Website { get; set; }
		//Iproperty_normal
        DateTime? ActivatedOn { get; set; }
		//Iproperty_normal
        string ActivatedIP { get; set; }
		//Iproperty_normal
        string ActivationCode { get; set; }
		//Iproperty_normal
        string SessionGUID { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase PreferredCultureInfo {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase ReferredByMember {get; set; }

		//Iproperty_normal
        bool AccountTerminated { get; set; }
		//Iproperty_normal
        DateTime? AccountTerminatedOn { get; set; }
		//Iproperty_normal
        string AccountTerminatedIP { get; set; }
		//Iproperty_normal
        string ForgottenPassCode { get; set; }
		//Iproperty_normal
        bool SubscribedToNewsletter { get; set; }
		//Iproperty_normal
        string HowDidYouFindUs { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase LinkedAffiliate {get; set; }

		//Iproperty_normal
        string CompanyName { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE PasswordEncryptionType { get; set; }
		//Iproperty_normal
        string PasswordSalt { get; set; }
		//Iproperty_normal
        string Title { get; set; }
		//Iproperty_normal
        bool SubscriptionNotification1Sent { get; set; }
		//Iproperty_normal
        bool SubscriptionNotification2Sent { get; set; }
		//Iproperty_normal
        bool Paid { get; set; }
		//Iproperty_normal
        bool Activated { get; set; }
		//Iproperty_normal
        string MiddleName { get; set; }
		//Iproperty_normal
        bool SubscriptionExpiryNotificationSent { get; set; }
		//Iproperty_normal
        bool ReferralSuccessful { get; set; }
		//Iproperty_normal
        string ReferredFromUrl { get; set; }
		//Iproperty_normal
        bool Blocked { get; set; }
		//Iproperty_normal
        DateTime? SelfExcludedUntil { get; set; }
		//Iproperty_normal
        DateTime? SelfExclusionSetOn { get; set; }
		//Iproperty_normal
        string SelfExclusionSetByIp { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase AssignedToTicketingSystemSupportUser {get; set; }

		//Iproperty_normal
        string PreferredTimezoneID { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.MEMBER_BLOCKED_REASON BlockedReason { get; set; }
		//Iproperty_normal
        DateTime BlockedOn { get; set; }
		//Iproperty_normal
        DateTime? BlockedUntil { get; set; }
		//Iproperty_normal
        string BlockedCustomMessage { get; set; }
		//Iproperty_normal
        int CurrentTotalInvalidLoginAttempts { get; set; }
		//Iproperty_normal
        bool KYCVerified { get; set; }
		//Iproperty_normal
        string KYCProofOfAddressFilename { get; set; }
		//Iproperty_normal
        string KYCProofOfIdentityFilename { get; set; }
		//Iproperty_normal
        bool KYCProcessStarted { get; set; }
		//Iproperty_normal
        DateTime? KYCProcessStartedOn { get; set; }
		//Iproperty_normal
        bool IsFeatured { get; set; }
		//Iproperty_normal
        string ImageFilename { get; set; }
		//Iproperty_normal
        string CompanyProfile { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBase AutoRenewSubscriptionTypePrice {get; set; }

		//Iproperty_normal
        string AutoRenewLastMessage { get; set; }
		//Iproperty_normal
        string KYCProcessRemarks { get; set; }
		//Iproperty_normal
        DateTime? KYCVerifiedOn { get; set; }
		//Iproperty_normal
        DateTime? NextRenewableAccountBalanceTopupOn { get; set; }
		//Iproperty_normal
        string CompanyEmail { get; set; }
		//Iproperty_normal
        string CompanyTelephone { get; set; }
		//Iproperty_normal
        string VAT { get; set; }
		//Iproperty_normal
        string Nationality { get; set; }
		//Iproperty_normal
        string PassportNo { get; set; }
		//Iproperty_normal
        string PassportPlaceOfIssue { get; set; }
		//Iproperty_normal
        string AccountRegistrationIP { get; set; }
		//Iproperty_normal
        string IDCardPlaceOfIssue { get; set; }
		//Iproperty_normal
        string DrivingLicense { get; set; }
		//Iproperty_normal
        string DrivingLicensePlaceOfIssue { get; set; }
		//Iproperty_normal
        bool RegisteredThroughSocialMedium { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.PERSONAL_ACCOUNT_TYPE PersonalAccountType { get; set; }
		//Iproperty_normal
        long? TwitterUserID { get; set; }
		//Iproperty_normal
        string TwitterScreenName { get; set; }
		//Iproperty_normal
        bool AllowPromotions { get; set; }
		//Iproperty_normal
        string JobPosition { get; set; }
		//Iproperty_normal
        bool IsContactPerson { get; set; }
		//Iproperty_normal
        string MemberType { get; set; }
		//Iproperty_normal
        bool IsTestMember { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase> Classifieds { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase> ContactForms { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> Members { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBase> AccountBalanceHistoryItems { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBase> LoginInfo { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBase> SentReferrals { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBase> RegisteredReferrals { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBase> ResponsibleLimits { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.IMemberSelfBarringPeriodBase> MemberSelfBarringPeriods { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase> MemberSubscriptionLinks { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.OrderModule.IOrderBase> Orders { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBase> ShoppingCarts { get; }

        //IBaseClass_CollectionManyToManyLeftSide  
        ICollectionManager<BusinessLogic_v3.Modules.CategoryModule.ICategoryBase> SubscribedCategories { get; }

        //IBaseClass_CollectionManyToManyLeftSide  
        ICollectionManager<BusinessLogic_v3.Modules.EventModule.IEventBase> Events { get; }

        //IBaseClass_CollectionManyToManyLeftSide  
        ICollectionManager<BusinessLogic_v3.Modules.ArticleModule.IArticleBase> ArticlesRead { get; }

 
		

    	
    	
      
      

    }
}
