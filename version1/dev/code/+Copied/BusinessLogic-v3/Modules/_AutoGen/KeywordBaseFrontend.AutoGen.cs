using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.KeywordModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.KeywordModule.KeywordBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.KeywordModule.IKeywordBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.KeywordModule.KeywordBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.KeywordModule.IKeywordBase item)
        {
        	return BusinessLogic_v3.Frontend.KeywordModule.KeywordBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class KeywordBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<KeywordBaseFrontend, BusinessLogic_v3.Modules.KeywordModule.IKeywordBase>

    {
		
        
        protected KeywordBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
