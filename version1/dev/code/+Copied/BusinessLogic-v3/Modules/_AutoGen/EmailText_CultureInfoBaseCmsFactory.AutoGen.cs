using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.EmailText_CultureInfoModule;
using BusinessLogic_v3.Cms.EmailText_CultureInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class EmailText_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<EmailText_CultureInfoBaseCmsInfo, EmailText_CultureInfoBase>
    {
       
       public new static EmailText_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EmailText_CultureInfoBaseCmsFactory)CmsFactoryBase<EmailText_CultureInfoBaseCmsInfo, EmailText_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = EmailText_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EmailText_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EmailText_CultureInfo";

            this.QueryStringParamID = "EmailText_CultureInfoId";

            cmsInfo.TitlePlural = "Email Text _ Culture Infos";

            cmsInfo.TitleSingular =  "Email Text _ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EmailText_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "EmailText_CultureInfo/";
			UsedInProject = EmailText_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
