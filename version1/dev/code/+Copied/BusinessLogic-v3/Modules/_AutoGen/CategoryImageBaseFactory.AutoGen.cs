using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CategoryImageModule;

//DbObjectBuilder v8

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class CategoryImageBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<CategoryImageBase, ICategoryImageBase>, ICategoryImageBaseFactoryAutoGen
    
    {
    
        public CategoryImageBaseFactoryAutoGen()
        {
        	
            
        }
        
		static CategoryImageBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static CategoryImageBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<CategoryImageBaseFactory>(null);
            }
        }
        
		public IQueryOver<CategoryImageBase, CategoryImageBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<CategoryImageBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
