using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.BrandModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IBrandBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Title { get; set; }
		//IProperty_Multilingual
	   string Description { get; set ;}
		//Iproperty_normal
        string BrandLogoFilename { get; set; }
		//Iproperty_normal
        bool IsFeatured { get; set; }
		//Iproperty_normal
        string Url { get; set; }
		//Iproperty_normal
        string ImportReference { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.HREF_TARGET HrefTarget { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBase> Brand_CultureInfos { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ProductModule.IProductBase> Products { get; }

 
		

    	
    	
      
      

    }
}
