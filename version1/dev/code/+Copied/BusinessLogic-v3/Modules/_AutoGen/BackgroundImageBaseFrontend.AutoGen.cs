using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.BackgroundImageModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.BackgroundImageModule.BackgroundImageBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.BackgroundImageModule.IBackgroundImageBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.BackgroundImageModule.BackgroundImageBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.BackgroundImageModule.IBackgroundImageBase item)
        {
        	return BusinessLogic_v3.Frontend.BackgroundImageModule.BackgroundImageBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class BackgroundImageBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<BackgroundImageBaseFrontend, BusinessLogic_v3.Modules.BackgroundImageModule.IBackgroundImageBase>

    {
		
        
        protected BackgroundImageBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
