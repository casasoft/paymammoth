using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.AdvertColumnModule;
using BusinessLogic_v3.Cms.AdvertColumnModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class AdvertColumnBaseCmsFactory_AutoGen : CmsFactoryBase<AdvertColumnBaseCmsInfo, AdvertColumnBase>
    {
       
       public new static AdvertColumnBaseCmsFactory Instance
	    {
	         get
	         {
                 return (AdvertColumnBaseCmsFactory)CmsFactoryBase<AdvertColumnBaseCmsInfo, AdvertColumnBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = AdvertColumnBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "AdvertColumn.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "AdvertColumn";

            this.QueryStringParamID = "AdvertColumnId";

            cmsInfo.TitlePlural = "Advert Columns";

            cmsInfo.TitleSingular =  "Advert Column";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public AdvertColumnBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "AdvertColumn/";
			UsedInProject = AdvertColumnBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
