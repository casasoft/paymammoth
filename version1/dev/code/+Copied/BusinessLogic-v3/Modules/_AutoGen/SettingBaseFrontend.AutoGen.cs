using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.SettingModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.SettingModule.SettingBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.SettingModule.ISettingBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.SettingModule.SettingBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.SettingModule.ISettingBase item)
        {
        	return BusinessLogic_v3.Frontend.SettingModule.SettingBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class SettingBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<SettingBaseFrontend, BusinessLogic_v3.Modules.SettingModule.ISettingBase>

    {
		
        
        protected SettingBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
