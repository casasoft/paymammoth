using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductCategoryModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ProductCategory")]
    public abstract class ProductCategoryBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductCategoryBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ProductCategoryBaseFactory Factory
        {
            get
            {
                return ProductCategoryBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductCategoryBase CreateNewItem()
        {
            return (ProductCategoryBase)Factory.CreateNewItem();
        }

		public static ProductCategoryBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductCategoryBase, ProductCategoryBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductCategoryBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductCategoryBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductCategoryBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductCategoryBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductCategoryBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? ProductID
		{
		 	get 
		 	{
		 		return (this.Product != null ? (long?)this.Product.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _product;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase Product 
        {
        	get
        	{
        		return _product;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_product,value);
        		_product = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.IProductCategoryBaseAutoGen.Product 
        {
            get
            {
            	return this.Product;
            }
            set
            {
            	this.Product = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
/*
		public virtual long? CategoryID
		{
		 	get 
		 	{
		 		return (this.Category != null ? (long?)this.Category.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _category;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase Category 
        {
        	get
        	{
        		return _category;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_category,value);
        		_category = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.IProductCategoryBaseAutoGen.Category 
        {
            get
            {
            	return this.Category;
            }
            set
            {
            	this.Category = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
