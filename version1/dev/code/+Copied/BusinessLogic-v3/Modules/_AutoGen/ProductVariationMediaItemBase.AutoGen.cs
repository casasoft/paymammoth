using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ProductVariationMediaItem")]
    public abstract class ProductVariationMediaItemBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductVariationMediaItemBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ProductVariationMediaItemBaseFactory Factory
        {
            get
            {
                return ProductVariationMediaItemBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductVariationMediaItemBase CreateNewItem()
        {
            return (ProductVariationMediaItemBase)Factory.CreateNewItem();
        }

		public static ProductVariationMediaItemBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductVariationMediaItemBase, ProductVariationMediaItemBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductVariationMediaItemBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductVariationMediaItemBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductVariationMediaItemBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductVariationMediaItemBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductVariationMediaItemBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? ProductVariationID
		{
		 	get 
		 	{
		 		return (this.ProductVariation != null ? (long?)this.ProductVariation.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase _productvariation;
        public virtual BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase ProductVariation 
        {
        	get
        	{
        		return _productvariation;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_productvariation,value);
        		_productvariation = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase BusinessLogic_v3.Modules._AutoGen.IProductVariationMediaItemBaseAutoGen.ProductVariation 
        {
            get
            {
            	return this.ProductVariation;
            }
            set
            {
            	this.ProductVariation = (BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _imagefilename;
        
        public virtual string ImageFilename 
        {
        	get
        	{
        		return _imagefilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagefilename,value);
        		_imagefilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _caption;
        
        public virtual string Caption 
        {
        	get
        	{
        		return _caption;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_caption,value);
        		_caption = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _extravaluechoice;
        
        public virtual string ExtraValueChoice 
        {
        	get
        	{
        		return _extravaluechoice;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_extravaluechoice,value);
        		_extravaluechoice = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _reference;
        
        public virtual string Reference 
        {
        	get
        	{
        		return _reference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_reference,value);
        		_reference = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _importreference;
        
        public virtual string ImportReference 
        {
        	get
        	{
        		return _importreference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_importreference,value);
        		_importreference = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _size;
        
        public virtual string Size 
        {
        	get
        	{
        		return _size;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_size,value);
        		_size = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _colour;
        
        public virtual string Colour 
        {
        	get
        	{
        		return _colour;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_colour,value);
        		_colour = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _videolink;
        
        public virtual string VideoLink 
        {
        	get
        	{
        		return _videolink;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_videolink,value);
        		_videolink = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
