using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.MemberReferralCommissionModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "MemberReferralCommission")]
    public abstract class MemberReferralCommissionBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IMemberReferralCommissionBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static MemberReferralCommissionBaseFactory Factory
        {
            get
            {
                return MemberReferralCommissionBaseFactory.Instance; 
            }
        }    
        /*
        public static MemberReferralCommissionBase CreateNewItem()
        {
            return (MemberReferralCommissionBase)Factory.CreateNewItem();
        }

		public static MemberReferralCommissionBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MemberReferralCommissionBase, MemberReferralCommissionBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MemberReferralCommissionBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MemberReferralCommissionBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MemberReferralCommissionBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MemberReferralCommissionBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MemberReferralCommissionBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private DateTime _dateissued;
        
        public virtual DateTime DateIssued 
        {
        	get
        	{
        		return _dateissued;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dateissued,value);
        		_dateissued = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _amount;
        
        public virtual double Amount 
        {
        	get
        	{
        		return _amount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_amount,value);
        		_amount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.MEMBER_REFERRAL_COMMISSION_STATUS _status;
        
        public virtual BusinessLogic_v3.Enums.MEMBER_REFERRAL_COMMISSION_STATUS Status 
        {
        	get
        	{
        		return _status;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_status,value);
        		_status = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _paidon;
        
        public virtual DateTime PaidOn 
        {
        	get
        	{
        		return _paidon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paidon,value);
        		_paidon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.MEMBER_REFERRAL_COMMISSION_PAID_BY _paidby;
        
        public virtual BusinessLogic_v3.Enums.MEMBER_REFERRAL_COMMISSION_PAID_BY PaidBy 
        {
        	get
        	{
        		return _paidby;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paidby,value);
        		_paidby = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentremarks;
        
        public virtual string PaymentRemarks 
        {
        	get
        	{
        		return _paymentremarks;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentremarks,value);
        		_paymentremarks = value;
        		
        	}
        }
        
/*
		public virtual long? MemberReferralID
		{
		 	get 
		 	{
		 		return (this.MemberReferral != null ? (long?)this.MemberReferral.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase _memberreferral;
        public virtual BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase MemberReferral 
        {
        	get
        	{
        		return _memberreferral;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_memberreferral,value);
        		_memberreferral = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBase BusinessLogic_v3.Modules._AutoGen.IMemberReferralCommissionBaseAutoGen.MemberReferral 
        {
            get
            {
            	return this.MemberReferral;
            }
            set
            {
            	this.MemberReferral = (BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
