using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.Product_CultureInfoModule;
using BusinessLogic_v3.Cms.Product_CultureInfoModule;
using BusinessLogic_v3.Frontend.Product_CultureInfoModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Product_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase>
    {
		public Product_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.Product_CultureInfoModule.Product_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new Product_CultureInfoBaseFrontend FrontendItem
        {
            get { return (Product_CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new Product_CultureInfoBase DbItem
        {
            get
            {
                return (Product_CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; protected set; }

        public CmsPropertyInfo Product { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
