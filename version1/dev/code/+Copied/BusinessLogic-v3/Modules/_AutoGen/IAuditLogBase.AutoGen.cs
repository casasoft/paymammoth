using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.AuditLogModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IAuditLogBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string ItemType { get; set; }
		//Iproperty_normal
        long ItemID { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase CmsUser {get; set; }

		//Iproperty_normal
        string CmsUserIpAddress { get; set; }
		//Iproperty_normal
        DateTime DateTime { get; set; }
		//Iproperty_normal
        string StackTrace { get; set; }
		//Iproperty_normal
        string Method { get; set; }
		//Iproperty_normal
        string Changes { get; set; }
		//Iproperty_normal
        string Remarks { get; set; }
		//Iproperty_normal
        string Message { get; set; }
		//Iproperty_normal
        string ItemTypeFull { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.AUDITLOG_MSG_TYPE UpdateType { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
