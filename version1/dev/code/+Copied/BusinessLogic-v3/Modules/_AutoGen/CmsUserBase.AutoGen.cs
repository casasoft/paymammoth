using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.CmsUserModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "CmsUser")]
    public abstract class CmsUserBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ICmsUserBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static CmsUserBaseFactory Factory
        {
            get
            {
                return CmsUserBaseFactory.Instance; 
            }
        }    
        /*
        public static CmsUserBase CreateNewItem()
        {
            return (CmsUserBase)Factory.CreateNewItem();
        }

		public static CmsUserBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<CmsUserBase, CmsUserBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static CmsUserBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static CmsUserBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<CmsUserBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<CmsUserBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<CmsUserBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _name;
        
        public virtual string Name 
        {
        	get
        	{
        		return _name;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_name,value);
        		_name = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _surname;
        
        public virtual string Surname 
        {
        	get
        	{
        		return _surname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_surname,value);
        		_surname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _username;
        
        public virtual string Username 
        {
        	get
        	{
        		return _username;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_username,value);
        		_username = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _password;
        
        public virtual string Password 
        {
        	get
        	{
        		return _password;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_password,value);
        		_password = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _lastloggedin;
        
        public virtual DateTime? LastLoggedIn 
        {
        	get
        	{
        		return _lastloggedin;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_lastloggedin,value);
        		_lastloggedin = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? __lastloggedin_new;
        
        public virtual DateTime? _LastLoggedIn_New 
        {
        	get
        	{
        		return __lastloggedin_new;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__lastloggedin_new,value);
        		__lastloggedin_new = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.CMS_ACCESS_TYPE _accesstype;
        
        public virtual CS.General_v3.Enums.CMS_ACCESS_TYPE AccessType 
        {
        	get
        	{
        		return _accesstype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_accesstype,value);
        		_accesstype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _sessionguid;
        
        public virtual string SessionGUID 
        {
        	get
        	{
        		return _sessionguid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_sessionguid,value);
        		_sessionguid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _passwordsalt;
        
        public virtual string PasswordSalt 
        {
        	get
        	{
        		return _passwordsalt;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_passwordsalt,value);
        		_passwordsalt = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE _passwordencryptiontype;
        
        public virtual CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE PasswordEncryptionType 
        {
        	get
        	{
        		return _passwordencryptiontype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_passwordencryptiontype,value);
        		_passwordencryptiontype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _passworditerations;
        
        public virtual int PasswordIterations 
        {
        	get
        	{
        		return _passworditerations;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_passworditerations,value);
        		_passworditerations = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _hiddenfromnoncasasoft;
        
        public virtual bool HiddenFromNonCasaSoft 
        {
        	get
        	{
        		return _hiddenfromnoncasasoft;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_hiddenfromnoncasasoft,value);
        		_hiddenfromnoncasasoft = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _cmscustomlogofilename;
        
        public virtual string CmsCustomLogoFilename 
        {
        	get
        	{
        		return _cmscustomlogofilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cmscustomlogofilename,value);
        		_cmscustomlogofilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _cmscustomlogolinkurl;
        
        public virtual string CmsCustomLogoLinkUrl 
        {
        	get
        	{
        		return _cmscustomlogolinkurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cmscustomlogolinkurl,value);
        		_cmscustomlogolinkurl = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _disabled;
        
        public virtual bool Disabled 
        {
        	get
        	{
        		return _disabled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_disabled,value);
        		_disabled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _ticketingsystemsupportuser;
        
        public virtual bool TicketingSystemSupportUser 
        {
        	get
        	{
        		return _ticketingsystemsupportuser;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ticketingsystemsupportuser,value);
        		_ticketingsystemsupportuser = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _email;
        
        public virtual string Email 
        {
        	get
        	{
        		return _email;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_email,value);
        		_email = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __AffiliatesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, 
        	BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase,
        	BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>
        {
            private BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _item = null;
            public __AffiliatesCollectionInfo(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase> Collection
            {
                get { return _item.__collection__Affiliates; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.SetLinkOnItem(BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase item, BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase value)
            {
                item.LinkedCmsUser = value;
            }
        }
        
        private __AffiliatesCollectionInfo _Affiliates = null;
        public __AffiliatesCollectionInfo Affiliates
        {
            get
            {
                if (_Affiliates == null)
                    _Affiliates = new __AffiliatesCollectionInfo((BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase)this);
                return _Affiliates;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase> ICmsUserBaseAutoGen.Affiliates
        {
            get {  return this.Affiliates; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase> __collection__Affiliates
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'AffiliateBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ClassifiedsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, 
        	BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase,
        	BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase>
        {
            private BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _item = null;
            public __ClassifiedsCollectionInfo(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase> Collection
            {
                get { return _item.__collection__Classifieds; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase item, BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase value)
            {
                item.ApprovedBy = value;
            }
        }
        
        private __ClassifiedsCollectionInfo _Classifieds = null;
        public __ClassifiedsCollectionInfo Classifieds
        {
            get
            {
                if (_Classifieds == null)
                    _Classifieds = new __ClassifiedsCollectionInfo((BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase)this);
                return _Classifieds;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase> ICmsUserBaseAutoGen.Classifieds
        {
            get {  return this.Classifieds; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase> __collection__Classifieds
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ClassifiedBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EventSubmissionsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, 
        	BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase,
        	BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase>
        {
            private BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _item = null;
            public __EventSubmissionsCollectionInfo(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase> Collection
            {
                get { return _item.__collection__EventSubmissions; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase item, BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase value)
            {
                item.ApprovedBy = value;
            }
        }
        
        private __EventSubmissionsCollectionInfo _EventSubmissions = null;
        public __EventSubmissionsCollectionInfo EventSubmissions
        {
            get
            {
                if (_EventSubmissions == null)
                    _EventSubmissions = new __EventSubmissionsCollectionInfo((BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase)this);
                return _EventSubmissions;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBase> ICmsUserBaseAutoGen.EventSubmissions
        {
            get {  return this.EventSubmissions; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase> __collection__EventSubmissions
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventSubmissionBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __MembersCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, 
        	BusinessLogic_v3.Modules.MemberModule.MemberBase,
        	BusinessLogic_v3.Modules.MemberModule.IMemberBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, BusinessLogic_v3.Modules.MemberModule.MemberBase>
        {
            private BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _item = null;
            public __MembersCollectionInfo(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> Collection
            {
                get { return _item.__collection__Members; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, BusinessLogic_v3.Modules.MemberModule.MemberBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberModule.MemberBase item, BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase value)
            {
                item.AssignedToTicketingSystemSupportUser = value;
            }
        }
        
        private __MembersCollectionInfo _Members = null;
        public __MembersCollectionInfo Members
        {
            get
            {
                if (_Members == null)
                    _Members = new __MembersCollectionInfo((BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase)this);
                return _Members;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> ICmsUserBaseAutoGen.Members
        {
            get {  return this.Members; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> __collection__Members
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __AssignedTicketsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, 
        	BusinessLogic_v3.Modules.TicketModule.TicketBase,
        	BusinessLogic_v3.Modules.TicketModule.ITicketBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, BusinessLogic_v3.Modules.TicketModule.TicketBase>
        {
            private BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _item = null;
            public __AssignedTicketsCollectionInfo(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.TicketModule.TicketBase> Collection
            {
                get { return _item.__collection__AssignedTickets; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, BusinessLogic_v3.Modules.TicketModule.TicketBase>.SetLinkOnItem(BusinessLogic_v3.Modules.TicketModule.TicketBase item, BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase value)
            {
                item.AssignedTo = value;
            }
        }
        
        private __AssignedTicketsCollectionInfo _AssignedTickets = null;
        public __AssignedTicketsCollectionInfo AssignedTickets
        {
            get
            {
                if (_AssignedTickets == null)
                    _AssignedTickets = new __AssignedTicketsCollectionInfo((BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase)this);
                return _AssignedTickets;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.TicketModule.ITicketBase> ICmsUserBaseAutoGen.AssignedTickets
        {
            get {  return this.AssignedTickets; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.TicketModule.TicketBase> __collection__AssignedTickets
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'TicketBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionManyToManyLeftSide  
        
		public class __CmsUserRolesCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, 
			BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase,
			BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase, BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase>
        {
            private BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _item = null;
            public __CmsUserRolesCollectionInfo(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase> Collection
            {
                get { return _item.__collection__CmsUserRoles; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase,BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase item)
            {
                return item.CmsUsers;
                
            }


            #endregion
        }

        private __CmsUserRolesCollectionInfo _CmsUserRoles = null;
        public __CmsUserRolesCollectionInfo CmsUserRoles
        {
            get
            {
                if (_CmsUserRoles == null)
                    _CmsUserRoles = new __CmsUserRolesCollectionInfo((BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase)this);
                return _CmsUserRoles;
            }
        }
            
        ICollectionManager<BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBase> ICmsUserBaseAutoGen.CmsUserRoles
        {
            get {  return this.CmsUserRoles; }
        }
            
        
		protected virtual IEnumerable<BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase> __collection__CmsUserRoles
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'CmsUserRoleBase' is not marked as UsedInProject"); }
        }





        

    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
