using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.CategorySpecificationModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.CategorySpecificationModule.CategorySpecificationBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.CategorySpecificationModule.CategorySpecificationBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBase item)
        {
        	return BusinessLogic_v3.Frontend.CategorySpecificationModule.CategorySpecificationBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CategorySpecificationBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<CategorySpecificationBaseFrontend, BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBase>

    {
		
        
        protected CategorySpecificationBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
