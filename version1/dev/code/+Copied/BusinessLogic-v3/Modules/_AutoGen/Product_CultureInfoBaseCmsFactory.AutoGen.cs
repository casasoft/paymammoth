using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.Product_CultureInfoModule;
using BusinessLogic_v3.Cms.Product_CultureInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class Product_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<Product_CultureInfoBaseCmsInfo, Product_CultureInfoBase>
    {
       
       public new static Product_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Product_CultureInfoBaseCmsFactory)CmsFactoryBase<Product_CultureInfoBaseCmsInfo, Product_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = Product_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Product_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Product_CultureInfo";

            this.QueryStringParamID = "Product_CultureInfoId";

            cmsInfo.TitlePlural = "Product _ Culture Infos";

            cmsInfo.TitleSingular =  "Product _ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Product_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Product_CultureInfo/";
			UsedInProject = Product_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
