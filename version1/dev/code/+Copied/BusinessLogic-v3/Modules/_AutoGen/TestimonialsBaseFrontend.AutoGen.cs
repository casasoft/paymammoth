using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.TestimonialsModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.TestimonialsModule.TestimonialsBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.TestimonialsModule.ITestimonialsBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.TestimonialsModule.TestimonialsBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.TestimonialsModule.ITestimonialsBase item)
        {
        	return BusinessLogic_v3.Frontend.TestimonialsModule.TestimonialsBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class TestimonialsBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<TestimonialsBaseFrontend, BusinessLogic_v3.Modules.TestimonialsModule.ITestimonialsBase>

    {
		
        
        protected TestimonialsBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
