using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.LocationModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.LocationModule.LocationBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.LocationModule.ILocationBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.LocationModule.LocationBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.LocationModule.ILocationBase item)
        {
        	return BusinessLogic_v3.Frontend.LocationModule.LocationBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class LocationBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<LocationBaseFrontend, BusinessLogic_v3.Modules.LocationModule.ILocationBase>

    {
		
        
        protected LocationBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
