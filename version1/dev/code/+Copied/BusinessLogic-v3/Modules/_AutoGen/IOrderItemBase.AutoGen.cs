using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.OrderItemModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IOrderItemBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        int Quantity { get; set; }
		//Iproperty_normal
        string Remarks { get; set; }
		//Iproperty_normal
        long ItemID { get; set; }
		//Iproperty_normal
        string ItemType { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.OrderModule.IOrderBase Order {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase LinkedProductVariation {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBase SubscriptionTypePrice {get; set; }

		//Iproperty_normal
        bool AutoRenewSubscriptionTypePrice { get; set; }
		//Iproperty_normal
        string LinkedProductVariationColour { get; set; }
		//Iproperty_normal
        string LinkedProductVariationSize { get; set; }
		//Iproperty_normal
        string Description { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
