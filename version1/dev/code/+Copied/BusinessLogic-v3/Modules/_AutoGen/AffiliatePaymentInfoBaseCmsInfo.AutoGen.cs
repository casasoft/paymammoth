using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.AffiliatePaymentInfoModule;
using BusinessLogic_v3.Cms.AffiliatePaymentInfoModule;
using BusinessLogic_v3.Frontend.AffiliatePaymentInfoModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AffiliatePaymentInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>
    {
		public AffiliatePaymentInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.AffiliatePaymentInfoModule.AffiliatePaymentInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AffiliatePaymentInfoBaseFrontend FrontendItem
        {
            get { return (AffiliatePaymentInfoBaseFrontend)base.FrontendItem; }

        }
        public new AffiliatePaymentInfoBase DbItem
        {
            get
            {
                return (AffiliatePaymentInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Affiliate { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo PaymentMethod { get; protected set; }

        public CmsPropertyInfo DatePaymentIssued { get; protected set; }

        public CmsPropertyInfo AdditionalComments { get; protected set; }

        public CmsPropertyInfo Total { get; protected set; }

        public CmsPropertyInfo Currency { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
