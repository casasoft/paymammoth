using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.MemberSubscriptionLinkModule;
using BusinessLogic_v3.Cms.MemberSubscriptionLinkModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class MemberSubscriptionLinkBaseCmsFactory_AutoGen : CmsFactoryBase<MemberSubscriptionLinkBaseCmsInfo, MemberSubscriptionLinkBase>
    {
       
       public new static MemberSubscriptionLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberSubscriptionLinkBaseCmsFactory)CmsFactoryBase<MemberSubscriptionLinkBaseCmsInfo, MemberSubscriptionLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = MemberSubscriptionLinkBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberSubscriptionLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberSubscriptionLink";

            this.QueryStringParamID = "MemberSubscriptionLinkId";

            cmsInfo.TitlePlural = "Member Subscription Links";

            cmsInfo.TitleSingular =  "Member Subscription Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberSubscriptionLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "MemberSubscriptionLink/";
			UsedInProject = MemberSubscriptionLinkBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
