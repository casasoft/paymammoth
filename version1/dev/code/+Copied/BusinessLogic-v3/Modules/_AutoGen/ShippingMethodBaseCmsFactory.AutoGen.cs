using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ShippingMethodModule;
using BusinessLogic_v3.Cms.ShippingMethodModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ShippingMethodBaseCmsFactory_AutoGen : CmsFactoryBase<ShippingMethodBaseCmsInfo, ShippingMethodBase>
    {
       
       public new static ShippingMethodBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ShippingMethodBaseCmsFactory)CmsFactoryBase<ShippingMethodBaseCmsInfo, ShippingMethodBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ShippingMethodBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ShippingMethod.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ShippingMethod";

            this.QueryStringParamID = "ShippingMethodId";

            cmsInfo.TitlePlural = "Shipping Methods";

            cmsInfo.TitleSingular =  "Shipping Method";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ShippingMethodBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ShippingMethod/";
			UsedInProject = ShippingMethodBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
