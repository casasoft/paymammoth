using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.MemberSubscriptionTypeModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "MemberSubscriptionType")]
    public abstract class MemberSubscriptionTypeBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IMemberSubscriptionTypeBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static MemberSubscriptionTypeBaseFactory Factory
        {
            get
            {
                return MemberSubscriptionTypeBaseFactory.Instance; 
            }
        }    
        /*
        public static MemberSubscriptionTypeBase CreateNewItem()
        {
            return (MemberSubscriptionTypeBase)Factory.CreateNewItem();
        }

		public static MemberSubscriptionTypeBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MemberSubscriptionTypeBase, MemberSubscriptionTypeBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MemberSubscriptionTypeBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MemberSubscriptionTypeBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MemberSubscriptionTypeBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MemberSubscriptionTypeBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MemberSubscriptionTypeBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.MEMBER_SUBSCRIPTION_TYPE _membersubscriptiontype;
        
        public virtual BusinessLogic_v3.Enums.MEMBER_SUBSCRIPTION_TYPE MemberSubscriptionType 
        {
        	get
        	{
        		return _membersubscriptiontype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_membersubscriptiontype,value);
        		_membersubscriptiontype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _renewablebalancevalue;
        
        public virtual double RenewableBalanceValue 
        {
        	get
        	{
        		return _renewablebalancevalue;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_renewablebalancevalue,value);
        		_renewablebalancevalue = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _renewablebalancerefreshperiodindays;
        
        public virtual int RenewableBalanceRefreshPeriodInDays 
        {
        	get
        	{
        		return _renewablebalancerefreshperiodindays;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_renewablebalancerefreshperiodindays,value);
        		_renewablebalancerefreshperiodindays = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __MemberSubscriptionTypePricesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase, 
        	BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase,
        	BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase, BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>
        {
            private BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase _item = null;
            public __MemberSubscriptionTypePricesCollectionInfo(BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase> Collection
            {
                get { return _item.__collection__MemberSubscriptionTypePrices; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase, BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase item, BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase value)
            {
                item.MemberSubscriptionType = value;
            }
        }
        
        private __MemberSubscriptionTypePricesCollectionInfo _MemberSubscriptionTypePrices = null;
        public __MemberSubscriptionTypePricesCollectionInfo MemberSubscriptionTypePrices
        {
            get
            {
                if (_MemberSubscriptionTypePrices == null)
                    _MemberSubscriptionTypePrices = new __MemberSubscriptionTypePricesCollectionInfo((BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase)this);
                return _MemberSubscriptionTypePrices;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBase> IMemberSubscriptionTypeBaseAutoGen.MemberSubscriptionTypePrices
        {
            get {  return this.MemberSubscriptionTypePrices; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase> __collection__MemberSubscriptionTypePrices
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberSubscriptionTypePriceBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
