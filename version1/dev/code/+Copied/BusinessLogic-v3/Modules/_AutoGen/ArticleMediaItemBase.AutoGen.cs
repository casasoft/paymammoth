using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ArticleMediaItemModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ArticleMediaItem")]
    public abstract class ArticleMediaItemBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IArticleMediaItemBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ArticleMediaItemBaseFactory Factory
        {
            get
            {
                return ArticleMediaItemBaseFactory.Instance; 
            }
        }    
        /*
        public static ArticleMediaItemBase CreateNewItem()
        {
            return (ArticleMediaItemBase)Factory.CreateNewItem();
        }

		public static ArticleMediaItemBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ArticleMediaItemBase, ArticleMediaItemBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ArticleMediaItemBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ArticleMediaItemBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ArticleMediaItemBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ArticleMediaItemBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ArticleMediaItemBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _caption;
        
        public virtual string Caption 
        {
        	get
        	{
        		return _caption;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_caption,value);
        		_caption = value;
        		
        	}
        }
        
/*
		public virtual long? ContentPageID
		{
		 	get 
		 	{
		 		return (this.ContentPage != null ? (long?)this.ContentPage.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ArticleModule.ArticleBase _contentpage;
        public virtual BusinessLogic_v3.Modules.ArticleModule.ArticleBase ContentPage 
        {
        	get
        	{
        		return _contentpage;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_contentpage,value);
        		_contentpage = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase BusinessLogic_v3.Modules._AutoGen.IArticleMediaItemBaseAutoGen.ContentPage 
        {
            get
            {
            	return this.ContentPage;
            }
            set
            {
            	this.ContentPage = (BusinessLogic_v3.Modules.ArticleModule.ArticleBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private int _videoduration;
        
        public virtual int VideoDuration 
        {
        	get
        	{
        		return _videoduration;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_videoduration,value);
        		_videoduration = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _filename;
        
        public virtual string Filename 
        {
        	get
        	{
        		return _filename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_filename,value);
        		_filename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _videolink;
        
        public virtual string VideoLink 
        {
        	get
        	{
        		return _videolink;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_videolink,value);
        		_videolink = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _ismainimage;
        
        public virtual bool IsMainImage 
        {
        	get
        	{
        		return _ismainimage;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ismainimage,value);
        		_ismainimage = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
