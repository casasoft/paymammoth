using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "MemberAccountBalanceHistory")]
    public abstract class MemberAccountBalanceHistoryBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IMemberAccountBalanceHistoryBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static MemberAccountBalanceHistoryBaseFactory Factory
        {
            get
            {
                return MemberAccountBalanceHistoryBaseFactory.Instance; 
            }
        }    
        /*
        public static MemberAccountBalanceHistoryBase CreateNewItem()
        {
            return (MemberAccountBalanceHistoryBase)Factory.CreateNewItem();
        }

		public static MemberAccountBalanceHistoryBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MemberAccountBalanceHistoryBase, MemberAccountBalanceHistoryBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MemberAccountBalanceHistoryBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MemberAccountBalanceHistoryBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MemberAccountBalanceHistoryBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MemberAccountBalanceHistoryBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MemberAccountBalanceHistoryBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? MemberID
		{
		 	get 
		 	{
		 		return (this.Member != null ? (long?)this.Member.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _member;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase Member 
        {
        	get
        	{
        		return _member;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_member,value);
        		_member = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IMemberAccountBalanceHistoryBaseAutoGen.Member 
        {
            get
            {
            	return this.Member;
            }
            set
            {
            	this.Member = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private double _balanceupdate;
        
        public virtual double BalanceUpdate 
        {
        	get
        	{
        		return _balanceupdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_balanceupdate,value);
        		_balanceupdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _date;
        
        public virtual DateTime Date 
        {
        	get
        	{
        		return _date;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_date,value);
        		_date = value;
        		
        	}
        }
        
/*
		public virtual long? OrderLinkID
		{
		 	get 
		 	{
		 		return (this.OrderLink != null ? (long?)this.OrderLink.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.OrderModule.OrderBase _orderlink;
        public virtual BusinessLogic_v3.Modules.OrderModule.OrderBase OrderLink 
        {
        	get
        	{
        		return _orderlink;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_orderlink,value);
        		_orderlink = value;
        	}
        }
            
        BusinessLogic_v3.Modules.OrderModule.IOrderBase BusinessLogic_v3.Modules._AutoGen.IMemberAccountBalanceHistoryBaseAutoGen.OrderLink 
        {
            get
            {
            	return this.OrderLink;
            }
            set
            {
            	this.OrderLink = (BusinessLogic_v3.Modules.OrderModule.OrderBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE _updatetype;
        
        public virtual BusinessLogic_v3.Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE UpdateType 
        {
        	get
        	{
        		return _updatetype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_updatetype,value);
        		_updatetype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _balancetotal;
        
        public virtual double BalanceTotal 
        {
        	get
        	{
        		return _balancetotal;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_balancetotal,value);
        		_balancetotal = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _comments;
        
        public virtual string Comments 
        {
        	get
        	{
        		return _comments;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_comments,value);
        		_comments = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _renewablebalancetotal;
        
        public virtual double RenewableBalanceTotal 
        {
        	get
        	{
        		return _renewablebalancetotal;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_renewablebalancetotal,value);
        		_renewablebalancetotal = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
