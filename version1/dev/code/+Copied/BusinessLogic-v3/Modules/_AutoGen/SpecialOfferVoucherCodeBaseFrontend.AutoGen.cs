using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.SpecialOfferVoucherCodeModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.ISpecialOfferVoucherCodeBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.ISpecialOfferVoucherCodeBase item)
        {
        	return BusinessLogic_v3.Frontend.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class SpecialOfferVoucherCodeBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<SpecialOfferVoucherCodeBaseFrontend, BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.ISpecialOfferVoucherCodeBase>

    {
		
        
        protected SpecialOfferVoucherCodeBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
