using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.AffiliateModule;
using BusinessLogic_v3.Cms.AffiliateModule;
using BusinessLogic_v3.Frontend.AffiliateModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AffiliateBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>
    {
		public AffiliateBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase dbItem)
            : base(BusinessLogic_v3.Cms.AffiliateModule.AffiliateBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AffiliateBaseFrontend FrontendItem
        {
            get { return (AffiliateBaseFrontend)base.FrontendItem; }

        }
        public new AffiliateBase DbItem
        {
            get
            {
                return (AffiliateBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo CssFilename { get; protected set; }

        public CmsPropertyInfo LogoFilename { get; protected set; }

        public CmsPropertyInfo Username { get; protected set; }

        public CmsPropertyInfo Password { get; protected set; }

        public CmsPropertyInfo ActiveFrom { get; protected set; }

        public CmsPropertyInfo AffiliateCode { get; protected set; }

        public CmsPropertyInfo ReferralCode { get; protected set; }

        public CmsPropertyInfo LinkedContentPageNode { get; protected set; }

        public CmsPropertyInfo CommissionRate { get; protected set; }

        public CmsPropertyInfo AffiliateUrl { get; protected set; }

        public CmsPropertyInfo LinkedCmsUser { get; protected set; }

        public CmsPropertyInfo HasWhiteLabellingFunctionality { get; protected set; }

        public CmsPropertyInfo AffiliateWhiteLabelBaseUrl { get; protected set; }

        public CmsPropertyInfo MainPanelHtml { get; protected set; }

        public CmsPropertyInfo Activated { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo AffiliatePaymentInfos { get; protected set; }
        

        public CmsCollectionInfo Members { get; protected set; }
        

        public CmsCollectionInfo Orders { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.AffiliatePaymentInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector(item => item.AffiliatePaymentInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>.GetPropertyBySelector(item => item.Affiliate)));
		
		//InitCollectionBaseOneToMany
		this.Members = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector(item => item.Members),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.LinkedAffiliate)));
		
		//InitCollectionBaseOneToMany
		this.Orders = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector(item => item.Orders),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector(item => item.LinkedAffiliate)));


			base.initBasicFields();
          
        }

    }
}
