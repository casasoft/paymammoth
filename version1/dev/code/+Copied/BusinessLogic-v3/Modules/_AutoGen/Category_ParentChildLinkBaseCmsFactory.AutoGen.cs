using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;
using BusinessLogic_v3.Cms.Category_ParentChildLinkModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class Category_ParentChildLinkBaseCmsFactory_AutoGen : CmsFactoryBase<Category_ParentChildLinkBaseCmsInfo, Category_ParentChildLinkBase>
    {
       
       public new static Category_ParentChildLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Category_ParentChildLinkBaseCmsFactory)CmsFactoryBase<Category_ParentChildLinkBaseCmsInfo, Category_ParentChildLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = Category_ParentChildLinkBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Category_ParentChildLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Category_ParentChildLink";

            this.QueryStringParamID = "Category_ParentChildLinkId";

            cmsInfo.TitlePlural = "Category _ Parent Child Links";

            cmsInfo.TitleSingular =  "Category _ Parent Child Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Category_ParentChildLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Category_ParentChildLink/";
			UsedInProject = Category_ParentChildLinkBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
