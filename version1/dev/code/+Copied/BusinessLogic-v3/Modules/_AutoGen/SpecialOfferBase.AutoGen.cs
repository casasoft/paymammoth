using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.SpecialOfferModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "SpecialOffer")]
    public abstract class SpecialOfferBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ISpecialOfferBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
        

			var cultureInfo = __getCurrentCultureInfo(); //since this is a new item, it is imperative that the culture is preloaded
            
            
            base.initPropertiesForNewItem();
        }
    	
  
    	public BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase) __getCultureInfoByLanguage(lang);
        }

     	public static SpecialOfferBaseFactory Factory
        {
            get
            {
                return SpecialOfferBaseFactory.Instance; 
            }
        }    
        /*
        public static SpecialOfferBase CreateNewItem()
        {
            return (SpecialOfferBase)Factory.CreateNewItem();
        }

		public static SpecialOfferBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<SpecialOfferBase, SpecialOfferBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static SpecialOfferBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static SpecialOfferBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<SpecialOfferBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<SpecialOfferBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<SpecialOfferBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Title
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Title;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Title_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Title,value);
                cultureInfo.Title = value;
            }
        }
        
        private string _title;
        public virtual string __Title_cultureBase
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		
        		_title = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private string _name;
        
        public virtual string Name 
        {
        	get
        	{
        		return _name;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_name,value);
        		_name = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _datefrom;
        
        public virtual DateTime? DateFrom 
        {
        	get
        	{
        		return _datefrom;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datefrom,value);
        		_datefrom = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _dateto;
        
        public virtual DateTime? DateTo 
        {
        	get
        	{
        		return _dateto;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dateto,value);
        		_dateto = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _requiredtotalitemstobuy;
        
        public virtual int RequiredTotalItemsToBuy 
        {
        	get
        	{
        		return _requiredtotalitemstobuy;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_requiredtotalitemstobuy,value);
        		_requiredtotalitemstobuy = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _requiredtotaltospend;
        
        public virtual double RequiredTotalToSpend 
        {
        	get
        	{
        		return _requiredtotaltospend;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_requiredtotaltospend,value);
        		_requiredtotaltospend = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _discountpercentage;
        
        public virtual double DiscountPercentage 
        {
        	get
        	{
        		return _discountpercentage;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_discountpercentage,value);
        		_discountpercentage = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _requirespromocode;
        
        public virtual bool RequiresPromoCode 
        {
        	get
        	{
        		return _requirespromocode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_requirespromocode,value);
        		_requirespromocode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Description
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Description;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Description_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Description,value);
                cultureInfo.Description = value;
            }
        }
        
        private string _description;
        public virtual string __Description_cultureBase
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		
        		_description = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private int _quantityleft;
        
        public virtual int QuantityLeft 
        {
        	get
        	{
        		return _quantityleft;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_quantityleft,value);
        		_quantityleft = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _activated;
        
        public virtual bool Activated 
        {
        	get
        	{
        		return _activated;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_activated,value);
        		_activated = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _requirementscanmatchanyitem;
        
        public virtual bool RequirementsCanMatchAnyItem 
        {
        	get
        	{
        		return _requirementscanmatchanyitem;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_requirementscanmatchanyitem,value);
        		_requirementscanmatchanyitem = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isexclusiveoffer;
        
        public virtual bool IsExclusiveOffer 
        {
        	get
        	{
        		return _isexclusiveoffer;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isexclusiveoffer,value);
        		_isexclusiveoffer = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isnotlimitedbyquantity;
        
        public virtual bool IsNotLimitedByQuantity 
        {
        	get
        	{
        		return _isnotlimitedbyquantity;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isnotlimitedbyquantity,value);
        		_isnotlimitedbyquantity = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __SpecialOffer_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase, 
        	BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase,
        	BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.ISpecialOffer_CultureInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase, BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase>
        {
            private BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase _item = null;
            public __SpecialOffer_CultureInfosCollectionInfo(BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase> Collection
            {
                get { return _item.__collection__SpecialOffer_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase, BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase item, BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase value)
            {
                item.SpecialOffer = value;
            }
        }
        
        private __SpecialOffer_CultureInfosCollectionInfo _SpecialOffer_CultureInfos = null;
        public __SpecialOffer_CultureInfosCollectionInfo SpecialOffer_CultureInfos
        {
            get
            {
                if (_SpecialOffer_CultureInfos == null)
                    _SpecialOffer_CultureInfos = new __SpecialOffer_CultureInfosCollectionInfo((BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase)this);
                return _SpecialOffer_CultureInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.ISpecialOffer_CultureInfoBase> ISpecialOfferBaseAutoGen.SpecialOffer_CultureInfos
        {
            get {  return this.SpecialOffer_CultureInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase> __collection__SpecialOffer_CultureInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'SpecialOffer_CultureInfoBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __SpecialOfferVoucherCodesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase, 
        	BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase,
        	BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.ISpecialOfferVoucherCodeBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase, BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase>
        {
            private BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase _item = null;
            public __SpecialOfferVoucherCodesCollectionInfo(BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase> Collection
            {
                get { return _item.__collection__SpecialOfferVoucherCodes; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase, BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase>.SetLinkOnItem(BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase item, BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase value)
            {
                item.SpecialOffer = value;
            }
        }
        
        private __SpecialOfferVoucherCodesCollectionInfo _SpecialOfferVoucherCodes = null;
        public __SpecialOfferVoucherCodesCollectionInfo SpecialOfferVoucherCodes
        {
            get
            {
                if (_SpecialOfferVoucherCodes == null)
                    _SpecialOfferVoucherCodes = new __SpecialOfferVoucherCodesCollectionInfo((BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase)this);
                return _SpecialOfferVoucherCodes;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.ISpecialOfferVoucherCodeBase> ISpecialOfferBaseAutoGen.SpecialOfferVoucherCodes
        {
            get {  return this.SpecialOfferVoucherCodes; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase> __collection__SpecialOfferVoucherCodes
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'SpecialOfferVoucherCodeBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionManyToManyLeftSide  
        
		public class __ApplicableCategoriesCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase, 
			BusinessLogic_v3.Modules.CategoryModule.CategoryBase,
			BusinessLogic_v3.Modules.CategoryModule.ICategoryBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase, BusinessLogic_v3.Modules.CategoryModule.CategoryBase>
        {
            private BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase _item = null;
            public __ApplicableCategoriesCollectionInfo(BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.CategoryModule.CategoryBase> Collection
            {
                get { return _item.__collection__ApplicableCategories; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase,BusinessLogic_v3.Modules.CategoryModule.CategoryBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item)
            {
                return item.ApplicableSpecialOffers;
                
            }


            #endregion
        }

        private __ApplicableCategoriesCollectionInfo _ApplicableCategories = null;
        public __ApplicableCategoriesCollectionInfo ApplicableCategories
        {
            get
            {
                if (_ApplicableCategories == null)
                    _ApplicableCategories = new __ApplicableCategoriesCollectionInfo((BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase)this);
                return _ApplicableCategories;
            }
        }
            
        ICollectionManager<BusinessLogic_v3.Modules.CategoryModule.ICategoryBase> ISpecialOfferBaseAutoGen.ApplicableCategories
        {
            get {  return this.ApplicableCategories; }
        }
            
        
		protected virtual IEnumerable<BusinessLogic_v3.Modules.CategoryModule.CategoryBase> __collection__ApplicableCategories
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'CategoryBase' is not marked as UsedInProject"); }
        }





        

    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
