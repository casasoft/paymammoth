using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.SettingModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Setting")]
    public abstract class SettingBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ISettingBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static SettingBaseFactory Factory
        {
            get
            {
                return SettingBaseFactory.Instance; 
            }
        }    
        /*
        public static SettingBase CreateNewItem()
        {
            return (SettingBase)Factory.CreateNewItem();
        }

		public static SettingBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<SettingBase, SettingBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static SettingBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static SettingBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<SettingBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<SettingBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<SettingBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _name;
        
        public virtual string Name 
        {
        	get
        	{
        		return _name;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_name,value);
        		_name = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _value;
        
        public virtual string Value 
        {
        	get
        	{
        		return _value;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_value,value);
        		_value = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.CMS_ACCESS_TYPE _visibleincms_accessrequired;
        
        public virtual CS.General_v3.Enums.CMS_ACCESS_TYPE VisibleInCMS_AccessRequired 
        {
        	get
        	{
        		return _visibleincms_accessrequired;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_visibleincms_accessrequired,value);
        		_visibleincms_accessrequired = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.DATA_TYPE _datatype;
        
        public virtual CS.General_v3.Enums.DATA_TYPE DataType 
        {
        	get
        	{
        		return _datatype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datatype,value);
        		_datatype = value;
        		
        	}
        }
        
/*
		public virtual long? ParentID
		{
		 	get 
		 	{
		 		return (this.Parent != null ? (long?)this.Parent.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.SettingModule.SettingBase _parent;
        public virtual BusinessLogic_v3.Modules.SettingModule.SettingBase Parent 
        {
        	get
        	{
        		return _parent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_parent,value);
        		_parent = value;
        	}
        }
            
        BusinessLogic_v3.Modules.SettingModule.ISettingBase BusinessLogic_v3.Modules._AutoGen.ISettingBaseAutoGen.Parent 
        {
            get
            {
            	return this.Parent;
            }
            set
            {
            	this.Parent = (BusinessLogic_v3.Modules.SettingModule.SettingBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _usedinproject;
        
        public virtual bool UsedInProject 
        {
        	get
        	{
        		return _usedinproject;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_usedinproject,value);
        		_usedinproject = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _localhostvalue;
        
        public virtual string LocalhostValue 
        {
        	get
        	{
        		return _localhostvalue;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_localhostvalue,value);
        		_localhostvalue = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _enumtype;
        
        public virtual string EnumType 
        {
        	get
        	{
        		return _enumtype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_enumtype,value);
        		_enumtype = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ChildSettingsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.SettingModule.SettingBase, 
        	BusinessLogic_v3.Modules.SettingModule.SettingBase,
        	BusinessLogic_v3.Modules.SettingModule.ISettingBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.SettingModule.SettingBase, BusinessLogic_v3.Modules.SettingModule.SettingBase>
        {
            private BusinessLogic_v3.Modules.SettingModule.SettingBase _item = null;
            public __ChildSettingsCollectionInfo(BusinessLogic_v3.Modules.SettingModule.SettingBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.SettingModule.SettingBase> Collection
            {
                get { return _item.__collection__ChildSettings; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.SettingModule.SettingBase, BusinessLogic_v3.Modules.SettingModule.SettingBase>.SetLinkOnItem(BusinessLogic_v3.Modules.SettingModule.SettingBase item, BusinessLogic_v3.Modules.SettingModule.SettingBase value)
            {
                item.Parent = value;
            }
        }
        
        private __ChildSettingsCollectionInfo _ChildSettings = null;
        public __ChildSettingsCollectionInfo ChildSettings
        {
            get
            {
                if (_ChildSettings == null)
                    _ChildSettings = new __ChildSettingsCollectionInfo((BusinessLogic_v3.Modules.SettingModule.SettingBase)this);
                return _ChildSettings;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.SettingModule.ISettingBase> ISettingBaseAutoGen.ChildSettings
        {
            get {  return this.ChildSettings; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.SettingModule.SettingBase> __collection__ChildSettings
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'SettingBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
