using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.OrderApplicableSpecialOfferLinkModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "OrderApplicableSpecialOfferLink")]
    public abstract class OrderApplicableSpecialOfferLinkBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IOrderApplicableSpecialOfferLinkBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static OrderApplicableSpecialOfferLinkBaseFactory Factory
        {
            get
            {
                return OrderApplicableSpecialOfferLinkBaseFactory.Instance; 
            }
        }    
        /*
        public static OrderApplicableSpecialOfferLinkBase CreateNewItem()
        {
            return (OrderApplicableSpecialOfferLinkBase)Factory.CreateNewItem();
        }

		public static OrderApplicableSpecialOfferLinkBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<OrderApplicableSpecialOfferLinkBase, OrderApplicableSpecialOfferLinkBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static OrderApplicableSpecialOfferLinkBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static OrderApplicableSpecialOfferLinkBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<OrderApplicableSpecialOfferLinkBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<OrderApplicableSpecialOfferLinkBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<OrderApplicableSpecialOfferLinkBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? OrderID
		{
		 	get 
		 	{
		 		return (this.Order != null ? (long?)this.Order.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.OrderModule.OrderBase _order;
        public virtual BusinessLogic_v3.Modules.OrderModule.OrderBase Order 
        {
        	get
        	{
        		return _order;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_order,value);
        		_order = value;
        	}
        }
            
        BusinessLogic_v3.Modules.OrderModule.IOrderBase BusinessLogic_v3.Modules._AutoGen.IOrderApplicableSpecialOfferLinkBaseAutoGen.Order 
        {
            get
            {
            	return this.Order;
            }
            set
            {
            	this.Order = (BusinessLogic_v3.Modules.OrderModule.OrderBase) value;
            }
        }
            
/*
		public virtual long? SpecialOfferLinkID
		{
		 	get 
		 	{
		 		return (this.SpecialOfferLink != null ? (long?)this.SpecialOfferLink.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase _specialofferlink;
        public virtual BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase SpecialOfferLink 
        {
        	get
        	{
        		return _specialofferlink;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_specialofferlink,value);
        		_specialofferlink = value;
        	}
        }
            
        BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBase BusinessLogic_v3.Modules._AutoGen.IOrderApplicableSpecialOfferLinkBaseAutoGen.SpecialOfferLink 
        {
            get
            {
            	return this.SpecialOfferLink;
            }
            set
            {
            	this.SpecialOfferLink = (BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase) value;
            }
        }
            
/*
		public virtual long? SpecialOfferVoucherCodeLinkID
		{
		 	get 
		 	{
		 		return (this.SpecialOfferVoucherCodeLink != null ? (long?)this.SpecialOfferVoucherCodeLink.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase _specialoffervouchercodelink;
        public virtual BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase SpecialOfferVoucherCodeLink 
        {
        	get
        	{
        		return _specialoffervouchercodelink;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_specialoffervouchercodelink,value);
        		_specialoffervouchercodelink = value;
        	}
        }
            
        BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.ISpecialOfferVoucherCodeBase BusinessLogic_v3.Modules._AutoGen.IOrderApplicableSpecialOfferLinkBaseAutoGen.SpecialOfferVoucherCodeLink 
        {
            get
            {
            	return this.SpecialOfferVoucherCodeLink;
            }
            set
            {
            	this.SpecialOfferVoucherCodeLink = (BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _vouchercode;
        
        public virtual string VoucherCode 
        {
        	get
        	{
        		return _vouchercode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_vouchercode,value);
        		_vouchercode = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
