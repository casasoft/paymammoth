using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ArticleRatingModule;
using BusinessLogic_v3.Cms.ArticleRatingModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ArticleRatingBaseCmsFactory_AutoGen : CmsFactoryBase<ArticleRatingBaseCmsInfo, ArticleRatingBase>
    {
       
       public new static ArticleRatingBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ArticleRatingBaseCmsFactory)CmsFactoryBase<ArticleRatingBaseCmsInfo, ArticleRatingBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ArticleRatingBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ArticleRating.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ArticleRating";

            this.QueryStringParamID = "ArticleRatingId";

            cmsInfo.TitlePlural = "Article Ratings";

            cmsInfo.TitleSingular =  "Article Rating";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ArticleRatingBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ArticleRating/";
			UsedInProject = ArticleRatingBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
