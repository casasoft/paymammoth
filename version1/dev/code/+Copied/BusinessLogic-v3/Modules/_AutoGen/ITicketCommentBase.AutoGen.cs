using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.TicketCommentModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface ITicketCommentBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.TicketModule.ITicketBase Ticket {get; set; }

		//Iproperty_normal
        string Comment { get; set; }
		//Iproperty_normal
        string IPAddress { get; set; }
		//Iproperty_normal
        DateTime PostedOn { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase AssignedStaffUser {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase Member {get; set; }

		//Iproperty_normal
        BusinessLogic_v3.Enums.TICKET_COMMENT_TYPE CommentType { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBase> Attachments { get; }

 
		

    	
    	
      
      

    }
}
