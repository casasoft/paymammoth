using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using BusinessLogic_v3.Cms.ProductCategoryModule;
using BusinessLogic_v3.Frontend.ProductCategoryModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductCategoryBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase>
    {
		public ProductCategoryBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductCategoryModule.ProductCategoryBase dbItem)
            : base(BusinessLogic_v3.Cms.ProductCategoryModule.ProductCategoryBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductCategoryBaseFrontend FrontendItem
        {
            get { return (ProductCategoryBaseFrontend)base.FrontendItem; }

        }
        public new ProductCategoryBase DbItem
        {
            get
            {
                return (ProductCategoryBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Product { get; protected set; }

        public CmsPropertyInfo Category { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
