using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ClassifiedModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Classified")]
    public abstract class ClassifiedBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IClassifiedBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ClassifiedBaseFactory Factory
        {
            get
            {
                return ClassifiedBaseFactory.Instance; 
            }
        }    
        /*
        public static ClassifiedBase CreateNewItem()
        {
            return (ClassifiedBase)Factory.CreateNewItem();
        }

		public static ClassifiedBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ClassifiedBase, ClassifiedBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ClassifiedBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ClassifiedBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ClassifiedBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ClassifiedBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ClassifiedBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
/*
		public virtual long? CategoryID
		{
		 	get 
		 	{
		 		return (this.Category != null ? (long?)this.Category.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _category;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase Category 
        {
        	get
        	{
        		return _category;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_category,value);
        		_category = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.IClassifiedBaseAutoGen.Category 
        {
            get
            {
            	return this.Category;
            }
            set
            {
            	this.Category = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _photofilename;
        
        public virtual string PhotoFilename 
        {
        	get
        	{
        		return _photofilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_photofilename,value);
        		_photofilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _price;
        
        public virtual double Price 
        {
        	get
        	{
        		return _price;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_price,value);
        		_price = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _postedon;
        
        public virtual DateTime PostedOn 
        {
        	get
        	{
        		return _postedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_postedon,value);
        		_postedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _approvedon;
        
        public virtual DateTime? ApprovedOn 
        {
        	get
        	{
        		return _approvedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_approvedon,value);
        		_approvedon = value;
        		
        	}
        }
        
/*
		public virtual long? ApprovedByID
		{
		 	get 
		 	{
		 		return (this.ApprovedBy != null ? (long?)this.ApprovedBy.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _approvedby;
        public virtual BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase ApprovedBy 
        {
        	get
        	{
        		return _approvedby;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_approvedby,value);
        		_approvedby = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase BusinessLogic_v3.Modules._AutoGen.IClassifiedBaseAutoGen.ApprovedBy 
        {
            get
            {
            	return this.ApprovedBy;
            }
            set
            {
            	this.ApprovedBy = (BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _approved;
        
        public virtual bool Approved 
        {
        	get
        	{
        		return _approved;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_approved,value);
        		_approved = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _ispricenegotiable;
        
        public virtual bool IsPriceNegotiable 
        {
        	get
        	{
        		return _ispricenegotiable;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ispricenegotiable,value);
        		_ispricenegotiable = value;
        		
        	}
        }
        
/*
		public virtual long? CreatedByID
		{
		 	get 
		 	{
		 		return (this.CreatedBy != null ? (long?)this.CreatedBy.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _createdby;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase CreatedBy 
        {
        	get
        	{
        		return _createdby;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_createdby,value);
        		_createdby = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IClassifiedBaseAutoGen.CreatedBy 
        {
            get
            {
            	return this.CreatedBy;
            }
            set
            {
            	this.CreatedBy = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _submitted;
        
        public virtual bool Submitted 
        {
        	get
        	{
        		return _submitted;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_submitted,value);
        		_submitted = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customcontenttags;
        
        public virtual string CustomContentTags 
        {
        	get
        	{
        		return _customcontenttags;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customcontenttags,value);
        		_customcontenttags = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
