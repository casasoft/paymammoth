using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "SpecialOffer_CultureInfo")]
    public abstract class SpecialOffer_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ISpecialOffer_CultureInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
      , IMultilingualContentInfo
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static SpecialOffer_CultureInfoBaseFactory Factory
        {
            get
            {
                return SpecialOffer_CultureInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static SpecialOffer_CultureInfoBase CreateNewItem()
        {
            return (SpecialOffer_CultureInfoBase)Factory.CreateNewItem();
        }

		public static SpecialOffer_CultureInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<SpecialOffer_CultureInfoBase, SpecialOffer_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static SpecialOffer_CultureInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static SpecialOffer_CultureInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<SpecialOffer_CultureInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<SpecialOffer_CultureInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<SpecialOffer_CultureInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? CultureInfoID
		{
		 	get 
		 	{
		 		return (this.CultureInfo != null ? (long?)this.CultureInfo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase _cultureinfo;
        public virtual BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase CultureInfo 
        {
        	get
        	{
        		return _cultureinfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cultureinfo,value);
        		_cultureinfo = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase BusinessLogic_v3.Modules._AutoGen.ISpecialOffer_CultureInfoBaseAutoGen.CultureInfo 
        {
            get
            {
            	return this.CultureInfo;
            }
            set
            {
            	this.CultureInfo = (BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase) value;
            }
        }
            
/*
		public virtual long? SpecialOfferID
		{
		 	get 
		 	{
		 		return (this.SpecialOffer != null ? (long?)this.SpecialOffer.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase _specialoffer;
        public virtual BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase SpecialOffer 
        {
        	get
        	{
        		return _specialoffer;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_specialoffer,value);
        		_specialoffer = value;
        	}
        }
            
        BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBase BusinessLogic_v3.Modules._AutoGen.ISpecialOffer_CultureInfoBaseAutoGen.SpecialOffer 
        {
            get
            {
            	return this.SpecialOffer;
            }
            set
            {
            	this.SpecialOffer = (BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)




#region IMultilingualContentInfo Members

        long IMultilingualContentInfo.CultureInfoID
        {
            get
            {                          
            	if (this.CultureInfo != null)
                	return this.CultureInfo.ID;
               	else
               		return 0;
            }
        }
  		BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject IMultilingualContentInfo.GetLinkedItem()
        {
            return this.SpecialOffer;
        }
#endregion



#endregion       

		

    }
}
