using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductVariationMediaItemModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductVariationMediaItemModule.ProductVariationMediaItemBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductVariationMediaItemModule.ProductVariationMediaItemBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductVariationMediaItemModule.ProductVariationMediaItemBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductVariationMediaItemBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductVariationMediaItemBaseFrontend, BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBase>

    {
		
        
        protected ProductVariationMediaItemBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
