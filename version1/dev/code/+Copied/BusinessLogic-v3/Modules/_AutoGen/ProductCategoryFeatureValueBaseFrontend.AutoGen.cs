using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductCategoryFeatureValueModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductCategoryFeatureValueBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductCategoryFeatureValueBaseFrontend, BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBase>

    {
		
        
        protected ProductCategoryFeatureValueBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
