using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.AffiliatePaymentInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "AffiliatePaymentInfo")]
    public abstract class AffiliatePaymentInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IAffiliatePaymentInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static AffiliatePaymentInfoBaseFactory Factory
        {
            get
            {
                return AffiliatePaymentInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static AffiliatePaymentInfoBase CreateNewItem()
        {
            return (AffiliatePaymentInfoBase)Factory.CreateNewItem();
        }

		public static AffiliatePaymentInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<AffiliatePaymentInfoBase, AffiliatePaymentInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static AffiliatePaymentInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static AffiliatePaymentInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<AffiliatePaymentInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<AffiliatePaymentInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<AffiliatePaymentInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? AffiliateID
		{
		 	get 
		 	{
		 		return (this.Affiliate != null ? (long?)this.Affiliate.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase _affiliate;
        public virtual BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase Affiliate 
        {
        	get
        	{
        		return _affiliate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_affiliate,value);
        		_affiliate = value;
        	}
        }
            
        BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase BusinessLogic_v3.Modules._AutoGen.IAffiliatePaymentInfoBaseAutoGen.Affiliate 
        {
            get
            {
            	return this.Affiliate;
            }
            set
            {
            	this.Affiliate = (BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentmethod;
        
        public virtual string PaymentMethod 
        {
        	get
        	{
        		return _paymentmethod;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentmethod,value);
        		_paymentmethod = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _datepaymentissued;
        
        public virtual DateTime DatePaymentIssued 
        {
        	get
        	{
        		return _datepaymentissued;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datepaymentissued,value);
        		_datepaymentissued = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _additionalcomments;
        
        public virtual string AdditionalComments 
        {
        	get
        	{
        		return _additionalcomments;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_additionalcomments,value);
        		_additionalcomments = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _total;
        
        public virtual double Total 
        {
        	get
        	{
        		return _total;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_total,value);
        		_total = value;
        		
        	}
        }
        
/*
		public virtual long? CurrencyID
		{
		 	get 
		 	{
		 		return (this.Currency != null ? (long?)this.Currency.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase _currency;
        public virtual BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase Currency 
        {
        	get
        	{
        		return _currency;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_currency,value);
        		_currency = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase BusinessLogic_v3.Modules._AutoGen.IAffiliatePaymentInfoBaseAutoGen.Currency 
        {
            get
            {
            	return this.Currency;
            }
            set
            {
            	this.Currency = (BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
