using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "MemberSubscriptionTypePrice")]
    public abstract class MemberSubscriptionTypePriceBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IMemberSubscriptionTypePriceBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static MemberSubscriptionTypePriceBaseFactory Factory
        {
            get
            {
                return MemberSubscriptionTypePriceBaseFactory.Instance; 
            }
        }    
        /*
        public static MemberSubscriptionTypePriceBase CreateNewItem()
        {
            return (MemberSubscriptionTypePriceBase)Factory.CreateNewItem();
        }

		public static MemberSubscriptionTypePriceBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MemberSubscriptionTypePriceBase, MemberSubscriptionTypePriceBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MemberSubscriptionTypePriceBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MemberSubscriptionTypePriceBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MemberSubscriptionTypePriceBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MemberSubscriptionTypePriceBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MemberSubscriptionTypePriceBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private int _durationindays;
        
        public virtual int DurationInDays 
        {
        	get
        	{
        		return _durationindays;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_durationindays,value);
        		_durationindays = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
/*
		public virtual long? MemberSubscriptionTypeID
		{
		 	get 
		 	{
		 		return (this.MemberSubscriptionType != null ? (long?)this.MemberSubscriptionType.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase _membersubscriptiontype;
        public virtual BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase MemberSubscriptionType 
        {
        	get
        	{
        		return _membersubscriptiontype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_membersubscriptiontype,value);
        		_membersubscriptiontype = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.IMemberSubscriptionTypeBase BusinessLogic_v3.Modules._AutoGen.IMemberSubscriptionTypePriceBaseAutoGen.MemberSubscriptionType 
        {
            get
            {
            	return this.MemberSubscriptionType;
            }
            set
            {
            	this.MemberSubscriptionType = (BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private int? _notification1beforeindays;
        
        public virtual int? Notification1BeforeInDays 
        {
        	get
        	{
        		return _notification1beforeindays;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notification1beforeindays,value);
        		_notification1beforeindays = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int? _notification2beforeindays;
        
        public virtual int? Notification2BeforeInDays 
        {
        	get
        	{
        		return _notification2beforeindays;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notification2beforeindays,value);
        		_notification2beforeindays = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _price;
        
        public virtual double Price 
        {
        	get
        	{
        		return _price;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_price,value);
        		_price = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __MemberSubscriptionLinksCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase, 
        	BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase,
        	BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase, BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase>
        {
            private BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase _item = null;
            public __MemberSubscriptionLinksCollectionInfo(BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase> Collection
            {
                get { return _item.__collection__MemberSubscriptionLinks; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase, BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase item, BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase value)
            {
                item.SubscriptionTypePrice = value;
            }
        }
        
        private __MemberSubscriptionLinksCollectionInfo _MemberSubscriptionLinks = null;
        public __MemberSubscriptionLinksCollectionInfo MemberSubscriptionLinks
        {
            get
            {
                if (_MemberSubscriptionLinks == null)
                    _MemberSubscriptionLinks = new __MemberSubscriptionLinksCollectionInfo((BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase)this);
                return _MemberSubscriptionLinks;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase> IMemberSubscriptionTypePriceBaseAutoGen.MemberSubscriptionLinks
        {
            get {  return this.MemberSubscriptionLinks; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase> __collection__MemberSubscriptionLinks
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberSubscriptionLinkBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __OrderItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase, 
        	BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase,
        	BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase, BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>
        {
            private BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase _item = null;
            public __OrderItemsCollectionInfo(BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase> Collection
            {
                get { return _item.__collection__OrderItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase, BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase item, BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase value)
            {
                item.SubscriptionTypePrice = value;
            }
        }
        
        private __OrderItemsCollectionInfo _OrderItems = null;
        public __OrderItemsCollectionInfo OrderItems
        {
            get
            {
                if (_OrderItems == null)
                    _OrderItems = new __OrderItemsCollectionInfo((BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase)this);
                return _OrderItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase> IMemberSubscriptionTypePriceBaseAutoGen.OrderItems
        {
            get {  return this.OrderItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase> __collection__OrderItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'OrderItemBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ShoppingCartItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase, 
        	BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase,
        	BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase, BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>
        {
            private BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase _item = null;
            public __ShoppingCartItemsCollectionInfo(BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase> Collection
            {
                get { return _item.__collection__ShoppingCartItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase, BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase item, BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase value)
            {
                item.LinkedSubscriptionTypePrice = value;
            }
        }
        
        private __ShoppingCartItemsCollectionInfo _ShoppingCartItems = null;
        public __ShoppingCartItemsCollectionInfo ShoppingCartItems
        {
            get
            {
                if (_ShoppingCartItems == null)
                    _ShoppingCartItems = new __ShoppingCartItemsCollectionInfo((BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase)this);
                return _ShoppingCartItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase> IMemberSubscriptionTypePriceBaseAutoGen.ShoppingCartItems
        {
            get {  return this.ShoppingCartItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ShoppingCartItemModule.ShoppingCartItemBase> __collection__ShoppingCartItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ShoppingCartItemBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
