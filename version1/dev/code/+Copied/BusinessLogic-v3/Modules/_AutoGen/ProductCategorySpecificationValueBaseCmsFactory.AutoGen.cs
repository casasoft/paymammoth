using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;
using BusinessLogic_v3.Cms.ProductCategorySpecificationValueModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ProductCategorySpecificationValueBaseCmsFactory_AutoGen : CmsFactoryBase<ProductCategorySpecificationValueBaseCmsInfo, ProductCategorySpecificationValueBase>
    {
       
       public new static ProductCategorySpecificationValueBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductCategorySpecificationValueBaseCmsFactory)CmsFactoryBase<ProductCategorySpecificationValueBaseCmsInfo, ProductCategorySpecificationValueBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ProductCategorySpecificationValueBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductCategorySpecificationValue.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductCategorySpecificationValue";

            this.QueryStringParamID = "ProductCategorySpecificationValueId";

            cmsInfo.TitlePlural = "Product Category Specification Values";

            cmsInfo.TitleSingular =  "Product Category Specification Value";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductCategorySpecificationValueBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ProductCategorySpecificationValue/";
			UsedInProject = ProductCategorySpecificationValueBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
