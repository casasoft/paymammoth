using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductVariationSizeModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductVariationSizeModule.ProductVariationSizeBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductVariationSizeModule.IProductVariationSizeBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductVariationSizeModule.ProductVariationSizeBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductVariationSizeModule.IProductVariationSizeBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductVariationSizeModule.ProductVariationSizeBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductVariationSizeBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductVariationSizeBaseFrontend, BusinessLogic_v3.Modules.ProductVariationSizeModule.IProductVariationSizeBase>

    {
		
        
        protected ProductVariationSizeBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
