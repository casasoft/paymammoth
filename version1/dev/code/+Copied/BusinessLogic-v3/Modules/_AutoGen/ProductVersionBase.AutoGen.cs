using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductVersionModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ProductVersion")]
    public abstract class ProductVersionBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductVersionBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ProductVersionBaseFactory Factory
        {
            get
            {
                return ProductVersionBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductVersionBase CreateNewItem()
        {
            return (ProductVersionBase)Factory.CreateNewItem();
        }

		public static ProductVersionBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductVersionBase, ProductVersionBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductVersionBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductVersionBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductVersionBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductVersionBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductVersionBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private DateTime _releasedate;
        
        public virtual DateTime ReleaseDate 
        {
        	get
        	{
        		return _releasedate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_releasedate,value);
        		_releasedate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _downloadlinkfilename;
        
        public virtual string DownloadLinkFilename 
        {
        	get
        	{
        		return _downloadlinkfilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_downloadlinkfilename,value);
        		_downloadlinkfilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _triallinkfilename;
        
        public virtual string TrialLinkFilename 
        {
        	get
        	{
        		return _triallinkfilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_triallinkfilename,value);
        		_triallinkfilename = value;
        		
        	}
        }
        
/*
		public virtual long? ProductID
		{
		 	get 
		 	{
		 		return (this.Product != null ? (long?)this.Product.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _product;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase Product 
        {
        	get
        	{
        		return _product;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_product,value);
        		_product = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.IProductVersionBaseAutoGen.Product 
        {
            get
            {
            	return this.Product;
            }
            set
            {
            	this.Product = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _releasedetails;
        
        public virtual string ReleaseDetails 
        {
        	get
        	{
        		return _releasedetails;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_releasedetails,value);
        		_releasedetails = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ProductVersionMediaItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase, 
        	BusinessLogic_v3.Modules.ProductVersionMediaItemModule.ProductVersionMediaItemBase,
        	BusinessLogic_v3.Modules.ProductVersionMediaItemModule.IProductVersionMediaItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase, BusinessLogic_v3.Modules.ProductVersionMediaItemModule.ProductVersionMediaItemBase>
        {
            private BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase _item = null;
            public __ProductVersionMediaItemsCollectionInfo(BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductVersionMediaItemModule.ProductVersionMediaItemBase> Collection
            {
                get { return _item.__collection__ProductVersionMediaItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase, BusinessLogic_v3.Modules.ProductVersionMediaItemModule.ProductVersionMediaItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductVersionMediaItemModule.ProductVersionMediaItemBase item, BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase value)
            {
                item.ProductVersion = value;
            }
        }
        
        private __ProductVersionMediaItemsCollectionInfo _ProductVersionMediaItems = null;
        public __ProductVersionMediaItemsCollectionInfo ProductVersionMediaItems
        {
            get
            {
                if (_ProductVersionMediaItems == null)
                    _ProductVersionMediaItems = new __ProductVersionMediaItemsCollectionInfo((BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase)this);
                return _ProductVersionMediaItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductVersionMediaItemModule.IProductVersionMediaItemBase> IProductVersionBaseAutoGen.ProductVersionMediaItems
        {
            get {  return this.ProductVersionMediaItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductVersionMediaItemModule.ProductVersionMediaItemBase> __collection__ProductVersionMediaItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductVersionMediaItemBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
