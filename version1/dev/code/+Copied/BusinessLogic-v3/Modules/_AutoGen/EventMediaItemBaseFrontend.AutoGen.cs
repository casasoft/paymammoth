using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.EventMediaItemModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.EventMediaItemModule.EventMediaItemBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.EventMediaItemModule.EventMediaItemBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBase item)
        {
        	return BusinessLogic_v3.Frontend.EventMediaItemModule.EventMediaItemBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventMediaItemBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<EventMediaItemBaseFrontend, BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBase>

    {
		
        
        protected EventMediaItemBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
