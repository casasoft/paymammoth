using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Modules.BannerModule;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;

namespace BusinessLogic_v3.Modules._AutoGen
{
    
    public abstract class BannerBaseMap_AutoGen<TData> : BusinessLogic_v3.Classes.NHibernateClasses.Mappings.BaseClassMap<TData>
    	where TData : BannerBase
    {
        public BannerBaseMap_AutoGen()
        {
            
        }
        
        


// [baseclassmap_properties]

//BaseClassMap_Property
             
        protected virtual void TitleMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void MediaItemFilenameMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void LinkMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void DurationMSMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void HrefTargetMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void SlideshowDelaySecMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void HTMLDescriptionMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        



// [baseclassmap_collections]

//BaseClassMap_Collection_ManyToManyLeft

        protected virtual void CulturesMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
           //mapInfo.RelationshipType = NHIBERNATE_COLLECTION_RELATIONSHIP.ManyToMany;
           if (string.IsNullOrEmpty(mapInfo.ManyToManyTableName )) mapInfo.ManyToManyTableName = "BannerCultures";
                           
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }  
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = false;
           if (string.IsNullOrEmpty(mapInfo.ParentKey)) mapInfo.ParentKey = "CultureID";
           if (string.IsNullOrEmpty(mapInfo.ChildKey)) mapInfo.ChildKey = "BannerID";

        }
        
            
        
        protected override void initMappings()
        {
           
            
//[BaseClassMap_InitProperties]

//[BaseClassMap_InitCollections]                
            
            base.initMappings();
        }
        
        
        
        
        
    }
   
}
