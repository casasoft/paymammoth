using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.MemberSubscriptionLinkModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "MemberSubscriptionLink")]
    public abstract class MemberSubscriptionLinkBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IMemberSubscriptionLinkBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static MemberSubscriptionLinkBaseFactory Factory
        {
            get
            {
                return MemberSubscriptionLinkBaseFactory.Instance; 
            }
        }    
        /*
        public static MemberSubscriptionLinkBase CreateNewItem()
        {
            return (MemberSubscriptionLinkBase)Factory.CreateNewItem();
        }

		public static MemberSubscriptionLinkBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MemberSubscriptionLinkBase, MemberSubscriptionLinkBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MemberSubscriptionLinkBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MemberSubscriptionLinkBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MemberSubscriptionLinkBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MemberSubscriptionLinkBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MemberSubscriptionLinkBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? SubscriptionTypePriceID
		{
		 	get 
		 	{
		 		return (this.SubscriptionTypePrice != null ? (long?)this.SubscriptionTypePrice.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase _subscriptiontypeprice;
        public virtual BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase SubscriptionTypePrice 
        {
        	get
        	{
        		return _subscriptiontypeprice;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subscriptiontypeprice,value);
        		_subscriptiontypeprice = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBase BusinessLogic_v3.Modules._AutoGen.IMemberSubscriptionLinkBaseAutoGen.SubscriptionTypePrice 
        {
            get
            {
            	return this.SubscriptionTypePrice;
            }
            set
            {
            	this.SubscriptionTypePrice = (BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private DateTime _currentsubscriptionenddate;
        
        public virtual DateTime CurrentSubscriptionEndDate 
        {
        	get
        	{
        		return _currentsubscriptionenddate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_currentsubscriptionenddate,value);
        		_currentsubscriptionenddate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _currentsubscriptionstartdate;
        
        public virtual DateTime CurrentSubscriptionStartDate 
        {
        	get
        	{
        		return _currentsubscriptionstartdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_currentsubscriptionstartdate,value);
        		_currentsubscriptionstartdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _issubscriptionexpired;
        
        public virtual bool IsSubscriptionExpired 
        {
        	get
        	{
        		return _issubscriptionexpired;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_issubscriptionexpired,value);
        		_issubscriptionexpired = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _subscriptionexpirynotificationsent;
        
        public virtual bool SubscriptionExpiryNotificationSent 
        {
        	get
        	{
        		return _subscriptionexpirynotificationsent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subscriptionexpirynotificationsent,value);
        		_subscriptionexpirynotificationsent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _subscriptionnotification1sent;
        
        public virtual bool SubscriptionNotification1Sent 
        {
        	get
        	{
        		return _subscriptionnotification1sent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subscriptionnotification1sent,value);
        		_subscriptionnotification1sent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _subscriptionnotification2sent;
        
        public virtual bool SubscriptionNotification2Sent 
        {
        	get
        	{
        		return _subscriptionnotification2sent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subscriptionnotification2sent,value);
        		_subscriptionnotification2sent = value;
        		
        	}
        }
        
/*
		public virtual long? MemberID
		{
		 	get 
		 	{
		 		return (this.Member != null ? (long?)this.Member.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _member;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase Member 
        {
        	get
        	{
        		return _member;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_member,value);
        		_member = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IMemberSubscriptionLinkBaseAutoGen.Member 
        {
            get
            {
            	return this.Member;
            }
            set
            {
            	this.Member = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
/*
		public virtual long? OrderID
		{
		 	get 
		 	{
		 		return (this.Order != null ? (long?)this.Order.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.OrderModule.OrderBase _order;
        public virtual BusinessLogic_v3.Modules.OrderModule.OrderBase Order 
        {
        	get
        	{
        		return _order;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_order,value);
        		_order = value;
        	}
        }
            
        BusinessLogic_v3.Modules.OrderModule.IOrderBase BusinessLogic_v3.Modules._AutoGen.IMemberSubscriptionLinkBaseAutoGen.Order 
        {
            get
            {
            	return this.Order;
            }
            set
            {
            	this.Order = (BusinessLogic_v3.Modules.OrderModule.OrderBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
