using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Modules.Article_CultureInfoModule;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;

namespace BusinessLogic_v3.Modules._AutoGen
{
    
    public abstract class Article_CultureInfoBaseMap_AutoGen<TData> : BusinessLogic_v3.Classes.NHibernateClasses.Mappings.BaseClassMap<TData>
    	where TData : Article_CultureInfoBase
    {
        public Article_CultureInfoBaseMap_AutoGen()
        {
            
        }
        
        


// [baseclassmap_properties]

//BaseClassMap_PropertyLinkedObject

        protected virtual void CultureInfoMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            mapInfo.CollectionType = BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "CultureInfoId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        
//BaseClassMap_PropertyLinkedObject

        protected virtual void ArticleMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            mapInfo.CollectionType = BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "ArticleId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        
//BaseClassMap_Property
             
        protected virtual void HtmlTextMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void HtmlText_SearchMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void MetaKeywordsMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void MetaKeywords_SearchMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void MetaDescriptionMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void MetaDescription_SearchMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void PageTitleMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void PageTitle_SearchMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void DialogWidthMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void DialogHeightMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void SubTitleMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void _ComputedURLMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void _Computed_ParentArticleNamesMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        



// [baseclassmap_collections]

            
        
        protected override void initMappings()
        {
           
            
//[BaseClassMap_InitProperties]

//[BaseClassMap_InitCollections]                
            
            base.initMappings();
        }
        
        
        
        
        
    }
   
}
