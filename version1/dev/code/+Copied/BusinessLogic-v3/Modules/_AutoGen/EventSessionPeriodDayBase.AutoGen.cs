using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.EventSessionPeriodDayModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "EventSessionPeriodDay")]
    public abstract class EventSessionPeriodDayBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IEventSessionPeriodDayBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static EventSessionPeriodDayBaseFactory Factory
        {
            get
            {
                return EventSessionPeriodDayBaseFactory.Instance; 
            }
        }    
        /*
        public static EventSessionPeriodDayBase CreateNewItem()
        {
            return (EventSessionPeriodDayBase)Factory.CreateNewItem();
        }

		public static EventSessionPeriodDayBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<EventSessionPeriodDayBase, EventSessionPeriodDayBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static EventSessionPeriodDayBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static EventSessionPeriodDayBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<EventSessionPeriodDayBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<EventSessionPeriodDayBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<EventSessionPeriodDayBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private DateTime _day;
        
        public virtual DateTime Day 
        {
        	get
        	{
        		return _day;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_day,value);
        		_day = value;
        		
        	}
        }
        
/*
		public virtual long? EventSessionPeriodID
		{
		 	get 
		 	{
		 		return (this.EventSessionPeriod != null ? (long?)this.EventSessionPeriod.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase _eventsessionperiod;
        public virtual BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase EventSessionPeriod 
        {
        	get
        	{
        		return _eventsessionperiod;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_eventsessionperiod,value);
        		_eventsessionperiod = value;
        	}
        }
            
        BusinessLogic_v3.Modules.EventSessionPeriodModule.IEventSessionPeriodBase BusinessLogic_v3.Modules._AutoGen.IEventSessionPeriodDayBaseAutoGen.EventSessionPeriod 
        {
            get
            {
            	return this.EventSessionPeriod;
            }
            set
            {
            	this.EventSessionPeriod = (BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
