using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.AdvertSlotModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "AdvertSlot")]
    public abstract class AdvertSlotBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IAdvertSlotBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static AdvertSlotBaseFactory Factory
        {
            get
            {
                return AdvertSlotBaseFactory.Instance; 
            }
        }    
        /*
        public static AdvertSlotBase CreateNewItem()
        {
            return (AdvertSlotBase)Factory.CreateNewItem();
        }

		public static AdvertSlotBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<AdvertSlotBase, AdvertSlotBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static AdvertSlotBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static AdvertSlotBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<AdvertSlotBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<AdvertSlotBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<AdvertSlotBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? AdvertColumnID
		{
		 	get 
		 	{
		 		return (this.AdvertColumn != null ? (long?)this.AdvertColumn.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase _advertcolumn;
        public virtual BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase AdvertColumn 
        {
        	get
        	{
        		return _advertcolumn;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_advertcolumn,value);
        		_advertcolumn = value;
        	}
        }
            
        BusinessLogic_v3.Modules.AdvertColumnModule.IAdvertColumnBase BusinessLogic_v3.Modules._AutoGen.IAdvertSlotBaseAutoGen.AdvertColumn 
        {
            get
            {
            	return this.AdvertColumn;
            }
            set
            {
            	this.AdvertColumn = (BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _roundno;
        
        public virtual int RoundNo 
        {
        	get
        	{
        		return _roundno;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_roundno,value);
        		_roundno = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _width;
        
        public virtual int Width 
        {
        	get
        	{
        		return _width;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_width,value);
        		_width = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _height;
        
        public virtual int Height 
        {
        	get
        	{
        		return _height;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_height,value);
        		_height = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __AdvertsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase, 
        	BusinessLogic_v3.Modules.AdvertModule.AdvertBase,
        	BusinessLogic_v3.Modules.AdvertModule.IAdvertBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase, BusinessLogic_v3.Modules.AdvertModule.AdvertBase>
        {
            private BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase _item = null;
            public __AdvertsCollectionInfo(BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.AdvertModule.AdvertBase> Collection
            {
                get { return _item.__collection__Adverts; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase, BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.SetLinkOnItem(BusinessLogic_v3.Modules.AdvertModule.AdvertBase item, BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase value)
            {
                item.AdvertSlot = value;
            }
        }
        
        private __AdvertsCollectionInfo _Adverts = null;
        public __AdvertsCollectionInfo Adverts
        {
            get
            {
                if (_Adverts == null)
                    _Adverts = new __AdvertsCollectionInfo((BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase)this);
                return _Adverts;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.AdvertModule.IAdvertBase> IAdvertSlotBaseAutoGen.Adverts
        {
            get {  return this.Adverts; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.AdvertModule.AdvertBase> __collection__Adverts
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'AdvertBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
