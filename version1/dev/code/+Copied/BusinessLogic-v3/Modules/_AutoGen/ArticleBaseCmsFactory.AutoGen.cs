using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Cms.ArticleModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ArticleBaseCmsFactory_AutoGen : CmsFactoryBase<ArticleBaseCmsInfo, ArticleBase>
    {
       
       public new static ArticleBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ArticleBaseCmsFactory)CmsFactoryBase<ArticleBaseCmsInfo, ArticleBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ArticleBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Article.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Article";

            this.QueryStringParamID = "ArticleId";

            cmsInfo.TitlePlural = "Articles";

            cmsInfo.TitleSingular =  "Article";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ArticleBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Article/";
			UsedInProject = ArticleBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
