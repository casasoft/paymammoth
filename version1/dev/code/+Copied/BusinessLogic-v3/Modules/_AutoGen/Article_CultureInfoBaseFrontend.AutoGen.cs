using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.Article_CultureInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.Article_CultureInfoModule.Article_CultureInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.Article_CultureInfoModule.Article_CultureInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.Article_CultureInfoModule.Article_CultureInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Article_CultureInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<Article_CultureInfoBaseFrontend, BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBase>

    {
		
        
        protected Article_CultureInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
