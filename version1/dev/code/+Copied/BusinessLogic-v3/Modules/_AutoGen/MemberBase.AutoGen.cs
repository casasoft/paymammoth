using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.MemberModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Member")]
    public abstract class MemberBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IMemberBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static MemberBaseFactory Factory
        {
            get
            {
                return MemberBaseFactory.Instance; 
            }
        }    
        /*
        public static MemberBase CreateNewItem()
        {
            return (MemberBase)Factory.CreateNewItem();
        }

		public static MemberBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MemberBase, MemberBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MemberBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MemberBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MemberBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MemberBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MemberBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private DateTime _dateregistered;
        
        public virtual DateTime DateRegistered 
        {
        	get
        	{
        		return _dateregistered;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dateregistered,value);
        		_dateregistered = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _username;
        
        public virtual string Username 
        {
        	get
        	{
        		return _username;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_username,value);
        		_username = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _password;
        
        public virtual string Password 
        {
        	get
        	{
        		return _password;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_password,value);
        		_password = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _firstname;
        
        public virtual string FirstName 
        {
        	get
        	{
        		return _firstname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_firstname,value);
        		_firstname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _lastname;
        
        public virtual string LastName 
        {
        	get
        	{
        		return _lastname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_lastname,value);
        		_lastname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.GENDER _gender;
        
        public virtual CS.General_v3.Enums.GENDER Gender 
        {
        	get
        	{
        		return _gender;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_gender,value);
        		_gender = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _dateofbirth;
        
        public virtual DateTime? DateOfBirth 
        {
        	get
        	{
        		return _dateofbirth;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dateofbirth,value);
        		_dateofbirth = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _address1;
        
        public virtual string Address1 
        {
        	get
        	{
        		return _address1;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_address1,value);
        		_address1 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _address2;
        
        public virtual string Address2 
        {
        	get
        	{
        		return _address2;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_address2,value);
        		_address2 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _address3;
        
        public virtual string Address3 
        {
        	get
        	{
        		return _address3;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_address3,value);
        		_address3 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _locality;
        
        public virtual string Locality 
        {
        	get
        	{
        		return _locality;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_locality,value);
        		_locality = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _state;
        
        public virtual string State 
        {
        	get
        	{
        		return _state;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_state,value);
        		_state = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _postcode;
        
        public virtual string PostCode 
        {
        	get
        	{
        		return _postcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_postcode,value);
        		_postcode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? _country;
        
        public virtual CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? Country 
        {
        	get
        	{
        		return _country;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_country,value);
        		_country = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _idcard;
        
        public virtual string IDCard 
        {
        	get
        	{
        		return _idcard;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_idcard,value);
        		_idcard = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _lastloggedin;
        
        public virtual DateTime? LastLoggedIn 
        {
        	get
        	{
        		return _lastloggedin;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_lastloggedin,value);
        		_lastloggedin = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? __lastloggedin_new;
        
        public virtual DateTime? _LastLoggedIn_New 
        {
        	get
        	{
        		return __lastloggedin_new;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__lastloggedin_new,value);
        		__lastloggedin_new = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _accepted;
        
        public virtual bool Accepted 
        {
        	get
        	{
        		return _accepted;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_accepted,value);
        		_accepted = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _email;
        
        public virtual string Email 
        {
        	get
        	{
        		return _email;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_email,value);
        		_email = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _telephone;
        
        public virtual string Telephone 
        {
        	get
        	{
        		return _telephone;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_telephone,value);
        		_telephone = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _mobile;
        
        public virtual string Mobile 
        {
        	get
        	{
        		return _mobile;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_mobile,value);
        		_mobile = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _fax;
        
        public virtual string Fax 
        {
        	get
        	{
        		return _fax;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_fax,value);
        		_fax = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _remarks;
        
        public virtual string Remarks 
        {
        	get
        	{
        		return _remarks;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_remarks,value);
        		_remarks = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _website;
        
        public virtual string Website 
        {
        	get
        	{
        		return _website;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_website,value);
        		_website = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _activatedon;
        
        public virtual DateTime? ActivatedOn 
        {
        	get
        	{
        		return _activatedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_activatedon,value);
        		_activatedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _activatedip;
        
        public virtual string ActivatedIP 
        {
        	get
        	{
        		return _activatedip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_activatedip,value);
        		_activatedip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _activationcode;
        
        public virtual string ActivationCode 
        {
        	get
        	{
        		return _activationcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_activationcode,value);
        		_activationcode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _sessionguid;
        
        public virtual string SessionGUID 
        {
        	get
        	{
        		return _sessionguid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_sessionguid,value);
        		_sessionguid = value;
        		
        	}
        }
        
/*
		public virtual long? PreferredCultureInfoID
		{
		 	get 
		 	{
		 		return (this.PreferredCultureInfo != null ? (long?)this.PreferredCultureInfo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase _preferredcultureinfo;
        public virtual BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase PreferredCultureInfo 
        {
        	get
        	{
        		return _preferredcultureinfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_preferredcultureinfo,value);
        		_preferredcultureinfo = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase BusinessLogic_v3.Modules._AutoGen.IMemberBaseAutoGen.PreferredCultureInfo 
        {
            get
            {
            	return this.PreferredCultureInfo;
            }
            set
            {
            	this.PreferredCultureInfo = (BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase) value;
            }
        }
            
/*
		public virtual long? ReferredByMemberID
		{
		 	get 
		 	{
		 		return (this.ReferredByMember != null ? (long?)this.ReferredByMember.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _referredbymember;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase ReferredByMember 
        {
        	get
        	{
        		return _referredbymember;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_referredbymember,value);
        		_referredbymember = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IMemberBaseAutoGen.ReferredByMember 
        {
            get
            {
            	return this.ReferredByMember;
            }
            set
            {
            	this.ReferredByMember = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _accountterminated;
        
        public virtual bool AccountTerminated 
        {
        	get
        	{
        		return _accountterminated;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_accountterminated,value);
        		_accountterminated = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _accountterminatedon;
        
        public virtual DateTime? AccountTerminatedOn 
        {
        	get
        	{
        		return _accountterminatedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_accountterminatedon,value);
        		_accountterminatedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _accountterminatedip;
        
        public virtual string AccountTerminatedIP 
        {
        	get
        	{
        		return _accountterminatedip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_accountterminatedip,value);
        		_accountterminatedip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _forgottenpasscode;
        
        public virtual string ForgottenPassCode 
        {
        	get
        	{
        		return _forgottenpasscode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_forgottenpasscode,value);
        		_forgottenpasscode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _subscribedtonewsletter;
        
        public virtual bool SubscribedToNewsletter 
        {
        	get
        	{
        		return _subscribedtonewsletter;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subscribedtonewsletter,value);
        		_subscribedtonewsletter = value;
        		
        	}
        }
        
/*
		public virtual long? CurrentShoppingCartID
		{
		 	get 
		 	{
		 		return (this.CurrentShoppingCart != null ? (long?)this.CurrentShoppingCart.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase _currentshoppingcart;
        public virtual BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase CurrentShoppingCart 
        {
        	get
        	{
        		return _currentshoppingcart;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_currentshoppingcart,value);
        		_currentshoppingcart = value;
        	}
        }
            
		//BaseClass/Fields/Property_Normal
        private string _howdidyoufindus;
        
        public virtual string HowDidYouFindUs 
        {
        	get
        	{
        		return _howdidyoufindus;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_howdidyoufindus,value);
        		_howdidyoufindus = value;
        		
        	}
        }
        
/*
		public virtual long? LinkedAffiliateID
		{
		 	get 
		 	{
		 		return (this.LinkedAffiliate != null ? (long?)this.LinkedAffiliate.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase _linkedaffiliate;
        public virtual BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase LinkedAffiliate 
        {
        	get
        	{
        		return _linkedaffiliate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkedaffiliate,value);
        		_linkedaffiliate = value;
        	}
        }
            
        BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase BusinessLogic_v3.Modules._AutoGen.IMemberBaseAutoGen.LinkedAffiliate 
        {
            get
            {
            	return this.LinkedAffiliate;
            }
            set
            {
            	this.LinkedAffiliate = (BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _companyname;
        
        public virtual string CompanyName 
        {
        	get
        	{
        		return _companyname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_companyname,value);
        		_companyname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE _passwordencryptiontype;
        
        public virtual CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE PasswordEncryptionType 
        {
        	get
        	{
        		return _passwordencryptiontype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_passwordencryptiontype,value);
        		_passwordencryptiontype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _passwordsalt;
        
        public virtual string PasswordSalt 
        {
        	get
        	{
        		return _passwordsalt;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_passwordsalt,value);
        		_passwordsalt = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _passworditerations;
        
        public virtual int PasswordIterations 
        {
        	get
        	{
        		return _passworditerations;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_passworditerations,value);
        		_passworditerations = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _subscriptionnotification1sent;
        
        public virtual bool SubscriptionNotification1Sent 
        {
        	get
        	{
        		return _subscriptionnotification1sent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subscriptionnotification1sent,value);
        		_subscriptionnotification1sent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _subscriptionnotification2sent;
        
        public virtual bool SubscriptionNotification2Sent 
        {
        	get
        	{
        		return _subscriptionnotification2sent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subscriptionnotification2sent,value);
        		_subscriptionnotification2sent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paid;
        
        public virtual bool Paid 
        {
        	get
        	{
        		return _paid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paid,value);
        		_paid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _activated;
        
        public virtual bool Activated 
        {
        	get
        	{
        		return _activated;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_activated,value);
        		_activated = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _middlename;
        
        public virtual string MiddleName 
        {
        	get
        	{
        		return _middlename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_middlename,value);
        		_middlename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _subscriptionexpirynotificationsent;
        
        public virtual bool SubscriptionExpiryNotificationSent 
        {
        	get
        	{
        		return _subscriptionexpirynotificationsent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subscriptionexpirynotificationsent,value);
        		_subscriptionexpirynotificationsent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _referralsuccessful;
        
        public virtual bool ReferralSuccessful 
        {
        	get
        	{
        		return _referralsuccessful;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_referralsuccessful,value);
        		_referralsuccessful = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _referredfromurl;
        
        public virtual string ReferredFromUrl 
        {
        	get
        	{
        		return _referredfromurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_referredfromurl,value);
        		_referredfromurl = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _accountbalance;
        
        public virtual double AccountBalance 
        {
        	get
        	{
        		return _accountbalance;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_accountbalance,value);
        		_accountbalance = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _blocked;
        
        public virtual bool Blocked 
        {
        	get
        	{
        		return _blocked;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_blocked,value);
        		_blocked = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _selfexcludeduntil;
        
        public virtual DateTime? SelfExcludedUntil 
        {
        	get
        	{
        		return _selfexcludeduntil;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_selfexcludeduntil,value);
        		_selfexcludeduntil = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _selfexclusionseton;
        
        public virtual DateTime? SelfExclusionSetOn 
        {
        	get
        	{
        		return _selfexclusionseton;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_selfexclusionseton,value);
        		_selfexclusionseton = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _selfexclusionsetbyip;
        
        public virtual string SelfExclusionSetByIp 
        {
        	get
        	{
        		return _selfexclusionsetbyip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_selfexclusionsetbyip,value);
        		_selfexclusionsetbyip = value;
        		
        	}
        }
        
/*
		public virtual long? AssignedToTicketingSystemSupportUserID
		{
		 	get 
		 	{
		 		return (this.AssignedToTicketingSystemSupportUser != null ? (long?)this.AssignedToTicketingSystemSupportUser.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _assignedtoticketingsystemsupportuser;
        public virtual BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase AssignedToTicketingSystemSupportUser 
        {
        	get
        	{
        		return _assignedtoticketingsystemsupportuser;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_assignedtoticketingsystemsupportuser,value);
        		_assignedtoticketingsystemsupportuser = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase BusinessLogic_v3.Modules._AutoGen.IMemberBaseAutoGen.AssignedToTicketingSystemSupportUser 
        {
            get
            {
            	return this.AssignedToTicketingSystemSupportUser;
            }
            set
            {
            	this.AssignedToTicketingSystemSupportUser = (BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _preferredtimezoneid;
        
        public virtual string PreferredTimezoneID 
        {
        	get
        	{
        		return _preferredtimezoneid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_preferredtimezoneid,value);
        		_preferredtimezoneid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE _usernameencryptiontype;
        
        public virtual CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE UsernameEncryptionType 
        {
        	get
        	{
        		return _usernameencryptiontype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_usernameencryptiontype,value);
        		_usernameencryptiontype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _usernameencryptionsalt;
        
        public virtual string UsernameEncryptionSalt 
        {
        	get
        	{
        		return _usernameencryptionsalt;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_usernameencryptionsalt,value);
        		_usernameencryptionsalt = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _usernameencryptioniterations;
        
        public virtual int UsernameEncryptionIterations 
        {
        	get
        	{
        		return _usernameencryptioniterations;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_usernameencryptioniterations,value);
        		_usernameencryptioniterations = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.MEMBER_BLOCKED_REASON _blockedreason;
        
        public virtual CS.General_v3.Enums.MEMBER_BLOCKED_REASON BlockedReason 
        {
        	get
        	{
        		return _blockedreason;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_blockedreason,value);
        		_blockedreason = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _blockedon;
        
        public virtual DateTime BlockedOn 
        {
        	get
        	{
        		return _blockedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_blockedon,value);
        		_blockedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _blockeduntil;
        
        public virtual DateTime? BlockedUntil 
        {
        	get
        	{
        		return _blockeduntil;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_blockeduntil,value);
        		_blockeduntil = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _blockedcustommessage;
        
        public virtual string BlockedCustomMessage 
        {
        	get
        	{
        		return _blockedcustommessage;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_blockedcustommessage,value);
        		_blockedcustommessage = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _currenttotalinvalidloginattempts;
        
        public virtual int CurrentTotalInvalidLoginAttempts 
        {
        	get
        	{
        		return _currenttotalinvalidloginattempts;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_currenttotalinvalidloginattempts,value);
        		_currenttotalinvalidloginattempts = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _kycverified;
        
        public virtual bool KYCVerified 
        {
        	get
        	{
        		return _kycverified;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_kycverified,value);
        		_kycverified = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _kycproofofaddressfilename;
        
        public virtual string KYCProofOfAddressFilename 
        {
        	get
        	{
        		return _kycproofofaddressfilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_kycproofofaddressfilename,value);
        		_kycproofofaddressfilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _kycproofofidentityfilename;
        
        public virtual string KYCProofOfIdentityFilename 
        {
        	get
        	{
        		return _kycproofofidentityfilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_kycproofofidentityfilename,value);
        		_kycproofofidentityfilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _kycprocessstarted;
        
        public virtual bool KYCProcessStarted 
        {
        	get
        	{
        		return _kycprocessstarted;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_kycprocessstarted,value);
        		_kycprocessstarted = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _kycprocessstartedon;
        
        public virtual DateTime? KYCProcessStartedOn 
        {
        	get
        	{
        		return _kycprocessstartedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_kycprocessstartedon,value);
        		_kycprocessstartedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isfeatured;
        
        public virtual bool IsFeatured 
        {
        	get
        	{
        		return _isfeatured;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isfeatured,value);
        		_isfeatured = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _imagefilename;
        
        public virtual string ImageFilename 
        {
        	get
        	{
        		return _imagefilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagefilename,value);
        		_imagefilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _companyprofile;
        
        public virtual string CompanyProfile 
        {
        	get
        	{
        		return _companyprofile;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_companyprofile,value);
        		_companyprofile = value;
        		
        	}
        }
        
/*
		public virtual long? AutoRenewSubscriptionTypePriceID
		{
		 	get 
		 	{
		 		return (this.AutoRenewSubscriptionTypePrice != null ? (long?)this.AutoRenewSubscriptionTypePrice.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase _autorenewsubscriptiontypeprice;
        public virtual BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase AutoRenewSubscriptionTypePrice 
        {
        	get
        	{
        		return _autorenewsubscriptiontypeprice;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_autorenewsubscriptiontypeprice,value);
        		_autorenewsubscriptiontypeprice = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBase BusinessLogic_v3.Modules._AutoGen.IMemberBaseAutoGen.AutoRenewSubscriptionTypePrice 
        {
            get
            {
            	return this.AutoRenewSubscriptionTypePrice;
            }
            set
            {
            	this.AutoRenewSubscriptionTypePrice = (BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _autorenewlastmessage;
        
        public virtual string AutoRenewLastMessage 
        {
        	get
        	{
        		return _autorenewlastmessage;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_autorenewlastmessage,value);
        		_autorenewlastmessage = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _kycprocessremarks;
        
        public virtual string KYCProcessRemarks 
        {
        	get
        	{
        		return _kycprocessremarks;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_kycprocessremarks,value);
        		_kycprocessremarks = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _kycverifiedon;
        
        public virtual DateTime? KYCVerifiedOn 
        {
        	get
        	{
        		return _kycverifiedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_kycverifiedon,value);
        		_kycverifiedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _renewableaccountbalance;
        
        public virtual double RenewableAccountBalance 
        {
        	get
        	{
        		return _renewableaccountbalance;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_renewableaccountbalance,value);
        		_renewableaccountbalance = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _nextrenewableaccountbalancetopupon;
        
        public virtual DateTime? NextRenewableAccountBalanceTopupOn 
        {
        	get
        	{
        		return _nextrenewableaccountbalancetopupon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_nextrenewableaccountbalancetopupon,value);
        		_nextrenewableaccountbalancetopupon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _nextmonthlytopupon;
        
        public virtual DateTime NextMonthlyTopupOn 
        {
        	get
        	{
        		return _nextmonthlytopupon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_nextmonthlytopupon,value);
        		_nextmonthlytopupon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _companyemail;
        
        public virtual string CompanyEmail 
        {
        	get
        	{
        		return _companyemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_companyemail,value);
        		_companyemail = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _companytelephone;
        
        public virtual string CompanyTelephone 
        {
        	get
        	{
        		return _companytelephone;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_companytelephone,value);
        		_companytelephone = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _vat;
        
        public virtual string VAT 
        {
        	get
        	{
        		return _vat;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_vat,value);
        		_vat = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _nationality;
        
        public virtual string Nationality 
        {
        	get
        	{
        		return _nationality;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_nationality,value);
        		_nationality = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _passportno;
        
        public virtual string PassportNo 
        {
        	get
        	{
        		return _passportno;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_passportno,value);
        		_passportno = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _passportplaceofissue;
        
        public virtual string PassportPlaceOfIssue 
        {
        	get
        	{
        		return _passportplaceofissue;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_passportplaceofissue,value);
        		_passportplaceofissue = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _accountregistrationip;
        
        public virtual string AccountRegistrationIP 
        {
        	get
        	{
        		return _accountregistrationip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_accountregistrationip,value);
        		_accountregistrationip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _idcardplaceofissue;
        
        public virtual string IDCardPlaceOfIssue 
        {
        	get
        	{
        		return _idcardplaceofissue;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_idcardplaceofissue,value);
        		_idcardplaceofissue = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _drivinglicense;
        
        public virtual string DrivingLicense 
        {
        	get
        	{
        		return _drivinglicense;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_drivinglicense,value);
        		_drivinglicense = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _drivinglicenseplaceofissue;
        
        public virtual string DrivingLicensePlaceOfIssue 
        {
        	get
        	{
        		return _drivinglicenseplaceofissue;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_drivinglicenseplaceofissue,value);
        		_drivinglicenseplaceofissue = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _registeredthroughsocialmedium;
        
        public virtual bool RegisteredThroughSocialMedium 
        {
        	get
        	{
        		return _registeredthroughsocialmedium;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_registeredthroughsocialmedium,value);
        		_registeredthroughsocialmedium = value;
        		
        	}
        }
        
/*
		public virtual long? ApplicableSpecialOfferVoucherCodeID
		{
		 	get 
		 	{
		 		return (this.ApplicableSpecialOfferVoucherCode != null ? (long?)this.ApplicableSpecialOfferVoucherCode.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase _applicablespecialoffervouchercode;
        public virtual BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase ApplicableSpecialOfferVoucherCode 
        {
        	get
        	{
        		return _applicablespecialoffervouchercode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_applicablespecialoffervouchercode,value);
        		_applicablespecialoffervouchercode = value;
        	}
        }
            
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.PERSONAL_ACCOUNT_TYPE _personalaccounttype;
        
        public virtual CS.General_v3.Enums.PERSONAL_ACCOUNT_TYPE PersonalAccountType 
        {
        	get
        	{
        		return _personalaccounttype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_personalaccounttype,value);
        		_personalaccounttype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private long? _twitteruserid;
        
        public virtual long? TwitterUserID 
        {
        	get
        	{
        		return _twitteruserid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_twitteruserid,value);
        		_twitteruserid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _twitterscreenname;
        
        public virtual string TwitterScreenName 
        {
        	get
        	{
        		return _twitterscreenname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_twitterscreenname,value);
        		_twitterscreenname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _allowpromotions;
        
        public virtual bool AllowPromotions 
        {
        	get
        	{
        		return _allowpromotions;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_allowpromotions,value);
        		_allowpromotions = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _jobposition;
        
        public virtual string JobPosition 
        {
        	get
        	{
        		return _jobposition;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_jobposition,value);
        		_jobposition = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _iscontactperson;
        
        public virtual bool IsContactPerson 
        {
        	get
        	{
        		return _iscontactperson;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_iscontactperson,value);
        		_iscontactperson = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _membertype;
        
        public virtual string MemberType 
        {
        	get
        	{
        		return _membertype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_membertype,value);
        		_membertype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _istestmember;
        
        public virtual bool IsTestMember 
        {
        	get
        	{
        		return _istestmember;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_istestmember,value);
        		_istestmember = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ClassifiedsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase,
        	BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __ClassifiedsCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase> Collection
            {
                get { return _item.__collection__Classifieds; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.CreatedBy = value;
            }
        }
        
        private __ClassifiedsCollectionInfo _Classifieds = null;
        public __ClassifiedsCollectionInfo Classifieds
        {
            get
            {
                if (_Classifieds == null)
                    _Classifieds = new __ClassifiedsCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _Classifieds;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase> IMemberBaseAutoGen.Classifieds
        {
            get {  return this.Classifieds; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ClassifiedModule.ClassifiedBase> __collection__Classifieds
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ClassifiedBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ContactFormsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase,
        	BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __ContactFormsCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase> Collection
            {
                get { return _item.__collection__ContactForms; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.SentByMember = value;
            }
        }
        
        private __ContactFormsCollectionInfo _ContactForms = null;
        public __ContactFormsCollectionInfo ContactForms
        {
            get
            {
                if (_ContactForms == null)
                    _ContactForms = new __ContactFormsCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _ContactForms;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase> IMemberBaseAutoGen.ContactForms
        {
            get {  return this.ContactForms; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase> __collection__ContactForms
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ContactFormBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __MembersCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.MemberModule.MemberBase,
        	BusinessLogic_v3.Modules.MemberModule.IMemberBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberModule.MemberBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __MembersCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> Collection
            {
                get { return _item.__collection__Members; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberModule.MemberBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberModule.MemberBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.ReferredByMember = value;
            }
        }
        
        private __MembersCollectionInfo _Members = null;
        public __MembersCollectionInfo Members
        {
            get
            {
                if (_Members == null)
                    _Members = new __MembersCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _Members;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> IMemberBaseAutoGen.Members
        {
            get {  return this.Members; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> __collection__Members
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __AccountBalanceHistoryItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase,
        	BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __AccountBalanceHistoryItemsCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase> Collection
            {
                get { return _item.__collection__AccountBalanceHistoryItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.Member = value;
            }
        }
        
        private __AccountBalanceHistoryItemsCollectionInfo _AccountBalanceHistoryItems = null;
        public __AccountBalanceHistoryItemsCollectionInfo AccountBalanceHistoryItems
        {
            get
            {
                if (_AccountBalanceHistoryItems == null)
                    _AccountBalanceHistoryItems = new __AccountBalanceHistoryItemsCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _AccountBalanceHistoryItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBase> IMemberBaseAutoGen.AccountBalanceHistoryItems
        {
            get {  return this.AccountBalanceHistoryItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase> __collection__AccountBalanceHistoryItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberAccountBalanceHistoryBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __LoginInfoCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase,
        	BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __LoginInfoCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase> Collection
            {
                get { return _item.__collection__LoginInfo; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.Member = value;
            }
        }
        
        private __LoginInfoCollectionInfo _LoginInfo = null;
        public __LoginInfoCollectionInfo LoginInfo
        {
            get
            {
                if (_LoginInfo == null)
                    _LoginInfo = new __LoginInfoCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _LoginInfo;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBase> IMemberBaseAutoGen.LoginInfo
        {
            get {  return this.LoginInfo; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase> __collection__LoginInfo
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberLoginInfoBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __SentReferralsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase,
        	BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __SentReferralsCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase> Collection
            {
                get { return _item.__collection__SentReferrals; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.SentByMember = value;
            }
        }
        
        private __SentReferralsCollectionInfo _SentReferrals = null;
        public __SentReferralsCollectionInfo SentReferrals
        {
            get
            {
                if (_SentReferrals == null)
                    _SentReferrals = new __SentReferralsCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _SentReferrals;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBase> IMemberBaseAutoGen.SentReferrals
        {
            get {  return this.SentReferrals; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase> __collection__SentReferrals
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberReferralBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __RegisteredReferralsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase,
        	BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __RegisteredReferralsCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase> Collection
            {
                get { return _item.__collection__RegisteredReferrals; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.RegisteredMember = value;
            }
        }
        
        private __RegisteredReferralsCollectionInfo _RegisteredReferrals = null;
        public __RegisteredReferralsCollectionInfo RegisteredReferrals
        {
            get
            {
                if (_RegisteredReferrals == null)
                    _RegisteredReferrals = new __RegisteredReferralsCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _RegisteredReferrals;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBase> IMemberBaseAutoGen.RegisteredReferrals
        {
            get {  return this.RegisteredReferrals; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase> __collection__RegisteredReferrals
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberReferralBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ResponsibleLimitsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase,
        	BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __ResponsibleLimitsCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase> Collection
            {
                get { return _item.__collection__ResponsibleLimits; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.Member = value;
            }
        }
        
        private __ResponsibleLimitsCollectionInfo _ResponsibleLimits = null;
        public __ResponsibleLimitsCollectionInfo ResponsibleLimits
        {
            get
            {
                if (_ResponsibleLimits == null)
                    _ResponsibleLimits = new __ResponsibleLimitsCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _ResponsibleLimits;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBase> IMemberBaseAutoGen.ResponsibleLimits
        {
            get {  return this.ResponsibleLimits; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase> __collection__ResponsibleLimits
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberResponsibleLimitsBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __MemberSelfBarringPeriodsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBase,
        	BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.IMemberSelfBarringPeriodBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __MemberSelfBarringPeriodsCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBase> Collection
            {
                get { return _item.__collection__MemberSelfBarringPeriods; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.Member = value;
            }
        }
        
        private __MemberSelfBarringPeriodsCollectionInfo _MemberSelfBarringPeriods = null;
        public __MemberSelfBarringPeriodsCollectionInfo MemberSelfBarringPeriods
        {
            get
            {
                if (_MemberSelfBarringPeriods == null)
                    _MemberSelfBarringPeriods = new __MemberSelfBarringPeriodsCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _MemberSelfBarringPeriods;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.IMemberSelfBarringPeriodBase> IMemberBaseAutoGen.MemberSelfBarringPeriods
        {
            get {  return this.MemberSelfBarringPeriods; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBase> __collection__MemberSelfBarringPeriods
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberSelfBarringPeriodBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __MemberSubscriptionLinksCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase,
        	BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __MemberSubscriptionLinksCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase> Collection
            {
                get { return _item.__collection__MemberSubscriptionLinks; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.Member = value;
            }
        }
        
        private __MemberSubscriptionLinksCollectionInfo _MemberSubscriptionLinks = null;
        public __MemberSubscriptionLinksCollectionInfo MemberSubscriptionLinks
        {
            get
            {
                if (_MemberSubscriptionLinks == null)
                    _MemberSubscriptionLinks = new __MemberSubscriptionLinksCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _MemberSubscriptionLinks;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase> IMemberBaseAutoGen.MemberSubscriptionLinks
        {
            get {  return this.MemberSubscriptionLinks; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.MemberSubscriptionLinkBase> __collection__MemberSubscriptionLinks
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberSubscriptionLinkBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __OrdersCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.OrderModule.OrderBase,
        	BusinessLogic_v3.Modules.OrderModule.IOrderBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.OrderModule.OrderBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __OrdersCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.OrderModule.OrderBase> Collection
            {
                get { return _item.__collection__Orders; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.OrderModule.OrderBase>.SetLinkOnItem(BusinessLogic_v3.Modules.OrderModule.OrderBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.Member = value;
            }
        }
        
        private __OrdersCollectionInfo _Orders = null;
        public __OrdersCollectionInfo Orders
        {
            get
            {
                if (_Orders == null)
                    _Orders = new __OrdersCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _Orders;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.OrderModule.IOrderBase> IMemberBaseAutoGen.Orders
        {
            get {  return this.Orders; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.OrderModule.OrderBase> __collection__Orders
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'OrderBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ShoppingCartsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.MemberModule.MemberBase, 
        	BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase,
        	BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __ShoppingCartsCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase> Collection
            {
                get { return _item.__collection__ShoppingCarts; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase item, BusinessLogic_v3.Modules.MemberModule.MemberBase value)
            {
                item.Member = value;
            }
        }
        
        private __ShoppingCartsCollectionInfo _ShoppingCarts = null;
        public __ShoppingCartsCollectionInfo ShoppingCarts
        {
            get
            {
                if (_ShoppingCarts == null)
                    _ShoppingCarts = new __ShoppingCartsCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _ShoppingCarts;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBase> IMemberBaseAutoGen.ShoppingCarts
        {
            get {  return this.ShoppingCarts; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase> __collection__ShoppingCarts
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ShoppingCartBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionManyToManyLeftSide  
        
		public class __SubscribedCategoriesCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.MemberModule.MemberBase, 
			BusinessLogic_v3.Modules.CategoryModule.CategoryBase,
			BusinessLogic_v3.Modules.CategoryModule.ICategoryBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.CategoryModule.CategoryBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __SubscribedCategoriesCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.CategoryModule.CategoryBase> Collection
            {
                get { return _item.__collection__SubscribedCategories; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase,BusinessLogic_v3.Modules.CategoryModule.CategoryBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.MemberModule.MemberBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item)
            {
                return item.SubscribedMembers;
                
            }


            #endregion
        }

        private __SubscribedCategoriesCollectionInfo _SubscribedCategories = null;
        public __SubscribedCategoriesCollectionInfo SubscribedCategories
        {
            get
            {
                if (_SubscribedCategories == null)
                    _SubscribedCategories = new __SubscribedCategoriesCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _SubscribedCategories;
            }
        }
            
        ICollectionManager<BusinessLogic_v3.Modules.CategoryModule.ICategoryBase> IMemberBaseAutoGen.SubscribedCategories
        {
            get {  return this.SubscribedCategories; }
        }
            
        
		protected virtual IEnumerable<BusinessLogic_v3.Modules.CategoryModule.CategoryBase> __collection__SubscribedCategories
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'CategoryBase' is not marked as UsedInProject"); }
        }





        
        //BaseClass_CollectionManyToManyLeftSide  
        
		public class __EventsCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.MemberModule.MemberBase, 
			BusinessLogic_v3.Modules.EventModule.EventBase,
			BusinessLogic_v3.Modules.EventModule.IEventBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.EventModule.EventBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __EventsCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.EventModule.EventBase> Collection
            {
                get { return _item.__collection__Events; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase,BusinessLogic_v3.Modules.EventModule.EventBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.MemberModule.MemberBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.EventModule.EventBase item)
            {
                return item.Members;
                
            }


            #endregion
        }

        private __EventsCollectionInfo _Events = null;
        public __EventsCollectionInfo Events
        {
            get
            {
                if (_Events == null)
                    _Events = new __EventsCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _Events;
            }
        }
            
        ICollectionManager<BusinessLogic_v3.Modules.EventModule.IEventBase> IMemberBaseAutoGen.Events
        {
            get {  return this.Events; }
        }
            
        
		protected virtual IEnumerable<BusinessLogic_v3.Modules.EventModule.EventBase> __collection__Events
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventBase' is not marked as UsedInProject"); }
        }





        
        //BaseClass_CollectionManyToManyLeftSide  
        
		public class __ArticlesReadCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.MemberModule.MemberBase, 
			BusinessLogic_v3.Modules.ArticleModule.ArticleBase,
			BusinessLogic_v3.Modules.ArticleModule.IArticleBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase, BusinessLogic_v3.Modules.ArticleModule.ArticleBase>
        {
            private BusinessLogic_v3.Modules.MemberModule.MemberBase _item = null;
            public __ArticlesReadCollectionInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.ArticleModule.ArticleBase> Collection
            {
                get { return _item.__collection__ArticlesRead; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.MemberModule.MemberBase,BusinessLogic_v3.Modules.ArticleModule.ArticleBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.MemberModule.MemberBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item)
            {
                return item.ReadBy;
                
            }


            #endregion
        }

        private __ArticlesReadCollectionInfo _ArticlesRead = null;
        public __ArticlesReadCollectionInfo ArticlesRead
        {
            get
            {
                if (_ArticlesRead == null)
                    _ArticlesRead = new __ArticlesReadCollectionInfo((BusinessLogic_v3.Modules.MemberModule.MemberBase)this);
                return _ArticlesRead;
            }
        }
            
        ICollectionManager<BusinessLogic_v3.Modules.ArticleModule.IArticleBase> IMemberBaseAutoGen.ArticlesRead
        {
            get {  return this.ArticlesRead; }
        }
            
        
		protected virtual IEnumerable<BusinessLogic_v3.Modules.ArticleModule.ArticleBase> __collection__ArticlesRead
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ArticleBase' is not marked as UsedInProject"); }
        }





        

    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
