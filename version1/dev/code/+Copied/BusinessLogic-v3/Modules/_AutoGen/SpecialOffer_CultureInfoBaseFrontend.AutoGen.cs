using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.SpecialOffer_CultureInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.ISpecialOffer_CultureInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.ISpecialOffer_CultureInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class SpecialOffer_CultureInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<SpecialOffer_CultureInfoBaseFrontend, BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.ISpecialOffer_CultureInfoBase>

    {
		
        
        protected SpecialOffer_CultureInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
