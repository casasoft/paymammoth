using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.MemberResponsibleLimitsModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "MemberResponsibleLimits")]
    public abstract class MemberResponsibleLimitsBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IMemberResponsibleLimitsBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static MemberResponsibleLimitsBaseFactory Factory
        {
            get
            {
                return MemberResponsibleLimitsBaseFactory.Instance; 
            }
        }    
        /*
        public static MemberResponsibleLimitsBase CreateNewItem()
        {
            return (MemberResponsibleLimitsBase)Factory.CreateNewItem();
        }

		public static MemberResponsibleLimitsBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MemberResponsibleLimitsBase, MemberResponsibleLimitsBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MemberResponsibleLimitsBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MemberResponsibleLimitsBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MemberResponsibleLimitsBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MemberResponsibleLimitsBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MemberResponsibleLimitsBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? MemberID
		{
		 	get 
		 	{
		 		return (this.Member != null ? (long?)this.Member.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _member;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase Member 
        {
        	get
        	{
        		return _member;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_member,value);
        		_member = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IMemberResponsibleLimitsBaseAutoGen.Member 
        {
            get
            {
            	return this.Member;
            }
            set
            {
            	this.Member = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private DateTime _startdate;
        
        public virtual DateTime StartDate 
        {
        	get
        	{
        		return _startdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_startdate,value);
        		_startdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _value;
        
        public virtual double Value 
        {
        	get
        	{
        		return _value;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_value,value);
        		_value = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.MEMBER_RESPONSIBLE_LIMITS_FREQUENCY _frequencytype;
        
        public virtual BusinessLogic_v3.Enums.MEMBER_RESPONSIBLE_LIMITS_FREQUENCY FrequencyType 
        {
        	get
        	{
        		return _frequencytype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_frequencytype,value);
        		_frequencytype = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
