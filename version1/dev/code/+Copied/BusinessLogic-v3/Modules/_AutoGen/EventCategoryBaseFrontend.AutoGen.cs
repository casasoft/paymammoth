using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.EventCategoryModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.EventCategoryModule.EventCategoryBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.EventCategoryModule.IEventCategoryBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.EventCategoryModule.EventCategoryBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.EventCategoryModule.IEventCategoryBase item)
        {
        	return BusinessLogic_v3.Frontend.EventCategoryModule.EventCategoryBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventCategoryBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<EventCategoryBaseFrontend, BusinessLogic_v3.Modules.EventCategoryModule.IEventCategoryBase>

    {
		
        
        protected EventCategoryBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
