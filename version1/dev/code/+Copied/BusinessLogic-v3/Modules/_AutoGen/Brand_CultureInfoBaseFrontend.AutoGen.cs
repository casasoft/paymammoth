using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.Brand_CultureInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.Brand_CultureInfoModule.Brand_CultureInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.Brand_CultureInfoModule.Brand_CultureInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.Brand_CultureInfoModule.Brand_CultureInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Brand_CultureInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<Brand_CultureInfoBaseFrontend, BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBase>

    {
		
        
        protected Brand_CultureInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
