using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ProductCategoryFeatureValue")]
    public abstract class ProductCategoryFeatureValueBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductCategoryFeatureValueBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ProductCategoryFeatureValueBaseFactory Factory
        {
            get
            {
                return ProductCategoryFeatureValueBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductCategoryFeatureValueBase CreateNewItem()
        {
            return (ProductCategoryFeatureValueBase)Factory.CreateNewItem();
        }

		public static ProductCategoryFeatureValueBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductCategoryFeatureValueBase, ProductCategoryFeatureValueBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductCategoryFeatureValueBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductCategoryFeatureValueBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductCategoryFeatureValueBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductCategoryFeatureValueBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductCategoryFeatureValueBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? ProductID
		{
		 	get 
		 	{
		 		return (this.Product != null ? (long?)this.Product.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _product;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase Product 
        {
        	get
        	{
        		return _product;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_product,value);
        		_product = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.IProductCategoryFeatureValueBaseAutoGen.Product 
        {
            get
            {
            	return this.Product;
            }
            set
            {
            	this.Product = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
/*
		public virtual long? CategoryFeatureID
		{
		 	get 
		 	{
		 		return (this.CategoryFeature != null ? (long?)this.CategoryFeature.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase _categoryfeature;
        public virtual BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase CategoryFeature 
        {
        	get
        	{
        		return _categoryfeature;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_categoryfeature,value);
        		_categoryfeature = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBase BusinessLogic_v3.Modules._AutoGen.IProductCategoryFeatureValueBaseAutoGen.CategoryFeature 
        {
            get
            {
            	return this.CategoryFeature;
            }
            set
            {
            	this.CategoryFeature = (BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
