using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ProductVariation_CultureInfo")]
    public abstract class ProductVariation_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductVariation_CultureInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
      , IMultilingualContentInfo
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ProductVariation_CultureInfoBaseFactory Factory
        {
            get
            {
                return ProductVariation_CultureInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductVariation_CultureInfoBase CreateNewItem()
        {
            return (ProductVariation_CultureInfoBase)Factory.CreateNewItem();
        }

		public static ProductVariation_CultureInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductVariation_CultureInfoBase, ProductVariation_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductVariation_CultureInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductVariation_CultureInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductVariation_CultureInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductVariation_CultureInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductVariation_CultureInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? CultureInfoID
		{
		 	get 
		 	{
		 		return (this.CultureInfo != null ? (long?)this.CultureInfo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase _cultureinfo;
        public virtual BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase CultureInfo 
        {
        	get
        	{
        		return _cultureinfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cultureinfo,value);
        		_cultureinfo = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase BusinessLogic_v3.Modules._AutoGen.IProductVariation_CultureInfoBaseAutoGen.CultureInfo 
        {
            get
            {
            	return this.CultureInfo;
            }
            set
            {
            	this.CultureInfo = (BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase) value;
            }
        }
            
/*
		public virtual long? ProductVariationID
		{
		 	get 
		 	{
		 		return (this.ProductVariation != null ? (long?)this.ProductVariation.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase _productvariation;
        public virtual BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase ProductVariation 
        {
        	get
        	{
        		return _productvariation;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_productvariation,value);
        		_productvariation = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase BusinessLogic_v3.Modules._AutoGen.IProductVariation_CultureInfoBaseAutoGen.ProductVariation 
        {
            get
            {
            	return this.ProductVariation;
            }
            set
            {
            	this.ProductVariation = (BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _size;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Size 
        {
        	get
        	{
        		return _size;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_size,value);
        		_size = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)




#region IMultilingualContentInfo Members

        long IMultilingualContentInfo.CultureInfoID
        {
            get
            {                          
            	if (this.CultureInfo != null)
                	return this.CultureInfo.ID;
               	else
               		return 0;
            }
        }
  		BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject IMultilingualContentInfo.GetLinkedItem()
        {
            return this.ProductVariation;
        }
#endregion



#endregion       

		

    }
}
