using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.FileItemModule;
using BusinessLogic_v3.Cms.FileItemModule;
using BusinessLogic_v3.Frontend.FileItemModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class FileItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.FileItemModule.FileItemBase>
    {
		public FileItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.FileItemModule.FileItemBase dbItem)
            : base(BusinessLogic_v3.Cms.FileItemModule.FileItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new FileItemBaseFrontend FrontendItem
        {
            get { return (FileItemBaseFrontend)base.FrontendItem; }

        }
        public new FileItemBase DbItem
        {
            get
            {
                return (FileItemBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo UploadedOn { get; protected set; }

        public CmsPropertyInfo UploadedBy { get; protected set; }

        public CmsPropertyInfo UploadFilename { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
