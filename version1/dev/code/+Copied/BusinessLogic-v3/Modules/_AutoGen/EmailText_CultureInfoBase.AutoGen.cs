using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.EmailText_CultureInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "EmailText_CultureInfo")]
    public abstract class EmailText_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IEmailText_CultureInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
      , IMultilingualContentInfo
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static EmailText_CultureInfoBaseFactory Factory
        {
            get
            {
                return EmailText_CultureInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static EmailText_CultureInfoBase CreateNewItem()
        {
            return (EmailText_CultureInfoBase)Factory.CreateNewItem();
        }

		public static EmailText_CultureInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<EmailText_CultureInfoBase, EmailText_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static EmailText_CultureInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static EmailText_CultureInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<EmailText_CultureInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<EmailText_CultureInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<EmailText_CultureInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? CultureInfoID
		{
		 	get 
		 	{
		 		return (this.CultureInfo != null ? (long?)this.CultureInfo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase _cultureinfo;
        public virtual BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase CultureInfo 
        {
        	get
        	{
        		return _cultureinfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cultureinfo,value);
        		_cultureinfo = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase BusinessLogic_v3.Modules._AutoGen.IEmailText_CultureInfoBaseAutoGen.CultureInfo 
        {
            get
            {
            	return this.CultureInfo;
            }
            set
            {
            	this.CultureInfo = (BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase) value;
            }
        }
            
/*
		public virtual long? EmailTextID
		{
		 	get 
		 	{
		 		return (this.EmailText != null ? (long?)this.EmailText.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase _emailtext;
        public virtual BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase EmailText 
        {
        	get
        	{
        		return _emailtext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_emailtext,value);
        		_emailtext = value;
        	}
        }
            
        BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase BusinessLogic_v3.Modules._AutoGen.IEmailText_CultureInfoBaseAutoGen.EmailText 
        {
            get
            {
            	return this.EmailText;
            }
            set
            {
            	this.EmailText = (BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _subject;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Subject 
        {
        	get
        	{
        		return _subject;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subject,value);
        		_subject = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _subject_search;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Subject_Search 
        {
        	get
        	{
        		return _subject_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subject_search,value);
        		_subject_search = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _body;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Body 
        {
        	get
        	{
        		return _body;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_body,value);
        		_body = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _body_search;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Body_Search 
        {
        	get
        	{
        		return _body_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_body_search,value);
        		_body_search = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)




#region IMultilingualContentInfo Members

        long IMultilingualContentInfo.CultureInfoID
        {
            get
            {                          
            	if (this.CultureInfo != null)
                	return this.CultureInfo.ID;
               	else
               		return 0;
            }
        }
  		BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject IMultilingualContentInfo.GetLinkedItem()
        {
            return this.EmailText;
        }
#endregion



#endregion       

		

    }
}
