using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ContentTextModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ContentText")]
    public abstract class ContentTextBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IContentTextBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
        

			var cultureInfo = __getCurrentCultureInfo(); //since this is a new item, it is imperative that the culture is preloaded
            
            
            base.initPropertiesForNewItem();
        }
    	
  
    	public BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase) __getCultureInfoByLanguage(lang);
        }

     	public static ContentTextBaseFactory Factory
        {
            get
            {
                return ContentTextBaseFactory.Instance; 
            }
        }    
        /*
        public static ContentTextBase CreateNewItem()
        {
            return (ContentTextBase)Factory.CreateNewItem();
        }

		public static ContentTextBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ContentTextBase, ContentTextBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ContentTextBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ContentTextBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ContentTextBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ContentTextBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ContentTextBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _name;
        
        public virtual string Name 
        {
        	get
        	{
        		return _name;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_name,value);
        		_name = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _name_search;
        
        public virtual string Name_Search 
        {
        	get
        	{
        		return _name_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_name_search,value);
        		_name_search = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
/*
		public virtual long? ParentID
		{
		 	get 
		 	{
		 		return (this.Parent != null ? (long?)this.Parent.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase _parent;
        public virtual BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase Parent 
        {
        	get
        	{
        		return _parent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_parent,value);
        		_parent = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase BusinessLogic_v3.Modules._AutoGen.IContentTextBaseAutoGen.Parent 
        {
            get
            {
            	return this.Parent;
            }
            set
            {
            	this.Parent = (BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.CMS_ACCESS_TYPE _allowaddsubitems_accessrequired;
        
        public virtual CS.General_v3.Enums.CMS_ACCESS_TYPE AllowAddSubItems_AccessRequired 
        {
        	get
        	{
        		return _allowaddsubitems_accessrequired;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_allowaddsubitems_accessrequired,value);
        		_allowaddsubitems_accessrequired = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.CMS_ACCESS_TYPE _visibleincms_accessrequired;
        
        public virtual CS.General_v3.Enums.CMS_ACCESS_TYPE VisibleInCMS_AccessRequired 
        {
        	get
        	{
        		return _visibleincms_accessrequired;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_visibleincms_accessrequired,value);
        		_visibleincms_accessrequired = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Content
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Content;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Content_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Content,value);
                cultureInfo.Content = value;
            }
        }
        
        private string _content;
        public virtual string __Content_cultureBase
        {
        	get
        	{
        		return _content;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_content,value);
        		
        		_content = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE _contenttype;
        
        public virtual BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE ContentType 
        {
        	get
        	{
        		return _contenttype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_contenttype,value);
        		_contenttype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _usedinproject;
        
        public virtual bool UsedInProject 
        {
        	get
        	{
        		return _usedinproject;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_usedinproject,value);
        		_usedinproject = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string ImageAlternateText
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.ImageAlternateText;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__ImageAlternateText_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.ImageAlternateText,value);
                cultureInfo.ImageAlternateText = value;
            }
        }
        
        private string _imagealternatetext;
        public virtual string __ImageAlternateText_cultureBase
        {
        	get
        	{
        		return _imagealternatetext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagealternatetext,value);
        		
        		_imagealternatetext = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private string _customcontenttags;
        
        public virtual string CustomContentTags 
        {
        	get
        	{
        		return _customcontenttags;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customcontenttags,value);
        		_customcontenttags = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _processcontentusingnvelocity;
        
        public virtual bool ProcessContentUsingNVelocity 
        {
        	get
        	{
        		return _processcontentusingnvelocity;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_processcontentusingnvelocity,value);
        		_processcontentusingnvelocity = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool __computed_ishtml;
        
        public virtual bool _Computed_IsHtml 
        {
        	get
        	{
        		return __computed_ishtml;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__computed_ishtml,value);
        		__computed_ishtml = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ChildContentTextsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase, 
        	BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase,
        	BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase, BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>
        {
            private BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase _item = null;
            public __ChildContentTextsCollectionInfo(BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase> Collection
            {
                get { return _item.__collection__ChildContentTexts; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase, BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase item, BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase value)
            {
                item.Parent = value;
            }
        }
        
        private __ChildContentTextsCollectionInfo _ChildContentTexts = null;
        public __ChildContentTextsCollectionInfo ChildContentTexts
        {
            get
            {
                if (_ChildContentTexts == null)
                    _ChildContentTexts = new __ChildContentTextsCollectionInfo((BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase)this);
                return _ChildContentTexts;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase> IContentTextBaseAutoGen.ChildContentTexts
        {
            get {  return this.ChildContentTexts; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase> __collection__ChildContentTexts
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ContentTextBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ContentText_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase, 
        	BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase,
        	BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase, BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase>
        {
            private BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase _item = null;
            public __ContentText_CultureInfosCollectionInfo(BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase> Collection
            {
                get { return _item.__collection__ContentText_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase, BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase item, BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase value)
            {
                item.ContentText = value;
            }
        }
        
        private __ContentText_CultureInfosCollectionInfo _ContentText_CultureInfos = null;
        public __ContentText_CultureInfosCollectionInfo ContentText_CultureInfos
        {
            get
            {
                if (_ContentText_CultureInfos == null)
                    _ContentText_CultureInfos = new __ContentText_CultureInfosCollectionInfo((BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase)this);
                return _ContentText_CultureInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBase> IContentTextBaseAutoGen.ContentText_CultureInfos
        {
            get {  return this.ContentText_CultureInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase> __collection__ContentText_CultureInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ContentText_CultureInfoBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
