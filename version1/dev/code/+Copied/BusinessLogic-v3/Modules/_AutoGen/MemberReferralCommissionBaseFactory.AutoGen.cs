using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberReferralCommissionModule;

//DbObjectBuilder v8

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class MemberReferralCommissionBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<MemberReferralCommissionBase, IMemberReferralCommissionBase>, IMemberReferralCommissionBaseFactoryAutoGen
    
    {
    
        public MemberReferralCommissionBaseFactoryAutoGen()
        {
        	
            
        }
        
		static MemberReferralCommissionBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static MemberReferralCommissionBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<MemberReferralCommissionBaseFactory>(null);
            }
        }
        
		public IQueryOver<MemberReferralCommissionBase, MemberReferralCommissionBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<MemberReferralCommissionBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
