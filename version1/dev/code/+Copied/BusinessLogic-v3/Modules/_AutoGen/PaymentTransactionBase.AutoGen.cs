using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using CS.General_v3.Classes.DbObjects.Collections;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.PaymentTransactionModule;                

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public abstract class PaymentTransactionBaseAutoGen 
    : BusinessLogic_v3.Classes.DB.BaseDbObject, IPaymentTransactionBaseAutoGen
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	
    	
    	
        
     	public static PaymentTransactionBaseFactory Factory
        {
            get
            {
                return PaymentTransactionBaseFactory.Instance; 
            }
        }    
        /*
        public static PaymentTransactionBase CreateNewItem()
        {
            return (PaymentTransactionBase)Factory.CreateNewItem();
        }

		public static PaymentTransactionBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<PaymentTransactionBase, PaymentTransactionBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static PaymentTransactionBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static PaymentTransactionBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<PaymentTransactionBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<PaymentTransactionBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<PaymentTransactionBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private DateTime _date;
        public virtual DateTime Date 
        {
        	get
        	{
        		return _date;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_date,value);
        		_date = value;
        		
        	}
        }
        
/*
		public virtual long? OrderID
		{
		 	get 
		 	{
		 		return (this.Order != null ? (long?)this.Order.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.OrderModule.OrderBase _order;
        public virtual BusinessLogic_v3.Modules.OrderModule.OrderBase Order 
        {
        	get
        	{
        		return _order;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_order,value);
        		_order = value;
        	}
        }
            
        BusinessLogic_v3.Modules.OrderModule.IOrderBase BusinessLogic_v3.Modules._AutoGen.IPaymentTransactionBaseAutoGen.Order 
        {
            get
            {
            	return this.Order;
            }
            set
            {
            	this.Order = (BusinessLogic_v3.Modules.OrderModule.OrderBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _details;
        public virtual string Details 
        {
        	get
        	{
        		return _details;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_details,value);
        		_details = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _amount;
        public virtual double Amount 
        {
        	get
        	{
        		return _amount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_amount,value);
        		_amount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _success;
        public virtual bool Success 
        {
        	get
        	{
        		return _success;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_success,value);
        		_success = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _authcode;
        public virtual string AuthCode 
        {
        	get
        	{
        		return _authcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_authcode,value);
        		_authcode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _remarks;
        public virtual string Remarks 
        {
        	get
        	{
        		return _remarks;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_remarks,value);
        		_remarks = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _itemid;
        public virtual int ItemID 
        {
        	get
        	{
        		return _itemid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemid,value);
        		_itemid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _itemtype;
        public virtual string ItemType 
        {
        	get
        	{
        		return _itemtype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_itemtype,value);
        		_itemtype = value;
        		
        	}
        }
        
/*
		public virtual long? LinkedItemID
		{
		 	get 
		 	{
		 		return (this.LinkedItem != null ? (long?)this.LinkedItem.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase _linkeditem;
        public virtual BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase LinkedItem 
        {
        	get
        	{
        		return _linkeditem;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkeditem,value);
        		_linkeditem = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase BusinessLogic_v3.Modules._AutoGen.IPaymentTransactionBaseAutoGen.LinkedItem 
        {
            get
            {
            	return this.LinkedItem;
            }
            set
            {
            	this.LinkedItem = (BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase) value;
            }
        }
            
/*
		public virtual long? MemberID
		{
		 	get 
		 	{
		 		return (this.Member != null ? (long?)this.Member.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _member;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase Member 
        {
        	get
        	{
        		return _member;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_member,value);
        		_member = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IPaymentTransactionBaseAutoGen.Member 
        {
            get
            {
            	return this.Member;
            }
            set
            {
            	this.Member = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _externalreference1;
        public virtual string ExternalReference1 
        {
        	get
        	{
        		return _externalreference1;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_externalreference1,value);
        		_externalreference1 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _externalreference2;
        public virtual string ExternalReference2 
        {
        	get
        	{
        		return _externalreference2;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_externalreference2,value);
        		_externalreference2 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _lastupdated;
        public virtual DateTime LastUpdated 
        {
        	get
        	{
        		return _lastupdated;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_lastupdated,value);
        		_lastupdated = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _errormsg;
        public virtual string ErrorMsg 
        {
        	get
        	{
        		return _errormsg;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_errormsg,value);
        		_errormsg = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _internalreference;
        public virtual string InternalReference 
        {
        	get
        	{
        		return _internalreference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_internalreference,value);
        		_internalreference = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    
#region Multilingual
          


#endregion    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
