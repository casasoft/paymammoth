using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ShippingRatePriceModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ShippingRatePrice")]
    public abstract class ShippingRatePriceBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IShippingRatePriceBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ShippingRatePriceBaseFactory Factory
        {
            get
            {
                return ShippingRatePriceBaseFactory.Instance; 
            }
        }    
        /*
        public static ShippingRatePriceBase CreateNewItem()
        {
            return (ShippingRatePriceBase)Factory.CreateNewItem();
        }

		public static ShippingRatePriceBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ShippingRatePriceBase, ShippingRatePriceBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ShippingRatePriceBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ShippingRatePriceBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ShippingRatePriceBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ShippingRatePriceBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ShippingRatePriceBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? ShippingRatesGroupID
		{
		 	get 
		 	{
		 		return (this.ShippingRatesGroup != null ? (long?)this.ShippingRatesGroup.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase _shippingratesgroup;
        public virtual BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase ShippingRatesGroup 
        {
        	get
        	{
        		return _shippingratesgroup;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shippingratesgroup,value);
        		_shippingratesgroup = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ShippingRatesGroupModule.IShippingRatesGroupBase BusinessLogic_v3.Modules._AutoGen.IShippingRatePriceBaseAutoGen.ShippingRatesGroup 
        {
            get
            {
            	return this.ShippingRatesGroup;
            }
            set
            {
            	this.ShippingRatesGroup = (BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private double _fromweight;
        
        public virtual double FromWeight 
        {
        	get
        	{
        		return _fromweight;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_fromweight,value);
        		_fromweight = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _toweight;
        
        public virtual double ToWeight 
        {
        	get
        	{
        		return _toweight;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_toweight,value);
        		_toweight = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _price;
        
        public virtual double Price 
        {
        	get
        	{
        		return _price;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_price,value);
        		_price = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.ShippingPricingType _pricingtype;
        
        public virtual BusinessLogic_v3.Enums.ShippingPricingType PricingType 
        {
        	get
        	{
        		return _pricingtype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_pricingtype,value);
        		_pricingtype = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
