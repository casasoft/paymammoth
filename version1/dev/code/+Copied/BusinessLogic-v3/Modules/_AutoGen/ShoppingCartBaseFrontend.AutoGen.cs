using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ShoppingCartModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ShoppingCartModule.ShoppingCartBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ShoppingCartModule.ShoppingCartBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBase item)
        {
        	return BusinessLogic_v3.Frontend.ShoppingCartModule.ShoppingCartBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ShoppingCartBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ShoppingCartBaseFrontend, BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBase>

    {
		
        
        protected ShoppingCartBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
