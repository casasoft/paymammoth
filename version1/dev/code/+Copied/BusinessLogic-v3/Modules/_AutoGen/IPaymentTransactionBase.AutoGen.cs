using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.PaymentTransactionModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IPaymentTransactionBaseAutoGen : BusinessLogic_v3.Classes.DB.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        DateTime Date { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.OrderModule.IOrderBase Order {get; set; }

		//Iproperty_normal
        string Details { get; set; }
		//Iproperty_normal
        double Amount { get; set; }
		//Iproperty_normal
        bool Success { get; set; }
		//Iproperty_normal
        string AuthCode { get; set; }
		//Iproperty_normal
        string Remarks { get; set; }
		//Iproperty_normal
        int ItemID { get; set; }
		//Iproperty_normal
        string ItemType { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase LinkedItem {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase Member {get; set; }

		//Iproperty_normal
        string ExternalReference1 { get; set; }
		//Iproperty_normal
        string ExternalReference2 { get; set; }
		//Iproperty_normal
        DateTime LastUpdated { get; set; }
		//Iproperty_normal
        string ErrorMsg { get; set; }
		//Iproperty_normal
        string InternalReference { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
