using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CmsAuditLogModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface ICmsAuditLogBaseAutoGen : BusinessLogic_v3.Classes.DB.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase CmsUser {get; set; }

		//Iproperty_normal
        DateTime DateTime { get; set; }
		//Iproperty_normal
        string Message { get; set; }
		//Iproperty_normal
        string FurtherInformation { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.CMS_AUDIT_MESSAGE_TYPE MsgType { get; set; }
		//Iproperty_normal
        string IPAddress { get; set; }
		//Iproperty_normal
        string ObjectName { get; set; }
		//Iproperty_normal
        string Remarks { get; set; }
		//Iproperty_normal
        long ItemID { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
