using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.Product_CultureInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.Product_CultureInfoModule.Product_CultureInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.Product_CultureInfoModule.Product_CultureInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.Product_CultureInfoModule.Product_CultureInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Product_CultureInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<Product_CultureInfoBaseFrontend, BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBase>

    {
		
        
        protected Product_CultureInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
