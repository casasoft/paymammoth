using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Cms.CultureDetailsModule;
using BusinessLogic_v3.Frontend.CultureDetailsModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CultureDetailsBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>
    {
		public CultureDetailsBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase dbItem)
            : base(BusinessLogic_v3.Cms.CultureDetailsModule.CultureDetailsBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CultureDetailsBaseFrontend FrontendItem
        {
            get { return (CultureDetailsBaseFrontend)base.FrontendItem; }

        }
        public new CultureDetailsBase DbItem
        {
            get
            {
                return (CultureDetailsBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo LanguageISOCode { get; protected set; }

        public CmsPropertyInfo DefaultCurrency { get; protected set; }

        public CmsPropertyInfo SpecificCountryCode { get; protected set; }

        public CmsPropertyInfo ScriptSuffix { get; protected set; }

        public CmsPropertyInfo IsDefaultCulture { get; protected set; }

        public CmsPropertyInfo BaseUrlRegex { get; protected set; }

        public CmsPropertyInfo BaseRedirectionUrl { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
