using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ShippingRatesGroupModule;
using BusinessLogic_v3.Cms.ShippingRatesGroupModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ShippingRatesGroupBaseCmsFactory_AutoGen : CmsFactoryBase<ShippingRatesGroupBaseCmsInfo, ShippingRatesGroupBase>
    {
       
       public new static ShippingRatesGroupBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ShippingRatesGroupBaseCmsFactory)CmsFactoryBase<ShippingRatesGroupBaseCmsInfo, ShippingRatesGroupBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ShippingRatesGroupBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ShippingRatesGroup.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ShippingRatesGroup";

            this.QueryStringParamID = "ShippingRatesGroupId";

            cmsInfo.TitlePlural = "Shipping Rates Groups";

            cmsInfo.TitleSingular =  "Shipping Rates Group";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ShippingRatesGroupBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ShippingRatesGroup/";
			UsedInProject = ShippingRatesGroupBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
