using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ArticleMediaItemModule;
using BusinessLogic_v3.Cms.ArticleMediaItemModule;
using BusinessLogic_v3.Frontend.ArticleMediaItemModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ArticleMediaItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase>
    {
		public ArticleMediaItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase dbItem)
            : base(BusinessLogic_v3.Cms.ArticleMediaItemModule.ArticleMediaItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ArticleMediaItemBaseFrontend FrontendItem
        {
            get { return (ArticleMediaItemBaseFrontend)base.FrontendItem; }

        }
        public new ArticleMediaItemBase DbItem
        {
            get
            {
                return (ArticleMediaItemBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Caption { get; protected set; }

        public CmsPropertyInfo ContentPage { get; protected set; }

        public CmsPropertyInfo VideoDuration { get; protected set; }

        public CmsPropertyInfo Filename { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
