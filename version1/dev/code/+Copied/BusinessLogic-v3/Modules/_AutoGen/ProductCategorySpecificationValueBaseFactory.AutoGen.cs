using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;

//DbObjectBuilder v8

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class ProductCategorySpecificationValueBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<ProductCategorySpecificationValueBase, IProductCategorySpecificationValueBase>, IProductCategorySpecificationValueBaseFactoryAutoGen
    
    {
    
        public ProductCategorySpecificationValueBaseFactoryAutoGen()
        {
        	
            
        }
        
		static ProductCategorySpecificationValueBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static ProductCategorySpecificationValueBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<ProductCategorySpecificationValueBaseFactory>(null);
            }
        }
        
		public IQueryOver<ProductCategorySpecificationValueBase, ProductCategorySpecificationValueBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ProductCategorySpecificationValueBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
