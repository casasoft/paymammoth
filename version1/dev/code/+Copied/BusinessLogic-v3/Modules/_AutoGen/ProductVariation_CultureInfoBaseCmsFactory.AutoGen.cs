using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule;
using BusinessLogic_v3.Cms.ProductVariation_CultureInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ProductVariation_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<ProductVariation_CultureInfoBaseCmsInfo, ProductVariation_CultureInfoBase>
    {
       
       public new static ProductVariation_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductVariation_CultureInfoBaseCmsFactory)CmsFactoryBase<ProductVariation_CultureInfoBaseCmsInfo, ProductVariation_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ProductVariation_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductVariation_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductVariation_CultureInfo";

            this.QueryStringParamID = "ProductVariation_CultureInfoId";

            cmsInfo.TitlePlural = "Product Variation _ Culture Infos";

            cmsInfo.TitleSingular =  "Product Variation _ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductVariation_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ProductVariation_CultureInfo/";
			UsedInProject = ProductVariation_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
