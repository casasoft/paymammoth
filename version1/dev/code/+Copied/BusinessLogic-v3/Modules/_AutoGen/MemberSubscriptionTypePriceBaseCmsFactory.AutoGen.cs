using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;
using BusinessLogic_v3.Cms.MemberSubscriptionTypePriceModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class MemberSubscriptionTypePriceBaseCmsFactory_AutoGen : CmsFactoryBase<MemberSubscriptionTypePriceBaseCmsInfo, MemberSubscriptionTypePriceBase>
    {
       
       public new static MemberSubscriptionTypePriceBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberSubscriptionTypePriceBaseCmsFactory)CmsFactoryBase<MemberSubscriptionTypePriceBaseCmsInfo, MemberSubscriptionTypePriceBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = MemberSubscriptionTypePriceBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberSubscriptionTypePrice.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberSubscriptionTypePrice";

            this.QueryStringParamID = "MemberSubscriptionTypePriceId";

            cmsInfo.TitlePlural = "Member Subscription Type Prices";

            cmsInfo.TitleSingular =  "Member Subscription Type Price";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberSubscriptionTypePriceBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "MemberSubscriptionTypePrice/";
			UsedInProject = MemberSubscriptionTypePriceBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
