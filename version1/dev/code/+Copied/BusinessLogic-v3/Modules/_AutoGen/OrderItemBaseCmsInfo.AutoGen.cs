using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.OrderItemModule;
using BusinessLogic_v3.Cms.OrderItemModule;
using BusinessLogic_v3.Frontend.OrderItemModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class OrderItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>
    {
		public OrderItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase dbItem)
            : base(BusinessLogic_v3.Cms.OrderItemModule.OrderItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new OrderItemBaseFrontend FrontendItem
        {
            get { return (OrderItemBaseFrontend)base.FrontendItem; }

        }
        public new OrderItemBase DbItem
        {
            get
            {
                return (OrderItemBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Quantity { get; protected set; }

        public CmsPropertyInfo PriceExcTaxPerUnit { get; protected set; }

        public CmsPropertyInfo TaxAmountPerUnit { get; protected set; }

        public CmsPropertyInfo ItemReference { get; protected set; }

        public CmsPropertyInfo Remarks { get; protected set; }

        public CmsPropertyInfo DiscountPerUnit { get; protected set; }

        public CmsPropertyInfo ItemID { get; protected set; }

        public CmsPropertyInfo ItemType { get; protected set; }

        public CmsPropertyInfo ShipmentWeight { get; protected set; }

        public CmsPropertyInfo ShippingCost { get; protected set; }

        public CmsPropertyInfo HandlingCost { get; protected set; }

        public CmsPropertyInfo Order { get; protected set; }

        public CmsPropertyInfo SpecialOffer { get; protected set; }

        public CmsPropertyInfo SpecialOfferVoucherCode { get; protected set; }

        public CmsPropertyInfo LinkedProductVariation { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
