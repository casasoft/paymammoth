using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.SettingModule;
using BusinessLogic_v3.Cms.SettingModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class SettingBaseCmsFactory_AutoGen : CmsFactoryBase<SettingBaseCmsInfo, SettingBase>
    {
       
       public new static SettingBaseCmsFactory Instance
	    {
	         get
	         {
                 return (SettingBaseCmsFactory)CmsFactoryBase<SettingBaseCmsInfo, SettingBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = SettingBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Setting.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Setting";

            this.QueryStringParamID = "SettingId";

            cmsInfo.TitlePlural = "Settings";

            cmsInfo.TitleSingular =  "Setting";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public SettingBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Setting/";
			UsedInProject = SettingBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
