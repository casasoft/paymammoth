using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductVersionMediaItemModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductVersionMediaItemModule.ProductVersionMediaItemBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductVersionMediaItemModule.IProductVersionMediaItemBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductVersionMediaItemModule.ProductVersionMediaItemBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductVersionMediaItemModule.IProductVersionMediaItemBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductVersionMediaItemModule.ProductVersionMediaItemBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductVersionMediaItemBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductVersionMediaItemBaseFrontend, BusinessLogic_v3.Modules.ProductVersionMediaItemModule.IProductVersionMediaItemBase>

    {
		
        
        protected ProductVersionMediaItemBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
