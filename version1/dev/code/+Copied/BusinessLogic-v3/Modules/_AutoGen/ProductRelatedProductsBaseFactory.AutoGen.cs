using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductRelatedProductsModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class ProductRelatedProductsBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DB.BaseDbFactory<ProductRelatedProductsBase, IProductRelatedProductsBase>, IProductRelatedProductsBaseFactoryAutoGen
    
    {
    
        public ProductRelatedProductsBaseFactoryAutoGen()
        {
        	
            
        }
        
		static ProductRelatedProductsBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static ProductRelatedProductsBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<ProductRelatedProductsBaseFactory>(null);
            }
        }
        
		public IQueryOver<ProductRelatedProductsBase, ProductRelatedProductsBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ProductRelatedProductsBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
