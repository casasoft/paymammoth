using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Category_ParentChildLink")]
    public abstract class Category_ParentChildLinkBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ICategory_ParentChildLinkBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static Category_ParentChildLinkBaseFactory Factory
        {
            get
            {
                return Category_ParentChildLinkBaseFactory.Instance; 
            }
        }    
        /*
        public static Category_ParentChildLinkBase CreateNewItem()
        {
            return (Category_ParentChildLinkBase)Factory.CreateNewItem();
        }

		public static Category_ParentChildLinkBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<Category_ParentChildLinkBase, Category_ParentChildLinkBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static Category_ParentChildLinkBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static Category_ParentChildLinkBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<Category_ParentChildLinkBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<Category_ParentChildLinkBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<Category_ParentChildLinkBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? ParentCategoryID
		{
		 	get 
		 	{
		 		return (this.ParentCategory != null ? (long?)this.ParentCategory.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _parentcategory;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase ParentCategory 
        {
        	get
        	{
        		return _parentcategory;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_parentcategory,value);
        		_parentcategory = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.ICategory_ParentChildLinkBaseAutoGen.ParentCategory 
        {
            get
            {
            	return this.ParentCategory;
            }
            set
            {
            	this.ParentCategory = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
/*
		public virtual long? ChildCategoryID
		{
		 	get 
		 	{
		 		return (this.ChildCategory != null ? (long?)this.ChildCategory.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _childcategory;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase ChildCategory 
        {
        	get
        	{
        		return _childcategory;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_childcategory,value);
        		_childcategory = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.ICategory_ParentChildLinkBaseAutoGen.ChildCategory 
        {
            get
            {
            	return this.ChildCategory;
            }
            set
            {
            	this.ChildCategory = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
