using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CmsAuditLogModule;
using BusinessLogic_v3.Cms.CmsAuditLogModule;
using BusinessLogic_v3.Frontend.CmsAuditLogModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CmsAuditLogBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>
    {
		public CmsAuditLogBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase dbItem)
            : base(BusinessLogic_v3.Cms.CmsAuditLogModule.CmsAuditLogBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CmsAuditLogBaseFrontend FrontendItem
        {
            get { return (CmsAuditLogBaseFrontend)base.FrontendItem; }

        }
        public new CmsAuditLogBase DbItem
        {
            get
            {
                return (CmsAuditLogBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CmsUser { get; protected set; }

        public CmsPropertyInfo DateTime { get; protected set; }

        public CmsPropertyInfo Message { get; protected set; }

        public CmsPropertyInfo FurtherInformation { get; protected set; }

        public CmsPropertyInfo MsgType { get; protected set; }

        public CmsPropertyInfo IPAddress { get; protected set; }

        public CmsPropertyInfo ObjectName { get; protected set; }

        public CmsPropertyInfo Remarks { get; protected set; }

        public CmsPropertyInfo ItemID { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
