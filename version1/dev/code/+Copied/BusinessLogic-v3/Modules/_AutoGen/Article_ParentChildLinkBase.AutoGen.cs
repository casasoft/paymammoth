using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Article_ParentChildLink")]
    public abstract class Article_ParentChildLinkBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IArticle_ParentChildLinkBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static Article_ParentChildLinkBaseFactory Factory
        {
            get
            {
                return Article_ParentChildLinkBaseFactory.Instance; 
            }
        }    
        /*
        public static Article_ParentChildLinkBase CreateNewItem()
        {
            return (Article_ParentChildLinkBase)Factory.CreateNewItem();
        }

		public static Article_ParentChildLinkBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<Article_ParentChildLinkBase, Article_ParentChildLinkBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static Article_ParentChildLinkBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static Article_ParentChildLinkBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<Article_ParentChildLinkBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<Article_ParentChildLinkBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<Article_ParentChildLinkBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? ParentID
		{
		 	get 
		 	{
		 		return (this.Parent != null ? (long?)this.Parent.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ArticleModule.ArticleBase _parent;
        public virtual BusinessLogic_v3.Modules.ArticleModule.ArticleBase Parent 
        {
        	get
        	{
        		return _parent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_parent,value);
        		_parent = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase BusinessLogic_v3.Modules._AutoGen.IArticle_ParentChildLinkBaseAutoGen.Parent 
        {
            get
            {
            	return this.Parent;
            }
            set
            {
            	this.Parent = (BusinessLogic_v3.Modules.ArticleModule.ArticleBase) value;
            }
        }
            
/*
		public virtual long? ChildID
		{
		 	get 
		 	{
		 		return (this.Child != null ? (long?)this.Child.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ArticleModule.ArticleBase _child;
        public virtual BusinessLogic_v3.Modules.ArticleModule.ArticleBase Child 
        {
        	get
        	{
        		return _child;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_child,value);
        		_child = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase BusinessLogic_v3.Modules._AutoGen.IArticle_ParentChildLinkBaseAutoGen.Child 
        {
            get
            {
            	return this.Child;
            }
            set
            {
            	this.Child = (BusinessLogic_v3.Modules.ArticleModule.ArticleBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
