using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.CategoryImageModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.CategoryImageModule.CategoryImageBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.CategoryImageModule.ICategoryImageBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.CategoryImageModule.CategoryImageBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.CategoryImageModule.ICategoryImageBase item)
        {
        	return BusinessLogic_v3.Frontend.CategoryImageModule.CategoryImageBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CategoryImageBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<CategoryImageBaseFrontend, BusinessLogic_v3.Modules.CategoryImageModule.ICategoryImageBase>

    {
		
        
        protected CategoryImageBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
