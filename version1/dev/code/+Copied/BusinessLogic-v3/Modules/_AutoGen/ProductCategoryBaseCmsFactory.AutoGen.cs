using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using BusinessLogic_v3.Cms.ProductCategoryModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ProductCategoryBaseCmsFactory_AutoGen : CmsFactoryBase<ProductCategoryBaseCmsInfo, ProductCategoryBase>
    {
       
       public new static ProductCategoryBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ProductCategoryBaseCmsFactory)CmsFactoryBase<ProductCategoryBaseCmsInfo, ProductCategoryBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ProductCategoryBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ProductCategory.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ProductCategory";

            this.QueryStringParamID = "ProductCategoryId";

            cmsInfo.TitlePlural = "Product Categories";

            cmsInfo.TitleSingular =  "Product Category";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ProductCategoryBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ProductCategory/";
			UsedInProject = ProductCategoryBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
