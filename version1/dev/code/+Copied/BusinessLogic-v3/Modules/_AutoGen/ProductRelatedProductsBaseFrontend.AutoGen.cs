using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ProductRelatedProductsModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ProductRelatedProductsModule.ProductRelatedProductsBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ProductRelatedProductsModule.IProductRelatedProductsBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ProductRelatedProductsModule.ProductRelatedProductsBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ProductRelatedProductsModule.IProductRelatedProductsBase item)
        {
        	return BusinessLogic_v3.Frontend.ProductRelatedProductsModule.ProductRelatedProductsBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductRelatedProductsBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ProductRelatedProductsBaseFrontend, BusinessLogic_v3.Modules.ProductRelatedProductsModule.IProductRelatedProductsBase>

    {
		
        
        protected ProductRelatedProductsBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
