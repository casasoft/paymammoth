using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.EventSessionModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.EventSessionModule.EventSessionBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.EventSessionModule.IEventSessionBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.EventSessionModule.EventSessionBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.EventSessionModule.IEventSessionBase item)
        {
        	return BusinessLogic_v3.Frontend.EventSessionModule.EventSessionBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventSessionBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<EventSessionBaseFrontend, BusinessLogic_v3.Modules.EventSessionModule.IEventSessionBase>

    {
		
        
        protected EventSessionBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
