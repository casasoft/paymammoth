using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.AffiliateModule;
using BusinessLogic_v3.Cms.AffiliateModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class AffiliateBaseCmsFactory_AutoGen : CmsFactoryBase<AffiliateBaseCmsInfo, AffiliateBase>
    {
       
       public new static AffiliateBaseCmsFactory Instance
	    {
	         get
	         {
                 return (AffiliateBaseCmsFactory)CmsFactoryBase<AffiliateBaseCmsInfo, AffiliateBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = AffiliateBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Affiliate.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Affiliate";

            this.QueryStringParamID = "AffiliateId";

            cmsInfo.TitlePlural = "Affiliates";

            cmsInfo.TitleSingular =  "Affiliate";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public AffiliateBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Affiliate/";
			UsedInProject = AffiliateBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
