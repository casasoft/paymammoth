using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductVersionMediaItemModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ProductVersionMediaItem")]
    public abstract class ProductVersionMediaItemBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductVersionMediaItemBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ProductVersionMediaItemBaseFactory Factory
        {
            get
            {
                return ProductVersionMediaItemBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductVersionMediaItemBase CreateNewItem()
        {
            return (ProductVersionMediaItemBase)Factory.CreateNewItem();
        }

		public static ProductVersionMediaItemBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductVersionMediaItemBase, ProductVersionMediaItemBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductVersionMediaItemBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductVersionMediaItemBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductVersionMediaItemBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductVersionMediaItemBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductVersionMediaItemBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _imagefilename;
        
        public virtual string ImageFilename 
        {
        	get
        	{
        		return _imagefilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagefilename,value);
        		_imagefilename = value;
        		
        	}
        }
        
/*
		public virtual long? ProductVersionID
		{
		 	get 
		 	{
		 		return (this.ProductVersion != null ? (long?)this.ProductVersion.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase _productversion;
        public virtual BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase ProductVersion 
        {
        	get
        	{
        		return _productversion;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_productversion,value);
        		_productversion = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductVersionModule.IProductVersionBase BusinessLogic_v3.Modules._AutoGen.IProductVersionMediaItemBaseAutoGen.ProductVersion 
        {
            get
            {
            	return this.ProductVersion;
            }
            set
            {
            	this.ProductVersion = (BusinessLogic_v3.Modules.ProductVersionModule.ProductVersionBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _ismainimage;
        
        public virtual bool IsMainImage 
        {
        	get
        	{
        		return _ismainimage;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ismainimage,value);
        		_ismainimage = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _caption;
        
        public virtual string Caption 
        {
        	get
        	{
        		return _caption;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_caption,value);
        		_caption = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
