using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberResponsibleLimitsModule;
using BusinessLogic_v3.Cms.MemberResponsibleLimitsModule;
using BusinessLogic_v3.Frontend.MemberResponsibleLimitsModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberResponsibleLimitsBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase>
    {
		public MemberResponsibleLimitsBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBase dbItem)
            : base(BusinessLogic_v3.Cms.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberResponsibleLimitsBaseFrontend FrontendItem
        {
            get { return (MemberResponsibleLimitsBaseFrontend)base.FrontendItem; }

        }
        public new MemberResponsibleLimitsBase DbItem
        {
            get
            {
                return (MemberResponsibleLimitsBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Member { get; protected set; }

        public CmsPropertyInfo StartDate { get; protected set; }

        public CmsPropertyInfo Value { get; protected set; }

        public CmsPropertyInfo FrequencyType { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
