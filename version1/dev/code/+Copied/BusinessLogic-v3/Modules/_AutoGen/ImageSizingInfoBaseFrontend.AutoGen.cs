using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ImageSizingInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ImageSizingInfoModule.ImageSizingInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ImageSizingInfoModule.ImageSizingInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.ImageSizingInfoModule.ImageSizingInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ImageSizingInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ImageSizingInfoBaseFrontend, BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBase>

    {
		
        
        protected ImageSizingInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
