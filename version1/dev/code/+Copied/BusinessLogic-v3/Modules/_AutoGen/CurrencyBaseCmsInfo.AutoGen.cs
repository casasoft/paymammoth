using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CurrencyModule;
using BusinessLogic_v3.Cms.CurrencyModule;
using BusinessLogic_v3.Frontend.CurrencyModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CurrencyBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase>
    {
		public CurrencyBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase dbItem)
            : base(BusinessLogic_v3.Cms.CurrencyModule.CurrencyBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CurrencyBaseFrontend FrontendItem
        {
            get { return (CurrencyBaseFrontend)base.FrontendItem; }

        }
        public new CurrencyBase DbItem
        {
            get
            {
                return (CurrencyBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo CurrencyISOCode { get; protected set; }

        public CmsPropertyInfo Symbol { get; protected set; }

        public CmsPropertyInfo ExchangeRateMultiplier { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo AffiliatePaymentInfos { get; protected set; }
        

        public CmsCollectionInfo CultureDetailses { get; protected set; }
        

        public CmsCollectionInfo Orders { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.AffiliatePaymentInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase>.GetPropertyBySelector(item => item.AffiliatePaymentInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>.GetPropertyBySelector(item => item.Currency)));
		
		//InitCollectionBaseOneToMany
		this.CultureDetailses = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase>.GetPropertyBySelector(item => item.CultureDetailses),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>.GetPropertyBySelector(item => item.DefaultCurrency)));
		
		//InitCollectionBaseOneToMany
		this.Orders = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase>.GetPropertyBySelector(item => item.Orders),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector(item => item.OrderCurrency)));


			base.initBasicFields();
          
        }

    }
}
